package com.flurry.android.impl.ads.avro.protocol.v6;

import com.flurry.android.monolithic.sdk.impl.jg;
import com.flurry.android.monolithic.sdk.impl.ji;
import com.flurry.android.monolithic.sdk.impl.ke;
import com.flurry.android.monolithic.sdk.impl.nt;
import com.flurry.android.monolithic.sdk.impl.nu;
import com.flurry.android.monolithic.sdk.impl.nv;
import com.ideaworks3d.marmalade.s3eAndroidGooglePlayBilling.util.IabHelper;
import com.ideaworks3d.marmalade.s3eAndroidMarketBilling.s3eAndroidMarketBillingReceiver;
import java.util.List;
import java.util.Map;

public class AdRequest extends nu implements nt {
    public static final ji SCHEMA$ = new ke().a("{\"type\":\"record\",\"name\":\"AdRequest\",\"namespace\":\"com.flurry.android.impl.ads.avro.protocol.v6\",\"fields\":[{\"name\":\"apiKey\",\"type\":\"string\"},{\"name\":\"agentVersion\",\"type\":\"string\",\"default\":\"null\"},{\"name\":\"adSpaceName\",\"type\":\"string\"},{\"name\":\"sessionId\",\"type\":\"long\"},{\"name\":\"adReportedIds\",\"type\":{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"AdReportedId\",\"fields\":[{\"name\":\"type\",\"type\":\"int\"},{\"name\":\"id\",\"type\":\"bytes\"}]}}},{\"name\":\"location\",\"type\":{\"type\":\"record\",\"name\":\"Location\",\"fields\":[{\"name\":\"lat\",\"type\":\"float\",\"default\":0.0},{\"name\":\"lon\",\"type\":\"float\",\"default\":0.0}]},\"default\":\"null\"},{\"name\":\"testDevice\",\"type\":\"boolean\",\"default\":false},{\"name\":\"bindings\",\"type\":{\"type\":\"array\",\"items\":\"int\"}},{\"name\":\"adViewContainer\",\"type\":{\"type\":\"record\",\"name\":\"AdViewContainer\",\"fields\":[{\"name\":\"viewWidth\",\"type\":\"int\",\"default\":0},{\"name\":\"viewHeight\",\"type\":\"int\",\"default\":0},{\"name\":\"screenWidth\",\"type\":\"int\",\"default\":0},{\"name\":\"screenHeight\",\"type\":\"int\",\"default\":0},{\"name\":\"density\",\"type\":\"float\",\"default\":1.0}]},\"default\":\"null\"},{\"name\":\"locale\",\"type\":\"string\",\"default\":\"null\"},{\"name\":\"timezone\",\"type\":\"string\",\"default\":\"null\"},{\"name\":\"osVersion\",\"type\":\"string\",\"default\":\"null\"},{\"name\":\"devicePlatform\",\"type\":\"string\",\"default\":\"null\"},{\"name\":\"testAds\",\"type\":{\"type\":\"record\",\"name\":\"TestAds\",\"fields\":[{\"name\":\"adspacePlacement\",\"type\":\"int\",\"default\":0}]},\"default\":\"null\"},{\"name\":\"keywords\",\"type\":{\"type\":\"map\",\"values\":\"string\"},\"default\":[]},{\"name\":\"refresh\",\"type\":\"boolean\",\"default\":false},{\"name\":\"canDoSKAppStore\",\"type\":\"boolean\",\"default\":false},{\"name\":\"networkStatus\",\"type\":\"int\",\"default\":1},{\"name\":\"frequencyCapInfos\",\"type\":{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"FrequencyCapInfo\",\"fields\":[{\"name\":\"idHash\",\"type\":\"string\",\"default\":\"null\"},{\"name\":\"serveTime\",\"type\":\"long\"},{\"name\":\"expirationTime\",\"type\":\"long\"},{\"name\":\"views\",\"type\":\"int\"},{\"name\":\"newCap\",\"type\":\"int\"},{\"name\":\"previousCap\",\"type\":\"int\"},{\"name\":\"previousCapType\",\"type\":\"int\"}]}}},{\"name\":\"adTrackingEnabled\",\"type\":\"boolean\",\"default\":false},{\"name\":\"preferredLanguage\",\"type\":\"string\",\"default\":\"null\"},{\"name\":\"bcat\",\"type\":{\"type\":\"array\",\"items\":\"string\"},\"default\":[]}]}");
    @Deprecated
    public CharSequence a;
    @Deprecated
    public CharSequence b;
    @Deprecated
    public CharSequence c;
    @Deprecated
    public long d;
    @Deprecated
    public List<AdReportedId> e;
    @Deprecated
    public Location f;
    @Deprecated
    public boolean g;
    @Deprecated
    public List<Integer> h;
    @Deprecated
    public AdViewContainer i;
    @Deprecated
    public CharSequence j;
    @Deprecated
    public CharSequence k;
    @Deprecated
    public CharSequence l;
    @Deprecated
    public CharSequence m;
    @Deprecated
    public TestAds n;
    @Deprecated
    public Map<CharSequence, CharSequence> o;
    @Deprecated
    public boolean p;
    @Deprecated
    public boolean q;
    @Deprecated
    public int r;
    @Deprecated
    public List<FrequencyCapInfo> s;
    @Deprecated
    public boolean t;
    @Deprecated
    public CharSequence u;
    @Deprecated
    public List<CharSequence> v;

    public ji a() {
        return SCHEMA$;
    }

    public Object a(int i2) {
        switch (i2) {
            case 0:
                return this.a;
            case 1:
                return this.b;
            case 2:
                return this.c;
            case 3:
                return Long.valueOf(this.d);
            case 4:
                return this.e;
            case 5:
                return this.f;
            case 6:
                return Boolean.valueOf(this.g);
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED /*7*/:
                return this.h;
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED /*8*/:
                return this.i;
            case 9:
                return this.j;
            case 10:
                return this.k;
            case 11:
                return this.l;
            case 12:
                return this.m;
            case 13:
                return this.n;
            case 14:
                return this.o;
            case 15:
                return Boolean.valueOf(this.p);
            case s3eAndroidMarketBillingReceiver.S3E_ANDROIDMARKETBILLING_SECURITY_NO_NONCE /*16*/:
                return Boolean.valueOf(this.q);
            case 17:
                return Integer.valueOf(this.r);
            case 18:
                return this.s;
            case 19:
                return Boolean.valueOf(this.t);
            case 20:
                return this.u;
            case 21:
                return this.v;
            default:
                throw new jg("Bad index");
        }
    }

    public void a(int i2, Object obj) {
        switch (i2) {
            case 0:
                this.a = (CharSequence) obj;
                return;
            case 1:
                this.b = (CharSequence) obj;
                return;
            case 2:
                this.c = (CharSequence) obj;
                return;
            case 3:
                this.d = ((Long) obj).longValue();
                return;
            case 4:
                this.e = (List) obj;
                return;
            case 5:
                this.f = (Location) obj;
                return;
            case 6:
                this.g = ((Boolean) obj).booleanValue();
                return;
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED /*7*/:
                this.h = (List) obj;
                return;
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED /*8*/:
                this.i = (AdViewContainer) obj;
                return;
            case 9:
                this.j = (CharSequence) obj;
                return;
            case 10:
                this.k = (CharSequence) obj;
                return;
            case 11:
                this.l = (CharSequence) obj;
                return;
            case 12:
                this.m = (CharSequence) obj;
                return;
            case 13:
                this.n = (TestAds) obj;
                return;
            case 14:
                this.o = (Map) obj;
                return;
            case 15:
                this.p = ((Boolean) obj).booleanValue();
                return;
            case s3eAndroidMarketBillingReceiver.S3E_ANDROIDMARKETBILLING_SECURITY_NO_NONCE /*16*/:
                this.q = ((Boolean) obj).booleanValue();
                return;
            case 17:
                this.r = ((Integer) obj).intValue();
                return;
            case 18:
                this.s = (List) obj;
                return;
            case 19:
                this.t = ((Boolean) obj).booleanValue();
                return;
            case 20:
                this.u = (CharSequence) obj;
                return;
            case 21:
                this.v = (List) obj;
                return;
            default:
                throw new jg("Bad index");
        }
    }

    public void a(CharSequence charSequence) {
        this.c = charSequence;
    }

    public void a(TestAds testAds) {
        this.n = testAds;
    }

    public void a(Map<CharSequence, CharSequence> map) {
        this.o = map;
    }

    public void a(Boolean bool) {
        this.p = bool.booleanValue();
    }

    public static Builder b() {
        return new Builder();
    }

    public class Builder extends nv<AdRequest> {
        private CharSequence a;
        private CharSequence b;
        private CharSequence c;
        private long d;
        private List<AdReportedId> e;
        private Location f;
        private boolean g;
        private List<Integer> h;
        private AdViewContainer i;
        private CharSequence j;
        private CharSequence k;
        private CharSequence l;
        private CharSequence m;
        private TestAds n;
        private Map<CharSequence, CharSequence> o;
        private boolean p;
        private boolean q;
        private int r;
        private List<FrequencyCapInfo> s;
        private boolean t;
        private CharSequence u;
        private List<CharSequence> v;

        private Builder() {
            super(AdRequest.SCHEMA$);
        }

        public Builder a(CharSequence charSequence) {
            a(b()[0], charSequence);
            this.a = charSequence;
            c()[0] = true;
            return this;
        }

        public Builder b(CharSequence charSequence) {
            a(b()[1], charSequence);
            this.b = charSequence;
            c()[1] = true;
            return this;
        }

        public Builder c(CharSequence charSequence) {
            a(b()[2], charSequence);
            this.c = charSequence;
            c()[2] = true;
            return this;
        }

        public Builder a(long j2) {
            a(b()[3], Long.valueOf(j2));
            this.d = j2;
            c()[3] = true;
            return this;
        }

        public Builder a(List<AdReportedId> list) {
            a(b()[4], list);
            this.e = list;
            c()[4] = true;
            return this;
        }

        public Builder a(Location location) {
            a(b()[5], location);
            this.f = location;
            c()[5] = true;
            return this;
        }

        public Builder a(boolean z) {
            a(b()[6], Boolean.valueOf(z));
            this.g = z;
            c()[6] = true;
            return this;
        }

        public Builder b(List<Integer> list) {
            a(b()[7], list);
            this.h = list;
            c()[7] = true;
            return this;
        }

        public Builder a(AdViewContainer adViewContainer) {
            a(b()[8], adViewContainer);
            this.i = adViewContainer;
            c()[8] = true;
            return this;
        }

        public Builder d(CharSequence charSequence) {
            a(b()[9], charSequence);
            this.j = charSequence;
            c()[9] = true;
            return this;
        }

        public Builder e(CharSequence charSequence) {
            a(b()[10], charSequence);
            this.k = charSequence;
            c()[10] = true;
            return this;
        }

        public Builder f(CharSequence charSequence) {
            a(b()[11], charSequence);
            this.l = charSequence;
            c()[11] = true;
            return this;
        }

        public Builder g(CharSequence charSequence) {
            a(b()[12], charSequence);
            this.m = charSequence;
            c()[12] = true;
            return this;
        }

        public Builder b(boolean z) {
            a(b()[16], Boolean.valueOf(z));
            this.q = z;
            c()[16] = true;
            return this;
        }

        public Builder a(int i2) {
            a(b()[17], Integer.valueOf(i2));
            this.r = i2;
            c()[17] = true;
            return this;
        }

        public Builder c(List<FrequencyCapInfo> list) {
            a(b()[18], list);
            this.s = list;
            c()[18] = true;
            return this;
        }

        public Builder h(CharSequence charSequence) {
            a(b()[20], charSequence);
            this.u = charSequence;
            c()[20] = true;
            return this;
        }

        public Builder d(List<CharSequence> list) {
            a(b()[21], list);
            this.v = list;
            c()[21] = true;
            return this;
        }

        public AdRequest a() {
            try {
                AdRequest adRequest = new AdRequest();
                adRequest.a = c()[0] ? this.a : (CharSequence) a(b()[0]);
                adRequest.b = c()[1] ? this.b : (CharSequence) a(b()[1]);
                adRequest.c = c()[2] ? this.c : (CharSequence) a(b()[2]);
                adRequest.d = c()[3] ? this.d : ((Long) a(b()[3])).longValue();
                adRequest.e = c()[4] ? this.e : (List) a(b()[4]);
                adRequest.f = c()[5] ? this.f : (Location) a(b()[5]);
                adRequest.g = c()[6] ? this.g : ((Boolean) a(b()[6])).booleanValue();
                adRequest.h = c()[7] ? this.h : (List) a(b()[7]);
                adRequest.i = c()[8] ? this.i : (AdViewContainer) a(b()[8]);
                adRequest.j = c()[9] ? this.j : (CharSequence) a(b()[9]);
                adRequest.k = c()[10] ? this.k : (CharSequence) a(b()[10]);
                adRequest.l = c()[11] ? this.l : (CharSequence) a(b()[11]);
                adRequest.m = c()[12] ? this.m : (CharSequence) a(b()[12]);
                adRequest.n = c()[13] ? this.n : (TestAds) a(b()[13]);
                adRequest.o = c()[14] ? this.o : (Map) a(b()[14]);
                adRequest.p = c()[15] ? this.p : ((Boolean) a(b()[15])).booleanValue();
                adRequest.q = c()[16] ? this.q : ((Boolean) a(b()[16])).booleanValue();
                adRequest.r = c()[17] ? this.r : ((Integer) a(b()[17])).intValue();
                adRequest.s = c()[18] ? this.s : (List) a(b()[18]);
                adRequest.t = c()[19] ? this.t : ((Boolean) a(b()[19])).booleanValue();
                adRequest.u = c()[20] ? this.u : (CharSequence) a(b()[20]);
                adRequest.v = c()[21] ? this.v : (List) a(b()[21]);
                return adRequest;
            } catch (Exception e2) {
                throw new jg(e2);
            }
        }
    }
}
