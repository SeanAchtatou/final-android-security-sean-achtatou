package com.flurry.android.impl.ads.avro.protocol.v6;

import com.flurry.android.monolithic.sdk.impl.jg;
import com.flurry.android.monolithic.sdk.impl.ji;
import com.flurry.android.monolithic.sdk.impl.ke;
import com.flurry.android.monolithic.sdk.impl.nt;
import com.flurry.android.monolithic.sdk.impl.nu;
import com.flurry.android.monolithic.sdk.impl.nv;

public class Location extends nu implements nt {
    public static final ji SCHEMA$ = new ke().a("{\"type\":\"record\",\"name\":\"Location\",\"namespace\":\"com.flurry.android.impl.ads.avro.protocol.v6\",\"fields\":[{\"name\":\"lat\",\"type\":\"float\",\"default\":0.0},{\"name\":\"lon\",\"type\":\"float\",\"default\":0.0}]}");
    @Deprecated
    public float a;
    @Deprecated
    public float b;

    public ji a() {
        return SCHEMA$;
    }

    public Object a(int i) {
        switch (i) {
            case 0:
                return Float.valueOf(this.a);
            case 1:
                return Float.valueOf(this.b);
            default:
                throw new jg("Bad index");
        }
    }

    public void a(int i, Object obj) {
        switch (i) {
            case 0:
                this.a = ((Float) obj).floatValue();
                return;
            case 1:
                this.b = ((Float) obj).floatValue();
                return;
            default:
                throw new jg("Bad index");
        }
    }

    public static Builder b() {
        return new Builder();
    }

    public class Builder extends nv<Location> {
        private float a;
        private float b;

        private Builder() {
            super(Location.SCHEMA$);
        }

        public Builder a(float f) {
            a(b()[0], Float.valueOf(f));
            this.a = f;
            c()[0] = true;
            return this;
        }

        public Builder b(float f) {
            a(b()[1], Float.valueOf(f));
            this.b = f;
            c()[1] = true;
            return this;
        }

        public Location a() {
            try {
                Location location = new Location();
                location.a = c()[0] ? this.a : ((Float) a(b()[0])).floatValue();
                location.b = c()[1] ? this.b : ((Float) a(b()[1])).floatValue();
                return location;
            } catch (Exception e) {
                throw new jg(e);
            }
        }
    }
}
