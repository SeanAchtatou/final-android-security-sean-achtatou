package com.flurry.android.impl.ads.avro.protocol.v6;

import com.flurry.android.monolithic.sdk.impl.jg;
import com.flurry.android.monolithic.sdk.impl.ji;
import com.flurry.android.monolithic.sdk.impl.ke;
import com.flurry.android.monolithic.sdk.impl.nt;
import com.flurry.android.monolithic.sdk.impl.nu;
import com.flurry.android.monolithic.sdk.impl.nv;

public class TestAds extends nu implements nt {
    public static final ji SCHEMA$ = new ke().a("{\"type\":\"record\",\"name\":\"TestAds\",\"namespace\":\"com.flurry.android.impl.ads.avro.protocol.v6\",\"fields\":[{\"name\":\"adspacePlacement\",\"type\":\"int\",\"default\":0}]}");
    @Deprecated
    public int a;

    public ji a() {
        return SCHEMA$;
    }

    public Object a(int i) {
        switch (i) {
            case 0:
                break;
            default:
                throw new jg("Bad index");
        }
        return Integer.valueOf(this.a);
    }

    public void a(int i, Object obj) {
        switch (i) {
            case 0:
                break;
            default:
                throw new jg("Bad index");
        }
        this.a = ((Integer) obj).intValue();
    }

    public static Builder b() {
        return new Builder();
    }

    public class Builder extends nv<TestAds> {
        private int a;

        private Builder() {
            super(TestAds.SCHEMA$);
        }

        public Builder a(int i) {
            a(b()[0], Integer.valueOf(i));
            this.a = i;
            c()[0] = true;
            return this;
        }

        public TestAds a() {
            try {
                TestAds testAds = new TestAds();
                testAds.a = c()[0] ? this.a : ((Integer) a(b()[0])).intValue();
                return testAds;
            } catch (Exception e) {
                throw new jg(e);
            }
        }
    }
}
