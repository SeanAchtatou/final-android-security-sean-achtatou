package com.flurry.android.impl.ads.avro.protocol.v6;

import com.flurry.android.monolithic.sdk.impl.jg;
import com.flurry.android.monolithic.sdk.impl.ji;
import com.flurry.android.monolithic.sdk.impl.ke;
import com.flurry.android.monolithic.sdk.impl.nt;
import com.flurry.android.monolithic.sdk.impl.nu;
import com.flurry.android.monolithic.sdk.impl.nv;

public class AdSpaceLayout extends nu implements nt {
    public static final ji SCHEMA$ = new ke().a("{\"type\":\"record\",\"name\":\"AdSpaceLayout\",\"namespace\":\"com.flurry.android.impl.ads.avro.protocol.v6\",\"fields\":[{\"name\":\"adWidth\",\"type\":\"int\"},{\"name\":\"adHeight\",\"type\":\"int\"},{\"name\":\"fix\",\"type\":\"string\"},{\"name\":\"format\",\"type\":\"string\"},{\"name\":\"alignment\",\"type\":\"string\"}]}");
    @Deprecated
    public int a;
    @Deprecated
    public int b;
    @Deprecated
    public CharSequence c;
    @Deprecated
    public CharSequence d;
    @Deprecated
    public CharSequence e;

    public ji a() {
        return SCHEMA$;
    }

    public Object a(int i) {
        switch (i) {
            case 0:
                return Integer.valueOf(this.a);
            case 1:
                return Integer.valueOf(this.b);
            case 2:
                return this.c;
            case 3:
                return this.d;
            case 4:
                return this.e;
            default:
                throw new jg("Bad index");
        }
    }

    public void a(int i, Object obj) {
        switch (i) {
            case 0:
                this.a = ((Integer) obj).intValue();
                return;
            case 1:
                this.b = ((Integer) obj).intValue();
                return;
            case 2:
                this.c = (CharSequence) obj;
                return;
            case 3:
                this.d = (CharSequence) obj;
                return;
            case 4:
                this.e = (CharSequence) obj;
                return;
            default:
                throw new jg("Bad index");
        }
    }

    public Integer b() {
        return Integer.valueOf(this.a);
    }

    public Integer c() {
        return Integer.valueOf(this.b);
    }

    public CharSequence d() {
        return this.c;
    }

    public CharSequence e() {
        return this.d;
    }

    public CharSequence f() {
        return this.e;
    }

    public class Builder extends nv<AdSpaceLayout> {
        private Builder() {
            super(AdSpaceLayout.SCHEMA$);
        }
    }
}
