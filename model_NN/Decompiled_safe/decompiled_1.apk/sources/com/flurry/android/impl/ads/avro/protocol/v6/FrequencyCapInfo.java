package com.flurry.android.impl.ads.avro.protocol.v6;

import com.flurry.android.monolithic.sdk.impl.jg;
import com.flurry.android.monolithic.sdk.impl.ji;
import com.flurry.android.monolithic.sdk.impl.ke;
import com.flurry.android.monolithic.sdk.impl.nt;
import com.flurry.android.monolithic.sdk.impl.nu;
import com.flurry.android.monolithic.sdk.impl.nv;

public class FrequencyCapInfo extends nu implements nt {
    public static final ji SCHEMA$ = new ke().a("{\"type\":\"record\",\"name\":\"FrequencyCapInfo\",\"namespace\":\"com.flurry.android.impl.ads.avro.protocol.v6\",\"fields\":[{\"name\":\"idHash\",\"type\":\"string\",\"default\":\"null\"},{\"name\":\"serveTime\",\"type\":\"long\"},{\"name\":\"expirationTime\",\"type\":\"long\"},{\"name\":\"views\",\"type\":\"int\"},{\"name\":\"newCap\",\"type\":\"int\"},{\"name\":\"previousCap\",\"type\":\"int\"},{\"name\":\"previousCapType\",\"type\":\"int\"}]}");
    @Deprecated
    public CharSequence a;
    @Deprecated
    public long b;
    @Deprecated
    public long c;
    @Deprecated
    public int d;
    @Deprecated
    public int e;
    @Deprecated
    public int f;
    @Deprecated
    public int g;

    public ji a() {
        return SCHEMA$;
    }

    public Object a(int i) {
        switch (i) {
            case 0:
                return this.a;
            case 1:
                return Long.valueOf(this.b);
            case 2:
                return Long.valueOf(this.c);
            case 3:
                return Integer.valueOf(this.d);
            case 4:
                return Integer.valueOf(this.e);
            case 5:
                return Integer.valueOf(this.f);
            case 6:
                return Integer.valueOf(this.g);
            default:
                throw new jg("Bad index");
        }
    }

    public void a(int i, Object obj) {
        switch (i) {
            case 0:
                this.a = (CharSequence) obj;
                return;
            case 1:
                this.b = ((Long) obj).longValue();
                return;
            case 2:
                this.c = ((Long) obj).longValue();
                return;
            case 3:
                this.d = ((Integer) obj).intValue();
                return;
            case 4:
                this.e = ((Integer) obj).intValue();
                return;
            case 5:
                this.f = ((Integer) obj).intValue();
                return;
            case 6:
                this.g = ((Integer) obj).intValue();
                return;
            default:
                throw new jg("Bad index");
        }
    }

    public static Builder b() {
        return new Builder();
    }

    public class Builder extends nv<FrequencyCapInfo> {
        private CharSequence a;
        private long b;
        private long c;
        private int d;
        private int e;
        private int f;
        private int g;

        private Builder() {
            super(FrequencyCapInfo.SCHEMA$);
        }

        public Builder a(CharSequence charSequence) {
            a(b()[0], charSequence);
            this.a = charSequence;
            c()[0] = true;
            return this;
        }

        public Builder a(long j) {
            a(b()[1], Long.valueOf(j));
            this.b = j;
            c()[1] = true;
            return this;
        }

        public Builder b(long j) {
            a(b()[2], Long.valueOf(j));
            this.c = j;
            c()[2] = true;
            return this;
        }

        public Builder a(int i) {
            a(b()[3], Integer.valueOf(i));
            this.d = i;
            c()[3] = true;
            return this;
        }

        public Builder b(int i) {
            a(b()[4], Integer.valueOf(i));
            this.e = i;
            c()[4] = true;
            return this;
        }

        public Builder c(int i) {
            a(b()[5], Integer.valueOf(i));
            this.f = i;
            c()[5] = true;
            return this;
        }

        public Builder d(int i) {
            a(b()[6], Integer.valueOf(i));
            this.g = i;
            c()[6] = true;
            return this;
        }

        public FrequencyCapInfo a() {
            try {
                FrequencyCapInfo frequencyCapInfo = new FrequencyCapInfo();
                frequencyCapInfo.a = c()[0] ? this.a : (CharSequence) a(b()[0]);
                frequencyCapInfo.b = c()[1] ? this.b : ((Long) a(b()[1])).longValue();
                frequencyCapInfo.c = c()[2] ? this.c : ((Long) a(b()[2])).longValue();
                frequencyCapInfo.d = c()[3] ? this.d : ((Integer) a(b()[3])).intValue();
                frequencyCapInfo.e = c()[4] ? this.e : ((Integer) a(b()[4])).intValue();
                frequencyCapInfo.f = c()[5] ? this.f : ((Integer) a(b()[5])).intValue();
                frequencyCapInfo.g = c()[6] ? this.g : ((Integer) a(b()[6])).intValue();
                return frequencyCapInfo;
            } catch (Exception e2) {
                throw new jg(e2);
            }
        }
    }
}
