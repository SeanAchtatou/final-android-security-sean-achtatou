package com.flurry.android.impl.ads.avro.protocol.v6;

import com.flurry.android.monolithic.sdk.impl.jg;
import com.flurry.android.monolithic.sdk.impl.ji;
import com.flurry.android.monolithic.sdk.impl.ke;
import com.flurry.android.monolithic.sdk.impl.nt;
import com.flurry.android.monolithic.sdk.impl.nu;
import com.flurry.android.monolithic.sdk.impl.nv;
import java.util.List;

public class SdkLogRequest extends nu implements nt {
    public static final ji SCHEMA$ = new ke().a("{\"type\":\"record\",\"name\":\"SdkLogRequest\",\"namespace\":\"com.flurry.android.impl.ads.avro.protocol.v6\",\"fields\":[{\"name\":\"apiKey\",\"type\":\"string\"},{\"name\":\"adReportedIds\",\"type\":{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"AdReportedId\",\"fields\":[{\"name\":\"type\",\"type\":\"int\"},{\"name\":\"id\",\"type\":\"bytes\"}]}}},{\"name\":\"sdkAdLogs\",\"type\":{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"SdkAdLog\",\"fields\":[{\"name\":\"sessionId\",\"type\":\"long\"},{\"name\":\"adLogGUID\",\"type\":\"string\"},{\"name\":\"sdkAdEvents\",\"type\":{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"SdkAdEvent\",\"fields\":[{\"name\":\"type\",\"type\":\"string\"},{\"name\":\"params\",\"type\":{\"type\":\"map\",\"values\":\"string\"}},{\"name\":\"timeOffset\",\"type\":\"long\"}]}}}]}}},{\"name\":\"agentTimestamp\",\"type\":\"long\"},{\"name\":\"testDevice\",\"type\":\"boolean\",\"default\":false}]}");
    @Deprecated
    public CharSequence a;
    @Deprecated
    public List<AdReportedId> b;
    @Deprecated
    public List<SdkAdLog> c;
    @Deprecated
    public long d;
    @Deprecated
    public boolean e;

    public ji a() {
        return SCHEMA$;
    }

    public Object a(int i) {
        switch (i) {
            case 0:
                return this.a;
            case 1:
                return this.b;
            case 2:
                return this.c;
            case 3:
                return Long.valueOf(this.d);
            case 4:
                return Boolean.valueOf(this.e);
            default:
                throw new jg("Bad index");
        }
    }

    public void a(int i, Object obj) {
        switch (i) {
            case 0:
                this.a = (CharSequence) obj;
                return;
            case 1:
                this.b = (List) obj;
                return;
            case 2:
                this.c = (List) obj;
                return;
            case 3:
                this.d = ((Long) obj).longValue();
                return;
            case 4:
                this.e = ((Boolean) obj).booleanValue();
                return;
            default:
                throw new jg("Bad index");
        }
    }

    public static Builder b() {
        return new Builder();
    }

    public class Builder extends nv<SdkLogRequest> {
        private CharSequence a;
        private List<AdReportedId> b;
        private List<SdkAdLog> c;
        private long d;
        private boolean e;

        private Builder() {
            super(SdkLogRequest.SCHEMA$);
        }

        public Builder a(CharSequence charSequence) {
            a(b()[0], charSequence);
            this.a = charSequence;
            c()[0] = true;
            return this;
        }

        public Builder a(List<AdReportedId> list) {
            a(b()[1], list);
            this.b = list;
            c()[1] = true;
            return this;
        }

        public Builder b(List<SdkAdLog> list) {
            a(b()[2], list);
            this.c = list;
            c()[2] = true;
            return this;
        }

        public Builder a(long j) {
            a(b()[3], Long.valueOf(j));
            this.d = j;
            c()[3] = true;
            return this;
        }

        public Builder a(boolean z) {
            a(b()[4], Boolean.valueOf(z));
            this.e = z;
            c()[4] = true;
            return this;
        }

        public SdkLogRequest a() {
            try {
                SdkLogRequest sdkLogRequest = new SdkLogRequest();
                sdkLogRequest.a = c()[0] ? this.a : (CharSequence) a(b()[0]);
                sdkLogRequest.b = c()[1] ? this.b : (List) a(b()[1]);
                sdkLogRequest.c = c()[2] ? this.c : (List) a(b()[2]);
                sdkLogRequest.d = c()[3] ? this.d : ((Long) a(b()[3])).longValue();
                sdkLogRequest.e = c()[4] ? this.e : ((Boolean) a(b()[4])).booleanValue();
                return sdkLogRequest;
            } catch (Exception e2) {
                throw new jg(e2);
            }
        }
    }
}
