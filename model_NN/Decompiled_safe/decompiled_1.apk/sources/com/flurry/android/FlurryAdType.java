package com.flurry.android;

public enum FlurryAdType {
    WEB_BANNER,
    WEB_TAKEOVER,
    VIDEO_TAKEOVER
}
