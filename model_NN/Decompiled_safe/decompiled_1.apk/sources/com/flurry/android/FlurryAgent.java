package com.flurry.android;

import android.content.Context;
import android.location.Criteria;
import com.flurry.android.monolithic.sdk.impl.eg;
import com.flurry.android.monolithic.sdk.impl.ia;
import com.flurry.android.monolithic.sdk.impl.ib;
import com.flurry.android.monolithic.sdk.impl.ig;
import com.flurry.android.monolithic.sdk.impl.ij;
import com.flurry.android.monolithic.sdk.impl.ja;
import com.flurry.android.monolithic.sdk.impl.je;
import java.util.Date;
import java.util.Map;

public final class FlurryAgent {
    private static final String a = FlurryAgent.class.getSimpleName();

    private FlurryAgent() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.Object, java.lang.Object):boolean
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, com.flurry.android.monolithic.sdk.impl.id):void
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void */
    public static void setVersionName(String str) {
        if (str == null) {
            ja.b(a, "String versionName passed to setVersionName was null.");
        } else {
            ib.a().a("VesionName", (Object) str);
        }
    }

    public static int getAgentVersion() {
        return eg.a().b();
    }

    public static String getReleaseVersion() {
        return eg.a().c();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Boolean]
     candidates:
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.Object, java.lang.Object):boolean
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, com.flurry.android.monolithic.sdk.impl.id):void
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void */
    public static void setReportLocation(boolean z) {
        ib.a().a("ReportLocation", (Object) Boolean.valueOf(z));
    }

    public static void setLogEnabled(boolean z) {
        if (z) {
            ja.b();
        } else {
            ja.a();
        }
    }

    public static void setLogLevel(int i) {
        ja.a(i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.Object, java.lang.Object):boolean
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, com.flurry.android.monolithic.sdk.impl.id):void
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void */
    public static void setContinueSessionMillis(long j) {
        if (j < 5000) {
            ja.b(a, "Invalid time set for session resumption: " + j);
        } else {
            ib.a().a("ContinueSessionMillis", (Object) Long.valueOf(j));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Boolean]
     candidates:
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.Object, java.lang.Object):boolean
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, com.flurry.android.monolithic.sdk.impl.id):void
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void */
    public static void setLogEvents(boolean z) {
        ib.a().a("LogEvents", (Object) Boolean.valueOf(z));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Boolean]
     candidates:
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.Object, java.lang.Object):boolean
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, com.flurry.android.monolithic.sdk.impl.id):void
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void */
    public static void setUseHttps(boolean z) {
        ib.a().a("UseHttps", (Object) Boolean.valueOf(z));
    }

    public static boolean getUseHttps() {
        return ((Boolean) ib.a().a("UseHttps")).booleanValue();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Boolean]
     candidates:
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.Object, java.lang.Object):boolean
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, com.flurry.android.monolithic.sdk.impl.id):void
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void */
    public static void setCaptureUncaughtExceptions(boolean z) {
        ib.a().a("CaptureUncaughtExceptions", (Object) Boolean.valueOf(z));
    }

    public static void onStartSession(Context context, String str) {
        if (context == null) {
            throw new NullPointerException("Null context");
        } else if (str == null || str.length() == 0) {
            throw new IllegalArgumentException("Api key not specified");
        } else {
            ia.a(context);
            try {
                eg.a().a(context, str);
            } catch (Throwable th) {
                ja.b(a, "", th);
            }
            ij.a().a(context);
            ig.a().f(context);
        }
    }

    public static void onEndSession(Context context) {
        if (context == null) {
            throw new NullPointerException("Null context");
        }
        ij.a().b(context);
        ig.a().g(context);
        try {
            eg.a().a(context);
        } catch (Throwable th) {
            ja.b(a, "", th);
        }
    }

    public static void logEvent(String str) {
        if (str == null) {
            ja.b(a, "String eventId passed to logEvent was null.");
            return;
        }
        try {
            eg.a().a(str);
        } catch (Throwable th) {
            ja.b(a, "Failed to log event: " + str, th);
        }
    }

    public static void logEvent(String str, Map<String, String> map) {
        if (str == null) {
            ja.b(a, "String eventId passed to logEvent was null.");
        } else if (map == null) {
            ja.b(a, "String parameters passed to logEvent was null.");
        } else {
            try {
                eg.a().a(str, map);
            } catch (Throwable th) {
                ja.b(a, "Failed to log event: " + str, th);
            }
        }
    }

    public static void logEvent(String str, boolean z) {
        if (str == null) {
            ja.b(a, "String eventId passed to logEvent was null.");
            return;
        }
        try {
            eg.a().a(str, z);
        } catch (Throwable th) {
            ja.b(a, "Failed to log event: " + str, th);
        }
    }

    public static void logEvent(String str, Map<String, String> map, boolean z) {
        if (str == null) {
            ja.b(a, "String eventId passed to logEvent was null.");
        } else if (map == null) {
            ja.b(a, "String parameters passed to logEvent was null.");
        } else {
            try {
                eg.a().a(str, map, z);
            } catch (Throwable th) {
                ja.b(a, "Failed to log event: " + str, th);
            }
        }
    }

    public static void endTimedEvent(String str) {
        if (str == null) {
            ja.b(a, "String eventId passed to endTimedEvent was null.");
            return;
        }
        try {
            eg.a().b(str);
        } catch (Throwable th) {
            ja.b(a, "Failed to signify the end of event: " + str, th);
        }
    }

    public static void endTimedEvent(String str, Map<String, String> map) {
        if (str == null) {
            ja.b(a, "String eventId passed to endTimedEvent was null.");
        } else if (map == null) {
            ja.b(a, "String eventId passed to endTimedEvent was null.");
        } else {
            try {
                eg.a().b(str, map);
            } catch (Throwable th) {
                ja.b(a, "Failed to signify the end of event: " + str, th);
            }
        }
    }

    @Deprecated
    public static void onError(String str, String str2, String str3) {
        if (str == null) {
            ja.b(a, "String errorId passed to onError was null.");
        } else if (str2 == null) {
            ja.b(a, "String message passed to onError was null.");
        } else if (str3 == null) {
            ja.b(a, "String errorClass passed to onError was null.");
        } else {
            try {
                eg.a().a(str, str2, str3);
            } catch (Throwable th) {
                ja.b(a, "", th);
            }
        }
    }

    public static void onError(String str, String str2, Throwable th) {
        if (str == null) {
            ja.b(a, "String errorId passed to onError was null.");
        } else if (str2 == null) {
            ja.b(a, "String message passed to onError was null.");
        } else if (th == null) {
            ja.b(a, "Throwable passed to onError was null.");
        } else {
            try {
                eg.a().a(str, str2, th);
            } catch (Throwable th2) {
                ja.b(a, "", th2);
            }
        }
    }

    @Deprecated
    public static void onEvent(String str) {
        if (str == null) {
            ja.b(a, "String eventId passed to onEvent was null.");
            return;
        }
        try {
            eg.a().c(str);
        } catch (Throwable th) {
            ja.b(a, "", th);
        }
    }

    @Deprecated
    public static void onEvent(String str, Map<String, String> map) {
        if (str == null) {
            ja.b(a, "String eventId passed to onEvent was null.");
        } else if (map == null) {
            ja.b(a, "Parameters Map passed to onEvent was null.");
        } else {
            try {
                eg.a().c(str, map);
            } catch (Throwable th) {
                ja.b(a, "", th);
            }
        }
    }

    public static void onPageView() {
        try {
            eg.a().e();
        } catch (Throwable th) {
            ja.b(a, "", th);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.Object, java.lang.Object):boolean
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, com.flurry.android.monolithic.sdk.impl.id):void
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void */
    public static void setReportUrl(String str) {
        ib.a().a("ReportUrl", (Object) str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, android.location.Criteria]
     candidates:
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.Object, java.lang.Object):boolean
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, com.flurry.android.monolithic.sdk.impl.id):void
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void */
    public static void setLocationCriteria(Criteria criteria) {
        ib.a().a("LocationCriteria", (Object) criteria);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.Object, java.lang.Object):boolean
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, com.flurry.android.monolithic.sdk.impl.id):void
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void */
    public static void setAge(int i) {
        if (i > 0 && i < 110) {
            ib.a().a("Age", (Object) Long.valueOf(new Date(new Date(System.currentTimeMillis() - (((long) i) * 31449600000L)).getYear(), 1, 1).getTime()));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, byte]
     candidates:
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.Object, java.lang.Object):boolean
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, com.flurry.android.monolithic.sdk.impl.id):void
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Byte]
     candidates:
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.Object, java.lang.Object):boolean
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, com.flurry.android.monolithic.sdk.impl.id):void
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void */
    public static void setGender(byte b) {
        switch (b) {
            case 0:
            case 1:
                ib.a().a("Gender", (Object) Byte.valueOf(b));
                return;
            default:
                ib.a().a("Gender", (Object) (byte) -1);
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.Object, java.lang.Object):boolean
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, com.flurry.android.monolithic.sdk.impl.id):void
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void */
    public static void setUserId(String str) {
        if (str == null) {
            ja.b(a, "String userId passed to setUserId was null.");
        } else {
            ib.a().a("UserId", (Object) je.a(str));
        }
    }
}
