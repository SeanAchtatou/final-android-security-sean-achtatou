package com.flurry.android;

public class FlurryWalletInfo {
    private final String a;
    private final float b;

    public FlurryWalletInfo(String str, float f) {
        this.a = str;
        this.b = f;
    }

    public String getCurrencyKey() {
        return this.a;
    }

    public float getCurrencyAmount() {
        return this.b;
    }
}
