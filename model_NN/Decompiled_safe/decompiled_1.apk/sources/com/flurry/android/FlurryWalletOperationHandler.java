package com.flurry.android;

public interface FlurryWalletOperationHandler {
    void onError(FlurryWalletError flurryWalletError);

    void onOperationSucceed();
}
