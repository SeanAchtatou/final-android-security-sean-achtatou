package com.flurry.android;

public class FlurryWalletError {
    private int a;
    private String b;

    public FlurryWalletError(int i, String str) {
        this.a = i;
        this.b = str;
    }

    public int getErrorCode() {
        return this.a;
    }

    public String getErrorMessage() {
        return this.b;
    }
}
