package com.flurry.android;

public interface FlurryWalletHandler {
    void onError(FlurryWalletError flurryWalletError);

    void onValueUpdated(FlurryWalletInfo flurryWalletInfo);
}
