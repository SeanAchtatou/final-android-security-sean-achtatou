package com.flurry.android;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.FrameLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.VideoView;
import com.flurry.android.impl.ads.FlurryAdModule;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;
import com.flurry.android.monolithic.sdk.impl.ar;
import com.flurry.android.monolithic.sdk.impl.ay;
import com.flurry.android.monolithic.sdk.impl.b;
import com.flurry.android.monolithic.sdk.impl.c;
import com.flurry.android.monolithic.sdk.impl.cq;
import com.flurry.android.monolithic.sdk.impl.d;
import com.flurry.android.monolithic.sdk.impl.ja;
import com.flurry.android.monolithic.sdk.impl.n;
import com.flurry.android.monolithic.sdk.impl.o;
import java.util.Collections;

public final class FlurryFullscreenTakeoverActivity extends Activity implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener {
    public static final String EXTRA_KEY_ADSPACENAME = "adSpaceName";
    public static final String EXTRA_KEY_FRAMEINDEX = "frameIndex";
    public static final String EXTRA_KEY_TARGETINTENT = "targetIntent";
    public static final String EXTRA_KEY_URL = "url";
    private static final String a = FlurryFullscreenTakeoverActivity.class.getSimpleName();
    private o b;
    private AdUnit c;
    private ViewGroup d;
    private ar e;
    private ProgressDialog f;
    /* access modifiers changed from: private */
    public VideoView g;
    /* access modifiers changed from: private */
    public boolean h;
    private MediaController i;
    private boolean j;
    private Intent k;
    private long l;
    /* access modifiers changed from: private */
    public String m;
    /* access modifiers changed from: private */
    public ay n = ay.WEB_RESULT_UNKNOWN;

    public void onCreate(Bundle bundle) {
        int i2;
        int i3;
        ja.a(3, a, "onCreate");
        setTheme(16973831);
        super.onCreate(bundle);
        cq.a(getWindow());
        Intent intent = getIntent();
        this.k = (Intent) intent.getParcelableExtra(EXTRA_KEY_TARGETINTENT);
        this.m = intent.getStringExtra(EXTRA_KEY_ADSPACENAME);
        if (this.k != null) {
            try {
                startActivity(this.k);
                this.l = SystemClock.elapsedRealtime();
            } catch (ActivityNotFoundException e2) {
                ja.b(a, "Cannot launch Activity", e2);
                this.k = null;
                finish();
            }
        } else {
            String stringExtra = intent.getStringExtra(EXTRA_KEY_URL);
            if (stringExtra == null) {
                this.d = new RelativeLayout(this);
                if (bundle == null) {
                    i2 = -1;
                } else {
                    i2 = bundle.getInt(EXTRA_KEY_FRAMEINDEX, -1);
                }
                if (i2 < 0) {
                    i3 = intent.getIntExtra(EXTRA_KEY_FRAMEINDEX, 0);
                } else {
                    i3 = i2;
                }
                FlurryAdModule instance = FlurryAdModule.getInstance();
                this.c = instance.H();
                if (this.c != null) {
                    this.b = new o(this, instance, instance.G(), this.c, i3);
                    this.b.initLayout();
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                    layoutParams.addRule(13);
                    this.d.addView(this.b, layoutParams);
                    setContentView(this.d);
                } else {
                    ja.a(3, a, "appSpotModule.getTakeoverAdUnit() IS null ");
                }
            } else {
                this.d = new FrameLayout(this);
                setContentView(this.d);
                if (a(stringExtra)) {
                    this.g = new n(this);
                    c(stringExtra);
                } else {
                    this.e = new ar(this, stringExtra);
                    this.e.setBasicWebViewUrlLoadingHandler(new d(this));
                    this.e.setBasicWebViewClosingHandler(new b(this));
                    this.e.setBasicWebViewFullScreenTransitionHandler(new c(this));
                    this.d.addView(this.e);
                }
            }
        }
        FlurryAdModule.getInstance().a(this, bundle);
    }

    public void onStart() {
        ja.a(3, a, "onStart");
        super.onStart();
        if (FlurryAdModule.getInstance().f() != null) {
            FlurryAgent.onStartSession(this, FlurryAdModule.getInstance().f());
        } else {
            finish();
        }
    }

    public void onStop() {
        ja.a(3, a, "onStop");
        FlurryAgent.onEndSession(this);
        super.onStop();
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (this.b != null) {
            bundle.putInt(EXTRA_KEY_FRAMEINDEX, this.b.getAdFrameIndex());
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        ja.a(3, a, "onConfigurationChange");
        super.onConfigurationChanged(configuration);
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        ja.a(3, a, "onRestart");
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        ja.a(3, a, "onResume");
        super.onResume();
        this.j = true;
        if (this.i != null) {
            this.i.show(0);
        }
        if (this.b != null) {
            this.b.b();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        ja.a(3, a, "onPause");
        super.onPause();
        this.j = false;
        if (this.g != null && this.g.isPlaying()) {
            this.g.pause();
        }
        if (this.b != null) {
            this.b.c();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        ja.a(3, a, "onDestroy");
        a();
        if (this.e != null) {
            this.e.c();
        }
        if (this.b != null) {
            this.b.stop();
        }
        FlurryAdModule.getInstance().b((Activity) this);
        super.onDestroy();
    }

    public void onWindowFocusChanged(boolean z) {
        ja.a(3, a, "onWindowFocusChanged hasFocus = " + z);
        if (this.j && z && this.k != null && SystemClock.elapsedRealtime() - this.l > 250) {
            ja.a(3, a, "terminate this launcher activity because launched activity was terminated or wasn't launched");
            finish();
        }
        super.onWindowFocusChanged(z);
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            if (this.b != null) {
                this.b.a("adWillClose", Collections.emptyMap(), this.b.getAdUnit(), this.b.getAdLog(), this.b.getAdFrameIndex(), 0);
                return true;
            } else if (this.g != null) {
                if (this.e != null) {
                    if (!this.h) {
                        a();
                        return true;
                    } else if (this.e.a()) {
                        this.e.b();
                        a();
                        return true;
                    }
                }
            } else if (this.e != null) {
                if (this.e.a()) {
                    this.e.b();
                } else {
                    this.n = ay.WEB_RESULT_BACK;
                    finish();
                }
                return true;
            }
        }
        return super.onKeyUp(i2, keyEvent);
    }

    public boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        ja.a(3, a, "onError");
        ja.b(a, "Error occurs during video playback");
        if (this.e == null) {
            finish();
            return true;
        } else if (!this.h) {
            a();
            return true;
        } else if (this.e.a()) {
            this.e.b();
            a();
            return true;
        } else {
            finish();
            return true;
        }
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        ja.a(3, a, "onCompletion");
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        ja.a(3, a, "onPrepared");
        if (this.f != null && this.f.isShowing()) {
            this.f.dismiss();
        }
        if (this.g != null && this.j) {
            this.g.start();
        }
    }

    /* access modifiers changed from: private */
    public boolean a(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        String mimeTypeFromExtension = MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(str));
        if (mimeTypeFromExtension == null || !mimeTypeFromExtension.startsWith("video/")) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public boolean b(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        Uri parse = Uri.parse(str);
        if (parse.getScheme() == null || !parse.getScheme().equals("market")) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public boolean a(String str, String str2) {
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            return false;
        }
        String queryParameter = Uri.parse(str2).getQueryParameter("link");
        if (TextUtils.isEmpty(queryParameter) || !queryParameter.equalsIgnoreCase(str)) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        if (!TextUtils.isEmpty(str) && this.g != null && this.d != null) {
            this.i = new MediaController(this);
            this.g.setOnPreparedListener(this);
            this.g.setOnCompletionListener(this);
            this.g.setOnErrorListener(this);
            this.g.setMediaController(this.i);
            this.g.setVideoURI(Uri.parse(str));
            this.d.addView(this.g, new FrameLayout.LayoutParams(-1, -1, 17));
            this.f = new ProgressDialog(this);
            this.f.setProgressStyle(0);
            this.f.setMessage("Loading...");
            this.f.setCancelable(true);
            this.f.show();
            if (this.e != null) {
                this.e.setVisibility(8);
            }
        }
    }

    private void a() {
        if (this.f != null) {
            if (this.f.isShowing()) {
                this.f.dismiss();
            }
            this.f = null;
        }
        if (this.e != null) {
            this.e.setVisibility(0);
        }
        if (this.g != null) {
            if (this.g.isPlaying()) {
                this.g.stopPlayback();
            }
            if (this.d != null) {
                this.d.removeView(this.g);
            }
            this.g = null;
        }
        this.h = false;
        this.i = null;
    }

    public AdUnit getAdUnit() {
        return this.c;
    }

    public o getAdUnityView() {
        return this.b;
    }

    public ay getResult() {
        return this.n;
    }
}
