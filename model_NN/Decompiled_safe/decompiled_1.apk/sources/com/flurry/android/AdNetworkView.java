package com.flurry.android;

import android.content.Context;
import com.flurry.android.impl.ads.FlurryAdModule;
import com.flurry.android.monolithic.sdk.impl.ac;
import com.flurry.android.monolithic.sdk.impl.m;
import java.util.Map;

public abstract class AdNetworkView extends ac {
    private final AdCreative a;

    public AdNetworkView(Context context, FlurryAdModule flurryAdModule, m mVar, AdCreative adCreative) {
        super(context, flurryAdModule, mVar);
        this.a = adCreative;
    }

    public AdNetworkView(Context context, AdCreative adCreative) {
        super(context, null, null);
        this.a = adCreative;
    }

    public AdCreative getAdCreative() {
        return this.a;
    }

    public void onAdFilled(Map<String, String> map) {
        super.onEvent("filled", map);
    }

    public void onAdUnFilled(Map<String, String> map) {
        super.onEvent("unfilled", map);
    }

    public void onAdShown(Map<String, String> map) {
        super.onEvent("rendered", map);
    }

    public void onAdClicked(Map<String, String> map) {
        super.onEvent("clicked", map);
    }

    public void onAdClosed(Map<String, String> map) {
        super.onEvent("adClosed", map);
    }

    public void onRenderFailed(Map<String, String> map) {
        super.onEvent("renderFailed", map);
    }
}
