package com.flurry.android;

import android.content.Context;
import android.net.Uri;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.flurry.android.impl.appcloud.AppCloudModule;
import com.flurry.android.monolithic.sdk.impl.f;
import com.flurry.android.monolithic.sdk.impl.fk;
import com.flurry.android.monolithic.sdk.impl.ft;
import com.flurry.android.monolithic.sdk.impl.fy;
import com.flurry.android.monolithic.sdk.impl.g;
import com.flurry.android.monolithic.sdk.impl.ga;
import com.flurry.android.monolithic.sdk.impl.go;
import com.flurry.android.monolithic.sdk.impl.ja;
import java.util.UUID;

public class FlurryWallet {
    private static Context a = null;

    public static void initWalletModule(Context context) {
        if (context == null) {
            throw new NullPointerException("Null context");
        }
        a = context.getApplicationContext();
        go.a(context);
    }

    public static void addObserverForCurrencyKey(String str, FlurryWalletHandler flurryWalletHandler) {
        String d = fy.d();
        String h = fy.h();
        if (TextUtils.isEmpty(d) || TextUtils.isEmpty(h)) {
            ja.a(4, "FlurryWallet", "There is no last logged in user");
            String a2 = a();
            if (a2 == null) {
                ja.a(6, "FlurryWallet", "Could not generate Wallet user.");
            } else {
                a(a2, a2, a2 + "@" + a2 + ".flurry.com", str, flurryWalletHandler);
            }
        } else {
            a(str, flurryWalletHandler);
        }
    }

    static void a(String str, String str2, String str3, String str4, FlurryWalletHandler flurryWalletHandler) {
        try {
            ft.a(str, str2, str3, true, new f(str4, flurryWalletHandler));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(String str, FlurryWalletHandler flurryWalletHandler) {
        ft f = ft.f();
        if (f == null) {
            flurryWalletHandler.onError(new FlurryWalletError(400, "Please assign an object id."));
            return;
        }
        AppCloudModule.getInstance().b().a(f.a(), str, new ga(flurryWalletHandler));
        AppCloudModule.getInstance().b().b(f.a());
    }

    private static String a() {
        if (a == null) {
            ja.a(6, "FlurryWallet", "Please call initWalletModule first.");
            return null;
        }
        try {
            return new UUID((long) ("" + Settings.Secure.getString(a.getContentResolver(), "android_id")).hashCode(), ((long) "".hashCode()) | (((long) ("" + ((TelephonyManager) a.getSystemService("phone")).getDeviceId()).hashCode()) << 32)).toString();
        } catch (Exception e) {
            ja.a(6, "FlurryWallet", "Could not get device id.");
            e.printStackTrace();
            return null;
        }
    }

    public static boolean removeObserversForCurrencyKey(String str) {
        ft f = ft.f();
        if (f == null) {
            return false;
        }
        String a2 = f.a();
        if (TextUtils.isEmpty(a2)) {
            return false;
        }
        return AppCloudModule.getInstance().b().a(a2, str);
    }

    public static boolean removeObserver(FlurryWalletHandler flurryWalletHandler) {
        ft f = ft.f();
        if (f == null) {
            return false;
        }
        String a2 = f.a();
        if (TextUtils.isEmpty(a2)) {
            return false;
        }
        return AppCloudModule.getInstance().b().a(a2, new ga(flurryWalletHandler));
    }

    public static boolean removeAllObservers() {
        ft f = ft.f();
        if (f != null) {
            return f.c();
        }
        return false;
    }

    public static FlurryWalletInfo getLastReceivedValueForCurrencyKey(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        ft f = ft.f();
        if (f == null) {
            return null;
        }
        return new FlurryWalletInfo(str, f.a(str));
    }

    public static void decrementWalletForCurrencyKey(String str, float f, FlurryWalletOperationHandler flurryWalletOperationHandler) {
        String d = fy.d();
        String h = fy.h();
        if (TextUtils.isEmpty(d) || TextUtils.isEmpty(h)) {
            ja.a(4, "FlurryWallet", "There is no last logged in user");
            String a2 = a();
            if (a2 == null) {
                ja.a(6, "FlurryWallet", "Could not generate Wallet user.");
                return;
            }
            try {
                ft.a(a2, a2, a2 + "@" + a2 + ".flurry.com", true, new g(str, f, flurryWalletOperationHandler));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            b(str, f, flurryWalletOperationHandler);
        }
    }

    public static void setWalletServerUrl(String str) {
        String str2 = null;
        if (!TextUtils.isEmpty(str)) {
            str2 = Uri.parse(str).getHost();
        }
        go.b(str2);
    }

    /* access modifiers changed from: private */
    public static void b(String str, float f, FlurryWalletOperationHandler flurryWalletOperationHandler) {
        ft f2 = ft.f();
        if (f2 == null) {
            flurryWalletOperationHandler.onError(new FlurryWalletError(400, "Please assign an object id."));
        } else if (f <= 0.0f) {
            flurryWalletOperationHandler.onError(new FlurryWalletError(400, "decrementWalletForCurrencyKey may only be called with a positive value."));
        } else {
            f2.a(str, f, new fk(flurryWalletOperationHandler));
        }
    }
}
