package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public abstract class th {
    public abstract String a();

    public boolean b() {
        return h() || i() || j() || c() || d() || e() || f() || g();
    }

    public boolean c() {
        return false;
    }

    public boolean d() {
        return false;
    }

    public boolean e() {
        return false;
    }

    public boolean f() {
        return false;
    }

    public boolean g() {
        return false;
    }

    public boolean h() {
        return n() != null;
    }

    public boolean i() {
        return l() != null;
    }

    public boolean j() {
        return false;
    }

    public sw[] k() {
        return null;
    }

    public afm l() {
        return null;
    }

    public Object m() throws IOException, oz {
        throw new qw("Can not instantiate value of type " + a() + "; no default creator found");
    }

    public Object a(Object[] objArr) throws IOException, oz {
        throw new qw("Can not instantiate value of type " + a() + " with arguments");
    }

    public Object a(Object obj) throws IOException, oz {
        throw new qw("Can not instantiate value of type " + a() + " using delegate");
    }

    public Object a(String str) throws IOException, oz {
        throw new qw("Can not instantiate value of type " + a() + " from JSON String");
    }

    public Object a(int i) throws IOException, oz {
        throw new qw("Can not instantiate value of type " + a() + " from JSON int number");
    }

    public Object a(long j) throws IOException, oz {
        throw new qw("Can not instantiate value of type " + a() + " from JSON long number");
    }

    public Object a(double d) throws IOException, oz {
        throw new qw("Can not instantiate value of type " + a() + " from JSON floating-point number");
    }

    public Object a(boolean z) throws IOException, oz {
        throw new qw("Can not instantiate value of type " + a() + " from JSON boolean value");
    }

    public xo n() {
        return null;
    }

    public xo o() {
        return null;
    }
}
