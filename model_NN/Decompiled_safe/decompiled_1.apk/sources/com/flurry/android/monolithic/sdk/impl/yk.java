package com.flurry.android.monolithic.sdk.impl;

import com.flurry.org.codehaus.jackson.annotate.JsonTypeInfo;
import java.io.IOException;

public class yk extends yy {
    public yk(afm afm, yi yiVar, qc qcVar, Class<?> cls) {
        super(afm, yiVar, qcVar, cls);
    }

    public JsonTypeInfo.As a() {
        return JsonTypeInfo.As.WRAPPER_ARRAY;
    }

    public Object b(ow owVar, qm qmVar) throws IOException, oz {
        return f(owVar, qmVar);
    }

    public Object a(ow owVar, qm qmVar) throws IOException, oz {
        return f(owVar, qmVar);
    }

    public Object c(ow owVar, qm qmVar) throws IOException, oz {
        return f(owVar, qmVar);
    }

    public Object d(ow owVar, qm qmVar) throws IOException, oz {
        return f(owVar, qmVar);
    }

    private final Object f(ow owVar, qm qmVar) throws IOException, oz {
        boolean j = owVar.j();
        Object a = a(qmVar, e(owVar, qmVar)).a(owVar, qmVar);
        if (!j || owVar.b() == pb.END_ARRAY) {
            return a;
        }
        throw qmVar.a(owVar, pb.END_ARRAY, "expected closing END_ARRAY after type information and deserialized value");
    }

    /* access modifiers changed from: protected */
    public final String e(ow owVar, qm qmVar) throws IOException, oz {
        if (!owVar.j()) {
            if ((this.b instanceof yz) && this.e != null) {
                return ((yz) this.b).a();
            }
            throw qmVar.a(owVar, pb.START_ARRAY, "need JSON Array to contain As.WRAPPER_ARRAY type information for class " + c());
        } else if (owVar.b() == pb.VALUE_STRING) {
            String k = owVar.k();
            owVar.b();
            return k;
        } else if ((this.b instanceof yz) && this.e != null) {
            return ((yz) this.b).a();
        } else {
            throw qmVar.a(owVar, pb.VALUE_STRING, "need JSON String that contains type id (for subtype of " + c() + ")");
        }
    }
}
