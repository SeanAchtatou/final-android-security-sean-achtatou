package com.flurry.android.monolithic.sdk.impl;

import java.lang.reflect.Constructor;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.HashMap;

public class wu {
    protected final HashMap<afm, rc> a = new HashMap<>();

    protected wu() {
        a(new wf());
        a(new wg());
        a(new wi());
        a(new wp());
        a(new wn());
        a(new wo());
        a(new wm());
        a(new wk());
        a(new wj());
        a(new wh());
        a(new wt());
    }

    private void a(we weVar) {
        this.a.put(adk.a().a(weVar.a()), weVar);
    }

    public static HashMap<afm, rc> a() {
        return new wu().a;
    }

    public static rc a(qk qkVar, afm afm) {
        return ws.a(afm.getClass());
    }

    public static rc a(aed<?> aed) {
        return new wl(aed, null);
    }

    public static rc a(aed<?> aed, xl xlVar) {
        return new wl(aed, xlVar);
    }

    public static rc b(qk qkVar, afm afm) {
        Class<String> cls = String.class;
        xq xqVar = (xq) qkVar.b(afm);
        Class<String> cls2 = String.class;
        Constructor<?> a2 = xqVar.a(cls);
        if (a2 != null) {
            if (qkVar.a(ql.CAN_OVERRIDE_ACCESS_MODIFIERS)) {
                adz.a(a2);
            }
            return new wq(a2);
        }
        Class<String> cls3 = String.class;
        Method b = xqVar.b(cls);
        if (b == null) {
            return null;
        }
        if (qkVar.a(ql.CAN_OVERRIDE_ACCESS_MODIFIERS)) {
            adz.a((Member) b);
        }
        return new wr(b);
    }
}
