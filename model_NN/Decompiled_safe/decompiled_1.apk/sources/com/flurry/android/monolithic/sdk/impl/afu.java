package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.Arrays;

public class afu implements pf {
    static final String a;
    static final char[] b = new char[64];

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Arrays.fill(char[], char):void}
     arg types: [char[], int]
     candidates:
      ClspMth{java.util.Arrays.fill(double[], double):void}
      ClspMth{java.util.Arrays.fill(byte[], byte):void}
      ClspMth{java.util.Arrays.fill(long[], long):void}
      ClspMth{java.util.Arrays.fill(boolean[], boolean):void}
      ClspMth{java.util.Arrays.fill(short[], short):void}
      ClspMth{java.util.Arrays.fill(java.lang.Object[], java.lang.Object):void}
      ClspMth{java.util.Arrays.fill(int[], int):void}
      ClspMth{java.util.Arrays.fill(float[], float):void}
      ClspMth{java.util.Arrays.fill(char[], char):void} */
    static {
        String str = null;
        try {
            str = System.getProperty("line.separator");
        } catch (Throwable th) {
        }
        if (str == null) {
            str = "\n";
        }
        a = str;
        Arrays.fill(b, ' ');
    }

    public boolean a() {
        return false;
    }

    public void a(or orVar, int i) throws IOException, oq {
        orVar.c(a);
        int i2 = i + i;
        while (i2 > 64) {
            orVar.b(b, 0, 64);
            i2 -= b.length;
        }
        orVar.b(b, 0, i2);
    }
}
