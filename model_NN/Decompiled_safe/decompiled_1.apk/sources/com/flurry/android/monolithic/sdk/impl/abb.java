package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;

public class abb extends aay<Collection<?>> {
    public /* synthetic */ void b(Object obj, or orVar, ru ruVar) throws IOException, oq {
        a((Collection<?>) ((Collection) obj), orVar, ruVar);
    }

    public abb(afm afm, boolean z, rx rxVar, qc qcVar, ra<Object> raVar) {
        super(Collection.class, afm, z, rxVar, qcVar, raVar);
    }

    public abc<?> a(rx rxVar) {
        return new abb(this.b, this.a, rxVar, this.e, this.d);
    }

    public void a(Collection<?> collection, or orVar, ru ruVar) throws IOException, oq {
        aal aal;
        ra<Object> raVar;
        if (this.d != null) {
            a(collection, orVar, ruVar, this.d);
            return;
        }
        Iterator<?> it = collection.iterator();
        if (it.hasNext()) {
            aal aal2 = this.f;
            rx rxVar = this.c;
            aal aal3 = aal2;
            int i = 0;
            do {
                try {
                    Object next = it.next();
                    if (next == null) {
                        ruVar.a(orVar);
                    } else {
                        Class<?> cls = next.getClass();
                        ra<Object> a = aal3.a(cls);
                        if (a == null) {
                            if (this.b.e()) {
                                raVar = a(aal3, ruVar.a(this.b, cls), ruVar);
                            } else {
                                raVar = a(aal3, cls, ruVar);
                            }
                            aal = this.f;
                        } else {
                            aal = aal3;
                            raVar = a;
                        }
                        if (rxVar == null) {
                            raVar.a(next, orVar, ruVar);
                            aal3 = aal;
                        } else {
                            raVar.a(next, orVar, ruVar, rxVar);
                            aal3 = aal;
                        }
                    }
                    i++;
                } catch (Exception e) {
                    a(ruVar, e, collection, i);
                    return;
                }
            } while (it.hasNext());
        }
    }

    public void a(Collection<?> collection, or orVar, ru ruVar, ra<Object> raVar) throws IOException, oq {
        Iterator<?> it = collection.iterator();
        if (it.hasNext()) {
            rx rxVar = this.c;
            int i = 0;
            do {
                Object next = it.next();
                if (next == null) {
                    try {
                        ruVar.a(orVar);
                    } catch (Exception e) {
                        a(ruVar, e, collection, i);
                    }
                } else if (rxVar == null) {
                    raVar.a(next, orVar, ruVar);
                } else {
                    raVar.a(next, orVar, ruVar, rxVar);
                }
                i++;
            } while (it.hasNext());
        }
    }
}
