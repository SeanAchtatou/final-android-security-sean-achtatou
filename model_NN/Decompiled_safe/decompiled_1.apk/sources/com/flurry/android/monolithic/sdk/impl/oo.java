package com.flurry.android.monolithic.sdk.impl;

import v2.com.playhaven.requests.base.PHAsyncRequest;

public final class oo {
    public static final on a = new on("MIME", "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", true, '=', 76);
    public static final on b = new on(a, "MIME-NO-LINEFEEDS", PHAsyncRequest.INFINITE_REDIRECTS);
    public static final on c = new on(a, "PEM", true, '=', 64);
    public static final on d;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.on.<init>(java.lang.String, java.lang.String, boolean, char, int):void
     arg types: [java.lang.String, java.lang.String, int, int, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.on.<init>(com.flurry.android.monolithic.sdk.impl.on, java.lang.String, boolean, char, int):void
      com.flurry.android.monolithic.sdk.impl.on.<init>(java.lang.String, java.lang.String, boolean, char, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.on.<init>(com.flurry.android.monolithic.sdk.impl.on, java.lang.String, boolean, char, int):void
     arg types: [com.flurry.android.monolithic.sdk.impl.on, java.lang.String, int, int, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.on.<init>(java.lang.String, java.lang.String, boolean, char, int):void
      com.flurry.android.monolithic.sdk.impl.on.<init>(com.flurry.android.monolithic.sdk.impl.on, java.lang.String, boolean, char, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.on.<init>(java.lang.String, java.lang.String, boolean, char, int):void
     arg types: [java.lang.String, java.lang.String, int, int, ?]
     candidates:
      com.flurry.android.monolithic.sdk.impl.on.<init>(com.flurry.android.monolithic.sdk.impl.on, java.lang.String, boolean, char, int):void
      com.flurry.android.monolithic.sdk.impl.on.<init>(java.lang.String, java.lang.String, boolean, char, int):void */
    static {
        StringBuffer stringBuffer = new StringBuffer("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/");
        stringBuffer.setCharAt(stringBuffer.indexOf("+"), '-');
        stringBuffer.setCharAt(stringBuffer.indexOf("/"), '_');
        d = new on("MODIFIED-FOR-URL", stringBuffer.toString(), false, 0, (int) PHAsyncRequest.INFINITE_REDIRECTS);
    }

    public static on a() {
        return b;
    }
}
