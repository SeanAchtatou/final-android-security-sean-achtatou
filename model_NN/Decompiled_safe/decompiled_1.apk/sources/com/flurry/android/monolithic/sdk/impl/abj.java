package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.Iterator;

@rz
public class abj extends aay<Iterable<?>> {
    public /* synthetic */ void b(Object obj, or orVar, ru ruVar) throws IOException, oq {
        a((Iterable<?>) ((Iterable) obj), orVar, ruVar);
    }

    public abj(afm afm, boolean z, rx rxVar, qc qcVar) {
        super(Iterable.class, afm, z, rxVar, qcVar, null);
    }

    public abc<?> a(rx rxVar) {
        return new abj(this.b, this.a, rxVar, this.e);
    }

    public void a(Iterable<?> iterable, or orVar, ru ruVar) throws IOException, oq {
        ra<Object> a;
        Class<?> cls;
        ra<Object> raVar;
        Class<?> cls2 = null;
        Iterator<?> it = iterable.iterator();
        if (it.hasNext()) {
            rx rxVar = this.c;
            ra<Object> raVar2 = null;
            do {
                Object next = it.next();
                if (next == null) {
                    ruVar.a(orVar);
                } else {
                    Class<?> cls3 = next.getClass();
                    if (cls3 == cls2) {
                        raVar = raVar2;
                        Class<?> cls4 = cls2;
                        a = raVar2;
                        cls = cls4;
                    } else {
                        a = ruVar.a(cls3, this.e);
                        cls = cls3;
                        raVar = a;
                    }
                    if (rxVar == null) {
                        a.a(next, orVar, ruVar);
                        cls2 = cls;
                        raVar2 = raVar;
                    } else {
                        a.a(next, orVar, ruVar, rxVar);
                        cls2 = cls;
                        raVar2 = raVar;
                    }
                }
            } while (it.hasNext());
        }
    }
}
