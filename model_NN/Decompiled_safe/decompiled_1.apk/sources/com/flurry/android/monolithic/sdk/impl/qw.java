package com.flurry.android.monolithic.sdk.impl;

import java.util.Iterator;
import java.util.LinkedList;

public class qw extends oz {
    protected LinkedList<qx> b;

    public qw(String str) {
        super(str);
    }

    public qw(String str, Throwable th) {
        super(str, th);
    }

    public qw(String str, ot otVar) {
        super(str, otVar);
    }

    public qw(String str, ot otVar, Throwable th) {
        super(str, otVar, th);
    }

    public static qw a(ow owVar, String str) {
        return new qw(str, owVar.h());
    }

    public static qw a(ow owVar, String str, Throwable th) {
        return new qw(str, owVar.h(), th);
    }

    public static qw a(Throwable th, Object obj, String str) {
        return a(th, new qx(obj, str));
    }

    public static qw a(Throwable th, Object obj, int i) {
        return a(th, new qx(obj, i));
    }

    public static qw a(Throwable th, qx qxVar) {
        qw qwVar;
        if (th instanceof qw) {
            qwVar = (qw) th;
        } else {
            String message = th.getMessage();
            if (message == null || message.length() == 0) {
                message = "(was " + th.getClass().getName() + ")";
            }
            qwVar = new qw(message, null, th);
        }
        qwVar.a(qxVar);
        return qwVar;
    }

    public void a(Object obj, String str) {
        a(new qx(obj, str));
    }

    public void a(qx qxVar) {
        if (this.b == null) {
            this.b = new LinkedList<>();
        }
        if (this.b.size() < 1000) {
            this.b.addFirst(qxVar);
        }
    }

    public String getMessage() {
        String message = super.getMessage();
        if (this.b == null) {
            return message;
        }
        StringBuilder sb = message == null ? new StringBuilder() : new StringBuilder(message);
        sb.append(" (through reference chain: ");
        a(sb);
        sb.append(')');
        return sb.toString();
    }

    public String toString() {
        return getClass().getName() + ": " + getMessage();
    }

    /* access modifiers changed from: protected */
    public void a(StringBuilder sb) {
        Iterator<qx> it = this.b.iterator();
        while (it.hasNext()) {
            sb.append(it.next().toString());
            if (it.hasNext()) {
                sb.append("->");
            }
        }
    }
}
