package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;

public class gh extends gf {
    public gh(Context context, String str) {
        this.a = gd.a(context, str);
        this.c = "CacheServerOperationsDB";
        this.b = "serverDataTable";
    }

    public hk b(String str) {
        hk hkVar;
        hk hkVar2 = null;
        Cursor query = this.a.getReadableDatabase().query(this.b, null, null, null, null, null, null);
        if (query.moveToFirst()) {
            int columnIndex = query.getColumnIndex("objectsId");
            int columnIndex2 = query.getColumnIndex("objectsLocalId");
            int columnIndex3 = query.getColumnIndex("collectionName");
            int columnIndex4 = query.getColumnIndex("key");
            int columnIndex5 = query.getColumnIndex("value");
            do {
                String string = query.getString(columnIndex);
                String string2 = query.getString(columnIndex2);
                String string3 = query.getString(columnIndex3);
                String string4 = query.getString(columnIndex4);
                String string5 = query.getString(columnIndex5);
                if (str.equals(string) || str.equals(string2)) {
                    if (hkVar2 == null) {
                        if (TextUtils.isEmpty(string3)) {
                            hkVar2 = new hk(string);
                        } else {
                            hkVar2 = new hk(string, string3);
                        }
                    }
                    hkVar2.a(string4, string5);
                }
            } while (query.moveToNext());
            hkVar = hkVar2;
        } else {
            hkVar = null;
        }
        query.close();
        return hkVar;
    }
}
