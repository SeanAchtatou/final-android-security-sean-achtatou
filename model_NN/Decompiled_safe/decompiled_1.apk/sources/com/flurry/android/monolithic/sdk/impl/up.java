package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.net.InetAddress;

public class up extends um<InetAddress> {
    public up() {
        super(InetAddress.class);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public InetAddress a(String str, qm qmVar) throws IOException {
        return InetAddress.getByName(str);
    }
}
