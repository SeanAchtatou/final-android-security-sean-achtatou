package com.flurry.android.monolithic.sdk.impl;

public class aem {
    protected aef<adb, pw> a;

    public pw a(afm afm, rf<?> rfVar) {
        return a(afm.p(), rfVar);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0042, code lost:
        if (r0 != null) goto L_0x0038;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized com.flurry.android.monolithic.sdk.impl.pw a(java.lang.Class<?> r5, com.flurry.android.monolithic.sdk.impl.rf<?> r6) {
        /*
            r4 = this;
            monitor-enter(r4)
            com.flurry.android.monolithic.sdk.impl.adb r1 = new com.flurry.android.monolithic.sdk.impl.adb     // Catch:{ all -> 0x0045 }
            r1.<init>(r5)     // Catch:{ all -> 0x0045 }
            com.flurry.android.monolithic.sdk.impl.aef<com.flurry.android.monolithic.sdk.impl.adb, com.flurry.android.monolithic.sdk.impl.pw> r0 = r4.a     // Catch:{ all -> 0x0045 }
            if (r0 != 0) goto L_0x003a
            com.flurry.android.monolithic.sdk.impl.aef r0 = new com.flurry.android.monolithic.sdk.impl.aef     // Catch:{ all -> 0x0045 }
            r2 = 20
            r3 = 200(0xc8, float:2.8E-43)
            r0.<init>(r2, r3)     // Catch:{ all -> 0x0045 }
            r4.a = r0     // Catch:{ all -> 0x0045 }
        L_0x0015:
            com.flurry.android.monolithic.sdk.impl.qb r0 = r6.c(r5)     // Catch:{ all -> 0x0045 }
            com.flurry.android.monolithic.sdk.impl.xq r0 = (com.flurry.android.monolithic.sdk.impl.xq) r0     // Catch:{ all -> 0x0045 }
            com.flurry.android.monolithic.sdk.impl.py r2 = r6.a()     // Catch:{ all -> 0x0045 }
            com.flurry.android.monolithic.sdk.impl.xh r0 = r0.c()     // Catch:{ all -> 0x0045 }
            java.lang.String r0 = r2.b(r0)     // Catch:{ all -> 0x0045 }
            if (r0 != 0) goto L_0x002d
            java.lang.String r0 = r5.getSimpleName()     // Catch:{ all -> 0x0045 }
        L_0x002d:
            com.flurry.android.monolithic.sdk.impl.pw r2 = new com.flurry.android.monolithic.sdk.impl.pw     // Catch:{ all -> 0x0045 }
            r2.<init>(r0)     // Catch:{ all -> 0x0045 }
            com.flurry.android.monolithic.sdk.impl.aef<com.flurry.android.monolithic.sdk.impl.adb, com.flurry.android.monolithic.sdk.impl.pw> r0 = r4.a     // Catch:{ all -> 0x0045 }
            r0.put(r1, r2)     // Catch:{ all -> 0x0045 }
            r0 = r2
        L_0x0038:
            monitor-exit(r4)
            return r0
        L_0x003a:
            com.flurry.android.monolithic.sdk.impl.aef<com.flurry.android.monolithic.sdk.impl.adb, com.flurry.android.monolithic.sdk.impl.pw> r0 = r4.a     // Catch:{ all -> 0x0045 }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x0045 }
            com.flurry.android.monolithic.sdk.impl.pw r0 = (com.flurry.android.monolithic.sdk.impl.pw) r0     // Catch:{ all -> 0x0045 }
            if (r0 == 0) goto L_0x0015
            goto L_0x0038
        L_0x0045:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.aem.a(java.lang.Class, com.flurry.android.monolithic.sdk.impl.rf):com.flurry.android.monolithic.sdk.impl.pw");
    }
}
