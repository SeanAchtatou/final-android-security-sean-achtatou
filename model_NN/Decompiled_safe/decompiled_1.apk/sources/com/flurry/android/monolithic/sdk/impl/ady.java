package com.flurry.android.monolithic.sdk.impl;

public class ady {
    public static String a(xl xlVar) {
        String b = xlVar.b();
        String b2 = b(xlVar, b);
        if (b2 == null) {
            return a(xlVar, b);
        }
        return b2;
    }

    public static String a(xl xlVar, String str) {
        if (!str.startsWith("get")) {
            return null;
        }
        if ("getCallbacks".equals(str)) {
            if (c(xlVar)) {
                return null;
            }
        } else if ("getMetaClass".equals(str) && e(xlVar)) {
            return null;
        }
        return a(str.substring(3));
    }

    public static String b(xl xlVar, String str) {
        if (!str.startsWith("is")) {
            return null;
        }
        Class<?> d = xlVar.d();
        if (d == Boolean.class || d == Boolean.TYPE) {
            return a(str.substring(2));
        }
        return null;
    }

    public static String b(xl xlVar) {
        String b = xlVar.b();
        if (!b.startsWith("set")) {
            return null;
        }
        String a = a(b.substring(3));
        if (a == null) {
            return null;
        }
        if (!"metaClass".equals(a) || !d(xlVar)) {
            return a;
        }
        return null;
    }

    protected static boolean c(xl xlVar) {
        Class<?> d = xlVar.d();
        if (d == null || !d.isArray()) {
            return false;
        }
        Package packageR = d.getComponentType().getPackage();
        if (packageR != null) {
            String name = packageR.getName();
            if (name.startsWith("net.sf.cglib") || name.startsWith("org.hibernate.repackage.cglib")) {
                return true;
            }
        }
        return false;
    }

    protected static boolean d(xl xlVar) {
        Package packageR = xlVar.a(0).getPackage();
        if (packageR == null || !packageR.getName().startsWith("groovy.lang")) {
            return false;
        }
        return true;
    }

    protected static boolean e(xl xlVar) {
        Class<?> d = xlVar.d();
        if (d == null || d.isArray()) {
            return false;
        }
        Package packageR = d.getPackage();
        return packageR != null && packageR.getName().startsWith("groovy.lang");
    }

    protected static String a(String str) {
        StringBuilder sb = null;
        int length = str.length();
        if (length == 0) {
            return null;
        }
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            char lowerCase = Character.toLowerCase(charAt);
            if (charAt == lowerCase) {
                break;
            }
            if (sb == null) {
                sb = new StringBuilder(str);
            }
            sb.setCharAt(i, lowerCase);
        }
        if (sb == null) {
            return str;
        }
        return sb.toString();
    }
}
