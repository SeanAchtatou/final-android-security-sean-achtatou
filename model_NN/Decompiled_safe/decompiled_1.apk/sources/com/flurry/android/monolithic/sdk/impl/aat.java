package com.flurry.android.monolithic.sdk.impl;

import java.util.HashMap;

public final class aat {
    private HashMap<aau, ra<Object>> a = new HashMap<>(64);
    private aas b = null;

    public aas a() {
        aas aas;
        synchronized (this) {
            aas = this.b;
            if (aas == null) {
                aas = aas.a(this.a);
                this.b = aas;
            }
        }
        return aas.a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.aau.<init>(java.lang.Class<?>, boolean):void
     arg types: [java.lang.Class<?>, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.aau.<init>(com.flurry.android.monolithic.sdk.impl.afm, boolean):void
      com.flurry.android.monolithic.sdk.impl.aau.<init>(java.lang.Class<?>, boolean):void */
    public ra<Object> a(Class<?> cls) {
        ra<Object> raVar;
        synchronized (this) {
            raVar = this.a.get(new aau(cls, false));
        }
        return raVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.aau.<init>(com.flurry.android.monolithic.sdk.impl.afm, boolean):void
     arg types: [com.flurry.android.monolithic.sdk.impl.afm, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.aau.<init>(java.lang.Class<?>, boolean):void
      com.flurry.android.monolithic.sdk.impl.aau.<init>(com.flurry.android.monolithic.sdk.impl.afm, boolean):void */
    public ra<Object> a(afm afm) {
        ra<Object> raVar;
        synchronized (this) {
            raVar = this.a.get(new aau(afm, false));
        }
        return raVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.aau.<init>(com.flurry.android.monolithic.sdk.impl.afm, boolean):void
     arg types: [com.flurry.android.monolithic.sdk.impl.afm, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.aau.<init>(java.lang.Class<?>, boolean):void
      com.flurry.android.monolithic.sdk.impl.aau.<init>(com.flurry.android.monolithic.sdk.impl.afm, boolean):void */
    public ra<Object> b(afm afm) {
        ra<Object> raVar;
        synchronized (this) {
            raVar = this.a.get(new aau(afm, true));
        }
        return raVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.aau.<init>(java.lang.Class<?>, boolean):void
     arg types: [java.lang.Class<?>, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.aau.<init>(com.flurry.android.monolithic.sdk.impl.afm, boolean):void
      com.flurry.android.monolithic.sdk.impl.aau.<init>(java.lang.Class<?>, boolean):void */
    public ra<Object> b(Class<?> cls) {
        ra<Object> raVar;
        synchronized (this) {
            raVar = this.a.get(new aau(cls, true));
        }
        return raVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.aau.<init>(com.flurry.android.monolithic.sdk.impl.afm, boolean):void
     arg types: [com.flurry.android.monolithic.sdk.impl.afm, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.aau.<init>(java.lang.Class<?>, boolean):void
      com.flurry.android.monolithic.sdk.impl.aau.<init>(com.flurry.android.monolithic.sdk.impl.afm, boolean):void */
    public void a(afm afm, ra<Object> raVar) {
        synchronized (this) {
            if (this.a.put(new aau(afm, true), raVar) == null) {
                this.b = null;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.aau.<init>(java.lang.Class<?>, boolean):void
     arg types: [java.lang.Class<?>, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.aau.<init>(com.flurry.android.monolithic.sdk.impl.afm, boolean):void
      com.flurry.android.monolithic.sdk.impl.aau.<init>(java.lang.Class<?>, boolean):void */
    public void a(Class<?> cls, ra<Object> raVar) {
        synchronized (this) {
            if (this.a.put(new aau(cls, true), raVar) == null) {
                this.b = null;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.aau.<init>(java.lang.Class<?>, boolean):void
     arg types: [java.lang.Class<?>, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.aau.<init>(com.flurry.android.monolithic.sdk.impl.afm, boolean):void
      com.flurry.android.monolithic.sdk.impl.aau.<init>(java.lang.Class<?>, boolean):void */
    public void a(Class<?> cls, ra<Object> raVar, ru ruVar) throws qw {
        synchronized (this) {
            if (this.a.put(new aau(cls, false), raVar) == null) {
                this.b = null;
            }
            if (raVar instanceof rp) {
                ((rp) raVar).a(ruVar);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.aau.<init>(com.flurry.android.monolithic.sdk.impl.afm, boolean):void
     arg types: [com.flurry.android.monolithic.sdk.impl.afm, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.aau.<init>(java.lang.Class<?>, boolean):void
      com.flurry.android.monolithic.sdk.impl.aau.<init>(com.flurry.android.monolithic.sdk.impl.afm, boolean):void */
    public void a(afm afm, ra<Object> raVar, ru ruVar) throws qw {
        synchronized (this) {
            if (this.a.put(new aau(afm, false), raVar) == null) {
                this.b = null;
            }
            if (raVar instanceof rp) {
                ((rp) raVar).a(ruVar);
            }
        }
    }
}
