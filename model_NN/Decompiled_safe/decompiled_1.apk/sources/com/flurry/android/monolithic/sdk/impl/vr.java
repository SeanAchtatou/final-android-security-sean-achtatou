package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.math.BigInteger;

@rz
public class vr extends wv<BigInteger> {
    public vr() {
        super(BigInteger.class);
    }

    /* renamed from: b */
    public BigInteger a(ow owVar, qm qmVar) throws IOException, oz {
        pb e = owVar.e();
        if (e == pb.VALUE_NUMBER_INT) {
            switch (vp.b[owVar.q().ordinal()]) {
                case 1:
                case 2:
                    return BigInteger.valueOf(owVar.u());
            }
        } else if (e == pb.VALUE_NUMBER_FLOAT) {
            return owVar.y().toBigInteger();
        } else {
            if (e != pb.VALUE_STRING) {
                throw qmVar.a(this.q, e);
            }
        }
        String trim = owVar.k().trim();
        if (trim.length() == 0) {
            return null;
        }
        try {
            return new BigInteger(trim);
        } catch (IllegalArgumentException e2) {
            throw qmVar.b(this.q, "not a valid representation");
        }
    }
}
