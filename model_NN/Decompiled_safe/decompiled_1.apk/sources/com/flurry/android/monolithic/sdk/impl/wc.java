package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.sql.Date;

public class wc extends wv<Date> {
    public wc() {
        super(Date.class);
    }

    /* renamed from: b */
    public Date a(ow owVar, qm qmVar) throws IOException, oz {
        java.util.Date B = B(owVar, qmVar);
        if (B == null) {
            return null;
        }
        return new Date(B.getTime());
    }
}
