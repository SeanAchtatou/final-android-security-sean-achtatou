package com.flurry.android.monolithic.sdk.impl;

import com.inmobi.androidsdk.IMAdListener;
import com.inmobi.androidsdk.IMAdRequest;
import com.inmobi.androidsdk.IMAdView;
import java.util.Collections;

/* renamed from: com.flurry.android.monolithic.sdk.impl.do  reason: invalid class name */
class Cdo implements IMAdListener {
    final /* synthetic */ dn a;

    Cdo(dn dnVar) {
        this.a = dnVar;
    }

    public void onShowAdScreen(IMAdView iMAdView) {
        this.a.onAdClicked(Collections.emptyMap());
        ja.a(3, dn.a, "InMobi imAdView ad shown.");
    }

    public void onDismissAdScreen(IMAdView iMAdView) {
        this.a.onAdClosed(Collections.emptyMap());
        ja.a(3, dn.a, "InMobi imAdView dismiss ad.");
    }

    public void onAdRequestFailed(IMAdView iMAdView, IMAdRequest.ErrorCode errorCode) {
        this.a.onRenderFailed(Collections.emptyMap());
        ja.a(3, dn.a, "InMobi imAdView ad request failed. " + errorCode.toString());
    }

    public void onAdRequestCompleted(IMAdView iMAdView) {
        this.a.onAdShown(Collections.emptyMap());
        ja.a(3, dn.a, "InMobi imAdView ad request completed.");
    }

    public void onLeaveApplication(IMAdView iMAdView) {
        ja.a(3, dn.a, "InMobi onLeaveApplication");
    }
}
