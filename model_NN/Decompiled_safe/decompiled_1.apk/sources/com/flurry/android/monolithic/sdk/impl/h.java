package com.flurry.android.monolithic.sdk.impl;

import com.flurry.android.InstallReceiver;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Map;

public class h extends jf {
    final /* synthetic */ String a;
    final /* synthetic */ InstallReceiver b;

    public h(InstallReceiver installReceiver, String str) {
        this.b = installReceiver;
        this.a = str;
    }

    public void a() {
        FileOutputStream fileOutputStream;
        Throwable th;
        Throwable th2;
        ja.a(3, "InstallReceiver", "persistInstallReport ");
        try {
            File parentFile = this.b.b.getParentFile();
            ja.a(3, "InstallReceiver", "dir is..." + parentFile.toString());
            if (parentFile.mkdirs() || parentFile.exists()) {
                fileOutputStream = new FileOutputStream(this.b.b);
                try {
                    ja.a(3, "InstallReceiver", "writing to ouput stream ");
                    fileOutputStream.write(this.a.getBytes());
                    Map<String, List<String>> a2 = InstallReceiver.a(this.b.a(this.b.b));
                    if (a2 != null) {
                        for (Map.Entry next : a2.entrySet()) {
                            ja.a(3, "InstallReceiver", "entry: " + ((String) next.getKey()) + "=" + next.getValue());
                        }
                    }
                    je.a(fileOutputStream);
                } catch (Throwable th3) {
                    th = th3;
                    try {
                        ja.a(6, "InstallReceiver", "", th);
                        je.a(fileOutputStream);
                    } catch (Throwable th4) {
                        th2 = th4;
                        je.a(fileOutputStream);
                        throw th2;
                    }
                }
            } else {
                ja.a(6, "InstallReceiver", "Unable to create persistent dir: " + parentFile);
                je.a((Closeable) null);
            }
        } catch (Throwable th5) {
            Throwable th6 = th5;
            fileOutputStream = null;
            th2 = th6;
            je.a(fileOutputStream);
            throw th2;
        }
    }
}
