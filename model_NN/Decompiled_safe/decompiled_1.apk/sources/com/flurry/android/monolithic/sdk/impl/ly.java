package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.io.InputStream;

public class ly {
    private static final ly b = new ma();
    int a = 8192;

    public static ly a() {
        return b;
    }

    public ll a(InputStream inputStream, ll llVar) {
        if (llVar == null || !llVar.getClass().equals(ll.class)) {
            return new ll(inputStream, this.a);
        }
        return llVar.a(inputStream, this.a);
    }

    public ll a(byte[] bArr, int i, int i2, ll llVar) {
        if (llVar == null || !llVar.getClass().equals(ll.class)) {
            return new ll(bArr, i, i2);
        }
        return llVar.a(bArr, i, i2);
    }

    public ll a(byte[] bArr, ll llVar) {
        return a(bArr, 0, bArr.length, llVar);
    }

    public mh a(ji jiVar, ji jiVar2, lx lxVar) throws IOException {
        return new mh(jiVar, jiVar2, lxVar);
    }
}
