package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.io.OutputStream;

public class ls extends lr {
    private byte[] a;
    private int b;
    private lu c;
    private int d;

    ls(OutputStream outputStream, int i) {
        a(outputStream, i);
    }

    /* access modifiers changed from: package-private */
    public ls a(OutputStream outputStream, int i) {
        if (outputStream == null) {
            throw new NullPointerException("OutputStream cannot be null!");
        }
        if (this.c != null && this.b > 0) {
            try {
                h();
            } catch (IOException e) {
                throw new jg("Failure flushing old output", e);
            }
        }
        this.c = new lv(outputStream);
        this.b = 0;
        if (this.a == null || this.a.length != i) {
            this.a = new byte[i];
        }
        this.d = this.a.length >>> 1;
        if (this.d > 512) {
            this.d = 512;
        }
        return this;
    }

    public void flush() throws IOException {
        h();
        this.c.a();
    }

    private void h() throws IOException {
        if (this.b > 0) {
            this.c.a(this.a, 0, this.b);
            this.b = 0;
        }
    }

    private void d(int i) throws IOException {
        if (this.a.length - this.b < i) {
            h();
        }
    }

    public void a(boolean z) throws IOException {
        if (this.a.length == this.b) {
            h();
        }
        this.b += lg.a(z, this.a, this.b);
    }

    public void c(int i) throws IOException {
        d(5);
        this.b += lg.a(i, this.a, this.b);
    }

    public void b(long j) throws IOException {
        d(10);
        this.b += lg.a(j, this.a, this.b);
    }

    public void a(float f) throws IOException {
        d(4);
        this.b += lg.a(f, this.a, this.b);
    }

    public void a(double d2) throws IOException {
        d(8);
        this.b += lg.a(d2, this.a, this.b);
    }

    public void b(byte[] bArr, int i, int i2) throws IOException {
        if (i2 > this.d) {
            h();
            this.c.a(bArr, i, i2);
            return;
        }
        d(i2);
        System.arraycopy(bArr, i, this.a, this.b, i2);
        this.b += i2;
    }

    /* access modifiers changed from: protected */
    public void g() throws IOException {
        e(0);
    }

    private void e(int i) throws IOException {
        if (this.b == this.a.length) {
            h();
        }
        byte[] bArr = this.a;
        int i2 = this.b;
        this.b = i2 + 1;
        bArr[i2] = (byte) (i & 255);
    }
}
