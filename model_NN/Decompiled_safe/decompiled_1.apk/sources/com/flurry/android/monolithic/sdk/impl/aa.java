package com.flurry.android.monolithic.sdk.impl;

import android.graphics.Bitmap;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.util.Collections;

class aa extends WebViewClient {
    final /* synthetic */ o a;

    private aa(o oVar) {
        this.a = oVar;
    }

    /* synthetic */ aa(o oVar, p pVar) {
        this(oVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.o.a(com.flurry.android.monolithic.sdk.impl.o, boolean):boolean
     arg types: [com.flurry.android.monolithic.sdk.impl.o, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.o.a(com.flurry.android.monolithic.sdk.impl.o, int):int
      com.flurry.android.monolithic.sdk.impl.o.a(com.flurry.android.monolithic.sdk.impl.o, android.app.Dialog):android.app.Dialog
      com.flurry.android.monolithic.sdk.impl.o.a(com.flurry.android.monolithic.sdk.impl.o, android.view.View):android.view.View
      com.flurry.android.monolithic.sdk.impl.o.a(com.flurry.android.monolithic.sdk.impl.o, android.webkit.WebChromeClient$CustomViewCallback):android.webkit.WebChromeClient$CustomViewCallback
      com.flurry.android.monolithic.sdk.impl.o.a(com.flurry.android.monolithic.sdk.impl.o, android.widget.FrameLayout):android.widget.FrameLayout
      com.flurry.android.monolithic.sdk.impl.o.a(com.flurry.android.monolithic.sdk.impl.o, java.lang.String):com.flurry.android.impl.ads.avro.protocol.v6.AdUnit
      com.flurry.android.monolithic.sdk.impl.o.a(java.util.List<com.flurry.android.impl.ads.avro.protocol.v6.AdUnit>, java.lang.String):java.lang.String
      com.flurry.android.monolithic.sdk.impl.o.a(int, int):void
      com.flurry.android.monolithic.sdk.impl.o.a(com.flurry.android.monolithic.sdk.impl.i, int):void
      com.flurry.android.monolithic.sdk.impl.o.a(com.flurry.android.monolithic.sdk.impl.o, boolean):boolean */
    public void onLoadResource(WebView webView, String str) {
        String lastPathSegment;
        ja.a(3, this.a.f, "onLoadResource: url = " + str);
        super.onLoadResource(webView, str);
        if (str != null && webView != null && webView == this.a.l) {
            if (!str.equalsIgnoreCase(this.a.l.getUrl())) {
                this.a.g();
            }
            if (!this.a.y && (lastPathSegment = Uri.parse(str).getLastPathSegment()) != null && lastPathSegment.equalsIgnoreCase("mraid.js")) {
                boolean unused = this.a.y = true;
                this.a.l();
                if (this.a.x) {
                    this.a.o();
                    this.a.p();
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.o.b(com.flurry.android.monolithic.sdk.impl.o, boolean):boolean
     arg types: [com.flurry.android.monolithic.sdk.impl.o, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.o.b(com.flurry.android.monolithic.sdk.impl.o, java.lang.String):com.flurry.android.monolithic.sdk.impl.m
      com.flurry.android.monolithic.sdk.impl.o.b(int, int):void
      com.flurry.android.monolithic.sdk.impl.o.b(com.flurry.android.monolithic.sdk.impl.o, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.o.a(com.flurry.android.monolithic.sdk.impl.o, boolean):boolean
     arg types: [com.flurry.android.monolithic.sdk.impl.o, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.o.a(com.flurry.android.monolithic.sdk.impl.o, int):int
      com.flurry.android.monolithic.sdk.impl.o.a(com.flurry.android.monolithic.sdk.impl.o, android.app.Dialog):android.app.Dialog
      com.flurry.android.monolithic.sdk.impl.o.a(com.flurry.android.monolithic.sdk.impl.o, android.view.View):android.view.View
      com.flurry.android.monolithic.sdk.impl.o.a(com.flurry.android.monolithic.sdk.impl.o, android.webkit.WebChromeClient$CustomViewCallback):android.webkit.WebChromeClient$CustomViewCallback
      com.flurry.android.monolithic.sdk.impl.o.a(com.flurry.android.monolithic.sdk.impl.o, android.widget.FrameLayout):android.widget.FrameLayout
      com.flurry.android.monolithic.sdk.impl.o.a(com.flurry.android.monolithic.sdk.impl.o, java.lang.String):com.flurry.android.impl.ads.avro.protocol.v6.AdUnit
      com.flurry.android.monolithic.sdk.impl.o.a(java.util.List<com.flurry.android.impl.ads.avro.protocol.v6.AdUnit>, java.lang.String):java.lang.String
      com.flurry.android.monolithic.sdk.impl.o.a(int, int):void
      com.flurry.android.monolithic.sdk.impl.o.a(com.flurry.android.monolithic.sdk.impl.i, int):void
      com.flurry.android.monolithic.sdk.impl.o.a(com.flurry.android.monolithic.sdk.impl.o, boolean):boolean */
    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        ja.a(3, this.a.f, "onPageStarted: url = " + str);
        if (str != null && webView != null && webView == this.a.l) {
            this.a.h();
            this.a.m();
            boolean unused = this.a.x = false;
            boolean unused2 = this.a.y = false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.o.b(com.flurry.android.monolithic.sdk.impl.o, boolean):boolean
     arg types: [com.flurry.android.monolithic.sdk.impl.o, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.o.b(com.flurry.android.monolithic.sdk.impl.o, java.lang.String):com.flurry.android.monolithic.sdk.impl.m
      com.flurry.android.monolithic.sdk.impl.o.b(int, int):void
      com.flurry.android.monolithic.sdk.impl.o.b(com.flurry.android.monolithic.sdk.impl.o, boolean):boolean */
    public void onPageFinished(WebView webView, String str) {
        ja.a(3, this.a.f, "onPageFinished: url = " + str);
        if (str != null && webView != null && webView == this.a.l) {
            this.a.g();
            this.a.j();
            if (this.a.h != null && this.a.h.isShowing()) {
                this.a.h.dismiss();
            }
            if (!this.a.a(this.a.l) && (this.a.getCurrentBinding() == 2 || this.a.getCurrentBinding() == 1)) {
                ja.a(3, this.a.f, "adding WebView to AdUnityView");
                this.a.addView(this.a.l);
            }
            boolean unused = this.a.x = true;
            if (this.a.y) {
                this.a.o();
                this.a.p();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.bi.a(android.content.Context, java.lang.String, boolean, com.flurry.android.impl.ads.avro.protocol.v6.AdUnit):void
     arg types: [android.content.Context, java.lang.String, int, com.flurry.android.impl.ads.avro.protocol.v6.AdUnit]
     candidates:
      com.flurry.android.monolithic.sdk.impl.bi.a(java.lang.String, int, int, boolean):org.apache.http.HttpResponse
      com.flurry.android.monolithic.sdk.impl.bi.a(com.flurry.android.monolithic.sdk.impl.m, com.flurry.android.impl.ads.avro.protocol.v6.AdUnit, com.flurry.android.monolithic.sdk.impl.i, java.lang.String):java.lang.String
      com.flurry.android.monolithic.sdk.impl.bi.a(android.content.Context, java.lang.String, boolean, com.flurry.android.impl.ads.avro.protocol.v6.AdUnit):void */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x011d  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0129  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean shouldOverrideUrlLoading(android.webkit.WebView r9, java.lang.String r10) {
        /*
            r8 = this;
            r0 = 3
            com.flurry.android.monolithic.sdk.impl.o r1 = r8.a
            java.lang.String r1 = r1.f
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "shouldOverrideUrlLoading: url = "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r10)
            java.lang.String r2 = r2.toString()
            com.flurry.android.monolithic.sdk.impl.ja.a(r0, r1, r2)
            if (r10 == 0) goto L_0x0029
            if (r9 == 0) goto L_0x0029
            com.flurry.android.monolithic.sdk.impl.o r0 = r8.a
            android.webkit.WebView r0 = r0.l
            if (r9 == r0) goto L_0x002b
        L_0x0029:
            r0 = 0
        L_0x002a:
            return r0
        L_0x002b:
            com.flurry.android.monolithic.sdk.impl.o r0 = r8.a
            android.webkit.WebView r0 = r0.l
            java.lang.String r0 = r0.getUrl()
            java.lang.String r0 = com.flurry.android.monolithic.sdk.impl.cp.b(r0)
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 != 0) goto L_0x0140
            boolean r1 = r10.startsWith(r0)
            if (r1 == 0) goto L_0x0140
            int r0 = r0.length()
            java.lang.String r0 = r10.substring(r0)
            android.net.Uri r1 = android.net.Uri.parse(r0)
            boolean r2 = r1.isHierarchical()
            if (r2 == 0) goto L_0x0140
            java.lang.String r2 = r1.getScheme()
            boolean r2 = android.text.TextUtils.isEmpty(r2)
            if (r2 != 0) goto L_0x0140
            java.lang.String r1 = r1.getAuthority()
            boolean r1 = android.text.TextUtils.isEmpty(r1)
            if (r1 != 0) goto L_0x0140
            r1 = 3
            com.flurry.android.monolithic.sdk.impl.o r2 = r8.a
            java.lang.String r2 = r2.f
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "shouldOverrideUrlLoading: target url = "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r3 = r3.toString()
            com.flurry.android.monolithic.sdk.impl.ja.a(r1, r2, r3)
            r7 = r0
        L_0x0089:
            android.net.Uri r0 = android.net.Uri.parse(r7)
            java.lang.String r1 = r0.getScheme()
            if (r1 == 0) goto L_0x00fd
            java.lang.String r1 = r0.getScheme()
            java.lang.String r2 = "flurry"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x00fd
            java.lang.String r1 = "event"
            java.lang.String r1 = r0.getQueryParameter(r1)
            if (r1 == 0) goto L_0x00e7
            com.flurry.android.monolithic.sdk.impl.o r2 = r8.a
            java.util.List r2 = r2.H
            r2.add(r1)
            java.lang.String r0 = r0.getEncodedQuery()
            java.util.Map r2 = com.flurry.android.monolithic.sdk.impl.je.f(r0)
            java.lang.String r0 = "guid"
            boolean r0 = r2.containsKey(r0)
            if (r0 == 0) goto L_0x00ea
            com.flurry.android.monolithic.sdk.impl.o r0 = r8.a
            java.lang.String r3 = "guid"
            java.lang.Object r9 = r2.get(r3)
            java.lang.String r9 = (java.lang.String) r9
            com.flurry.android.impl.ads.avro.protocol.v6.AdUnit r3 = r0.c(r9)
            com.flurry.android.monolithic.sdk.impl.o r0 = r8.a
            java.lang.String r4 = "guid"
            java.lang.Object r9 = r2.get(r4)
            java.lang.String r9 = (java.lang.String) r9
            com.flurry.android.monolithic.sdk.impl.m r4 = r0.b(r9)
            if (r3 == 0) goto L_0x00e7
            if (r4 == 0) goto L_0x00e7
            com.flurry.android.monolithic.sdk.impl.o r0 = r8.a
            r5 = 0
            r6 = 0
            r0.a(r1, r2, r3, r4, r5, r6)
        L_0x00e7:
            r0 = 1
            goto L_0x002a
        L_0x00ea:
            com.flurry.android.monolithic.sdk.impl.o r0 = r8.a
            com.flurry.android.monolithic.sdk.impl.o r3 = r8.a
            com.flurry.android.impl.ads.avro.protocol.v6.AdUnit r3 = r3.d
            com.flurry.android.monolithic.sdk.impl.o r4 = r8.a
            com.flurry.android.monolithic.sdk.impl.m r4 = r4.c
            com.flurry.android.monolithic.sdk.impl.o r5 = r8.a
            int r5 = r5.e
            r6 = 0
            r0.a(r1, r2, r3, r4, r5, r6)
            goto L_0x00e7
        L_0x00fd:
            com.flurry.android.monolithic.sdk.impl.o r0 = r8.a
            java.lang.String r1 = "clicked"
            java.util.Map r2 = java.util.Collections.emptyMap()
            com.flurry.android.monolithic.sdk.impl.o r3 = r8.a
            com.flurry.android.impl.ads.avro.protocol.v6.AdUnit r3 = r3.d
            com.flurry.android.monolithic.sdk.impl.o r4 = r8.a
            com.flurry.android.monolithic.sdk.impl.m r4 = r4.c
            com.flurry.android.monolithic.sdk.impl.o r5 = r8.a
            int r5 = r5.e
            r6 = 0
            r0.a(r1, r2, r3, r4, r5, r6)
            com.flurry.android.monolithic.sdk.impl.o r0 = r8.a
            boolean r0 = r0.d()
            if (r0 == 0) goto L_0x0129
            com.flurry.android.monolithic.sdk.impl.o r0 = r8.a
            android.webkit.WebView r0 = r0.l
            r0.loadUrl(r7)
        L_0x0126:
            r0 = 1
            goto L_0x002a
        L_0x0129:
            com.flurry.android.monolithic.sdk.impl.o r0 = r8.a
            com.flurry.android.impl.ads.FlurryAdModule r0 = r0.b
            com.flurry.android.monolithic.sdk.impl.bi r0 = r0.a()
            com.flurry.android.monolithic.sdk.impl.o r1 = r8.a
            android.content.Context r1 = r1.getContext()
            r2 = 1
            com.flurry.android.monolithic.sdk.impl.o r3 = r8.a
            com.flurry.android.impl.ads.avro.protocol.v6.AdUnit r3 = r3.d
            r0.a(r1, r7, r2, r3)
            goto L_0x0126
        L_0x0140:
            r7 = r10
            goto L_0x0089
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.aa.shouldOverrideUrlLoading(android.webkit.WebView, java.lang.String):boolean");
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        if (this.a.h != null && this.a.h.isShowing()) {
            this.a.h.dismiss();
        }
        this.a.a("renderFailed", Collections.emptyMap(), this.a.d, this.a.c, this.a.e, 0);
    }
}
