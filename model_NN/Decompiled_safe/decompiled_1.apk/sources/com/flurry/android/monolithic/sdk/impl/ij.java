package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import android.os.Build;

public final class ij implements ik {
    private static ij a;
    private final ik b = b();

    public static synchronized ij a() {
        ij ijVar;
        synchronized (ij.class) {
            if (a == null) {
                a = new ij();
            }
            ijVar = a;
        }
        return ijVar;
    }

    public void a(Context context) {
        if (this.b != null) {
            this.b.a(context);
        }
    }

    public void b(Context context) {
        if (this.b != null) {
            this.b.b(context);
        }
    }

    private static ik b() {
        if (c()) {
            return ii.a();
        }
        return null;
    }

    private static boolean c() {
        return Build.VERSION.SDK_INT >= 8;
    }
}
