package com.flurry.android.monolithic.sdk.impl;

import com.flurry.android.impl.ads.FlurryAdModule;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;
import java.nio.ByteBuffer;
import java.util.Map;

public class ck {
    private static final String a = ck.class.getSimpleName();
    private final FlurryAdModule b;

    public ck(FlurryAdModule flurryAdModule) {
        this.b = flurryAdModule;
    }

    private boolean a(String str, String str2) {
        return str2.equals("%{" + str + "}");
    }

    public String a(m mVar, AdUnit adUnit, i iVar, String str, String str2) {
        if (a("fids", str2)) {
            StringBuilder sb = new StringBuilder();
            sb.append(eg.i()).append(":").append(this.b.l());
            Map<ie, ByteBuffer> m = this.b.m();
            if (m != null) {
                for (Map.Entry next : m.entrySet()) {
                    sb.append(",").append(((ie) next.getKey()).c).append(":");
                    if (((ie) next.getKey()).d) {
                        sb.append(((ByteBuffer) next.getValue()).array());
                    } else {
                        sb.append(je.a(((ByteBuffer) next.getValue()).array()));
                    }
                }
            }
            ja.a(3, a, "Replacing param fids with: " + sb.toString());
            return str.replace(str2, je.b(sb.toString()));
        } else if (a("sid", str2)) {
            String valueOf = String.valueOf(this.b.d());
            ja.a(3, a, "Replacing param sid with: " + valueOf);
            return str.replace(str2, je.b(valueOf));
        } else if (a("lid", str2)) {
            String valueOf2 = String.valueOf(mVar.a());
            ja.a(3, a, "Replacing param lid with: " + valueOf2);
            return str.replace(str2, je.b(valueOf2));
        } else if (a("guid", str2)) {
            String b2 = mVar.b();
            ja.a(3, a, "Replacing param guid with: " + b2);
            return str.replace(str2, je.b(b2));
        } else if (a("ats", str2)) {
            String valueOf3 = String.valueOf(System.currentTimeMillis());
            ja.a(3, a, "Replacing param ats with: " + valueOf3);
            return str.replace(str2, je.b(valueOf3));
        } else if (a("apik", str2)) {
            String f = this.b.f();
            ja.a(3, a, "Replacing param apik with: " + f);
            return str.replace(str2, je.b(f));
        } else if (a("hid", str2)) {
            String obj = adUnit.b().toString();
            ja.a(3, a, "Replacing param hid with: " + obj);
            return str.replace(str2, je.b(obj));
        } else if (a("eso", str2)) {
            String l = Long.toString(System.currentTimeMillis() - this.b.d());
            ja.a(3, a, "Replacing param eso with: " + l);
            return str.replace(str2, je.b(l));
        } else if (a("uc", str2)) {
            String str3 = "";
            for (Map.Entry next2 : this.b.v().entrySet()) {
                str3 = str3 + "c_" + je.b((String) next2.getKey()) + "=" + je.b((String) next2.getValue()) + "&";
            }
            ja.a(3, a, "Replacing param uc with: " + str3);
            String replace = str.replace(str2, str3);
            if (!str3.equals("") || replace.length() <= 0) {
                return replace;
            }
            return replace.substring(0, replace.length() - 1);
        } else {
            ja.a(3, a, "Unknown param: " + str2);
            return str.replace(str2, "");
        }
    }
}
