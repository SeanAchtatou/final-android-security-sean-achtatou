package com.flurry.android.monolithic.sdk.impl;

import com.flurry.android.impl.appcloud.AppCloudModule;

class ht {
    private ho a = new ho();
    private hu b = new hu();

    public hn a(hm hmVar) {
        long a2 = this.b.a(hmVar);
        if (a2 == -1) {
            ja.a(4, "RetryManager", "timeToStart == OPERATION_DIDNT_PROCCEED_CODE");
        } else {
            try {
                Thread.sleep(a2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return b(hmVar);
    }

    public hn b(hm hmVar) {
        hn hnVar;
        hmVar.e();
        if (a()) {
            hnVar = this.a.a(hmVar);
        } else {
            hnVar = new hn();
            hnVar.a = 711;
            hnVar.b = new fq(null);
        }
        switch (hnVar.a) {
            case 701:
            case 731:
                return hnVar;
            case 711:
            case 721:
                if (this.b.a(hmVar) != -1) {
                    return a(hmVar);
                }
                return hnVar;
            default:
                ja.a(6, "RetryManager", "ERROR! UNKNOWN RESPONSE CODE!");
                return hnVar;
        }
    }

    private boolean a() {
        return AppCloudModule.getInstance().d();
    }
}
