package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

abstract class ve<T> extends vo<T> {
    protected ve(Class<T> cls) {
        super((Class<?>) cls);
    }

    public Object a(ow owVar, qm qmVar, rw rwVar) throws IOException, oz {
        return rwVar.b(owVar, qmVar);
    }
}
