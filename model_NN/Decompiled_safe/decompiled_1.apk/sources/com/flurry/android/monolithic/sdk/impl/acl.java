package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

public final class acl extends abq<AtomicInteger> {
    public acl() {
        super(AtomicInteger.class, false);
    }

    public void a(AtomicInteger atomicInteger, or orVar, ru ruVar) throws IOException, oq {
        orVar.b(atomicInteger.get());
    }
}
