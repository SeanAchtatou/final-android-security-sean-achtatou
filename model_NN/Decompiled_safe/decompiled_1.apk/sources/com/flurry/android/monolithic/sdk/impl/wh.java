package com.flurry.android.monolithic.sdk.impl;

import java.util.Calendar;
import java.util.Date;

final class wh extends we {
    protected wh() {
        super(Calendar.class);
    }

    /* renamed from: c */
    public Calendar b(String str, qm qmVar) throws IllegalArgumentException, qw {
        Date a = qmVar.a(str);
        if (a == null) {
            return null;
        }
        return qmVar.a(a);
    }
}
