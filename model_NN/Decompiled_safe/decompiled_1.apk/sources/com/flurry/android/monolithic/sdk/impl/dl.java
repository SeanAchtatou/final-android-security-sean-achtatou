package com.flurry.android.monolithic.sdk.impl;

import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import java.util.Collections;

final class dl implements AdListener {
    final /* synthetic */ dj a;

    private dl(dj djVar) {
        this.a = djVar;
    }

    public void onReceiveAd(Ad ad) {
        if (ad == this.a.f) {
            ja.a(3, dj.b, "Admob Interstitial received takeover.");
            this.a.a(Collections.emptyMap());
            this.a.f.show();
        }
    }

    public void onFailedToReceiveAd(Ad ad, AdRequest.ErrorCode errorCode) {
        this.a.d(Collections.emptyMap());
        ja.a(3, dj.b, "Admob Interstitial failed to receive takeover.");
    }

    public void onPresentScreen(Ad ad) {
        ja.a(3, dj.b, "Admob Interstitial present on screen.");
    }

    public void onDismissScreen(Ad ad) {
        this.a.c(Collections.emptyMap());
        ja.a(3, dj.b, "Admob Interstitial dismissed from screen.");
    }

    public void onLeaveApplication(Ad ad) {
        this.a.b(Collections.emptyMap());
        ja.a(4, dj.b, "Admob Interstitial leave application.");
    }
}
