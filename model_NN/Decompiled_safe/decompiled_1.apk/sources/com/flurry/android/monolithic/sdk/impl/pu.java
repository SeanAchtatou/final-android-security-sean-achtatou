package com.flurry.android.monolithic.sdk.impl;

public final class pu {
    static final String a = String.valueOf(Long.MIN_VALUE);
    static final char[] b = new char[4000];
    static final char[] c = new char[4000];
    static final byte[] d = new byte[4000];
    static final String[] e = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
    static final String[] f = {"-1", "-2", "-3", "-4", "-5", "-6", "-7", "-8", "-9", "-10"};
    private static int g = 1000000;
    private static int h = 1000000000;
    private static long i = 10000000000L;
    private static long j = 1000;
    private static long k = -2147483648L;
    private static long l = 2147483647L;

    static {
        char c2;
        char c3;
        int i2 = 0;
        int i3 = 0;
        while (i2 < 10) {
            char c4 = (char) (i2 + 48);
            if (i2 == 0) {
                c2 = 0;
            } else {
                c2 = c4;
            }
            int i4 = i3;
            int i5 = 0;
            while (i5 < 10) {
                char c5 = (char) (i5 + 48);
                if (i2 == 0 && i5 == 0) {
                    c3 = 0;
                } else {
                    c3 = c5;
                }
                int i6 = i4;
                for (int i7 = 0; i7 < 10; i7++) {
                    char c6 = (char) (i7 + 48);
                    b[i6] = c2;
                    b[i6 + 1] = c3;
                    b[i6 + 2] = c6;
                    c[i6] = c4;
                    c[i6 + 1] = c5;
                    c[i6 + 2] = c6;
                    i6 += 4;
                }
                i5++;
                i4 = i6;
            }
            i2++;
            i3 = i4;
        }
        for (int i8 = 0; i8 < 4000; i8++) {
            d[i8] = (byte) c[i8];
        }
    }

    public static int a(int i2, char[] cArr, int i3) {
        int i4;
        int i5;
        int b2;
        if (i2 >= 0) {
            i4 = i3;
            i5 = i2;
        } else if (i2 == Integer.MIN_VALUE) {
            return a((long) i2, cArr, i3);
        } else {
            i4 = i3 + 1;
            cArr[i3] = '-';
            i5 = -i2;
        }
        if (i5 >= g) {
            boolean z = i5 >= h;
            if (z) {
                i5 -= h;
                if (i5 >= h) {
                    i5 -= h;
                    cArr[i4] = '2';
                    i4++;
                } else {
                    cArr[i4] = '1';
                    i4++;
                }
            }
            int i6 = i5 / 1000;
            int i7 = i5 - (i6 * 1000);
            int i8 = i6 / 1000;
            int i9 = i6 - (i8 * 1000);
            if (z) {
                b2 = c(i8, cArr, i4);
            } else {
                b2 = b(i8, cArr, i4);
            }
            return c(i7, cArr, c(i9, cArr, b2));
        } else if (i5 >= 1000) {
            int i10 = i5 / 1000;
            return c(i5 - (i10 * 1000), cArr, b(i10, cArr, i4));
        } else if (i5 >= 10) {
            return b(i5, cArr, i4);
        } else {
            cArr[i4] = (char) (i5 + 48);
            return i4 + 1;
        }
    }

    public static int a(long j2, char[] cArr, int i2) {
        int i3;
        long j3;
        if (j2 < 0) {
            if (j2 > k) {
                return a((int) j2, cArr, i2);
            }
            if (j2 == Long.MIN_VALUE) {
                int length = a.length();
                a.getChars(0, length, cArr, i2);
                return length + i2;
            }
            i3 = i2 + 1;
            cArr[i2] = '-';
            j3 = -j2;
        } else if (j2 <= l) {
            return a((int) j2, cArr, i2);
        } else {
            i3 = i2;
            j3 = j2;
        }
        int b2 = b(j3) + i3;
        long j4 = j3;
        int i4 = b2;
        while (j4 > l) {
            i4 -= 3;
            long j5 = j4 / j;
            c((int) (j4 - (j * j5)), cArr, i4);
            j4 = j5;
        }
        int i5 = i4;
        int i6 = (int) j4;
        while (i6 >= 1000) {
            i5 -= 3;
            int i7 = i6 / 1000;
            c(i6 - (i7 * 1000), cArr, i5);
            i6 = i7;
        }
        b(i6, cArr, i3);
        return b2;
    }

    public static String a(int i2) {
        if (i2 < e.length) {
            if (i2 >= 0) {
                return e[i2];
            }
            int i3 = (-i2) - 1;
            if (i3 < f.length) {
                return f[i3];
            }
        }
        return Integer.toString(i2);
    }

    public static String a(long j2) {
        if (j2 > 2147483647L || j2 < -2147483648L) {
            return Long.toString(j2);
        }
        return a((int) j2);
    }

    public static String a(double d2) {
        return Double.toString(d2);
    }

    private static int b(int i2, char[] cArr, int i3) {
        int i4;
        int i5 = i2 << 2;
        int i6 = i5 + 1;
        char c2 = b[i5];
        if (c2 != 0) {
            cArr[i3] = c2;
            i4 = i3 + 1;
        } else {
            i4 = i3;
        }
        int i7 = i6 + 1;
        char c3 = b[i6];
        if (c3 != 0) {
            cArr[i4] = c3;
            i4++;
        }
        int i8 = i4 + 1;
        cArr[i4] = b[i7];
        return i8;
    }

    private static int c(int i2, char[] cArr, int i3) {
        int i4 = i2 << 2;
        int i5 = i3 + 1;
        int i6 = i4 + 1;
        cArr[i3] = c[i4];
        int i7 = i5 + 1;
        cArr[i5] = c[i6];
        int i8 = i7 + 1;
        cArr[i7] = c[i6 + 1];
        return i8;
    }

    private static int b(long j2) {
        int i2 = 10;
        for (long j3 = i; j2 >= j3 && i2 != 19; j3 = (j3 << 1) + (j3 << 3)) {
            i2++;
        }
        return i2;
    }
}
