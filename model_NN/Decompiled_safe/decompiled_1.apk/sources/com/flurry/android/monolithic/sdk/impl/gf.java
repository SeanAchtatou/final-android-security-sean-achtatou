package com.flurry.android.monolithic.sdk.impl;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

public abstract class gf extends ge {
    protected String c;

    public abstract hk b(String str);

    public void a(gi giVar) {
        ja.a(4, this.c, "--- Insert in " + this.b + " : ---");
        SQLiteDatabase writableDatabase = this.a.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        String str = giVar.c;
        String str2 = giVar.d;
        String str3 = giVar.e;
        String str4 = giVar.a;
        String str5 = giVar.b;
        contentValues.put("objectsId", str);
        contentValues.put("timestamp", Long.valueOf(System.currentTimeMillis()));
        contentValues.put("objectsLocalId", str2);
        contentValues.put("collectionName", str3);
        contentValues.put("key", str4);
        contentValues.put("value", str5);
        writableDatabase.insert(this.b, null, contentValues);
    }

    public String a(String str, String str2, String str3) {
        String str4;
        Cursor query = this.a.getReadableDatabase().query(this.b, null, "objectsId='" + str2 + "'", null, null, null, "objectsId ASC, julianday(timestamp)");
        if (query.moveToFirst()) {
            int columnIndex = query.getColumnIndex("key");
            int columnIndex2 = query.getColumnIndex("value");
            int columnIndex3 = query.getColumnIndex("collectionName");
            while (true) {
                String string = query.getString(columnIndex3);
                String string2 = query.getString(columnIndex);
                if (TextUtils.isEmpty(string) || !string.equals(str3) || !string2.equals(str)) {
                    if (!TextUtils.isEmpty(string) || !TextUtils.isEmpty(str3) || !string2.equals(str)) {
                        if (!query.moveToNext()) {
                            break;
                        }
                    } else {
                        str4 = query.getString(columnIndex2);
                        break;
                    }
                } else {
                    str4 = query.getString(columnIndex2);
                    break;
                }
            }
        } else {
            str4 = null;
        }
        query.close();
        return str4;
    }

    public void a(String str, String str2, String str3, String str4) {
        boolean z;
        ja.a(4, this.c, "--- update item in " + this.b + ": ---");
        Cursor query = this.a.getReadableDatabase().query(this.b, null, "objectsId='" + str3 + "'", null, null, null, "objectsId ASC, julianday(timestamp)");
        if (query.moveToFirst()) {
            int columnIndex = query.getColumnIndex("primery_key_id");
            int columnIndex2 = query.getColumnIndex("key");
            while (true) {
                int i = query.getInt(columnIndex);
                if (!query.getString(columnIndex2).equals(str)) {
                    if (!query.moveToNext()) {
                        break;
                    }
                } else {
                    a(i, str, str2, str3, null, str4);
                    z = true;
                    break;
                }
            }
            z = false;
        } else {
            z = false;
        }
        if (!z) {
            a(new gi(str, str2, str3, null, str4));
        }
        query.close();
    }

    /* access modifiers changed from: package-private */
    public void a(int i, String str, String str2, String str3, String str4, String str5) {
        SQLiteDatabase writableDatabase = this.a.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("objectsId", str3);
        contentValues.put("objectsLocalId", str4);
        contentValues.put("timestamp", Long.valueOf(System.currentTimeMillis()));
        contentValues.put("collectionName", str5);
        contentValues.put("key", str);
        contentValues.put("value", str2);
        writableDatabase.update(this.b, contentValues, "primery_key_id='" + i + "'", null);
    }
}
