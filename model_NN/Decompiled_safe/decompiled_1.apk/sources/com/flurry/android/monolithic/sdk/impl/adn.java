package com.flurry.android.monolithic.sdk.impl;

import java.util.StringTokenizer;

final class adn extends StringTokenizer {
    protected final String a;
    protected int b;
    protected String c;

    public adn(String str) {
        super(str, "<,>", true);
        this.a = str;
    }

    public boolean hasMoreTokens() {
        return this.c != null || super.hasMoreTokens();
    }

    public String nextToken() {
        String nextToken;
        if (this.c != null) {
            nextToken = this.c;
            this.c = null;
        } else {
            nextToken = super.nextToken();
        }
        this.b += nextToken.length();
        return nextToken;
    }

    public void a(String str) {
        this.c = str;
        this.b -= str.length();
    }

    public String a() {
        return this.a;
    }

    public String b() {
        return this.a.substring(this.b);
    }
}
