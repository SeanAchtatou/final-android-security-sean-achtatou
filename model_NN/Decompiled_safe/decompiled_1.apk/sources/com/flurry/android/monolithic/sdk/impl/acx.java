package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.TimeZone;

public class acx extends abq<TimeZone> {
    public static final acx a = new acx();

    public acx() {
        super(TimeZone.class);
    }

    public void a(TimeZone timeZone, or orVar, ru ruVar) throws IOException, oq {
        orVar.b(timeZone.getID());
    }

    public void a(TimeZone timeZone, or orVar, ru ruVar, rx rxVar) throws IOException, oq {
        rxVar.a(timeZone, orVar, TimeZone.class);
        a(timeZone, orVar, ruVar);
        rxVar.d(timeZone, orVar);
    }
}
