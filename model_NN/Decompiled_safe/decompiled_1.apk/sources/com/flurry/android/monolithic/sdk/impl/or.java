package com.flurry.android.monolithic.sdk.impl;

import java.io.Closeable;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;

public abstract class or implements Closeable {
    protected pd a;

    public abstract or a();

    public abstract void a(char c) throws IOException, oq;

    public abstract void a(double d) throws IOException, oq;

    public abstract void a(float f) throws IOException, oq;

    public abstract void a(long j) throws IOException, oq;

    public abstract void a(on onVar, byte[] bArr, int i, int i2) throws IOException, oq;

    public abstract void a(ou ouVar) throws IOException, oz;

    public abstract void a(Object obj) throws IOException, oz;

    public abstract void a(String str) throws IOException, oq;

    public abstract void a(BigDecimal bigDecimal) throws IOException, oq;

    public abstract void a(BigInteger bigInteger) throws IOException, oq;

    public abstract void a(boolean z) throws IOException, oq;

    public abstract void a(char[] cArr, int i, int i2) throws IOException, oq;

    public abstract void b() throws IOException, oq;

    public abstract void b(int i) throws IOException, oq;

    public abstract void b(String str) throws IOException, oq;

    public abstract void b(char[] cArr, int i, int i2) throws IOException, oq;

    public abstract void c() throws IOException, oq;

    public abstract void c(String str) throws IOException, oq;

    public abstract void d() throws IOException, oq;

    public abstract void d(String str) throws IOException, oq;

    public abstract void e() throws IOException, oq;

    public abstract void e(String str) throws IOException, oq, UnsupportedOperationException;

    public abstract void f() throws IOException, oq;

    public abstract void g() throws IOException;

    protected or() {
    }

    public or a(pd pdVar) {
        this.a = pdVar;
        return this;
    }

    public or a(int i) {
        return this;
    }

    public or a(pp ppVar) {
        return this;
    }

    public void a(pw pwVar) throws IOException, oq {
        a(pwVar.a());
    }

    public void a(pe peVar) throws IOException, oq {
        a(peVar.a());
    }

    public void b(pe peVar) throws IOException, oq {
        b(peVar.a());
    }

    public void a(byte[] bArr) throws IOException, oq {
        a(oo.a(), bArr, 0, bArr.length);
    }

    public void a(String str, String str2) throws IOException, oq {
        a(str);
        b(str2);
    }

    public final void a(String str, int i) throws IOException, oq {
        a(str);
        b(i);
    }

    public final void f(String str) throws IOException, oq {
        a(str);
        b();
    }

    public final void g(String str) throws IOException, oq {
        a(str);
        d();
    }
}
