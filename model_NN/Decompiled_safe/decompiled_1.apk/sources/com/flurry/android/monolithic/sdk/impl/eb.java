package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import com.flurry.android.AdCreative;
import com.flurry.android.impl.ads.FlurryAdModule;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;
import java.util.ArrayList;
import java.util.List;

public final class eb extends cy {
    /* access modifiers changed from: protected */
    public String f() {
        return "Mobclix";
    }

    /* access modifiers changed from: protected */
    public List<cu> g() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new cu("mobclix", "3.2.1", "com.mobclix.android.sdk.MobclixFullScreenAdView"));
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public List<ActivityInfo> j() {
        ArrayList arrayList = new ArrayList();
        ActivityInfo activityInfo = new ActivityInfo();
        activityInfo.name = "com.mobclix.android.sdk.MobclixBrowserActivity";
        arrayList.add(activityInfo);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public List<cu> k() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new cu("mobclix", "3.2.1", "com.mobclix.android.sdk.MobclixAdView"));
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public List<String> n() {
        ArrayList arrayList = new ArrayList();
        arrayList.add("com.mobclix.APPLICATION_ID");
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public List<String> o() {
        ArrayList arrayList = new ArrayList();
        arrayList.add("android.permission.INTERNET");
        arrayList.add("android.permission.READ_PHONE_STATE");
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public cn a(Context context, FlurryAdModule flurryAdModule, m mVar, AdUnit adUnit, Bundle bundle) {
        if (context == null || flurryAdModule == null || mVar == null || adUnit == null || bundle == null) {
            return null;
        }
        return new ee(context, flurryAdModule, mVar, adUnit, bundle);
    }

    /* access modifiers changed from: protected */
    public ac a(Context context, FlurryAdModule flurryAdModule, m mVar, AdCreative adCreative, Bundle bundle) {
        if (context == null || flurryAdModule == null || mVar == null || adCreative == null || bundle == null) {
            return null;
        }
        return new ec(context, flurryAdModule, mVar, adCreative, bundle);
    }
}
