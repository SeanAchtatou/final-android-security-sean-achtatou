package com.flurry.android.monolithic.sdk.impl;

public enum na {
    TERMINAL,
    ROOT,
    SEQUENCE,
    REPEATER,
    ALTERNATIVE,
    IMPLICIT_ACTION,
    EXPLICIT_ACTION
}
