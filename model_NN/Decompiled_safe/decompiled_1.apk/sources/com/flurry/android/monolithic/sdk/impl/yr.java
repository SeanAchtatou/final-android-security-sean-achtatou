package com.flurry.android.monolithic.sdk.impl;

import com.flurry.org.codehaus.jackson.annotate.JsonTypeInfo;
import java.io.IOException;

public class yr extends yy {
    public yr(afm afm, yi yiVar, qc qcVar, Class<?> cls) {
        super(afm, yiVar, qcVar, null);
    }

    public JsonTypeInfo.As a() {
        return JsonTypeInfo.As.WRAPPER_OBJECT;
    }

    public Object a(ow owVar, qm qmVar) throws IOException, oz {
        return e(owVar, qmVar);
    }

    public Object b(ow owVar, qm qmVar) throws IOException, oz {
        return e(owVar, qmVar);
    }

    public Object c(ow owVar, qm qmVar) throws IOException, oz {
        return e(owVar, qmVar);
    }

    public Object d(ow owVar, qm qmVar) throws IOException, oz {
        return e(owVar, qmVar);
    }

    private final Object e(ow owVar, qm qmVar) throws IOException, oz {
        if (owVar.e() != pb.START_OBJECT) {
            throw qmVar.a(owVar, pb.START_OBJECT, "need JSON Object to contain As.WRAPPER_OBJECT type information for class " + c());
        } else if (owVar.b() != pb.FIELD_NAME) {
            throw qmVar.a(owVar, pb.FIELD_NAME, "need JSON String that contains type id (for subtype of " + c() + ")");
        } else {
            qu<Object> a = a(qmVar, owVar.k());
            owVar.b();
            Object a2 = a.a(owVar, qmVar);
            if (owVar.b() == pb.END_OBJECT) {
                return a2;
            }
            throw qmVar.a(owVar, pb.END_OBJECT, "expected closing END_OBJECT after type information and deserialized value");
        }
    }
}
