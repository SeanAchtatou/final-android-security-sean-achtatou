package com.flurry.android.monolithic.sdk.impl;

public class ku implements lc {
    private ji a;
    private String b;

    public ku(ji jiVar, String str) {
        this.a = jiVar;
        this.b = str;
    }

    public ji a() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return (obj instanceof lc) && this.b.equals(obj.toString());
    }

    public int hashCode() {
        return this.b.hashCode();
    }

    public String toString() {
        return this.b;
    }
}
