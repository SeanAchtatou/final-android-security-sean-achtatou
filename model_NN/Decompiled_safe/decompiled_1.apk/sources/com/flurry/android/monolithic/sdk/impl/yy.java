package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.HashMap;

public abstract class yy extends rw {
    protected final yi b;
    protected final afm c;
    protected final qc d;
    protected final afm e;
    protected final HashMap<String, qu<Object>> f = new HashMap<>();
    protected qu<Object> g;

    protected yy(afm afm, yi yiVar, qc qcVar, Class<?> cls) {
        this.c = afm;
        this.b = yiVar;
        this.d = qcVar;
        if (cls == null) {
            this.e = null;
        } else {
            this.e = afm.g(cls);
        }
    }

    public String c() {
        return this.c.p().getName();
    }

    public String b() {
        return null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append('[').append(getClass().getName());
        sb.append("; base-type:").append(this.c);
        sb.append("; id-resolver: ").append(this.b);
        sb.append(']');
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public final qu<Object> a(qm qmVar, String str) throws IOException, oz {
        qu<Object> quVar;
        synchronized (this.f) {
            quVar = this.f.get(str);
            if (quVar == null) {
                afm a = this.b.a(str);
                if (a != null) {
                    if (this.c != null && this.c.getClass() == a.getClass()) {
                        a = this.c.f(a.p());
                    }
                    quVar = qmVar.b().a(qmVar.a(), a, this.d);
                } else if (this.e == null) {
                    throw qmVar.a(this.c, str);
                } else {
                    quVar = a(qmVar);
                }
                this.f.put(str, quVar);
            }
        }
        return quVar;
    }

    /* access modifiers changed from: protected */
    public final qu<Object> a(qm qmVar) throws IOException, oz {
        qu<Object> quVar;
        if (this.e == null) {
            return null;
        }
        synchronized (this.e) {
            if (this.g == null) {
                this.g = qmVar.b().a(qmVar.a(), this.e, this.d);
            }
            quVar = this.g;
        }
        return quVar;
    }
}
