package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.Collection;

@rz
public final class wx extends ug<Collection<String>> implements ro {
    protected final afm a;
    protected final qu<String> b;
    protected final boolean c;
    protected final th d;
    protected qu<Object> e;

    public /* bridge */ /* synthetic */ Object a(ow owVar, qm qmVar, Object obj) throws IOException, oz {
        return a(owVar, qmVar, (Collection<String>) ((Collection) obj));
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [com.flurry.android.monolithic.sdk.impl.qu<java.lang.String>, com.flurry.android.monolithic.sdk.impl.qu<?>, com.flurry.android.monolithic.sdk.impl.qu] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public wx(com.flurry.android.monolithic.sdk.impl.afm r2, com.flurry.android.monolithic.sdk.impl.qu<?> r3, com.flurry.android.monolithic.sdk.impl.th r4) {
        /*
            r1 = this;
            java.lang.Class r0 = r2.p()
            r1.<init>(r0)
            r1.a = r2
            r1.b = r3
            r1.d = r4
            boolean r0 = r1.a(r3)
            r1.c = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.wx.<init>(com.flurry.android.monolithic.sdk.impl.afm, com.flurry.android.monolithic.sdk.impl.qu, com.flurry.android.monolithic.sdk.impl.th):void");
    }

    public void a(qk qkVar, qq qqVar) throws qw {
        xo o = this.d.o();
        if (o != null) {
            afm l = this.d.l();
            this.e = a(qkVar, qqVar, l, new qd(null, l, null, o));
        }
    }

    public qu<Object> d() {
        return this.b;
    }

    /* renamed from: b */
    public Collection<String> a(ow owVar, qm qmVar) throws IOException, oz {
        if (this.e != null) {
            return (Collection) this.d.a(this.e.a(owVar, qmVar));
        }
        return a(owVar, qmVar, (Collection<String>) ((Collection) this.d.m()));
    }

    public Collection<String> a(ow owVar, qm qmVar, Collection<String> collection) throws IOException, oz {
        if (!owVar.j()) {
            return c(owVar, qmVar, collection);
        }
        if (!this.c) {
            return b(owVar, qmVar, collection);
        }
        while (true) {
            pb b2 = owVar.b();
            if (b2 == pb.END_ARRAY) {
                return collection;
            }
            collection.add(b2 == pb.VALUE_NULL ? null : owVar.k());
        }
    }

    private Collection<String> b(ow owVar, qm qmVar, Collection<String> collection) throws IOException, oz {
        String a2;
        qu<String> quVar = this.b;
        while (true) {
            pb b2 = owVar.b();
            if (b2 == pb.END_ARRAY) {
                return collection;
            }
            if (b2 == pb.VALUE_NULL) {
                a2 = null;
            } else {
                a2 = quVar.a(owVar, qmVar);
            }
            collection.add(a2);
        }
    }

    public Object a(ow owVar, qm qmVar, rw rwVar) throws IOException, oz {
        return rwVar.b(owVar, qmVar);
    }

    private final Collection<String> c(ow owVar, qm qmVar, Collection<String> collection) throws IOException, oz {
        String k;
        if (!qmVar.a(ql.ACCEPT_SINGLE_VALUE_AS_ARRAY)) {
            throw qmVar.b(this.a.p());
        }
        qu<String> quVar = this.b;
        if (owVar.e() == pb.VALUE_NULL) {
            k = null;
        } else {
            k = quVar == null ? owVar.k() : quVar.a(owVar, qmVar);
        }
        collection.add(k);
        return collection;
    }
}
