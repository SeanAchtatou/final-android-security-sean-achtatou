package com.flurry.android.monolithic.sdk.impl;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import com.flurry.android.impl.ads.FlurryAdModule;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;
import com.jumptap.adtag.JtAdInterstitial;
import com.jumptap.adtag.JtAdWidgetSettings;
import com.jumptap.adtag.JtAdWidgetSettingsFactory;
import com.jumptap.adtag.utils.JtException;

public final class du extends cr {
    /* access modifiers changed from: private */
    public static final String b = du.class.getSimpleName();
    private final String c;
    private final String d;
    private final String e;

    public du(Context context, FlurryAdModule flurryAdModule, m mVar, AdUnit adUnit, Bundle bundle) {
        super(context, flurryAdModule, mVar, adUnit);
        this.c = bundle.getString("com.flurry.jumptap.PUBLISHER_ID");
        this.d = bundle.getString("com.flurry.jumptap.INT_SPOT_ID");
        this.e = bundle.getString("com.flurry.jumptap.INT_SITE_ID");
    }

    public void a() {
        JtAdInterstitial jtAdInterstitial;
        JtAdWidgetSettings createWidgetSettings = JtAdWidgetSettingsFactory.createWidgetSettings();
        createWidgetSettings.setPublisherId(this.c);
        if (!TextUtils.isEmpty(this.d)) {
            createWidgetSettings.setSpotId(this.d);
        }
        if (!TextUtils.isEmpty(this.e)) {
            createWidgetSettings.setSiteId(this.e);
        }
        createWidgetSettings.setApplicationId(il.c(b()));
        createWidgetSettings.setApplicationVersion(il.d(b()));
        createWidgetSettings.setRefreshPeriod(0);
        createWidgetSettings.setShouldSendLocation(false);
        try {
            jtAdInterstitial = new JtAdInterstitial((Activity) b(), createWidgetSettings);
        } catch (JtException e2) {
            ja.a(3, b, "Jumptap JtException when creating ad object.");
            jtAdInterstitial = null;
        }
        jtAdInterstitial.setAdViewListener(new dv(this));
        jtAdInterstitial.showAsPopup();
    }
}
