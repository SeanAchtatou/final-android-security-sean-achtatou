package com.flurry.android.monolithic.sdk.impl;

public enum os {
    AUTO_CLOSE_TARGET(true),
    AUTO_CLOSE_JSON_CONTENT(true),
    QUOTE_FIELD_NAMES(true),
    QUOTE_NON_NUMERIC_NUMBERS(true),
    WRITE_NUMBERS_AS_STRINGS(false),
    FLUSH_PASSED_TO_STREAM(true),
    ESCAPE_NON_ASCII(false);
    
    final boolean h;
    final int i = (1 << ordinal());

    public static int a() {
        int i2 = 0;
        for (os osVar : values()) {
            if (osVar.b()) {
                i2 |= osVar.c();
            }
        }
        return i2;
    }

    private os(boolean z) {
        this.h = z;
    }

    public boolean b() {
        return this.h;
    }

    public int c() {
        return this.i;
    }
}
