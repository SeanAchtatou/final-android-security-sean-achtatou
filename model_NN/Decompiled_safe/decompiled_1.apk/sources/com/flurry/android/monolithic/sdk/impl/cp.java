package com.flurry.android.monolithic.sdk.impl;

import android.text.TextUtils;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.regex.Pattern;
import org.apache.http.client.utils.URIUtils;

public final class cp {
    private static final Pattern a = Pattern.compile("/");

    public static String a(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        URI c = c(str);
        if (c == null) {
            return str;
        }
        URI normalize = c.normalize();
        if (normalize.isOpaque()) {
            return str;
        }
        URI a2 = a(normalize.getScheme(), normalize.getAuthority(), "/", null, null);
        if (a2 == null) {
            return str;
        }
        return a2.toString();
    }

    public static String b(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        URI c = c(str);
        if (c == null) {
            return str;
        }
        URI normalize = c.normalize();
        if (normalize.isOpaque()) {
            return str;
        }
        URI resolve = URIUtils.resolve(normalize, "./");
        if (resolve == null) {
            return str;
        }
        return resolve.toString();
    }

    public static String a(String str, String str2) {
        String str3;
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            return str;
        }
        URI c = c(str);
        if (c == null) {
            return str;
        }
        URI normalize = c.normalize();
        URI c2 = c(str2);
        if (c2 == null) {
            return str;
        }
        URI normalize2 = c2.normalize();
        if (normalize.isOpaque() || normalize2.isOpaque()) {
            return str;
        }
        String scheme = normalize.getScheme();
        String scheme2 = normalize2.getScheme();
        if (scheme2 == null && scheme != null) {
            return str;
        }
        if (scheme2 != null && !scheme2.equals(scheme)) {
            return str;
        }
        String authority = normalize.getAuthority();
        String authority2 = normalize2.getAuthority();
        if (authority2 == null && authority != null) {
            return str;
        }
        if (authority2 != null && !authority2.equals(authority)) {
            return str;
        }
        String path = normalize.getPath();
        String path2 = normalize2.getPath();
        String[] split = a.split(path, -1);
        String[] split2 = a.split(path2, -1);
        int length = split.length;
        int length2 = split2.length;
        int i = 0;
        while (i < length2 && i < length && split[i].equals(split2[i])) {
            i++;
        }
        String query = normalize.getQuery();
        String fragment = normalize.getFragment();
        StringBuilder sb = new StringBuilder();
        if (i == length2 && i == length) {
            String query2 = normalize2.getQuery();
            String fragment2 = normalize2.getFragment();
            boolean equals = TextUtils.equals(query, query2);
            boolean equals2 = TextUtils.equals(fragment, fragment2);
            if (!equals || !equals2) {
                if (!equals || TextUtils.isEmpty(fragment)) {
                    str3 = query;
                } else {
                    str3 = null;
                }
                if (TextUtils.isEmpty(str3) && TextUtils.isEmpty(fragment)) {
                    sb.append(split[length - 1]);
                    str3 = query;
                }
            } else {
                fragment = null;
                str3 = null;
            }
        } else {
            int i2 = length - 1;
            int i3 = length2 - 1;
            for (int i4 = i; i4 < i3; i4++) {
                sb.append("..");
                sb.append("/");
            }
            int i5 = i;
            while (i5 < i2) {
                sb.append(split[i5]);
                sb.append("/");
                i5++;
            }
            if (i5 < length) {
                sb.append(split[i5]);
            }
            if (sb.length() == 0) {
                sb.append(".");
                sb.append("/");
            }
            str3 = query;
        }
        URI a2 = a(null, null, sb.toString(), str3, fragment);
        if (a2 == null) {
            return str;
        }
        return a2.toString();
    }

    private static URI c(String str) {
        try {
            return new URI(str);
        } catch (URISyntaxException e) {
            return null;
        }
    }

    private static URI a(String str, String str2, String str3, String str4, String str5) {
        try {
            return new URI(str, str2, str3, str4, str5);
        } catch (URISyntaxException e) {
            return null;
        }
    }
}
