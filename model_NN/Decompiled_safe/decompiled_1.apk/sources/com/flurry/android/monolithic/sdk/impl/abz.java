package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
public final class abz extends abt<char[]> {
    public abz() {
        super(char[].class);
    }

    public void a(char[] cArr, or orVar, ru ruVar) throws IOException, oq {
        if (ruVar.a(rr.WRITE_CHAR_ARRAYS_AS_JSON_ARRAYS)) {
            orVar.b();
            a(orVar, cArr);
            orVar.c();
            return;
        }
        orVar.a(cArr, 0, cArr.length);
    }

    public void a(char[] cArr, or orVar, ru ruVar, rx rxVar) throws IOException, oq {
        if (ruVar.a(rr.WRITE_CHAR_ARRAYS_AS_JSON_ARRAYS)) {
            rxVar.c(cArr, orVar);
            a(orVar, cArr);
            rxVar.f(cArr, orVar);
            return;
        }
        rxVar.a(cArr, orVar);
        orVar.a(cArr, 0, cArr.length);
        rxVar.d(cArr, orVar);
    }

    private final void a(or orVar, char[] cArr) throws IOException, oq {
        int length = cArr.length;
        for (int i = 0; i < length; i++) {
            orVar.a(cArr, i, 1);
        }
    }
}
