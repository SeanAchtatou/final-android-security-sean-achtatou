package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public class mj {
    protected final mk a;
    protected mq[] b = new mq[5];
    protected int c;

    public mj(mq mqVar, mk mkVar) throws IOException {
        this.a = mkVar;
        this.b[0] = mqVar;
        this.c = 1;
    }

    private void f() {
        mq[] mqVarArr = new mq[(this.b.length + Math.max(this.b.length, 1024))];
        System.arraycopy(this.b, 0, mqVarArr, 0, this.b.length);
        this.b = mqVarArr;
    }

    public final mq a(mq mqVar) throws IOException {
        while (true) {
            mq[] mqVarArr = this.b;
            int i = this.c - 1;
            this.c = i;
            mq mqVar2 = mqVarArr[i];
            if (mqVar2 == mqVar) {
                return mqVar2;
            }
            na naVar = mqVar2.a;
            if (naVar == na.IMPLICIT_ACTION) {
                mq a2 = this.a.a(mqVar, mqVar2);
                if (a2 != null) {
                    return a2;
                }
            } else if (naVar == na.TERMINAL) {
                throw new jh("Attempt to process a " + mqVar + " when a " + mqVar2 + " was expected.");
            } else if (naVar == na.REPEATER && mqVar == ((nb) mqVar2).z) {
                return mqVar;
            } else {
                b(mqVar2);
            }
        }
    }

    public final void a() throws IOException {
        while (this.c > 1) {
            mq mqVar = this.b[this.c - 1];
            if (mqVar.a == na.IMPLICIT_ACTION) {
                this.c--;
                this.a.a(null, mqVar);
            } else if (mqVar.a != na.TERMINAL) {
                this.c--;
                b(mqVar);
            } else {
                return;
            }
        }
    }

    public final void b() throws IOException {
        while (this.c >= 1) {
            mq mqVar = this.b[this.c - 1];
            if (mqVar.a == na.IMPLICIT_ACTION && ((my) mqVar).A) {
                this.c--;
                this.a.a(null, mqVar);
            } else {
                return;
            }
        }
    }

    public final void b(mq mqVar) {
        mq[] mqVarArr = mqVar.b;
        while (this.c + mqVarArr.length > this.b.length) {
            f();
        }
        System.arraycopy(mqVarArr, 0, this.b, this.c, mqVarArr.length);
        this.c = mqVarArr.length + this.c;
    }

    public mq c() {
        mq[] mqVarArr = this.b;
        int i = this.c - 1;
        this.c = i;
        return mqVarArr[i];
    }

    public mq d() {
        return this.b[this.c - 1];
    }

    public void c(mq mqVar) {
        if (this.c == this.b.length) {
            f();
        }
        mq[] mqVarArr = this.b;
        int i = this.c;
        this.c = i + 1;
        mqVarArr[i] = mqVar;
    }

    public void e() {
        this.c = 1;
    }
}
