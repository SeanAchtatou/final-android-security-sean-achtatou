package com.flurry.android.monolithic.sdk.impl;

import java.lang.reflect.Member;

public abstract class xk extends xg {
    protected final xp b;

    public abstract void a(Object obj, Object obj2) throws UnsupportedOperationException, IllegalArgumentException;

    public abstract Class<?> h();

    public abstract Member i();

    protected xk(xp xpVar) {
        this.b = xpVar;
    }

    /* access modifiers changed from: protected */
    public xp j() {
        return this.b;
    }

    public final void k() {
        adz.a(i());
    }
}
