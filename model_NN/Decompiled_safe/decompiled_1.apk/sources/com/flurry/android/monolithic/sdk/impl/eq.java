package com.flurry.android.monolithic.sdk.impl;

class eq implements ex {
    final /* synthetic */ em a;

    eq(em emVar) {
        this.a = emVar;
    }

    public void a(int i, String str, String str2, String str3) {
        ja.a(5, em.f, "--onReport " + str2 + " sent. HTTP response: " + i + " : " + str);
        if (ja.c() <= 3) {
            ia.a().a(new er(this, i));
        }
        this.a.a(str2, str3, i);
        this.a.g();
    }

    public void a(String str, String str2) {
        ja.a(5, em.f, "--onServerError " + str);
        this.a.b(str, str2);
    }
}
