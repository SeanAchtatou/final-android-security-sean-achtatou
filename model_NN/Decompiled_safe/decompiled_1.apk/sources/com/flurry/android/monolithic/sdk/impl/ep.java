package com.flurry.android.monolithic.sdk.impl;

class ep implements Runnable {
    final /* synthetic */ byte[] a;
    final /* synthetic */ String b;
    final /* synthetic */ String c;
    final /* synthetic */ em d;

    ep(em emVar, byte[] bArr, String str, String str2) {
        this.d = emVar;
        this.a = bArr;
        this.b = str;
        this.c = str2;
    }

    public void run() {
        this.d.i();
        try {
            this.d.b(this.a, this.b, this.c);
        } catch (Exception e) {
            ja.a(6, em.f, "storeData error", e);
        }
    }
}
