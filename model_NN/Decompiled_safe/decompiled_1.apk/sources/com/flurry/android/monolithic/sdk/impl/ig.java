package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import android.os.Build;

public final class ig implements ih {
    private static ig a;
    private final ih b = b();

    public static synchronized ig a() {
        ig igVar;
        synchronized (ig.class) {
            if (a == null) {
                a = new ig();
            }
            igVar = a;
        }
        return igVar;
    }

    public void f(Context context) {
        if (this.b != null) {
            this.b.f(context);
        }
    }

    public void g(Context context) {
        if (this.b != null) {
            this.b.g(context);
        }
    }

    private static ih b() {
        if (c()) {
            return Cif.a();
        }
        return null;
    }

    private static boolean c() {
        return Build.VERSION.SDK_INT >= 8;
    }
}
