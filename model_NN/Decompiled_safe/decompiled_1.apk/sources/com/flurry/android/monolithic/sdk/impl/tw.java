package com.flurry.android.monolithic.sdk.impl;

public final class tw {
    final ow a;
    final qm b;
    final Object[] c;
    private int d;
    private ts e;

    public tw(ow owVar, qm qmVar, int i) {
        this.a = owVar;
        this.b = qmVar;
        this.d = i;
        this.c = new Object[i];
    }

    public void a(sw[] swVarArr) {
        int length = swVarArr.length;
        for (int i = 0; i < length; i++) {
            sw swVar = swVarArr[i];
            if (swVar != null) {
                this.c[i] = this.b.a(swVar.k(), swVar, (Object) null);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final Object[] a(Object[] objArr) {
        Object obj;
        if (objArr != null) {
            int length = this.c.length;
            for (int i = 0; i < length; i++) {
                if (this.c[i] == null && (obj = objArr[i]) != null) {
                    this.c[i] = obj;
                }
            }
        }
        return this.c;
    }

    /* access modifiers changed from: protected */
    public ts a() {
        return this.e;
    }

    public boolean a(int i, Object obj) {
        this.c[i] = obj;
        int i2 = this.d - 1;
        this.d = i2;
        return i2 <= 0;
    }

    public void a(sw swVar, Object obj) {
        this.e = new tv(this.e, obj, swVar);
    }

    public void a(sv svVar, String str, Object obj) {
        this.e = new tt(this.e, obj, svVar, str);
    }

    public void a(Object obj, Object obj2) {
        this.e = new tu(this.e, obj2, obj);
    }
}
