package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
final class vk extends ve<int[]> {
    public vk() {
        super(int[].class);
    }

    /* renamed from: b */
    public int[] a(ow owVar, qm qmVar) throws IOException, oz {
        if (!owVar.j()) {
            return c(owVar, qmVar);
        }
        adv d = qmVar.h().d();
        int[] iArr = (int[]) d.a();
        int i = 0;
        while (owVar.b() != pb.END_ARRAY) {
            int t = t(owVar, qmVar);
            if (i >= iArr.length) {
                iArr = (int[]) d.a(iArr, i);
                i = 0;
            }
            iArr[i] = t;
            i++;
        }
        return (int[]) d.b(iArr, i);
    }

    private final int[] c(ow owVar, qm qmVar) throws IOException, oz {
        if (owVar.e() == pb.VALUE_STRING && qmVar.a(ql.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT) && owVar.k().length() == 0) {
            return null;
        }
        if (!qmVar.a(ql.ACCEPT_SINGLE_VALUE_AS_ARRAY)) {
            throw qmVar.b(this.q);
        }
        return new int[]{t(owVar, qmVar)};
    }
}
