package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.ArrayList;

public class tx {
    protected final ArrayList<sw> a = new ArrayList<>();

    public void a(sw swVar) {
        this.a.add(swVar);
    }

    public Object a(ow owVar, qm qmVar, Object obj, afz afz) throws IOException, oz {
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            ow h = afz.h();
            h.b();
            this.a.get(i).a(h, qmVar, obj);
        }
        return obj;
    }
}
