package com.flurry.android.monolithic.sdk.impl;

import java.util.Map;

public class aaj {
    private final aak[] a;
    private final int b;

    public aaj(Map<aau, ra<Object>> map) {
        int a2 = a(map.size());
        this.b = a2;
        int i = a2 - 1;
        aak[] aakArr = new aak[a2];
        for (Map.Entry next : map.entrySet()) {
            aau aau = (aau) next.getKey();
            int hashCode = aau.hashCode() & i;
            aakArr[hashCode] = new aak(aakArr[hashCode], aau, (ra) next.getValue());
        }
        this.a = aakArr;
    }

    private static final int a(int i) {
        int i2 = 8;
        while (i2 < (i <= 64 ? i + i : (i >> 2) + i)) {
            i2 += i2;
        }
        return i2;
    }

    public ra<Object> a(aau aau) {
        aak aak = this.a[aau.hashCode() & (this.a.length - 1)];
        if (aak == null) {
            return null;
        }
        if (aau.equals(aak.a)) {
            return aak.b;
        }
        do {
            aak = aak.c;
            if (aak == null) {
                return null;
            }
        } while (!aau.equals(aak.a));
        return aak.b;
    }
}
