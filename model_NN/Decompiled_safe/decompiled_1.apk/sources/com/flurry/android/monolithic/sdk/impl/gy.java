package com.flurry.android.monolithic.sdk.impl;

import com.flurry.android.FlurryFullscreenTakeoverActivity;
import com.flurry.android.impl.appcloud.AppCloudModule;
import java.util.HashMap;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.json.JSONTokener;

public class gy extends he {
    protected static final String a = gy.class.getSimpleName();

    public gy(HashMap<String, Object> hashMap) {
        super(hashMap);
    }

    public void a() {
        String str;
        JSONObject jSONObject;
        b("GetOperation Thread");
        try {
            this.f = (fr) this.h.get("del");
            this.g = (fs) this.h.get("del_internal");
            String a2 = a((String) this.h.get(FlurryFullscreenTakeoverActivity.EXTRA_KEY_URL));
            HttpGet httpGet = new HttpGet(a2);
            ja.a(4, a, "Get operation URL = " + a2);
            if (AppCloudModule.c()) {
                this.d = iz.a(new BasicHttpParams());
                this.e = new BasicHttpContext();
                this.e.setAttribute("http.cookie-store", new BasicCookieStore());
                String str2 = (String) this.d.execute(httpGet, new BasicResponseHandler(), this.e);
                JSONTokener jSONTokener = new JSONTokener(str2);
                jSONTokener.skipPast("(");
                JSONObject jSONObject2 = (JSONObject) jSONTokener.nextValue();
                str = str2;
                jSONObject = jSONObject2;
            } else {
                this.d = iz.b(new BasicHttpParams());
                a(httpGet, this.h);
                String str3 = EntityUtils.toString(this.d.execute(this.c, httpGet, new BasicHttpContext()).getEntity()).toString();
                str = str3;
                jSONObject = new JSONObject(str3);
            }
            ja.a(4, a, "responseText = " + str);
            if (jSONObject.getInt("code") != 400 && jSONObject.has("APPCLOUD_USER_SESSION")) {
                gr.a = jSONObject.getString("APPCLOUD_USER_SESSION");
            }
            a(jSONObject);
            if (this.d != null) {
                this.d.getConnectionManager().shutdown();
            }
        } catch (Exception e) {
            ja.a(6, a, "Exception during communication with server: ", e);
            a((JSONObject) null);
            if (this.d != null) {
                this.d.getConnectionManager().shutdown();
            }
        } catch (Throwable th) {
            if (this.d != null) {
                this.d.getConnectionManager().shutdown();
            }
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public void a(JSONObject jSONObject) {
        try {
            fq fqVar = new fq(jSONObject);
            if (this.g != null) {
                this.g.a(fqVar, this.f, gx.METHOD_GET, this.h);
            } else {
                this.f.a(fqVar);
            }
        } catch (Exception e) {
            ja.a(6, a, "Exception in onPostExecute: ", e);
        }
    }
}
