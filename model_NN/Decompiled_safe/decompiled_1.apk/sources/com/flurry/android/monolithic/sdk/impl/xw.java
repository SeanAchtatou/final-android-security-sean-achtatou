package com.flurry.android.monolithic.sdk.impl;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

@Deprecated
public class xw implements xz {
    public boolean a(Method method) {
        if (Modifier.isStatic(method.getModifiers())) {
            return false;
        }
        switch (method.getParameterTypes().length) {
            case 1:
                return true;
            case 2:
                return true;
            default:
                return false;
        }
    }
}
