package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

public final class sz extends sw {
    protected final String i;
    protected final boolean j;
    protected final sw k;
    protected final sw l;

    public sz(String str, sw swVar, sw swVar2, ado ado, boolean z) {
        super(swVar.c(), swVar.a(), swVar.e, ado);
        this.i = str;
        this.k = swVar;
        this.l = swVar2;
        this.j = z;
    }

    protected sz(sz szVar, qu<Object> quVar) {
        super(szVar, quVar);
        this.i = szVar.i;
        this.j = szVar.j;
        this.k = szVar.k;
        this.l = szVar.l;
    }

    /* renamed from: b */
    public sz a(qu<Object> quVar) {
        return new sz(this, quVar);
    }

    public xk b() {
        return this.k.b();
    }

    public void a(ow owVar, qm qmVar, Object obj) throws IOException, oz {
        a(obj, this.k.a(owVar, qmVar));
    }

    public final void a(Object obj, Object obj2) throws IOException {
        this.k.a(obj, obj2);
        if (obj2 == null) {
            return;
        }
        if (!this.j) {
            this.l.a(obj2, obj);
        } else if (obj2 instanceof Object[]) {
            for (Object obj3 : (Object[]) obj2) {
                if (obj3 != null) {
                    this.l.a(obj3, obj);
                }
            }
        } else if (obj2 instanceof Collection) {
            for (Object next : (Collection) obj2) {
                if (next != null) {
                    this.l.a(next, obj);
                }
            }
        } else if (obj2 instanceof Map) {
            for (Object next2 : ((Map) obj2).values()) {
                if (next2 != null) {
                    this.l.a(next2, obj);
                }
            }
        } else {
            throw new IllegalStateException("Unsupported container type (" + obj2.getClass().getName() + ") when resolving reference '" + this.i + "'");
        }
    }
}
