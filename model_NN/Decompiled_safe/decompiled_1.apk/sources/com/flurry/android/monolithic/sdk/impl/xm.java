package com.flurry.android.monolithic.sdk.impl;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;

public final class xm implements Iterable<xl> {
    protected LinkedHashMap<xy, xl> a;

    public void a(xl xlVar) {
        if (this.a == null) {
            this.a = new LinkedHashMap<>();
        }
        this.a.put(new xy(xlVar.a()), xlVar);
    }

    public xl a(Method method) {
        if (this.a != null) {
            return this.a.remove(new xy(method));
        }
        return null;
    }

    public boolean a() {
        return this.a == null || this.a.size() == 0;
    }

    public xl a(String str, Class<?>[] clsArr) {
        if (this.a == null) {
            return null;
        }
        return this.a.get(new xy(str, clsArr));
    }

    public xl b(Method method) {
        if (this.a == null) {
            return null;
        }
        return this.a.get(new xy(method));
    }

    public Iterator<xl> iterator() {
        if (this.a != null) {
            return this.a.values().iterator();
        }
        return Collections.emptyList().iterator();
    }
}
