package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import v2.com.playhaven.model.PHContent;

public final class afe extends afg {
    protected final Object c;

    public afe(Object obj) {
        this.c = obj;
    }

    public String m() {
        return this.c == null ? PHContent.PARCEL_NULL : this.c.toString();
    }

    public double a(double d) {
        if (this.c instanceof Number) {
            return ((Number) this.c).doubleValue();
        }
        return d;
    }

    public final void a(or orVar, ru ruVar) throws IOException, oz {
        if (this.c == null) {
            orVar.f();
        } else {
            orVar.a(this.c);
        }
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        afe afe = (afe) obj;
        if (this.c == null) {
            return afe.c == null;
        }
        return this.c.equals(afe.c);
    }

    public int hashCode() {
        return this.c.hashCode();
    }

    public String toString() {
        return String.valueOf(this.c);
    }
}
