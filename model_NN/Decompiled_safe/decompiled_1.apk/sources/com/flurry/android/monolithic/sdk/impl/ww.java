package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public class ww extends th {
    protected final String a;
    protected final boolean b;
    protected xo c;
    protected tn[] d;
    protected xo e;
    protected afm f;
    protected xo g;
    protected xo h;
    protected xo i;
    protected xo j;
    protected xo k;
    protected xo l;

    public ww(qk qkVar, afm afm) {
        this.b = qkVar == null ? false : qkVar.a(ql.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
        this.a = afm == null ? "UNKNOWN TYPE" : afm.toString();
    }

    public void a(xo xoVar, xo xoVar2, afm afm, xo xoVar3, tn[] tnVarArr) {
        this.c = xoVar;
        this.g = xoVar2;
        this.f = afm;
        this.e = xoVar3;
        this.d = tnVarArr;
    }

    public void a(xo xoVar) {
        this.h = xoVar;
    }

    public void b(xo xoVar) {
        this.i = xoVar;
    }

    public void c(xo xoVar) {
        this.j = xoVar;
    }

    public void d(xo xoVar) {
        this.k = xoVar;
    }

    public void e(xo xoVar) {
        this.l = xoVar;
    }

    public String a() {
        return this.a;
    }

    public boolean c() {
        return this.h != null;
    }

    public boolean d() {
        return this.i != null;
    }

    public boolean e() {
        return this.j != null;
    }

    public boolean f() {
        return this.k != null;
    }

    public boolean g() {
        return this.l != null;
    }

    public boolean h() {
        return this.c != null;
    }

    public boolean j() {
        return this.e != null;
    }

    public afm l() {
        return this.f;
    }

    public sw[] k() {
        return this.d;
    }

    public Object m() throws IOException, oz {
        if (this.c == null) {
            throw new IllegalStateException("No default constructor for " + a());
        }
        try {
            return this.c.g();
        } catch (ExceptionInInitializerError e2) {
            throw a((Throwable) e2);
        } catch (Exception e3) {
            throw a((Throwable) e3);
        }
    }

    public Object a(Object[] objArr) throws IOException, oz {
        if (this.e == null) {
            throw new IllegalStateException("No with-args constructor for " + a());
        }
        try {
            return this.e.a(objArr);
        } catch (ExceptionInInitializerError e2) {
            throw a((Throwable) e2);
        } catch (Exception e3) {
            throw a((Throwable) e3);
        }
    }

    public Object a(Object obj) throws IOException, oz {
        if (this.g == null) {
            throw new IllegalStateException("No delegate constructor for " + a());
        }
        try {
            return this.g.a(obj);
        } catch (ExceptionInInitializerError e2) {
            throw a((Throwable) e2);
        } catch (Exception e3) {
            throw a((Throwable) e3);
        }
    }

    public Object a(String str) throws IOException, oz {
        if (this.h == null) {
            return b(str);
        }
        try {
            return this.h.a(str);
        } catch (Exception e2) {
            throw a((Throwable) e2);
        }
    }

    public Object a(int i2) throws IOException, oz {
        try {
            if (this.i != null) {
                return this.i.a(Integer.valueOf(i2));
            }
            if (this.j != null) {
                return this.j.a(Long.valueOf((long) i2));
            }
            throw new qw("Can not instantiate value of type " + a() + " from JSON integral number; no single-int-arg constructor/factory method");
        } catch (Exception e2) {
            throw a((Throwable) e2);
        }
    }

    public Object a(long j2) throws IOException, oz {
        try {
            if (this.j != null) {
                return this.j.a(Long.valueOf(j2));
            }
            throw new qw("Can not instantiate value of type " + a() + " from JSON long integral number; no single-long-arg constructor/factory method");
        } catch (Exception e2) {
            throw a((Throwable) e2);
        }
    }

    public Object a(double d2) throws IOException, oz {
        try {
            if (this.k != null) {
                return this.k.a(Double.valueOf(d2));
            }
            throw new qw("Can not instantiate value of type " + a() + " from JSON floating-point number; no one-double/Double-arg constructor/factory method");
        } catch (Exception e2) {
            throw a((Throwable) e2);
        }
    }

    public Object a(boolean z) throws IOException, oz {
        try {
            if (this.l != null) {
                return this.l.a(Boolean.valueOf(z));
            }
            throw new qw("Can not instantiate value of type " + a() + " from JSON boolean value; no single-boolean/Boolean-arg constructor/factory method");
        } catch (Exception e2) {
            throw a((Throwable) e2);
        }
    }

    public xo o() {
        return this.g;
    }

    public xo n() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public Object b(String str) throws IOException, oz {
        if (this.l != null) {
            String trim = str.trim();
            if ("true".equals(trim)) {
                return a(true);
            }
            if ("false".equals(trim)) {
                return a(false);
            }
        }
        if (this.b && str.length() == 0) {
            return null;
        }
        throw new qw("Can not instantiate value of type " + a() + " from JSON String; no single-String constructor/factory method");
    }

    /* access modifiers changed from: protected */
    public qw a(Throwable th) {
        Throwable th2 = th;
        while (th2.getCause() != null) {
            th2 = th2.getCause();
        }
        return new qw("Instantiation of " + a() + " value failed: " + th2.getMessage(), th2);
    }
}
