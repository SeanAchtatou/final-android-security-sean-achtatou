package com.flurry.android.monolithic.sdk.impl;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.HashMap;

public class zf implements qc {
    protected final xk a;
    protected final ado b;
    protected final afm c;
    protected final Method d;
    protected final Field e;
    protected HashMap<Object, Object> f;
    protected final pw g;
    protected final afm h;
    protected final ra<Object> i;
    protected aal j;
    protected final boolean k;
    protected final Object l;
    protected Class<?>[] m;
    protected rx n;
    protected afm o;

    public zf(xk xkVar, ado ado, String str, afm afm, ra<Object> raVar, rx rxVar, afm afm2, Method method, Field field, boolean z, Object obj) {
        this(xkVar, ado, new pw(str), afm, raVar, rxVar, afm2, method, field, z, obj);
    }

    public zf(xk xkVar, ado ado, pw pwVar, afm afm, ra<Object> raVar, rx rxVar, afm afm2, Method method, Field field, boolean z, Object obj) {
        this.a = xkVar;
        this.b = ado;
        this.g = pwVar;
        this.c = afm;
        this.i = raVar;
        this.j = raVar == null ? aal.a() : null;
        this.n = rxVar;
        this.h = afm2;
        this.d = method;
        this.e = field;
        this.k = z;
        this.l = obj;
    }

    protected zf(zf zfVar) {
        this(zfVar, zfVar.i);
    }

    protected zf(zf zfVar, ra<Object> raVar) {
        this.i = raVar;
        this.a = zfVar.a;
        this.b = zfVar.b;
        this.c = zfVar.c;
        this.d = zfVar.d;
        this.e = zfVar.e;
        if (zfVar.f != null) {
            this.f = new HashMap<>(zfVar.f);
        }
        this.g = zfVar.g;
        this.h = zfVar.h;
        this.j = zfVar.j;
        this.k = zfVar.k;
        this.l = zfVar.l;
        this.m = zfVar.m;
        this.n = zfVar.n;
        this.o = zfVar.o;
    }

    public zf a(ra<Object> raVar) {
        if (getClass() == zf.class) {
            return new zf(this, raVar);
        }
        throw new IllegalStateException("BeanPropertyWriter sub-class does not override 'withSerializer()'; needs to!");
    }

    public zf c() {
        return new aaw(this);
    }

    public void a(Class<?>[] clsArr) {
        this.m = clsArr;
    }

    public void a(afm afm) {
        this.o = afm;
    }

    public String d() {
        return this.g.a();
    }

    public afm a() {
        return this.c;
    }

    public xk b() {
        return this.a;
    }

    public boolean e() {
        return this.i != null;
    }

    public afm f() {
        return this.h;
    }

    public Type g() {
        if (this.d != null) {
            return this.d.getGenericReturnType();
        }
        return this.e.getGenericType();
    }

    public Class<?>[] h() {
        return this.m;
    }

    public void a(Object obj, or orVar, ru ruVar) throws Exception {
        Object a2 = a(obj);
        if (a2 != null) {
            if (a2 == obj) {
                b(obj);
            }
            if (this.l == null || !this.l.equals(a2)) {
                ra<Object> raVar = this.i;
                if (raVar == null) {
                    Class<?> cls = a2.getClass();
                    aal aal = this.j;
                    ra<Object> a3 = aal.a(cls);
                    if (a3 == null) {
                        raVar = a(aal, cls, ruVar);
                    } else {
                        raVar = a3;
                    }
                }
                orVar.a(this.g);
                if (this.n == null) {
                    raVar.a(a2, orVar, ruVar);
                } else {
                    raVar.a(a2, orVar, ruVar, this.n);
                }
            }
        } else if (!this.k) {
            orVar.a(this.g);
            ruVar.a(orVar);
        }
    }

    /* access modifiers changed from: protected */
    public ra<Object> a(aal aal, Class<?> cls, ru ruVar) throws qw {
        aap a2;
        if (this.o != null) {
            a2 = aal.a(ruVar.a(this.o, cls), ruVar, this);
        } else {
            a2 = aal.a(cls, ruVar, this);
        }
        if (aal != a2.b) {
            this.j = a2.b;
        }
        return a2.a;
    }

    public final Object a(Object obj) throws Exception {
        if (this.d != null) {
            return this.d.invoke(obj, new Object[0]);
        }
        return this.e.get(obj);
    }

    /* access modifiers changed from: protected */
    public void b(Object obj) throws qw {
        throw new qw("Direct self-reference leading to cycle");
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(40);
        sb.append("property '").append(d()).append("' (");
        if (this.d != null) {
            sb.append("via method ").append(this.d.getDeclaringClass().getName()).append("#").append(this.d.getName());
        } else {
            sb.append("field \"").append(this.e.getDeclaringClass().getName()).append("#").append(this.e.getName());
        }
        if (this.i == null) {
            sb.append(", no static serializer");
        } else {
            sb.append(", static serializer of type " + this.i.getClass().getName());
        }
        sb.append(')');
        return sb.toString();
    }
}
