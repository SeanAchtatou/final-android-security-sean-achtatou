package com.flurry.android.monolithic.sdk.impl;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import com.flurry.android.FlurryAgent;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import v2.com.playhaven.model.PHContent;

public class eg implements ff, id, jb, Thread.UncaughtExceptionHandler {
    private static final String a = eg.class.getSimpleName();
    private static eg b;
    private String c = "";
    private boolean d;
    private fb e;
    private Map<String, fb> f = new HashMap();
    private em g;

    private eg() {
        jc.a().a(this);
        is.a().a(this);
        l();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, com.flurry.android.monolithic.sdk.impl.id):void
     arg types: [java.lang.String, com.flurry.android.monolithic.sdk.impl.eg]
     candidates:
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.Object, java.lang.Object):boolean
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, com.flurry.android.monolithic.sdk.impl.id):void */
    private void l() {
        ic a2 = ib.a();
        this.d = ((Boolean) a2.a("CaptureUncaughtExceptions")).booleanValue();
        a2.a("CaptureUncaughtExceptions", (id) this);
        ja.a(4, a, "initSettings, CrashReportingEnabled = " + this.d);
        String str = (String) a2.a("VesionName");
        a2.a("VesionName", (id) this);
        ir.a(str);
        ja.a(4, a, "initSettings, VersionName = " + str);
    }

    public void a(String str, Object obj) {
        if (str.equals("CaptureUncaughtExceptions")) {
            this.d = ((Boolean) obj).booleanValue();
            ja.a(4, a, "onSettingUpdate, CrashReportingEnabled = " + this.d);
        } else if (str.equals("VesionName")) {
            String str2 = (String) obj;
            ir.a(str2);
            ja.a(4, a, "onSettingUpdate, VersionName = " + str2);
        } else {
            ja.a(6, a, "onSettingUpdate internal error!");
        }
    }

    public static eg a() {
        if (b == null) {
            b = new eg();
        }
        return b;
    }

    public int b() {
        int intValue = ((Integer) ib.a().a("AgentVersion")).intValue();
        ja.a(4, a, "getAgentVersion() = " + intValue);
        return intValue;
    }

    public String c() {
        String str;
        if (this.c.length() > 0) {
            str = ".";
        } else {
            str = "";
        }
        return String.format(Locale.getDefault(), "Flurry_Android_%d_%d.%d.%d%s%s", Integer.valueOf(b()), 3, 2, 2, str, this.c);
    }

    public void a(Context context, String str) {
        fb fbVar;
        jc.a().b();
        in.a().b();
        m();
        if (this.f.isEmpty()) {
            in.a().c();
        }
        if (this.f.containsKey(str)) {
            fbVar = this.f.get(str);
        } else {
            fbVar = new fb(context, str, this);
            fbVar.a(b(context));
            this.f.put(str, fbVar);
        }
        fbVar.c();
        a(fbVar);
    }

    public void a(Context context) {
        fb d2 = d();
        if (d2 != null) {
            d2.d();
        }
    }

    private void m() {
        if (this.g == null) {
            this.g = new em();
        }
    }

    private Map<String, List<String>> b(Context context) {
        Bundle extras;
        HashMap hashMap = null;
        if ((context instanceof Activity) && (extras = ((Activity) context).getIntent().getExtras()) != null) {
            ja.a(3, a, "Launch Options Bundle is present " + extras.toString());
            hashMap = new HashMap();
            for (String next : extras.keySet()) {
                if (next != null) {
                    Object obj = extras.get(next);
                    String obj2 = obj != null ? obj.toString() : PHContent.PARCEL_NULL;
                    hashMap.put(next, new ArrayList(Arrays.asList(obj2)));
                    ja.a(3, a, "Launch options Key: " + next + ". Its value: " + obj2);
                }
            }
        }
        return hashMap;
    }

    private void a(fb fbVar) {
        this.e = fbVar;
    }

    public fb d() {
        return this.e;
    }

    public void a(String str) {
        fb d2 = d();
        if (d2 != null) {
            d2.a(str, null, false);
        }
    }

    public void a(String str, Map<String, String> map) {
        fb d2 = d();
        if (d2 != null) {
            d2.a(str, map, false);
        }
    }

    public void a(String str, boolean z) {
        fb d2 = d();
        if (d2 != null) {
            d2.a(str, null, z);
        }
    }

    public void a(String str, Map<String, String> map, boolean z) {
        fb d2 = d();
        if (d2 != null) {
            d2.a(str, map, z);
        }
    }

    public void b(String str) {
        fb d2 = d();
        if (d2 != null) {
            d2.a(str, (Map<String, String>) null);
        }
    }

    public void b(String str, Map<String, String> map) {
        fb d2 = d();
        if (d2 != null) {
            d2.a(str, map);
        }
    }

    @Deprecated
    public void a(String str, String str2, String str3) {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        if (stackTrace != null && stackTrace.length > 2) {
            StackTraceElement[] stackTraceElementArr = new StackTraceElement[(stackTrace.length - 2)];
            System.arraycopy(stackTrace, 2, stackTraceElementArr, 0, stackTraceElementArr.length);
            stackTrace = stackTraceElementArr;
        }
        Throwable th = new Throwable(str2);
        th.setStackTrace(stackTrace);
        fb d2 = d();
        if (d2 != null) {
            d2.a(str, str2, str3, th);
        }
    }

    public void a(String str, String str2, Throwable th) {
        fb d2 = d();
        if (d2 != null) {
            d2.a(str, str2, th.getClass().getName(), th);
        }
    }

    public void c(String str) {
        fb d2 = d();
        if (d2 != null) {
            d2.a(str, null, false);
        }
    }

    public void c(String str, Map<String, String> map) {
        fb d2 = d();
        if (d2 != null) {
            d2.a(str, map, false);
        }
    }

    public void e() {
        fb d2 = d();
        if (d2 != null) {
            d2.f();
        }
    }

    public void uncaughtException(Thread thread, Throwable th) {
        th.printStackTrace();
        if (this.d) {
            String str = "";
            StackTraceElement[] stackTrace = th.getStackTrace();
            if (stackTrace != null && stackTrace.length > 0) {
                StringBuilder sb = new StringBuilder();
                if (th.getMessage() != null) {
                    sb.append(" (" + th.getMessage() + ")\n");
                }
                str = sb.toString();
            } else if (th.getMessage() != null) {
                str = th.getMessage();
            }
            FlurryAgent.onError("uncaught", str, th);
        }
        for (fb fbVar : new HashMap(this.f).values()) {
            if (fbVar != null) {
                fbVar.e();
            }
        }
        in.a().f();
    }

    public String f() {
        fb d2 = d();
        if (d2 != null) {
            return d2.i();
        }
        return null;
    }

    public String g() {
        fb d2 = d();
        if (d2 != null) {
            return d2.j();
        }
        return null;
    }

    public String h() {
        fb d2 = d();
        if (d2 != null) {
            return d2.k();
        }
        return null;
    }

    public static int i() {
        return 0;
    }

    public Location j() {
        return in.a().e();
    }

    public em k() {
        return this.g;
    }

    public void b(boolean z) {
    }

    public void d(String str) {
        if (!this.f.containsKey(str)) {
            ja.a(6, a, "Ended session is not in the session map! Maybe it was already destroyed.");
        } else {
            fb d2 = d();
            if (d2 != null && TextUtils.equals(d2.i(), str)) {
                a((fb) null);
            }
            this.f.remove(str);
        }
        if (this.f.isEmpty()) {
            ja.a(5, a, "LocationProvider is going to be unsubscribed");
            in.a().d();
        }
    }
}
