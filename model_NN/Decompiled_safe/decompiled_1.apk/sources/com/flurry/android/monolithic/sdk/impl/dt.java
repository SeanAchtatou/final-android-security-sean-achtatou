package com.flurry.android.monolithic.sdk.impl;

import com.jumptap.adtag.JtAdView;
import com.jumptap.adtag.JtAdViewListener;
import java.util.Collections;

class dt implements JtAdViewListener {
    final /* synthetic */ ds a;

    dt(ds dsVar) {
        this.a = dsVar;
    }

    public void onNoAdFound(JtAdView jtAdView, int i) {
        this.a.onRenderFailed(Collections.emptyMap());
        ja.a(3, ds.a, "Jumptap AdView no ad found.");
    }

    public void onNewAd(JtAdView jtAdView, int i, String str) {
        this.a.onAdShown(Collections.emptyMap());
        ja.a(3, ds.a, "Jumptap AdView new ad.");
    }

    public void onInterstitialDismissed(JtAdView jtAdView, int i) {
        this.a.onAdClosed(Collections.emptyMap());
        ja.a(3, ds.a, "Jumptap AdView dismissed.");
    }

    public void onFocusChange(JtAdView jtAdView, int i, boolean z) {
        ja.a(3, ds.a, "Jumptap AdView focus changed.");
    }

    public void onAdError(JtAdView jtAdView, int i, int i2) {
        ja.a(3, ds.a, "Jumptap AdView error.");
        this.a.onRenderFailed(Collections.emptyMap());
    }
}
