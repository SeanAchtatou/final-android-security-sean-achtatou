package com.flurry.android.monolithic.sdk.impl;

import com.flurry.org.codehaus.jackson.annotate.JsonAutoDetect;
import java.util.HashMap;

public class qk extends ri<ql, qk> {
    protected aeg<qn> a;
    protected final aez b;
    protected boolean c;

    public qk(qf<? extends qb> qfVar, py pyVar, ye<?> yeVar, yh yhVar, rl rlVar, adk adk, qs qsVar) {
        super(qfVar, pyVar, yeVar, yhVar, rlVar, adk, qsVar, d(ql.class));
        this.b = aez.a;
    }

    private qk(qk qkVar, HashMap<adb, Class<?>> hashMap, yh yhVar) {
        this(qkVar, qkVar.e);
        this.f = hashMap;
        this.h = yhVar;
    }

    protected qk(qk qkVar, rg rgVar) {
        super(qkVar, rgVar, qkVar.h);
        this.a = qkVar.a;
        this.b = qkVar.b;
        this.c = qkVar.c;
    }

    /* access modifiers changed from: protected */
    public qk a(int i) {
        this.c = (rr.SORT_PROPERTIES_ALPHABETICALLY.b() & i) != 0;
        return this;
    }

    public qk a(yh yhVar) {
        HashMap hashMap = this.f;
        this.g = true;
        return new qk(this, hashMap, yhVar);
    }

    public py a() {
        if (a(ql.USE_ANNOTATIONS)) {
            return super.a();
        }
        return ya.a;
    }

    public <T extends qb> T a(afm afm) {
        return i().a((rf<?>) this, afm, this);
    }

    public boolean b() {
        return a(ql.USE_ANNOTATIONS);
    }

    public boolean c() {
        return a(ql.CAN_OVERRIDE_ACCESS_MODIFIERS);
    }

    public boolean d() {
        return this.c;
    }

    public ye<?> e() {
        ye e = super.e();
        if (!a(ql.AUTO_DETECT_SETTERS)) {
            e = e.c(JsonAutoDetect.Visibility.NONE);
        }
        if (!a(ql.AUTO_DETECT_CREATORS)) {
            e = e.d(JsonAutoDetect.Visibility.NONE);
        }
        if (!a(ql.AUTO_DETECT_FIELDS)) {
            return e.e(JsonAutoDetect.Visibility.NONE);
        }
        return e;
    }

    public boolean a(ql qlVar) {
        return (this.i & qlVar.b()) != 0;
    }

    public aeg<qn> f() {
        return this.a;
    }

    public on g() {
        return oo.a();
    }

    public final aez h() {
        return this.b;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.qf.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.afm, com.flurry.android.monolithic.sdk.impl.qg):T
     arg types: [com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.afm, com.flurry.android.monolithic.sdk.impl.qk]
     candidates:
      com.flurry.android.monolithic.sdk.impl.qf.a(com.flurry.android.monolithic.sdk.impl.rf<?>, com.flurry.android.monolithic.sdk.impl.afm, com.flurry.android.monolithic.sdk.impl.qg):T
      com.flurry.android.monolithic.sdk.impl.qf.a(com.flurry.android.monolithic.sdk.impl.rq, com.flurry.android.monolithic.sdk.impl.afm, com.flurry.android.monolithic.sdk.impl.qg):T
      com.flurry.android.monolithic.sdk.impl.qf.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.afm, com.flurry.android.monolithic.sdk.impl.qg):T */
    public <T extends qb> T b(afm afm) {
        return i().a(this, afm, (qg) this);
    }

    public <T extends qb> T c(afm afm) {
        return i().b(this, afm, this);
    }

    public qu<Object> a(xg xgVar, Class<? extends qu<?>> cls) {
        qu<?> a2;
        qs k = k();
        return (k == null || (a2 = k.a(this, xgVar, cls)) == null) ? (qu) adz.b(cls, c()) : a2;
    }

    public rc b(xg xgVar, Class<? extends rc> cls) {
        rc b2;
        qs k = k();
        return (k == null || (b2 = k.b(this, xgVar, cls)) == null) ? (rc) adz.b(cls, c()) : b2;
    }

    public th c(xg xgVar, Class<? extends th> cls) {
        th c2;
        qs k = k();
        return (k == null || (c2 = k.c(this, xgVar, cls)) == null) ? (th) adz.b(cls, c()) : c2;
    }
}
