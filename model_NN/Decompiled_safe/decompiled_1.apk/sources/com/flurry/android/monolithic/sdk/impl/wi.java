package com.flurry.android.monolithic.sdk.impl;

final class wi extends we {
    wi() {
        super(Character.class);
    }

    /* renamed from: c */
    public Character b(String str, qm qmVar) throws qw {
        if (str.length() == 1) {
            return Character.valueOf(str.charAt(0));
        }
        throw qmVar.a(this.a, str, "can only convert 1-character Strings");
    }
}
