package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

final class uz extends ub<aeo> {
    protected static final uz a = new uz();

    protected uz() {
        super(aeo.class);
    }

    public static uz d() {
        return a;
    }

    /* renamed from: b */
    public aeo a(ow owVar, qm qmVar) throws IOException, oz {
        if (owVar.j()) {
            return b(owVar, qmVar, qmVar.e());
        }
        throw qmVar.b(aeo.class);
    }
}
