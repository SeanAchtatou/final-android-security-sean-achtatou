package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

class jr extends kb {
    private final List<String> i;
    private final Map<String, Integer> j = new HashMap();

    public jr(ka kaVar, String str, jx<String> jxVar) {
        super(kj.ENUM, kaVar, str);
        this.i = jxVar.a();
        Iterator<String> it = jxVar.iterator();
        int i2 = 0;
        while (it.hasNext()) {
            String next = it.next();
            int i3 = i2 + 1;
            if (this.j.put(ji.h(next), Integer.valueOf(i2)) != null) {
                throw new kl("Duplicate enum symbol: " + next);
            }
            i2 = i3;
        }
    }

    public List<String> c() {
        return this.i;
    }

    public int c(String str) {
        return this.j.get(str).intValue();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof jr)) {
            return false;
        }
        jr jrVar = (jr) obj;
        return c(jrVar) && a(jrVar) && this.i.equals(jrVar.i) && this.c.equals(jrVar.c);
    }

    /* access modifiers changed from: package-private */
    public int m() {
        return super.m() + this.i.hashCode();
    }

    /* access modifiers changed from: package-private */
    public void a(kc kcVar, or orVar) throws IOException {
        if (!c(kcVar, orVar)) {
            orVar.d();
            orVar.a("type", "enum");
            d(kcVar, orVar);
            if (e() != null) {
                orVar.a("doc", e());
            }
            orVar.f("symbols");
            for (String b : this.i) {
                orVar.b(b);
            }
            orVar.c();
            this.c.a(orVar);
            a(orVar);
            orVar.e();
        }
    }
}
