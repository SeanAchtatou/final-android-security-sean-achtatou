package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public final class tf extends qu<Object> {
    final rw a;
    final qu<Object> b;

    public tf(rw rwVar, qu<Object> quVar) {
        this.a = rwVar;
        this.b = quVar;
    }

    public Object a(ow owVar, qm qmVar) throws IOException, oz {
        return this.b.a(owVar, qmVar, this.a);
    }

    public Object a(ow owVar, qm qmVar, rw rwVar) throws IOException, oz {
        throw new IllegalStateException("Type-wrapped deserializer's deserializeWithType should never get called");
    }
}
