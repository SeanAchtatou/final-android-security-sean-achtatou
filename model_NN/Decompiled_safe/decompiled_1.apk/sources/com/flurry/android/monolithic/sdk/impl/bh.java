package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import com.flurry.android.impl.ads.avro.protocol.v6.AdFrame;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;
import java.util.Map;

public class bh {
    public final String a;
    public final Map<String, String> b;
    public final Context c;
    public final AdUnit d;
    public final m e;
    public final int f;

    public bh(String str, Map<String, String> map, Context context, AdUnit adUnit, m mVar, int i) {
        this.a = str;
        this.b = map;
        this.c = context;
        this.d = adUnit;
        this.e = mVar;
        this.f = i;
    }

    public AdFrame a() {
        return this.d.d().get(this.f);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("event=").append(this.a);
        sb.append(",params=").append(this.b);
        sb.append(",adspace=").append(this.d.b());
        return sb.toString();
    }
}
