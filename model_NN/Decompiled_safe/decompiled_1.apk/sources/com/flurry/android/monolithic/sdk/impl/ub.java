package com.flurry.android.monolithic.sdk.impl;

import com.flurry.android.monolithic.sdk.impl.ou;
import com.ideaworks3d.marmalade.s3eAndroidGooglePlayBilling.util.IabHelper;
import java.io.IOException;

abstract class ub<N extends ou> extends vo<N> {
    public ub(Class<N> cls) {
        super((Class<?>) cls);
    }

    public Object a(ow owVar, qm qmVar, rw rwVar) throws IOException, oz {
        return rwVar.d(owVar, qmVar);
    }

    /* access modifiers changed from: protected */
    public void a(String str, afd afd, ou ouVar, ou ouVar2) throws oz {
    }

    /* access modifiers changed from: protected */
    public final afd a(ow owVar, qm qmVar, aez aez) throws IOException, oz {
        ou a;
        afd c = aez.c();
        pb e = owVar.e();
        if (e == pb.START_OBJECT) {
            e = owVar.b();
        }
        while (e == pb.FIELD_NAME) {
            String g = owVar.g();
            switch (owVar.b()) {
                case START_OBJECT:
                    a = a(owVar, qmVar, aez);
                    break;
                case START_ARRAY:
                    a = b(owVar, qmVar, aez);
                    break;
                case VALUE_STRING:
                    a = aez.a(owVar.k());
                    break;
                default:
                    a = c(owVar, qmVar, aez);
                    break;
            }
            ou a2 = c.a(g, a);
            if (a2 != null) {
                a(g, c, a2, a);
            }
            e = owVar.b();
        }
        return c;
    }

    /* access modifiers changed from: protected */
    public final aeo b(ow owVar, qm qmVar, aez aez) throws IOException, oz {
        aeo b = aez.b();
        while (true) {
            switch (owVar.b()) {
                case START_OBJECT:
                    b.a(a(owVar, qmVar, aez));
                    break;
                case START_ARRAY:
                    b.a(b(owVar, qmVar, aez));
                    break;
                case VALUE_STRING:
                    b.a(aez.a(owVar.k()));
                    break;
                case END_ARRAY:
                    return b;
                default:
                    b.a(c(owVar, qmVar, aez));
                    break;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final ou c(ow owVar, qm qmVar, aez aez) throws IOException, oz {
        switch (uc.a[owVar.e().ordinal()]) {
            case 1:
                return a(owVar, qmVar, aez);
            case 2:
                return b(owVar, qmVar, aez);
            case 3:
                return aez.a(owVar.k());
            case 4:
            default:
                throw qmVar.b(f());
            case 5:
                return a(owVar, qmVar, aez);
            case 6:
                Object z = owVar.z();
                if (z == null) {
                    return aez.a();
                }
                if (z.getClass() == byte[].class) {
                    return aez.a((byte[]) z);
                }
                return aez.a(z);
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED /*7*/:
                oy q = owVar.q();
                if (q == oy.BIG_INTEGER || qmVar.a(ql.USE_BIG_INTEGER_FOR_INTS)) {
                    return aez.a(owVar.v());
                }
                if (q == oy.INT) {
                    return aez.a(owVar.t());
                }
                return aez.a(owVar.u());
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED /*8*/:
                if (owVar.q() == oy.BIG_DECIMAL || qmVar.a(ql.USE_BIG_DECIMAL_FOR_FLOATS)) {
                    return aez.a(owVar.y());
                }
                return aez.a(owVar.x());
            case 9:
                return aez.a(true);
            case 10:
                return aez.a(false);
            case 11:
                return aez.a();
        }
    }
}
