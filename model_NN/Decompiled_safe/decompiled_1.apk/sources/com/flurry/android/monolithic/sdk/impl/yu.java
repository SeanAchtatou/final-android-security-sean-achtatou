package com.flurry.android.monolithic.sdk.impl;

public class yu extends yt {
    protected final String a;
    protected final String b;

    protected yu(afm afm, adk adk) {
        super(afm, adk);
        String name = afm.p().getName();
        int lastIndexOf = name.lastIndexOf(46);
        if (lastIndexOf < 0) {
            this.a = "";
            this.b = ".";
            return;
        }
        this.b = name.substring(0, lastIndexOf + 1);
        this.a = name.substring(0, lastIndexOf);
    }

    public String a(Object obj) {
        String name = obj.getClass().getName();
        if (name.startsWith(this.b)) {
            return name.substring(this.b.length() - 1);
        }
        return name;
    }

    public afm a(String str) {
        String str2;
        if (str.startsWith(".")) {
            StringBuilder sb = new StringBuilder(str.length() + this.a.length());
            if (this.a.length() == 0) {
                sb.append(str.substring(1));
            } else {
                sb.append(this.a).append(str);
            }
            str2 = sb.toString();
        } else {
            str2 = str;
        }
        return super.a(str2);
    }
}
