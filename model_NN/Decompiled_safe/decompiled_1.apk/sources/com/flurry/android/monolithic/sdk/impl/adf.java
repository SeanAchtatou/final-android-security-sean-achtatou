package com.flurry.android.monolithic.sdk.impl;

import java.util.Map;

public class adf extends adi {
    protected final afm a;
    protected final afm b;

    protected adf(Class<?> cls, afm afm, afm afm2, Object obj, Object obj2) {
        super(cls, afm.hashCode() ^ afm2.hashCode(), obj, obj2);
        this.a = afm;
        this.b = afm2;
    }

    /* access modifiers changed from: protected */
    public afm a(Class<?> cls) {
        return new adf(cls, this.a, this.b, this.f, this.g);
    }

    public afm b(Class<?> cls) {
        return cls == this.b.p() ? this : new adf(this.d, this.a, this.b.f(cls), this.f, this.g);
    }

    public afm c(Class<?> cls) {
        return cls == this.b.p() ? this : new adf(this.d, this.a, this.b.h(cls), this.f, this.g);
    }

    public afm d(Class<?> cls) {
        return cls == this.a.p() ? this : new adf(this.d, this.a.f(cls), this.b, this.f, this.g);
    }

    public afm e(Class<?> cls) {
        return cls == this.a.p() ? this : new adf(this.d, this.a.h(cls), this.b, this.f, this.g);
    }

    /* renamed from: a */
    public adf f(Object obj) {
        return new adf(this.d, this.a, this.b, this.f, obj);
    }

    /* renamed from: b */
    public adf e(Object obj) {
        return new adf(this.d, this.a, this.b.f(obj), this.f, this.g);
    }

    /* renamed from: c */
    public adf d(Object obj) {
        return new adf(this.d, this.a, this.b, obj, this.g);
    }

    /* access modifiers changed from: protected */
    public String a() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.d.getName());
        if (this.a != null) {
            sb.append('<');
            sb.append(this.a.m());
            sb.append(',');
            sb.append(this.b.m());
            sb.append('>');
        }
        return sb.toString();
    }

    public boolean f() {
        return true;
    }

    public boolean j() {
        return true;
    }

    public afm k() {
        return this.a;
    }

    public afm g() {
        return this.b;
    }

    public int h() {
        return 2;
    }

    public afm b(int i) {
        if (i == 0) {
            return this.a;
        }
        if (i == 1) {
            return this.b;
        }
        return null;
    }

    public String a(int i) {
        if (i == 0) {
            return "K";
        }
        if (i == 1) {
            return "V";
        }
        return null;
    }

    public boolean l() {
        return Map.class.isAssignableFrom(this.d);
    }

    public String toString() {
        return "[map-like type; class " + this.d.getName() + ", " + this.a + " -> " + this.b + "]";
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        adf adf = (adf) obj;
        return this.d == adf.d && this.a.equals(adf.a) && this.b.equals(adf.b);
    }
}
