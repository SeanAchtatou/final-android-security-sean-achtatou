package com.flurry.android.monolithic.sdk.impl;

import com.flurry.org.codehaus.jackson.annotate.JsonTypeInfo;

/* synthetic */ class yx {
    static final /* synthetic */ int[] a = new int[JsonTypeInfo.As.values().length];
    static final /* synthetic */ int[] b = new int[JsonTypeInfo.Id.values().length];

    static {
        try {
            b[JsonTypeInfo.Id.CLASS.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            b[JsonTypeInfo.Id.MINIMAL_CLASS.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            b[JsonTypeInfo.Id.NAME.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            b[JsonTypeInfo.Id.NONE.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            b[JsonTypeInfo.Id.CUSTOM.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
        try {
            a[JsonTypeInfo.As.WRAPPER_ARRAY.ordinal()] = 1;
        } catch (NoSuchFieldError e6) {
        }
        try {
            a[JsonTypeInfo.As.PROPERTY.ordinal()] = 2;
        } catch (NoSuchFieldError e7) {
        }
        try {
            a[JsonTypeInfo.As.WRAPPER_OBJECT.ordinal()] = 3;
        } catch (NoSuchFieldError e8) {
        }
        try {
            a[JsonTypeInfo.As.EXTERNAL_PROPERTY.ordinal()] = 4;
        } catch (NoSuchFieldError e9) {
        }
    }
}
