package com.flurry.android.monolithic.sdk.impl;

public class aaw extends zf {
    public aaw(zf zfVar) {
        super(zfVar);
    }

    public aaw(zf zfVar, ra<Object> raVar) {
        super(zfVar, raVar);
    }

    public zf a(ra<Object> raVar) {
        ra<Object> raVar2;
        if (getClass() != aaw.class) {
            throw new IllegalStateException("UnwrappingBeanPropertyWriter sub-class does not override 'withSerializer()'; needs to!");
        }
        if (!raVar.b()) {
            raVar2 = raVar.a();
        } else {
            raVar2 = raVar;
        }
        return new aaw(this, raVar2);
    }

    public void a(Object obj, or orVar, ru ruVar) throws Exception {
        Object a = a(obj);
        if (a != null) {
            if (a == obj) {
                b(obj);
            }
            if (this.l == null || !this.l.equals(a)) {
                ra<Object> raVar = this.i;
                if (raVar == null) {
                    Class<?> cls = a.getClass();
                    aal aal = this.j;
                    ra<Object> a2 = aal.a(cls);
                    if (a2 == null) {
                        raVar = a(aal, cls, ruVar);
                    } else {
                        raVar = a2;
                    }
                }
                if (!raVar.b()) {
                    orVar.a(this.g);
                }
                if (this.n == null) {
                    raVar.a(a, orVar, ruVar);
                } else {
                    raVar.a(a, orVar, ruVar, this.n);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public ra<Object> a(aal aal, Class<?> cls, ru ruVar) throws qw {
        ra<Object> a;
        if (this.o != null) {
            a = ruVar.a(ruVar.a(this.o, cls), this);
        } else {
            a = ruVar.a(cls, this);
        }
        if (!a.b()) {
            a = a.a();
        }
        this.j = this.j.a(cls, a);
        return a;
    }
}
