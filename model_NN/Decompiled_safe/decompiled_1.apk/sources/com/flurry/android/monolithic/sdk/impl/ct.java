package com.flurry.android.monolithic.sdk.impl;

import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.util.SparseArray;
import com.ideaworks3d.marmalade.S3EVideoView;
import java.util.List;

public final class ct implements cx {
    private static final String a = ct.class.getSimpleName();
    private static final SparseArray<String> b = a();

    public boolean a(Context context, db dbVar) {
        boolean z;
        if (dbVar == null) {
            return false;
        }
        String a2 = dbVar.a();
        if (TextUtils.isEmpty(a2)) {
            return false;
        }
        List<ActivityInfo> e = dbVar.e();
        if (e == null) {
            return false;
        }
        boolean z2 = true;
        PackageManager packageManager = context.getPackageManager();
        String packageName = context.getPackageName();
        for (ActivityInfo a3 : e) {
            if (!a(a2, packageManager, packageName, a3)) {
                z = false;
            } else {
                z = z2;
            }
            z2 = z;
        }
        return z2;
    }

    private boolean a(String str, PackageManager packageManager, String str2, ActivityInfo activityInfo) {
        if (TextUtils.isEmpty(str) || packageManager == null || TextUtils.isEmpty(str2) || activityInfo == null || TextUtils.isEmpty(activityInfo.name)) {
            return false;
        }
        ActivityInfo a2 = j.a(packageManager, new ComponentName(str2, activityInfo.name));
        if (a2 == null) {
            ja.b(a, str + ": package=\"" + str2 + "\": AndroidManifest.xml should include activity node with android:name=\"" + activityInfo.name + "\"");
            return false;
        }
        ja.a(3, a, str + ": package=\"" + str2 + "\": AndroidManifest.xml has activity node with android:name=\"" + activityInfo.name + "\"");
        int i = activityInfo.configChanges;
        if (i == 0) {
            return true;
        }
        int a3 = j.a(a2);
        SparseArray<String> b2 = b();
        boolean z = true;
        for (int i2 = 0; i2 < b2.size(); i2++) {
            int keyAt = b2.keyAt(i2);
            if ((keyAt & i) != 0) {
                if ((keyAt & a3) == 0) {
                    ja.b(a, str + ": package=\"" + str2 + "\": AndroidManifest.xml activity node with android:name=\"" + activityInfo.name + "\" should include android:configChanges value=\"" + b2.valueAt(i2) + "\"");
                    z = false;
                } else {
                    ja.a(3, a, str + ": package=\"" + str2 + "\": AndroidManifest.xml activity node with " + "android:name=\"" + activityInfo.name + "\" has android:configChanges value=\"" + b2.valueAt(i2) + "\"");
                }
            }
        }
        return z;
    }

    @TargetApi(13)
    private static SparseArray<String> a() {
        SparseArray<String> sparseArray = new SparseArray<>();
        sparseArray.append(1, "mcc");
        sparseArray.append(2, "mnc");
        sparseArray.append(4, "locale");
        sparseArray.append(8, "touchscreen");
        sparseArray.append(16, "keyboard");
        sparseArray.append(32, "keyboardHidden");
        sparseArray.append(64, "navigation");
        sparseArray.append(128, "orientation");
        sparseArray.append(S3EVideoView.S3E_VIDEO_MAX_VOLUME, "screenLayout");
        sparseArray.append(512, "uiMode");
        sparseArray.append(1024, "screenSize");
        sparseArray.append(2048, "smallestScreenSize");
        return sparseArray;
    }

    private static SparseArray<String> b() {
        return b;
    }
}
