package com.flurry.android.monolithic.sdk.impl;

import com.flurry.android.Constants;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

public class ll extends lx {
    private lp a = null;
    /* access modifiers changed from: private */
    public byte[] b = null;
    /* access modifiers changed from: private */
    public int c = 0;
    /* access modifiers changed from: private */
    public int d = 0;
    /* access modifiers changed from: private */
    public int e = 0;
    private final nw f = new nw();

    /* access modifiers changed from: package-private */
    public ln a() {
        return new ln(this);
    }

    protected ll() {
    }

    ll(InputStream inputStream, int i) {
        a(inputStream, i);
    }

    ll(byte[] bArr, int i, int i2) {
        a(bArr, i, i2);
    }

    /* access modifiers changed from: package-private */
    public ll a(InputStream inputStream, int i) {
        a(i, new lq(inputStream));
        return this;
    }

    /* access modifiers changed from: package-private */
    public ll a(byte[] bArr, int i, int i2) {
        a(8192, new lo(bArr, i, i2));
        return this;
    }

    private void a(int i, lp lpVar) {
        if (this.a != null) {
            this.a.a();
        }
        lpVar.a(i, this);
        this.a = lpVar;
    }

    public void b() throws IOException {
    }

    public boolean c() throws IOException {
        if (this.e == this.d) {
            this.e = this.a.b(this.b, 0, this.b.length);
            this.d = 0;
            if (this.e == 0) {
                throw new EOFException();
            }
        }
        byte[] bArr = this.b;
        int i = this.d;
        this.d = i + 1;
        return (bArr[i] & Constants.UNKNOWN) == 1;
    }

    public int d() throws IOException {
        int i;
        byte b2;
        b(5);
        byte b3 = this.b[this.d] & Constants.UNKNOWN;
        byte b4 = b3 & Byte.MAX_VALUE;
        if (b3 > Byte.MAX_VALUE) {
            int i2 = 1 + 1;
            byte b5 = this.b[1 + this.d] & Constants.UNKNOWN;
            byte b6 = ((b5 & Byte.MAX_VALUE) << 7) ^ b4;
            if (b5 > Byte.MAX_VALUE) {
                int i3 = i2 + 1;
                byte b7 = this.b[this.d + 2] & Constants.UNKNOWN;
                byte b8 = b6 ^ ((b7 & Byte.MAX_VALUE) << 14);
                if (b7 > Byte.MAX_VALUE) {
                    i3++;
                    byte b9 = this.b[this.d + 3] & Constants.UNKNOWN;
                    b8 ^= (b9 & Byte.MAX_VALUE) << 21;
                    if (b9 > Byte.MAX_VALUE) {
                        i3++;
                        byte b10 = this.b[this.d + 4] & Constants.UNKNOWN;
                        b8 ^= (b10 & Byte.MAX_VALUE) << 28;
                        if (b10 > Byte.MAX_VALUE) {
                            throw new IOException("Invalid int encoding");
                        }
                    }
                }
                b2 = b8;
                i = i3;
            } else {
                b2 = b6;
                i = i2;
            }
        } else {
            i = 1;
            b2 = b4;
        }
        this.d = i + this.d;
        if (this.d > this.e) {
            throw new EOFException();
        }
        return (-(b2 & 1)) ^ (b2 >>> 1);
    }

    public long e() throws IOException {
        long j;
        b(10);
        byte[] bArr = this.b;
        int i = this.d;
        this.d = i + 1;
        byte b2 = bArr[i] & Constants.UNKNOWN;
        byte b3 = b2 & Byte.MAX_VALUE;
        if (b2 > Byte.MAX_VALUE) {
            byte[] bArr2 = this.b;
            int i2 = this.d;
            this.d = i2 + 1;
            byte b4 = bArr2[i2] & Constants.UNKNOWN;
            byte b5 = b3 ^ ((b4 & Byte.MAX_VALUE) << 7);
            if (b4 > Byte.MAX_VALUE) {
                byte[] bArr3 = this.b;
                int i3 = this.d;
                this.d = i3 + 1;
                byte b6 = bArr3[i3] & Constants.UNKNOWN;
                byte b7 = b5 ^ ((b6 & Byte.MAX_VALUE) << 14);
                if (b6 > Byte.MAX_VALUE) {
                    byte[] bArr4 = this.b;
                    int i4 = this.d;
                    this.d = i4 + 1;
                    byte b8 = bArr4[i4] & Constants.UNKNOWN;
                    byte b9 = b7 ^ ((b8 & Byte.MAX_VALUE) << 21);
                    if (b8 > Byte.MAX_VALUE) {
                        j = b((long) b9);
                    } else {
                        j = (long) b9;
                    }
                } else {
                    j = (long) b7;
                }
            } else {
                j = (long) b5;
            }
        } else {
            j = (long) b3;
        }
        if (this.d > this.e) {
            throw new EOFException();
        }
        return (-(j & 1)) ^ (j >>> 1);
    }

    private long b(long j) throws IOException {
        long j2;
        int i = 1;
        byte b2 = this.b[this.d] & Constants.UNKNOWN;
        long j3 = ((((long) b2) & 127) << 28) ^ j;
        if (b2 > Byte.MAX_VALUE) {
            int i2 = 1 + 1;
            byte b3 = this.b[1 + this.d] & Constants.UNKNOWN;
            j2 = j3 ^ ((((long) b3) & 127) << 35);
            if (b3 > Byte.MAX_VALUE) {
                int i3 = i2 + 1;
                byte b4 = this.b[this.d + 2] & Constants.UNKNOWN;
                j2 ^= (((long) b4) & 127) << 42;
                if (b4 > Byte.MAX_VALUE) {
                    i3++;
                    byte b5 = this.b[this.d + 3] & Constants.UNKNOWN;
                    j2 ^= (((long) b5) & 127) << 49;
                    if (b5 > Byte.MAX_VALUE) {
                        i3++;
                        byte b6 = this.b[this.d + 4] & Constants.UNKNOWN;
                        j2 ^= (((long) b6) & 127) << 56;
                        if (b6 > Byte.MAX_VALUE) {
                            i3++;
                            byte b7 = this.b[this.d + 5] & Constants.UNKNOWN;
                            j2 ^= (((long) b7) & 127) << 63;
                            if (b7 > Byte.MAX_VALUE) {
                                throw new IOException("Invalid long encoding");
                            }
                        }
                    }
                }
                i = i3;
            } else {
                i = i2;
            }
        } else {
            j2 = j3;
        }
        this.d = i + this.d;
        return j2;
    }

    public float f() throws IOException {
        b(4);
        int i = 1 + 1 + 1 + 1;
        byte b2 = ((this.b[1 + this.d] & Constants.UNKNOWN) << 8) | (this.b[this.d] & Constants.UNKNOWN) | ((this.b[this.d + 2] & Constants.UNKNOWN) << 16) | ((this.b[this.d + 3] & Constants.UNKNOWN) << 24);
        if (this.d + 4 > this.e) {
            throw new EOFException();
        }
        this.d += 4;
        return Float.intBitsToFloat(b2);
    }

    public double g() throws IOException {
        b(8);
        byte b2 = ((this.b[1 + this.d] & Constants.UNKNOWN) << 8) | (this.b[this.d] & Constants.UNKNOWN) | ((this.b[this.d + 2] & Constants.UNKNOWN) << 16) | ((this.b[this.d + 3] & Constants.UNKNOWN) << 24);
        int i = 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1;
        byte b3 = (this.b[this.d + 4] & Constants.UNKNOWN) | ((this.b[this.d + 5] & Constants.UNKNOWN) << 8) | ((this.b[this.d + 6] & Constants.UNKNOWN) << 16) | ((this.b[this.d + 7] & Constants.UNKNOWN) << 24);
        if (this.d + 8 > this.e) {
            throw new EOFException();
        }
        this.d += 8;
        return Double.longBitsToDouble((((long) b3) << 32) | (((long) b2) & 4294967295L));
    }

    public nw a(nw nwVar) throws IOException {
        int d2 = d();
        nw nwVar2 = nwVar != null ? nwVar : new nw();
        nwVar2.a(d2);
        if (d2 != 0) {
            c(nwVar2.a(), 0, d2);
        }
        return nwVar2;
    }

    public String h() throws IOException {
        return a(this.f).toString();
    }

    public void i() throws IOException {
        a((long) d());
    }

    public ByteBuffer a(ByteBuffer byteBuffer) throws IOException {
        ByteBuffer allocate;
        int d2 = d();
        if (byteBuffer == null || d2 > byteBuffer.capacity()) {
            allocate = ByteBuffer.allocate(d2);
        } else {
            byteBuffer.clear();
            allocate = byteBuffer;
        }
        c(allocate.array(), allocate.position(), d2);
        allocate.limit(d2);
        return allocate;
    }

    public void j() throws IOException {
        a((long) d());
    }

    public void b(byte[] bArr, int i, int i2) throws IOException {
        c(bArr, i, i2);
    }

    public void a(int i) throws IOException {
        a((long) i);
    }

    public int k() throws IOException {
        return d();
    }

    /* access modifiers changed from: protected */
    public void a(long j) throws IOException {
        int i = this.e - this.d;
        if (j <= ((long) i)) {
            this.d = (int) (((long) this.d) + j);
            return;
        }
        this.d = 0;
        this.e = 0;
        this.a.a(j - ((long) i));
    }

    /* access modifiers changed from: protected */
    public void c(byte[] bArr, int i, int i2) throws IOException {
        int i3 = this.e - this.d;
        if (i2 <= i3) {
            System.arraycopy(this.b, this.d, bArr, i, i2);
            this.d += i2;
            return;
        }
        System.arraycopy(this.b, this.d, bArr, i, i3);
        this.d = this.e;
        this.a.a(bArr, i + i3, i2 - i3);
    }

    /* access modifiers changed from: protected */
    public long l() throws IOException {
        long e2 = e();
        if (e2 >= 0) {
            return e2;
        }
        e();
        return -e2;
    }

    private long t() throws IOException {
        int d2 = d();
        while (true) {
            long j = (long) d2;
            if (j >= 0) {
                return j;
            }
            a(e());
            d2 = d();
        }
    }

    public long m() throws IOException {
        return l();
    }

    public long n() throws IOException {
        return l();
    }

    public long o() throws IOException {
        return t();
    }

    public long p() throws IOException {
        return l();
    }

    public long q() throws IOException {
        return l();
    }

    public long r() throws IOException {
        return t();
    }

    public int s() throws IOException {
        return d();
    }

    private void b(int i) throws IOException {
        int i2 = this.e - this.d;
        if (i2 < i) {
            this.a.a(this.b, this.d, this.c, i2);
        }
    }
}
