package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
public final class ace extends abw<short[]> {
    public ace() {
        this(null);
    }

    public ace(rx rxVar) {
        super(short[].class, rxVar, null);
    }

    public abc<?> a(rx rxVar) {
        return new ace(rxVar);
    }

    /* renamed from: a */
    public void b(short[] sArr, or orVar, ru ruVar) throws IOException, oq {
        for (short b : sArr) {
            orVar.b(b);
        }
    }
}
