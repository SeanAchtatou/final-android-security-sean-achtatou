package com.flurry.android.monolithic.sdk.impl;

import java.util.Set;

public class js {
    /* access modifiers changed from: private */
    public final String a;
    /* access modifiers changed from: private */
    public transient int b = -1;
    /* access modifiers changed from: private */
    public final ji c;
    /* access modifiers changed from: private */
    public final String d;
    /* access modifiers changed from: private */
    public final ou e;
    /* access modifiers changed from: private */
    public final jt f;
    /* access modifiers changed from: private */
    public Set<String> g;
    /* access modifiers changed from: private */
    public final kf h = new kf(ji.h);

    public js(String str, ji jiVar, String str2, ou ouVar, jt jtVar) {
        this.a = ji.h(str);
        this.c = jiVar;
        this.d = str2;
        this.e = ouVar;
        this.f = jtVar;
    }

    public String a() {
        return this.a;
    }

    public int b() {
        return this.b;
    }

    public ji c() {
        return this.c;
    }

    public String d() {
        return this.d;
    }

    public ou e() {
        return this.e;
    }

    public jt f() {
        return this.f;
    }

    public synchronized void a(String str, String str2) {
        this.h.a(str, str2);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof js)) {
            return false;
        }
        js jsVar = (js) obj;
        return this.a.equals(jsVar.a) && this.c.equals(jsVar.c) && a(jsVar.e) && this.h.equals(jsVar.h);
    }

    public int hashCode() {
        return this.a.hashCode() + this.c.m();
    }

    private boolean a(ou ouVar) {
        if (this.e == null) {
            return ouVar == null;
        }
        if (Double.isNaN(this.e.n())) {
            return Double.isNaN(ouVar.n());
        }
        return this.e.equals(ouVar);
    }

    public String toString() {
        return this.a + " type:" + this.c.f + " pos:" + this.b;
    }
}
