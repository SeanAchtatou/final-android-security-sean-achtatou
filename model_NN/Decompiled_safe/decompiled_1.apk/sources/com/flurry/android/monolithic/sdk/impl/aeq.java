package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.math.BigInteger;

public final class aeq extends afc {
    protected final BigInteger c;

    public aeq(BigInteger bigInteger) {
        this.c = bigInteger;
    }

    public static aeq a(BigInteger bigInteger) {
        return new aeq(bigInteger);
    }

    public int j() {
        return this.c.intValue();
    }

    public long k() {
        return this.c.longValue();
    }

    public double l() {
        return this.c.doubleValue();
    }

    public String m() {
        return this.c.toString();
    }

    public final void a(or orVar, ru ruVar) throws IOException, oz {
        orVar.a(this.c);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        return ((aeq) obj).c == this.c;
    }

    public int hashCode() {
        return this.c.hashCode();
    }
}
