package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public final class aey extends afc {
    private static final aey[] d = new aey[12];
    final int c;

    static {
        for (int i = 0; i < 12; i++) {
            d[i] = new aey(i - 1);
        }
    }

    public aey(int i) {
        this.c = i;
    }

    public static aey a(int i) {
        if (i > 10 || i < -1) {
            return new aey(i);
        }
        return d[i - -1];
    }

    public boolean d() {
        return true;
    }

    public int j() {
        return this.c;
    }

    public long k() {
        return (long) this.c;
    }

    public double l() {
        return (double) this.c;
    }

    public String m() {
        return pu.a(this.c);
    }

    public final void a(or orVar, ru ruVar) throws IOException, oz {
        orVar.b(this.c);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        return ((aey) obj).c == this.c;
    }

    public int hashCode() {
        return this.c;
    }
}
