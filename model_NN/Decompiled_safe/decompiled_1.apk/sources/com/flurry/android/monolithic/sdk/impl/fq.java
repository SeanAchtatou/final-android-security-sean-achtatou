package com.flurry.android.monolithic.sdk.impl;

import org.json.JSONException;
import org.json.JSONObject;

public class fq {
    ft a;
    JSONObject b;

    public fq(JSONObject jSONObject) {
        this.b = jSONObject;
    }

    public boolean a() {
        try {
            if (this.b == null || (this.b.getInt("code") != 200 && this.b.getInt("code") != 201)) {
                return false;
            }
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public String b() {
        try {
            if (this.b != null) {
                return this.b.get("note").toString();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "Bad response";
    }

    public JSONObject c() {
        return this.b;
    }

    public int d() {
        try {
            if (this.b != null) {
                return this.b.getInt("code");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return 400;
    }
}
