package com.flurry.android.monolithic.sdk.impl;

import com.flurry.org.codehaus.jackson.annotate.JsonAutoDetect;
import java.util.HashMap;

public class rq extends ri<rr, rq> {
    protected sf a;
    protected Class<?> b;
    protected zl c;

    public rq(qf<? extends qb> qfVar, py pyVar, ye<?> yeVar, yh yhVar, rl rlVar, adk adk, qs qsVar) {
        super(qfVar, pyVar, yeVar, yhVar, rlVar, adk, qsVar, d(rr.class));
        this.a = null;
        this.c = null;
    }

    protected rq(rq rqVar, HashMap<adb, Class<?>> hashMap, yh yhVar) {
        this(rqVar, rqVar.e);
        this.f = hashMap;
        this.h = yhVar;
    }

    protected rq(rq rqVar, rg rgVar) {
        super(rqVar, rgVar, rqVar.h);
        this.a = null;
        this.a = rqVar.a;
        this.b = rqVar.b;
        this.c = rqVar.c;
    }

    public rq a(yh yhVar) {
        HashMap hashMap = this.f;
        this.g = true;
        return new rq(this, hashMap, yhVar);
    }

    public py a() {
        if (a(rr.USE_ANNOTATIONS)) {
            return super.a();
        }
        return py.a();
    }

    public <T extends qb> T a(afm afm) {
        return i().a((rf<?>) this, afm, this);
    }

    public boolean b() {
        return a(rr.USE_ANNOTATIONS);
    }

    public boolean c() {
        return a(rr.CAN_OVERRIDE_ACCESS_MODIFIERS);
    }

    public boolean d() {
        return a(rr.SORT_PROPERTIES_ALPHABETICALLY);
    }

    public ye<?> e() {
        ye e = super.e();
        if (!a(rr.AUTO_DETECT_GETTERS)) {
            e = e.a(JsonAutoDetect.Visibility.NONE);
        }
        if (!a(rr.AUTO_DETECT_IS_GETTERS)) {
            e = e.b(JsonAutoDetect.Visibility.NONE);
        }
        if (!a(rr.AUTO_DETECT_FIELDS)) {
            return e.e(JsonAutoDetect.Visibility.NONE);
        }
        return e;
    }

    public boolean a(rr rrVar) {
        return (this.i & rrVar.b()) != 0;
    }

    public Class<?> f() {
        return this.b;
    }

    public sf g() {
        if (this.a != null) {
            return this.a;
        }
        return a(rr.WRITE_NULL_PROPERTIES) ? sf.ALWAYS : sf.NON_NULL;
    }

    public zl h() {
        return this.c;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.qf.a(com.flurry.android.monolithic.sdk.impl.rq, com.flurry.android.monolithic.sdk.impl.afm, com.flurry.android.monolithic.sdk.impl.qg):T
     arg types: [com.flurry.android.monolithic.sdk.impl.rq, com.flurry.android.monolithic.sdk.impl.afm, com.flurry.android.monolithic.sdk.impl.rq]
     candidates:
      com.flurry.android.monolithic.sdk.impl.qf.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.afm, com.flurry.android.monolithic.sdk.impl.qg):T
      com.flurry.android.monolithic.sdk.impl.qf.a(com.flurry.android.monolithic.sdk.impl.rf<?>, com.flurry.android.monolithic.sdk.impl.afm, com.flurry.android.monolithic.sdk.impl.qg):T
      com.flurry.android.monolithic.sdk.impl.qf.a(com.flurry.android.monolithic.sdk.impl.rq, com.flurry.android.monolithic.sdk.impl.afm, com.flurry.android.monolithic.sdk.impl.qg):T */
    public <T extends qb> T b(afm afm) {
        return i().a(this, afm, (qg) this);
    }

    public ra<Object> a(xg xgVar, Class<? extends ra<?>> cls) {
        ra<?> a2;
        qs k = k();
        return (k == null || (a2 = k.a(this, xgVar, cls)) == null) ? (ra) adz.b(cls, c()) : a2;
    }

    public String toString() {
        return "[SerializationConfig: flags=0x" + Integer.toHexString(this.i) + "]";
    }
}
