package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public abstract class aay<T> extends abc<T> implements rp {
    protected final boolean a;
    protected final afm b;
    protected final rx c;
    protected ra<Object> d;
    protected final qc e;
    protected aal f;

    /* access modifiers changed from: protected */
    public abstract void b(T t, or orVar, ru ruVar) throws IOException, oq;

    protected aay(Class<?> cls, afm afm, boolean z, rx rxVar, qc qcVar, ra<Object> raVar) {
        super(cls, false);
        this.b = afm;
        this.a = z || (afm != null && afm.u());
        this.c = rxVar;
        this.e = qcVar;
        this.d = raVar;
        this.f = aal.a();
    }

    public final void a(T t, or orVar, ru ruVar) throws IOException, oq {
        orVar.b();
        b(t, orVar, ruVar);
        orVar.c();
    }

    public final void a(T t, or orVar, ru ruVar, rx rxVar) throws IOException, oq {
        rxVar.c(t, orVar);
        b(t, orVar, ruVar);
        rxVar.f(t, orVar);
    }

    public void a(ru ruVar) throws qw {
        if (this.a && this.b != null && this.d == null) {
            this.d = ruVar.a(this.b, this.e);
        }
    }

    /* access modifiers changed from: protected */
    public final ra<Object> a(aal aal, Class<?> cls, ru ruVar) throws qw {
        aap a2 = aal.a(cls, ruVar, this.e);
        if (aal != a2.b) {
            this.f = a2.b;
        }
        return a2.a;
    }

    /* access modifiers changed from: protected */
    public final ra<Object> a(aal aal, afm afm, ru ruVar) throws qw {
        aap a2 = aal.a(afm, ruVar, this.e);
        if (aal != a2.b) {
            this.f = a2.b;
        }
        return a2.a;
    }
}
