package com.flurry.android.monolithic.sdk.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ss extends so {
    public static final ss h = new ss(null);
    private static final Class<?>[] j = {Throwable.class};
    protected final qp i;

    @Deprecated
    public ss() {
        this(null);
    }

    public ss(qp qpVar) {
        qp qpVar2;
        if (qpVar == null) {
            qpVar2 = new st();
        } else {
            qpVar2 = qpVar;
        }
        this.i = qpVar2;
    }

    public rc a(qk qkVar, afm afm, qc qcVar) throws qw {
        if (this.i.f()) {
            xq xqVar = (xq) qkVar.c(afm.p());
            for (re a : this.i.b()) {
                rc a2 = a.a(afm, qkVar, xqVar, qcVar);
                if (a2 != null) {
                    return a2;
                }
            }
        }
        Class<?> p = afm.p();
        if (p == String.class || p == Object.class) {
            return wu.a(qkVar, afm);
        }
        rc rcVar = (rc) c.get(afm);
        if (rcVar != null) {
            return rcVar;
        }
        if (afm.r()) {
            return c(qkVar, afm, qcVar);
        }
        return wu.b(qkVar, afm);
    }

    private rc c(qk qkVar, afm afm, qc qcVar) throws qw {
        Class<?> p = afm.p();
        aed<?> a = a(p, qkVar);
        for (xl next : ((xq) qkVar.b(afm)).o()) {
            if (qkVar.a().k(next)) {
                if (next.f() != 1 || !next.d().isAssignableFrom(p)) {
                    throw new IllegalArgumentException("Unsuitable method (" + next + ") decorated with @JsonCreator (for Enum type " + p.getName() + ")");
                } else if (next.b(0) != String.class) {
                    throw new IllegalArgumentException("Parameter #0 type for factory method (" + next + ") not suitable, must be java.lang.String");
                } else {
                    if (qkVar.c()) {
                        adz.a(next.i());
                    }
                    return wu.a(a, next);
                }
            }
        }
        return wu.a(a);
    }

    /* access modifiers changed from: protected */
    public qu<?> a(ada ada, qk qkVar, qq qqVar, qc qcVar, rw rwVar, qu<?> quVar) throws qw {
        for (qr a : this.i.a()) {
            qu<?> a2 = a.a(ada, qkVar, qqVar, qcVar, rwVar, quVar);
            if (a2 != null) {
                return a2;
            }
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.qr.a(com.flurry.android.monolithic.sdk.impl.add, com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.qb, com.flurry.android.monolithic.sdk.impl.qc, com.flurry.android.monolithic.sdk.impl.rw, com.flurry.android.monolithic.sdk.impl.qu<?>):com.flurry.android.monolithic.sdk.impl.qu<?>
     arg types: [com.flurry.android.monolithic.sdk.impl.add, com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.xq, com.flurry.android.monolithic.sdk.impl.qc, com.flurry.android.monolithic.sdk.impl.rw, com.flurry.android.monolithic.sdk.impl.qu<?>]
     candidates:
      com.flurry.android.monolithic.sdk.impl.qr.a(com.flurry.android.monolithic.sdk.impl.adc, com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.qb, com.flurry.android.monolithic.sdk.impl.qc, com.flurry.android.monolithic.sdk.impl.rw, com.flurry.android.monolithic.sdk.impl.qu<?>):com.flurry.android.monolithic.sdk.impl.qu<?>
      com.flurry.android.monolithic.sdk.impl.qr.a(com.flurry.android.monolithic.sdk.impl.add, com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.qb, com.flurry.android.monolithic.sdk.impl.qc, com.flurry.android.monolithic.sdk.impl.rw, com.flurry.android.monolithic.sdk.impl.qu<?>):com.flurry.android.monolithic.sdk.impl.qu<?> */
    /* access modifiers changed from: protected */
    public qu<?> a(add add, qk qkVar, qq qqVar, xq xqVar, qc qcVar, rw rwVar, qu<?> quVar) throws qw {
        for (qr a : this.i.a()) {
            qu<?> a2 = a.a(add, qkVar, qqVar, (qb) xqVar, qcVar, rwVar, quVar);
            if (a2 != null) {
                return a2;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public qu<?> a(adc adc, qk qkVar, qq qqVar, xq xqVar, qc qcVar, rw rwVar, qu<?> quVar) throws qw {
        for (qr a : this.i.a()) {
            qu<?> a2 = a.a(adc, qkVar, qqVar, xqVar, qcVar, rwVar, quVar);
            if (a2 != null) {
                return a2;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public qu<?> a(Class<?> cls, qk qkVar, xq xqVar, qc qcVar) throws qw {
        for (qr a : this.i.a()) {
            qu<?> a2 = a.a(cls, qkVar, xqVar, qcVar);
            if (a2 != null) {
                return a2;
            }
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.qr.a(com.flurry.android.monolithic.sdk.impl.adg, com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.qb, com.flurry.android.monolithic.sdk.impl.qc, com.flurry.android.monolithic.sdk.impl.rc, com.flurry.android.monolithic.sdk.impl.rw, com.flurry.android.monolithic.sdk.impl.qu<?>):com.flurry.android.monolithic.sdk.impl.qu<?>
     arg types: [com.flurry.android.monolithic.sdk.impl.adg, com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.xq, com.flurry.android.monolithic.sdk.impl.qc, com.flurry.android.monolithic.sdk.impl.rc, com.flurry.android.monolithic.sdk.impl.rw, com.flurry.android.monolithic.sdk.impl.qu<?>]
     candidates:
      com.flurry.android.monolithic.sdk.impl.qr.a(com.flurry.android.monolithic.sdk.impl.adf, com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.qb, com.flurry.android.monolithic.sdk.impl.qc, com.flurry.android.monolithic.sdk.impl.rc, com.flurry.android.monolithic.sdk.impl.rw, com.flurry.android.monolithic.sdk.impl.qu<?>):com.flurry.android.monolithic.sdk.impl.qu<?>
      com.flurry.android.monolithic.sdk.impl.qr.a(com.flurry.android.monolithic.sdk.impl.adg, com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.qb, com.flurry.android.monolithic.sdk.impl.qc, com.flurry.android.monolithic.sdk.impl.rc, com.flurry.android.monolithic.sdk.impl.rw, com.flurry.android.monolithic.sdk.impl.qu<?>):com.flurry.android.monolithic.sdk.impl.qu<?> */
    /* access modifiers changed from: protected */
    public qu<?> a(adg adg, qk qkVar, qq qqVar, xq xqVar, qc qcVar, rc rcVar, rw rwVar, qu<?> quVar) throws qw {
        for (qr a : this.i.a()) {
            qu<?> a2 = a.a(adg, qkVar, qqVar, (qb) xqVar, qcVar, rcVar, rwVar, quVar);
            if (a2 != null) {
                return a2;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public qu<?> a(adf adf, qk qkVar, qq qqVar, xq xqVar, qc qcVar, rc rcVar, rw rwVar, qu<?> quVar) throws qw {
        for (qr a : this.i.a()) {
            qu<?> a2 = a.a(adf, qkVar, qqVar, xqVar, qcVar, rcVar, rwVar, quVar);
            if (a2 != null) {
                return a2;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public qu<?> a(Class<? extends ou> cls, qk qkVar, qc qcVar) throws qw {
        for (qr a : this.i.a()) {
            qu<?> a2 = a.a(cls, qkVar, qcVar);
            if (a2 != null) {
                return a2;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public qu<Object> a(afm afm, qk qkVar, qq qqVar, xq xqVar, qc qcVar) throws qw {
        for (qr a : this.i.a()) {
            qu<?> a2 = a.a(afm, qkVar, qqVar, xqVar, qcVar);
            if (a2 != null) {
                return a2;
            }
        }
        return null;
    }

    public afm a(qk qkVar, afm afm) throws qw {
        afm b;
        afm afm2 = afm;
        while (true) {
            b = b(qkVar, afm2);
            if (b == null) {
                return afm2;
            }
            Class<?> p = afm2.p();
            Class<?> p2 = b.p();
            if (p != p2 && p.isAssignableFrom(p2)) {
                afm2 = b;
            }
        }
        throw new IllegalArgumentException("Invalid abstract type resolution from " + afm2 + " to " + b + ": latter is not a subtype of former");
    }

    public th a(qk qkVar, xq xqVar) throws qw {
        th c;
        xh c2 = xqVar.c();
        Object j2 = qkVar.a().j(c2);
        if (j2 == null) {
            c = c(qkVar, xqVar);
        } else if (j2 instanceof th) {
            c = (th) j2;
        } else if (!(j2 instanceof Class)) {
            throw new IllegalStateException("Invalid value instantiator returned for type " + xqVar + ": neither a Class nor ValueInstantiator");
        } else {
            Class cls = (Class) j2;
            if (!th.class.isAssignableFrom(cls)) {
                throw new IllegalStateException("Invalid instantiator Class<?> returned for type " + xqVar + ": " + cls.getName() + " not a ValueInstantiator");
            }
            c = qkVar.c(c2, cls);
        }
        if (this.i.i()) {
            for (ti next : this.i.e()) {
                c = next.a(qkVar, xqVar, c);
                if (c == null) {
                    throw new qw("Broken registered ValueInstantiators (of type " + next.getClass().getName() + "): returned null ValueInstantiator");
                }
            }
        }
        return c;
    }

    public qu<Object> a(qk qkVar, qq qqVar, afm afm, qc qcVar) throws qw {
        afm afm2;
        xq xqVar;
        afm b;
        if (afm.c()) {
            afm2 = a(qkVar, afm);
        } else {
            afm2 = afm;
        }
        xq xqVar2 = (xq) qkVar.b(afm2);
        qu<Object> a = a(qkVar, xqVar2.c(), qcVar);
        if (a != null) {
            return a;
        }
        afm a2 = a(qkVar, xqVar2.c(), afm2, (String) null);
        if (a2.p() != afm2.p()) {
            xqVar = (xq) qkVar.b(a2);
            afm2 = a2;
        } else {
            xqVar = xqVar2;
        }
        qu<Object> a3 = a(afm2, qkVar, qqVar, xqVar, qcVar);
        if (a3 != null) {
            return a3;
        }
        if (afm2.q()) {
            return b(qkVar, afm2, xqVar, qcVar);
        }
        if (afm2.c() && (b = b(qkVar, xqVar)) != null) {
            return a(qkVar, b, (xq) qkVar.b(b), qcVar);
        }
        qu<Object> d = d(qkVar, qqVar, afm2, qcVar);
        if (d != null) {
            return d;
        }
        if (!a(afm2.p())) {
            return null;
        }
        return a(qkVar, afm2, xqVar, qcVar);
    }

    /* access modifiers changed from: protected */
    public afm b(qk qkVar, afm afm) throws qw {
        Class<?> p = afm.p();
        if (this.i.h()) {
            for (px a : this.i.d()) {
                afm a2 = a.a(qkVar, afm);
                if (a2 != null && a2.p() != p) {
                    return a2;
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public afm b(qk qkVar, xq xqVar) throws qw {
        afm a = xqVar.a();
        for (px b : this.i.d()) {
            afm b2 = b.b(qkVar, a);
            if (b2 != null) {
                return b2;
            }
        }
        return null;
    }

    public qu<Object> a(qk qkVar, afm afm, xq xqVar, qc qcVar) throws qw {
        th a = a(qkVar, xqVar);
        if (afm.c() && !a.b()) {
            return new sm(afm);
        }
        sr a2 = a(xqVar);
        a2.a(a);
        a(qkVar, xqVar, a2);
        b(qkVar, xqVar, a2);
        c(qkVar, xqVar, a2);
        if (this.i.g()) {
            for (su a3 : this.i.c()) {
                a2 = a3.a(qkVar, xqVar, a2);
            }
        }
        qu<?> a4 = a2.a(qcVar);
        if (!this.i.g()) {
            return a4;
        }
        for (su a5 : this.i.c()) {
            a4 = a5.a(qkVar, xqVar, a4);
        }
        return a4;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.sr.a(com.flurry.android.monolithic.sdk.impl.sw, boolean):void
     arg types: [com.flurry.android.monolithic.sdk.impl.sw, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.sr.a(java.lang.String, com.flurry.android.monolithic.sdk.impl.sw):void
      com.flurry.android.monolithic.sdk.impl.sr.a(com.flurry.android.monolithic.sdk.impl.sw, boolean):void */
    public qu<Object> b(qk qkVar, afm afm, xq xqVar, qc qcVar) throws qw {
        sr srVar;
        sw a;
        sr a2 = a(xqVar);
        a2.a(a(qkVar, xqVar));
        a(qkVar, xqVar, a2);
        xl a3 = xqVar.a("initCause", j);
        if (!(a3 == null || (a = a(qkVar, xqVar, "cause", a3)) == null)) {
            a2.a(a, true);
        }
        a2.a("localizedMessage");
        a2.a("message");
        if (this.i.g()) {
            Iterator<su> it = this.i.c().iterator();
            while (true) {
                srVar = a2;
                if (!it.hasNext()) {
                    break;
                }
                a2 = it.next().a(qkVar, xqVar, srVar);
            }
            a2 = srVar;
        }
        qu<?> a4 = a2.a(qcVar);
        if (a4 instanceof sp) {
            a4 = new wz((sp) a4);
        }
        if (this.i.g()) {
            for (su a5 : this.i.c()) {
                a4 = a5.a(qkVar, xqVar, a4);
            }
        }
        return a4;
    }

    /* access modifiers changed from: protected */
    public sr a(xq xqVar) {
        return new sr(xqVar);
    }

    /* access modifiers changed from: protected */
    public th c(qk qkVar, xq xqVar) throws qw {
        xi k;
        boolean a = qkVar.a(ql.CAN_OVERRIDE_ACCESS_MODIFIERS);
        tm tmVar = new tm(xqVar, a);
        py a2 = qkVar.a();
        if (xqVar.a().d() && (k = xqVar.k()) != null) {
            if (a) {
                adz.a(k.a());
            }
            tmVar.a(k);
        }
        ye<?> a3 = qkVar.a().a(xqVar.c(), qkVar.e());
        b(qkVar, xqVar, a3, a2, tmVar);
        a(qkVar, xqVar, a3, a2, tmVar);
        return tmVar.a(qkVar);
    }

    /* access modifiers changed from: protected */
    public void a(qk qkVar, xq xqVar, ye<?> yeVar, py pyVar, tm tmVar) throws qw {
        for (xi next : xqVar.n()) {
            int f = next.f();
            if (f >= 1) {
                boolean k = pyVar.k(next);
                boolean a = yeVar.a(next);
                if (f == 1) {
                    a(qkVar, xqVar, yeVar, pyVar, tmVar, next, k, a);
                } else if (k || a) {
                    int i2 = 0;
                    tn[] tnVarArr = new tn[f];
                    xn xnVar = null;
                    int i3 = 0;
                    for (int i4 = 0; i4 < f; i4++) {
                        xn c = next.c(i4);
                        String a2 = c == null ? null : pyVar.a(c);
                        Object d = pyVar.d((xk) c);
                        if (a2 != null && a2.length() > 0) {
                            i2++;
                            tnVarArr[i4] = a(qkVar, xqVar, a2, i4, c, d);
                        } else if (d != null) {
                            i3++;
                            tnVarArr[i4] = a(qkVar, xqVar, a2, i4, c, d);
                        } else if (xnVar == null) {
                            xnVar = c;
                        }
                    }
                    if (k || i2 > 0 || i3 > 0) {
                        if (i2 + i3 == f) {
                            tmVar.a(next, tnVarArr);
                        } else if (i2 == 0 && i3 + 1 == f) {
                            throw new IllegalArgumentException("Delegated constructor with Injectables not yet supported (see [JACKSON-712]) for " + next);
                        } else {
                            throw new IllegalArgumentException("Argument #" + xnVar.g() + " of constructor " + next + " has no property name annotation; must have name when multiple-paramater constructor annotated as Creator");
                        }
                    }
                    if (0 != 0) {
                        tmVar.a(next, tnVarArr);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(qk qkVar, xq xqVar, ye<?> yeVar, py pyVar, tm tmVar, xi xiVar, boolean z, boolean z2) throws qw {
        xn c = xiVar.c(0);
        String a = pyVar.a(c);
        Object d = pyVar.d((xk) c);
        if (d != null || (a != null && a.length() > 0)) {
            tmVar.a(xiVar, new tn[]{a(qkVar, xqVar, a, 0, c, d)});
            return true;
        }
        Class<?> a2 = xiVar.a(0);
        if (a2 == String.class) {
            if (z || z2) {
                tmVar.a((xo) xiVar);
            }
            return true;
        } else if (a2 == Integer.TYPE || a2 == Integer.class) {
            if (z || z2) {
                tmVar.b(xiVar);
            }
            return true;
        } else if (a2 == Long.TYPE || a2 == Long.class) {
            if (z || z2) {
                tmVar.c(xiVar);
            }
            return true;
        } else if (a2 == Double.TYPE || a2 == Double.class) {
            if (z || z2) {
                tmVar.d(xiVar);
            }
            return true;
        } else if (!z) {
            return false;
        } else {
            tmVar.f(xiVar);
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public void b(qk qkVar, xq xqVar, ye<?> yeVar, py pyVar, tm tmVar) throws qw {
        for (xl next : xqVar.o()) {
            int f = next.f();
            if (f >= 1) {
                boolean k = pyVar.k(next);
                if (f == 1) {
                    xn c = next.c(0);
                    String a = pyVar.a(c);
                    if (pyVar.d((xk) c) == null && (a == null || a.length() == 0)) {
                        a(qkVar, xqVar, yeVar, pyVar, tmVar, next, k);
                    }
                } else if (!pyVar.k(next)) {
                    continue;
                }
                tn[] tnVarArr = new tn[f];
                for (int i2 = 0; i2 < f; i2++) {
                    xn c2 = next.c(i2);
                    String a2 = pyVar.a(c2);
                    Object d = pyVar.d((xk) c2);
                    if ((a2 == null || a2.length() == 0) && d == null) {
                        throw new IllegalArgumentException("Argument #" + i2 + " of factory method " + next + " has no property name annotation; must have when multiple-paramater static method annotated as Creator");
                    }
                    tnVarArr[i2] = a(qkVar, xqVar, a2, i2, c2, d);
                }
                tmVar.a(next, tnVarArr);
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(qk qkVar, xq xqVar, ye<?> yeVar, py pyVar, tm tmVar, xl xlVar, boolean z) throws qw {
        Class<?> a = xlVar.a(0);
        if (a == String.class) {
            if (z || yeVar.a((xk) xlVar)) {
                tmVar.a(xlVar);
            }
            return true;
        } else if (a == Integer.TYPE || a == Integer.class) {
            if (z || yeVar.a((xk) xlVar)) {
                tmVar.b(xlVar);
            }
            return true;
        } else if (a == Long.TYPE || a == Long.class) {
            if (z || yeVar.a((xk) xlVar)) {
                tmVar.c(xlVar);
            }
            return true;
        } else if (a == Double.TYPE || a == Double.class) {
            if (z || yeVar.a((xk) xlVar)) {
                tmVar.d(xlVar);
            }
            return true;
        } else if (a == Boolean.TYPE || a == Boolean.class) {
            if (z || yeVar.a((xk) xlVar)) {
                tmVar.e(xlVar);
            }
            return true;
        } else if (!pyVar.k(xlVar)) {
            return false;
        } else {
            tmVar.f(xlVar);
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public tn a(qk qkVar, xq xqVar, String str, int i2, xn xnVar, Object obj) throws qw {
        qd qdVar;
        rw rwVar;
        afm a = qkVar.m().a(xnVar.e(), xqVar.j());
        qd qdVar2 = new qd(str, a, xqVar.i(), xnVar);
        afm a2 = a(qkVar, xqVar, a, xnVar, qdVar2);
        if (a2 != a) {
            qdVar = qdVar2.a(a2);
        } else {
            qdVar = qdVar2;
        }
        qu<Object> a3 = a(qkVar, xnVar, qdVar);
        afm a4 = a(qkVar, xnVar, a2, str);
        rw rwVar2 = (rw) a4.o();
        if (rwVar2 == null) {
            rwVar = b(qkVar, a4, qdVar);
        } else {
            rwVar = rwVar2;
        }
        tn tnVar = new tn(str, a4, rwVar, xqVar.i(), xnVar, i2, obj);
        if (a3 != null) {
            return tnVar.a(a3);
        }
        return tnVar;
    }

    /* access modifiers changed from: protected */
    public void a(qk qkVar, xq xqVar, sr srVar) throws qw {
        List<qe> d = xqVar.d();
        py a = qkVar.a();
        Boolean d2 = a.d(xqVar.c());
        if (d2 != null) {
            srVar.a(d2.booleanValue());
        }
        HashSet<String> a2 = adp.a(a.c(xqVar.c()));
        for (String a3 : a2) {
            srVar.a(a3);
        }
        xl l = xqVar.l();
        Set<String> f = l == null ? xqVar.f() : xqVar.g();
        if (f != null) {
            for (String a4 : f) {
                srVar.a(a4);
            }
        }
        HashMap hashMap = new HashMap();
        for (qe next : d) {
            String a5 = next.a();
            if (!a2.contains(a5)) {
                if (next.e()) {
                    srVar.a(next);
                } else if (next.c()) {
                    xl h2 = next.h();
                    if (a(qkVar, xqVar, h2.a(0), hashMap)) {
                        srVar.a(a5);
                    } else {
                        sw a6 = a(qkVar, xqVar, a5, h2);
                        if (a6 != null) {
                            srVar.a(a6);
                        }
                    }
                } else if (next.d()) {
                    xj i2 = next.i();
                    if (a(qkVar, xqVar, i2.d(), hashMap)) {
                        srVar.a(a5);
                    } else {
                        sw a7 = a(qkVar, xqVar, a5, i2);
                        if (a7 != null) {
                            srVar.a(a7);
                        }
                    }
                }
            }
        }
        if (l != null) {
            srVar.a(a(qkVar, xqVar, l));
        }
        if (qkVar.a(ql.USE_GETTERS_AS_SETTERS)) {
            for (qe next2 : d) {
                if (next2.b()) {
                    String a8 = next2.a();
                    if (!srVar.b(a8) && !a2.contains(a8)) {
                        xl g = next2.g();
                        Class<?> d3 = g.d();
                        if ((Collection.class.isAssignableFrom(d3) || Map.class.isAssignableFrom(d3)) && !a2.contains(a8) && !srVar.b(a8)) {
                            srVar.a(b(qkVar, xqVar, a8, g));
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b(qk qkVar, xq xqVar, sr srVar) throws qw {
        Map<String, xk> q = xqVar.q();
        if (q != null) {
            for (Map.Entry next : q.entrySet()) {
                String str = (String) next.getKey();
                xk xkVar = (xk) next.getValue();
                if (xkVar instanceof xl) {
                    srVar.a(str, a(qkVar, xqVar, xkVar.b(), (xl) xkVar));
                } else {
                    srVar.a(str, a(qkVar, xqVar, xkVar.b(), (xj) xkVar));
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void c(qk qkVar, xq xqVar, sr srVar) throws qw {
        Map<Object, xk> m = xqVar.m();
        if (m != null) {
            boolean a = qkVar.a(ql.CAN_OVERRIDE_ACCESS_MODIFIERS);
            for (Map.Entry next : m.entrySet()) {
                xk xkVar = (xk) next.getValue();
                if (a) {
                    xkVar.k();
                }
                srVar.a(xkVar.b(), xqVar.a(xkVar.c()), xqVar.i(), xkVar, next.getKey());
            }
        }
    }

    /* access modifiers changed from: protected */
    public sv a(qk qkVar, xq xqVar, xl xlVar) throws qw {
        if (qkVar.a(ql.CAN_OVERRIDE_ACCESS_MODIFIERS)) {
            xlVar.k();
        }
        afm a = xqVar.j().a(xlVar.b(1));
        qd qdVar = new qd(xlVar.b(), a, xqVar.i(), xlVar);
        afm a2 = a(qkVar, xqVar, a, xlVar, qdVar);
        qu<Object> a3 = a(qkVar, xlVar, qdVar);
        if (a3 != null) {
            return new sv(qdVar, xlVar, a2, a3);
        }
        return new sv(qdVar, xlVar, a(qkVar, xlVar, a2, qdVar.c()), (qu<Object>) null);
    }

    /* access modifiers changed from: protected */
    public sw a(qk qkVar, xq xqVar, String str, xl xlVar) throws qw {
        qd qdVar;
        if (qkVar.a(ql.CAN_OVERRIDE_ACCESS_MODIFIERS)) {
            xlVar.k();
        }
        afm a = xqVar.j().a(xlVar.b(0));
        qd qdVar2 = new qd(str, a, xqVar.i(), xlVar);
        afm a2 = a(qkVar, xqVar, a, xlVar, qdVar2);
        if (a2 != a) {
            qdVar = qdVar2.a(a2);
        } else {
            qdVar = qdVar2;
        }
        qu<Object> a3 = a(qkVar, xlVar, qdVar);
        afm a4 = a(qkVar, xlVar, a2, str);
        sw taVar = new ta(str, a4, (rw) a4.o(), xqVar.i(), xlVar);
        if (a3 != null) {
            taVar = taVar.a(a3);
        }
        pz a5 = qkVar.a().a((xk) xlVar);
        if (a5 != null && a5.b()) {
            taVar.a(a5.a());
        }
        return taVar;
    }

    /* access modifiers changed from: protected */
    public sw a(qk qkVar, xq xqVar, String str, xj xjVar) throws qw {
        qd qdVar;
        if (qkVar.a(ql.CAN_OVERRIDE_ACCESS_MODIFIERS)) {
            xjVar.k();
        }
        afm a = xqVar.j().a(xjVar.c());
        qd qdVar2 = new qd(str, a, xqVar.i(), xjVar);
        afm a2 = a(qkVar, xqVar, a, xjVar, qdVar2);
        if (a2 != a) {
            qdVar = qdVar2.a(a2);
        } else {
            qdVar = qdVar2;
        }
        qu<Object> a3 = a(qkVar, xjVar, qdVar);
        afm a4 = a(qkVar, xjVar, a2, str);
        sw sxVar = new sx(str, a4, (rw) a4.o(), xqVar.i(), xjVar);
        if (a3 != null) {
            sxVar = sxVar.a(a3);
        }
        pz a5 = qkVar.a().a((xk) xjVar);
        if (a5 != null && a5.b()) {
            sxVar.a(a5.a());
        }
        return sxVar;
    }

    /* access modifiers changed from: protected */
    public sw b(qk qkVar, xq xqVar, String str, xl xlVar) throws qw {
        if (qkVar.a(ql.CAN_OVERRIDE_ACCESS_MODIFIERS)) {
            xlVar.k();
        }
        afm a = xlVar.a(xqVar.j());
        qu<Object> a2 = a(qkVar, xlVar, new qd(str, a, xqVar.i(), xlVar));
        afm a3 = a(qkVar, xlVar, a, str);
        tc tcVar = new tc(str, a3, (rw) a3.o(), xqVar.i(), xlVar);
        if (a2 != null) {
            return tcVar.a(a2);
        }
        return tcVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.adz.a(java.lang.Class<?>, boolean):java.lang.String
     arg types: [java.lang.Class<?>, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.adz.a(java.lang.Class<?>, java.lang.Class<?>):java.util.List<java.lang.Class<?>>
      com.flurry.android.monolithic.sdk.impl.adz.a(java.lang.Throwable, java.lang.String):void
      com.flurry.android.monolithic.sdk.impl.adz.a(java.lang.Class<?>, boolean):java.lang.String */
    /* access modifiers changed from: protected */
    public boolean a(Class<?> cls) {
        String a = adz.a(cls);
        if (a != null) {
            throw new IllegalArgumentException("Can not deserialize Class " + cls.getName() + " (of type " + a + ") as a Bean");
        } else if (adz.c(cls)) {
            throw new IllegalArgumentException("Can not deserialize Proxy class " + cls.getName() + " as a Bean");
        } else {
            String a2 = adz.a(cls, true);
            if (a2 == null) {
                return true;
            }
            throw new IllegalArgumentException("Can not deserialize Class " + cls.getName() + " (of type " + a2 + ") as a Bean");
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(qk qkVar, xq xqVar, Class<?> cls, Map<Class<?>, Boolean> map) {
        Boolean bool;
        Boolean bool2 = map.get(cls);
        if (bool2 == null) {
            bool = qkVar.a().e(((xq) qkVar.c(cls)).c());
            if (bool == null) {
                bool = Boolean.FALSE;
            }
        } else {
            bool = bool2;
        }
        return bool.booleanValue();
    }
}
