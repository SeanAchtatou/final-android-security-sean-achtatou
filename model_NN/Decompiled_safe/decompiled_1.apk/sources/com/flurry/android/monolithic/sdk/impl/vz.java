package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;

@rz
public final class vz extends wv<Number> {
    public vz() {
        super(Number.class);
    }

    /* renamed from: b */
    public Number a(ow owVar, qm qmVar) throws IOException, oz {
        pb e = owVar.e();
        if (e == pb.VALUE_NUMBER_INT) {
            if (qmVar.a(ql.USE_BIG_INTEGER_FOR_INTS)) {
                return owVar.v();
            }
            return owVar.p();
        } else if (e == pb.VALUE_NUMBER_FLOAT) {
            if (qmVar.a(ql.USE_BIG_DECIMAL_FOR_FLOATS)) {
                return owVar.y();
            }
            return Double.valueOf(owVar.x());
        } else if (e == pb.VALUE_STRING) {
            String trim = owVar.k().trim();
            try {
                if (trim.indexOf(46) >= 0) {
                    if (qmVar.a(ql.USE_BIG_DECIMAL_FOR_FLOATS)) {
                        return new BigDecimal(trim);
                    }
                    return new Double(trim);
                } else if (qmVar.a(ql.USE_BIG_INTEGER_FOR_INTS)) {
                    return new BigInteger(trim);
                } else {
                    long parseLong = Long.parseLong(trim);
                    if (parseLong > 2147483647L || parseLong < -2147483648L) {
                        return Long.valueOf(parseLong);
                    }
                    return Integer.valueOf((int) parseLong);
                }
            } catch (IllegalArgumentException e2) {
                throw qmVar.b(this.q, "not a valid number");
            }
        } else {
            throw qmVar.a(this.q, e);
        }
    }

    public Object a(ow owVar, qm qmVar, rw rwVar) throws IOException, oz {
        switch (vp.a[owVar.e().ordinal()]) {
            case 1:
            case 2:
            case 3:
                return a(owVar, qmVar);
            default:
                return rwVar.c(owVar, qmVar);
        }
    }
}
