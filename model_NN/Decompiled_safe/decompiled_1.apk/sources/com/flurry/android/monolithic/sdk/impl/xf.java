package com.flurry.android.monolithic.sdk.impl;

import java.util.Collection;
import java.util.Map;

public class xf {
    public static final xf a = new xf();

    protected xf() {
    }

    public ra<?> a(rq rqVar, afm afm) {
        String str;
        Class<?> p = afm.p();
        String name = p.getName();
        if (name.startsWith("org.joda.time.")) {
            str = "com.flurry.org.codehaus.jackson.map.ext.JodaSerializers";
        } else if (name.startsWith("javax.xml.") || c(p, "javax.xml.")) {
            str = "com.flurry.org.codehaus.jackson.map.ext.CoreXMLSerializers";
        } else if (a(p, "org.w3c.dom.Node")) {
            return (ra) a("com.flurry.org.codehaus.jackson.map.ext.DOMSerializer");
        } else {
            return null;
        }
        Object a2 = a(str);
        if (a2 == null) {
            return null;
        }
        Collection<Map.Entry> a3 = ((ael) a2).a();
        for (Map.Entry entry : a3) {
            if (p == entry.getKey()) {
                return (ra) entry.getValue();
            }
        }
        for (Map.Entry entry2 : a3) {
            if (((Class) entry2.getKey()).isAssignableFrom(p)) {
                return (ra) entry2.getValue();
            }
        }
        return null;
    }

    public qu<?> a(afm afm, qk qkVar, qq qqVar) {
        String str;
        Class<?> p = afm.p();
        String name = p.getName();
        if (name.startsWith("org.joda.time.")) {
            str = "com.flurry.org.codehaus.jackson.map.ext.JodaDeserializers";
        } else if (name.startsWith("javax.xml.") || c(p, "javax.xml.")) {
            str = "com.flurry.org.codehaus.jackson.map.ext.CoreXMLDeserializers";
        } else if (a(p, "org.w3c.dom.Node")) {
            return (qu) a("com.flurry.org.codehaus.jackson.map.ext.DOMDeserializer$DocumentDeserializer");
        } else {
            if (a(p, "org.w3c.dom.Node")) {
                return (qu) a("com.flurry.org.codehaus.jackson.map.ext.DOMDeserializer$NodeDeserializer");
            }
            return null;
        }
        Object a2 = a(str);
        if (a2 == null) {
            return null;
        }
        Collection<vo> a3 = ((ael) a2).a();
        for (vo voVar : a3) {
            if (p == voVar.f()) {
                return voVar;
            }
        }
        for (vo voVar2 : a3) {
            if (voVar2.f().isAssignableFrom(p)) {
                return voVar2;
            }
        }
        return null;
    }

    private Object a(String str) {
        try {
            return Class.forName(str).newInstance();
        } catch (Exception | LinkageError e) {
            return null;
        }
    }

    private boolean a(Class<?> cls, String str) {
        for (Class<?> cls2 = cls; cls2 != null; cls2 = cls2.getSuperclass()) {
            if (cls2.getName().equals(str)) {
                return true;
            }
            if (b(cls2, str)) {
                return true;
            }
        }
        return false;
    }

    private boolean b(Class<?> cls, String str) {
        Class<?>[] interfaces = cls.getInterfaces();
        for (Class<?> name : interfaces) {
            if (name.getName().equals(str)) {
                return true;
            }
        }
        for (Class<?> b : interfaces) {
            if (b(b, str)) {
                return true;
            }
        }
        return false;
    }

    private boolean c(Class<?> cls, String str) {
        for (Class<? super Object> superclass = cls.getSuperclass(); superclass != null; superclass = superclass.getSuperclass()) {
            if (superclass.getName().startsWith(str)) {
                return true;
            }
        }
        for (Class<?> cls2 = cls; cls2 != null; cls2 = cls2.getSuperclass()) {
            if (d(cls2, str)) {
                return true;
            }
        }
        return false;
    }

    private boolean d(Class<?> cls, String str) {
        Class<?>[] interfaces = cls.getInterfaces();
        for (Class<?> name : interfaces) {
            if (name.getName().startsWith(str)) {
                return true;
            }
        }
        for (Class<?> d : interfaces) {
            if (d(d, str)) {
                return true;
            }
        }
        return false;
    }
}
