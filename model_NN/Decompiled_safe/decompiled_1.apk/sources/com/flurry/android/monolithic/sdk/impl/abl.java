package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;

@rz
public class abl extends abc<Map<?, ?>> implements rp {
    protected static final afm a = adk.b();
    protected final qc b;
    protected final HashSet<String> c;
    protected final boolean d;
    protected final afm e;
    protected final afm f;
    protected ra<Object> g;
    protected ra<Object> h;
    protected final rx i;
    protected aal j;

    public /* bridge */ /* synthetic */ void a(Object obj, or orVar, ru ruVar) throws IOException, oq {
        a((Map<?, ?>) ((Map) obj), orVar, ruVar);
    }

    public /* bridge */ /* synthetic */ void a(Object obj, or orVar, ru ruVar, rx rxVar) throws IOException, oz {
        a((Map<?, ?>) ((Map) obj), orVar, ruVar, rxVar);
    }

    protected abl() {
        this(null, null, null, false, null, null, null, null);
    }

    protected abl(HashSet<String> hashSet, afm afm, afm afm2, boolean z, rx rxVar, ra<Object> raVar, ra<Object> raVar2, qc qcVar) {
        super(Map.class, false);
        this.b = qcVar;
        this.c = hashSet;
        this.e = afm;
        this.f = afm2;
        this.d = z;
        this.i = rxVar;
        this.g = raVar;
        this.h = raVar2;
        this.j = aal.a();
    }

    public abc<?> a(rx rxVar) {
        abl abl = new abl(this.c, this.e, this.f, this.d, rxVar, this.g, this.h, this.b);
        if (this.h != null) {
            abl.h = this.h;
        }
        return abl;
    }

    public static abl a(String[] strArr, afm afm, boolean z, rx rxVar, qc qcVar, ra<Object> raVar, ra<Object> raVar2) {
        afm g2;
        afm afm2;
        boolean z2;
        HashSet<String> a2 = a(strArr);
        if (afm == null) {
            afm afm3 = a;
            afm2 = afm3;
            g2 = afm3;
        } else {
            afm k = afm.k();
            g2 = afm.g();
            afm2 = k;
        }
        if (!z) {
            z2 = g2 != null && g2.u();
        } else {
            z2 = z;
        }
        return new abl(a2, afm2, g2, z2, rxVar, raVar, raVar2, qcVar);
    }

    private static HashSet<String> a(String[] strArr) {
        if (strArr == null || strArr.length == 0) {
            return null;
        }
        HashSet<String> hashSet = new HashSet<>(strArr.length);
        for (String add : strArr) {
            hashSet.add(add);
        }
        return hashSet;
    }

    public void a(Map<?, ?> map, or orVar, ru ruVar) throws IOException, oq {
        orVar.d();
        if (!map.isEmpty()) {
            if (this.h != null) {
                a(map, orVar, ruVar, this.h);
            } else {
                b(map, orVar, ruVar);
            }
        }
        orVar.e();
    }

    public void a(Map<?, ?> map, or orVar, ru ruVar, rx rxVar) throws IOException, oq {
        rxVar.b(map, orVar);
        if (!map.isEmpty()) {
            if (this.h != null) {
                a(map, orVar, ruVar, this.h);
            } else {
                b(map, orVar, ruVar);
            }
        }
        rxVar.e(map, orVar);
    }

    public void b(Map<?, ?> map, or orVar, ru ruVar) throws IOException, oq {
        boolean z;
        aal aal;
        ra<Object> raVar;
        aal aal2;
        if (this.i != null) {
            c(map, orVar, ruVar);
            return;
        }
        ra<Object> raVar2 = this.g;
        HashSet<String> hashSet = this.c;
        if (!ruVar.a(rr.WRITE_NULL_MAP_VALUES)) {
            z = true;
        } else {
            z = false;
        }
        aal aal3 = this.j;
        aal aal4 = aal3;
        for (Map.Entry next : map.entrySet()) {
            Object value = next.getValue();
            Object key = next.getKey();
            if (key == null) {
                ruVar.c().a(null, orVar, ruVar);
            } else if ((!z || value != null) && (hashSet == null || !hashSet.contains(key))) {
                raVar2.a(key, orVar, ruVar);
            }
            if (value == null) {
                ruVar.a(orVar);
                aal2 = aal4;
            } else {
                Class<?> cls = value.getClass();
                ra<Object> a2 = aal4.a(cls);
                if (a2 == null) {
                    if (this.f.e()) {
                        raVar = a(aal4, ruVar.a(this.f, cls), ruVar);
                    } else {
                        raVar = a(aal4, cls, ruVar);
                    }
                    aal = this.j;
                } else {
                    aal = aal4;
                    raVar = a2;
                }
                try {
                    raVar.a(value, orVar, ruVar);
                    aal2 = aal;
                } catch (Exception e2) {
                    a(ruVar, e2, map, "" + key);
                    aal2 = aal;
                }
            }
            aal4 = aal2;
        }
    }

    /* access modifiers changed from: protected */
    public void a(Map<?, ?> map, or orVar, ru ruVar, ra<Object> raVar) throws IOException, oq {
        ra<Object> raVar2 = this.g;
        HashSet<String> hashSet = this.c;
        rx rxVar = this.i;
        boolean z = !ruVar.a(rr.WRITE_NULL_MAP_VALUES);
        for (Map.Entry next : map.entrySet()) {
            Object value = next.getValue();
            Object key = next.getKey();
            if (key == null) {
                ruVar.c().a(null, orVar, ruVar);
            } else if ((!z || value != null) && (hashSet == null || !hashSet.contains(key))) {
                raVar2.a(key, orVar, ruVar);
            }
            if (value == null) {
                ruVar.a(orVar);
            } else if (rxVar == null) {
                try {
                    raVar.a(value, orVar, ruVar);
                } catch (Exception e2) {
                    a(ruVar, e2, map, "" + key);
                }
            } else {
                raVar.a(value, orVar, ruVar, rxVar);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void c(Map<?, ?> map, or orVar, ru ruVar) throws IOException, oq {
        ra<Object> a2;
        Class<?> cls;
        ra<Object> raVar;
        Class<?> cls2;
        ra<Object> raVar2;
        ra<Object> raVar3 = this.g;
        HashSet<String> hashSet = this.c;
        boolean z = !ruVar.a(rr.WRITE_NULL_MAP_VALUES);
        Class<?> cls3 = null;
        ra<Object> raVar4 = null;
        for (Map.Entry next : map.entrySet()) {
            Object value = next.getValue();
            Object key = next.getKey();
            if (key == null) {
                ruVar.c().a(null, orVar, ruVar);
            } else if ((!z || value != null) && (hashSet == null || !hashSet.contains(key))) {
                raVar3.a(key, orVar, ruVar);
            }
            if (value == null) {
                ruVar.a(orVar);
                cls2 = cls3;
                raVar2 = raVar4;
            } else {
                Class<?> cls4 = value.getClass();
                if (cls4 == cls3) {
                    raVar = raVar4;
                    Class<?> cls5 = cls3;
                    a2 = raVar4;
                    cls = cls5;
                } else {
                    a2 = ruVar.a(cls4, this.b);
                    cls = cls4;
                    raVar = a2;
                }
                try {
                    a2.a(value, orVar, ruVar, this.i);
                    cls2 = cls;
                    raVar2 = raVar;
                } catch (Exception e2) {
                    a(ruVar, e2, map, "" + key);
                    cls2 = cls;
                    raVar2 = raVar;
                }
            }
            raVar4 = raVar2;
            cls3 = cls2;
        }
    }

    public void a(ru ruVar) throws qw {
        if (this.d && this.h == null) {
            this.h = ruVar.a(this.f, this.b);
        }
        if (this.g == null) {
            this.g = ruVar.b(this.e, this.b);
        }
    }

    /* access modifiers changed from: protected */
    public final ra<Object> a(aal aal, Class<?> cls, ru ruVar) throws qw {
        aap a2 = aal.a(cls, ruVar, this.b);
        if (aal != a2.b) {
            this.j = a2.b;
        }
        return a2.a;
    }

    /* access modifiers changed from: protected */
    public final ra<Object> a(aal aal, afm afm, ru ruVar) throws qw {
        aap a2 = aal.a(afm, ruVar, this.b);
        if (aal != a2.b) {
            this.j = a2.b;
        }
        return a2.a;
    }
}
