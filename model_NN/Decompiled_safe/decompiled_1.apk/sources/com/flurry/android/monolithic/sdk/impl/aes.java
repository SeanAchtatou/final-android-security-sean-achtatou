package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public final class aes extends afg {
    public static final aes c = new aes();
    public static final aes d = new aes();

    private aes() {
    }

    public static aes r() {
        return c;
    }

    public static aes s() {
        return d;
    }

    public boolean f() {
        return true;
    }

    public boolean i() {
        return this == c;
    }

    public String m() {
        return this == c ? "true" : "false";
    }

    public double a(double d2) {
        return this == c ? 1.0d : 0.0d;
    }

    public final void a(or orVar, ru ruVar) throws IOException, oz {
        orVar.a(this == c);
    }

    public boolean equals(Object obj) {
        return obj == this;
    }
}
