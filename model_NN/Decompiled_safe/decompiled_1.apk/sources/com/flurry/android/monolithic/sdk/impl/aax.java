package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public class aax extends aaz {
    public aax(aaz aaz) {
        super(aaz);
    }

    public ra<Object> a() {
        return this;
    }

    public boolean b() {
        return true;
    }

    public final void a(Object obj, or orVar, ru ruVar) throws IOException, oq {
        if (this.e != null) {
            c(obj, orVar, ruVar);
        } else {
            b(obj, orVar, ruVar);
        }
    }

    public String toString() {
        return "UnwrappingBeanSerializer for " + c().getName();
    }
}
