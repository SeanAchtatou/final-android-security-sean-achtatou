package com.flurry.android.monolithic.sdk.impl;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.flurry.android.AdCreative;
import com.flurry.android.AdNetworkView;
import com.flurry.android.impl.ads.FlurryAdModule;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public final class dg extends AdNetworkView {
    /* access modifiers changed from: private */
    public static final String a = dg.class.getSimpleName();
    private final String f;
    private final String g;
    private final boolean h;

    public dg(Context context, FlurryAdModule flurryAdModule, m mVar, AdCreative adCreative, Bundle bundle) {
        super(context, flurryAdModule, mVar, adCreative);
        this.f = bundle.getString("com.flurry.admob.MY_AD_UNIT_ID");
        this.g = bundle.getString("com.flurry.admob.MYTEST_AD_DEVICE_ID");
        this.h = bundle.getBoolean("com.flurry.admob.test");
        setFocusable(true);
    }

    private AdSize a(int i, int i2) {
        if (i >= 728 && i2 >= 90) {
            return AdSize.IAB_LEADERBOARD;
        }
        if (i >= 468 && i2 >= 60) {
            return AdSize.IAB_BANNER;
        }
        if (i >= 320 && i2 >= 50) {
            return AdSize.BANNER;
        }
        if (i >= 300 && i2 >= 250) {
            return AdSize.IAB_MRECT;
        }
        ja.a(3, a, "Could not find AdMob AdSize that matches size");
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{com.flurry.android.monolithic.sdk.impl.dg.addView(android.view.View, android.view.ViewGroup$LayoutParams):void}
     arg types: [com.google.ads.AdView, android.widget.RelativeLayout$LayoutParams]
     candidates:
      ClspMth{android.view.ViewGroup.addView(android.view.View, int):void}
      SimpleMethodDetails{com.flurry.android.monolithic.sdk.impl.dg.addView(android.view.View, android.view.ViewGroup$LayoutParams):void} */
    public void initLayout() {
        Context context = getContext();
        int width = getAdCreative().getWidth();
        int height = getAdCreative().getHeight();
        AdSize a2 = a(context, width, height);
        if (a2 == null) {
            ja.a(6, a, "Could not find Admob AdSize that matches {width = " + width + ", height " + height + "}");
            return;
        }
        ja.a(3, a, "Determined Admob AdSize as " + a2 + " that best matches {width = " + width + ", height = " + height + "}");
        AdView adView = new AdView((Activity) context, a2, this.f);
        adView.setAdListener(new di(this));
        setGravity(17);
        addView((View) adView, (ViewGroup.LayoutParams) new RelativeLayout.LayoutParams(a2.getWidthInPixels(context), a2.getHeightInPixels(context)));
        AdRequest adRequest = new AdRequest();
        if (this.h) {
            ja.a(3, a, "Admob AdView set to Test Mode.");
            adRequest.addTestDevice(AdRequest.TEST_EMULATOR);
            if (!TextUtils.isEmpty(this.g)) {
                adRequest.addTestDevice(this.g);
            }
        }
        adView.loadAd(adRequest);
    }

    private AdSize a(Context context, int i, int i2) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int i3 = (int) (((float) displayMetrics.heightPixels) / displayMetrics.density);
        int i4 = (int) (((float) displayMetrics.widthPixels) / displayMetrics.density);
        if (i > 0 && i <= i4) {
            i4 = i;
        }
        if (i2 > 0 && i2 <= i3) {
            i3 = i2;
        }
        return a(i4, i3);
    }
}
