package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.HashMap;

public class to {
    private final tq[] a;
    private final HashMap<String, Integer> b;
    private final String[] c;
    private final afz[] d;

    protected to(tq[] tqVarArr, HashMap<String, Integer> hashMap, String[] strArr, afz[] afzArr) {
        this.a = tqVarArr;
        this.b = hashMap;
        this.c = strArr;
        this.d = afzArr;
    }

    protected to(to toVar) {
        this.a = toVar.a;
        this.b = toVar.b;
        int length = this.a.length;
        this.c = new String[length];
        this.d = new afz[length];
    }

    public to a() {
        return new to(this);
    }

    public boolean a(ow owVar, qm qmVar, String str, Object obj) throws IOException, oz {
        boolean z;
        Integer num = this.b.get(str);
        if (num == null) {
            return false;
        }
        int intValue = num.intValue();
        if (this.a[intValue].a(str)) {
            this.c[intValue] = owVar.k();
            owVar.d();
            z = (obj == null || this.d[intValue] == null) ? false : true;
        } else {
            afz afz = new afz(owVar.a());
            afz.c(owVar);
            this.d[intValue] = afz;
            z = (obj == null || this.c[intValue] == null) ? false : true;
        }
        if (z) {
            a(owVar, qmVar, obj, intValue);
            this.c[intValue] = null;
            this.d[intValue] = null;
        }
        return true;
    }

    public Object a(ow owVar, qm qmVar, Object obj) throws IOException, oz {
        int length = this.a.length;
        for (int i = 0; i < length; i++) {
            if (this.c[i] == null) {
                if (this.d[i] != null) {
                    throw qmVar.b("Missing external type id property '" + this.a[i].a());
                }
            } else if (this.d[i] == null) {
                throw qmVar.b("Missing property '" + this.a[i].b().c() + "' for external type id '" + this.a[i].a());
            } else {
                a(owVar, qmVar, obj, i);
            }
        }
        return obj;
    }

    /* access modifiers changed from: protected */
    public final void a(ow owVar, qm qmVar, Object obj, int i) throws IOException, oz {
        afz afz = new afz(owVar.a());
        afz.b();
        afz.b(this.c[i]);
        ow a2 = this.d[i].a(owVar);
        a2.b();
        afz.c(a2);
        afz.c();
        ow a3 = afz.a(owVar);
        a3.b();
        this.a[i].b().a(a3, qmVar, obj);
    }
}
