package com.flurry.android.monolithic.sdk.impl;

public final class aau {
    protected int a;
    protected Class<?> b;
    protected afm c;
    protected boolean d;

    public aau(Class<?> cls, boolean z) {
        this.b = cls;
        this.c = null;
        this.d = z;
        this.a = a(cls, z);
    }

    public aau(afm afm, boolean z) {
        this.c = afm;
        this.b = null;
        this.d = z;
        this.a = a(afm, z);
    }

    private static final int a(Class<?> cls, boolean z) {
        int hashCode = cls.getName().hashCode();
        if (z) {
            return hashCode + 1;
        }
        return hashCode;
    }

    private static final int a(afm afm, boolean z) {
        int hashCode = afm.hashCode() - 1;
        if (z) {
            return hashCode - 1;
        }
        return hashCode;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.aau.a(java.lang.Class<?>, boolean):int
     arg types: [java.lang.Class<?>, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.aau.a(com.flurry.android.monolithic.sdk.impl.afm, boolean):int
      com.flurry.android.monolithic.sdk.impl.aau.a(java.lang.Class<?>, boolean):int */
    public void a(Class<?> cls) {
        this.c = null;
        this.b = cls;
        this.d = true;
        this.a = a(cls, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.aau.a(java.lang.Class<?>, boolean):int
     arg types: [java.lang.Class<?>, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.aau.a(com.flurry.android.monolithic.sdk.impl.afm, boolean):int
      com.flurry.android.monolithic.sdk.impl.aau.a(java.lang.Class<?>, boolean):int */
    public void b(Class<?> cls) {
        this.c = null;
        this.b = cls;
        this.d = false;
        this.a = a(cls, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.aau.a(com.flurry.android.monolithic.sdk.impl.afm, boolean):int
     arg types: [com.flurry.android.monolithic.sdk.impl.afm, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.aau.a(java.lang.Class<?>, boolean):int
      com.flurry.android.monolithic.sdk.impl.aau.a(com.flurry.android.monolithic.sdk.impl.afm, boolean):int */
    public void a(afm afm) {
        this.c = afm;
        this.b = null;
        this.d = true;
        this.a = a(afm, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.aau.a(com.flurry.android.monolithic.sdk.impl.afm, boolean):int
     arg types: [com.flurry.android.monolithic.sdk.impl.afm, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.aau.a(java.lang.Class<?>, boolean):int
      com.flurry.android.monolithic.sdk.impl.aau.a(com.flurry.android.monolithic.sdk.impl.afm, boolean):int */
    public void b(afm afm) {
        this.c = afm;
        this.b = null;
        this.d = false;
        this.a = a(afm, false);
    }

    public final int hashCode() {
        return this.a;
    }

    public final String toString() {
        if (this.b != null) {
            return "{class: " + this.b.getName() + ", typed? " + this.d + "}";
        }
        return "{type: " + this.c + ", typed? " + this.d + "}";
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        aau aau = (aau) obj;
        if (aau.d != this.d) {
            return false;
        }
        if (this.b != null) {
            return aau.b == this.b;
        }
        return this.c.equals(aau.c);
    }
}
