package com.flurry.android.monolithic.sdk.impl;

import com.flurry.org.codehaus.jackson.annotate.JsonCreator;
import com.flurry.org.codehaus.jackson.annotate.JsonProperty;
import java.io.Serializable;

public class ot implements Serializable {
    public static final ot a = new ot("N/A", -1, -1, -1, -1);
    final long b;
    final long c;
    final int d;
    final int e;
    final Object f;

    public ot(Object obj, long j, int i, int i2) {
        this(obj, -1, j, i, i2);
    }

    @JsonCreator
    public ot(@JsonProperty("sourceRef") Object obj, @JsonProperty("byteOffset") long j, @JsonProperty("charOffset") long j2, @JsonProperty("lineNr") int i, @JsonProperty("columnNr") int i2) {
        this.f = obj;
        this.b = j;
        this.c = j2;
        this.d = i;
        this.e = i2;
    }

    public long a() {
        return this.b;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(80);
        sb.append("[Source: ");
        if (this.f == null) {
            sb.append("UNKNOWN");
        } else {
            sb.append(this.f.toString());
        }
        sb.append("; line: ");
        sb.append(this.d);
        sb.append(", column: ");
        sb.append(this.e);
        sb.append(']');
        return sb.toString();
    }

    public int hashCode() {
        return ((((this.f == null ? 1 : this.f.hashCode()) ^ this.d) + this.e) ^ ((int) this.c)) + ((int) this.b);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof ot)) {
            return false;
        }
        ot otVar = (ot) obj;
        if (this.f == null) {
            if (otVar.f != null) {
                return false;
            }
        } else if (!this.f.equals(otVar.f)) {
            return false;
        }
        return this.d == otVar.d && this.e == otVar.e && this.c == otVar.c && a() == otVar.a();
    }
}
