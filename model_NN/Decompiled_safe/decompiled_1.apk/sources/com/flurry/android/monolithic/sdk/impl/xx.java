package com.flurry.android.monolithic.sdk.impl;

import com.flurry.org.codehaus.jackson.annotate.JacksonAnnotation;
import com.flurry.org.codehaus.jackson.annotate.JsonAnyGetter;
import com.flurry.org.codehaus.jackson.annotate.JsonAnySetter;
import com.flurry.org.codehaus.jackson.annotate.JsonAutoDetect;
import com.flurry.org.codehaus.jackson.annotate.JsonBackReference;
import com.flurry.org.codehaus.jackson.annotate.JsonCreator;
import com.flurry.org.codehaus.jackson.annotate.JsonGetter;
import com.flurry.org.codehaus.jackson.annotate.JsonIgnore;
import com.flurry.org.codehaus.jackson.annotate.JsonIgnoreProperties;
import com.flurry.org.codehaus.jackson.annotate.JsonIgnoreType;
import com.flurry.org.codehaus.jackson.annotate.JsonManagedReference;
import com.flurry.org.codehaus.jackson.annotate.JsonProperty;
import com.flurry.org.codehaus.jackson.annotate.JsonPropertyOrder;
import com.flurry.org.codehaus.jackson.annotate.JsonRawValue;
import com.flurry.org.codehaus.jackson.annotate.JsonSetter;
import com.flurry.org.codehaus.jackson.annotate.JsonSubTypes;
import com.flurry.org.codehaus.jackson.annotate.JsonTypeInfo;
import com.flurry.org.codehaus.jackson.annotate.JsonTypeName;
import com.flurry.org.codehaus.jackson.annotate.JsonUnwrapped;
import com.flurry.org.codehaus.jackson.annotate.JsonValue;
import com.flurry.org.codehaus.jackson.annotate.JsonWriteNullProperties;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

public class xx extends py {
    public boolean a(Annotation annotation) {
        return annotation.annotationType().getAnnotation(JacksonAnnotation.class) != null;
    }

    public String a(Enum<?> enumR) {
        return enumR.name();
    }

    public Boolean a(xh xhVar) {
        sa saVar = (sa) xhVar.a(sa.class);
        if (saVar == null) {
            return null;
        }
        return saVar.a() ? Boolean.TRUE : Boolean.FALSE;
    }

    public String b(xh xhVar) {
        sd sdVar = (sd) xhVar.a(sd.class);
        if (sdVar == null) {
            return null;
        }
        return sdVar.a();
    }

    public String[] c(xh xhVar) {
        JsonIgnoreProperties jsonIgnoreProperties = (JsonIgnoreProperties) xhVar.a(JsonIgnoreProperties.class);
        if (jsonIgnoreProperties == null) {
            return null;
        }
        return jsonIgnoreProperties.value();
    }

    public Boolean d(xh xhVar) {
        JsonIgnoreProperties jsonIgnoreProperties = (JsonIgnoreProperties) xhVar.a(JsonIgnoreProperties.class);
        if (jsonIgnoreProperties == null) {
            return null;
        }
        return Boolean.valueOf(jsonIgnoreProperties.ignoreUnknown());
    }

    public Boolean e(xh xhVar) {
        JsonIgnoreType jsonIgnoreType = (JsonIgnoreType) xhVar.a(JsonIgnoreType.class);
        if (jsonIgnoreType == null) {
            return null;
        }
        return Boolean.valueOf(jsonIgnoreType.value());
    }

    public Object f(xh xhVar) {
        sc scVar = (sc) xhVar.a(sc.class);
        if (scVar != null) {
            String a = scVar.a();
            if (a.length() > 0) {
                return a;
            }
        }
        return null;
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [com.flurry.android.monolithic.sdk.impl.ye<?>, com.flurry.android.monolithic.sdk.impl.ye] */
    public ye<?> a(xh xhVar, ye<?> yeVar) {
        JsonAutoDetect jsonAutoDetect = (JsonAutoDetect) xhVar.a(JsonAutoDetect.class);
        return jsonAutoDetect == null ? yeVar : yeVar.a(jsonAutoDetect);
    }

    public pz a(xk xkVar) {
        JsonManagedReference jsonManagedReference = (JsonManagedReference) xkVar.a(JsonManagedReference.class);
        if (jsonManagedReference != null) {
            return pz.a(jsonManagedReference.value());
        }
        JsonBackReference jsonBackReference = (JsonBackReference) xkVar.a(JsonBackReference.class);
        if (jsonBackReference != null) {
            return pz.b(jsonBackReference.value());
        }
        return null;
    }

    public Boolean b(xk xkVar) {
        JsonUnwrapped jsonUnwrapped = (JsonUnwrapped) xkVar.a(JsonUnwrapped.class);
        if (jsonUnwrapped == null || !jsonUnwrapped.enabled()) {
            return null;
        }
        return Boolean.TRUE;
    }

    public boolean c(xk xkVar) {
        return m(xkVar);
    }

    public Object d(xk xkVar) {
        ry ryVar = (ry) xkVar.a(ry.class);
        if (ryVar == null) {
            return null;
        }
        String a = ryVar.a();
        if (a.length() != 0) {
            return a;
        }
        if (!(xkVar instanceof xl)) {
            return xkVar.d().getName();
        }
        xl xlVar = (xl) xkVar;
        if (xlVar.f() == 0) {
            return xkVar.d().getName();
        }
        return xlVar.a(0).getName();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.xx.a(com.flurry.android.monolithic.sdk.impl.rf<?>, com.flurry.android.monolithic.sdk.impl.xg, com.flurry.android.monolithic.sdk.impl.afm):com.flurry.android.monolithic.sdk.impl.yj<?>
     arg types: [com.flurry.android.monolithic.sdk.impl.rf<?>, com.flurry.android.monolithic.sdk.impl.xh, com.flurry.android.monolithic.sdk.impl.afm]
     candidates:
      com.flurry.android.monolithic.sdk.impl.xx.a(com.flurry.android.monolithic.sdk.impl.rf<?>, com.flurry.android.monolithic.sdk.impl.xh, com.flurry.android.monolithic.sdk.impl.afm):com.flurry.android.monolithic.sdk.impl.yj<?>
      com.flurry.android.monolithic.sdk.impl.xx.a(com.flurry.android.monolithic.sdk.impl.rf<?>, com.flurry.android.monolithic.sdk.impl.xk, com.flurry.android.monolithic.sdk.impl.afm):com.flurry.android.monolithic.sdk.impl.yj<?>
      com.flurry.android.monolithic.sdk.impl.xx.a(com.flurry.android.monolithic.sdk.impl.xg, com.flurry.android.monolithic.sdk.impl.afm, java.lang.String):java.lang.Class<?>
      com.flurry.android.monolithic.sdk.impl.py.a(com.flurry.android.monolithic.sdk.impl.rf<?>, com.flurry.android.monolithic.sdk.impl.xh, com.flurry.android.monolithic.sdk.impl.afm):com.flurry.android.monolithic.sdk.impl.yj<?>
      com.flurry.android.monolithic.sdk.impl.py.a(com.flurry.android.monolithic.sdk.impl.rf<?>, com.flurry.android.monolithic.sdk.impl.xk, com.flurry.android.monolithic.sdk.impl.afm):com.flurry.android.monolithic.sdk.impl.yj<?>
      com.flurry.android.monolithic.sdk.impl.py.a(com.flurry.android.monolithic.sdk.impl.xg, com.flurry.android.monolithic.sdk.impl.afm, java.lang.String):java.lang.Class<?>
      com.flurry.android.monolithic.sdk.impl.xx.a(com.flurry.android.monolithic.sdk.impl.rf<?>, com.flurry.android.monolithic.sdk.impl.xg, com.flurry.android.monolithic.sdk.impl.afm):com.flurry.android.monolithic.sdk.impl.yj<?> */
    public yj<?> a(rf<?> rfVar, xh xhVar, afm afm) {
        return a(rfVar, (xg) xhVar, afm);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.xx.a(com.flurry.android.monolithic.sdk.impl.rf<?>, com.flurry.android.monolithic.sdk.impl.xg, com.flurry.android.monolithic.sdk.impl.afm):com.flurry.android.monolithic.sdk.impl.yj<?>
     arg types: [com.flurry.android.monolithic.sdk.impl.rf<?>, com.flurry.android.monolithic.sdk.impl.xk, com.flurry.android.monolithic.sdk.impl.afm]
     candidates:
      com.flurry.android.monolithic.sdk.impl.xx.a(com.flurry.android.monolithic.sdk.impl.rf<?>, com.flurry.android.monolithic.sdk.impl.xh, com.flurry.android.monolithic.sdk.impl.afm):com.flurry.android.monolithic.sdk.impl.yj<?>
      com.flurry.android.monolithic.sdk.impl.xx.a(com.flurry.android.monolithic.sdk.impl.rf<?>, com.flurry.android.monolithic.sdk.impl.xk, com.flurry.android.monolithic.sdk.impl.afm):com.flurry.android.monolithic.sdk.impl.yj<?>
      com.flurry.android.monolithic.sdk.impl.xx.a(com.flurry.android.monolithic.sdk.impl.xg, com.flurry.android.monolithic.sdk.impl.afm, java.lang.String):java.lang.Class<?>
      com.flurry.android.monolithic.sdk.impl.py.a(com.flurry.android.monolithic.sdk.impl.rf<?>, com.flurry.android.monolithic.sdk.impl.xh, com.flurry.android.monolithic.sdk.impl.afm):com.flurry.android.monolithic.sdk.impl.yj<?>
      com.flurry.android.monolithic.sdk.impl.py.a(com.flurry.android.monolithic.sdk.impl.rf<?>, com.flurry.android.monolithic.sdk.impl.xk, com.flurry.android.monolithic.sdk.impl.afm):com.flurry.android.monolithic.sdk.impl.yj<?>
      com.flurry.android.monolithic.sdk.impl.py.a(com.flurry.android.monolithic.sdk.impl.xg, com.flurry.android.monolithic.sdk.impl.afm, java.lang.String):java.lang.Class<?>
      com.flurry.android.monolithic.sdk.impl.xx.a(com.flurry.android.monolithic.sdk.impl.rf<?>, com.flurry.android.monolithic.sdk.impl.xg, com.flurry.android.monolithic.sdk.impl.afm):com.flurry.android.monolithic.sdk.impl.yj<?> */
    public yj<?> a(rf<?> rfVar, xk xkVar, afm afm) {
        if (afm.f()) {
            return null;
        }
        return a(rfVar, (xg) xkVar, afm);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.xx.a(com.flurry.android.monolithic.sdk.impl.rf<?>, com.flurry.android.monolithic.sdk.impl.xg, com.flurry.android.monolithic.sdk.impl.afm):com.flurry.android.monolithic.sdk.impl.yj<?>
     arg types: [com.flurry.android.monolithic.sdk.impl.rf<?>, com.flurry.android.monolithic.sdk.impl.xk, com.flurry.android.monolithic.sdk.impl.afm]
     candidates:
      com.flurry.android.monolithic.sdk.impl.xx.a(com.flurry.android.monolithic.sdk.impl.rf<?>, com.flurry.android.monolithic.sdk.impl.xh, com.flurry.android.monolithic.sdk.impl.afm):com.flurry.android.monolithic.sdk.impl.yj<?>
      com.flurry.android.monolithic.sdk.impl.xx.a(com.flurry.android.monolithic.sdk.impl.rf<?>, com.flurry.android.monolithic.sdk.impl.xk, com.flurry.android.monolithic.sdk.impl.afm):com.flurry.android.monolithic.sdk.impl.yj<?>
      com.flurry.android.monolithic.sdk.impl.xx.a(com.flurry.android.monolithic.sdk.impl.xg, com.flurry.android.monolithic.sdk.impl.afm, java.lang.String):java.lang.Class<?>
      com.flurry.android.monolithic.sdk.impl.py.a(com.flurry.android.monolithic.sdk.impl.rf<?>, com.flurry.android.monolithic.sdk.impl.xh, com.flurry.android.monolithic.sdk.impl.afm):com.flurry.android.monolithic.sdk.impl.yj<?>
      com.flurry.android.monolithic.sdk.impl.py.a(com.flurry.android.monolithic.sdk.impl.rf<?>, com.flurry.android.monolithic.sdk.impl.xk, com.flurry.android.monolithic.sdk.impl.afm):com.flurry.android.monolithic.sdk.impl.yj<?>
      com.flurry.android.monolithic.sdk.impl.py.a(com.flurry.android.monolithic.sdk.impl.xg, com.flurry.android.monolithic.sdk.impl.afm, java.lang.String):java.lang.Class<?>
      com.flurry.android.monolithic.sdk.impl.xx.a(com.flurry.android.monolithic.sdk.impl.rf<?>, com.flurry.android.monolithic.sdk.impl.xg, com.flurry.android.monolithic.sdk.impl.afm):com.flurry.android.monolithic.sdk.impl.yj<?> */
    public yj<?> b(rf<?> rfVar, xk xkVar, afm afm) {
        if (afm.f()) {
            return a(rfVar, (xg) xkVar, afm);
        }
        throw new IllegalArgumentException("Must call method with a container type (got " + afm + ")");
    }

    public List<yg> a(xg xgVar) {
        JsonSubTypes jsonSubTypes = (JsonSubTypes) xgVar.a(JsonSubTypes.class);
        if (jsonSubTypes == null) {
            return null;
        }
        JsonSubTypes.Type[] value = jsonSubTypes.value();
        ArrayList arrayList = new ArrayList(value.length);
        for (JsonSubTypes.Type type : value) {
            arrayList.add(new yg(type.value(), type.name()));
        }
        return arrayList;
    }

    public String g(xh xhVar) {
        JsonTypeName jsonTypeName = (JsonTypeName) xhVar.a(JsonTypeName.class);
        if (jsonTypeName == null) {
            return null;
        }
        return jsonTypeName.value();
    }

    public boolean a(xl xlVar) {
        return m(xlVar);
    }

    public boolean a(xi xiVar) {
        return m(xiVar);
    }

    public boolean a(xj xjVar) {
        return m(xjVar);
    }

    public Object b(xg xgVar) {
        Class<? extends ra<?>> a;
        se seVar = (se) xgVar.a(se.class);
        if (seVar != null && (a = seVar.a()) != rb.class) {
            return a;
        }
        JsonRawValue jsonRawValue = (JsonRawValue) xgVar.a(JsonRawValue.class);
        if (jsonRawValue == null || !jsonRawValue.value()) {
            return null;
        }
        return new abp(xgVar.d());
    }

    public Class<? extends ra<?>> c(xg xgVar) {
        Class<? extends ra<?>> c;
        se seVar = (se) xgVar.a(se.class);
        if (seVar == null || (c = seVar.c()) == rb.class) {
            return null;
        }
        return c;
    }

    public Class<? extends ra<?>> d(xg xgVar) {
        Class<? extends ra<?>> b;
        se seVar = (se) xgVar.a(se.class);
        if (seVar == null || (b = seVar.b()) == rb.class) {
            return null;
        }
        return b;
    }

    public sf a(xg xgVar, sf sfVar) {
        se seVar = (se) xgVar.a(se.class);
        if (seVar != null) {
            return seVar.h();
        }
        JsonWriteNullProperties jsonWriteNullProperties = (JsonWriteNullProperties) xgVar.a(JsonWriteNullProperties.class);
        if (jsonWriteNullProperties != null) {
            return jsonWriteNullProperties.value() ? sf.ALWAYS : sf.NON_NULL;
        }
        return sfVar;
    }

    public Class<?> e(xg xgVar) {
        Class<?> d;
        se seVar = (se) xgVar.a(se.class);
        if (seVar == null || (d = seVar.d()) == sl.class) {
            return null;
        }
        return d;
    }

    public Class<?> a(xg xgVar, afm afm) {
        Class<?> e;
        se seVar = (se) xgVar.a(se.class);
        if (seVar == null || (e = seVar.e()) == sl.class) {
            return null;
        }
        return e;
    }

    public Class<?> b(xg xgVar, afm afm) {
        Class<?> f;
        se seVar = (se) xgVar.a(se.class);
        if (seVar == null || (f = seVar.f()) == sl.class) {
            return null;
        }
        return f;
    }

    public sg f(xg xgVar) {
        se seVar = (se) xgVar.a(se.class);
        if (seVar == null) {
            return null;
        }
        return seVar.g();
    }

    public Class<?>[] g(xg xgVar) {
        sk skVar = (sk) xgVar.a(sk.class);
        if (skVar == null) {
            return null;
        }
        return skVar.a();
    }

    public String[] h(xh xhVar) {
        JsonPropertyOrder jsonPropertyOrder = (JsonPropertyOrder) xhVar.a(JsonPropertyOrder.class);
        if (jsonPropertyOrder == null) {
            return null;
        }
        return jsonPropertyOrder.value();
    }

    public Boolean i(xh xhVar) {
        JsonPropertyOrder jsonPropertyOrder = (JsonPropertyOrder) xhVar.a(JsonPropertyOrder.class);
        if (jsonPropertyOrder == null) {
            return null;
        }
        return Boolean.valueOf(jsonPropertyOrder.alphabetic());
    }

    public String b(xl xlVar) {
        JsonProperty jsonProperty = (JsonProperty) xlVar.a(JsonProperty.class);
        if (jsonProperty != null) {
            return jsonProperty.value();
        }
        JsonGetter jsonGetter = (JsonGetter) xlVar.a(JsonGetter.class);
        if (jsonGetter != null) {
            return jsonGetter.value();
        }
        if (xlVar.b(se.class) || xlVar.b(sk.class)) {
            return "";
        }
        return null;
    }

    public boolean c(xl xlVar) {
        JsonValue jsonValue = (JsonValue) xlVar.a(JsonValue.class);
        return jsonValue != null && jsonValue.value();
    }

    public String b(xj xjVar) {
        JsonProperty jsonProperty = (JsonProperty) xjVar.a(JsonProperty.class);
        if (jsonProperty != null) {
            return jsonProperty.value();
        }
        if (xjVar.b(se.class) || xjVar.b(sk.class)) {
            return "";
        }
        return null;
    }

    /* renamed from: l */
    public Class<? extends qu<?>> h(xg xgVar) {
        Class<? extends qu<?>> a;
        sb sbVar = (sb) xgVar.a(sb.class);
        if (sbVar == null || (a = sbVar.a()) == qv.class) {
            return null;
        }
        return a;
    }

    public Class<? extends rc> i(xg xgVar) {
        Class<? extends rc> c;
        sb sbVar = (sb) xgVar.a(sb.class);
        if (sbVar == null || (c = sbVar.c()) == rd.class) {
            return null;
        }
        return c;
    }

    public Class<? extends qu<?>> j(xg xgVar) {
        Class<? extends qu<?>> b;
        sb sbVar = (sb) xgVar.a(sb.class);
        if (sbVar == null || (b = sbVar.b()) == qv.class) {
            return null;
        }
        return b;
    }

    public Class<?> a(xg xgVar, afm afm, String str) {
        Class<?> d;
        sb sbVar = (sb) xgVar.a(sb.class);
        if (sbVar == null || (d = sbVar.d()) == sl.class) {
            return null;
        }
        return d;
    }

    public Class<?> b(xg xgVar, afm afm, String str) {
        Class<?> e;
        sb sbVar = (sb) xgVar.a(sb.class);
        if (sbVar == null || (e = sbVar.e()) == sl.class) {
            return null;
        }
        return e;
    }

    public Class<?> c(xg xgVar, afm afm, String str) {
        Class<?> f;
        sb sbVar = (sb) xgVar.a(sb.class);
        if (sbVar == null || (f = sbVar.f()) == sl.class) {
            return null;
        }
        return f;
    }

    public Object j(xh xhVar) {
        sj sjVar = (sj) xhVar.a(sj.class);
        if (sjVar == null) {
            return null;
        }
        return sjVar.a();
    }

    public String d(xl xlVar) {
        JsonProperty jsonProperty = (JsonProperty) xlVar.a(JsonProperty.class);
        if (jsonProperty != null) {
            return jsonProperty.value();
        }
        JsonSetter jsonSetter = (JsonSetter) xlVar.a(JsonSetter.class);
        if (jsonSetter != null) {
            return jsonSetter.value();
        }
        if (xlVar.b(sb.class) || xlVar.b(sk.class) || xlVar.b(JsonBackReference.class) || xlVar.b(JsonManagedReference.class)) {
            return "";
        }
        return null;
    }

    public boolean e(xl xlVar) {
        return xlVar.b(JsonAnySetter.class);
    }

    public boolean f(xl xlVar) {
        return xlVar.b(JsonAnyGetter.class);
    }

    public boolean k(xg xgVar) {
        return xgVar.b(JsonCreator.class);
    }

    public String c(xj xjVar) {
        JsonProperty jsonProperty = (JsonProperty) xjVar.a(JsonProperty.class);
        if (jsonProperty != null) {
            return jsonProperty.value();
        }
        if (xjVar.b(sb.class) || xjVar.b(sk.class) || xjVar.b(JsonBackReference.class) || xjVar.b(JsonManagedReference.class)) {
            return "";
        }
        return null;
    }

    public String a(xn xnVar) {
        JsonProperty jsonProperty;
        if (xnVar == null || (jsonProperty = (JsonProperty) xnVar.a(JsonProperty.class)) == null) {
            return null;
        }
        return jsonProperty.value();
    }

    /* access modifiers changed from: protected */
    public boolean m(xg xgVar) {
        JsonIgnore jsonIgnore = (JsonIgnore) xgVar.a(JsonIgnore.class);
        return jsonIgnore != null && jsonIgnore.value();
    }

    /* JADX WARN: Type inference failed for: r0v7, types: [com.flurry.android.monolithic.sdk.impl.yj<?>, com.flurry.android.monolithic.sdk.impl.yj] */
    /* access modifiers changed from: protected */
    public yj<?> a(rf<?> rfVar, xg xgVar, afm afm) {
        yj b;
        JsonTypeInfo jsonTypeInfo = (JsonTypeInfo) xgVar.a(JsonTypeInfo.class);
        si siVar = (si) xgVar.a(si.class);
        if (siVar != null) {
            if (jsonTypeInfo == null) {
                return null;
            }
            b = rfVar.d(xgVar, siVar.a());
        } else if (jsonTypeInfo == null) {
            return null;
        } else {
            if (jsonTypeInfo.use() == JsonTypeInfo.Id.NONE) {
                return c();
            }
            b = b();
        }
        sh shVar = (sh) xgVar.a(sh.class);
        yi e = shVar == null ? null : rfVar.e(xgVar, shVar.a());
        if (e != null) {
            e.a(afm);
        }
        yj a = b.a(jsonTypeInfo.use(), e);
        JsonTypeInfo.As include = jsonTypeInfo.include();
        if (include == JsonTypeInfo.As.EXTERNAL_PROPERTY && (xgVar instanceof xh)) {
            include = JsonTypeInfo.As.PROPERTY;
        }
        yj<?> a2 = a.a(include).a(jsonTypeInfo.property());
        Class<?> defaultImpl = jsonTypeInfo.defaultImpl();
        if (defaultImpl != JsonTypeInfo.None.class) {
            return a2.a(defaultImpl);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public yw b() {
        return new yw();
    }

    /* access modifiers changed from: protected */
    public yw c() {
        return yw.b();
    }
}
