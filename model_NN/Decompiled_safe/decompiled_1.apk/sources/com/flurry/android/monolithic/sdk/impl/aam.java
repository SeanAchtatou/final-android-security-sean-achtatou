package com.flurry.android.monolithic.sdk.impl;

final class aam extends aal {
    private final Class<?> a;
    private final Class<?> b;
    private final ra<Object> c;
    private final ra<Object> d;

    public aam(Class<?> cls, ra<Object> raVar, Class<?> cls2, ra<Object> raVar2) {
        this.a = cls;
        this.c = raVar;
        this.b = cls2;
        this.d = raVar2;
    }

    public ra<Object> a(Class<?> cls) {
        if (cls == this.a) {
            return this.c;
        }
        if (cls == this.b) {
            return this.d;
        }
        return null;
    }

    public aal a(Class<?> cls, ra<Object> raVar) {
        return new aao(new aar[]{new aar(this.a, this.c), new aar(this.b, this.d)});
    }
}
