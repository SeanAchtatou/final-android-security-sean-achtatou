package com.flurry.android.monolithic.sdk.impl;

import com.flurry.android.impl.ads.avro.protocol.v6.AdRequest;
import com.flurry.android.impl.ads.avro.protocol.v6.AdResponse;
import com.flurry.android.impl.ads.avro.protocol.v6.SdkLogRequest;
import com.flurry.android.impl.ads.avro.protocol.v6.SdkLogResponse;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

public class bv {
    private static final String a = bv.class.getSimpleName();
    private static ly b;

    public bv() {
        b = new ly();
    }

    public SdkLogResponse a(SdkLogRequest sdkLogRequest, String str) {
        return (SdkLogResponse) a(sdkLogRequest, str, SdkLogRequest.class, SdkLogResponse.class);
    }

    public AdResponse a(AdRequest adRequest, String str) {
        return (AdResponse) a(adRequest, str, AdRequest.class, AdResponse.class);
    }

    private <A extends nu, B extends nu> B a(A a2, String str, Class<A> cls, Class<B> cls2) {
        byte[] a3 = a(a2, cls);
        if (a3 != null) {
            return a(a(a3, str), cls2);
        }
        ja.a(6, a, "Response in bytes is null");
        return null;
    }

    /* access modifiers changed from: package-private */
    public <A extends nu> byte[] a(nu nuVar, Class cls) {
        if (nuVar == null) {
            ja.a(6, a, "convertRequestToByte: Request is null");
            return null;
        }
        ns nsVar = new ns(cls);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        lr b2 = md.a().b(byteArrayOutputStream, null);
        try {
            nsVar.a(nuVar, b2);
            b2.flush();
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            ja.a(6, a, "Error generating adlog request" + e.getMessage());
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public <A extends nu> A a(byte[] bArr, Class cls) {
        if (bArr != null) {
            return b(bArr, cls);
        }
        ja.a(6, a, "convertBytesToResponse: bytes array is null");
        return null;
    }

    private byte[] a(byte[] bArr, String str) {
        ByteArrayEntity byteArrayEntity = new ByteArrayEntity(bArr);
        byteArrayEntity.setContentType("avro/binary");
        ja.a(4, a, "httpPOSTAvroBytesToUrl: reportUrl = " + str);
        HttpPost httpPost = new HttpPost(str);
        httpPost.setEntity(byteArrayEntity);
        httpPost.setHeader("accept", "avro/binary");
        httpPost.setHeader("FM-Checksum", Integer.toString(a(bArr)));
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 10000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 15000);
        httpPost.getParams().setBooleanParameter("http.protocol.expect-continue", false);
        HttpClient a2 = iz.a(basicHttpParams);
        try {
            HttpResponse execute = a2.execute(httpPost);
            int statusCode = execute.getStatusLine().getStatusCode();
            if (statusCode != 200 || execute.getEntity() == null || execute.getEntity().getContentLength() == 0) {
                ja.a(6, a, "Request to url = " + str + " failed with HTTP = " + statusCode);
            } else {
                ja.a(4, a, "Request successful");
                byte[] a3 = a(execute.getEntity().getContent());
                byteArrayEntity.consumeContent();
                String num = Integer.toString(a(a3));
                if (!execute.containsHeader("FM-Checksum") || execute.getFirstHeader("FM-Checksum").getValue().equals(num)) {
                    if (a2 != null) {
                        a2.getConnectionManager().shutdown();
                    }
                    return a3;
                }
                ja.a(6, a, "Response was received, but checksum failed.");
            }
            if (a2 != null) {
                a2.getConnectionManager().shutdown();
            }
        } catch (IOException e) {
            ja.a(6, a, "Request to url = " + str + " failed with IOException = " + e.toString(), e);
            if (a2 != null) {
                a2.getConnectionManager().shutdown();
            }
        } catch (Throwable th) {
            if (a2 != null) {
                a2.getConnectionManager().shutdown();
            }
            throw th;
        }
        return null;
    }

    private byte[] a(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[128];
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    /* access modifiers changed from: package-private */
    public int a(byte[] bArr) {
        iv ivVar = new iv();
        ivVar.update(bArr);
        return ivVar.b();
    }

    private <A extends nu> A b(byte[] bArr, Class<A> cls) {
        try {
            return (nu) new nr(cls).a((Object) null, b.a(new ByteArrayInputStream(bArr), (ll) null));
        } catch (ClassCastException e) {
            ja.a(6, a, "ClassCastException in parseAvroBinary:" + e.getMessage());
            ja.a(3, a, "ClassCastException in parseAvroBinary: bytes = " + bArr + " type = " + cls.getSimpleName());
            return null;
        } catch (IOException e2) {
            ja.a(6, a, "IOException in parseAvroBinary:" + e2.getMessage());
            ja.a(3, a, "IOException in parseAvroBinary: bytes = " + bArr + " type = " + cls.getSimpleName());
            return null;
        }
    }
}
