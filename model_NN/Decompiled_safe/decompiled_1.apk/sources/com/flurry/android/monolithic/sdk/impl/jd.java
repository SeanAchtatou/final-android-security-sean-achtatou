package com.flurry.android.monolithic.sdk.impl;

public enum jd {
    NONE_OR_UNKNOWN(0),
    WIFI(1),
    CELL(2);
    
    private int d;

    private jd(int i) {
        this.d = i;
    }

    public int a() {
        return this.d;
    }
}
