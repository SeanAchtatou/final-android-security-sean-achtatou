package com.flurry.android.monolithic.sdk.impl;

import com.flurry.org.codehaus.jackson.annotate.JsonTypeInfo;
import com.ideaworks3d.marmalade.s3eAndroidGooglePlayBilling.util.IabHelper;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@sa
public class sp extends vo<Object> implements ro {
    protected final xh a;
    protected final afm b;
    protected final qc c;
    protected final th d;
    protected qu<Object> e;
    protected final tr f;
    protected boolean g;
    protected final tj h;
    protected final ty[] i;
    protected sv j;
    protected final HashSet<String> k;
    protected final boolean l;
    protected final Map<String, sw> m;
    protected HashMap<adb, qu<Object>> n;
    protected tx o;
    protected to p;

    public sp(qb qbVar, qc qcVar, th thVar, tj tjVar, Map<String, sw> map, HashSet<String> hashSet, boolean z, sv svVar, List<ty> list) {
        this(qbVar.c(), qbVar.a(), qcVar, thVar, tjVar, map, hashSet, z, svVar, list);
    }

    protected sp(xh xhVar, afm afm, qc qcVar, th thVar, tj tjVar, Map<String, sw> map, HashSet<String> hashSet, boolean z, sv svVar, List<ty> list) {
        super(afm);
        this.a = xhVar;
        this.b = afm;
        this.c = qcVar;
        this.d = thVar;
        if (thVar.j()) {
            this.f = new tr(thVar);
        } else {
            this.f = null;
        }
        this.h = tjVar;
        this.m = map;
        this.k = hashSet;
        this.l = z;
        this.j = svVar;
        this.i = (list == null || list.isEmpty()) ? null : (ty[]) list.toArray(new ty[list.size()]);
        this.g = thVar.i() || this.f != null || !thVar.h() || this.o != null;
    }

    protected sp(sp spVar) {
        this(spVar, spVar.l);
    }

    protected sp(sp spVar, boolean z) {
        super(spVar.b);
        this.a = spVar.a;
        this.b = spVar.b;
        this.c = spVar.c;
        this.d = spVar.d;
        this.e = spVar.e;
        this.f = spVar.f;
        this.h = spVar.h;
        this.m = spVar.m;
        this.k = spVar.k;
        this.l = z;
        this.j = spVar.j;
        this.i = spVar.i;
        this.g = spVar.g;
        this.o = spVar.o;
    }

    public qu<Object> a() {
        return getClass() != sp.class ? this : new sp(this, true);
    }

    public final Class<?> d() {
        return this.b.p();
    }

    public sw a(String str) {
        if (this.m == null) {
            return null;
        }
        return this.m.get(str);
    }

    public th e() {
        return this.d;
    }

    public void a(qk qkVar, qq qqVar) throws qw {
        sw swVar;
        tx txVar;
        sw swVar2;
        Iterator<sw> c2 = this.h.c();
        tp tpVar = null;
        tx txVar2 = null;
        while (c2.hasNext()) {
            sw next = c2.next();
            if (!next.f()) {
                swVar = next.a(a(qkVar, qqVar, next.a(), next));
            } else {
                swVar = next;
            }
            sw a2 = a(qkVar, swVar);
            sw b2 = b(qkVar, a2);
            if (b2 != null) {
                if (txVar2 == null) {
                    txVar2 = new tx();
                }
                txVar2.a(b2);
                txVar = txVar2;
                swVar2 = b2;
            } else {
                sw swVar3 = a2;
                txVar = txVar2;
                swVar2 = swVar3;
            }
            sw c3 = c(qkVar, swVar2);
            if (c3 != next) {
                this.h.a(c3);
            }
            if (c3.g()) {
                rw i2 = c3.i();
                if (i2.a() == JsonTypeInfo.As.EXTERNAL_PROPERTY) {
                    if (tpVar == null) {
                        tpVar = new tp();
                    }
                    tpVar.a(c3, i2.b());
                    this.h.b(c3);
                }
            }
            tpVar = tpVar;
            txVar2 = txVar;
        }
        if (this.j != null && !this.j.b()) {
            this.j = this.j.a(a(qkVar, qqVar, this.j.c(), this.j.a()));
        }
        if (this.d.i()) {
            afm l2 = this.d.l();
            if (l2 == null) {
                throw new IllegalArgumentException("Invalid delegate-creator definition for " + this.b + ": value instantiator (" + this.d.getClass().getName() + ") returned true for 'canCreateUsingDelegate()', but null for 'getDelegateType()'");
            }
            this.e = a(qkVar, qqVar, l2, new qd(null, l2, this.a.f(), this.d.o()));
        }
        if (this.f != null) {
            for (sw next2 : this.f.a()) {
                if (!next2.f()) {
                    this.f.a(next2, a(qkVar, qqVar, next2.a(), next2));
                }
            }
        }
        if (tpVar != null) {
            this.p = tpVar.a();
            this.g = true;
        }
        this.o = txVar2;
        if (txVar2 != null) {
            this.g = true;
        }
    }

    /* access modifiers changed from: protected */
    public sw a(qk qkVar, sw swVar) {
        boolean z;
        sw a2;
        String e2 = swVar.e();
        if (e2 == null) {
            return swVar;
        }
        qu<Object> h2 = swVar.h();
        if (h2 instanceof sp) {
            z = false;
            a2 = ((sp) h2).a(e2);
        } else if (h2 instanceof ug) {
            qu<Object> d2 = ((ug) h2).d();
            if (!(d2 instanceof sp)) {
                throw new IllegalArgumentException("Can not handle managed/back reference '" + e2 + "': value deserializer is of type ContainerDeserializerBase, but content type is not handled by a BeanDeserializer " + " (instead it's of type " + d2.getClass().getName() + ")");
            }
            z = true;
            a2 = ((sp) d2).a(e2);
        } else if (h2 instanceof sm) {
            throw new IllegalArgumentException("Can not handle managed/back reference for abstract types (property " + this.b.p().getName() + "." + swVar.c() + ")");
        } else {
            throw new IllegalArgumentException("Can not handle managed/back reference '" + e2 + "': type for value deserializer is not BeanDeserializer or ContainerDeserializerBase, but " + h2.getClass().getName());
        }
        if (a2 == null) {
            throw new IllegalArgumentException("Can not handle managed/back reference '" + e2 + "': no back reference property found from type " + swVar.a());
        }
        afm afm = this.b;
        afm a3 = a2.a();
        if (!a3.p().isAssignableFrom(afm.p())) {
            throw new IllegalArgumentException("Can not handle managed/back reference '" + e2 + "': back reference type (" + a3.p().getName() + ") not compatible with managed type (" + afm.p().getName() + ")");
        }
        return new sz(e2, swVar, a2, this.a.f(), z);
    }

    /* access modifiers changed from: protected */
    public sw b(qk qkVar, sw swVar) {
        qu<Object> h2;
        qu<Object> a2;
        xk b2 = swVar.b();
        if (b2 == null || qkVar.a().b(b2) != Boolean.TRUE || (a2 = (h2 = swVar.h()).a()) == h2 || a2 == null) {
            return null;
        }
        return swVar.a(a2);
    }

    /* access modifiers changed from: protected */
    public sw c(qk qkVar, sw swVar) {
        Class<?> p2;
        Class<?> b2;
        qu<Object> h2 = swVar.h();
        if ((h2 instanceof sp) && !((sp) h2).e().h() && (b2 = adz.b((p2 = swVar.a().p()))) != null && b2 == this.b.p()) {
            for (Constructor<?> constructor : p2.getConstructors()) {
                Class<?>[] parameterTypes = constructor.getParameterTypes();
                if (parameterTypes.length == 1 && parameterTypes[0] == b2) {
                    if (qkVar.a(ql.CAN_OVERRIDE_ACCESS_MODIFIERS)) {
                        adz.a(constructor);
                    }
                    return new sy(swVar, constructor);
                }
            }
        }
        return swVar;
    }

    public final Object a(ow owVar, qm qmVar) throws IOException, oz {
        pb e2 = owVar.e();
        if (e2 == pb.START_OBJECT) {
            owVar.b();
            return b(owVar, qmVar);
        }
        switch (sq.a[e2.ordinal()]) {
            case 1:
                return d(owVar, qmVar);
            case 2:
                return e(owVar, qmVar);
            case 3:
                return f(owVar, qmVar);
            case 4:
                return owVar.z();
            case 5:
            case 6:
                return g(owVar, qmVar);
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED /*7*/:
                return h(owVar, qmVar);
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED /*8*/:
            case 9:
                return b(owVar, qmVar);
            default:
                throw qmVar.b(d());
        }
    }

    public Object a(ow owVar, qm qmVar, Object obj) throws IOException, oz {
        if (this.i != null) {
            a(qmVar, obj);
        }
        if (this.o != null) {
            return b(owVar, qmVar, obj);
        }
        if (this.p != null) {
            return c(owVar, qmVar, obj);
        }
        pb e2 = owVar.e();
        if (e2 == pb.START_OBJECT) {
            e2 = owVar.b();
        }
        while (e2 == pb.FIELD_NAME) {
            String g2 = owVar.g();
            owVar.b();
            sw a2 = this.h.a(g2);
            if (a2 != null) {
                try {
                    a2.a(owVar, qmVar, obj);
                } catch (Exception e3) {
                    a(e3, obj, g2, qmVar);
                }
            } else if (this.k != null && this.k.contains(g2)) {
                owVar.d();
            } else if (this.j != null) {
                this.j.a(owVar, qmVar, obj, g2);
            } else {
                a(owVar, qmVar, obj, g2);
            }
            e2 = owVar.b();
        }
        return obj;
    }

    public Object a(ow owVar, qm qmVar, rw rwVar) throws IOException, oz {
        return rwVar.a(owVar, qmVar);
    }

    public Object b(ow owVar, qm qmVar) throws IOException, oz {
        if (!this.g) {
            Object m2 = this.d.m();
            if (this.i != null) {
                a(qmVar, m2);
            }
            while (owVar.e() != pb.END_OBJECT) {
                String g2 = owVar.g();
                owVar.b();
                sw a2 = this.h.a(g2);
                if (a2 != null) {
                    try {
                        a2.a(owVar, qmVar, m2);
                    } catch (Exception e2) {
                        a(e2, m2, g2, qmVar);
                    }
                } else {
                    b(owVar, qmVar, m2, g2);
                }
                owVar.b();
            }
            return m2;
        } else if (this.o != null) {
            return j(owVar, qmVar);
        } else {
            if (this.p != null) {
                return l(owVar, qmVar);
            }
            return c(owVar, qmVar);
        }
    }

    private final void b(ow owVar, qm qmVar, Object obj, String str) throws IOException, oz {
        if (this.k != null && this.k.contains(str)) {
            owVar.d();
        } else if (this.j != null) {
            try {
                this.j.a(owVar, qmVar, obj, str);
            } catch (Exception e2) {
                a(e2, obj, str, qmVar);
            }
        } else {
            a(owVar, qmVar, obj, str);
        }
    }

    /* access modifiers changed from: protected */
    public Object c(ow owVar, qm qmVar) throws IOException, oz {
        if (this.e != null) {
            return this.d.a(this.e.a(owVar, qmVar));
        }
        if (this.f != null) {
            return i(owVar, qmVar);
        }
        if (this.b.c()) {
            throw qw.a(owVar, "Can not instantiate abstract type " + this.b + " (need to add/enable type information?)");
        }
        throw qw.a(owVar, "No suitable constructor found for type " + this.b + ": can not instantiate from JSON object (need to add/enable type information?)");
    }

    public Object d(ow owVar, qm qmVar) throws IOException, oz {
        if (this.e == null || this.d.c()) {
            return this.d.a(owVar.k());
        }
        Object a2 = this.d.a(this.e.a(owVar, qmVar));
        if (this.i == null) {
            return a2;
        }
        a(qmVar, a2);
        return a2;
    }

    public Object e(ow owVar, qm qmVar) throws IOException, oz {
        switch (owVar.q()) {
            case INT:
                if (this.e == null || this.d.d()) {
                    return this.d.a(owVar.t());
                }
                Object a2 = this.d.a(this.e.a(owVar, qmVar));
                if (this.i == null) {
                    return a2;
                }
                a(qmVar, a2);
                return a2;
            case LONG:
                if (this.e == null || this.d.d()) {
                    return this.d.a(owVar.u());
                }
                Object a3 = this.d.a(this.e.a(owVar, qmVar));
                if (this.i == null) {
                    return a3;
                }
                a(qmVar, a3);
                return a3;
            default:
                if (this.e != null) {
                    Object a4 = this.d.a(this.e.a(owVar, qmVar));
                    if (this.i == null) {
                        return a4;
                    }
                    a(qmVar, a4);
                    return a4;
                }
                throw qmVar.a(d(), "no suitable creator method found to deserialize from JSON integer number");
        }
    }

    public Object f(ow owVar, qm qmVar) throws IOException, oz {
        switch (owVar.q()) {
            case FLOAT:
            case DOUBLE:
                if (this.e == null || this.d.f()) {
                    return this.d.a(owVar.x());
                }
                Object a2 = this.d.a(this.e.a(owVar, qmVar));
                if (this.i == null) {
                    return a2;
                }
                a(qmVar, a2);
                return a2;
            default:
                if (this.e != null) {
                    return this.d.a(this.e.a(owVar, qmVar));
                }
                throw qmVar.a(d(), "no suitable creator method found to deserialize from JSON floating-point number");
        }
    }

    public Object g(ow owVar, qm qmVar) throws IOException, oz {
        if (this.e == null || this.d.g()) {
            return this.d.a(owVar.e() == pb.VALUE_TRUE);
        }
        Object a2 = this.d.a(this.e.a(owVar, qmVar));
        if (this.i == null) {
            return a2;
        }
        a(qmVar, a2);
        return a2;
    }

    public Object h(ow owVar, qm qmVar) throws IOException, oz {
        if (this.e != null) {
            try {
                Object a2 = this.d.a(this.e.a(owVar, qmVar));
                if (this.i != null) {
                    a(qmVar, a2);
                }
                return a2;
            } catch (Exception e2) {
                a(e2, qmVar);
            }
        }
        throw qmVar.b(d());
    }

    /* access modifiers changed from: protected */
    public final Object i(ow owVar, qm qmVar) throws IOException, oz {
        tr trVar = this.f;
        tw a2 = trVar.a(owVar, qmVar);
        pb e2 = owVar.e();
        afz afz = null;
        while (e2 == pb.FIELD_NAME) {
            String g2 = owVar.g();
            owVar.b();
            sw a3 = trVar.a(g2);
            if (a3 != null) {
                if (a2.a(a3.j(), a3.a(owVar, qmVar))) {
                    owVar.b();
                    try {
                        Object a4 = trVar.a(a2);
                        if (a4.getClass() != this.b.p()) {
                            return a(owVar, qmVar, a4, afz);
                        }
                        if (afz != null) {
                            a4 = a(qmVar, a4, afz);
                        }
                        return a(owVar, qmVar, a4);
                    } catch (Exception e3) {
                        a(e3, this.b.p(), g2, qmVar);
                    }
                } else {
                    continue;
                }
            } else {
                sw a5 = this.h.a(g2);
                if (a5 != null) {
                    a2.a(a5, a5.a(owVar, qmVar));
                } else if (this.k != null && this.k.contains(g2)) {
                    owVar.d();
                } else if (this.j != null) {
                    a2.a(this.j, g2, this.j.a(owVar, qmVar));
                } else {
                    if (afz == null) {
                        afz = new afz(owVar.a());
                    }
                    afz.a(g2);
                    afz.c(owVar);
                }
            }
            afz = afz;
            e2 = owVar.b();
        }
        try {
            Object a6 = trVar.a(a2);
            if (afz == null) {
                return a6;
            }
            if (a6.getClass() != this.b.p()) {
                return a((ow) null, qmVar, a6, afz);
            }
            return a(qmVar, a6, afz);
        } catch (Exception e4) {
            a(e4, qmVar);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public Object a(ow owVar, qm qmVar, Object obj, afz afz) throws IOException, oz {
        Object obj2;
        Object obj3;
        qu<Object> b2 = b(qmVar, obj, afz);
        if (b2 != null) {
            if (afz != null) {
                afz.e();
                ow h2 = afz.h();
                h2.b();
                obj3 = b2.a(h2, qmVar, obj);
            } else {
                obj3 = obj;
            }
            if (owVar != null) {
                return b2.a(owVar, qmVar, obj3);
            }
            return obj3;
        }
        if (afz != null) {
            obj2 = a(qmVar, obj, afz);
        } else {
            obj2 = obj;
        }
        if (owVar != null) {
            return a(owVar, qmVar, obj2);
        }
        return obj2;
    }

    /* access modifiers changed from: protected */
    public Object j(ow owVar, qm qmVar) throws IOException, oz {
        if (this.e != null) {
            return this.d.a(this.e.a(owVar, qmVar));
        }
        if (this.f != null) {
            return k(owVar, qmVar);
        }
        afz afz = new afz(owVar.a());
        afz.d();
        Object m2 = this.d.m();
        if (this.i != null) {
            a(qmVar, m2);
        }
        while (owVar.e() != pb.END_OBJECT) {
            String g2 = owVar.g();
            owVar.b();
            sw a2 = this.h.a(g2);
            if (a2 != null) {
                try {
                    a2.a(owVar, qmVar, m2);
                } catch (Exception e2) {
                    a(e2, m2, g2, qmVar);
                }
            } else if (this.k == null || !this.k.contains(g2)) {
                afz.a(g2);
                afz.c(owVar);
                if (this.j != null) {
                    try {
                        this.j.a(owVar, qmVar, m2, g2);
                    } catch (Exception e3) {
                        a(e3, m2, g2, qmVar);
                    }
                }
            } else {
                owVar.d();
            }
            owVar.b();
        }
        afz.e();
        this.o.a(owVar, qmVar, m2, afz);
        return m2;
    }

    /* access modifiers changed from: protected */
    public Object b(ow owVar, qm qmVar, Object obj) throws IOException, oz {
        pb e2 = owVar.e();
        if (e2 == pb.START_OBJECT) {
            e2 = owVar.b();
        }
        afz afz = new afz(owVar.a());
        afz.d();
        while (e2 == pb.FIELD_NAME) {
            String g2 = owVar.g();
            sw a2 = this.h.a(g2);
            owVar.b();
            if (a2 != null) {
                try {
                    a2.a(owVar, qmVar, obj);
                } catch (Exception e3) {
                    a(e3, obj, g2, qmVar);
                }
            } else if (this.k == null || !this.k.contains(g2)) {
                afz.a(g2);
                afz.c(owVar);
                if (this.j != null) {
                    this.j.a(owVar, qmVar, obj, g2);
                }
            } else {
                owVar.d();
            }
            e2 = owVar.b();
        }
        afz.e();
        this.o.a(owVar, qmVar, obj, afz);
        return obj;
    }

    /* access modifiers changed from: protected */
    public Object k(ow owVar, qm qmVar) throws IOException, oz {
        tr trVar = this.f;
        tw a2 = trVar.a(owVar, qmVar);
        afz afz = new afz(owVar.a());
        afz.d();
        pb e2 = owVar.e();
        while (e2 == pb.FIELD_NAME) {
            String g2 = owVar.g();
            owVar.b();
            sw a3 = trVar.a(g2);
            if (a3 != null) {
                if (a2.a(a3.j(), a3.a(owVar, qmVar))) {
                    pb b2 = owVar.b();
                    try {
                        Object a4 = trVar.a(a2);
                        pb pbVar = b2;
                        while (pbVar == pb.FIELD_NAME) {
                            owVar.b();
                            afz.c(owVar);
                            pbVar = owVar.b();
                        }
                        afz.e();
                        if (a4.getClass() == this.b.p()) {
                            return this.o.a(owVar, qmVar, a4, afz);
                        }
                        throw qmVar.b("Can not create polymorphic instances with unwrapped values");
                    } catch (Exception e3) {
                        a(e3, this.b.p(), g2, qmVar);
                    }
                } else {
                    continue;
                }
            } else {
                sw a5 = this.h.a(g2);
                if (a5 != null) {
                    a2.a(a5, a5.a(owVar, qmVar));
                } else if (this.k == null || !this.k.contains(g2)) {
                    afz.a(g2);
                    afz.c(owVar);
                    if (this.j != null) {
                        a2.a(this.j, g2, this.j.a(owVar, qmVar));
                    }
                } else {
                    owVar.d();
                }
            }
            e2 = owVar.b();
        }
        try {
            return this.o.a(owVar, qmVar, trVar.a(a2), afz);
        } catch (Exception e4) {
            a(e4, qmVar);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public Object l(ow owVar, qm qmVar) throws IOException, oz {
        if (this.f != null) {
            return m(owVar, qmVar);
        }
        return c(owVar, qmVar, this.d.m());
    }

    /* access modifiers changed from: protected */
    public Object c(ow owVar, qm qmVar, Object obj) throws IOException, oz {
        to a2 = this.p.a();
        while (owVar.e() != pb.END_OBJECT) {
            String g2 = owVar.g();
            owVar.b();
            sw a3 = this.h.a(g2);
            if (a3 != null) {
                try {
                    a3.a(owVar, qmVar, obj);
                } catch (Exception e2) {
                    a(e2, obj, g2, qmVar);
                }
            } else if (this.k != null && this.k.contains(g2)) {
                owVar.d();
            } else if (!a2.a(owVar, qmVar, g2, obj)) {
                if (this.j != null) {
                    try {
                        this.j.a(owVar, qmVar, obj, g2);
                    } catch (Exception e3) {
                        a(e3, obj, g2, qmVar);
                    }
                } else {
                    a(owVar, qmVar, obj, g2);
                }
            }
            owVar.b();
        }
        return a2.a(owVar, qmVar, obj);
    }

    /* access modifiers changed from: protected */
    public Object m(ow owVar, qm qmVar) throws IOException, oz {
        to a2 = this.p.a();
        tr trVar = this.f;
        tw a3 = trVar.a(owVar, qmVar);
        afz afz = new afz(owVar.a());
        afz.d();
        pb e2 = owVar.e();
        while (e2 == pb.FIELD_NAME) {
            String g2 = owVar.g();
            owVar.b();
            sw a4 = trVar.a(g2);
            if (a4 != null) {
                if (a3.a(a4.j(), a4.a(owVar, qmVar))) {
                    pb b2 = owVar.b();
                    try {
                        Object a5 = trVar.a(a3);
                        pb pbVar = b2;
                        while (pbVar == pb.FIELD_NAME) {
                            owVar.b();
                            afz.c(owVar);
                            pbVar = owVar.b();
                        }
                        if (a5.getClass() == this.b.p()) {
                            return a2.a(owVar, qmVar, a5);
                        }
                        throw qmVar.b("Can not create polymorphic instances with unwrapped values");
                    } catch (Exception e3) {
                        a(e3, this.b.p(), g2, qmVar);
                    }
                } else {
                    continue;
                }
            } else {
                sw a6 = this.h.a(g2);
                if (a6 != null) {
                    a3.a(a6, a6.a(owVar, qmVar));
                } else if (!a2.a(owVar, qmVar, g2, (Object) null)) {
                    if (this.k != null && this.k.contains(g2)) {
                        owVar.d();
                    } else if (this.j != null) {
                        a3.a(this.j, g2, this.j.a(owVar, qmVar));
                    }
                }
            }
            e2 = owVar.b();
        }
        try {
            return a2.a(owVar, qmVar, trVar.a(a3));
        } catch (Exception e4) {
            a(e4, qmVar);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void a(qm qmVar, Object obj) throws IOException, oz {
        for (ty b2 : this.i) {
            b2.b(qmVar, obj);
        }
    }

    /* access modifiers changed from: protected */
    public void a(ow owVar, qm qmVar, Object obj, String str) throws IOException, oz {
        if (this.l || (this.k != null && this.k.contains(str))) {
            owVar.d();
        } else {
            super.a(owVar, qmVar, obj, str);
        }
    }

    /* access modifiers changed from: protected */
    public Object a(qm qmVar, Object obj, afz afz) throws IOException, oz {
        afz.e();
        ow h2 = afz.h();
        while (h2.b() != pb.END_OBJECT) {
            String g2 = h2.g();
            h2.b();
            a(h2, qmVar, obj, g2);
        }
        return obj;
    }

    /* access modifiers changed from: protected */
    public qu<Object> b(qm qmVar, Object obj, afz afz) throws IOException, oz {
        qu<Object> quVar;
        qq b2;
        synchronized (this) {
            quVar = this.n == null ? null : this.n.get(new adb(obj.getClass()));
        }
        if (!(quVar != null || (b2 = qmVar.b()) == null || (quVar = b2.a(qmVar.a(), qmVar.a(obj.getClass()), this.c)) == null)) {
            synchronized (this) {
                if (this.n == null) {
                    this.n = new HashMap<>();
                }
                this.n.put(new adb(obj.getClass()), quVar);
            }
        }
        return quVar;
    }

    public void a(Throwable th, Object obj, String str, qm qmVar) throws IOException {
        Throwable th2 = th;
        while ((th2 instanceof InvocationTargetException) && th2.getCause() != null) {
            th2 = th2.getCause();
        }
        if (th2 instanceof Error) {
            throw ((Error) th2);
        }
        boolean z = qmVar == null || qmVar.a(ql.WRAP_EXCEPTIONS);
        if (th2 instanceof IOException) {
            if (!z || !(th2 instanceof qw)) {
                throw ((IOException) th2);
            }
        } else if (!z && (th2 instanceof RuntimeException)) {
            throw ((RuntimeException) th2);
        }
        throw qw.a(th2, obj, str);
    }

    /* access modifiers changed from: protected */
    public void a(Throwable th, qm qmVar) throws IOException {
        Throwable th2 = th;
        while ((th2 instanceof InvocationTargetException) && th2.getCause() != null) {
            th2 = th2.getCause();
        }
        if (th2 instanceof Error) {
            throw ((Error) th2);
        }
        boolean z = qmVar == null || qmVar.a(ql.WRAP_EXCEPTIONS);
        if (th2 instanceof IOException) {
            throw ((IOException) th2);
        } else if (z || !(th2 instanceof RuntimeException)) {
            throw qmVar.a(this.b.p(), th2);
        } else {
            throw ((RuntimeException) th2);
        }
    }
}
