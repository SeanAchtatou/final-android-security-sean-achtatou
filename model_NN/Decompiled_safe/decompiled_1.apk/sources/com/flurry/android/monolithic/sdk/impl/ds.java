package com.flurry.android.monolithic.sdk.impl;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.LinearLayout;
import com.flurry.android.AdCreative;
import com.flurry.android.AdNetworkView;
import com.flurry.android.impl.ads.FlurryAdModule;
import com.jumptap.adtag.JtAdView;
import com.jumptap.adtag.JtAdWidgetSettings;
import com.jumptap.adtag.JtAdWidgetSettingsFactory;
import com.jumptap.adtag.utils.JtException;

public class ds extends AdNetworkView {
    /* access modifiers changed from: private */
    public static final String a = ds.class.getSimpleName();
    private final String f;
    private final String g;
    private final String h;

    ds(Context context, FlurryAdModule flurryAdModule, m mVar, AdCreative adCreative, Bundle bundle) {
        super(context, flurryAdModule, mVar, adCreative);
        this.f = bundle.getString("com.flurry.jumptap.PUBLISHER_ID");
        this.g = bundle.getString("com.flurry.jumptap.SPOT_ID");
        this.h = bundle.getString("com.flurry.jumptap.SITE_ID");
        setFocusable(true);
    }

    public void initLayout() {
        JtAdView jtAdView;
        Context context = getContext();
        JtAdWidgetSettings createWidgetSettings = JtAdWidgetSettingsFactory.createWidgetSettings();
        createWidgetSettings.setPublisherId(this.f);
        if (!TextUtils.isEmpty(this.g)) {
            createWidgetSettings.setSpotId(this.g);
        }
        if (!TextUtils.isEmpty(this.h)) {
            createWidgetSettings.setSiteId(this.h);
        }
        createWidgetSettings.setApplicationId(il.c(context));
        createWidgetSettings.setApplicationVersion(il.d(context));
        createWidgetSettings.setRefreshPeriod(0);
        createWidgetSettings.setShouldSendLocation(false);
        setGravity(17);
        try {
            JtAdView jtAdView2 = new JtAdView((Activity) context, createWidgetSettings);
            try {
                float f2 = getResources().getDisplayMetrics().density;
                ja.a(3, a, "scale is " + f2);
                jtAdView2.setLayoutParams(new LinearLayout.LayoutParams((int) ((320.0f * f2) + 0.5f), (int) ((f2 * 50.0f) + 0.5f)));
                jtAdView = jtAdView2;
            } catch (JtException e) {
                jtAdView = jtAdView2;
                ja.a(3, a, "Jumptap JtException when creating ad object.");
                jtAdView.setAdViewListener(new dt(this));
                addView(jtAdView);
            }
        } catch (JtException e2) {
            jtAdView = null;
            ja.a(3, a, "Jumptap JtException when creating ad object.");
            jtAdView.setAdViewListener(new dt(this));
            addView(jtAdView);
        }
        jtAdView.setAdViewListener(new dt(this));
        addView(jtAdView);
    }
}
