package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import java.util.List;

public final class cz implements cx {
    private static final String a = cz.class.getSimpleName();

    public boolean a(Context context, db dbVar) {
        boolean z;
        if (dbVar == null) {
            return false;
        }
        String a2 = dbVar.a();
        if (TextUtils.isEmpty(a2)) {
            return false;
        }
        List<String> c = dbVar.c();
        if (c == null) {
            return false;
        }
        boolean z2 = true;
        Bundle e = il.e(context);
        String packageName = context.getPackageName();
        for (String a3 : c) {
            if (!a(a2, packageName, e, a3)) {
                z = false;
            } else {
                z = z2;
            }
            z2 = z;
        }
        return z2;
    }

    private boolean a(String str, String str2, Bundle bundle, String str3) {
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2) || bundle == null || TextUtils.isEmpty(str3)) {
            return false;
        }
        String string = bundle.getString(str3);
        if (TextUtils.isEmpty(string)) {
            ja.b(a, str + ": package=\"" + str2 + "\": AndroidManifest.xml should include meta-data node with android:name=\"" + str3 + "\" and not empty value for android:value");
            return false;
        }
        ja.a(3, a, str + ": package=\"" + str2 + "\": AndroidManifest.xml has meta-data node with android:name=\"" + str3 + "\" and android:value=\"" + string + "\"");
        return true;
    }
}
