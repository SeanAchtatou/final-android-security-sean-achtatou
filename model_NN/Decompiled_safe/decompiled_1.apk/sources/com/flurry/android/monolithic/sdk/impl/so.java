package com.flurry.android.monolithic.sdk.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicReference;

public abstract class so extends qo {
    static final HashMap<adb, qu<Object>> b = tg.a();
    static final HashMap<afm, rc> c = wu.a();
    static final HashMap<String, Class<? extends Map>> d = new HashMap<>();
    static final HashMap<String, Class<? extends Collection>> e = new HashMap<>();
    protected static final HashMap<afm, qu<Object>> f = vd.a();
    protected xf g = xf.a;

    public abstract afm a(qk qkVar, afm afm) throws qw;

    /* access modifiers changed from: protected */
    public abstract qu<?> a(ada ada, qk qkVar, qq qqVar, qc qcVar, rw rwVar, qu<?> quVar) throws qw;

    /* access modifiers changed from: protected */
    public abstract qu<?> a(adc adc, qk qkVar, qq qqVar, xq xqVar, qc qcVar, rw rwVar, qu<?> quVar) throws qw;

    /* access modifiers changed from: protected */
    public abstract qu<?> a(add add, qk qkVar, qq qqVar, xq xqVar, qc qcVar, rw rwVar, qu<?> quVar) throws qw;

    /* access modifiers changed from: protected */
    public abstract qu<?> a(adf adf, qk qkVar, qq qqVar, xq xqVar, qc qcVar, rc rcVar, rw rwVar, qu<?> quVar) throws qw;

    /* access modifiers changed from: protected */
    public abstract qu<?> a(adg adg, qk qkVar, qq qqVar, xq xqVar, qc qcVar, rc rcVar, rw rwVar, qu<?> quVar) throws qw;

    /* access modifiers changed from: protected */
    public abstract qu<?> a(Class<? extends ou> cls, qk qkVar, qc qcVar) throws qw;

    /* access modifiers changed from: protected */
    public abstract qu<?> a(Class<?> cls, qk qkVar, xq xqVar, qc qcVar) throws qw;

    public abstract th a(qk qkVar, xq xqVar) throws qw;

    static {
        Class<TreeSet> cls = TreeSet.class;
        Class<TreeMap> cls2 = TreeMap.class;
        Class<LinkedList> cls3 = LinkedList.class;
        Class<ArrayList> cls4 = ArrayList.class;
        d.put(Map.class.getName(), LinkedHashMap.class);
        d.put(ConcurrentMap.class.getName(), ConcurrentHashMap.class);
        Class<TreeMap> cls5 = TreeMap.class;
        d.put(SortedMap.class.getName(), cls2);
        Class<TreeMap> cls6 = TreeMap.class;
        d.put("java.util.NavigableMap", cls2);
        try {
            Class<?> cls7 = Class.forName("java.util.ConcurrentNavigableMap");
            d.put(cls7.getName(), Class.forName("java.util.ConcurrentSkipListMap"));
        } catch (ClassNotFoundException e2) {
        }
        Class<ArrayList> cls8 = ArrayList.class;
        e.put(Collection.class.getName(), cls4);
        Class<ArrayList> cls9 = ArrayList.class;
        e.put(List.class.getName(), cls4);
        e.put(Set.class.getName(), HashSet.class);
        Class<TreeSet> cls10 = TreeSet.class;
        e.put(SortedSet.class.getName(), cls);
        Class<LinkedList> cls11 = LinkedList.class;
        e.put(Queue.class.getName(), cls3);
        Class<LinkedList> cls12 = LinkedList.class;
        e.put("java.util.Deque", cls3);
        Class<TreeSet> cls13 = TreeSet.class;
        e.put("java.util.NavigableSet", cls);
    }

    protected so() {
    }

    public qu<?> a(qk qkVar, qq qqVar, ada ada, qc qcVar) throws qw {
        rw rwVar;
        qu<Object> quVar;
        afm g2 = ada.g();
        qu<Object> quVar2 = (qu) g2.n();
        if (quVar2 == null) {
            qu<?> quVar3 = f.get(g2);
            if (quVar3 != null) {
                qu<?> a = a(ada, qkVar, qqVar, qcVar, null, null);
                if (a != null) {
                    return a;
                }
                return quVar3;
            } else if (g2.t()) {
                throw new IllegalArgumentException("Internal error: primitive type (" + ada + ") passed, no array deserializer found");
            }
        }
        rw rwVar2 = (rw) g2.o();
        if (rwVar2 == null) {
            rwVar = b(qkVar, g2, qcVar);
        } else {
            rwVar = rwVar2;
        }
        qu<?> a2 = a(ada, qkVar, qqVar, qcVar, rwVar, quVar2);
        if (a2 != null) {
            return a2;
        }
        if (quVar2 == null) {
            quVar = qqVar.a(qkVar, g2, qcVar);
        } else {
            quVar = quVar2;
        }
        return new vc(ada, quVar, rwVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.so.a(com.flurry.android.monolithic.sdk.impl.add, com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.xq, com.flurry.android.monolithic.sdk.impl.qc, com.flurry.android.monolithic.sdk.impl.rw, com.flurry.android.monolithic.sdk.impl.qu<?>):com.flurry.android.monolithic.sdk.impl.qu<?>
     arg types: [com.flurry.android.monolithic.sdk.impl.add, com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.xq, com.flurry.android.monolithic.sdk.impl.qc, com.flurry.android.monolithic.sdk.impl.rw, com.flurry.android.monolithic.sdk.impl.qu<java.lang.Object>]
     candidates:
      com.flurry.android.monolithic.sdk.impl.so.a(com.flurry.android.monolithic.sdk.impl.adc, com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.xq, com.flurry.android.monolithic.sdk.impl.qc, com.flurry.android.monolithic.sdk.impl.rw, com.flurry.android.monolithic.sdk.impl.qu<?>):com.flurry.android.monolithic.sdk.impl.qu<?>
      com.flurry.android.monolithic.sdk.impl.so.a(com.flurry.android.monolithic.sdk.impl.add, com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.xq, com.flurry.android.monolithic.sdk.impl.qc, com.flurry.android.monolithic.sdk.impl.rw, com.flurry.android.monolithic.sdk.impl.qu<?>):com.flurry.android.monolithic.sdk.impl.qu<?> */
    public qu<?> a(qk qkVar, qq qqVar, add add, qc qcVar) throws qw {
        rw rwVar;
        qu<Object> quVar;
        xq xqVar;
        add add2 = (add) a(qkVar, add);
        Class<?> p = add2.p();
        xq xqVar2 = (xq) qkVar.c(add2);
        qu<?> a = a(qkVar, xqVar2.c(), qcVar);
        if (a != null) {
            return a;
        }
        add add3 = (add) a(qkVar, xqVar2.c(), add2, (String) null);
        afm g2 = add3.g();
        qu<Object> quVar2 = (qu) g2.n();
        rw rwVar2 = (rw) g2.o();
        if (rwVar2 == null) {
            rwVar = b(qkVar, g2, qcVar);
        } else {
            rwVar = rwVar2;
        }
        qu<?> a2 = a(add3, qkVar, qqVar, xqVar2, qcVar, rwVar, (qu<?>) quVar2);
        if (a2 != null) {
            return a2;
        }
        if (quVar2 != null) {
            quVar = quVar2;
        } else if (EnumSet.class.isAssignableFrom(p)) {
            return new ul(g2.p(), b(qkVar, qqVar, g2, qcVar));
        } else {
            quVar = qqVar.a(qkVar, g2, qcVar);
        }
        if (add3.s() || add3.c()) {
            Class cls = e.get(p.getName());
            if (cls == null) {
                throw new IllegalArgumentException("Can not find a deserializer for non-concrete Collection type " + add3);
            }
            add add4 = (add) qkVar.a(add3, cls);
            add3 = add4;
            xqVar = (xq) qkVar.c(add4);
        } else {
            xqVar = xqVar2;
        }
        th a3 = a(qkVar, xqVar);
        if (g2.p() == String.class) {
            return new wx(add3, quVar, a3);
        }
        return new uf(add3, quVar, rwVar, a3);
    }

    public qu<?> a(qk qkVar, qq qqVar, adc adc, qc qcVar) throws qw {
        rw rwVar;
        adc adc2 = (adc) a(qkVar, adc);
        xq xqVar = (xq) qkVar.c(adc2.p());
        qu<?> a = a(qkVar, xqVar.c(), qcVar);
        if (a != null) {
            return a;
        }
        adc adc3 = (adc) a(qkVar, xqVar.c(), adc2, (String) null);
        afm g2 = adc3.g();
        qu quVar = (qu) g2.n();
        rw rwVar2 = (rw) g2.o();
        if (rwVar2 == null) {
            rwVar = b(qkVar, g2, qcVar);
        } else {
            rwVar = rwVar2;
        }
        return a(adc3, qkVar, qqVar, xqVar, qcVar, rwVar, quVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.so.a(com.flurry.android.monolithic.sdk.impl.adg, com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.xq, com.flurry.android.monolithic.sdk.impl.qc, com.flurry.android.monolithic.sdk.impl.rc, com.flurry.android.monolithic.sdk.impl.rw, com.flurry.android.monolithic.sdk.impl.qu<?>):com.flurry.android.monolithic.sdk.impl.qu<?>
     arg types: [com.flurry.android.monolithic.sdk.impl.adg, com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.xq, com.flurry.android.monolithic.sdk.impl.qc, com.flurry.android.monolithic.sdk.impl.rc, com.flurry.android.monolithic.sdk.impl.rw, com.flurry.android.monolithic.sdk.impl.qu<java.lang.Object>]
     candidates:
      com.flurry.android.monolithic.sdk.impl.so.a(com.flurry.android.monolithic.sdk.impl.adf, com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.xq, com.flurry.android.monolithic.sdk.impl.qc, com.flurry.android.monolithic.sdk.impl.rc, com.flurry.android.monolithic.sdk.impl.rw, com.flurry.android.monolithic.sdk.impl.qu<?>):com.flurry.android.monolithic.sdk.impl.qu<?>
      com.flurry.android.monolithic.sdk.impl.so.a(com.flurry.android.monolithic.sdk.impl.adg, com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.xq, com.flurry.android.monolithic.sdk.impl.qc, com.flurry.android.monolithic.sdk.impl.rc, com.flurry.android.monolithic.sdk.impl.rw, com.flurry.android.monolithic.sdk.impl.qu<?>):com.flurry.android.monolithic.sdk.impl.qu<?> */
    public qu<?> a(qk qkVar, qq qqVar, adg adg, qc qcVar) throws qw {
        rc rcVar;
        rw rwVar;
        qu<Object> quVar;
        xq xqVar;
        adg adg2 = (adg) a(qkVar, adg);
        xq xqVar2 = (xq) qkVar.c(adg2);
        qu<?> a = a(qkVar, xqVar2.c(), qcVar);
        if (a != null) {
            return a;
        }
        adg adg3 = (adg) a(qkVar, xqVar2.c(), adg2, (String) null);
        afm k = adg3.k();
        afm g2 = adg3.g();
        qu<Object> quVar2 = (qu) g2.n();
        rc rcVar2 = (rc) k.n();
        if (rcVar2 == null) {
            rcVar = qqVar.c(qkVar, k, qcVar);
        } else {
            rcVar = rcVar2;
        }
        rw rwVar2 = (rw) g2.o();
        if (rwVar2 == null) {
            rwVar = b(qkVar, g2, qcVar);
        } else {
            rwVar = rwVar2;
        }
        qu<?> a2 = a(adg3, qkVar, qqVar, xqVar2, qcVar, rcVar, rwVar, (qu<?>) quVar2);
        if (a2 != null) {
            return a2;
        }
        if (quVar2 == null) {
            quVar = qqVar.a(qkVar, g2, qcVar);
        } else {
            quVar = quVar2;
        }
        Class<?> p = adg3.p();
        if (EnumMap.class.isAssignableFrom(p)) {
            Class<?> p2 = k.p();
            if (p2 != null && p2.isEnum()) {
                return new uk(k.p(), b(qkVar, qqVar, k, qcVar), quVar);
            }
            throw new IllegalArgumentException("Can not construct EnumMap; generic (key) type not available");
        }
        if (adg3.s() || adg3.c()) {
            Class cls = d.get(p.getName());
            if (cls == null) {
                throw new IllegalArgumentException("Can not find a deserializer for non-concrete Map type " + adg3);
            }
            adg adg4 = (adg) qkVar.a(adg3, cls);
            xqVar = (xq) qkVar.c(adg4);
            adg3 = adg4;
        } else {
            xqVar = xqVar2;
        }
        vb vbVar = new vb(adg3, a(qkVar, xqVar), rcVar, quVar, rwVar);
        vbVar.a(qkVar.a().c(xqVar.c()));
        return vbVar;
    }

    public qu<?> a(qk qkVar, qq qqVar, adf adf, qc qcVar) throws qw {
        rc rcVar;
        rw rwVar;
        adf adf2 = (adf) a(qkVar, adf);
        xq xqVar = (xq) qkVar.c(adf2);
        qu<?> a = a(qkVar, xqVar.c(), qcVar);
        if (a != null) {
            return a;
        }
        adf adf3 = (adf) a(qkVar, xqVar.c(), adf2, (String) null);
        afm k = adf3.k();
        afm g2 = adf3.g();
        qu quVar = (qu) g2.n();
        rc rcVar2 = (rc) k.n();
        if (rcVar2 == null) {
            rcVar = qqVar.c(qkVar, k, qcVar);
        } else {
            rcVar = rcVar2;
        }
        rw rwVar2 = (rw) g2.o();
        if (rwVar2 == null) {
            rwVar = b(qkVar, g2, qcVar);
        } else {
            rwVar = rwVar2;
        }
        return a(adf3, qkVar, qqVar, xqVar, qcVar, rcVar, rwVar, quVar);
    }

    public qu<?> b(qk qkVar, qq qqVar, afm afm, qc qcVar) throws qw {
        xq xqVar = (xq) qkVar.c(afm);
        qu<?> a = a(qkVar, xqVar.c(), qcVar);
        if (a != null) {
            return a;
        }
        Class<?> p = afm.p();
        qu<?> a2 = a(p, qkVar, xqVar, qcVar);
        if (a2 != null) {
            return a2;
        }
        for (xl next : xqVar.o()) {
            if (qkVar.a().k(next)) {
                if (next.f() == 1 && next.d().isAssignableFrom(p)) {
                    return ui.a(qkVar, p, next);
                }
                throw new IllegalArgumentException("Unsuitable method (" + next + ") decorated with @JsonCreator (for Enum type " + p.getName() + ")");
            }
        }
        return new ui(a(p, qkVar));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.so.a(java.lang.Class<? extends com.flurry.android.monolithic.sdk.impl.ou>, com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<?>
     arg types: [java.lang.Class<?>, com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qc]
     candidates:
      com.flurry.android.monolithic.sdk.impl.so.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.xg, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<java.lang.Object>
      com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.afm, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.rc
      com.flurry.android.monolithic.sdk.impl.so.a(java.lang.Class<? extends com.flurry.android.monolithic.sdk.impl.ou>, com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<?> */
    public qu<?> c(qk qkVar, qq qqVar, afm afm, qc qcVar) throws qw {
        Class<?> p = afm.p();
        qu<?> a = a((Class<? extends ou>) p, qkVar, qcVar);
        if (a != null) {
            return a;
        }
        return ux.a(p);
    }

    /* access modifiers changed from: protected */
    public qu<Object> d(qk qkVar, qq qqVar, afm afm, qc qcVar) throws qw {
        afm afm2;
        Class<AtomicReference> cls = AtomicReference.class;
        Class<?> p = afm.p();
        qu<Object> quVar = b.get(new adb(p));
        if (quVar != null) {
            return quVar;
        }
        Class<AtomicReference> cls2 = AtomicReference.class;
        if (cls.isAssignableFrom(p)) {
            Class<AtomicReference> cls3 = AtomicReference.class;
            afm[] b2 = qkVar.m().b(afm, cls);
            if (b2 == null || b2.length < 1) {
                afm2 = adk.b();
            } else {
                afm2 = b2[0];
            }
            return new ua(afm2, qcVar);
        }
        qu<?> a = this.g.a(afm, qkVar, qqVar);
        if (a == null) {
            return null;
        }
        return a;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r1v3, types: [com.flurry.android.monolithic.sdk.impl.yj] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.flurry.android.monolithic.sdk.impl.rw b(com.flurry.android.monolithic.sdk.impl.qk r6, com.flurry.android.monolithic.sdk.impl.afm r7, com.flurry.android.monolithic.sdk.impl.qc r8) throws com.flurry.android.monolithic.sdk.impl.qw {
        /*
            r5 = this;
            r3 = 0
            java.lang.Class r0 = r7.p()
            com.flurry.android.monolithic.sdk.impl.qb r0 = r6.c(r0)
            com.flurry.android.monolithic.sdk.impl.xq r0 = (com.flurry.android.monolithic.sdk.impl.xq) r0
            com.flurry.android.monolithic.sdk.impl.xh r0 = r0.c()
            com.flurry.android.monolithic.sdk.impl.py r1 = r6.a()
            com.flurry.android.monolithic.sdk.impl.yj r2 = r1.a(r6, r0, r7)
            if (r2 != 0) goto L_0x0021
            com.flurry.android.monolithic.sdk.impl.yj r0 = r6.d(r7)
            if (r0 != 0) goto L_0x0053
            r0 = r3
        L_0x0020:
            return r0
        L_0x0021:
            com.flurry.android.monolithic.sdk.impl.yh r3 = r6.l()
            java.util.Collection r0 = r3.a(r0, r6, r1)
            r1 = r2
        L_0x002a:
            java.lang.Class r2 = r1.a()
            if (r2 != 0) goto L_0x004e
            boolean r2 = r7.c()
            if (r2 == 0) goto L_0x004e
            com.flurry.android.monolithic.sdk.impl.afm r2 = r5.a(r6, r7)
            if (r2 == 0) goto L_0x004e
            java.lang.Class r3 = r2.p()
            java.lang.Class r4 = r7.p()
            if (r3 == r4) goto L_0x004e
            java.lang.Class r2 = r2.p()
            com.flurry.android.monolithic.sdk.impl.yj r1 = r1.a(r2)
        L_0x004e:
            com.flurry.android.monolithic.sdk.impl.rw r0 = r1.a(r6, r7, r0, r8)
            goto L_0x0020
        L_0x0053:
            r1 = r0
            r0 = r3
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.so.b(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.afm, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.rw");
    }

    public rw a(qk qkVar, afm afm, xk xkVar, qc qcVar) throws qw {
        py a = qkVar.a();
        yj<?> a2 = a.a(qkVar, xkVar, afm);
        if (a2 == null) {
            return b(qkVar, afm, qcVar);
        }
        return a2.a(qkVar, afm, qkVar.l().a(xkVar, qkVar, a), qcVar);
    }

    public rw b(qk qkVar, afm afm, xk xkVar, qc qcVar) throws qw {
        py a = qkVar.a();
        yj<?> b2 = a.b(qkVar, xkVar, afm);
        afm g2 = afm.g();
        if (b2 == null) {
            return b(qkVar, g2, qcVar);
        }
        return b2.a(qkVar, g2, qkVar.l().a(xkVar, qkVar, a), qcVar);
    }

    /* access modifiers changed from: protected */
    public qu<Object> a(qk qkVar, xg xgVar, qc qcVar) throws qw {
        Object h = qkVar.a().h(xgVar);
        if (h != null) {
            return a(qkVar, xgVar, qcVar, h);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public qu<Object> a(qk qkVar, xg xgVar, qc qcVar, Object obj) throws qw {
        if (obj instanceof qu) {
            qu<Object> quVar = (qu) obj;
            if (quVar instanceof qh) {
                return ((qh) quVar).a(qkVar, qcVar);
            }
            return quVar;
        } else if (!(obj instanceof Class)) {
            throw new IllegalStateException("AnnotationIntrospector returned deserializer definition of type " + obj.getClass().getName() + "; expected type JsonDeserializer or Class<JsonDeserializer> instead");
        } else {
            Class cls = (Class) obj;
            if (!qu.class.isAssignableFrom(cls)) {
                throw new IllegalStateException("AnnotationIntrospector returned Class " + cls.getName() + "; expected Class<JsonDeserializer>");
            }
            qu<Object> a = qkVar.a(xgVar, cls);
            return a instanceof qh ? ((qh) a).a(qkVar, qcVar) : a;
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected <T extends com.flurry.android.monolithic.sdk.impl.afm> T a(com.flurry.android.monolithic.sdk.impl.qk r10, com.flurry.android.monolithic.sdk.impl.xg r11, T r12, java.lang.String r13) throws com.flurry.android.monolithic.sdk.impl.qw {
        /*
            r9 = this;
            r7 = 0
            java.lang.String r8 = "): "
            com.flurry.android.monolithic.sdk.impl.py r2 = r10.a()
            java.lang.Class r1 = r2.a(r11, r12, r13)
            if (r1 == 0) goto L_0x0147
            com.flurry.android.monolithic.sdk.impl.afm r1 = r12.f(r1)     // Catch:{ IllegalArgumentException -> 0x0044 }
        L_0x0011:
            boolean r3 = r1.f()
            if (r3 == 0) goto L_0x00da
            com.flurry.android.monolithic.sdk.impl.afm r3 = r1.k()
            java.lang.Class r3 = r2.b(r11, r3, r13)
            if (r3 == 0) goto L_0x0090
            boolean r4 = r1 instanceof com.flurry.android.monolithic.sdk.impl.adf
            if (r4 != 0) goto L_0x0088
            com.flurry.android.monolithic.sdk.impl.qw r2 = new com.flurry.android.monolithic.sdk.impl.qw
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Illegal key-type annotation: type "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r3 = " is not a Map(-like) type"
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
            r2.<init>(r1)
            throw r2
        L_0x0044:
            r2 = move-exception
            com.flurry.android.monolithic.sdk.impl.qw r3 = new com.flurry.android.monolithic.sdk.impl.qw
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Failed to narrow type "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r12)
            java.lang.String r5 = " with concrete-type annotation (value "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r1 = r1.getName()
            java.lang.StringBuilder r1 = r4.append(r1)
            java.lang.String r4 = "), method '"
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r4 = r11.b()
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r4 = "': "
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r4 = r2.getMessage()
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r1 = r1.toString()
            r3.<init>(r1, r7, r2)
            throw r3
        L_0x0088:
            r0 = r1
            com.flurry.android.monolithic.sdk.impl.adf r0 = (com.flurry.android.monolithic.sdk.impl.adf) r0     // Catch:{ IllegalArgumentException -> 0x00db }
            r9 = r0
            com.flurry.android.monolithic.sdk.impl.afm r1 = r9.d(r3)     // Catch:{ IllegalArgumentException -> 0x00db }
        L_0x0090:
            com.flurry.android.monolithic.sdk.impl.afm r3 = r1.k()
            if (r3 == 0) goto L_0x00ad
            java.lang.Object r4 = r3.n()
            if (r4 != 0) goto L_0x00ad
            java.lang.Class r4 = r2.i(r11)
            if (r4 == 0) goto L_0x00ad
            java.lang.Class<com.flurry.android.monolithic.sdk.impl.rd> r5 = com.flurry.android.monolithic.sdk.impl.rd.class
            if (r4 == r5) goto L_0x00ad
            com.flurry.android.monolithic.sdk.impl.rc r4 = r10.b(r11, r4)
            r3.j(r4)
        L_0x00ad:
            com.flurry.android.monolithic.sdk.impl.afm r3 = r1.g()
            java.lang.Class r3 = r2.c(r11, r3, r13)
            if (r3 == 0) goto L_0x00bb
            com.flurry.android.monolithic.sdk.impl.afm r1 = r1.b(r3)     // Catch:{ IllegalArgumentException -> 0x0111 }
        L_0x00bb:
            com.flurry.android.monolithic.sdk.impl.afm r3 = r1.g()
            java.lang.Object r3 = r3.n()
            if (r3 != 0) goto L_0x00da
            java.lang.Class r2 = r2.j(r11)
            if (r2 == 0) goto L_0x00da
            java.lang.Class<com.flurry.android.monolithic.sdk.impl.qv> r3 = com.flurry.android.monolithic.sdk.impl.qv.class
            if (r2 == r3) goto L_0x00da
            com.flurry.android.monolithic.sdk.impl.qu r2 = r10.a(r11, r2)
            com.flurry.android.monolithic.sdk.impl.afm r3 = r1.g()
            r3.j(r2)
        L_0x00da:
            return r1
        L_0x00db:
            r2 = move-exception
            com.flurry.android.monolithic.sdk.impl.qw r4 = new com.flurry.android.monolithic.sdk.impl.qw
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "Failed to narrow key type "
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.StringBuilder r1 = r5.append(r1)
            java.lang.String r5 = " with key-type annotation ("
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r3 = r3.getName()
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r3 = "): "
            java.lang.StringBuilder r1 = r1.append(r8)
            java.lang.String r3 = r2.getMessage()
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
            r4.<init>(r1, r7, r2)
            throw r4
        L_0x0111:
            r2 = move-exception
            com.flurry.android.monolithic.sdk.impl.qw r4 = new com.flurry.android.monolithic.sdk.impl.qw
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "Failed to narrow content type "
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.StringBuilder r1 = r5.append(r1)
            java.lang.String r5 = " with content-type annotation ("
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r3 = r3.getName()
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r3 = "): "
            java.lang.StringBuilder r1 = r1.append(r8)
            java.lang.String r3 = r2.getMessage()
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
            r4.<init>(r1, r7, r2)
            throw r4
        L_0x0147:
            r1 = r12
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.so.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.xg, com.flurry.android.monolithic.sdk.impl.afm, java.lang.String):com.flurry.android.monolithic.sdk.impl.afm");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:27:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.flurry.android.monolithic.sdk.impl.afm a(com.flurry.android.monolithic.sdk.impl.qk r5, com.flurry.android.monolithic.sdk.impl.xq r6, com.flurry.android.monolithic.sdk.impl.afm r7, com.flurry.android.monolithic.sdk.impl.xk r8, com.flurry.android.monolithic.sdk.impl.qc r9) throws com.flurry.android.monolithic.sdk.impl.qw {
        /*
            r4 = this;
            boolean r0 = r7.f()
            if (r0 == 0) goto L_0x0059
            com.flurry.android.monolithic.sdk.impl.py r0 = r5.a()
            com.flurry.android.monolithic.sdk.impl.afm r1 = r7.k()
            if (r1 == 0) goto L_0x0021
            java.lang.Class r2 = r0.i(r8)
            if (r2 == 0) goto L_0x0021
            java.lang.Class<com.flurry.android.monolithic.sdk.impl.rd> r3 = com.flurry.android.monolithic.sdk.impl.rd.class
            if (r2 == r3) goto L_0x0021
            com.flurry.android.monolithic.sdk.impl.rc r2 = r5.b(r8, r2)
            r1.j(r2)
        L_0x0021:
            java.lang.Class r0 = r0.j(r8)
            if (r0 == 0) goto L_0x0036
            java.lang.Class<com.flurry.android.monolithic.sdk.impl.qv> r1 = com.flurry.android.monolithic.sdk.impl.qv.class
            if (r0 == r1) goto L_0x0036
            com.flurry.android.monolithic.sdk.impl.qu r0 = r5.a(r8, r0)
            com.flurry.android.monolithic.sdk.impl.afm r1 = r7.g()
            r1.j(r0)
        L_0x0036:
            boolean r0 = r8 instanceof com.flurry.android.monolithic.sdk.impl.xk
            if (r0 == 0) goto L_0x0059
            com.flurry.android.monolithic.sdk.impl.rw r0 = r4.b(r5, r7, r8, r9)
            if (r0 == 0) goto L_0x0059
            com.flurry.android.monolithic.sdk.impl.afm r0 = r7.e(r0)
        L_0x0044:
            boolean r1 = r8 instanceof com.flurry.android.monolithic.sdk.impl.xk
            if (r1 == 0) goto L_0x0053
            com.flurry.android.monolithic.sdk.impl.rw r1 = r4.a(r5, r0, r8, r9)
        L_0x004c:
            if (r1 == 0) goto L_0x0052
            com.flurry.android.monolithic.sdk.impl.afm r0 = r0.f(r1)
        L_0x0052:
            return r0
        L_0x0053:
            r1 = 0
            com.flurry.android.monolithic.sdk.impl.rw r1 = r4.b(r5, r0, r1)
            goto L_0x004c
        L_0x0059:
            r0 = r7
            goto L_0x0044
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.so.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.xq, com.flurry.android.monolithic.sdk.impl.afm, com.flurry.android.monolithic.sdk.impl.xk, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.afm");
    }

    /* access modifiers changed from: protected */
    public aed<?> a(Class<?> cls, qk qkVar) {
        if (qkVar.a(ql.READ_ENUMS_USING_TO_STRING)) {
            return aed.b(cls);
        }
        return aed.b(cls, qkVar.a());
    }
}
