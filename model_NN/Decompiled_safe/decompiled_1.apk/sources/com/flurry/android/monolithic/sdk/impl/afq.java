package com.flurry.android.monolithic.sdk.impl;

import java.io.OutputStream;
import java.util.Iterator;
import java.util.LinkedList;

public final class afq extends OutputStream {
    private static final byte[] a = new byte[0];
    private final afn b;
    private final LinkedList<byte[]> c;
    private int d;
    private byte[] e;
    private int f;

    public afq() {
        this((afn) null);
    }

    public afq(afn afn) {
        this(afn, 500);
    }

    public afq(int i) {
        this(null, i);
    }

    public afq(afn afn, int i) {
        this.c = new LinkedList<>();
        this.b = afn;
        if (afn == null) {
            this.e = new byte[i];
        } else {
            this.e = afn.a(afo.WRITE_CONCAT_BUFFER);
        }
    }

    public void a() {
        this.d = 0;
        this.f = 0;
        if (!this.c.isEmpty()) {
            this.c.clear();
        }
    }

    public void a(int i) {
        if (this.f >= this.e.length) {
            c();
        }
        byte[] bArr = this.e;
        int i2 = this.f;
        this.f = i2 + 1;
        bArr[i2] = (byte) i;
    }

    public void b(int i) {
        if (this.f + 1 < this.e.length) {
            byte[] bArr = this.e;
            int i2 = this.f;
            this.f = i2 + 1;
            bArr[i2] = (byte) (i >> 8);
            byte[] bArr2 = this.e;
            int i3 = this.f;
            this.f = i3 + 1;
            bArr2[i3] = (byte) i;
            return;
        }
        a(i >> 8);
        a(i);
    }

    public void c(int i) {
        if (this.f + 2 < this.e.length) {
            byte[] bArr = this.e;
            int i2 = this.f;
            this.f = i2 + 1;
            bArr[i2] = (byte) (i >> 16);
            byte[] bArr2 = this.e;
            int i3 = this.f;
            this.f = i3 + 1;
            bArr2[i3] = (byte) (i >> 8);
            byte[] bArr3 = this.e;
            int i4 = this.f;
            this.f = i4 + 1;
            bArr3[i4] = (byte) i;
            return;
        }
        a(i >> 16);
        a(i >> 8);
        a(i);
    }

    public byte[] b() {
        int i = this.f + this.d;
        if (i == 0) {
            return a;
        }
        byte[] bArr = new byte[i];
        Iterator<byte[]> it = this.c.iterator();
        int i2 = 0;
        while (it.hasNext()) {
            byte[] next = it.next();
            int length = next.length;
            System.arraycopy(next, 0, bArr, i2, length);
            i2 += length;
        }
        System.arraycopy(this.e, 0, bArr, i2, this.f);
        int i3 = this.f + i2;
        if (i3 != i) {
            throw new RuntimeException("Internal error: total len assumed to be " + i + ", copied " + i3 + " bytes");
        }
        if (!this.c.isEmpty()) {
            a();
        }
        return bArr;
    }

    public void write(byte[] bArr) {
        write(bArr, 0, bArr.length);
    }

    public void write(byte[] bArr, int i, int i2) {
        int i3 = i2;
        int i4 = i;
        while (true) {
            int min = Math.min(this.e.length - this.f, i3);
            if (min > 0) {
                System.arraycopy(bArr, i4, this.e, this.f, min);
                i4 += min;
                this.f += min;
                i3 -= min;
            }
            if (i3 > 0) {
                c();
            } else {
                return;
            }
        }
    }

    public void write(int i) {
        a(i);
    }

    public void close() {
    }

    public void flush() {
    }

    private void c() {
        this.d += this.e.length;
        int max = Math.max(this.d >> 1, 1000);
        if (max > 262144) {
            max = 262144;
        }
        this.c.add(this.e);
        this.e = new byte[max];
        this.f = 0;
    }
}
