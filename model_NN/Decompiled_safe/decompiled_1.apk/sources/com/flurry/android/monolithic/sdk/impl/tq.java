package com.flurry.android.monolithic.sdk.impl;

final class tq {
    private final sw a;
    private final String b;

    public tq(sw swVar, String str) {
        this.a = swVar;
        this.b = str;
    }

    public boolean a(String str) {
        return str.equals(this.b);
    }

    public String a() {
        return this.b;
    }

    public sw b() {
        return this.a;
    }
}
