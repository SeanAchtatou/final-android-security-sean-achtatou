package com.flurry.android.monolithic.sdk.impl;

import android.os.Looper;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.UUID;

public class ew {
    private static final String d = ew.class.getSimpleName();
    String a;
    long b;
    int c;
    private File e;

    public ew() {
        this.a = null;
        this.b = -1;
        this.c = -1;
        this.e = null;
        this.a = UUID.randomUUID().toString();
        this.e = ia.a().b().getFileStreamPath(".flurrydatasenderblock." + this.a);
    }

    public ew(String str) {
        this.a = null;
        this.b = -1;
        this.c = -1;
        this.e = null;
        this.a = str;
        this.e = ia.a().b().getFileStreamPath(".flurrydatasenderblock." + this.a);
    }

    public String a() {
        return this.a;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x007f, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0080, code lost:
        r1 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0084, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0085, code lost:
        r1 = r2;
        r2 = false;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x007f A[ExcHandler: all (th java.lang.Throwable), Splitter:B:10:0x004c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(byte[] r8) {
        /*
            r7 = this;
            r2 = 6
            r4 = 0
            android.os.Looper r0 = android.os.Looper.myLooper()
            android.os.Looper r1 = android.os.Looper.getMainLooper()
            if (r0 != r1) goto L_0x0013
            java.lang.String r0 = com.flurry.android.monolithic.sdk.impl.ew.d
            java.lang.String r1 = "setData(byte[]) running on the MAIN thread!"
            com.flurry.android.monolithic.sdk.impl.ja.a(r2, r0, r1)
        L_0x0013:
            r0 = 4
            java.lang.String r1 = com.flurry.android.monolithic.sdk.impl.ew.d
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Writing FlurryDataSenderBlockInfo: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.io.File r3 = r7.e
            java.lang.String r3 = r3.getAbsolutePath()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.flurry.android.monolithic.sdk.impl.ja.a(r0, r1, r2)
            r0 = 0
            java.io.File r1 = r7.e     // Catch:{ Throwable -> 0x0065, all -> 0x0077 }
            boolean r1 = com.flurry.android.monolithic.sdk.impl.iw.a(r1)     // Catch:{ Throwable -> 0x0065, all -> 0x0077 }
            if (r1 != 0) goto L_0x0040
            com.flurry.android.monolithic.sdk.impl.je.a(r0)
            r0 = r4
        L_0x003f:
            return r0
        L_0x0040:
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Throwable -> 0x0065, all -> 0x0077 }
            java.io.File r2 = r7.e     // Catch:{ Throwable -> 0x0065, all -> 0x0077 }
            r1.<init>(r2)     // Catch:{ Throwable -> 0x0065, all -> 0x0077 }
            java.io.DataOutputStream r2 = new java.io.DataOutputStream     // Catch:{ Throwable -> 0x0065, all -> 0x0077 }
            r2.<init>(r1)     // Catch:{ Throwable -> 0x0065, all -> 0x0077 }
            int r0 = r8.length     // Catch:{ Throwable -> 0x0084, all -> 0x007f }
            r2.writeShort(r0)     // Catch:{ Throwable -> 0x0084, all -> 0x007f }
            r2.write(r8)     // Catch:{ Throwable -> 0x0084, all -> 0x007f }
            r1 = 0
            r2.writeShort(r1)     // Catch:{ Throwable -> 0x0084, all -> 0x007f }
            r1 = 1
            r7.c = r0     // Catch:{ Throwable -> 0x0088, all -> 0x007f }
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x0088, all -> 0x007f }
            r7.b = r3     // Catch:{ Throwable -> 0x0088, all -> 0x007f }
            com.flurry.android.monolithic.sdk.impl.je.a(r2)
            r0 = r1
            goto L_0x003f
        L_0x0065:
            r1 = move-exception
            r2 = r4
            r6 = r0
            r0 = r1
            r1 = r6
        L_0x006a:
            r3 = 6
            java.lang.String r4 = com.flurry.android.monolithic.sdk.impl.ew.d     // Catch:{ all -> 0x0082 }
            java.lang.String r5 = ""
            com.flurry.android.monolithic.sdk.impl.ja.a(r3, r4, r5, r0)     // Catch:{ all -> 0x0082 }
            com.flurry.android.monolithic.sdk.impl.je.a(r1)
            r0 = r2
            goto L_0x003f
        L_0x0077:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x007b:
            com.flurry.android.monolithic.sdk.impl.je.a(r1)
            throw r0
        L_0x007f:
            r0 = move-exception
            r1 = r2
            goto L_0x007b
        L_0x0082:
            r0 = move-exception
            goto L_0x007b
        L_0x0084:
            r0 = move-exception
            r1 = r2
            r2 = r4
            goto L_0x006a
        L_0x0088:
            r0 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
            goto L_0x006a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.ew.a(byte[]):boolean");
    }

    public byte[] b() {
        DataInputStream dataInputStream;
        byte[] bArr;
        if (Looper.myLooper() == Looper.getMainLooper()) {
            ja.a(6, d, "getData() running on the MAIN thread!");
        }
        if (this.e.exists()) {
            ja.a(4, d, "Reading FlurryDataSenderBlockInfo: " + this.e.getAbsolutePath());
            try {
                dataInputStream = new DataInputStream(new FileInputStream(this.e));
                try {
                    int readUnsignedShort = dataInputStream.readUnsignedShort();
                    if (readUnsignedShort == 0) {
                        je.a(dataInputStream);
                        return null;
                    }
                    byte[] bArr2 = new byte[readUnsignedShort];
                    try {
                        dataInputStream.readFully(bArr2);
                        if (dataInputStream.readUnsignedShort() == 0) {
                        }
                        je.a(dataInputStream);
                        return bArr2;
                    } catch (Throwable th) {
                        Throwable th2 = th;
                        bArr = bArr2;
                        th = th2;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    bArr = null;
                }
            } catch (Throwable th4) {
                th = th4;
                dataInputStream = null;
                je.a(dataInputStream);
                throw th;
            }
        } else {
            ja.a(4, d, "Agent cache file doesn't exist.");
            return null;
        }
    }

    public boolean c() {
        if (this.e.exists()) {
            if (this.e.delete()) {
                ja.a(4, d, "Deleted persistence file");
                this.b = -1;
                this.c = -1;
                return true;
            }
            ja.a(6, d, "Cannot delete persistence file");
        }
        return false;
    }
}
