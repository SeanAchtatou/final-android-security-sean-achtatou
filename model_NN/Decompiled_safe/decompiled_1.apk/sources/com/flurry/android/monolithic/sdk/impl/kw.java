package com.flurry.android.monolithic.sdk.impl;

public class kw implements le, Comparable<kw> {
    private final ji a;
    private final Object[] b;

    public kw(ji jiVar) {
        if (jiVar == null || !kj.RECORD.equals(jiVar.a())) {
            throw new jg("Not a record schema: " + jiVar);
        }
        this.a = jiVar;
        this.b = new Object[jiVar.b().size()];
    }

    public ji a() {
        return this.a;
    }

    public void a(int i, Object obj) {
        this.b[i] = obj;
    }

    public Object a(int i) {
        return this.b[i];
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof kw)) {
            return false;
        }
        kw kwVar = (kw) obj;
        if (!this.a.g().equals(kwVar.a.g())) {
            return false;
        }
        return kq.a().a(this, kwVar, this.a, true) == 0;
    }

    public int hashCode() {
        return kq.a().b(this, this.a);
    }

    /* renamed from: a */
    public int compareTo(kw kwVar) {
        return kq.a().a(this, kwVar, this.a);
    }

    public String toString() {
        return kq.a().a(this);
    }
}
