package com.flurry.android.monolithic.sdk.impl;

import java.util.Iterator;

class kt implements Iterator<T> {
    final /* synthetic */ ks a;
    private int b = 0;

    kt(ks ksVar) {
        this.a = ksVar;
    }

    public boolean hasNext() {
        return this.b < this.a.c;
    }

    public T next() {
        T[] b2 = this.a.d;
        int i = this.b;
        this.b = i + 1;
        return b2[i];
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }
}
