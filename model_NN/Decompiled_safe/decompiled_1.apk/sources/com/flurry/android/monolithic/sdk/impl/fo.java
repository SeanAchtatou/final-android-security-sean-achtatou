package com.flurry.android.monolithic.sdk.impl;

class fo implements fr {
    final /* synthetic */ fr a;
    final /* synthetic */ fm b;

    fo(fm fmVar, fr frVar) {
        this.b = fmVar;
        this.a = frVar;
    }

    public void a(fq fqVar) throws Exception {
        if (fqVar != null && fqVar.a()) {
            ft ftVar = new ft(fqVar.c().getJSONArray("set").getJSONObject(0));
            for (int i = 0; i < ftVar.b().size(); i++) {
                String name = ftVar.b().get(i).getName();
                String value = ftVar.b().get(i).getValue();
                this.b.c.put(name, value);
                go.c().a(name, value, this.b.a, null);
            }
        }
        if (this.a != null) {
            this.a.a(fqVar);
        }
    }
}
