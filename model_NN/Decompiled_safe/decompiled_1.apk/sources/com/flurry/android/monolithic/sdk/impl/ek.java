package com.flurry.android.monolithic.sdk.impl;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Map;

public final class ek {
    private int a;
    private String b;
    private Map<String, String> c;
    private long d;
    private boolean e;
    private boolean f;
    private long g;

    public ek(int i, String str, Map<String, String> map, long j, boolean z) {
        this.a = i;
        this.b = str;
        this.c = map;
        this.d = j;
        this.e = z;
        if (this.e) {
            this.f = false;
        } else {
            this.f = true;
        }
    }

    public boolean a() {
        return this.e;
    }

    public boolean b() {
        return this.f;
    }

    public boolean a(String str) {
        return this.e && this.g == 0 && this.b.equals(str);
    }

    public void a(long j) {
        this.f = true;
        this.g = j - this.d;
        ja.a(3, "FlurryAgent", "Ended event '" + this.b + "' (" + this.d + ") after " + this.g + "ms");
    }

    public void a(Map<String, String> map) {
        if (this.c == null || this.c.size() == 0) {
            this.c = map;
            return;
        }
        for (Map.Entry next : map.entrySet()) {
            if (this.c.containsKey(next.getKey())) {
                this.c.remove(next.getKey());
                this.c.put(next.getKey(), next.getValue());
            } else {
                this.c.put(next.getKey(), next.getValue());
            }
        }
    }

    public Map<String, String> c() {
        return this.c;
    }

    public void b(Map<String, String> map) {
        this.c = map;
    }

    public int d() {
        return e().length;
    }

    public byte[] e() {
        DataOutputStream dataOutputStream;
        Throwable th;
        DataOutputStream dataOutputStream2 = null;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            DataOutputStream dataOutputStream3 = new DataOutputStream(byteArrayOutputStream);
            try {
                dataOutputStream3.writeShort(this.a);
                dataOutputStream3.writeUTF(this.b);
                if (this.c == null) {
                    dataOutputStream3.writeShort(0);
                } else {
                    dataOutputStream3.writeShort(this.c.size());
                    for (Map.Entry next : this.c.entrySet()) {
                        dataOutputStream3.writeUTF(je.a((String) next.getKey()));
                        dataOutputStream3.writeUTF(je.a((String) next.getValue()));
                    }
                }
                dataOutputStream3.writeLong(this.d);
                dataOutputStream3.writeLong(this.g);
                dataOutputStream3.flush();
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                je.a(dataOutputStream3);
                return byteArray;
            } catch (IOException e2) {
                dataOutputStream2 = dataOutputStream3;
                try {
                    byte[] bArr = new byte[0];
                    je.a(dataOutputStream2);
                    return bArr;
                } catch (Throwable th2) {
                    Throwable th3 = th2;
                    dataOutputStream = dataOutputStream2;
                    th = th3;
                    je.a(dataOutputStream);
                    throw th;
                }
            } catch (Throwable th4) {
                th = th4;
                dataOutputStream = dataOutputStream3;
                je.a(dataOutputStream);
                throw th;
            }
        } catch (IOException e3) {
            byte[] bArr2 = new byte[0];
            je.a(dataOutputStream2);
            return bArr2;
        } catch (Throwable th5) {
            Throwable th6 = th5;
            dataOutputStream = null;
            th = th6;
            je.a(dataOutputStream);
            throw th;
        }
    }
}
