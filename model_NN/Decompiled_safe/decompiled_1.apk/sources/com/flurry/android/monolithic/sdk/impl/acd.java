package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
public final class acd extends abw<long[]> {
    public acd() {
        this(null);
    }

    public acd(rx rxVar) {
        super(long[].class, rxVar, null);
    }

    public abc<?> a(rx rxVar) {
        return new acd(rxVar);
    }

    /* renamed from: a */
    public void b(long[] jArr, or orVar, ru ruVar) throws IOException, oq {
        for (long a : jArr) {
            orVar.a(a);
        }
    }
}
