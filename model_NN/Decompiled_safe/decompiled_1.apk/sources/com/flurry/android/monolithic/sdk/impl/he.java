package com.flurry.android.monolithic.sdk.impl;

import com.flurry.android.impl.appcloud.AppCloudModule;
import java.util.HashMap;
import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.message.AbstractHttpMessage;
import org.apache.http.protocol.BasicHttpContext;

public abstract class he extends jf {
    protected static final String b = he.class.getSimpleName();
    protected final HttpHost c = new HttpHost(go.d(), 443, "https");
    protected HttpClient d;
    protected BasicHttpContext e;
    protected fr f;
    protected fs g;
    protected HashMap<String, Object> h;

    public he(HashMap<String, Object> hashMap) {
        this.h = hashMap;
    }

    /* access modifiers changed from: protected */
    public void a(AbstractHttpMessage abstractHttpMessage, HashMap<String, Object> hashMap) {
        abstractHttpMessage.setHeader("Authorization", (String) hashMap.get("auth"));
        abstractHttpMessage.setHeader("APPCLOUD-USER-SESSION", (String) hashMap.get("session"));
    }

    /* access modifiers changed from: package-private */
    public String a(String str) {
        if (AppCloudModule.c()) {
            return go.d() + "/" + str;
        }
        return "https://" + go.d() + "/" + str;
    }

    /* access modifiers changed from: protected */
    public void b(String str) {
        Thread.currentThread().setName(str + " , id = " + Long.valueOf(Thread.currentThread().getId()));
    }
}
