package com.flurry.android.monolithic.sdk.impl;

import com.flurry.android.Constants;

public class lg {
    private static final ThreadLocal<lj> a = new lh();
    private static final ThreadLocal<lk> b = new li();

    public static int a(byte[] bArr, int i, int i2, byte[] bArr2, int i3, int i4) {
        int i5 = i + i2;
        int i6 = i3 + i4;
        int i7 = i3;
        int i8 = i;
        while (i8 < i5 && i7 < i6) {
            byte b2 = bArr[i8] & Constants.UNKNOWN;
            byte b3 = bArr2[i7] & Constants.UNKNOWN;
            if (b2 != b3) {
                return b2 - b3;
            }
            i8++;
            i7++;
        }
        return i2 - i4;
    }

    public static int a(boolean z, byte[] bArr, int i) {
        bArr[i] = z ? (byte) 1 : 0;
        return 1;
    }

    public static int a(int i, byte[] bArr, int i2) {
        int i3;
        int i4;
        int i5 = (i << 1) ^ (i >> 31);
        if ((i5 & -128) != 0) {
            int i6 = i2 + 1;
            bArr[i2] = (byte) ((i5 | 128) & 255);
            int i7 = i5 >>> 7;
            if (i7 > 127) {
                int i8 = i6 + 1;
                bArr[i6] = (byte) ((i7 | 128) & 255);
                int i9 = i7 >>> 7;
                if (i9 > 127) {
                    i6 = i8 + 1;
                    bArr[i8] = (byte) ((i9 | 128) & 255);
                    i7 = i9 >>> 7;
                    if (i7 > 127) {
                        int i10 = i6 + 1;
                        bArr[i6] = (byte) ((i7 | 128) & 255);
                        i3 = i7 >>> 7;
                        i4 = i10;
                    }
                } else {
                    i3 = i9;
                    i4 = i8;
                }
            }
            int i11 = i6;
            i3 = i7;
            i4 = i11;
        } else {
            i3 = i5;
            i4 = i2;
        }
        bArr[i4] = (byte) i3;
        return (i4 + 1) - i2;
    }

    public static int a(long j, byte[] bArr, int i) {
        long j2;
        int i2;
        long j3 = (j >> 63) ^ (j << 1);
        if ((-128 & j3) != 0) {
            int i3 = i + 1;
            bArr[i] = (byte) ((int) ((128 | j3) & 255));
            long j4 = j3 >>> 7;
            if (j4 > 127) {
                int i4 = i3 + 1;
                bArr[i3] = (byte) ((int) ((128 | j4) & 255));
                long j5 = j4 >>> 7;
                if (j5 > 127) {
                    i3 = i4 + 1;
                    bArr[i4] = (byte) ((int) ((128 | j5) & 255));
                    j4 = j5 >>> 7;
                    if (j4 > 127) {
                        i4 = i3 + 1;
                        bArr[i3] = (byte) ((int) ((128 | j4) & 255));
                        j5 = j4 >>> 7;
                        if (j5 > 127) {
                            i3 = i4 + 1;
                            bArr[i4] = (byte) ((int) ((128 | j5) & 255));
                            j4 = j5 >>> 7;
                            if (j4 > 127) {
                                i4 = i3 + 1;
                                bArr[i3] = (byte) ((int) ((128 | j4) & 255));
                                j5 = j4 >>> 7;
                                if (j5 > 127) {
                                    i3 = i4 + 1;
                                    bArr[i4] = (byte) ((int) ((128 | j5) & 255));
                                    j4 = j5 >>> 7;
                                    if (j4 > 127) {
                                        i4 = i3 + 1;
                                        bArr[i3] = (byte) ((int) ((128 | j4) & 255));
                                        j5 = j4 >>> 7;
                                        if (j5 > 127) {
                                            bArr[i4] = (byte) ((int) ((128 | j5) & 255));
                                            j2 = j5 >>> 7;
                                            i2 = i4 + 1;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                j2 = j5;
                i2 = i4;
            }
            int i5 = i3;
            j2 = j4;
            i2 = i5;
        } else {
            j2 = j3;
            i2 = i;
        }
        bArr[i2] = (byte) ((int) j2);
        return (i2 + 1) - i;
    }

    public static int a(float f, byte[] bArr, int i) {
        int floatToRawIntBits = Float.floatToRawIntBits(f);
        bArr[i] = (byte) (floatToRawIntBits & 255);
        bArr[1 + i] = (byte) ((floatToRawIntBits >>> 8) & 255);
        bArr[i + 2] = (byte) ((floatToRawIntBits >>> 16) & 255);
        int i2 = 1 + 1 + 1 + 1;
        bArr[i + 3] = (byte) ((floatToRawIntBits >>> 24) & 255);
        return 4;
    }

    public static int a(double d, byte[] bArr, int i) {
        long doubleToRawLongBits = Double.doubleToRawLongBits(d);
        int i2 = (int) (doubleToRawLongBits & -1);
        int i3 = (int) ((doubleToRawLongBits >>> 32) & -1);
        bArr[i] = (byte) (i2 & 255);
        bArr[i + 4] = (byte) (i3 & 255);
        bArr[i + 5] = (byte) ((i3 >>> 8) & 255);
        bArr[i + 1] = (byte) ((i2 >>> 8) & 255);
        bArr[i + 2] = (byte) ((i2 >>> 16) & 255);
        bArr[i + 6] = (byte) ((i3 >>> 16) & 255);
        bArr[i + 7] = (byte) ((i3 >>> 24) & 255);
        bArr[i + 3] = (byte) ((i2 >>> 24) & 255);
        return 8;
    }
}
