package com.flurry.android.monolithic.sdk.impl;

import java.lang.annotation.Annotation;
import java.util.HashMap;

public final class xp implements ado {
    protected HashMap<Class<? extends Annotation>, Annotation> a;

    public xp() {
    }

    private xp(HashMap<Class<? extends Annotation>, Annotation> hashMap) {
        this.a = hashMap;
    }

    public <A extends Annotation> A a(Class cls) {
        if (this.a == null) {
            return null;
        }
        return (Annotation) this.a.get(cls);
    }

    public static xp a(xp xpVar, xp xpVar2) {
        if (xpVar == null || xpVar.a == null || xpVar.a.isEmpty()) {
            return xpVar2;
        }
        if (xpVar2 == null || xpVar2.a == null || xpVar2.a.isEmpty()) {
            return xpVar;
        }
        HashMap hashMap = new HashMap();
        for (Annotation next : xpVar2.a.values()) {
            hashMap.put(next.annotationType(), next);
        }
        for (Annotation next2 : xpVar.a.values()) {
            hashMap.put(next2.annotationType(), next2);
        }
        return new xp(hashMap);
    }

    public int a() {
        if (this.a == null) {
            return 0;
        }
        return this.a.size();
    }

    public void a(Annotation annotation) {
        if (this.a == null || !this.a.containsKey(annotation.annotationType())) {
            c(annotation);
        }
    }

    public void b(Annotation annotation) {
        c(annotation);
    }

    public String toString() {
        if (this.a == null) {
            return "[null]";
        }
        return this.a.toString();
    }

    /* access modifiers changed from: protected */
    public final void c(Annotation annotation) {
        if (this.a == null) {
            this.a = new HashMap<>();
        }
        this.a.put(annotation.annotationType(), annotation);
    }
}
