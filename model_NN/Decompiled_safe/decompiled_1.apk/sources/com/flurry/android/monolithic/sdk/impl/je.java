package com.flurry.android.monolithic.sdk.impl;

import android.content.Intent;
import android.graphics.Point;
import android.text.TextUtils;
import android.util.Pair;
import java.io.Closeable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

public final class je {
    private static final String a = je.class.getSimpleName();

    public static String a(String str) {
        return a(str, 255);
    }

    public static String a(String str, int i) {
        if (str == null) {
            return "";
        }
        if (str.length() <= i) {
            return str;
        }
        return str.substring(0, i);
    }

    public static String b(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            ja.a(5, a, "Cannot encode '" + str + "'");
            return "";
        }
    }

    public static String c(String str) {
        try {
            return URLDecoder.decode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            ja.a(5, a, "Cannot decode '" + str + "'");
            return "";
        }
    }

    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Throwable th) {
            }
        }
    }

    public static byte[] d(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-1");
            instance.update(str.getBytes(), 0, str.length());
            return instance.digest();
        } catch (NoSuchAlgorithmException e) {
            ja.a(6, a, "Unsupported SHA1: " + e.getMessage());
            return null;
        }
    }

    public static String a(byte[] bArr) {
        StringBuilder sb = new StringBuilder(bArr.length * 2);
        char[] cArr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        for (byte b : bArr) {
            sb.append(cArr[(byte) ((b & 240) >> 4)]);
            sb.append(cArr[(byte) (b & 15)]);
        }
        return sb.toString();
    }

    public static boolean a(long j) {
        if (System.currentTimeMillis() <= j) {
            return true;
        }
        return false;
    }

    public static boolean a(Intent intent) {
        return ia.a().c().queryIntentActivities(intent, 65536).size() > 0;
    }

    public static boolean b(Intent intent) {
        if (intent == null) {
            return false;
        }
        return ia.a().b().getPackageName().equals(intent.resolveActivity(ia.a().c()).getPackageName());
    }

    public static float a() {
        return ia.a().d().density;
    }

    public static int a(int i) {
        return Math.round(((float) i) / ia.a().d().density);
    }

    public static int b(int i) {
        return Math.round(ia.a().d().density * ((float) i));
    }

    public static int b() {
        return ia.a().d().widthPixels;
    }

    public static int c() {
        return ia.a().d().heightPixels;
    }

    public static int d() {
        return a(b());
    }

    public static int e() {
        return a(c());
    }

    public static int f() {
        Point point = new Point(b(), c());
        if (point.x == point.y) {
            return 3;
        }
        if (point.x < point.y) {
            return 1;
        }
        return 2;
    }

    public static Pair<Integer, Integer> g() {
        return Pair.create(Integer.valueOf(d()), Integer.valueOf(e()));
    }

    public static Pair<Integer, Integer> h() {
        int d = d();
        int e = e();
        switch (f()) {
            case 2:
                return Pair.create(Integer.valueOf(e), Integer.valueOf(d));
            default:
                return Pair.create(Integer.valueOf(d), Integer.valueOf(e));
        }
    }

    public static String e(String str) {
        return str.replace("'", "\\'").replace("\\n", "").replace("\\r", "").replace("\\t", "");
    }

    public static Map<String, String> f(String str) {
        HashMap hashMap = new HashMap();
        if (!TextUtils.isEmpty(str)) {
            for (String split : str.split("&")) {
                String[] split2 = split.split("=");
                if (!split2[0].equals("event")) {
                    hashMap.put(c(split2[0]), c(split2[1]));
                }
            }
        }
        return hashMap;
    }
}
