package com.flurry.android.monolithic.sdk.impl;

import java.util.Collection;

public class adc extends adi {
    protected final afm a;

    protected adc(Class<?> cls, afm afm, Object obj, Object obj2) {
        super(cls, afm.hashCode(), obj, obj2);
        this.a = afm;
    }

    /* access modifiers changed from: protected */
    public afm a(Class<?> cls) {
        return new adc(cls, this.a, this.f, this.g);
    }

    public afm b(Class<?> cls) {
        return cls == this.a.p() ? this : new adc(this.d, this.a.f(cls), this.f, this.g);
    }

    public afm c(Class<?> cls) {
        return cls == this.a.p() ? this : new adc(this.d, this.a.h(cls), this.f, this.g);
    }

    /* renamed from: a */
    public adc f(Object obj) {
        return new adc(this.d, this.a, this.f, obj);
    }

    /* renamed from: b */
    public adc e(Object obj) {
        return new adc(this.d, this.a.f(obj), this.f, this.g);
    }

    /* renamed from: c */
    public adc d(Object obj) {
        return new adc(this.d, this.a, obj, this.g);
    }

    public boolean f() {
        return true;
    }

    public boolean i() {
        return true;
    }

    public afm g() {
        return this.a;
    }

    public int h() {
        return 1;
    }

    public afm b(int i) {
        if (i == 0) {
            return this.a;
        }
        return null;
    }

    public String a(int i) {
        if (i == 0) {
            return "E";
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public String a() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.d.getName());
        if (this.a != null) {
            sb.append('<');
            sb.append(this.a.m());
            sb.append('>');
        }
        return sb.toString();
    }

    public boolean a_() {
        return Collection.class.isAssignableFrom(this.d);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        adc adc = (adc) obj;
        return this.d == adc.d && this.a.equals(adc.a);
    }

    public String toString() {
        return "[collection-like type; class " + this.d.getName() + ", contains " + this.a + "]";
    }
}
