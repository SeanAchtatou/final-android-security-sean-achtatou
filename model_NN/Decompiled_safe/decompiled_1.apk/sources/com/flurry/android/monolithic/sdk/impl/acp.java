package com.flurry.android.monolithic.sdk.impl;

import java.io.File;
import java.io.IOException;

public final class acp extends abq<File> {
    public acp() {
        super(File.class);
    }

    public void a(File file, or orVar, ru ruVar) throws IOException, oq {
        orVar.b(file.getAbsolutePath());
    }
}
