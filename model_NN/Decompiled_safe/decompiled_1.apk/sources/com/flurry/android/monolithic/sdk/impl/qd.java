package com.flurry.android.monolithic.sdk.impl;

public class qd implements qc {
    protected final String a;
    protected final afm b;
    protected final xk c;
    protected final ado d;

    public qd(String str, afm afm, ado ado, xk xkVar) {
        this.a = str;
        this.b = afm;
        this.c = xkVar;
        this.d = ado;
    }

    public qd a(afm afm) {
        return new qd(this.a, afm, this.d, this.c);
    }

    public String c() {
        return this.a;
    }

    public afm a() {
        return this.b;
    }

    public xk b() {
        return this.c;
    }
}
