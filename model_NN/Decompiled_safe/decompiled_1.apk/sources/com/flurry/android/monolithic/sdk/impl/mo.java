package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public class mo extends mj {
    static final /* synthetic */ boolean d = (!mo.class.desiredAssertionStatus());
    private final mp e;

    public mo(mq mqVar, mk mkVar, mp mpVar) throws IOException {
        super(mqVar, mkVar);
        this.e = mpVar;
    }

    public final void a(int i) throws IOException {
        while (i < this.c) {
            mq mqVar = this.b[this.c - 1];
            if (mqVar.a == na.TERMINAL) {
                this.e.t();
            } else if (mqVar.a == na.IMPLICIT_ACTION || mqVar.a == na.EXPLICIT_ACTION) {
                this.e.l();
            } else {
                this.c--;
                b(mqVar);
            }
        }
    }

    public final void f() throws IOException {
        int i = this.c;
        mq[] mqVarArr = this.b;
        int i2 = this.c - 1;
        this.c = i2;
        mq mqVar = mqVarArr[i2];
        if (d || mqVar.a == na.REPEATER) {
            b(mqVar);
            a(i);
            return;
        }
        throw new AssertionError();
    }

    public final void d(mq mqVar) throws IOException {
        int i = this.c;
        c(mqVar);
        a(i);
    }
}
