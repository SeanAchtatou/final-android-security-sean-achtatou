package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.EnumSet;

public class ul extends vo<EnumSet<?>> {
    protected final Class<Enum> a;
    protected final qu<Enum<?>> b;

    /* JADX WARN: Type inference failed for: r2v0, types: [java.lang.Class<?>, java.lang.Class<java.lang.Enum>] */
    /* JADX WARN: Type inference failed for: r3v0, types: [com.flurry.android.monolithic.sdk.impl.qu<java.lang.Enum<?>>, com.flurry.android.monolithic.sdk.impl.qu<?>] */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ul(java.lang.Class<?> r2, com.flurry.android.monolithic.sdk.impl.qu<?> r3) {
        /*
            r1 = this;
            java.lang.Class<java.util.EnumSet> r0 = java.util.EnumSet.class
            r1.<init>(r0)
            r1.a = r2
            r1.b = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.ul.<init>(java.lang.Class, com.flurry.android.monolithic.sdk.impl.qu):void");
    }

    /* renamed from: b */
    public EnumSet<?> a(ow owVar, qm qmVar) throws IOException, oz {
        if (!owVar.j()) {
            throw qmVar.b(EnumSet.class);
        }
        EnumSet<?> d = d();
        while (true) {
            pb b2 = owVar.b();
            if (b2 == pb.END_ARRAY) {
                return d;
            }
            if (b2 == pb.VALUE_NULL) {
                throw qmVar.b(this.a);
            }
            d.add(this.b.a(owVar, qmVar));
        }
    }

    public Object a(ow owVar, qm qmVar, rw rwVar) throws IOException, oz {
        return rwVar.b(owVar, qmVar);
    }

    private EnumSet d() {
        return EnumSet.noneOf(this.a);
    }
}
