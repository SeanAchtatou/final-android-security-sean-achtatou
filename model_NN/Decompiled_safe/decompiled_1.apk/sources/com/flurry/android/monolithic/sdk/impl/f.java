package com.flurry.android.monolithic.sdk.impl;

import com.flurry.android.FlurryWallet;
import com.flurry.android.FlurryWalletError;
import com.flurry.android.FlurryWalletHandler;

public final class f implements hz {
    final /* synthetic */ String a;
    final /* synthetic */ FlurryWalletHandler b;

    public f(String str, FlurryWalletHandler flurryWalletHandler) {
        this.a = str;
        this.b = flurryWalletHandler;
    }

    public void a(ft ftVar) {
        FlurryWallet.a(this.a, this.b);
    }

    public void a(hy hyVar) {
        this.b.onError(new FlurryWalletError(hyVar.a(), hyVar.b()));
    }
}
