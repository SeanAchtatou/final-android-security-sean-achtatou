package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public class wz extends sp {
    public wz(sp spVar) {
        super(spVar);
    }

    protected wz(sp spVar, boolean z) {
        super(spVar, z);
    }

    public qu<Object> a() {
        return getClass() != wz.class ? this : new wz(this, true);
    }

    public Object b(ow owVar, qm qmVar) throws IOException, oz {
        Object m;
        Object[] objArr;
        Object obj;
        int i;
        if (this.f != null) {
            return i(owVar, qmVar);
        }
        if (this.e != null) {
            return this.d.a(this.e.a(owVar, qmVar));
        }
        if (this.b.c()) {
            throw qw.a(owVar, "Can not instantiate abstract type " + this.b + " (need to add/enable type information?)");
        }
        boolean c = this.d.c();
        boolean h = this.d.h();
        if (c || h) {
            int i2 = 0;
            Object[] objArr2 = null;
            Object obj2 = null;
            while (owVar.e() != pb.END_OBJECT) {
                String g = owVar.g();
                sw a = this.h.a(g);
                owVar.b();
                if (a != null) {
                    if (obj2 != null) {
                        a.a(owVar, qmVar, obj2);
                        int i3 = i2;
                        objArr = objArr2;
                        obj = obj2;
                        i = i3;
                    } else {
                        if (objArr2 == null) {
                            int b = this.h.b();
                            objArr2 = new Object[(b + b)];
                        }
                        int i4 = i2 + 1;
                        objArr2[i2] = a;
                        objArr2[i4] = a.a(owVar, qmVar);
                        objArr = objArr2;
                        obj = obj2;
                        i = i4 + 1;
                    }
                } else if ("message".equals(g) && c) {
                    Object a2 = this.d.a(owVar.k());
                    if (objArr2 != null) {
                        for (int i5 = 0; i5 < i2; i5 += 2) {
                            ((sw) objArr2[i5]).a(a2, objArr2[i5 + 1]);
                        }
                        i = i2;
                        obj = a2;
                        objArr = null;
                    } else {
                        i = i2;
                        objArr = objArr2;
                        obj = a2;
                    }
                } else if (this.k != null && this.k.contains(g)) {
                    owVar.d();
                    int i6 = i2;
                    objArr = objArr2;
                    obj = obj2;
                    i = i6;
                } else if (this.j != null) {
                    this.j.a(owVar, qmVar, obj2, g);
                    int i7 = i2;
                    objArr = objArr2;
                    obj = obj2;
                    i = i7;
                } else {
                    a(owVar, qmVar, obj2, g);
                    int i8 = i2;
                    objArr = objArr2;
                    obj = obj2;
                    i = i8;
                }
                owVar.b();
                int i9 = i;
                obj2 = obj;
                objArr2 = objArr;
                i2 = i9;
            }
            if (obj2 != null) {
                return obj2;
            }
            if (c) {
                m = this.d.a((String) null);
            } else {
                m = this.d.m();
            }
            if (objArr2 == null) {
                return m;
            }
            for (int i10 = 0; i10 < i2; i10 += 2) {
                ((sw) objArr2[i10]).a(m, objArr2[i10 + 1]);
            }
            return m;
        }
        throw new qw("Can not deserialize Throwable of type " + this.b + " without having a default contructor, a single-String-arg constructor; or explicit @JsonCreator");
    }
}
