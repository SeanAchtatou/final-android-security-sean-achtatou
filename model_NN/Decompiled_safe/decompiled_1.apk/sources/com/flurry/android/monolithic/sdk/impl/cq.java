package com.flurry.android.monolithic.sdk.impl;

import android.annotation.TargetApi;
import android.os.Build;
import android.view.Window;

public final class cq {
    @TargetApi(11)
    public static void a(Window window) {
        if (window != null && Build.VERSION.SDK_INT >= 11) {
            window.setFlags(16777216, 16777216);
        }
    }
}
