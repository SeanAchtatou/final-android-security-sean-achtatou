package com.flurry.android.monolithic.sdk.impl;

import java.util.ArrayList;
import java.util.List;

public class adm {
    final adk a;

    public adm(adk adk) {
        this.a = adk;
    }

    public afm a(String str) throws IllegalArgumentException {
        adn adn = new adn(str.trim());
        afm a2 = a(adn);
        if (!adn.hasMoreTokens()) {
            return a2;
        }
        throw a(adn, "Unexpected tokens after complete type");
    }

    /* access modifiers changed from: protected */
    public afm a(adn adn) throws IllegalArgumentException {
        if (!adn.hasMoreTokens()) {
            throw a(adn, "Unexpected end-of-string");
        }
        Class<?> a2 = a(adn.nextToken(), adn);
        if (adn.hasMoreTokens()) {
            String nextToken = adn.nextToken();
            if ("<".equals(nextToken)) {
                return this.a.a(a2, b(adn));
            }
            adn.a(nextToken);
        }
        return this.a.a(a2, (adj) null);
    }

    /* access modifiers changed from: protected */
    public List<afm> b(adn adn) throws IllegalArgumentException {
        ArrayList arrayList = new ArrayList();
        while (adn.hasMoreTokens()) {
            arrayList.add(a(adn));
            if (!adn.hasMoreTokens()) {
                break;
            }
            String nextToken = adn.nextToken();
            if (">".equals(nextToken)) {
                return arrayList;
            }
            if (!",".equals(nextToken)) {
                throw a(adn, "Unexpected token '" + nextToken + "', expected ',' or '>')");
            }
        }
        throw a(adn, "Unexpected end-of-string");
    }

    /* access modifiers changed from: protected */
    public Class<?> a(String str, adn adn) {
        try {
            return Class.forName(str, true, Thread.currentThread().getContextClassLoader());
        } catch (Exception e) {
            if (e instanceof RuntimeException) {
                throw ((RuntimeException) e);
            }
            throw a(adn, "Can not locate class '" + str + "', problem: " + e.getMessage());
        }
    }

    /* access modifiers changed from: protected */
    public IllegalArgumentException a(adn adn, String str) {
        return new IllegalArgumentException("Failed to parse type '" + adn.a() + "' (remaining: '" + adn.b() + "'): " + str);
    }
}
