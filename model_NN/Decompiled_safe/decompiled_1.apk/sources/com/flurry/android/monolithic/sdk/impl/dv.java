package com.flurry.android.monolithic.sdk.impl;

import com.jumptap.adtag.JtAdView;
import com.jumptap.adtag.JtAdViewListener;
import java.util.Collections;

class dv implements JtAdViewListener {
    final /* synthetic */ du a;

    dv(du duVar) {
        this.a = duVar;
    }

    public void onNoAdFound(JtAdView jtAdView, int i) {
        this.a.d(Collections.emptyMap());
        ja.a(3, du.b, "Jumptap Interstitial no ad found.");
    }

    public void onNewAd(JtAdView jtAdView, int i, String str) {
        this.a.a(Collections.emptyMap());
        ja.a(3, du.b, "Jumptap Interstitial new ad.");
    }

    public void onInterstitialDismissed(JtAdView jtAdView, int i) {
        this.a.c(Collections.emptyMap());
        ja.a(3, du.b, "Jumptap Interstitial dismissed.");
    }

    public void onFocusChange(JtAdView jtAdView, int i, boolean z) {
        ja.a(3, du.b, "Jumptap Interstitial focus changed.");
    }

    public void onAdError(JtAdView jtAdView, int i, int i2) {
        ja.a(3, du.b, "Jumptap Interstitial error.");
        this.a.d(Collections.emptyMap());
    }
}
