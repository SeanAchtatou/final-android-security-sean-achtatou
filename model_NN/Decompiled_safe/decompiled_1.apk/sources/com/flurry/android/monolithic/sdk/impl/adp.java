package com.flurry.android.monolithic.sdk.impl;

import java.util.HashSet;

public final class adp {
    adr a = null;
    ads b = null;
    adx c = null;
    adv d = null;
    adw e = null;
    adu f = null;
    adt g = null;

    public adr a() {
        if (this.a == null) {
            this.a = new adr();
        }
        return this.a;
    }

    public ads b() {
        if (this.b == null) {
            this.b = new ads();
        }
        return this.b;
    }

    public adx c() {
        if (this.c == null) {
            this.c = new adx();
        }
        return this.c;
    }

    public adv d() {
        if (this.d == null) {
            this.d = new adv();
        }
        return this.d;
    }

    public adw e() {
        if (this.e == null) {
            this.e = new adw();
        }
        return this.e;
    }

    public adu f() {
        if (this.f == null) {
            this.f = new adu();
        }
        return this.f;
    }

    public adt g() {
        if (this.g == null) {
            this.g = new adt();
        }
        return this.g;
    }

    public static <T> HashSet<T> a(T[] tArr) {
        HashSet<T> hashSet = new HashSet<>();
        if (tArr != null) {
            for (T add : tArr) {
                hashSet.add(add);
            }
        }
        return hashSet;
    }

    public static <T> Iterable<T> b(T[] tArr) {
        return new adq(tArr);
    }
}
