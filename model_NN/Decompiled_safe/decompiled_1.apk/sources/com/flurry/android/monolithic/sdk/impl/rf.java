package com.flurry.android.monolithic.sdk.impl;

import com.flurry.android.monolithic.sdk.impl.rf;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.util.HashMap;

public abstract class rf<T extends rf<T>> implements qg {
    protected static final DateFormat d = aen.f;
    protected rg e;
    protected HashMap<adb, Class<?>> f;
    protected boolean g = true;
    protected yh h;

    public abstract <DESC extends qb> DESC a(afm afm);

    public abstract boolean b();

    public abstract boolean c();

    public abstract boolean d();

    protected rf(qf<? extends qb> qfVar, py pyVar, ye<?> yeVar, yh yhVar, rl rlVar, adk adk, qs qsVar) {
        this.e = new rg(qfVar, pyVar, yeVar, rlVar, adk, null, d, qsVar);
        this.h = yhVar;
    }

    protected rf(rf<T> rfVar, rg rgVar, yh yhVar) {
        this.e = rgVar;
        this.h = yhVar;
        this.f = rfVar.f;
    }

    public qf<? extends qb> i() {
        return this.e.a();
    }

    public py a() {
        return this.e.b();
    }

    public ye<?> e() {
        return this.e.c();
    }

    public final rl j() {
        return this.e.d();
    }

    public final qs k() {
        return this.e.h();
    }

    public final Class<?> a(Class<?> cls) {
        if (this.f == null) {
            return null;
        }
        return this.f.get(new adb(cls));
    }

    public final yj<?> d(afm afm) {
        return this.e.f();
    }

    public final yh l() {
        if (this.h == null) {
            this.h = new yv();
        }
        return this.h;
    }

    public final adk m() {
        return this.e.e();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.adk.a(java.lang.reflect.Type, com.flurry.android.monolithic.sdk.impl.adj):com.flurry.android.monolithic.sdk.impl.afm
     arg types: [java.lang.Class<?>, ?[OBJECT, ARRAY]]
     candidates:
      com.flurry.android.monolithic.sdk.impl.adk.a(com.flurry.android.monolithic.sdk.impl.ade, java.lang.Class<?>):com.flurry.android.monolithic.sdk.impl.ade
      com.flurry.android.monolithic.sdk.impl.adk.a(java.lang.reflect.Type, java.lang.Class<?>):com.flurry.android.monolithic.sdk.impl.ade
      com.flurry.android.monolithic.sdk.impl.adk.a(com.flurry.android.monolithic.sdk.impl.afm, java.lang.Class<?>):com.flurry.android.monolithic.sdk.impl.afm
      com.flurry.android.monolithic.sdk.impl.adk.a(java.lang.Class<?>, com.flurry.android.monolithic.sdk.impl.adj):com.flurry.android.monolithic.sdk.impl.afm
      com.flurry.android.monolithic.sdk.impl.adk.a(java.lang.Class<?>, java.util.List<com.flurry.android.monolithic.sdk.impl.afm>):com.flurry.android.monolithic.sdk.impl.afm
      com.flurry.android.monolithic.sdk.impl.adk.a(java.lang.Class<?>, com.flurry.android.monolithic.sdk.impl.afm[]):com.flurry.android.monolithic.sdk.impl.afm
      com.flurry.android.monolithic.sdk.impl.adk.a(java.lang.reflect.GenericArrayType, com.flurry.android.monolithic.sdk.impl.adj):com.flurry.android.monolithic.sdk.impl.afm
      com.flurry.android.monolithic.sdk.impl.adk.a(java.lang.reflect.ParameterizedType, com.flurry.android.monolithic.sdk.impl.adj):com.flurry.android.monolithic.sdk.impl.afm
      com.flurry.android.monolithic.sdk.impl.adk.a(java.lang.reflect.TypeVariable<?>, com.flurry.android.monolithic.sdk.impl.adj):com.flurry.android.monolithic.sdk.impl.afm
      com.flurry.android.monolithic.sdk.impl.adk.a(java.lang.reflect.WildcardType, com.flurry.android.monolithic.sdk.impl.adj):com.flurry.android.monolithic.sdk.impl.afm
      com.flurry.android.monolithic.sdk.impl.adk.a(java.lang.Class<?>, java.lang.Class<?>):com.flurry.android.monolithic.sdk.impl.afm[]
      com.flurry.android.monolithic.sdk.impl.adk.a(java.lang.reflect.Type, com.flurry.android.monolithic.sdk.impl.adj):com.flurry.android.monolithic.sdk.impl.afm */
    public final afm b(Class<?> cls) {
        return m().a((Type) cls, (adj) null);
    }

    public afm a(afm afm, Class<?> cls) {
        return m().a(afm, cls);
    }

    public final DateFormat n() {
        return this.e.g();
    }

    public <DESC extends qb> DESC c(Class<?> cls) {
        return a(b(cls));
    }

    public yj<?> d(xg xgVar, Class<? extends yj<?>> cls) {
        yj<?> a;
        qs k = k();
        return (k == null || (a = k.a(this, xgVar, cls)) == null) ? (yj) adz.b(cls, c()) : a;
    }

    public yi e(xg xgVar, Class<? extends yi> cls) {
        yi b;
        qs k = k();
        return (k == null || (b = k.b(this, xgVar, cls)) == null) ? (yi) adz.b(cls, c()) : b;
    }
}
