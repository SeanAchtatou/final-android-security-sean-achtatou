package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.lang.reflect.Constructor;

public final class sy extends sw {
    protected final sw i;
    protected final Constructor<?> j;

    public sy(sw swVar, Constructor<?> constructor) {
        super(swVar);
        this.i = swVar;
        this.j = constructor;
    }

    protected sy(sy syVar, qu<Object> quVar) {
        super(syVar, quVar);
        this.i = syVar.i.a(quVar);
        this.j = syVar.j;
    }

    /* renamed from: b */
    public sy a(qu<Object> quVar) {
        return new sy(this, quVar);
    }

    public xk b() {
        return this.i.b();
    }

    public void a(ow owVar, qm qmVar, Object obj) throws IOException, oz {
        Object obj2;
        if (owVar.e() == pb.VALUE_NULL) {
            if (this.f == null) {
                obj2 = null;
            } else {
                obj2 = this.f.a(qmVar);
            }
        } else if (this.e != null) {
            obj2 = this.d.a(owVar, qmVar, this.e);
        } else {
            try {
                obj2 = this.j.newInstance(obj);
            } catch (Exception e) {
                adz.b(e, "Failed to instantiate class " + this.j.getDeclaringClass().getName() + ", problem: " + e.getMessage());
                obj2 = null;
            }
            this.d.a(owVar, qmVar, obj2);
        }
        a(obj, obj2);
    }

    public final void a(Object obj, Object obj2) throws IOException {
        this.i.a(obj, obj2);
    }
}
