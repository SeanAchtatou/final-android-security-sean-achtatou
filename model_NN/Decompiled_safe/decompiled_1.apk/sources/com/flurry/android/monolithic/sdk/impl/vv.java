package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
public final class vv extends wa<Double> {
    public vv(Class<Double> cls, Double d) {
        super(cls, d);
    }

    /* renamed from: b */
    public Double a(ow owVar, qm qmVar) throws IOException, oz {
        return z(owVar, qmVar);
    }

    /* renamed from: b */
    public Double a(ow owVar, qm qmVar, rw rwVar) throws IOException, oz {
        return z(owVar, qmVar);
    }
}
