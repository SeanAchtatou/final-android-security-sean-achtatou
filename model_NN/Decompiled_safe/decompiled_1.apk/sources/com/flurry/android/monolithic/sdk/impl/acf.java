package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
public final class acf extends abw<String[]> implements rp {
    protected ra<Object> a;

    public acf(qc qcVar) {
        super(String[].class, null, qcVar);
    }

    public abc<?> a(rx rxVar) {
        return this;
    }

    /* renamed from: a */
    public void b(String[] strArr, or orVar, ru ruVar) throws IOException, oq {
        int length = strArr.length;
        if (length != 0) {
            if (this.a != null) {
                a(strArr, orVar, ruVar, this.a);
                return;
            }
            for (int i = 0; i < length; i++) {
                if (strArr[i] == null) {
                    orVar.f();
                } else {
                    orVar.b(strArr[i]);
                }
            }
        }
    }

    private void a(String[] strArr, or orVar, ru ruVar, ra<Object> raVar) throws IOException, oq {
        int length = strArr.length;
        for (int i = 0; i < length; i++) {
            if (strArr[i] == null) {
                ruVar.a(orVar);
            } else {
                raVar.a(strArr[i], orVar, ruVar);
            }
        }
    }

    public void a(ru ruVar) throws qw {
        ra<Object> a2 = ruVar.a(String.class, this.f);
        if (a2 != null && a2.getClass().getAnnotation(rz.class) == null) {
            this.a = a2;
        }
    }
}
