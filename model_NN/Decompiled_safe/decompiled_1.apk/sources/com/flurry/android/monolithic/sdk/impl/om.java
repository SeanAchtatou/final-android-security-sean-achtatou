package com.flurry.android.monolithic.sdk.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

public class om extends oi {
    private final byte[] a;
    private final Charset b;

    public om(String str, String str2, Charset charset) throws UnsupportedEncodingException {
        super(str2);
        Charset charset2;
        if (str == null) {
            throw new IllegalArgumentException("Text may not be null");
        }
        if (charset == null) {
            charset2 = Charset.forName("US-ASCII");
        } else {
            charset2 = charset;
        }
        this.a = str.getBytes(charset2.name());
        this.b = charset2;
    }

    public om(String str) throws UnsupportedEncodingException {
        this(str, "text/plain", null);
    }

    public void a(OutputStream outputStream) throws IOException {
        if (outputStream == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        }
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(this.a);
        byte[] bArr = new byte[4096];
        while (true) {
            int read = byteArrayInputStream.read(bArr);
            if (read != -1) {
                outputStream.write(bArr, 0, read);
            } else {
                outputStream.flush();
                return;
            }
        }
    }

    public String d() {
        return "8bit";
    }

    public String c() {
        return this.b.name();
    }

    public long e() {
        return (long) this.a.length;
    }

    public String b() {
        return null;
    }
}
