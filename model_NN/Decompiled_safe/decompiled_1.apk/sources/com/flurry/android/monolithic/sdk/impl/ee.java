package com.flurry.android.monolithic.sdk.impl;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import com.flurry.android.impl.ads.FlurryAdModule;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;
import com.mobclix.android.sdk.MobclixFullScreenAdView;

public final class ee extends cr {
    /* access modifiers changed from: private */
    public static final String b = ee.class.getSimpleName();

    public ee(Context context, FlurryAdModule flurryAdModule, m mVar, AdUnit adUnit, Bundle bundle) {
        super(context, flurryAdModule, mVar, adUnit);
    }

    public void a() {
        MobclixFullScreenAdView mobclixFullScreenAdView = new MobclixFullScreenAdView((Activity) b());
        mobclixFullScreenAdView.addMobclixAdViewListener(new ef(this));
        mobclixFullScreenAdView.requestAndDisplayAd();
    }
}
