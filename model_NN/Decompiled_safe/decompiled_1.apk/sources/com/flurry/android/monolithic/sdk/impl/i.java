package com.flurry.android.monolithic.sdk.impl;

import java.util.Map;

public class i {
    public final String a;
    public final Map<String, String> b;
    public final bh c;

    public i(String str, Map<String, String> map, bh bhVar) {
        this.a = str;
        this.b = map;
        this.c = bhVar;
    }

    public String toString() {
        return "action=" + this.a + ",params=" + this.b + "," + this.c;
    }
}
