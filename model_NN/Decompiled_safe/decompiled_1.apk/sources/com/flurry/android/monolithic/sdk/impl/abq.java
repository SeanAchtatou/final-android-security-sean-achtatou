package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public abstract class abq<T> extends abt<T> {
    protected abq(Class<T> cls) {
        super(cls);
    }

    protected abq(Class<?> cls, boolean z) {
        super(cls);
    }

    public void a(T t, or orVar, ru ruVar, rx rxVar) throws IOException, oq {
        rxVar.a(t, orVar);
        a(t, orVar, ruVar);
        rxVar.d(t, orVar);
    }
}
