package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

public class tz extends wv<AtomicBoolean> {
    public tz() {
        super(AtomicBoolean.class);
    }

    /* renamed from: b */
    public AtomicBoolean a(ow owVar, qm qmVar) throws IOException, oz {
        return new AtomicBoolean(n(owVar, qmVar));
    }
}
