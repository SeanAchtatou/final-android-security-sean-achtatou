package com.flurry.android.monolithic.sdk.impl;

public abstract class qb {
    protected final afm a;

    public abstract xh c();

    protected qb(afm afm) {
        this.a = afm;
    }

    public afm a() {
        return this.a;
    }

    public Class<?> b() {
        return this.a.p();
    }
}
