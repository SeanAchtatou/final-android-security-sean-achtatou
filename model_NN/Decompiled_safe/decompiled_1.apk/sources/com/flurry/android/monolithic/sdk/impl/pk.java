package com.flurry.android.monolithic.sdk.impl;

public final class pk extends pa {
    protected final pk c;
    protected int d;
    protected int e;
    protected String f;
    protected pk g = null;

    public pk(pk pkVar, int i, int i2, int i3) {
        this.a = i;
        this.c = pkVar;
        this.d = i2;
        this.e = i3;
        this.b = -1;
    }

    /* access modifiers changed from: protected */
    public final void a(int i, int i2, int i3) {
        this.a = i;
        this.b = -1;
        this.d = i2;
        this.e = i3;
        this.f = null;
    }

    public static pk a(int i, int i2) {
        return new pk(null, 0, i, i2);
    }

    public static pk g() {
        return new pk(null, 0, 1, 0);
    }

    public final pk b(int i, int i2) {
        pk pkVar = this.g;
        if (pkVar == null) {
            pk pkVar2 = new pk(this, 1, i, i2);
            this.g = pkVar2;
            return pkVar2;
        }
        pkVar.a(1, i, i2);
        return pkVar;
    }

    public final pk c(int i, int i2) {
        pk pkVar = this.g;
        if (pkVar == null) {
            pk pkVar2 = new pk(this, 2, i, i2);
            this.g = pkVar2;
            return pkVar2;
        }
        pkVar.a(2, i, i2);
        return pkVar;
    }

    public final String h() {
        return this.f;
    }

    public final pk i() {
        return this.c;
    }

    public final ot a(Object obj) {
        return new ot(obj, -1, this.d, this.e);
    }

    public final boolean j() {
        int i = this.b + 1;
        this.b = i;
        return this.a != 0 && i > 0;
    }

    public void a(String str) {
        this.f = str;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder(64);
        switch (this.a) {
            case 0:
                sb.append("/");
                break;
            case 1:
                sb.append('[');
                sb.append(f());
                sb.append(']');
                break;
            case 2:
                sb.append('{');
                if (this.f != null) {
                    sb.append('\"');
                    afr.a(sb, this.f);
                    sb.append('\"');
                } else {
                    sb.append('?');
                }
                sb.append('}');
                break;
        }
        return sb.toString();
    }
}
