package com.flurry.android.monolithic.sdk.impl;

import com.flurry.android.monolithic.sdk.impl.lf;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public abstract class kn<T extends lf> {
    private static final ConcurrentMap<String, ConcurrentMap<Integer, Object>> a = new ConcurrentHashMap();
    private static final js[] b = new js[0];
    private final ji c;
    private final js[] d;
    private final boolean[] e;
    private final kq f;
    private lr g = null;
    private ll h = null;

    /* access modifiers changed from: protected */
    public final js[] b() {
        return this.d;
    }

    /* access modifiers changed from: protected */
    public final boolean[] c() {
        return this.e;
    }

    protected kn(ji jiVar, kq kqVar) {
        this.c = jiVar;
        this.f = kqVar;
        this.d = (js[]) jiVar.b().toArray(b);
        this.e = new boolean[this.d.length];
    }

    /* access modifiers changed from: protected */
    public void a(js jsVar, Object obj) {
        if (!b(jsVar, obj) && jsVar.e() == null) {
            throw new jg("Field " + jsVar + " does not accept null values");
        }
    }

    protected static boolean b(js jsVar, Object obj) {
        if (obj != null) {
            return true;
        }
        ji c2 = jsVar.c();
        kj a2 = c2.a();
        if (a2 == kj.NULL) {
            return true;
        }
        if (a2 == kj.UNION) {
            for (ji a3 : c2.k()) {
                if (a3.a() == kj.NULL) {
                    return true;
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public Object a(js jsVar) throws IOException {
        Object obj;
        ou e2 = jsVar.e();
        if (e2 == null) {
            throw new jg("Field " + jsVar + " not set and has no default value");
        } else if (e2.g() && (jsVar.c().a() == kj.NULL || (jsVar.c().a() == kj.UNION && jsVar.c().k().get(0).a() == kj.NULL))) {
            return null;
        } else {
            ConcurrentMap concurrentMap = a.get(this.c.g());
            if (concurrentMap == null) {
                a.putIfAbsent(this.c.g(), new ConcurrentHashMap(this.d.length));
                concurrentMap = a.get(this.c.g());
            }
            Object obj2 = concurrentMap.get(Integer.valueOf(jsVar.b()));
            if (obj2 == null) {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                this.g = md.a().a(byteArrayOutputStream, this.g);
                ml.a(this.g, jsVar.c(), e2);
                this.g.flush();
                this.h = ly.a().a(byteArrayOutputStream.toByteArray(), this.h);
                Object a2 = this.f.a(jsVar.c()).a(null, this.h);
                concurrentMap.putIfAbsent(Integer.valueOf(jsVar.b()), a2);
                obj = a2;
            } else {
                obj = obj2;
            }
            return this.f.b(jsVar.c(), obj);
        }
    }

    public int hashCode() {
        int i = 1 * 31;
        return ((Arrays.hashCode(this.e) + 31) * 31) + (this.c == null ? 0 : this.c.hashCode());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        kn knVar = (kn) obj;
        if (!Arrays.equals(this.e, knVar.e)) {
            return false;
        }
        if (this.c == null) {
            if (knVar.c != null) {
                return false;
            }
        } else if (!this.c.equals(knVar.c)) {
            return false;
        }
        return true;
    }
}
