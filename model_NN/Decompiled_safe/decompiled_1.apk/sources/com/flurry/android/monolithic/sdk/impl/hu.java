package com.flurry.android.monolithic.sdk.impl;

import java.util.LinkedHashMap;

class hu {
    hu() {
    }

    public long a(hm hmVar) {
        int f = hmVar.f();
        LinkedHashMap<Integer, Long> d = hmVar.d();
        ja.a(4, "RetryPolicyChecker", "checkOperation retryAttemps = " + (f + 1) + " / " + d.size());
        if (f >= d.size()) {
            return -1;
        }
        return d.get(Integer.valueOf(f)).longValue();
    }
}
