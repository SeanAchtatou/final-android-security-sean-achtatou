package com.flurry.android.monolithic.sdk.impl;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import com.flurry.android.impl.ads.FlurryAdModule;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;
import com.google.ads.AdRequest;
import com.google.ads.InterstitialAd;

public final class dj extends cr {
    /* access modifiers changed from: private */
    public static final String b = dj.class.getSimpleName();
    private final String c;
    private final String d;
    private final boolean e;
    /* access modifiers changed from: private */
    public final InterstitialAd f = new InterstitialAd((Activity) b(), this.c);

    public dj(Context context, FlurryAdModule flurryAdModule, m mVar, AdUnit adUnit, Bundle bundle) {
        super(context, flurryAdModule, mVar, adUnit);
        this.c = bundle.getString("com.flurry.admob.MY_AD_UNIT_ID");
        this.d = bundle.getString("com.flurry.admob.MYTEST_AD_DEVICE_ID");
        this.e = bundle.getBoolean("com.flurry.admob.test");
        this.f.setAdListener(new dl(this));
    }

    public void a() {
        AdRequest adRequest = new AdRequest();
        if (this.e) {
            ja.a(3, b, "Admob AdView set to Test Mode.");
            adRequest.addTestDevice(AdRequest.TEST_EMULATOR);
            if (!TextUtils.isEmpty(this.d)) {
                adRequest.addTestDevice(this.d);
            }
        }
        this.f.loadAd(adRequest);
    }
}
