package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

public class td extends qm {
    protected ow c;
    protected final qq d;
    protected final qt e;
    protected adp f;
    protected aeh g;
    protected DateFormat h;

    public td(qk qkVar, ow owVar, qq qqVar, qt qtVar) {
        super(qkVar);
        this.c = owVar;
        this.d = qqVar;
        this.e = qtVar;
    }

    public qq b() {
        return this.d;
    }

    public ow d() {
        return this.c;
    }

    public Object a(Object obj, qc qcVar, Object obj2) {
        if (this.e != null) {
            return this.e.a(obj, this, qcVar, obj2);
        }
        throw new IllegalStateException("No 'injectableValues' configured, can not inject value with id [" + obj + "]");
    }

    public final aeh g() {
        aeh aeh = this.g;
        if (aeh == null) {
            return new aeh();
        }
        this.g = null;
        return aeh;
    }

    public final void a(aeh aeh) {
        if (this.g == null || aeh.b() >= this.g.b()) {
            this.g = aeh;
        }
    }

    public final adp h() {
        if (this.f == null) {
            this.f = new adp();
        }
        return this.f;
    }

    public Date a(String str) throws IllegalArgumentException {
        try {
            return i().parse(str);
        } catch (ParseException e2) {
            throw new IllegalArgumentException(e2.getMessage());
        }
    }

    public Calendar a(Date date) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        return instance;
    }

    public boolean a(ow owVar, qu<?> quVar, Object obj, String str) throws IOException, oz {
        aeg<qn> f2 = this.a.f();
        if (f2 != null) {
            ow owVar2 = this.c;
            this.c = owVar;
            aeg<qn> aeg = f2;
            while (aeg != null) {
                try {
                    if (aeg.b().a(this, quVar, obj, str)) {
                        return true;
                    }
                    aeg = aeg.a();
                } finally {
                    this.c = owVar2;
                }
            }
            this.c = owVar2;
        }
        return false;
    }

    public qw b(Class<?> cls) {
        return a(cls, this.c.e());
    }

    public qw a(Class<?> cls, pb pbVar) {
        return qw.a(this.c, "Can not deserialize instance of " + c(cls) + " out of " + pbVar + " token");
    }

    public qw a(Class<?> cls, Throwable th) {
        return qw.a(this.c, "Can not construct instance of " + cls.getName() + ", problem: " + th.getMessage(), th);
    }

    public qw a(Class<?> cls, String str) {
        return qw.a(this.c, "Can not construct instance of " + cls.getName() + ", problem: " + str);
    }

    public qw b(Class<?> cls, String str) {
        return qw.a(this.c, "Can not construct instance of " + cls.getName() + " from String value '" + j() + "': " + str);
    }

    public qw c(Class<?> cls, String str) {
        return qw.a(this.c, "Can not construct instance of " + cls.getName() + " from number value (" + j() + "): " + str);
    }

    public qw a(Class<?> cls, String str, String str2) {
        return qw.a(this.c, "Can not construct Map key of type " + cls.getName() + " from String \"" + c(str) + "\": " + str2);
    }

    public qw a(ow owVar, pb pbVar, String str) {
        return qw.a(owVar, "Unexpected token (" + owVar.e() + "), expected " + pbVar + ": " + str);
    }

    public qw a(Object obj, String str) {
        return xe.a(this.c, obj, str);
    }

    public qw a(afm afm, String str) {
        return qw.a(this.c, "Could not resolve type id '" + str + "' into a subtype of " + afm);
    }

    /* access modifiers changed from: protected */
    public DateFormat i() {
        if (this.h == null) {
            this.h = (DateFormat) this.a.n().clone();
        }
        return this.h;
    }

    /* access modifiers changed from: protected */
    public String c(Class<?> cls) {
        if (cls.isArray()) {
            return c(cls.getComponentType()) + "[]";
        }
        return cls.getName();
    }

    /* access modifiers changed from: protected */
    public String j() {
        try {
            return c(this.c.k());
        } catch (Exception e2) {
            return "[N/A]";
        }
    }

    /* access modifiers changed from: protected */
    public String c(String str) {
        if (str.length() > 500) {
            return str.substring(0, 500) + "]...[" + str.substring(str.length() - 500);
        }
        return str;
    }
}
