package com.flurry.android.monolithic.sdk.impl;

import android.telephony.TelephonyManager;
import java.util.Arrays;

public class im {
    private static final String a = im.class.getSimpleName();
    private static byte[] b;

    public static byte[] a() {
        if (b != null) {
            return b;
        }
        if (ia.a().b().checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") != 0) {
            return null;
        }
        b();
        return b;
    }

    private static void b() {
        String deviceId;
        TelephonyManager telephonyManager = (TelephonyManager) ia.a().b().getSystemService("phone");
        if (telephonyManager != null && (deviceId = telephonyManager.getDeviceId()) != null && deviceId.trim().length() > 0) {
            try {
                byte[] d = je.d(deviceId);
                if (d == null || d.length != 20) {
                    ja.a(6, a, "sha1 is not " + 20 + " bytes long: " + Arrays.toString(d));
                } else {
                    b = d;
                }
            } catch (Exception e) {
                ja.a(6, a, "Exception in generateHashedImei()");
            }
        }
    }
}
