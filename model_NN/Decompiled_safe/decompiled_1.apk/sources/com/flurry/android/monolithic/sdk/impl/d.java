package com.flurry.android.monolithic.sdk.impl;

import android.widget.VideoView;
import com.flurry.android.FlurryFullscreenTakeoverActivity;
import com.flurry.android.impl.ads.FlurryAdModule;

public final class d implements az {
    final /* synthetic */ FlurryFullscreenTakeoverActivity a;

    private d(FlurryFullscreenTakeoverActivity flurryFullscreenTakeoverActivity) {
        this.a = flurryFullscreenTakeoverActivity;
    }

    public boolean a(ar arVar, String str, boolean z) {
        boolean z2;
        if (this.a.a(str)) {
            VideoView unused = this.a.g = new VideoView(this.a);
            this.a.g.setFocusable(true);
            this.a.g.setFocusableInTouchMode(true);
            boolean unused2 = this.a.h = z;
            this.a.c(str);
            return true;
        } else if (!this.a.b(str)) {
            return false;
        } else {
            if (!z) {
                z2 = this.a.a(str, arVar.getUrl());
            } else {
                z2 = z;
            }
            FlurryAdModule.getInstance().a().a(this.a, str, this.a.m);
            if (z2) {
                arVar.post(new e(this, arVar));
            }
            return true;
        }
    }
}
