package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public abstract class aaz extends abt<Object> implements rp {
    protected static final zf[] a = new zf[0];
    protected final zf[] b;
    protected final zf[] c;
    protected final zc d;
    protected final Object e;

    protected aaz(afm afm, zf[] zfVarArr, zf[] zfVarArr2, zc zcVar, Object obj) {
        super(afm);
        this.b = zfVarArr;
        this.c = zfVarArr2;
        this.d = zcVar;
        this.e = obj;
    }

    public aaz(Class<?> cls, zf[] zfVarArr, zf[] zfVarArr2, zc zcVar, Object obj) {
        super(cls);
        this.b = zfVarArr;
        this.c = zfVarArr2;
        this.d = zcVar;
        this.e = obj;
    }

    protected aaz(aaz aaz) {
        this(aaz.k, aaz.b, aaz.c, aaz.d, aaz.e);
    }

    public void a(Object obj, or orVar, ru ruVar, rx rxVar) throws IOException, oq {
        rxVar.b(obj, orVar);
        if (this.e != null) {
            c(obj, orVar, ruVar);
        } else {
            b(obj, orVar, ruVar);
        }
        rxVar.e(obj, orVar);
    }

    /* access modifiers changed from: protected */
    public void b(Object obj, or orVar, ru ruVar) throws IOException, oq {
        zf[] zfVarArr;
        String d2;
        String d3;
        if (this.c == null || ruVar.a() == null) {
            zfVarArr = this.b;
        } else {
            zfVarArr = this.c;
        }
        int i = 0;
        try {
            int length = zfVarArr.length;
            while (i < length) {
                zf zfVar = zfVarArr[i];
                if (zfVar != null) {
                    zfVar.a(obj, orVar, ruVar);
                }
                i++;
            }
            if (this.d != null) {
                this.d.a(obj, orVar, ruVar);
            }
        } catch (Exception e2) {
            Exception exc = e2;
            int i2 = i;
            Exception exc2 = exc;
            if (i2 == zfVarArr.length) {
                d3 = "[anySetter]";
            } else {
                d3 = zfVarArr[i2].d();
            }
            a(ruVar, exc2, obj, d3);
        } catch (StackOverflowError e3) {
            qw qwVar = new qw("Infinite recursion (StackOverflowError)");
            if (i == zfVarArr.length) {
                d2 = "[anySetter]";
            } else {
                d2 = zfVarArr[i].d();
            }
            qwVar.a(new qx(obj, d2));
            throw qwVar;
        }
    }

    /* access modifiers changed from: protected */
    public void c(Object obj, or orVar, ru ruVar) throws IOException, oq {
        zf[] zfVarArr;
        String d2;
        String d3;
        if (this.c == null || ruVar.a() == null) {
            zfVarArr = this.b;
        } else {
            zfVarArr = this.c;
        }
        ze b2 = b(ruVar);
        if (b2 == null) {
            b(obj, orVar, ruVar);
            return;
        }
        try {
            for (zf zfVar : zfVarArr) {
                if (zfVar != null) {
                    b2.a(obj, orVar, ruVar, zfVar);
                }
            }
            if (this.d != null) {
                this.d.a(obj, orVar, ruVar);
            }
        } catch (Exception e2) {
            if (0 == zfVarArr.length) {
                d3 = "[anySetter]";
            } else {
                d3 = zfVarArr[0].d();
            }
            a(ruVar, e2, obj, d3);
        } catch (StackOverflowError e3) {
            qw qwVar = new qw("Infinite recursion (StackOverflowError)");
            if (0 == zfVarArr.length) {
                d2 = "[anySetter]";
            } else {
                d2 = zfVarArr[0].d();
            }
            qwVar.a(new qx(obj, d2));
            throw qwVar;
        }
    }

    /* access modifiers changed from: protected */
    public ze b(ru ruVar) throws qw {
        Object obj = this.e;
        zl b2 = ruVar.b();
        if (b2 != null) {
            return b2.a(obj);
        }
        throw new qw("Can not resolve BeanPropertyFilter with id '" + obj + "'; no FilterProvider configured");
    }

    public void a(ru ruVar) throws qw {
        ra<Object> raVar;
        zf zfVar;
        rx rxVar;
        int length = this.c == null ? 0 : this.c.length;
        int length2 = this.b.length;
        for (int i = 0; i < length2; i++) {
            zf zfVar2 = this.b[i];
            if (!zfVar2.e()) {
                afm f = zfVar2.f();
                if (f == null) {
                    f = ruVar.a(zfVar2.g());
                    if (!f.u()) {
                        if (f.f() || f.h() > 0) {
                            zfVar2.a(f);
                        }
                    }
                }
                ra<Object> a2 = ruVar.a(f, zfVar2);
                if (!f.f() || (rxVar = (rx) f.g().o()) == null || !(a2 instanceof abc)) {
                    raVar = a2;
                } else {
                    raVar = ((abc) a2).b(rxVar);
                }
                this.b[i] = zfVar2.a(raVar);
                if (i < length && (zfVar = this.c[i]) != null) {
                    this.c[i] = zfVar.a(raVar);
                }
            }
        }
        if (this.d != null) {
            this.d.a(ruVar);
        }
    }
}
