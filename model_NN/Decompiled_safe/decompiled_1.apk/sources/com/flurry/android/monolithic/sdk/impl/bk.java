package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import android.content.Intent;
import com.flurry.android.FlurryFullscreenTakeoverActivity;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;

class bk extends jf {
    final /* synthetic */ String a;
    final /* synthetic */ AdUnit b;
    final /* synthetic */ Context c;
    final /* synthetic */ boolean d;
    final /* synthetic */ bi e;

    bk(bi biVar, String str, AdUnit adUnit, Context context, boolean z) {
        this.e = biVar;
        this.a = str;
        this.b = adUnit;
        this.c = context;
        this.d = z;
    }

    public void a() {
        if (this.a != null) {
            String obj = this.b.b().toString();
            if (this.a.startsWith("market://")) {
                this.e.a(this.c, this.a, obj);
            } else if (this.a.startsWith("http")) {
                Intent intent = new Intent(ia.a().b(), FlurryFullscreenTakeoverActivity.class);
                intent.putExtra(FlurryFullscreenTakeoverActivity.EXTRA_KEY_URL, this.a);
                if (!this.d || !je.a(intent)) {
                    ja.a(6, bi.d, "Unable to launch FlurryFullscreenTakeoverActivity, falling back to browser. Fix by declaring this Activity in your AndroidManifest.xml");
                    this.e.b(this.c, this.a, obj);
                    return;
                }
                this.e.a(this.c, intent, obj);
            } else if (!this.e.b(this.c, this.a, obj)) {
                ja.a(5, bi.d, "Failed to launch intent for:" + this.a);
            }
        } else {
            ja.a(5, bi.d, "Unable to launch intent for: " + this.a);
        }
    }
}
