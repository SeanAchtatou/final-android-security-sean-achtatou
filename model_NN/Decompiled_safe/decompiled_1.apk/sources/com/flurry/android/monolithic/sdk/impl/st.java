package com.flurry.android.monolithic.sdk.impl;

public class st extends qp {
    protected static final re[] a = new re[0];
    protected static final su[] b = new su[0];
    protected static final px[] c = new px[0];
    protected static final ti[] d = new ti[0];
    protected final qr[] e;
    protected final re[] f;
    protected final su[] g;
    protected final px[] h;
    protected final ti[] i;

    public st() {
        this(null, null, null, null, null);
    }

    protected st(qr[] qrVarArr, re[] reVarArr, su[] suVarArr, px[] pxVarArr, ti[] tiVarArr) {
        re[] reVarArr2;
        su[] suVarArr2;
        px[] pxVarArr2;
        ti[] tiVarArr2;
        this.e = qrVarArr == null ? ss.a : qrVarArr;
        if (reVarArr == null) {
            reVarArr2 = a;
        } else {
            reVarArr2 = reVarArr;
        }
        this.f = reVarArr2;
        if (suVarArr == null) {
            suVarArr2 = b;
        } else {
            suVarArr2 = suVarArr;
        }
        this.g = suVarArr2;
        if (pxVarArr == null) {
            pxVarArr2 = c;
        } else {
            pxVarArr2 = pxVarArr;
        }
        this.h = pxVarArr2;
        if (tiVarArr == null) {
            tiVarArr2 = d;
        } else {
            tiVarArr2 = tiVarArr;
        }
        this.i = tiVarArr2;
    }

    public boolean f() {
        return this.f.length > 0;
    }

    public boolean g() {
        return this.g.length > 0;
    }

    public boolean h() {
        return this.h.length > 0;
    }

    public boolean i() {
        return this.i.length > 0;
    }

    public Iterable<qr> a() {
        return adp.b(this.e);
    }

    public Iterable<re> b() {
        return adp.b(this.f);
    }

    public Iterable<su> c() {
        return adp.b(this.g);
    }

    public Iterable<px> d() {
        return adp.b(this.h);
    }

    public Iterable<ti> e() {
        return adp.b(this.i);
    }
}
