package com.flurry.android.monolithic.sdk.impl;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class aev implements Iterator<String> {
    static final aev a = new aev();

    private aev() {
    }

    public static aev a() {
        return a;
    }

    public boolean hasNext() {
        return false;
    }

    /* renamed from: b */
    public String next() {
        throw new NoSuchElementException();
    }

    public void remove() {
        throw new IllegalStateException();
    }
}
