package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import android.text.TextUtils;
import com.flurry.android.impl.ads.FlurryAdModule;
import com.flurry.android.impl.ads.avro.protocol.v6.AdFrame;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public final class dc implements am {
    private static final String a = dc.class.getSimpleName();
    private static final Map<String, am> b = a();

    public ac a(Context context, FlurryAdModule flurryAdModule, m mVar, AdUnit adUnit) {
        if (context == null || flurryAdModule == null || mVar == null || adUnit == null) {
            return null;
        }
        List<AdFrame> d = adUnit.d();
        if (d == null || d.isEmpty()) {
            return null;
        }
        AdFrame adFrame = d.get(0);
        if (adFrame == null) {
            return null;
        }
        String obj = adFrame.d().toString();
        if (TextUtils.isEmpty(obj)) {
            return null;
        }
        am a2 = a(obj.toUpperCase(Locale.US));
        if (a2 == null) {
            return null;
        }
        ja.a(3, a, "Creating ad network view for type: " + obj);
        ac a3 = a2.a(context, flurryAdModule, mVar, adUnit);
        if (a3 == null) {
            ja.b(a, "Cannot create ad network view for type: " + obj);
            return null;
        }
        if (a3 != null) {
            a3.setAdUnit(adUnit);
        }
        return a3;
    }

    private static Map<String, am> a() {
        HashMap hashMap = new HashMap();
        hashMap.put("Admob".toUpperCase(Locale.US), new df());
        hashMap.put("Millennial Media".toUpperCase(Locale.US), new dw());
        hashMap.put("InMobi".toUpperCase(Locale.US), new dm());
        hashMap.put("Mobclix".toUpperCase(Locale.US), new eb());
        hashMap.put("Jumptap".toUpperCase(Locale.US), new dr());
        return Collections.unmodifiableMap(hashMap);
    }

    private static am a(String str) {
        return b.get(str);
    }
}
