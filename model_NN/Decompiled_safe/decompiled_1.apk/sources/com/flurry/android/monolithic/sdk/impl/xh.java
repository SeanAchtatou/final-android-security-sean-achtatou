package com.flurry.android.monolithic.sdk.impl;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class xh extends xg {
    private static final xp[] l = new xp[0];
    protected final Class<?> a;
    protected final List<Class<?>> b;
    protected final py c;
    protected final qg d;
    protected final Class<?> e;
    protected xp f;
    protected xi g;
    protected List<xi> h;
    protected List<xl> i;
    protected xm j;
    protected List<xj> k;

    private xh(Class<?> cls, List<Class<?>> list, py pyVar, qg qgVar, xp xpVar) {
        this.a = cls;
        this.b = list;
        this.c = pyVar;
        this.d = qgVar;
        this.e = this.d == null ? null : this.d.a(this.a);
        this.f = xpVar;
    }

    public static xh a(Class<?> cls, py pyVar, qg qgVar) {
        xh xhVar = new xh(cls, adz.a(cls, (Class<?>) null), pyVar, qgVar, null);
        xhVar.m();
        return xhVar;
    }

    public static xh b(Class<?> cls, py pyVar, qg qgVar) {
        xh xhVar = new xh(cls, Collections.emptyList(), pyVar, qgVar, null);
        xhVar.m();
        return xhVar;
    }

    /* renamed from: e */
    public Class<?> a() {
        return this.a;
    }

    public String b() {
        return this.a.getName();
    }

    public <A extends Annotation> A a(Class<A> cls) {
        if (this.f == null) {
            return null;
        }
        return this.f.a(cls);
    }

    public Type c() {
        return this.a;
    }

    public Class<?> d() {
        return this.a;
    }

    public ado f() {
        return this.f;
    }

    public boolean g() {
        return this.f.a() > 0;
    }

    public xi h() {
        return this.g;
    }

    public List<xi> i() {
        if (this.h == null) {
            return Collections.emptyList();
        }
        return this.h;
    }

    public List<xl> j() {
        if (this.i == null) {
            return Collections.emptyList();
        }
        return this.i;
    }

    public Iterable<xl> k() {
        return this.j;
    }

    public xl a(String str, Class<?>[] clsArr) {
        return this.j.a(str, clsArr);
    }

    public Iterable<xj> l() {
        if (this.k == null) {
            return Collections.emptyList();
        }
        return this.k;
    }

    public void m() {
        this.f = new xp();
        if (this.c != null) {
            if (this.e != null) {
                a(this.f, this.a, this.e);
            }
            for (Annotation annotation : this.a.getDeclaredAnnotations()) {
                if (this.c.a(annotation)) {
                    this.f.a(annotation);
                }
            }
            for (Class next : this.b) {
                a(this.f, next);
                for (Annotation annotation2 : next.getDeclaredAnnotations()) {
                    if (this.c.a(annotation2)) {
                        this.f.a(annotation2);
                    }
                }
            }
            a(this.f, Object.class);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.xh.a(java.lang.reflect.Constructor<?>, boolean):com.flurry.android.monolithic.sdk.impl.xi
     arg types: [java.lang.reflect.Constructor<?>, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.xh.a(java.lang.String, java.lang.Class<?>[]):com.flurry.android.monolithic.sdk.impl.xl
      com.flurry.android.monolithic.sdk.impl.xh.a(com.flurry.android.monolithic.sdk.impl.xp, java.lang.Class<?>):void
      com.flurry.android.monolithic.sdk.impl.xh.a(java.lang.Class<?>, java.util.Map<java.lang.String, com.flurry.android.monolithic.sdk.impl.xj>):void
      com.flurry.android.monolithic.sdk.impl.xh.a(java.lang.reflect.Method, com.flurry.android.monolithic.sdk.impl.xl):void
      com.flurry.android.monolithic.sdk.impl.xh.a(java.util.Map<java.lang.String, com.flurry.android.monolithic.sdk.impl.xj>, java.lang.Class<?>):void
      com.flurry.android.monolithic.sdk.impl.xh.a(java.lang.reflect.Method, com.flurry.android.monolithic.sdk.impl.xz):boolean
      com.flurry.android.monolithic.sdk.impl.xh.a(java.lang.reflect.Constructor<?>, boolean):com.flurry.android.monolithic.sdk.impl.xi */
    public void a(boolean z) {
        this.h = null;
        Constructor<?>[] declaredConstructors = this.a.getDeclaredConstructors();
        for (Constructor<?> constructor : declaredConstructors) {
            if (constructor.getParameterTypes().length == 0) {
                this.g = a(constructor, true);
            } else if (z) {
                if (this.h == null) {
                    this.h = new ArrayList(Math.max(10, declaredConstructors.length));
                }
                this.h.add(a(constructor, false));
            }
        }
        if (!(this.e == null || (this.g == null && this.h == null))) {
            c(this.e);
        }
        if (this.c != null) {
            if (this.g != null && this.c.a(this.g)) {
                this.g = null;
            }
            if (this.h != null) {
                int size = this.h.size();
                while (true) {
                    int i2 = size - 1;
                    if (i2 < 0) {
                        break;
                    } else if (this.c.a(this.h.get(i2))) {
                        this.h.remove(i2);
                        size = i2;
                    } else {
                        size = i2;
                    }
                }
            }
        }
        this.i = null;
        if (z) {
            for (Method method : this.a.getDeclaredMethods()) {
                if (Modifier.isStatic(method.getModifiers()) && method.getParameterTypes().length >= 1) {
                    if (this.i == null) {
                        this.i = new ArrayList(8);
                    }
                    this.i.add(b(method));
                }
            }
            if (!(this.e == null || this.i == null)) {
                d(this.e);
            }
            if (this.c != null && this.i != null) {
                int size2 = this.i.size();
                while (true) {
                    int i3 = size2 - 1;
                    if (i3 < 0) {
                        return;
                    }
                    if (this.c.a(this.i.get(i3))) {
                        this.i.remove(i3);
                        size2 = i3;
                    } else {
                        size2 = i3;
                    }
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.xh.a(java.lang.reflect.Method, com.flurry.android.monolithic.sdk.impl.xl, boolean):void
     arg types: [java.lang.reflect.Method, com.flurry.android.monolithic.sdk.impl.xl, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.xh.a(java.lang.Class<?>, com.flurry.android.monolithic.sdk.impl.py, com.flurry.android.monolithic.sdk.impl.qg):com.flurry.android.monolithic.sdk.impl.xh
      com.flurry.android.monolithic.sdk.impl.xh.a(com.flurry.android.monolithic.sdk.impl.xp, java.lang.Class<?>, java.lang.Class<?>):void
      com.flurry.android.monolithic.sdk.impl.xh.a(java.lang.reflect.Constructor<?>, com.flurry.android.monolithic.sdk.impl.xi, boolean):void
      com.flurry.android.monolithic.sdk.impl.xh.a(java.lang.reflect.Method, com.flurry.android.monolithic.sdk.impl.xl, boolean):void */
    public void a(xz xzVar) {
        Class<Object> cls = Object.class;
        this.j = new xm();
        xm xmVar = new xm();
        a(this.a, xzVar, this.j, this.e, xmVar);
        for (Class next : this.b) {
            a(next, xzVar, this.j, this.d == null ? null : this.d.a(next), xmVar);
        }
        if (this.d != null) {
            Class<Object> cls2 = Object.class;
            Class<?> a2 = this.d.a(cls);
            if (a2 != null) {
                a(xzVar, this.j, a2, xmVar);
            }
        }
        if (this.c != null && !xmVar.a()) {
            Iterator<xl> it = xmVar.iterator();
            while (it.hasNext()) {
                xl next2 = it.next();
                try {
                    Method declaredMethod = Object.class.getDeclaredMethod(next2.b(), next2.m());
                    if (declaredMethod != null) {
                        xl a3 = a(declaredMethod);
                        a(next2.a(), a3, false);
                        this.j.a(a3);
                    }
                } catch (Exception e2) {
                }
            }
        }
    }

    public void n() {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        a(linkedHashMap, this.a);
        if (linkedHashMap.isEmpty()) {
            this.k = Collections.emptyList();
            return;
        }
        this.k = new ArrayList(linkedHashMap.size());
        this.k.addAll(linkedHashMap.values());
    }

    /* access modifiers changed from: protected */
    public void a(xp xpVar, Class<?> cls) {
        if (this.d != null) {
            a(xpVar, cls, this.d.a(cls));
        }
    }

    /* access modifiers changed from: protected */
    public void a(xp xpVar, Class<?> cls, Class<?> cls2) {
        if (cls2 != null) {
            for (Annotation annotation : cls2.getDeclaredAnnotations()) {
                if (this.c.a(annotation)) {
                    xpVar.a(annotation);
                }
            }
            for (Class<?> declaredAnnotations : adz.a(cls2, cls)) {
                for (Annotation annotation2 : declaredAnnotations.getDeclaredAnnotations()) {
                    if (this.c.a(annotation2)) {
                        xpVar.a(annotation2);
                    }
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.xh.a(java.lang.reflect.Constructor<?>, com.flurry.android.monolithic.sdk.impl.xi, boolean):void
     arg types: [java.lang.reflect.Constructor<?>, com.flurry.android.monolithic.sdk.impl.xi, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.xh.a(java.lang.Class<?>, com.flurry.android.monolithic.sdk.impl.py, com.flurry.android.monolithic.sdk.impl.qg):com.flurry.android.monolithic.sdk.impl.xh
      com.flurry.android.monolithic.sdk.impl.xh.a(com.flurry.android.monolithic.sdk.impl.xp, java.lang.Class<?>, java.lang.Class<?>):void
      com.flurry.android.monolithic.sdk.impl.xh.a(java.lang.reflect.Method, com.flurry.android.monolithic.sdk.impl.xl, boolean):void
      com.flurry.android.monolithic.sdk.impl.xh.a(java.lang.reflect.Constructor<?>, com.flurry.android.monolithic.sdk.impl.xi, boolean):void */
    /* access modifiers changed from: protected */
    public void c(Class<?> cls) {
        xy[] xyVarArr;
        xy[] xyVarArr2 = null;
        int size = this.h == null ? 0 : this.h.size();
        for (Constructor<?> constructor : cls.getDeclaredConstructors()) {
            if (constructor.getParameterTypes().length != 0) {
                if (xyVarArr2 == null) {
                    xyVarArr = new xy[size];
                    for (int i2 = 0; i2 < size; i2++) {
                        xyVarArr[i2] = new xy(this.h.get(i2).a());
                    }
                } else {
                    xyVarArr = xyVarArr2;
                }
                xy xyVar = new xy(constructor);
                int i3 = 0;
                while (true) {
                    if (i3 < size) {
                        if (xyVar.equals(xyVarArr[i3])) {
                            a(constructor, this.h.get(i3), true);
                            xyVarArr2 = xyVarArr;
                            break;
                        }
                        i3++;
                    } else {
                        xyVarArr2 = xyVarArr;
                        break;
                    }
                }
            } else if (this.g != null) {
                a(constructor, this.g, false);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.xh.a(java.lang.reflect.Method, com.flurry.android.monolithic.sdk.impl.xl, boolean):void
     arg types: [java.lang.reflect.Method, com.flurry.android.monolithic.sdk.impl.xl, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.xh.a(java.lang.Class<?>, com.flurry.android.monolithic.sdk.impl.py, com.flurry.android.monolithic.sdk.impl.qg):com.flurry.android.monolithic.sdk.impl.xh
      com.flurry.android.monolithic.sdk.impl.xh.a(com.flurry.android.monolithic.sdk.impl.xp, java.lang.Class<?>, java.lang.Class<?>):void
      com.flurry.android.monolithic.sdk.impl.xh.a(java.lang.reflect.Constructor<?>, com.flurry.android.monolithic.sdk.impl.xi, boolean):void
      com.flurry.android.monolithic.sdk.impl.xh.a(java.lang.reflect.Method, com.flurry.android.monolithic.sdk.impl.xl, boolean):void */
    /* access modifiers changed from: protected */
    public void d(Class<?> cls) {
        xy[] xyVarArr;
        xy[] xyVarArr2 = null;
        int size = this.i.size();
        for (Method method : cls.getDeclaredMethods()) {
            if (Modifier.isStatic(method.getModifiers()) && method.getParameterTypes().length != 0) {
                if (xyVarArr2 == null) {
                    xyVarArr = new xy[size];
                    for (int i2 = 0; i2 < size; i2++) {
                        xyVarArr[i2] = new xy(this.i.get(i2).a());
                    }
                } else {
                    xyVarArr = xyVarArr2;
                }
                xy xyVar = new xy(method);
                int i3 = 0;
                while (true) {
                    if (i3 < size) {
                        if (xyVar.equals(xyVarArr[i3])) {
                            a(method, this.i.get(i3), true);
                            xyVarArr2 = xyVarArr;
                            break;
                        }
                        i3++;
                    } else {
                        xyVarArr2 = xyVarArr;
                        break;
                    }
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.xh.a(java.lang.reflect.Method, com.flurry.android.monolithic.sdk.impl.xl, boolean):void
     arg types: [java.lang.reflect.Method, com.flurry.android.monolithic.sdk.impl.xl, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.xh.a(java.lang.Class<?>, com.flurry.android.monolithic.sdk.impl.py, com.flurry.android.monolithic.sdk.impl.qg):com.flurry.android.monolithic.sdk.impl.xh
      com.flurry.android.monolithic.sdk.impl.xh.a(com.flurry.android.monolithic.sdk.impl.xp, java.lang.Class<?>, java.lang.Class<?>):void
      com.flurry.android.monolithic.sdk.impl.xh.a(java.lang.reflect.Constructor<?>, com.flurry.android.monolithic.sdk.impl.xi, boolean):void
      com.flurry.android.monolithic.sdk.impl.xh.a(java.lang.reflect.Method, com.flurry.android.monolithic.sdk.impl.xl, boolean):void */
    /* access modifiers changed from: protected */
    public void a(Class<?> cls, xz xzVar, xm xmVar, Class<?> cls2, xm xmVar2) {
        if (cls2 != null) {
            a(xzVar, xmVar, cls2, xmVar2);
        }
        if (cls != null) {
            for (Method method : cls.getDeclaredMethods()) {
                if (a(method, xzVar)) {
                    xl b2 = xmVar.b(method);
                    if (b2 == null) {
                        xl a2 = a(method);
                        xmVar.a(a2);
                        xl a3 = xmVar2.a(method);
                        if (a3 != null) {
                            a(a3.a(), a2, false);
                        }
                    } else {
                        a(method, b2);
                        if (b2.h().isInterface() && !method.getDeclaringClass().isInterface()) {
                            xmVar.a(b2.a(method));
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(xz xzVar, xm xmVar, Class<?> cls, xm xmVar2) {
        for (Method method : cls.getDeclaredMethods()) {
            if (a(method, xzVar)) {
                xl b2 = xmVar.b(method);
                if (b2 != null) {
                    a(method, b2);
                } else {
                    xmVar2.a(a(method));
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(Map<String, xj> map, Class<?> cls) {
        Class<?> a2;
        Class<? super Object> superclass = cls.getSuperclass();
        if (superclass != null) {
            a(map, superclass);
            for (Field field : cls.getDeclaredFields()) {
                if (b(field)) {
                    map.put(field.getName(), a(field));
                }
            }
            if (this.d != null && (a2 = this.d.a(cls)) != null) {
                a(a2, map);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(Class<?> cls, Map<String, xj> map) {
        xj xjVar;
        for (Field field : cls.getDeclaredFields()) {
            if (b(field) && (xjVar = map.get(field.getName())) != null) {
                for (Annotation annotation : field.getDeclaredAnnotations()) {
                    if (this.c.a(annotation)) {
                        xjVar.a(annotation);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public xl a(Method method) {
        if (this.c == null) {
            return new xl(method, o(), null);
        }
        return new xl(method, a(method.getDeclaredAnnotations()), null);
    }

    /* access modifiers changed from: protected */
    public xi a(Constructor<?> constructor, boolean z) {
        xp[] xpVarArr;
        Annotation[][] annotationArr;
        if (this.c == null) {
            return new xi(constructor, o(), a(constructor.getParameterTypes().length));
        }
        if (z) {
            return new xi(constructor, a(constructor.getDeclaredAnnotations()), null);
        }
        Annotation[][] parameterAnnotations = constructor.getParameterAnnotations();
        int length = constructor.getParameterTypes().length;
        if (length != parameterAnnotations.length) {
            Class<?> declaringClass = constructor.getDeclaringClass();
            if (declaringClass.isEnum() && length == parameterAnnotations.length + 2) {
                annotationArr = new Annotation[(parameterAnnotations.length + 2)][];
                System.arraycopy(parameterAnnotations, 0, annotationArr, 2, parameterAnnotations.length);
                xpVarArr = a(annotationArr);
            } else if (!declaringClass.isMemberClass() || length != parameterAnnotations.length + 1) {
                annotationArr = parameterAnnotations;
                xpVarArr = null;
            } else {
                annotationArr = new Annotation[(parameterAnnotations.length + 1)][];
                System.arraycopy(parameterAnnotations, 0, annotationArr, 1, parameterAnnotations.length);
                xpVarArr = a(annotationArr);
            }
            if (xpVarArr == null) {
                throw new IllegalStateException("Internal error: constructor for " + constructor.getDeclaringClass().getName() + " has mismatch: " + length + " parameters; " + annotationArr.length + " sets of annotations");
            }
        } else {
            xpVarArr = a(parameterAnnotations);
        }
        return new xi(constructor, a(constructor.getDeclaredAnnotations()), xpVarArr);
    }

    /* access modifiers changed from: protected */
    public xl b(Method method) {
        if (this.c == null) {
            return new xl(method, o(), a(method.getParameterTypes().length));
        }
        return new xl(method, a(method.getDeclaredAnnotations()), a(method.getParameterAnnotations()));
    }

    /* access modifiers changed from: protected */
    public xj a(Field field) {
        if (this.c == null) {
            return new xj(field, o());
        }
        return new xj(field, a(field.getDeclaredAnnotations()));
    }

    /* access modifiers changed from: protected */
    public xp[] a(Annotation[][] annotationArr) {
        int length = annotationArr.length;
        xp[] xpVarArr = new xp[length];
        for (int i2 = 0; i2 < length; i2++) {
            xpVarArr[i2] = a(annotationArr[i2]);
        }
        return xpVarArr;
    }

    /* access modifiers changed from: protected */
    public xp a(Annotation[] annotationArr) {
        xp xpVar = new xp();
        if (annotationArr != null) {
            for (Annotation annotation : annotationArr) {
                if (this.c.a(annotation)) {
                    xpVar.b(annotation);
                }
            }
        }
        return xpVar;
    }

    private xp o() {
        return new xp();
    }

    private xp[] a(int i2) {
        if (i2 == 0) {
            return l;
        }
        xp[] xpVarArr = new xp[i2];
        for (int i3 = 0; i3 < i2; i3++) {
            xpVarArr[i3] = o();
        }
        return xpVarArr;
    }

    /* access modifiers changed from: protected */
    public boolean a(Method method, xz xzVar) {
        if (xzVar != null && !xzVar.a(method)) {
            return false;
        }
        if (method.isSynthetic() || method.isBridge()) {
            return false;
        }
        return true;
    }

    private boolean b(Field field) {
        if (field.isSynthetic()) {
            return false;
        }
        int modifiers = field.getModifiers();
        if (Modifier.isStatic(modifiers) || Modifier.isTransient(modifiers)) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void a(Constructor<?> constructor, xi xiVar, boolean z) {
        for (Annotation annotation : constructor.getDeclaredAnnotations()) {
            if (this.c.a(annotation)) {
                xiVar.a(annotation);
            }
        }
        if (z) {
            Annotation[][] parameterAnnotations = constructor.getParameterAnnotations();
            int length = parameterAnnotations.length;
            for (int i2 = 0; i2 < length; i2++) {
                for (Annotation a2 : parameterAnnotations[i2]) {
                    xiVar.a(i2, a2);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(Method method, xl xlVar, boolean z) {
        for (Annotation annotation : method.getDeclaredAnnotations()) {
            if (this.c.a(annotation)) {
                xlVar.a(annotation);
            }
        }
        if (z) {
            Annotation[][] parameterAnnotations = method.getParameterAnnotations();
            int length = parameterAnnotations.length;
            for (int i2 = 0; i2 < length; i2++) {
                for (Annotation a2 : parameterAnnotations[i2]) {
                    xlVar.a(i2, a2);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(Method method, xl xlVar) {
        for (Annotation annotation : method.getDeclaredAnnotations()) {
            if (this.c.a(annotation)) {
                xlVar.b(annotation);
            }
        }
    }

    public String toString() {
        return "[AnnotedClass " + this.a.getName() + "]";
    }
}
