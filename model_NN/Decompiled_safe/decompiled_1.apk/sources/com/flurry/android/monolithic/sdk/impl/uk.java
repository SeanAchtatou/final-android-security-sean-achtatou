package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.EnumMap;

public class uk extends vo<EnumMap<?, ?>> {
    protected final Class<?> a;
    protected final qu<Enum<?>> b;
    protected final qu<Object> c;

    /* JADX WARN: Type inference failed for: r3v0, types: [com.flurry.android.monolithic.sdk.impl.qu<java.lang.Enum<?>>, com.flurry.android.monolithic.sdk.impl.qu<?>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public uk(java.lang.Class<?> r2, com.flurry.android.monolithic.sdk.impl.qu<?> r3, com.flurry.android.monolithic.sdk.impl.qu<java.lang.Object> r4) {
        /*
            r1 = this;
            java.lang.Class<java.util.EnumMap> r0 = java.util.EnumMap.class
            r1.<init>(r0)
            r1.a = r2
            r1.b = r3
            r1.c = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.uk.<init>(java.lang.Class, com.flurry.android.monolithic.sdk.impl.qu, com.flurry.android.monolithic.sdk.impl.qu):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.EnumMap.put(?, ?):V}
     arg types: [java.lang.Enum, java.lang.Object]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Object, java.lang.Object):java.lang.Object}
      ClspMth{java.util.AbstractMap.put(?, ?):V}
      ClspMth{java.util.Map.put(?, ?):V}
      ClspMth{java.util.EnumMap.put(?, ?):V} */
    /* renamed from: b */
    public EnumMap<?, ?> a(ow owVar, qm qmVar) throws IOException, oz {
        if (owVar.e() != pb.START_OBJECT) {
            throw qmVar.b(EnumMap.class);
        }
        EnumMap<?, ?> d = d();
        while (owVar.b() != pb.END_OBJECT) {
            Enum a2 = this.b.a(owVar, qmVar);
            if (a2 == null) {
                throw qmVar.b(this.a, "value not one of declared Enum instance names");
            }
            d.put((Object) a2, (Object) (owVar.b() == pb.VALUE_NULL ? null : this.c.a(owVar, qmVar)));
        }
        return d;
    }

    public Object a(ow owVar, qm qmVar, rw rwVar) throws IOException, oz {
        return rwVar.a(owVar, qmVar);
    }

    private EnumMap<?, ?> d() {
        return new EnumMap<>(this.a);
    }
}
