package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
public class acy extends abt<Object> {
    public static final acy a = new acy();

    public acy() {
        super(Object.class);
    }

    public void a(Object obj, or orVar, ru ruVar) throws IOException, oq {
        orVar.b(obj.toString());
    }

    public void a(Object obj, or orVar, ru ruVar, rx rxVar) throws IOException, oq {
        rxVar.a(obj, orVar);
        a(obj, orVar, ruVar);
        rxVar.d(obj, orVar);
    }
}
