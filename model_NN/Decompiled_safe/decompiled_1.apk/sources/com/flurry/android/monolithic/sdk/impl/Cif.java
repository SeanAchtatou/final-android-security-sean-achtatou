package com.flurry.android.monolithic.sdk.impl;

/* renamed from: com.flurry.android.monolithic.sdk.impl.if  reason: invalid class name */
public final class Cif {
    private static final String a = Cif.class.getSimpleName();

    public static ih a() {
        try {
            return (ih) Class.forName("com.flurry.android.impl.ads.FlurryAdModule").getDeclaredMethod("getInstance", new Class[0]).invoke(null, new Object[0]);
        } catch (Exception e) {
            ja.a(5, a, "FlurryAdModule is not available:", e);
            return null;
        }
    }
}
