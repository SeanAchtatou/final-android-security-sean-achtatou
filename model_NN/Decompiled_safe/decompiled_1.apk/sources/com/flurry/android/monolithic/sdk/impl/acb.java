package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
public final class acb extends abw<float[]> {
    public acb() {
        this(null);
    }

    public acb(rx rxVar) {
        super(float[].class, rxVar, null);
    }

    public abc<?> a(rx rxVar) {
        return new acb(rxVar);
    }

    /* renamed from: a */
    public void b(float[] fArr, or orVar, ru ruVar) throws IOException, oq {
        for (float a : fArr) {
            orVar.a(a);
        }
    }
}
