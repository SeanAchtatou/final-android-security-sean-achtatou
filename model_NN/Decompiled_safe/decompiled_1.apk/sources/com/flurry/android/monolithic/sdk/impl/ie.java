package com.flurry.android.monolithic.sdk.impl;

public enum ie {
    PhoneId(1, true),
    Sha1Imei(5, false);
    
    public final int c;
    public final boolean d;

    private ie(int i, boolean z) {
        this.c = i;
        this.d = z;
    }
}
