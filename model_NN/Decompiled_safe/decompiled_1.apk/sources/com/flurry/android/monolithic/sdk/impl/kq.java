package com.flurry.android.monolithic.sdk.impl;

import com.ideaworks3d.marmalade.s3eAndroidGooglePlayBilling.util.IabHelper;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class kq {
    private static final kq a = new kq();
    private static final ji b = ji.a(kj.STRING);

    public static kq a() {
        return a;
    }

    protected kq() {
    }

    public lw a(ji jiVar) {
        return new kx(jiVar, jiVar, this);
    }

    public String a(Object obj) {
        StringBuilder sb = new StringBuilder();
        a(obj, sb);
        return sb.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.kq.a(java.lang.Object, java.lang.StringBuilder):void
     arg types: [java.lang.String, java.lang.StringBuilder]
     candidates:
      com.flurry.android.monolithic.sdk.impl.kq.a(java.lang.String, java.lang.StringBuilder):void
      com.flurry.android.monolithic.sdk.impl.kq.a(com.flurry.android.monolithic.sdk.impl.ji, java.lang.Object):int
      com.flurry.android.monolithic.sdk.impl.kq.a(java.lang.Object, com.flurry.android.monolithic.sdk.impl.ji):java.lang.Object
      com.flurry.android.monolithic.sdk.impl.kq.a(java.lang.Object, java.lang.StringBuilder):void */
    /* access modifiers changed from: protected */
    public void a(Object obj, StringBuilder sb) {
        int i = 0;
        if (d(obj)) {
            sb.append("{");
            ji e = e(obj);
            for (js next : e.b()) {
                a((Object) next.a(), sb);
                sb.append(": ");
                a(a(obj, next.a(), next.b()), sb);
                int i2 = i + 1;
                if (i2 < e.b().size()) {
                    sb.append(", ");
                }
                i = i2;
            }
            sb.append("}");
        } else if (obj instanceof Collection) {
            Collection<Object> collection = (Collection) obj;
            sb.append("[");
            long size = (long) (collection.size() - 1);
            for (Object a2 : collection) {
                a(a2, sb);
                int i3 = i + 1;
                if (((long) i) < size) {
                    sb.append(", ");
                }
                i = i3;
            }
            sb.append("]");
        } else if (obj instanceof Map) {
            sb.append("{");
            Map map = (Map) obj;
            int i4 = 0;
            for (Map.Entry entry : map.entrySet()) {
                a(entry.getKey(), sb);
                sb.append(": ");
                a(entry.getValue(), sb);
                int i5 = i4 + 1;
                if (i5 < map.size()) {
                    sb.append(", ");
                }
                i4 = i5;
            }
            sb.append("}");
        } else if ((obj instanceof CharSequence) || (obj instanceof lc)) {
            sb.append("\"");
            a(obj.toString(), sb);
            sb.append("\"");
        } else if (obj instanceof ByteBuffer) {
            sb.append("{\"bytes\": \"");
            ByteBuffer byteBuffer = (ByteBuffer) obj;
            for (int position = byteBuffer.position(); position < byteBuffer.limit(); position++) {
                sb.append((char) byteBuffer.get(position));
            }
            sb.append("\"}");
        } else {
            sb.append(obj);
        }
    }

    private void a(String str, StringBuilder sb) {
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            switch (charAt) {
                case IabHelper.BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED /*8*/:
                    sb.append("\\b");
                    break;
                case 9:
                    sb.append("\\t");
                    break;
                case 10:
                    sb.append("\\n");
                    break;
                case 12:
                    sb.append("\\f");
                    break;
                case 13:
                    sb.append("\\r");
                    break;
                case '\"':
                    sb.append("\\\"");
                    break;
                case '/':
                    sb.append("\\/");
                    break;
                case '\\':
                    sb.append("\\\\");
                    break;
                default:
                    if ((charAt >= 0 && charAt <= 31) || ((charAt >= 127 && charAt <= 159) || (charAt >= 8192 && charAt <= 8447))) {
                        Integer.toHexString(charAt);
                        sb.append("\\u");
                        for (int i2 = 0; i2 < 4 - sb.length(); i2++) {
                            sb.append('0');
                        }
                        sb.append(str.toUpperCase());
                        break;
                    } else {
                        sb.append(charAt);
                        break;
                    }
            }
        }
    }

    public void a(Object obj, String str, int i, Object obj2) {
        ((lf) obj).a(i, obj2);
    }

    public Object a(Object obj, String str, int i) {
        return ((lf) obj).a(i);
    }

    /* access modifiers changed from: protected */
    public Object a(Object obj, ji jiVar) {
        return null;
    }

    /* access modifiers changed from: protected */
    public void a(Object obj, String str, int i, Object obj2, Object obj3) {
        a(obj, str, i, obj2);
    }

    /* access modifiers changed from: protected */
    public Object b(Object obj, String str, int i, Object obj2) {
        return a(obj, str, i);
    }

    public int a(ji jiVar, Object obj) {
        Integer e = jiVar.e(b(obj));
        if (e != null) {
            return e.intValue();
        }
        throw new km(jiVar, obj);
    }

    /* access modifiers changed from: protected */
    public String b(Object obj) {
        if (obj == null) {
            return kj.NULL.a();
        }
        if (d(obj)) {
            return e(obj).g();
        }
        if (f(obj)) {
            return g(obj).g();
        }
        if (c(obj)) {
            return kj.ARRAY.a();
        }
        if (h(obj)) {
            return kj.MAP.a();
        }
        if (i(obj)) {
            return j(obj).g();
        }
        if (k(obj)) {
            return kj.STRING.a();
        }
        if (l(obj)) {
            return kj.BYTES.a();
        }
        if (obj instanceof Integer) {
            return kj.INT.a();
        }
        if (obj instanceof Long) {
            return kj.LONG.a();
        }
        if (obj instanceof Float) {
            return kj.FLOAT.a();
        }
        if (obj instanceof Double) {
            return kj.DOUBLE.a();
        }
        if (obj instanceof Boolean) {
            return kj.BOOLEAN.a();
        }
        throw new jg("Unknown datum type: " + obj);
    }

    /* access modifiers changed from: protected */
    public boolean c(Object obj) {
        return obj instanceof Collection;
    }

    /* access modifiers changed from: protected */
    public boolean d(Object obj) {
        return obj instanceof lf;
    }

    /* access modifiers changed from: protected */
    public ji e(Object obj) {
        return ((kp) obj).a();
    }

    /* access modifiers changed from: protected */
    public boolean f(Object obj) {
        return obj instanceof lc;
    }

    /* access modifiers changed from: protected */
    public ji g(Object obj) {
        return ((kp) obj).a();
    }

    /* access modifiers changed from: protected */
    public boolean h(Object obj) {
        return obj instanceof Map;
    }

    /* access modifiers changed from: protected */
    public boolean i(Object obj) {
        return obj instanceof ld;
    }

    /* access modifiers changed from: protected */
    public ji j(Object obj) {
        return ((kp) obj).a();
    }

    /* access modifiers changed from: protected */
    public boolean k(Object obj) {
        return obj instanceof CharSequence;
    }

    /* access modifiers changed from: protected */
    public boolean l(Object obj) {
        return obj instanceof ByteBuffer;
    }

    public int b(Object obj, ji jiVar) {
        if (obj == null) {
            return 0;
        }
        int i = 1;
        switch (kr.a[jiVar.a().ordinal()]) {
            case 1:
                int i2 = 1;
                for (js next : jiVar.b()) {
                    if (next.f() != jt.IGNORE) {
                        i2 = a(i2, a(obj, next.a(), next.b()), next.c());
                    }
                }
                return i2;
            case 2:
                return jiVar.c(obj.toString());
            case 3:
                ji i3 = jiVar.i();
                for (Object a2 : (Collection) obj) {
                    i = a(i, a2, i3);
                }
                return i;
            case 4:
            case 6:
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED /*8*/:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            default:
                return obj.hashCode();
            case 5:
                return b(obj, jiVar.k().get(a(jiVar, obj)));
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED /*7*/:
                return (obj instanceof nw ? obj : new nw(obj.toString())).hashCode();
            case 14:
                return 0;
        }
    }

    /* access modifiers changed from: protected */
    public int a(int i, Object obj, ji jiVar) {
        return (i * 31) + b(obj, jiVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.kq.a(java.lang.Object, java.lang.Object, com.flurry.android.monolithic.sdk.impl.ji, boolean):int
     arg types: [java.lang.Object, java.lang.Object, com.flurry.android.monolithic.sdk.impl.ji, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.kq.a(java.lang.Object, java.lang.String, int, java.lang.Object):void
      com.flurry.android.monolithic.sdk.impl.kq.a(java.lang.Object, java.lang.Object, com.flurry.android.monolithic.sdk.impl.ji, boolean):int */
    public int a(Object obj, Object obj2, ji jiVar) {
        return a(obj, obj2, jiVar, false);
    }

    /* access modifiers changed from: protected */
    public int a(Object obj, Object obj2, ji jiVar, boolean z) {
        nw nwVar;
        if (obj == obj2) {
            return 0;
        }
        switch (kr.a[jiVar.a().ordinal()]) {
            case 1:
                for (js next : jiVar.b()) {
                    if (next.f() != jt.IGNORE) {
                        int b2 = next.b();
                        String a2 = next.a();
                        int a3 = a(a(obj, a2, b2), a(obj2, a2, b2), next.c(), z);
                        if (a3 != 0) {
                            return next.f() == jt.DESCENDING ? -a3 : a3;
                        }
                    }
                }
                return 0;
            case 2:
                return jiVar.c(obj.toString()) - jiVar.c(obj2.toString());
            case 3:
                Iterator it = ((Collection) obj).iterator();
                Iterator it2 = ((Collection) obj2).iterator();
                ji i = jiVar.i();
                while (it.hasNext() && it2.hasNext()) {
                    int a4 = a(it.next(), it2.next(), i, z);
                    if (a4 != 0) {
                        return a4;
                    }
                }
                if (it.hasNext()) {
                    return 1;
                }
                return it2.hasNext() ? -1 : 0;
            case 4:
                if (z) {
                    return ((Map) obj).equals(obj2) ? 0 : 1;
                }
                throw new jg("Can't compare maps!");
            case 5:
                int a5 = a(jiVar, obj);
                int a6 = a(jiVar, obj2);
                return a5 == a6 ? a(obj, obj2, jiVar.k().get(a5), z) : a5 - a6;
            case 6:
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED /*8*/:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            default:
                return ((Comparable) obj).compareTo(obj2);
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED /*7*/:
                nw nwVar2 = obj instanceof nw ? (nw) obj : new nw(obj.toString());
                if (obj2 instanceof nw) {
                    nwVar = (nw) obj2;
                } else {
                    nwVar = new nw(obj2.toString());
                }
                return nwVar2.compareTo(nwVar);
            case 14:
                return 0;
        }
    }

    public Object b(ji jiVar, Object obj) {
        if (obj == null) {
            return null;
        }
        switch (kr.a[jiVar.a().ordinal()]) {
            case 1:
                lf lfVar = (lf) obj;
                lf lfVar2 = (lf) d(null, jiVar);
                for (js next : jiVar.b()) {
                    lfVar2.a(next.b(), b(next.c(), lfVar.a(next.b())));
                }
                return lfVar2;
            case 2:
                return obj;
            case 3:
                List<Object> list = (List) obj;
                ks ksVar = new ks(list.size(), jiVar);
                for (Object b2 : list) {
                    ksVar.add(b(jiVar.i(), b2));
                }
                return ksVar;
            case 4:
                Map map = (Map) obj;
                HashMap hashMap = new HashMap(map.size());
                for (Map.Entry entry : map.entrySet()) {
                    hashMap.put((CharSequence) b(b, entry.getKey()), b(jiVar.j(), entry.getValue()));
                }
                return hashMap;
            case 5:
                return b(jiVar.k().get(a(jiVar, obj)), obj);
            case 6:
                return a((Object) null, ((ld) obj).b(), jiVar);
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED /*7*/:
                if (obj instanceof String) {
                    return obj;
                }
                if (obj instanceof nw) {
                    return new nw((nw) obj);
                }
                return new nw(obj.toString());
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED /*8*/:
                ByteBuffer byteBuffer = (ByteBuffer) obj;
                byte[] bArr = new byte[byteBuffer.capacity()];
                byteBuffer.rewind();
                byteBuffer.get(bArr);
                byteBuffer.rewind();
                return ByteBuffer.wrap(bArr);
            case 9:
                return new Integer(((Integer) obj).intValue());
            case 10:
                return new Long(((Long) obj).longValue());
            case 11:
                return new Float(((Float) obj).floatValue());
            case 12:
                return new Double(((Double) obj).doubleValue());
            case 13:
                return new Boolean(((Boolean) obj).booleanValue());
            case 14:
                return null;
            default:
                throw new jg("Deep copy failed for schema \"" + jiVar + "\" and value \"" + obj + "\"");
        }
    }

    public Object c(Object obj, ji jiVar) {
        if (!(obj instanceof ld) || ((ld) obj).b().length != jiVar.l()) {
            return new kv(jiVar);
        }
        return obj;
    }

    public Object a(Object obj, byte[] bArr, ji jiVar) {
        ld ldVar = (ld) c(obj, jiVar);
        System.arraycopy(bArr, 0, ldVar.b(), 0, jiVar.l());
        return ldVar;
    }

    public Object d(Object obj, ji jiVar) {
        if (obj instanceof lf) {
            lf lfVar = (lf) obj;
            if (lfVar.a() == jiVar) {
                return lfVar;
            }
        }
        return new kw(jiVar);
    }
}
