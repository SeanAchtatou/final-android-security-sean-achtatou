package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;

class bl extends jf {
    final /* synthetic */ String a;
    final /* synthetic */ Context b;
    final /* synthetic */ boolean c;
    final /* synthetic */ AdUnit d;
    final /* synthetic */ bi e;

    bl(bi biVar, String str, Context context, boolean z, AdUnit adUnit) {
        this.e = biVar;
        this.a = str;
        this.b = context;
        this.c = z;
        this.d = adUnit;
    }

    public void a() {
        String b2 = this.e.b(this.a);
        if (b2 != null) {
            this.e.a(this.b, b2, this.c, this.d);
        } else {
            ja.a(5, bi.d, "Redirect URL could not be found for: " + this.a);
        }
    }
}
