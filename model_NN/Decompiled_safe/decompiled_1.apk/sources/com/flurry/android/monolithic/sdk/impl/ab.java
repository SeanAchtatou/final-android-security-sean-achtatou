package com.flurry.android.monolithic.sdk.impl;

import com.flurry.android.AdCreative;
import com.flurry.android.FlurryAdSize;
import com.flurry.android.impl.ads.FlurryAdModule;
import com.flurry.android.impl.ads.avro.protocol.v6.AdFrame;
import com.flurry.android.impl.ads.avro.protocol.v6.AdSpaceLayout;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;
import java.util.List;

public final class ab {
    public static m a(FlurryAdModule flurryAdModule, String str) {
        m mVar = new m(flurryAdModule.d(), str, flurryAdModule.i());
        flurryAdModule.a(mVar);
        return mVar;
    }

    public static AdCreative a(AdSpaceLayout adSpaceLayout) {
        return new AdCreative(adSpaceLayout.c().intValue(), adSpaceLayout.b().intValue(), adSpaceLayout.e().toString(), adSpaceLayout.d().toString(), adSpaceLayout.f().toString());
    }

    public static AdCreative a(AdUnit adUnit) {
        if (adUnit == null) {
            return null;
        }
        List<AdFrame> d = adUnit.d();
        if (d == null || d.isEmpty()) {
            return null;
        }
        AdFrame adFrame = d.get(0);
        if (adFrame == null) {
            return null;
        }
        AdSpaceLayout e = adFrame.e();
        if (e == null) {
            return null;
        }
        return a(e);
    }

    public static FlurryAdSize b(AdSpaceLayout adSpaceLayout) {
        if (adSpaceLayout == null) {
            return FlurryAdSize.BANNER_TOP;
        }
        if (adSpaceLayout.e().toString().equals(AdCreative.kFormatTakeover)) {
            return FlurryAdSize.FULLSCREEN;
        }
        String[] split = adSpaceLayout.f().toString().split("-");
        if (split.length <= 0 || !split[0].equals("b")) {
            return FlurryAdSize.BANNER_TOP;
        }
        return FlurryAdSize.BANNER_BOTTOM;
    }
}
