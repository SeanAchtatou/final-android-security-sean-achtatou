package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import android.location.Location;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.SystemClock;
import android.text.TextUtils;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicInteger;

public class fb implements fj, id {
    private static final String a = fb.class.getSimpleName();
    private static AtomicInteger b = new AtomicInteger(0);
    private static AtomicInteger c = new AtomicInteger(0);
    private long A;
    private Map<String, eh> B = new HashMap();
    private List<ek> C = new ArrayList();
    private boolean D;
    private int E;
    private List<ej> F = new ArrayList();
    private int G;
    private Map<String, List<String>> H;
    private Map<String, List<String>> I;
    private final Handler J;
    private fh K;
    private ff L;
    private int M;
    private boolean N = false;
    private File d = null;
    private File e = null;
    /* access modifiers changed from: private */
    public volatile boolean f = false;
    private String g;
    private String h;
    private List<ez> i;
    private Map<ie, ByteBuffer> j = new HashMap();
    private boolean k;
    private long l;
    /* access modifiers changed from: private */
    public List<ez> m = new ArrayList();
    /* access modifiers changed from: private */
    public String n;
    private long o;
    private long p;
    private long q;
    private long r;
    private String s = "";
    private String t = "";
    private byte u = -1;
    private int v;
    private boolean w;
    private String x;
    private byte y;
    private long z;

    private void m() {
        if (this.K.b()) {
            this.K.a();
        }
        this.M++;
    }

    private void n() {
        this.M--;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.M;
    }

    public void a(Map<String, List<String>> map) {
        this.I = map;
    }

    public void a(jf jfVar) {
        this.J.post(jfVar);
    }

    public void b() {
        this.k = true;
    }

    public fb(Context context, String str, ff ffVar) {
        ja.a(4, a, "Initializing new Flurry session");
        HandlerThread handlerThread = new HandlerThread("FlurryAgentSession_" + str);
        handlerThread.start();
        this.J = new Handler(handlerThread.getLooper());
        s();
        this.K = new fh(this);
        this.L = ffVar;
        this.g = str;
        this.d = context.getFileStreamPath(w());
        this.e = context.getFileStreamPath(g());
        this.h = ir.a();
        this.q = -1;
        this.v = 0;
        this.t = TimeZone.getDefault().getID();
        this.s = Locale.getDefault().getLanguage() + "_" + Locale.getDefault().getCountry();
        this.B = new HashMap();
        this.C = new ArrayList();
        this.D = true;
        this.F = new ArrayList();
        this.E = 0;
        this.G = 0;
        b.set(0);
        c.set(0);
        o();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, com.flurry.android.monolithic.sdk.impl.id):void
     arg types: [java.lang.String, com.flurry.android.monolithic.sdk.impl.fb]
     candidates:
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.Object, java.lang.Object):boolean
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, com.flurry.android.monolithic.sdk.impl.id):void */
    private void o() {
        ic a2 = ib.a();
        this.y = ((Byte) a2.a("Gender")).byteValue();
        a2.a("Gender", (id) this);
        ja.a(4, a, "initSettings, Gender = " + ((int) this.y));
        this.x = (String) a2.a("UserId");
        a2.a("UserId", (id) this);
        ja.a(4, a, "initSettings, UserId = " + this.x);
        this.w = ((Boolean) a2.a("LogEvents")).booleanValue();
        a2.a("LogEvents", (id) this);
        ja.a(4, a, "initSettings, LogEvents = " + this.w);
        this.z = ((Long) a2.a("Age")).longValue();
        a2.a("Age", (id) this);
        ja.a(4, a, "initSettings, BirthDate = " + this.z);
        this.A = ((Long) a2.a("ContinueSessionMillis")).longValue();
        a2.a("ContinueSessionMillis", (id) this);
        ja.a(4, a, "initSettings, ContinueSessionMillis = " + this.A);
    }

    public void a(String str, Object obj) {
        if (str.equals("Gender")) {
            this.y = ((Byte) obj).byteValue();
            ja.a(4, a, "onSettingUpdate, Gender = " + ((int) this.y));
        } else if (str.equals("UserId")) {
            this.x = (String) obj;
            ja.a(4, a, "onSettingUpdate, UserId = " + this.x);
        } else if (str.equals("LogEvents")) {
            this.w = ((Boolean) obj).booleanValue();
            ja.a(4, a, "onSettingUpdate, LogEvents = " + this.w);
        } else if (str.equals("Age")) {
            this.z = ((Long) obj).longValue();
            ja.a(4, a, "onSettingUpdate, Birthdate = " + this.z);
        } else if (str.equals("ContinueSessionMillis")) {
            this.A = ((Long) obj).longValue();
            ja.a(4, a, "onSettingUpdate, ContinueSessionMillis = " + this.A);
        } else {
            ja.a(6, a, "onSettingUpdate internal error!");
        }
    }

    public void c() {
        m();
        if (!this.N) {
            p();
            this.N = true;
            return;
        }
        q();
    }

    private void p() {
        ja.a(5, a, "Start session");
        long elapsedRealtime = SystemClock.elapsedRealtime();
        this.o = System.currentTimeMillis();
        this.p = elapsedRealtime;
        a(new fc(this));
    }

    private void q() {
        ja.a(5, a, "Continuing previous session");
        if (!this.m.isEmpty()) {
            this.m.remove(this.m.size() - 1);
        }
    }

    public void d() {
        ja.a(5, a, "Trying to end session");
        if (this.N) {
            if (a() > 0) {
                n();
            }
            if (a() == 0) {
                this.K.a(this.A);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(Context context) {
        try {
            byte[] a2 = im.a();
            if (a2 != null) {
                ja.a(3, a, "Fetched hashed IMEI");
                this.j.put(ie.Sha1Imei, ByteBuffer.wrap(a2));
            }
            c(context);
        } catch (Throwable th) {
            ja.a(6, a, "", th);
        }
    }

    private void r() {
        if (a() != 0) {
            ja.a(6, a, "Error! Session with apiKey = " + i() + " was ended while getSessionCount() is not 0");
        }
        e();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.ic.b(java.lang.String, com.flurry.android.monolithic.sdk.impl.id):boolean
     arg types: [java.lang.String, com.flurry.android.monolithic.sdk.impl.fb]
     candidates:
      com.flurry.android.monolithic.sdk.impl.ic.b(java.lang.String, java.lang.Object):void
      com.flurry.android.monolithic.sdk.impl.ic.b(java.lang.String, com.flurry.android.monolithic.sdk.impl.id):boolean */
    public void e() {
        if (this.N) {
            ja.a(5, a, "Ending session");
            this.M = 0;
            if (this.K.b()) {
                this.K.a();
            }
            this.q = SystemClock.elapsedRealtime() - this.p;
            a(this.q);
            t();
            if (this.L != null) {
                this.L.d(i());
            }
            ib.a().b("Gender", (id) this);
            ib.a().b("UserId", (id) this);
            ib.a().b("Age", (id) this);
            ib.a().b("LogEvents", (id) this);
            ib.a().b("ContinueSessionMillis", (id) this);
            this.J.getLooper().quit();
        }
    }

    private void s() {
        if (TextUtils.isEmpty(this.n)) {
            a(new fd(this));
        }
    }

    private void a(long j2) {
        for (ek next : this.C) {
            if (next.a() && !next.b()) {
                next.a(j2);
            }
        }
    }

    private void t() {
        a(new fe(this, ia.a().b()));
    }

    /* access modifiers changed from: private */
    public void b(Context context) {
        boolean z2;
        try {
            synchronized (this) {
                z2 = this.m.size() > 0;
            }
            if (z2) {
                c(context);
            }
        } catch (Throwable th) {
            ja.a(6, a, "", th);
        }
    }

    /* access modifiers changed from: private */
    public synchronized ez u() {
        ez ezVar;
        fa faVar = new fa();
        faVar.a(this.h);
        faVar.a(this.o);
        faVar.b(this.q);
        faVar.c(this.r);
        faVar.b(j());
        faVar.c(k());
        faVar.a((int) this.u);
        faVar.d(h());
        faVar.a(A());
        faVar.b(this.G);
        faVar.a(this.y);
        faVar.a(Long.valueOf(this.z));
        faVar.a(this.C);
        faVar.a(this.B);
        faVar.b(this.F);
        faVar.c(this.v);
        faVar.a(this.D);
        try {
            ezVar = new ez(faVar);
        } catch (IOException e2) {
            e2.printStackTrace();
            ezVar = null;
        }
        if (ezVar == null) {
            ja.a(6, a, "New session report wasn't created");
        }
        return ezVar;
    }

    public synchronized void f() {
        this.G++;
    }

    public synchronized void a(String str, Map<String, String> map, boolean z2) {
        Map<String, String> map2;
        if (this.C == null) {
            ja.a(6, a, "onEvent called before onStartSession.  Event: " + str);
        } else {
            long elapsedRealtime = SystemClock.elapsedRealtime() - this.p;
            String a2 = je.a(str);
            if (a2.length() != 0) {
                eh ehVar = this.B.get(a2);
                if (ehVar != null) {
                    ehVar.a++;
                } else if (this.B.size() < 100) {
                    eh ehVar2 = new eh();
                    ehVar2.a = 1;
                    this.B.put(a2, ehVar2);
                    ja.a(3, a, "Event count incremented: " + a2);
                } else {
                    ja.a(5, a, "Too many different events. Event not counted: " + a2);
                }
                if (!this.w || this.C.size() >= 1000 || this.E >= 160000) {
                    this.D = false;
                } else {
                    if (map == null) {
                        map2 = Collections.emptyMap();
                    } else {
                        map2 = map;
                    }
                    if (map2.size() > 10) {
                        ja.a(5, a, "MaxEventParams exceeded: " + map2.size());
                    } else {
                        ek ekVar = new ek(x(), a2, map2, elapsedRealtime, z2);
                        if (ekVar.d() + this.E <= 160000) {
                            this.C.add(ekVar);
                            this.E = ekVar.d() + this.E;
                        } else {
                            this.E = 160000;
                            this.D = false;
                            ja.a(5, a, "Event Log size exceeded. No more event details logged.");
                        }
                    }
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0029, code lost:
        if (r10.size() <= 0) goto L_0x0079;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002d, code lost:
        if (r8.E >= 160000) goto L_0x0079;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002f, code lost:
        r3 = r8.E - r0.d();
        r4 = new java.util.HashMap(r0.c());
        r0.a(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0047, code lost:
        if ((r0.d() + r3) > 160000) goto L_0x0089;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0053, code lost:
        if (r0.c().size() <= 10) goto L_0x007e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0055, code lost:
        com.flurry.android.monolithic.sdk.impl.ja.a(5, com.flurry.android.monolithic.sdk.impl.fb.a, "MaxEventParams exceeded on endEvent: " + r0.c().size());
        r0.b(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0079, code lost:
        r0.a(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r8.E = r3 + r0.d();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        r0.b(r4);
        r8.D = false;
        r8.E = 160000;
        com.flurry.android.monolithic.sdk.impl.ja.a(5, com.flurry.android.monolithic.sdk.impl.fb.a, "Event Log size exceeded. No more event details logged.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001c, code lost:
        r1 = android.os.SystemClock.elapsedRealtime() - r8.p;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0023, code lost:
        if (r10 == null) goto L_0x0079;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a(java.lang.String r9, java.util.Map<java.lang.String, java.lang.String> r10) {
        /*
            r8 = this;
            r6 = 160000(0x27100, float:2.24208E-40)
            monitor-enter(r8)
            java.util.List<com.flurry.android.monolithic.sdk.impl.ek> r0 = r8.C     // Catch:{ all -> 0x0086 }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ all -> 0x0086 }
        L_0x000a:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x0086 }
            if (r0 == 0) goto L_0x007c
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x0086 }
            com.flurry.android.monolithic.sdk.impl.ek r0 = (com.flurry.android.monolithic.sdk.impl.ek) r0     // Catch:{ all -> 0x0086 }
            boolean r2 = r0.a(r9)     // Catch:{ all -> 0x0086 }
            if (r2 == 0) goto L_0x000a
            long r1 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0086 }
            long r3 = r8.p     // Catch:{ all -> 0x0086 }
            long r1 = r1 - r3
            if (r10 == 0) goto L_0x0079
            int r3 = r10.size()     // Catch:{ all -> 0x0086 }
            if (r3 <= 0) goto L_0x0079
            int r3 = r8.E     // Catch:{ all -> 0x0086 }
            if (r3 >= r6) goto L_0x0079
            int r3 = r8.E     // Catch:{ all -> 0x0086 }
            int r4 = r0.d()     // Catch:{ all -> 0x0086 }
            int r3 = r3 - r4
            java.util.HashMap r4 = new java.util.HashMap     // Catch:{ all -> 0x0086 }
            java.util.Map r5 = r0.c()     // Catch:{ all -> 0x0086 }
            r4.<init>(r5)     // Catch:{ all -> 0x0086 }
            r0.a(r10)     // Catch:{ all -> 0x0086 }
            int r5 = r0.d()     // Catch:{ all -> 0x0086 }
            int r5 = r5 + r3
            if (r5 > r6) goto L_0x0089
            java.util.Map r5 = r0.c()     // Catch:{ all -> 0x0086 }
            int r5 = r5.size()     // Catch:{ all -> 0x0086 }
            r6 = 10
            if (r5 <= r6) goto L_0x007e
            r3 = 5
            java.lang.String r5 = com.flurry.android.monolithic.sdk.impl.fb.a     // Catch:{ all -> 0x0086 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0086 }
            r6.<init>()     // Catch:{ all -> 0x0086 }
            java.lang.String r7 = "MaxEventParams exceeded on endEvent: "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0086 }
            java.util.Map r7 = r0.c()     // Catch:{ all -> 0x0086 }
            int r7 = r7.size()     // Catch:{ all -> 0x0086 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0086 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x0086 }
            com.flurry.android.monolithic.sdk.impl.ja.a(r3, r5, r6)     // Catch:{ all -> 0x0086 }
            r0.b(r4)     // Catch:{ all -> 0x0086 }
        L_0x0079:
            r0.a(r1)     // Catch:{ all -> 0x0086 }
        L_0x007c:
            monitor-exit(r8)
            return
        L_0x007e:
            int r4 = r0.d()     // Catch:{ all -> 0x0086 }
            int r3 = r3 + r4
            r8.E = r3     // Catch:{ all -> 0x0086 }
            goto L_0x0079
        L_0x0086:
            r0 = move-exception
            monitor-exit(r8)
            throw r0
        L_0x0089:
            r0.b(r4)     // Catch:{ all -> 0x0086 }
            r3 = 0
            r8.D = r3     // Catch:{ all -> 0x0086 }
            r3 = 160000(0x27100, float:2.24208E-40)
            r8.E = r3     // Catch:{ all -> 0x0086 }
            r3 = 5
            java.lang.String r4 = com.flurry.android.monolithic.sdk.impl.fb.a     // Catch:{ all -> 0x0086 }
            java.lang.String r5 = "Event Log size exceeded. No more event details logged."
            com.flurry.android.monolithic.sdk.impl.ja.a(r3, r4, r5)     // Catch:{ all -> 0x0086 }
            goto L_0x0079
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.fb.a(java.lang.String, java.util.Map):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x007f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a(java.lang.String r10, java.lang.String r11, java.lang.String r12, java.lang.Throwable r13) {
        /*
            r9 = this;
            r3 = 0
            java.lang.String r0 = "uncaught"
            monitor-enter(r9)
            java.util.List<com.flurry.android.monolithic.sdk.impl.ej> r0 = r9.F     // Catch:{ all -> 0x007a }
            if (r0 != 0) goto L_0x0023
            r0 = 6
            java.lang.String r1 = com.flurry.android.monolithic.sdk.impl.fb.a     // Catch:{ all -> 0x007a }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x007a }
            r2.<init>()     // Catch:{ all -> 0x007a }
            java.lang.String r3 = "onError called before onStartSession.  Error: "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x007a }
            java.lang.StringBuilder r2 = r2.append(r10)     // Catch:{ all -> 0x007a }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x007a }
            com.flurry.android.monolithic.sdk.impl.ja.a(r0, r1, r2)     // Catch:{ all -> 0x007a }
        L_0x0021:
            monitor-exit(r9)
            return
        L_0x0023:
            if (r10 == 0) goto L_0x007d
            java.lang.String r0 = "uncaught"
            boolean r0 = r0.equals(r10)     // Catch:{ all -> 0x007a }
            if (r0 == 0) goto L_0x007d
            r0 = 1
        L_0x002e:
            int r1 = r9.v     // Catch:{ all -> 0x007a }
            int r1 = r1 + 1
            r9.v = r1     // Catch:{ all -> 0x007a }
            java.util.List<com.flurry.android.monolithic.sdk.impl.ej> r1 = r9.F     // Catch:{ all -> 0x007a }
            int r1 = r1.size()     // Catch:{ all -> 0x007a }
            r2 = 50
            if (r1 >= r2) goto L_0x007f
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x007a }
            java.lang.Long r2 = java.lang.Long.valueOf(r0)     // Catch:{ all -> 0x007a }
            com.flurry.android.monolithic.sdk.impl.ej r0 = new com.flurry.android.monolithic.sdk.impl.ej     // Catch:{ all -> 0x007a }
            int r1 = y()     // Catch:{ all -> 0x007a }
            long r2 = r2.longValue()     // Catch:{ all -> 0x007a }
            r4 = r10
            r5 = r11
            r6 = r12
            r7 = r13
            r0.<init>(r1, r2, r4, r5, r6, r7)     // Catch:{ all -> 0x007a }
            java.util.List<com.flurry.android.monolithic.sdk.impl.ej> r1 = r9.F     // Catch:{ all -> 0x007a }
            r1.add(r0)     // Catch:{ all -> 0x007a }
            r1 = 3
            java.lang.String r2 = com.flurry.android.monolithic.sdk.impl.fb.a     // Catch:{ all -> 0x007a }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x007a }
            r3.<init>()     // Catch:{ all -> 0x007a }
            java.lang.String r4 = "Error logged: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x007a }
            java.lang.String r0 = r0.c()     // Catch:{ all -> 0x007a }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x007a }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x007a }
            com.flurry.android.monolithic.sdk.impl.ja.a(r1, r2, r0)     // Catch:{ all -> 0x007a }
            goto L_0x0021
        L_0x007a:
            r0 = move-exception
            monitor-exit(r9)
            throw r0
        L_0x007d:
            r0 = r3
            goto L_0x002e
        L_0x007f:
            if (r0 == 0) goto L_0x00c8
            r8 = r3
        L_0x0082:
            java.util.List<com.flurry.android.monolithic.sdk.impl.ej> r0 = r9.F     // Catch:{ all -> 0x007a }
            int r0 = r0.size()     // Catch:{ all -> 0x007a }
            if (r8 >= r0) goto L_0x0021
            java.util.List<com.flurry.android.monolithic.sdk.impl.ej> r0 = r9.F     // Catch:{ all -> 0x007a }
            java.lang.Object r0 = r0.get(r8)     // Catch:{ all -> 0x007a }
            com.flurry.android.monolithic.sdk.impl.ej r0 = (com.flurry.android.monolithic.sdk.impl.ej) r0     // Catch:{ all -> 0x007a }
            java.lang.String r1 = r0.c()     // Catch:{ all -> 0x007a }
            if (r1 == 0) goto L_0x00c4
            java.lang.String r1 = "uncaught"
            java.lang.String r0 = r0.c()     // Catch:{ all -> 0x007a }
            boolean r0 = r1.equals(r0)     // Catch:{ all -> 0x007a }
            if (r0 != 0) goto L_0x00c4
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x007a }
            java.lang.Long r2 = java.lang.Long.valueOf(r0)     // Catch:{ all -> 0x007a }
            com.flurry.android.monolithic.sdk.impl.ej r0 = new com.flurry.android.monolithic.sdk.impl.ej     // Catch:{ all -> 0x007a }
            int r1 = y()     // Catch:{ all -> 0x007a }
            long r2 = r2.longValue()     // Catch:{ all -> 0x007a }
            r4 = r10
            r5 = r11
            r6 = r12
            r7 = r13
            r0.<init>(r1, r2, r4, r5, r6, r7)     // Catch:{ all -> 0x007a }
            java.util.List<com.flurry.android.monolithic.sdk.impl.ej> r1 = r9.F     // Catch:{ all -> 0x007a }
            r1.set(r8, r0)     // Catch:{ all -> 0x007a }
            goto L_0x0021
        L_0x00c4:
            int r0 = r8 + 1
            r8 = r0
            goto L_0x0082
        L_0x00c8:
            r0 = 3
            java.lang.String r1 = com.flurry.android.monolithic.sdk.impl.fb.a     // Catch:{ all -> 0x007a }
            java.lang.String r2 = "Max errors logged. No more errors logged."
            com.flurry.android.monolithic.sdk.impl.ja.a(r0, r1, r2)     // Catch:{ all -> 0x007a }
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.fb.a(java.lang.String, java.lang.String, java.lang.String, java.lang.Throwable):void");
    }

    private void c(Context context) {
        try {
            ja.a(3, a, "generating agent report");
            el elVar = new el(this.g, this.h, z(), this.k, this.l, this.o, this.m, this.e, this.j, this.H, this.I);
            this.i = new ArrayList(this.m);
            if (elVar == null || elVar.a() == null) {
                ja.a(6, a, "Error generating report");
                return;
            }
            ja.a(3, a, "generated report of size " + elVar.a().length + " with " + this.m.size() + " reports.");
            a(elVar.a());
            this.m.removeAll(this.i);
            this.i = null;
            v();
        } catch (Throwable th) {
            ja.a(6, a, "", th);
        }
    }

    private void a(byte[] bArr) {
        eg.a().k().a(bArr, this.g, "" + eg.a().b());
    }

    /* access modifiers changed from: private */
    public synchronized void v() {
        if (!iw.a(this.d)) {
            ja.a(6, a, "ERROR! CREATE PARENT DIR DIDN'T PROCCEED");
        } else {
            try {
                DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(this.d));
                fg fgVar = new fg();
                fgVar.a(this.k);
                fgVar.a(this.l);
                fgVar.a(this.m);
                fgVar.a(dataOutputStream, this.g, z());
            } catch (FileNotFoundException e2) {
                e2.printStackTrace();
            } catch (IOException e3) {
                e3.printStackTrace();
            }
        }
        return;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005f A[Catch:{ Throwable -> 0x00b3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0073 A[Catch:{ Throwable -> 0x00b3 }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:42:0x00a6=Splitter:B:42:0x00a6, B:12:0x0058=Splitter:B:12:0x0058, B:20:0x006f=Splitter:B:20:0x006f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void d(android.content.Context r7) {
        /*
            r6 = this;
            monitor-enter(r6)
            java.io.File r0 = r6.d     // Catch:{ all -> 0x009f }
            boolean r0 = r0.exists()     // Catch:{ all -> 0x009f }
            if (r0 == 0) goto L_0x00bd
            r0 = 4
            java.lang.String r1 = com.flurry.android.monolithic.sdk.impl.fb.a     // Catch:{ all -> 0x009f }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x009f }
            r2.<init>()     // Catch:{ all -> 0x009f }
            java.lang.String r3 = "loading persistent data: "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x009f }
            java.io.File r3 = r6.d     // Catch:{ all -> 0x009f }
            java.lang.String r3 = r3.getAbsolutePath()     // Catch:{ all -> 0x009f }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x009f }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x009f }
            com.flurry.android.monolithic.sdk.impl.ja.a(r0, r1, r2)     // Catch:{ all -> 0x009f }
            r0 = 0
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x00cb, all -> 0x00a2 }
            java.io.File r2 = r6.d     // Catch:{ Throwable -> 0x00cb, all -> 0x00a2 }
            r1.<init>(r2)     // Catch:{ Throwable -> 0x00cb, all -> 0x00a2 }
            java.io.DataInputStream r2 = new java.io.DataInputStream     // Catch:{ Throwable -> 0x00cb, all -> 0x00a2 }
            r2.<init>(r1)     // Catch:{ Throwable -> 0x00cb, all -> 0x00a2 }
            com.flurry.android.monolithic.sdk.impl.fg r0 = new com.flurry.android.monolithic.sdk.impl.fg     // Catch:{ IOException -> 0x008c }
            r0.<init>()     // Catch:{ IOException -> 0x008c }
            java.lang.String r1 = r6.g     // Catch:{ IOException -> 0x008c }
            boolean r1 = r0.a(r2, r1)     // Catch:{ IOException -> 0x008c }
            r6.f = r1     // Catch:{ IOException -> 0x008c }
            boolean r1 = r6.f     // Catch:{ IOException -> 0x008c }
            if (r1 == 0) goto L_0x0058
            boolean r1 = r0.a()     // Catch:{ IOException -> 0x008c }
            r6.k = r1     // Catch:{ IOException -> 0x008c }
            long r3 = r0.c()     // Catch:{ IOException -> 0x008c }
            r6.l = r3     // Catch:{ IOException -> 0x008c }
            java.util.List r0 = r0.b()     // Catch:{ IOException -> 0x008c }
            r6.m = r0     // Catch:{ IOException -> 0x008c }
        L_0x0058:
            com.flurry.android.monolithic.sdk.impl.je.a(r2)     // Catch:{ all -> 0x009f }
        L_0x005b:
            boolean r0 = r6.f     // Catch:{ Throwable -> 0x00b3 }
            if (r0 != 0) goto L_0x006f
            java.io.File r0 = r6.d     // Catch:{ Throwable -> 0x00b3 }
            boolean r0 = r0.delete()     // Catch:{ Throwable -> 0x00b3 }
            if (r0 == 0) goto L_0x00aa
            r0 = 3
            java.lang.String r1 = com.flurry.android.monolithic.sdk.impl.fb.a     // Catch:{ Throwable -> 0x00b3 }
            java.lang.String r2 = "Deleted persistence file"
            com.flurry.android.monolithic.sdk.impl.ja.a(r0, r1, r2)     // Catch:{ Throwable -> 0x00b3 }
        L_0x006f:
            boolean r0 = r6.f     // Catch:{ all -> 0x009f }
            if (r0 != 0) goto L_0x007d
            r0 = 0
            r6.k = r0     // Catch:{ all -> 0x009f }
            long r0 = r6.o     // Catch:{ all -> 0x009f }
            r6.l = r0     // Catch:{ all -> 0x009f }
            r0 = 1
            r6.f = r0     // Catch:{ all -> 0x009f }
        L_0x007d:
            com.flurry.android.monolithic.sdk.impl.ei r0 = new com.flurry.android.monolithic.sdk.impl.ei     // Catch:{ all -> 0x009f }
            java.io.File r1 = r6.e     // Catch:{ all -> 0x009f }
            r0.<init>(r1)     // Catch:{ all -> 0x009f }
            java.util.Map r0 = r0.a()     // Catch:{ all -> 0x009f }
            r6.H = r0     // Catch:{ all -> 0x009f }
            monitor-exit(r6)
            return
        L_0x008c:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Throwable -> 0x0091, all -> 0x00c6 }
            goto L_0x0058
        L_0x0091:
            r0 = move-exception
            r1 = r2
        L_0x0093:
            r2 = 6
            java.lang.String r3 = com.flurry.android.monolithic.sdk.impl.fb.a     // Catch:{ all -> 0x00c9 }
            java.lang.String r4 = "Error when loading persistent file"
            com.flurry.android.monolithic.sdk.impl.ja.a(r2, r3, r4, r0)     // Catch:{ all -> 0x00c9 }
            com.flurry.android.monolithic.sdk.impl.je.a(r1)     // Catch:{ all -> 0x009f }
            goto L_0x005b
        L_0x009f:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        L_0x00a2:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x00a6:
            com.flurry.android.monolithic.sdk.impl.je.a(r1)     // Catch:{ all -> 0x009f }
            throw r0     // Catch:{ all -> 0x009f }
        L_0x00aa:
            r0 = 6
            java.lang.String r1 = com.flurry.android.monolithic.sdk.impl.fb.a     // Catch:{ Throwable -> 0x00b3 }
            java.lang.String r2 = "Cannot delete persistence file"
            com.flurry.android.monolithic.sdk.impl.ja.a(r0, r1, r2)     // Catch:{ Throwable -> 0x00b3 }
            goto L_0x006f
        L_0x00b3:
            r0 = move-exception
            r1 = 6
            java.lang.String r2 = com.flurry.android.monolithic.sdk.impl.fb.a     // Catch:{ all -> 0x009f }
            java.lang.String r3 = ""
            com.flurry.android.monolithic.sdk.impl.ja.a(r1, r2, r3, r0)     // Catch:{ all -> 0x009f }
            goto L_0x006f
        L_0x00bd:
            r0 = 4
            java.lang.String r1 = com.flurry.android.monolithic.sdk.impl.fb.a     // Catch:{ all -> 0x009f }
            java.lang.String r2 = "Agent cache file doesn't exist."
            com.flurry.android.monolithic.sdk.impl.ja.a(r0, r1, r2)     // Catch:{ all -> 0x009f }
            goto L_0x006f
        L_0x00c6:
            r0 = move-exception
            r1 = r2
            goto L_0x00a6
        L_0x00c9:
            r0 = move-exception
            goto L_0x00a6
        L_0x00cb:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0093
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.fb.d(android.content.Context):void");
    }

    private String w() {
        return ".flurryagent." + Integer.toString(this.g.hashCode(), 16);
    }

    /* access modifiers changed from: package-private */
    public String g() {
        return ".flurryinstallreceiver.";
    }

    private static int x() {
        return b.incrementAndGet();
    }

    private static int y() {
        return c.incrementAndGet();
    }

    /* access modifiers changed from: package-private */
    public String h() {
        return this.x == null ? "" : this.x;
    }

    private String z() {
        return this.n;
    }

    public String i() {
        return this.g;
    }

    public String j() {
        return this.s;
    }

    public String k() {
        return this.t;
    }

    private Location A() {
        return eg.a().j();
    }

    public void l() {
        r();
    }
}
