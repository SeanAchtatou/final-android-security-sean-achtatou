package com.flurry.android.monolithic.sdk.impl;

public class gi {
    public String a;
    public String b;
    public String c;
    public String d;
    public String e = null;

    public gi(String str, String str2, String str3, String str4, String str5) {
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        gi giVar = (gi) obj;
        if (!giVar.a.equals(this.a) || !giVar.c.equals(this.c)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i = 36 * 36;
        return ((this.a.hashCode() + 1296) * 36) + this.c.hashCode();
    }
}
