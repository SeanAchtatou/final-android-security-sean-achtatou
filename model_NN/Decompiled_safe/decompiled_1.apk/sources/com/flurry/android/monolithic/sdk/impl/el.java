package com.flurry.android.monolithic.sdk.impl;

import android.os.Build;
import com.flurry.android.FlurryAgent;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.DigestOutputStream;
import java.util.List;
import java.util.Map;

public class el {
    private static final String a = el.class.getSimpleName();
    private byte[] b = null;

    public el(String str, String str2, String str3, boolean z, long j, long j2, List<ez> list, File file, Map<ie, ByteBuffer> map, Map<String, List<String>> map2, Map<String, List<String>> map3) throws IOException {
        DataOutputStream dataOutputStream;
        Throwable th;
        Map<String, List<String>> map4;
        byte[] bArr = null;
        DataOutputStream dataOutputStream2 = null;
        try {
            iv ivVar = new iv();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            DigestOutputStream digestOutputStream = new DigestOutputStream(byteArrayOutputStream, ivVar);
            DataOutputStream dataOutputStream3 = new DataOutputStream(digestOutputStream);
            try {
                dataOutputStream3.writeShort(27);
                dataOutputStream3.writeShort(0);
                dataOutputStream3.writeLong(0);
                dataOutputStream3.writeShort(0);
                dataOutputStream3.writeShort(3);
                dataOutputStream3.writeShort(FlurryAgent.getAgentVersion());
                dataOutputStream3.writeLong(System.currentTimeMillis());
                dataOutputStream3.writeUTF(str);
                dataOutputStream3.writeUTF(str2);
                dataOutputStream3.writeShort(map.size() + 1);
                dataOutputStream3.writeShort(eg.i());
                dataOutputStream3.writeUTF(str3);
                if (!map.isEmpty()) {
                    for (Map.Entry next : map.entrySet()) {
                        dataOutputStream3.writeShort(((ie) next.getKey()).c);
                        byte[] array = ((ByteBuffer) next.getValue()).array();
                        dataOutputStream3.writeShort(array.length);
                        dataOutputStream3.write(array);
                    }
                }
                dataOutputStream3.writeByte(0);
                dataOutputStream3.writeBoolean(z);
                dataOutputStream3.writeLong(j);
                dataOutputStream3.writeLong(j2);
                dataOutputStream3.writeShort(6);
                dataOutputStream3.writeUTF("device.model");
                dataOutputStream3.writeUTF(Build.MODEL);
                dataOutputStream3.writeUTF("build.brand");
                dataOutputStream3.writeUTF(Build.BRAND);
                dataOutputStream3.writeUTF("build.id");
                dataOutputStream3.writeUTF(Build.ID);
                dataOutputStream3.writeUTF("version.release");
                dataOutputStream3.writeUTF(Build.VERSION.RELEASE);
                dataOutputStream3.writeUTF("build.device");
                dataOutputStream3.writeUTF(Build.DEVICE);
                dataOutputStream3.writeUTF("build.product");
                dataOutputStream3.writeUTF(Build.PRODUCT);
                int size = map2 != null ? map2.keySet().size() : 0;
                ja.a(3, a, "refMapSize is:  " + size);
                if (size == 0) {
                    map4 = new ei(file).a();
                    ja.a(3, a, "after loading referrer file:  ");
                } else {
                    map4 = map2;
                }
                dataOutputStream3.writeShort(size);
                if (map4 != null) {
                    ja.a(3, a, "sending referrer values because it exists");
                    for (Map.Entry next2 : map4.entrySet()) {
                        ja.a(3, a, "Referrer Entry:  " + ((String) next2.getKey()) + "=" + next2.getValue());
                        dataOutputStream3.writeUTF((String) next2.getKey());
                        ja.a(3, a, "referrer key is :" + ((String) next2.getKey()));
                        dataOutputStream3.writeShort(((List) next2.getValue()).size());
                        for (String str4 : (List) next2.getValue()) {
                            dataOutputStream3.writeUTF(str4);
                            ja.a(3, a, "referrer value is :" + str4);
                        }
                    }
                }
                dataOutputStream3.writeBoolean(false);
                int size2 = map3 != null ? map3.keySet().size() : 0;
                ja.a(3, a, "optionsMapSize is:  " + size2);
                dataOutputStream3.writeShort(size2);
                if (map3 != null) {
                    ja.a(3, a, "sending launch options");
                    for (Map.Entry next3 : map3.entrySet()) {
                        ja.a(3, a, "Launch Options Key:  " + ((String) next3.getKey()));
                        dataOutputStream3.writeUTF((String) next3.getKey());
                        dataOutputStream3.writeShort(((List) next3.getValue()).size());
                        for (String str5 : (List) next3.getValue()) {
                            dataOutputStream3.writeUTF(str5);
                            ja.a(3, a, "Launch Options value is :" + str5);
                        }
                    }
                }
                int size3 = list.size();
                dataOutputStream3.writeShort(size3);
                for (int i = 0; i < size3; i++) {
                    dataOutputStream3.write(list.get(i).a());
                }
                digestOutputStream.on(false);
                dataOutputStream3.write(ivVar.a());
                dataOutputStream3.close();
                bArr = byteArrayOutputStream.toByteArray();
                je.a(dataOutputStream3);
            } catch (Throwable th2) {
                th = th2;
                dataOutputStream2 = dataOutputStream3;
                je.a(dataOutputStream2);
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            je.a(dataOutputStream2);
            throw th;
        }
        this.b = bArr;
    }

    public byte[] a() {
        return this.b;
    }
}
