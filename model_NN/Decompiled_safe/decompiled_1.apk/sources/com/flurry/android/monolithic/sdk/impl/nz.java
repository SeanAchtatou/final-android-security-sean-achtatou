package com.flurry.android.monolithic.sdk.impl;

import java.lang.ref.WeakReference;

class nz extends WeakReference<K> {
    int a;
    final /* synthetic */ nx b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    nz(nx nxVar, Object obj) {
        super(obj, nxVar.a);
        this.b = nxVar;
        this.a = System.identityHashCode(obj);
    }

    public int hashCode() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (get() == ((nz) obj).get()) {
            return true;
        }
        return false;
    }
}
