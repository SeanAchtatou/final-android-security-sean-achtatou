package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.lang.reflect.Method;

public final class ta extends sw {
    protected final xl i;
    protected final Method j;

    public ta(String str, afm afm, rw rwVar, ado ado, xl xlVar) {
        super(str, afm, rwVar, ado);
        this.i = xlVar;
        this.j = xlVar.a();
    }

    protected ta(ta taVar, qu<Object> quVar) {
        super(taVar, quVar);
        this.i = taVar.i;
        this.j = taVar.j;
    }

    /* renamed from: b */
    public ta a(qu<Object> quVar) {
        return new ta(this, quVar);
    }

    public xk b() {
        return this.i;
    }

    public void a(ow owVar, qm qmVar, Object obj) throws IOException, oz {
        a(obj, a(owVar, qmVar));
    }

    public final void a(Object obj, Object obj2) throws IOException {
        try {
            this.j.invoke(obj, obj2);
        } catch (Exception e) {
            a(e, obj2);
        }
    }
}
