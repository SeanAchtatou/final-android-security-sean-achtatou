package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
public final class aca extends abw<double[]> {
    public aca() {
        super(double[].class, null, null);
    }

    public abc<?> a(rx rxVar) {
        return this;
    }

    /* renamed from: a */
    public void b(double[] dArr, or orVar, ru ruVar) throws IOException, oq {
        for (double a : dArr) {
            orVar.a(a);
        }
    }
}
