package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
final class vj extends ve<float[]> {
    public vj() {
        super(float[].class);
    }

    /* renamed from: b */
    public float[] a(ow owVar, qm qmVar) throws IOException, oz {
        if (!owVar.j()) {
            return c(owVar, qmVar);
        }
        adu f = qmVar.h().f();
        float[] fArr = (float[]) f.a();
        int i = 0;
        while (owVar.b() != pb.END_ARRAY) {
            float y = y(owVar, qmVar);
            if (i >= fArr.length) {
                fArr = (float[]) f.a(fArr, i);
                i = 0;
            }
            fArr[i] = y;
            i++;
        }
        return (float[]) f.b(fArr, i);
    }

    private final float[] c(ow owVar, qm qmVar) throws IOException, oz {
        if (owVar.e() == pb.VALUE_STRING && qmVar.a(ql.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT) && owVar.k().length() == 0) {
            return null;
        }
        if (!qmVar.a(ql.ACCEPT_SINGLE_VALUE_AS_ARRAY)) {
            throw qmVar.b(this.q);
        }
        return new float[]{y(owVar, qmVar)};
    }
}
