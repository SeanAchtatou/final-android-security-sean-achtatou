package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import com.flurry.android.impl.ads.FlurryAdModule;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;

public abstract class cn {
    static final String a = cn.class.getSimpleName();
    private final Context b;
    private final FlurryAdModule c;
    private final m d;
    private final AdUnit e;

    public abstract void a();

    public cn(Context context, FlurryAdModule flurryAdModule, m mVar, AdUnit adUnit) {
        this.b = context;
        this.c = flurryAdModule;
        this.d = mVar;
        this.e = adUnit;
    }

    public Context b() {
        return this.b;
    }

    public FlurryAdModule c() {
        return this.c;
    }

    public m d() {
        return this.d;
    }

    public AdUnit e() {
        return this.e;
    }
}
