package com.flurry.android.monolithic.sdk.impl;

import java.io.UnsupportedEncodingException;

public class nw implements CharSequence, Comparable<nw> {
    private static final byte[] a = new byte[0];
    private byte[] b = a;
    private int c;
    private String d;

    public nw() {
    }

    public nw(String str) {
        this.b = a(str);
        this.c = this.b.length;
        this.d = str;
    }

    public nw(nw nwVar) {
        this.c = nwVar.c;
        this.b = new byte[nwVar.c];
        System.arraycopy(nwVar.b, 0, this.b, 0, this.c);
        this.d = nwVar.d;
    }

    public byte[] a() {
        return this.b;
    }

    public int b() {
        return this.c;
    }

    public nw a(int i) {
        if (this.c < i) {
            byte[] bArr = new byte[i];
            System.arraycopy(this.b, 0, bArr, 0, this.c);
            this.b = bArr;
        }
        this.c = i;
        this.d = null;
        return this;
    }

    public String toString() {
        if (this.d == null) {
            try {
                this.d = new String(this.b, 0, this.c, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return this.d;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof nw)) {
            return false;
        }
        nw nwVar = (nw) obj;
        if (this.c != nwVar.c) {
            return false;
        }
        byte[] bArr = nwVar.b;
        for (int i = 0; i < this.c; i++) {
            if (this.b[i] != bArr[i]) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int i = 0;
        for (int i2 = 0; i2 < this.c; i2++) {
            i = (i * 31) + this.b[i2];
        }
        return i;
    }

    /* renamed from: a */
    public int compareTo(nw nwVar) {
        return lg.a(this.b, 0, this.c, nwVar.b, 0, nwVar.c);
    }

    public char charAt(int i) {
        return toString().charAt(i);
    }

    public int length() {
        return toString().length();
    }

    public CharSequence subSequence(int i, int i2) {
        return toString().subSequence(i, i2);
    }

    public static final byte[] a(String str) {
        try {
            return str.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return new byte[0];
        }
    }
}
