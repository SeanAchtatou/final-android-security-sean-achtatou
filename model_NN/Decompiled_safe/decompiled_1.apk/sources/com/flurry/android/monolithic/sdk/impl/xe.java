package com.flurry.android.monolithic.sdk.impl;

public class xe extends qw {
    protected final Class<?> c;
    protected final String d;

    public xe(String str, ot otVar, Class<?> cls, String str2) {
        super(str, otVar);
        this.c = cls;
        this.d = str2;
    }

    public static xe a(ow owVar, Object obj, String str) {
        Class<?> cls;
        if (obj == null) {
            throw new IllegalArgumentException();
        }
        if (obj instanceof Class) {
            cls = (Class) obj;
        } else {
            cls = obj.getClass();
        }
        xe xeVar = new xe("Unrecognized field \"" + str + "\" (Class " + cls.getName() + "), not marked as ignorable", owVar.i(), cls, str);
        xeVar.a(obj, str);
        return xeVar;
    }
}
