package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.net.InetAddress;

public class abi extends abq<InetAddress> {
    public static final abi a = new abi();

    public abi() {
        super(InetAddress.class);
    }

    public void a(InetAddress inetAddress, or orVar, ru ruVar) throws IOException, oq {
        String trim = inetAddress.toString().trim();
        int indexOf = trim.indexOf(47);
        if (indexOf >= 0) {
            if (indexOf == 0) {
                trim = trim.substring(1);
            } else {
                trim = trim.substring(0, indexOf);
            }
        }
        orVar.b(trim);
    }

    public void a(InetAddress inetAddress, or orVar, ru ruVar, rx rxVar) throws IOException, oq {
        rxVar.a(inetAddress, orVar, InetAddress.class);
        a(inetAddress, orVar, ruVar);
        rxVar.d(inetAddress, orVar);
    }
}
