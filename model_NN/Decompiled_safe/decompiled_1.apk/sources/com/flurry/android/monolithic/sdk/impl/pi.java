package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public abstract class pi extends ow {
    /* access modifiers changed from: protected */
    public abstract void H() throws ov;

    public abstract pb b() throws IOException, ov;

    public abstract String k() throws IOException, ov;

    protected pi() {
    }

    protected pi(int i) {
        super(i);
    }

    public ow d() throws IOException, ov {
        if (this.b == pb.START_OBJECT || this.b == pb.START_ARRAY) {
            int i = 1;
            while (true) {
                pb b = b();
                if (b != null) {
                    switch (pj.a[b.ordinal()]) {
                        case 1:
                        case 2:
                            i++;
                            break;
                        case 3:
                        case 4:
                            i--;
                            if (i != 0) {
                                break;
                            } else {
                                return this;
                            }
                    }
                } else {
                    H();
                    return this;
                }
            }
        } else {
            return this;
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0023, code lost:
        R();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0026, code lost:
        r1 = r2 + 1;
        r2 = r11.charAt(r2);
        r4 = r13.b(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0030, code lost:
        if (r4 >= 0) goto L_0x0036;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0032, code lost:
        a(r13, r2, 1, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0036, code lost:
        r2 = (r3 << 6) | r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0039, code lost:
        if (r1 < r0) goto L_0x004a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003f, code lost:
        if (r13.a() != false) goto L_0x0047;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0041, code lost:
        r12.a(r2 >> 4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0047, code lost:
        R();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x004a, code lost:
        r3 = r1 + 1;
        r1 = r11.charAt(r1);
        r4 = r13.b(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0054, code lost:
        if (r4 >= 0) goto L_0x0094;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0056, code lost:
        if (r4 == -2) goto L_0x005c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0058, code lost:
        a(r13, r1, 2, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x005c, code lost:
        if (r3 < r0) goto L_0x0061;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x005e, code lost:
        R();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0061, code lost:
        r1 = r3 + 1;
        r3 = r11.charAt(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x006b, code lost:
        if (r13.a(r3) != false) goto L_0x008d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x006d, code lost:
        a(r13, r3, 3, "expected padding character '" + r13.b() + "'");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x008d, code lost:
        r12.a(r2 >> 4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0094, code lost:
        r1 = (r2 << 6) | r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0097, code lost:
        if (r3 < r0) goto L_0x00a9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x009d, code lost:
        if (r13.a() != false) goto L_0x00a6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x009f, code lost:
        r12.b(r1 >> 2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00a6, code lost:
        R();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00a9, code lost:
        r2 = r3 + 1;
        r3 = r11.charAt(r3);
        r4 = r13.b(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00b3, code lost:
        if (r4 >= 0) goto L_0x00c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00b5, code lost:
        if (r4 == -2) goto L_0x00ba;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00b7, code lost:
        a(r13, r3, 3, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00ba, code lost:
        r12.b(r1 >> 2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00bf, code lost:
        r1 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00c2, code lost:
        r12.c((r1 << 6) | r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0018, code lost:
        r3 = r13.b(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001c, code lost:
        if (r3 >= 0) goto L_0x0021;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001e, code lost:
        a(r13, r1, 0, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0021, code lost:
        if (r2 < r0) goto L_0x0026;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.String r11, com.flurry.android.monolithic.sdk.impl.afq r12, com.flurry.android.monolithic.sdk.impl.on r13) throws java.io.IOException, com.flurry.android.monolithic.sdk.impl.ov {
        /*
            r10 = this;
            r9 = 3
            r8 = 0
            r7 = -2
            r6 = 0
            int r0 = r11.length()
            r1 = r8
        L_0x0009:
            if (r1 >= r0) goto L_0x0013
        L_0x000b:
            int r2 = r1 + 1
            char r1 = r11.charAt(r1)
            if (r2 < r0) goto L_0x0014
        L_0x0013:
            return
        L_0x0014:
            r3 = 32
            if (r1 <= r3) goto L_0x00c9
            int r3 = r13.b(r1)
            if (r3 >= 0) goto L_0x0021
            r10.a(r13, r1, r8, r6)
        L_0x0021:
            if (r2 < r0) goto L_0x0026
            r10.R()
        L_0x0026:
            int r1 = r2 + 1
            char r2 = r11.charAt(r2)
            int r4 = r13.b(r2)
            if (r4 >= 0) goto L_0x0036
            r5 = 1
            r10.a(r13, r2, r5, r6)
        L_0x0036:
            int r2 = r3 << 6
            r2 = r2 | r4
            if (r1 < r0) goto L_0x004a
            boolean r3 = r13.a()
            if (r3 != 0) goto L_0x0047
            int r0 = r2 >> 4
            r12.a(r0)
            goto L_0x0013
        L_0x0047:
            r10.R()
        L_0x004a:
            int r3 = r1 + 1
            char r1 = r11.charAt(r1)
            int r4 = r13.b(r1)
            if (r4 >= 0) goto L_0x0094
            if (r4 == r7) goto L_0x005c
            r4 = 2
            r10.a(r13, r1, r4, r6)
        L_0x005c:
            if (r3 < r0) goto L_0x0061
            r10.R()
        L_0x0061:
            int r1 = r3 + 1
            char r3 = r11.charAt(r3)
            boolean r4 = r13.a(r3)
            if (r4 != 0) goto L_0x008d
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "expected padding character '"
            java.lang.StringBuilder r4 = r4.append(r5)
            char r5 = r13.b()
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = "'"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r4 = r4.toString()
            r10.a(r13, r3, r9, r4)
        L_0x008d:
            int r2 = r2 >> 4
            r12.a(r2)
            goto L_0x0009
        L_0x0094:
            int r1 = r2 << 6
            r1 = r1 | r4
            if (r3 < r0) goto L_0x00a9
            boolean r2 = r13.a()
            if (r2 != 0) goto L_0x00a6
            int r0 = r1 >> 2
            r12.b(r0)
            goto L_0x0013
        L_0x00a6:
            r10.R()
        L_0x00a9:
            int r2 = r3 + 1
            char r3 = r11.charAt(r3)
            int r4 = r13.b(r3)
            if (r4 >= 0) goto L_0x00c2
            if (r4 == r7) goto L_0x00ba
            r10.a(r13, r3, r9, r6)
        L_0x00ba:
            int r1 = r1 >> 2
            r12.b(r1)
        L_0x00bf:
            r1 = r2
            goto L_0x0009
        L_0x00c2:
            int r1 = r1 << 6
            r1 = r1 | r4
            r12.c(r1)
            goto L_0x00bf
        L_0x00c9:
            r1 = r2
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.pi.a(java.lang.String, com.flurry.android.monolithic.sdk.impl.afq, com.flurry.android.monolithic.sdk.impl.on):void");
    }

    /* access modifiers changed from: protected */
    public void a(on onVar, char c, int i, String str) throws ov {
        String str2;
        if (c <= ' ') {
            str2 = "Illegal white space character (code 0x" + Integer.toHexString(c) + ") as character #" + (i + 1) + " of 4-char base64 unit: can only used between units";
        } else if (onVar.a(c)) {
            str2 = "Unexpected padding character ('" + onVar.b() + "') as character #" + (i + 1) + " of 4-char base64 unit: padding only legal as 3rd or 4th character";
        } else if (!Character.isDefined(c) || Character.isISOControl(c)) {
            str2 = "Illegal character (code 0x" + Integer.toHexString(c) + ") in base64 content";
        } else {
            str2 = "Illegal character '" + c + "' (code 0x" + Integer.toHexString(c) + ") in base64 content";
        }
        if (str != null) {
            str2 = str2 + ": " + str;
        }
        throw a(str2);
    }

    /* access modifiers changed from: protected */
    public void R() throws ov {
        throw a("Unexpected end-of-String in base64 content");
    }

    /* access modifiers changed from: protected */
    public void b(int i, String str) throws ov {
        String str2 = "Unexpected character (" + c(i) + ")";
        if (str != null) {
            str2 = str2 + ": " + str;
        }
        d(str2);
    }

    /* access modifiers changed from: protected */
    public void S() throws ov {
        c(" in " + this.b);
    }

    /* access modifiers changed from: protected */
    public void c(String str) throws ov {
        d("Unexpected end-of-input" + str);
    }

    /* access modifiers changed from: protected */
    public void T() throws ov {
        c(" in a value");
    }

    /* access modifiers changed from: protected */
    public void b(int i) throws ov {
        d("Illegal character (" + c((char) i) + "): only regular white space (\\r, \\n, \\t) is allowed between tokens");
    }

    /* access modifiers changed from: protected */
    public void c(int i, String str) throws ov {
        if (!a(ox.ALLOW_UNQUOTED_CONTROL_CHARS) || i >= 32) {
            d("Illegal unquoted character (" + c((char) i) + "): has to be escaped using backslash to be included in " + str);
        }
    }

    /* access modifiers changed from: protected */
    public char a(char c) throws oz {
        if (a(ox.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER)) {
            return c;
        }
        if (c == '\'' && a(ox.ALLOW_SINGLE_QUOTES)) {
            return c;
        }
        d("Unrecognized character escape " + c(c));
        return c;
    }

    protected static final String c(int i) {
        char c = (char) i;
        if (Character.isISOControl(c)) {
            return "(CTRL-CHAR, code " + i + ")";
        }
        if (i > 255) {
            return "'" + c + "' (code " + i + " / 0x" + Integer.toHexString(i) + ")";
        }
        return "'" + c + "' (code " + i + ")";
    }

    /* access modifiers changed from: protected */
    public final void d(String str) throws ov {
        throw a(str);
    }

    /* access modifiers changed from: protected */
    public final void a(String str, Throwable th) throws ov {
        throw b(str, th);
    }

    /* access modifiers changed from: protected */
    public final void U() {
        throw new RuntimeException("Internal error: this code path should never get executed");
    }

    /* access modifiers changed from: protected */
    public final ov b(String str, Throwable th) {
        return new ov(str, i(), th);
    }
}
