package com.flurry.android.monolithic.sdk.impl;

import com.ideaworks3d.marmalade.s3eAndroidGooglePlayBilling.util.IabHelper;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import v2.com.playhaven.model.PHContent;

public abstract class ji {
    static final op a = new op();
    static final rk b = new rk(a);
    static final Map<String, kj> e = new HashMap();
    private static final Set<String> g = new HashSet();
    /* access modifiers changed from: private */
    public static final Set<String> h = new HashSet();
    /* access modifiers changed from: private */
    public static final ThreadLocal<Set> i = new jj();
    /* access modifiers changed from: private */
    public static final ThreadLocal<Map> j = new jk();
    /* access modifiers changed from: private */
    public static ThreadLocal<Boolean> k = new jl();
    kf c = new kf(g);
    int d = Integer.MIN_VALUE;
    /* access modifiers changed from: private */
    public final kj f;

    static {
        a.a(ox.ALLOW_COMMENTS);
        a.a(b);
        Collections.addAll(g, "doc", "fields", "items", "name", "namespace", "size", "symbols", "values", "type");
        Collections.addAll(h, "default", "doc", "name", "order", "type");
        e.put("string", kj.STRING);
        e.put("bytes", kj.BYTES);
        e.put("int", kj.INT);
        e.put("long", kj.LONG);
        e.put("float", kj.FLOAT);
        e.put("double", kj.DOUBLE);
        e.put("boolean", kj.BOOLEAN);
        e.put(PHContent.PARCEL_NULL, kj.NULL);
    }

    ji(kj kjVar) {
        this.f = kjVar;
    }

    public static ji a(kj kjVar) {
        switch (jm.a[kjVar.ordinal()]) {
            case 1:
                return new ki();
            case 2:
                return new jp();
            case 3:
                return new jw();
            case 4:
                return new jy();
            case 5:
                return new jv();
            case 6:
                return new jq();
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED /*7*/:
                return new jo();
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED /*8*/:
                return new kd();
            default:
                throw new jg("Can't create a: " + kjVar);
        }
    }

    public synchronized String a(String str) {
        return (String) this.c.get(str);
    }

    public synchronized void a(String str, String str2) {
        this.c.a(str, str2);
        this.d = Integer.MIN_VALUE;
    }

    public static ji a(String str, String str2, String str3, boolean z) {
        return new kg(new ka(str, str3), str2, z);
    }

    public static ji a(String str, String str2, String str3, List<String> list) {
        return new jr(new ka(str, str3), str2, new jx(list));
    }

    public static ji a(ji jiVar) {
        return new jn(jiVar);
    }

    public static ji b(ji jiVar) {
        return new jz(jiVar);
    }

    public static ji a(List<ji> list) {
        return new kk(new jx(list));
    }

    public static ji a(String str, String str2, String str3, int i2) {
        return new ju(new ka(str, str3), str2, i2);
    }

    public kj a() {
        return this.f;
    }

    public js b(String str) {
        throw new jg("Not a record: " + this);
    }

    public List<js> b() {
        throw new jg("Not a record: " + this);
    }

    public void b(List<js> list) {
        throw new jg("Not a record: " + this);
    }

    public List<String> c() {
        throw new jg("Not an enum: " + this);
    }

    public int c(String str) {
        throw new jg("Not an enum: " + this);
    }

    public String d() {
        return kj.a(this.f);
    }

    public String e() {
        return null;
    }

    public String f() {
        throw new jg("Not a named type: " + this);
    }

    public String g() {
        return d();
    }

    public void d(String str) {
        throw new jg("Not a named type: " + this);
    }

    public boolean h() {
        throw new jg("Not a record: " + this);
    }

    public ji i() {
        throw new jg("Not an array: " + this);
    }

    public ji j() {
        throw new jg("Not a map: " + this);
    }

    public List<ji> k() {
        throw new jg("Not a union: " + this);
    }

    public Integer e(String str) {
        throw new jg("Not a union: " + this);
    }

    public int l() {
        throw new jg("Not fixed: " + this);
    }

    public String toString() {
        return a(false);
    }

    public String a(boolean z) {
        try {
            StringWriter stringWriter = new StringWriter();
            or a2 = a.a(stringWriter);
            if (z) {
                a2.a();
            }
            a(new kc(), a2);
            a2.g();
            return stringWriter.toString();
        } catch (IOException e2) {
            throw new jg(e2);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(kc kcVar, or orVar) throws IOException {
        if (this.c.size() == 0) {
            orVar.b(d());
            return;
        }
        orVar.d();
        orVar.a("type", d());
        this.c.a(orVar);
        orVar.e();
    }

    /* access modifiers changed from: package-private */
    public void b(kc kcVar, or orVar) throws IOException {
        throw new jg("Not a record: " + this);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ji)) {
            return false;
        }
        ji jiVar = (ji) obj;
        if (this.f != jiVar.f) {
            return false;
        }
        return c(jiVar) && this.c.equals(jiVar.c);
    }

    public final int hashCode() {
        if (this.d == Integer.MIN_VALUE) {
            this.d = m();
        }
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public int m() {
        return a().hashCode() + this.c.hashCode();
    }

    /* access modifiers changed from: package-private */
    public final boolean c(ji jiVar) {
        return this.d == jiVar.d || this.d == Integer.MIN_VALUE || jiVar.d == Integer.MIN_VALUE;
    }

    public static ji f(String str) {
        return new ke().a(str);
    }

    /* access modifiers changed from: private */
    public static String h(String str) {
        if (!k.get().booleanValue()) {
            return str;
        }
        int length = str.length();
        if (length == 0) {
            throw new kl("Empty name");
        }
        char charAt = str.charAt(0);
        if (Character.isLetter(charAt) || charAt == '_') {
            int i2 = 1;
            while (i2 < length) {
                char charAt2 = str.charAt(i2);
                if (Character.isLetterOrDigit(charAt2) || charAt2 == '_') {
                    i2++;
                } else {
                    throw new kl("Illegal character in: " + str);
                }
            }
            return str;
        }
        throw new kl("Illegal initial character: " + str);
    }

    static ji a(ou ouVar, kc kcVar) {
        String str;
        ka kaVar;
        String str2;
        ji jiVar;
        jt jtVar;
        ou ouVar2;
        Set<String> a2;
        if (ouVar.e()) {
            ji a3 = kcVar.get((Object) ouVar.h());
            if (a3 != null) {
                return a3;
            }
            throw new kl("Undefined name: " + ouVar);
        } else if (ouVar.b()) {
            String a4 = a(ouVar, "type", "No type");
            if (a4.equals("record") || a4.equals("error") || a4.equals("enum") || a4.equals("fixed")) {
                String a5 = a(ouVar, "namespace");
                String a6 = a(ouVar, "doc");
                if (a5 == null) {
                    a5 = kcVar.a();
                }
                ka kaVar2 = new ka(a(ouVar, "name", "No name in schema"), a5);
                if (kaVar2.b != null) {
                    String a7 = kcVar.a();
                    kcVar.a(kaVar2.b);
                    str = a7;
                    str2 = a6;
                    kaVar = kaVar2;
                } else {
                    str2 = a6;
                    str = null;
                    kaVar = kaVar2;
                }
            } else {
                str2 = null;
                str = null;
                kaVar = null;
            }
            if (e.containsKey(a4)) {
                jiVar = a(e.get(a4));
            } else if (a4.equals("record") || a4.equals("error")) {
                ArrayList arrayList = new ArrayList();
                kg kgVar = new kg(kaVar, str2, a4.equals("error"));
                if (kaVar != null) {
                    kcVar.a((ji) kgVar);
                }
                ou a8 = ouVar.a("fields");
                if (a8 == null || !a8.a()) {
                    throw new kl("Record has no fields: " + ouVar);
                }
                Iterator<ou> it = a8.iterator();
                while (it.hasNext()) {
                    ou next = it.next();
                    String a9 = a(next, "name", "No field name");
                    String a10 = a(next, "doc");
                    ou a11 = next.a("type");
                    if (a11 == null) {
                        throw new kl("No field type: " + next);
                    } else if (!a11.e() || kcVar.get((Object) a11.h()) != null) {
                        ji a12 = a(a11, kcVar);
                        jt jtVar2 = jt.ASCENDING;
                        ou a13 = next.a("order");
                        if (a13 != null) {
                            jtVar = jt.valueOf(a13.h().toUpperCase());
                        } else {
                            jtVar = jtVar2;
                        }
                        ou a14 = next.a("default");
                        if (a14 == null || ((!kj.FLOAT.equals(a12.a()) && !kj.DOUBLE.equals(a12.a())) || !a14.e())) {
                            ouVar2 = a14;
                        } else {
                            ouVar2 = new aex(Double.valueOf(a14.h()).doubleValue());
                        }
                        js jsVar = new js(a9, a12, a10, ouVar2, jtVar);
                        Iterator<String> q = next.q();
                        while (q.hasNext()) {
                            String next2 = q.next();
                            String h2 = next.a(next2).h();
                            if (!h.contains(next2) && h2 != null) {
                                jsVar.a(next2, h2);
                            }
                        }
                        Set unused = jsVar.g = a(next);
                        arrayList.add(jsVar);
                    } else {
                        throw new kl(a11 + " is not a defined name." + " The type of the \"" + a9 + "\" field must be" + " a defined name or a {\"type\": ...} expression.");
                    }
                }
                kgVar.b(arrayList);
                jiVar = kgVar;
            } else if (a4.equals("enum")) {
                ou a15 = ouVar.a("symbols");
                if (a15 == null || !a15.a()) {
                    throw new kl("Enum has no symbols: " + ouVar);
                }
                jx jxVar = new jx();
                Iterator<ou> it2 = a15.iterator();
                while (it2.hasNext()) {
                    jxVar.add(it2.next().h());
                }
                jr jrVar = new jr(kaVar, str2, jxVar);
                if (kaVar != null) {
                    kcVar.a((ji) jrVar);
                }
                jiVar = jrVar;
            } else if (a4.equals("array")) {
                ou a16 = ouVar.a("items");
                if (a16 == null) {
                    throw new kl("Array has no items type: " + ouVar);
                }
                jiVar = new jn(a(a16, kcVar));
            } else if (a4.equals("map")) {
                ou a17 = ouVar.a("values");
                if (a17 == null) {
                    throw new kl("Map has no values type: " + ouVar);
                }
                jiVar = new jz(a(a17, kcVar));
            } else if (a4.equals("fixed")) {
                ou a18 = ouVar.a("size");
                if (a18 == null || !a18.d()) {
                    throw new kl("Invalid or no size: " + ouVar);
                }
                ju juVar = new ju(kaVar, str2, a18.j());
                if (kaVar != null) {
                    kcVar.a((ji) juVar);
                }
                jiVar = juVar;
            } else {
                throw new kl("Type not supported: " + a4);
            }
            Iterator<String> q2 = ouVar.q();
            while (q2.hasNext()) {
                String next3 = q2.next();
                String h3 = ouVar.a(next3).h();
                if (!g.contains(next3) && h3 != null) {
                    jiVar.a(next3, h3);
                }
            }
            if (str != null) {
                kcVar.a(str);
            }
            if ((jiVar instanceof kb) && (a2 = a(ouVar)) != null) {
                for (String d2 : a2) {
                    jiVar.d(d2);
                }
            }
            return jiVar;
        } else if (ouVar.a()) {
            jx jxVar2 = new jx(ouVar.o());
            Iterator<ou> it3 = ouVar.iterator();
            while (it3.hasNext()) {
                jxVar2.add(a(it3.next(), kcVar));
            }
            return new kk(jxVar2);
        } else {
            throw new kl("Schema not yet supported: " + ouVar);
        }
    }

    private static Set<String> a(ou ouVar) {
        ou a2 = ouVar.a("aliases");
        if (a2 == null) {
            return null;
        }
        if (!a2.a()) {
            throw new kl("aliases not an array: " + ouVar);
        }
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        Iterator<ou> it = a2.iterator();
        while (it.hasNext()) {
            ou next = it.next();
            if (!next.e()) {
                throw new kl("alias not a string: " + next);
            }
            linkedHashSet.add(next.h());
        }
        return linkedHashSet;
    }

    private static String a(ou ouVar, String str, String str2) {
        String a2 = a(ouVar, str);
        if (a2 != null) {
            return a2;
        }
        throw new kl(str2 + ": " + ouVar);
    }

    private static String a(ou ouVar, String str) {
        ou a2 = ouVar.a(str);
        if (a2 != null) {
            return a2.h();
        }
        return null;
    }

    public static ji a(ji jiVar, ji jiVar2) {
        if (jiVar == jiVar2) {
            return jiVar;
        }
        IdentityHashMap identityHashMap = new IdentityHashMap(1);
        HashMap hashMap = new HashMap(1);
        HashMap hashMap2 = new HashMap(1);
        b(jiVar2, identityHashMap, hashMap, hashMap2);
        if (hashMap.size() == 0 && hashMap2.size() == 0) {
            return jiVar;
        }
        identityHashMap.clear();
        return a(jiVar, identityHashMap, hashMap, hashMap2);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private static ji a(ji jiVar, Map<ji, ji> map, Map<ka, ka> map2, Map<ka, Map<String, String>> map3) {
        ji a2;
        ka kaVar;
        ka kaVar2 = jiVar instanceof kb ? ((kb) jiVar).f : null;
        switch (jm.a[jiVar.a().ordinal()]) {
            case 9:
                if (!map.containsKey(jiVar)) {
                    if (map2.containsKey(kaVar2)) {
                        kaVar = map2.get(kaVar2);
                    } else {
                        kaVar = kaVar2;
                    }
                    ji a3 = a(kaVar.c, jiVar.e(), (String) null, jiVar.h());
                    map.put(jiVar, a3);
                    ArrayList arrayList = new ArrayList();
                    for (js next : jiVar.b()) {
                        js jsVar = new js(a(kaVar, next.a, map3), a(next.c, map, map2, map3), next.d, next.e, next.f);
                        jsVar.h.putAll(next.h);
                        arrayList.add(jsVar);
                    }
                    a3.b(arrayList);
                    a2 = a3;
                    break;
                } else {
                    return map.get(jiVar);
                }
            case 10:
                if (map2.containsKey(kaVar2)) {
                    a2 = a(map2.get(kaVar2).c, jiVar.e(), (String) null, jiVar.c());
                    break;
                }
                a2 = jiVar;
                break;
            case 11:
                ji a4 = a(jiVar.i(), map, map2, map3);
                if (a4 != jiVar.i()) {
                    a2 = a(a4);
                    break;
                }
                a2 = jiVar;
                break;
            case 12:
                ji a5 = a(jiVar.j(), map, map2, map3);
                if (a5 != jiVar.j()) {
                    a2 = b(a5);
                    break;
                }
                a2 = jiVar;
                break;
            case 13:
                ArrayList arrayList2 = new ArrayList();
                for (ji a6 : jiVar.k()) {
                    arrayList2.add(a(a6, map, map2, map3));
                }
                a2 = a(arrayList2);
                break;
            case 14:
                if (map2.containsKey(kaVar2)) {
                    a2 = a(map2.get(kaVar2).c, jiVar.e(), (String) null, jiVar.l());
                    break;
                }
                a2 = jiVar;
                break;
            default:
                a2 = jiVar;
                break;
        }
        if (a2 == jiVar) {
            return a2;
        }
        a2.c.putAll(jiVar.c);
        return a2;
    }

    private static void b(ji jiVar, Map<ji, ji> map, Map<ka, ka> map2, Map<ka, Map<String, String>> map3) {
        if (jiVar instanceof kb) {
            kb kbVar = (kb) jiVar;
            if (kbVar.h != null) {
                for (ka put : kbVar.h) {
                    map2.put(put, kbVar.f);
                }
            }
        }
        switch (jm.a[jiVar.a().ordinal()]) {
            case 9:
                if (!map.containsKey(jiVar)) {
                    map.put(jiVar, jiVar);
                    kg kgVar = (kg) jiVar;
                    for (js next : jiVar.b()) {
                        if (next.g != null) {
                            for (String str : next.g) {
                                Map map4 = map3.get(kgVar.f);
                                if (map4 == null) {
                                    ka kaVar = kgVar.f;
                                    HashMap hashMap = new HashMap();
                                    map3.put(kaVar, hashMap);
                                    map4 = hashMap;
                                }
                                map4.put(str, next.a);
                            }
                        }
                        b(next.c, map, map2, map3);
                    }
                    if (kgVar.h != null && map3.containsKey(kgVar.f)) {
                        for (ka put2 : kgVar.h) {
                            map3.put(put2, map3.get(kgVar.f));
                        }
                        return;
                    }
                    return;
                }
                return;
            case 10:
            default:
                return;
            case 11:
                b(jiVar.i(), map, map2, map3);
                return;
            case 12:
                b(jiVar.j(), map, map2, map3);
                return;
            case 13:
                for (ji b2 : jiVar.k()) {
                    b(b2, map, map2, map3);
                }
                return;
        }
    }

    private static String a(ka kaVar, String str, Map<ka, Map<String, String>> map) {
        Map map2 = map.get(kaVar);
        if (map2 == null) {
            return str;
        }
        String str2 = (String) map2.get(str);
        if (str2 == null) {
            return str;
        }
        return str2;
    }
}
