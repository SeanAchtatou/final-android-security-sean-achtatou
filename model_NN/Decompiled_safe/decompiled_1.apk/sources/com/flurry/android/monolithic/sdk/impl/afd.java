package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class afd extends aet {
    protected LinkedHashMap<String, ou> c = null;

    public afd(aez aez) {
        super(aez);
    }

    public boolean b() {
        return true;
    }

    public int o() {
        if (this.c == null) {
            return 0;
        }
        return this.c.size();
    }

    public Iterator<ou> p() {
        return this.c == null ? aeu.a() : this.c.values().iterator();
    }

    public ou a(String str) {
        if (this.c != null) {
            return this.c.get(str);
        }
        return null;
    }

    public Iterator<String> q() {
        return this.c == null ? aev.a() : this.c.keySet().iterator();
    }

    public final void a(or orVar, ru ruVar) throws IOException, oz {
        orVar.d();
        if (this.c != null) {
            for (Map.Entry next : this.c.entrySet()) {
                orVar.a((String) next.getKey());
                ((aep) next.getValue()).a(orVar, ruVar);
            }
        }
        orVar.e();
    }

    public void a(or orVar, ru ruVar, rx rxVar) throws IOException, oz {
        rxVar.b(this, orVar);
        if (this.c != null) {
            for (Map.Entry next : this.c.entrySet()) {
                orVar.a((String) next.getKey());
                ((aep) next.getValue()).a(orVar, ruVar);
            }
        }
        rxVar.e(this, orVar);
    }

    public ou a(String str, ou ouVar) {
        ou ouVar2;
        if (ouVar == null) {
            ouVar2 = r();
        } else {
            ouVar2 = ouVar;
        }
        return b(str, ouVar2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0038  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r5) {
        /*
            r4 = this;
            r3 = 1
            r2 = 0
            if (r5 != r4) goto L_0x0006
            r0 = r3
        L_0x0005:
            return r0
        L_0x0006:
            if (r5 != 0) goto L_0x000a
            r0 = r2
            goto L_0x0005
        L_0x000a:
            java.lang.Class r0 = r5.getClass()
            java.lang.Class r1 = r4.getClass()
            if (r0 == r1) goto L_0x0016
            r0 = r2
            goto L_0x0005
        L_0x0016:
            com.flurry.android.monolithic.sdk.impl.afd r5 = (com.flurry.android.monolithic.sdk.impl.afd) r5
            int r0 = r5.o()
            int r1 = r4.o()
            if (r0 == r1) goto L_0x0024
            r0 = r2
            goto L_0x0005
        L_0x0024:
            java.util.LinkedHashMap<java.lang.String, com.flurry.android.monolithic.sdk.impl.ou> r0 = r4.c
            if (r0 == 0) goto L_0x0058
            java.util.LinkedHashMap<java.lang.String, com.flurry.android.monolithic.sdk.impl.ou> r0 = r4.c
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r1 = r0.iterator()
        L_0x0032:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0058
            java.lang.Object r4 = r1.next()
            java.util.Map$Entry r4 = (java.util.Map.Entry) r4
            java.lang.Object r0 = r4.getKey()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r4 = r4.getValue()
            com.flurry.android.monolithic.sdk.impl.ou r4 = (com.flurry.android.monolithic.sdk.impl.ou) r4
            com.flurry.android.monolithic.sdk.impl.ou r0 = r5.a(r0)
            if (r0 == 0) goto L_0x0056
            boolean r0 = r0.equals(r4)
            if (r0 != 0) goto L_0x0032
        L_0x0056:
            r0 = r2
            goto L_0x0005
        L_0x0058:
            r0 = r3
            goto L_0x0005
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.afd.equals(java.lang.Object):boolean");
    }

    public int hashCode() {
        if (this.c == null) {
            return -1;
        }
        return this.c.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder((o() << 4) + 32);
        sb.append("{");
        if (this.c != null) {
            int i = 0;
            for (Map.Entry next : this.c.entrySet()) {
                if (i > 0) {
                    sb.append(",");
                }
                aff.a(sb, (String) next.getKey());
                sb.append(':');
                sb.append(((ou) next.getValue()).toString());
                i++;
            }
        }
        sb.append("}");
        return sb.toString();
    }

    private final ou b(String str, ou ouVar) {
        if (this.c == null) {
            this.c = new LinkedHashMap<>();
        }
        return this.c.put(str, ouVar);
    }
}
