package com.flurry.android.monolithic.sdk.impl;

import android.view.ViewGroup;
import com.flurry.android.impl.ads.avro.protocol.v6.AdSpaceLayout;

abstract class aj {
    public abstract ViewGroup.LayoutParams a(AdSpaceLayout adSpaceLayout);

    private aj() {
    }

    public int b(AdSpaceLayout adSpaceLayout) {
        return h(adSpaceLayout) ? d(adSpaceLayout) : f(adSpaceLayout);
    }

    public int c(AdSpaceLayout adSpaceLayout) {
        return i(adSpaceLayout) ? e(adSpaceLayout) : g(adSpaceLayout);
    }

    public int d(AdSpaceLayout adSpaceLayout) {
        return je.b(adSpaceLayout.b().intValue());
    }

    public int e(AdSpaceLayout adSpaceLayout) {
        return je.b(adSpaceLayout.c().intValue());
    }

    public int f(AdSpaceLayout adSpaceLayout) {
        return -1;
    }

    public int g(AdSpaceLayout adSpaceLayout) {
        return -2;
    }

    private static boolean h(AdSpaceLayout adSpaceLayout) {
        return adSpaceLayout.b().intValue() != 0;
    }

    private static boolean i(AdSpaceLayout adSpaceLayout) {
        return adSpaceLayout.c().intValue() != 0;
    }
}
