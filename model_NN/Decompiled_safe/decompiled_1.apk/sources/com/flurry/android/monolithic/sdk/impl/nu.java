package com.flurry.android.monolithic.sdk.impl;

public abstract class nu implements nt, Comparable<nt> {
    public abstract ji a();

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof nt)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return compareTo((nt) obj) == 0;
    }

    public int hashCode() {
        return nn.b().b(this, a());
    }

    /* renamed from: a */
    public int compareTo(nt ntVar) {
        return nn.b().a(this, ntVar, a());
    }

    public String toString() {
        return nn.b().a(this);
    }
}
