package com.flurry.android.monolithic.sdk.impl;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.LinearLayout;
import com.flurry.android.AdCreative;
import com.flurry.android.AdNetworkView;
import com.flurry.android.impl.ads.FlurryAdModule;
import com.inmobi.androidsdk.IMAdRequest;
import com.inmobi.androidsdk.IMAdView;

public final class dn extends AdNetworkView {
    /* access modifiers changed from: private */
    public static final String a = dn.class.getSimpleName();
    private final String f;
    private final boolean g;

    dn(Context context, FlurryAdModule flurryAdModule, m mVar, AdCreative adCreative, Bundle bundle) {
        super(context, flurryAdModule, mVar, adCreative);
        this.f = bundle.getString("com.flurry.inmobi.MY_APP_ID");
        this.g = bundle.getBoolean("com.flurry.inmobi.test");
        setFocusable(true);
    }

    private int a(int i, int i2) {
        if (i2 >= 728 && i >= 90) {
            ja.a(3, a, "Determined InMobi AdSize as 728x90");
            return 11;
        } else if (i2 >= 468 && i >= 60) {
            ja.a(3, a, "Determined InMobi AdSize as 468x60");
            return 12;
        } else if (i2 >= 320 && i >= 50) {
            ja.a(3, a, "Determined InMobi AdSize as 320x50");
            return 15;
        } else if (i2 >= 300 && i >= 250) {
            ja.a(3, a, "Determined InMobi AdSize as 300x250");
            return 10;
        } else if (i2 < 120 || i < 600) {
            ja.a(3, a, "Could not find InMobi AdSize that matches size");
            return -1;
        } else {
            ja.a(3, a, "Determined InMobi AdSize as 120x600");
            return 13;
        }
    }

    public void initLayout() {
        int i;
        int i2;
        int i3;
        Context context = getContext();
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int a2 = a((int) (((float) displayMetrics.heightPixels) / displayMetrics.density), (int) (((float) displayMetrics.widthPixels) / displayMetrics.density));
        if (a2 != -1) {
            IMAdView iMAdView = new IMAdView((Activity) context, a2, this.f);
            if (a2 == 15) {
                i = 50;
                i2 = 320;
            } else {
                i = 50;
                i2 = 320;
            }
            if (a2 == 11) {
                i2 = 728;
                i = 90;
            }
            if (a2 == 12) {
                i2 = 468;
                i = 60;
            }
            if (a2 == 10) {
                i2 = 300;
                i = 250;
            }
            if (a2 == 13) {
                i3 = 120;
                i = 600;
            } else {
                i3 = i2;
            }
            float f2 = getResources().getDisplayMetrics().density;
            iMAdView.setLayoutParams(new LinearLayout.LayoutParams((int) ((((float) i3) * f2) + 0.5f), (int) ((((float) i) * f2) + 0.5f)));
            iMAdView.setIMAdListener(new Cdo(this));
            setGravity(17);
            addView(iMAdView);
            IMAdRequest iMAdRequest = new IMAdRequest();
            if (this.g) {
                ja.a(3, a, "InMobi AdView set to Test Mode.");
                iMAdRequest.setTestMode(true);
            }
            iMAdView.setIMAdRequest(iMAdRequest);
            iMAdView.setRefreshInterval(-1);
            iMAdView.loadNewAd();
            return;
        }
        ja.a(3, a, "**********Could not load InMobi Ad");
    }
}
