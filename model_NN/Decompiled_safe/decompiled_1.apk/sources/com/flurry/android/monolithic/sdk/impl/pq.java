package com.flurry.android.monolithic.sdk.impl;

public final class pq {
    protected final Object a;
    protected final boolean b;
    protected final afn c;
    protected byte[] d = null;
    protected byte[] e = null;
    protected char[] f = null;
    protected char[] g = null;
    protected char[] h = null;

    public pq(afn afn, Object obj, boolean z) {
        this.c = afn;
        this.a = obj;
        this.b = z;
    }

    public final Object a() {
        return this.a;
    }

    public final boolean b() {
        return this.b;
    }

    public final afy c() {
        return new afy(this.c);
    }

    public final char[] d() {
        if (this.f != null) {
            throw new IllegalStateException("Trying to call allocTokenBuffer() second time");
        }
        this.f = this.c.a(afp.TOKEN_BUFFER);
        return this.f;
    }

    public final char[] e() {
        if (this.g != null) {
            throw new IllegalStateException("Trying to call allocConcatBuffer() second time");
        }
        this.g = this.c.a(afp.CONCAT_BUFFER);
        return this.g;
    }

    public final char[] a(int i) {
        if (this.h != null) {
            throw new IllegalStateException("Trying to call allocNameCopyBuffer() second time");
        }
        this.h = this.c.a(afp.NAME_COPY_BUFFER, i);
        return this.h;
    }

    public final void a(char[] cArr) {
        if (cArr == null) {
            return;
        }
        if (cArr != this.f) {
            throw new IllegalArgumentException("Trying to release buffer not owned by the context");
        }
        this.f = null;
        this.c.a(afp.TOKEN_BUFFER, cArr);
    }

    public final void b(char[] cArr) {
        if (cArr == null) {
            return;
        }
        if (cArr != this.g) {
            throw new IllegalArgumentException("Trying to release buffer not owned by the context");
        }
        this.g = null;
        this.c.a(afp.CONCAT_BUFFER, cArr);
    }

    public final void c(char[] cArr) {
        if (cArr == null) {
            return;
        }
        if (cArr != this.h) {
            throw new IllegalArgumentException("Trying to release buffer not owned by the context");
        }
        this.h = null;
        this.c.a(afp.NAME_COPY_BUFFER, cArr);
    }
}
