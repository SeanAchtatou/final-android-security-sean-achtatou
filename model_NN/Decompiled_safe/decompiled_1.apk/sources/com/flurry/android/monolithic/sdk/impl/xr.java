package com.flurry.android.monolithic.sdk.impl;

public class xr extends qf<xq> {
    protected static final xq a;
    protected static final xq b = xq.a(null, adh.d((Class<?>) Boolean.TYPE), xh.b(Boolean.TYPE, null, null));
    protected static final xq c = xq.a(null, adh.d((Class<?>) Integer.TYPE), xh.b(Integer.TYPE, null, null));
    protected static final xq d = xq.a(null, adh.d((Class<?>) Long.TYPE), xh.b(Long.TYPE, null, null));
    @Deprecated
    public static final xt e = new xt();
    @Deprecated
    public static final xw f = new xw();
    @Deprecated
    public static final xv g = new xv();
    protected static final xz h = new xu();
    public static final xr i = new xr();

    static {
        Class<String> cls = String.class;
        Class<String> cls2 = String.class;
        Class<String> cls3 = String.class;
        a = xq.a(null, adh.d((Class<?>) cls), xh.b(cls, null, null));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.xr.a(com.flurry.android.monolithic.sdk.impl.rf<?>, com.flurry.android.monolithic.sdk.impl.afm, com.flurry.android.monolithic.sdk.impl.qg, boolean):com.flurry.android.monolithic.sdk.impl.yb
     arg types: [com.flurry.android.monolithic.sdk.impl.rq, com.flurry.android.monolithic.sdk.impl.afm, com.flurry.android.monolithic.sdk.impl.qg, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.xr.a(com.flurry.android.monolithic.sdk.impl.rf<?>, com.flurry.android.monolithic.sdk.impl.xh, com.flurry.android.monolithic.sdk.impl.afm, boolean):com.flurry.android.monolithic.sdk.impl.yb
      com.flurry.android.monolithic.sdk.impl.xr.a(com.flurry.android.monolithic.sdk.impl.rf<?>, com.flurry.android.monolithic.sdk.impl.afm, com.flurry.android.monolithic.sdk.impl.qg, boolean):com.flurry.android.monolithic.sdk.impl.yb */
    /* renamed from: b */
    public xq a(rq rqVar, afm afm, qg qgVar) {
        xq a2 = a(afm);
        if (a2 == null) {
            return xq.b(a((rf<?>) rqVar, afm, qgVar, true));
        }
        return a2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.xr.a(com.flurry.android.monolithic.sdk.impl.rf<?>, com.flurry.android.monolithic.sdk.impl.afm, com.flurry.android.monolithic.sdk.impl.qg, boolean):com.flurry.android.monolithic.sdk.impl.yb
     arg types: [com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.afm, com.flurry.android.monolithic.sdk.impl.qg, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.xr.a(com.flurry.android.monolithic.sdk.impl.rf<?>, com.flurry.android.monolithic.sdk.impl.xh, com.flurry.android.monolithic.sdk.impl.afm, boolean):com.flurry.android.monolithic.sdk.impl.yb
      com.flurry.android.monolithic.sdk.impl.xr.a(com.flurry.android.monolithic.sdk.impl.rf<?>, com.flurry.android.monolithic.sdk.impl.afm, com.flurry.android.monolithic.sdk.impl.qg, boolean):com.flurry.android.monolithic.sdk.impl.yb */
    /* renamed from: c */
    public xq a(qk qkVar, afm afm, qg qgVar) {
        xq a2 = a(afm);
        if (a2 == null) {
            return xq.a(a((rf<?>) qkVar, afm, qgVar, false));
        }
        return a2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.xr.a(com.flurry.android.monolithic.sdk.impl.rf<?>, com.flurry.android.monolithic.sdk.impl.afm, com.flurry.android.monolithic.sdk.impl.qg, boolean):com.flurry.android.monolithic.sdk.impl.yb
     arg types: [com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.afm, com.flurry.android.monolithic.sdk.impl.qg, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.xr.a(com.flurry.android.monolithic.sdk.impl.rf<?>, com.flurry.android.monolithic.sdk.impl.xh, com.flurry.android.monolithic.sdk.impl.afm, boolean):com.flurry.android.monolithic.sdk.impl.yb
      com.flurry.android.monolithic.sdk.impl.xr.a(com.flurry.android.monolithic.sdk.impl.rf<?>, com.flurry.android.monolithic.sdk.impl.afm, com.flurry.android.monolithic.sdk.impl.qg, boolean):com.flurry.android.monolithic.sdk.impl.yb */
    /* renamed from: d */
    public xq b(qk qkVar, afm afm, qg qgVar) {
        xq a2 = a(afm);
        if (a2 == null) {
            return xq.a(a((rf<?>) qkVar, afm, qgVar, false));
        }
        return a2;
    }

    /* renamed from: b */
    public xq a(rf<?> rfVar, afm afm, qg qgVar) {
        return xq.a(rfVar, afm, xh.a(afm.p(), rfVar.b() ? rfVar.a() : null, qgVar));
    }

    public yb a(rf<?> rfVar, afm afm, qg qgVar, boolean z) {
        xh c2 = c(rfVar, afm, qgVar);
        c2.a(h);
        c2.n();
        return a(rfVar, c2, afm, z).k();
    }

    /* access modifiers changed from: protected */
    public yb a(rf<?> rfVar, xh xhVar, afm afm, boolean z) {
        return new yb(rfVar, z, afm, xhVar);
    }

    public xh c(rf<?> rfVar, afm afm, qg qgVar) {
        xh a2 = xh.a(afm.p(), rfVar.b() ? rfVar.a() : null, qgVar);
        a2.a(h);
        a2.a(true);
        return a2;
    }

    /* access modifiers changed from: protected */
    public xq a(afm afm) {
        Class<?> p = afm.p();
        if (p == String.class) {
            return a;
        }
        if (p == Boolean.TYPE) {
            return b;
        }
        if (p == Integer.TYPE) {
            return c;
        }
        if (p == Long.TYPE) {
            return d;
        }
        return null;
    }
}
