package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.Collection;

@rz
public class acv extends abu<Collection<String>> implements rp {
    protected ra<String> a;

    public /* bridge */ /* synthetic */ void a(Object obj, or orVar, ru ruVar) throws IOException, oq {
        a((Collection<String>) ((Collection) obj), orVar, ruVar);
    }

    public /* bridge */ /* synthetic */ void a(Object obj, or orVar, ru ruVar, rx rxVar) throws IOException, oz {
        a((Collection<String>) ((Collection) obj), orVar, ruVar, rxVar);
    }

    public acv(qc qcVar) {
        super(Collection.class, qcVar);
    }

    public void a(ru ruVar) throws qw {
        ra a2 = ruVar.a(String.class, this.b);
        if (!a(a2)) {
            this.a = a2;
        }
    }

    public void a(Collection<String> collection, or orVar, ru ruVar) throws IOException, oq {
        orVar.b();
        if (this.a == null) {
            b(collection, orVar, ruVar);
        } else {
            c(collection, orVar, ruVar);
        }
        orVar.c();
    }

    public void a(Collection<String> collection, or orVar, ru ruVar, rx rxVar) throws IOException, oq {
        rxVar.c(collection, orVar);
        if (this.a == null) {
            b(collection, orVar, ruVar);
        } else {
            c(collection, orVar, ruVar);
        }
        rxVar.f(collection, orVar);
    }

    private final void b(Collection<String> collection, or orVar, ru ruVar) throws IOException, oq {
        int i;
        if (this.a != null) {
            c(collection, orVar, ruVar);
            return;
        }
        int i2 = 0;
        for (String next : collection) {
            if (next == null) {
                try {
                    ruVar.a(orVar);
                } catch (Exception e) {
                    a(ruVar, e, collection, i2);
                    i = i2;
                }
            } else {
                orVar.b(next);
            }
            i = i2 + 1;
            i2 = i;
        }
    }

    private void c(Collection<String> collection, or orVar, ru ruVar) throws IOException, oq {
        ra<String> raVar = this.a;
        for (String next : collection) {
            if (next == null) {
                try {
                    ruVar.a(orVar);
                } catch (Exception e) {
                    a(ruVar, e, collection, 0);
                }
            } else {
                raVar.a(next, orVar, ruVar);
            }
        }
    }
}
