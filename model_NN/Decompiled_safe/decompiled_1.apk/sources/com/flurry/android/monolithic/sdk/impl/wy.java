package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
public class wy extends wv<String> {
    public wy() {
        super(String.class);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.on.a(byte[], boolean):java.lang.String
     arg types: [byte[], int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.on.a(java.lang.StringBuilder, int):void
      com.flurry.android.monolithic.sdk.impl.on.a(byte[], boolean):java.lang.String */
    /* renamed from: b */
    public String a(ow owVar, qm qmVar) throws IOException, oz {
        pb e = owVar.e();
        if (e == pb.VALUE_STRING) {
            return owVar.k();
        }
        if (e == pb.VALUE_EMBEDDED_OBJECT) {
            Object z = owVar.z();
            if (z == null) {
                return null;
            }
            if (z instanceof byte[]) {
                return oo.a().a((byte[]) z, false);
            }
            return z.toString();
        } else if (e.d()) {
            return owVar.k();
        } else {
            throw qmVar.a(this.q, e);
        }
    }

    /* renamed from: b */
    public String a(ow owVar, qm qmVar, rw rwVar) throws IOException, oz {
        return a(owVar, qmVar);
    }
}
