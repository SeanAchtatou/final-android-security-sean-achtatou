package com.flurry.android.monolithic.sdk.impl;

import java.lang.reflect.Constructor;

final class wq extends we {
    protected final Constructor<?> b;

    public wq(Constructor<?> constructor) {
        super(constructor.getDeclaringClass());
        this.b = constructor;
    }

    public Object b(String str, qm qmVar) throws Exception {
        return this.b.newInstance(str);
    }
}
