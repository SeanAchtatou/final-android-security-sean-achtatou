package com.flurry.android.monolithic.sdk.impl;

import android.provider.Settings;
import android.text.TextUtils;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import v2.com.playhaven.model.PHContent;

public final class ip {
    private static final String a = ip.class.getSimpleName();
    private static final Set<String> b = i();
    private static String c;

    public static synchronized String a() {
        String str;
        synchronized (ip.class) {
            if (TextUtils.isEmpty(c)) {
                c = g();
            }
            str = c;
        }
        return str;
    }

    private static String g() {
        String b2 = b();
        return !TextUtils.isEmpty(b2) ? b2 : c();
    }

    static String b() {
        String string = Settings.Secure.getString(ia.a().b().getContentResolver(), "android_id");
        if (!a(string)) {
            return null;
        }
        return "AND" + string;
    }

    static String c() {
        String e = e();
        if (TextUtils.isEmpty(e)) {
            e = f();
            if (TextUtils.isEmpty(e)) {
                e = d();
            }
            b(e);
        }
        return e;
    }

    static boolean a(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        if (c(str.toLowerCase(Locale.US))) {
            return false;
        }
        return true;
    }

    static String d() {
        return "ID" + Long.toString(Double.doubleToLongBits(Math.random()) + (37 * (System.nanoTime() + ((long) (il.c(ia.a().b()).hashCode() * 37)))), 16);
    }

    static void b(String str) {
        if (!TextUtils.isEmpty(str)) {
            File fileStreamPath = ia.a().b().getFileStreamPath(h());
            if (iw.a(fileStreamPath)) {
                a(str, fileStreamPath);
            }
        }
    }

    static void a(String str, File file) {
        DataOutputStream dataOutputStream;
        Throwable th;
        Throwable th2;
        try {
            dataOutputStream = new DataOutputStream(new FileOutputStream(file));
            try {
                a(str, dataOutputStream);
                je.a(dataOutputStream);
            } catch (Throwable th3) {
                th2 = th3;
                try {
                    ja.a(6, a, "Error when saving phoneId", th2);
                    je.a(dataOutputStream);
                } catch (Throwable th4) {
                    th = th4;
                    je.a(dataOutputStream);
                    throw th;
                }
            }
        } catch (Throwable th5) {
            Throwable th6 = th5;
            dataOutputStream = null;
            th = th6;
            je.a(dataOutputStream);
            throw th;
        }
    }

    static String e() {
        DataInputStream dataInputStream;
        File fileStreamPath = ia.a().b().getFileStreamPath(h());
        if (fileStreamPath == null || !fileStreamPath.exists()) {
            return null;
        }
        try {
            dataInputStream = new DataInputStream(new FileInputStream(fileStreamPath));
            try {
                String a2 = a(dataInputStream);
                je.a(dataInputStream);
                return a2;
            } catch (Throwable th) {
                th = th;
                try {
                    ja.a(6, a, "Error when loading phoneId", th);
                    je.a(dataInputStream);
                    return null;
                } catch (Throwable th2) {
                    th = th2;
                    je.a(dataInputStream);
                    throw th;
                }
            }
        } catch (Throwable th3) {
            th = th3;
            dataInputStream = null;
            je.a(dataInputStream);
            throw th;
        }
    }

    static String f() {
        DataInputStream dataInputStream;
        File filesDir = ia.a().b().getFilesDir();
        if (filesDir == null) {
            return null;
        }
        String[] list = filesDir.list(new iq());
        if (list == null || list.length == 0) {
            return null;
        }
        File fileStreamPath = ia.a().b().getFileStreamPath(list[0]);
        if (fileStreamPath == null || !fileStreamPath.exists()) {
            return null;
        }
        try {
            dataInputStream = new DataInputStream(new FileInputStream(fileStreamPath));
            try {
                String b2 = b(dataInputStream);
                je.a(dataInputStream);
                return b2;
            } catch (Throwable th) {
                th = th;
                try {
                    ja.a(6, a, "Error when loading phoneId", th);
                    je.a(dataInputStream);
                    return null;
                } catch (Throwable th2) {
                    th = th2;
                    je.a(dataInputStream);
                    throw th;
                }
            }
        } catch (Throwable th3) {
            th = th3;
            dataInputStream = null;
            je.a(dataInputStream);
            throw th;
        }
    }

    private static void a(String str, DataOutput dataOutput) throws IOException {
        dataOutput.writeInt(1);
        dataOutput.writeUTF(str);
    }

    private static String a(DataInput dataInput) throws IOException {
        if (1 != dataInput.readInt()) {
            return null;
        }
        return dataInput.readUTF();
    }

    private static String b(DataInput dataInput) throws IOException {
        if (46586 != dataInput.readUnsignedShort()) {
            return null;
        }
        if (2 != dataInput.readUnsignedShort()) {
            return null;
        }
        dataInput.readUTF();
        return dataInput.readUTF();
    }

    private static String h() {
        return ".flurryb.";
    }

    private static boolean c(String str) {
        return b.contains(str);
    }

    private static Set<String> i() {
        HashSet hashSet = new HashSet();
        hashSet.add(PHContent.PARCEL_NULL);
        hashSet.add("9774d56d682e549c");
        hashSet.add("dead00beef");
        return Collections.unmodifiableSet(hashSet);
    }
}
