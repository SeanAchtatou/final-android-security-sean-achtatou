package com.flurry.android.monolithic.sdk.impl;

public enum afp {
    TOKEN_BUFFER(2000),
    CONCAT_BUFFER(2000),
    TEXT_BUFFER(200),
    NAME_COPY_BUFFER(200);
    
    private final int e;

    private afp(int i) {
        this.e = i;
    }
}
