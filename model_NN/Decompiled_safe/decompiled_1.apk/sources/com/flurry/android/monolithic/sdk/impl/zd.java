package com.flurry.android.monolithic.sdk.impl;

import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.nio.charset.Charset;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.RandomAccess;
import java.util.TimeZone;

public abstract class zd extends rs {
    protected static final HashMap<String, ra<?>> a = new HashMap<>();
    protected static final HashMap<String, Class<? extends ra<?>>> b = new HashMap<>();
    protected static final HashMap<String, ra<?>> c = new HashMap<>();
    protected xf d = xf.a;

    /* access modifiers changed from: protected */
    public abstract Iterable<rv> a();

    static {
        a.put(String.class.getName(), new acw());
        acy acy = acy.a;
        a.put(StringBuffer.class.getName(), acy);
        a.put(StringBuilder.class.getName(), acy);
        a.put(Character.class.getName(), acy);
        a.put(Character.TYPE.getName(), acy);
        a.put(Boolean.TYPE.getName(), new zz(true));
        a.put(Boolean.class.getName(), new zz(false));
        aad aad = new aad();
        a.put(Integer.class.getName(), aad);
        a.put(Integer.TYPE.getName(), aad);
        a.put(Long.class.getName(), aae.a);
        a.put(Long.TYPE.getName(), aae.a);
        a.put(Byte.class.getName(), aac.a);
        a.put(Byte.TYPE.getName(), aac.a);
        a.put(Short.class.getName(), aac.a);
        a.put(Short.TYPE.getName(), aac.a);
        a.put(Float.class.getName(), aab.a);
        a.put(Float.TYPE.getName(), aab.a);
        a.put(Double.class.getName(), aaa.a);
        a.put(Double.TYPE.getName(), aaa.a);
        aaf aaf = new aaf();
        a.put(BigInteger.class.getName(), aaf);
        a.put(BigDecimal.class.getName(), aaf);
        a.put(Calendar.class.getName(), aba.a);
        abd abd = abd.a;
        a.put(Date.class.getName(), abd);
        a.put(Timestamp.class.getName(), abd);
        a.put(java.sql.Date.class.getName(), new aag());
        a.put(Time.class.getName(), new aah());
        for (Map.Entry next : new acj().a()) {
            Object value = next.getValue();
            if (value instanceof ra) {
                a.put(((Class) next.getKey()).getName(), (ra) value);
            } else if (value instanceof Class) {
                b.put(((Class) next.getKey()).getName(), (Class) value);
            } else {
                throw new IllegalStateException("Internal error: unrecognized value of type " + next.getClass().getName());
            }
        }
        b.put(afz.class.getName(), acz.class);
        c.put(boolean[].class.getName(), new abx());
        c.put(byte[].class.getName(), new aby());
        c.put(char[].class.getName(), new abz());
        c.put(short[].class.getName(), new ace());
        c.put(int[].class.getName(), new acc());
        c.put(long[].class.getName(), new acd());
        c.put(float[].class.getName(), new acb());
        c.put(double[].class.getName(), new aca());
    }

    protected zd() {
    }

    public rx b(rq rqVar, afm afm, qc qcVar) {
        Collection<yg> a2;
        yj<?> yjVar;
        xh c2 = ((xq) rqVar.c(afm.p())).c();
        py a3 = rqVar.a();
        yj<?> a4 = a3.a(rqVar, c2, afm);
        if (a4 == null) {
            yjVar = rqVar.d(afm);
            a2 = null;
        } else {
            a2 = rqVar.l().a(c2, rqVar, a3);
            yjVar = a4;
        }
        if (yjVar == null) {
            return null;
        }
        return yjVar.a(rqVar, afm, a2, qcVar);
    }

    public final ra<?> a(afm afm, rq rqVar, xq xqVar, qc qcVar, boolean z) {
        String name = afm.p().getName();
        ra<?> raVar = a.get(name);
        if (raVar != null) {
            return raVar;
        }
        Class cls = b.get(name);
        if (cls == null) {
            return null;
        }
        try {
            return (ra) cls.newInstance();
        } catch (Exception e) {
            throw new IllegalStateException("Failed to instantiate standard serializer (of type " + cls.getName() + "): " + e.getMessage(), e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.abf.a(java.lang.Class<java.lang.Enum<?>>, com.flurry.android.monolithic.sdk.impl.rq, com.flurry.android.monolithic.sdk.impl.xq):com.flurry.android.monolithic.sdk.impl.abf
     arg types: [java.lang.Class<?>, com.flurry.android.monolithic.sdk.impl.rq, com.flurry.android.monolithic.sdk.impl.xq]
     candidates:
      com.flurry.android.monolithic.sdk.impl.abf.a(java.lang.Enum<?>, com.flurry.android.monolithic.sdk.impl.or, com.flurry.android.monolithic.sdk.impl.ru):void
      com.flurry.android.monolithic.sdk.impl.abf.a(java.lang.Object, com.flurry.android.monolithic.sdk.impl.or, com.flurry.android.monolithic.sdk.impl.ru):void
      com.flurry.android.monolithic.sdk.impl.abt.a(java.lang.Object, com.flurry.android.monolithic.sdk.impl.or, com.flurry.android.monolithic.sdk.impl.ru):void
      com.flurry.android.monolithic.sdk.impl.ra.a(java.lang.Object, com.flurry.android.monolithic.sdk.impl.or, com.flurry.android.monolithic.sdk.impl.ru):void
      com.flurry.android.monolithic.sdk.impl.abf.a(java.lang.Class<java.lang.Enum<?>>, com.flurry.android.monolithic.sdk.impl.rq, com.flurry.android.monolithic.sdk.impl.xq):com.flurry.android.monolithic.sdk.impl.abf */
    public final ra<?> b(afm afm, rq rqVar, xq xqVar, qc qcVar, boolean z) throws qw {
        Class<?> p = afm.p();
        if (!qy.class.isAssignableFrom(p)) {
            xl e = xqVar.e();
            if (e != null) {
                Method e2 = e.a();
                if (rqVar.a(rr.CAN_OVERRIDE_ACCESS_MODIFIERS)) {
                    adz.a((Member) e2);
                }
                return new abk(e2, a(rqVar, e, qcVar), qcVar);
            } else if (InetAddress.class.isAssignableFrom(p)) {
                return abi.a;
            } else {
                if (TimeZone.class.isAssignableFrom(p)) {
                    return acx.a;
                }
                if (Charset.class.isAssignableFrom(p)) {
                    return acy.a;
                }
                ra<?> a2 = this.d.a(rqVar, afm);
                if (a2 != null) {
                    return a2;
                }
                if (Number.class.isAssignableFrom(p)) {
                    return aaf.a;
                }
                if (Enum.class.isAssignableFrom(p)) {
                    return abf.a((Class<Enum<?>>) p, rqVar, xqVar);
                }
                if (Calendar.class.isAssignableFrom(p)) {
                    return aba.a;
                }
                if (Date.class.isAssignableFrom(p)) {
                    return abd.a;
                }
                return null;
            }
        } else if (qz.class.isAssignableFrom(p)) {
            return abs.a;
        } else {
            return abr.a;
        }
    }

    public final ra<?> a(rq rqVar, afm afm, xq xqVar, qc qcVar, boolean z) throws qw {
        Class<?> p = afm.p();
        if (Iterator.class.isAssignableFrom(p)) {
            return c(rqVar, afm, xqVar, qcVar, z);
        }
        if (Iterable.class.isAssignableFrom(p)) {
            return d(rqVar, afm, xqVar, qcVar, z);
        }
        if (CharSequence.class.isAssignableFrom(p)) {
            return acy.a;
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public ra<Object> a(rq rqVar, xg xgVar, qc qcVar) throws qw {
        Object b2 = rqVar.a().b(xgVar);
        if (b2 == null) {
            return null;
        }
        if (b2 instanceof ra) {
            ra<Object> raVar = (ra) b2;
            return raVar instanceof qj ? ((qj) raVar).a(rqVar, qcVar) : raVar;
        } else if (!(b2 instanceof Class)) {
            throw new IllegalStateException("AnnotationIntrospector returned value of type " + b2.getClass().getName() + "; expected type JsonSerializer or Class<JsonSerializer> instead");
        } else {
            Class cls = (Class) b2;
            if (!ra.class.isAssignableFrom(cls)) {
                throw new IllegalStateException("AnnotationIntrospector returned Class " + cls.getName() + "; expected Class<JsonSerializer>");
            }
            ra<Object> a2 = rqVar.a(xgVar, cls);
            return a2 instanceof qj ? ((qj) a2).a(rqVar, qcVar) : a2;
        }
    }

    public ra<?> b(rq rqVar, afm afm, xq xqVar, qc qcVar, boolean z) {
        boolean z2;
        rx b2 = b(rqVar, afm.g(), qcVar);
        if (b2 != null) {
            z2 = false;
        } else if (!z) {
            z2 = a(rqVar, xqVar, b2, qcVar);
        } else {
            z2 = z;
        }
        ra<Object> c2 = c(rqVar, xqVar.c(), qcVar);
        if (afm.j()) {
            adf adf = (adf) afm;
            ra<Object> b3 = b(rqVar, xqVar.c(), qcVar);
            if (!adf.l()) {
                return a(rqVar, adf, xqVar, qcVar, z2, b3, b2, c2);
            }
            return a(rqVar, (adg) adf, xqVar, qcVar, z2, b3, b2, c2);
        } else if (afm.i()) {
            adc adc = (adc) afm;
            if (!adc.a_()) {
                return a(rqVar, adc, xqVar, qcVar, z2, b2, c2);
            }
            return a(rqVar, (add) adc, xqVar, qcVar, z2, b2, c2);
        } else if (!afm.b()) {
            return null;
        } else {
            return a(rqVar, (ada) afm, xqVar, qcVar, z2, b2, c2);
        }
    }

    /* access modifiers changed from: protected */
    public ra<?> a(rq rqVar, adc adc, xq xqVar, qc qcVar, boolean z, rx rxVar, ra<Object> raVar) {
        for (rv a2 : a()) {
            ra<?> a3 = a2.a(rqVar, adc, xqVar, qcVar, rxVar, raVar);
            if (a3 != null) {
                return a3;
            }
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.rv.a(com.flurry.android.monolithic.sdk.impl.rq, com.flurry.android.monolithic.sdk.impl.add, com.flurry.android.monolithic.sdk.impl.qb, com.flurry.android.monolithic.sdk.impl.qc, com.flurry.android.monolithic.sdk.impl.rx, com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object>):com.flurry.android.monolithic.sdk.impl.ra<?>
     arg types: [com.flurry.android.monolithic.sdk.impl.rq, com.flurry.android.monolithic.sdk.impl.add, com.flurry.android.monolithic.sdk.impl.xq, com.flurry.android.monolithic.sdk.impl.qc, com.flurry.android.monolithic.sdk.impl.rx, com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object>]
     candidates:
      com.flurry.android.monolithic.sdk.impl.rv.a(com.flurry.android.monolithic.sdk.impl.rq, com.flurry.android.monolithic.sdk.impl.adc, com.flurry.android.monolithic.sdk.impl.qb, com.flurry.android.monolithic.sdk.impl.qc, com.flurry.android.monolithic.sdk.impl.rx, com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object>):com.flurry.android.monolithic.sdk.impl.ra<?>
      com.flurry.android.monolithic.sdk.impl.rv.a(com.flurry.android.monolithic.sdk.impl.rq, com.flurry.android.monolithic.sdk.impl.add, com.flurry.android.monolithic.sdk.impl.qb, com.flurry.android.monolithic.sdk.impl.qc, com.flurry.android.monolithic.sdk.impl.rx, com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object>):com.flurry.android.monolithic.sdk.impl.ra<?> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.zd.a(com.flurry.android.monolithic.sdk.impl.rq, com.flurry.android.monolithic.sdk.impl.afm, com.flurry.android.monolithic.sdk.impl.xq, com.flurry.android.monolithic.sdk.impl.qc, boolean, com.flurry.android.monolithic.sdk.impl.rx, com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object>):com.flurry.android.monolithic.sdk.impl.ra<?>
     arg types: [com.flurry.android.monolithic.sdk.impl.rq, com.flurry.android.monolithic.sdk.impl.add, com.flurry.android.monolithic.sdk.impl.xq, com.flurry.android.monolithic.sdk.impl.qc, boolean, com.flurry.android.monolithic.sdk.impl.rx, com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object>]
     candidates:
      com.flurry.android.monolithic.sdk.impl.zd.a(com.flurry.android.monolithic.sdk.impl.rq, com.flurry.android.monolithic.sdk.impl.ada, com.flurry.android.monolithic.sdk.impl.xq, com.flurry.android.monolithic.sdk.impl.qc, boolean, com.flurry.android.monolithic.sdk.impl.rx, com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object>):com.flurry.android.monolithic.sdk.impl.ra<?>
      com.flurry.android.monolithic.sdk.impl.zd.a(com.flurry.android.monolithic.sdk.impl.rq, com.flurry.android.monolithic.sdk.impl.adc, com.flurry.android.monolithic.sdk.impl.xq, com.flurry.android.monolithic.sdk.impl.qc, boolean, com.flurry.android.monolithic.sdk.impl.rx, com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object>):com.flurry.android.monolithic.sdk.impl.ra<?>
      com.flurry.android.monolithic.sdk.impl.zd.a(com.flurry.android.monolithic.sdk.impl.rq, com.flurry.android.monolithic.sdk.impl.add, com.flurry.android.monolithic.sdk.impl.xq, com.flurry.android.monolithic.sdk.impl.qc, boolean, com.flurry.android.monolithic.sdk.impl.rx, com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object>):com.flurry.android.monolithic.sdk.impl.ra<?>
      com.flurry.android.monolithic.sdk.impl.zd.a(com.flurry.android.monolithic.sdk.impl.rq, com.flurry.android.monolithic.sdk.impl.afm, com.flurry.android.monolithic.sdk.impl.xq, com.flurry.android.monolithic.sdk.impl.qc, boolean, com.flurry.android.monolithic.sdk.impl.rx, com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object>):com.flurry.android.monolithic.sdk.impl.ra<?> */
    /* access modifiers changed from: protected */
    public ra<?> a(rq rqVar, add add, xq xqVar, qc qcVar, boolean z, rx rxVar, ra<Object> raVar) {
        for (rv a2 : a()) {
            ra<?> a3 = a2.a(rqVar, add, (qb) xqVar, qcVar, rxVar, raVar);
            if (a3 != null) {
                return a3;
            }
        }
        Class<?> p = add.p();
        if (EnumSet.class.isAssignableFrom(p)) {
            return a(rqVar, (afm) add, xqVar, qcVar, z, rxVar, raVar);
        }
        Class<?> p2 = add.g().p();
        if (a(p)) {
            if (p2 == String.class) {
                return new abh(qcVar);
            }
            return acg.a(add.g(), z, rxVar, qcVar, raVar);
        } else if (p2 == String.class) {
            return new acv(qcVar);
        } else {
            return acg.b(add.g(), z, rxVar, qcVar, raVar);
        }
    }

    /* access modifiers changed from: protected */
    public ra<?> a(rq rqVar, afm afm, xq xqVar, qc qcVar, boolean z, rx rxVar, ra<Object> raVar) {
        afm g = afm.g();
        if (!g.r()) {
            g = null;
        }
        return acg.a(g, qcVar);
    }

    /* access modifiers changed from: protected */
    public boolean a(Class<?> cls) {
        return RandomAccess.class.isAssignableFrom(cls);
    }

    /* access modifiers changed from: protected */
    public ra<?> a(rq rqVar, adf adf, xq xqVar, qc qcVar, boolean z, ra<Object> raVar, rx rxVar, ra<Object> raVar2) {
        for (rv a2 : a()) {
            ra<?> a3 = a2.a(rqVar, adf, xqVar, qcVar, raVar, rxVar, raVar2);
            if (a3 != null) {
                return a3;
            }
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.rv.a(com.flurry.android.monolithic.sdk.impl.rq, com.flurry.android.monolithic.sdk.impl.adg, com.flurry.android.monolithic.sdk.impl.qb, com.flurry.android.monolithic.sdk.impl.qc, com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object>, com.flurry.android.monolithic.sdk.impl.rx, com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object>):com.flurry.android.monolithic.sdk.impl.ra<?>
     arg types: [com.flurry.android.monolithic.sdk.impl.rq, com.flurry.android.monolithic.sdk.impl.adg, com.flurry.android.monolithic.sdk.impl.xq, com.flurry.android.monolithic.sdk.impl.qc, com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object>, com.flurry.android.monolithic.sdk.impl.rx, com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object>]
     candidates:
      com.flurry.android.monolithic.sdk.impl.rv.a(com.flurry.android.monolithic.sdk.impl.rq, com.flurry.android.monolithic.sdk.impl.adf, com.flurry.android.monolithic.sdk.impl.qb, com.flurry.android.monolithic.sdk.impl.qc, com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object>, com.flurry.android.monolithic.sdk.impl.rx, com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object>):com.flurry.android.monolithic.sdk.impl.ra<?>
      com.flurry.android.monolithic.sdk.impl.rv.a(com.flurry.android.monolithic.sdk.impl.rq, com.flurry.android.monolithic.sdk.impl.adg, com.flurry.android.monolithic.sdk.impl.qb, com.flurry.android.monolithic.sdk.impl.qc, com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object>, com.flurry.android.monolithic.sdk.impl.rx, com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object>):com.flurry.android.monolithic.sdk.impl.ra<?> */
    /* access modifiers changed from: protected */
    public ra<?> a(rq rqVar, adg adg, xq xqVar, qc qcVar, boolean z, ra<Object> raVar, rx rxVar, ra<Object> raVar2) {
        for (rv a2 : a()) {
            ra<?> a3 = a2.a(rqVar, adg, (qb) xqVar, qcVar, raVar, rxVar, raVar2);
            if (a3 != null) {
                return a3;
            }
        }
        if (EnumMap.class.isAssignableFrom(adg.p())) {
            return b(rqVar, adg, xqVar, qcVar, z, rxVar, raVar2);
        }
        return abl.a(rqVar.a().c(xqVar.c()), adg, z, rxVar, qcVar, raVar, raVar2);
    }

    /* access modifiers changed from: protected */
    public ra<?> b(rq rqVar, afm afm, xq xqVar, qc qcVar, boolean z, rx rxVar, ra<Object> raVar) {
        aee aee;
        afm k = afm.k();
        if (k.r()) {
            aee = aee.a(k.p(), rqVar.a());
        } else {
            aee = null;
        }
        return new abe(afm.g(), z, aee, rxVar, qcVar, raVar);
    }

    /* access modifiers changed from: protected */
    public ra<?> a(rq rqVar, ada ada, xq xqVar, qc qcVar, boolean z, rx rxVar, ra<Object> raVar) {
        Class<?> p = ada.p();
        if (String[].class == p) {
            return new acf(qcVar);
        }
        ra<?> raVar2 = c.get(p.getName());
        if (raVar2 != null) {
            return raVar2;
        }
        return new abo(ada.g(), z, rxVar, qcVar, raVar);
    }

    /* access modifiers changed from: protected */
    public ra<?> c(rq rqVar, afm afm, xq xqVar, qc qcVar, boolean z) {
        afm b2 = afm.b(0);
        if (b2 == null) {
            b2 = adk.b();
        }
        rx b3 = b(rqVar, b2, qcVar);
        return acg.a(b2, a(rqVar, xqVar, b3, qcVar), b3, qcVar);
    }

    /* access modifiers changed from: protected */
    public ra<?> d(rq rqVar, afm afm, xq xqVar, qc qcVar, boolean z) {
        afm b2 = afm.b(0);
        if (b2 == null) {
            b2 = adk.b();
        }
        rx b3 = b(rqVar, b2, qcVar);
        return acg.b(b2, a(rqVar, xqVar, b3, qcVar), b3, qcVar);
    }

    /* access modifiers changed from: protected */
    public <T extends afm> T a(rq rqVar, xg xgVar, afm afm) {
        afm afm2;
        Class<?> e = rqVar.a().e(xgVar);
        if (e != null) {
            try {
                afm2 = afm.h(e);
            } catch (IllegalArgumentException e2) {
                throw new IllegalArgumentException("Failed to widen type " + afm + " with concrete-type annotation (value " + e.getName() + "), method '" + xgVar.b() + "': " + e2.getMessage());
            }
        } else {
            afm2 = afm;
        }
        return b(rqVar, xgVar, afm2);
    }

    protected static <T extends afm> T b(rq rqVar, xg xgVar, afm afm) {
        afm afm2;
        py a2 = rqVar.a();
        if (!afm.f()) {
            return afm;
        }
        Class<?> a3 = a2.a(xgVar, afm.k());
        if (a3 == null) {
            afm2 = afm;
        } else if (!(afm instanceof adg)) {
            throw new IllegalArgumentException("Illegal key-type annotation: type " + afm + " is not a Map type");
        } else {
            try {
                afm2 = ((adg) afm).e(a3);
            } catch (IllegalArgumentException e) {
                throw new IllegalArgumentException("Failed to narrow key type " + afm + " with key-type annotation (" + a3.getName() + "): " + e.getMessage());
            }
        }
        Class<?> b2 = a2.b(xgVar, afm2.g());
        if (b2 == null) {
            return afm2;
        }
        try {
            return afm2.c(b2);
        } catch (IllegalArgumentException e2) {
            throw new IllegalArgumentException("Failed to narrow content type " + afm2 + " with content-type annotation (" + b2.getName() + "): " + e2.getMessage());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000e, code lost:
        if (r1 == r3) goto L_0x0010;
     */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object> b(com.flurry.android.monolithic.sdk.impl.rq r4, com.flurry.android.monolithic.sdk.impl.xg r5, com.flurry.android.monolithic.sdk.impl.qc r6) {
        /*
            java.lang.Class<com.flurry.android.monolithic.sdk.impl.rb> r3 = com.flurry.android.monolithic.sdk.impl.rb.class
            com.flurry.android.monolithic.sdk.impl.py r0 = r4.a()
            java.lang.Class r1 = r0.c(r5)
            if (r1 == 0) goto L_0x0010
            java.lang.Class<com.flurry.android.monolithic.sdk.impl.rb> r2 = com.flurry.android.monolithic.sdk.impl.rb.class
            if (r1 != r3) goto L_0x0027
        L_0x0010:
            if (r6 == 0) goto L_0x0027
            com.flurry.android.monolithic.sdk.impl.xk r1 = r6.b()
            java.lang.Class r0 = r0.c(r1)
        L_0x001a:
            if (r0 == 0) goto L_0x0025
            java.lang.Class<com.flurry.android.monolithic.sdk.impl.rb> r1 = com.flurry.android.monolithic.sdk.impl.rb.class
            if (r0 == r3) goto L_0x0025
            com.flurry.android.monolithic.sdk.impl.ra r0 = r4.a(r5, r0)
        L_0x0024:
            return r0
        L_0x0025:
            r0 = 0
            goto L_0x0024
        L_0x0027:
            r0 = r1
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.zd.b(com.flurry.android.monolithic.sdk.impl.rq, com.flurry.android.monolithic.sdk.impl.xg, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.ra");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000e, code lost:
        if (r1 == r3) goto L_0x0010;
     */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object> c(com.flurry.android.monolithic.sdk.impl.rq r4, com.flurry.android.monolithic.sdk.impl.xg r5, com.flurry.android.monolithic.sdk.impl.qc r6) {
        /*
            java.lang.Class<com.flurry.android.monolithic.sdk.impl.rb> r3 = com.flurry.android.monolithic.sdk.impl.rb.class
            com.flurry.android.monolithic.sdk.impl.py r0 = r4.a()
            java.lang.Class r1 = r0.d(r5)
            if (r1 == 0) goto L_0x0010
            java.lang.Class<com.flurry.android.monolithic.sdk.impl.rb> r2 = com.flurry.android.monolithic.sdk.impl.rb.class
            if (r1 != r3) goto L_0x0027
        L_0x0010:
            if (r6 == 0) goto L_0x0027
            com.flurry.android.monolithic.sdk.impl.xk r1 = r6.b()
            java.lang.Class r0 = r0.d(r1)
        L_0x001a:
            if (r0 == 0) goto L_0x0025
            java.lang.Class<com.flurry.android.monolithic.sdk.impl.rb> r1 = com.flurry.android.monolithic.sdk.impl.rb.class
            if (r0 == r3) goto L_0x0025
            com.flurry.android.monolithic.sdk.impl.ra r0 = r4.a(r5, r0)
        L_0x0024:
            return r0
        L_0x0025:
            r0 = 0
            goto L_0x0024
        L_0x0027:
            r0 = r1
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.zd.c(com.flurry.android.monolithic.sdk.impl.rq, com.flurry.android.monolithic.sdk.impl.xg, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.ra");
    }

    /* access modifiers changed from: protected */
    public boolean a(rq rqVar, xq xqVar, rx rxVar, qc qcVar) {
        if (rxVar != null) {
            return false;
        }
        py a2 = rqVar.a();
        sg f = a2.f((xg) xqVar.c());
        if (f != null) {
            if (f == sg.STATIC) {
                return true;
            }
        } else if (rqVar.a(rr.USE_STATIC_TYPING)) {
            return true;
        }
        if (qcVar != null) {
            afm a3 = qcVar.a();
            if (a3.f()) {
                if (a2.b(qcVar.b(), qcVar.a()) != null) {
                    return true;
                }
                if ((a3 instanceof adg) && a2.a(qcVar.b(), qcVar.a()) != null) {
                    return true;
                }
            }
        }
        return false;
    }
}
