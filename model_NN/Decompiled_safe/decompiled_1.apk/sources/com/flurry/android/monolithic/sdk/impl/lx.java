package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.nio.ByteBuffer;

public abstract class lx {
    public abstract nw a(nw nwVar) throws IOException;

    public abstract ByteBuffer a(ByteBuffer byteBuffer) throws IOException;

    public abstract void a(int i) throws IOException;

    public abstract void b() throws IOException;

    public abstract void b(byte[] bArr, int i, int i2) throws IOException;

    public abstract boolean c() throws IOException;

    public abstract int d() throws IOException;

    public abstract long e() throws IOException;

    public abstract float f() throws IOException;

    public abstract double g() throws IOException;

    public abstract String h() throws IOException;

    public abstract void i() throws IOException;

    public abstract void j() throws IOException;

    public abstract int k() throws IOException;

    public abstract long m() throws IOException;

    public abstract long n() throws IOException;

    public abstract long o() throws IOException;

    public abstract long p() throws IOException;

    public abstract long q() throws IOException;

    public abstract long r() throws IOException;

    public abstract int s() throws IOException;
}
