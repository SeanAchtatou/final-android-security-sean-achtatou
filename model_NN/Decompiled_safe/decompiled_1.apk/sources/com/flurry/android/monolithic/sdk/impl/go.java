package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import android.text.TextUtils;
import com.flurry.android.impl.appcloud.AppCloudModule;

public class go {
    static int a = 0;
    static int b = 5;
    private static String c = "appcloud.flurry.com";
    private static String d = "appcloud-node-stage.corp.flurry.com";
    private static final String e = go.class.getSimpleName();
    private static hi f = null;
    private static gn g = null;
    private static gj h = null;
    private static gl i = null;
    private static gm j = null;
    private static boolean k = true;
    private static String l;
    private static boolean m = false;
    private static AppCloudModule n = AppCloudModule.getInstance();

    public static void a(Context context, String str) {
        b(context, str);
        g.a();
    }

    private static void b(Context context, String str) {
        i = new gl(context, str);
        j = new gm(context, str);
    }

    public static Object a(gq gqVar) {
        switch (gp.a[gqVar.ordinal()]) {
            case 1:
                return i;
            case 2:
                return j;
            default:
                return null;
        }
    }

    public static hi a() {
        return f;
    }

    public static gn b() {
        return g;
    }

    public static gj c() {
        return h;
    }

    public static void a(String str) {
        gr.a(str);
    }

    public static void b(String str) {
        l = str;
    }

    public static String d() {
        if (!TextUtils.isEmpty(l)) {
            return l;
        }
        if (k) {
            return c;
        }
        return d;
    }

    private static void b(Context context) {
        if (!m) {
            if (!n.a()) {
                ja.a(3, e, "Initializing Flurry AppCloud");
                fl flVar = new fl();
                flVar.a = eg.a().f();
                AppCloudModule.getInstance().a(context, flVar);
                ja.a(3, e, "Flurry AppCloud initialized");
            }
            m = true;
        }
    }

    public static void a(Context context) {
        if (context == null) {
            try {
                ja.b(e, "Context passed to AppCLoudModule was null.");
            } catch (Throwable th) {
                ja.a(6, e, "", th);
                return;
            }
        }
        ia.a(context);
        b(context);
        h = new gj();
        f = new hi();
        g = new gn();
        fy.a();
        String d2 = fy.d();
        if (!TextUtils.isEmpty(d2)) {
            a(context, d2);
        }
    }
}
