package com.flurry.android.monolithic.sdk.impl;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

public abstract class xo extends xk {
    protected final xp[] d;

    public abstract Object a(Object obj) throws Exception;

    public abstract Object a(Object[] objArr) throws Exception;

    public abstract Type b(int i);

    public abstract Object g() throws Exception;

    protected xo(xp xpVar, xp[] xpVarArr) {
        super(xpVar);
        this.d = xpVarArr;
    }

    public final void a(Annotation annotation) {
        this.b.b(annotation);
    }

    public final void a(int i, Annotation annotation) {
        xp xpVar = this.d[i];
        if (xpVar == null) {
            xpVar = new xp();
            this.d[i] = xpVar;
        }
        xpVar.b(annotation);
    }

    public final void b(Annotation annotation) {
        this.b.a(annotation);
    }

    /* access modifiers changed from: protected */
    public xn a(int i, xp xpVar) {
        this.d[i] = xpVar;
        return c(i);
    }

    /* access modifiers changed from: protected */
    public afm a(adj adj, TypeVariable<?>[] typeVariableArr) {
        adj adj2;
        if (typeVariableArr == null || typeVariableArr.length <= 0) {
            adj2 = adj;
        } else {
            adj2 = adj.a();
            for (TypeVariable<?> typeVariable : typeVariableArr) {
                adj2.b(typeVariable.getName());
                Type type = typeVariable.getBounds()[0];
                adj2.a(typeVariable.getName(), type == null ? adk.b() : adj2.a(type));
            }
        }
        return adj2.a(c());
    }

    public final <A extends Annotation> A a(Class cls) {
        return this.b.a(cls);
    }

    public final xn c(int i) {
        return new xn(this, b(i), this.d[i], i);
    }
}
