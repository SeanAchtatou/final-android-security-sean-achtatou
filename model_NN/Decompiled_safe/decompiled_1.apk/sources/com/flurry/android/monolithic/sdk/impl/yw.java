package com.flurry.android.monolithic.sdk.impl;

import com.flurry.org.codehaus.jackson.annotate.JsonTypeInfo;
import java.util.Collection;

public class yw implements yj<yw> {
    protected JsonTypeInfo.Id a;
    protected JsonTypeInfo.As b;
    protected String c;
    protected Class<?> d;
    protected yi e;

    public Class<?> a() {
        return this.d;
    }

    public static yw b() {
        return new yw().a(JsonTypeInfo.Id.NONE, null);
    }

    /* renamed from: b */
    public yw a(JsonTypeInfo.Id id, yi yiVar) {
        if (id == null) {
            throw new IllegalArgumentException("idType can not be null");
        }
        this.a = id;
        this.e = yiVar;
        this.c = id.getDefaultPropertyName();
        return this;
    }

    public rx a(rq rqVar, afm afm, Collection<yg> collection, qc qcVar) {
        if (this.a == JsonTypeInfo.Id.NONE) {
            return null;
        }
        yi a2 = a(rqVar, afm, collection, true, false);
        switch (yx.a[this.b.ordinal()]) {
            case 1:
                return new yl(a2, qcVar);
            case 2:
                return new yq(a2, qcVar, this.c);
            case 3:
                return new ys(a2, qcVar);
            case 4:
                return new yn(a2, qcVar, this.c);
            default:
                throw new IllegalStateException("Do not know how to construct standard type serializer for inclusion type: " + this.b);
        }
    }

    public rw a(qk qkVar, afm afm, Collection<yg> collection, qc qcVar) {
        if (this.a == JsonTypeInfo.Id.NONE) {
            return null;
        }
        yi a2 = a(qkVar, afm, collection, false, true);
        switch (yx.a[this.b.ordinal()]) {
            case 1:
                return new yk(afm, a2, qcVar, this.d);
            case 2:
                return new yo(afm, a2, qcVar, this.d, this.c);
            case 3:
                return new yr(afm, a2, qcVar, this.d);
            case 4:
                return new ym(afm, a2, qcVar, this.d, this.c);
            default:
                throw new IllegalStateException("Do not know how to construct standard type serializer for inclusion type: " + this.b);
        }
    }

    /* renamed from: b */
    public yw a(JsonTypeInfo.As as) {
        if (as == null) {
            throw new IllegalArgumentException("includeAs can not be null");
        }
        this.b = as;
        return this;
    }

    /* renamed from: b */
    public yw a(String str) {
        String str2;
        if (str == null || str.length() == 0) {
            str2 = this.a.getDefaultPropertyName();
        } else {
            str2 = str;
        }
        this.c = str2;
        return this;
    }

    /* renamed from: b */
    public yw a(Class<?> cls) {
        this.d = cls;
        return this;
    }

    /* access modifiers changed from: protected */
    public yi a(rf<?> rfVar, afm afm, Collection<yg> collection, boolean z, boolean z2) {
        if (this.e != null) {
            return this.e;
        }
        if (this.a == null) {
            throw new IllegalStateException("Can not build, 'init()' not yet called");
        }
        switch (yx.b[this.a.ordinal()]) {
            case 1:
                return new yt(afm, rfVar.m());
            case 2:
                return new yu(afm, rfVar.m());
            case 3:
                return za.a(rfVar, afm, collection, z, z2);
            case 4:
                return null;
            default:
                throw new IllegalStateException("Do not know how to construct standard type id resolver for idType: " + this.a);
        }
    }
}
