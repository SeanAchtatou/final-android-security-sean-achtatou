package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
final class vm extends ve<short[]> {
    public vm() {
        super(short[].class);
    }

    /* renamed from: b */
    public short[] a(ow owVar, qm qmVar) throws IOException, oz {
        if (!owVar.j()) {
            return c(owVar, qmVar);
        }
        adx c = qmVar.h().c();
        short[] sArr = (short[]) c.a();
        int i = 0;
        while (owVar.b() != pb.END_ARRAY) {
            short s = s(owVar, qmVar);
            if (i >= sArr.length) {
                sArr = (short[]) c.a(sArr, i);
                i = 0;
            }
            sArr[i] = s;
            i++;
        }
        return (short[]) c.b(sArr, i);
    }

    private final short[] c(ow owVar, qm qmVar) throws IOException, oz {
        if (owVar.e() == pb.VALUE_STRING && qmVar.a(ql.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT) && owVar.k().length() == 0) {
            return null;
        }
        if (!qmVar.a(ql.ACCEPT_SINGLE_VALUE_AS_ARRAY)) {
            throw qmVar.b(this.q);
        }
        return new short[]{s(owVar, qmVar)};
    }
}
