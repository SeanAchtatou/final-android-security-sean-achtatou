package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
public final class aab extends abq<Float> {
    static final aab a = new aab();

    public aab() {
        super(Float.class);
    }

    public void a(Float f, or orVar, ru ruVar) throws IOException, oq {
        orVar.a(f.floatValue());
    }
}
