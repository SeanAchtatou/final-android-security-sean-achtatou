package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public abstract class wv<T> extends vo<T> {
    protected wv(Class<?> cls) {
        super(cls);
    }

    public Object a(ow owVar, qm qmVar, rw rwVar) throws IOException, oz {
        return rwVar.c(owVar, qmVar);
    }
}
