package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.lang.reflect.Method;

public final class sv {
    protected final qc a;
    protected final Method b;
    protected final afm c;
    protected qu<Object> d;

    public sv(qc qcVar, xl xlVar, afm afm, qu<Object> quVar) {
        this(qcVar, xlVar.a(), afm, quVar);
    }

    public sv(qc qcVar, Method method, afm afm, qu<Object> quVar) {
        this.a = qcVar;
        this.c = afm;
        this.b = method;
        this.d = quVar;
    }

    public sv a(qu<Object> quVar) {
        return new sv(this.a, this.b, this.c, quVar);
    }

    public qc a() {
        return this.a;
    }

    public boolean b() {
        return this.d != null;
    }

    public afm c() {
        return this.c;
    }

    public final void a(ow owVar, qm qmVar, Object obj, String str) throws IOException, oz {
        a(obj, str, a(owVar, qmVar));
    }

    public final Object a(ow owVar, qm qmVar) throws IOException, oz {
        if (owVar.e() == pb.VALUE_NULL) {
            return null;
        }
        return this.d.a(owVar, qmVar);
    }

    public final void a(Object obj, String str, Object obj2) throws IOException {
        try {
            this.b.invoke(obj, str, obj2);
        } catch (Exception e) {
            a(e, str, obj2);
        }
    }

    /* access modifiers changed from: protected */
    public void a(Exception exc, String str, Object obj) throws IOException {
        if (exc instanceof IllegalArgumentException) {
            String name = obj == null ? "[NULL]" : obj.getClass().getName();
            StringBuilder append = new StringBuilder("Problem deserializing \"any\" property '").append(str);
            append.append("' of class " + d() + " (expected type: ").append(this.c);
            append.append("; actual type: ").append(name).append(")");
            String message = exc.getMessage();
            if (message != null) {
                append.append(", problem: ").append(message);
            } else {
                append.append(" (no error message provided)");
            }
            throw new qw(append.toString(), null, exc);
        } else if (exc instanceof IOException) {
            throw ((IOException) exc);
        } else if (exc instanceof RuntimeException) {
            throw ((RuntimeException) exc);
        } else {
            Throwable th = exc;
            while (th.getCause() != null) {
                th = th.getCause();
            }
            throw new qw(th.getMessage(), null, th);
        }
    }

    private String d() {
        return this.b.getDeclaringClass().getName();
    }

    public String toString() {
        return "[any property on class " + d() + "]";
    }
}
