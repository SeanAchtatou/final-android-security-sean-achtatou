package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public class aav extends abt<Object> {
    public aav() {
        super(Object.class);
    }

    public void a(Object obj, or orVar, ru ruVar) throws IOException, qw {
        if (ruVar.a(rr.FAIL_ON_EMPTY_BEANS)) {
            a(obj);
        }
        orVar.d();
        orVar.e();
    }

    public final void a(Object obj, or orVar, ru ruVar, rx rxVar) throws IOException, oq {
        if (ruVar.a(rr.FAIL_ON_EMPTY_BEANS)) {
            a(obj);
        }
        rxVar.b(obj, orVar);
        rxVar.e(obj, orVar);
    }

    /* access modifiers changed from: protected */
    public void a(Object obj) throws qw {
        throw new qw("No serializer found for class " + obj.getClass().getName() + " and no properties discovered to create BeanSerializer (to avoid exception, disable SerializationConfig.Feature.FAIL_ON_EMPTY_BEANS) )");
    }
}
