package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

abstract class kb extends ji {
    final ka f;
    final String g;
    Set<ka> h;

    public kb(kj kjVar, ka kaVar, String str) {
        super(kjVar);
        this.f = kaVar;
        this.g = str;
        if (e.containsKey(kaVar.c)) {
            throw new jh("Schemas may not be named after primitives: " + kaVar.c);
        }
    }

    public String d() {
        return this.f.a;
    }

    public String e() {
        return this.g;
    }

    public String f() {
        return this.f.b;
    }

    public String g() {
        return this.f.c;
    }

    public void d(String str) {
        if (this.h == null) {
            this.h = new LinkedHashSet();
        }
        this.h.add(new ka(str, this.f.b));
    }

    public boolean c(kc kcVar, or orVar) throws IOException {
        if (equals(kcVar.get(this.f))) {
            orVar.b(this.f.a(kcVar.a()));
            return true;
        }
        if (this.f.a != null) {
            kcVar.put(this.f, this);
        }
        return false;
    }

    public void d(kc kcVar, or orVar) throws IOException {
        this.f.a(kcVar, orVar);
    }

    public boolean a(kb kbVar) {
        return this.f.equals(kbVar.f);
    }

    /* access modifiers changed from: package-private */
    public int m() {
        return super.m() + this.f.hashCode();
    }

    public void a(or orVar) throws IOException {
        if (this.h != null && this.h.size() != 0) {
            orVar.a("aliases");
            orVar.b();
            for (ka a : this.h) {
                orVar.b(a.a(this.f.b));
            }
            orVar.c();
        }
    }
}
