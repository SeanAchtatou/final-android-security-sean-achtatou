package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

final class va extends ub<afd> {
    protected static final va a = new va();

    protected va() {
        super(afd.class);
    }

    public static va d() {
        return a;
    }

    /* renamed from: b */
    public afd a(ow owVar, qm qmVar) throws IOException, oz {
        if (owVar.e() == pb.START_OBJECT) {
            owVar.b();
            return a(owVar, qmVar, qmVar.e());
        } else if (owVar.e() == pb.FIELD_NAME) {
            return a(owVar, qmVar, qmVar.e());
        } else {
            throw qmVar.b(afd.class);
        }
    }
}
