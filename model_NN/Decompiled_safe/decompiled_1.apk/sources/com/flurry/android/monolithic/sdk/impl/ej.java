package com.flurry.android.monolithic.sdk.impl;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public final class ej {
    private int a;
    private long b;
    private String c;
    private String d;
    private String e;
    private Throwable f;

    public ej(int i, long j, String str, String str2, String str3, Throwable th) {
        this.a = i;
        this.b = j;
        this.c = str;
        this.d = str2;
        this.e = str3;
        this.f = th;
    }

    public int a() {
        return b().length;
    }

    public byte[] b() {
        DataOutputStream dataOutputStream;
        Throwable th;
        DataOutputStream dataOutputStream2 = null;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            DataOutputStream dataOutputStream3 = new DataOutputStream(byteArrayOutputStream);
            try {
                dataOutputStream3.writeShort(this.a);
                dataOutputStream3.writeLong(this.b);
                dataOutputStream3.writeUTF(this.c);
                dataOutputStream3.writeUTF(this.d);
                dataOutputStream3.writeUTF(this.e);
                if (this.f != null) {
                    if (this.c == "uncaught") {
                        dataOutputStream3.writeByte(3);
                    } else {
                        dataOutputStream3.writeByte(2);
                    }
                    dataOutputStream3.writeByte(2);
                    StringBuilder sb = new StringBuilder("");
                    String property = System.getProperty("line.separator");
                    for (StackTraceElement append : this.f.getStackTrace()) {
                        sb.append(append);
                        sb.append(property);
                    }
                    if (this.f.getCause() != null) {
                        sb.append(property);
                        sb.append("Caused by: ");
                        for (StackTraceElement append2 : this.f.getCause().getStackTrace()) {
                            sb.append(append2);
                            sb.append(property);
                        }
                    }
                    byte[] bytes = sb.toString().getBytes();
                    dataOutputStream3.writeInt(bytes.length);
                    dataOutputStream3.write(bytes);
                } else {
                    dataOutputStream3.writeByte(1);
                    dataOutputStream3.writeByte(0);
                }
                dataOutputStream3.flush();
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                je.a(dataOutputStream3);
                return byteArray;
            } catch (IOException e2) {
                dataOutputStream2 = dataOutputStream3;
            } catch (Throwable th2) {
                th = th2;
                dataOutputStream = dataOutputStream3;
                je.a(dataOutputStream);
                throw th;
            }
        } catch (IOException e3) {
            try {
                byte[] bArr = new byte[0];
                je.a(dataOutputStream2);
                return bArr;
            } catch (Throwable th3) {
                Throwable th4 = th3;
                dataOutputStream = dataOutputStream2;
                th = th4;
                je.a(dataOutputStream);
                throw th;
            }
        } catch (Throwable th5) {
            Throwable th6 = th5;
            dataOutputStream = null;
            th = th6;
            je.a(dataOutputStream);
            throw th;
        }
    }

    public String c() {
        return this.c;
    }
}
