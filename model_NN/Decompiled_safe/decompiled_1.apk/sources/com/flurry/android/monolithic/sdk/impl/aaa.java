package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
public final class aaa extends abm<Double> {
    static final aaa a = new aaa();

    public aaa() {
        super(Double.class);
    }

    public void a(Double d, or orVar, ru ruVar) throws IOException, oq {
        orVar.a(d.doubleValue());
    }
}
