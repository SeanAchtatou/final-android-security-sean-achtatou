package com.flurry.android.monolithic.sdk.impl;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.UUID;

public class uv extends um<UUID> {
    public uv() {
        super(UUID.class);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public UUID a(String str, qm qmVar) throws IOException, oz {
        return UUID.fromString(str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public UUID a(Object obj, qm qmVar) throws IOException, oz {
        if (obj instanceof byte[]) {
            byte[] bArr = (byte[]) obj;
            if (bArr.length != 16) {
                qmVar.b("Can only construct UUIDs from 16 byte arrays; got " + bArr.length + " bytes");
            }
            DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(bArr));
            return new UUID(dataInputStream.readLong(), dataInputStream.readLong());
        }
        super.a(obj, qmVar);
        return null;
    }
}
