package com.flurry.android.monolithic.sdk.impl;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public abstract class ou implements Iterable<ou> {
    protected static final List<ou> a = Collections.emptyList();
    protected static final List<String> b = Collections.emptyList();

    public abstract boolean equals(Object obj);

    public abstract String m();

    public abstract String toString();

    protected ou() {
    }

    public boolean a() {
        return false;
    }

    public boolean b() {
        return false;
    }

    public boolean c() {
        return false;
    }

    public boolean d() {
        return false;
    }

    public boolean e() {
        return false;
    }

    public boolean f() {
        return false;
    }

    public boolean g() {
        return false;
    }

    public String h() {
        return null;
    }

    public boolean i() {
        return false;
    }

    public int j() {
        return 0;
    }

    public long k() {
        return 0;
    }

    public double l() {
        return 0.0d;
    }

    public ou a(String str) {
        return null;
    }

    public double a(double d) {
        return d;
    }

    @Deprecated
    public double n() {
        return a(0.0d);
    }

    public int o() {
        return 0;
    }

    public final Iterator<ou> iterator() {
        return p();
    }

    public Iterator<ou> p() {
        return a.iterator();
    }

    public Iterator<String> q() {
        return b.iterator();
    }
}
