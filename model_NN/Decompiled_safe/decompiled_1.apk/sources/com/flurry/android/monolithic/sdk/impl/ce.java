package com.flurry.android.monolithic.sdk.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public final class ce {
    private static final String a = ce.class.getSimpleName();
    private static ce b;
    private HashMap<String, cd> c = new HashMap<>();

    ce() {
    }

    public static ce a() {
        if (b == null) {
            b = new ce();
        }
        return b;
    }

    public HashMap<String, cd> b() {
        return this.c;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.flurry.android.monolithic.sdk.impl.cd r3) {
        /*
            r2 = this;
            java.lang.String r0 = r3.b()     // Catch:{ Exception -> 0x0036 }
            int r0 = r0.length()     // Catch:{ Exception -> 0x0036 }
            if (r0 != 0) goto L_0x000b
        L_0x000a:
            return
        L_0x000b:
            monitor-enter(r2)     // Catch:{ Exception -> 0x0036 }
            java.util.HashMap<java.lang.String, com.flurry.android.monolithic.sdk.impl.cd> r0 = r2.c     // Catch:{ all -> 0x0033 }
            java.lang.String r1 = r3.b()     // Catch:{ all -> 0x0033 }
            boolean r0 = r0.containsKey(r1)     // Catch:{ all -> 0x0033 }
            if (r0 == 0) goto L_0x003b
            java.util.HashMap<java.lang.String, com.flurry.android.monolithic.sdk.impl.cd> r0 = r2.c     // Catch:{ all -> 0x0033 }
            java.lang.String r1 = r3.b()     // Catch:{ all -> 0x0033 }
            r0.remove(r1)     // Catch:{ all -> 0x0033 }
            int r0 = r3.e()     // Catch:{ all -> 0x0033 }
            r1 = -1
            if (r0 == r1) goto L_0x0031
            java.util.HashMap<java.lang.String, com.flurry.android.monolithic.sdk.impl.cd> r0 = r2.c     // Catch:{ all -> 0x0033 }
            java.lang.String r1 = r3.b()     // Catch:{ all -> 0x0033 }
            r0.put(r1, r3)     // Catch:{ all -> 0x0033 }
        L_0x0031:
            monitor-exit(r2)     // Catch:{ all -> 0x0033 }
            goto L_0x000a
        L_0x0033:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0033 }
            throw r0     // Catch:{ Exception -> 0x0036 }
        L_0x0036:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x000a
        L_0x003b:
            java.util.HashMap<java.lang.String, com.flurry.android.monolithic.sdk.impl.cd> r0 = r2.c     // Catch:{ all -> 0x0033 }
            java.lang.String r1 = r3.b()     // Catch:{ all -> 0x0033 }
            r0.put(r1, r3)     // Catch:{ all -> 0x0033 }
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.ce.a(com.flurry.android.monolithic.sdk.impl.cd):void");
    }

    public cd a(String str) {
        try {
            synchronized (this) {
                if (!this.c.containsKey(str)) {
                    return null;
                }
                cd cdVar = this.c.get(str);
                return cdVar;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<cd> c() {
        ArrayList arrayList;
        try {
            synchronized (this) {
                arrayList = new ArrayList();
                for (cd a2 : b().values()) {
                    arrayList.add(a2.a());
                }
            }
            return arrayList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void b(String str) {
        try {
            synchronized (this) {
                if (this.c.containsKey(str)) {
                    this.c.remove(str);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void d() {
        try {
            synchronized (this) {
                for (cd next : c()) {
                    if (a(next.h())) {
                        ja.a(3, a, "expiring adunit freq cap for idHash: " + next.b() + " adunit exp: " + next.h() + " device epoch" + System.currentTimeMillis());
                        b(next.b());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean a(long j) {
        if (j <= System.currentTimeMillis()) {
            return true;
        }
        return false;
    }
}
