package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public class yl extends zb {
    public yl(yi yiVar, qc qcVar) {
        super(yiVar, qcVar);
    }

    public void b(Object obj, or orVar) throws IOException, oz {
        orVar.b();
        orVar.b(this.b.a(obj));
        orVar.d();
    }

    public void c(Object obj, or orVar) throws IOException, oz {
        orVar.b();
        orVar.b(this.b.a(obj));
        orVar.b();
    }

    public void a(Object obj, or orVar) throws IOException, oz {
        orVar.b();
        orVar.b(this.b.a(obj));
    }

    public void a(Object obj, or orVar, Class<?> cls) throws IOException, oz {
        orVar.b();
        orVar.b(this.b.a(obj, cls));
    }

    public void e(Object obj, or orVar) throws IOException, oz {
        orVar.e();
        orVar.c();
    }

    public void f(Object obj, or orVar) throws IOException, oz {
        orVar.c();
        orVar.c();
    }

    public void d(Object obj, or orVar) throws IOException, oz {
        orVar.c();
    }
}
