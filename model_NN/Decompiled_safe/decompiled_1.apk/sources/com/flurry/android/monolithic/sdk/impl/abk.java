package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

@rz
public class abk extends abt<Object> implements rp {
    protected final Method a;
    protected ra<Object> b;
    protected final qc c;
    protected boolean d;

    public abk(Method method, ra<Object> raVar, qc qcVar) {
        super(Object.class);
        this.a = method;
        this.b = raVar;
        this.c = qcVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.ru.a(java.lang.Class<?>, boolean, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object>
     arg types: [java.lang.Class<?>, int, com.flurry.android.monolithic.sdk.impl.qc]
     candidates:
      com.flurry.android.monolithic.sdk.impl.ru.a(com.flurry.android.monolithic.sdk.impl.afm, boolean, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object>
      com.flurry.android.monolithic.sdk.impl.ru.a(java.lang.Class<?>, boolean, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object> */
    public void a(Object obj, or orVar, ru ruVar) throws IOException, oq {
        try {
            Object invoke = this.a.invoke(obj, new Object[0]);
            if (invoke == null) {
                ruVar.a(orVar);
                return;
            }
            ra<Object> raVar = this.b;
            if (raVar == null) {
                raVar = ruVar.a(invoke.getClass(), true, this.c);
            }
            raVar.a(invoke, orVar, ruVar);
        } catch (IOException e) {
            throw e;
        } catch (Exception e2) {
            e = e2;
            while ((e instanceof InvocationTargetException) && e.getCause() != null) {
                e = e.getCause();
            }
            if (e instanceof Error) {
                throw ((Error) e);
            }
            throw qw.a(e, obj, this.a.getName() + "()");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.ru.a(java.lang.Class<?>, boolean, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object>
     arg types: [java.lang.Class<?>, int, com.flurry.android.monolithic.sdk.impl.qc]
     candidates:
      com.flurry.android.monolithic.sdk.impl.ru.a(com.flurry.android.monolithic.sdk.impl.afm, boolean, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object>
      com.flurry.android.monolithic.sdk.impl.ru.a(java.lang.Class<?>, boolean, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object> */
    public void a(Object obj, or orVar, ru ruVar, rx rxVar) throws IOException, oz {
        try {
            Object invoke = this.a.invoke(obj, new Object[0]);
            if (invoke == null) {
                ruVar.a(orVar);
                return;
            }
            ra<Object> raVar = this.b;
            if (raVar != null) {
                if (this.d) {
                    rxVar.a(obj, orVar);
                }
                raVar.a(invoke, orVar, ruVar, rxVar);
                if (this.d) {
                    rxVar.d(obj, orVar);
                    return;
                }
                return;
            }
            ruVar.a(invoke.getClass(), true, this.c).a(invoke, orVar, ruVar);
        } catch (IOException e) {
            throw e;
        } catch (Exception e2) {
            e = e2;
            while ((e instanceof InvocationTargetException) && e.getCause() != null) {
                e = e.getCause();
            }
            if (e instanceof Error) {
                throw ((Error) e);
            }
            throw qw.a(e, obj, this.a.getName() + "()");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.ru.a(com.flurry.android.monolithic.sdk.impl.afm, boolean, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object>
     arg types: [com.flurry.android.monolithic.sdk.impl.afm, int, com.flurry.android.monolithic.sdk.impl.qc]
     candidates:
      com.flurry.android.monolithic.sdk.impl.ru.a(java.lang.Class<?>, boolean, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object>
      com.flurry.android.monolithic.sdk.impl.ru.a(com.flurry.android.monolithic.sdk.impl.afm, boolean, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object> */
    public void a(ru ruVar) throws qw {
        if (this.b != null) {
            return;
        }
        if (ruVar.a(rr.USE_STATIC_TYPING) || Modifier.isFinal(this.a.getReturnType().getModifiers())) {
            afm a2 = ruVar.a(this.a.getGenericReturnType());
            this.b = ruVar.a(a2, false, this.c);
            this.d = a(a2, this.b);
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(afm afm, ra<?> raVar) {
        Class<?> p = afm.p();
        if (afm.t()) {
            if (!(p == Integer.TYPE || p == Boolean.TYPE || p == Double.TYPE)) {
                return false;
            }
        } else if (!(p == String.class || p == Integer.class || p == Boolean.class || p == Double.class)) {
            return false;
        }
        return raVar.getClass().getAnnotation(rz.class) != null;
    }

    public String toString() {
        return "(@JsonValue serializer for method " + this.a.getDeclaringClass() + "#" + this.a.getName() + ")";
    }
}
