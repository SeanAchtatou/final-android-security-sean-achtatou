package com.flurry.android.monolithic.sdk.impl;

import java.util.LinkedHashMap;

class hv {
    public static LinkedHashMap<Integer, Long> a(int i, long j) {
        long j2;
        long j3;
        int i2 = 1;
        if (i >= 1) {
            i2 = i;
        }
        if (j < 0) {
            j2 = 0;
        } else {
            j2 = j;
        }
        LinkedHashMap<Integer, Long> linkedHashMap = new LinkedHashMap<>();
        for (int i3 = 0; i3 < i2; i3++) {
            if (i3 == 0) {
                j3 = 0;
            } else {
                j3 = j2;
            }
            linkedHashMap.put(Integer.valueOf(i3), Long.valueOf(j3));
        }
        return linkedHashMap;
    }
}
