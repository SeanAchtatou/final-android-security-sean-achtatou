package com.flurry.android.monolithic.sdk.impl;

import com.flurry.android.Constants;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;

class lq extends lp {
    protected boolean b;
    private InputStream c;

    private lq(InputStream inputStream) {
        this.b = false;
        this.c = inputStream;
    }

    /* access modifiers changed from: protected */
    public void a(long j) throws IOException {
        boolean z = false;
        long j2 = j;
        while (j2 > 0) {
            long skip = this.c.skip(j2);
            if (skip > 0) {
                j2 -= skip;
            } else if (skip != 0) {
                this.b = true;
                throw new EOFException();
            } else if (z) {
                this.b = true;
                throw new EOFException();
            } else {
                z = true;
            }
        }
    }

    /* access modifiers changed from: protected */
    public long b(long j) throws IOException {
        boolean z = false;
        long j2 = j;
        while (true) {
            if (j2 <= 0) {
                break;
            }
            try {
                long skip = this.c.skip(j);
                if (skip <= 0) {
                    if (skip != 0) {
                        this.b = true;
                        break;
                    } else if (z) {
                        this.b = true;
                        break;
                    } else {
                        z = true;
                    }
                } else {
                    j2 -= skip;
                }
            } catch (EOFException e) {
                this.b = true;
            }
        }
        return j - j2;
    }

    /* access modifiers changed from: protected */
    public void a(byte[] bArr, int i, int i2) throws IOException {
        int i3 = i2;
        int i4 = i;
        while (i3 > 0) {
            int read = this.c.read(bArr, i4, i3);
            if (read < 0) {
                this.b = true;
                throw new EOFException();
            } else {
                i3 -= read;
                i4 += read;
            }
        }
    }

    /* access modifiers changed from: protected */
    public int b(byte[] bArr, int i, int i2) throws IOException {
        int i3 = i2;
        int i4 = i;
        while (true) {
            if (i3 <= 0) {
                break;
            }
            try {
                int read = this.c.read(bArr, i4, i3);
                if (read < 0) {
                    this.b = true;
                    break;
                }
                i3 -= read;
                i4 += read;
            } catch (EOFException e) {
                this.b = true;
            }
        }
        return i2 - i3;
    }

    public int read() throws IOException {
        if (this.a.c() - this.a.b() == 0) {
            return this.c.read();
        }
        int b2 = this.a.b();
        byte b3 = this.a.d()[b2] & Constants.UNKNOWN;
        this.a.a(b2 + 1);
        return b3;
    }

    public void close() throws IOException {
        this.c.close();
    }
}
