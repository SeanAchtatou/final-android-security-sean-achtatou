package com.flurry.android.monolithic.sdk.impl;

import com.ideaworks3d.marmalade.s3eAndroidGooglePlayBilling.util.IabHelper;
import java.util.List;
import java.util.Map;

public class nk {
    public mq a(ji jiVar, Map<nm, mq> map) {
        switch (nl.a[jiVar.a().ordinal()]) {
            case 1:
                return mq.c;
            case 2:
                return mq.d;
            case 3:
                return mq.e;
            case 4:
                return mq.f;
            case 5:
                return mq.g;
            case 6:
                return mq.h;
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED /*7*/:
                return mq.i;
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED /*8*/:
                return mq.j;
            case 9:
                return mq.b(new mz(jiVar.l()), mq.k);
            case 10:
                return mq.b(new mz(jiVar.c().size()), mq.l);
            case 11:
                return mq.b(mq.a(mq.o, a(jiVar.i(), map)), mq.n);
            case 12:
                return mq.b(mq.a(mq.q, a(jiVar.j(), map), mq.i), mq.p);
            case 13:
                nm nmVar = new nm(jiVar);
                mq mqVar = map.get(nmVar);
                if (mqVar != null) {
                    return mqVar;
                }
                mq[] mqVarArr = new mq[jiVar.b().size()];
                mq b = mq.b(mqVarArr);
                map.put(nmVar, b);
                int length = mqVarArr.length;
                int i = length;
                for (js c : jiVar.b()) {
                    i--;
                    mqVarArr[i] = a(c.c(), map);
                }
                return b;
            case 14:
                List<ji> k = jiVar.k();
                mq[] mqVarArr2 = new mq[k.size()];
                String[] strArr = new String[k.size()];
                int i2 = 0;
                for (ji next : jiVar.k()) {
                    mqVarArr2[i2] = a(next, map);
                    strArr[i2] = next.g();
                    i2++;
                }
                return mq.b(mq.a(mqVarArr2, strArr), mq.m);
            default:
                throw new RuntimeException("Unexpected schema type");
        }
    }
}
