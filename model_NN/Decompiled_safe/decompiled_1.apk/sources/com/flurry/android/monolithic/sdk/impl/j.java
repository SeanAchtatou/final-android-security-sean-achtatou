package com.flurry.android.monolithic.sdk.impl;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ComponentName;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.SparseArray;
import android.util.SparseIntArray;

public final class j {
    private static final String a = j.class.getSimpleName();
    private static final SparseArray<SparseIntArray> b = c();

    public static void a(Activity activity, int i) {
        if (activity != null) {
            activity.setRequestedOrientation(i);
        }
    }

    @TargetApi(9)
    public static int a() {
        if (Build.VERSION.SDK_INT >= 9) {
            return 7;
        }
        return 1;
    }

    @TargetApi(9)
    public static int b() {
        if (Build.VERSION.SDK_INT >= 9) {
            return 6;
        }
        return 0;
    }

    public static boolean a(Activity activity, int i, boolean z) {
        int i2;
        boolean z2;
        if (activity == null) {
            return false;
        }
        if (!b(activity)) {
            i2 = b(activity, i);
            if (-1 == i2) {
                ja.a(5, a, "cannot set requested orientation without restarting activity, requestedOrientation = " + i);
                ja.b(a, "cannot set requested orientation without restarting activity. It is recommended to add keyboardHidden|orientation|screenSize for android:configChanges attribute for activity: " + activity.getComponentName().getClassName());
                return false;
            }
            z2 = true;
        } else {
            i2 = i;
            z2 = z;
        }
        activity.setRequestedOrientation(i2);
        if (!z2) {
            activity.setRequestedOrientation(-1);
        }
        return true;
    }

    public static ActivityInfo a(PackageManager packageManager, ComponentName componentName) {
        if (packageManager == null || componentName == null) {
            return null;
        }
        try {
            return packageManager.getActivityInfo(componentName, 0);
        } catch (PackageManager.NameNotFoundException e) {
            ja.a(5, a, "cannot find info for activity: " + componentName);
            return null;
        }
    }

    public static ActivityInfo a(Activity activity) {
        if (activity == null) {
            return null;
        }
        return a(activity.getPackageManager(), activity.getComponentName());
    }

    public static boolean b(Activity activity) {
        int a2 = a(a(activity));
        if ((a2 & 128) == 0) {
            return false;
        }
        if ((a2 & 1024) == 0) {
            return false;
        }
        return true;
    }

    @TargetApi(13)
    public static int a(ActivityInfo activityInfo) {
        if (activityInfo == null) {
            return 0;
        }
        int i = activityInfo.configChanges;
        if (activityInfo.applicationInfo.targetSdkVersion < 13) {
            return i | 3072;
        }
        return i;
    }

    public static int b(Activity activity, int i) {
        SparseIntArray sparseIntArray;
        if (activity == null || (sparseIntArray = b.get(activity.getResources().getConfiguration().orientation)) == null) {
            return -1;
        }
        return sparseIntArray.get(i, -1);
    }

    private static SparseArray<SparseIntArray> c() {
        SparseArray<SparseIntArray> sparseArray = new SparseArray<>();
        sparseArray.put(1, d());
        sparseArray.put(2, e());
        return sparseArray;
    }

    @TargetApi(9)
    private static SparseIntArray d() {
        int a2 = a();
        SparseIntArray sparseIntArray = new SparseIntArray();
        sparseIntArray.put(-1, a2);
        sparseIntArray.put(2, a2);
        sparseIntArray.put(3, a2);
        sparseIntArray.put(4, a2);
        sparseIntArray.put(1, 1);
        sparseIntArray.put(5, 5);
        sparseIntArray.put(7, 7);
        sparseIntArray.put(9, 9);
        sparseIntArray.put(10, 7);
        return sparseIntArray;
    }

    @TargetApi(9)
    private static SparseIntArray e() {
        int b2 = b();
        SparseIntArray sparseIntArray = new SparseIntArray();
        sparseIntArray.put(-1, b2);
        sparseIntArray.put(2, b2);
        sparseIntArray.put(3, b2);
        sparseIntArray.put(4, b2);
        sparseIntArray.put(0, 0);
        sparseIntArray.put(5, 5);
        sparseIntArray.put(6, 6);
        sparseIntArray.put(8, 8);
        sparseIntArray.put(10, 6);
        return sparseIntArray;
    }
}
