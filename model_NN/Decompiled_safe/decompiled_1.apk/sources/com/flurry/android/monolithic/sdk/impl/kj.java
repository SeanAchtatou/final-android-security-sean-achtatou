package com.flurry.android.monolithic.sdk.impl;

public enum kj {
    RECORD,
    ENUM,
    ARRAY,
    MAP,
    UNION,
    FIXED,
    STRING,
    BYTES,
    INT,
    LONG,
    FLOAT,
    DOUBLE,
    BOOLEAN,
    NULL;
    
    private String o = name().toLowerCase();

    public String a() {
        return this.o;
    }
}
