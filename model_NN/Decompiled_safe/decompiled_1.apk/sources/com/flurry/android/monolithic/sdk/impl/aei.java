package com.flurry.android.monolithic.sdk.impl;

final class aei {
    final Object[] a;
    aei b;

    public aei(Object[] objArr) {
        this.a = objArr;
    }

    public Object[] a() {
        return this.a;
    }

    public aei b() {
        return this.b;
    }

    public void a(aei aei) {
        if (this.b != null) {
            throw new IllegalStateException();
        }
        this.b = aei;
    }
}
