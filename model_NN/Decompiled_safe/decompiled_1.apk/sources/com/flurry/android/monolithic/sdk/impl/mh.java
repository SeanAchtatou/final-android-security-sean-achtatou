package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public class mh extends mi {
    static final /* synthetic */ boolean b = (!mh.class.desiredAssertionStatus());
    private lx d;

    mh(ji jiVar, ji jiVar2, lx lxVar) throws IOException {
        this(a(jiVar, jiVar2), lxVar);
    }

    private mh(Object obj, lx lxVar) throws IOException {
        super((mq) obj, lxVar);
    }

    public static Object a(ji jiVar, ji jiVar2) throws IOException {
        if (jiVar == null) {
            throw new NullPointerException("writer cannot be null!");
        } else if (jiVar2 != null) {
            return new ml().a(jiVar, jiVar2);
        } else {
            throw new NullPointerException("reader cannot be null!");
        }
    }

    public final js[] u() throws IOException {
        return ((mw) this.a.a(mq.s)).z;
    }

    public final void v() throws IOException {
        this.a.a();
    }

    public long e() throws IOException {
        mq a = this.a.a(mq.f);
        if (a == mq.e) {
            return (long) this.c.d();
        }
        if (a == mq.h) {
            return (long) this.c.g();
        }
        if (b || a == mq.f) {
            return this.c.e();
        }
        throw new AssertionError();
    }

    public float f() throws IOException {
        mq a = this.a.a(mq.g);
        if (a == mq.e) {
            return (float) this.c.d();
        }
        if (a == mq.f) {
            return (float) this.c.e();
        }
        if (b || a == mq.g) {
            return this.c.f();
        }
        throw new AssertionError();
    }

    public double g() throws IOException {
        mq a = this.a.a(mq.h);
        if (a == mq.e) {
            return (double) this.c.d();
        }
        if (a == mq.f) {
            return (double) this.c.e();
        }
        if (a == mq.g) {
            return (double) this.c.f();
        }
        if (b || a == mq.h) {
            return this.c.g();
        }
        throw new AssertionError();
    }

    public int k() throws IOException {
        this.a.a(mq.l);
        Object obj = ((mu) this.a.c()).z[this.c.k()];
        if (obj instanceof Integer) {
            return ((Integer) obj).intValue();
        }
        throw new jh((String) obj);
    }

    public int s() throws IOException {
        this.a.a(mq.m);
        ni niVar = (ni) this.a.c();
        this.a.c(niVar.B);
        return niVar.z;
    }

    public mq a(mq mqVar, mq mqVar2) throws IOException {
        if (mqVar2 instanceof mw) {
            if (mqVar == mq.s) {
                return mqVar2;
            }
            return null;
        } else if (mqVar2 instanceof nc) {
            nc ncVar = (nc) mqVar2;
            if (ncVar.B == mqVar) {
                return ncVar.z;
            }
            throw new jh("Found " + ncVar.B + " while looking for " + mqVar);
        } else {
            if (mqVar2 instanceof ng) {
                this.a.d(((ng) mqVar2).z);
            } else if (mqVar2 instanceof nj) {
                this.a.c(((ms) this.a.c()).a(this.c.s()));
            } else if (mqVar2 instanceof mv) {
                throw new jh(((mv) mqVar2).z);
            } else if (mqVar2 instanceof mt) {
                this.d = this.c;
                this.c = ly.a().a(((mt) mqVar2).z, (ll) null);
            } else if (mqVar2 == mq.x) {
                this.c = this.d;
            } else {
                throw new jh("Unknown action: " + mqVar2);
            }
            return null;
        }
    }

    public void l() throws IOException {
        mq c = this.a.c();
        if (c instanceof nc) {
            this.a.c(((nc) c).z);
        } else if (c instanceof ng) {
            this.a.c(((ng) c).z);
        } else if (c instanceof nj) {
            this.a.c(((ms) this.a.c()).a(this.c.s()));
        } else if (c instanceof mv) {
            throw new jh(((mv) c).z);
        } else if (c instanceof mt) {
            this.d = this.c;
            this.c = ly.a().a(((mt) c).z, (ll) null);
        } else if (c == mq.x) {
            this.c = this.d;
        }
    }
}
