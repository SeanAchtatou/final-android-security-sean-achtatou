package com.flurry.android.monolithic.sdk.impl;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Type;

public final class xj extends xk {
    protected final Field a;

    public xj(Field field, xp xpVar) {
        super(xpVar);
        this.a = field;
    }

    public xj a(xp xpVar) {
        return new xj(this.a, xpVar);
    }

    public void a(Annotation annotation) {
        this.b.b(annotation);
    }

    /* renamed from: e */
    public Field a() {
        return this.a;
    }

    public String b() {
        return this.a.getName();
    }

    public <A extends Annotation> A a(Class cls) {
        return this.b.a(cls);
    }

    public Type c() {
        return this.a.getGenericType();
    }

    public Class<?> d() {
        return this.a.getType();
    }

    public Class<?> h() {
        return this.a.getDeclaringClass();
    }

    public Member i() {
        return this.a;
    }

    public void a(Object obj, Object obj2) throws IllegalArgumentException {
        try {
            this.a.set(obj, obj2);
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException("Failed to setValue() for field " + f() + ": " + e.getMessage(), e);
        }
    }

    public String f() {
        return h().getName() + "#" + b();
    }

    public String toString() {
        return "[field " + b() + ", annotations: " + this.b + "]";
    }
}
