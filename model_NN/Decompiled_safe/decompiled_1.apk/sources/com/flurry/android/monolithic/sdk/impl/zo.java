package com.flurry.android.monolithic.sdk.impl;

final class zo extends zf {
    protected final zf p;
    protected final Class<?> q;

    protected zo(zf zfVar, Class<?> cls) {
        super(zfVar);
        this.p = zfVar;
        this.q = cls;
    }

    public zf a(ra<Object> raVar) {
        return new zo(this.p.a(raVar), this.q);
    }

    public void a(Object obj, or orVar, ru ruVar) throws Exception {
        Class<?> a = ruVar.a();
        if (a == null || this.q.isAssignableFrom(a)) {
            this.p.a(obj, orVar, ruVar);
        }
    }
}
