package com.flurry.android.monolithic.sdk.impl;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class aeu implements Iterator<ou> {
    static final aeu a = new aeu();

    private aeu() {
    }

    public static aeu a() {
        return a;
    }

    public boolean hasNext() {
        return false;
    }

    /* renamed from: b */
    public ou next() {
        throw new NoSuchElementException();
    }

    public void remove() {
        throw new IllegalStateException();
    }
}
