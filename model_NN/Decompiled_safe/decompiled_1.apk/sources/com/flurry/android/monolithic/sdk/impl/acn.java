package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;

public final class acn extends abt<AtomicReference<?>> {
    public /* bridge */ /* synthetic */ void a(Object obj, or orVar, ru ruVar) throws IOException, oq {
        a((AtomicReference<?>) ((AtomicReference) obj), orVar, ruVar);
    }

    public acn() {
        super(AtomicReference.class, false);
    }

    public void a(AtomicReference<?> atomicReference, or orVar, ru ruVar) throws IOException, oq {
        ruVar.a(atomicReference.get(), orVar);
    }
}
