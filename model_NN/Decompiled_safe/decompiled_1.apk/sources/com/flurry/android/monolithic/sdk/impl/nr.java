package com.flurry.android.monolithic.sdk.impl;

import java.lang.reflect.Type;

public class nr<T> extends kx<T> {
    public nr() {
        this(null, null, nn.b());
    }

    public nr(Class cls) {
        this(nn.b().a((Type) cls));
    }

    public nr(ji jiVar) {
        this(jiVar, jiVar, nn.b());
    }

    public nr(ji jiVar, ji jiVar2, nn nnVar) {
        super(jiVar, jiVar2, nnVar);
    }

    public nn b() {
        return (nn) a();
    }

    /* access modifiers changed from: protected */
    public Object a(String str, ji jiVar) {
        Class b = b().b(jiVar);
        if (b == null) {
            return super.a(str, jiVar);
        }
        return Enum.valueOf(b, str);
    }
}
