package com.flurry.android.monolithic.sdk.impl;

class ln {
    boolean a;
    private final ll b;
    private byte[] c;
    private int d;
    private int e;

    private ln(ll llVar) {
        this.a = false;
        this.b = llVar;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.c = this.b.b;
        this.d = this.b.d;
        this.e = this.b.e;
        this.a = true;
    }

    /* access modifiers changed from: package-private */
    public int b() {
        if (this.a) {
            return this.d;
        }
        return this.b.d;
    }

    /* access modifiers changed from: package-private */
    public int c() {
        if (this.a) {
            return this.e;
        }
        return this.b.e;
    }

    /* access modifiers changed from: package-private */
    public byte[] d() {
        if (this.a) {
            return this.c;
        }
        return this.b.b;
    }

    /* access modifiers changed from: package-private */
    public void a(int i) {
        if (this.a) {
            this.d = i;
        } else {
            int unused = this.b.d = i;
        }
    }

    /* access modifiers changed from: package-private */
    public void b(int i) {
        if (this.a) {
            this.e = i;
        } else {
            int unused = this.b.e = i;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(byte[] bArr, int i, int i2) {
        if (this.a) {
            this.c = bArr;
            this.e = i + i2;
            this.d = i;
            return;
        }
        byte[] unused = this.b.b = bArr;
        int unused2 = this.b.e = i + i2;
        int unused3 = this.b.d = i;
        int unused4 = this.b.c = i;
    }
}
