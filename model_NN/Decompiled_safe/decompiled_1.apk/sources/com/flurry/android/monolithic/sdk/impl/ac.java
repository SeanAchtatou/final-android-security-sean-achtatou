package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import android.widget.RelativeLayout;
import com.flurry.android.AdCreative;
import com.flurry.android.impl.ads.FlurryAdModule;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;
import java.util.Map;

public abstract class ac extends RelativeLayout {
    private static final String a = ac.class.getSimpleName();
    FlurryAdModule b;
    m c;
    AdUnit d;
    int e;

    public abstract void initLayout();

    protected ac(Context context) {
        super(context);
    }

    public ac(Context context, FlurryAdModule flurryAdModule, m mVar) {
        super(context);
        this.b = flurryAdModule;
        this.c = mVar;
    }

    public m getAdLog() {
        return this.c;
    }

    public void setAdLog(m mVar) {
        this.c = mVar;
    }

    public void setPlatformModule(FlurryAdModule flurryAdModule) {
        this.b = flurryAdModule;
    }

    public AdUnit getAdUnit() {
        return this.d;
    }

    public void setAdUnit(AdUnit adUnit) {
        this.d = adUnit;
    }

    public int getAdFrameIndex() {
        return this.e;
    }

    public void setAdFrameIndex(int i) {
        this.e = i;
    }

    public void onEvent(String str, Map<String, String> map) {
        ja.a(3, a, "AppSpotBannerView.onEvent " + str);
        if (this.d != null) {
            this.b.a(new bh(str, map, getContext(), this.d, this.c, this.e), this.b.a(), 0);
            return;
        }
        ja.a(3, a, "fAdUnit == null");
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        if (this.d.d().get(this.e).e().e().toString().equals(AdCreative.kFormatTakeover)) {
            return false;
        }
        return true;
    }

    public void stop() {
    }
}
