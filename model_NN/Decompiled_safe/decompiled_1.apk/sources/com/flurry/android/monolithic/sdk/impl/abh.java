package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.List;

@rz
public final class abh extends abu<List<String>> implements rp {
    protected ra<String> a;

    public /* bridge */ /* synthetic */ void a(Object obj, or orVar, ru ruVar) throws IOException, oq {
        a((List<String>) ((List) obj), orVar, ruVar);
    }

    public /* bridge */ /* synthetic */ void a(Object obj, or orVar, ru ruVar, rx rxVar) throws IOException, oz {
        a((List<String>) ((List) obj), orVar, ruVar, rxVar);
    }

    public abh(qc qcVar) {
        super(List.class, qcVar);
    }

    public void a(ru ruVar) throws qw {
        ra a2 = ruVar.a(String.class, this.b);
        if (!a(a2)) {
            this.a = a2;
        }
    }

    public void a(List<String> list, or orVar, ru ruVar) throws IOException, oq {
        orVar.b();
        if (this.a == null) {
            b(list, orVar, ruVar);
        } else {
            c(list, orVar, ruVar);
        }
        orVar.c();
    }

    public void a(List<String> list, or orVar, ru ruVar, rx rxVar) throws IOException, oq {
        rxVar.c(list, orVar);
        if (this.a == null) {
            b(list, orVar, ruVar);
        } else {
            c(list, orVar, ruVar);
        }
        rxVar.f(list, orVar);
    }

    private final void b(List<String> list, or orVar, ru ruVar) throws IOException, oq {
        int i;
        Exception e;
        try {
            int size = list.size();
            int i2 = 0;
            while (i2 < size) {
                try {
                    String str = list.get(i2);
                    if (str == null) {
                        ruVar.a(orVar);
                    } else {
                        orVar.b(str);
                    }
                    i2++;
                } catch (Exception e2) {
                    e = e2;
                    i = i2;
                    a(ruVar, e, list, i);
                }
            }
        } catch (Exception e3) {
            Exception exc = e3;
            i = 0;
            e = exc;
            a(ruVar, e, list, i);
        }
    }

    private final void c(List<String> list, or orVar, ru ruVar) throws IOException, oq {
        try {
            int size = list.size();
            ra<String> raVar = this.a;
            for (int i = 0; i < size; i++) {
                String str = list.get(i);
                if (str == null) {
                    ruVar.a(orVar);
                } else {
                    raVar.a(str, orVar, ruVar);
                }
            }
        } catch (Exception e) {
            a(ruVar, e, list, 0);
        }
    }
}
