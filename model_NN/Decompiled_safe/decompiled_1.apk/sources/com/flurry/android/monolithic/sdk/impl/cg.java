package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import android.content.Intent;
import com.flurry.android.FlurryFullscreenTakeoverActivity;
import com.flurry.android.impl.ads.FlurryAdModule;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;

public final class cg extends cn {
    public cg(Context context, FlurryAdModule flurryAdModule, m mVar, AdUnit adUnit) {
        super(context, flurryAdModule, mVar, adUnit);
    }

    public void a() {
        if (!c().a().a(b(), new Intent(ia.a().b(), FlurryFullscreenTakeoverActivity.class), e().b().toString())) {
            ja.b(a, "Unable to launch FlurryFullscreenTakeoverActivity. Fix by declaring this Activity in your AndroidManifest.xml");
        }
    }
}
