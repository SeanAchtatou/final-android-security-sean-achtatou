package com.flurry.android.monolithic.sdk.impl;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

public class te extends qq {
    protected final ConcurrentHashMap<afm, qu<Object>> a;
    protected final HashMap<afm, qu<Object>> b;
    protected final aem c;
    protected qo d;

    public te() {
        this(ss.h);
    }

    public te(qo qoVar) {
        this.a = new ConcurrentHashMap<>(64, 0.75f, 2);
        this.b = new HashMap<>(8);
        this.d = qoVar;
        this.c = new aem();
    }

    public pw a(qk qkVar, afm afm) throws qw {
        return this.c.a(afm, qkVar);
    }

    public qu<Object> a(qk qkVar, afm afm, qc qcVar) throws qw {
        qu<Object> quVar;
        qu<Object> a2 = a(afm);
        if (a2 != null) {
            return a2 instanceof qh ? ((qh) a2).a(qkVar, qcVar) : a2;
        }
        qu<Object> d2 = d(qkVar, afm, qcVar);
        if (d2 == null) {
            quVar = b(afm);
        } else {
            quVar = d2;
        }
        return quVar instanceof qh ? ((qh) quVar).a(qkVar, qcVar) : quVar;
    }

    public qu<Object> b(qk qkVar, afm afm, qc qcVar) throws qw {
        qu<Object> a2 = a(qkVar, afm, qcVar);
        rw b2 = this.d.b(qkVar, afm, qcVar);
        if (b2 != null) {
            return new tf(b2, a2);
        }
        return a2;
    }

    public rc c(qk qkVar, afm afm, qc qcVar) throws qw {
        rc a2 = this.d.a(qkVar, afm, qcVar);
        if (a2 instanceof qi) {
            a2 = ((qi) a2).a(qkVar, qcVar);
        }
        if (a2 == null) {
            return c(afm);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public qu<Object> a(afm afm) {
        if (afm != null) {
            return this.a.get(afm);
        }
        throw new IllegalArgumentException();
    }

    /* access modifiers changed from: protected */
    public qu<Object> d(qk qkVar, afm afm, qc qcVar) throws qw {
        qu<Object> a2;
        synchronized (this.b) {
            a2 = a(afm);
            if (a2 == null) {
                int size = this.b.size();
                if (size <= 0 || (a2 = this.b.get(afm)) == null) {
                    try {
                        a2 = e(qkVar, afm, qcVar);
                        if (size == 0) {
                            if (this.b.size() > 0) {
                                this.b.clear();
                            }
                        }
                    } catch (Throwable th) {
                        if (size == 0 && this.b.size() > 0) {
                            this.b.clear();
                        }
                        throw th;
                    }
                }
            }
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public qu<Object> e(qk qkVar, afm afm, qc qcVar) throws qw {
        try {
            qu<Object> f = f(qkVar, afm, qcVar);
            if (f == null) {
                return null;
            }
            boolean z = f instanceof ro;
            boolean z2 = f.getClass() == sp.class;
            if (!z2 && qkVar.a(ql.USE_ANNOTATIONS)) {
                py a2 = qkVar.a();
                Boolean a3 = a2.a(xh.a(f.getClass(), a2, (qg) null));
                if (a3 != null) {
                    z2 = a3.booleanValue();
                }
            }
            if (z) {
                this.b.put(afm, f);
                a(qkVar, (ro) f);
                this.b.remove(afm);
            }
            if (z2) {
                this.a.put(afm, f);
            }
            return f;
        } catch (IllegalArgumentException e) {
            throw new qw(e.getMessage(), null, e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.ada, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<?>
     arg types: [com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.te, com.flurry.android.monolithic.sdk.impl.ada, com.flurry.android.monolithic.sdk.impl.qc]
     candidates:
      com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.adc, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<?>
      com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.add, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<?>
      com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.adf, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<?>
      com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.adg, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<?>
      com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.afm, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<java.lang.Object>
      com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.ada, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<?> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.adg, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<?>
     arg types: [com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.te, com.flurry.android.monolithic.sdk.impl.adg, com.flurry.android.monolithic.sdk.impl.qc]
     candidates:
      com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.ada, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<?>
      com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.adc, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<?>
      com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.add, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<?>
      com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.adf, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<?>
      com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.afm, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<java.lang.Object>
      com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.adg, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<?> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.adf, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<?>
     arg types: [com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.te, com.flurry.android.monolithic.sdk.impl.adf, com.flurry.android.monolithic.sdk.impl.qc]
     candidates:
      com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.ada, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<?>
      com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.adc, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<?>
      com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.add, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<?>
      com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.adg, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<?>
      com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.afm, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<java.lang.Object>
      com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.adf, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<?> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.add, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<?>
     arg types: [com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.te, com.flurry.android.monolithic.sdk.impl.add, com.flurry.android.monolithic.sdk.impl.qc]
     candidates:
      com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.ada, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<?>
      com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.adc, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<?>
      com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.adf, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<?>
      com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.adg, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<?>
      com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.afm, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<java.lang.Object>
      com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.add, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<?> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.adc, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<?>
     arg types: [com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.te, com.flurry.android.monolithic.sdk.impl.adc, com.flurry.android.monolithic.sdk.impl.qc]
     candidates:
      com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.ada, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<?>
      com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.add, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<?>
      com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.adf, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<?>
      com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.adg, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<?>
      com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.afm, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<java.lang.Object>
      com.flurry.android.monolithic.sdk.impl.qo.a(com.flurry.android.monolithic.sdk.impl.qk, com.flurry.android.monolithic.sdk.impl.qq, com.flurry.android.monolithic.sdk.impl.adc, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.qu<?> */
    /* access modifiers changed from: protected */
    public qu<Object> f(qk qkVar, afm afm, qc qcVar) throws qw {
        if (afm.r()) {
            return this.d.b(qkVar, this, afm, qcVar);
        }
        if (afm.f()) {
            if (afm.b()) {
                return this.d.a(qkVar, (qq) this, (ada) afm, qcVar);
            }
            if (afm.j()) {
                adf adf = (adf) afm;
                if (adf.l()) {
                    return this.d.a(qkVar, (qq) this, (adg) adf, qcVar);
                }
                return this.d.a(qkVar, (qq) this, adf, qcVar);
            } else if (afm.i()) {
                adc adc = (adc) afm;
                if (adc.a_()) {
                    return this.d.a(qkVar, (qq) this, (add) adc, qcVar);
                }
                return this.d.a(qkVar, (qq) this, adc, qcVar);
            }
        }
        if (ou.class.isAssignableFrom(afm.p())) {
            return this.d.c(qkVar, this, afm, qcVar);
        }
        return this.d.a(qkVar, this, afm, qcVar);
    }

    /* access modifiers changed from: protected */
    public void a(qk qkVar, ro roVar) throws qw {
        roVar.a(qkVar, this);
    }

    /* access modifiers changed from: protected */
    public qu<Object> b(afm afm) throws qw {
        if (!adz.d(afm.p())) {
            throw new qw("Can not find a Value deserializer for abstract type " + afm);
        }
        throw new qw("Can not find a Value deserializer for type " + afm);
    }

    /* access modifiers changed from: protected */
    public rc c(afm afm) throws qw {
        throw new qw("Can not find a (Map) Key deserializer for type " + afm);
    }
}
