package com.flurry.android.monolithic.sdk.impl;

final class yd<T> {
    public final T a;
    public final yd<T> b;
    public final String c;
    public final boolean d;
    public final boolean e;

    public yd(T t, yd<T> ydVar, String str, boolean z, boolean z2) {
        this.a = t;
        this.b = ydVar;
        if (str == null) {
            this.c = null;
        } else {
            this.c = str.length() == 0 ? null : str;
        }
        this.d = z;
        this.e = z2;
    }

    public yd<T> a(xl xlVar) {
        if (xlVar == this.a) {
            return this;
        }
        return new yd<>(xlVar, this.b, this.c, this.d, this.e);
    }

    public yd<T> a(yd<xl> ydVar) {
        if (ydVar == this.b) {
            return this;
        }
        return new yd<>(this.a, ydVar, this.c, this.d, this.e);
    }

    public yd<T> a() {
        yd<T> a2;
        if (this.e) {
            if (this.b == null) {
                return null;
            }
            return this.b.a();
        } else if (this.b == null || (a2 = this.b.a()) == this.b) {
            return this;
        } else {
            return a(a2);
        }
    }

    public yd<T> b() {
        yd<T> b2 = this.b == null ? null : this.b.b();
        return this.d ? a(b2) : b2;
    }

    /* access modifiers changed from: private */
    public yd<T> b(yd<T> ydVar) {
        if (this.b == null) {
            return a(ydVar);
        }
        return a(this.b.b(ydVar));
    }

    public yd<T> c() {
        if (this.b == null) {
            return this;
        }
        yd<T> c2 = this.b.c();
        if (this.c != null) {
            if (c2.c == null) {
                return a((yd<xl>) null);
            }
            return a(c2);
        } else if (c2.c != null) {
            return c2;
        } else {
            if (this.d == c2.d) {
                return a(c2);
            }
            return this.d ? a((yd<xl>) null) : c2;
        }
    }

    public String toString() {
        String str = this.a.toString() + "[visible=" + this.d + "]";
        if (this.b != null) {
            return str + ", " + this.b.toString();
        }
        return str;
    }
}
