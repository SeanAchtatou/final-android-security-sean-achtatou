package com.flurry.android.monolithic.sdk.impl;

import com.flurry.android.FlurryFullscreenTakeoverActivity;
import com.flurry.android.impl.appcloud.AppCloudModule;
import java.util.HashMap;
import java.util.List;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.json.JSONTokener;

public class hg extends he {
    protected static final String a = hg.class.getSimpleName();

    public hg(HashMap<String, Object> hashMap) {
        super(hashMap);
    }

    public void a() {
        String str;
        JSONObject jSONObject;
        b("PutOperation Thread");
        try {
            this.f = (fr) this.h.get("del");
            this.g = (fs) this.h.get("del_internal");
            String a2 = a((String) this.h.get(FlurryFullscreenTakeoverActivity.EXTRA_KEY_URL));
            HttpPut httpPut = new HttpPut(a2);
            ja.a(4, a, "Put operation URL = " + a2);
            if (AppCloudModule.c()) {
                this.d = iz.a(new BasicHttpParams());
                this.e = new BasicHttpContext();
                this.e.setAttribute("http.cookie-store", new BasicCookieStore());
                str = (String) this.d.execute(httpPut, new BasicResponseHandler(), this.e);
                JSONTokener jSONTokener = new JSONTokener(str);
                jSONTokener.skipPast("(");
                jSONObject = (JSONObject) jSONTokener.nextValue();
            } else {
                this.d = iz.b(new BasicHttpParams());
                a(httpPut, this.h);
                httpPut.setEntity(new UrlEncodedFormEntity((List) this.h.get("data")));
                str = EntityUtils.toString(this.d.execute(this.c, httpPut, new BasicHttpContext()).getEntity()).toString();
                jSONObject = new JSONObject(str);
            }
            ja.a(4, a, "responseText = " + str);
            if (((String) this.h.get(FlurryFullscreenTakeoverActivity.EXTRA_KEY_URL)).matches(".*v1/user/.*/logout.*")) {
                gr.a = "";
            }
            a(jSONObject);
            if (this.d != null) {
                this.d.getConnectionManager().shutdown();
            }
        } catch (Exception e) {
            ja.a(6, a, "Exception during communication with server: ", e);
            a((JSONObject) null);
            if (this.d != null) {
                this.d.getConnectionManager().shutdown();
            }
        } catch (Throwable th) {
            if (this.d != null) {
                this.d.getConnectionManager().shutdown();
            }
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public void a(JSONObject jSONObject) {
        try {
            fq fqVar = new fq(jSONObject);
            if (this.g != null) {
                this.g.a(fqVar, this.f, gx.METHOD_PUT, this.h);
            } else {
                this.f.a(fqVar);
            }
        } catch (Exception e) {
            ja.a(6, a, "Exception in onPostExecute: ", e);
        }
    }
}
