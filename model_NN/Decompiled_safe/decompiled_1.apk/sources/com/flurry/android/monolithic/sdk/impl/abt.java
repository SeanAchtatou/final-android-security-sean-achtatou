package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public abstract class abt<T> extends ra<T> {
    protected final Class<T> k;

    public abstract void a(Object obj, or orVar, ru ruVar) throws IOException, oq;

    protected abt(Class cls) {
        this.k = cls;
    }

    protected abt(afm afm) {
        this.k = afm.p();
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [java.lang.Class<?>, java.lang.Class<T>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected abt(java.lang.Class<?> r1, boolean r2) {
        /*
            r0 = this;
            r0.<init>()
            r0.k = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.abt.<init>(java.lang.Class, boolean):void");
    }

    public final Class<T> c() {
        return this.k;
    }

    /* access modifiers changed from: protected */
    public boolean a(ra<?> raVar) {
        return (raVar == null || raVar.getClass().getAnnotation(rz.class) == null) ? false : true;
    }

    public void a(ru ruVar, Throwable th, Object obj, String str) throws IOException {
        Throwable th2 = th;
        while ((th2 instanceof InvocationTargetException) && th2.getCause() != null) {
            th2 = th2.getCause();
        }
        if (th2 instanceof Error) {
            throw ((Error) th2);
        }
        boolean z = ruVar == null || ruVar.a(rr.WRAP_EXCEPTIONS);
        if (th2 instanceof IOException) {
            if (!z || !(th2 instanceof qw)) {
                throw ((IOException) th2);
            }
        } else if (!z && (th2 instanceof RuntimeException)) {
            throw ((RuntimeException) th2);
        }
        throw qw.a(th2, obj, str);
    }

    public void a(ru ruVar, Throwable th, Object obj, int i) throws IOException {
        Throwable th2 = th;
        while ((th2 instanceof InvocationTargetException) && th2.getCause() != null) {
            th2 = th2.getCause();
        }
        if (th2 instanceof Error) {
            throw ((Error) th2);
        }
        boolean z = ruVar == null || ruVar.a(rr.WRAP_EXCEPTIONS);
        if (th2 instanceof IOException) {
            if (!z || !(th2 instanceof qw)) {
                throw ((IOException) th2);
            }
        } else if (!z && (th2 instanceof RuntimeException)) {
            throw ((RuntimeException) th2);
        }
        throw qw.a(th2, obj, i);
    }
}
