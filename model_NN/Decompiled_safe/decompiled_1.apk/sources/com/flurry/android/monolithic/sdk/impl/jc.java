package com.flurry.android.monolithic.sdk.impl;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import java.lang.ref.WeakReference;
import java.util.LinkedList;
import java.util.List;

public class jc extends BroadcastReceiver {
    private static jc d;
    boolean a;
    private List<WeakReference<jb>> b = new LinkedList();
    private boolean c = false;

    public static synchronized jc a() {
        jc jcVar;
        synchronized (jc.class) {
            if (d == null) {
                d = new jc();
            }
            jcVar = d;
        }
        return jcVar;
    }

    public synchronized void b() {
        Context b2 = ia.a().b();
        this.c = b2.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") == 0;
        this.a = a(b2);
        if (this.c) {
            d();
        }
    }

    public synchronized void a(jb jbVar) {
        if (jbVar != null) {
            this.b.add(new WeakReference(jbVar));
        }
    }

    public boolean c() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public void d() {
        Context b2 = ia.a().b();
        this.a = a(b2);
        b2.registerReceiver(this, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    public void onReceive(Context context, Intent intent) {
        boolean a2 = a(context);
        if (this.a != a2) {
            this.a = a2;
            for (WeakReference weakReference : new LinkedList(this.b)) {
                jb jbVar = (jb) weakReference.get();
                if (jbVar != null) {
                    jbVar.b(this.a);
                }
            }
        }
    }

    private boolean a(Context context) {
        if (!this.c || context == null) {
            return true;
        }
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public jd e() {
        if (!this.c) {
            return jd.NONE_OR_UNKNOWN;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) ia.a().b().getSystemService("connectivity");
        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(1);
        if (networkInfo != null && networkInfo.isConnected()) {
            return jd.WIFI;
        }
        NetworkInfo networkInfo2 = connectivityManager.getNetworkInfo(0);
        if (networkInfo2 == null || !networkInfo2.isConnected()) {
            return jd.NONE_OR_UNKNOWN;
        }
        return jd.CELL;
    }
}
