package com.flurry.android.monolithic.sdk.impl;

import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.flurry.android.impl.ads.avro.protocol.v6.AdSpaceLayout;

final class ak extends aj {
    ak() {
        super();
    }

    public ViewGroup.LayoutParams a(AdSpaceLayout adSpaceLayout) {
        return new LinearLayout.LayoutParams(b(adSpaceLayout), c(adSpaceLayout));
    }
}
