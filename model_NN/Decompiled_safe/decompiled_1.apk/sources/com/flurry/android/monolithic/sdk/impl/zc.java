package com.flurry.android.monolithic.sdk.impl;

import java.lang.reflect.Method;
import java.util.Map;

public class zc {
    protected final Method a;
    protected final abl b;

    public zc(xl xlVar, abl abl) {
        this.a = xlVar.a();
        this.b = abl;
    }

    public void a(Object obj, or orVar, ru ruVar) throws Exception {
        Object invoke = this.a.invoke(obj, new Object[0]);
        if (invoke != null) {
            if (!(invoke instanceof Map)) {
                throw new qw("Value returned by 'any-getter' (" + this.a.getName() + "()) not java.util.Map but " + invoke.getClass().getName());
            }
            this.b.b((Map) invoke, orVar, ruVar);
        }
    }

    public void a(ru ruVar) throws qw {
        this.b.a(ruVar);
    }
}
