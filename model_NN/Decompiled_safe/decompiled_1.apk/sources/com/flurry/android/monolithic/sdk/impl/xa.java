package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.sql.Timestamp;

public class xa extends wv<Timestamp> {
    public xa() {
        super(Timestamp.class);
    }

    /* renamed from: b */
    public Timestamp a(ow owVar, qm qmVar) throws IOException, oz {
        return new Timestamp(B(owVar, qmVar).getTime());
    }
}
