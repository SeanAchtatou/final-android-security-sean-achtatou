package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

@rz
public class ud extends wv<Calendar> {
    protected final Class<? extends Calendar> a;

    public ud() {
        this(null);
    }

    public ud(Class<? extends Calendar> cls) {
        super(Calendar.class);
        this.a = cls;
    }

    /* renamed from: b */
    public Calendar a(ow owVar, qm qmVar) throws IOException, oz {
        Date B = B(owVar, qmVar);
        if (B == null) {
            return null;
        }
        if (this.a == null) {
            return qmVar.a(B);
        }
        try {
            Calendar calendar = (Calendar) this.a.newInstance();
            calendar.setTimeInMillis(B.getTime());
            return calendar;
        } catch (Exception e) {
            throw qmVar.a(this.a, e);
        }
    }
}
