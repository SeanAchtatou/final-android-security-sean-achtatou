package com.flurry.android.monolithic.sdk.impl;

public abstract class wa<T> extends wv<T> {
    final T a;

    protected wa(Class<T> cls, T t) {
        super(cls);
        this.a = t;
    }

    public final T b() {
        return this.a;
    }
}
