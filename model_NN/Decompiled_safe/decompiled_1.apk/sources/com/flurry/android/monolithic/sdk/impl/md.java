package com.flurry.android.monolithic.sdk.impl;

import java.io.OutputStream;

public class md {
    private static final md c = new mf();
    protected int a = 2048;
    protected int b = 65536;

    public static md a() {
        return c;
    }

    public md a(int i) {
        int i2 = 32;
        if (i >= 32) {
            i2 = i;
        }
        if (i2 > 16777216) {
            i2 = 16777216;
        }
        this.a = i2;
        return this;
    }

    public lr a(OutputStream outputStream, lr lrVar) {
        if (lrVar == null || !lrVar.getClass().equals(ls.class)) {
            return new ls(outputStream, this.a);
        }
        return ((ls) lrVar).a(outputStream, this.a);
    }

    public lr b(OutputStream outputStream, lr lrVar) {
        if (lrVar == null || !lrVar.getClass().equals(mb.class)) {
            return new mb(outputStream);
        }
        return ((mb) lrVar).a(outputStream);
    }
}
