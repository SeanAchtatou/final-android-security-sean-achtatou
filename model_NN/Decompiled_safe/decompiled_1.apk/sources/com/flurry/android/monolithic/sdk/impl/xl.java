package com.flurry.android.monolithic.sdk.impl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

public final class xl extends xo {
    protected final Method a;
    protected Class<?>[] c;

    public xl(Method method, xp xpVar, xp[] xpVarArr) {
        super(xpVar, xpVarArr);
        this.a = method;
    }

    public xl a(Method method) {
        return new xl(method, this.b, this.d);
    }

    public xl a(xp xpVar) {
        return new xl(this.a, xpVar, this.d);
    }

    /* renamed from: e */
    public Method a() {
        return this.a;
    }

    public String b() {
        return this.a.getName();
    }

    public Type c() {
        return this.a.getGenericReturnType();
    }

    public Class<?> d() {
        return this.a.getReturnType();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.xo.a(com.flurry.android.monolithic.sdk.impl.adj, java.lang.reflect.TypeVariable<?>[]):com.flurry.android.monolithic.sdk.impl.afm
     arg types: [com.flurry.android.monolithic.sdk.impl.adj, java.lang.reflect.TypeVariable<java.lang.reflect.Method>[]]
     candidates:
      com.flurry.android.monolithic.sdk.impl.xl.a(java.lang.Object, java.lang.Object):void
      com.flurry.android.monolithic.sdk.impl.xo.a(int, com.flurry.android.monolithic.sdk.impl.xp):com.flurry.android.monolithic.sdk.impl.xn
      com.flurry.android.monolithic.sdk.impl.xo.a(int, java.lang.annotation.Annotation):void
      com.flurry.android.monolithic.sdk.impl.xk.a(java.lang.Object, java.lang.Object):void
      com.flurry.android.monolithic.sdk.impl.xo.a(com.flurry.android.monolithic.sdk.impl.adj, java.lang.reflect.TypeVariable<?>[]):com.flurry.android.monolithic.sdk.impl.afm */
    public afm a(adj adj) {
        return a(adj, (TypeVariable<?>[]) this.a.getTypeParameters());
    }

    public final Object g() throws Exception {
        return this.a.invoke(null, new Object[0]);
    }

    public final Object a(Object[] objArr) throws Exception {
        return this.a.invoke(null, objArr);
    }

    public final Object a(Object obj) throws Exception {
        return this.a.invoke(null, obj);
    }

    public Class<?> h() {
        return this.a.getDeclaringClass();
    }

    public Member i() {
        return this.a;
    }

    public void a(Object obj, Object obj2) throws IllegalArgumentException {
        try {
            this.a.invoke(obj, obj2);
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException("Failed to setValue() with method " + n() + ": " + e.getMessage(), e);
        } catch (InvocationTargetException e2) {
            throw new IllegalArgumentException("Failed to setValue() with method " + n() + ": " + e2.getMessage(), e2);
        }
    }

    public int f() {
        return l().length;
    }

    public Type[] l() {
        return this.a.getGenericParameterTypes();
    }

    public Class<?> a(int i) {
        Class<?>[] parameterTypes = this.a.getParameterTypes();
        if (i >= parameterTypes.length) {
            return null;
        }
        return parameterTypes[i];
    }

    public Type b(int i) {
        Type[] genericParameterTypes = this.a.getGenericParameterTypes();
        if (i >= genericParameterTypes.length) {
            return null;
        }
        return genericParameterTypes[i];
    }

    public Class<?>[] m() {
        if (this.c == null) {
            this.c = this.a.getParameterTypes();
        }
        return this.c;
    }

    public String n() {
        return h().getName() + "#" + b() + "(" + f() + " params)";
    }

    public String toString() {
        return "[method " + b() + ", annotations: " + this.b + "]";
    }
}
