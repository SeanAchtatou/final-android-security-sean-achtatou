package com.flurry.android.monolithic.sdk.impl;

public class jg extends RuntimeException {
    public jg(Throwable th) {
        super(th);
    }

    public jg(String str) {
        super(str);
    }

    public jg(String str, Throwable th) {
        super(str, th);
    }
}
