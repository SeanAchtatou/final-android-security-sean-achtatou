package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

class jz extends ji {
    private final ji f;

    public jz(ji jiVar) {
        super(kj.MAP);
        this.f = jiVar;
    }

    public ji j() {
        return this.f;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof jz)) {
            return false;
        }
        jz jzVar = (jz) obj;
        return c(jzVar) && this.f.equals(jzVar.f) && this.c.equals(jzVar.c);
    }

    /* access modifiers changed from: package-private */
    public int m() {
        return super.m() + this.f.m();
    }

    /* access modifiers changed from: package-private */
    public void a(kc kcVar, or orVar) throws IOException {
        orVar.d();
        orVar.a("type", "map");
        orVar.a("values");
        this.f.a(kcVar, orVar);
        this.c.a(orVar);
        orVar.e();
    }
}
