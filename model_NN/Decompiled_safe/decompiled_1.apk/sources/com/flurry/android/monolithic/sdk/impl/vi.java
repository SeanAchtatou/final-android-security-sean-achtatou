package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
final class vi extends ve<double[]> {
    public vi() {
        super(double[].class);
    }

    /* renamed from: b */
    public double[] a(ow owVar, qm qmVar) throws IOException, oz {
        if (!owVar.j()) {
            return c(owVar, qmVar);
        }
        adt g = qmVar.h().g();
        double[] dArr = (double[]) g.a();
        int i = 0;
        while (owVar.b() != pb.END_ARRAY) {
            double A = A(owVar, qmVar);
            if (i >= dArr.length) {
                dArr = (double[]) g.a(dArr, i);
                i = 0;
            }
            dArr[i] = A;
            i++;
        }
        return (double[]) g.b(dArr, i);
    }

    private final double[] c(ow owVar, qm qmVar) throws IOException, oz {
        if (owVar.e() == pb.VALUE_STRING && qmVar.a(ql.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT) && owVar.k().length() == 0) {
            return null;
        }
        if (!qmVar.a(ql.ACCEPT_SINGLE_VALUE_AS_ARRAY)) {
            throw qmVar.b(this.q);
        }
        return new double[]{A(owVar, qmVar)};
    }
}
