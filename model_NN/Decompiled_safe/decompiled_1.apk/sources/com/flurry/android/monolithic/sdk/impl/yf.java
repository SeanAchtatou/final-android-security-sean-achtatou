package com.flurry.android.monolithic.sdk.impl;

import com.flurry.org.codehaus.jackson.annotate.JsonAutoDetect;
import com.flurry.org.codehaus.jackson.annotate.JsonMethod;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;

@JsonAutoDetect(creatorVisibility = JsonAutoDetect.Visibility.ANY, fieldVisibility = JsonAutoDetect.Visibility.PUBLIC_ONLY, getterVisibility = JsonAutoDetect.Visibility.PUBLIC_ONLY, isGetterVisibility = JsonAutoDetect.Visibility.PUBLIC_ONLY, setterVisibility = JsonAutoDetect.Visibility.ANY)
public class yf implements ye<yf> {
    protected static final yf a = new yf((JsonAutoDetect) yf.class.getAnnotation(JsonAutoDetect.class));
    protected final JsonAutoDetect.Visibility b;
    protected final JsonAutoDetect.Visibility c;
    protected final JsonAutoDetect.Visibility d;
    protected final JsonAutoDetect.Visibility e;
    protected final JsonAutoDetect.Visibility f;

    public static yf a() {
        return a;
    }

    public yf(JsonAutoDetect jsonAutoDetect) {
        JsonMethod[] value = jsonAutoDetect.value();
        this.b = a(value, JsonMethod.GETTER) ? jsonAutoDetect.getterVisibility() : JsonAutoDetect.Visibility.NONE;
        this.c = a(value, JsonMethod.IS_GETTER) ? jsonAutoDetect.isGetterVisibility() : JsonAutoDetect.Visibility.NONE;
        this.d = a(value, JsonMethod.SETTER) ? jsonAutoDetect.setterVisibility() : JsonAutoDetect.Visibility.NONE;
        this.e = a(value, JsonMethod.CREATOR) ? jsonAutoDetect.creatorVisibility() : JsonAutoDetect.Visibility.NONE;
        this.f = a(value, JsonMethod.FIELD) ? jsonAutoDetect.fieldVisibility() : JsonAutoDetect.Visibility.NONE;
    }

    public yf(JsonAutoDetect.Visibility visibility, JsonAutoDetect.Visibility visibility2, JsonAutoDetect.Visibility visibility3, JsonAutoDetect.Visibility visibility4, JsonAutoDetect.Visibility visibility5) {
        this.b = visibility;
        this.c = visibility2;
        this.d = visibility3;
        this.e = visibility4;
        this.f = visibility5;
    }

    /* renamed from: b */
    public yf a(JsonAutoDetect jsonAutoDetect) {
        if (jsonAutoDetect == null) {
            return this;
        }
        JsonMethod[] value = jsonAutoDetect.value();
        return a(a(value, JsonMethod.GETTER) ? jsonAutoDetect.getterVisibility() : JsonAutoDetect.Visibility.NONE).b(a(value, JsonMethod.IS_GETTER) ? jsonAutoDetect.isGetterVisibility() : JsonAutoDetect.Visibility.NONE).c(a(value, JsonMethod.SETTER) ? jsonAutoDetect.setterVisibility() : JsonAutoDetect.Visibility.NONE).d(a(value, JsonMethod.CREATOR) ? jsonAutoDetect.creatorVisibility() : JsonAutoDetect.Visibility.NONE).e(a(value, JsonMethod.FIELD) ? jsonAutoDetect.fieldVisibility() : JsonAutoDetect.Visibility.NONE);
    }

    /* renamed from: f */
    public yf a(JsonAutoDetect.Visibility visibility) {
        JsonAutoDetect.Visibility visibility2;
        if (visibility == JsonAutoDetect.Visibility.DEFAULT) {
            visibility2 = a.b;
        } else {
            visibility2 = visibility;
        }
        return this.b == visibility2 ? this : new yf(visibility2, this.c, this.d, this.e, this.f);
    }

    /* renamed from: g */
    public yf b(JsonAutoDetect.Visibility visibility) {
        JsonAutoDetect.Visibility visibility2;
        if (visibility == JsonAutoDetect.Visibility.DEFAULT) {
            visibility2 = a.c;
        } else {
            visibility2 = visibility;
        }
        return this.c == visibility2 ? this : new yf(this.b, visibility2, this.d, this.e, this.f);
    }

    /* renamed from: h */
    public yf c(JsonAutoDetect.Visibility visibility) {
        JsonAutoDetect.Visibility visibility2;
        if (visibility == JsonAutoDetect.Visibility.DEFAULT) {
            visibility2 = a.d;
        } else {
            visibility2 = visibility;
        }
        return this.d == visibility2 ? this : new yf(this.b, this.c, visibility2, this.e, this.f);
    }

    /* renamed from: i */
    public yf d(JsonAutoDetect.Visibility visibility) {
        JsonAutoDetect.Visibility visibility2;
        if (visibility == JsonAutoDetect.Visibility.DEFAULT) {
            visibility2 = a.e;
        } else {
            visibility2 = visibility;
        }
        return this.e == visibility2 ? this : new yf(this.b, this.c, this.d, visibility2, this.f);
    }

    /* renamed from: j */
    public yf e(JsonAutoDetect.Visibility visibility) {
        JsonAutoDetect.Visibility visibility2;
        if (visibility == JsonAutoDetect.Visibility.DEFAULT) {
            visibility2 = a.f;
        } else {
            visibility2 = visibility;
        }
        return this.f == visibility2 ? this : new yf(this.b, this.c, this.d, this.e, visibility2);
    }

    public boolean a(Member member) {
        return this.e.isVisible(member);
    }

    public boolean a(xk xkVar) {
        return a(xkVar.i());
    }

    public boolean a(Field field) {
        return this.f.isVisible(field);
    }

    public boolean a(xj xjVar) {
        return a(xjVar.a());
    }

    public boolean a(Method method) {
        return this.b.isVisible(method);
    }

    public boolean a(xl xlVar) {
        return a(xlVar.a());
    }

    public boolean b(Method method) {
        return this.c.isVisible(method);
    }

    public boolean b(xl xlVar) {
        return b(xlVar.a());
    }

    public boolean c(Method method) {
        return this.d.isVisible(method);
    }

    public boolean c(xl xlVar) {
        return c(xlVar.a());
    }

    private static boolean a(JsonMethod[] jsonMethodArr, JsonMethod jsonMethod) {
        for (JsonMethod jsonMethod2 : jsonMethodArr) {
            if (jsonMethod2 == jsonMethod || jsonMethod2 == JsonMethod.ALL) {
                return true;
            }
        }
        return false;
    }

    public String toString() {
        return "[Visibility:" + " getter: " + this.b + ", isGetter: " + this.c + ", setter: " + this.d + ", creator: " + this.e + ", field: " + this.f + "]";
    }
}
