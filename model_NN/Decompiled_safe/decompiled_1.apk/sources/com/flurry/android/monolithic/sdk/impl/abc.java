package com.flurry.android.monolithic.sdk.impl;

public abstract class abc<T> extends abt<T> {
    public abstract abc<?> a(rx rxVar);

    protected abc(Class<T> cls) {
        super(cls);
    }

    protected abc(Class<?> cls, boolean z) {
        super(cls, z);
    }

    public abc<?> b(rx rxVar) {
        return rxVar == null ? this : a(rxVar);
    }
}
