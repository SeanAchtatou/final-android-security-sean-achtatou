package com.flurry.android.monolithic.sdk.impl;

public final class adh extends adi {
    protected final afm[] a;
    protected final String[] b;

    protected adh(Class<?> cls) {
        this(cls, null, null, null, null);
    }

    protected adh(Class<?> cls, String[] strArr, afm[] afmArr, Object obj, Object obj2) {
        super(cls, 0, obj, obj2);
        if (strArr == null || strArr.length == 0) {
            this.b = null;
            this.a = null;
            return;
        }
        this.b = strArr;
        this.a = afmArr;
    }

    public static adh d(Class<?> cls) {
        return new adh(cls, null, null, null, null);
    }

    /* access modifiers changed from: protected */
    public afm a(Class<?> cls) {
        return new adh(cls, this.b, this.a, this.f, this.g);
    }

    public afm b(Class<?> cls) {
        throw new IllegalArgumentException("Internal error: SimpleType.narrowContentsBy() should never be called");
    }

    public afm c(Class<?> cls) {
        throw new IllegalArgumentException("Internal error: SimpleType.widenContentsBy() should never be called");
    }

    /* renamed from: a */
    public adh f(Object obj) {
        return new adh(this.d, this.b, this.a, this.f, obj);
    }

    public afm e(Object obj) {
        throw new IllegalArgumentException("Simple types have no content types; can not call withContenTypeHandler()");
    }

    /* renamed from: b */
    public adh d(Object obj) {
        if (obj == this.f) {
            return this;
        }
        return new adh(this.d, this.b, this.a, obj, this.g);
    }

    /* access modifiers changed from: protected */
    public String a() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.d.getName());
        if (this.a != null && this.a.length > 0) {
            sb.append('<');
            boolean z = true;
            for (afm afm : this.a) {
                if (z) {
                    z = false;
                } else {
                    sb.append(',');
                }
                sb.append(afm.m());
            }
            sb.append('>');
        }
        return sb.toString();
    }

    public boolean f() {
        return false;
    }

    public int h() {
        if (this.a == null) {
            return 0;
        }
        return this.a.length;
    }

    public afm b(int i) {
        if (i < 0 || this.a == null || i >= this.a.length) {
            return null;
        }
        return this.a[i];
    }

    public String a(int i) {
        if (i < 0 || this.b == null || i >= this.b.length) {
            return null;
        }
        return this.b[i];
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(40);
        sb.append("[simple type, class ").append(a()).append(']');
        return sb.toString();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        adh adh = (adh) obj;
        if (adh.d != this.d) {
            return false;
        }
        afm[] afmArr = this.a;
        afm[] afmArr2 = adh.a;
        if (afmArr == null) {
            return afmArr2 == null || afmArr2.length == 0;
        }
        if (afmArr2 == null) {
            return false;
        }
        if (afmArr.length != afmArr2.length) {
            return false;
        }
        int length = afmArr.length;
        for (int i = 0; i < length; i++) {
            if (!afmArr[i].equals(afmArr2[i])) {
                return false;
            }
        }
        return true;
    }
}
