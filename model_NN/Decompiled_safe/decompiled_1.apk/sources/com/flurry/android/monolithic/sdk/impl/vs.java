package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
public final class vs extends wa<Boolean> {
    public vs(Class<Boolean> cls, Boolean bool) {
        super(cls, bool);
    }

    /* renamed from: b */
    public Boolean a(ow owVar, qm qmVar) throws IOException, oz {
        return o(owVar, qmVar);
    }

    /* renamed from: b */
    public Boolean a(ow owVar, qm qmVar, rw rwVar) throws IOException, oz {
        return o(owVar, qmVar);
    }
}
