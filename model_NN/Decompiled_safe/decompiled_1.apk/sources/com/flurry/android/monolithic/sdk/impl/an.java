package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.flurry.android.FlurryAdSize;
import com.flurry.android.impl.ads.FlurryAdModule;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public final class an extends RelativeLayout {
    private static final String a = an.class.getSimpleName();
    private final FlurryAdModule b;
    private final String c;
    private final ViewGroup d;
    private long e;
    private final ScheduledExecutorService f;
    /* access modifiers changed from: private */
    public final Runnable g = new ap(this, null);
    private ScheduledFuture<?> h;

    public an(FlurryAdModule flurryAdModule, Context context, String str, ViewGroup viewGroup, ScheduledExecutorService scheduledExecutorService) {
        super(context);
        this.b = flurryAdModule;
        this.c = str;
        this.d = viewGroup;
        this.f = scheduledExecutorService;
    }

    public long getRotationRateInMilliseconds() {
        return this.e;
    }

    public ViewGroup getViewGroup() {
        return this.d;
    }

    public String getAdSpace() {
        return this.c;
    }

    public synchronized void a(long j) {
        if (this.e != j) {
            d();
            this.e = j;
            c();
        }
    }

    public synchronized void a() {
        d();
        this.e = 0;
        removeCallbacks(this.g);
    }

    private boolean c() {
        if (!(this.f == null || this.h != null || 0 == getRotationRateInMilliseconds())) {
            ja.a(3, a, "schedule banner rotation for adSpace = " + getAdSpace() + " with fixed rate in milliseconds = " + getRotationRateInMilliseconds());
            this.h = this.f.scheduleAtFixedRate(new ao(this), getRotationRateInMilliseconds(), getRotationRateInMilliseconds(), TimeUnit.MILLISECONDS);
        }
        return this.h != null;
    }

    private void d() {
        if (this.h != null) {
            ja.a(3, a, "cancel banner rotation for adSpace = " + getAdSpace() + " with fixed rate in milliseconds = " + getRotationRateInMilliseconds());
            this.h.cancel(true);
            this.h = null;
        }
    }

    public void b() {
        a();
        ac currentBannerView = getCurrentBannerView();
        if (currentBannerView != null) {
            currentBannerView.stop();
        }
    }

    private ac getCurrentBannerView() {
        if (getChildCount() < 1) {
            return null;
        }
        try {
            return (ac) getChildAt(0);
        } catch (ClassCastException e2) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public boolean e() {
        if (FlurryAdModule.e()) {
            ja.a(3, a, "Device is locked: banner will NOT rotate for adSpace: " + getAdSpace());
            return false;
        } else if (this.b.c()) {
            ja.a(3, a, "Ad fullscreen panel is opened: banner will NOT rotate for adSpace: " + getAdSpace());
            return false;
        } else {
            ac currentBannerView = getCurrentBannerView();
            return currentBannerView != null && currentBannerView.e();
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        ja.a(3, a, "Rotating banner for adSpace: " + getAdSpace());
        this.b.b().a(getContext(), this.c, FlurryAdSize.BANNER_BOTTOM, this.d, 1);
    }
}
