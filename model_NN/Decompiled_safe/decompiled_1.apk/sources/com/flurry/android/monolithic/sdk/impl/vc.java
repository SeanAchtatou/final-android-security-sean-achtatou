package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.lang.reflect.Array;

@rz
public class vc extends ug<Object[]> {
    protected final afm a;
    protected final boolean b;
    protected final Class<?> c;
    protected final qu<Object> d;
    protected final rw e;

    public vc(ada ada, qu<Object> quVar, rw rwVar) {
        super(Object[].class);
        this.a = ada;
        this.c = ada.g().p();
        this.b = this.c == Object.class;
        this.d = quVar;
        this.e = rwVar;
    }

    public qu<Object> d() {
        return this.d;
    }

    /* renamed from: b */
    public Object[] a(ow owVar, qm qmVar) throws IOException, oz {
        Object[] a2;
        Object a3;
        if (!owVar.j()) {
            return d(owVar, qmVar);
        }
        aeh g = qmVar.g();
        Object[] a4 = g.a();
        rw rwVar = this.e;
        Object[] objArr = a4;
        int i = 0;
        while (true) {
            pb b2 = owVar.b();
            if (b2 == pb.END_ARRAY) {
                break;
            }
            if (b2 == pb.VALUE_NULL) {
                a3 = null;
            } else if (rwVar == null) {
                a3 = this.d.a(owVar, qmVar);
            } else {
                a3 = this.d.a(owVar, qmVar, rwVar);
            }
            if (i >= objArr.length) {
                objArr = g.a(objArr);
                i = 0;
            }
            objArr[i] = a3;
            i++;
        }
        if (this.b) {
            a2 = g.a(objArr, i);
        } else {
            a2 = g.a(objArr, i, this.c);
        }
        qmVar.a(g);
        return a2;
    }

    /* renamed from: b */
    public Object[] a(ow owVar, qm qmVar, rw rwVar) throws IOException, oz {
        return (Object[]) rwVar.b(owVar, qmVar);
    }

    /* access modifiers changed from: protected */
    public Byte[] c(ow owVar, qm qmVar) throws IOException, oz {
        byte[] a2 = owVar.a(qmVar.c());
        Byte[] bArr = new Byte[a2.length];
        int length = a2.length;
        for (int i = 0; i < length; i++) {
            bArr[i] = Byte.valueOf(a2[i]);
        }
        return bArr;
    }

    private final Object[] d(ow owVar, qm qmVar) throws IOException, oz {
        Object a2;
        Object[] objArr;
        if (owVar.e() == pb.VALUE_STRING && qmVar.a(ql.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT) && owVar.k().length() == 0) {
            return null;
        }
        if (qmVar.a(ql.ACCEPT_SINGLE_VALUE_AS_ARRAY)) {
            if (owVar.e() == pb.VALUE_NULL) {
                a2 = null;
            } else if (this.e == null) {
                a2 = this.d.a(owVar, qmVar);
            } else {
                a2 = this.d.a(owVar, qmVar, this.e);
            }
            if (this.b) {
                objArr = new Object[1];
            } else {
                objArr = (Object[]) Array.newInstance(this.c, 1);
            }
            objArr[0] = a2;
            return objArr;
        } else if (owVar.e() == pb.VALUE_STRING && this.c == Byte.class) {
            return c(owVar, qmVar);
        } else {
            throw qmVar.b(this.a.p());
        }
    }
}
