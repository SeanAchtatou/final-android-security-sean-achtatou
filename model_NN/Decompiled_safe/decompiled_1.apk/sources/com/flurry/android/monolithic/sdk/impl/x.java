package com.flurry.android.monolithic.sdk.impl;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.MotionEvent;

class x extends Dialog {
    final /* synthetic */ Activity a;
    final /* synthetic */ w b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    x(w wVar, Context context, int i, Activity activity) {
        super(context, i);
        this.b = wVar;
        this.a = activity;
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        return this.a.dispatchTouchEvent(motionEvent);
    }

    public boolean dispatchTrackballEvent(MotionEvent motionEvent) {
        return this.a.dispatchTrackballEvent(motionEvent);
    }
}
