package com.flurry.android.monolithic.sdk.impl;

import android.text.TextUtils;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class m {
    private static int a = 1;
    private final int b;
    private final long c;
    private final String d;
    private List<k> e;

    public m(long j, String str, long j2) {
        int i = a;
        a = i + 1;
        this.b = i;
        this.c = j;
        this.d = str;
        this.e = new ArrayList();
    }

    public m(DataInput dataInput) throws IOException {
        this.b = dataInput.readInt();
        this.c = dataInput.readLong();
        String readUTF = dataInput.readUTF();
        this.d = readUTF.equals("") ? null : readUTF;
        this.e = new ArrayList();
        short readShort = dataInput.readShort();
        for (short s = 0; s < readShort; s = (short) (s + 1)) {
            this.e.add(new k(dataInput));
        }
    }

    public void a(k kVar) {
        this.e.add(kVar);
    }

    public void a(DataOutput dataOutput) throws IOException {
        dataOutput.writeInt(this.b);
        dataOutput.writeLong(this.c);
        dataOutput.writeUTF(this.d == null ? "" : this.d);
        dataOutput.writeShort(this.e.size());
        for (k a2 : this.e) {
            a2.a(dataOutput);
        }
    }

    public int a() {
        return this.b;
    }

    public String b() {
        return this.d;
    }

    public long c() {
        return this.c;
    }

    public List<k> d() {
        return this.e;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof m)) {
            return false;
        }
        m mVar = (m) obj;
        return this.b == mVar.b && this.c == mVar.c && TextUtils.equals(this.d, mVar.d) && (this.e == mVar.e || (this.e != null && this.e.equals(mVar.e)));
    }

    public int hashCode() {
        int i = (int) (((long) (17 | this.b)) | this.c);
        if (this.d != null) {
            i |= this.d.hashCode();
        }
        if (this.e != null) {
            return i | this.e.hashCode();
        }
        return i;
    }
}
