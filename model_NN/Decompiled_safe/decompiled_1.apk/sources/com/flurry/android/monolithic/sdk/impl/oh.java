package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Random;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.message.BasicHeader;

public class oh implements HttpEntity {
    private static final char[] a = "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
    private final oc b;
    private final Header c;
    private long d;
    private volatile boolean e;

    public oh(oe oeVar, String str, Charset charset) {
        String str2;
        oe oeVar2;
        if (str == null) {
            str2 = a();
        } else {
            str2 = str;
        }
        if (oeVar == null) {
            oeVar2 = oe.STRICT;
        } else {
            oeVar2 = oeVar;
        }
        this.b = new oc("form-data", charset, str2, oeVar2);
        this.c = new BasicHeader("Content-Type", a(str2, charset));
        this.e = true;
    }

    public oh(oe oeVar) {
        this(oeVar, null, null);
    }

    public oh() {
        this(oe.STRICT, null, null);
    }

    /* access modifiers changed from: protected */
    public String a(String str, Charset charset) {
        StringBuilder sb = new StringBuilder();
        sb.append("multipart/form-data; boundary=");
        sb.append(str);
        if (charset != null) {
            sb.append("; charset=");
            sb.append(charset.name());
        }
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public String a() {
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        int nextInt = random.nextInt(11) + 30;
        for (int i = 0; i < nextInt; i++) {
            sb.append(a[random.nextInt(a.length)]);
        }
        return sb.toString();
    }

    public void a(oa oaVar) {
        this.b.a(oaVar);
        this.e = true;
    }

    public void a(String str, oj ojVar) {
        a(new oa(str, ojVar));
    }

    public boolean isRepeatable() {
        for (oa b2 : this.b.a()) {
            if (b2.b().e() < 0) {
                return false;
            }
        }
        return true;
    }

    public boolean isChunked() {
        return !isRepeatable();
    }

    public boolean isStreaming() {
        return !isRepeatable();
    }

    public long getContentLength() {
        if (this.e) {
            this.d = this.b.c();
            this.e = false;
        }
        return this.d;
    }

    public Header getContentType() {
        return this.c;
    }

    public Header getContentEncoding() {
        return null;
    }

    public void consumeContent() throws IOException, UnsupportedOperationException {
        if (isStreaming()) {
            throw new UnsupportedOperationException("Streaming entity does not implement #consumeContent()");
        }
    }

    public InputStream getContent() throws IOException, UnsupportedOperationException {
        throw new UnsupportedOperationException("Multipart form entity does not implement #getContent()");
    }

    public void writeTo(OutputStream outputStream) throws IOException {
        this.b.a(outputStream);
    }
}
