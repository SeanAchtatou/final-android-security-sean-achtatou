package com.flurry.android.monolithic.sdk.impl;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public final class l {
    private static final String a = l.class.getSimpleName();

    private l() {
    }

    public static void a(List<m> list, DataOutput dataOutput) throws IOException {
        dataOutput.writeShort(46586);
        b(list, dataOutput);
        dataOutput.writeShort(0);
    }

    public static List<m> a(DataInput dataInput) throws IOException {
        if (46586 == dataInput.readUnsignedShort()) {
            return b(dataInput);
        }
        throw new IOException("Unexpected data format");
    }

    private static void b(List<m> list, DataOutput dataOutput) {
        for (m next : list) {
            try {
                dataOutput.writeShort(1);
                next.a(dataOutput);
            } catch (IOException e) {
                ja.a(3, a, "unable to write adLog with GUID: " + next.b(), e);
                return;
            }
        }
    }

    private static List<m> b(DataInput dataInput) {
        ArrayList arrayList = new ArrayList();
        while (1 == dataInput.readUnsignedShort()) {
            try {
                arrayList.add(new m(dataInput));
            } catch (IOException e) {
                ja.a(3, a, "unable to read adLog: ", e);
            }
        }
        return arrayList;
    }
}
