package com.flurry.android.monolithic.sdk.impl;

import com.millennialmedia.android.MMAd;
import com.millennialmedia.android.MMException;
import com.millennialmedia.android.RequestListener;
import java.util.Collections;

class ea implements RequestListener {
    final /* synthetic */ dz a;

    ea(dz dzVar) {
        this.a = dzVar;
    }

    public void requestFailed(MMAd mMAd, MMException mMException) {
        this.a.d(Collections.emptyMap());
        ja.a(3, dz.c, "Millennial MMAdView Interstitial failed to load ad.");
    }

    public void requestCompleted(MMAd mMAd) {
        ja.a(3, dz.c, "Millennial MMAdView returned interstitial ad: " + System.currentTimeMillis());
        if (!this.a.d) {
            this.a.b.display();
        }
    }

    public void MMAdOverlayLaunched(MMAd mMAd) {
        this.a.a(Collections.emptyMap());
        this.a.c(Collections.emptyMap());
        ja.a(3, dz.c, "Millennial MMAdView Interstitial overlay launched." + System.currentTimeMillis());
    }

    public void MMAdRequestIsCaching(MMAd mMAd) {
        ja.a(3, dz.c, "Millennial MMAdView Interstitial request is caching.");
    }
}
