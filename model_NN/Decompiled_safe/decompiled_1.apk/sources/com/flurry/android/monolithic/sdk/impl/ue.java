package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
public class ue extends wv<Class<?>> {
    public ue() {
        super(Class.class);
    }

    /* renamed from: b */
    public Class<?> a(ow owVar, qm qmVar) throws IOException, oz {
        pb e = owVar.e();
        if (e == pb.VALUE_STRING) {
            String k = owVar.k();
            if (k.indexOf(46) < 0) {
                if ("int".equals(k)) {
                    return Integer.TYPE;
                }
                if ("long".equals(k)) {
                    return Long.TYPE;
                }
                if ("float".equals(k)) {
                    return Float.TYPE;
                }
                if ("double".equals(k)) {
                    return Double.TYPE;
                }
                if ("boolean".equals(k)) {
                    return Boolean.TYPE;
                }
                if ("byte".equals(k)) {
                    return Byte.TYPE;
                }
                if ("char".equals(k)) {
                    return Character.TYPE;
                }
                if ("short".equals(k)) {
                    return Short.TYPE;
                }
                if ("void".equals(k)) {
                    return Void.TYPE;
                }
            }
            try {
                return Class.forName(owVar.k());
            } catch (ClassNotFoundException e2) {
                throw qmVar.a(this.q, e2);
            }
        } else {
            throw qmVar.a(this.q, e);
        }
    }
}
