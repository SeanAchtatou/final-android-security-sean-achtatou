package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public class uw extends wv<afm> {
    public uw() {
        super(afm.class);
    }

    /* renamed from: b */
    public afm a(ow owVar, qm qmVar) throws IOException, oz {
        pb e = owVar.e();
        if (e == pb.VALUE_STRING) {
            String trim = owVar.k().trim();
            if (trim.length() == 0) {
                return (afm) c();
            }
            return qmVar.f().b(trim);
        } else if (e == pb.VALUE_EMBEDDED_OBJECT) {
            return (afm) owVar.z();
        } else {
            throw qmVar.b(this.q);
        }
    }
}
