package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.lang.reflect.Method;

public final class tc extends sw {
    protected final xl i;
    protected final Method j;

    public tc(String str, afm afm, rw rwVar, ado ado, xl xlVar) {
        super(str, afm, rwVar, ado);
        this.i = xlVar;
        this.j = xlVar.a();
    }

    protected tc(tc tcVar, qu<Object> quVar) {
        super(tcVar, quVar);
        this.i = tcVar.i;
        this.j = tcVar.j;
    }

    /* renamed from: b */
    public tc a(qu<Object> quVar) {
        return new tc(this, quVar);
    }

    public xk b() {
        return this.i;
    }

    public final void a(ow owVar, qm qmVar, Object obj) throws IOException, oz {
        if (owVar.e() != pb.VALUE_NULL) {
            try {
                Object invoke = this.j.invoke(obj, new Object[0]);
                if (invoke == null) {
                    throw new qw("Problem deserializing 'setterless' property '" + c() + "': get method returned null");
                }
                this.d.a(owVar, qmVar, invoke);
            } catch (Exception e) {
                a(e);
            }
        }
    }

    public final void a(Object obj, Object obj2) throws IOException {
        throw new UnsupportedOperationException("Should never call 'set' on setterless property");
    }
}
