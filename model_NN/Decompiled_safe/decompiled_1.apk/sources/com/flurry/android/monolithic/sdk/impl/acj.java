package com.flurry.android.monolithic.sdk.impl;

import java.io.File;
import java.net.URI;
import java.net.URL;
import java.util.Collection;
import java.util.Currency;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;

public class acj implements ael<Map.Entry<Class<?>, Object>> {
    public Collection<Map.Entry<Class<?>, Object>> a() {
        Class<Locale> cls = Locale.class;
        HashMap hashMap = new HashMap();
        acy acy = acy.a;
        hashMap.put(URL.class, acy);
        hashMap.put(URI.class, acy);
        hashMap.put(Currency.class, acy);
        hashMap.put(UUID.class, acy);
        hashMap.put(Pattern.class, acy);
        Class<Locale> cls2 = Locale.class;
        hashMap.put(cls, acy);
        Class<Locale> cls3 = Locale.class;
        hashMap.put(cls, acy);
        hashMap.put(AtomicReference.class, acn.class);
        hashMap.put(AtomicBoolean.class, ack.class);
        hashMap.put(AtomicInteger.class, acl.class);
        hashMap.put(AtomicLong.class, acm.class);
        hashMap.put(File.class, acp.class);
        hashMap.put(Class.class, aco.class);
        hashMap.put(Void.TYPE, abn.class);
        return hashMap.entrySet();
    }
}
