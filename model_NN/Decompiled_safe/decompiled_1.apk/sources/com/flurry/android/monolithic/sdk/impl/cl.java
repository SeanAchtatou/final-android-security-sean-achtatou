package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import android.view.ViewGroup;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;

public abstract class cl {
    private final AdUnit a;
    private final String b;

    public abstract void a(Context context, ViewGroup viewGroup);

    public cl(AdUnit adUnit) {
        this.a = adUnit;
        this.b = adUnit != null ? adUnit.b().toString() : null;
    }

    public AdUnit b() {
        return this.a;
    }

    public String c() {
        return this.b;
    }
}
