package com.flurry.android.monolithic.sdk.impl;

import java.lang.reflect.Array;

final class aec {
    final /* synthetic */ Object a;
    final /* synthetic */ int b;

    aec(Object obj, int i) {
        this.a = obj;
        this.b = i;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.a.getClass()) {
            return false;
        }
        if (Array.getLength(obj) != this.b) {
            return false;
        }
        for (int i = 0; i < this.b; i++) {
            Object obj2 = Array.get(this.a, i);
            Object obj3 = Array.get(obj, i);
            if (obj2 != obj3 && obj2 != null && !obj2.equals(obj3)) {
                return false;
            }
        }
        return true;
    }
}
