package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

final class zx extends ra<Object> {
    protected final rx a;
    protected final ra<Object> b;

    public zx(rx rxVar, ra<Object> raVar) {
        this.a = rxVar;
        this.b = raVar;
    }

    public void a(Object obj, or orVar, ru ruVar) throws IOException, oz {
        this.b.a(obj, orVar, ruVar, this.a);
    }

    public void a(Object obj, or orVar, ru ruVar, rx rxVar) throws IOException, oz {
        this.b.a(obj, orVar, ruVar, rxVar);
    }

    public Class<Object> c() {
        return Object.class;
    }
}
