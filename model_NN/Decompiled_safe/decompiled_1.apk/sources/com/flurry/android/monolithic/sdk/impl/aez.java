package com.flurry.android.monolithic.sdk.impl;

import java.math.BigDecimal;
import java.math.BigInteger;

public class aez {
    public static final aez a = new aez();

    protected aez() {
    }

    public aes a(boolean z) {
        return z ? aes.r() : aes.s();
    }

    public afb a() {
        return afb.r();
    }

    public afc a(int i) {
        return aey.a(i);
    }

    public afc a(long j) {
        return afa.a(j);
    }

    public afc a(BigInteger bigInteger) {
        return aeq.a(bigInteger);
    }

    public afc a(double d) {
        return aex.b(d);
    }

    public afc a(BigDecimal bigDecimal) {
        return aew.a(bigDecimal);
    }

    public aff a(String str) {
        return aff.b(str);
    }

    public aer a(byte[] bArr) {
        return aer.a(bArr);
    }

    public aeo b() {
        return new aeo(this);
    }

    public afd c() {
        return new afd(this);
    }

    public afe a(Object obj) {
        return new afe(obj);
    }
}
