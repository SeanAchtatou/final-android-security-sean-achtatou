package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.Locale;

public class uq extends um<Locale> {
    public uq() {
        super(Locale.class);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public Locale a(String str, qm qmVar) throws IOException {
        int indexOf = str.indexOf(95);
        if (indexOf < 0) {
            return new Locale(str);
        }
        String substring = str.substring(0, indexOf);
        String substring2 = str.substring(indexOf + 1);
        int indexOf2 = substring2.indexOf(95);
        if (indexOf2 < 0) {
            return new Locale(substring, substring2);
        }
        return new Locale(substring, substring2.substring(0, indexOf2), substring2.substring(indexOf2 + 1));
    }
}
