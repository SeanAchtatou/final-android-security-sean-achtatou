package com.flurry.android.monolithic.sdk.impl;

public class afn {
    protected final byte[][] a = new byte[afo.values().length][];
    protected final char[][] b = new char[afp.values().length][];

    public final byte[] a(afo afo) {
        int ordinal = afo.ordinal();
        byte[] bArr = this.a[ordinal];
        if (bArr == null) {
            return a(afo.a(afo));
        }
        this.a[ordinal] = null;
        return bArr;
    }

    public final char[] a(afp afp) {
        return a(afp, 0);
    }

    public final char[] a(afp afp, int i) {
        int i2;
        if (afp.a(afp) > i) {
            i2 = afp.a(afp);
        } else {
            i2 = i;
        }
        int ordinal = afp.ordinal();
        char[] cArr = this.b[ordinal];
        if (cArr == null || cArr.length < i2) {
            return b(i2);
        }
        this.b[ordinal] = null;
        return cArr;
    }

    public final void a(afp afp, char[] cArr) {
        this.b[afp.ordinal()] = cArr;
    }

    private final byte[] a(int i) {
        return new byte[i];
    }

    private final char[] b(int i) {
        return new char[i];
    }
}
