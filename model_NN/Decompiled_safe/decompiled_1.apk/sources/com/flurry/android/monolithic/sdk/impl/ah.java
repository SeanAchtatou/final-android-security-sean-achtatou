package com.flurry.android.monolithic.sdk.impl;

import android.view.ViewGroup;
import android.widget.AbsoluteLayout;
import com.flurry.android.impl.ads.avro.protocol.v6.AdSpaceLayout;

final class ah extends aj {
    ah() {
        super();
    }

    public ViewGroup.LayoutParams a(AdSpaceLayout adSpaceLayout) {
        ja.a(5, af.a, "AbsoluteLayout is deprecated, please consider to use FrameLayout or RelativeLayout for banner ad container view");
        return new AbsoluteLayout.LayoutParams(b(adSpaceLayout), c(adSpaceLayout), 0, 0);
    }
}
