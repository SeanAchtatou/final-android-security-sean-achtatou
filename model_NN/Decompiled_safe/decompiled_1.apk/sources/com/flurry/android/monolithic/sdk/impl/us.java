package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.TimeZone;

public class us extends um<TimeZone> {
    public us() {
        super(TimeZone.class);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public TimeZone a(String str, qm qmVar) throws IOException {
        return TimeZone.getTimeZone(str);
    }
}
