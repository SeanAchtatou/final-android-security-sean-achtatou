package com.flurry.android.monolithic.sdk.impl;

import com.flurry.org.codehaus.jackson.annotate.JacksonAnnotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.FIELD, ElementType.TYPE, ElementType.PARAMETER})
@JacksonAnnotation
@Retention(RetentionPolicy.RUNTIME)
public @interface sb {
    Class<? extends qu<?>> a() default qv.class;

    Class<? extends qu<?>> b() default qv.class;

    Class<? extends rc> c() default rd.class;

    Class<?> d() default sl.class;

    Class<?> e() default sl.class;

    Class<?> f() default sl.class;
}
