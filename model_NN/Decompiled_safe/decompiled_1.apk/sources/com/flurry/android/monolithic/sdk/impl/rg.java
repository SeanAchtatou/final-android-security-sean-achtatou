package com.flurry.android.monolithic.sdk.impl;

import java.text.DateFormat;

public class rg {
    protected final qf<? extends qb> a;
    protected final py b;
    protected final ye<?> c;
    protected final rl d;
    protected final adk e;
    protected final yj<?> f;
    protected final DateFormat g;
    protected final qs h;

    public rg(qf<? extends qb> qfVar, py pyVar, ye<?> yeVar, rl rlVar, adk adk, yj<?> yjVar, DateFormat dateFormat, qs qsVar) {
        this.a = qfVar;
        this.b = pyVar;
        this.c = yeVar;
        this.d = rlVar;
        this.e = adk;
        this.f = yjVar;
        this.g = dateFormat;
        this.h = qsVar;
    }

    public qf<? extends qb> a() {
        return this.a;
    }

    public py b() {
        return this.b;
    }

    public ye<?> c() {
        return this.c;
    }

    public rl d() {
        return this.d;
    }

    public adk e() {
        return this.e;
    }

    public yj<?> f() {
        return this.f;
    }

    public DateFormat g() {
        return this.g;
    }

    public qs h() {
        return this.h;
    }
}
