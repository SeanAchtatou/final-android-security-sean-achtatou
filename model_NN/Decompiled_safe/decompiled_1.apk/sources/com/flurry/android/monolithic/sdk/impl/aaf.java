package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;

@rz
public final class aaf extends abq<Number> {
    public static final aaf a = new aaf();

    public aaf() {
        super(Number.class);
    }

    public void a(Number number, or orVar, ru ruVar) throws IOException, oq {
        if (number instanceof BigDecimal) {
            orVar.a((BigDecimal) number);
        } else if (number instanceof BigInteger) {
            orVar.a((BigInteger) number);
        } else if (number instanceof Integer) {
            orVar.b(number.intValue());
        } else if (number instanceof Long) {
            orVar.a(number.longValue());
        } else if (number instanceof Double) {
            orVar.a(number.doubleValue());
        } else if (number instanceof Float) {
            orVar.a(number.floatValue());
        } else if ((number instanceof Byte) || (number instanceof Short)) {
            orVar.b(number.intValue());
        } else {
            orVar.e(number.toString());
        }
    }
}
