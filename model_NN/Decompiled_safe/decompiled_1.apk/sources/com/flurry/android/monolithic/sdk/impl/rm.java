package com.flurry.android.monolithic.sdk.impl;

public class rm extends rn {
    public String a(String str) {
        char c;
        int i;
        boolean z;
        int i2;
        if (str == null) {
            return str;
        }
        int length = str.length();
        StringBuilder sb = new StringBuilder(length * 2);
        boolean z2 = false;
        int i3 = 0;
        for (int i4 = 0; i4 < length; i4++) {
            char charAt = str.charAt(i4);
            if (i4 > 0 || charAt != '_') {
                if (Character.isUpperCase(charAt)) {
                    if (z2 || i3 <= 0 || sb.charAt(i3 - 1) == '_') {
                        i2 = i3;
                    } else {
                        sb.append('_');
                        i2 = i3 + 1;
                    }
                    char lowerCase = Character.toLowerCase(charAt);
                    i = i2;
                    c = lowerCase;
                    z = true;
                } else {
                    c = charAt;
                    i = i3;
                    z = false;
                }
                sb.append(c);
                boolean z3 = z;
                i3 = i + 1;
                z2 = z3;
            }
        }
        return i3 > 0 ? sb.toString() : str;
    }
}
