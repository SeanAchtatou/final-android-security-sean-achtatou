package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public class oz extends IOException {
    protected ot a;

    protected oz(String str, ot otVar, Throwable th) {
        super(str);
        if (th != null) {
            initCause(th);
        }
        this.a = otVar;
    }

    protected oz(String str) {
        super(str);
    }

    protected oz(String str, ot otVar) {
        this(str, otVar, null);
    }

    protected oz(String str, Throwable th) {
        this(str, null, th);
    }

    public ot a() {
        return this.a;
    }

    public String getMessage() {
        String message = super.getMessage();
        if (message == null) {
            message = "N/A";
        }
        ot a2 = a();
        if (a2 == null) {
            return message;
        }
        return message + 10 + " at " + a2.toString();
    }

    public String toString() {
        return getClass().getName() + ": " + getMessage();
    }
}
