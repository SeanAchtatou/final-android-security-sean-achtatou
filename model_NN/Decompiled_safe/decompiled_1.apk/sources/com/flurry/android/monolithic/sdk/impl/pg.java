package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public abstract class pg extends or {
    protected pc b;
    protected int c;
    protected boolean d;
    protected pl e = pl.g();
    protected boolean f;

    /* access modifiers changed from: protected */
    public abstract void h(String str) throws IOException, oq;

    protected pg(int i, pc pcVar) {
        this.c = i;
        this.b = pcVar;
        this.d = a(os.WRITE_NUMBERS_AS_STRINGS);
    }

    public final boolean a(os osVar) {
        return (this.c & osVar.c()) != 0;
    }

    public or a() {
        return a((pd) new afs());
    }

    public final pl h() {
        return this.e;
    }

    public void b() throws IOException, oq {
        h("start an array");
        this.e = this.e.h();
        if (this.a != null) {
            this.a.e(this);
        } else {
            i();
        }
    }

    /* access modifiers changed from: protected */
    @Deprecated
    public void i() throws IOException, oq {
    }

    public void c() throws IOException, oq {
        if (!this.e.a()) {
            i("Current context not an ARRAY but " + this.e.d());
        }
        if (this.a != null) {
            this.a.b(this, this.e.e());
        } else {
            j();
        }
        this.e = this.e.j();
    }

    /* access modifiers changed from: protected */
    @Deprecated
    public void j() throws IOException, oq {
    }

    public void d() throws IOException, oq {
        h("start an object");
        this.e = this.e.i();
        if (this.a != null) {
            this.a.b(this);
        } else {
            k();
        }
    }

    /* access modifiers changed from: protected */
    @Deprecated
    public void k() throws IOException, oq {
    }

    public void e() throws IOException, oq {
        if (!this.e.c()) {
            i("Current context not an object but " + this.e.d());
        }
        this.e = this.e.j();
        if (this.a != null) {
            this.a.a(this, this.e.e());
        } else {
            l();
        }
    }

    /* access modifiers changed from: protected */
    @Deprecated
    public void l() throws IOException, oq {
    }

    public void d(String str) throws IOException, oq {
        h("write raw value");
        c(str);
    }

    public void a(Object obj) throws IOException, oz {
        if (obj == null) {
            f();
        } else if (this.b != null) {
            this.b.a(this, obj);
        } else {
            b(obj);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.pc.a(com.flurry.android.monolithic.sdk.impl.or, com.flurry.android.monolithic.sdk.impl.ou):void
     arg types: [com.flurry.android.monolithic.sdk.impl.pg, com.flurry.android.monolithic.sdk.impl.ou]
     candidates:
      com.flurry.android.monolithic.sdk.impl.pc.a(com.flurry.android.monolithic.sdk.impl.or, java.lang.Object):void
      com.flurry.android.monolithic.sdk.impl.pc.a(com.flurry.android.monolithic.sdk.impl.or, com.flurry.android.monolithic.sdk.impl.ou):void */
    public void a(ou ouVar) throws IOException, oz {
        if (ouVar == null) {
            f();
        } else if (this.b == null) {
            throw new IllegalStateException("No ObjectCodec defined for the generator, can not serialize JsonNode-based trees");
        } else {
            this.b.a((or) this, ouVar);
        }
    }

    public void close() throws IOException {
        this.f = true;
    }

    /* access modifiers changed from: protected */
    public void i(String str) throws oq {
        throw new oq(str);
    }

    /* access modifiers changed from: protected */
    public void m() {
        throw new RuntimeException("Internal error: should never end up through this code path");
    }

    /* access modifiers changed from: protected */
    public void b(Object obj) throws IOException, oq {
        if (obj == null) {
            f();
        } else if (obj instanceof String) {
            b((String) obj);
        } else {
            if (obj instanceof Number) {
                Number number = (Number) obj;
                if (number instanceof Integer) {
                    b(number.intValue());
                    return;
                } else if (number instanceof Long) {
                    a(number.longValue());
                    return;
                } else if (number instanceof Double) {
                    a(number.doubleValue());
                    return;
                } else if (number instanceof Float) {
                    a(number.floatValue());
                    return;
                } else if (number instanceof Short) {
                    b(number.shortValue());
                    return;
                } else if (number instanceof Byte) {
                    b(number.byteValue());
                    return;
                } else if (number instanceof BigInteger) {
                    a((BigInteger) number);
                    return;
                } else if (number instanceof BigDecimal) {
                    a((BigDecimal) number);
                    return;
                } else if (number instanceof AtomicInteger) {
                    b(((AtomicInteger) number).get());
                    return;
                } else if (number instanceof AtomicLong) {
                    a(((AtomicLong) number).get());
                    return;
                }
            } else if (obj instanceof byte[]) {
                a((byte[]) obj);
                return;
            } else if (obj instanceof Boolean) {
                a(((Boolean) obj).booleanValue());
                return;
            } else if (obj instanceof AtomicBoolean) {
                a(((AtomicBoolean) obj).get());
                return;
            }
            throw new IllegalStateException("No ObjectCodec defined for the generator, can only serialize simple wrapper types (type passed " + obj.getClass().getName() + ")");
        }
    }
}
