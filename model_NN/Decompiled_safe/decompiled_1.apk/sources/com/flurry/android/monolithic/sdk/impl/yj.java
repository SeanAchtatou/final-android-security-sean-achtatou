package com.flurry.android.monolithic.sdk.impl;

import com.flurry.android.monolithic.sdk.impl.yj;
import com.flurry.org.codehaus.jackson.annotate.JsonTypeInfo;
import java.util.Collection;

public interface yj<T extends yj<T>> {
    rw a(qk qkVar, afm afm, Collection<yg> collection, qc qcVar);

    rx a(rq rqVar, afm afm, Collection<yg> collection, qc qcVar);

    T a(JsonTypeInfo.As as);

    T a(JsonTypeInfo.Id id, yi yiVar);

    T a(Class<?> cls);

    T a(String str);

    Class<?> a();
}
