package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;

public class gg extends gf {
    public gg(Context context, String str) {
        this.a = gd.a(context, str);
        this.c = "PendingOperationsDB";
        this.b = "pendingDataTable";
    }

    public hk b(String str) {
        hk hkVar;
        hk hkVar2 = null;
        Cursor query = this.a.getReadableDatabase().query(this.b, null, "objectsId='" + str + "'", null, null, null, "objectsId ASC, julianday(timestamp)");
        if (query.moveToFirst()) {
            int columnIndex = query.getColumnIndex("objectsId");
            int columnIndex2 = query.getColumnIndex("collectionName");
            int columnIndex3 = query.getColumnIndex("key");
            int columnIndex4 = query.getColumnIndex("value");
            do {
                String string = query.getString(columnIndex);
                String string2 = query.getString(columnIndex2);
                String string3 = query.getString(columnIndex3);
                String string4 = query.getString(columnIndex4);
                if (hkVar2 == null) {
                    if (TextUtils.isEmpty(string2)) {
                        hkVar2 = new hk(string);
                    } else {
                        hkVar2 = new hk(string, string2);
                    }
                }
                hkVar2.a(string3, string4);
            } while (query.moveToNext());
            hkVar = hkVar2;
        } else {
            hkVar = null;
        }
        query.close();
        return hkVar;
    }
}
