package com.flurry.android.monolithic.sdk.impl;

import com.ideaworks3d.marmalade.s3eAndroidGooglePlayBilling.util.IabHelper;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

public class la<D> {
    private final kq a;
    private ji b;

    protected la(kq kqVar) {
        this.a = kqVar;
    }

    protected la(ji jiVar, kq kqVar) {
        this(kqVar);
        a(jiVar);
    }

    public void a(ji jiVar) {
        this.b = jiVar;
    }

    public void a(Object obj, mc mcVar) throws IOException {
        a(this.b, obj, mcVar);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public void a(ji jiVar, Object obj, mc mcVar) throws IOException {
        try {
            switch (lb.a[jiVar.a().ordinal()]) {
                case 1:
                    b(jiVar, obj, mcVar);
                    return;
                case 2:
                    c(jiVar, obj, mcVar);
                    return;
                case 3:
                    d(jiVar, obj, mcVar);
                    return;
                case 4:
                    e(jiVar, obj, mcVar);
                    return;
                case 5:
                    int a2 = a(jiVar, obj);
                    mcVar.b(a2);
                    a(jiVar.k().get(a2), obj, mcVar);
                    return;
                case 6:
                    g(jiVar, obj, mcVar);
                    return;
                case IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED /*7*/:
                    f(jiVar, obj, mcVar);
                    return;
                case IabHelper.BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED /*8*/:
                    c(obj, mcVar);
                    return;
                case 9:
                    mcVar.c(((Number) obj).intValue());
                    return;
                case 10:
                    mcVar.b(((Long) obj).longValue());
                    return;
                case 11:
                    mcVar.a(((Float) obj).floatValue());
                    return;
                case 12:
                    mcVar.a(((Double) obj).doubleValue());
                    return;
                case 13:
                    mcVar.a(((Boolean) obj).booleanValue());
                    return;
                case 14:
                    mcVar.a();
                    return;
                default:
                    b(jiVar, obj);
                    return;
            }
        } catch (NullPointerException e) {
            throw a(e, " of " + jiVar.g());
        }
        throw a(e, " of " + jiVar.g());
    }

    /* access modifiers changed from: protected */
    public NullPointerException a(NullPointerException nullPointerException, String str) {
        NullPointerException nullPointerException2 = new NullPointerException(nullPointerException.getMessage() + str);
        nullPointerException2.initCause(nullPointerException.getCause() == null ? nullPointerException : nullPointerException.getCause());
        return nullPointerException2;
    }

    /* access modifiers changed from: protected */
    public void b(ji jiVar, Object obj, mc mcVar) throws IOException {
        Object a2 = this.a.a(obj, jiVar);
        for (js next : jiVar.b()) {
            try {
                a(next.c(), this.a.b(obj, next.a(), next.b(), a2), mcVar);
            } catch (NullPointerException e) {
                throw a(e, " in field " + next.a());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void c(ji jiVar, Object obj, mc mcVar) throws IOException {
        mcVar.a(jiVar.c(obj.toString()));
    }

    /* access modifiers changed from: protected */
    public void d(ji jiVar, Object obj, mc mcVar) throws IOException {
        ji i = jiVar.i();
        long a2 = a(obj);
        mcVar.b();
        mcVar.a(a2);
        Iterator<? extends Object> b2 = b(obj);
        while (b2.hasNext()) {
            mcVar.c();
            a(i, b2.next(), mcVar);
        }
        mcVar.d();
    }

    /* access modifiers changed from: protected */
    public int a(ji jiVar, Object obj) {
        return this.a.a(jiVar, obj);
    }

    /* access modifiers changed from: protected */
    public long a(Object obj) {
        return (long) ((Collection) obj).size();
    }

    /* access modifiers changed from: protected */
    public Iterator<? extends Object> b(Object obj) {
        return ((Collection) obj).iterator();
    }

    /* access modifiers changed from: protected */
    public void e(ji jiVar, Object obj, mc mcVar) throws IOException {
        ji j = jiVar.j();
        int c = c(obj);
        mcVar.e();
        mcVar.a((long) c);
        for (Map.Entry next : d(obj)) {
            mcVar.c();
            b(next.getKey(), mcVar);
            a(j, next.getValue(), mcVar);
        }
        mcVar.f();
    }

    /* access modifiers changed from: protected */
    public int c(Object obj) {
        return ((Map) obj).size();
    }

    /* access modifiers changed from: protected */
    public Iterable<Map.Entry<Object, Object>> d(Object obj) {
        return ((Map) obj).entrySet();
    }

    /* access modifiers changed from: protected */
    public void f(ji jiVar, Object obj, mc mcVar) throws IOException {
        b(obj, mcVar);
    }

    /* access modifiers changed from: protected */
    public void b(Object obj, mc mcVar) throws IOException {
        mcVar.a((CharSequence) obj);
    }

    /* access modifiers changed from: protected */
    public void c(Object obj, mc mcVar) throws IOException {
        mcVar.a((ByteBuffer) obj);
    }

    /* access modifiers changed from: protected */
    public void g(ji jiVar, Object obj, mc mcVar) throws IOException {
        mcVar.b(((ld) obj).b(), 0, jiVar.l());
    }

    private void b(ji jiVar, Object obj) {
        throw new jh("Not a " + jiVar + ": " + obj);
    }
}
