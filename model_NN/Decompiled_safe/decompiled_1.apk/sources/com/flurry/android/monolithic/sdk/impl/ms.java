package com.flurry.android.monolithic.sdk.impl;

import java.util.List;
import java.util.Map;

public class ms extends mq {
    public final String[] A;
    public final mq[] z;

    private ms(mq[] mqVarArr, String[] strArr) {
        super(na.ALTERNATIVE);
        this.z = mqVarArr;
        this.A = strArr;
    }

    public mq a(int i) {
        return this.z[i];
    }

    /* renamed from: b */
    public ms a(Map<ne, ne> map, Map<ne, List<mx>> map2) {
        mq[] mqVarArr = new mq[this.z.length];
        for (int i = 0; i < mqVarArr.length; i++) {
            mqVarArr[i] = this.z[i].a(map, map2);
        }
        return new ms(mqVarArr, this.A);
    }
}
