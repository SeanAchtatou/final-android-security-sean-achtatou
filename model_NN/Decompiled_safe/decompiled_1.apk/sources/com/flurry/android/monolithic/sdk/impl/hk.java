package com.flurry.android.monolithic.sdk.impl;

import java.util.Vector;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class hk {
    protected String a = "";
    protected String b = "";
    protected Vector<NameValuePair> c = new Vector<>();
    hl d = hl.NONE;
    private int e = 31;

    public hk(String str) {
        this.a = str;
        this.d = hl.USER;
    }

    public hk(String str, String str2) {
        this.a = str;
        this.b = str2;
        this.d = hl.OBJECT;
    }

    public void a(String str, String str2) {
        this.c.add(new BasicNameValuePair(str, str2));
    }

    public void a(Vector<NameValuePair> vector) {
        this.c = vector;
    }

    public Vector<NameValuePair> a() {
        return this.c;
    }

    public String b() {
        return this.b;
    }

    public String c() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        hm hmVar = (hm) obj;
        if (!hmVar.a.equals(this.a) || !hmVar.b.equals(this.b) || !hmVar.c.equals(this.c)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (((((this.e * this.e) + this.a.hashCode()) * this.e) + this.b.hashCode()) * this.e) + this.c.hashCode();
    }
}
