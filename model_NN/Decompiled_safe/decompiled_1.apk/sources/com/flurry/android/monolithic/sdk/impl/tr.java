package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;

public final class tr {
    protected final th a;
    protected final HashMap<String, sw> b = new HashMap<>();
    protected Object[] c;
    protected final sw[] d;

    public tr(th thVar) {
        sw[] swVarArr = null;
        this.a = thVar;
        sw[] k = thVar.k();
        int length = k.length;
        Object[] objArr = null;
        for (int i = 0; i < length; i++) {
            sw swVar = k[i];
            this.b.put(swVar.c(), swVar);
            if (swVar.a().t()) {
                objArr = objArr == null ? new Object[length] : objArr;
                objArr[i] = adz.f(swVar.a().p());
            }
            if (swVar.k() != null) {
                swVarArr = swVarArr == null ? new sw[length] : swVarArr;
                swVarArr[i] = swVar;
            }
        }
        this.c = objArr;
        this.d = swVarArr;
    }

    public Collection<sw> a() {
        return this.b.values();
    }

    public sw a(String str) {
        return this.b.get(str);
    }

    public void a(sw swVar, qu<Object> quVar) {
        sw a2 = swVar.a(quVar);
        this.b.put(a2.c(), a2);
        Object b2 = quVar.b();
        if (b2 != null) {
            if (this.c == null) {
                this.c = new Object[this.b.size()];
            }
            this.c[a2.j()] = b2;
        }
    }

    public tw a(ow owVar, qm qmVar) {
        tw twVar = new tw(owVar, qmVar, this.b.size());
        if (this.d != null) {
            twVar.a(this.d);
        }
        return twVar;
    }

    public Object a(tw twVar) throws IOException {
        Object a2 = this.a.a(twVar.a(this.c));
        for (ts a3 = twVar.a(); a3 != null; a3 = a3.a) {
            a3.a(a2);
        }
        return a2;
    }
}
