package com.flurry.android.monolithic.sdk.impl;

final class wg extends we {
    wg() {
        super(Byte.class);
    }

    /* renamed from: c */
    public Byte b(String str, qm qmVar) throws qw {
        int a = a(str);
        if (a >= -128 && a <= 127) {
            return Byte.valueOf((byte) a);
        }
        throw qmVar.a(this.a, str, "overflow, value can not be represented as 8-bit value");
    }
}
