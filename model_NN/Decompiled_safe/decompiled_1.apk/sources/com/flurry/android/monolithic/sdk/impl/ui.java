package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@sa
public class ui extends wv<Enum<?>> {
    protected final aed<?> a;

    public ui(aed<?> aed) {
        super(Enum.class);
        this.a = aed;
    }

    public static qu<?> a(qk qkVar, Class<?> cls, xl xlVar) {
        if (xlVar.b(0) != String.class) {
            throw new IllegalArgumentException("Parameter #0 type for factory method (" + xlVar + ") not suitable, must be java.lang.String");
        }
        if (qkVar.a(ql.CAN_OVERRIDE_ACCESS_MODIFIERS)) {
            adz.a(xlVar.i());
        }
        return new uj(cls, xlVar);
    }

    /* renamed from: b */
    public Enum<?> a(ow owVar, qm qmVar) throws IOException, oz {
        Enum<?> enumR;
        pb e = owVar.e();
        if (e == pb.VALUE_STRING || e == pb.FIELD_NAME) {
            enumR = this.a.a(owVar.k());
            if (enumR == null) {
                throw qmVar.b(this.a.a(), "value not one of declared Enum instance names");
            }
        } else if (e != pb.VALUE_NUMBER_INT) {
            throw qmVar.b(this.a.a());
        } else if (qmVar.a(ql.FAIL_ON_NUMBERS_FOR_ENUMS)) {
            throw qmVar.b("Not allowed to deserialize Enum value out of JSON number (disable DeserializationConfig.Feature.FAIL_ON_NUMBERS_FOR_ENUMS to allow)");
        } else {
            enumR = this.a.a(owVar.t());
            if (enumR == null) {
                throw qmVar.c(this.a.a(), "index value outside legal index range [0.." + this.a.b() + "]");
            }
        }
        return enumR;
    }
}
