package com.flurry.android.monolithic.sdk.impl;

import java.util.LinkedHashMap;
import java.util.Map;

public final class afv extends LinkedHashMap<String, String> {
    public static final afv a = new afv();

    private afv() {
        super(192, 0.8f, true);
    }

    /* access modifiers changed from: protected */
    public boolean removeEldestEntry(Map.Entry<String, String> entry) {
        return size() > 192;
    }

    public synchronized String a(String str) {
        String str2;
        str2 = (String) get(str);
        if (str2 == null) {
            str2 = str.intern();
            put(str2, str2);
        }
        return str2;
    }
}
