package com.flurry.android.monolithic.sdk.impl;

public enum ox {
    AUTO_CLOSE_SOURCE(true),
    ALLOW_COMMENTS(false),
    ALLOW_UNQUOTED_FIELD_NAMES(false),
    ALLOW_SINGLE_QUOTES(false),
    ALLOW_UNQUOTED_CONTROL_CHARS(false),
    ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER(false),
    ALLOW_NUMERIC_LEADING_ZEROS(false),
    ALLOW_NON_NUMERIC_NUMBERS(false),
    INTERN_FIELD_NAMES(true),
    CANONICALIZE_FIELD_NAMES(true);
    
    final boolean k;

    public static int a() {
        int i = 0;
        for (ox oxVar : values()) {
            if (oxVar.b()) {
                i |= oxVar.c();
            }
        }
        return i;
    }

    private ox(boolean z) {
        this.k = z;
    }

    public boolean b() {
        return this.k;
    }

    public int c() {
        return 1 << ordinal();
    }
}
