package com.flurry.android.monolithic.sdk.impl;

public final class afh {
    final afh a = null;
    final boolean b;
    private int c;
    private int d;
    private int[] e;
    private afl[] f;
    private afi[] g;
    private int h;
    private transient boolean i;
    private boolean j;
    private boolean k;
    private boolean l;

    public static afh a() {
        return new afh(64, true);
    }

    private afh(int i2, boolean z) {
        int i3;
        this.b = z;
        if (i2 < 16) {
            i3 = 16;
        } else if (((i2 - 1) & i2) != 0) {
            i3 = 16;
            while (i3 < i2) {
                i3 += i3;
            }
        } else {
            i3 = i2;
        }
        a(i3);
    }

    private void a(int i2) {
        this.c = 0;
        this.e = new int[i2];
        this.f = new afl[i2];
        this.j = false;
        this.k = false;
        this.d = i2 - 1;
        this.l = true;
        this.g = null;
        this.h = 0;
        this.i = false;
    }
}
