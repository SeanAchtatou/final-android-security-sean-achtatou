package com.flurry.android.monolithic.sdk.impl;

import android.view.ViewGroup;
import com.flurry.android.FlurryAdSize;
import com.flurry.android.impl.ads.FlurryAdModule;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class br {
    final /* synthetic */ FlurryAdModule a;
    /* access modifiers changed from: private */
    public final String b;
    /* access modifiers changed from: private */
    public final ViewGroup c;
    /* access modifiers changed from: private */
    public final boolean d;
    /* access modifiers changed from: private */
    public final FlurryAdSize e;
    /* access modifiers changed from: private */
    public final Runnable f;
    private Future<?> g;

    public br(FlurryAdModule flurryAdModule, String str, ViewGroup viewGroup, boolean z, FlurryAdSize flurryAdSize, jf jfVar) {
        this.a = flurryAdModule;
        this.b = str;
        this.c = viewGroup;
        this.d = z;
        this.e = flurryAdSize;
        this.f = jfVar;
    }

    public br a() {
        this.g = this.a.b(new bs(this));
        return this;
    }

    public void a(long j, TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        if (this.g != null) {
            this.g.get(j, timeUnit);
        }
    }

    public void a(boolean z) {
        if (this.g != null) {
            this.g.cancel(z);
        }
    }
}
