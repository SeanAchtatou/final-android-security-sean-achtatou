package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public class sm extends qu<Object> {
    protected final afm a;
    protected final boolean b;
    protected final boolean c;
    protected final boolean d;
    protected final boolean e;

    public sm(afm afm) {
        boolean z;
        boolean z2;
        boolean z3;
        this.a = afm;
        Class<?> p = afm.p();
        this.b = p.isAssignableFrom(String.class);
        if (p == Boolean.TYPE || p.isAssignableFrom(Boolean.class)) {
            z = true;
        } else {
            z = false;
        }
        this.c = z;
        if (p == Integer.TYPE || p.isAssignableFrom(Integer.class)) {
            z2 = true;
        } else {
            z2 = false;
        }
        this.d = z2;
        if (p == Double.TYPE || p.isAssignableFrom(Double.class)) {
            z3 = true;
        } else {
            z3 = false;
        }
        this.e = z3;
    }

    public Object a(ow owVar, qm qmVar, rw rwVar) throws IOException, oz {
        Object b2 = b(owVar, qmVar);
        return b2 != null ? b2 : rwVar.a(owVar, qmVar);
    }

    public Object a(ow owVar, qm qmVar) throws IOException, oz {
        throw qmVar.a(this.a.p(), "abstract types can only be instantiated with additional type information");
    }

    /* access modifiers changed from: protected */
    public Object b(ow owVar, qm qmVar) throws IOException, oz {
        switch (sn.a[owVar.e().ordinal()]) {
            case 1:
                if (this.b) {
                    return owVar.k();
                }
                break;
            case 2:
                if (this.d) {
                    return Integer.valueOf(owVar.t());
                }
                break;
            case 3:
                if (this.e) {
                    return Double.valueOf(owVar.x());
                }
                break;
            case 4:
                if (this.c) {
                    return Boolean.TRUE;
                }
                break;
            case 5:
                if (this.c) {
                    return Boolean.FALSE;
                }
                break;
        }
        return null;
    }
}
