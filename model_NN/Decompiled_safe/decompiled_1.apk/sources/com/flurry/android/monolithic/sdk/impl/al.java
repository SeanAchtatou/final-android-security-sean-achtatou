package com.flurry.android.monolithic.sdk.impl;

import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.flurry.android.impl.ads.avro.protocol.v6.AdSpaceLayout;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

final class al extends aj {
    private static final Map<String, Integer> a = a();

    al() {
        super();
    }

    public int g(AdSpaceLayout adSpaceLayout) {
        return -1;
    }

    public ViewGroup.LayoutParams a(AdSpaceLayout adSpaceLayout) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(b(adSpaceLayout), c(adSpaceLayout));
        String[] split = adSpaceLayout.f().toString().split("-");
        if (split.length == 2) {
            Integer a2 = a(split[0]);
            if (a2 != null) {
                layoutParams.addRule(a2.intValue());
            }
            Integer a3 = a(split[1]);
            if (a3 != null) {
                layoutParams.addRule(a3.intValue());
            }
        }
        return layoutParams;
    }

    private static Map<String, Integer> a() {
        HashMap hashMap = new HashMap();
        hashMap.put("b", 12);
        hashMap.put("t", 10);
        hashMap.put("m", 15);
        hashMap.put("c", 14);
        hashMap.put("l", 9);
        hashMap.put("r", 11);
        return Collections.unmodifiableMap(hashMap);
    }

    private static Integer a(String str) {
        return a.get(str);
    }
}
