package com.flurry.android.monolithic.sdk.impl;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import com.flurry.android.impl.ads.FlurryAdModule;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;
import com.millennialmedia.android.MMInterstitial;

public final class dz extends cr {
    /* access modifiers changed from: private */
    public static final String c = dz.class.getSimpleName();
    MMInterstitial b;
    /* access modifiers changed from: private */
    public boolean d;
    private final String e;

    public dz(Context context, FlurryAdModule flurryAdModule, m mVar, AdUnit adUnit, Bundle bundle) {
        super(context, flurryAdModule, mVar, adUnit);
        this.e = bundle.getString("com.flurry.millennial.MYAPIDINTERSTITIAL");
    }

    public void a() {
        this.b = new MMInterstitial((Activity) b());
        this.b.setApid(this.e);
        this.b.setListener(new ea(this));
        this.b.fetch();
        this.d = this.b.display();
        if (this.d) {
            ja.a(3, c, "Millennial MMAdView Interstitial ad displayed immediately:" + System.currentTimeMillis() + " " + this.d);
        } else {
            ja.a(3, c, "Millennial MMAdView Interstitial ad did not display immediately:" + System.currentTimeMillis() + " " + this.d);
        }
    }
}
