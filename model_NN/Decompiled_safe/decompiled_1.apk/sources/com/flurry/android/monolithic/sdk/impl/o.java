package com.flurry.android.monolithic.sdk.impl;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import com.flurry.android.AdCreative;
import com.flurry.android.FlurryFullscreenTakeoverActivity;
import com.flurry.android.impl.ads.FlurryAdModule;
import com.flurry.android.impl.ads.avro.protocol.v6.AdFrame;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@SuppressLint({"SetJavaScriptEnabled"})
public class o extends ac implements DialogInterface.OnKeyListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener, ci {
    private AlertDialog A;
    private List<AdFrame> B;
    private boolean C;
    private Map<String, AdUnit> D;
    private Map<String, m> E;
    private FlurryAdModule F;
    private ExecutorService G;
    /* access modifiers changed from: private */
    public List<String> H;
    String a = null;
    /* access modifiers changed from: private */
    public final String f = getClass().getSimpleName();
    private final String g = "mraid.js";
    /* access modifiers changed from: private */
    public ProgressDialog h;
    private cf i;
    /* access modifiers changed from: private */
    public n j;
    private boolean k;
    /* access modifiers changed from: private */
    public WebView l;
    private boolean m;
    private WebViewClient n;
    /* access modifiers changed from: private */
    public WebChromeClient o;
    /* access modifiers changed from: private */
    public View p;
    /* access modifiers changed from: private */
    public int q;
    /* access modifiers changed from: private */
    public WebChromeClient.CustomViewCallback r;
    /* access modifiers changed from: private */
    public Dialog s;
    /* access modifiers changed from: private */
    public FrameLayout t;
    private int u;
    /* access modifiers changed from: private */
    public Dialog v;
    private FrameLayout w;
    /* access modifiers changed from: private */
    public boolean x;
    /* access modifiers changed from: private */
    public boolean y;
    private boolean z;

    private synchronized void setFlurryJsEnvInitialized(boolean z2) {
        this.m = z2;
    }

    private synchronized boolean f() {
        return this.m;
    }

    /* access modifiers changed from: private */
    public synchronized void g() {
        if (!f()) {
            i();
            setFlurryJsEnvInitialized(true);
        }
    }

    /* access modifiers changed from: private */
    public synchronized void h() {
        setFlurryJsEnvInitialized(false);
    }

    private void i() {
        ja.a(3, this.f, "initializeFlurryJsEnv");
        if (this.l != null) {
            this.l.loadUrl("javascript:(function() {" + "var Hogan={};(function(Hogan,useArrayBuffer){Hogan.Template=function(renderFunc,text,compiler,options){this.r=renderFunc||this.r;this.c=compiler;this.options=options;this.text=text||\"\";this.buf=useArrayBuffer?[]:\"\"};Hogan.Template.prototype={r:function(context,partials,indent){return\"\"},v:hoganEscape,t:coerceToString,render:function render(context,partials,indent){return this.ri([context],partials||{},indent)},ri:function(context,partials,indent){return this.r(context,partials,indent)},rp:function(name,context,partials,indent){var partial=partials[name];if(!partial)return\"\";if(this.c&&typeof partial==\"string\")partial=this.c.compile(partial,this.options);return partial.ri(context,partials,indent)},rs:function(context,partials,section){var tail=context[context.length-1];if(!isArray(tail)){section(context,partials,this);return}for(var i=0;i<tail.length;i++){context.push(tail[i]);section(context,partials,this);context.pop()}},s:function(val,ctx,partials,inverted,start,end,tags){var pass;if(isArray(val)&&val.length===0)return false;if(typeof val==\"function\")val=this.ls(val,ctx,partials,inverted,start,end,tags);pass=val===\"\"||!!val;if(!inverted&&pass&&ctx)ctx.push(typeof val==\"object\"?val:ctx[ctx.length-1]);return pass},d:function(key,ctx,partials,returnFound){var names=key.split(\".\"),val=this.f(names[0],ctx,partials,returnFound),cx=null;if(key===\".\"&&isArray(ctx[ctx.length-2]))return ctx[ctx.length-1];for(var i=1;i<names.length;i++)if(val&&typeof val==\"object\"&&names[i]in val){cx=val;val=val[names[i]]}else val=\"\";if(returnFound&&!val)return false;if(!returnFound&&typeof val==\"function\"){ctx.push(cx);val=this.lv(val,ctx,partials);ctx.pop()}return val},f:function(key,ctx,partials,returnFound){var val=false,v=null,found=false;for(var i=ctx.length-1;i>=0;i--){v=ctx[i];if(v&&typeof v==\"object\"&&key in v){val=v[key];found=true;break}}if(!found)return returnFound?false:\"\";if(!returnFound&&typeof val==\"function\")val=this.lv(val,ctx,partials);return val},ho:function(val,cx,partials,text,tags){var compiler=this.c;var options=this.options;options.delimiters=tags;var t=val.call(cx,text,function(t){return compiler.compile(t,options).render(cx,partials)});this.b(compiler.compile(t.toString(),options).render(cx,partials));return false},b:useArrayBuffer?function(s){this.buf.push(s)}:function(s){this.buf+=s},fl:useArrayBuffer?function(){var r=this.buf.join(\"\");this.buf=[];return r}:function(){var r=this.buf;this.buf=\"\";return r},ls:function(val,ctx,partials,inverted,start,end,tags){var cx=ctx[ctx.length-1],t=null;if(!inverted&&this.c&&val.length>0)return this.ho(val,cx,partials,this.text.substring(start,end),tags);t=val.call(cx);if(typeof t==\"function\")if(inverted)return true;else if(this.c)return this.ho(t,cx,partials,this.text.substring(start,end),tags);return t},lv:function(val,ctx,partials){var cx=ctx[ctx.length-1];var result=val.call(cx);if(typeof result==\"function\")result=result.call(cx);result=coerceToString(result);if(this.c&&~result.indexOf(\"{{\"))return this.c.compile(result,this.options).render(cx,partials);return result}};var rAmp=/&/g,rLt=/</g,rGt=/>/g,rApos=/\\'/g,rQuot=/\\\"/g,hChars=/[&<>\\\"\\']/;function coerceToString(val){return String(val===null||val===undefined?\"\":val)}function hoganEscape(str){str=coerceToString(str);return hChars.test(str)?str.replace(rAmp,\"&amp;\").replace(rLt,\"&lt;\").replace(rGt,\"&gt;\").replace(rApos,\"&#39;\").replace(rQuot,\"&quot;\"):str}var isArray=Array.isArray||function(a){return Object.prototype.toString.call(a)===\"[object Array]\"}})(typeof exports!==\"undefined\"?exports:Hogan);(function(Hogan){var rIsWhitespace=/\\S/,rQuot=/\\\"/g,rNewline=/\\n/g,rCr=/\\r/g,rSlash=/\\\\/g,tagTypes={\"#\":1,\"^\":2,\"/\":3,\"!\":4,\">\":5,\"<\":6,\"=\":7,\"_v\":8,\"{\":9,\"&\":10};Hogan.scan=function scan(text,delimiters){var len=text.length,IN_TEXT=0,IN_TAG_TYPE=1,IN_TAG=2,state=IN_TEXT,tagType=null,tag=null,buf=\"\",tokens=[],seenTag=false,i=0,lineStart=0,otag=\"{{\",ctag=\"}}\";function addBuf(){if(buf.length>0){tokens.push(new String(buf));buf=\"\"}}function lineIsWhitespace(){var isAllWhitespace=true;for(var j=lineStart;j<tokens.length;j++){isAllWhitespace=tokens[j].tag&&tagTypes[tokens[j].tag]<tagTypes[\"_v\"]||!tokens[j].tag&&tokens[j].match(rIsWhitespace)===null;if(!isAllWhitespace)return false}return isAllWhitespace}function filterLine(haveSeenTag,noNewLine){addBuf();if(haveSeenTag&&lineIsWhitespace())for(var j=lineStart,next;j<tokens.length;j++){if(!tokens[j].tag){if((next=tokens[j+1])&&next.tag==\">\")next.indent=tokens[j].toString();tokens.splice(j,1)}}else if(!noNewLine)tokens.push({tag:\"\\n\"});seenTag=false;lineStart=tokens.length}function changeDelimiters(text,index){var close=\"=\"+ctag,closeIndex=text.indexOf(close,index),delimiters=trim(text.substring(text.indexOf(\"=\",index)+1,closeIndex)).split(\" \");otag=delimiters[0];ctag=delimiters[1];return closeIndex+close.length-1}if(delimiters){delimiters=delimiters.split(\" \");otag=delimiters[0];ctag=delimiters[1]}for(i=0;i<len;i++)if(state==IN_TEXT)if(tagChange(otag,text,i)){--i;addBuf();state=IN_TAG_TYPE}else if(text.charAt(i)==\"\\n\")filterLine(seenTag);else buf+=text.charAt(i);else if(state==IN_TAG_TYPE){i+=otag.length-1;tag=tagTypes[text.charAt(i+1)];tagType=tag?text.charAt(i+1):\"_v\";if(tagType==\"=\"){i=changeDelimiters(text,i);state=IN_TEXT}else{if(tag)i++;state=IN_TAG}seenTag=i}else if(tagChange(ctag,text,i)){tokens.push({tag:tagType,n:trim(buf),otag:otag,ctag:ctag,i:tagType==\"/\"?seenTag-ctag.length:i+otag.length});buf=\"\";i+=ctag.length-1;state=IN_TEXT;if(tagType==\"{\")if(ctag==\"}}\")i++;else cleanTripleStache(tokens[tokens.length-1])}else buf+=text.charAt(i);filterLine(seenTag,true);return tokens};function cleanTripleStache(token){if(token.n.substr(token.n.length-1)===\"}\")token.n=token.n.substring(0,token.n.length-1)}function trim(s){if(s.trim)return s.trim();return s.replace(/^\\s*|\\s*$/g,\"\")}function tagChange(tag,text,index){if(text.charAt(index)!=tag.charAt(0))return false;for(var i=1,l=tag.length;i<l;i++)if(text.charAt(index+i)!=tag.charAt(i))return false;return true}function buildTree(tokens,kind,stack,customTags){var instructions=[],opener=null,token=null;while(tokens.length>0){token=tokens.shift();if(token.tag==\"#\"||token.tag==\"^\"||isOpener(token,customTags)){stack.push(token);token.nodes=buildTree(tokens,token.tag,stack,customTags);instructions.push(token)}else if(token.tag==\"/\"){if(stack.length===0)throw new Error(\"Closing tag without opener: /\"+token.n);opener=stack.pop();if(token.n!=opener.n&&!isCloser(token.n,opener.n,customTags))throw new Error(\"Nesting error: \"+opener.n+\" vs. \"+token.n);opener.end=token.i;return instructions}else instructions.push(token)}if(stack.length>0)throw new Error(\"missing closing tag: \"+stack.pop().n);return instructions}function isOpener(token,tags){for(var i=0,l=tags.length;i<l;i++)if(tags[i].o==token.n){token.tag=\"#\";return true}}function isCloser(close,open,tags){for(var i=0,l=tags.length;i<l;i++)if(tags[i].c==close&&tags[i].o==open)return true}function writeCode(tree){return'var _=this;_.b(i=i||\"\");'+walk(tree)+\"return _.fl();\"}Hogan.generate=function(code,text,options){if(options.asString)return\"function(c,p,i){\"+code+\";}\";return new Hogan.Template(new Function(\"c\",\"p\",\"i\",code),text,Hogan,options)};function esc(s){return s.replace(rSlash,\"\\\\\\\\\").replace(rQuot,'\\\\\"').replace(rNewline,\"\\\\n\").replace(rCr,\"\\\\r\")}function chooseMethod(s){return~s.indexOf(\".\")?\"d\":\"f\"}function walk(tree){var code=\"\";for(var i=0,l=tree.length;i<l;i++){var tag=tree[i].tag;if(tag==\"#\")code+=section(tree[i].nodes,tree[i].n,chooseMethod(tree[i].n),tree[i].i,tree[i].end,tree[i].otag+\" \"+tree[i].ctag);else if(tag==\"^\")code+=invertedSection(tree[i].nodes,tree[i].n,chooseMethod(tree[i].n));else if(tag==\"<\"||tag==\">\")code+=partial(tree[i]);else if(tag==\"{\"||tag==\"&\")code+=tripleStache(tree[i].n,chooseMethod(tree[i].n));else if(tag==\"\\n\")code+=text('\"\\\\n\"'+(tree.length-1==i?\"\":\" + i\"));else if(tag==\"_v\")code+=variable(tree[i].n,chooseMethod(tree[i].n));else if(tag===undefined)code+=text('\"'+esc(tree[i])+'\"')}return code}function section(nodes,id,method,start,end,tags){return\"if(_.s(_.\"+method+'(\"'+esc(id)+'\",c,p,1),'+\"c,p,0,\"+start+\",\"+end+',\"'+tags+'\")){'+\"_.rs(c,p,\"+\"function(c,p,_){\"+walk(nodes)+\"});c.pop();}\"}function invertedSection(nodes,id,method){return\"if(!_.s(_.\"+method+'(\"'+esc(id)+'\",c,p,1),c,p,1,0,0,\"\")){'+walk(nodes)+\"};\"}function partial(tok){return'_.b(_.rp(\"'+esc(tok.n)+'\",c,p,\"'+(tok.indent||\"\")+'\"));'}function tripleStache(id,method){return\"_.b(_.t(_.\"+method+'(\"'+esc(id)+'\",c,p,0)));'}function variable(id,method){return\"_.b(_.v(_.\"+method+'(\"'+esc(id)+'\",c,p,0)));'}function text(id){return\"_.b(\"+id+\");\"}Hogan.parse=function(tokens,text,options){options=options||{};return buildTree(tokens,\"\",[],options.sectionTags||[])},Hogan.cache={};Hogan.compile=function(text,options){options=options||{};var key=text+\"||\"+!!options.asString;var t=this.cache[key];if(t)return t;t=this.generate(writeCode(this.parse(this.scan(text,options.delimiters),text,options)),text,options);return this.cache[key]=t}})(typeof exports!==\"undefined\"?exports:Hogan);" + "var flurryBridgeCtor=function(w){var flurryadapter={};flurryadapter.flurryCallQueue=[];flurryadapter.flurryCallInProgress=false;flurryadapter.callComplete=function(cmd){if(this.flurryCallQueue.length==0){this.flurryCallInProgress=false;return}var adapterCall=this.flurryCallQueue.splice(0,1)[0];this.executeNativeCall(adapterCall);return\"OK\"};flurryadapter.executeCall=function(command){var adapterCall=\"flurry://flurrycall?event=\"+command;var value;for(var i=1;i<arguments.length;i+=2){value=arguments[i+1];if(value==null)continue;adapterCall+=\"&\"+arguments[i]+\"=\"+escape(value)}if(this.flurryCallInProgress)this.flurryCallQueue.push(adapterCall);else this.executeNativeCall(adapterCall)};flurryadapter.executeNativeCall=function(adapterCall){if(adapterCall.length==0)return;this.flurryCallInProgress=true;w.location=adapterCall};return flurryadapter};" + "window.Hogan=Hogan;window.flurryadapter=flurryBridgeCtor(window);" + "window.flurryAdapterAvailable=true;if(typeof window.FlurryAdapterReady === 'function'){window.FlurryAdapterReady();}" + "})();");
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        ja.a(3, this.f, "activateFlurryJsEnv");
        String currentContent = getCurrentContent();
        if (!this.C && currentContent != null && currentContent.length() > 0 && !currentContent.equals("{}")) {
            String url = this.l.getUrl();
            String a2 = cp.a(url);
            String a3 = cp.a(a2, url);
            if (!TextUtils.isEmpty(a3) && a3 != a2) {
                ja.a(3, this.f, "content before {{mustached}} tags replacement = '" + currentContent + "'");
                currentContent = currentContent.replace(a2, a3);
                ja.a(3, this.f, "content after {{mustached}} tags replacement = '" + currentContent + "'");
            }
            StringBuilder sb = new StringBuilder();
            sb.append("javascript:");
            sb.append("(function(){");
            sb.append("if(!window.Hogan){var Hogan={};(function(Hogan,useArrayBuffer){Hogan.Template=function(renderFunc,text,compiler,options){this.r=renderFunc||this.r;this.c=compiler;this.options=options;this.text=text||\"\";this.buf=useArrayBuffer?[]:\"\"};Hogan.Template.prototype={r:function(context,partials,indent){return\"\"},v:hoganEscape,t:coerceToString,render:function render(context,partials,indent){return this.ri([context],partials||{},indent)},ri:function(context,partials,indent){return this.r(context,partials,indent)},rp:function(name,context,partials,indent){var partial=partials[name];if(!partial)return\"\";if(this.c&&typeof partial==\"string\")partial=this.c.compile(partial,this.options);return partial.ri(context,partials,indent)},rs:function(context,partials,section){var tail=context[context.length-1];if(!isArray(tail)){section(context,partials,this);return}for(var i=0;i<tail.length;i++){context.push(tail[i]);section(context,partials,this);context.pop()}},s:function(val,ctx,partials,inverted,start,end,tags){var pass;if(isArray(val)&&val.length===0)return false;if(typeof val==\"function\")val=this.ls(val,ctx,partials,inverted,start,end,tags);pass=val===\"\"||!!val;if(!inverted&&pass&&ctx)ctx.push(typeof val==\"object\"?val:ctx[ctx.length-1]);return pass},d:function(key,ctx,partials,returnFound){var names=key.split(\".\"),val=this.f(names[0],ctx,partials,returnFound),cx=null;if(key===\".\"&&isArray(ctx[ctx.length-2]))return ctx[ctx.length-1];for(var i=1;i<names.length;i++)if(val&&typeof val==\"object\"&&names[i]in val){cx=val;val=val[names[i]]}else val=\"\";if(returnFound&&!val)return false;if(!returnFound&&typeof val==\"function\"){ctx.push(cx);val=this.lv(val,ctx,partials);ctx.pop()}return val},f:function(key,ctx,partials,returnFound){var val=false,v=null,found=false;for(var i=ctx.length-1;i>=0;i--){v=ctx[i];if(v&&typeof v==\"object\"&&key in v){val=v[key];found=true;break}}if(!found)return returnFound?false:\"\";if(!returnFound&&typeof val==\"function\")val=this.lv(val,ctx,partials);return val},ho:function(val,cx,partials,text,tags){var compiler=this.c;var options=this.options;options.delimiters=tags;var t=val.call(cx,text,function(t){return compiler.compile(t,options).render(cx,partials)});this.b(compiler.compile(t.toString(),options).render(cx,partials));return false},b:useArrayBuffer?function(s){this.buf.push(s)}:function(s){this.buf+=s},fl:useArrayBuffer?function(){var r=this.buf.join(\"\");this.buf=[];return r}:function(){var r=this.buf;this.buf=\"\";return r},ls:function(val,ctx,partials,inverted,start,end,tags){var cx=ctx[ctx.length-1],t=null;if(!inverted&&this.c&&val.length>0)return this.ho(val,cx,partials,this.text.substring(start,end),tags);t=val.call(cx);if(typeof t==\"function\")if(inverted)return true;else if(this.c)return this.ho(t,cx,partials,this.text.substring(start,end),tags);return t},lv:function(val,ctx,partials){var cx=ctx[ctx.length-1];var result=val.call(cx);if(typeof result==\"function\")result=result.call(cx);result=coerceToString(result);if(this.c&&~result.indexOf(\"{{\"))return this.c.compile(result,this.options).render(cx,partials);return result}};var rAmp=/&/g,rLt=/</g,rGt=/>/g,rApos=/\\'/g,rQuot=/\\\"/g,hChars=/[&<>\\\"\\']/;function coerceToString(val){return String(val===null||val===undefined?\"\":val)}function hoganEscape(str){str=coerceToString(str);return hChars.test(str)?str.replace(rAmp,\"&amp;\").replace(rLt,\"&lt;\").replace(rGt,\"&gt;\").replace(rApos,\"&#39;\").replace(rQuot,\"&quot;\"):str}var isArray=Array.isArray||function(a){return Object.prototype.toString.call(a)===\"[object Array]\"}})(typeof exports!==\"undefined\"?exports:Hogan);(function(Hogan){var rIsWhitespace=/\\S/,rQuot=/\\\"/g,rNewline=/\\n/g,rCr=/\\r/g,rSlash=/\\\\/g,tagTypes={\"#\":1,\"^\":2,\"/\":3,\"!\":4,\">\":5,\"<\":6,\"=\":7,\"_v\":8,\"{\":9,\"&\":10};Hogan.scan=function scan(text,delimiters){var len=text.length,IN_TEXT=0,IN_TAG_TYPE=1,IN_TAG=2,state=IN_TEXT,tagType=null,tag=null,buf=\"\",tokens=[],seenTag=false,i=0,lineStart=0,otag=\"{{\",ctag=\"}}\";function addBuf(){if(buf.length>0){tokens.push(new String(buf));buf=\"\"}}function lineIsWhitespace(){var isAllWhitespace=true;for(var j=lineStart;j<tokens.length;j++){isAllWhitespace=tokens[j].tag&&tagTypes[tokens[j].tag]<tagTypes[\"_v\"]||!tokens[j].tag&&tokens[j].match(rIsWhitespace)===null;if(!isAllWhitespace)return false}return isAllWhitespace}function filterLine(haveSeenTag,noNewLine){addBuf();if(haveSeenTag&&lineIsWhitespace())for(var j=lineStart,next;j<tokens.length;j++){if(!tokens[j].tag){if((next=tokens[j+1])&&next.tag==\">\")next.indent=tokens[j].toString();tokens.splice(j,1)}}else if(!noNewLine)tokens.push({tag:\"\\n\"});seenTag=false;lineStart=tokens.length}function changeDelimiters(text,index){var close=\"=\"+ctag,closeIndex=text.indexOf(close,index),delimiters=trim(text.substring(text.indexOf(\"=\",index)+1,closeIndex)).split(\" \");otag=delimiters[0];ctag=delimiters[1];return closeIndex+close.length-1}if(delimiters){delimiters=delimiters.split(\" \");otag=delimiters[0];ctag=delimiters[1]}for(i=0;i<len;i++)if(state==IN_TEXT)if(tagChange(otag,text,i)){--i;addBuf();state=IN_TAG_TYPE}else if(text.charAt(i)==\"\\n\")filterLine(seenTag);else buf+=text.charAt(i);else if(state==IN_TAG_TYPE){i+=otag.length-1;tag=tagTypes[text.charAt(i+1)];tagType=tag?text.charAt(i+1):\"_v\";if(tagType==\"=\"){i=changeDelimiters(text,i);state=IN_TEXT}else{if(tag)i++;state=IN_TAG}seenTag=i}else if(tagChange(ctag,text,i)){tokens.push({tag:tagType,n:trim(buf),otag:otag,ctag:ctag,i:tagType==\"/\"?seenTag-ctag.length:i+otag.length});buf=\"\";i+=ctag.length-1;state=IN_TEXT;if(tagType==\"{\")if(ctag==\"}}\")i++;else cleanTripleStache(tokens[tokens.length-1])}else buf+=text.charAt(i);filterLine(seenTag,true);return tokens};function cleanTripleStache(token){if(token.n.substr(token.n.length-1)===\"}\")token.n=token.n.substring(0,token.n.length-1)}function trim(s){if(s.trim)return s.trim();return s.replace(/^\\s*|\\s*$/g,\"\")}function tagChange(tag,text,index){if(text.charAt(index)!=tag.charAt(0))return false;for(var i=1,l=tag.length;i<l;i++)if(text.charAt(index+i)!=tag.charAt(i))return false;return true}function buildTree(tokens,kind,stack,customTags){var instructions=[],opener=null,token=null;while(tokens.length>0){token=tokens.shift();if(token.tag==\"#\"||token.tag==\"^\"||isOpener(token,customTags)){stack.push(token);token.nodes=buildTree(tokens,token.tag,stack,customTags);instructions.push(token)}else if(token.tag==\"/\"){if(stack.length===0)throw new Error(\"Closing tag without opener: /\"+token.n);opener=stack.pop();if(token.n!=opener.n&&!isCloser(token.n,opener.n,customTags))throw new Error(\"Nesting error: \"+opener.n+\" vs. \"+token.n);opener.end=token.i;return instructions}else instructions.push(token)}if(stack.length>0)throw new Error(\"missing closing tag: \"+stack.pop().n);return instructions}function isOpener(token,tags){for(var i=0,l=tags.length;i<l;i++)if(tags[i].o==token.n){token.tag=\"#\";return true}}function isCloser(close,open,tags){for(var i=0,l=tags.length;i<l;i++)if(tags[i].c==close&&tags[i].o==open)return true}function writeCode(tree){return'var _=this;_.b(i=i||\"\");'+walk(tree)+\"return _.fl();\"}Hogan.generate=function(code,text,options){if(options.asString)return\"function(c,p,i){\"+code+\";}\";return new Hogan.Template(new Function(\"c\",\"p\",\"i\",code),text,Hogan,options)};function esc(s){return s.replace(rSlash,\"\\\\\\\\\").replace(rQuot,'\\\\\"').replace(rNewline,\"\\\\n\").replace(rCr,\"\\\\r\")}function chooseMethod(s){return~s.indexOf(\".\")?\"d\":\"f\"}function walk(tree){var code=\"\";for(var i=0,l=tree.length;i<l;i++){var tag=tree[i].tag;if(tag==\"#\")code+=section(tree[i].nodes,tree[i].n,chooseMethod(tree[i].n),tree[i].i,tree[i].end,tree[i].otag+\" \"+tree[i].ctag);else if(tag==\"^\")code+=invertedSection(tree[i].nodes,tree[i].n,chooseMethod(tree[i].n));else if(tag==\"<\"||tag==\">\")code+=partial(tree[i]);else if(tag==\"{\"||tag==\"&\")code+=tripleStache(tree[i].n,chooseMethod(tree[i].n));else if(tag==\"\\n\")code+=text('\"\\\\n\"'+(tree.length-1==i?\"\":\" + i\"));else if(tag==\"_v\")code+=variable(tree[i].n,chooseMethod(tree[i].n));else if(tag===undefined)code+=text('\"'+esc(tree[i])+'\"')}return code}function section(nodes,id,method,start,end,tags){return\"if(_.s(_.\"+method+'(\"'+esc(id)+'\",c,p,1),'+\"c,p,0,\"+start+\",\"+end+',\"'+tags+'\")){'+\"_.rs(c,p,\"+\"function(c,p,_){\"+walk(nodes)+\"});c.pop();}\"}function invertedSection(nodes,id,method){return\"if(!_.s(_.\"+method+'(\"'+esc(id)+'\",c,p,1),c,p,1,0,0,\"\")){'+walk(nodes)+\"};\"}function partial(tok){return'_.b(_.rp(\"'+esc(tok.n)+'\",c,p,\"'+(tok.indent||\"\")+'\"));'}function tripleStache(id,method){return\"_.b(_.t(_.\"+method+'(\"'+esc(id)+'\",c,p,0)));'}function variable(id,method){return\"_.b(_.v(_.\"+method+'(\"'+esc(id)+'\",c,p,0)));'}function text(id){return\"_.b(\"+id+\");\"}Hogan.parse=function(tokens,text,options){options=options||{};return buildTree(tokens,\"\",[],options.sectionTags||[])},Hogan.cache={};Hogan.compile=function(text,options){options=options||{};var key=text+\"||\"+!!options.asString;var t=this.cache[key];if(t)return t;t=this.generate(writeCode(this.parse(this.scan(text,options.delimiters),text,options)),text,options);return this.cache[key]=t}})(typeof exports!==\"undefined\"?exports:Hogan);window.Hogan=Hogan;}");
            sb.append("if(!window.flurryadapter){var flurryBridgeCtor=function(w){var flurryadapter={};flurryadapter.flurryCallQueue=[];flurryadapter.flurryCallInProgress=false;flurryadapter.callComplete=function(cmd){if(this.flurryCallQueue.length==0){this.flurryCallInProgress=false;return}var adapterCall=this.flurryCallQueue.splice(0,1)[0];this.executeNativeCall(adapterCall);return\"OK\"};flurryadapter.executeCall=function(command){var adapterCall=\"flurry://flurrycall?event=\"+command;var value;for(var i=1;i<arguments.length;i+=2){value=arguments[i+1];if(value==null)continue;adapterCall+=\"&\"+arguments[i]+\"=\"+escape(value)}if(this.flurryCallInProgress)this.flurryCallQueue.push(adapterCall);else this.executeNativeCall(adapterCall)};flurryadapter.executeNativeCall=function(adapterCall){if(adapterCall.length==0)return;this.flurryCallInProgress=true;w.location=adapterCall};return flurryadapter};window.flurryadapter=flurryBridgeCtor(window);}");
            sb.append("if(!window.flurryAdapterAvailable){window.flurryAdapterAvailable=true;if(typeof window.FlurryAdapterReady === 'function'){window.FlurryAdapterReady();} }");
            String e = je.e(currentContent);
            sb.append("var content='");
            sb.append(e);
            sb.append("';var compiled=window.Hogan.compile(document.body.innerHTML);var rendered=compiled.render(JSON.parse(content));document.body.innerHTML=rendered;");
            sb.append("})();");
            if (this.l != null) {
                this.l.loadUrl(sb.toString());
            }
        }
    }

    private synchronized void setMraidJsEnvInitialized(boolean z2) {
        this.z = z2;
    }

    private synchronized boolean k() {
        return this.z;
    }

    /* access modifiers changed from: private */
    public synchronized void l() {
        if (!k()) {
            n();
            setMraidJsEnvInitialized(true);
        }
    }

    /* access modifiers changed from: private */
    public synchronized void m() {
        setMraidJsEnvInitialized(false);
    }

    private void n() {
        ja.a(3, this.f, "initializeMraid");
        StringBuilder sb = new StringBuilder();
        sb.append("javascript:(function() {");
        sb.append("var mraidCtor=function(flurryBridge,initState){var mraid={};var STATES=mraid.STATES={LOADING:\"loading\",UNKNOWN:\"unknown\",DEFAULT:\"default\",EXPANDED:\"expanded\",HIDDEN:\"hidden\"};var EVENTS=mraid.EVENTS={ASSETREADY:\"assetReady\",ASSETREMOVED:\"assetRemoved\",ASSETRETIRED:\"assetRetired\",INFO:\"info\",ERROR:\"error\",ORIENTATIONCHANGE:\"orientationChange\",READY:\"ready\",STATECHANGE:\"stateChange\",VIEWABLECHANGE:\"viewableChange\"};var listeners={};var currentState=STATES.LOADING;var expandProperties={width:initState.width,height:initState.height,isModal:initState.isModal,useCustomClose:false};var collapseProperties={};var placementType=initState.placementType;var disable=false;var closeId=\"flurry-mraid-default-close\";var imgUrl=\"http://flurry.cachefly.net/adSpaceStyles/images/bttn-close-bw.png\";var safeClose=function(){try{if(window.mraid)window.mraid.close();else if(window.flurryadapter)flurryadapter.executeCall(\"adWillClose\");else console.log(\"unable to close\")}catch(error){console.log(\"unable to close: \"+error)}};var makeDefaultClose=function(){var img=document.createElement(\"img\");img.src=imgUrl;img.id=closeId;img.style.position=\"absolute\";img.style.top=\"10px\";img.style.right=\"10px\";img.style.width=\"50px\";img.style.height=\"50px\";img.style.zIndex=1E4;return img};var updateDefaultClose=function(){if(!expandProperties.useCustomClose&&(placementType===\"interstitial\"||currentState===STATES.EXPANDED))addDefaultClose();else removeDefaultClose()};var addDefaultClose=function(){var closeButton=document.getElementById(closeId);if(!closeButton){closeButton=makeDefaultClose();document.body.appendChild(closeButton)}};var removeDefaultClose=function(){var closeButton=document.getElementById(closeId);if(closeButton)document.body.removeChild(closeButton)};var setupDefaultCloseHandler=function(){document.body.addEventListener(\"click\",function(e){e=e||window.event;var target=e.target||e.srcElement;if(target.id===closeId)safeClose()})};var contains=function(value,obj){for(var i in obj)if(obj[i]===value)return true;return false};var stringify=function(obj){if(typeof obj==\"object\")if(obj.push){var out=[];for(var p in obj)if(obj.hasOwnProperty(p))out.push(obj[p]);return\"[\"+out.join(\",\")+\"]\"}else{var out=[];for(var p in obj)if(obj.hasOwnProperty(p))out.push(\"'\"+p+\"':\"+obj[p]);return\"{\"+out.join(\",\")+\"}\"}else return new String(obj)};var broadcastEvent=function(){var args=new Array(arguments.length);for(var i=0;i<arguments.length;i++)args[i]=arguments[i];var event=args.shift();try{if(listeners[event])for(var j=0;j<listeners[event].length;j++)if(typeof listeners[event][j]===\"function\")listeners[event][j].apply(undefined,args);else if(typeof listeners[event][j]===\"string\"&&typeof window[listeners[event][j]]===\"function\")window[listeners[event][j]].apply(undefined,args)}catch(e){console.log(e)}};mraid.disable=function(){removeDefaultClose();disable=true};mraid.addEventListener=function(event,listener){if(disable)return;if(!event||!listener)broadcastEvent(EVENTS.ERROR,\"Both event and listener are required.\",\"addEventListener\");else if(!contains(event,EVENTS))broadcastEvent(EVENTS.ERROR,\"Unknown event: \"+event,\"addEventListener\");else if(!listeners[event])listeners[event]=[listener];else listeners[event].push(listener);flurryBridge.executeCall(\"eventListenerAdded\")};mraid.stateChange=function(newState){if(disable)return;if(currentState===newState)return;broadcastEvent(EVENTS.INFO,\"setting state to \"+stringify(newState));var oldState=currentState;currentState=newState;if(oldState===STATES.LOADING&&newState===STATES.DEFAULT){setupDefaultCloseHandler();updateDefaultClose();broadcastEvent(EVENTS.READY)}else if(oldState===STATES.HIDDEN||newState===STATES.HIDDEN)broadcastEvent(EVENTS.VIEWABLECHANGE);else if(oldState===STATES.DEFAULT&&newState===STATES.EXPANDED)updateDefaultClose();else if(newState===STATES.DEFAULT&&oldState===STATES.EXPANDED)updateDefaultClose();broadcastEvent(EVENTS.STATECHANGE,currentState)};mraid.close=function(){if(disable)return;var state=mraid.getState();if(state===STATES.DEFAULT){mraid.stateChange(STATES.HIDDEN);flurryBridge.executeCall(\"adWillClose\")}else if(state===STATES.EXPANDED){mraid.stateChange(STATES.DEFAULT);flurryBridge.executeCall(\"collapse\")}else console.log(\"close() called in state \"+state)};mraid.expand=function(url){if(disable)return;var state=mraid.getState();if(state!==STATES.DEFAULT){console.log(\"expand() called in state \"+state);return}if(placementType===\"interstitial\"){console.log(\"expand() called for placement type \"+placementType);return}if(url)flurryBridge.executeCall(\"expand\",\"width\",expandProperties.width,\"height\",expandProperties.height,\"url\",url);else flurryBridge.executeCall(\"expand\",\"width\",expandProperties.width,\"height\",expandProperties.height);mraid.stateChange(STATES.EXPANDED)};mraid.setExpandProperties=function(properties){if(disable)return;if(typeof properties.width===\"number\"&&!isNaN(properties.width))expandProperties.width=properties.width;if(typeof properties.height===\"number\"&&!isNaN(properties.height))expandProperties.height=properties.height;if(typeof properties.useCustomClose===\"boolean\"){expandProperties.useCustomClose=properties.useCustomClose;updateDefaultClose()}};mraid.getExpandProperties=function(properties){if(disable)return;var ret={};ret.width=expandProperties.width;ret.height=expandProperties.height;ret.isModal=expandProperties.isModal;ret.useCustomClose=expandProperties.useCustomClose;return ret};mraid.getPlacementType=function(){return placementType};mraid.getVersion=function(){if(disable)return\"\";return\"1.0\"};mraid.getState=function(){if(disable)return\"\";return currentState};mraid.isViewable=function(){if(disable)return false;if(mraid.getState()===\"hidden\")return false;else return true};mraid.open=function(url){if(disable)return;try{flurryBridge.executeCall(\"open\",\"url\",url)}catch(e){console.log(e)}};mraid.removeEventListener=function(event,listener){if(disable)return;if(!event)broadcastEvent(\"error\",\"Must specify an event.\",\"removeEventListener\");else if(listener&&listeners[event])for(var i=0;i<listeners[event].length;i++){if(listeners[event][i]===listener)listeners[event].splice(i,1)}else if(listeners[event])listeners[event]=[]};mraid.useCustomClose=function(use){if(disable)return;if(typeof use===\"boolean\"){expandProperties.useCustomClose=use;updateDefaultClose()}};return mraid};");
        sb.append("window.mraid=mraidCtor(window.flurryadapter," + ("{useCustomClose:" + false + ",isModal:" + false + ",width:undefined,height:undefined,placementType:\"" + (d() ? "interstitial" : "inline") + "\"}") + ");");
        sb.append("})();");
        if (this.l != null) {
            this.l.loadUrl(sb.toString());
        }
    }

    /* access modifiers changed from: private */
    public void o() {
        ja.a(3, this.f, "activateMraid");
        if (this.l != null) {
            this.l.loadUrl("javascript:" + "if(window.mraid){window.mraid.stateChange(window.mraid.STATES.DEFAULT);}");
        }
    }

    /* access modifiers changed from: private */
    public void p() {
        if (!(getContext() instanceof Activity)) {
            ja.a(3, this.f, "no activity present");
            return;
        }
        Activity activity = (Activity) getContext();
        if (d()) {
            j.a(activity, j.a(), true);
        }
    }

    private boolean q() {
        return this.s != null;
    }

    public o(Context context, FlurryAdModule flurryAdModule, m mVar, AdUnit adUnit, int i2) {
        super(context, flurryAdModule, mVar);
        setClickable(true);
        this.d = adUnit;
        this.e = i2;
        this.c = mVar;
        this.G = Executors.newSingleThreadExecutor();
        if (this.d != null) {
            this.B = this.d.d();
            this.C = this.d.e().intValue() == 1;
            if (this.C) {
                this.E = new HashMap();
                this.D = new HashMap();
                this.E.put(mVar.b(), mVar);
                this.D.put(adUnit.d().get(0).g().toString(), adUnit);
            }
            this.F = flurryAdModule;
            setAdUnit(this.d);
            setAdLog(this.c);
            this.H = new LinkedList();
            return;
        }
        ja.a(3, this.f, "adunit is Null");
    }

    private void a(i iVar, int i2) {
        if (this.C) {
            String str = iVar.c.b.get("guid");
            if (str != null) {
                this.d = c(str);
                this.B = this.d.d();
                this.c = iVar.c.e;
                if (d()) {
                    this.F.a(this.d);
                    this.F.b(this.c);
                }
                this.e = i2;
                this.C = false;
                initLayout();
            }
        } else if (i2 != this.e && i2 < this.B.size()) {
            String currentFormat = getCurrentFormat();
            String obj = this.d.d().get(i2).e().e().toString();
            if (obj.equals(currentFormat)) {
                this.e = i2;
                initLayout();
            } else if (obj.equals(AdCreative.kFormatTakeover)) {
                this.F.b(this.c);
                this.F.a(this.d);
                Intent intent = new Intent(ia.a().b(), FlurryFullscreenTakeoverActivity.class);
                intent.putExtra(FlurryFullscreenTakeoverActivity.EXTRA_KEY_FRAMEINDEX, i2);
                this.F.a().a(getContext(), intent, this.d.b().toString());
            }
        }
    }

    private void r() {
        ja.a(3, this.f, "closing ad");
        if (!d()) {
            return;
        }
        if (!(getContext() instanceof Activity)) {
            ja.a(3, this.f, "no activity present");
        } else {
            ((Activity) getContext()).finish();
        }
    }

    public void initLayout() {
        ja.a(3, this.f, "initLayout: ad creative layout: {width = " + getCurrentAdFrame().e().b() + ", height = " + getCurrentAdFrame().e().c() + "}");
        Context context = getContext();
        removeAllViews();
        setFocusable(true);
        setFocusableInTouchMode(true);
        switch (getCurrentBinding()) {
            case 1:
            case 2:
                if (this.l == null) {
                    this.l = new WebView(context);
                    this.l.getSettings().setJavaScriptEnabled(true);
                    this.l.setVerticalScrollBarEnabled(false);
                    this.l.setHorizontalScrollBarEnabled(false);
                    this.l.setBackgroundColor(0);
                    this.l.clearCache(false);
                    this.o = new w(this, null);
                    this.l.setWebChromeClient(this.o);
                    this.n = new aa(this, null);
                    this.l.setWebViewClient(this.n);
                }
                if (getCurrentBinding() == 1) {
                    if (this.a != null) {
                        a(this.a);
                    } else {
                        a(getCurrentDisplay());
                    }
                } else if (getCurrentBinding() == 2) {
                    this.l.loadDataWithBaseURL("base://url/", getCurrentDisplay(), "text/html", "utf-8", "base://url/");
                    a("rendered", Collections.emptyMap(), this.d, this.c, this.e, 0);
                }
                this.l.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
                if (d()) {
                    this.h = new ProgressDialog(context);
                    this.h.setProgressStyle(0);
                    this.h.setMessage("Loading...");
                    this.h.setCancelable(true);
                    this.h.setOnKeyListener(this);
                    this.h.show();
                    return;
                }
                return;
            case 3:
                if (this.j == null) {
                    this.j = new n(context);
                    this.j.setOnPreparedListener(this);
                    this.j.setOnCompletionListener(this);
                    this.j.setOnErrorListener(this);
                    this.i = new cf(context, this.j, this);
                    this.j.setMediaController(this.i);
                }
                this.j.setVideoURI(Uri.parse(getCurrentDisplay()));
                ja.a(3, this.f, "URI: " + Uri.parse(getCurrentDisplay()).toString());
                this.j.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
                addView(this.j);
                ((RelativeLayout) this.j.getParent()).setGravity(17);
                this.h = new ProgressDialog(context);
                this.h.setProgressStyle(0);
                this.h.setMessage("Loading...");
                this.h.setCancelable(true);
                this.h.setOnKeyListener(this);
                this.h.show();
                return;
            default:
                a("renderFailed", Collections.emptyMap(), this.d, this.c, this.e, 0);
                return;
        }
    }

    private void a(String str) {
        this.G.submit(new p(this, str));
    }

    public void a(i iVar, cj cjVar, int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        String str;
        String str2;
        String str3;
        String str4;
        String str5 = iVar.a;
        bh bhVar = iVar.c;
        Map<String, String> map = iVar.b;
        if (i2 > 10) {
            ja.a(5, this.f, "Maximum depth for event/action loop exceeded when performing action:" + str5 + "," + map + ",triggered by:" + bhVar.a);
        } else if (!bhVar.a.equals("clicked") || (str4 = bhVar.b.get("noop")) == null || !str4.equals("true")) {
            ja.a(3, this.f, "performAction(action=" + str5 + ",params=" + iVar.b + ",triggering event=" + bhVar.a + ")");
            if (str5.equals("nextFrame")) {
                int i7 = this.e + 1;
                String str6 = map.get("offset");
                if (str6 != null) {
                    if (str6.equals("next")) {
                        i7 = this.e + 1;
                    } else if (!str6.equals("current")) {
                        try {
                            i7 = Integer.parseInt(str6);
                        } catch (NumberFormatException e) {
                            ja.a(6, this.f, "caught: " + e.getMessage());
                        }
                    } else {
                        return;
                    }
                }
                a(iVar, i7);
            } else if (str5.equals("closeAd")) {
                r();
            } else if (str5.equals("notifyUser")) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                if (!map.containsKey("message") || !map.containsKey("confirmDisplay") || !map.containsKey("cancelDisplay")) {
                    str = "Cancel";
                    str2 = "Are you sure?";
                    str3 = "OK";
                } else {
                    str2 = map.get("message");
                    String str7 = map.get("confirmDisplay");
                    str3 = map.get("cancelDisplay");
                    str = str7;
                }
                builder.setMessage(str2).setCancelable(false).setPositiveButton(str3, new u(this, bhVar, i2)).setNegativeButton(str, new t(this, bhVar, i2));
                this.A = builder.create();
                if (this.j != null && getCurrentBinding() == 3) {
                    this.j.pause();
                }
                this.A.show();
            } else if (str5.equals("loadAdComponents")) {
                if (!map.containsKey("min") || !map.containsKey("max")) {
                    i5 = 1;
                    i6 = 3;
                } else {
                    try {
                        i5 = Integer.parseInt(map.get("min"));
                        i6 = Integer.parseInt(map.get("max"));
                    } catch (NumberFormatException e2) {
                        i5 = 1;
                        i6 = 3;
                    }
                }
                List<AdUnit> a2 = a(this.d.b().toString(), i5, i6);
                if (a2.size() > 0) {
                    String a3 = a(a2, this.d.b().toString());
                    if (this.l != null) {
                        this.l.loadUrl("javascript:(function() {var multiadwraps=document.getElementsByClassName('multiAdWrap');if(multiadwraps.length>0){var template=document.getElementsByClassName('multiAdWrap')[0];var compiled=Hogan.compile(template.innerHTML);template.innerHTML='';template.innerHTML=compiled.render(JSON.parse(" + a3 + "));}})();");
                        this.l.loadUrl("javascript:flurryadapter.callComplete();");
                    }
                    for (AdUnit next : a2) {
                        HashMap hashMap = new HashMap();
                        hashMap.put("guid", next.d().get(0).g().toString());
                        a("rendered", hashMap, next, b(next.d().get(0).g().toString()), 0, 0);
                    }
                    if (this.l != null && !a(this.l)) {
                        addView(this.l);
                    }
                } else {
                    a("renderFailed", Collections.emptyMap(), this.d, this.c, this.e, 0);
                }
            } else if (str5.equals("doExpand")) {
                int b = je.b();
                int c = je.c();
                if (!iVar.c.b.containsKey(AdCreative.kFixWidth) || !iVar.c.b.containsKey(AdCreative.kFixHeight)) {
                    i3 = c;
                    i4 = b;
                } else {
                    try {
                        int b2 = je.b(Integer.parseInt(iVar.c.b.get(AdCreative.kFixWidth)));
                        i3 = je.b(Integer.parseInt(iVar.c.b.get(AdCreative.kFixHeight)));
                        i4 = b2;
                    } catch (NumberFormatException e3) {
                        ja.a(6, this.f, e3.getMessage());
                        int b3 = je.b();
                        i3 = je.c();
                        i4 = b3;
                    }
                }
                ja.a(3, this.f, "expand to width = " + i4 + " height = " + i3);
                if (getHolder() != null) {
                    a("clicked", Collections.emptyMap(), this.d, this.c, this.e, 0);
                    a(i4, i3);
                }
                if (iVar.c.b.containsKey(FlurryFullscreenTakeoverActivity.EXTRA_KEY_URL)) {
                    this.a = iVar.c.b.get(FlurryFullscreenTakeoverActivity.EXTRA_KEY_URL);
                    initLayout();
                }
            } else if (str5.equals("doCollapse")) {
                int intValue = getCurrentAdFrame().e().b().intValue();
                int intValue2 = getCurrentAdFrame().e().c().intValue();
                int b4 = je.b(intValue);
                int b5 = je.b(intValue2);
                if (this.a != null) {
                    this.a = null;
                    initLayout();
                }
                if (getHolder() != null) {
                    b(b4, b5);
                }
            } else {
                if (str5.equals("directOpen")) {
                    HashMap hashMap2 = new HashMap();
                    hashMap2.put("noop", "true");
                    a("clicked", hashMap2, this.d, this.c, this.e, 0);
                }
                this.b.a().a(iVar, cjVar, i2);
            }
            a(iVar);
        } else {
            ja.a(4, this.f, "'clicked' event is a noop");
        }
    }

    private void a(i iVar) {
        if (this.H.contains(iVar.c.a) && this.l != null) {
            this.l.loadUrl("javascript:flurryadapter.callComplete('" + iVar.c.a + "');");
            this.H.remove(iVar.c.a);
        }
    }

    private void a(int i2, int i3) {
        if (!(getContext() instanceof Activity)) {
            ja.a(3, this.f, "no activity present");
            return;
        }
        Activity activity = (Activity) getContext();
        if (this.v == null) {
            ja.a(3, this.f, "expand(" + i2 + "," + i3 + ")");
            if (!(this.l == null || -1 == indexOfChild(this.l))) {
                removeView(this.l);
            }
            this.u = activity.getRequestedOrientation();
            if (this.w == null) {
                this.w = new FrameLayout(activity);
                this.w.setBackgroundColor(-16777216);
                if (this.l != null && this.l.getParent() == null) {
                    this.w.addView(this.l, new FrameLayout.LayoutParams(-1, -1, 17));
                }
            }
            if (this.v == null) {
                this.v = new Dialog(activity, 16973834);
                cq.a(this.v.getWindow());
                this.v.setContentView(this.w, new ViewGroup.LayoutParams(-1, -1));
                this.v.setOnDismissListener(new v(this));
                this.v.setCancelable(true);
                this.v.show();
            }
            j.a(activity, j.a(), true);
            this.b.a(activity, this.d.b().toString());
        }
    }

    private void b(int i2, int i3) {
        if (!(getContext() instanceof Activity)) {
            ja.a(3, this.f, "no activity present");
            return;
        }
        Activity activity = (Activity) getContext();
        if (this.v != null) {
            ja.a(3, this.f, "collapse(" + i2 + "," + i3 + ")");
            if (this.v != null && this.v.isShowing()) {
                this.v.hide();
                this.v.setOnDismissListener(null);
                this.v.dismiss();
            }
            this.v = null;
            j.a(activity, this.u);
            if (this.w != null) {
                if (!(this.l == null || -1 == this.w.indexOfChild(this.l))) {
                    this.w.removeView(this.l);
                }
                this.w = null;
            }
            if (this.l != null && this.l.getParent() == null) {
                addView(this.l);
            }
            this.b.b(activity, this.d.b().toString());
        }
    }

    private boolean s() {
        return this.v != null;
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        a("videoCompleted", Collections.emptyMap(), this.d, this.c, this.e, 0);
    }

    public void a() {
        a("adWillClose", Collections.emptyMap(), this.d, this.c, this.e, 0);
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        if (getCurrentBinding() == 3) {
            if (this.h.isShowing()) {
                this.h.dismiss();
            }
            if ((this.A == null || !this.A.isShowing()) && this.j != null && this.k) {
                this.j.start();
            }
            a("rendered", Collections.emptyMap(), this.d, this.c, this.e, 0);
            a("videoStart", Collections.emptyMap(), this.d, this.c, this.e, 0);
        }
    }

    public boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        if (this.h != null && this.h.isShowing()) {
            this.h.dismiss();
        }
        a("renderFailed", Collections.emptyMap(), this.d, this.c, this.e, 0);
        removeView(this.j);
        return false;
    }

    @TargetApi(11)
    public void b() {
        this.k = true;
        if (this.l != null && Build.VERSION.SDK_INT >= 11) {
            this.l.onResume();
        }
        if (this.j != null && getCurrentBinding() == 3 && this.i != null) {
            this.i.show(0);
        }
    }

    @TargetApi(11)
    public void c() {
        if (this.l != null && Build.VERSION.SDK_INT >= 11) {
            this.l.onPause();
        }
        if (this.j != null && getCurrentBinding() == 3) {
            this.j.pause();
        }
        this.k = false;
    }

    @TargetApi(11)
    public void stop() {
        ja.a(3, this.f, "stop");
        if (getCurrentBinding() == 3) {
            if (this.h != null && this.h.isShowing()) {
                this.h.dismiss();
            }
            if (this.j != null && this.j.isPlaying()) {
                this.j.stopPlayback();
            }
        }
        if (this.l != null) {
            if (!(this.p == null || this.o == null)) {
                this.o.onHideCustomView();
            }
            if (this.v != null) {
                b(0, 0);
            }
            removeView(this.l);
            this.l.stopLoading();
            if (Build.VERSION.SDK_INT >= 11) {
                this.l.onPause();
            }
            this.l.destroy();
            this.l = null;
        }
        if (d()) {
            a("adClosed", Collections.emptyMap(), this.d, this.c, this.e, 0);
        }
    }

    public void a(String str, Map<String, String> map, AdUnit adUnit, m mVar, int i2, int i3) {
        ja.a(3, this.f, "fireEvent(event=" + str + ",params=" + map + ")");
        this.b.a(new bh(str, map, getContext(), adUnit, mVar, i2), this, i3);
    }

    /* access modifiers changed from: package-private */
    public int getCurrentBinding() {
        return getCurrentAdFrame().b().intValue();
    }

    /* access modifiers changed from: package-private */
    public String getCurrentContent() {
        return getCurrentAdFrame().d().toString();
    }

    /* access modifiers changed from: package-private */
    public String getCurrentDisplay() {
        return getCurrentAdFrame().c().toString();
    }

    /* access modifiers changed from: package-private */
    public String getCurrentFormat() {
        return getCurrentAdFrame().e().e().toString();
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return getCurrentFormat().equals(AdCreative.kFormatTakeover);
    }

    /* access modifiers changed from: package-private */
    public AdFrame getCurrentAdFrame() {
        return this.B.get(this.e);
    }

    private String a(List<AdUnit> list, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("'{\"adComponents\":[");
        String url = this.l.getUrl();
        String a2 = cp.a(url);
        String a3 = cp.a(a2, url);
        Iterator<AdUnit> it = list.iterator();
        while (it.hasNext()) {
            String obj = it.next().d().get(0).d().toString();
            if (!TextUtils.isEmpty(obj) && !TextUtils.isEmpty(a3) && a3 != a2) {
                obj = obj.replace(a2, a3);
            }
            sb.append(je.e(obj));
            if (it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("]}'");
        return sb.toString();
    }

    private List<AdUnit> a(String str, int i2, int i3) {
        List<AdUnit> a2 = this.F.b().a(this.d.b().toString(), i2, i3);
        for (AdUnit next : a2) {
            if (next.d().size() > 0) {
                this.D.put(next.d().get(0).g().toString(), next);
            }
        }
        return a2;
    }

    /* access modifiers changed from: private */
    public m b(String str) {
        if (this.E == null) {
            return null;
        }
        m mVar = this.E.get(str);
        if (mVar != null) {
            return mVar;
        }
        m a2 = ab.a(this.F, str);
        this.E.put(str, a2);
        return a2;
    }

    /* access modifiers changed from: private */
    public AdUnit c(String str) {
        if (this.D == null) {
            return null;
        }
        return this.D.get(str);
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        boolean c = jc.a().c();
        if (!c) {
            ja.a(5, this.f, "There is no network connectivity (ad will not rotate)");
        }
        return !s() && !q() && c;
    }

    /* access modifiers changed from: package-private */
    public boolean a(View view) {
        ViewParent parent = view.getParent();
        return parent != null && parent == this;
    }

    public boolean onKey(DialogInterface dialogInterface, int i2, KeyEvent keyEvent) {
        ja.a(3, "listeners", "onkey,keycode=" + i2 + ",event=" + keyEvent.getAction());
        if (dialogInterface != this.h || i2 != 4 || keyEvent.getAction() != 0) {
            return false;
        }
        a();
        dialogInterface.dismiss();
        return true;
    }

    /* access modifiers changed from: package-private */
    public an getHolder() {
        try {
            ViewParent parent = getParent();
            if (parent != null) {
                return (an) parent;
            }
            return null;
        } catch (ClassCastException e) {
            ja.a(5, this.f, "AdUnityView parent not a BannerHolder:" + e.getMessage());
            return null;
        }
    }
}
