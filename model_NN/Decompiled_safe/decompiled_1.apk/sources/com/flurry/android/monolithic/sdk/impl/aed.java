package com.flurry.android.monolithic.sdk.impl;

import java.lang.Enum;
import java.util.HashMap;

public class aed<T extends Enum<T>> {
    protected final Class<T> a;
    protected final T[] b;
    protected final HashMap<String, T> c;

    protected aed(Class<T> cls, T[] tArr, HashMap<String, T> hashMap) {
        this.a = cls;
        this.b = tArr;
        this.c = hashMap;
    }

    public static <ET extends Enum<ET>> aed<ET> a(Class<ET> cls, py pyVar) {
        Enum[] enumArr = (Enum[]) cls.getEnumConstants();
        if (enumArr == null) {
            throw new IllegalArgumentException("No enum constants for class " + cls.getName());
        }
        HashMap hashMap = new HashMap();
        for (Enum enumR : enumArr) {
            hashMap.put(pyVar.a(enumR), enumR);
        }
        return new aed<>(cls, enumArr, hashMap);
    }

    public static <ET extends Enum<ET>> aed<ET> a(Class<ET> cls) {
        Enum[] enumArr = (Enum[]) cls.getEnumConstants();
        HashMap hashMap = new HashMap();
        int length = enumArr.length;
        while (true) {
            length--;
            if (length < 0) {
                return new aed<>(cls, enumArr, hashMap);
            }
            Enum enumR = enumArr[length];
            hashMap.put(enumR.toString(), enumR);
        }
    }

    public static aed<?> b(Class<?> cls, py pyVar) {
        return a(cls, pyVar);
    }

    public static aed<?> b(Class<?> cls) {
        return a(cls);
    }

    public T a(String str) {
        return (Enum) this.c.get(str);
    }

    public T a(int i) {
        if (i < 0 || i >= this.b.length) {
            return null;
        }
        return this.b[i];
    }

    public Class<T> a() {
        return this.a;
    }

    public int b() {
        return this.b.length - 1;
    }
}
