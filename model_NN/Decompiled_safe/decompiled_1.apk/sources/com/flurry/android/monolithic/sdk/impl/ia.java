package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;

public class ia {
    private static ia a;
    private final Context b;
    private final Handler c = new Handler(Looper.getMainLooper());

    public static synchronized void a(Context context) {
        synchronized (ia.class) {
            if (a == null) {
                if (context == null) {
                    throw new IllegalArgumentException("Context cannot be null");
                }
                a = new ia(context);
            }
        }
    }

    public static ia a() {
        return a;
    }

    private ia(Context context) {
        this.b = context.getApplicationContext();
    }

    public Context b() {
        return this.b;
    }

    public PackageManager c() {
        return this.b.getPackageManager();
    }

    public DisplayMetrics d() {
        return this.b.getResources().getDisplayMetrics();
    }

    public void a(Runnable runnable) {
        if (runnable != null) {
            this.c.post(runnable);
        }
    }
}
