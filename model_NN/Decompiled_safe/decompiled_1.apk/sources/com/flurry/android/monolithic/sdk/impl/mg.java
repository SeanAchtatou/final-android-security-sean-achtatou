package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public abstract class mg extends lx implements mk, mp {
    protected final mo a;

    /* access modifiers changed from: protected */
    public abstract void a() throws IOException;

    protected mg(mq mqVar) throws IOException {
        this.a = new mo(mqVar, this, this);
    }

    public void l() throws IOException {
        this.a.c();
    }

    public void t() throws IOException {
        mq d = this.a.d();
        if (d == mq.c) {
            b();
        }
        if (d == mq.d) {
            c();
        } else if (d == mq.e) {
            d();
        } else if (d == mq.f) {
            e();
        } else if (d == mq.g) {
            f();
        } else if (d == mq.h) {
            g();
        } else if (d == mq.i) {
            i();
        } else if (d == mq.j) {
            j();
        } else if (d == mq.l) {
            k();
        } else if (d == mq.k) {
            a();
        } else if (d == mq.m) {
            s();
        } else if (d == mq.n) {
            o();
        } else if (d == mq.p) {
            r();
        }
    }
}
