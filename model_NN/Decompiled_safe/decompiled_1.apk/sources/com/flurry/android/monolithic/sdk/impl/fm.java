package com.flurry.android.monolithic.sdk.impl;

import android.text.TextUtils;
import com.flurry.android.impl.appcloud.AppCloudModule;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class fm {
    protected String a = "";
    protected String b = "";
    protected HashMap<String, String> c = new HashMap<>();
    protected String d;
    protected String e;
    protected String f;

    public void a(String str, Object obj) {
        if (!str.equals("code") && !str.equals("note")) {
            if (this.c.containsKey(str)) {
                this.c.remove(str);
                this.c.put(str, obj.toString());
                return;
            }
            this.c.put(str, obj.toString());
        }
    }

    public float a(String str) {
        try {
            if (this.c.containsKey(str)) {
                return Float.parseFloat(this.c.get(str));
            }
            return 0.0f;
        } catch (Exception e2) {
            return 0.0f;
        }
    }

    public String a() {
        return this.a;
    }

    public void b(String str) {
        this.a = str;
    }

    public Vector<NameValuePair> b() {
        Vector<NameValuePair> vector = new Vector<>();
        for (Map.Entry next : this.c.entrySet()) {
            String str = (String) next.getKey();
            if (!str.startsWith("_") && !str.equals("username") && !str.equals("email") && !str.equals("code") && !str.equals("note")) {
                vector.add(new BasicNameValuePair(str, (String) next.getValue()));
            }
        }
        return vector;
    }

    public String toString() {
        Vector<NameValuePair> b2 = b();
        String str = "" + "id : " + this.a + " ; ";
        for (int i = 0; i < b2.size(); i++) {
            str = str + "\n( key : value ) = ( " + b2.get(i).getName() + " : " + b2.get(i).getValue() + " )";
        }
        return str;
    }

    /* access modifiers changed from: protected */
    public void a(JSONObject jSONObject) {
        try {
            if (jSONObject.has("username")) {
                this.e = jSONObject.getString("username");
            }
            if (jSONObject.has("email")) {
                this.d = jSONObject.getString("email");
            }
            if (jSONObject.has("_id")) {
                this.a = jSONObject.getString("_id");
            }
            for (int i = 0; i < jSONObject.length(); i++) {
                String string = jSONObject.names().getString(i);
                this.c.put(string, jSONObject.getString(string));
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    public boolean c() {
        if (TextUtils.isEmpty(this.a)) {
            return false;
        }
        return AppCloudModule.getInstance().b().a(this.a);
    }

    public void d() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < b().size()) {
                go.b().b().c(b().get(i2).getName(), b().get(i2).getValue(), this.a, this.b);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public void c(String str) {
        hk hkVar;
        hk hkVar2 = null;
        if (go.b().c().b(str)) {
            hkVar = go.b().c().a(str);
        } else {
            hkVar = null;
        }
        if (go.b().b().b(str)) {
            hkVar2 = go.b().b().a(str);
        }
        if (hkVar != null || hkVar2 != null) {
            b(str);
            if (hkVar2 != null) {
                for (int i = 0; i < hkVar2.a().size(); i++) {
                    a(hkVar2.a().get(i).getName(), hkVar2.a().get(i).getValue());
                }
            }
            if (hkVar != null) {
                for (int i2 = 0; i2 < hkVar.a().size(); i2++) {
                    a(hkVar.a().get(i2).getName(), hkVar.a().get(i2).getValue());
                }
            }
        }
    }

    public void a(hw hwVar) throws Exception {
        hk hkVar = new hk(this.a);
        hkVar.a(b());
        if (this.e != null && !this.e.equals("")) {
            hkVar.a("username", this.e);
        }
        if (this.f != null && !this.f.equals("")) {
            hkVar.a("password", this.f);
        }
        if (this.d != null && !this.d.equals("")) {
            hkVar.a("email", this.d);
        }
        if (TextUtils.isEmpty(this.a)) {
            hm hmVar = new hm(null);
            Vector<NameValuePair> a2 = hkVar.a();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < a2.size()) {
                    hmVar.a(a2.get(i2).getName(), a2.get(i2).getValue());
                    i = i2 + 1;
                } else {
                    go.a().a(hmVar, new fn(this, hwVar));
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(fr frVar) {
        try {
            go.a().b(new hm(this.a), new fo(this, frVar));
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void a(String str, String str2, hx hxVar) {
        hm hmVar = new hm(this.a);
        hmVar.a(str, str2);
        go.a().c(hmVar, new fp(this, hxVar));
    }
}
