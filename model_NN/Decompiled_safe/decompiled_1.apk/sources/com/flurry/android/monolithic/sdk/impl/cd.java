package com.flurry.android.monolithic.sdk.impl;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public final class cd {
    private String a;
    private long b;
    private long c;
    private int d;
    private int e;
    private int f;
    private int g;

    public cd() {
    }

    public cd(String str, long j, long j2, int i, int i2, int i3) {
        this.a = str;
        this.b = j;
        this.c = j2;
        this.e = i;
        this.f = i2;
        this.g = i3;
        this.d = 0;
    }

    public cd(DataInput dataInput) throws IOException {
        this.a = dataInput.readUTF();
        this.b = dataInput.readLong();
        this.c = dataInput.readLong();
        this.d = dataInput.readInt();
        this.e = dataInput.readInt();
        this.f = dataInput.readInt();
        this.g = dataInput.readInt();
    }

    public void a(DataOutput dataOutput) throws IOException {
        dataOutput.writeUTF(this.a);
        dataOutput.writeLong(this.b);
        dataOutput.writeLong(this.c);
        dataOutput.writeInt(this.d);
        dataOutput.writeInt(this.e);
        dataOutput.writeInt(this.f);
        dataOutput.writeInt(this.g);
    }

    public cd a() {
        cd cdVar = new cd(b(), i(), h(), e(), f(), g());
        cdVar.a(c());
        return cdVar;
    }

    public String b() {
        return this.a;
    }

    public int c() {
        return this.d;
    }

    public void a(int i) {
        this.d = i;
    }

    public void d() {
        this.d++;
    }

    public int e() {
        return this.e;
    }

    public int f() {
        return this.f;
    }

    public int g() {
        return this.g;
    }

    public long h() {
        return this.c;
    }

    public long i() {
        return this.b;
    }
}
