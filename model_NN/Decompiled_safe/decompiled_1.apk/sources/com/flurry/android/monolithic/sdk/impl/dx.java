package com.flurry.android.monolithic.sdk.impl;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import com.flurry.android.AdCreative;
import com.flurry.android.AdNetworkView;
import com.flurry.android.impl.ads.FlurryAdModule;
import com.millennialmedia.android.MMAdView;
import com.millennialmedia.android.MMSDK;

public final class dx extends AdNetworkView {
    /* access modifiers changed from: private */
    public static final String a = dx.class.getSimpleName();
    private final String f;
    private final String g;

    dx(Context context, FlurryAdModule flurryAdModule, m mVar, AdCreative adCreative, Bundle bundle) {
        super(context, flurryAdModule, mVar, adCreative);
        this.f = bundle.getString("com.flurry.millennial.MYAPID");
        this.g = bundle.getString("com.flurry.millennial.MYAPIDRECTANGLE");
        setFocusable(true);
    }

    public void initLayout() {
        int i;
        int i2;
        Context context = getContext();
        int width = getAdCreative().getWidth();
        int height = getAdCreative().getHeight();
        ja.a(3, a, String.format("Ad space width: %d Ad space height: %d", Integer.valueOf(width), Integer.valueOf(height)));
        if ((width < 320 || height < 50) && (width < 300 || height < 250)) {
            ja.a(3, a, "**********Could not load Millennial Ad");
            return;
        }
        MMAdView mMAdView = new MMAdView((Activity) context);
        setId(MMSDK.getDefaultAdId());
        if (width < 320 || height < 50) {
            if (width >= 300 && height >= 250) {
                mMAdView.setApid(this.g);
                i2 = 250;
                i = 300;
            }
            i = 320;
            i2 = 50;
        } else {
            mMAdView.setApid(this.f);
            if (width < 728 || height < 90) {
                if (width >= 480 && height >= 60) {
                    i = 480;
                    i2 = 60;
                }
                i = 320;
                i2 = 50;
            } else {
                i = 728;
                i2 = 90;
            }
        }
        ja.a(3, a, String.format("Determined Millennial AdSize as %d x %d", Integer.valueOf(i), Integer.valueOf(i2)));
        mMAdView.setWidth(i);
        mMAdView.setHeight(i2);
        setHorizontalScrollBarEnabled(false);
        setVerticalScrollBarEnabled(false);
        setGravity(17);
        mMAdView.setListener(new dy(this));
        addView(mMAdView);
        mMAdView.getAd();
    }
}
