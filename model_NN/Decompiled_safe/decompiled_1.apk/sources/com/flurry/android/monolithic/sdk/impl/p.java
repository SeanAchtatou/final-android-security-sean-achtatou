package com.flurry.android.monolithic.sdk.impl;

class p extends jf {
    final /* synthetic */ String a;
    final /* synthetic */ o b;

    p(o oVar, String str) {
        this.b = oVar;
        this.a = str;
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x0102  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0110  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a() {
        /*
            r9 = this;
            com.flurry.android.impl.ads.FlurryAdModule r0 = com.flurry.android.impl.ads.FlurryAdModule.getInstance()
            boolean r0 = r0.I()
            if (r0 != 0) goto L_0x0034
            com.flurry.android.monolithic.sdk.impl.o r0 = r9.b
            android.app.ProgressDialog r0 = r0.h
            if (r0 == 0) goto L_0x0027
            com.flurry.android.monolithic.sdk.impl.o r0 = r9.b
            android.app.ProgressDialog r0 = r0.h
            boolean r0 = r0.isShowing()
            if (r0 == 0) goto L_0x0027
            com.flurry.android.monolithic.sdk.impl.o r0 = r9.b
            android.app.ProgressDialog r0 = r0.h
            r0.dismiss()
        L_0x0027:
            com.flurry.android.monolithic.sdk.impl.ia r0 = com.flurry.android.monolithic.sdk.impl.ia.a()
            com.flurry.android.monolithic.sdk.impl.q r1 = new com.flurry.android.monolithic.sdk.impl.q
            r1.<init>(r9)
            r0.a(r1)
        L_0x0033:
            return
        L_0x0034:
            r0 = 0
            org.apache.http.params.BasicHttpParams r1 = new org.apache.http.params.BasicHttpParams     // Catch:{ ClientProtocolException -> 0x00dc, IOException -> 0x00f3, all -> 0x010a }
            r1.<init>()     // Catch:{ ClientProtocolException -> 0x00dc, IOException -> 0x00f3, all -> 0x010a }
            org.apache.http.client.HttpClient r7 = com.flurry.android.monolithic.sdk.impl.iz.a(r1)     // Catch:{ ClientProtocolException -> 0x00dc, IOException -> 0x00f3, all -> 0x010a }
            org.apache.http.client.methods.HttpGet r0 = new org.apache.http.client.methods.HttpGet     // Catch:{ ClientProtocolException -> 0x0123, IOException -> 0x0120, all -> 0x0118 }
            java.lang.String r1 = r9.a     // Catch:{ ClientProtocolException -> 0x0123, IOException -> 0x0120, all -> 0x0118 }
            r0.<init>(r1)     // Catch:{ ClientProtocolException -> 0x0123, IOException -> 0x0120, all -> 0x0118 }
            org.apache.http.HttpResponse r0 = r7.execute(r0)     // Catch:{ ClientProtocolException -> 0x0123, IOException -> 0x0120, all -> 0x0118 }
            org.apache.http.StatusLine r1 = r0.getStatusLine()     // Catch:{ ClientProtocolException -> 0x0123, IOException -> 0x0120, all -> 0x0118 }
            int r1 = r1.getStatusCode()     // Catch:{ ClientProtocolException -> 0x0123, IOException -> 0x0120, all -> 0x0118 }
            org.apache.http.StatusLine r0 = r0.getStatusLine()     // Catch:{ ClientProtocolException -> 0x0123, IOException -> 0x0120, all -> 0x0118 }
            int r0 = r0.getStatusCode()     // Catch:{ ClientProtocolException -> 0x0123, IOException -> 0x0120, all -> 0x0118 }
            r2 = 200(0xc8, float:2.8E-43)
            if (r0 != r2) goto L_0x008b
            com.flurry.android.monolithic.sdk.impl.o r0 = r9.b     // Catch:{ ClientProtocolException -> 0x0123, IOException -> 0x0120, all -> 0x0118 }
            java.lang.String r1 = "rendered"
            java.util.Map r2 = java.util.Collections.emptyMap()     // Catch:{ ClientProtocolException -> 0x0123, IOException -> 0x0120, all -> 0x0118 }
            com.flurry.android.monolithic.sdk.impl.o r3 = r9.b     // Catch:{ ClientProtocolException -> 0x0123, IOException -> 0x0120, all -> 0x0118 }
            com.flurry.android.impl.ads.avro.protocol.v6.AdUnit r3 = r3.d     // Catch:{ ClientProtocolException -> 0x0123, IOException -> 0x0120, all -> 0x0118 }
            com.flurry.android.monolithic.sdk.impl.o r4 = r9.b     // Catch:{ ClientProtocolException -> 0x0123, IOException -> 0x0120, all -> 0x0118 }
            com.flurry.android.monolithic.sdk.impl.m r4 = r4.c     // Catch:{ ClientProtocolException -> 0x0123, IOException -> 0x0120, all -> 0x0118 }
            com.flurry.android.monolithic.sdk.impl.o r5 = r9.b     // Catch:{ ClientProtocolException -> 0x0123, IOException -> 0x0120, all -> 0x0118 }
            int r5 = r5.e     // Catch:{ ClientProtocolException -> 0x0123, IOException -> 0x0120, all -> 0x0118 }
            r6 = 0
            r0.a(r1, r2, r3, r4, r5, r6)     // Catch:{ ClientProtocolException -> 0x0123, IOException -> 0x0120, all -> 0x0118 }
            com.flurry.android.monolithic.sdk.impl.ia r0 = com.flurry.android.monolithic.sdk.impl.ia.a()     // Catch:{ ClientProtocolException -> 0x0123, IOException -> 0x0120, all -> 0x0118 }
            com.flurry.android.monolithic.sdk.impl.r r1 = new com.flurry.android.monolithic.sdk.impl.r     // Catch:{ ClientProtocolException -> 0x0123, IOException -> 0x0120, all -> 0x0118 }
            r1.<init>(r9)     // Catch:{ ClientProtocolException -> 0x0123, IOException -> 0x0120, all -> 0x0118 }
            r0.a(r1)     // Catch:{ ClientProtocolException -> 0x0123, IOException -> 0x0120, all -> 0x0118 }
            if (r7 == 0) goto L_0x0033
            org.apache.http.conn.ClientConnectionManager r0 = r7.getConnectionManager()
            r0.shutdown()
            goto L_0x0033
        L_0x008b:
            r0 = 4
            com.flurry.android.monolithic.sdk.impl.o r2 = r9.b     // Catch:{ ClientProtocolException -> 0x0123, IOException -> 0x0120, all -> 0x0118 }
            java.lang.String r2 = r2.f     // Catch:{ ClientProtocolException -> 0x0123, IOException -> 0x0120, all -> 0x0118 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x0123, IOException -> 0x0120, all -> 0x0118 }
            r3.<init>()     // Catch:{ ClientProtocolException -> 0x0123, IOException -> 0x0120, all -> 0x0118 }
            java.lang.String r4 = "http status code is:"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ ClientProtocolException -> 0x0123, IOException -> 0x0120, all -> 0x0118 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ ClientProtocolException -> 0x0123, IOException -> 0x0120, all -> 0x0118 }
            java.lang.String r1 = r1.toString()     // Catch:{ ClientProtocolException -> 0x0123, IOException -> 0x0120, all -> 0x0118 }
            com.flurry.android.monolithic.sdk.impl.ja.a(r0, r2, r1)     // Catch:{ ClientProtocolException -> 0x0123, IOException -> 0x0120, all -> 0x0118 }
            if (r7 == 0) goto L_0x00b1
            org.apache.http.conn.ClientConnectionManager r0 = r7.getConnectionManager()
            r0.shutdown()
        L_0x00b1:
            com.flurry.android.monolithic.sdk.impl.o r0 = r9.b
            android.app.ProgressDialog r0 = r0.h
            if (r0 == 0) goto L_0x00ce
            com.flurry.android.monolithic.sdk.impl.o r0 = r9.b
            android.app.ProgressDialog r0 = r0.h
            boolean r0 = r0.isShowing()
            if (r0 == 0) goto L_0x00ce
            com.flurry.android.monolithic.sdk.impl.o r0 = r9.b
            android.app.ProgressDialog r0 = r0.h
            r0.dismiss()
        L_0x00ce:
            com.flurry.android.monolithic.sdk.impl.ia r0 = com.flurry.android.monolithic.sdk.impl.ia.a()
            com.flurry.android.monolithic.sdk.impl.s r1 = new com.flurry.android.monolithic.sdk.impl.s
            r1.<init>(r9)
            r0.a(r1)
            goto L_0x0033
        L_0x00dc:
            r1 = move-exception
        L_0x00dd:
            r1 = 4
            com.flurry.android.monolithic.sdk.impl.o r2 = r9.b     // Catch:{ all -> 0x011b }
            java.lang.String r2 = r2.f     // Catch:{ all -> 0x011b }
            java.lang.String r3 = "client protocol exception..."
            com.flurry.android.monolithic.sdk.impl.ja.a(r1, r2, r3)     // Catch:{ all -> 0x011b }
            if (r0 == 0) goto L_0x00b1
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()
            r0.shutdown()
            goto L_0x00b1
        L_0x00f3:
            r1 = move-exception
        L_0x00f4:
            r1 = 4
            com.flurry.android.monolithic.sdk.impl.o r2 = r9.b     // Catch:{ all -> 0x011b }
            java.lang.String r2 = r2.f     // Catch:{ all -> 0x011b }
            java.lang.String r3 = "ioexception...."
            com.flurry.android.monolithic.sdk.impl.ja.a(r1, r2, r3)     // Catch:{ all -> 0x011b }
            if (r0 == 0) goto L_0x00b1
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()
            r0.shutdown()
            goto L_0x00b1
        L_0x010a:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x010e:
            if (r1 == 0) goto L_0x0117
            org.apache.http.conn.ClientConnectionManager r1 = r1.getConnectionManager()
            r1.shutdown()
        L_0x0117:
            throw r0
        L_0x0118:
            r0 = move-exception
            r1 = r7
            goto L_0x010e
        L_0x011b:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x010e
        L_0x0120:
            r0 = move-exception
            r0 = r7
            goto L_0x00f4
        L_0x0123:
            r0 = move-exception
            r0 = r7
            goto L_0x00dd
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.p.a():void");
    }
}
