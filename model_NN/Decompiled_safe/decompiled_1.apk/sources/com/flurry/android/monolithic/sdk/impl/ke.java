package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.io.StringReader;

public class ke {
    private kc a = new kc();
    private boolean b = true;

    public ji a(String str) {
        try {
            return a(ji.a.a(new StringReader(str)));
        } catch (IOException e) {
            throw new kl(e);
        }
    }

    private ji a(ow owVar) throws IOException {
        boolean booleanValue = ((Boolean) ji.k.get()).booleanValue();
        try {
            ji.k.set(Boolean.valueOf(this.b));
            ji a2 = ji.a(ji.b.a(owVar), this.a);
            ji.k.set(Boolean.valueOf(booleanValue));
            return a2;
        } catch (ov e) {
            throw new kl(e);
        } catch (Throwable th) {
            ji.k.set(Boolean.valueOf(booleanValue));
            throw th;
        }
    }
}
