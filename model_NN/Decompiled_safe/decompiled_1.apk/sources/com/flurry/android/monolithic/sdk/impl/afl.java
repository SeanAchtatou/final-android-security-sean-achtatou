package com.flurry.android.monolithic.sdk.impl;

public abstract class afl {
    protected final String a;
    protected final int b;

    public String toString() {
        return this.a;
    }

    public final int hashCode() {
        return this.b;
    }

    public boolean equals(Object obj) {
        return obj == this;
    }
}
