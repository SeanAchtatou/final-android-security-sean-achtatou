package com.flurry.android.monolithic.sdk.impl;

public class pw implements pe {
    protected final String a;
    protected char[] b;

    public pw(String str) {
        this.a = str;
    }

    public final String a() {
        return this.a;
    }

    public final char[] b() {
        char[] cArr = this.b;
        if (cArr != null) {
            return cArr;
        }
        char[] a2 = ps.a().a(this.a);
        this.b = a2;
        return a2;
    }

    public final String toString() {
        return this.a;
    }

    public final int hashCode() {
        return this.a.hashCode();
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        return this.a.equals(((pw) obj).a);
    }
}
