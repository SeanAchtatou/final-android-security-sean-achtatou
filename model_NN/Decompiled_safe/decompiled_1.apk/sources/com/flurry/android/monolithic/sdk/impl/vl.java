package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
final class vl extends ve<long[]> {
    public vl() {
        super(long[].class);
    }

    /* renamed from: b */
    public long[] a(ow owVar, qm qmVar) throws IOException, oz {
        if (!owVar.j()) {
            return c(owVar, qmVar);
        }
        adw e = qmVar.h().e();
        long[] jArr = (long[]) e.a();
        int i = 0;
        while (owVar.b() != pb.END_ARRAY) {
            long w = w(owVar, qmVar);
            if (i >= jArr.length) {
                jArr = (long[]) e.a(jArr, i);
                i = 0;
            }
            jArr[i] = w;
            i++;
        }
        return (long[]) e.b(jArr, i);
    }

    private final long[] c(ow owVar, qm qmVar) throws IOException, oz {
        if (owVar.e() == pb.VALUE_STRING && qmVar.a(ql.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT) && owVar.k().length() == 0) {
            return null;
        }
        if (!qmVar.a(ql.ACCEPT_SINGLE_VALUE_AS_ARRAY)) {
            throw qmVar.b(this.q);
        }
        return new long[]{w(owVar, qmVar)};
    }
}
