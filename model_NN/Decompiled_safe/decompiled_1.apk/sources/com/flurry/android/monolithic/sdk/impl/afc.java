package com.flurry.android.monolithic.sdk.impl;

public abstract class afc extends afg {
    public abstract double l();

    protected afc() {
    }

    public final boolean c() {
        return true;
    }

    public double a(double d) {
        return l();
    }
}
