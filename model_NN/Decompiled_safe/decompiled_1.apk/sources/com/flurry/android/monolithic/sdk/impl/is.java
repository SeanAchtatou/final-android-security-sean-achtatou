package com.flurry.android.monolithic.sdk.impl;

import java.lang.Thread;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

public final class is {
    private static is a;
    private final Thread.UncaughtExceptionHandler b = Thread.getDefaultUncaughtExceptionHandler();
    private final Map<Thread.UncaughtExceptionHandler, Void> c = new WeakHashMap();

    public static synchronized is a() {
        is isVar;
        synchronized (is.class) {
            if (a == null) {
                a = new is();
            }
            isVar = a;
        }
        return isVar;
    }

    public void a(Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
        synchronized (this.c) {
            this.c.put(uncaughtExceptionHandler, null);
        }
    }

    private Set<Thread.UncaughtExceptionHandler> b() {
        Set<Thread.UncaughtExceptionHandler> keySet;
        synchronized (this.c) {
            keySet = this.c.keySet();
        }
        return keySet;
    }

    private is() {
        Thread.setDefaultUncaughtExceptionHandler(new iu(this));
    }

    /* access modifiers changed from: private */
    public void a(Thread thread, Throwable th) {
        for (Thread.UncaughtExceptionHandler uncaughtException : b()) {
            try {
                uncaughtException.uncaughtException(thread, th);
            } catch (Throwable th2) {
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(Thread thread, Throwable th) {
        if (this.b != null) {
            this.b.uncaughtException(thread, th);
        }
    }
}
