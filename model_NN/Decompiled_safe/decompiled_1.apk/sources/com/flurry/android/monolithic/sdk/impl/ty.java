package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public class ty extends qd {
    protected final Object e;

    public ty(String str, afm afm, ado ado, xk xkVar, Object obj) {
        super(str, afm, ado, xkVar);
        this.e = obj;
    }

    public Object a(qm qmVar, Object obj) {
        return qmVar.a(this.e, this, obj);
    }

    public void b(qm qmVar, Object obj) throws IOException {
        this.c.a(obj, a(qmVar, obj));
    }
}
