package com.flurry.android.monolithic.sdk.impl;

import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class aen extends DateFormat {
    protected static final String[] a = {"yyyy-MM-dd'T'HH:mm:ss.SSSZ", "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "EEE, dd MMM yyyy HH:mm:ss zzz", "yyyy-MM-dd"};
    protected static final DateFormat b = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
    protected static final DateFormat c = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
    protected static final DateFormat d = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    protected static final DateFormat e = new SimpleDateFormat("yyyy-MM-dd");
    public static final aen f = new aen();
    protected transient DateFormat g;
    protected transient DateFormat h;
    protected transient DateFormat i;
    protected transient DateFormat j;

    static {
        TimeZone timeZone = TimeZone.getTimeZone("GMT");
        b.setTimeZone(timeZone);
        c.setTimeZone(timeZone);
        d.setTimeZone(timeZone);
        e.setTimeZone(timeZone);
    }

    /* renamed from: a */
    public aen clone() {
        return new aen();
    }

    public Date parse(String str) throws ParseException {
        String trim = str.trim();
        ParsePosition parsePosition = new ParsePosition(0);
        Date parse = parse(trim, parsePosition);
        if (parse != null) {
            return parse;
        }
        StringBuilder sb = new StringBuilder();
        for (String str2 : a) {
            if (sb.length() > 0) {
                sb.append("\", \"");
            } else {
                sb.append('\"');
            }
            sb.append(str2);
        }
        sb.append('\"');
        throw new ParseException(String.format("Can not parse date \"%s\": not compatible with any of standard forms (%s)", trim, sb.toString()), parsePosition.getErrorIndex());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.pt.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.pt.a(java.lang.String, double):double
      com.flurry.android.monolithic.sdk.impl.pt.a(java.lang.String, boolean):boolean */
    public Date parse(String str, ParsePosition parsePosition) {
        char charAt;
        if (a(str)) {
            return a(str, parsePosition);
        }
        int length = str.length();
        do {
            length--;
            if (length < 0 || (charAt = str.charAt(length)) < '0') {
                if (length < 0 || !pt.a(str, false)) {
                    return b(str, parsePosition);
                }
                return new Date(Long.parseLong(str));
            }
        } while (charAt <= '9');
        if (length < 0) {
        }
        return b(str, parsePosition);
    }

    public StringBuffer format(Date date, StringBuffer stringBuffer, FieldPosition fieldPosition) {
        if (this.h == null) {
            this.h = (DateFormat) c.clone();
        }
        return this.h.format(date, stringBuffer, fieldPosition);
    }

    /* access modifiers changed from: protected */
    public boolean a(String str) {
        if (str.length() < 5 || !Character.isDigit(str.charAt(0)) || !Character.isDigit(str.charAt(3)) || str.charAt(4) != '-') {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public Date a(String str, ParsePosition parsePosition) {
        DateFormat dateFormat;
        String str2;
        String str3;
        int length = str.length();
        char charAt = str.charAt(length - 1);
        if (length <= 10 && Character.isDigit(charAt)) {
            dateFormat = this.j;
            if (dateFormat == null) {
                dateFormat = (DateFormat) e.clone();
                this.j = dateFormat;
                str2 = str;
            }
            str2 = str;
        } else if (charAt == 'Z') {
            dateFormat = this.i;
            if (dateFormat == null) {
                dateFormat = (DateFormat) d.clone();
                this.i = dateFormat;
            }
            if (str.charAt(length - 4) == ':') {
                StringBuilder sb = new StringBuilder(str);
                sb.insert(length - 1, ".000");
                str2 = sb.toString();
            }
            str2 = str;
        } else if (b(str)) {
            char charAt2 = str.charAt(length - 3);
            if (charAt2 == ':') {
                StringBuilder sb2 = new StringBuilder(str);
                sb2.delete(length - 3, length - 2);
                str3 = sb2.toString();
            } else if (charAt2 == '+' || charAt2 == '-') {
                str3 = str + "00";
            } else {
                str3 = str;
            }
            int length2 = str3.length();
            if (Character.isDigit(str3.charAt(length2 - 9))) {
                StringBuilder sb3 = new StringBuilder(str3);
                sb3.insert(length2 - 5, ".000");
                str2 = sb3.toString();
            } else {
                str2 = str3;
            }
            dateFormat = this.h;
            if (this.h == null) {
                dateFormat = (DateFormat) c.clone();
                this.h = dateFormat;
            }
        } else {
            StringBuilder sb4 = new StringBuilder(str);
            if ((length - str.lastIndexOf(84)) - 1 <= 8) {
                sb4.append(".000");
            }
            sb4.append('Z');
            str2 = sb4.toString();
            dateFormat = this.i;
            if (dateFormat == null) {
                dateFormat = (DateFormat) d.clone();
                this.i = dateFormat;
            }
        }
        return dateFormat.parse(str2, parsePosition);
    }

    /* access modifiers changed from: protected */
    public Date b(String str, ParsePosition parsePosition) {
        if (this.g == null) {
            this.g = (DateFormat) b.clone();
        }
        return this.g.parse(str, parsePosition);
    }

    private static final boolean b(String str) {
        int length = str.length();
        if (length >= 6) {
            char charAt = str.charAt(length - 6);
            if (charAt == '+' || charAt == '-') {
                return true;
            }
            char charAt2 = str.charAt(length - 5);
            if (charAt2 == '+' || charAt2 == '-') {
                return true;
            }
            char charAt3 = str.charAt(length - 3);
            if (charAt3 == '+' || charAt3 == '-') {
                return true;
            }
        }
        return false;
    }
}
