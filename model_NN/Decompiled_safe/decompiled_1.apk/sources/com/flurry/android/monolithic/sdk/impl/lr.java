package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.nio.ByteBuffer;

public abstract class lr extends mc {
    /* access modifiers changed from: protected */
    public abstract void g() throws IOException;

    public void a() throws IOException {
    }

    public void a(nw nwVar) throws IOException {
        a(nwVar.a(), 0, nwVar.b());
    }

    public void a(String str) throws IOException {
        if (str.length() == 0) {
            g();
            return;
        }
        byte[] bytes = str.getBytes("UTF-8");
        c(bytes.length);
        b(bytes, 0, bytes.length);
    }

    public void a(ByteBuffer byteBuffer) throws IOException {
        int position = byteBuffer.position();
        a(byteBuffer.array(), byteBuffer.arrayOffset() + position, byteBuffer.limit() - position);
    }

    public void a(byte[] bArr, int i, int i2) throws IOException {
        if (i2 == 0) {
            g();
            return;
        }
        c(i2);
        b(bArr, i, i2);
    }

    public void a(int i) throws IOException {
        c(i);
    }

    public void b() throws IOException {
    }

    public void a(long j) throws IOException {
        if (j > 0) {
            b(j);
        }
    }

    public void c() throws IOException {
    }

    public void d() throws IOException {
        g();
    }

    public void e() throws IOException {
    }

    public void f() throws IOException {
        g();
    }

    public void b(int i) throws IOException {
        c(i);
    }
}
