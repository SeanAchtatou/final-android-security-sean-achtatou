package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class afx extends afw {
    protected final ow[] e;
    protected int f = 1;

    protected afx(ow[] owVarArr) {
        super(owVarArr[0]);
        this.e = owVarArr;
    }

    public static afx a(ow owVar, ow owVar2) {
        if ((owVar instanceof afx) || (owVar2 instanceof afx)) {
            ArrayList arrayList = new ArrayList();
            if (owVar instanceof afx) {
                ((afx) owVar).a(arrayList);
            } else {
                arrayList.add(owVar);
            }
            if (owVar2 instanceof afx) {
                ((afx) owVar2).a(arrayList);
            } else {
                arrayList.add(owVar2);
            }
            return new afx((ow[]) arrayList.toArray(new ow[arrayList.size()]));
        }
        return new afx(new ow[]{owVar, owVar2});
    }

    /* access modifiers changed from: protected */
    public void a(List<ow> list) {
        int length = this.e.length;
        for (int i = this.f - 1; i < length; i++) {
            ow owVar = this.e[i];
            if (owVar instanceof afx) {
                ((afx) owVar).a(list);
            } else {
                list.add(owVar);
            }
        }
    }

    public void close() throws IOException {
        do {
            this.d.close();
        } while (A());
    }

    public pb b() throws IOException, ov {
        pb b = this.d.b();
        if (b != null) {
            return b;
        }
        while (A()) {
            pb b2 = this.d.b();
            if (b2 != null) {
                return b2;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean A() {
        if (this.f >= this.e.length) {
            return false;
        }
        ow[] owVarArr = this.e;
        int i = this.f;
        this.f = i + 1;
        this.d = owVarArr[i];
        return true;
    }
}
