package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
public final class vt extends wa<Byte> {
    public vt(Class<Byte> cls, Byte b) {
        super(cls, b);
    }

    /* renamed from: b */
    public Byte a(ow owVar, qm qmVar) throws IOException, oz {
        return q(owVar, qmVar);
    }
}
