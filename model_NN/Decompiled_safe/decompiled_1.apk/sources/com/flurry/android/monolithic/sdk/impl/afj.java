package com.flurry.android.monolithic.sdk.impl;

import java.util.Arrays;

public final class afj {
    static final afj a = new afj();
    protected afj b;
    protected final boolean c;
    protected final boolean d;
    protected String[] e;
    protected afk[] f;
    protected int g;
    protected int h;
    protected int i;
    protected boolean j;

    public static afj a() {
        return a.e();
    }

    private afj() {
        this.d = true;
        this.c = true;
        this.j = true;
        a(64);
    }

    private void a(int i2) {
        this.e = new String[i2];
        this.f = new afk[(i2 >> 1)];
        this.i = i2 - 1;
        this.g = 0;
        this.h = i2 - (i2 >> 2);
    }

    private afj(afj afj, boolean z, boolean z2, String[] strArr, afk[] afkArr, int i2) {
        this.b = afj;
        this.d = z;
        this.c = z2;
        this.e = strArr;
        this.f = afkArr;
        this.g = i2;
        int length = strArr.length;
        this.h = length - (length >> 2);
        this.i = length - 1;
        this.j = false;
    }

    public synchronized afj a(boolean z, boolean z2) {
        return new afj(this, z, z2, this.e, this.f, this.g);
    }

    private afj e() {
        return new afj(null, true, true, this.e, this.f, this.g);
    }

    private synchronized void a(afj afj) {
        if (afj.c() > 12000) {
            a(64);
        } else if (afj.c() > c()) {
            this.e = afj.e;
            this.f = afj.f;
            this.g = afj.g;
            this.h = afj.h;
            this.i = afj.i;
        }
        this.j = false;
    }

    public void b() {
        if (d() && this.b != null) {
            this.b.a(this);
            this.j = false;
        }
    }

    public int c() {
        return this.g;
    }

    public boolean d() {
        return this.j;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x002e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String a(char[] r7, int r8, int r9, int r10) {
        /*
            r6 = this;
            r5 = 1
            if (r9 >= r5) goto L_0x0006
            java.lang.String r0 = ""
        L_0x0005:
            return r0
        L_0x0006:
            boolean r0 = r6.d
            if (r0 != 0) goto L_0x0010
            java.lang.String r0 = new java.lang.String
            r0.<init>(r7, r8, r9)
            goto L_0x0005
        L_0x0010:
            int r0 = r6.i
            r0 = r0 & r10
            java.lang.String[] r1 = r6.e
            r1 = r1[r0]
            if (r1 == 0) goto L_0x0043
            int r2 = r1.length()
            if (r2 != r9) goto L_0x0033
            r2 = 0
        L_0x0020:
            char r3 = r1.charAt(r2)
            int r4 = r8 + r2
            char r4 = r7[r4]
            if (r3 == r4) goto L_0x002e
        L_0x002a:
            if (r2 != r9) goto L_0x0033
            r0 = r1
            goto L_0x0005
        L_0x002e:
            int r2 = r2 + 1
            if (r2 < r9) goto L_0x0020
            goto L_0x002a
        L_0x0033:
            com.flurry.android.monolithic.sdk.impl.afk[] r1 = r6.f
            int r2 = r0 >> 1
            r1 = r1[r2]
            if (r1 == 0) goto L_0x0043
            java.lang.String r1 = r1.a(r7, r8, r9)
            if (r1 == 0) goto L_0x0043
            r0 = r1
            goto L_0x0005
        L_0x0043:
            boolean r1 = r6.j
            if (r1 != 0) goto L_0x006d
            r6.f()
            r6.j = r5
        L_0x004c:
            int r1 = r6.g
            int r1 = r1 + 1
            r6.g = r1
            java.lang.String r1 = new java.lang.String
            r1.<init>(r7, r8, r9)
            boolean r2 = r6.c
            if (r2 == 0) goto L_0x0061
            com.flurry.android.monolithic.sdk.impl.afv r2 = com.flurry.android.monolithic.sdk.impl.afv.a
            java.lang.String r1 = r2.a(r1)
        L_0x0061:
            java.lang.String[] r2 = r6.e
            r2 = r2[r0]
            if (r2 != 0) goto L_0x007e
            java.lang.String[] r2 = r6.e
            r2[r0] = r1
        L_0x006b:
            r0 = r1
            goto L_0x0005
        L_0x006d:
            int r1 = r6.g
            int r2 = r6.h
            if (r1 < r2) goto L_0x004c
            r6.g()
            int r0 = a(r7, r8, r9)
            int r1 = r6.i
            r0 = r0 & r1
            goto L_0x004c
        L_0x007e:
            int r0 = r0 >> 1
            com.flurry.android.monolithic.sdk.impl.afk[] r2 = r6.f
            com.flurry.android.monolithic.sdk.impl.afk r3 = new com.flurry.android.monolithic.sdk.impl.afk
            com.flurry.android.monolithic.sdk.impl.afk[] r4 = r6.f
            r4 = r4[r0]
            r3.<init>(r1, r4)
            r2[r0] = r3
            goto L_0x006b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.afj.a(char[], int, int, int):java.lang.String");
    }

    public static int a(char[] cArr, int i2, int i3) {
        int i4 = cArr[0];
        for (int i5 = 1; i5 < i3; i5++) {
            i4 = (i4 * 31) + cArr[i5];
        }
        return i4;
    }

    public static int a(String str) {
        char charAt = str.charAt(0);
        int length = str.length();
        int i2 = charAt;
        for (int i3 = 1; i3 < length; i3++) {
            i2 = (i2 * 31) + str.charAt(i3);
        }
        return i2;
    }

    private void f() {
        String[] strArr = this.e;
        int length = strArr.length;
        this.e = new String[length];
        System.arraycopy(strArr, 0, this.e, 0, length);
        afk[] afkArr = this.f;
        int length2 = afkArr.length;
        this.f = new afk[length2];
        System.arraycopy(afkArr, 0, this.f, 0, length2);
    }

    private void g() {
        int length = this.e.length;
        int i2 = length + length;
        if (i2 > 65536) {
            this.g = 0;
            Arrays.fill(this.e, (Object) null);
            Arrays.fill(this.f, (Object) null);
            this.j = true;
            return;
        }
        String[] strArr = this.e;
        afk[] afkArr = this.f;
        this.e = new String[i2];
        this.f = new afk[(i2 >> 1)];
        this.i = i2 - 1;
        this.h += this.h;
        int i3 = 0;
        for (int i4 = 0; i4 < length; i4++) {
            String str = strArr[i4];
            if (str != null) {
                i3++;
                int a2 = a(str) & this.i;
                if (this.e[a2] == null) {
                    this.e[a2] = str;
                } else {
                    int i5 = a2 >> 1;
                    this.f[i5] = new afk(str, this.f[i5]);
                }
            }
        }
        int i6 = length >> 1;
        int i7 = 0;
        int i8 = i3;
        while (i7 < i6) {
            int i9 = i8;
            for (afk afk = afkArr[i7]; afk != null; afk = afk.b()) {
                i9++;
                String a3 = afk.a();
                int a4 = a(a3) & this.i;
                if (this.e[a4] == null) {
                    this.e[a4] = a3;
                } else {
                    int i10 = a4 >> 1;
                    this.f[i10] = new afk(a3, this.f[i10]);
                }
            }
            i7++;
            i8 = i9;
        }
        if (i8 != this.g) {
            throw new Error("Internal error on SymbolTable.rehash(): had " + this.g + " entries; now have " + i8 + ".");
        }
    }
}
