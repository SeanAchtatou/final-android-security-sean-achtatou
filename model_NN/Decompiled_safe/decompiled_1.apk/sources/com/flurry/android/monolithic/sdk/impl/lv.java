package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.io.OutputStream;

class lv extends lu {
    private final OutputStream a;

    private lv(OutputStream outputStream) {
        this.a = outputStream;
    }

    /* access modifiers changed from: protected */
    public void a(byte[] bArr, int i, int i2) throws IOException {
        this.a.write(bArr, i, i2);
    }

    /* access modifiers changed from: protected */
    public void a() throws IOException {
        this.a.flush();
    }
}
