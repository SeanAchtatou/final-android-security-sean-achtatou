package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
final class vh extends ve<char[]> {
    public vh() {
        super(char[].class);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.on.a(byte[], boolean):java.lang.String
     arg types: [byte[], int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.on.a(java.lang.StringBuilder, int):void
      com.flurry.android.monolithic.sdk.impl.on.a(byte[], boolean):java.lang.String */
    /* renamed from: b */
    public char[] a(ow owVar, qm qmVar) throws IOException, oz {
        pb e = owVar.e();
        if (e == pb.VALUE_STRING) {
            char[] l = owVar.l();
            int n = owVar.n();
            int m = owVar.m();
            char[] cArr = new char[m];
            System.arraycopy(l, n, cArr, 0, m);
            return cArr;
        } else if (owVar.j()) {
            StringBuilder sb = new StringBuilder(64);
            while (true) {
                pb b = owVar.b();
                if (b == pb.END_ARRAY) {
                    return sb.toString().toCharArray();
                }
                if (b != pb.VALUE_STRING) {
                    throw qmVar.b(Character.TYPE);
                }
                String k = owVar.k();
                if (k.length() != 1) {
                    throw qw.a(owVar, "Can not convert a JSON String of length " + k.length() + " into a char element of char array");
                }
                sb.append(k.charAt(0));
            }
        } else {
            if (e == pb.VALUE_EMBEDDED_OBJECT) {
                Object z = owVar.z();
                if (z == null) {
                    return null;
                }
                if (z instanceof char[]) {
                    return (char[]) z;
                }
                if (z instanceof String) {
                    return ((String) z).toCharArray();
                }
                if (z instanceof byte[]) {
                    return oo.a().a((byte[]) z, false).toCharArray();
                }
            }
            throw qmVar.b(this.q);
        }
    }
}
