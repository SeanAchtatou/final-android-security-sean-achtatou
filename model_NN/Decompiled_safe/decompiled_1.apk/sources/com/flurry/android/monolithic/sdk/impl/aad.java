package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
public final class aad extends abm<Integer> {
    public aad() {
        super(Integer.class);
    }

    public void a(Integer num, or orVar, ru ruVar) throws IOException, oq {
        orVar.b(num.intValue());
    }
}
