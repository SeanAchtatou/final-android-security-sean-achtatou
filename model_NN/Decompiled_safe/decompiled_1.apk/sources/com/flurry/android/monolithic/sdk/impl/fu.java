package com.flurry.android.monolithic.sdk.impl;

import org.json.JSONException;
import org.json.JSONObject;

final class fu implements fr {
    final /* synthetic */ String a;
    final /* synthetic */ hz b;

    fu(String str, hz hzVar) {
        this.a = str;
        this.b = hzVar;
    }

    public void a(fq fqVar) throws Exception {
        if (fqVar != null) {
            try {
                if (fqVar.a()) {
                    JSONObject jSONObject = fqVar.b.getJSONArray("set").getJSONObject(0);
                    ft ftVar = new ft(jSONObject);
                    fqVar.a = ftVar;
                    ja.a(4, "AppCloudUser", "Login user" + fqVar.toString());
                    ft.a(ftVar);
                    String string = jSONObject.getString("_id");
                    String string2 = jSONObject.getString("username");
                    String string3 = jSONObject.getString("email");
                    String string4 = fqVar.b.getString("APPCLOUD_USER_SESSION");
                    fy.a(string);
                    fy.a(string2, string3, string4);
                    fy.b(this.a);
                    ftVar.d();
                    this.b.a(ftVar);
                    return;
                }
                this.b.a(new hy(fqVar.d(), fqVar.b()));
                ja.a(4, "AppCloudUser", "Login Fail: " + fqVar.b().toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            this.b.a(new hy(400, "Bad response"));
        }
    }
}
