package com.flurry.android.monolithic.sdk.impl;

import android.text.TextUtils;
import com.flurry.android.impl.appcloud.AppCloudModule;

public abstract class gb {
    protected gf a = null;

    /* access modifiers changed from: protected */
    public void a(String str, String str2, String str3) {
        b(str, str2, str3);
        if (AppCloudModule.getInstance().b() != null) {
            AppCloudModule.getInstance().b().a(str, str2, str3);
        }
    }

    /* access modifiers changed from: protected */
    public void b(String str, String str2, String str3) {
        if (ft.e() != null && TextUtils.equals(ft.e().a(), str)) {
            ft.e().a(str2, str3);
        }
    }

    public boolean a(String str, String str2, String str3, String str4) {
        return b(str, str2, str3, str4);
    }

    /* access modifiers changed from: protected */
    public boolean b(String str, String str2, String str3, String str4) {
        String c = c(str, str3, str4);
        ja.a(4, "CacheContentProvider", "value in cache = " + c + " ; new value = " + str2);
        if (str2.equals(c)) {
            return false;
        }
        a(str3, str, str2);
        this.a.a(str, str2, str3, str4);
        return true;
    }

    public String c(String str, String str2, String str3) {
        return this.a.a(str, str2, str3);
    }

    public hk a(String str) {
        return this.a.b(str);
    }

    public boolean b(String str) {
        return this.a.a(str);
    }
}
