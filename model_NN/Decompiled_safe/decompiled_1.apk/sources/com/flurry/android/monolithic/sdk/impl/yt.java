package com.flurry.android.monolithic.sdk.impl;

import java.util.Collection;
import java.util.EnumMap;
import java.util.EnumSet;

public class yt extends yz {
    public yt(afm afm, adk adk) {
        super(afm, adk);
    }

    public String a(Object obj) {
        return b(obj, obj.getClass());
    }

    public String a(Object obj, Class<?> cls) {
        return b(obj, cls);
    }

    public afm a(String str) {
        if (str.indexOf(60) > 0) {
            return adk.a(str);
        }
        try {
            return this.c.a(this.d, Class.forName(str, true, Thread.currentThread().getContextClassLoader()));
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException("Invalid type id '" + str + "' (for id type 'Id.class'): no such class found");
        } catch (Exception e2) {
            throw new IllegalArgumentException("Invalid type id '" + str + "' (for id type 'Id.class'): " + e2.getMessage(), e2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.adk.b(java.lang.Class<? extends java.util.Collection>, java.lang.Class<?>):com.flurry.android.monolithic.sdk.impl.add
     arg types: [java.lang.Class, java.lang.Class<? extends java.lang.Enum<?>>]
     candidates:
      com.flurry.android.monolithic.sdk.impl.adk.b(java.lang.reflect.Type, java.lang.Class<?>):com.flurry.android.monolithic.sdk.impl.ade
      com.flurry.android.monolithic.sdk.impl.adk.b(java.lang.reflect.Type, com.flurry.android.monolithic.sdk.impl.adj):com.flurry.android.monolithic.sdk.impl.afm
      com.flurry.android.monolithic.sdk.impl.adk.b(com.flurry.android.monolithic.sdk.impl.afm, java.lang.Class<?>):com.flurry.android.monolithic.sdk.impl.afm[]
      com.flurry.android.monolithic.sdk.impl.adk.b(java.lang.Class<? extends java.util.Collection>, java.lang.Class<?>):com.flurry.android.monolithic.sdk.impl.add */
    /* access modifiers changed from: protected */
    public final String b(Object obj, Class<?> cls) {
        Class<?> cls2;
        if (!Enum.class.isAssignableFrom(cls) || cls.isEnum()) {
            cls2 = cls;
        } else {
            cls2 = cls.getSuperclass();
        }
        String name = cls2.getName();
        if (name.startsWith("java.util")) {
            if (obj instanceof EnumSet) {
                return adk.a().b((Class<? extends Collection>) EnumSet.class, (Class<?>) adz.a((EnumSet) obj)).m();
            } else if (obj instanceof EnumMap) {
                return adk.a().a(EnumMap.class, adz.a((EnumMap) obj), Object.class).m();
            } else {
                String substring = name.substring(9);
                if ((substring.startsWith(".Arrays$") || substring.startsWith(".Collections$")) && name.indexOf("List") >= 0) {
                    return "java.util.ArrayList";
                }
            }
        } else if (name.indexOf(36) >= 0 && adz.b(cls2) != null && adz.b(this.d.p()) == null) {
            return this.d.p().getName();
        }
        return name;
    }
}
