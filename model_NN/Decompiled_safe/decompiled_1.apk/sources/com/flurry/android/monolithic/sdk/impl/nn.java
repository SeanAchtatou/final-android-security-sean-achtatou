package com.flurry.android.monolithic.sdk.impl;

import com.ideaworks3d.marmalade.s3eAndroidGooglePlayBilling.util.IabHelper;
import java.lang.reflect.Constructor;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;

public class nn extends kq {
    private static final nn a = new nn();
    private static final Class<?>[] c = new Class[0];
    private static final Class<?>[] d = {ji.class};
    private static final Map<Class, Constructor> e = new ConcurrentHashMap();
    private static final Class g = new no().getClass();
    private static final ji h = ji.a(kj.NULL);
    private final ClassLoader b;
    private Map<String, Class> f;
    private final WeakHashMap<Type, ji> i;

    protected nn() {
        this(nn.class.getClassLoader());
    }

    public nn(ClassLoader classLoader) {
        this.f = new ConcurrentHashMap();
        this.i = new WeakHashMap<>();
        this.b = classLoader;
    }

    public lw a(ji jiVar) {
        return new nr(jiVar, jiVar, this);
    }

    public static nn b() {
        return a;
    }

    /* access modifiers changed from: protected */
    public boolean f(Object obj) {
        return (obj instanceof Enum) || super.f(obj);
    }

    /* access modifiers changed from: protected */
    public ji g(Object obj) {
        return obj instanceof Enum ? a((Type) obj.getClass()) : super.g(obj);
    }

    public Class b(ji jiVar) {
        switch (np.a[jiVar.a().ordinal()]) {
            case 1:
            case 2:
            case 3:
                String g2 = jiVar.g();
                if (g2 == null) {
                    return null;
                }
                Class<?> cls = this.f.get(g2);
                if (cls == null) {
                    try {
                        cls = this.b.loadClass(c(jiVar));
                    } catch (ClassNotFoundException e2) {
                        cls = g;
                    }
                    this.f.put(g2, cls);
                }
                if (cls == g) {
                    return null;
                }
                return cls;
            case 4:
                return List.class;
            case 5:
                return Map.class;
            case 6:
                List<ji> k = jiVar.k();
                if (k.size() != 2 || !k.contains(h)) {
                    return Object.class;
                }
                return b(k.get(k.get(0).equals(h) ? 1 : 0));
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED /*7*/:
                if ("String".equals(jiVar.a("avro.java.string"))) {
                    return String.class;
                }
                return CharSequence.class;
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED /*8*/:
                return ByteBuffer.class;
            case 9:
                return Integer.TYPE;
            case 10:
                return Long.TYPE;
            case 11:
                return Float.TYPE;
            case 12:
                return Double.TYPE;
            case 13:
                return Boolean.TYPE;
            case 14:
                return Void.TYPE;
            default:
                throw new jg("Unknown type: " + jiVar);
        }
    }

    public static String c(ji jiVar) {
        String str;
        String f2 = jiVar.f();
        String d2 = jiVar.d();
        if (f2 == null || "".equals(f2)) {
            return d2;
        }
        if (f2.endsWith("$")) {
            str = "";
        } else {
            str = ".";
        }
        return f2 + str + d2;
    }

    public ji a(Type type) {
        ji jiVar = this.i.get(type);
        if (jiVar != null) {
            return jiVar;
        }
        ji a2 = a(type, new LinkedHashMap());
        this.i.put(type, a2);
        return a2;
    }

    /* access modifiers changed from: protected */
    public ji a(Type type, Map<String, ji> map) {
        ji jiVar;
        Class<CharSequence> cls = CharSequence.class;
        if (type instanceof Class) {
            Class<CharSequence> cls2 = CharSequence.class;
            if (cls.isAssignableFrom((Class) type)) {
                return ji.a(kj.STRING);
            }
        }
        if (type == ByteBuffer.class) {
            return ji.a(kj.BYTES);
        }
        if (type == Integer.class || type == Integer.TYPE) {
            return ji.a(kj.INT);
        }
        if (type == Long.class || type == Long.TYPE) {
            return ji.a(kj.LONG);
        }
        if (type == Float.class || type == Float.TYPE) {
            return ji.a(kj.FLOAT);
        }
        if (type == Double.class || type == Double.TYPE) {
            return ji.a(kj.DOUBLE);
        }
        if (type == Boolean.class || type == Boolean.TYPE) {
            return ji.a(kj.BOOLEAN);
        }
        if (type == Void.class || type == Void.TYPE) {
            return ji.a(kj.NULL);
        }
        if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            Class cls3 = (Class) parameterizedType.getRawType();
            Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
            if (Collection.class.isAssignableFrom(cls3)) {
                if (actualTypeArguments.length == 1) {
                    return ji.a(a(actualTypeArguments[0], map));
                }
                throw new jh("No array type specified.");
            } else if (!Map.class.isAssignableFrom(cls3)) {
                return a(cls3, map);
            } else {
                Type type2 = actualTypeArguments[0];
                Type type3 = actualTypeArguments[1];
                if (type instanceof Class) {
                    Class<CharSequence> cls4 = CharSequence.class;
                    if (cls.isAssignableFrom((Class) type)) {
                        return ji.b(a(type3, map));
                    }
                }
                throw new jh("Map key class not CharSequence: " + type2);
            }
        } else if (type instanceof Class) {
            Class cls5 = (Class) type;
            String name = cls5.getName();
            ji jiVar2 = map.get(name);
            if (jiVar2 == null) {
                try {
                    jiVar2 = (ji) cls5.getDeclaredField("SCHEMA$").get(null);
                    if (!name.equals(c(jiVar2))) {
                        jiVar = ji.f(jiVar2.toString().replace(jiVar2.f(), cls5.getPackage().getName()));
                        map.put(name, jiVar);
                        return jiVar;
                    }
                } catch (NoSuchFieldException e2) {
                    throw new jg("Not a Specific class: " + cls5);
                } catch (IllegalAccessException e3) {
                    throw new jg(e3);
                }
            }
            jiVar = jiVar2;
            map.put(name, jiVar);
            return jiVar;
        } else {
            throw new jh("Unknown type: " + type);
        }
    }

    /* access modifiers changed from: protected */
    public int a(Object obj, Object obj2, ji jiVar, boolean z) {
        switch (np.a[jiVar.a().ordinal()]) {
            case 3:
                if (obj instanceof Enum) {
                    return ((Enum) obj).ordinal() - ((Enum) obj2).ordinal();
                }
                break;
        }
        return super.a(obj, obj2, jiVar, z);
    }

    public static Object a(Class cls, ji jiVar) {
        boolean isAssignableFrom = nq.class.isAssignableFrom(cls);
        try {
            Constructor constructor = e.get(cls);
            if (constructor == null) {
                constructor = cls.getDeclaredConstructor(isAssignableFrom ? d : c);
                constructor.setAccessible(true);
                e.put(cls, constructor);
            }
            return constructor.newInstance(isAssignableFrom ? new Object[]{jiVar} : null);
        } catch (Exception e2) {
            throw new RuntimeException(e2);
        }
    }

    public Object c(Object obj, ji jiVar) {
        Class b2 = b().b(jiVar);
        if (b2 == null) {
            return super.c(obj, jiVar);
        }
        return b2.isInstance(obj) ? obj : a(b2, jiVar);
    }

    public Object d(Object obj, ji jiVar) {
        Class b2 = b().b(jiVar);
        if (b2 == null) {
            return super.d(obj, jiVar);
        }
        return b2.isInstance(obj) ? obj : a(b2, jiVar);
    }
}
