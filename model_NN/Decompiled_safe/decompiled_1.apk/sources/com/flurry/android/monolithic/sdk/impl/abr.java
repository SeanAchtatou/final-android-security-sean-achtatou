package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
public class abr extends zv<qy> {
    public static final abr a = new abr();

    protected abr() {
        super(qy.class);
    }

    public void a(qy qyVar, or orVar, ru ruVar) throws IOException, oq {
        qyVar.a(orVar, ruVar);
    }

    public final void a(qy qyVar, or orVar, ru ruVar, rx rxVar) throws IOException, oq {
        if (qyVar instanceof qz) {
            ((qz) qyVar).a(orVar, ruVar, rxVar);
        } else {
            a(qyVar, orVar, ruVar);
        }
    }
}
