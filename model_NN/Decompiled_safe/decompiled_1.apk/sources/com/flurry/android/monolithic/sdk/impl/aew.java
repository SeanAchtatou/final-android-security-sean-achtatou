package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.math.BigDecimal;

public final class aew extends afc {
    protected final BigDecimal c;

    public aew(BigDecimal bigDecimal) {
        this.c = bigDecimal;
    }

    public static aew a(BigDecimal bigDecimal) {
        return new aew(bigDecimal);
    }

    public int j() {
        return this.c.intValue();
    }

    public long k() {
        return this.c.longValue();
    }

    public double l() {
        return this.c.doubleValue();
    }

    public String m() {
        return this.c.toString();
    }

    public final void a(or orVar, ru ruVar) throws IOException, oz {
        orVar.a(this.c);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        return ((aew) obj).c.equals(this.c);
    }

    public int hashCode() {
        return this.c.hashCode();
    }
}
