package com.flurry.android.monolithic.sdk.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.http.util.ByteArrayBuffer;

public class oc {
    private static final ByteArrayBuffer a = a(of.a, ": ");
    private static final ByteArrayBuffer b = a(of.a, "\r\n");
    private static final ByteArrayBuffer c = a(of.a, "--");
    private final String d;
    private final Charset e;
    private final String f;
    private final List<oa> g;
    private final oe h;

    private static ByteArrayBuffer a(Charset charset, String str) {
        ByteBuffer encode = charset.encode(CharBuffer.wrap(str));
        ByteArrayBuffer byteArrayBuffer = new ByteArrayBuffer(encode.remaining());
        byteArrayBuffer.append(encode.array(), encode.position(), encode.remaining());
        return byteArrayBuffer;
    }

    private static void a(ByteArrayBuffer byteArrayBuffer, OutputStream outputStream) throws IOException {
        outputStream.write(byteArrayBuffer.buffer(), 0, byteArrayBuffer.length());
    }

    private static void a(String str, Charset charset, OutputStream outputStream) throws IOException {
        a(a(charset, str), outputStream);
    }

    private static void a(String str, OutputStream outputStream) throws IOException {
        a(a(of.a, str), outputStream);
    }

    private static void a(og ogVar, OutputStream outputStream) throws IOException {
        a(ogVar.a(), outputStream);
        a(a, outputStream);
        a(ogVar.b(), outputStream);
        a(b, outputStream);
    }

    private static void a(og ogVar, Charset charset, OutputStream outputStream) throws IOException {
        a(ogVar.a(), charset, outputStream);
        a(a, outputStream);
        a(ogVar.b(), charset, outputStream);
        a(b, outputStream);
    }

    public oc(String str, Charset charset, String str2, oe oeVar) {
        if (str == null) {
            throw new IllegalArgumentException("Multipart subtype may not be null");
        } else if (str2 == null) {
            throw new IllegalArgumentException("Multipart boundary may not be null");
        } else {
            this.d = str;
            this.e = charset != null ? charset : of.a;
            this.f = str2;
            this.g = new ArrayList();
            this.h = oeVar;
        }
    }

    public List<oa> a() {
        return this.g;
    }

    public void a(oa oaVar) {
        if (oaVar != null) {
            this.g.add(oaVar);
        }
    }

    public String b() {
        return this.f;
    }

    private void a(oe oeVar, OutputStream outputStream, boolean z) throws IOException {
        ByteArrayBuffer a2 = a(this.e, b());
        for (oa next : this.g) {
            a(c, outputStream);
            a(a2, outputStream);
            a(b, outputStream);
            ob c2 = next.c();
            switch (od.a[oeVar.ordinal()]) {
                case 1:
                    Iterator<og> it = c2.iterator();
                    while (it.hasNext()) {
                        a(it.next(), outputStream);
                    }
                    break;
                case 2:
                    a(next.c().a("Content-Disposition"), this.e, outputStream);
                    if (next.b().b() != null) {
                        a(next.c().a("Content-Type"), this.e, outputStream);
                        break;
                    }
                    break;
            }
            a(b, outputStream);
            if (z) {
                next.b().a(outputStream);
            }
            a(b, outputStream);
        }
        a(c, outputStream);
        a(a2, outputStream);
        a(c, outputStream);
        a(b, outputStream);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.oc.a(com.flurry.android.monolithic.sdk.impl.oe, java.io.OutputStream, boolean):void
     arg types: [com.flurry.android.monolithic.sdk.impl.oe, java.io.OutputStream, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.oc.a(com.flurry.android.monolithic.sdk.impl.og, java.nio.charset.Charset, java.io.OutputStream):void
      com.flurry.android.monolithic.sdk.impl.oc.a(java.lang.String, java.nio.charset.Charset, java.io.OutputStream):void
      com.flurry.android.monolithic.sdk.impl.oc.a(com.flurry.android.monolithic.sdk.impl.oe, java.io.OutputStream, boolean):void */
    public void a(OutputStream outputStream) throws IOException {
        a(this.h, outputStream, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.oc.a(com.flurry.android.monolithic.sdk.impl.oe, java.io.OutputStream, boolean):void
     arg types: [com.flurry.android.monolithic.sdk.impl.oe, java.io.ByteArrayOutputStream, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.oc.a(com.flurry.android.monolithic.sdk.impl.og, java.nio.charset.Charset, java.io.OutputStream):void
      com.flurry.android.monolithic.sdk.impl.oc.a(java.lang.String, java.nio.charset.Charset, java.io.OutputStream):void
      com.flurry.android.monolithic.sdk.impl.oc.a(com.flurry.android.monolithic.sdk.impl.oe, java.io.OutputStream, boolean):void */
    public long c() {
        long j = 0;
        for (oa b2 : this.g) {
            long e2 = b2.b().e();
            if (e2 < 0) {
                return -1;
            }
            j += e2;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            a(this.h, (OutputStream) byteArrayOutputStream, false);
            return ((long) byteArrayOutputStream.toByteArray().length) + j;
        } catch (IOException e3) {
            return -1;
        }
    }
}
