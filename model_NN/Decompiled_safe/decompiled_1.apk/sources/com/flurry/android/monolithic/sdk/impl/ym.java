package com.flurry.android.monolithic.sdk.impl;

import com.flurry.org.codehaus.jackson.annotate.JsonTypeInfo;

public class ym extends yk {
    protected final String a;

    public ym(afm afm, yi yiVar, qc qcVar, Class<?> cls, String str) {
        super(afm, yiVar, qcVar, cls);
        this.a = str;
    }

    public JsonTypeInfo.As a() {
        return JsonTypeInfo.As.EXTERNAL_PROPERTY;
    }

    public String b() {
        return this.a;
    }
}
