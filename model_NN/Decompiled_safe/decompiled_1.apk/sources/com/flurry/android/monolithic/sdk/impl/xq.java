package com.flurry.android.monolithic.sdk.impl;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class xq extends qb {
    protected final rf<?> b;
    protected final py c;
    protected final xh d;
    protected adj e;
    protected final List<qe> f;
    protected xl g;
    protected Map<Object, xk> h;
    protected Set<String> i;
    protected Set<String> j;
    protected xl k;
    protected xl l;

    protected xq(rf<?> rfVar, afm afm, xh xhVar, List<qe> list) {
        super(afm);
        this.b = rfVar;
        this.c = rfVar == null ? null : rfVar.a();
        this.d = xhVar;
        this.f = list;
    }

    public static xq a(yb ybVar) {
        xq xqVar = new xq(ybVar.a(), ybVar.b(), ybVar.c(), ybVar.d());
        xqVar.g = ybVar.h();
        xqVar.i = ybVar.i();
        xqVar.j = ybVar.j();
        xqVar.h = ybVar.e();
        return xqVar;
    }

    public static xq b(yb ybVar) {
        xq xqVar = new xq(ybVar.a(), ybVar.b(), ybVar.c(), ybVar.d());
        xqVar.k = ybVar.f();
        xqVar.l = ybVar.g();
        return xqVar;
    }

    public static xq a(rf<?> rfVar, afm afm, xh xhVar) {
        return new xq(rfVar, afm, xhVar, Collections.emptyList());
    }

    public xh c() {
        return this.d;
    }

    public List<qe> d() {
        return this.f;
    }

    public xl e() {
        return this.k;
    }

    public Set<String> f() {
        if (this.i == null) {
            return Collections.emptySet();
        }
        return this.i;
    }

    public Set<String> g() {
        return this.j;
    }

    public boolean h() {
        return this.d.g();
    }

    public ado i() {
        return this.d.f();
    }

    public adj j() {
        if (this.e == null) {
            this.e = new adj(this.b.m(), this.a);
        }
        return this.e;
    }

    public afm a(Type type) {
        if (type == null) {
            return null;
        }
        return j().a(type);
    }

    public xi k() {
        return this.d.h();
    }

    public xl l() throws IllegalArgumentException {
        Class<?> a;
        if (this.g == null || (a = this.g.a(0)) == String.class || a == Object.class) {
            return this.g;
        }
        throw new IllegalArgumentException("Invalid 'any-setter' annotation on method " + this.g.b() + "(): first argument not of type String or Object, but " + a.getName());
    }

    public Map<Object, xk> m() {
        return this.h;
    }

    public List<xi> n() {
        return this.d.i();
    }

    public xl a(String str, Class<?>[] clsArr) {
        return this.d.a(str, clsArr);
    }

    public Object a(boolean z) {
        xi h2 = this.d.h();
        if (h2 == null) {
            return null;
        }
        if (z) {
            h2.k();
        }
        try {
            return h2.a().newInstance(new Object[0]);
        } catch (Exception e2) {
            e = e2;
            while (e.getCause() != null) {
                e = e.getCause();
            }
            if (e instanceof Error) {
                throw ((Error) e);
            } else if (e instanceof RuntimeException) {
                throw ((RuntimeException) e);
            } else {
                throw new IllegalArgumentException("Failed to instantiate bean of type " + this.d.a().getName() + ": (" + e.getClass().getName() + ") " + e.getMessage(), e);
            }
        }
    }

    public List<xl> o() {
        List<xl> j2 = this.d.j();
        if (j2.isEmpty()) {
            return j2;
        }
        ArrayList arrayList = new ArrayList();
        for (xl next : j2) {
            if (a(next)) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public Constructor<?> a(Class<?>... clsArr) {
        for (xi next : this.d.i()) {
            if (next.f() == 1) {
                Class<?> a = next.a(0);
                for (Class<?> cls : clsArr) {
                    if (cls == a) {
                        return next.a();
                    }
                }
                continue;
            }
        }
        return null;
    }

    public Method b(Class<?>... clsArr) {
        for (xl next : this.d.j()) {
            if (a(next)) {
                Class<?> a = next.a(0);
                for (Class<?> isAssignableFrom : clsArr) {
                    if (a.isAssignableFrom(isAssignableFrom)) {
                        return next.a();
                    }
                }
                continue;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean a(xl xlVar) {
        if (!b().isAssignableFrom(xlVar.d())) {
            return false;
        }
        if (this.c.k(xlVar)) {
            return true;
        }
        if ("valueOf".equals(xlVar.b())) {
            return true;
        }
        return false;
    }

    public sf a(sf sfVar) {
        if (this.c == null) {
            return sfVar;
        }
        return this.c.a(this.d, sfVar);
    }

    public xl p() throws IllegalArgumentException {
        if (this.l != null) {
            if (!Map.class.isAssignableFrom(this.l.d())) {
                throw new IllegalArgumentException("Invalid 'any-getter' annotation on method " + this.l.b() + "(): return type is not instance of java.util.Map");
            }
        }
        return this.l;
    }

    public Map<String, xk> q() {
        HashMap hashMap = null;
        for (qe k2 : this.f) {
            xk k3 = k2.k();
            if (k3 != null) {
                pz a = this.c.a(k3);
                if (a != null && a.c()) {
                    if (hashMap == null) {
                        hashMap = new HashMap();
                    }
                    String a2 = a.a();
                    if (hashMap.put(a2, k3) != null) {
                        throw new IllegalArgumentException("Multiple back-reference properties with name '" + a2 + "'");
                    }
                }
                hashMap = hashMap;
            }
        }
        return hashMap;
    }
}
