package com.flurry.android.monolithic.sdk.impl;

class fp implements fr {
    final /* synthetic */ hx a;
    final /* synthetic */ fm b;

    fp(fm fmVar, hx hxVar) {
        this.b = fmVar;
        this.a = hxVar;
    }

    public void a(fq fqVar) throws Exception {
        if (fqVar == null) {
            this.a.a(new hy(400, "Bad response"));
        } else if (fqVar.a()) {
            this.b.a((fr) null);
            this.a.a();
        } else {
            this.a.a(new hy(fqVar.d(), fqVar.b()));
        }
    }
}
