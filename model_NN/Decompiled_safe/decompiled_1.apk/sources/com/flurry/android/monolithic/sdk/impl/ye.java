package com.flurry.android.monolithic.sdk.impl;

import com.flurry.android.monolithic.sdk.impl.ye;
import com.flurry.org.codehaus.jackson.annotate.JsonAutoDetect;

public interface ye<T extends ye<T>> {
    T a(JsonAutoDetect.Visibility visibility);

    T a(JsonAutoDetect jsonAutoDetect);

    boolean a(xj xjVar);

    boolean a(xk xkVar);

    boolean a(xl xlVar);

    T b(JsonAutoDetect.Visibility visibility);

    boolean b(xl xlVar);

    T c(JsonAutoDetect.Visibility visibility);

    boolean c(xl xlVar);

    T d(JsonAutoDetect.Visibility visibility);

    T e(JsonAutoDetect.Visibility visibility);
}
