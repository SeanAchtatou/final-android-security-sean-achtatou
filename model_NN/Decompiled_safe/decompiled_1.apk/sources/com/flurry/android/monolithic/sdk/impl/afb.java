package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import v2.com.playhaven.model.PHContent;

public final class afb extends afg {
    public static final afb c = new afb();

    private afb() {
    }

    public static afb r() {
        return c;
    }

    public boolean g() {
        return true;
    }

    public String m() {
        return PHContent.PARCEL_NULL;
    }

    public double a(double d) {
        return 0.0d;
    }

    public final void a(or orVar, ru ruVar) throws IOException, oz {
        orVar.f();
    }

    public boolean equals(Object obj) {
        return obj == this;
    }
}
