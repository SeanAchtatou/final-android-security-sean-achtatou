package com.flurry.android.monolithic.sdk.impl;

public final class pt {
    static final String a = String.valueOf(Long.MIN_VALUE).substring(1);
    static final String b = String.valueOf(Long.MAX_VALUE);

    public static final int a(char[] cArr, int i, int i2) {
        int i3 = cArr[i] - '0';
        int i4 = i2 + i;
        int i5 = i + 1;
        if (i5 >= i4) {
            return i3;
        }
        int i6 = (i3 * 10) + (cArr[i5] - '0');
        int i7 = i5 + 1;
        if (i7 >= i4) {
            return i6;
        }
        int i8 = (i6 * 10) + (cArr[i7] - '0');
        int i9 = i7 + 1;
        if (i9 >= i4) {
            return i8;
        }
        int i10 = (i8 * 10) + (cArr[i9] - '0');
        int i11 = i9 + 1;
        if (i11 >= i4) {
            return i10;
        }
        int i12 = (i10 * 10) + (cArr[i11] - '0');
        int i13 = i11 + 1;
        if (i13 >= i4) {
            return i12;
        }
        int i14 = (i12 * 10) + (cArr[i13] - '0');
        int i15 = i13 + 1;
        if (i15 >= i4) {
            return i14;
        }
        int i16 = (i14 * 10) + (cArr[i15] - '0');
        int i17 = i15 + 1;
        if (i17 >= i4) {
            return i16;
        }
        int i18 = (i16 * 10) + (cArr[i17] - '0');
        int i19 = i17 + 1;
        if (i19 < i4) {
            return (i18 * 10) + (cArr[i19] - '0');
        }
        return i18;
    }

    public static final int a(String str) {
        char c;
        int i;
        int i2;
        char charAt = str.charAt(0);
        int length = str.length();
        boolean z = charAt == '-';
        if (z) {
            if (length == 1 || length > 10) {
                return Integer.parseInt(str);
            }
            i = 1 + 1;
            c = str.charAt(1);
        } else if (length > 9) {
            return Integer.parseInt(str);
        } else {
            c = charAt;
            i = 1;
        }
        if (c > '9' || c < '0') {
            return Integer.parseInt(str);
        }
        int i3 = c - '0';
        if (i < length) {
            int i4 = i + 1;
            char charAt2 = str.charAt(i);
            if (charAt2 > '9' || charAt2 < '0') {
                return Integer.parseInt(str);
            }
            i2 = (charAt2 - '0') + (i3 * 10);
            if (i4 < length) {
                int i5 = i4 + 1;
                char charAt3 = str.charAt(i4);
                if (charAt3 > '9' || charAt3 < '0') {
                    return Integer.parseInt(str);
                }
                i2 = (i2 * 10) + (charAt3 - '0');
                if (i5 < length) {
                    while (true) {
                        int i6 = i5 + 1;
                        char charAt4 = str.charAt(i5);
                        if (charAt4 <= '9' && charAt4 >= '0') {
                            i2 = (i2 * 10) + (charAt4 - '0');
                            if (i6 >= length) {
                                break;
                            }
                            i5 = i6;
                        }
                    }
                    return Integer.parseInt(str);
                }
            }
        } else {
            i2 = i3;
        }
        return z ? -i2 : i2;
    }

    public static final long b(char[] cArr, int i, int i2) {
        int i3 = i2 - 9;
        return (((long) a(cArr, i, i3)) * 1000000000) + ((long) a(cArr, i3 + i, 9));
    }

    public static final long b(String str) {
        if (str.length() <= 9) {
            return (long) a(str);
        }
        return Long.parseLong(str);
    }

    public static final boolean a(char[] cArr, int i, int i2, boolean z) {
        String str = z ? a : b;
        int length = str.length();
        if (i2 < length) {
            return true;
        }
        if (i2 > length) {
            return false;
        }
        for (int i3 = 0; i3 < length; i3++) {
            int charAt = cArr[i + i3] - str.charAt(i3);
            if (charAt != 0) {
                return charAt < 0;
            }
        }
        return true;
    }

    public static final boolean a(String str, boolean z) {
        String str2 = z ? a : b;
        int length = str2.length();
        int length2 = str.length();
        if (length2 < length) {
            return true;
        }
        if (length2 > length) {
            return false;
        }
        for (int i = 0; i < length; i++) {
            int charAt = str.charAt(i) - str2.charAt(i);
            if (charAt != 0) {
                return charAt < 0;
            }
        }
        return true;
    }

    public static double a(String str, double d) {
        if (str == null) {
            return d;
        }
        String trim = str.trim();
        if (trim.length() == 0) {
            return d;
        }
        try {
            return c(trim);
        } catch (NumberFormatException e) {
            return d;
        }
    }

    public static final double c(String str) throws NumberFormatException {
        if ("2.2250738585072012e-308".equals(str)) {
            return Double.MIN_NORMAL;
        }
        return Double.parseDouble(str);
    }
}
