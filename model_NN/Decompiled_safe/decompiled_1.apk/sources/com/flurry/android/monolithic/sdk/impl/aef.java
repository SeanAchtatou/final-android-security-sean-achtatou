package com.flurry.android.monolithic.sdk.impl;

import java.util.LinkedHashMap;
import java.util.Map;

public class aef<K, V> extends LinkedHashMap<K, V> {
    protected final int a;

    public aef(int i, int i2) {
        super(i, 0.8f, true);
        this.a = i2;
    }

    /* access modifiers changed from: protected */
    public boolean removeEldestEntry(Map.Entry<K, V> entry) {
        return size() > this.a;
    }
}
