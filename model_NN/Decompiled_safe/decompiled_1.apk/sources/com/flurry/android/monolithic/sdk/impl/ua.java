package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;

public class ua extends wv<AtomicReference<?>> implements ro {
    protected final afm a;
    protected final qc b;
    protected qu<?> c;

    public ua(afm afm, qc qcVar) {
        super(AtomicReference.class);
        this.a = afm;
        this.b = qcVar;
    }

    /* renamed from: b */
    public AtomicReference<?> a(ow owVar, qm qmVar) throws IOException, oz {
        return new AtomicReference<>(this.c.a(owVar, qmVar));
    }

    public void a(qk qkVar, qq qqVar) throws qw {
        this.c = qqVar.a(qkVar, this.a, this.b);
    }
}
