package com.flurry.android.monolithic.sdk.impl;

import v2.com.playhaven.model.PHContent;

public final class yg {
    protected final Class<?> a;
    protected final int b;
    protected String c;

    public yg(Class<?> cls, String str) {
        this.a = cls;
        this.b = cls.getName().hashCode();
        a(str);
    }

    public Class<?> a() {
        return this.a;
    }

    public String b() {
        return this.c;
    }

    public void a(String str) {
        String str2;
        if (str == null || str.length() == 0) {
            str2 = null;
        } else {
            str2 = str;
        }
        this.c = str2;
    }

    public boolean c() {
        return this.c != null;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        return this.a == ((yg) obj).a;
    }

    public int hashCode() {
        return this.b;
    }

    public String toString() {
        return "[NamedType, class " + this.a.getName() + ", name: " + (this.c == null ? PHContent.PARCEL_NULL : "'" + this.c + "'") + "]";
    }
}
