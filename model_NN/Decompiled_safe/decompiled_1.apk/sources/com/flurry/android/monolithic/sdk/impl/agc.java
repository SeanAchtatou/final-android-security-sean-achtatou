package com.flurry.android.monolithic.sdk.impl;

public final class agc {
    private static final pb[] d = new pb[16];
    protected agc a;
    protected long b;
    protected final Object[] c = new Object[16];

    static {
        pb[] values = pb.values();
        System.arraycopy(values, 1, d, 1, Math.min(15, values.length - 1));
    }

    public pb a(int i) {
        long j = this.b;
        if (i > 0) {
            j >>= i << 2;
        }
        return d[((int) j) & 15];
    }

    public Object b(int i) {
        return this.c[i];
    }

    public agc a() {
        return this.a;
    }

    public agc a(int i, pb pbVar) {
        if (i < 16) {
            b(i, pbVar);
            return null;
        }
        this.a = new agc();
        this.a.b(0, pbVar);
        return this.a;
    }

    public agc a(int i, pb pbVar, Object obj) {
        if (i < 16) {
            b(i, pbVar, obj);
            return null;
        }
        this.a = new agc();
        this.a.b(0, pbVar, obj);
        return this.a;
    }

    public void b(int i, pb pbVar) {
        long ordinal = (long) pbVar.ordinal();
        if (i > 0) {
            ordinal <<= i << 2;
        }
        this.b = ordinal | this.b;
    }

    public void b(int i, pb pbVar, Object obj) {
        this.c[i] = obj;
        long ordinal = (long) pbVar.ordinal();
        if (i > 0) {
            ordinal <<= i << 2;
        }
        this.b = ordinal | this.b;
    }
}
