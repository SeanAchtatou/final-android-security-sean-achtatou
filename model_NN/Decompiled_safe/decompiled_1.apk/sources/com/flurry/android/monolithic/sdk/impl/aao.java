package com.flurry.android.monolithic.sdk.impl;

final class aao extends aal {
    private final aar[] a;

    public aao(aar[] aarArr) {
        this.a = aarArr;
    }

    public ra<Object> a(Class<?> cls) {
        for (aar aar : this.a) {
            if (aar.a == cls) {
                return aar.b;
            }
        }
        return null;
    }

    public aal a(Class<?> cls, ra<Object> raVar) {
        int length = this.a.length;
        if (length == 8) {
            return this;
        }
        aar[] aarArr = new aar[(length + 1)];
        System.arraycopy(this.a, 0, aarArr, 0, length);
        aarArr[length] = new aar(cls, raVar);
        return new aao(aarArr);
    }
}
