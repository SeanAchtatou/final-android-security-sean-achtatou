package com.flurry.android.monolithic.sdk.impl;

import android.view.View;
import android.webkit.WebChromeClient;

final class at extends WebChromeClient {
    final /* synthetic */ ar a;

    private at(ar arVar) {
        this.a = arVar;
    }

    public void onShowCustomView(View view, WebChromeClient.CustomViewCallback customViewCallback) {
        ja.a(3, this.a.a, "onShowCustomView(7)");
        boolean unused = this.a.e = true;
        if (this.a.n != null) {
            this.a.n.a(this.a, view, new ba(this.a, customViewCallback));
        }
    }

    public void onShowCustomView(View view, int i, WebChromeClient.CustomViewCallback customViewCallback) {
        ja.a(3, this.a.a, "onShowCustomView(14)");
        boolean unused = this.a.e = true;
        if (this.a.n != null) {
            this.a.n.a(this.a, view, i, new ba(this.a, customViewCallback));
        }
    }

    public void onHideCustomView() {
        ja.a(3, this.a.a, "onHideCustomView()");
        if (this.a.n != null) {
            this.a.n.a(this.a);
        }
        boolean unused = this.a.e = false;
    }
}
