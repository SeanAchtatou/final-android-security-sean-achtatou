package com.flurry.android.monolithic.sdk.impl;

final class afk {
    private final String a;
    private final afk b;

    public afk(String str, afk afk) {
        this.a = str;
        this.b = afk;
    }

    public String a() {
        return this.a;
    }

    public afk b() {
        return this.b;
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x001c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String a(char[] r7, int r8, int r9) {
        /*
            r6 = this;
            java.lang.String r0 = r6.a
            com.flurry.android.monolithic.sdk.impl.afk r1 = r6.b
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0007:
            int r2 = r1.length()
            if (r2 != r9) goto L_0x0021
            r2 = 0
        L_0x000e:
            char r3 = r1.charAt(r2)
            int r4 = r8 + r2
            char r4 = r7[r4]
            if (r3 == r4) goto L_0x001c
        L_0x0018:
            if (r2 != r9) goto L_0x0021
            r0 = r1
        L_0x001b:
            return r0
        L_0x001c:
            int r2 = r2 + 1
            if (r2 < r9) goto L_0x000e
            goto L_0x0018
        L_0x0021:
            if (r0 != 0) goto L_0x0025
            r0 = 0
            goto L_0x001b
        L_0x0025:
            java.lang.String r1 = r0.a()
            com.flurry.android.monolithic.sdk.impl.afk r0 = r0.b()
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.afk.a(char[], int, int):java.lang.String");
    }
}
