package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public class wd extends wv<StackTraceElement> {
    public wd() {
        super(StackTraceElement.class);
    }

    /* renamed from: b */
    public StackTraceElement a(ow owVar, qm qmVar) throws IOException, oz {
        String str = "";
        pb e = owVar.e();
        if (e == pb.START_OBJECT) {
            int i = -1;
            String str2 = str;
            String str3 = str;
            while (true) {
                pb c = owVar.c();
                if (c == pb.END_OBJECT) {
                    return new StackTraceElement(str3, str, str2, i);
                }
                String g = owVar.g();
                if ("className".equals(g)) {
                    str3 = owVar.k();
                } else if ("fileName".equals(g)) {
                    str2 = owVar.k();
                } else if ("lineNumber".equals(g)) {
                    if (c.c()) {
                        i = owVar.t();
                    } else {
                        throw qw.a(owVar, "Non-numeric token (" + c + ") for property 'lineNumber'");
                    }
                } else if ("methodName".equals(g)) {
                    str = owVar.k();
                } else if (!"nativeMethod".equals(g)) {
                    a(owVar, qmVar, this.q, g);
                }
            }
        } else {
            throw qmVar.a(this.q, e);
        }
    }
}
