package com.flurry.android.monolithic.sdk.impl;

import java.lang.ref.SoftReference;

public final class ps {
    protected static final ThreadLocal<SoftReference<ps>> a = new ThreadLocal<>();
    private static final char[] d = afr.d();
    private static final byte[] e = afr.e();
    protected afy b;
    protected final char[] c = new char[6];

    public ps() {
        this.c[0] = '\\';
        this.c[2] = '0';
        this.c[3] = '0';
    }

    public static ps a() {
        SoftReference softReference = a.get();
        ps psVar = softReference == null ? null : (ps) softReference.get();
        if (psVar != null) {
            return psVar;
        }
        ps psVar2 = new ps();
        a.set(new SoftReference(psVar2));
        return psVar2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x003a, code lost:
        if ((r1 + r5) <= r6.length) goto L_0x0071;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x003c, code lost:
        r8 = r6.length - r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003e, code lost:
        if (r8 <= 0) goto L_0x0045;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0040, code lost:
        java.lang.System.arraycopy(r11.c, 0, r6, r1, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0045, code lost:
        r6 = r0.m();
        r5 = r5 - r8;
        java.lang.System.arraycopy(r11.c, r8, r6, r1, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0071, code lost:
        java.lang.System.arraycopy(r11.c, 0, r6, r1, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0029, code lost:
        r7 = r5 + 1;
        r5 = a(r2[r12.charAt(r5)], r11.c);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public char[] a(java.lang.String r12) {
        /*
            r11 = this;
            r10 = 0
            com.flurry.android.monolithic.sdk.impl.afy r0 = r11.b
            if (r0 != 0) goto L_0x000d
            com.flurry.android.monolithic.sdk.impl.afy r0 = new com.flurry.android.monolithic.sdk.impl.afy
            r1 = 0
            r0.<init>(r1)
            r11.b = r0
        L_0x000d:
            char[] r1 = r0.k()
            int[] r2 = com.flurry.android.monolithic.sdk.impl.afr.c()
            int r3 = r2.length
            int r4 = r12.length()
            r5 = r10
            r6 = r1
            r1 = r10
        L_0x001d:
            if (r5 >= r4) goto L_0x0066
        L_0x001f:
            char r7 = r12.charAt(r5)
            if (r7 >= r3) goto L_0x0054
            r8 = r2[r7]
            if (r8 == 0) goto L_0x0054
            int r7 = r5 + 1
            char r5 = r12.charAt(r5)
            r5 = r2[r5]
            char[] r8 = r11.c
            int r5 = r11.a(r5, r8)
            int r8 = r1 + r5
            int r9 = r6.length
            if (r8 <= r9) goto L_0x0071
            int r8 = r6.length
            int r8 = r8 - r1
            if (r8 <= 0) goto L_0x0045
            char[] r9 = r11.c
            java.lang.System.arraycopy(r9, r10, r6, r1, r8)
        L_0x0045:
            char[] r6 = r0.m()
            int r5 = r5 - r8
            char[] r9 = r11.c
            java.lang.System.arraycopy(r9, r8, r6, r1, r5)
            int r1 = r1 + r5
            r5 = r6
        L_0x0051:
            r6 = r5
            r5 = r7
            goto L_0x001d
        L_0x0054:
            int r8 = r6.length
            if (r1 < r8) goto L_0x005d
            char[] r1 = r0.m()
            r6 = r1
            r1 = r10
        L_0x005d:
            int r8 = r1 + 1
            r6[r1] = r7
            int r1 = r5 + 1
            if (r1 < r4) goto L_0x006e
            r1 = r8
        L_0x0066:
            r0.a(r1)
            char[] r0 = r0.g()
            return r0
        L_0x006e:
            r5 = r1
            r1 = r8
            goto L_0x001f
        L_0x0071:
            char[] r8 = r11.c
            java.lang.System.arraycopy(r8, r10, r6, r1, r5)
            int r1 = r1 + r5
            r5 = r6
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.ps.a(java.lang.String):char[]");
    }

    private int a(int i, char[] cArr) {
        if (i < 0) {
            int i2 = -(i + 1);
            cArr[1] = 'u';
            cArr[4] = d[i2 >> 4];
            cArr[5] = d[i2 & 15];
            return 6;
        }
        cArr[1] = (char) i;
        return 2;
    }
}
