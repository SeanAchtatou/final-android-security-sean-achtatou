package com.flurry.android.monolithic.sdk.impl;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Map;

@Deprecated
public final class xv extends xw {
    public boolean a(Method method) {
        if (super.a(method)) {
            return true;
        }
        if (!adz.a(method)) {
            return false;
        }
        Class<?> returnType = method.getReturnType();
        if (Collection.class.isAssignableFrom(returnType) || Map.class.isAssignableFrom(returnType)) {
            return true;
        }
        return false;
    }
}
