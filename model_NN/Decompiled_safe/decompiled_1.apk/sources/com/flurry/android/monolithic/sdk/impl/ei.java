package com.flurry.android.monolithic.sdk.impl;

import java.io.File;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ei {
    private static final String a = ei.class.getSimpleName();
    private File b;

    public ei(File file) {
        this.b = file;
        ja.a(3, a, "Referrer file Name if it exists:  " + this.b);
    }

    public Map<String, List<String>> a() {
        if (!this.b.exists()) {
            return null;
        }
        ja.a(4, a, "Loading referrer info from file:  " + this.b.getAbsolutePath());
        String a2 = a(this.b);
        if (a2 == null) {
            return null;
        }
        ja.a(3, a, "Parsing referrer map");
        return a(a2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0043, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0044, code lost:
        r7 = r1;
        r1 = r0;
        r0 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x004f, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0050, code lost:
        r2 = r0;
        r0 = r1;
        r1 = null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0031  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0043 A[ExcHandler: all (r1v7 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:3:0x0006] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String a(java.io.File r9) {
        /*
            r8 = this;
            r6 = 0
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x004b, all -> 0x003b }
            r0.<init>(r9)     // Catch:{ Throwable -> 0x004b, all -> 0x003b }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Throwable -> 0x004f, all -> 0x0043 }
            r1.<init>()     // Catch:{ Throwable -> 0x004f, all -> 0x0043 }
            r2 = 1024(0x400, float:1.435E-42)
            byte[] r2 = new byte[r2]     // Catch:{ Throwable -> 0x001f, all -> 0x0043 }
        L_0x000f:
            int r3 = r0.read(r2)     // Catch:{ Throwable -> 0x001f, all -> 0x0043 }
            if (r3 <= 0) goto L_0x0036
            java.lang.String r4 = new java.lang.String     // Catch:{ Throwable -> 0x001f, all -> 0x0043 }
            r5 = 0
            r4.<init>(r2, r5, r3)     // Catch:{ Throwable -> 0x001f, all -> 0x0043 }
            r1.append(r4)     // Catch:{ Throwable -> 0x001f, all -> 0x0043 }
            goto L_0x000f
        L_0x001f:
            r2 = move-exception
            r7 = r2
            r2 = r0
            r0 = r7
        L_0x0023:
            r3 = 6
            java.lang.String r4 = com.flurry.android.monolithic.sdk.impl.ei.a     // Catch:{ all -> 0x0048 }
            java.lang.String r5 = "Error when loading persistent file"
            com.flurry.android.monolithic.sdk.impl.ja.a(r3, r4, r5, r0)     // Catch:{ all -> 0x0048 }
            com.flurry.android.monolithic.sdk.impl.je.a(r2)
            r0 = r1
        L_0x002f:
            if (r0 == 0) goto L_0x0041
            java.lang.String r0 = r0.toString()
        L_0x0035:
            return r0
        L_0x0036:
            com.flurry.android.monolithic.sdk.impl.je.a(r0)
            r0 = r1
            goto L_0x002f
        L_0x003b:
            r0 = move-exception
            r1 = r6
        L_0x003d:
            com.flurry.android.monolithic.sdk.impl.je.a(r1)
            throw r0
        L_0x0041:
            r0 = r6
            goto L_0x0035
        L_0x0043:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x003d
        L_0x0048:
            r0 = move-exception
            r1 = r2
            goto L_0x003d
        L_0x004b:
            r0 = move-exception
            r1 = r6
            r2 = r6
            goto L_0x0023
        L_0x004f:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r6
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.ei.a(java.io.File):java.lang.String");
    }

    private Map<String, List<String>> a(String str) {
        HashMap hashMap = new HashMap();
        String[] split = str.split("&");
        int length = split.length;
        for (int i = 0; i < length; i++) {
            String[] split2 = split[i].split("=");
            if (split2.length != 2) {
                ja.a(3, a, "Invalid referrer Element: " + split[i] + " in referrer tag " + str);
            } else {
                String decode = URLDecoder.decode(split2[0]);
                String decode2 = URLDecoder.decode(split2[1]);
                if (hashMap.get(decode) == null) {
                    hashMap.put(decode, new ArrayList());
                }
                ((List) hashMap.get(decode)).add(decode2);
            }
        }
        StringBuilder sb = new StringBuilder();
        if (hashMap.get("utm_source") == null) {
            sb.append("Campaign Source is missing.\n");
        }
        if (hashMap.get("utm_medium") == null) {
            sb.append("Campaign Medium is missing.\n");
        }
        if (hashMap.get("utm_campaign") == null) {
            sb.append("Campaign Name is missing.\n");
        }
        if (sb.length() > 0) {
            ja.a(5, "Detected missing referrer keys", sb.toString());
        }
        return hashMap;
    }
}
