package com.flurry.android.monolithic.sdk.impl;

import com.flurry.android.Constants;
import java.io.IOException;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.BigInteger;

public final class po extends pg {
    protected static final char[] g = afr.d();
    protected static final int[] h = afr.c();
    protected final pq i;
    protected final Writer j;
    protected int[] k = h;
    protected int l;
    protected pp m;
    protected pe n;
    protected char[] o;
    protected int p = 0;
    protected int q = 0;
    protected int r;
    protected char[] s;

    public po(pq pqVar, int i2, pc pcVar, Writer writer) {
        super(i2, pcVar);
        this.i = pqVar;
        this.j = writer;
        this.o = pqVar.e();
        this.r = this.o.length;
        if (a(os.ESCAPE_NON_ASCII)) {
            a(127);
        }
    }

    public or a(int i2) {
        int i3;
        if (i2 < 0) {
            i3 = 0;
        } else {
            i3 = i2;
        }
        this.l = i3;
        return this;
    }

    public or a(pp ppVar) {
        this.m = ppVar;
        if (ppVar == null) {
            this.k = h;
        } else {
            this.k = ppVar.a();
        }
        return this;
    }

    public final void a(String str) throws IOException, oq {
        int a = this.e.a(str);
        if (a == 4) {
            i("Can not write a field name, expecting a value");
        }
        a(str, a == 1);
    }

    public final void a(String str, String str2) throws IOException, oq {
        a(str);
        b(str2);
    }

    public final void a(pw pwVar) throws IOException, oq {
        int a = this.e.a(pwVar.a());
        if (a == 4) {
            i("Can not write a field name, expecting a value");
        }
        a(pwVar, a == 1);
    }

    public final void a(pe peVar) throws IOException, oq {
        int a = this.e.a(peVar.a());
        if (a == 4) {
            i("Can not write a field name, expecting a value");
        }
        a(peVar, a == 1);
    }

    public final void b() throws IOException, oq {
        h("start an array");
        this.e = this.e.h();
        if (this.a != null) {
            this.a.e(this);
            return;
        }
        if (this.q >= this.r) {
            o();
        }
        char[] cArr = this.o;
        int i2 = this.q;
        this.q = i2 + 1;
        cArr[i2] = '[';
    }

    public final void c() throws IOException, oq {
        if (!this.e.a()) {
            i("Current context not an ARRAY but " + this.e.d());
        }
        if (this.a != null) {
            this.a.b(this, this.e.e());
        } else {
            if (this.q >= this.r) {
                o();
            }
            char[] cArr = this.o;
            int i2 = this.q;
            this.q = i2 + 1;
            cArr[i2] = ']';
        }
        this.e = this.e.j();
    }

    public final void d() throws IOException, oq {
        h("start an object");
        this.e = this.e.i();
        if (this.a != null) {
            this.a.b(this);
            return;
        }
        if (this.q >= this.r) {
            o();
        }
        char[] cArr = this.o;
        int i2 = this.q;
        this.q = i2 + 1;
        cArr[i2] = '{';
    }

    public final void e() throws IOException, oq {
        if (!this.e.c()) {
            i("Current context not an object but " + this.e.d());
        }
        this.e = this.e.j();
        if (this.a != null) {
            this.a.a(this, this.e.e());
            return;
        }
        if (this.q >= this.r) {
            o();
        }
        char[] cArr = this.o;
        int i2 = this.q;
        this.q = i2 + 1;
        cArr[i2] = '}';
    }

    /* access modifiers changed from: protected */
    public void a(String str, boolean z) throws IOException, oq {
        if (this.a != null) {
            b(str, z);
            return;
        }
        if (this.q + 1 >= this.r) {
            o();
        }
        if (z) {
            char[] cArr = this.o;
            int i2 = this.q;
            this.q = i2 + 1;
            cArr[i2] = ',';
        }
        if (!a(os.QUOTE_FIELD_NAMES)) {
            k(str);
            return;
        }
        char[] cArr2 = this.o;
        int i3 = this.q;
        this.q = i3 + 1;
        cArr2[i3] = '\"';
        k(str);
        if (this.q >= this.r) {
            o();
        }
        char[] cArr3 = this.o;
        int i4 = this.q;
        this.q = i4 + 1;
        cArr3[i4] = '\"';
    }

    public void a(pe peVar, boolean z) throws IOException, oq {
        if (this.a != null) {
            b(peVar, z);
            return;
        }
        if (this.q + 1 >= this.r) {
            o();
        }
        if (z) {
            char[] cArr = this.o;
            int i2 = this.q;
            this.q = i2 + 1;
            cArr[i2] = ',';
        }
        char[] b = peVar.b();
        if (!a(os.QUOTE_FIELD_NAMES)) {
            b(b, 0, b.length);
            return;
        }
        char[] cArr2 = this.o;
        int i3 = this.q;
        this.q = i3 + 1;
        cArr2[i3] = '\"';
        int length = b.length;
        if (this.q + length + 1 >= this.r) {
            b(b, 0, length);
            if (this.q >= this.r) {
                o();
            }
            char[] cArr3 = this.o;
            int i4 = this.q;
            this.q = i4 + 1;
            cArr3[i4] = '\"';
            return;
        }
        System.arraycopy(b, 0, this.o, this.q, length);
        this.q += length;
        char[] cArr4 = this.o;
        int i5 = this.q;
        this.q = i5 + 1;
        cArr4[i5] = '\"';
    }

    /* access modifiers changed from: protected */
    public final void b(String str, boolean z) throws IOException, oq {
        if (z) {
            this.a.c(this);
        } else {
            this.a.h(this);
        }
        if (a(os.QUOTE_FIELD_NAMES)) {
            if (this.q >= this.r) {
                o();
            }
            char[] cArr = this.o;
            int i2 = this.q;
            this.q = i2 + 1;
            cArr[i2] = '\"';
            k(str);
            if (this.q >= this.r) {
                o();
            }
            char[] cArr2 = this.o;
            int i3 = this.q;
            this.q = i3 + 1;
            cArr2[i3] = '\"';
            return;
        }
        k(str);
    }

    /* access modifiers changed from: protected */
    public final void b(pe peVar, boolean z) throws IOException, oq {
        if (z) {
            this.a.c(this);
        } else {
            this.a.h(this);
        }
        char[] b = peVar.b();
        if (a(os.QUOTE_FIELD_NAMES)) {
            if (this.q >= this.r) {
                o();
            }
            char[] cArr = this.o;
            int i2 = this.q;
            this.q = i2 + 1;
            cArr[i2] = '\"';
            b(b, 0, b.length);
            if (this.q >= this.r) {
                o();
            }
            char[] cArr2 = this.o;
            int i3 = this.q;
            this.q = i3 + 1;
            cArr2[i3] = '\"';
            return;
        }
        b(b, 0, b.length);
    }

    public void b(String str) throws IOException, oq {
        h("write text value");
        if (str == null) {
            p();
            return;
        }
        if (this.q >= this.r) {
            o();
        }
        char[] cArr = this.o;
        int i2 = this.q;
        this.q = i2 + 1;
        cArr[i2] = '\"';
        k(str);
        if (this.q >= this.r) {
            o();
        }
        char[] cArr2 = this.o;
        int i3 = this.q;
        this.q = i3 + 1;
        cArr2[i3] = '\"';
    }

    public void a(char[] cArr, int i2, int i3) throws IOException, oq {
        h("write text value");
        if (this.q >= this.r) {
            o();
        }
        char[] cArr2 = this.o;
        int i4 = this.q;
        this.q = i4 + 1;
        cArr2[i4] = '\"';
        c(cArr, i2, i3);
        if (this.q >= this.r) {
            o();
        }
        char[] cArr3 = this.o;
        int i5 = this.q;
        this.q = i5 + 1;
        cArr3[i5] = '\"';
    }

    public final void b(pe peVar) throws IOException, oq {
        h("write text value");
        if (this.q >= this.r) {
            o();
        }
        char[] cArr = this.o;
        int i2 = this.q;
        this.q = i2 + 1;
        cArr[i2] = '\"';
        char[] b = peVar.b();
        int length = b.length;
        if (length < 32) {
            if (length > this.r - this.q) {
                o();
            }
            System.arraycopy(b, 0, this.o, this.q, length);
            this.q += length;
        } else {
            o();
            this.j.write(b, 0, length);
        }
        if (this.q >= this.r) {
            o();
        }
        char[] cArr2 = this.o;
        int i3 = this.q;
        this.q = i3 + 1;
        cArr2[i3] = '\"';
    }

    public void c(String str) throws IOException, oq {
        int length = str.length();
        int i2 = this.r - this.q;
        if (i2 == 0) {
            o();
            i2 = this.r - this.q;
        }
        if (i2 >= length) {
            str.getChars(0, length, this.o, this.q);
            this.q = length + this.q;
            return;
        }
        j(str);
    }

    public void b(char[] cArr, int i2, int i3) throws IOException, oq {
        if (i3 < 32) {
            if (i3 > this.r - this.q) {
                o();
            }
            System.arraycopy(cArr, i2, this.o, this.q, i3);
            this.q += i3;
            return;
        }
        o();
        this.j.write(cArr, i2, i3);
    }

    public void a(char c) throws IOException, oq {
        if (this.q >= this.r) {
            o();
        }
        char[] cArr = this.o;
        int i2 = this.q;
        this.q = i2 + 1;
        cArr[i2] = c;
    }

    private void j(String str) throws IOException, oq {
        int i2 = this.r - this.q;
        str.getChars(0, i2, this.o, this.q);
        this.q += i2;
        o();
        int i3 = i2;
        int length = str.length() - i2;
        while (length > this.r) {
            int i4 = this.r;
            str.getChars(i3, i3 + i4, this.o, 0);
            this.p = 0;
            this.q = i4;
            o();
            i3 += i4;
            length -= i4;
        }
        str.getChars(i3, i3 + length, this.o, 0);
        this.p = 0;
        this.q = length;
    }

    public void a(on onVar, byte[] bArr, int i2, int i3) throws IOException, oq {
        h("write binary value");
        if (this.q >= this.r) {
            o();
        }
        char[] cArr = this.o;
        int i4 = this.q;
        this.q = i4 + 1;
        cArr[i4] = '\"';
        b(onVar, bArr, i2, i2 + i3);
        if (this.q >= this.r) {
            o();
        }
        char[] cArr2 = this.o;
        int i5 = this.q;
        this.q = i5 + 1;
        cArr2[i5] = '\"';
    }

    public void b(int i2) throws IOException, oq {
        h("write number");
        if (this.d) {
            c(i2);
            return;
        }
        if (this.q + 11 >= this.r) {
            o();
        }
        this.q = pu.a(i2, this.o, this.q);
    }

    private final void c(int i2) throws IOException {
        if (this.q + 13 >= this.r) {
            o();
        }
        char[] cArr = this.o;
        int i3 = this.q;
        this.q = i3 + 1;
        cArr[i3] = '\"';
        this.q = pu.a(i2, this.o, this.q);
        char[] cArr2 = this.o;
        int i4 = this.q;
        this.q = i4 + 1;
        cArr2[i4] = '\"';
    }

    public void a(long j2) throws IOException, oq {
        h("write number");
        if (this.d) {
            b(j2);
            return;
        }
        if (this.q + 21 >= this.r) {
            o();
        }
        this.q = pu.a(j2, this.o, this.q);
    }

    private final void b(long j2) throws IOException {
        if (this.q + 23 >= this.r) {
            o();
        }
        char[] cArr = this.o;
        int i2 = this.q;
        this.q = i2 + 1;
        cArr[i2] = '\"';
        this.q = pu.a(j2, this.o, this.q);
        char[] cArr2 = this.o;
        int i3 = this.q;
        this.q = i3 + 1;
        cArr2[i3] = '\"';
    }

    public void a(BigInteger bigInteger) throws IOException, oq {
        h("write number");
        if (bigInteger == null) {
            p();
        } else if (this.d) {
            c(bigInteger);
        } else {
            c(bigInteger.toString());
        }
    }

    public void a(double d) throws IOException, oq {
        if (this.d || ((Double.isNaN(d) || Double.isInfinite(d)) && a(os.QUOTE_NON_NUMERIC_NUMBERS))) {
            b(String.valueOf(d));
            return;
        }
        h("write number");
        c(String.valueOf(d));
    }

    public void a(float f) throws IOException, oq {
        if (this.d || ((Float.isNaN(f) || Float.isInfinite(f)) && a(os.QUOTE_NON_NUMERIC_NUMBERS))) {
            b(String.valueOf(f));
            return;
        }
        h("write number");
        c(String.valueOf(f));
    }

    public void a(BigDecimal bigDecimal) throws IOException, oq {
        h("write number");
        if (bigDecimal == null) {
            p();
        } else if (this.d) {
            c(bigDecimal);
        } else {
            c(bigDecimal.toString());
        }
    }

    public void e(String str) throws IOException, oq {
        h("write number");
        if (this.d) {
            c((Object) str);
        } else {
            c(str);
        }
    }

    private final void c(Object obj) throws IOException {
        if (this.q >= this.r) {
            o();
        }
        char[] cArr = this.o;
        int i2 = this.q;
        this.q = i2 + 1;
        cArr[i2] = '\"';
        c(obj.toString());
        if (this.q >= this.r) {
            o();
        }
        char[] cArr2 = this.o;
        int i3 = this.q;
        this.q = i3 + 1;
        cArr2[i3] = '\"';
    }

    public void a(boolean z) throws IOException, oq {
        int i2;
        h("write boolean value");
        if (this.q + 5 >= this.r) {
            o();
        }
        int i3 = this.q;
        char[] cArr = this.o;
        if (z) {
            cArr[i3] = 't';
            int i4 = i3 + 1;
            cArr[i4] = 'r';
            int i5 = i4 + 1;
            cArr[i5] = 'u';
            i2 = i5 + 1;
            cArr[i2] = 'e';
        } else {
            cArr[i3] = 'f';
            int i6 = i3 + 1;
            cArr[i6] = 'a';
            int i7 = i6 + 1;
            cArr[i7] = 'l';
            int i8 = i7 + 1;
            cArr[i8] = 's';
            i2 = i8 + 1;
            cArr[i2] = 'e';
        }
        this.q = i2 + 1;
    }

    public void f() throws IOException, oq {
        h("write null value");
        p();
    }

    /* access modifiers changed from: protected */
    public final void h(String str) throws IOException, oq {
        char c;
        int k2 = this.e.k();
        if (k2 == 5) {
            i("Can not " + str + ", expecting field name");
        }
        if (this.a == null) {
            switch (k2) {
                case 1:
                    c = ',';
                    break;
                case 2:
                    c = ':';
                    break;
                case 3:
                    c = ' ';
                    break;
                default:
                    return;
            }
            if (this.q >= this.r) {
                o();
            }
            this.o[this.q] = c;
            this.q++;
            return;
        }
        b(str, k2);
    }

    /* access modifiers changed from: protected */
    public final void b(String str, int i2) throws IOException, oq {
        switch (i2) {
            case 0:
                if (this.e.a()) {
                    this.a.g(this);
                    return;
                } else if (this.e.c()) {
                    this.a.h(this);
                    return;
                } else {
                    return;
                }
            case 1:
                this.a.f(this);
                return;
            case 2:
                this.a.d(this);
                return;
            case 3:
                this.a.a(this);
                return;
            default:
                m();
                return;
        }
    }

    public final void g() throws IOException {
        o();
        if (this.j != null && a(os.FLUSH_PASSED_TO_STREAM)) {
            this.j.flush();
        }
    }

    public void close() throws IOException {
        super.close();
        if (this.o != null && a(os.AUTO_CLOSE_JSON_CONTENT)) {
            while (true) {
                pl h2 = h();
                if (!h2.a()) {
                    if (!h2.c()) {
                        break;
                    }
                    e();
                } else {
                    c();
                }
            }
        }
        o();
        if (this.j != null) {
            if (this.i.b() || a(os.AUTO_CLOSE_TARGET)) {
                this.j.close();
            } else if (a(os.FLUSH_PASSED_TO_STREAM)) {
                this.j.flush();
            }
        }
        n();
    }

    /* access modifiers changed from: protected */
    public void n() {
        char[] cArr = this.o;
        if (cArr != null) {
            this.o = null;
            this.i.b(cArr);
        }
    }

    private void k(String str) throws IOException, oq {
        int length = str.length();
        if (length > this.r) {
            l(str);
            return;
        }
        if (this.q + length > this.r) {
            o();
        }
        str.getChars(0, length, this.o, this.q);
        if (this.m != null) {
            f(length);
        } else if (this.l != 0) {
            a(length, this.l);
        } else {
            d(length);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0026, code lost:
        r3 = r7.o;
        r4 = r7.q;
        r7.q = r4 + 1;
        r3 = r3[r4];
        a(r3, r1[r3]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0016, code lost:
        r3 = r7.q - r7.p;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001b, code lost:
        if (r3 <= 0) goto L_0x0026;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001d, code lost:
        r7.j.write(r7.o, r7.p, r3);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void d(int r8) throws java.io.IOException, com.flurry.android.monolithic.sdk.impl.oq {
        /*
            r7 = this;
            int r0 = r7.q
            int r0 = r0 + r8
            int[] r1 = r7.k
            int r2 = r1.length
        L_0x0006:
            int r3 = r7.q
            if (r3 >= r0) goto L_0x003e
        L_0x000a:
            char[] r3 = r7.o
            int r4 = r7.q
            char r3 = r3[r4]
            if (r3 >= r2) goto L_0x0036
            r3 = r1[r3]
            if (r3 == 0) goto L_0x0036
            int r3 = r7.q
            int r4 = r7.p
            int r3 = r3 - r4
            if (r3 <= 0) goto L_0x0026
            java.io.Writer r4 = r7.j
            char[] r5 = r7.o
            int r6 = r7.p
            r4.write(r5, r6, r3)
        L_0x0026:
            char[] r3 = r7.o
            int r4 = r7.q
            int r5 = r4 + 1
            r7.q = r5
            char r3 = r3[r4]
            r4 = r1[r3]
            r7.a(r3, r4)
            goto L_0x0006
        L_0x0036:
            int r3 = r7.q
            int r3 = r3 + 1
            r7.q = r3
            if (r3 < r0) goto L_0x000a
        L_0x003e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.po.d(int):void");
    }

    private void l(String str) throws IOException, oq {
        o();
        int length = str.length();
        int i2 = 0;
        do {
            int i3 = this.r;
            if (i2 + i3 > length) {
                i3 = length - i2;
            }
            str.getChars(i2, i2 + i3, this.o, 0);
            if (this.m != null) {
                g(i3);
            } else if (this.l != 0) {
                b(i3, this.l);
            } else {
                e(i3);
            }
            i2 += i3;
        } while (i2 < length);
    }

    private final void e(int i2) throws IOException, oq {
        char c;
        int[] iArr = this.k;
        int length = iArr.length;
        int i3 = 0;
        int i4 = 0;
        while (i4 < i2) {
            do {
                c = this.o[i4];
                if (c < length && iArr[c] != 0) {
                    break;
                }
                i4++;
            } while (i4 < i2);
            int i5 = i4 - i3;
            if (i5 > 0) {
                this.j.write(this.o, i3, i5);
                if (i4 >= i2) {
                    return;
                }
            }
            int i6 = i4 + 1;
            i3 = a(this.o, i6, i2, c, iArr[c]);
            i4 = i6;
        }
    }

    private final void c(char[] cArr, int i2, int i3) throws IOException, oq {
        if (this.m != null) {
            d(cArr, i2, i3);
        } else if (this.l != 0) {
            a(cArr, i2, i3, this.l);
        } else {
            int i4 = i3 + i2;
            int[] iArr = this.k;
            int length = iArr.length;
            int i5 = i2;
            while (i5 < i4) {
                int i6 = i5;
                do {
                    char c = cArr[i6];
                    if (c < length && iArr[c] != 0) {
                        break;
                    }
                    i6++;
                } while (i6 < i4);
                int i7 = i6 - i5;
                if (i7 < 32) {
                    if (this.q + i7 > this.r) {
                        o();
                    }
                    if (i7 > 0) {
                        System.arraycopy(cArr, i5, this.o, this.q, i7);
                        this.q += i7;
                    }
                } else {
                    o();
                    this.j.write(cArr, i5, i7);
                }
                if (i6 < i4) {
                    i5 = i6 + 1;
                    char c2 = cArr[i6];
                    b(c2, iArr[c2]);
                } else {
                    return;
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0044 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(int r10, int r11) throws java.io.IOException, com.flurry.android.monolithic.sdk.impl.oq {
        /*
            r9 = this;
            int r0 = r9.q
            int r0 = r0 + r10
            int[] r1 = r9.k
            int r2 = r1.length
            int r3 = r9.l
            int r3 = r3 + 1
            int r2 = java.lang.Math.min(r2, r3)
        L_0x000e:
            int r3 = r9.q
            if (r3 >= r0) goto L_0x0044
        L_0x0012:
            char[] r3 = r9.o
            int r4 = r9.q
            char r3 = r3[r4]
            if (r3 >= r2) goto L_0x0038
            r4 = r1[r3]
            if (r4 == 0) goto L_0x003c
        L_0x001e:
            int r5 = r9.q
            int r6 = r9.p
            int r5 = r5 - r6
            if (r5 <= 0) goto L_0x002e
            java.io.Writer r6 = r9.j
            char[] r7 = r9.o
            int r8 = r9.p
            r6.write(r7, r8, r5)
        L_0x002e:
            int r5 = r9.q
            int r5 = r5 + 1
            r9.q = r5
            r9.a(r3, r4)
            goto L_0x000e
        L_0x0038:
            if (r3 <= r11) goto L_0x003c
            r4 = -1
            goto L_0x001e
        L_0x003c:
            int r3 = r9.q
            int r3 = r3 + 1
            r9.q = r3
            if (r3 < r0) goto L_0x0012
        L_0x0044:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.po.a(int, int):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0034 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void b(int r10, int r11) throws java.io.IOException, com.flurry.android.monolithic.sdk.impl.oq {
        /*
            r9 = this;
            r2 = 0
            int[] r6 = r9.k
            int r0 = r6.length
            int r1 = r9.l
            int r1 = r1 + 1
            int r7 = java.lang.Math.min(r0, r1)
            r0 = r2
            r1 = r2
        L_0x000e:
            if (r2 >= r10) goto L_0x0029
        L_0x0010:
            char[] r3 = r9.o
            char r4 = r3[r2]
            if (r4 >= r7) goto L_0x002a
            r1 = r6[r4]
            if (r1 == 0) goto L_0x0030
            r5 = r1
            r1 = r2
        L_0x001c:
            int r2 = r1 - r0
            if (r2 <= 0) goto L_0x0037
            java.io.Writer r3 = r9.j
            char[] r8 = r9.o
            r3.write(r8, r0, r2)
            if (r1 < r10) goto L_0x0037
        L_0x0029:
            return
        L_0x002a:
            if (r4 <= r11) goto L_0x0030
            r1 = -1
            r5 = r1
            r1 = r2
            goto L_0x001c
        L_0x0030:
            int r2 = r2 + 1
            if (r2 < r10) goto L_0x0010
            r5 = r1
            r1 = r2
            goto L_0x001c
        L_0x0037:
            int r2 = r1 + 1
            char[] r1 = r9.o
            r0 = r9
            r3 = r10
            int r0 = r0.a(r1, r2, r3, r4, r5)
            r1 = r5
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.po.b(int, int):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0018 A[EDGE_INSN: B:25:0x0018->B:7:0x0018 ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void a(char[] r11, int r12, int r13, int r14) throws java.io.IOException, com.flurry.android.monolithic.sdk.impl.oq {
        /*
            r10 = this;
            int r0 = r13 + r12
            int[] r1 = r10.k
            int r2 = r1.length
            int r3 = r14 + 1
            int r2 = java.lang.Math.min(r2, r3)
            r3 = 0
            r4 = r12
        L_0x000d:
            if (r4 >= r0) goto L_0x0038
            r5 = r4
        L_0x0010:
            char r6 = r11[r5]
            if (r6 >= r2) goto L_0x0039
            r3 = r1[r6]
            if (r3 == 0) goto L_0x003d
        L_0x0018:
            int r7 = r5 - r4
            r8 = 32
            if (r7 >= r8) goto L_0x0042
            int r8 = r10.q
            int r8 = r8 + r7
            int r9 = r10.r
            if (r8 <= r9) goto L_0x0028
            r10.o()
        L_0x0028:
            if (r7 <= 0) goto L_0x0036
            char[] r8 = r10.o
            int r9 = r10.q
            java.lang.System.arraycopy(r11, r4, r8, r9, r7)
            int r4 = r10.q
            int r4 = r4 + r7
            r10.q = r4
        L_0x0036:
            if (r5 < r0) goto L_0x004b
        L_0x0038:
            return
        L_0x0039:
            if (r6 <= r14) goto L_0x003d
            r3 = -1
            goto L_0x0018
        L_0x003d:
            int r5 = r5 + 1
            if (r5 < r0) goto L_0x0010
            goto L_0x0018
        L_0x0042:
            r10.o()
            java.io.Writer r8 = r10.j
            r8.write(r11, r4, r7)
            goto L_0x0036
        L_0x004b:
            int r4 = r5 + 1
            r10.b(r6, r3)
            goto L_0x000d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.po.a(char[], int, int, int):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0059 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void f(int r12) throws java.io.IOException, com.flurry.android.monolithic.sdk.impl.oq {
        /*
            r11 = this;
            int r0 = r11.q
            int r0 = r0 + r12
            int[] r1 = r11.k
            int r2 = r11.l
            r3 = 1
            if (r2 >= r3) goto L_0x0040
            r2 = 65535(0xffff, float:9.1834E-41)
        L_0x000d:
            int r3 = r1.length
            int r4 = r2 + 1
            int r3 = java.lang.Math.min(r3, r4)
            com.flurry.android.monolithic.sdk.impl.pp r4 = r11.m
        L_0x0016:
            int r5 = r11.q
            if (r5 >= r0) goto L_0x0059
        L_0x001a:
            char[] r5 = r11.o
            int r6 = r11.q
            char r5 = r5[r6]
            if (r5 >= r3) goto L_0x0043
            r6 = r1[r5]
            if (r6 == 0) goto L_0x0051
        L_0x0026:
            int r7 = r11.q
            int r8 = r11.p
            int r7 = r7 - r8
            if (r7 <= 0) goto L_0x0036
            java.io.Writer r8 = r11.j
            char[] r9 = r11.o
            int r10 = r11.p
            r8.write(r9, r10, r7)
        L_0x0036:
            int r7 = r11.q
            int r7 = r7 + 1
            r11.q = r7
            r11.a(r5, r6)
            goto L_0x0016
        L_0x0040:
            int r2 = r11.l
            goto L_0x000d
        L_0x0043:
            if (r5 <= r2) goto L_0x0047
            r6 = -1
            goto L_0x0026
        L_0x0047:
            com.flurry.android.monolithic.sdk.impl.pe r6 = r4.a(r5)
            r11.n = r6
            if (r6 == 0) goto L_0x0051
            r6 = -2
            goto L_0x0026
        L_0x0051:
            int r5 = r11.q
            int r5 = r5 + 1
            r11.q = r5
            if (r5 < r0) goto L_0x001a
        L_0x0059:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.po.f(int):void");
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x004f A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void g(int r12) throws java.io.IOException, com.flurry.android.monolithic.sdk.impl.oq {
        /*
            r11 = this;
            r2 = 0
            int[] r6 = r11.k
            int r0 = r11.l
            r1 = 1
            if (r0 >= r1) goto L_0x0035
            r0 = 65535(0xffff, float:9.1834E-41)
            r7 = r0
        L_0x000c:
            int r0 = r6.length
            int r1 = r11.l
            int r1 = r1 + 1
            int r8 = java.lang.Math.min(r0, r1)
            com.flurry.android.monolithic.sdk.impl.pp r9 = r11.m
            r0 = r2
            r1 = r2
        L_0x0019:
            if (r2 >= r12) goto L_0x0034
        L_0x001b:
            char[] r3 = r11.o
            char r4 = r3[r2]
            if (r4 >= r8) goto L_0x0039
            r1 = r6[r4]
            if (r1 == 0) goto L_0x004b
            r5 = r1
            r1 = r2
        L_0x0027:
            int r2 = r1 - r0
            if (r2 <= 0) goto L_0x0052
            java.io.Writer r3 = r11.j
            char[] r10 = r11.o
            r3.write(r10, r0, r2)
            if (r1 < r12) goto L_0x0052
        L_0x0034:
            return
        L_0x0035:
            int r0 = r11.l
            r7 = r0
            goto L_0x000c
        L_0x0039:
            if (r4 <= r7) goto L_0x003f
            r1 = -1
            r5 = r1
            r1 = r2
            goto L_0x0027
        L_0x003f:
            com.flurry.android.monolithic.sdk.impl.pe r3 = r9.a(r4)
            r11.n = r3
            if (r3 == 0) goto L_0x004b
            r1 = -2
            r5 = r1
            r1 = r2
            goto L_0x0027
        L_0x004b:
            int r2 = r2 + 1
            if (r2 < r12) goto L_0x001b
            r5 = r1
            r1 = r2
            goto L_0x0027
        L_0x0052:
            int r2 = r1 + 1
            char[] r1 = r11.o
            r0 = r11
            r3 = r12
            int r0 = r0.a(r1, r2, r3, r4, r5)
            r1 = r5
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.po.g(int):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x0022 A[EDGE_INSN: B:32:0x0022->B:10:0x0022 ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void d(char[] r13, int r14, int r15) throws java.io.IOException, com.flurry.android.monolithic.sdk.impl.oq {
        /*
            r12 = this;
            int r0 = r15 + r14
            int[] r1 = r12.k
            int r2 = r12.l
            r3 = 1
            if (r2 >= r3) goto L_0x0043
            r2 = 65535(0xffff, float:9.1834E-41)
        L_0x000c:
            int r3 = r1.length
            int r4 = r2 + 1
            int r3 = java.lang.Math.min(r3, r4)
            com.flurry.android.monolithic.sdk.impl.pp r4 = r12.m
            r5 = 0
            r6 = r14
        L_0x0017:
            if (r6 >= r0) goto L_0x0042
            r7 = r6
        L_0x001a:
            char r8 = r13[r7]
            if (r8 >= r3) goto L_0x0046
            r5 = r1[r8]
            if (r5 == 0) goto L_0x0054
        L_0x0022:
            int r9 = r7 - r6
            r10 = 32
            if (r9 >= r10) goto L_0x0059
            int r10 = r12.q
            int r10 = r10 + r9
            int r11 = r12.r
            if (r10 <= r11) goto L_0x0032
            r12.o()
        L_0x0032:
            if (r9 <= 0) goto L_0x0040
            char[] r10 = r12.o
            int r11 = r12.q
            java.lang.System.arraycopy(r13, r6, r10, r11, r9)
            int r6 = r12.q
            int r6 = r6 + r9
            r12.q = r6
        L_0x0040:
            if (r7 < r0) goto L_0x0062
        L_0x0042:
            return
        L_0x0043:
            int r2 = r12.l
            goto L_0x000c
        L_0x0046:
            if (r8 <= r2) goto L_0x004a
            r5 = -1
            goto L_0x0022
        L_0x004a:
            com.flurry.android.monolithic.sdk.impl.pe r9 = r4.a(r8)
            r12.n = r9
            if (r9 == 0) goto L_0x0054
            r5 = -2
            goto L_0x0022
        L_0x0054:
            int r7 = r7 + 1
            if (r7 < r0) goto L_0x001a
            goto L_0x0022
        L_0x0059:
            r12.o()
            java.io.Writer r10 = r12.j
            r10.write(r13, r6, r9)
            goto L_0x0040
        L_0x0062:
            int r6 = r7 + 1
            r12.b(r8, r5)
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.po.d(char[], int, int):void");
    }

    /* access modifiers changed from: protected */
    public void b(on onVar, byte[] bArr, int i2, int i3) throws IOException, oq {
        int i4;
        int i5 = i3 - 3;
        int i6 = this.r - 6;
        int c = onVar.c() >> 2;
        int i7 = i2;
        while (i7 <= i5) {
            if (this.q > i6) {
                o();
            }
            int i8 = i7 + 1;
            int i9 = i8 + 1;
            int i10 = i9 + 1;
            this.q = onVar.a((((bArr[i7] << 8) | (bArr[i8] & Constants.UNKNOWN)) << 8) | (bArr[i9] & Constants.UNKNOWN), this.o, this.q);
            c--;
            if (c <= 0) {
                char[] cArr = this.o;
                int i11 = this.q;
                this.q = i11 + 1;
                cArr[i11] = '\\';
                char[] cArr2 = this.o;
                int i12 = this.q;
                this.q = i12 + 1;
                cArr2[i12] = 'n';
                c = onVar.c() >> 2;
            }
            i7 = i10;
        }
        int i13 = i3 - i7;
        if (i13 > 0) {
            if (this.q > i6) {
                o();
            }
            int i14 = i7 + 1;
            int i15 = bArr[i7] << 16;
            if (i13 == 2) {
                int i16 = i14 + 1;
                i4 = ((bArr[i14] & Constants.UNKNOWN) << 8) | i15;
            } else {
                i4 = i15;
            }
            this.q = onVar.a(i4, i13, this.o, this.q);
        }
    }

    private final void p() throws IOException {
        if (this.q + 4 >= this.r) {
            o();
        }
        int i2 = this.q;
        char[] cArr = this.o;
        cArr[i2] = 'n';
        int i3 = i2 + 1;
        cArr[i3] = 'u';
        int i4 = i3 + 1;
        cArr[i4] = 'l';
        int i5 = i4 + 1;
        cArr[i5] = 'l';
        this.q = i5 + 1;
    }

    private final void a(char c, int i2) throws IOException, oq {
        String a;
        int i3;
        char c2;
        if (i2 >= 0) {
            if (this.q >= 2) {
                int i4 = this.q - 2;
                this.p = i4;
                this.o[i4] = '\\';
                this.o[i4 + 1] = (char) i2;
                return;
            }
            char[] cArr = this.s;
            if (cArr == null) {
                cArr = q();
            }
            this.p = this.q;
            cArr[1] = (char) i2;
            this.j.write(cArr, 0, 2);
        } else if (i2 == -2) {
            if (this.n == null) {
                a = this.m.a(c).a();
            } else {
                a = this.n.a();
                this.n = null;
            }
            int length = a.length();
            if (this.q >= length) {
                int i5 = this.q - length;
                this.p = i5;
                a.getChars(0, length, this.o, i5);
                return;
            }
            this.p = this.q;
            this.j.write(a);
        } else if (this.q >= 6) {
            char[] cArr2 = this.o;
            int i6 = this.q - 6;
            this.p = i6;
            cArr2[i6] = '\\';
            int i7 = i6 + 1;
            cArr2[i7] = 'u';
            if (c > 255) {
                int i8 = (c >> 8) & 255;
                int i9 = i7 + 1;
                cArr2[i9] = g[i8 >> 4];
                i3 = i9 + 1;
                cArr2[i3] = g[i8 & 15];
                c2 = (char) (c & 255);
            } else {
                int i10 = i7 + 1;
                cArr2[i10] = '0';
                i3 = i10 + 1;
                cArr2[i3] = '0';
                c2 = c;
            }
            int i11 = i3 + 1;
            cArr2[i11] = g[c2 >> 4];
            cArr2[i11 + 1] = g[c2 & 15];
        } else {
            char[] cArr3 = this.s;
            if (cArr3 == null) {
                cArr3 = q();
            }
            this.p = this.q;
            if (c > 255) {
                int i12 = (c >> 8) & 255;
                char c3 = c & 255;
                cArr3[10] = g[i12 >> 4];
                cArr3[11] = g[i12 & 15];
                cArr3[12] = g[c3 >> 4];
                cArr3[13] = g[c3 & 15];
                this.j.write(cArr3, 8, 6);
                return;
            }
            cArr3[6] = g[c >> 4];
            cArr3[7] = g[c & 15];
            this.j.write(cArr3, 2, 6);
        }
    }

    private final int a(char[] cArr, int i2, int i3, char c, int i4) throws IOException, oq {
        String a;
        int i5;
        char c2;
        if (i4 >= 0) {
            if (i2 <= 1 || i2 >= i3) {
                char[] cArr2 = this.s;
                if (cArr2 == null) {
                    cArr2 = q();
                }
                cArr2[1] = (char) i4;
                this.j.write(cArr2, 0, 2);
            } else {
                int i6 = i2 - 2;
                cArr[i6] = '\\';
                cArr[i6 + 1] = (char) i4;
                return i6;
            }
        } else if (i4 == -2) {
            if (this.n == null) {
                a = this.m.a(c).a();
            } else {
                a = this.n.a();
                this.n = null;
            }
            int length = a.length();
            if (i2 < length || i2 >= i3) {
                this.j.write(a);
            } else {
                int i7 = i2 - length;
                a.getChars(0, length, cArr, i7);
                return i7;
            }
        } else if (i2 <= 5 || i2 >= i3) {
            char[] cArr3 = this.s;
            if (cArr3 == null) {
                cArr3 = q();
            }
            this.p = this.q;
            if (c > 255) {
                int i8 = (c >> 8) & 255;
                char c3 = c & 255;
                cArr3[10] = g[i8 >> 4];
                cArr3[11] = g[i8 & 15];
                cArr3[12] = g[c3 >> 4];
                cArr3[13] = g[c3 & 15];
                this.j.write(cArr3, 8, 6);
                return i2;
            }
            cArr3[6] = g[c >> 4];
            cArr3[7] = g[c & 15];
            this.j.write(cArr3, 2, 6);
        } else {
            int i9 = i2 - 6;
            int i10 = i9 + 1;
            cArr[i9] = '\\';
            int i11 = i10 + 1;
            cArr[i10] = 'u';
            if (c > 255) {
                int i12 = (c >> 8) & 255;
                int i13 = i11 + 1;
                cArr[i11] = g[i12 >> 4];
                cArr[i13] = g[i12 & 15];
                i5 = i13 + 1;
                c2 = (char) (c & 255);
            } else {
                int i14 = i11 + 1;
                cArr[i11] = '0';
                cArr[i14] = '0';
                i5 = i14 + 1;
                c2 = c;
            }
            int i15 = i5 + 1;
            cArr[i5] = g[c2 >> 4];
            cArr[i15] = g[c2 & 15];
            return i15 - 5;
        }
        return i2;
    }

    private final void b(char c, int i2) throws IOException, oq {
        String a;
        int i3;
        char c2;
        if (i2 >= 0) {
            if (this.q + 2 > this.r) {
                o();
            }
            char[] cArr = this.o;
            int i4 = this.q;
            this.q = i4 + 1;
            cArr[i4] = '\\';
            char[] cArr2 = this.o;
            int i5 = this.q;
            this.q = i5 + 1;
            cArr2[i5] = (char) i2;
        } else if (i2 != -2) {
            if (this.q + 2 > this.r) {
                o();
            }
            int i6 = this.q;
            char[] cArr3 = this.o;
            int i7 = i6 + 1;
            cArr3[i6] = '\\';
            int i8 = i7 + 1;
            cArr3[i7] = 'u';
            if (c > 255) {
                int i9 = (c >> 8) & 255;
                int i10 = i8 + 1;
                cArr3[i8] = g[i9 >> 4];
                i3 = i10 + 1;
                cArr3[i10] = g[i9 & 15];
                c2 = (char) (c & 255);
            } else {
                int i11 = i8 + 1;
                cArr3[i8] = '0';
                i3 = i11 + 1;
                cArr3[i11] = '0';
                c2 = c;
            }
            int i12 = i3 + 1;
            cArr3[i3] = g[c2 >> 4];
            cArr3[i12] = g[c2 & 15];
            this.q = i12;
        } else {
            if (this.n == null) {
                a = this.m.a(c).a();
            } else {
                a = this.n.a();
                this.n = null;
            }
            int length = a.length();
            if (this.q + length > this.r) {
                o();
                if (length > this.r) {
                    this.j.write(a);
                    return;
                }
            }
            a.getChars(0, length, this.o, this.q);
            this.q += length;
        }
    }

    private char[] q() {
        char[] cArr = new char[14];
        cArr[0] = '\\';
        cArr[2] = '\\';
        cArr[3] = 'u';
        cArr[4] = '0';
        cArr[5] = '0';
        cArr[8] = '\\';
        cArr[9] = 'u';
        this.s = cArr;
        return cArr;
    }

    /* access modifiers changed from: protected */
    public final void o() throws IOException {
        int i2 = this.q - this.p;
        if (i2 > 0) {
            int i3 = this.p;
            this.p = 0;
            this.q = 0;
            this.j.write(this.o, i3, i2);
        }
    }
}
