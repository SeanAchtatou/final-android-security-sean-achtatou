package com.flurry.android.monolithic.sdk.impl;

import java.lang.reflect.Member;
import java.util.HashMap;

public class tm {
    final xq a;
    final boolean b;
    protected xi c;
    protected xo d;
    protected xo e;
    protected xo f;
    protected xo g;
    protected xo h;
    protected xo i;
    protected xo j;
    protected tn[] k = null;

    public tm(xq xqVar, boolean z) {
        this.a = xqVar;
        this.b = z;
    }

    public th a(qk qkVar) {
        afm a2;
        ww wwVar = new ww(qkVar, this.a.a());
        if (this.i == null) {
            a2 = null;
        } else {
            a2 = this.a.j().a(this.i.b(0));
        }
        wwVar.a(this.c, this.i, a2, this.j, this.k);
        wwVar.a(this.d);
        wwVar.b(this.e);
        wwVar.c(this.f);
        wwVar.d(this.g);
        wwVar.e(this.h);
        return wwVar;
    }

    public void a(xi xiVar) {
        this.c = xiVar;
    }

    public void a(xo xoVar) {
        this.d = a(xoVar, this.d, "String");
    }

    public void b(xo xoVar) {
        this.e = a(xoVar, this.e, "int");
    }

    public void c(xo xoVar) {
        this.f = a(xoVar, this.f, "long");
    }

    public void d(xo xoVar) {
        this.g = a(xoVar, this.g, "double");
    }

    public void e(xo xoVar) {
        this.h = a(xoVar, this.h, "boolean");
    }

    public void f(xo xoVar) {
        this.i = a(xoVar, this.i, "delegate");
    }

    public void a(xo xoVar, tn[] tnVarArr) {
        this.j = a(xoVar, this.j, "property-based");
        if (tnVarArr.length > 1) {
            HashMap hashMap = new HashMap();
            int length = tnVarArr.length;
            for (int i2 = 0; i2 < length; i2++) {
                String c2 = tnVarArr[i2].c();
                Integer num = (Integer) hashMap.put(c2, Integer.valueOf(i2));
                if (num != null) {
                    throw new IllegalArgumentException("Duplicate creator property \"" + c2 + "\" (index " + num + " vs " + i2 + ")");
                }
            }
        }
        this.k = tnVarArr;
    }

    /* access modifiers changed from: protected */
    public xo a(xo xoVar, xo xoVar2, String str) {
        if (xoVar2 == null || xoVar2.getClass() != xoVar.getClass()) {
            if (this.b) {
                adz.a((Member) xoVar.a());
            }
            return xoVar;
        }
        throw new IllegalArgumentException("Conflicting " + str + " creators: already had " + xoVar2 + ", encountered " + xoVar);
    }
}
