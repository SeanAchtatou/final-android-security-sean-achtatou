package com.flurry.android.monolithic.sdk.impl;

import java.util.Map;

class ny implements Map.Entry<K, V> {
    final /* synthetic */ Object a;
    final /* synthetic */ Object b;
    final /* synthetic */ nx c;

    ny(nx nxVar, Object obj, Object obj2) {
        this.c = nxVar;
        this.a = obj;
        this.b = obj2;
    }

    public K getKey() {
        return this.a;
    }

    public V getValue() {
        return this.b;
    }

    public V setValue(V v) {
        throw new UnsupportedOperationException();
    }
}
