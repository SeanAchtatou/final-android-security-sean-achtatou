package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import android.text.TextUtils;
import java.util.List;

public final class cv implements cx {
    private static final String a = cv.class.getSimpleName();

    public boolean a(Context context, db dbVar) {
        boolean z;
        if (dbVar == null) {
            return false;
        }
        String a2 = dbVar.a();
        if (TextUtils.isEmpty(a2)) {
            return false;
        }
        List<cu> b = dbVar.b();
        if (b == null) {
            return false;
        }
        boolean z2 = true;
        String packageName = context.getPackageName();
        for (cu a3 : b) {
            if (!a(a2, packageName, a3)) {
                z = false;
            } else {
                z = z2;
            }
            z2 = z;
        }
        return z2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0089  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean a(java.lang.String r9, java.lang.String r10, com.flurry.android.monolithic.sdk.impl.cu r11) {
        /*
            r8 = this;
            r4 = 0
            r3 = 6
            java.lang.String r7 = ": package=\""
            java.lang.String r6 = "\" or higher"
            java.lang.String r5 = "\" and version=\""
            boolean r0 = android.text.TextUtils.isEmpty(r9)
            if (r0 != 0) goto L_0x0016
            boolean r0 = android.text.TextUtils.isEmpty(r10)
            if (r0 != 0) goto L_0x0016
            if (r11 != 0) goto L_0x0018
        L_0x0016:
            r0 = r4
        L_0x0017:
            return r0
        L_0x0018:
            java.lang.String r0 = r11.c()     // Catch:{ ClassNotFoundException -> 0x006b, ExceptionInInitializerError -> 0x0075, LinkageError -> 0x007f }
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch:{ ClassNotFoundException -> 0x006b, ExceptionInInitializerError -> 0x0075, LinkageError -> 0x007f }
            if (r0 != 0) goto L_0x0087
            java.lang.String r0 = r11.c()     // Catch:{ ClassNotFoundException -> 0x006b, ExceptionInInitializerError -> 0x0075, LinkageError -> 0x007f }
            java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException -> 0x006b, ExceptionInInitializerError -> 0x0075, LinkageError -> 0x007f }
            r0 = 1
        L_0x002a:
            if (r0 != 0) goto L_0x0089
            java.lang.String r1 = com.flurry.android.monolithic.sdk.impl.cv.a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r2 = r2.append(r9)
            java.lang.String r3 = ": package=\""
            java.lang.StringBuilder r2 = r2.append(r7)
            java.lang.StringBuilder r2 = r2.append(r10)
            java.lang.String r3 = "\": apk should include ad provider library with name=\""
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = r11.a()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "\" and version=\""
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.String r3 = r11.b()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "\" or higher"
            java.lang.StringBuilder r2 = r2.append(r6)
            java.lang.String r2 = r2.toString()
            com.flurry.android.monolithic.sdk.impl.ja.b(r1, r2)
            goto L_0x0017
        L_0x006b:
            r0 = move-exception
            java.lang.String r1 = com.flurry.android.monolithic.sdk.impl.cv.a
            java.lang.String r2 = "failed to find third party ad provider api with exception: "
            com.flurry.android.monolithic.sdk.impl.ja.a(r3, r1, r2, r0)
            r0 = r4
            goto L_0x002a
        L_0x0075:
            r0 = move-exception
            java.lang.String r1 = com.flurry.android.monolithic.sdk.impl.cv.a
            java.lang.String r2 = "failed to initialize third party ad provider api with exception: "
            com.flurry.android.monolithic.sdk.impl.ja.a(r3, r1, r2, r0)
            r0 = r4
            goto L_0x002a
        L_0x007f:
            r0 = move-exception
            java.lang.String r1 = com.flurry.android.monolithic.sdk.impl.cv.a
            java.lang.String r2 = "failed to link third party ad provider api with exception: "
            com.flurry.android.monolithic.sdk.impl.ja.a(r3, r1, r2, r0)
        L_0x0087:
            r0 = r4
            goto L_0x002a
        L_0x0089:
            r1 = 3
            java.lang.String r2 = com.flurry.android.monolithic.sdk.impl.cv.a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r3 = r3.append(r9)
            java.lang.String r4 = ": package=\""
            java.lang.StringBuilder r3 = r3.append(r7)
            java.lang.StringBuilder r3 = r3.append(r10)
            java.lang.String r4 = "\": apk has ad provider library with name=\""
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = r11.a()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = "\" and version=\""
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r4 = r11.b()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = "\" or higher"
            java.lang.StringBuilder r3 = r3.append(r6)
            java.lang.String r3 = r3.toString()
            com.flurry.android.monolithic.sdk.impl.ja.a(r1, r2, r3)
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.cv.a(java.lang.String, java.lang.String, com.flurry.android.monolithic.sdk.impl.cu):boolean");
    }
}
