package com.flurry.android.monolithic.sdk.impl;

public abstract class aej<T> {
    T a;
    aek<T> b;
    aek<T> c;
    int d;

    /* access modifiers changed from: protected */
    public abstract T b(int i);

    protected aej() {
    }

    public T a() {
        b();
        return this.a == null ? b(12) : this.a;
    }

    public final T a(T t, int i) {
        int i2;
        aek<T> aek = new aek<>(t, i);
        if (this.b == null) {
            this.c = aek;
            this.b = aek;
        } else {
            this.c.a(aek);
            this.c = aek;
        }
        this.d += i;
        if (i < 16384) {
            i2 = i + i;
        } else {
            i2 = (i >> 2) + i;
        }
        return b(i2);
    }

    public T b(T t, int i) {
        int i2 = this.d + i;
        T b2 = b(i2);
        int i3 = 0;
        for (aek<T> aek = this.b; aek != null; aek = aek.b()) {
            i3 = aek.a(b2, i3);
        }
        System.arraycopy(t, 0, b2, i3, i);
        int i4 = i3 + i;
        if (i4 == i2) {
            return b2;
        }
        throw new IllegalStateException("Should have gotten " + i2 + " entries, got " + i4);
    }

    /* access modifiers changed from: protected */
    public void b() {
        if (this.c != null) {
            this.a = this.c.a();
        }
        this.c = null;
        this.b = null;
        this.d = 0;
    }
}
