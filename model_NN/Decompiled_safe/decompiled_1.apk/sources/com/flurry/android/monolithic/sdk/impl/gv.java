package com.flurry.android.monolithic.sdk.impl;

import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

final class gv implements hz {
    final /* synthetic */ HashMap a;
    final /* synthetic */ gx b;
    final /* synthetic */ fr c;

    gv(HashMap hashMap, gx gxVar, fr frVar) {
        this.a = hashMap;
        this.b = gxVar;
        this.c = frVar;
    }

    public void a(ft ftVar) {
        ja.a(3, "FlurryAppCloudRequestManager", "RELOGIN SUCCEED, new APPCLOUD_USER_SESSION = " + gr.a);
        this.a.remove(gr.c);
        gr.b(this.a);
        switch (gw.a[this.b.ordinal()]) {
            case 1:
                gr.d.submit(new hf(this.a));
                return;
            case 2:
                gr.d.submit(new hg(this.a));
                return;
            case 3:
                gr.d.submit(new gy(this.a));
                return;
            default:
                return;
        }
    }

    public void a(hy hyVar) {
        ja.a(3, "FlurryAppCloudRequestManager", "RELOGIN FAILED DUE TO : " + hyVar.a() + " : " + hyVar.b());
        ft.a((ft) null);
        try {
            this.c.a(new fq(new JSONObject("{code:\"400\",note:\"invalid user session\"}")));
        } catch (JSONException e) {
            ja.a(6, "FlurryAppCloudRequestManager", "checkForTokenError onError JSONException: ", e);
        } catch (Exception e2) {
            ja.a(6, "FlurryAppCloudRequestManager", "checkForTokenError onError Exception: ", e2);
        }
    }
}
