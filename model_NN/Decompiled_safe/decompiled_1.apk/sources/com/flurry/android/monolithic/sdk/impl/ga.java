package com.flurry.android.monolithic.sdk.impl;

import com.flurry.android.FlurryWalletError;
import com.flurry.android.FlurryWalletHandler;
import com.flurry.android.FlurryWalletInfo;

public final class ga implements fz {
    private final FlurryWalletHandler a;

    public ga(FlurryWalletHandler flurryWalletHandler) {
        this.a = flurryWalletHandler;
    }

    public void a(String str, String str2) {
        if (this.a != null) {
            this.a.onValueUpdated(new FlurryWalletInfo(str, Float.parseFloat(str2)));
        }
    }

    public void a(hy hyVar) {
        if (this.a != null && hyVar != null) {
            this.a.onError(new FlurryWalletError(hyVar.a(), hyVar.b()));
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ga)) {
            return false;
        }
        return this.a == ((ga) obj).a;
    }

    public int hashCode() {
        int i = 17 * 31;
        return (this.a == null ? 0 : this.a.hashCode()) + 527;
    }
}
