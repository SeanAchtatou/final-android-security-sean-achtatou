package com.flurry.android.monolithic.sdk.impl;

final class aaq extends aal {
    private final Class<?> a;
    private final ra<Object> b;

    public aaq(Class<?> cls, ra<Object> raVar) {
        this.a = cls;
        this.b = raVar;
    }

    public ra<Object> a(Class<?> cls) {
        if (cls == this.a) {
            return this.b;
        }
        return null;
    }

    public aal a(Class<?> cls, ra<Object> raVar) {
        return new aam(this.a, this.b, cls, raVar);
    }
}
