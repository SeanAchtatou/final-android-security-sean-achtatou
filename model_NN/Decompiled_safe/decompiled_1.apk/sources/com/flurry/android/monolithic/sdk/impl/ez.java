package com.flurry.android.monolithic.sdk.impl;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class ez {
    private static final String b = ez.class.getSimpleName();
    byte[] a;

    public ez(byte[] bArr) {
        this.a = bArr;
    }

    public ez(fa faVar) throws IOException {
        DataOutputStream dataOutputStream;
        IOException e;
        Throwable th;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            DataOutputStream dataOutputStream2 = new DataOutputStream(byteArrayOutputStream);
            try {
                dataOutputStream2.writeShort(1);
                dataOutputStream2.writeUTF(faVar.a());
                dataOutputStream2.writeLong(faVar.b());
                dataOutputStream2.writeLong(faVar.c());
                dataOutputStream2.writeLong(faVar.d());
                dataOutputStream2.writeUTF(faVar.e());
                dataOutputStream2.writeUTF(faVar.f());
                dataOutputStream2.writeByte(faVar.g());
                dataOutputStream2.writeUTF(faVar.h());
                if (faVar.i() == null) {
                    dataOutputStream2.writeBoolean(false);
                } else {
                    dataOutputStream2.writeBoolean(true);
                    dataOutputStream2.writeDouble(a(faVar.i().getLatitude()));
                    dataOutputStream2.writeDouble(a(faVar.i().getLongitude()));
                    dataOutputStream2.writeFloat(faVar.i().getAccuracy());
                }
                dataOutputStream2.writeInt(faVar.j());
                dataOutputStream2.writeByte(-1);
                dataOutputStream2.writeByte(-1);
                dataOutputStream2.writeByte(faVar.k());
                if (faVar.l() == null) {
                    dataOutputStream2.writeBoolean(false);
                } else {
                    dataOutputStream2.writeBoolean(true);
                    dataOutputStream2.writeLong(faVar.l().longValue());
                }
                Map<String, eh> p = faVar.p();
                dataOutputStream2.writeShort(p.size());
                for (Map.Entry next : p.entrySet()) {
                    dataOutputStream2.writeUTF((String) next.getKey());
                    dataOutputStream2.writeInt(((eh) next.getValue()).a);
                }
                List<ek> n = faVar.n();
                dataOutputStream2.writeShort(n.size());
                for (ek e2 : n) {
                    dataOutputStream2.write(e2.e());
                }
                dataOutputStream2.writeBoolean(faVar.q());
                List<ej> o = faVar.o();
                int i = 0;
                int i2 = 0;
                int i3 = 0;
                while (true) {
                    if (i >= o.size()) {
                        break;
                    }
                    int a2 = o.get(i).a() + i2;
                    if (a2 > 160000) {
                        ja.a(5, b, "Error Log size exceeded. No more event details logged.");
                        break;
                    }
                    i++;
                    i3++;
                    i2 = a2;
                }
                dataOutputStream2.writeInt(faVar.m());
                dataOutputStream2.writeShort(i3);
                for (int i4 = 0; i4 < i3; i4++) {
                    dataOutputStream2.write(o.get(i4).b());
                }
                dataOutputStream2.writeShort(0);
                dataOutputStream2.writeShort(0);
                this.a = byteArrayOutputStream.toByteArray();
                je.a(dataOutputStream2);
            } catch (IOException e3) {
                e = e3;
                dataOutputStream = dataOutputStream2;
                try {
                    ja.a(6, b, "", e);
                    throw e;
                } catch (Throwable th2) {
                    th = th2;
                    je.a(dataOutputStream);
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                dataOutputStream = dataOutputStream2;
                je.a(dataOutputStream);
                throw th;
            }
        } catch (IOException e4) {
            IOException iOException = e4;
            dataOutputStream = null;
            e = iOException;
            ja.a(6, b, "", e);
            throw e;
        } catch (Throwable th4) {
            Throwable th5 = th4;
            dataOutputStream = null;
            th = th5;
            je.a(dataOutputStream);
            throw th;
        }
    }

    public byte[] a() {
        return this.a;
    }

    private double a(double d) {
        return ((double) Math.round(d * 1000.0d)) / 1000.0d;
    }
}
