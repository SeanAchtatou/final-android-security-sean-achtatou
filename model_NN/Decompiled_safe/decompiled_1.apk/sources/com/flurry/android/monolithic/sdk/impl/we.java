package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public abstract class we extends rc {
    protected final Class<?> a;

    /* access modifiers changed from: protected */
    public abstract Object b(String str, qm qmVar) throws Exception;

    protected we(Class<?> cls) {
        this.a = cls;
    }

    public final Object a(String str, qm qmVar) throws IOException, oz {
        if (str == null) {
            return null;
        }
        try {
            Object b = b(str, qmVar);
            if (b != null) {
                return b;
            }
            throw qmVar.a(this.a, str, "not a valid representation");
        } catch (Exception e) {
            throw qmVar.a(this.a, str, "not a valid representation: " + e.getMessage());
        }
    }

    public Class<?> a() {
        return this.a;
    }

    /* access modifiers changed from: protected */
    public int a(String str) throws IllegalArgumentException {
        return Integer.parseInt(str);
    }

    /* access modifiers changed from: protected */
    public long b(String str) throws IllegalArgumentException {
        return Long.parseLong(str);
    }

    /* access modifiers changed from: protected */
    public double c(String str) throws IllegalArgumentException {
        return pt.c(str);
    }
}
