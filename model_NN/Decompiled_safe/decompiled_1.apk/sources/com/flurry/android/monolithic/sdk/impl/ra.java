package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public abstract class ra<T> {
    public abstract void a(Object obj, or orVar, ru ruVar) throws IOException, oz;

    public ra<T> a() {
        return this;
    }

    public boolean b() {
        return false;
    }

    public void a(T t, or orVar, ru ruVar, rx rxVar) throws IOException, oz {
        a(t, orVar, ruVar);
    }

    public Class<T> c() {
        return null;
    }
}
