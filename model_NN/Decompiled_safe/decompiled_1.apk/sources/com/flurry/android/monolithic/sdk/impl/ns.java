package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.lang.reflect.Type;

public class ns<T> extends la<T> {
    public ns() {
        super(nn.b());
    }

    public ns(Class<T> cls) {
        super(nn.b().a((Type) cls), nn.b());
    }

    /* access modifiers changed from: protected */
    public void c(ji jiVar, Object obj, mc mcVar) throws IOException {
        if (!(obj instanceof Enum)) {
            super.c(jiVar, obj, mcVar);
        } else {
            mcVar.a(((Enum) obj).ordinal());
        }
    }
}
