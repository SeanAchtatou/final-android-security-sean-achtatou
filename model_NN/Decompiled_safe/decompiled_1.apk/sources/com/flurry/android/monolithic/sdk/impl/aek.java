package com.flurry.android.monolithic.sdk.impl;

final class aek<T> {
    final T a;
    final int b;
    aek<T> c;

    public aek(T t, int i) {
        this.a = t;
        this.b = i;
    }

    public T a() {
        return this.a;
    }

    public int a(T t, int i) {
        System.arraycopy(this.a, 0, t, i, this.b);
        return this.b + i;
    }

    public aek<T> b() {
        return this.c;
    }

    public void a(aek<T> aek) {
        if (this.c != null) {
            throw new IllegalStateException();
        }
        this.c = aek;
    }
}
