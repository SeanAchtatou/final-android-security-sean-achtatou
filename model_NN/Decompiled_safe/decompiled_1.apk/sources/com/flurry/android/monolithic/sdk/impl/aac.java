package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
public final class aac extends abq<Number> {
    static final aac a = new aac();

    public aac() {
        super(Number.class);
    }

    public void a(Number number, or orVar, ru ruVar) throws IOException, oq {
        orVar.b(number.intValue());
    }
}
