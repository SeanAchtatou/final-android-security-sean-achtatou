package com.flurry.android.monolithic.sdk.impl;

import java.util.Collection;
import java.util.HashMap;

public class za extends yz {
    protected final rf<?> a;
    protected final HashMap<String, String> b;
    protected final HashMap<String, afm> e;

    protected za(rf<?> rfVar, afm afm, HashMap<String, String> hashMap, HashMap<String, afm> hashMap2) {
        super(afm, rfVar.m());
        this.a = rfVar;
        this.b = hashMap;
        this.e = hashMap2;
    }

    public static za a(rf<?> rfVar, afm afm, Collection<yg> collection, boolean z, boolean z2) {
        HashMap hashMap;
        afm afm2;
        HashMap hashMap2 = null;
        if (z == z2) {
            throw new IllegalArgumentException();
        }
        if (z) {
            hashMap = new HashMap();
        } else {
            hashMap = null;
        }
        if (z2) {
            hashMap2 = new HashMap();
        }
        if (collection != null) {
            for (yg next : collection) {
                Class<?> a2 = next.a();
                String b2 = next.c() ? next.b() : a(a2);
                if (z) {
                    hashMap.put(a2.getName(), b2);
                }
                if (z2 && ((afm2 = (afm) hashMap2.get(b2)) == null || !a2.isAssignableFrom(afm2.p()))) {
                    hashMap2.put(b2, rfVar.b(a2));
                }
            }
        }
        return new za(rfVar, afm, hashMap, hashMap2);
    }

    public String a(Object obj) {
        String str;
        Class<?> cls = obj.getClass();
        String name = cls.getName();
        synchronized (this.b) {
            str = this.b.get(name);
            if (str == null) {
                if (this.a.b()) {
                    str = this.a.a().g(((xq) this.a.c(cls)).c());
                }
                if (str == null) {
                    str = a(cls);
                }
                this.b.put(name, str);
            }
        }
        return str;
    }

    public String a(Object obj, Class<?> cls) {
        return a(obj);
    }

    public afm a(String str) throws IllegalArgumentException {
        return this.e.get(str);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append('[').append(getClass().getName());
        sb.append("; id-to-type=").append(this.e);
        sb.append(']');
        return sb.toString();
    }

    protected static String a(Class<?> cls) {
        String name = cls.getName();
        int lastIndexOf = name.lastIndexOf(46);
        return lastIndexOf < 0 ? name : name.substring(lastIndexOf + 1);
    }
}
