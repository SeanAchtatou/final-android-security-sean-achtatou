package com.flurry.android.monolithic.sdk.impl;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public final class aee {
    private final EnumMap<?, pw> a;

    private aee(Map<Enum<?>, pw> map) {
        this.a = new EnumMap<>(map);
    }

    public static aee a(Class<Enum<?>> cls, py pyVar) {
        return b(cls, pyVar);
    }

    public static aee b(Class<Enum<?>> cls, py pyVar) {
        Enum[] enumArr = (Enum[]) adz.g(cls).getEnumConstants();
        if (enumArr != null) {
            HashMap hashMap = new HashMap();
            for (Enum enumR : enumArr) {
                hashMap.put(enumR, new pw(pyVar.a(enumR)));
            }
            return new aee(hashMap);
        }
        throw new IllegalArgumentException("Can not determine enum constants for Class " + cls.getName());
    }

    public static aee c(Class<Enum<?>> cls, py pyVar) {
        Enum[] enumArr = (Enum[]) adz.g(cls).getEnumConstants();
        if (enumArr != null) {
            HashMap hashMap = new HashMap();
            for (Enum enumR : enumArr) {
                hashMap.put(enumR, new pw(enumR.toString()));
            }
            return new aee(hashMap);
        }
        throw new IllegalArgumentException("Can not determine enum constants for Class " + cls.getName());
    }

    public pw a(Enum<?> enumR) {
        return this.a.get(enumR);
    }
}
