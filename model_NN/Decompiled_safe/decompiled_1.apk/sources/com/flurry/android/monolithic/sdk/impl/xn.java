package com.flurry.android.monolithic.sdk.impl;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Member;
import java.lang.reflect.Type;

public final class xn extends xk {
    protected final xo a;
    protected final Type c;
    protected final int d;

    public xn(xo xoVar, Type type, xp xpVar, int i) {
        super(xpVar);
        this.a = xoVar;
        this.c = type;
        this.d = i;
    }

    public xn a(xp xpVar) {
        return xpVar == this.b ? this : this.a.a(this.d, xpVar);
    }

    public AnnotatedElement a() {
        return null;
    }

    public String b() {
        return "";
    }

    public <A extends Annotation> A a(Class<A> cls) {
        return this.b.a(cls);
    }

    public Type c() {
        return this.c;
    }

    public Class<?> d() {
        if (this.c instanceof Class) {
            return (Class) this.c;
        }
        return adk.a().a(this.c).p();
    }

    public Class<?> h() {
        return this.a.h();
    }

    public Member i() {
        return this.a.i();
    }

    public void a(Object obj, Object obj2) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Cannot call setValue() on constructor parameter of " + h().getName());
    }

    public Type e() {
        return this.c;
    }

    public xo f() {
        return this.a;
    }

    public int g() {
        return this.d;
    }

    public String toString() {
        return "[parameter #" + g() + ", annotations: " + this.b + "]";
    }
}
