package com.flurry.android.monolithic.sdk.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public class zi extends zd {
    public static final zi e = new zi(null);
    protected final rt f;

    protected zi(rt rtVar) {
        rt rtVar2;
        if (rtVar == null) {
            rtVar2 = new zj();
        } else {
            rtVar2 = rtVar;
        }
        this.f = rtVar2;
    }

    /* access modifiers changed from: protected */
    public Iterable<rv> a() {
        return this.f.c();
    }

    public ra<Object> a(rq rqVar, afm afm, qc qcVar) throws qw {
        xq xqVar = (xq) rqVar.b(afm);
        ra<Object> a = a(rqVar, xqVar.c(), qcVar);
        if (a != null) {
            return a;
        }
        afm a2 = a(rqVar, xqVar.c(), afm);
        boolean z = a2 != afm;
        if (afm.f()) {
            return b(rqVar, a2, xqVar, qcVar, z);
        }
        for (rv a3 : this.f.c()) {
            ra<?> a4 = a3.a(rqVar, a2, xqVar, qcVar);
            if (a4 != null) {
                return a4;
            }
        }
        ra<?> a5 = a(a2, rqVar, xqVar, qcVar, z);
        if (a5 != null) {
            return a5;
        }
        ra<?> b = b(a2, rqVar, xqVar, qcVar, z);
        if (b != null) {
            return b;
        }
        ra<Object> a6 = a(rqVar, a2, xqVar, qcVar);
        if (a6 == null) {
            return a(rqVar, a2, xqVar, qcVar, z);
        }
        return a6;
    }

    public ra<Object> c(rq rqVar, afm afm, qc qcVar) {
        ra<?> raVar = null;
        if (!this.f.a()) {
            return null;
        }
        xq xqVar = (xq) rqVar.c(afm.p());
        for (rv a : this.f.d()) {
            raVar = a.a(rqVar, afm, xqVar, qcVar);
            if (raVar != null) {
                return raVar;
            }
        }
        return raVar;
    }

    public ra<Object> a(rq rqVar, afm afm, xq xqVar, qc qcVar) throws qw {
        if (!b(afm.p())) {
            return null;
        }
        ra<?> a = a(rqVar, xqVar, qcVar);
        if (!this.f.b()) {
            return a;
        }
        for (zk a2 : this.f.e()) {
            a = a2.a(rqVar, xqVar, a);
        }
        return a;
    }

    public rx a(afm afm, rq rqVar, xk xkVar, qc qcVar) throws qw {
        py a = rqVar.a();
        yj<?> a2 = a.a(rqVar, xkVar, afm);
        if (a2 == null) {
            return b(rqVar, afm, qcVar);
        }
        return a2.a(rqVar, afm, rqVar.l().a(xkVar, rqVar, a), qcVar);
    }

    public rx b(afm afm, rq rqVar, xk xkVar, qc qcVar) throws qw {
        afm g = afm.g();
        py a = rqVar.a();
        yj<?> b = a.b(rqVar, xkVar, afm);
        if (b == null) {
            return b(rqVar, g, qcVar);
        }
        return b.a(rqVar, g, rqVar.l().a(xkVar, rqVar, a), qcVar);
    }

    /* access modifiers changed from: protected */
    public ra<Object> a(rq rqVar, xq xqVar, qc qcVar) throws qw {
        zh zhVar;
        List<zf> list;
        List<zf> list2;
        if (xqVar.b() == Object.class) {
            throw new IllegalArgumentException("Can not create bean serializer for Object.class");
        }
        zh a = a(xqVar);
        List<zf> c = c(rqVar, xqVar);
        if (c == null) {
            c = new ArrayList<>();
        }
        if (this.f.b()) {
            Iterator<zk> it = this.f.e().iterator();
            while (true) {
                list2 = c;
                if (!it.hasNext()) {
                    break;
                }
                c = it.next().a(rqVar, xqVar, list2);
            }
            c = list2;
        }
        List<zf> b = b(rqVar, xqVar, a(rqVar, xqVar, c));
        if (this.f.b()) {
            Iterator<zk> it2 = this.f.e().iterator();
            while (true) {
                list = b;
                if (!it2.hasNext()) {
                    break;
                }
                b = it2.next().b(rqVar, xqVar, list);
            }
            b = list;
        }
        a.a(b);
        a.a(b(rqVar, xqVar));
        xl p = xqVar.p();
        if (p != null) {
            if (rqVar.a(rr.CAN_OVERRIDE_ACCESS_MODIFIERS)) {
                p.k();
            }
            afm a2 = p.a(xqVar.j());
            a.a(new zc(p, abl.a(null, a2, rqVar.a(rr.USE_STATIC_TYPING), b(rqVar, a2.g(), qcVar), qcVar, null, null)));
        }
        a(rqVar, a);
        if (this.f.b()) {
            zh zhVar2 = a;
            for (zk a3 : this.f.e()) {
                zhVar2 = a3.a(rqVar, xqVar, zhVar2);
            }
            zhVar = zhVar2;
        } else {
            zhVar = a;
        }
        ra<?> b2 = zhVar.b();
        if (b2 != null || !xqVar.h()) {
            return b2;
        }
        return zhVar.c();
    }

    /* access modifiers changed from: protected */
    public zf a(zf zfVar, Class<?>[] clsArr) {
        return zm.a(zfVar, clsArr);
    }

    /* access modifiers changed from: protected */
    public zp a(rq rqVar, xq xqVar) {
        return new zp(rqVar, xqVar);
    }

    /* access modifiers changed from: protected */
    public zh a(xq xqVar) {
        return new zh(xqVar);
    }

    /* access modifiers changed from: protected */
    public Object b(rq rqVar, xq xqVar) {
        return rqVar.a().f(xqVar.c());
    }

    /* access modifiers changed from: protected */
    public boolean b(Class<?> cls) {
        return adz.a(cls) == null && !adz.c(cls);
    }

    /* access modifiers changed from: protected */
    public List<zf> c(rq rqVar, xq xqVar) throws qw {
        List<qe> d = xqVar.d();
        py a = rqVar.a();
        c(rqVar, xqVar, d);
        if (rqVar.a(rr.REQUIRE_SETTERS_FOR_GETTERS)) {
            d(rqVar, xqVar, d);
        }
        if (d.isEmpty()) {
            return null;
        }
        boolean a2 = a(rqVar, xqVar, (rx) null, (qc) null);
        zp a3 = a(rqVar, xqVar);
        ArrayList arrayList = new ArrayList(d.size());
        adj j = xqVar.j();
        for (qe next : d) {
            xk j2 = next.j();
            pz a4 = a.a(j2);
            if (a4 == null || !a4.c()) {
                String a5 = next.a();
                if (j2 instanceof xl) {
                    arrayList.add(a(rqVar, j, a3, a2, a5, (xl) j2));
                } else {
                    arrayList.add(a(rqVar, j, a3, a2, a5, (xj) j2));
                }
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public List<zf> a(rq rqVar, xq xqVar, List<zf> list) {
        String[] c = rqVar.a().c(xqVar.c());
        if (c != null && c.length > 0) {
            HashSet a = adp.a(c);
            Iterator<zf> it = list.iterator();
            while (it.hasNext()) {
                if (a.contains(it.next().d())) {
                    it.remove();
                }
            }
        }
        return list;
    }

    /* access modifiers changed from: protected */
    @Deprecated
    public List<zf> b(rq rqVar, xq xqVar, List<zf> list) {
        return list;
    }

    /* access modifiers changed from: protected */
    public void a(rq rqVar, zh zhVar) {
        int i = 0;
        List<zf> a = zhVar.a();
        boolean a2 = rqVar.a(rr.DEFAULT_VIEW_INCLUSION);
        int size = a.size();
        zf[] zfVarArr = new zf[size];
        int i2 = 0;
        while (i < size) {
            zf zfVar = a.get(i);
            Class<?>[] h = zfVar.h();
            if (h != null) {
                i2++;
                zfVarArr[i] = a(zfVar, h);
            } else if (a2) {
                zfVarArr[i] = zfVar;
            }
            i++;
            i2 = i2;
        }
        if (!a2 || i2 != 0) {
            zhVar.a(zfVarArr);
        }
    }

    /* access modifiers changed from: protected */
    public void c(rq rqVar, xq xqVar, List<qe> list) {
        Boolean bool;
        py a = rqVar.a();
        HashMap hashMap = new HashMap();
        Iterator<qe> it = list.iterator();
        while (it.hasNext()) {
            xk j = it.next().j();
            if (j == null) {
                it.remove();
            } else {
                Class<?> d = j.d();
                Boolean bool2 = (Boolean) hashMap.get(d);
                if (bool2 == null) {
                    Boolean e2 = a.e(((xq) rqVar.c(d)).c());
                    if (e2 == null) {
                        e2 = Boolean.FALSE;
                    }
                    hashMap.put(d, e2);
                    bool = e2;
                } else {
                    bool = bool2;
                }
                if (bool.booleanValue()) {
                    it.remove();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void d(rq rqVar, xq xqVar, List<qe> list) {
        Iterator<qe> it = list.iterator();
        while (it.hasNext()) {
            if (!it.next().f()) {
                it.remove();
            }
        }
    }

    /* access modifiers changed from: protected */
    public zf a(rq rqVar, adj adj, zp zpVar, boolean z, String str, xk xkVar) throws qw {
        rx rxVar;
        if (rqVar.a(rr.CAN_OVERRIDE_ACCESS_MODIFIERS)) {
            xkVar.k();
        }
        afm a = xkVar.a(adj);
        qd qdVar = new qd(str, a, zpVar.a(), xkVar);
        ra<Object> a2 = a(rqVar, xkVar, qdVar);
        if (adz.e(a.p())) {
            rxVar = b(a, rqVar, xkVar, qdVar);
        } else {
            rxVar = null;
        }
        zf a3 = zpVar.a(str, a, a2, a(a, rqVar, xkVar, qdVar), rxVar, xkVar, z);
        a3.a(rqVar.a().g(xkVar));
        return a3;
    }
}
