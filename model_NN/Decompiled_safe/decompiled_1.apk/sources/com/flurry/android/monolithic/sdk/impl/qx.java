package com.flurry.android.monolithic.sdk.impl;

import java.io.Serializable;

public class qx implements Serializable {
    protected Object a;
    protected String b;
    protected int c = -1;

    protected qx() {
    }

    public qx(Object obj, String str) {
        this.a = obj;
        if (str == null) {
            throw new NullPointerException("Can not pass null fieldName");
        }
        this.b = str;
    }

    public qx(Object obj, int i) {
        this.a = obj;
        this.c = i;
    }

    public String toString() {
        Class<?> cls;
        StringBuilder sb = new StringBuilder();
        if (this.a instanceof Class) {
            cls = (Class) this.a;
        } else {
            cls = this.a.getClass();
        }
        Package packageR = cls.getPackage();
        if (packageR != null) {
            sb.append(packageR.getName());
            sb.append('.');
        }
        sb.append(cls.getSimpleName());
        sb.append('[');
        if (this.b != null) {
            sb.append('\"');
            sb.append(this.b);
            sb.append('\"');
        } else if (this.c >= 0) {
            sb.append(this.c);
        } else {
            sb.append('?');
        }
        sb.append(']');
        return sb.toString();
    }
}
