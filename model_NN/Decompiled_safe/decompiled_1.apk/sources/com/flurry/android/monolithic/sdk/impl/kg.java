package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

class kg extends kb {
    private List<js> i;
    private Map<String, js> j;
    private final boolean k;

    public kg(ka kaVar, String str, boolean z) {
        super(kj.RECORD, kaVar, str);
        this.k = z;
    }

    public boolean h() {
        return this.k;
    }

    public js b(String str) {
        if (this.j != null) {
            return this.j.get(str);
        }
        throw new jg("Schema fields not set yet");
    }

    public List<js> b() {
        if (this.i != null) {
            return this.i;
        }
        throw new jg("Schema fields not set yet");
    }

    public void b(List<js> list) {
        if (this.i != null) {
            throw new jg("Fields are already set");
        }
        this.j = new HashMap();
        jx jxVar = new jx();
        int i2 = 0;
        for (js next : list) {
            if (next.b != -1) {
                throw new jg("Field already used: " + next);
            }
            int unused = next.b = i2;
            this.j.put(next.a(), next);
            jxVar.add(next);
            i2++;
        }
        this.i = jxVar.a();
        this.d = Integer.MIN_VALUE;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof kg)) {
            return false;
        }
        kg kgVar = (kg) obj;
        if (!c(kgVar)) {
            return false;
        }
        if (!a((kb) kgVar)) {
            return false;
        }
        if (!this.c.equals(kgVar.c)) {
            return false;
        }
        Set set = (Set) ji.i.get();
        kh khVar = new kh(this, obj, null);
        if (set.contains(khVar)) {
            return true;
        }
        boolean isEmpty = set.isEmpty();
        try {
            set.add(khVar);
            return this.i.equals(((kg) obj).i);
        } finally {
            if (isEmpty) {
                set.clear();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int m() {
        Map map = (Map) ji.j.get();
        if (map.containsKey(this)) {
            return 0;
        }
        boolean isEmpty = map.isEmpty();
        try {
            map.put(this, this);
            return super.m() + this.i.hashCode();
        } finally {
            if (isEmpty) {
                map.clear();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(kc kcVar, or orVar) throws IOException {
        if (!c(kcVar, orVar)) {
            String a = kcVar.a;
            orVar.d();
            orVar.a("type", this.k ? "error" : "record");
            d(kcVar, orVar);
            String unused = kcVar.a = this.f.b;
            if (e() != null) {
                orVar.a("doc", e());
            }
            orVar.a("fields");
            b(kcVar, orVar);
            this.c.a(orVar);
            a(orVar);
            orVar.e();
            String unused2 = kcVar.a = a;
        }
    }

    /* access modifiers changed from: package-private */
    public void b(kc kcVar, or orVar) throws IOException {
        orVar.b();
        for (js next : this.i) {
            orVar.d();
            orVar.a("name", next.a());
            orVar.a("type");
            next.c().a(kcVar, orVar);
            if (next.d() != null) {
                orVar.a("doc", next.d());
            }
            if (next.e() != null) {
                orVar.a("default");
                orVar.a(next.e());
            }
            if (next.f() != jt.ASCENDING) {
                orVar.a("order", jt.a(next.f()));
            }
            if (!(next.g == null || next.g.size() == 0)) {
                orVar.a("aliases");
                orVar.b();
                for (String b : next.g) {
                    orVar.b(b);
                }
                orVar.c();
            }
            next.h.a(orVar);
            orVar.e();
        }
        orVar.c();
    }
}
