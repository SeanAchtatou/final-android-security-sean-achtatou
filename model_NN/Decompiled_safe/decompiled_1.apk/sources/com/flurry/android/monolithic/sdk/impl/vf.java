package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
final class vf extends ve<boolean[]> {
    public vf() {
        super(boolean[].class);
    }

    /* renamed from: b */
    public boolean[] a(ow owVar, qm qmVar) throws IOException, oz {
        if (!owVar.j()) {
            return c(owVar, qmVar);
        }
        adr a = qmVar.h().a();
        boolean[] zArr = (boolean[]) a.a();
        int i = 0;
        while (owVar.b() != pb.END_ARRAY) {
            boolean n = n(owVar, qmVar);
            if (i >= zArr.length) {
                zArr = (boolean[]) a.a(zArr, i);
                i = 0;
            }
            zArr[i] = n;
            i++;
        }
        return (boolean[]) a.b(zArr, i);
    }

    private final boolean[] c(ow owVar, qm qmVar) throws IOException, oz {
        if (owVar.e() == pb.VALUE_STRING && qmVar.a(ql.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT) && owVar.k().length() == 0) {
            return null;
        }
        if (!qmVar.a(ql.ACCEPT_SINGLE_VALUE_AS_ARRAY)) {
            throw qmVar.b(this.q);
        }
        return new boolean[]{n(owVar, qmVar)};
    }
}
