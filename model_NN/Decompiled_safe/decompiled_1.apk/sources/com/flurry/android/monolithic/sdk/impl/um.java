package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.ArrayList;

public abstract class um<T> extends wv<T> {
    /* access modifiers changed from: protected */
    public abstract T a(String str, qm qmVar) throws IOException, oz;

    protected um(Class<?> cls) {
        super(cls);
    }

    public static Iterable<um<?>> d() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new uv());
        arrayList.add(new uu());
        arrayList.add(new ut());
        arrayList.add(new uo());
        arrayList.add(new ur());
        arrayList.add(new uq());
        arrayList.add(new up());
        arrayList.add(new us());
        arrayList.add(new un());
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.um.a(java.lang.Object, com.flurry.android.monolithic.sdk.impl.qm):T
     arg types: [T, com.flurry.android.monolithic.sdk.impl.qm]
     candidates:
      com.flurry.android.monolithic.sdk.impl.um.a(com.flurry.android.monolithic.sdk.impl.ow, com.flurry.android.monolithic.sdk.impl.qm):T
      com.flurry.android.monolithic.sdk.impl.um.a(java.lang.String, com.flurry.android.monolithic.sdk.impl.qm):T
      com.flurry.android.monolithic.sdk.impl.qu.a(com.flurry.android.monolithic.sdk.impl.ow, com.flurry.android.monolithic.sdk.impl.qm):T
      com.flurry.android.monolithic.sdk.impl.um.a(java.lang.Object, com.flurry.android.monolithic.sdk.impl.qm):T */
    public final T a(ow owVar, qm qmVar) throws IOException, oz {
        if (owVar.e() == pb.VALUE_STRING) {
            String trim = owVar.k().trim();
            if (trim.length() == 0) {
                return null;
            }
            try {
                T a = a(trim, qmVar);
                if (a != null) {
                    return a;
                }
            } catch (IllegalArgumentException e) {
            }
            throw qmVar.b(this.q, "not a valid textual representation");
        } else if (owVar.e() == pb.VALUE_EMBEDDED_OBJECT) {
            T z = owVar.z();
            if (z == null) {
                return null;
            }
            return !this.q.isAssignableFrom(z.getClass()) ? a((Object) z, qmVar) : z;
        } else {
            throw qmVar.b(this.q);
        }
    }

    /* access modifiers changed from: protected */
    public T a(Object obj, qm qmVar) throws IOException, oz {
        throw qmVar.b("Don't know how to convert embedded Object of type " + obj.getClass().getName() + " into " + this.q.getName());
    }
}
