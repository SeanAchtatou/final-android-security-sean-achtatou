package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.io.OutputStream;

public class mb extends lr {
    private OutputStream a;
    private final byte[] b = new byte[12];

    mb(OutputStream outputStream) {
        a(outputStream);
    }

    /* access modifiers changed from: package-private */
    public mb a(OutputStream outputStream) {
        if (outputStream == null) {
            throw new NullPointerException("OutputStream cannot be null!");
        }
        this.a = outputStream;
        return this;
    }

    public void flush() throws IOException {
        this.a.flush();
    }

    public void a(boolean z) throws IOException {
        this.a.write(z ? 1 : 0);
    }

    public void c(int i) throws IOException {
        int i2 = (i << 1) ^ (i >> 31);
        if ((i2 & -128) == 0) {
            this.a.write(i2);
        } else if ((i2 & -16384) == 0) {
            this.a.write(i2 | 128);
            this.a.write(i2 >>> 7);
        } else {
            this.a.write(this.b, 0, lg.a(i, this.b, 0));
        }
    }

    public void b(long j) throws IOException {
        long j2 = (j << 1) ^ (j >> 63);
        if ((-2147483648L & j2) == 0) {
            int i = (int) j2;
            while ((i & -128) != 0) {
                this.a.write((byte) ((i | 128) & 255));
                i >>>= 7;
            }
            this.a.write((byte) i);
            return;
        }
        this.a.write(this.b, 0, lg.a(j, this.b, 0));
    }

    public void a(float f) throws IOException {
        this.a.write(this.b, 0, lg.a(f, this.b, 0));
    }

    public void a(double d) throws IOException {
        byte[] bArr = new byte[8];
        this.a.write(bArr, 0, lg.a(d, bArr, 0));
    }

    public void b(byte[] bArr, int i, int i2) throws IOException {
        this.a.write(bArr, i, i2);
    }

    /* access modifiers changed from: protected */
    public void g() throws IOException {
        this.a.write(0);
    }
}
