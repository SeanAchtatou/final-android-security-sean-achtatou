package com.flurry.android.monolithic.sdk.impl;

import android.location.Location;
import java.util.List;
import java.util.Map;

public class fa {
    private String a;
    private long b = -1;
    private long c = -1;
    private long d = -1;
    private String e;
    private String f;
    private int g = -1;
    private String h;
    private Location i;
    private int j = -1;
    private byte k = -1;
    private Long l;
    private List<ek> m;
    private int n = -1;
    private List<ej> o;
    private Map<String, eh> p;
    private boolean q = false;

    public void a(String str) {
        this.a = str;
    }

    public String a() {
        return this.a;
    }

    public void a(long j2) {
        this.b = j2;
    }

    public long b() {
        return this.b;
    }

    public void b(long j2) {
        this.c = j2;
    }

    public long c() {
        return this.c;
    }

    public void c(long j2) {
        this.d = j2;
    }

    public long d() {
        return this.d;
    }

    public void b(String str) {
        this.e = str;
    }

    public String e() {
        return this.e;
    }

    public void c(String str) {
        this.f = str;
    }

    public String f() {
        return this.f;
    }

    public void a(int i2) {
        this.g = i2;
    }

    public int g() {
        return this.g;
    }

    public void d(String str) {
        this.h = str;
    }

    public String h() {
        return this.h;
    }

    public void a(Location location) {
        this.i = location;
    }

    public Location i() {
        return this.i;
    }

    public void b(int i2) {
        this.j = i2;
    }

    public int j() {
        return this.j;
    }

    public void a(byte b2) {
        this.k = b2;
    }

    public byte k() {
        return this.k;
    }

    public void a(Long l2) {
        this.l = l2;
    }

    public Long l() {
        return this.l;
    }

    public void c(int i2) {
        this.n = i2;
    }

    public int m() {
        return this.n;
    }

    public void a(List<ek> list) {
        this.m = list;
    }

    public List<ek> n() {
        return this.m;
    }

    public void b(List<ej> list) {
        this.o = list;
    }

    public List<ej> o() {
        return this.o;
    }

    public void a(Map<String, eh> map) {
        this.p = map;
    }

    public Map<String, eh> p() {
        return this.p;
    }

    public void a(boolean z) {
        this.q = z;
    }

    public boolean q() {
        return this.q;
    }
}
