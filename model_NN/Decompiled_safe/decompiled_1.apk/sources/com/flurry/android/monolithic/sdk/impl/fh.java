package com.flurry.android.monolithic.sdk.impl;

import java.util.Timer;

class fh {
    private Timer a;
    private fi b;
    private fj c;

    fh(fj fjVar) {
        this.c = fjVar;
    }

    public synchronized void a(long j) {
        if (b()) {
            a();
        }
        this.a = new Timer("FlurrySessionTimer");
        this.b = new fi(this, this.c);
        this.a.schedule(this.b, j);
    }

    public synchronized void a() {
        if (this.a != null) {
            this.a.cancel();
            this.a = null;
        }
        this.b = null;
    }

    public boolean b() {
        if (this.a != null) {
            return true;
        }
        return false;
    }
}
