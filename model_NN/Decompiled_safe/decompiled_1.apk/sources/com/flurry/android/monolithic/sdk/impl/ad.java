package com.flurry.android.monolithic.sdk.impl;

import com.flurry.android.AdCreative;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ad {
    private static final String a = ad.class.getSimpleName();
    private bz b = new bz();
    private cb c = new cb();
    private int d = 0;
    private final Map<String, Integer> e = new HashMap();

    public ad() {
        this.b.a(this.c);
    }

    public synchronized void a(List<AdUnit> list) {
        int f = je.f();
        ja.a(3, a, "putting " + list.size() + " orientation: " + f);
        for (AdUnit next : list) {
            if (next.g().length() > 0) {
                ce.a().a(new cd(next.g().toString(), next.h().longValue(), next.l().longValue(), next.i().intValue(), next.j().intValue(), next.k().intValue()));
            }
            this.b.a(next.b().toString(), f, next);
        }
    }

    public synchronized void a(String str, String str2) {
        this.b.a(str, je.f(), str2);
    }

    public synchronized void a(String str, int i) {
        this.b.a(str, je.f(), i);
    }

    public synchronized void a(String str) {
        if (str != null) {
            if (str.length() > 0) {
                this.d = this.b.b(str, je.f());
                this.e.put(str, Integer.valueOf(this.d));
            }
        }
    }

    public synchronized int b(String str) {
        int i;
        Integer num = this.e.get(str);
        if (num != null) {
            i = num.intValue();
        } else {
            i = 0;
        }
        return i;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0070  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized com.flurry.android.impl.ads.avro.protocol.v6.AdUnit c(java.lang.String r9) {
        /*
            r8 = this;
            r6 = 0
            r5 = 1
            monitor-enter(r8)
            int r0 = com.flurry.android.monolithic.sdk.impl.je.f()     // Catch:{ all -> 0x00d1 }
            com.flurry.android.monolithic.sdk.impl.bz r1 = r8.b     // Catch:{ all -> 0x00d1 }
            java.util.List r1 = r1.a(r9, r0)     // Catch:{ all -> 0x00d1 }
            boolean r2 = r8.b(r1)     // Catch:{ all -> 0x00d1 }
            if (r2 == 0) goto L_0x00f5
            r1 = 3
            java.lang.String r2 = com.flurry.android.monolithic.sdk.impl.ad.a     // Catch:{ all -> 0x00d1 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d1 }
            r3.<init>()     // Catch:{ all -> 0x00d1 }
            java.lang.String r4 = "no valid ad units in cache for current orientation for "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00d1 }
            java.lang.StringBuilder r3 = r3.append(r9)     // Catch:{ all -> 0x00d1 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x00d1 }
            com.flurry.android.monolithic.sdk.impl.ja.a(r1, r2, r3)     // Catch:{ all -> 0x00d1 }
            if (r0 != r5) goto L_0x0065
            r0 = 2
            r1 = r0
        L_0x0030:
            com.flurry.android.monolithic.sdk.impl.bz r0 = r8.b     // Catch:{ all -> 0x00d1 }
            java.util.List r2 = r0.a(r9, r1)     // Catch:{ all -> 0x00d1 }
            boolean r0 = r8.b(r2)     // Catch:{ all -> 0x00d1 }
            if (r0 != 0) goto L_0x0049
            r0 = 0
            java.lang.Object r0 = r2.get(r0)     // Catch:{ all -> 0x00d1 }
            com.flurry.android.impl.ads.avro.protocol.v6.AdUnit r0 = (com.flurry.android.impl.ads.avro.protocol.v6.AdUnit) r0     // Catch:{ all -> 0x00d1 }
            boolean r0 = r8.a(r0)     // Catch:{ all -> 0x00d1 }
            if (r0 != 0) goto L_0x0067
        L_0x0049:
            r0 = 3
            java.lang.String r1 = com.flurry.android.monolithic.sdk.impl.ad.a     // Catch:{ all -> 0x00d1 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d1 }
            r2.<init>()     // Catch:{ all -> 0x00d1 }
            java.lang.String r3 = "no valid ad units in cache for other orientation for "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x00d1 }
            java.lang.StringBuilder r2 = r2.append(r9)     // Catch:{ all -> 0x00d1 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x00d1 }
            com.flurry.android.monolithic.sdk.impl.ja.a(r0, r1, r2)     // Catch:{ all -> 0x00d1 }
            r0 = r6
        L_0x0063:
            monitor-exit(r8)
            return r0
        L_0x0065:
            r1 = r5
            goto L_0x0030
        L_0x0067:
            r7 = r2
            r2 = r1
            r1 = r7
        L_0x006a:
            boolean r0 = r8.b(r1)     // Catch:{ all -> 0x00d1 }
            if (r0 != 0) goto L_0x00d4
            java.util.Iterator r3 = r1.iterator()     // Catch:{ all -> 0x00d1 }
        L_0x0074:
            boolean r0 = r3.hasNext()     // Catch:{ all -> 0x00d1 }
            if (r0 == 0) goto L_0x00d4
            java.lang.Object r0 = r3.next()     // Catch:{ all -> 0x00d1 }
            com.flurry.android.impl.ads.avro.protocol.v6.AdUnit r0 = (com.flurry.android.impl.ads.avro.protocol.v6.AdUnit) r0     // Catch:{ all -> 0x00d1 }
            java.lang.Long r4 = r0.c()     // Catch:{ all -> 0x00d1 }
            long r4 = r4.longValue()     // Catch:{ all -> 0x00d1 }
            boolean r4 = com.flurry.android.monolithic.sdk.impl.je.a(r4)     // Catch:{ all -> 0x00d1 }
            if (r4 == 0) goto L_0x0074
            java.util.List r4 = r0.d()     // Catch:{ all -> 0x00d1 }
            int r4 = r4.size()     // Catch:{ all -> 0x00d1 }
            if (r4 <= 0) goto L_0x0074
            com.flurry.android.monolithic.sdk.impl.bz r1 = r8.b     // Catch:{ all -> 0x00d1 }
            boolean r1 = r1.b(r9, r2, r0)     // Catch:{ all -> 0x00d1 }
            r2 = 3
            java.lang.String r3 = com.flurry.android.monolithic.sdk.impl.ad.a     // Catch:{ all -> 0x00d1 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d1 }
            r4.<init>()     // Catch:{ all -> 0x00d1 }
            java.lang.String r5 = "found valid ad unit for "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x00d1 }
            java.lang.StringBuilder r4 = r4.append(r9)     // Catch:{ all -> 0x00d1 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x00d1 }
            com.flurry.android.monolithic.sdk.impl.ja.a(r2, r3, r4)     // Catch:{ all -> 0x00d1 }
            r2 = 3
            java.lang.String r3 = com.flurry.android.monolithic.sdk.impl.ad.a     // Catch:{ all -> 0x00d1 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d1 }
            r4.<init>()     // Catch:{ all -> 0x00d1 }
            java.lang.String r5 = "Ad unit was removed = = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x00d1 }
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ all -> 0x00d1 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00d1 }
            com.flurry.android.monolithic.sdk.impl.ja.a(r2, r3, r1)     // Catch:{ all -> 0x00d1 }
            goto L_0x0063
        L_0x00d1:
            r0 = move-exception
            monitor-exit(r8)
            throw r0
        L_0x00d4:
            r0 = 3
            java.lang.String r2 = com.flurry.android.monolithic.sdk.impl.ad.a     // Catch:{ all -> 0x00d1 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d1 }
            r3.<init>()     // Catch:{ all -> 0x00d1 }
            java.lang.String r4 = "no valid ad units for "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00d1 }
            java.lang.StringBuilder r3 = r3.append(r9)     // Catch:{ all -> 0x00d1 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x00d1 }
            com.flurry.android.monolithic.sdk.impl.ja.a(r0, r2, r3)     // Catch:{ all -> 0x00d1 }
            com.flurry.android.monolithic.sdk.impl.cb r0 = r8.c     // Catch:{ all -> 0x00d1 }
            r0.a(r9, r1)     // Catch:{ all -> 0x00d1 }
            r0 = r6
            goto L_0x0063
        L_0x00f5:
            r2 = r0
            goto L_0x006a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.ad.c(java.lang.String):com.flurry.android.impl.ads.avro.protocol.v6.AdUnit");
    }

    public boolean a(AdUnit adUnit) {
        return adUnit.d().get(0).e().e().equals(AdCreative.kFormatTakeover);
    }

    public boolean b(List<AdUnit> list) {
        return list == null || list.size() == 0 || list.get(0).d().size() == 0 || list.get(0).d().get(0).e() == null;
    }

    public synchronized List<AdUnit> a(String str, int i, int i2) {
        int i3;
        List<AdUnit> list;
        ArrayList arrayList;
        int f = je.f();
        List<AdUnit> a2 = this.b.a(str, f);
        if (b(a2)) {
            ja.a(3, a, "no valid ad units in cache for current orientation for " + str);
            if (f == 1) {
                i3 = 2;
            } else {
                i3 = 1;
            }
            List<AdUnit> a3 = this.b.a(str, i3);
            if (b(a3) || !a(a3.get(0))) {
                ja.a(3, a, "no valid ad units in cache for other orientation for " + str);
            }
            list = a3;
        } else {
            List<AdUnit> list2 = a2;
            i3 = f;
            list = list2;
        }
        arrayList = new ArrayList();
        if (list != null) {
            Iterator<AdUnit> it = list.iterator();
            while (it.hasNext() && arrayList.size() <= i2) {
                AdUnit next = it.next();
                if (je.a(next.c().longValue()) && next.e().intValue() == 1 && next.d().size() > 0) {
                    arrayList.add(next);
                    ja.a(3, a, "Ad unit was removed = " + this.b.b(str, i3, next));
                }
            }
        }
        return arrayList;
    }

    public synchronized boolean d(String str) {
        boolean z;
        List<AdUnit> a2 = this.b.a(str, je.f());
        if (!b(a2)) {
            Iterator<AdUnit> it = a2.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (je.a(it.next().c().longValue())) {
                        z = true;
                        break;
                    }
                } else {
                    break;
                }
            }
        }
        z = false;
        return z;
    }

    public synchronized int e(String str) {
        return this.b.b(str, je.f());
    }
}
