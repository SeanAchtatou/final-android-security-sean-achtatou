package com.flurry.android.monolithic.sdk.impl;

import com.flurry.android.FlurryFullscreenTakeoverActivity;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.http.NameValuePair;

public class gr {
    public static String a = "";
    private static String b = "";
    /* access modifiers changed from: private */
    public static String c = "del_internal";
    /* access modifiers changed from: private */
    public static ExecutorService d = Executors.newCachedThreadPool();

    public static void a(String str) {
        b = hh.a(str, str);
    }

    /* access modifiers changed from: private */
    public static void b(HashMap<String, Object> hashMap) {
        hashMap.put("session", a);
        hashMap.put("auth", b);
    }

    public static void a(boolean z, String str, List<NameValuePair> list, fr frVar) throws Exception {
        HashMap hashMap = new HashMap();
        b(hashMap);
        hashMap.put(c, new gs());
        hashMap.put("del", frVar);
        hashMap.put(FlurryFullscreenTakeoverActivity.EXTRA_KEY_URL, str);
        hashMap.put("data", list);
        d.submit(new hf(hashMap));
    }

    public static void b(boolean z, String str, List<NameValuePair> list, fr frVar) throws Exception {
        HashMap hashMap = new HashMap();
        b(hashMap);
        hashMap.put(c, new gt());
        hashMap.put("del", frVar);
        hashMap.put(FlurryFullscreenTakeoverActivity.EXTRA_KEY_URL, str);
        if (list == null) {
            hashMap.put("data", new ArrayList());
        } else {
            hashMap.put("data", list);
        }
        d.submit(new hg(hashMap));
    }

    public static void c(boolean z, String str, List<NameValuePair> list, fr frVar) throws Exception {
        HashMap hashMap = new HashMap();
        b(hashMap);
        hashMap.put(c, new gu());
        hashMap.put("del", frVar);
        if (list != null) {
            hashMap.put(FlurryFullscreenTakeoverActivity.EXTRA_KEY_URL, str + "/" + hh.a(list));
        } else {
            hashMap.put(FlurryFullscreenTakeoverActivity.EXTRA_KEY_URL, str + "/");
        }
        d.submit(new gy(hashMap));
    }

    /* access modifiers changed from: private */
    public static synchronized void b(fq fqVar, fr frVar, gx gxVar, HashMap<String, Object> hashMap) {
        synchronized (gr.class) {
            try {
                if (fqVar.a()) {
                    frVar.a(fqVar);
                } else if (fqVar.d() != 400 || !fqVar.b().equals("invalid user session")) {
                    frVar.a(fqVar);
                } else {
                    ja.a(5, "FlurryAppCloudRequestManager", "CURRENT USER SESSION = " + a + " IS NOT VALID!");
                    a = "";
                    fy.b();
                    if (fy.c() == null) {
                        fy.i();
                        frVar.a(fqVar);
                    } else {
                        ft.a(fy.c());
                        ja.a(3, "FlurryAppCloudRequestManager", "TRYING TO RELOGIN WITH LAST USER!");
                        ft.a(fy.f(), fy.g(), new gv(hashMap, gxVar, frVar));
                    }
                }
            } catch (Exception e) {
                ja.a(6, "FlurryAppCloudRequestManager", "checkForTokenError Exception: ", e);
            }
        }
        return;
    }
}
