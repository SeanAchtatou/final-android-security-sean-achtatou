package com.flurry.android.monolithic.sdk.impl;

public final class adb implements Comparable<adb> {
    private String a;
    private Class<?> b;
    private int c;

    public adb() {
        this.b = null;
        this.a = null;
        this.c = 0;
    }

    public adb(Class<?> cls) {
        this.b = cls;
        this.a = cls.getName();
        this.c = this.a.hashCode();
    }

    /* renamed from: a */
    public int compareTo(adb adb) {
        return this.a.compareTo(adb.a);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        return ((adb) obj).b == this.b;
    }

    public int hashCode() {
        return this.c;
    }

    public String toString() {
        return this.a;
    }
}
