package com.flurry.android.monolithic.sdk.impl;

import android.annotation.TargetApi;
import android.content.Context;
import android.view.ViewGroup;
import android.widget.AbsoluteLayout;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.flurry.android.impl.ads.FlurryAdModule;
import com.flurry.android.impl.ads.avro.protocol.v6.AdSpaceLayout;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;
import com.ideaworks3d.marmalade.s3eAndroidGooglePlayBilling.util.IabHelper;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class af extends cl {
    /* access modifiers changed from: private */
    public static final String a = af.class.getSimpleName();
    private static final Map<Class<? extends ViewGroup>, aj> b = d();
    private final ac c;

    public af(ac acVar, AdUnit adUnit) {
        super(adUnit);
        this.c = acVar;
    }

    @TargetApi(IabHelper.BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED)
    public void a(Context context, ViewGroup viewGroup) {
        ja.a(3, a, "render(" + context + ", " + viewGroup + ")");
        if (this.c == null || context == null || viewGroup == null) {
            ja.a(6, a, "failed to render banner ad for bannerView = " + this.c + " for context = " + context + " for viewGroup = " + viewGroup);
            return;
        }
        an a2 = FlurryAdModule.getInstance().a(context, viewGroup, c());
        if (a2 == null) {
            ja.a(6, a, "failed to render banner ad for holder = " + a2 + " for adSpaceName = " + c());
            return;
        }
        ViewGroup viewGroup2 = (ViewGroup) a2.getParent();
        if (viewGroup2 != null) {
            viewGroup2.removeView(a2);
        }
        a2.b();
        a2.removeAllViews();
        ViewGroup viewGroup3 = (ViewGroup) this.c.getParent();
        if (viewGroup3 != null) {
            viewGroup3.removeView(this.c);
        }
        a2.addView(this.c, new RelativeLayout.LayoutParams(-1, -1));
        this.c.initLayout();
        ViewGroup.LayoutParams a3 = a(viewGroup, a2);
        if (a3 != null) {
            a2.setLayoutParams(a3);
            ja.a(3, a, "banner ad holder layout params = " + a3.getClass().getName() + " {width = " + a3.width + ", height = " + a3.height + "} for banner ad with adSpaceName = " + c());
        }
        viewGroup.addView(a2, a(viewGroup));
    }

    private int a(ViewGroup viewGroup) {
        AdSpaceLayout e;
        int childCount = viewGroup.getChildCount();
        if (b() == null || b().d().size() < 1 || (e = b().d().get(0).e()) == null) {
            return childCount;
        }
        String[] split = e.f().toString().split("-");
        if (split.length != 2 || !"t".equals(split[0])) {
            return childCount;
        }
        return 0;
    }

    private ViewGroup.LayoutParams a(ViewGroup viewGroup, an anVar) {
        AdSpaceLayout e;
        ViewGroup.LayoutParams layoutParams = null;
        if (!(b() == null || b().d().size() < 1 || (e = b().d().get(0).e()) == null)) {
            aj b2 = b(viewGroup);
            if (b2 == null) {
                ja.a(5, a, "Ad space layout and alignment from the server is being ignored for ViewGroup subclass " + viewGroup.getClass().getSimpleName());
            } else {
                layoutParams = b2.a(e);
                if (layoutParams == null) {
                    ja.a(6, a, "Ad space layout and alignment from the server is being ignored for ViewGroup subclass " + viewGroup.getClass().getSimpleName());
                }
            }
        }
        return layoutParams;
    }

    private static Map<Class<? extends ViewGroup>, aj> d() {
        HashMap hashMap = new HashMap();
        hashMap.put(LinearLayout.class, new ak());
        hashMap.put(AbsoluteLayout.class, new ah());
        hashMap.put(FrameLayout.class, new ai());
        hashMap.put(RelativeLayout.class, new al());
        return Collections.unmodifiableMap(hashMap);
    }

    private static aj b(ViewGroup viewGroup) {
        return b.get(viewGroup.getClass());
    }
}
