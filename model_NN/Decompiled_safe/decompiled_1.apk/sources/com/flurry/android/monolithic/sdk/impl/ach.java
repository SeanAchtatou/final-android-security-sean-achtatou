package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.List;

@rz
public class ach extends aay<List<?>> {
    public /* synthetic */ void b(Object obj, or orVar, ru ruVar) throws IOException, oq {
        a((List<?>) ((List) obj), orVar, ruVar);
    }

    public ach(afm afm, boolean z, rx rxVar, qc qcVar, ra<Object> raVar) {
        super(List.class, afm, z, rxVar, qcVar, raVar);
    }

    public abc<?> a(rx rxVar) {
        return new ach(this.b, this.a, rxVar, this.e, this.d);
    }

    public void a(List<?> list, or orVar, ru ruVar) throws IOException, oq {
        aal aal;
        ra<Object> raVar;
        if (this.d != null) {
            a(list, orVar, ruVar, this.d);
        } else if (this.c != null) {
            b(list, orVar, ruVar);
        } else {
            int size = list.size();
            if (size != 0) {
                int i = 0;
                try {
                    int i2 = 0;
                    aal aal2 = this.f;
                    while (i2 < size) {
                        try {
                            Object obj = list.get(i2);
                            if (obj == null) {
                                ruVar.a(orVar);
                            } else {
                                Class<?> cls = obj.getClass();
                                ra<Object> a = aal2.a(cls);
                                if (a == null) {
                                    if (this.b.e()) {
                                        raVar = a(aal2, ruVar.a(this.b, cls), ruVar);
                                    } else {
                                        raVar = a(aal2, cls, ruVar);
                                    }
                                    aal = this.f;
                                } else {
                                    aal = aal2;
                                    raVar = a;
                                }
                                raVar.a(obj, orVar, ruVar);
                                aal2 = aal;
                            }
                            i2++;
                        } catch (Exception e) {
                            e = e;
                            i = i2;
                            a(ruVar, e, list, i);
                        }
                    }
                } catch (Exception e2) {
                    e = e2;
                    a(ruVar, e, list, i);
                }
            }
        }
    }

    public void a(List<?> list, or orVar, ru ruVar, ra<Object> raVar) throws IOException, oq {
        int size = list.size();
        if (size != 0) {
            rx rxVar = this.c;
            for (int i = 0; i < size; i++) {
                Object obj = list.get(i);
                if (obj == null) {
                    try {
                        ruVar.a(orVar);
                    } catch (Exception e) {
                        a(ruVar, e, list, i);
                    }
                } else if (rxVar == null) {
                    raVar.a(obj, orVar, ruVar);
                } else {
                    raVar.a(obj, orVar, ruVar, rxVar);
                }
            }
        }
    }

    public void b(List<?> list, or orVar, ru ruVar) throws IOException, oq {
        aal aal;
        ra<Object> raVar;
        int size = list.size();
        if (size != 0) {
            int i = 0;
            try {
                rx rxVar = this.c;
                int i2 = 0;
                aal aal2 = this.f;
                while (i2 < size) {
                    try {
                        Object obj = list.get(i2);
                        if (obj == null) {
                            ruVar.a(orVar);
                        } else {
                            Class<?> cls = obj.getClass();
                            ra<Object> a = aal2.a(cls);
                            if (a == null) {
                                if (this.b.e()) {
                                    raVar = a(aal2, ruVar.a(this.b, cls), ruVar);
                                } else {
                                    raVar = a(aal2, cls, ruVar);
                                }
                                aal = this.f;
                            } else {
                                aal = aal2;
                                raVar = a;
                            }
                            raVar.a(obj, orVar, ruVar, rxVar);
                            aal2 = aal;
                        }
                        i2++;
                    } catch (Exception e) {
                        e = e;
                        i = i2;
                        a(ruVar, e, list, i);
                    }
                }
            } catch (Exception e2) {
                e = e2;
                a(ruVar, e, list, i);
            }
        }
    }
}
