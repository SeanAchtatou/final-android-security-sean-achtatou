package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
public final class aby extends abt<byte[]> {
    public aby() {
        super(byte[].class);
    }

    public void a(byte[] bArr, or orVar, ru ruVar) throws IOException, oq {
        orVar.a(bArr);
    }

    public void a(byte[] bArr, or orVar, ru ruVar, rx rxVar) throws IOException, oq {
        rxVar.a(bArr, orVar);
        orVar.a(bArr);
        rxVar.d(bArr, orVar);
    }
}
