package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Looper;
import android.text.TextUtils;

public class in implements id {
    private static final String c = in.class.getSimpleName();
    private static in l;
    boolean a = false;
    boolean b;
    private final long d = 1800000;
    private final long e = 0;
    private LocationManager f;
    private Criteria g;
    /* access modifiers changed from: private */
    public Location h;
    private io i = new io(this);
    private String j;
    private int k = 0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, com.flurry.android.monolithic.sdk.impl.id):void
     arg types: [java.lang.String, com.flurry.android.monolithic.sdk.impl.in]
     candidates:
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.Object, java.lang.Object):boolean
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, com.flurry.android.monolithic.sdk.impl.id):void */
    private in() {
        ic a2 = ib.a();
        this.g = (Criteria) a2.a("LocationCriteria");
        a2.a("LocationCriteria", (id) this);
        ja.a(4, c, "initSettings, LocationCriteria = " + this.g);
        this.b = ((Boolean) a2.a("ReportLocation")).booleanValue();
        a2.a("ReportLocation", (id) this);
        ja.a(4, c, "initSettings, ReportLocation = " + this.b);
    }

    public static synchronized in a() {
        in inVar;
        synchronized (in.class) {
            if (l == null) {
                l = new in();
            }
            inVar = l;
        }
        return inVar;
    }

    public synchronized void b() {
        if (this.f == null) {
            this.f = (LocationManager) ia.a().b().getSystemService("location");
        }
    }

    public synchronized void c() {
        ja.a(4, c, "Location provider subscribed");
        this.k++;
        if (!this.a) {
            h();
        }
    }

    public synchronized void d() {
        ja.a(4, c, "Location provider unsubscribed");
        if (this.k <= 0) {
            ja.a(6, c, "Error! Unsubscribed too many times!");
            g();
        } else {
            this.k--;
            if (this.k == 0) {
                g();
            }
        }
    }

    private void g() {
        this.f.removeUpdates(this.i);
        this.a = false;
        ja.a(4, c, "LocationProvider stoped");
    }

    private void h() {
        if (this.b) {
            Context b2 = ia.a().b();
            if (b2.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0 || b2.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
                g();
                String i2 = i();
                a(i2);
                this.h = b(i2);
                this.a = true;
                ja.a(4, c, "LocationProvider started");
            }
        }
    }

    private String i() {
        String str;
        Criteria criteria = this.g;
        if (criteria == null) {
            criteria = new Criteria();
        }
        if (TextUtils.isEmpty(this.j)) {
            str = this.f.getBestProvider(criteria, true);
        } else {
            str = this.j;
        }
        ja.a(4, c, "provider = " + str);
        return str;
    }

    private void a(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.f.requestLocationUpdates(str, 1800000, 0.0f, this.i, Looper.getMainLooper());
        }
    }

    private Location b(String str) {
        if (!TextUtils.isEmpty(str)) {
            return this.f.getLastKnownLocation(str);
        }
        return null;
    }

    public Location e() {
        Location location = null;
        if (this.b) {
            Location b2 = b(i());
            if (b2 != null) {
                this.h = b2;
            }
            location = this.h;
        }
        ja.a(4, c, "getLocation() = " + location);
        return location;
    }

    public void f() {
        this.k = 0;
        g();
    }

    public void a(String str, Object obj) {
        if (str.equals("LocationCriteria")) {
            this.g = (Criteria) obj;
            ja.a(4, c, "onSettingUpdate, LocationCriteria = " + this.g);
            if (this.a) {
                h();
            }
        } else if (str.equals("ReportLocation")) {
            this.b = ((Boolean) obj).booleanValue();
            ja.a(4, c, "onSettingUpdate, ReportLocation = " + this.b);
            if (!this.b) {
                g();
            } else if (!this.a && this.k > 0) {
                h();
            }
        } else {
            ja.a(6, c, "LocationProvider internal error! Had to be LocationCriteria or ReportLocation key.");
        }
    }
}
