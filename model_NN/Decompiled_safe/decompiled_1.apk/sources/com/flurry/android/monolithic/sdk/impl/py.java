package com.flurry.android.monolithic.sdk.impl;

import java.lang.annotation.Annotation;
import java.util.List;

public abstract class py {
    public abstract Class<?> a(xg xgVar, afm afm, String str);

    public abstract String a(xn xnVar);

    public abstract String a(Enum<?> enumR);

    public abstract boolean a(xi xiVar);

    public abstract boolean a(xj xjVar);

    public abstract boolean a(xl xlVar);

    public abstract boolean a(Annotation annotation);

    public abstract Class<?> b(xg xgVar, afm afm, String str);

    public abstract Object b(xg xgVar);

    public abstract String b(xh xhVar);

    public abstract String b(xj xjVar);

    public abstract String b(xl xlVar);

    public abstract Class<?> c(xg xgVar, afm afm, String str);

    public abstract String c(xj xjVar);

    public abstract boolean c(xl xlVar);

    public abstract String[] c(xh xhVar);

    public abstract Boolean d(xh xhVar);

    public abstract String d(xl xlVar);

    public abstract Class<?> e(xg xgVar);

    public abstract sg f(xg xgVar);

    public abstract Class<?>[] g(xg xgVar);

    public abstract Object h(xg xgVar);

    public abstract String[] h(xh xhVar);

    public abstract Boolean i(xh xhVar);

    public abstract Class<? extends rc> i(xg xgVar);

    public abstract Class<? extends qu<?>> j(xg xgVar);

    public static py a() {
        return ya.a;
    }

    public Boolean a(xh xhVar) {
        return null;
    }

    public Boolean e(xh xhVar) {
        return null;
    }

    public Object f(xh xhVar) {
        return null;
    }

    public ye<?> a(xh xhVar, ye<?> yeVar) {
        return yeVar;
    }

    public yj<?> a(rf<?> rfVar, xh xhVar, afm afm) {
        return null;
    }

    public yj<?> a(rf<?> rfVar, xk xkVar, afm afm) {
        return null;
    }

    public yj<?> b(rf<?> rfVar, xk xkVar, afm afm) {
        return null;
    }

    public List<yg> a(xg xgVar) {
        return null;
    }

    public String g(xh xhVar) {
        return null;
    }

    public pz a(xk xkVar) {
        return null;
    }

    public Boolean b(xk xkVar) {
        return null;
    }

    public boolean c(xk xkVar) {
        if (xkVar instanceof xl) {
            return a((xl) xkVar);
        }
        if (xkVar instanceof xj) {
            return a((xj) xkVar);
        }
        if (xkVar instanceof xi) {
            return a((xi) xkVar);
        }
        return false;
    }

    public Object d(xk xkVar) {
        return null;
    }

    public Class<? extends ra<?>> c(xg xgVar) {
        return null;
    }

    public Class<? extends ra<?>> d(xg xgVar) {
        return null;
    }

    public sf a(xg xgVar, sf sfVar) {
        return sfVar;
    }

    public Class<?> a(xg xgVar, afm afm) {
        return null;
    }

    public Class<?> b(xg xgVar, afm afm) {
        return null;
    }

    public Object j(xh xhVar) {
        return null;
    }

    public boolean e(xl xlVar) {
        return false;
    }

    public boolean f(xl xlVar) {
        return false;
    }

    public boolean k(xg xgVar) {
        return false;
    }
}
