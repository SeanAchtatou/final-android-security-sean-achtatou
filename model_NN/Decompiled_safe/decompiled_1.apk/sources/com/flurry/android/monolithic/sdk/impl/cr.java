package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import com.flurry.android.impl.ads.FlurryAdModule;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;
import java.util.Map;

public abstract class cr extends cn {
    private static final String b = cr.class.getSimpleName();

    public cr(Context context, FlurryAdModule flurryAdModule, m mVar, AdUnit adUnit) {
        super(context, flurryAdModule, mVar, adUnit);
    }

    public void a(Map<String, String> map) {
        a("rendered", map);
    }

    public void b(Map<String, String> map) {
        a("clicked", map);
    }

    public void c(Map<String, String> map) {
        a("adClosed", map);
    }

    public void d(Map<String, String> map) {
        a("renderFailed", map);
    }

    private void a(String str, Map<String, String> map) {
        ja.a(3, b, "onEvent: " + str);
        if (e() != null) {
            c().a(new bh(str, map, b(), e(), d(), 0), c().a(), 0);
            return;
        }
        ja.a(3, b, "adUnit == null");
    }
}
