package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public final class aeo extends aet {
    protected ArrayList<ou> c;

    public aeo(aez aez) {
        super(aez);
    }

    public boolean a() {
        return true;
    }

    public int o() {
        if (this.c == null) {
            return 0;
        }
        return this.c.size();
    }

    public Iterator<ou> p() {
        return this.c == null ? aeu.a() : this.c.iterator();
    }

    public ou a(String str) {
        return null;
    }

    public final void a(or orVar, ru ruVar) throws IOException, oz {
        orVar.b();
        if (this.c != null) {
            Iterator<ou> it = this.c.iterator();
            while (it.hasNext()) {
                ((aep) it.next()).a(orVar, ruVar);
            }
        }
        orVar.c();
    }

    public void a(or orVar, ru ruVar, rx rxVar) throws IOException, oz {
        rxVar.c(this, orVar);
        if (this.c != null) {
            Iterator<ou> it = this.c.iterator();
            while (it.hasNext()) {
                ((aep) it.next()).a(orVar, ruVar);
            }
        }
        rxVar.f(this, orVar);
    }

    public void a(ou ouVar) {
        ou ouVar2;
        if (ouVar == null) {
            ouVar2 = r();
        } else {
            ouVar2 = ouVar;
        }
        b(ouVar2);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        aeo aeo = (aeo) obj;
        if (this.c == null || this.c.size() == 0) {
            return aeo.o() == 0;
        }
        return aeo.a(this.c);
    }

    public int hashCode() {
        if (this.c == null) {
            return 1;
        }
        int size = this.c.size();
        Iterator<ou> it = this.c.iterator();
        while (it.hasNext()) {
            ou next = it.next();
            if (next != null) {
                size ^= next.hashCode();
            }
        }
        return size;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder((o() << 4) + 16);
        sb.append('[');
        if (this.c != null) {
            int size = this.c.size();
            for (int i = 0; i < size; i++) {
                if (i > 0) {
                    sb.append(',');
                }
                sb.append(this.c.get(i).toString());
            }
        }
        sb.append(']');
        return sb.toString();
    }

    private void b(ou ouVar) {
        if (this.c == null) {
            this.c = new ArrayList<>();
        }
        this.c.add(ouVar);
    }

    private boolean a(ArrayList<ou> arrayList) {
        int size = arrayList.size();
        if (o() != size) {
            return false;
        }
        for (int i = 0; i < size; i++) {
            if (!this.c.get(i).equals(arrayList.get(i))) {
                return false;
            }
        }
        return true;
    }
}
