package com.flurry.android.monolithic.sdk.impl;

final class wf extends we {
    wf() {
        super(Boolean.class);
    }

    /* renamed from: c */
    public Boolean b(String str, qm qmVar) throws qw {
        if ("true".equals(str)) {
            return Boolean.TRUE;
        }
        if ("false".equals(str)) {
            return Boolean.FALSE;
        }
        throw qmVar.a(this.a, str, "value not 'true' or 'false'");
    }
}
