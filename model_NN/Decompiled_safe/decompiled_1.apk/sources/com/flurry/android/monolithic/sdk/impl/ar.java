package com.flurry.android.monolithic.sdk.impl;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;

@SuppressLint({"SetJavaScriptEnabled"})
public final class ar extends RelativeLayout implements View.OnClickListener {
    /* access modifiers changed from: private */
    public final String a = getClass().getSimpleName();
    /* access modifiers changed from: private */
    public WebView b;
    private WebViewClient c;
    private WebChromeClient d;
    /* access modifiers changed from: private */
    public boolean e;
    private ImageView f;
    private ImageView g;
    private ImageView h;
    private final int i = 0;
    private final int j = 1;
    private final int k = 2;
    /* access modifiers changed from: private */
    public az l;
    private av m;
    /* access modifiers changed from: private */
    public aw n;
    /* access modifiers changed from: private */
    public boolean o;

    public void setBasicWebViewUrlLoadingHandler(az azVar) {
        this.l = azVar;
    }

    public az getBasicWebViewUrlLoadingHandler() {
        return this.l;
    }

    public void setBasicWebViewClosingHandler(av avVar) {
        this.m = avVar;
    }

    public av getBasicWebViewClosingHandler() {
        return this.m;
    }

    public void setBasicWebViewFullScreenTransitionHandler(aw awVar) {
        this.n = awVar;
    }

    public aw getBasicWebViewFullScreenTransitionHandler() {
        return this.n;
    }

    @TargetApi(11)
    public ar(Context context, String str) {
        super(context);
        this.b = new WebView(context);
        this.c = new au(this);
        this.d = new at(this);
        this.b.getSettings().setJavaScriptEnabled(true);
        this.b.getSettings().setUseWideViewPort(true);
        this.b.getSettings().setLoadWithOverviewMode(true);
        this.b.getSettings().setBuiltInZoomControls(true);
        if (Build.VERSION.SDK_INT >= 11) {
            this.b.getSettings().setDisplayZoomControls(false);
        }
        this.b.setWebViewClient(this.c);
        this.b.setWebChromeClient(this.d);
        this.b.loadUrl(str);
        this.f = new ImageView(context);
        this.f.setId(0);
        this.f.setImageDrawable(getResources().getDrawable(17301560));
        this.f.setOnClickListener(this);
        this.g = new ImageView(context);
        this.g.setId(1);
        this.g.setImageDrawable(getResources().getDrawable(17301580));
        this.g.setOnClickListener(this);
        this.h = new ImageView(context);
        this.h.setId(2);
        this.h.setImageDrawable(getResources().getDrawable(17301565));
        this.h.setOnClickListener(this);
        setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        addView(this.b);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(14);
        addView(this.f, layoutParams);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(9);
        addView(this.g, layoutParams2);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.addRule(11);
        addView(this.h, layoutParams3);
    }

    public String getUrl() {
        if (this.b != null) {
            return this.b.getUrl();
        }
        return null;
    }

    public boolean a() {
        return this.e || (this.b != null && this.b.canGoBack());
    }

    public void b() {
        if (this.e) {
            this.d.onHideCustomView();
        } else if (this.b != null) {
            this.b.goBack();
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case 0:
                if (this.m != null) {
                    this.m.a(this, ay.WEB_RESULT_CLOSE);
                    return;
                }
                return;
            case 1:
                if (this.b != null && this.b.canGoBack()) {
                    this.b.goBack();
                    return;
                } else if (this.m != null) {
                    this.m.a(this, ay.WEB_RESULT_BACK);
                    return;
                } else {
                    return;
                }
            case 2:
                if (this.b != null && this.b.canGoForward()) {
                    this.b.goForward();
                    return;
                }
                return;
            default:
                return;
        }
    }

    @TargetApi(11)
    public void c() {
        if (this.b != null) {
            removeView(this.b);
            this.b.stopLoading();
            if (Build.VERSION.SDK_INT >= 11) {
                this.b.onPause();
            }
            this.b.destroy();
            this.b = null;
        }
    }
}
