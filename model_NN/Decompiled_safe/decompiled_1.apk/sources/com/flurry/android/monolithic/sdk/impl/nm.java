package com.flurry.android.monolithic.sdk.impl;

class nm {
    public final ji b;

    public nm(ji jiVar) {
        this.b = jiVar;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof nm)) {
            return false;
        }
        return this.b == ((nm) obj).b;
    }

    public int hashCode() {
        return this.b.hashCode();
    }
}
