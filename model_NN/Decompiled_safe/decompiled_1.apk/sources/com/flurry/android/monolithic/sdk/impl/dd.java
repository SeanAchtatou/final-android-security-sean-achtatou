package com.flurry.android.monolithic.sdk.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class dd implements co {
    private final List<co> a;

    public dd() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new de());
        this.a = Collections.unmodifiableList(arrayList);
    }

    /* JADX WARNING: Removed duplicated region for block: B:3:0x000d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.flurry.android.monolithic.sdk.impl.cn a_(android.content.Context r4, com.flurry.android.impl.ads.FlurryAdModule r5, com.flurry.android.monolithic.sdk.impl.m r6, com.flurry.android.impl.ads.avro.protocol.v6.AdUnit r7) {
        /*
            r3 = this;
            r0 = 0
            java.util.List<com.flurry.android.monolithic.sdk.impl.co> r1 = r3.a
            java.util.Iterator r1 = r1.iterator()
        L_0x0007:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x0019
            java.lang.Object r3 = r1.next()
            com.flurry.android.monolithic.sdk.impl.co r3 = (com.flurry.android.monolithic.sdk.impl.co) r3
            com.flurry.android.monolithic.sdk.impl.cn r0 = r3.a_(r4, r5, r6, r7)
            if (r0 == 0) goto L_0x0007
        L_0x0019:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.dd.a_(android.content.Context, com.flurry.android.impl.ads.FlurryAdModule, com.flurry.android.monolithic.sdk.impl.m, com.flurry.android.impl.ads.avro.protocol.v6.AdUnit):com.flurry.android.monolithic.sdk.impl.cn");
    }
}
