package com.flurry.android.monolithic.sdk.impl;

import java.lang.reflect.Array;
import java.util.List;

public final class aeh {
    private aei a;
    private aei b;
    private int c;
    private Object[] d;

    public Object[] a() {
        c();
        if (this.d == null) {
            return new Object[12];
        }
        return this.d;
    }

    public Object[] a(Object[] objArr) {
        int i;
        aei aei = new aei(objArr);
        if (this.a == null) {
            this.b = aei;
            this.a = aei;
        } else {
            this.b.a(aei);
            this.b = aei;
        }
        int length = objArr.length;
        this.c += length;
        if (length < 16384) {
            i = length + length;
        } else {
            i = length + (length >> 2);
        }
        return new Object[i];
    }

    public Object[] a(Object[] objArr, int i) {
        int i2 = this.c + i;
        Object[] objArr2 = new Object[i2];
        a(objArr2, i2, objArr, i);
        return objArr2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException}
     arg types: [java.lang.Class<T>, int]
     candidates:
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException}
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException} */
    public <T> T[] a(Object[] objArr, int i, Class<T> cls) {
        int i2 = i + this.c;
        T[] tArr = (Object[]) Array.newInstance((Class<?>) cls, i2);
        a(tArr, i2, objArr, i);
        c();
        return tArr;
    }

    public void a(Object[] objArr, int i, List<Object> list) {
        for (aei aei = this.a; aei != null; aei = aei.b()) {
            for (Object add : aei.a()) {
                list.add(add);
            }
        }
        for (int i2 = 0; i2 < i; i2++) {
            list.add(objArr[i2]);
        }
    }

    public int b() {
        if (this.d == null) {
            return 0;
        }
        return this.d.length;
    }

    /* access modifiers changed from: protected */
    public void c() {
        if (this.b != null) {
            this.d = this.b.a();
        }
        this.b = null;
        this.a = null;
        this.c = 0;
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj, int i, Object[] objArr, int i2) {
        int i3 = 0;
        for (aei aei = this.a; aei != null; aei = aei.b()) {
            Object[] a2 = aei.a();
            int length = a2.length;
            System.arraycopy(a2, 0, obj, i3, length);
            i3 += length;
        }
        System.arraycopy(objArr, 0, obj, i3, i2);
        int i4 = i3 + i2;
        if (i4 != i) {
            throw new IllegalStateException("Should have gotten " + i + " entries, got " + i4);
        }
    }
}
