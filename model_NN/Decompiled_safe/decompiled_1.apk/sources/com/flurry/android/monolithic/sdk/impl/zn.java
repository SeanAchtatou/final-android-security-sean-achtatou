package com.flurry.android.monolithic.sdk.impl;

final class zn extends zf {
    protected final zf p;
    protected final Class<?>[] q;

    protected zn(zf zfVar, Class<?>[] clsArr) {
        super(zfVar);
        this.p = zfVar;
        this.q = clsArr;
    }

    public zf a(ra<Object> raVar) {
        return new zn(this.p.a(raVar), this.q);
    }

    public void a(Object obj, or orVar, ru ruVar) throws Exception {
        Class<?> a = ruVar.a();
        if (a != null) {
            int i = 0;
            int length = this.q.length;
            while (i < length && !this.q[i].isAssignableFrom(a)) {
                i++;
            }
            if (i == length) {
                return;
            }
        }
        this.p.a(obj, orVar, ruVar);
    }
}
