package com.flurry.android.monolithic.sdk.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ne extends mq implements Iterable<mq> {
    private ne(mq[] mqVarArr) {
        super(na.SEQUENCE, mqVarArr);
    }

    public Iterator<mq> iterator() {
        return new nf(this);
    }

    /* renamed from: b */
    public ne a(Map<ne, ne> map, Map<ne, List<mx>> map2) {
        ne neVar = map.get(this);
        if (neVar != null) {
            return neVar;
        }
        ne neVar2 = new ne(new mq[a()]);
        map.put(this, neVar2);
        ArrayList<mx> arrayList = new ArrayList<>();
        map2.put(neVar2, arrayList);
        a(this.b, 0, neVar2.b, 0, map, map2);
        for (mx mxVar : arrayList) {
            System.arraycopy(neVar2.b, 0, mxVar.a, mxVar.b, neVar2.b.length);
        }
        map2.remove(neVar2);
        return neVar2;
    }

    public final int a() {
        return a(this.b, 0);
    }
}
