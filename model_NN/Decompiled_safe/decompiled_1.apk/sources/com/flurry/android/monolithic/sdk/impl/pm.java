package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.io.Reader;
import v2.com.playhaven.model.PHContent;

public final class pm extends ph {
    protected Reader I;
    protected char[] J;
    protected pc K;
    protected final afj L;
    protected boolean M = false;

    public pm(pq pqVar, int i, Reader reader, pc pcVar, afj afj) {
        super(pqVar, i);
        this.I = reader;
        this.J = pqVar.d();
        this.K = pcVar;
        this.L = afj;
    }

    public pc a() {
        return this.K;
    }

    /* access modifiers changed from: protected */
    public final boolean E() throws IOException {
        this.h += (long) this.g;
        this.j -= this.g;
        if (this.I != null) {
            int read = this.I.read(this.J, 0, this.J.length);
            if (read > 0) {
                this.f = 0;
                this.g = read;
                return true;
            }
            F();
            if (read == 0) {
                throw new IOException("Reader returned 0 characters when trying to read " + this.g);
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public char e(String str) throws IOException, ov {
        if (this.f >= this.g && !E()) {
            c(str);
        }
        char[] cArr = this.J;
        int i = this.f;
        this.f = i + 1;
        return cArr[i];
    }

    /* access modifiers changed from: protected */
    public void F() throws IOException {
        if (this.I != null) {
            if (this.d.b() || a(ox.AUTO_CLOSE_SOURCE)) {
                this.I.close();
            }
            this.I = null;
        }
    }

    /* access modifiers changed from: protected */
    public void G() throws IOException {
        super.G();
        char[] cArr = this.J;
        if (cArr != null) {
            this.J = null;
            this.d.a(cArr);
        }
    }

    public final String k() throws IOException, ov {
        pb pbVar = this.b;
        if (pbVar != pb.VALUE_STRING) {
            return a(pbVar);
        }
        if (this.M) {
            this.M = false;
            X();
        }
        return this.p.f();
    }

    /* access modifiers changed from: protected */
    public final String a(pb pbVar) {
        if (pbVar == null) {
            return null;
        }
        switch (pn.a[pbVar.ordinal()]) {
            case 1:
                return this.n.h();
            case 2:
            case 3:
            case 4:
                return this.p.f();
            default:
                return pbVar.a();
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public char[] l() throws IOException, ov {
        if (this.b == null) {
            return null;
        }
        switch (pn.a[this.b.ordinal()]) {
            case 1:
                if (!this.r) {
                    String h = this.n.h();
                    int length = h.length();
                    if (this.q == null) {
                        this.q = this.d.a(length);
                    } else if (this.q.length < length) {
                        this.q = new char[length];
                    }
                    h.getChars(0, length, this.q, 0);
                    this.r = true;
                }
                return this.q;
            case 2:
                if (this.M) {
                    this.M = false;
                    X();
                    break;
                }
                break;
            case 3:
            case 4:
                break;
            default:
                return this.b.b();
        }
        return this.p.e();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public int m() throws IOException, ov {
        if (this.b == null) {
            return 0;
        }
        switch (pn.a[this.b.ordinal()]) {
            case 1:
                return this.n.h().length();
            case 2:
                if (this.M) {
                    this.M = false;
                    X();
                    break;
                }
                break;
            case 3:
            case 4:
                break;
            default:
                return this.b.b().length;
        }
        return this.p.c();
    }

    public int n() throws IOException, ov {
        if (this.b != null) {
            switch (pn.a[this.b.ordinal()]) {
                case 1:
                    return 0;
                case 2:
                    if (this.M) {
                        this.M = false;
                        X();
                    }
                    return this.p.d();
                case 3:
                case 4:
                    return this.p.d();
            }
        }
        return 0;
    }

    public byte[] a(on onVar) throws IOException, ov {
        if (this.b != pb.VALUE_STRING && (this.b != pb.VALUE_EMBEDDED_OBJECT || this.t == null)) {
            d("Current token (" + this.b + ") not VALUE_STRING or VALUE_EMBEDDED_OBJECT, can not access as binary");
        }
        if (this.M) {
            try {
                this.t = b(onVar);
                this.M = false;
            } catch (IllegalArgumentException e) {
                throw a("Failed to decode VALUE_STRING as base64 (" + onVar + "): " + e.getMessage());
            }
        } else if (this.t == null) {
            afq I2 = I();
            a(k(), I2, onVar);
            this.t = I2.b();
        }
        return this.t;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.ph.a(int, char):void
     arg types: [int, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.pm.a(int, boolean):com.flurry.android.monolithic.sdk.impl.pb
      com.flurry.android.monolithic.sdk.impl.pm.a(java.lang.String, int):void
      com.flurry.android.monolithic.sdk.impl.pm.a(java.lang.String, java.lang.String):void
      com.flurry.android.monolithic.sdk.impl.ph.a(java.lang.String, double):com.flurry.android.monolithic.sdk.impl.pb
      com.flurry.android.monolithic.sdk.impl.ph.a(boolean, int):com.flurry.android.monolithic.sdk.impl.pb
      com.flurry.android.monolithic.sdk.impl.ph.a(int, java.lang.String):void
      com.flurry.android.monolithic.sdk.impl.pi.a(java.lang.String, java.lang.Throwable):void
      com.flurry.android.monolithic.sdk.impl.ph.a(int, char):void */
    public pb b() throws IOException, ov {
        pb d;
        this.y = 0;
        if (this.b == pb.FIELD_NAME) {
            return ac();
        }
        if (this.M) {
            Z();
        }
        int af = af();
        if (af < 0) {
            close();
            this.b = null;
            return null;
        }
        this.k = (this.h + ((long) this.f)) - 1;
        this.l = this.i;
        this.m = (this.f - this.j) - 1;
        this.t = null;
        if (af == 93) {
            if (!this.n.a()) {
                a(af, '}');
            }
            this.n = this.n.i();
            pb pbVar = pb.END_ARRAY;
            this.b = pbVar;
            return pbVar;
        } else if (af == 125) {
            if (!this.n.c()) {
                a(af, ']');
            }
            this.n = this.n.i();
            pb pbVar2 = pb.END_OBJECT;
            this.b = pbVar2;
            return pbVar2;
        } else {
            if (this.n.j()) {
                if (af != 44) {
                    b(af, "was expecting comma to separate " + this.n.d() + " entries");
                }
                af = ae();
            }
            boolean c = this.n.c();
            if (c) {
                this.n.a(e(af));
                this.b = pb.FIELD_NAME;
                int ae = ae();
                if (ae != 58) {
                    b(ae, "was expecting a colon to separate field name and value");
                }
                af = ae();
            }
            switch (af) {
                case 34:
                    this.M = true;
                    d = pb.VALUE_STRING;
                    break;
                case 45:
                case 48:
                case 49:
                case 50:
                case 51:
                case 52:
                case 53:
                case 54:
                case 55:
                case 56:
                case 57:
                    d = d(af);
                    break;
                case 91:
                    if (!c) {
                        this.n = this.n.b(this.l, this.m);
                    }
                    d = pb.START_ARRAY;
                    break;
                case 93:
                case 125:
                    b(af, "expected a value");
                    a("true", 1);
                    d = pb.VALUE_TRUE;
                    break;
                case 102:
                    a("false", 1);
                    d = pb.VALUE_FALSE;
                    break;
                case 110:
                    a(PHContent.PARCEL_NULL, 1);
                    d = pb.VALUE_NULL;
                    break;
                case 116:
                    a("true", 1);
                    d = pb.VALUE_TRUE;
                    break;
                case 123:
                    if (!c) {
                        this.n = this.n.c(this.l, this.m);
                    }
                    d = pb.START_OBJECT;
                    break;
                default:
                    d = g(af);
                    break;
            }
            if (c) {
                this.o = d;
                return this.b;
            }
            this.b = d;
            return d;
        }
    }

    private final pb ac() {
        this.r = false;
        pb pbVar = this.o;
        this.o = null;
        if (pbVar == pb.START_ARRAY) {
            this.n = this.n.b(this.l, this.m);
        } else if (pbVar == pb.START_OBJECT) {
            this.n = this.n.c(this.l, this.m);
        }
        this.b = pbVar;
        return pbVar;
    }

    public void close() throws IOException {
        super.close();
        this.L.b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.pm.a(int, boolean):com.flurry.android.monolithic.sdk.impl.pb
     arg types: [char, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.pm.a(java.lang.String, int):void
      com.flurry.android.monolithic.sdk.impl.pm.a(java.lang.String, java.lang.String):void
      com.flurry.android.monolithic.sdk.impl.ph.a(java.lang.String, double):com.flurry.android.monolithic.sdk.impl.pb
      com.flurry.android.monolithic.sdk.impl.ph.a(boolean, int):com.flurry.android.monolithic.sdk.impl.pb
      com.flurry.android.monolithic.sdk.impl.ph.a(int, char):void
      com.flurry.android.monolithic.sdk.impl.ph.a(int, java.lang.String):void
      com.flurry.android.monolithic.sdk.impl.pi.a(java.lang.String, java.lang.Throwable):void
      com.flurry.android.monolithic.sdk.impl.pm.a(int, boolean):com.flurry.android.monolithic.sdk.impl.pb */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x004b, code lost:
        if (r4 != '.') goto L_0x00c1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x004d, code lost:
        r4 = 0;
        r5 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x004f, code lost:
        if (r5 >= r3) goto L_0x0017;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0051, code lost:
        r7 = r5 + 1;
        r5 = r12.J[r5];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0057, code lost:
        if (r5 < '0') goto L_0x005b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0059, code lost:
        if (r5 <= '9') goto L_0x0099;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x005b, code lost:
        if (r4 != 0) goto L_0x0062;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x005d, code lost:
        a(r5, "Decimal point not followed by a digit");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0062, code lost:
        r6 = r5;
        r5 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0066, code lost:
        if (r6 == 'e') goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x006a, code lost:
        if (r6 != 'E') goto L_0x00bf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x006c, code lost:
        if (r5 >= r3) goto L_0x0017;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x006e, code lost:
        r7 = r5 + 1;
        r5 = r12.J[r5];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0074, code lost:
        if (r5 == '-') goto L_0x007a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0078, code lost:
        if (r5 != '+') goto L_0x00bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x007a, code lost:
        if (r7 >= r3) goto L_0x0017;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x007c, code lost:
        r6 = r7 + 1;
        r7 = r12.J[r7];
        r5 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0084, code lost:
        if (r7 > '9') goto L_0x009d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0086, code lost:
        if (r7 < '0') goto L_0x009d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0088, code lost:
        r5 = r5 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x008a, code lost:
        if (r6 >= r3) goto L_0x0017;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x008c, code lost:
        r7 = r12.J[r6];
        r6 = r6 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0099, code lost:
        r4 = r4 + 1;
        r5 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x009d, code lost:
        if (r5 != 0) goto L_0x00a4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x009f, code lost:
        a(r7, "Exponent indicator not followed by a digit");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00a4, code lost:
        r3 = r5;
        r5 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00a6, code lost:
        r5 = r5 - 1;
        r12.f = r5;
        r12.p.a(r12.J, r2, r5 - r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00bb, code lost:
        r6 = r7;
        r7 = r5;
        r5 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00bf, code lost:
        r3 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00c1, code lost:
        r5 = r6;
        r6 = r4;
        r4 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:?, code lost:
        return a(r0, r1, r4, r3);
     */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0039  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.flurry.android.monolithic.sdk.impl.pb d(int r13) throws java.io.IOException, com.flurry.android.monolithic.sdk.impl.ov {
        /*
            r12 = this;
            r11 = 45
            r8 = 0
            r10 = 57
            r6 = 1
            r9 = 48
            if (r13 != r11) goto L_0x0022
            r0 = r6
        L_0x000b:
            int r1 = r12.f
            int r2 = r1 - r6
            int r3 = r12.g
            if (r0 == 0) goto L_0x00c5
            int r4 = r12.g
            if (r1 < r4) goto L_0x0024
        L_0x0017:
            if (r0 == 0) goto L_0x00b8
            int r1 = r2 + 1
        L_0x001b:
            r12.f = r1
            com.flurry.android.monolithic.sdk.impl.pb r0 = r12.a(r0)
        L_0x0021:
            return r0
        L_0x0022:
            r0 = r8
            goto L_0x000b
        L_0x0024:
            char[] r4 = r12.J
            int r5 = r1 + 1
            char r1 = r4[r1]
            if (r1 > r10) goto L_0x002e
            if (r1 >= r9) goto L_0x0035
        L_0x002e:
            r12.f = r5
            com.flurry.android.monolithic.sdk.impl.pb r0 = r12.a(r1, r6)
            goto L_0x0021
        L_0x0035:
            r4 = r1
            r1 = r5
        L_0x0037:
            if (r4 == r9) goto L_0x0017
            r4 = r1
            r1 = r6
        L_0x003b:
            int r5 = r12.g
            if (r4 >= r5) goto L_0x0017
            char[] r5 = r12.J
            int r6 = r4 + 1
            char r4 = r5[r4]
            if (r4 < r9) goto L_0x0049
            if (r4 <= r10) goto L_0x0095
        L_0x0049:
            r5 = 46
            if (r4 != r5) goto L_0x00c1
            r4 = r8
            r5 = r6
        L_0x004f:
            if (r5 >= r3) goto L_0x0017
            char[] r6 = r12.J
            int r7 = r5 + 1
            char r5 = r6[r5]
            if (r5 < r9) goto L_0x005b
            if (r5 <= r10) goto L_0x0099
        L_0x005b:
            if (r4 != 0) goto L_0x0062
            java.lang.String r6 = "Decimal point not followed by a digit"
            r12.a(r5, r6)
        L_0x0062:
            r6 = r5
            r5 = r7
        L_0x0064:
            r7 = 101(0x65, float:1.42E-43)
            if (r6 == r7) goto L_0x006c
            r7 = 69
            if (r6 != r7) goto L_0x00bf
        L_0x006c:
            if (r5 >= r3) goto L_0x0017
            char[] r6 = r12.J
            int r7 = r5 + 1
            char r5 = r6[r5]
            if (r5 == r11) goto L_0x007a
            r6 = 43
            if (r5 != r6) goto L_0x00bb
        L_0x007a:
            if (r7 >= r3) goto L_0x0017
            char[] r5 = r12.J
            int r6 = r7 + 1
            char r5 = r5[r7]
            r7 = r5
            r5 = r8
        L_0x0084:
            if (r7 > r10) goto L_0x009d
            if (r7 < r9) goto L_0x009d
            int r5 = r5 + 1
            if (r6 >= r3) goto L_0x0017
            char[] r7 = r12.J
            int r8 = r6 + 1
            char r6 = r7[r6]
            r7 = r6
            r6 = r8
            goto L_0x0084
        L_0x0095:
            int r1 = r1 + 1
            r4 = r6
            goto L_0x003b
        L_0x0099:
            int r4 = r4 + 1
            r5 = r7
            goto L_0x004f
        L_0x009d:
            if (r5 != 0) goto L_0x00a4
            java.lang.String r3 = "Exponent indicator not followed by a digit"
            r12.a(r7, r3)
        L_0x00a4:
            r3 = r5
            r5 = r6
        L_0x00a6:
            int r5 = r5 + -1
            r12.f = r5
            int r5 = r5 - r2
            com.flurry.android.monolithic.sdk.impl.afy r6 = r12.p
            char[] r7 = r12.J
            r6.a(r7, r2, r5)
            com.flurry.android.monolithic.sdk.impl.pb r0 = r12.a(r0, r1, r4, r3)
            goto L_0x0021
        L_0x00b8:
            r1 = r2
            goto L_0x001b
        L_0x00bb:
            r6 = r7
            r7 = r5
            r5 = r8
            goto L_0x0084
        L_0x00bf:
            r3 = r8
            goto L_0x00a6
        L_0x00c1:
            r5 = r6
            r6 = r4
            r4 = r8
            goto L_0x0064
        L_0x00c5:
            r4 = r13
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.pm.d(int):com.flurry.android.monolithic.sdk.impl.pb");
    }

    private final pb a(boolean z) throws IOException, ov {
        int i;
        char e;
        char[] cArr;
        int i2;
        int i3;
        char c;
        boolean z2;
        char[] cArr2;
        int i4;
        char c2;
        boolean z3;
        int i5;
        boolean z4;
        int i6;
        char e2;
        char[] cArr3;
        int i7;
        char c3;
        int i8;
        int i9;
        char e3;
        char[] k = this.p.k();
        if (z) {
            i = 0 + 1;
            k[0] = '-';
        } else {
            i = 0;
        }
        if (this.f < this.g) {
            char[] cArr4 = this.J;
            int i10 = this.f;
            this.f = i10 + 1;
            e = cArr4[i10];
        } else {
            e = e("No digit following minus sign");
        }
        if (e == '0') {
            e = ad();
        }
        char[] cArr5 = k;
        char c4 = e;
        int i11 = i;
        int i12 = 0;
        while (true) {
            if (c4 >= '0' && c4 <= '9') {
                i12++;
                if (i11 >= cArr5.length) {
                    cArr5 = this.p.m();
                    i11 = 0;
                }
                int i13 = i11 + 1;
                cArr5[i11] = c4;
                if (this.f >= this.g && !E()) {
                    z2 = true;
                    i3 = i12;
                    c = 0;
                    int i14 = i13;
                    cArr = cArr5;
                    i2 = i14;
                    break;
                }
                char[] cArr6 = this.J;
                int i15 = this.f;
                this.f = i15 + 1;
                c4 = cArr6[i15];
                i11 = i13;
            } else {
                cArr = cArr5;
                i2 = i11;
                i3 = i12;
                c = c4;
                z2 = false;
            }
        }
        cArr = cArr5;
        i2 = i11;
        i3 = i12;
        c = c4;
        z2 = false;
        if (i3 == 0) {
            b("Missing integer part (next char " + c(c) + ")");
        }
        if (c == '.') {
            int i16 = i2 + 1;
            cArr[i2] = c;
            c2 = c;
            int i17 = 0;
            int i18 = i16;
            cArr2 = cArr;
            i4 = i18;
            while (true) {
                if (this.f >= this.g && !E()) {
                    z2 = true;
                    break;
                }
                char[] cArr7 = this.J;
                int i19 = this.f;
                this.f = i19 + 1;
                c2 = cArr7[i19];
                if (c2 < '0' || c2 > '9') {
                    break;
                }
                i17++;
                if (i4 >= cArr2.length) {
                    cArr2 = this.p.m();
                    i4 = 0;
                }
                cArr2[i4] = c2;
                i4++;
            }
            if (i17 == 0) {
                a(c2, "Decimal point not followed by a digit");
            }
            int i20 = i17;
            z3 = z2;
            i5 = i20;
        } else {
            cArr2 = cArr;
            i4 = i2;
            c2 = c;
            z3 = z2;
            i5 = 0;
        }
        if (c2 == 'e' || c2 == 'E') {
            if (i4 >= cArr2.length) {
                cArr2 = this.p.m();
                i4 = 0;
            }
            int i21 = i4 + 1;
            cArr2[i4] = c2;
            if (this.f < this.g) {
                char[] cArr8 = this.J;
                int i22 = this.f;
                this.f = i22 + 1;
                e2 = cArr8[i22];
            } else {
                e2 = e("expected a digit for number exponent");
            }
            if (e2 == '-' || e2 == '+') {
                if (i21 >= cArr2.length) {
                    cArr2 = this.p.m();
                    i9 = 0;
                } else {
                    i9 = i21;
                }
                int i23 = i9 + 1;
                cArr2[i9] = e2;
                if (this.f < this.g) {
                    char[] cArr9 = this.J;
                    int i24 = this.f;
                    this.f = i24 + 1;
                    e3 = cArr9[i24];
                } else {
                    e3 = e("expected a digit for number exponent");
                }
                c3 = e3;
                i8 = 0;
                int i25 = i23;
                cArr3 = cArr2;
                i7 = i25;
            } else {
                c3 = e2;
                i8 = 0;
                int i26 = i21;
                cArr3 = cArr2;
                i7 = i26;
            }
            while (true) {
                if (c3 <= '9' && c3 >= '0') {
                    i8++;
                    if (i7 >= cArr3.length) {
                        cArr3 = this.p.m();
                        i7 = 0;
                    }
                    int i27 = i7 + 1;
                    cArr3[i7] = c3;
                    if (this.f >= this.g && !E()) {
                        i6 = i8;
                        i7 = i27;
                        z4 = true;
                        break;
                    }
                    char[] cArr10 = this.J;
                    int i28 = this.f;
                    this.f = i28 + 1;
                    c3 = cArr10[i28];
                    i7 = i27;
                } else {
                    int i29 = i8;
                    z4 = z3;
                    i6 = i29;
                }
            }
            int i292 = i8;
            z4 = z3;
            i6 = i292;
            if (i6 == 0) {
                a(c3, "Exponent indicator not followed by a digit");
            }
            i4 = i7;
        } else {
            z4 = z3;
            i6 = 0;
        }
        if (!z4) {
            this.f--;
        }
        this.p.a(i4);
        return a(z, i3, i5, i6);
    }

    private final char ad() throws IOException, ov {
        if (this.f >= this.g && !E()) {
            return '0';
        }
        char c = this.J[this.f];
        if (c < '0' || c > '9') {
            return '0';
        }
        if (!a(ox.ALLOW_NUMERIC_LEADING_ZEROS)) {
            b("Leading zeroes not allowed");
        }
        this.f++;
        if (c != '0') {
            return c;
        }
        do {
            if (this.f >= this.g && !E()) {
                return c;
            }
            c = this.J[this.f];
            if (c < '0' || c > '9') {
                return '0';
            }
            this.f++;
        } while (c == '0');
        return c;
    }

    /* JADX INFO: additional move instructions added (3) to help type inference */
    /* access modifiers changed from: protected */
    public pb a(int i, boolean z) throws IOException, ov {
        char c;
        double d;
        double d2;
        if (i == 73) {
            if (this.f >= this.g && !E()) {
                T();
            }
            char[] cArr = this.J;
            int i2 = this.f;
            this.f = i2 + 1;
            char c2 = cArr[i2];
            if (c2 == 'N') {
                String str = z ? "-INF" : "+INF";
                a(str, 3);
                if (a(ox.ALLOW_NON_NUMERIC_NUMBERS)) {
                    if (z) {
                        d2 = Double.NEGATIVE_INFINITY;
                    } else {
                        d2 = Double.POSITIVE_INFINITY;
                    }
                    return a(str, d2);
                }
                d("Non-standard token '" + str + "': enable JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS to allow");
                c = c2;
            } else {
                c = c2;
                if (c2 == 'n') {
                    String str2 = z ? "-Infinity" : "+Infinity";
                    a(str2, 3);
                    if (a(ox.ALLOW_NON_NUMERIC_NUMBERS)) {
                        if (z) {
                            d = Double.NEGATIVE_INFINITY;
                        } else {
                            d = Double.POSITIVE_INFINITY;
                        }
                        return a(str2, d);
                    }
                    d("Non-standard token '" + str2 + "': enable JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS to allow");
                    c = c2;
                }
            }
        } else {
            c = i;
        }
        a(c, "expected digit (0-9) to follow minus sign, for valid numeric value");
        return null;
    }

    /* access modifiers changed from: protected */
    public final String e(int i) throws IOException, ov {
        int i2;
        int i3;
        if (i != 34) {
            return f(i);
        }
        int i4 = this.f;
        int i5 = this.g;
        if (i4 < i5) {
            int[] a = afr.a();
            int length = a.length;
            i2 = i4;
            i3 = 0;
            while (true) {
                char c = this.J[i2];
                if (c >= length || a[c] == 0) {
                    i3 = (i3 * 31) + c;
                    i2++;
                    if (i2 >= i5) {
                        break;
                    }
                } else if (c == '\"') {
                    int i6 = this.f;
                    this.f = i2 + 1;
                    return this.L.a(this.J, i6, i2 - i6, i3);
                }
            }
        } else {
            i2 = i4;
            i3 = 0;
        }
        int i7 = this.f;
        this.f = i2;
        return a(i7, i3, 34);
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0095  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String a(int r9, int r10, int r11) throws java.io.IOException, com.flurry.android.monolithic.sdk.impl.ov {
        /*
            r8 = this;
            r6 = 92
            com.flurry.android.monolithic.sdk.impl.afy r0 = r8.p
            char[] r1 = r8.J
            int r2 = r8.f
            int r2 = r2 - r9
            r0.a(r1, r9, r2)
            com.flurry.android.monolithic.sdk.impl.afy r0 = r8.p
            char[] r0 = r0.j()
            com.flurry.android.monolithic.sdk.impl.afy r1 = r8.p
            int r1 = r1.l()
            r2 = r10
            r7 = r0
            r0 = r1
            r1 = r7
        L_0x001c:
            int r3 = r8.f
            int r4 = r8.g
            if (r3 < r4) goto L_0x0045
            boolean r3 = r8.E()
            if (r3 != 0) goto L_0x0045
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = ": was expecting closing '"
            java.lang.StringBuilder r3 = r3.append(r4)
            char r4 = (char) r11
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = "' for name"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r8.c(r3)
        L_0x0045:
            char[] r3 = r8.J
            int r4 = r8.f
            int r5 = r4 + 1
            r8.f = r5
            char r3 = r3[r4]
            if (r3 > r6) goto L_0x0093
            if (r3 != r6) goto L_0x006c
            char r4 = r8.Q()
        L_0x0057:
            int r2 = r2 * 31
            int r2 = r2 + r3
            int r3 = r0 + 1
            r1[r0] = r4
            int r0 = r1.length
            if (r3 < r0) goto L_0x0095
            com.flurry.android.monolithic.sdk.impl.afy r0 = r8.p
            char[] r0 = r0.m()
            r1 = 0
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x001c
        L_0x006c:
            if (r3 > r11) goto L_0x0093
            if (r3 != r11) goto L_0x008a
            com.flurry.android.monolithic.sdk.impl.afy r1 = r8.p
            r1.a(r0)
            com.flurry.android.monolithic.sdk.impl.afy r0 = r8.p
            char[] r1 = r0.e()
            int r3 = r0.d()
            int r0 = r0.c()
            com.flurry.android.monolithic.sdk.impl.afj r4 = r8.L
            java.lang.String r0 = r4.a(r1, r3, r0, r2)
            return r0
        L_0x008a:
            r4 = 32
            if (r3 >= r4) goto L_0x0093
            java.lang.String r4 = "name"
            r8.c(r3, r4)
        L_0x0093:
            r4 = r3
            goto L_0x0057
        L_0x0095:
            r0 = r3
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.pm.a(int, int, int):java.lang.String");
    }

    /* access modifiers changed from: protected */
    public final String f(int i) throws IOException, ov {
        boolean isJavaIdentifierPart;
        int i2;
        if (i == 39 && a(ox.ALLOW_SINGLE_QUOTES)) {
            return V();
        }
        if (!a(ox.ALLOW_UNQUOTED_FIELD_NAMES)) {
            b(i, "was expecting double-quote to start field name");
        }
        int[] b = afr.b();
        int length = b.length;
        if (i < length) {
            isJavaIdentifierPart = b[i] == 0 && (i < 48 || i > 57);
        } else {
            isJavaIdentifierPart = Character.isJavaIdentifierPart((char) i);
        }
        if (!isJavaIdentifierPart) {
            b(i, "was expecting either valid name character (for unquoted name) or double-quote (for quoted) to start field name");
        }
        int i3 = this.f;
        int i4 = this.g;
        if (i3 < i4) {
            int i5 = i3;
            int i6 = 0;
            do {
                char c = this.J[i5];
                if (c < length) {
                    if (b[c] != 0) {
                        int i7 = this.f - 1;
                        this.f = i5;
                        return this.L.a(this.J, i7, i5 - i7, i6);
                    }
                } else if (!Character.isJavaIdentifierPart((char) c)) {
                    int i8 = this.f - 1;
                    this.f = i5;
                    return this.L.a(this.J, i8, i5 - i8, i6);
                }
                i6 = (i6 * 31) + c;
                i5++;
            } while (i5 < i4);
            i2 = i6;
            i3 = i5;
        } else {
            i2 = 0;
        }
        this.f = i3;
        return a(this.f - 1, i2, b);
    }

    /* access modifiers changed from: protected */
    public final String V() throws IOException, ov {
        int i;
        int i2;
        int i3 = this.f;
        int i4 = this.g;
        if (i3 < i4) {
            int[] a = afr.a();
            int length = a.length;
            i = i3;
            i2 = 0;
            do {
                char c = this.J[i];
                if (c != '\'') {
                    if (c < length && a[c] != 0) {
                        break;
                    }
                    i2 = (i2 * 31) + c;
                    i++;
                } else {
                    int i5 = this.f;
                    this.f = i + 1;
                    return this.L.a(this.J, i5, i - i5, i2);
                }
            } while (i < i4);
        } else {
            i = i3;
            i2 = 0;
        }
        int i6 = this.f;
        this.f = i;
        return a(i6, i2, 39);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.ph.a(java.lang.String, double):com.flurry.android.monolithic.sdk.impl.pb
     arg types: [java.lang.String, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.pm.a(int, boolean):com.flurry.android.monolithic.sdk.impl.pb
      com.flurry.android.monolithic.sdk.impl.pm.a(java.lang.String, int):void
      com.flurry.android.monolithic.sdk.impl.pm.a(java.lang.String, java.lang.String):void
      com.flurry.android.monolithic.sdk.impl.ph.a(boolean, int):com.flurry.android.monolithic.sdk.impl.pb
      com.flurry.android.monolithic.sdk.impl.ph.a(int, char):void
      com.flurry.android.monolithic.sdk.impl.ph.a(int, java.lang.String):void
      com.flurry.android.monolithic.sdk.impl.pi.a(java.lang.String, java.lang.Throwable):void
      com.flurry.android.monolithic.sdk.impl.ph.a(java.lang.String, double):com.flurry.android.monolithic.sdk.impl.pb */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.pm.a(int, boolean):com.flurry.android.monolithic.sdk.impl.pb
     arg types: [char, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.pm.a(java.lang.String, int):void
      com.flurry.android.monolithic.sdk.impl.pm.a(java.lang.String, java.lang.String):void
      com.flurry.android.monolithic.sdk.impl.ph.a(java.lang.String, double):com.flurry.android.monolithic.sdk.impl.pb
      com.flurry.android.monolithic.sdk.impl.ph.a(boolean, int):com.flurry.android.monolithic.sdk.impl.pb
      com.flurry.android.monolithic.sdk.impl.ph.a(int, char):void
      com.flurry.android.monolithic.sdk.impl.ph.a(int, java.lang.String):void
      com.flurry.android.monolithic.sdk.impl.pi.a(java.lang.String, java.lang.Throwable):void
      com.flurry.android.monolithic.sdk.impl.pm.a(int, boolean):com.flurry.android.monolithic.sdk.impl.pb */
    /* access modifiers changed from: protected */
    public final pb g(int i) throws IOException, ov {
        switch (i) {
            case 39:
                if (a(ox.ALLOW_SINGLE_QUOTES)) {
                    return W();
                }
                break;
            case 43:
                if (this.f >= this.g && !E()) {
                    T();
                }
                char[] cArr = this.J;
                int i2 = this.f;
                this.f = i2 + 1;
                return a((int) cArr[i2], false);
            case 78:
                a("NaN", 1);
                if (!a(ox.ALLOW_NON_NUMERIC_NUMBERS)) {
                    d("Non-standard token 'NaN': enable JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS to allow");
                    break;
                } else {
                    return a("NaN", Double.NaN);
                }
        }
        b(i, "expected a valid value (number, String, array, object, 'true', 'false' or 'null')");
        return null;
    }

    /* access modifiers changed from: protected */
    public final pb W() throws IOException, ov {
        char[] k = this.p.k();
        int l = this.p.l();
        while (true) {
            if (this.f >= this.g && !E()) {
                c(": was expecting closing quote for a string value");
            }
            char[] cArr = this.J;
            int i = this.f;
            this.f = i + 1;
            char c = cArr[i];
            if (c <= '\\') {
                if (c == '\\') {
                    c = Q();
                } else if (c <= '\'') {
                    if (c == '\'') {
                        this.p.a(l);
                        return pb.VALUE_STRING;
                    } else if (c < ' ') {
                        c(c, "string value");
                    }
                }
            }
            if (l >= k.length) {
                k = this.p.m();
                l = 0;
            }
            k[l] = c;
            l++;
        }
    }

    private String a(int i, int i2, int[] iArr) throws IOException, ov {
        this.p.a(this.J, i, this.f - i);
        char[] j = this.p.j();
        int l = this.p.l();
        int length = iArr.length;
        int i3 = i2;
        char[] cArr = j;
        int i4 = l;
        char[] cArr2 = cArr;
        while (true) {
            if (this.f >= this.g && !E()) {
                break;
            }
            char c = this.J[this.f];
            if (c > length) {
                if (!Character.isJavaIdentifierPart(c)) {
                    break;
                }
            } else if (iArr[c] != 0) {
                break;
            }
            this.f++;
            i3 = (i3 * 31) + c;
            int i5 = i4 + 1;
            cArr2[i4] = c;
            if (i5 >= cArr2.length) {
                cArr2 = this.p.m();
                i4 = 0;
            } else {
                i4 = i5;
            }
        }
        this.p.a(i4);
        afy afy = this.p;
        return this.L.a(afy.e(), afy.d(), afy.c(), i3);
    }

    /* access modifiers changed from: protected */
    public void X() throws IOException, ov {
        int i = this.f;
        int i2 = this.g;
        if (i < i2) {
            int[] a = afr.a();
            int length = a.length;
            while (true) {
                char c = this.J[i];
                if (c >= length || a[c] == 0) {
                    i++;
                    if (i >= i2) {
                        break;
                    }
                } else if (c == '\"') {
                    this.p.a(this.J, this.f, i - this.f);
                    this.f = i + 1;
                    return;
                }
            }
        }
        this.p.b(this.J, this.f, i - this.f);
        this.f = i;
        Y();
    }

    /* access modifiers changed from: protected */
    public void Y() throws IOException, ov {
        char[] j = this.p.j();
        int l = this.p.l();
        while (true) {
            if (this.f >= this.g && !E()) {
                c(": was expecting closing quote for a string value");
            }
            char[] cArr = this.J;
            int i = this.f;
            this.f = i + 1;
            char c = cArr[i];
            if (c <= '\\') {
                if (c == '\\') {
                    c = Q();
                } else if (c <= '\"') {
                    if (c == '\"') {
                        this.p.a(l);
                        return;
                    } else if (c < ' ') {
                        c(c, "string value");
                    }
                }
            }
            if (l >= j.length) {
                j = this.p.m();
                l = 0;
            }
            j[l] = c;
            l++;
        }
    }

    /* access modifiers changed from: protected */
    public void Z() throws IOException, ov {
        this.M = false;
        int i = this.f;
        int i2 = this.g;
        char[] cArr = this.J;
        int i3 = i2;
        int i4 = i;
        int i5 = i3;
        while (true) {
            if (i4 >= i5) {
                this.f = i4;
                if (!E()) {
                    c(": was expecting closing quote for a string value");
                }
                int i6 = this.f;
                i4 = i6;
                i5 = this.g;
            }
            int i7 = i4 + 1;
            char c = cArr[i4];
            if (c <= '\\') {
                if (c == '\\') {
                    this.f = i7;
                    Q();
                    int i8 = this.f;
                    i4 = i8;
                    i5 = this.g;
                } else if (c <= '\"') {
                    if (c == '\"') {
                        this.f = i7;
                        return;
                    } else if (c < ' ') {
                        this.f = i7;
                        c(c, "string value");
                    }
                }
            }
            i4 = i7;
        }
    }

    /* access modifiers changed from: protected */
    public final void aa() throws IOException {
        if ((this.f < this.g || E()) && this.J[this.f] == 10) {
            this.f++;
        }
        this.i++;
        this.j = this.f;
    }

    /* access modifiers changed from: protected */
    public final void ab() throws IOException {
        this.i++;
        this.j = this.f;
    }

    private final int ae() throws IOException, ov {
        while (true) {
            if (this.f < this.g || E()) {
                char[] cArr = this.J;
                int i = this.f;
                this.f = i + 1;
                char c = cArr[i];
                if (c > ' ') {
                    if (c != '/') {
                        return c;
                    }
                    ag();
                } else if (c != ' ') {
                    if (c == 10) {
                        ab();
                    } else if (c == 13) {
                        aa();
                    } else if (c != 9) {
                        b(c);
                    }
                }
            } else {
                throw a("Unexpected end-of-input within/between " + this.n.d() + " entries");
            }
        }
    }

    private final int af() throws IOException, ov {
        while (true) {
            if (this.f < this.g || E()) {
                char[] cArr = this.J;
                int i = this.f;
                this.f = i + 1;
                char c = cArr[i];
                if (c > ' ') {
                    if (c != '/') {
                        return c;
                    }
                    ag();
                } else if (c != ' ') {
                    if (c == 10) {
                        ab();
                    } else if (c == 13) {
                        aa();
                    } else if (c != 9) {
                        b(c);
                    }
                }
            } else {
                H();
                return -1;
            }
        }
    }

    private final void ag() throws IOException, ov {
        if (!a(ox.ALLOW_COMMENTS)) {
            b(47, "maybe a (non-standard) comment? (not recognized as one since Feature 'ALLOW_COMMENTS' not enabled for parser)");
        }
        if (this.f >= this.g && !E()) {
            c(" in a comment");
        }
        char[] cArr = this.J;
        int i = this.f;
        this.f = i + 1;
        char c = cArr[i];
        if (c == '/') {
            ai();
        } else if (c == '*') {
            ah();
        } else {
            b(c, "was expecting either '*' or '/' for a comment");
        }
    }

    private final void ah() throws IOException, ov {
        while (true) {
            if (this.f >= this.g && !E()) {
                break;
            }
            char[] cArr = this.J;
            int i = this.f;
            this.f = i + 1;
            char c = cArr[i];
            if (c <= '*') {
                if (c == '*') {
                    if (this.f >= this.g && !E()) {
                        break;
                    } else if (this.J[this.f] == '/') {
                        this.f++;
                        return;
                    }
                } else if (c < ' ') {
                    if (c == 10) {
                        ab();
                    } else if (c == 13) {
                        aa();
                    } else if (c != 9) {
                        b(c);
                    }
                }
            }
        }
        c(" in a comment");
    }

    private final void ai() throws IOException, ov {
        while (true) {
            if (this.f < this.g || E()) {
                char[] cArr = this.J;
                int i = this.f;
                this.f = i + 1;
                char c = cArr[i];
                if (c < ' ') {
                    if (c == 10) {
                        ab();
                        return;
                    } else if (c == 13) {
                        aa();
                        return;
                    } else if (c != 9) {
                        b(c);
                    }
                }
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final char Q() throws IOException, ov {
        if (this.f >= this.g && !E()) {
            c(" in character escape sequence");
        }
        char[] cArr = this.J;
        int i = this.f;
        this.f = i + 1;
        char c = cArr[i];
        switch (c) {
            case '\"':
            case '/':
            case '\\':
                return c;
            case 'b':
                return 8;
            case 'f':
                return 12;
            case 'n':
                return 10;
            case 'r':
                return 13;
            case 't':
                return 9;
            case 'u':
                int i2 = 0;
                for (int i3 = 0; i3 < 4; i3++) {
                    if (this.f >= this.g && !E()) {
                        c(" in character escape sequence");
                    }
                    char[] cArr2 = this.J;
                    int i4 = this.f;
                    this.f = i4 + 1;
                    char c2 = cArr2[i4];
                    int a = afr.a(c2);
                    if (a < 0) {
                        b(c2, "expected a hex-digit for character escape sequence");
                    }
                    i2 = (i2 << 4) | a;
                }
                return (char) i2;
            default:
                return a(c);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(String str, int i) throws IOException, ov {
        char c;
        int length = str.length();
        int i2 = i;
        do {
            if (this.f >= this.g && !E()) {
                T();
            }
            if (this.J[this.f] != str.charAt(i2)) {
                a(str.substring(0, i2), "'null', 'true', 'false' or NaN");
            }
            this.f++;
            i2++;
        } while (i2 < length);
        if ((this.f < this.g || E()) && (c = this.J[this.f]) >= '0' && c != ']' && c != '}' && Character.isJavaIdentifierPart(c)) {
            this.f++;
            a(str.substring(0, i2), "'null', 'true', 'false' or NaN");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.ph.a(com.flurry.android.monolithic.sdk.impl.on, int, int, java.lang.String):java.lang.IllegalArgumentException
     arg types: [com.flurry.android.monolithic.sdk.impl.on, char, int, java.lang.String]
     candidates:
      com.flurry.android.monolithic.sdk.impl.ph.a(int, char[], int, int):void
      com.flurry.android.monolithic.sdk.impl.ph.a(boolean, int, int, int):com.flurry.android.monolithic.sdk.impl.pb
      com.flurry.android.monolithic.sdk.impl.pi.a(com.flurry.android.monolithic.sdk.impl.on, char, int, java.lang.String):void
      com.flurry.android.monolithic.sdk.impl.ph.a(com.flurry.android.monolithic.sdk.impl.on, int, int, java.lang.String):java.lang.IllegalArgumentException */
    /* access modifiers changed from: protected */
    public byte[] b(on onVar) throws IOException, ov {
        int i;
        int i2;
        int i3;
        int i4;
        afq I2 = I();
        while (true) {
            if (this.f >= this.g) {
                D();
            }
            char[] cArr = this.J;
            int i5 = this.f;
            this.f = i5 + 1;
            char c = cArr[i5];
            if (c > ' ') {
                int b = onVar.b(c);
                if (b >= 0) {
                    i = b;
                } else if (c == '\"') {
                    return I2.b();
                } else {
                    i = a(onVar, c, 0);
                    if (i < 0) {
                        continue;
                    }
                }
                if (this.f >= this.g) {
                    D();
                }
                char[] cArr2 = this.J;
                int i6 = this.f;
                this.f = i6 + 1;
                char c2 = cArr2[i6];
                int b2 = onVar.b(c2);
                if (b2 < 0) {
                    i2 = a(onVar, c2, 1);
                } else {
                    i2 = b2;
                }
                int i7 = (i << 6) | i2;
                if (this.f >= this.g) {
                    D();
                }
                char[] cArr3 = this.J;
                int i8 = this.f;
                this.f = i8 + 1;
                char c3 = cArr3[i8];
                int b3 = onVar.b(c3);
                if (b3 < 0) {
                    if (b3 == -2) {
                        i3 = b3;
                    } else if (c3 != '\"' || onVar.a()) {
                        i3 = a(onVar, c3, 2);
                    } else {
                        I2.a(i7 >> 4);
                        return I2.b();
                    }
                    if (i3 == -2) {
                        if (this.f >= this.g) {
                            D();
                        }
                        char[] cArr4 = this.J;
                        int i9 = this.f;
                        this.f = i9 + 1;
                        char c4 = cArr4[i9];
                        if (!onVar.a(c4)) {
                            throw a(onVar, (int) c4, 3, "expected padding character '" + onVar.b() + "'");
                        }
                        I2.a(i7 >> 4);
                    }
                } else {
                    i3 = b3;
                }
                int i10 = (i7 << 6) | i3;
                if (this.f >= this.g) {
                    D();
                }
                char[] cArr5 = this.J;
                int i11 = this.f;
                this.f = i11 + 1;
                char c5 = cArr5[i11];
                int b4 = onVar.b(c5);
                if (b4 < 0) {
                    if (b4 == -2) {
                        i4 = b4;
                    } else if (c5 != '\"' || onVar.a()) {
                        i4 = a(onVar, c5, 3);
                    } else {
                        I2.b(i10 >> 2);
                        return I2.b();
                    }
                    if (i4 == -2) {
                        I2.b(i10 >> 2);
                    }
                } else {
                    i4 = b4;
                }
                I2.c((i10 << 6) | i4);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(String str, String str2) throws IOException, ov {
        StringBuilder sb = new StringBuilder(str);
        while (true) {
            if (this.f >= this.g && !E()) {
                break;
            }
            char c = this.J[this.f];
            if (!Character.isJavaIdentifierPart(c)) {
                break;
            }
            this.f++;
            sb.append(c);
        }
        d("Unrecognized token '" + sb.toString() + "': was expecting ");
    }
}
