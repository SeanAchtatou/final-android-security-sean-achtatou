package com.flurry.android.monolithic.sdk.impl;

import android.location.Criteria;

public class ib {
    public static final Integer a = 151;
    public static final String b = null;
    public static final Boolean c = true;
    public static final Boolean d = false;
    public static final String e = null;
    public static final Boolean f = true;
    public static final Criteria g = null;
    public static final Long h = 10000L;
    public static final Boolean i = true;
    public static final Long j = 0L;
    public static final Byte k = (byte) -1;
    private static ic l;

    public static synchronized ic a() {
        ic icVar;
        synchronized (ib.class) {
            if (l == null) {
                l = new ic();
                b();
            }
            icVar = l;
        }
        return icVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Integer]
     candidates:
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.Object, java.lang.Object):boolean
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, com.flurry.android.monolithic.sdk.impl.id):void
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.Object, java.lang.Object):boolean
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, com.flurry.android.monolithic.sdk.impl.id):void
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Boolean]
     candidates:
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.Object, java.lang.Object):boolean
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, com.flurry.android.monolithic.sdk.impl.id):void
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, android.location.Criteria]
     candidates:
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.Object, java.lang.Object):boolean
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, com.flurry.android.monolithic.sdk.impl.id):void
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.Object, java.lang.Object):boolean
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, com.flurry.android.monolithic.sdk.impl.id):void
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Byte]
     candidates:
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.Object, java.lang.Object):boolean
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, com.flurry.android.monolithic.sdk.impl.id):void
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void */
    private static void b() {
        if (l == null) {
            l = new ic();
        }
        l.a("AgentVersion", (Object) a);
        l.a("VesionName", (Object) b);
        l.a("CaptureUncaughtExceptions", (Object) c);
        l.a("UseHttps", (Object) d);
        l.a("ReportUrl", (Object) e);
        l.a("ReportLocation", (Object) f);
        l.a("LocationCriteria", (Object) g);
        l.a("ContinueSessionMillis", (Object) h);
        l.a("LogEvents", (Object) i);
        l.a("Age", (Object) j);
        l.a("Gender", (Object) k);
        l.a("UserId", (Object) "");
    }
}
