package com.flurry.android.monolithic.sdk.impl;

import android.util.Pair;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class bz {
    private final Map<Pair<String, Integer>, List<AdUnit>> a = new HashMap();
    private List<ca> b = new LinkedList();

    public synchronized void a(ca caVar) {
        this.b.add(caVar);
    }

    public synchronized void a(String str, int i, AdUnit adUnit) {
        if (adUnit != null) {
            Pair create = Pair.create(str, Integer.valueOf(i));
            Object obj = this.a.get(create);
            if (obj == null) {
                obj = new ArrayList();
            }
            obj.add(adUnit);
            this.a.put(create, obj);
            c(str, i);
        }
    }

    public synchronized List<AdUnit> a(String str, int i) {
        ArrayList arrayList;
        List list = this.a.get(Pair.create(str, Integer.valueOf(i)));
        if (list == null) {
            arrayList = null;
        } else {
            arrayList = new ArrayList(list);
        }
        return arrayList;
    }

    public synchronized int b(String str, int i) {
        int size;
        List list = this.a.get(Pair.create(str, Integer.valueOf(i)));
        if (list == null) {
            size = 0;
        } else {
            size = list.size();
        }
        return size;
    }

    public synchronized void a(String str, int i, String str2) {
        List list = this.a.get(Pair.create(str, Integer.valueOf(i)));
        if (list != null) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                if (((AdUnit) it.next()).f().toString().equals(str2)) {
                    it.remove();
                }
            }
            c(str, i);
        }
    }

    public synchronized void a(String str, int i, int i2) {
        Pair create = Pair.create(str, Integer.valueOf(i));
        List list = this.a.get(create);
        if (list != null) {
            if (i2 > -1) {
                if (i2 <= list.size()) {
                    if (i2 > 0) {
                        list.subList(0, i2).clear();
                        this.a.put(create, list);
                    }
                    c(str, i);
                }
            }
            this.a.remove(create);
            c(str, i);
        }
    }

    public synchronized boolean b(String str, int i, AdUnit adUnit) {
        boolean remove;
        List list = this.a.get(Pair.create(str, Integer.valueOf(i)));
        if (list == null) {
            remove = false;
        } else {
            remove = list.remove(adUnit);
            if (remove) {
                c(str, i);
            }
        }
        return remove;
    }

    /* access modifiers changed from: package-private */
    public void c(String str, int i) {
        List list = this.a.get(Pair.create(str, Integer.valueOf(i)));
        int size = list == null ? 0 : list.size();
        for (ca next : this.b) {
            if (next != null) {
                next.a(str, i, size, list);
            }
        }
    }
}
