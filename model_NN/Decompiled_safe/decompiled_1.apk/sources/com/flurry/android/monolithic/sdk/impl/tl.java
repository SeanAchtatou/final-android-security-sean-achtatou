package com.flurry.android.monolithic.sdk.impl;

import java.util.Iterator;
import java.util.NoSuchElementException;

final class tl implements Iterator<sw> {
    private final tk[] a;
    private tk b;
    private int c;

    public tl(tk[] tkVarArr) {
        this.a = tkVarArr;
        int i = 0;
        int length = this.a.length;
        while (true) {
            if (i >= length) {
                break;
            }
            int i2 = i + 1;
            tk tkVar = this.a[i];
            if (tkVar != null) {
                this.b = tkVar;
                i = i2;
                break;
            }
            i = i2;
        }
        this.c = i;
    }

    public boolean hasNext() {
        return this.b != null;
    }

    /* renamed from: a */
    public sw next() {
        tk tkVar = this.b;
        if (tkVar == null) {
            throw new NoSuchElementException();
        }
        tk tkVar2 = tkVar.a;
        while (tkVar2 == null && this.c < this.a.length) {
            tk[] tkVarArr = this.a;
            int i = this.c;
            this.c = i + 1;
            tkVar2 = tkVarArr[i];
        }
        this.b = tkVar2;
        return tkVar.c;
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }
}
