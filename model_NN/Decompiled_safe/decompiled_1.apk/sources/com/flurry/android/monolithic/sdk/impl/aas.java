package com.flurry.android.monolithic.sdk.impl;

import java.util.HashMap;

public final class aas {
    protected final aaj a;
    protected final aau b = new aau(getClass(), false);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.aau.<init>(java.lang.Class<?>, boolean):void
     arg types: [java.lang.Class<?>, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.aau.<init>(com.flurry.android.monolithic.sdk.impl.afm, boolean):void
      com.flurry.android.monolithic.sdk.impl.aau.<init>(java.lang.Class<?>, boolean):void */
    private aas(aaj aaj) {
        this.a = aaj;
    }

    public aas a() {
        return new aas(this.a);
    }

    public static aas a(HashMap<aau, ra<Object>> hashMap) {
        return new aas(new aaj(hashMap));
    }

    public ra<Object> a(afm afm) {
        this.b.a(afm);
        return this.a.a(this.b);
    }

    public ra<Object> a(Class<?> cls) {
        this.b.a(cls);
        return this.a.a(this.b);
    }

    public ra<Object> b(Class<?> cls) {
        this.b.b(cls);
        return this.a.a(this.b);
    }

    public ra<Object> b(afm afm) {
        this.b.b(afm);
        return this.a.a(this.b);
    }
}
