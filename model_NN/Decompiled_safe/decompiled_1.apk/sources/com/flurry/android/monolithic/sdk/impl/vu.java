package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
public final class vu extends wa<Character> {
    public vu(Class<Character> cls, Character ch) {
        super(cls, ch);
    }

    /* renamed from: b */
    public Character a(ow owVar, qm qmVar) throws IOException, oz {
        pb e = owVar.e();
        if (e == pb.VALUE_NUMBER_INT) {
            int t = owVar.t();
            if (t >= 0 && t <= 65535) {
                return Character.valueOf((char) t);
            }
        } else if (e == pb.VALUE_STRING) {
            String k = owVar.k();
            if (k.length() == 1) {
                return Character.valueOf(k.charAt(0));
            }
            if (k.length() == 0) {
                return (Character) c();
            }
        }
        throw qmVar.a(this.q, e);
    }
}
