package com.flurry.android.monolithic.sdk.impl;

import java.lang.Thread;

final class iu implements Thread.UncaughtExceptionHandler {
    final /* synthetic */ is a;

    private iu(is isVar) {
        this.a = isVar;
    }

    public void uncaughtException(Thread thread, Throwable th) {
        this.a.a(thread, th);
        this.a.b(thread, th);
    }
}
