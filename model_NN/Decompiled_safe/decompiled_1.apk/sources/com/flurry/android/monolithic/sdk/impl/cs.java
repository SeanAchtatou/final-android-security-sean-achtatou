package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import android.text.TextUtils;
import com.flurry.android.impl.ads.FlurryAdModule;
import com.flurry.android.impl.ads.avro.protocol.v6.AdFrame;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public final class cs implements co {
    private static final String a = cs.class.getSimpleName();
    private final Map<String, co> b;

    public cs(Map<String, co> map) {
        this.b = map;
    }

    public cn a_(Context context, FlurryAdModule flurryAdModule, m mVar, AdUnit adUnit) {
        if (context == null || flurryAdModule == null || mVar == null || adUnit == null) {
            return null;
        }
        List<AdFrame> d = adUnit.d();
        if (d == null || d.isEmpty()) {
            return null;
        }
        AdFrame adFrame = d.get(0);
        if (adFrame == null) {
            return null;
        }
        String obj = adFrame.d().toString();
        if (TextUtils.isEmpty(obj)) {
            return null;
        }
        co a2 = a(obj);
        if (a2 == null) {
            return null;
        }
        ja.a(3, a, "Creating ad network takeover launcher: " + a2.getClass().getSimpleName() + " for type: " + obj);
        cn a_ = a2.a_(context, flurryAdModule, mVar, adUnit);
        if (a_ == null) {
            ja.b(a, "Cannot create ad network takeover launcher for type: " + obj);
        }
        return a_;
    }

    private co a(String str) {
        if (this.b != null) {
            return this.b.get(str.toUpperCase(Locale.US));
        }
        return null;
    }
}
