package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
public class abf extends abq<Enum<?>> {
    protected final aee a;

    public /* bridge */ /* synthetic */ void a(Object obj, or orVar, ru ruVar) throws IOException, oq {
        a((Enum<?>) ((Enum) obj), orVar, ruVar);
    }

    public abf(aee aee) {
        super(Enum.class, false);
        this.a = aee;
    }

    public static abf a(Class<Enum<?>> cls, rq rqVar, xq xqVar) {
        py a2 = rqVar.a();
        return new abf(rqVar.a(rr.WRITE_ENUMS_USING_TO_STRING) ? aee.c(cls, a2) : aee.b(cls, a2));
    }

    public final void a(Enum<?> enumR, or orVar, ru ruVar) throws IOException, oq {
        if (ruVar.a(rr.WRITE_ENUMS_USING_INDEX)) {
            orVar.b(enumR.ordinal());
        } else {
            orVar.b(this.a.a(enumR));
        }
    }

    public aee d() {
        return this.a;
    }
}
