package com.flurry.android.monolithic.sdk.impl;

import java.util.Calendar;
import java.util.Date;

public class acr {
    protected static final ra<Object> a = new acq();
    protected static final ra<Object> b = new acu();

    public static ra<Object> a(afm afm) {
        if (afm == null) {
            return a;
        }
        Class<?> p = afm.p();
        if (p == String.class) {
            return b;
        }
        if (p == Object.class) {
            return a;
        }
        if (Date.class.isAssignableFrom(p)) {
            return act.a;
        }
        if (Calendar.class.isAssignableFrom(p)) {
            return acs.a;
        }
        return a;
    }
}
