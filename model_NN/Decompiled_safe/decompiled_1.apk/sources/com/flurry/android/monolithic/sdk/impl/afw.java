package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;

public class afw extends ow {
    protected ow d;

    public afw(ow owVar) {
        this.d = owVar;
    }

    public pc a() {
        return this.d.a();
    }

    public boolean a(ox oxVar) {
        return this.d.a(oxVar);
    }

    public void close() throws IOException {
        this.d.close();
    }

    public pb e() {
        return this.d.e();
    }

    public void f() {
        this.d.f();
    }

    public String g() throws IOException, ov {
        return this.d.g();
    }

    public ot i() {
        return this.d.i();
    }

    public String k() throws IOException, ov {
        return this.d.k();
    }

    public char[] l() throws IOException, ov {
        return this.d.l();
    }

    public int m() throws IOException, ov {
        return this.d.m();
    }

    public int n() throws IOException, ov {
        return this.d.n();
    }

    public BigInteger v() throws IOException, ov {
        return this.d.v();
    }

    public byte r() throws IOException, ov {
        return this.d.r();
    }

    public short s() throws IOException, ov {
        return this.d.s();
    }

    public BigDecimal y() throws IOException, ov {
        return this.d.y();
    }

    public double x() throws IOException, ov {
        return this.d.x();
    }

    public float w() throws IOException, ov {
        return this.d.w();
    }

    public int t() throws IOException, ov {
        return this.d.t();
    }

    public long u() throws IOException, ov {
        return this.d.u();
    }

    public oy q() throws IOException, ov {
        return this.d.q();
    }

    public Number p() throws IOException, ov {
        return this.d.p();
    }

    public byte[] a(on onVar) throws IOException, ov {
        return this.d.a(onVar);
    }

    public Object z() throws IOException, ov {
        return this.d.z();
    }

    public ot h() {
        return this.d.h();
    }

    public pb b() throws IOException, ov {
        return this.d.b();
    }

    public ow d() throws IOException, ov {
        this.d.d();
        return this;
    }
}
