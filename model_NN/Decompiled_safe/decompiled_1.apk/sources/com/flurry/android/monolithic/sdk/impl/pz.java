package com.flurry.android.monolithic.sdk.impl;

public class pz {
    private final qa a;
    private final String b;

    public pz(qa qaVar, String str) {
        this.a = qaVar;
        this.b = str;
    }

    public static pz a(String str) {
        return new pz(qa.MANAGED_REFERENCE, str);
    }

    public static pz b(String str) {
        return new pz(qa.BACK_REFERENCE, str);
    }

    public String a() {
        return this.b;
    }

    public boolean b() {
        return this.a == qa.MANAGED_REFERENCE;
    }

    public boolean c() {
        return this.a == qa.BACK_REFERENCE;
    }
}
