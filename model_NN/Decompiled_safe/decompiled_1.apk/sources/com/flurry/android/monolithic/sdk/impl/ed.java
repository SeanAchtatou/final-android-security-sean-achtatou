package com.flurry.android.monolithic.sdk.impl;

import com.mobclix.android.sdk.MobclixAdView;
import com.mobclix.android.sdk.MobclixAdViewListener;
import java.util.Collections;

class ed implements MobclixAdViewListener {
    final /* synthetic */ ec a;

    ed(ec ecVar) {
        this.a = ecVar;
    }

    public void onSuccessfulLoad(MobclixAdView mobclixAdView) {
        this.a.onAdShown(Collections.emptyMap());
        ja.a(3, ec.a, "Mobclix AdView ad successfully loaded.");
    }

    public void onFailedLoad(MobclixAdView mobclixAdView, int i) {
        this.a.onRenderFailed(Collections.emptyMap());
        ja.a(3, ec.a, "Mobclix AdView ad failed to load.");
    }

    public boolean onOpenAllocationLoad(MobclixAdView mobclixAdView, int i) {
        ja.a(3, ec.a, "Mobclix AdView loaded an open allocation ad.");
        return true;
    }

    public void onAdClick(MobclixAdView mobclixAdView) {
        this.a.onAdClicked(Collections.emptyMap());
        ja.a(3, ec.a, "Mobclix AdView ad clicked.");
    }

    public void onCustomAdTouchThrough(MobclixAdView mobclixAdView, String str) {
        this.a.onAdClicked(Collections.emptyMap());
        ja.a(3, ec.a, "Mobclix AdView custom ad clicked.");
    }

    public String keywords() {
        ja.a(3, ec.a, "Mobclix keyword callback.");
        return null;
    }

    public String query() {
        ja.a(3, ec.a, "Mobclix query callback.");
        return null;
    }
}
