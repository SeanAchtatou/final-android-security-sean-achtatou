package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.lang.reflect.Field;

public final class sx extends sw {
    protected final xj i;
    protected final Field j;

    public sx(String str, afm afm, rw rwVar, ado ado, xj xjVar) {
        super(str, afm, rwVar, ado);
        this.i = xjVar;
        this.j = xjVar.a();
    }

    protected sx(sx sxVar, qu<Object> quVar) {
        super(sxVar, quVar);
        this.i = sxVar.i;
        this.j = sxVar.j;
    }

    /* renamed from: b */
    public sx a(qu<Object> quVar) {
        return new sx(this, quVar);
    }

    public xk b() {
        return this.i;
    }

    public void a(ow owVar, qm qmVar, Object obj) throws IOException, oz {
        a(obj, a(owVar, qmVar));
    }

    public final void a(Object obj, Object obj2) throws IOException {
        try {
            this.j.set(obj, obj2);
        } catch (Exception e) {
            a(e, obj2);
        }
    }
}
