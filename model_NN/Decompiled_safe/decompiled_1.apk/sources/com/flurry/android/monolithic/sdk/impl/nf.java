package com.flurry.android.monolithic.sdk.impl;

import java.util.Iterator;
import java.util.NoSuchElementException;

class nf implements Iterator<mq> {
    final /* synthetic */ ne a;
    private int b = this.a.b.length;

    nf(ne neVar) {
        this.a = neVar;
    }

    public boolean hasNext() {
        return this.b > 0;
    }

    /* renamed from: a */
    public mq next() {
        if (this.b > 0) {
            mq[] mqVarArr = this.a.b;
            int i = this.b - 1;
            this.b = i;
            return mqVarArr[i];
        }
        throw new NoSuchElementException();
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }
}
