package com.flurry.android.monolithic.sdk.impl;

public enum afo {
    READ_IO_BUFFER(4000),
    WRITE_ENCODING_BUFFER(4000),
    WRITE_CONCAT_BUFFER(2000);
    
    private final int d;

    private afo(int i) {
        this.d = i;
    }
}
