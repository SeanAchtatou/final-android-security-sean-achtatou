package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
public class abp<T> extends abt<T> {
    public abp(Class<?> cls) {
        super(cls, false);
    }

    public void a(T t, or orVar, ru ruVar) throws IOException, oq {
        orVar.d(t.toString());
    }

    public void a(T t, or orVar, ru ruVar, rx rxVar) throws IOException, oz {
        rxVar.a(t, orVar);
        a(t, orVar, ruVar);
        rxVar.d(t, orVar);
    }
}
