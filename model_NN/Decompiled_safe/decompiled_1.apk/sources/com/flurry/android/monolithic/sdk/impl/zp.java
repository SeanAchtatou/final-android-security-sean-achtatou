package com.flurry.android.monolithic.sdk.impl;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Map;

public class zp {
    protected final rq a;
    protected final xq b;
    protected final sf c;
    protected final py d = this.a.a();
    protected Object e;

    public zp(rq rqVar, xq xqVar) {
        this.a = rqVar;
        this.b = xqVar;
        this.c = xqVar.a(rqVar.g());
    }

    public ado a() {
        return this.b.i();
    }

    /* access modifiers changed from: protected */
    public zf a(String str, afm afm, ra<Object> raVar, rx rxVar, rx rxVar2, xk xkVar, boolean z) {
        Field field;
        Method e2;
        afm afm2;
        Object obj;
        boolean z2;
        zf zfVar;
        Boolean b2;
        if (xkVar instanceof xj) {
            field = ((xj) xkVar).a();
            e2 = null;
        } else {
            field = null;
            e2 = ((xl) xkVar).a();
        }
        afm a2 = a(xkVar, z, afm);
        if (rxVar2 != null) {
            if (a2 == null) {
                a2 = afm;
            }
            if (a2.g() == null) {
                throw new IllegalStateException("Problem trying to create BeanPropertyWriter for property '" + str + "' (of type " + this.b.a() + "); serialization type " + a2 + " has no content");
            }
            afm e3 = a2.e(rxVar2);
            e3.g();
            afm2 = e3;
        } else {
            afm2 = a2;
        }
        boolean z3 = false;
        sf a3 = this.d.a(xkVar, this.c);
        if (a3 != null) {
            switch (zq.a[a3.ordinal()]) {
                case 1:
                    Object a4 = a(str, e2, field);
                    if (a4 != null) {
                        if (!a4.getClass().isArray()) {
                            z2 = false;
                            obj = a4;
                            break;
                        } else {
                            z2 = false;
                            obj = aeb.a(a4);
                            break;
                        }
                    } else {
                        z2 = true;
                        obj = a4;
                        break;
                    }
                case 2:
                    z2 = true;
                    obj = b(str, afm);
                    break;
                case 3:
                    z3 = true;
                case 4:
                    if (afm.f()) {
                        z2 = z3;
                        obj = a(str, afm);
                        break;
                    }
                default:
                    z2 = z3;
                    obj = null;
                    break;
            }
            zfVar = new zf(xkVar, this.b.i(), str, afm, raVar, rxVar, afm2, e2, field, z2, obj);
            b2 = this.d.b(xkVar);
            if (b2 == null && b2.booleanValue()) {
                return zfVar.c();
            }
        }
        z2 = z3;
        obj = null;
        zfVar = new zf(xkVar, this.b.i(), str, afm, raVar, rxVar, afm2, e2, field, z2, obj);
        b2 = this.d.b(xkVar);
        return b2 == null ? zfVar : zfVar;
    }

    /* access modifiers changed from: protected */
    public afm a(xg xgVar, boolean z, afm afm) {
        afm afm2;
        boolean z2;
        sg f;
        Class<?> e2 = this.d.e(xgVar);
        if (e2 != null) {
            Class<?> p = afm.p();
            if (e2.isAssignableFrom(p)) {
                afm2 = afm.h(e2);
            } else if (!p.isAssignableFrom(e2)) {
                throw new IllegalArgumentException("Illegal concrete-type annotation for method '" + xgVar.b() + "': class " + e2.getName() + " not a super-type of (declared) class " + p.getName());
            } else {
                afm2 = this.a.a(afm, e2);
            }
            z2 = true;
        } else {
            afm2 = afm;
            z2 = z;
        }
        afm b2 = zi.b(this.a, xgVar, afm2);
        if (b2 != afm2) {
            afm2 = b2;
            z2 = true;
        }
        if (!z2 && (f = this.d.f(xgVar)) != null) {
            z2 = f == sg.STATIC;
        }
        if (z2) {
            return afm2;
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public Object b() {
        if (this.e == null) {
            this.e = this.b.a(this.a.a(rr.CAN_OVERRIDE_ACCESS_MODIFIERS));
            if (this.e == null) {
                throw new IllegalArgumentException("Class " + this.b.c().a().getName() + " has no default constructor; can not instantiate default bean value to support 'properties=JsonSerialize.Inclusion.NON_DEFAULT' annotation");
            }
        }
        return this.e;
    }

    /* access modifiers changed from: protected */
    public Object a(String str, Method method, Field field) {
        Object b2 = b();
        if (method == null) {
            return field.get(b2);
        }
        try {
            return method.invoke(b2, new Object[0]);
        } catch (Exception e2) {
            return a(e2, str, b2);
        }
    }

    /* access modifiers changed from: protected */
    public Object a(String str, afm afm) {
        if (!this.a.a(rr.WRITE_EMPTY_JSON_ARRAYS)) {
            if (afm.b()) {
                return new zr();
            }
            if (Collection.class.isAssignableFrom(afm.p())) {
                return new zs();
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public Object b(String str, afm afm) {
        Class<?> p = afm.p();
        if (p == String.class) {
            return new zu();
        }
        if (afm.b()) {
            return new zr();
        }
        if (Collection.class.isAssignableFrom(p)) {
            return new zs();
        }
        if (Map.class.isAssignableFrom(p)) {
            return new zt();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public Object a(Exception exc, String str, Object obj) {
        Throwable th = exc;
        while (th.getCause() != null) {
            th = th.getCause();
        }
        if (th instanceof Error) {
            throw ((Error) th);
        } else if (th instanceof RuntimeException) {
            throw ((RuntimeException) th);
        } else {
            throw new IllegalArgumentException("Failed to get property '" + str + "' of default " + obj.getClass().getName() + " instance");
        }
    }
}
