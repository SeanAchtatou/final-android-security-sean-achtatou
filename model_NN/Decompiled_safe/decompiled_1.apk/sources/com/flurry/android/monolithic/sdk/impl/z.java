package com.flurry.android.monolithic.sdk.impl;

import android.content.DialogInterface;

class z implements DialogInterface.OnDismissListener {
    final /* synthetic */ w a;

    z(w wVar) {
        this.a = wVar;
    }

    public void onDismiss(DialogInterface dialogInterface) {
        ja.a(3, this.a.a.f, "customViewFullScreenDialog.onDismiss()");
        if (this.a.a.p != null && this.a.a.o != null) {
            this.a.a.o.onHideCustomView();
        }
    }
}
