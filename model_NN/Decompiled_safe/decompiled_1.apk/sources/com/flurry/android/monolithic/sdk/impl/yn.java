package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public class yn extends zb {
    protected final String a;

    public yn(yi yiVar, qc qcVar, String str) {
        super(yiVar, qcVar);
        this.a = str;
    }

    public void b(Object obj, or orVar) throws IOException, oz {
        g(obj, orVar);
    }

    public void c(Object obj, or orVar) throws IOException, oz {
        g(obj, orVar);
    }

    public void a(Object obj, or orVar) throws IOException, oz {
        g(obj, orVar);
    }

    public void a(Object obj, or orVar, Class<?> cls) throws IOException, oz {
        b(obj, orVar, cls);
    }

    public void e(Object obj, or orVar) throws IOException, oz {
        h(obj, orVar);
    }

    public void f(Object obj, or orVar) throws IOException, oz {
        h(obj, orVar);
    }

    public void d(Object obj, or orVar) throws IOException, oz {
        h(obj, orVar);
    }

    /* access modifiers changed from: protected */
    public final void g(Object obj, or orVar) throws IOException, oz {
        orVar.d();
    }

    /* access modifiers changed from: protected */
    public final void b(Object obj, or orVar, Class<?> cls) throws IOException, oz {
        orVar.d();
    }

    /* access modifiers changed from: protected */
    public final void h(Object obj, or orVar) throws IOException, oz {
        orVar.e();
        orVar.a(this.a, this.b.a(obj));
    }
}
