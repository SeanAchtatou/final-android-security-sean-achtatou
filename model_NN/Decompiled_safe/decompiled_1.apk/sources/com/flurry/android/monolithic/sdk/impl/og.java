package com.flurry.android.monolithic.sdk.impl;

public class og {
    private final String a;
    private final String b;

    og(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    public String a() {
        return this.a;
    }

    public String b() {
        return this.b;
    }

    public String toString() {
        return this.a + ": " + this.b;
    }
}
