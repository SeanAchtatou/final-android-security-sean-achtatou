package com.flurry.android.monolithic.sdk.impl;

import java.io.File;

public final class iw {
    static String a = iw.class.getSimpleName();

    private iw() {
    }

    public static boolean a(File file) {
        if (file == null) {
            return false;
        }
        File parentFile = file.getParentFile();
        if (parentFile.mkdirs() || parentFile.exists()) {
            return true;
        }
        ja.a(6, a, "Unable to create persistent dir: " + parentFile);
        return false;
    }
}
