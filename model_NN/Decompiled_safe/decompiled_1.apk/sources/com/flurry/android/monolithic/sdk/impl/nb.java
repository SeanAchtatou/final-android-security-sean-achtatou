package com.flurry.android.monolithic.sdk.impl;

import java.util.List;
import java.util.Map;

public class nb extends mq {
    public final mq z;

    private nb(mq mqVar, mq... mqVarArr) {
        super(na.REPEATER, c(mqVarArr));
        this.z = mqVar;
        this.b[0] = this;
    }

    private static mq[] c(mq[] mqVarArr) {
        mq[] mqVarArr2 = new mq[(mqVarArr.length + 1)];
        System.arraycopy(mqVarArr, 0, mqVarArr2, 1, mqVarArr.length);
        return mqVarArr2;
    }

    /* renamed from: b */
    public nb a(Map<ne, ne> map, Map<ne, List<mx>> map2) {
        nb nbVar = new nb(this.z, new mq[a(this.b, 1)]);
        a(this.b, 1, nbVar.b, 1, map, map2);
        return nbVar;
    }
}
