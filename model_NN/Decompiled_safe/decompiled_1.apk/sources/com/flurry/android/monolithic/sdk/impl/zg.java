package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public class zg extends aaz {
    public zg(afm afm, zf[] zfVarArr, zf[] zfVarArr2, zc zcVar, Object obj) {
        super(afm, zfVarArr, zfVarArr2, zcVar, obj);
    }

    public zg(Class<?> cls, zf[] zfVarArr, zf[] zfVarArr2, zc zcVar, Object obj) {
        super(cls, zfVarArr, zfVarArr2, zcVar, obj);
    }

    public static zg a(Class<?> cls) {
        return new zg(cls, a, (zf[]) null, (zc) null, (Object) null);
    }

    public ra<Object> a() {
        return new aax(this);
    }

    public final void a(Object obj, or orVar, ru ruVar) throws IOException, oq {
        orVar.d();
        if (this.e != null) {
            c(obj, orVar, ruVar);
        } else {
            b(obj, orVar, ruVar);
        }
        orVar.e();
    }

    public String toString() {
        return "BeanSerializer for " + c().getName();
    }
}
