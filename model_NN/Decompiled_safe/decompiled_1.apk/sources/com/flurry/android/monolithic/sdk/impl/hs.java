package com.flurry.android.monolithic.sdk.impl;

import java.util.LinkedList;

class hs {
    private LinkedList<hm> a = new LinkedList<>();

    hs() {
    }

    public void a(hm hmVar) {
        this.a.add(hmVar);
    }

    public hm a() {
        if (this.a.size() > 0) {
            return this.a.getFirst();
        }
        ja.a(6, "AppCloudOperationsManager", "NO SUCH OPERATION");
        return null;
    }

    public hm b() {
        return this.a.removeFirst();
    }
}
