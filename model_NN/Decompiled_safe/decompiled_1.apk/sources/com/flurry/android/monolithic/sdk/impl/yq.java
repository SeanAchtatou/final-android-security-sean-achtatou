package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public class yq extends yl {
    protected final String a;

    public yq(yi yiVar, qc qcVar, String str) {
        super(yiVar, qcVar);
        this.a = str;
    }

    public void b(Object obj, or orVar) throws IOException, oz {
        orVar.d();
        orVar.a(this.a, this.b.a(obj));
    }

    public void e(Object obj, or orVar) throws IOException, oz {
        orVar.e();
    }
}
