package com.flurry.android.monolithic.sdk.impl;

import java.io.File;
import java.io.FilenameFilter;

final class iq implements FilenameFilter {
    iq() {
    }

    public boolean accept(File file, String str) {
        return str.startsWith(".flurryagent.");
    }
}
