package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
final class vn extends ve<String[]> {
    public vn() {
        super(String[].class);
    }

    /* renamed from: b */
    public String[] a(ow owVar, qm qmVar) throws IOException, oz {
        if (!owVar.j()) {
            return c(owVar, qmVar);
        }
        aeh g = qmVar.g();
        Object[] a = g.a();
        int i = 0;
        while (true) {
            pb b = owVar.b();
            if (b != pb.END_ARRAY) {
                String k = b == pb.VALUE_NULL ? null : owVar.k();
                if (i >= a.length) {
                    a = g.a(a);
                    i = 0;
                }
                a[i] = k;
                i++;
            } else {
                qmVar.a(g);
                return (String[]) g.a(a, i, String.class);
            }
        }
    }

    private final String[] c(ow owVar, qm qmVar) throws IOException, oz {
        if (qmVar.a(ql.ACCEPT_SINGLE_VALUE_AS_ARRAY)) {
            String[] strArr = new String[1];
            strArr[0] = owVar.e() == pb.VALUE_NULL ? null : owVar.k();
            return strArr;
        } else if (owVar.e() == pb.VALUE_STRING && qmVar.a(ql.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT) && owVar.k().length() == 0) {
            return null;
        } else {
            throw qmVar.b(this.q);
        }
    }
}
