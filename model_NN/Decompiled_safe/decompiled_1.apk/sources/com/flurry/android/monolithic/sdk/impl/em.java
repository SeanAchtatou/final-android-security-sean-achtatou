package com.flurry.android.monolithic.sdk.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class em implements id, jb {
    static String a;
    static String b = "http://data.flurry.com/aap.do";
    static String c = "https://data.flurry.com/aap.do";
    /* access modifiers changed from: private */
    public static final String f = em.class.getSimpleName();
    ey d;
    Set<String> e;
    private boolean g;
    private ExecutorService h;
    private ExecutorService i;

    public em() {
        this(null);
    }

    em(eu euVar) {
        this.e = new HashSet();
        jc.a().a(this);
        this.h = Executors.newSingleThreadExecutor();
        this.i = Executors.newCachedThreadPool();
        e();
        f();
        a(euVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, com.flurry.android.monolithic.sdk.impl.id):void
     arg types: [java.lang.String, com.flurry.android.monolithic.sdk.impl.em]
     candidates:
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.Object, java.lang.Object):boolean
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, java.lang.Object):void
      com.flurry.android.monolithic.sdk.impl.ic.a(java.lang.String, com.flurry.android.monolithic.sdk.impl.id):void */
    private void e() {
        ic a2 = ib.a();
        this.g = ((Boolean) a2.a("UseHttps")).booleanValue();
        a2.a("UseHttps", (id) this);
        ja.a(4, f, "initSettings, UseHttps = " + this.g);
        String str = (String) a2.a("ReportUrl");
        a2.a("ReportUrl", (id) this);
        a(str);
        ja.a(4, f, "initSettings, ReportUrl = " + str);
    }

    public void a(String str, Object obj) {
        if (str.equals("UseHttps")) {
            this.g = ((Boolean) obj).booleanValue();
            ja.a(4, f, "onSettingUpdate, UseHttps = " + this.g);
        } else if (str.equals("ReportUrl")) {
            String str2 = (String) obj;
            a(str2);
            ja.a(4, f, "onSettingUpdate, ReportUrl = " + str2);
        } else {
            ja.a(6, f, "onSettingUpdate internal error!");
        }
    }

    private void f() {
        this.h.submit(new en(this));
    }

    /* access modifiers changed from: private */
    public void g() {
        a((eu) null);
    }

    private void a(eu euVar) {
        this.h.submit(new eo(this, euVar));
    }

    public void b(boolean z) {
        ja.a(4, f, "onNetworkStateChanged : isNetworkEnable = " + z);
        if (z) {
            g();
        }
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.e.size();
    }

    private void a(String str) {
        if (str != null && !str.endsWith(".do")) {
            ja.a(5, f, "overriding analytics agent report URL without an endpoint, are you sure?");
        }
        a = str;
    }

    /* access modifiers changed from: package-private */
    public String b() {
        if (a != null) {
            return a;
        }
        if (this.g) {
            return c;
        }
        return b;
    }

    /* access modifiers changed from: package-private */
    public String a(String str, String str2) {
        return "Data_" + str + "_" + str2;
    }

    public void a(byte[] bArr, String str, String str2) {
        a(bArr, str, str2, (eu) null);
    }

    /* access modifiers changed from: package-private */
    public void a(byte[] bArr, String str, String str2, eu euVar) {
        if (bArr == null || bArr.length == 0) {
            ja.a(6, f, "Report that has to be sent is EMPTY or NULL");
            return;
        }
        c(bArr, str, str2);
        a(euVar);
    }

    private void c(byte[] bArr, String str, String str2) {
        this.h.submit(new ep(this, bArr, str, str2));
    }

    /* access modifiers changed from: package-private */
    public String b(byte[] bArr, String str, String str2) {
        String a2 = a(str, str2);
        ew ewVar = new ew();
        ewVar.a(bArr);
        String a3 = ewVar.a();
        this.d.a(ewVar, a2);
        return a3;
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return a() <= 8;
    }

    /* access modifiers changed from: private */
    public void h() {
        if (!jc.a().c()) {
            ja.a(5, f, "Reports were not sent! No Internet connection!");
            return;
        }
        List<String> b2 = this.d.b();
        if (b2 == null || b2.isEmpty()) {
            ja.a(4, f, "No more reports to send.");
            return;
        }
        for (String next : b2) {
            if (c()) {
                List<String> b3 = this.d.b(next);
                ja.a(4, f, "Number of not sent blocks = " + b3.size());
                int i2 = 0;
                while (true) {
                    int i3 = i2;
                    if (i3 >= b3.size()) {
                        break;
                    }
                    String str = b3.get(i3);
                    if (!this.e.contains(str)) {
                        if (!c()) {
                            break;
                        }
                        byte[] b4 = new ew(str).b();
                        if (b4 == null || b4.length == 0) {
                            ja.a(6, f, "Internal ERROR! Report is empty!");
                            this.d.a(str, next);
                        } else {
                            this.e.add(str);
                            d(b4, str, next);
                        }
                    }
                    i2 = i3 + 1;
                }
            } else {
                return;
            }
        }
    }

    private void d(byte[] bArr, String str, String str2) {
        String b2 = b();
        ja.a(4, f, "FlurryDataSender: start upload data with id = " + str + " to " + b2);
        this.i.submit(new ev(b2, str, str2, bArr, new eq(this)));
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2, int i2) {
        this.h.submit(new es(this, i2, str, str2));
    }

    /* access modifiers changed from: private */
    public void b(String str, String str2) {
        this.h.submit(new et(this, str));
    }

    /* access modifiers changed from: private */
    public void i() {
        Thread.currentThread().setName("DataSender Main Single Thread , id = " + Thread.currentThread().getId());
    }
}
