package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.sql.Date;

@rz
public final class aag extends abq<Date> {
    public aag() {
        super(Date.class);
    }

    public void a(Date date, or orVar, ru ruVar) throws IOException, oq {
        orVar.b(date.toString());
    }
}
