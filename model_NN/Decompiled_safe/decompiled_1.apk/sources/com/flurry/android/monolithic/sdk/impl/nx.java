package com.flurry.android.monolithic.sdk.impl;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class nx<K, V> implements Map<K, V> {
    /* access modifiers changed from: private */
    public final ReferenceQueue<K> a = new ReferenceQueue<>();
    private Map<nx<K, V>.nz, V> b = new HashMap();

    public void clear() {
        this.b.clear();
        a();
    }

    public boolean containsKey(Object obj) {
        a();
        return this.b.containsKey(new nz(this, obj));
    }

    public boolean containsValue(Object obj) {
        a();
        return this.b.containsValue(obj);
    }

    public Set<Map.Entry<K, V>> entrySet() {
        a();
        HashSet hashSet = new HashSet();
        for (Map.Entry next : this.b.entrySet()) {
            hashSet.add(new ny(this, ((nz) next.getKey()).get(), next.getValue()));
        }
        return Collections.unmodifiableSet(hashSet);
    }

    public Set<K> keySet() {
        a();
        HashSet hashSet = new HashSet();
        Iterator<nx<K, V>.nz> it = this.b.keySet().iterator();
        while (it.hasNext()) {
            hashSet.add(it.next().get());
        }
        return Collections.unmodifiableSet(hashSet);
    }

    public boolean equals(Object obj) {
        return this.b.equals(((nx) obj).b);
    }

    public V get(Object obj) {
        a();
        return this.b.get(new nz(this, obj));
    }

    public V put(K k, V v) {
        a();
        return this.b.put(new nz(this, k), v);
    }

    public int hashCode() {
        a();
        return this.b.hashCode();
    }

    public boolean isEmpty() {
        a();
        return this.b.isEmpty();
    }

    public void putAll(Map map) {
        throw new UnsupportedOperationException();
    }

    public V remove(Object obj) {
        a();
        return this.b.remove(new nz(this, obj));
    }

    public int size() {
        a();
        return this.b.size();
    }

    public Collection<V> values() {
        a();
        return this.b.values();
    }

    private synchronized void a() {
        Reference<? extends K> poll = this.a.poll();
        while (poll != null) {
            this.b.remove((nz) poll);
            poll = this.a.poll();
        }
    }
}
