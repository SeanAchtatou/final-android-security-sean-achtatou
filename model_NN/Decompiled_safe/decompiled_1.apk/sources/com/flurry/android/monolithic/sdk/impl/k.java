package com.flurry.android.monolithic.sdk.impl;

import android.text.TextUtils;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class k {
    static final List<String> a = Arrays.asList("requested", "filled", "unfilled", "rendered", "clicked", "videoStart", "videoCompleted", "videoProgressed", "sentToUrl", "adClosed", "adWillClose", "renderFailed", "requestAdComponents", "urlVerified", "capExhausted", "capNotExhausted");
    private static final String b = k.class.getSimpleName();
    private final String c;
    private final boolean d;
    private final long e;
    private final Map<String, String> f;

    public k(String str, boolean z, long j, Map<String, String> map) {
        if (!a.contains(str)) {
            ja.a(b, "AdEvent initialized with unrecognized type: " + str);
        }
        this.c = str;
        this.d = z;
        this.e = j;
        if (map == null) {
            this.f = new HashMap();
        } else {
            this.f = map;
        }
    }

    public k(DataInput dataInput) throws IOException {
        this.c = dataInput.readUTF();
        this.d = dataInput.readBoolean();
        this.e = dataInput.readLong();
        this.f = new HashMap();
        short readShort = dataInput.readShort();
        for (short s = 0; s < readShort; s = (short) (s + 1)) {
            this.f.put(dataInput.readUTF(), dataInput.readUTF());
        }
    }

    public void a(DataOutput dataOutput) throws IOException {
        dataOutput.writeUTF(this.c);
        dataOutput.writeBoolean(this.d);
        dataOutput.writeLong(this.e);
        dataOutput.writeShort(this.f.size());
        for (Map.Entry next : this.f.entrySet()) {
            dataOutput.writeUTF((String) next.getKey());
            dataOutput.writeUTF((String) next.getValue());
        }
    }

    public String a() {
        return this.c;
    }

    public boolean b() {
        return this.d;
    }

    public long c() {
        return this.e;
    }

    public Map<String, String> d() {
        return this.f;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof k)) {
            return false;
        }
        k kVar = (k) obj;
        return TextUtils.equals(this.c, kVar.c) && this.d == kVar.d && this.e == kVar.e && (this.f == kVar.f || (this.f != null && this.f.equals(kVar.f)));
    }

    public int hashCode() {
        int i = 17;
        if (this.c != null) {
            i = 17 | this.c.hashCode();
        }
        if (this.d) {
            i |= 1;
        }
        int i2 = (int) (((long) i) | this.e);
        if (this.f != null) {
            return i2 | this.f.hashCode();
        }
        return i2;
    }
}
