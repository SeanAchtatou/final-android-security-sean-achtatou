package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import android.view.ViewGroup;
import com.flurry.android.FlurryAdListener;
import com.flurry.android.FlurryAdSize;
import com.flurry.android.FlurryAdType;
import com.flurry.android.impl.ads.FlurryAdModule;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class bw {
    private static final String a = bw.class.getSimpleName();
    private aq b = new aq();
    private ad c = new ad();
    private FlurryAdModule d;

    public bw(FlurryAdModule flurryAdModule) {
        this.d = flurryAdModule;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.bw.a(java.lang.String, android.view.ViewGroup, boolean, com.flurry.android.FlurryAdSize, com.flurry.android.monolithic.sdk.impl.jf):com.flurry.android.monolithic.sdk.impl.br
     arg types: [java.lang.String, android.view.ViewGroup, int, com.flurry.android.FlurryAdSize, com.flurry.android.monolithic.sdk.impl.bx]
     candidates:
      com.flurry.android.monolithic.sdk.impl.bw.a(android.content.Context, java.lang.String, com.flurry.android.FlurryAdSize, android.view.ViewGroup, long):boolean
      com.flurry.android.monolithic.sdk.impl.bw.a(java.lang.String, android.view.ViewGroup, boolean, com.flurry.android.FlurryAdSize, com.flurry.android.monolithic.sdk.impl.jf):com.flurry.android.monolithic.sdk.impl.br */
    public boolean a(Context context, String str, FlurryAdSize flurryAdSize, ViewGroup viewGroup, long j) {
        FlurryAdListener K;
        bx bxVar;
        if (!e(str)) {
            if (b() && !c()) {
                ja.a(3, a, "getAd:no ads in cache, starting AsyncTask");
                if (0 == j) {
                    bxVar = new bx(this, context, str, viewGroup);
                } else {
                    bxVar = null;
                }
                br a2 = a(str, viewGroup, false, flurryAdSize, (jf) bxVar);
                if (j > 0) {
                    try {
                        a2.a(j, TimeUnit.MILLISECONDS);
                        if (e(str)) {
                            ja.a(3, a, "getAd:cache has ads now, rendering");
                            return b(context, str, viewGroup);
                        }
                    } catch (InterruptedException e) {
                        ja.a(3, a, "AsyncTask timeout InterruptedException");
                        e.printStackTrace();
                        return false;
                    } catch (ExecutionException e2) {
                        ja.a(3, a, "AsyncTask timeout ExecutionException");
                        e2.printStackTrace();
                        return false;
                    } catch (TimeoutException e3) {
                        ja.a(3, a, "getAd:AsyncAdTask timeout TimeoutException. No ads retrieved within the timeout");
                        return false;
                    }
                }
            } else if (!b() && (K = this.d.K()) != null) {
                K.spaceDidFailToReceiveAd(str.toString());
            }
            return false;
        } else if (!c()) {
            return b(context, str, viewGroup);
        } else {
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.bw.a(java.lang.String, android.view.ViewGroup, boolean, com.flurry.android.FlurryAdSize, com.flurry.android.monolithic.sdk.impl.jf):com.flurry.android.monolithic.sdk.impl.br
     arg types: [java.lang.String, android.view.ViewGroup, int, com.flurry.android.FlurryAdSize, com.flurry.android.monolithic.sdk.impl.by]
     candidates:
      com.flurry.android.monolithic.sdk.impl.bw.a(android.content.Context, java.lang.String, com.flurry.android.FlurryAdSize, android.view.ViewGroup, long):boolean
      com.flurry.android.monolithic.sdk.impl.bw.a(java.lang.String, android.view.ViewGroup, boolean, com.flurry.android.FlurryAdSize, com.flurry.android.monolithic.sdk.impl.jf):com.flurry.android.monolithic.sdk.impl.br */
    public void a(Context context, String str, ViewGroup viewGroup, FlurryAdSize flurryAdSize) {
        if (!b()) {
            ja.a(5, a, "There is no network connectivity (ad will not Fetch)");
            FlurryAdListener K = this.d.K();
            if (K != null) {
                K.spaceDidFailToReceiveAd(str.toString());
            }
        } else if (!e(str) || c()) {
            a(str, viewGroup, false, flurryAdSize, (jf) new by(this, context, str));
        } else {
            b(context, str);
        }
    }

    public boolean a(Context context, String str, ViewGroup viewGroup) {
        if (!b()) {
            ja.a(5, a, "There is no network connectivity (ad will not Display)");
            FlurryAdListener K = this.d.K();
            if (K != null) {
                K.spaceDidFailToReceiveAd(str.toString());
            }
            return false;
        }
        cl c2 = c(str);
        if (c2 == null) {
            return false;
        }
        AdUnit b2 = c2.b();
        FlurryAdListener K2 = this.d.K();
        FlurryAdType flurryAdType = c2 instanceof af ? FlurryAdType.WEB_BANNER : FlurryAdType.WEB_TAKEOVER;
        if (K2 == null || K2.shouldDisplayAd(str, flurryAdType)) {
            this.d.a(context, b2, c2);
            c2.a(context, viewGroup);
            return true;
        }
        a(str, c2);
        return false;
    }

    public void a(Context context, String str) {
        ja.a(3, a, "_removeAd(context = " + context + ", adSpaceName = " + str + ")");
        an a2 = this.b.a(str);
        if (a2 == null) {
            d(str);
            return;
        }
        a2.b();
        ViewGroup viewGroup = a2.getViewGroup();
        if (viewGroup != null) {
            a2.removeAllViews();
            viewGroup.removeView(a2);
        }
        d(str);
        this.b.a(a2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.bw.a(java.lang.String, android.view.ViewGroup, boolean, com.flurry.android.FlurryAdSize, com.flurry.android.monolithic.sdk.impl.jf):com.flurry.android.monolithic.sdk.impl.br
     arg types: [java.lang.String, ?[OBJECT, ARRAY], int, com.flurry.android.FlurryAdSize, ?[OBJECT, ARRAY]]
     candidates:
      com.flurry.android.monolithic.sdk.impl.bw.a(android.content.Context, java.lang.String, com.flurry.android.FlurryAdSize, android.view.ViewGroup, long):boolean
      com.flurry.android.monolithic.sdk.impl.bw.a(java.lang.String, android.view.ViewGroup, boolean, com.flurry.android.FlurryAdSize, com.flurry.android.monolithic.sdk.impl.jf):com.flurry.android.monolithic.sdk.impl.br */
    public boolean a(String str, FlurryAdSize flurryAdSize, long j) {
        if (e(str)) {
            return true;
        }
        ja.a(3, a, "isAdAvailable: no ads in cache, starting AsyncTask");
        try {
            a(str, (ViewGroup) null, false, flurryAdSize, (jf) null).a(j, TimeUnit.MILLISECONDS);
            if (!e(str)) {
                return false;
            }
            ja.a(3, a, "getAd:cache has ads now, rendering");
            return true;
        } catch (InterruptedException e) {
            ja.a(3, a, "AsyncTask timeout InterruptedException");
            e.printStackTrace();
            return false;
        } catch (ExecutionException e2) {
            ja.a(3, a, "AsyncTask timeout ExecutionException");
            e2.printStackTrace();
            return false;
        } catch (TimeoutException e3) {
            ja.a(3, a, "isAdAvailable:AsyncAdTask timeout TimeoutException. No ads retrieved within the timeout");
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean b(Context context, String str) {
        AdUnit g = g(str);
        if (g == null) {
            this.d.F();
            FlurryAdListener K = this.d.K();
            if (K != null) {
                K.spaceDidFailToReceiveAd(str.toString());
            }
            return false;
        }
        this.d.J();
        a(str, this.d.a(context, g));
        FlurryAdListener K2 = this.d.K();
        if (K2 != null) {
            K2.spaceDidReceiveAd(str.toString());
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean b(Context context, String str, ViewGroup viewGroup) {
        if (!b(context, str)) {
            return false;
        }
        a(context, str, viewGroup);
        return true;
    }

    public boolean a(Context context) {
        int i = 0;
        for (an next : this.b.a(context)) {
            next.b();
            next.removeAllViews();
            ViewGroup viewGroup = next.getViewGroup();
            if (viewGroup != null) {
                viewGroup.removeView(next);
            }
            this.b.a(next);
            i++;
        }
        if (i > 0) {
            return true;
        }
        return false;
    }

    public an a(FlurryAdModule flurryAdModule, Context context, ViewGroup viewGroup, String str) {
        return this.b.a(flurryAdModule, context, viewGroup, str);
    }

    public an a(String str) {
        return this.b.a(str);
    }

    public boolean b(String str) {
        return this.b.d(str);
    }

    /* access modifiers changed from: package-private */
    public void a(String str, cl clVar) {
        this.b.a(str, clVar);
    }

    /* access modifiers changed from: package-private */
    public cl c(String str) {
        return this.b.b(str);
    }

    /* access modifiers changed from: package-private */
    public void d(String str) {
        this.b.c(str);
    }

    /* access modifiers changed from: package-private */
    public void a(List<AdUnit> list) {
        this.c.a(list);
    }

    /* access modifiers changed from: package-private */
    public boolean e(String str) {
        return this.c.d(str);
    }

    public void f(String str) {
        this.c.a(str, this.c.e(str));
    }

    /* access modifiers changed from: package-private */
    public void a(String str, int i) {
        this.c.a(str, i);
    }

    /* access modifiers changed from: package-private */
    public void a(String str, String str2) {
        this.c.a(str, str2);
    }

    /* access modifiers changed from: package-private */
    public AdUnit g(String str) {
        return this.c.c(str);
    }

    public List<AdUnit> a(String str, int i, int i2) {
        return this.c.a(str, i, i2);
    }

    /* access modifiers changed from: package-private */
    public ad a() {
        return this.c;
    }

    public void h(String str) {
        this.c.a(str);
        ja.a(3, a, "fetchAdTaskExecute :setting cache request limit count");
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return jc.a().c();
    }

    public boolean c() {
        return FlurryAdModule.e();
    }

    /* access modifiers changed from: package-private */
    public br a(String str, ViewGroup viewGroup, boolean z, FlurryAdSize flurryAdSize, jf jfVar) {
        FlurryAdModule flurryAdModule = this.d;
        flurryAdModule.getClass();
        br brVar = new br(flurryAdModule, str, viewGroup, z, flurryAdSize, jfVar);
        brVar.a();
        return brVar;
    }
}
