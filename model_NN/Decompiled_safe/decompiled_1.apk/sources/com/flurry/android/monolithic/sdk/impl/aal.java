package com.flurry.android.monolithic.sdk.impl;

public abstract class aal {
    public abstract aal a(Class<?> cls, ra<Object> raVar);

    public abstract ra<Object> a(Class<?> cls);

    public final aap a(Class<?> cls, ru ruVar, qc qcVar) throws qw {
        ra<Object> a = ruVar.a(cls, qcVar);
        return new aap(a, a(cls, a));
    }

    public final aap a(afm afm, ru ruVar, qc qcVar) throws qw {
        ra<Object> a = ruVar.a(afm, qcVar);
        return new aap(a, a(afm.p(), a));
    }

    public static aal a() {
        return aan.a;
    }
}
