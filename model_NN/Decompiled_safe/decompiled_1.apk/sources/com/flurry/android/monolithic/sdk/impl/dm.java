package com.flurry.android.monolithic.sdk.impl;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import com.flurry.android.AdCreative;
import com.flurry.android.impl.ads.FlurryAdModule;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;
import java.util.ArrayList;
import java.util.List;

public final class dm extends cy {
    /* access modifiers changed from: protected */
    public String f() {
        return "InMobi";
    }

    /* access modifiers changed from: protected */
    public List<cu> g() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new cu("InMobiAndroidSDK", "3.5.0", "com.inmobi.androidsdk.IMAdInterstitial"));
        return arrayList;
    }

    /* access modifiers changed from: protected */
    @TargetApi(13)
    public List<ActivityInfo> j() {
        ArrayList arrayList = new ArrayList();
        ActivityInfo activityInfo = new ActivityInfo();
        activityInfo.name = "com.inmobi.androidsdk.IMBrowserActivity";
        activityInfo.configChanges = 3248;
        arrayList.add(activityInfo);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public List<cu> k() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new cu("InMobiAndroidSDK", "3.5.0", "com.inmobi.androidsdk.IMAdView"));
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public List<String> n() {
        ArrayList arrayList = new ArrayList();
        arrayList.add("com.flurry.inmobi.MY_APP_ID");
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public List<String> o() {
        ArrayList arrayList = new ArrayList();
        arrayList.add("android.permission.INTERNET");
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public cn a(Context context, FlurryAdModule flurryAdModule, m mVar, AdUnit adUnit, Bundle bundle) {
        if (context == null || flurryAdModule == null || mVar == null || adUnit == null || bundle == null) {
            return null;
        }
        return new dp(context, flurryAdModule, mVar, adUnit, bundle);
    }

    /* access modifiers changed from: protected */
    public ac a(Context context, FlurryAdModule flurryAdModule, m mVar, AdCreative adCreative, Bundle bundle) {
        if (context == null || flurryAdModule == null || mVar == null || adCreative == null || bundle == null) {
            return null;
        }
        return new dn(context, flurryAdModule, mVar, adCreative, bundle);
    }
}
