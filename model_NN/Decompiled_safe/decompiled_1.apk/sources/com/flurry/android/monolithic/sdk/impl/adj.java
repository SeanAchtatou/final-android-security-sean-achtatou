package com.flurry.android.monolithic.sdk.impl;

import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;

public class adj {
    public static final afm a = new adh(Object.class);
    private static final afm[] g = new afm[0];
    protected final adk b;
    protected final afm c;
    protected final Class<?> d;
    protected Map<String, afm> e;
    protected HashSet<String> f;
    private final adj h;

    public adj(adk adk, Class<?> cls) {
        this(adk, null, cls, null);
    }

    public adj(adk adk, afm afm) {
        this(adk, null, afm.p(), afm);
    }

    public adj a() {
        return new adj(this.b, this, this.d, this.c);
    }

    private adj(adk adk, adj adj, Class<?> cls, afm afm) {
        this.b = adk;
        this.h = adj;
        this.d = cls;
        this.c = afm;
    }

    public afm a(Type type) {
        return this.b.b(type, this);
    }

    public afm a(String str) {
        String str2;
        if (this.e == null) {
            c();
        }
        afm afm = this.e.get(str);
        if (afm != null) {
            return afm;
        }
        if (this.f != null && this.f.contains(str)) {
            return a;
        }
        if (this.h != null) {
            return this.h.a(str);
        }
        if (this.d != null && this.d.getEnclosingClass() != null && !Modifier.isStatic(this.d.getModifiers())) {
            return a;
        }
        if (this.d != null) {
            str2 = this.d.getName();
        } else if (this.c != null) {
            str2 = this.c.toString();
        } else {
            str2 = "UNKNOWN";
        }
        throw new IllegalArgumentException("Type variable '" + str + "' can not be resolved (with context of class " + str2 + ")");
    }

    public void a(String str, afm afm) {
        if (this.e == null || this.e.size() == 0) {
            this.e = new LinkedHashMap();
        }
        this.e.put(str, afm);
    }

    public afm[] b() {
        if (this.e == null) {
            c();
        }
        if (this.e.size() == 0) {
            return g;
        }
        return (afm[]) this.e.values().toArray(new afm[this.e.size()]);
    }

    /* access modifiers changed from: protected */
    public void c() {
        int h2;
        b(this.d);
        if (this.c != null && (h2 = this.c.h()) > 0) {
            if (this.e == null) {
                this.e = new LinkedHashMap();
            }
            for (int i = 0; i < h2; i++) {
                this.e.put(this.c.a(i), this.c.b(i));
            }
        }
        if (this.e == null) {
            this.e = Collections.emptyMap();
        }
    }

    public void b(String str) {
        if (this.f == null) {
            this.f = new HashSet<>();
        }
        this.f.add(str);
    }

    /* access modifiers changed from: protected */
    public void b(Type type) {
        Class cls;
        if (type != null) {
            if (type instanceof ParameterizedType) {
                ParameterizedType parameterizedType = (ParameterizedType) type;
                Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
                if (actualTypeArguments != null && actualTypeArguments.length > 0) {
                    Class cls2 = (Class) parameterizedType.getRawType();
                    TypeVariable[] typeParameters = cls2.getTypeParameters();
                    if (typeParameters.length != actualTypeArguments.length) {
                        throw new IllegalArgumentException("Strange parametrized type (in class " + cls2.getName() + "): number of type arguments != number of type parameters (" + actualTypeArguments.length + " vs " + typeParameters.length + ")");
                    }
                    int length = actualTypeArguments.length;
                    for (int i = 0; i < length; i++) {
                        String name = typeParameters[i].getName();
                        if (this.e == null) {
                            this.e = new LinkedHashMap();
                        } else if (this.e.containsKey(name)) {
                        }
                        b(name);
                        this.e.put(name, this.b.b(actualTypeArguments[i], this));
                    }
                }
                cls = (Class) parameterizedType.getRawType();
            } else if (type instanceof Class) {
                Class cls3 = (Class) type;
                b(cls3.getDeclaringClass());
                TypeVariable[] typeParameters2 = cls3.getTypeParameters();
                if (typeParameters2 != null && typeParameters2.length > 0) {
                    afm[] afmArr = null;
                    if (this.c != null && cls3.isAssignableFrom(this.c.p())) {
                        afmArr = this.b.b(this.c, cls3);
                    }
                    for (int i2 = 0; i2 < typeParameters2.length; i2++) {
                        TypeVariable typeVariable = typeParameters2[i2];
                        String name2 = typeVariable.getName();
                        Type type2 = typeVariable.getBounds()[0];
                        if (type2 != null) {
                            if (this.e == null) {
                                this.e = new LinkedHashMap();
                            } else if (this.e.containsKey(name2)) {
                            }
                            b(name2);
                            if (afmArr != null) {
                                this.e.put(name2, afmArr[i2]);
                            } else {
                                this.e.put(name2, this.b.b(type2, this));
                            }
                        }
                    }
                }
                cls = cls3;
            } else {
                return;
            }
            b(cls.getGenericSuperclass());
            for (Type b2 : cls.getGenericInterfaces()) {
                b(b2);
            }
        }
    }

    public String toString() {
        if (this.e == null) {
            c();
        }
        StringBuilder sb = new StringBuilder("[TypeBindings for ");
        if (this.c != null) {
            sb.append(this.c.toString());
        } else {
            sb.append(this.d.getName());
        }
        sb.append(": ").append(this.e).append("]");
        return sb.toString();
    }
}
