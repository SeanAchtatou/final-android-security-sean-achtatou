package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
public final class acc extends abw<int[]> {
    public acc() {
        super(int[].class, null, null);
    }

    public abc<?> a(rx rxVar) {
        return this;
    }

    /* renamed from: a */
    public void b(int[] iArr, or orVar, ru ruVar) throws IOException, oq {
        for (int b : iArr) {
            orVar.b(b);
        }
    }
}
