package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import java.util.List;

public final class da implements cx {
    private static final String a = da.class.getSimpleName();

    public boolean a(Context context, db dbVar) {
        boolean z;
        if (dbVar == null) {
            return false;
        }
        String a2 = dbVar.a();
        if (TextUtils.isEmpty(a2)) {
            return false;
        }
        List<String> d = dbVar.d();
        if (d == null) {
            return false;
        }
        boolean z2 = true;
        PackageManager packageManager = context.getPackageManager();
        String packageName = context.getPackageName();
        for (String a3 : d) {
            if (!a(a2, packageManager, packageName, a3)) {
                z = false;
            } else {
                z = z2;
            }
            z2 = z;
        }
        return z2;
    }

    private boolean a(String str, PackageManager packageManager, String str2, String str3) {
        if (TextUtils.isEmpty(str) || packageManager == null || TextUtils.isEmpty(str2) || TextUtils.isEmpty(str3)) {
            return false;
        }
        if (-1 == packageManager.checkPermission(str3, str2)) {
            ja.b(a, str + ": package=\"" + str2 + "\": AndroidManifest.xml should include uses-permission node with " + "android:name=\"" + str3 + "\"");
            return false;
        }
        ja.a(3, a, str + ": package=\"" + str2 + "\": AndroidManifest.xml has uses-permission node with " + "android:name=\"" + str3 + "\"");
        return true;
    }
}
