package com.flurry.android.monolithic.sdk.impl;

public final class cu {
    private final String a;
    private final String b;
    private final String c;

    public cu(String str, String str2, String str3) {
        this.a = str;
        this.b = str2;
        this.c = str3;
    }

    public String a() {
        return this.a;
    }

    public String b() {
        return this.b;
    }

    public String c() {
        return this.c;
    }
}
