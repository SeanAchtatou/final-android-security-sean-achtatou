package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import com.flurry.android.AdCreative;
import com.flurry.android.impl.ads.FlurryAdModule;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;
import java.util.Collections;
import java.util.List;

public abstract class cy implements am, co {
    /* access modifiers changed from: protected */
    public abstract ac a(Context context, FlurryAdModule flurryAdModule, m mVar, AdCreative adCreative, Bundle bundle);

    /* access modifiers changed from: protected */
    public abstract cn a(Context context, FlurryAdModule flurryAdModule, m mVar, AdUnit adUnit, Bundle bundle);

    /* access modifiers changed from: protected */
    public abstract String f();

    /* access modifiers changed from: protected */
    public abstract List<cu> g();

    /* access modifiers changed from: protected */
    public abstract List<ActivityInfo> j();

    /* access modifiers changed from: protected */
    public abstract List<cu> k();

    /* access modifiers changed from: protected */
    public abstract List<String> n();

    /* access modifiers changed from: protected */
    public abstract List<String> o();

    public cn a_(Context context, FlurryAdModule flurryAdModule, m mVar, AdUnit adUnit) {
        if (context == null || flurryAdModule == null || mVar == null || adUnit == null) {
            return null;
        }
        if (!a(context, d())) {
            return null;
        }
        Bundle b = b(context, flurryAdModule, mVar, adUnit);
        if (b == null) {
            return null;
        }
        return a(context, flurryAdModule, mVar, adUnit, b);
    }

    public ac a(Context context, FlurryAdModule flurryAdModule, m mVar, AdUnit adUnit) {
        if (context == null || flurryAdModule == null || mVar == null || adUnit == null) {
            return null;
        }
        if (!b(context, e())) {
            return null;
        }
        Bundle c = c(context, flurryAdModule, mVar, adUnit);
        if (c == null) {
            return null;
        }
        AdCreative a = ab.a(adUnit);
        if (a == null) {
            return null;
        }
        return a(context, flurryAdModule, mVar, a, c);
    }

    /* access modifiers changed from: protected */
    public boolean a(Context context, db dbVar) {
        if (context == null || dbVar == null) {
            return false;
        }
        cx a = a();
        if (a == null) {
            return false;
        }
        return a.a(context, dbVar);
    }

    /* access modifiers changed from: protected */
    public boolean b(Context context, db dbVar) {
        if (context == null || dbVar == null) {
            return false;
        }
        cx b = b();
        if (b == null) {
            return false;
        }
        return b.a(context, dbVar);
    }

    /* access modifiers changed from: protected */
    public cx a() {
        return c();
    }

    /* access modifiers changed from: protected */
    public cx b() {
        return c();
    }

    /* access modifiers changed from: protected */
    public cx c() {
        return new cw();
    }

    /* access modifiers changed from: protected */
    public db d() {
        return new db(f(), g(), h(), i(), j());
    }

    /* access modifiers changed from: protected */
    public db e() {
        return new db(f(), k(), l(), m(), Collections.emptyList());
    }

    /* access modifiers changed from: protected */
    public List<String> h() {
        return n();
    }

    /* access modifiers changed from: protected */
    public List<String> i() {
        return o();
    }

    /* access modifiers changed from: protected */
    public List<String> l() {
        return n();
    }

    /* access modifiers changed from: protected */
    public List<String> m() {
        return o();
    }

    /* access modifiers changed from: protected */
    public Bundle b(Context context, FlurryAdModule flurryAdModule, m mVar, AdUnit adUnit) {
        return d(context, flurryAdModule, mVar, adUnit);
    }

    /* access modifiers changed from: protected */
    public Bundle c(Context context, FlurryAdModule flurryAdModule, m mVar, AdUnit adUnit) {
        return d(context, flurryAdModule, mVar, adUnit);
    }

    /* access modifiers changed from: protected */
    public Bundle d(Context context, FlurryAdModule flurryAdModule, m mVar, AdUnit adUnit) {
        return il.e(context);
    }
}
