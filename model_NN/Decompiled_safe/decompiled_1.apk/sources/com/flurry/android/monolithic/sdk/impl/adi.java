package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public abstract class adi extends afm implements qz {
    volatile String c;

    /* access modifiers changed from: protected */
    public abstract String a();

    protected adi(Class<?> cls, int i, Object obj, Object obj2) {
        super(cls, i);
        this.f = obj;
        this.g = obj2;
    }

    public String m() {
        String str = this.c;
        if (str == null) {
            return a();
        }
        return str;
    }

    public <T> T n() {
        return this.f;
    }

    public <T> T o() {
        return this.g;
    }

    public void a(or orVar, ru ruVar, rx rxVar) throws IOException, oz {
        rxVar.a(this, orVar);
        a(orVar, ruVar);
        rxVar.d(this, orVar);
    }

    public void a(or orVar, ru ruVar) throws IOException, oz {
        orVar.b(m());
    }
}
