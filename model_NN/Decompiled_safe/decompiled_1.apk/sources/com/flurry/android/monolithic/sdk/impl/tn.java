package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public class tn extends sw {
    protected final xn i;
    protected final Object j;

    public tn(String str, afm afm, rw rwVar, ado ado, xn xnVar, int i2, Object obj) {
        super(str, afm, rwVar, ado);
        this.i = xnVar;
        this.h = i2;
        this.j = obj;
    }

    protected tn(tn tnVar, qu<Object> quVar) {
        super(tnVar, quVar);
        this.i = tnVar.i;
        this.j = tnVar.j;
    }

    /* renamed from: b */
    public tn a(qu<Object> quVar) {
        return new tn(this, quVar);
    }

    public xk b() {
        return this.i;
    }

    public void a(ow owVar, qm qmVar, Object obj) throws IOException, oz {
        a(obj, a(owVar, qmVar));
    }

    public void a(Object obj, Object obj2) throws IOException {
    }

    public Object k() {
        return this.j;
    }
}
