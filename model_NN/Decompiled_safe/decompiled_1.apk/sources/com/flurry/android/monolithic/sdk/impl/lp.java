package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.io.InputStream;

abstract class lp extends InputStream {
    protected ln a;

    /* access modifiers changed from: protected */
    public abstract void a(long j) throws IOException;

    /* access modifiers changed from: protected */
    public abstract void a(byte[] bArr, int i, int i2) throws IOException;

    /* access modifiers changed from: protected */
    public abstract int b(byte[] bArr, int i, int i2) throws IOException;

    /* access modifiers changed from: protected */
    public abstract long b(long j) throws IOException;

    protected lp() {
    }

    /* access modifiers changed from: protected */
    public void a(int i, ll llVar) {
        byte[] unused = llVar.b = new byte[i];
        int unused2 = llVar.d = 0;
        int unused3 = llVar.c = 0;
        int unused4 = llVar.e = 0;
        this.a = new ln(llVar);
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.a.a();
    }

    /* access modifiers changed from: protected */
    public void a(byte[] bArr, int i, int i2, int i3) throws IOException {
        System.arraycopy(bArr, i, bArr, i2, i3);
        this.a.a(i2);
        this.a.b(b(bArr, i2 + i3, bArr.length - i3) + i3);
    }

    public int read(byte[] bArr, int i, int i2) throws IOException {
        int c = this.a.c();
        int b = this.a.b();
        byte[] d = this.a.d();
        int i3 = c - b;
        if (i3 >= i2) {
            System.arraycopy(d, b, bArr, i, i2);
            this.a.a(b + i2);
            return i2;
        }
        System.arraycopy(d, b, bArr, i, i3);
        this.a.a(b + i3);
        int b2 = i3 + b(bArr, i + i3, i2 - i3);
        if (b2 == 0) {
            return -1;
        }
        return b2;
    }

    public long skip(long j) throws IOException {
        int c = this.a.c();
        int b = this.a.b();
        int i = c - b;
        if (((long) i) > j) {
            this.a.a((int) (((long) b) + j));
            return j;
        }
        this.a.a(c);
        return b(j - ((long) i)) + ((long) i);
    }

    public int available() throws IOException {
        return this.a.c() - this.a.b();
    }
}
