package com.flurry.android.monolithic.sdk.impl;

import android.content.pm.ActivityInfo;
import java.util.List;

public final class db {
    private final String a;
    private final List<cu> b;
    private final List<String> c;
    private final List<String> d;
    private final List<ActivityInfo> e;

    public db(String str, List<cu> list, List<String> list2, List<String> list3, List<ActivityInfo> list4) {
        this.a = str;
        this.b = list;
        this.c = list2;
        this.d = list3;
        this.e = list4;
    }

    public String a() {
        return this.a;
    }

    public List<cu> b() {
        return this.b;
    }

    public List<String> c() {
        return this.c;
    }

    public List<String> d() {
        return this.d;
    }

    public List<ActivityInfo> e() {
        return this.e;
    }
}
