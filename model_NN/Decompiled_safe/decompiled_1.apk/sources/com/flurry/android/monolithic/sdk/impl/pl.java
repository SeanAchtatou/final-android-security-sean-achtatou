package com.flurry.android.monolithic.sdk.impl;

public class pl extends pa {
    protected final pl c;
    protected String d;
    protected pl e = null;

    protected pl(int i, pl plVar) {
        this.a = i;
        this.c = plVar;
        this.b = -1;
    }

    public static pl g() {
        return new pl(0, null);
    }

    private final pl a(int i) {
        this.a = i;
        this.b = -1;
        this.d = null;
        return this;
    }

    public final pl h() {
        pl plVar = this.e;
        if (plVar != null) {
            return plVar.a(1);
        }
        pl plVar2 = new pl(1, this);
        this.e = plVar2;
        return plVar2;
    }

    public final pl i() {
        pl plVar = this.e;
        if (plVar != null) {
            return plVar.a(2);
        }
        pl plVar2 = new pl(2, this);
        this.e = plVar2;
        return plVar2;
    }

    public final pl j() {
        return this.c;
    }

    public final int a(String str) {
        if (this.a != 2) {
            return 4;
        }
        if (this.d != null) {
            return 4;
        }
        this.d = str;
        return this.b < 0 ? 0 : 1;
    }

    public final int k() {
        if (this.a == 2) {
            if (this.d == null) {
                return 5;
            }
            this.d = null;
            this.b++;
            return 2;
        } else if (this.a == 1) {
            int i = this.b;
            this.b++;
            return i < 0 ? 0 : 1;
        } else {
            this.b++;
            return this.b == 0 ? 0 : 3;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(StringBuilder sb) {
        if (this.a == 2) {
            sb.append('{');
            if (this.d != null) {
                sb.append('\"');
                sb.append(this.d);
                sb.append('\"');
            } else {
                sb.append('?');
            }
            sb.append('}');
        } else if (this.a == 1) {
            sb.append('[');
            sb.append(f());
            sb.append(']');
        } else {
            sb.append("/");
        }
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder(64);
        a(sb);
        return sb.toString();
    }
}
