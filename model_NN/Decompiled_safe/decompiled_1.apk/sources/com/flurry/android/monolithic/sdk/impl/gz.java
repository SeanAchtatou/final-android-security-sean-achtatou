package com.flurry.android.monolithic.sdk.impl;

import android.text.TextUtils;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Timer;

public class gz {
    public long a = 30000;
    protected HashMap<String, hb> b = new HashMap<>();
    private String c = null;
    private String d = null;
    private boolean e = false;

    gz(String str) {
        this.c = str;
        this.d = null;
    }

    public int a() {
        return this.b.size();
    }

    /* access modifiers changed from: protected */
    public void a(String str, Long l) {
        c(str);
        if (this.e) {
            hb hbVar = this.b.get(str);
            if (hbVar == null) {
                ja.a(6, "ObserverItem", "scheduleTimerForKey ERROR, info == null");
                return;
            }
            hbVar.a = new Timer();
            hc hcVar = new hc(this, str);
            ja.a(4, "ObserverItem", "schedule");
            hbVar.a.schedule(hcVar, l.longValue());
            this.b.put(str, hbVar);
        }
    }

    private void c(String str) {
        if (this.b.get(str).a != null) {
            hb hbVar = this.b.get(str);
            if (hbVar == null) {
                ja.a(6, "ObserverItem", "stopTimerForKey ERROR, info == null");
                return;
            }
            hbVar.b();
            if (hbVar.b() != null) {
                hbVar.b().cancel();
                hbVar.a(null);
            }
            this.b.put(str, hbVar);
        }
    }

    /* access modifiers changed from: protected */
    public void a(String str, fz fzVar) {
        if (this.b.containsKey(str)) {
            hb hbVar = this.b.get(str);
            if (hbVar == null) {
                ja.a(6, "ObserverItem", "addObserverForKey ERROR, info == null");
                return;
            } else {
                hbVar.c().add(fzVar);
                this.b.put(str, hbVar);
            }
        } else {
            LinkedList linkedList = new LinkedList();
            linkedList.add(fzVar);
            this.b.put(str, new hb(this, linkedList));
        }
        if (this.e) {
            d(str);
        }
    }

    private void d(String str) {
        a(str, Long.valueOf(a(0)));
    }

    public boolean a(fz fzVar) {
        boolean z = false;
        Iterator<Map.Entry<String, hb>> it = this.b.entrySet().iterator();
        while (it.hasNext()) {
            List<fz> c2 = ((hb) it.next().getValue()).c();
            if (c2.contains(fzVar)) {
                c2.remove(fzVar);
                if (c2.size() == 0) {
                    it.remove();
                }
                z = true;
            }
        }
        return z;
    }

    /* access modifiers changed from: protected */
    public boolean a(String str) {
        if (!this.b.containsKey(str)) {
            return false;
        }
        c(str);
        this.b.remove(str);
        return true;
    }

    public void a(String str, String str2) {
        if (this.b.containsKey(str)) {
            for (fz a2 : this.b.get(str).c()) {
                a2.a(str, str2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b(String str) {
        hb hbVar = this.b.get(str);
        if (hbVar == null) {
            ja.a(6, "ObserverItem", "addObserverForKey ERROR, info == null");
            return;
        }
        hbVar.a();
        long a2 = a(hbVar.d());
        this.b.put(str, hbVar);
        a(str, a2);
    }

    private void a(String str, long j) {
        if (TextUtils.isEmpty(this.d)) {
            ft ftVar = new ft();
            ftVar.b(this.c);
            try {
                ftVar.a(new ha(this, str, j));
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean a(fq fqVar, String str) {
        if (fqVar != null) {
            if (fqVar.a()) {
                return true;
            }
            if (fqVar.b().equals("invalid user session")) {
                if (this.b.containsKey(str)) {
                    for (fz a2 : this.b.get(str).c()) {
                        a2.a(new hy(405, "Session not valid, user has to relogin!"));
                    }
                }
                return false;
            }
        }
        return true;
    }

    private long a(int i) {
        if (i == 0) {
            return 0;
        }
        return this.a;
    }

    public void b() {
        this.e = true;
        e();
    }

    private void e() {
        for (Map.Entry<String, hb> key : this.b.entrySet()) {
            d((String) key.getKey());
        }
    }

    private void f() {
        for (Map.Entry<String, hb> key : this.b.entrySet()) {
            c((String) key.getKey());
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.e) {
            e();
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        f();
    }
}
