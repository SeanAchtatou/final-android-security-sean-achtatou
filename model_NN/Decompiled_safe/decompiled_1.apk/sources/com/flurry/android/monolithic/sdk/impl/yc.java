package com.flurry.android.monolithic.sdk.impl;

public class yc extends qe implements Comparable<yc> {
    protected final String a;
    protected final String b;
    protected yd<xj> c;
    protected yd<xn> d;
    protected yd<xl> e;
    protected yd<xl> f;

    public yc(String str) {
        this.b = str;
        this.a = str;
    }

    public yc(yc ycVar, String str) {
        this.b = ycVar.b;
        this.a = str;
        this.c = ycVar.c;
        this.d = ycVar.d;
        this.e = ycVar.e;
        this.f = ycVar.f;
    }

    public yc a(String str) {
        return new yc(this, str);
    }

    /* renamed from: a */
    public int compareTo(yc ycVar) {
        if (this.d != null) {
            if (ycVar.d == null) {
                return -1;
            }
        } else if (ycVar.d != null) {
            return 1;
        }
        return a().compareTo(ycVar.a());
    }

    public String a() {
        return this.a;
    }

    public String l() {
        return this.b;
    }

    public boolean b() {
        return this.e != null;
    }

    public boolean c() {
        return this.f != null;
    }

    public boolean d() {
        return this.c != null;
    }

    public boolean e() {
        return this.d != null;
    }

    public xk j() {
        xl g = g();
        if (g == null) {
            return i();
        }
        return g;
    }

    public xk k() {
        xn m = m();
        if (m != null) {
            return m;
        }
        xl h = h();
        if (h == null) {
            return i();
        }
        return h;
    }

    public xl g() {
        if (this.e == null) {
            return null;
        }
        yd<T> ydVar = this.e.b;
        xl xlVar = (xl) this.e.a;
        while (ydVar != null) {
            xl xlVar2 = (xl) ydVar.a;
            Class<?> h = xlVar.h();
            Class<?> h2 = xlVar2.h();
            if (h != h2) {
                if (!h.isAssignableFrom(h2)) {
                    if (h2.isAssignableFrom(h)) {
                        xlVar2 = xlVar;
                    }
                }
                ydVar = ydVar.b;
                xlVar = xlVar2;
            }
            throw new IllegalArgumentException("Conflicting getter definitions for property \"" + a() + "\": " + xlVar.n() + " vs " + xlVar2.n());
        }
        return xlVar;
    }

    public xl h() {
        if (this.f == null) {
            return null;
        }
        yd<T> ydVar = this.f.b;
        xl xlVar = (xl) this.f.a;
        while (ydVar != null) {
            xl xlVar2 = (xl) ydVar.a;
            Class<?> h = xlVar.h();
            Class<?> h2 = xlVar2.h();
            if (h != h2) {
                if (!h.isAssignableFrom(h2)) {
                    if (h2.isAssignableFrom(h)) {
                        xlVar2 = xlVar;
                    }
                }
                ydVar = ydVar.b;
                xlVar = xlVar2;
            }
            throw new IllegalArgumentException("Conflicting setter definitions for property \"" + a() + "\": " + xlVar.n() + " vs " + xlVar2.n());
        }
        return xlVar;
    }

    public xj i() {
        if (this.c == null) {
            return null;
        }
        yd<T> ydVar = this.c.b;
        xj xjVar = (xj) this.c.a;
        while (ydVar != null) {
            xj xjVar2 = (xj) ydVar.a;
            Class<?> h = xjVar.h();
            Class<?> h2 = xjVar2.h();
            if (h != h2) {
                if (!h.isAssignableFrom(h2)) {
                    if (h2.isAssignableFrom(h)) {
                        xjVar2 = xjVar;
                    }
                }
                ydVar = ydVar.b;
                xjVar = xjVar2;
            }
            throw new IllegalArgumentException("Multiple fields representing property \"" + a() + "\": " + xjVar.f() + " vs " + xjVar2.f());
        }
        return xjVar;
    }

    public xn m() {
        if (this.d == null) {
            return null;
        }
        yd<T> ydVar = this.d;
        while (true) {
            yd<T> ydVar2 = ydVar;
            if (((xn) ydVar2.a).f() instanceof xi) {
                return (xn) ydVar2.a;
            }
            ydVar = ydVar2.b;
            if (ydVar == null) {
                return (xn) this.d.a;
            }
        }
    }

    public void a(xj xjVar, String str, boolean z, boolean z2) {
        this.c = new yd<>(xjVar, this.c, str, z, z2);
    }

    public void a(xn xnVar, String str, boolean z, boolean z2) {
        this.d = new yd<>(xnVar, this.d, str, z, z2);
    }

    public void a(xl xlVar, String str, boolean z, boolean z2) {
        this.e = new yd<>(xlVar, this.e, str, z, z2);
    }

    public void b(xl xlVar, String str, boolean z, boolean z2) {
        this.f = new yd<>(xlVar, this.f, str, z, z2);
    }

    public void b(yc ycVar) {
        this.c = a(this.c, ycVar.c);
        this.d = a(this.d, ycVar.d);
        this.e = a(this.e, ycVar.e);
        this.f = a(this.f, ycVar.f);
    }

    private static <T> yd<T> a(yd<T> ydVar, yd<T> ydVar2) {
        if (ydVar == null) {
            return ydVar2;
        }
        if (ydVar2 == null) {
            return ydVar;
        }
        return ydVar.b(ydVar2);
    }

    public void n() {
        this.c = a(this.c);
        this.e = a(this.e);
        this.f = a(this.f);
        this.d = a(this.d);
    }

    public void o() {
        this.e = b(this.e);
        this.d = b(this.d);
        if (this.e == null) {
            this.c = b(this.c);
            this.f = b(this.f);
        }
    }

    public void p() {
        this.c = c(this.c);
        this.e = c(this.e);
        this.f = c(this.f);
        this.d = c(this.d);
    }

    public void a(boolean z) {
        if (z) {
            if (this.e != null) {
                this.e = this.e.a(((xl) this.e.a).a(a(0, this.e, this.c, this.d, this.f)));
            } else if (this.c != null) {
                this.c = this.c.a((xl) ((xj) this.c.a).a(a(0, this.c, this.d, this.f)));
            }
        } else if (this.d != null) {
            this.d = this.d.a((xl) ((xn) this.d.a).a(a(0, this.d, this.f, this.c, this.e)));
        } else if (this.f != null) {
            this.f = this.f.a(((xl) this.f.a).a(a(0, this.f, this.c, this.e)));
        } else if (this.c != null) {
            this.c = this.c.a((xl) ((xj) this.c.a).a(a(0, this.c, this.e)));
        }
    }

    private xp a(int i, yd<? extends xk>... ydVarArr) {
        xp j = ((xk) ydVarArr[i].a).j();
        for (int i2 = i + 1; i2 < ydVarArr.length; i2++) {
            if (ydVarArr[i2] != null) {
                return xp.a(j, a(i2, ydVarArr));
            }
        }
        return j;
    }

    private <T> yd<T> a(yd ydVar) {
        if (ydVar == null) {
            return ydVar;
        }
        return ydVar.a();
    }

    private <T> yd<T> b(yd ydVar) {
        if (ydVar == null) {
            return ydVar;
        }
        return ydVar.b();
    }

    private <T> yd<T> c(yd<T> ydVar) {
        if (ydVar == null) {
            return ydVar;
        }
        return ydVar.c();
    }

    public boolean q() {
        return d(this.c) || d(this.e) || d(this.f) || d(this.d);
    }

    private <T> boolean d(yd<T> ydVar) {
        for (yd<T> ydVar2 = ydVar; ydVar2 != null; ydVar2 = ydVar2.b) {
            if (ydVar2.c != null && ydVar2.c.length() > 0) {
                return true;
            }
        }
        return false;
    }

    public boolean r() {
        return e(this.c) || e(this.e) || e(this.f) || e(this.d);
    }

    private <T> boolean e(yd<T> ydVar) {
        for (yd<T> ydVar2 = ydVar; ydVar2 != null; ydVar2 = ydVar2.b) {
            if (ydVar2.d) {
                return true;
            }
        }
        return false;
    }

    public boolean s() {
        return f(this.c) || f(this.e) || f(this.f) || f(this.d);
    }

    public boolean t() {
        return f(this.c) || f(this.f) || f(this.d);
    }

    private <T> boolean f(yd<T> ydVar) {
        for (yd<T> ydVar2 = ydVar; ydVar2 != null; ydVar2 = ydVar2.b) {
            if (ydVar2.e) {
                return true;
            }
        }
        return false;
    }

    public String u() {
        yd<? extends xk> b2 = b(this.d, b(this.f, b(this.e, b(this.c, null))));
        if (b2 == null) {
            return null;
        }
        return b2.c;
    }

    private yd<? extends xk> b(yd<? extends xk> ydVar, yd<? extends xk> ydVar2) {
        yd<? extends xk> ydVar3 = ydVar2;
        for (yd<? extends xk> ydVar4 = ydVar; ydVar4 != null; ydVar4 = ydVar4.b) {
            String str = ydVar4.c;
            if (str != null && !str.equals(this.a)) {
                if (ydVar3 == null) {
                    ydVar3 = ydVar4;
                } else if (!str.equals(ydVar3.c)) {
                    throw new IllegalStateException("Conflicting property name definitions: '" + ydVar3.c + "' (for " + ((Object) ydVar3.a) + ") vs '" + ydVar4.c + "' (for " + ((Object) ydVar4.a) + ")");
                }
            }
        }
        return ydVar3;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[Property '").append(this.a).append("'; ctors: ").append(this.d).append(", field(s): ").append(this.c).append(", getter(s): ").append(this.e).append(", setter(s): ").append(this.f);
        sb.append("]");
        return sb.toString();
    }
}
