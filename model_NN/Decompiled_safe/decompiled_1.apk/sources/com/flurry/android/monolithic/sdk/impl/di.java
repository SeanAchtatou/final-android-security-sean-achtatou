package com.flurry.android.monolithic.sdk.impl;

import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import java.util.Collections;

final class di implements AdListener {
    final /* synthetic */ dg a;

    private di(dg dgVar) {
        this.a = dgVar;
    }

    public void onReceiveAd(Ad ad) {
        this.a.onAdShown(Collections.emptyMap());
        ja.a(4, dg.a, "Admob AdView received ad.");
    }

    public void onFailedToReceiveAd(Ad ad, AdRequest.ErrorCode errorCode) {
        this.a.onRenderFailed(Collections.emptyMap());
        ja.a(5, dg.a, "Admob AdView failed to receive ad.");
    }

    public void onPresentScreen(Ad ad) {
        ja.a(4, dg.a, "Admob AdView present on screen.");
    }

    public void onDismissScreen(Ad ad) {
        this.a.onAdClosed(Collections.emptyMap());
        ja.a(4, dg.a, "Admob AdView dismissed from screen.");
    }

    public void onLeaveApplication(Ad ad) {
        this.a.onAdClicked(Collections.emptyMap());
        ja.a(4, dg.a, "Admob AdView leave application.");
    }
}
