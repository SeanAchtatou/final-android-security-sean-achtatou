package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.Date;
import v2.com.playhaven.requests.base.PHAsyncRequest;

public abstract class vo<T> extends qu<T> {
    protected final Class<?> q;

    protected vo(Class<?> cls) {
        this.q = cls;
    }

    protected vo(afm afm) {
        this.q = afm == null ? null : afm.p();
    }

    public Class<?> f() {
        return this.q;
    }

    /* access modifiers changed from: protected */
    public boolean a(qu<?> quVar) {
        return (quVar == null || quVar.getClass().getAnnotation(rz.class) == null) ? false : true;
    }

    public Object a(ow owVar, qm qmVar, rw rwVar) throws IOException, oz {
        return rwVar.d(owVar, qmVar);
    }

    /* access modifiers changed from: protected */
    public final boolean n(ow owVar, qm qmVar) throws IOException, oz {
        pb e = owVar.e();
        if (e == pb.VALUE_TRUE) {
            return true;
        }
        if (e == pb.VALUE_FALSE) {
            return false;
        }
        if (e == pb.VALUE_NULL) {
            return false;
        }
        if (e == pb.VALUE_NUMBER_INT) {
            if (owVar.q() == oy.INT) {
                return owVar.t() != 0;
            }
            return p(owVar, qmVar);
        } else if (e == pb.VALUE_STRING) {
            String trim = owVar.k().trim();
            if ("true".equals(trim)) {
                return true;
            }
            if ("false".equals(trim) || trim.length() == 0) {
                return Boolean.FALSE.booleanValue();
            }
            throw qmVar.b(this.q, "only \"true\" or \"false\" recognized");
        } else {
            throw qmVar.a(this.q, e);
        }
    }

    /* access modifiers changed from: protected */
    public final Boolean o(ow owVar, qm qmVar) throws IOException, oz {
        pb e = owVar.e();
        if (e == pb.VALUE_TRUE) {
            return Boolean.TRUE;
        }
        if (e == pb.VALUE_FALSE) {
            return Boolean.FALSE;
        }
        if (e == pb.VALUE_NUMBER_INT) {
            if (owVar.q() == oy.INT) {
                return owVar.t() == 0 ? Boolean.FALSE : Boolean.TRUE;
            }
            return Boolean.valueOf(p(owVar, qmVar));
        } else if (e == pb.VALUE_NULL) {
            return (Boolean) b();
        } else {
            if (e == pb.VALUE_STRING) {
                String trim = owVar.k().trim();
                if ("true".equals(trim)) {
                    return Boolean.TRUE;
                }
                if ("false".equals(trim)) {
                    return Boolean.FALSE;
                }
                if (trim.length() == 0) {
                    return (Boolean) c();
                }
                throw qmVar.b(this.q, "only \"true\" or \"false\" recognized");
            }
            throw qmVar.a(this.q, e);
        }
    }

    /* access modifiers changed from: protected */
    public final boolean p(ow owVar, qm qmVar) throws IOException, oz {
        if (owVar.q() == oy.LONG) {
            return (owVar.u() == 0 ? Boolean.FALSE : Boolean.TRUE).booleanValue();
        }
        String k = owVar.k();
        if ("0.0".equals(k) || "0".equals(k)) {
            return Boolean.FALSE.booleanValue();
        }
        return Boolean.TRUE.booleanValue();
    }

    /* access modifiers changed from: protected */
    public Byte q(ow owVar, qm qmVar) throws IOException, oz {
        pb e = owVar.e();
        if (e == pb.VALUE_NUMBER_INT || e == pb.VALUE_NUMBER_FLOAT) {
            return Byte.valueOf(owVar.r());
        }
        if (e == pb.VALUE_STRING) {
            String trim = owVar.k().trim();
            try {
                if (trim.length() == 0) {
                    return (Byte) c();
                }
                int a = pt.a(trim);
                if (a >= -128 && a <= 127) {
                    return Byte.valueOf((byte) a);
                }
                throw qmVar.b(this.q, "overflow, value can not be represented as 8-bit value");
            } catch (IllegalArgumentException e2) {
                throw qmVar.b(this.q, "not a valid Byte value");
            }
        } else if (e == pb.VALUE_NULL) {
            return (Byte) b();
        } else {
            throw qmVar.a(this.q, e);
        }
    }

    /* access modifiers changed from: protected */
    public Short r(ow owVar, qm qmVar) throws IOException, oz {
        pb e = owVar.e();
        if (e == pb.VALUE_NUMBER_INT || e == pb.VALUE_NUMBER_FLOAT) {
            return Short.valueOf(owVar.s());
        }
        if (e == pb.VALUE_STRING) {
            String trim = owVar.k().trim();
            try {
                if (trim.length() == 0) {
                    return (Short) c();
                }
                int a = pt.a(trim);
                if (a >= -32768 && a <= 32767) {
                    return Short.valueOf((short) a);
                }
                throw qmVar.b(this.q, "overflow, value can not be represented as 16-bit value");
            } catch (IllegalArgumentException e2) {
                throw qmVar.b(this.q, "not a valid Short value");
            }
        } else if (e == pb.VALUE_NULL) {
            return (Short) b();
        } else {
            throw qmVar.a(this.q, e);
        }
    }

    /* access modifiers changed from: protected */
    public final short s(ow owVar, qm qmVar) throws IOException, oz {
        int t = t(owVar, qmVar);
        if (t >= -32768 && t <= 32767) {
            return (short) t;
        }
        throw qmVar.b(this.q, "overflow, value can not be represented as 16-bit value");
    }

    /* access modifiers changed from: protected */
    public final int t(ow owVar, qm qmVar) throws IOException, oz {
        pb e = owVar.e();
        if (e == pb.VALUE_NUMBER_INT || e == pb.VALUE_NUMBER_FLOAT) {
            return owVar.t();
        }
        if (e == pb.VALUE_STRING) {
            String trim = owVar.k().trim();
            try {
                int length = trim.length();
                if (length > 9) {
                    long parseLong = Long.parseLong(trim);
                    if (parseLong >= -2147483648L && parseLong <= 2147483647L) {
                        return (int) parseLong;
                    }
                    throw qmVar.b(this.q, "Overflow: numeric value (" + trim + ") out of range of int (" + Integer.MIN_VALUE + " - " + ((int) PHAsyncRequest.INFINITE_REDIRECTS) + ")");
                } else if (length == 0) {
                    return 0;
                } else {
                    return pt.a(trim);
                }
            } catch (IllegalArgumentException e2) {
                throw qmVar.b(this.q, "not a valid int value");
            }
        } else if (e == pb.VALUE_NULL) {
            return 0;
        } else {
            throw qmVar.a(this.q, e);
        }
    }

    /* access modifiers changed from: protected */
    public final Integer u(ow owVar, qm qmVar) throws IOException, oz {
        pb e = owVar.e();
        if (e == pb.VALUE_NUMBER_INT || e == pb.VALUE_NUMBER_FLOAT) {
            return Integer.valueOf(owVar.t());
        }
        if (e == pb.VALUE_STRING) {
            String trim = owVar.k().trim();
            try {
                int length = trim.length();
                if (length > 9) {
                    long parseLong = Long.parseLong(trim);
                    if (parseLong >= -2147483648L && parseLong <= 2147483647L) {
                        return Integer.valueOf((int) parseLong);
                    }
                    throw qmVar.b(this.q, "Overflow: numeric value (" + trim + ") out of range of Integer (" + Integer.MIN_VALUE + " - " + ((int) PHAsyncRequest.INFINITE_REDIRECTS) + ")");
                } else if (length == 0) {
                    return (Integer) c();
                } else {
                    return Integer.valueOf(pt.a(trim));
                }
            } catch (IllegalArgumentException e2) {
                throw qmVar.b(this.q, "not a valid Integer value");
            }
        } else if (e == pb.VALUE_NULL) {
            return (Integer) b();
        } else {
            throw qmVar.a(this.q, e);
        }
    }

    /* access modifiers changed from: protected */
    public final Long v(ow owVar, qm qmVar) throws IOException, oz {
        pb e = owVar.e();
        if (e == pb.VALUE_NUMBER_INT || e == pb.VALUE_NUMBER_FLOAT) {
            return Long.valueOf(owVar.u());
        }
        if (e == pb.VALUE_STRING) {
            String trim = owVar.k().trim();
            if (trim.length() == 0) {
                return (Long) c();
            }
            try {
                return Long.valueOf(pt.b(trim));
            } catch (IllegalArgumentException e2) {
                throw qmVar.b(this.q, "not a valid Long value");
            }
        } else if (e == pb.VALUE_NULL) {
            return (Long) b();
        } else {
            throw qmVar.a(this.q, e);
        }
    }

    /* access modifiers changed from: protected */
    public final long w(ow owVar, qm qmVar) throws IOException, oz {
        pb e = owVar.e();
        if (e == pb.VALUE_NUMBER_INT || e == pb.VALUE_NUMBER_FLOAT) {
            return owVar.u();
        }
        if (e == pb.VALUE_STRING) {
            String trim = owVar.k().trim();
            if (trim.length() == 0) {
                return 0;
            }
            try {
                return pt.b(trim);
            } catch (IllegalArgumentException e2) {
                throw qmVar.b(this.q, "not a valid long value");
            }
        } else if (e == pb.VALUE_NULL) {
            return 0;
        } else {
            throw qmVar.a(this.q, e);
        }
    }

    /* access modifiers changed from: protected */
    public final Float x(ow owVar, qm qmVar) throws IOException, oz {
        pb e = owVar.e();
        if (e == pb.VALUE_NUMBER_INT || e == pb.VALUE_NUMBER_FLOAT) {
            return Float.valueOf(owVar.w());
        }
        if (e == pb.VALUE_STRING) {
            String trim = owVar.k().trim();
            if (trim.length() == 0) {
                return (Float) c();
            }
            switch (trim.charAt(0)) {
                case '-':
                    if ("-Infinity".equals(trim) || "-INF".equals(trim)) {
                        return Float.valueOf(Float.NEGATIVE_INFINITY);
                    }
                case 'I':
                    if ("Infinity".equals(trim) || "INF".equals(trim)) {
                        return Float.valueOf(Float.POSITIVE_INFINITY);
                    }
                case 'N':
                    if ("NaN".equals(trim)) {
                        return Float.valueOf(Float.NaN);
                    }
                    break;
            }
            try {
                return Float.valueOf(Float.parseFloat(trim));
            } catch (IllegalArgumentException e2) {
                throw qmVar.b(this.q, "not a valid Float value");
            }
        } else if (e == pb.VALUE_NULL) {
            return (Float) b();
        } else {
            throw qmVar.a(this.q, e);
        }
    }

    /* access modifiers changed from: protected */
    public final float y(ow owVar, qm qmVar) throws IOException, oz {
        pb e = owVar.e();
        if (e == pb.VALUE_NUMBER_INT || e == pb.VALUE_NUMBER_FLOAT) {
            return owVar.w();
        }
        if (e == pb.VALUE_STRING) {
            String trim = owVar.k().trim();
            if (trim.length() == 0) {
                return 0.0f;
            }
            switch (trim.charAt(0)) {
                case '-':
                    if ("-Infinity".equals(trim) || "-INF".equals(trim)) {
                        return Float.NEGATIVE_INFINITY;
                    }
                case 'I':
                    if ("Infinity".equals(trim) || "INF".equals(trim)) {
                        return Float.POSITIVE_INFINITY;
                    }
                case 'N':
                    if ("NaN".equals(trim)) {
                        return Float.NaN;
                    }
                    break;
            }
            try {
                return Float.parseFloat(trim);
            } catch (IllegalArgumentException e2) {
                throw qmVar.b(this.q, "not a valid float value");
            }
        } else if (e == pb.VALUE_NULL) {
            return 0.0f;
        } else {
            throw qmVar.a(this.q, e);
        }
    }

    /* access modifiers changed from: protected */
    public final Double z(ow owVar, qm qmVar) throws IOException, oz {
        pb e = owVar.e();
        if (e == pb.VALUE_NUMBER_INT || e == pb.VALUE_NUMBER_FLOAT) {
            return Double.valueOf(owVar.x());
        }
        if (e == pb.VALUE_STRING) {
            String trim = owVar.k().trim();
            if (trim.length() == 0) {
                return (Double) c();
            }
            switch (trim.charAt(0)) {
                case '-':
                    if ("-Infinity".equals(trim) || "-INF".equals(trim)) {
                        return Double.valueOf(Double.NEGATIVE_INFINITY);
                    }
                case 'I':
                    if ("Infinity".equals(trim) || "INF".equals(trim)) {
                        return Double.valueOf(Double.POSITIVE_INFINITY);
                    }
                case 'N':
                    if ("NaN".equals(trim)) {
                        return Double.valueOf(Double.NaN);
                    }
                    break;
            }
            try {
                return Double.valueOf(b(trim));
            } catch (IllegalArgumentException e2) {
                throw qmVar.b(this.q, "not a valid Double value");
            }
        } else if (e == pb.VALUE_NULL) {
            return (Double) b();
        } else {
            throw qmVar.a(this.q, e);
        }
    }

    /* access modifiers changed from: protected */
    public final double A(ow owVar, qm qmVar) throws IOException, oz {
        pb e = owVar.e();
        if (e == pb.VALUE_NUMBER_INT || e == pb.VALUE_NUMBER_FLOAT) {
            return owVar.x();
        }
        if (e == pb.VALUE_STRING) {
            String trim = owVar.k().trim();
            if (trim.length() == 0) {
                return 0.0d;
            }
            switch (trim.charAt(0)) {
                case '-':
                    if ("-Infinity".equals(trim) || "-INF".equals(trim)) {
                        return Double.NEGATIVE_INFINITY;
                    }
                case 'I':
                    if ("Infinity".equals(trim) || "INF".equals(trim)) {
                        return Double.POSITIVE_INFINITY;
                    }
                case 'N':
                    if ("NaN".equals(trim)) {
                        return Double.NaN;
                    }
                    break;
            }
            try {
                return b(trim);
            } catch (IllegalArgumentException e2) {
                throw qmVar.b(this.q, "not a valid double value");
            }
        } else if (e == pb.VALUE_NULL) {
            return 0.0d;
        } else {
            throw qmVar.a(this.q, e);
        }
    }

    /* access modifiers changed from: protected */
    public Date B(ow owVar, qm qmVar) throws IOException, oz {
        pb e = owVar.e();
        if (e == pb.VALUE_NUMBER_INT) {
            return new Date(owVar.u());
        }
        if (e == pb.VALUE_NULL) {
            return (Date) b();
        }
        if (e == pb.VALUE_STRING) {
            try {
                String trim = owVar.k().trim();
                if (trim.length() == 0) {
                    return (Date) c();
                }
                return qmVar.a(trim);
            } catch (IllegalArgumentException e2) {
                throw qmVar.b(this.q, "not a valid representation (error: " + e2.getMessage() + ")");
            }
        } else {
            throw qmVar.a(this.q, e);
        }
    }

    protected static final double b(String str) throws NumberFormatException {
        if ("2.2250738585072012e-308".equals(str)) {
            return Double.MIN_NORMAL;
        }
        return Double.parseDouble(str);
    }

    /* access modifiers changed from: protected */
    public qu<Object> a(qk qkVar, qq qqVar, afm afm, qc qcVar) throws qw {
        return qqVar.a(qkVar, afm, qcVar);
    }

    /* access modifiers changed from: protected */
    public void a(ow owVar, qm qmVar, Object obj, String str) throws IOException, oz {
        Object obj2;
        if (obj == null) {
            obj2 = f();
        } else {
            obj2 = obj;
        }
        if (!qmVar.a(owVar, this, obj2, str)) {
            a(qmVar, obj2, str);
            owVar.d();
        }
    }

    /* access modifiers changed from: protected */
    public void a(qm qmVar, Object obj, String str) throws IOException, oz {
        if (qmVar.a(ql.FAIL_ON_UNKNOWN_PROPERTIES)) {
            throw qmVar.a(obj, str);
        }
    }
}
