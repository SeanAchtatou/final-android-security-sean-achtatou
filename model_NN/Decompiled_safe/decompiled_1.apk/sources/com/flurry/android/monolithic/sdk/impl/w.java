package com.flurry.android.monolithic.sdk.impl;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.widget.FrameLayout;

final class w extends WebChromeClient {
    final /* synthetic */ o a;

    private w(o oVar) {
        this.a = oVar;
    }

    /* synthetic */ w(o oVar, p pVar) {
        this(oVar);
    }

    public void onShowCustomView(View view, WebChromeClient.CustomViewCallback customViewCallback) {
        ja.a(3, this.a.f, "onShowCustomView(7)");
        if (!(this.a.getContext() instanceof Activity)) {
            ja.a(3, this.a.f, "no activity present");
        } else {
            onShowCustomView(view, ((Activity) this.a.getContext()).getRequestedOrientation(), customViewCallback);
        }
    }

    public void onShowCustomView(View view, int i, WebChromeClient.CustomViewCallback customViewCallback) {
        ja.a(3, this.a.f, "onShowCustomView(14)");
        if (!(this.a.getContext() instanceof Activity)) {
            ja.a(3, this.a.f, "no activity present");
            return;
        }
        Activity activity = (Activity) this.a.getContext();
        if (this.a.p == null || this.a.o == null) {
            View unused = this.a.p = view;
            int unused2 = this.a.q = activity.getRequestedOrientation();
            WebChromeClient.CustomViewCallback unused3 = this.a.r = customViewCallback;
            FrameLayout unused4 = this.a.t = new FrameLayout(activity);
            this.a.t.setBackgroundColor(-16777216);
            this.a.t.addView(this.a.p, new FrameLayout.LayoutParams(-1, -1, 17));
            ((ViewGroup) activity.getWindow().getDecorView()).addView(this.a.t, -1, -1);
            if (this.a.s == null) {
                Dialog unused5 = this.a.s = new x(this, activity, 16973841, activity);
                this.a.s.getWindow().setType(1000);
                this.a.s.setOnShowListener(new y(this));
                this.a.s.setOnDismissListener(new z(this));
                this.a.s.setCancelable(true);
                this.a.s.show();
            }
            j.a(activity, i, true);
            this.a.b.a(activity, this.a.d.b().toString());
            return;
        }
        this.a.o.onHideCustomView();
    }

    public void onHideCustomView() {
        ja.a(3, this.a.f, "onHideCustomView()");
        if (!(this.a.getContext() instanceof Activity)) {
            ja.a(3, this.a.f, "no activity present");
            return;
        }
        Activity activity = (Activity) this.a.getContext();
        if (this.a.p != null) {
            if (this.a.v != null) {
                this.a.v.show();
            }
            ((ViewGroup) activity.getWindow().getDecorView()).removeView(this.a.t);
            this.a.t.removeView(this.a.p);
            if (this.a.s != null && this.a.s.isShowing()) {
                this.a.s.hide();
                this.a.s.setOnDismissListener(null);
                this.a.s.dismiss();
            }
            Dialog unused = this.a.s = (Dialog) null;
            j.a(activity, this.a.q);
            this.a.r.onCustomViewHidden();
            WebChromeClient.CustomViewCallback unused2 = this.a.r = (WebChromeClient.CustomViewCallback) null;
            FrameLayout unused3 = this.a.t = (FrameLayout) null;
            View unused4 = this.a.p = (View) null;
            this.a.b.b(activity, this.a.d.b().toString());
        }
    }
}
