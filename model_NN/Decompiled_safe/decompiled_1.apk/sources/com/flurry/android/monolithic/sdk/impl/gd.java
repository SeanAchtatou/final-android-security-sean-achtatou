package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import android.text.TextUtils;
import java.util.HashMap;
import java.util.Map;

public final class gd {
    private static final gd a = new gd();
    private final Map<String, gc> b = new HashMap();

    public static gc a(Context context, String str) {
        if (context == null || TextUtils.isEmpty(str)) {
            return null;
        }
        return a.b(context, str);
    }

    private gd() {
    }

    private synchronized gc b(Context context, String str) {
        gc gcVar;
        gcVar = this.b.get(str);
        if (gcVar == null) {
            gcVar = new gc(context.getApplicationContext(), str);
            this.b.put(str, gcVar);
        }
        return gcVar;
    }
}
