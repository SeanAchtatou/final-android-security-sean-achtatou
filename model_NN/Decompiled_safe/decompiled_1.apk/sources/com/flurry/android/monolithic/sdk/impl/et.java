package com.flurry.android.monolithic.sdk.impl;

class et implements Runnable {
    final /* synthetic */ String a;
    final /* synthetic */ em b;

    et(em emVar, String str) {
        this.b = emVar;
        this.a = str;
    }

    public void run() {
        this.b.i();
        if (!this.b.e.remove(this.a)) {
            ja.a(6, em.f, "Internal error. Block with id = " + this.a + " was not in progress state");
        }
    }
}
