package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.Iterator;

@rz
public class aci extends aay<Iterator<?>> {
    public /* synthetic */ void b(Object obj, or orVar, ru ruVar) throws IOException, oq {
        a((Iterator<?>) ((Iterator) obj), orVar, ruVar);
    }

    public aci(afm afm, boolean z, rx rxVar, qc qcVar) {
        super(Iterator.class, afm, z, rxVar, qcVar, null);
    }

    public abc<?> a(rx rxVar) {
        return new aci(this.b, this.a, rxVar, this.e);
    }

    public void a(Iterator<?> it, or orVar, ru ruVar) throws IOException, oq {
        ra<Object> a;
        Class<?> cls;
        ra<Object> raVar;
        Class<?> cls2 = null;
        if (it.hasNext()) {
            rx rxVar = this.c;
            ra<Object> raVar2 = null;
            do {
                Object next = it.next();
                if (next == null) {
                    ruVar.a(orVar);
                } else {
                    Class<?> cls3 = next.getClass();
                    if (cls3 == cls2) {
                        raVar = raVar2;
                        Class<?> cls4 = cls2;
                        a = raVar2;
                        cls = cls4;
                    } else {
                        a = ruVar.a(cls3, this.e);
                        cls = cls3;
                        raVar = a;
                    }
                    if (rxVar == null) {
                        a.a(next, orVar, ruVar);
                        cls2 = cls;
                        raVar2 = raVar;
                    } else {
                        a.a(next, orVar, ruVar, rxVar);
                        cls2 = cls;
                        raVar2 = raVar;
                    }
                }
            } while (it.hasNext());
        }
    }
}
