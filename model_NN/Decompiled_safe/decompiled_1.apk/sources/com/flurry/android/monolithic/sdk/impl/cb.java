package com.flurry.android.monolithic.sdk.impl;

import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;
import java.util.List;

public class cb implements ca {
    private static final String a = cb.class.getSimpleName();
    private br b;

    /* JADX WARNING: Removed duplicated region for block: B:17:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x009f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a(java.lang.String r8, java.util.List<com.flurry.android.impl.ads.avro.protocol.v6.AdUnit> r9) {
        /*
            r7 = this;
            r2 = 0
            monitor-enter(r7)
            com.flurry.android.monolithic.sdk.impl.br r0 = r7.b     // Catch:{ all -> 0x007e }
            if (r0 == 0) goto L_0x0008
        L_0x0006:
            monitor-exit(r7)
            return
        L_0x0008:
            if (r9 == 0) goto L_0x0039
            int r0 = r9.size()     // Catch:{ all -> 0x007e }
            if (r0 == 0) goto L_0x0039
            r0 = 0
            java.lang.Object r0 = r9.get(r0)     // Catch:{ all -> 0x007e }
            com.flurry.android.impl.ads.avro.protocol.v6.AdUnit r0 = (com.flurry.android.impl.ads.avro.protocol.v6.AdUnit) r0     // Catch:{ all -> 0x007e }
            java.util.List r0 = r0.d()     // Catch:{ all -> 0x007e }
            int r0 = r0.size()     // Catch:{ all -> 0x007e }
            if (r0 == 0) goto L_0x0039
            r0 = 0
            java.lang.Object r0 = r9.get(r0)     // Catch:{ all -> 0x007e }
            com.flurry.android.impl.ads.avro.protocol.v6.AdUnit r0 = (com.flurry.android.impl.ads.avro.protocol.v6.AdUnit) r0     // Catch:{ all -> 0x007e }
            java.util.List r0 = r0.d()     // Catch:{ all -> 0x007e }
            r1 = 0
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x007e }
            com.flurry.android.impl.ads.avro.protocol.v6.AdFrame r0 = (com.flurry.android.impl.ads.avro.protocol.v6.AdFrame) r0     // Catch:{ all -> 0x007e }
            com.flurry.android.impl.ads.avro.protocol.v6.AdSpaceLayout r0 = r0.e()     // Catch:{ all -> 0x007e }
            if (r0 != 0) goto L_0x0081
        L_0x0039:
            r0 = 1
        L_0x003a:
            if (r0 == 0) goto L_0x0083
            com.flurry.android.FlurryAdSize r0 = com.flurry.android.FlurryAdSize.BANNER_BOTTOM     // Catch:{ all -> 0x007e }
            r5 = r0
        L_0x003f:
            r0 = 3
            java.lang.String r1 = com.flurry.android.monolithic.sdk.impl.cb.a     // Catch:{ all -> 0x007e }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x007e }
            r2.<init>()     // Catch:{ all -> 0x007e }
            java.lang.String r3 = "Starting AsyncAdTask from EnsureCacheNotEmpty size: "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x007e }
            if (r9 == 0) goto L_0x009f
            int r3 = r9.size()     // Catch:{ all -> 0x007e }
            java.lang.String r3 = java.lang.Integer.toString(r3)     // Catch:{ all -> 0x007e }
        L_0x0057:
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x007e }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x007e }
            com.flurry.android.monolithic.sdk.impl.ja.a(r0, r1, r2)     // Catch:{ all -> 0x007e }
            com.flurry.android.impl.ads.FlurryAdModule r1 = com.flurry.android.impl.ads.FlurryAdModule.getInstance()     // Catch:{ all -> 0x007e }
            com.flurry.android.monolithic.sdk.impl.br r0 = new com.flurry.android.monolithic.sdk.impl.br     // Catch:{ all -> 0x007e }
            r1.getClass()     // Catch:{ all -> 0x007e }
            r3 = 0
            r4 = 0
            com.flurry.android.monolithic.sdk.impl.cc r6 = new com.flurry.android.monolithic.sdk.impl.cc     // Catch:{ all -> 0x007e }
            r6.<init>(r7)     // Catch:{ all -> 0x007e }
            r2 = r8
            r0.<init>(r1, r2, r3, r4, r5, r6)     // Catch:{ all -> 0x007e }
            r7.b = r0     // Catch:{ all -> 0x007e }
            com.flurry.android.monolithic.sdk.impl.br r0 = r7.b     // Catch:{ all -> 0x007e }
            r0.a()     // Catch:{ all -> 0x007e }
            goto L_0x0006
        L_0x007e:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        L_0x0081:
            r0 = r2
            goto L_0x003a
        L_0x0083:
            r0 = 0
            java.lang.Object r0 = r9.get(r0)     // Catch:{ all -> 0x007e }
            com.flurry.android.impl.ads.avro.protocol.v6.AdUnit r0 = (com.flurry.android.impl.ads.avro.protocol.v6.AdUnit) r0     // Catch:{ all -> 0x007e }
            java.util.List r0 = r0.d()     // Catch:{ all -> 0x007e }
            r1 = 0
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x007e }
            com.flurry.android.impl.ads.avro.protocol.v6.AdFrame r0 = (com.flurry.android.impl.ads.avro.protocol.v6.AdFrame) r0     // Catch:{ all -> 0x007e }
            com.flurry.android.impl.ads.avro.protocol.v6.AdSpaceLayout r0 = r0.e()     // Catch:{ all -> 0x007e }
            com.flurry.android.FlurryAdSize r0 = com.flurry.android.monolithic.sdk.impl.ab.b(r0)     // Catch:{ all -> 0x007e }
            r5 = r0
            goto L_0x003f
        L_0x009f:
            java.lang.String r3 = ""
            goto L_0x0057
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.cb.a(java.lang.String, java.util.List):void");
    }

    /* access modifiers changed from: private */
    public synchronized void a() {
        if (this.b != null) {
            this.b.a(true);
            this.b = null;
        }
    }

    public void a(String str, int i, int i2, List<AdUnit> list) {
        if (i2 < 2) {
            a(str, list);
        }
    }
}
