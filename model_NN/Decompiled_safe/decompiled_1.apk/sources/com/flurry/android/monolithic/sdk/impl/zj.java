package com.flurry.android.monolithic.sdk.impl;

public class zj extends rt {
    protected static final rv[] a = new rv[0];
    protected static final zk[] b = new zk[0];
    protected final rv[] c;
    protected final rv[] d;
    protected final zk[] e;

    public zj() {
        this(null, null, null);
    }

    protected zj(rv[] rvVarArr, rv[] rvVarArr2, zk[] zkVarArr) {
        rv[] rvVarArr3;
        zk[] zkVarArr2;
        this.c = rvVarArr == null ? a : rvVarArr;
        if (rvVarArr2 == null) {
            rvVarArr3 = a;
        } else {
            rvVarArr3 = rvVarArr2;
        }
        this.d = rvVarArr3;
        if (zkVarArr == null) {
            zkVarArr2 = b;
        } else {
            zkVarArr2 = zkVarArr;
        }
        this.e = zkVarArr2;
    }

    public boolean a() {
        return this.d.length > 0;
    }

    public boolean b() {
        return this.e.length > 0;
    }

    public Iterable<rv> c() {
        return adp.b(this.c);
    }

    public Iterable<rv> d() {
        return adp.b(this.d);
    }

    public Iterable<zk> e() {
        return adp.b(this.e);
    }
}
