package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;

public class zw extends ru {
    public static final ra<Object> d = new aai("Null key for a Map not allowed in JSON (use a converting NullKeySerializer?)");
    @Deprecated
    public static final ra<Object> e = new acq();
    public static final ra<Object> f = new aav();
    protected final rs g;
    protected final aat h;
    protected final aem i;
    protected ra<Object> j;
    protected ra<Object> k;
    protected ra<Object> l;
    protected ra<Object> m;
    protected final aas n;
    protected DateFormat o;

    public zw() {
        super(null);
        this.j = f;
        this.l = abn.a;
        this.m = d;
        this.g = null;
        this.h = new aat();
        this.n = null;
        this.i = new aem();
    }

    protected zw(rq rqVar, zw zwVar, rs rsVar) {
        super(rqVar);
        this.j = f;
        this.l = abn.a;
        this.m = d;
        if (rqVar == null) {
            throw new NullPointerException();
        }
        this.g = rsVar;
        this.h = zwVar.h;
        this.j = zwVar.j;
        this.k = zwVar.k;
        this.l = zwVar.l;
        this.m = zwVar.m;
        this.i = zwVar.i;
        this.n = this.h.a();
    }

    /* access modifiers changed from: protected */
    public zw a(rq rqVar, rs rsVar) {
        return new zw(rqVar, this, rsVar);
    }

    public final void a(rq rqVar, or orVar, Object obj, rs rsVar) throws IOException, oq {
        if (rsVar == null) {
            throw new IllegalArgumentException("Can not pass null serializerFactory");
        }
        zw a = a(rqVar, rsVar);
        if (a.getClass() != getClass()) {
            throw new IllegalStateException("Broken serializer provider: createInstance returned instance of type " + a.getClass() + "; blueprint of type " + getClass());
        }
        a.a(orVar, obj);
    }

    public ra<Object> a(Class<?> cls, qc qcVar) throws qw {
        ra<Object> b = this.n.b(cls);
        if (b == null && (b = this.h.a(cls)) == null && (b = this.h.a(this.b.b(cls))) == null && (b = b(cls, qcVar)) == null) {
            return a(cls);
        }
        return a(b, qcVar);
    }

    public ra<Object> a(afm afm, qc qcVar) throws qw {
        ra<Object> b = this.n.b(afm);
        if (b == null && (b = this.h.a(afm)) == null && (b = c(afm, qcVar)) == null) {
            return a(afm.p());
        }
        return a(b, qcVar);
    }

    public ra<Object> a(Class<?> cls, boolean z, qc qcVar) throws qw {
        ra<Object> a = this.n.a(cls);
        if (a == null && (a = this.h.b(cls)) == null) {
            a = a(cls, qcVar);
            rx b = this.g.b(this.b, this.b.b(cls), qcVar);
            if (b != null) {
                a = new zx(b, a);
            }
            if (z) {
                this.h.a(cls, a);
            }
        }
        return a;
    }

    public ra<Object> a(afm afm, boolean z, qc qcVar) throws qw {
        ra<Object> a = this.n.a(afm);
        if (a == null && (a = this.h.b(afm)) == null) {
            a = a(afm, qcVar);
            rx b = this.g.b(this.b, afm, qcVar);
            if (b != null) {
                a = new zx(b, a);
            }
            if (z) {
                this.h.a(afm, a);
            }
        }
        return a;
    }

    public ra<Object> b(afm afm, qc qcVar) throws qw {
        ra<Object> c = this.g.c(this.b, afm, qcVar);
        if (c == null) {
            if (this.k == null) {
                c = acr.a(afm);
            } else {
                c = this.k;
            }
        }
        if (c instanceof qj) {
            return ((qj) c).a(this.b, qcVar);
        }
        return c;
    }

    public ra<Object> c() {
        return this.m;
    }

    public ra<Object> d() {
        return this.l;
    }

    public ra<Object> a(Class<?> cls) {
        return this.j;
    }

    public final void a(long j2, or orVar) throws IOException, oz {
        if (a(rr.WRITE_DATES_AS_TIMESTAMPS)) {
            orVar.a(j2);
            return;
        }
        if (this.o == null) {
            this.o = (DateFormat) this.b.n().clone();
        }
        orVar.b(this.o.format(new Date(j2)));
    }

    public final void a(Date date, or orVar) throws IOException, oz {
        if (a(rr.WRITE_DATES_AS_TIMESTAMPS)) {
            orVar.a(date.getTime());
            return;
        }
        if (this.o == null) {
            this.o = (DateFormat) this.b.n().clone();
        }
        orVar.b(this.o.format(date));
    }

    public void b(long j2, or orVar) throws IOException, oz {
        if (a(rr.WRITE_DATE_KEYS_AS_TIMESTAMPS)) {
            orVar.a(String.valueOf(j2));
            return;
        }
        if (this.o == null) {
            this.o = (DateFormat) this.b.n().clone();
        }
        orVar.a(this.o.format(new Date(j2)));
    }

    public void b(Date date, or orVar) throws IOException, oz {
        if (a(rr.WRITE_DATE_KEYS_AS_TIMESTAMPS)) {
            orVar.a(String.valueOf(date.getTime()));
            return;
        }
        if (this.o == null) {
            this.o = (DateFormat) this.b.n().clone();
        }
        orVar.a(this.o.format(date));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.zw.a(java.lang.Class<?>, boolean, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object>
     arg types: [java.lang.Class<?>, int, ?[OBJECT, ARRAY]]
     candidates:
      com.flurry.android.monolithic.sdk.impl.zw.a(com.flurry.android.monolithic.sdk.impl.afm, boolean, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object>
      com.flurry.android.monolithic.sdk.impl.ru.a(com.flurry.android.monolithic.sdk.impl.afm, boolean, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object>
      com.flurry.android.monolithic.sdk.impl.zw.a(java.lang.Class<?>, boolean, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object> */
    /* access modifiers changed from: protected */
    public void a(or orVar, Object obj) throws IOException, oz {
        ra<Object> raVar;
        boolean z;
        if (obj == null) {
            raVar = d();
            z = false;
        } else {
            ra<Object> a = a(obj.getClass(), true, (qc) null);
            boolean a2 = this.b.a(rr.WRAP_ROOT_VALUE);
            if (a2) {
                orVar.d();
                orVar.a(this.i.a(obj.getClass(), this.b));
            }
            boolean z2 = a2;
            raVar = a;
            z = z2;
        }
        try {
            raVar.a(obj, orVar, this);
            if (z) {
                orVar.e();
            }
        } catch (IOException e2) {
            throw e2;
        } catch (Exception e3) {
            String message = e3.getMessage();
            if (message == null) {
                message = "[no message for " + e3.getClass().getName() + "]";
            }
            throw new qw(message, e3);
        }
    }

    /* access modifiers changed from: protected */
    public ra<Object> b(Class<?> cls, qc qcVar) throws qw {
        try {
            ra<Object> d2 = d(this.b.b(cls), qcVar);
            if (d2 != null) {
                this.h.a(cls, d2, this);
            }
            return d2;
        } catch (IllegalArgumentException e2) {
            throw new qw(e2.getMessage(), null, e2);
        }
    }

    /* access modifiers changed from: protected */
    public ra<Object> c(afm afm, qc qcVar) throws qw {
        try {
            ra<Object> d2 = d(afm, qcVar);
            if (d2 != null) {
                this.h.a(afm, d2, this);
            }
            return d2;
        } catch (IllegalArgumentException e2) {
            throw new qw(e2.getMessage(), null, e2);
        }
    }

    /* access modifiers changed from: protected */
    public ra<Object> d(afm afm, qc qcVar) throws qw {
        return this.g.a(this.b, afm, qcVar);
    }

    /* access modifiers changed from: protected */
    public ra<Object> a(ra<Object> raVar, qc qcVar) throws qw {
        if (!(raVar instanceof qj)) {
            return raVar;
        }
        ra<Object> a = ((qj) raVar).a(this.b, qcVar);
        if (a == raVar) {
            return raVar;
        }
        if (a instanceof rp) {
            ((rp) a).a(this);
        }
        return a;
    }
}
