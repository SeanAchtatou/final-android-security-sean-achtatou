package com.flurry.android.monolithic.sdk.impl;

import java.lang.reflect.Field;
import java.util.EnumMap;
import java.util.EnumSet;

class aea {
    static final aea a = new aea();
    private final Field b;
    private final Field c;

    private aea() {
        Class<Class> cls = Class.class;
        Class<Class> cls2 = Class.class;
        this.b = a(EnumSet.class, "elementType", cls);
        Class<Class> cls3 = Class.class;
        this.c = a(EnumMap.class, "elementType", cls);
    }

    public Class<? extends Enum<?>> a(EnumSet<?> enumSet) {
        if (this.b != null) {
            return (Class) a(enumSet, this.b);
        }
        throw new IllegalStateException("Can not figure out type for EnumSet (odd JDK platform?)");
    }

    public Class<? extends Enum<?>> a(EnumMap<?, ?> enumMap) {
        if (this.c != null) {
            return (Class) a(enumMap, this.c);
        }
        throw new IllegalStateException("Can not figure out type for EnumMap (odd JDK platform?)");
    }

    private Object a(Object obj, Field field) {
        try {
            return field.get(obj);
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    private static Field a(Class<?> cls, String str, Class<?> cls2) {
        Field field;
        Field field2;
        Field[] declaredFields = cls.getDeclaredFields();
        int length = declaredFields.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                field = null;
                break;
            }
            Field field3 = declaredFields[i];
            if (str.equals(field3.getName()) && field3.getType() == cls2) {
                field = field3;
                break;
            }
            i++;
        }
        if (field == null) {
            Field field4 = field;
            for (Field field5 : declaredFields) {
                if (field5.getType() == cls2) {
                    if (field4 != null) {
                        return null;
                    }
                    field4 = field5;
                }
            }
            field2 = field4;
        } else {
            field2 = field;
        }
        if (field2 == null) {
            return field2;
        }
        try {
            field2.setAccessible(true);
            return field2;
        } catch (Throwable th) {
            return field2;
        }
    }
}
