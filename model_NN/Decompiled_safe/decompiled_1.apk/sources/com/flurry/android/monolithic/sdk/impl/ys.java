package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public class ys extends zb {
    public ys(yi yiVar, qc qcVar) {
        super(yiVar, qcVar);
    }

    public void b(Object obj, or orVar) throws IOException, oz {
        orVar.d();
        orVar.g(this.b.a(obj));
    }

    public void c(Object obj, or orVar) throws IOException, oz {
        orVar.d();
        orVar.f(this.b.a(obj));
    }

    public void a(Object obj, or orVar) throws IOException, oz {
        orVar.d();
        orVar.a(this.b.a(obj));
    }

    public void a(Object obj, or orVar, Class<?> cls) throws IOException, oz {
        orVar.d();
        orVar.a(this.b.a(obj, cls));
    }

    public void e(Object obj, or orVar) throws IOException, oz {
        orVar.e();
        orVar.e();
    }

    public void f(Object obj, or orVar) throws IOException, oz {
        orVar.c();
        orVar.e();
    }

    public void d(Object obj, or orVar) throws IOException, oz {
        orVar.e();
    }
}
