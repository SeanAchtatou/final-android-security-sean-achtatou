package com.flurry.android.monolithic.sdk.impl;

import android.os.Looper;
import java.io.Closeable;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

public class ey {
    static final Integer a = 50;
    private static final String c = ey.class.getSimpleName();
    LinkedHashMap<String, List<String>> b;

    public ey() {
        a();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.b = new LinkedHashMap<>();
        List<String> d = d("Main");
        if (d != null) {
            for (String next : d) {
                List<String> d2 = d(next);
                if (d2 != null) {
                    this.b.put(next, d2);
                }
            }
        }
    }

    private synchronized void d() {
        LinkedList linkedList = new LinkedList(this.b.keySet());
        c();
        if (!linkedList.isEmpty()) {
            a("Main", linkedList);
        }
    }

    public synchronized void a(ew ewVar, String str) {
        boolean z;
        LinkedList linkedList;
        ja.a(4, c, "addBlockInfo");
        String a2 = ewVar.a();
        List list = this.b.get(str);
        if (list == null) {
            ja.a(4, c, "New Data Key");
            z = true;
            linkedList = new LinkedList();
        } else {
            z = false;
            linkedList = list;
        }
        linkedList.add(a2);
        if (linkedList.size() > a.intValue()) {
            a((String) linkedList.get(0));
            linkedList.remove(0);
        }
        this.b.put(str, linkedList);
        a(str, linkedList);
        if (z) {
            d();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(String str) {
        return new ew(str).c();
    }

    public boolean a(String str, String str2) {
        List list = this.b.get(str2);
        boolean z = false;
        if (list != null) {
            a(str);
            z = list.remove(str);
        }
        if (list == null || list.isEmpty()) {
            c(str2);
        } else {
            this.b.put(str2, list);
            a(str2, list);
        }
        return z;
    }

    public List<String> b() {
        return new ArrayList(this.b.keySet());
    }

    public List<String> b(String str) {
        return this.b.get(str);
    }

    public synchronized boolean c(String str) {
        boolean a2;
        if (Looper.myLooper() == Looper.getMainLooper()) {
            ja.a(6, c, "discardOutdatedBlocksForDataKey(ID) running on the MAIN thread!");
        }
        File fileStreamPath = ia.a().b().getFileStreamPath(".FlurrySenderIndex.info." + str);
        List<String> b2 = b(str);
        if (b2 != null) {
            ja.a(4, c, "discardOutdatedBlocksForDataKey: notSentBlocks = " + b2.size());
            for (int i = 0; i < b2.size(); i++) {
                String str2 = b2.get(i);
                a(str2);
                ja.a(4, c, "discardOutdatedBlocksForDataKey: removed block = " + str2);
            }
        }
        this.b.remove(str);
        a2 = a(fileStreamPath);
        d();
        return a2;
    }

    private synchronized boolean a(File file) {
        boolean z;
        z = false;
        if (file != null) {
            if (file.exists()) {
                ja.a(4, c, "Trying to delete persistence file : " + file.getAbsolutePath());
                z = file.delete();
                if (z) {
                    ja.a(4, c, "Deleted persistence file");
                } else {
                    ja.a(6, c, "Cannot delete persistence file");
                }
            }
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        a(ia.a().b().getFileStreamPath(".FlurrySenderIndex.info.Main"));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00bc, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00bd, code lost:
        r9 = r1;
        r1 = r0;
        r0 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00c9, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00ca, code lost:
        r9 = r1;
        r1 = r0;
        r0 = r9;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00bc A[ExcHandler: all (r1v14 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:10:0x0043] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized java.util.List<java.lang.String> d(java.lang.String r11) {
        /*
            r10 = this;
            r3 = 0
            monitor-enter(r10)
            android.os.Looper r0 = android.os.Looper.myLooper()     // Catch:{ all -> 0x00af }
            android.os.Looper r1 = android.os.Looper.getMainLooper()     // Catch:{ all -> 0x00af }
            if (r0 != r1) goto L_0x0014
            r0 = 6
            java.lang.String r1 = com.flurry.android.monolithic.sdk.impl.ey.c     // Catch:{ all -> 0x00af }
            java.lang.String r2 = "readFromFile(byte[], ID) running on the MAIN thread!"
            com.flurry.android.monolithic.sdk.impl.ja.a(r0, r1, r2)     // Catch:{ all -> 0x00af }
        L_0x0014:
            com.flurry.android.monolithic.sdk.impl.ia r0 = com.flurry.android.monolithic.sdk.impl.ia.a()     // Catch:{ all -> 0x00af }
            android.content.Context r0 = r0.b()     // Catch:{ all -> 0x00af }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00af }
            r1.<init>()     // Catch:{ all -> 0x00af }
            java.lang.String r2 = ".FlurrySenderIndex.info."
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00af }
            java.lang.StringBuilder r1 = r1.append(r11)     // Catch:{ all -> 0x00af }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00af }
            java.io.File r0 = r0.getFileStreamPath(r1)     // Catch:{ all -> 0x00af }
            boolean r1 = r0.exists()     // Catch:{ all -> 0x00af }
            if (r1 == 0) goto L_0x00b2
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x0099, all -> 0x00a9 }
            r1.<init>(r0)     // Catch:{ Throwable -> 0x0099, all -> 0x00a9 }
            java.io.DataInputStream r0 = new java.io.DataInputStream     // Catch:{ Throwable -> 0x0099, all -> 0x00a9 }
            r0.<init>(r1)     // Catch:{ Throwable -> 0x0099, all -> 0x00a9 }
            int r1 = r0.readUnsignedShort()     // Catch:{ Throwable -> 0x00c3, all -> 0x00bc }
            if (r1 != 0) goto L_0x004f
            com.flurry.android.monolithic.sdk.impl.je.a(r0)     // Catch:{ all -> 0x00af }
            r0 = r3
        L_0x004d:
            monitor-exit(r10)
            return r0
        L_0x004f:
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ Throwable -> 0x00c3, all -> 0x00bc }
            r2.<init>(r1)     // Catch:{ Throwable -> 0x00c3, all -> 0x00bc }
            r3 = 0
        L_0x0055:
            if (r3 >= r1) goto L_0x008e
            int r4 = r0.readUnsignedShort()     // Catch:{ Throwable -> 0x00c9, all -> 0x00bc }
            r5 = 4
            java.lang.String r6 = com.flurry.android.monolithic.sdk.impl.ey.c     // Catch:{ Throwable -> 0x00c9, all -> 0x00bc }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00c9, all -> 0x00bc }
            r7.<init>()     // Catch:{ Throwable -> 0x00c9, all -> 0x00bc }
            java.lang.String r8 = "read iter "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Throwable -> 0x00c9, all -> 0x00bc }
            java.lang.StringBuilder r7 = r7.append(r3)     // Catch:{ Throwable -> 0x00c9, all -> 0x00bc }
            java.lang.String r8 = " dataLength = "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Throwable -> 0x00c9, all -> 0x00bc }
            java.lang.StringBuilder r7 = r7.append(r4)     // Catch:{ Throwable -> 0x00c9, all -> 0x00bc }
            java.lang.String r7 = r7.toString()     // Catch:{ Throwable -> 0x00c9, all -> 0x00bc }
            com.flurry.android.monolithic.sdk.impl.ja.a(r5, r6, r7)     // Catch:{ Throwable -> 0x00c9, all -> 0x00bc }
            byte[] r4 = new byte[r4]     // Catch:{ Throwable -> 0x00c9, all -> 0x00bc }
            r0.readFully(r4)     // Catch:{ Throwable -> 0x00c9, all -> 0x00bc }
            java.lang.String r5 = new java.lang.String     // Catch:{ Throwable -> 0x00c9, all -> 0x00bc }
            r5.<init>(r4)     // Catch:{ Throwable -> 0x00c9, all -> 0x00bc }
            r2.add(r5)     // Catch:{ Throwable -> 0x00c9, all -> 0x00bc }
            int r3 = r3 + 1
            goto L_0x0055
        L_0x008e:
            int r1 = r0.readUnsignedShort()     // Catch:{ Throwable -> 0x00c9, all -> 0x00bc }
            if (r1 != 0) goto L_0x0094
        L_0x0094:
            com.flurry.android.monolithic.sdk.impl.je.a(r0)     // Catch:{ all -> 0x00af }
            r0 = r2
            goto L_0x004d
        L_0x0099:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x009c:
            r3 = 6
            java.lang.String r4 = com.flurry.android.monolithic.sdk.impl.ey.c     // Catch:{ all -> 0x00c1 }
            java.lang.String r5 = "Error when loading persistent file"
            com.flurry.android.monolithic.sdk.impl.ja.a(r3, r4, r5, r0)     // Catch:{ all -> 0x00c1 }
            com.flurry.android.monolithic.sdk.impl.je.a(r1)     // Catch:{ all -> 0x00af }
            r0 = r2
            goto L_0x004d
        L_0x00a9:
            r0 = move-exception
            r1 = r3
        L_0x00ab:
            com.flurry.android.monolithic.sdk.impl.je.a(r1)     // Catch:{ all -> 0x00af }
            throw r0     // Catch:{ all -> 0x00af }
        L_0x00af:
            r0 = move-exception
            monitor-exit(r10)
            throw r0
        L_0x00b2:
            r0 = 5
            java.lang.String r1 = com.flurry.android.monolithic.sdk.impl.ey.c     // Catch:{ all -> 0x00af }
            java.lang.String r2 = "Agent cache file doesn't exist."
            com.flurry.android.monolithic.sdk.impl.ja.a(r0, r1, r2)     // Catch:{ all -> 0x00af }
            r0 = r3
            goto L_0x004d
        L_0x00bc:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x00ab
        L_0x00c1:
            r0 = move-exception
            goto L_0x00ab
        L_0x00c3:
            r1 = move-exception
            r2 = r3
            r9 = r0
            r0 = r1
            r1 = r9
            goto L_0x009c
        L_0x00c9:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x009c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.ey.d(java.lang.String):java.util.List");
    }

    private synchronized boolean a(String str, List<String> list) {
        boolean z;
        if (Looper.myLooper() == Looper.getMainLooper()) {
            ja.a(6, c, "saveToFile(byte[], ID) running on the MAIN thread!");
        }
        File fileStreamPath = ia.a().b().getFileStreamPath(".FlurrySenderIndex.info." + str);
        DataOutputStream dataOutputStream = null;
        try {
            if (!iw.a(fileStreamPath)) {
                je.a((Closeable) null);
                z = false;
            } else {
                DataOutputStream dataOutputStream2 = new DataOutputStream(new FileOutputStream(fileStreamPath));
                try {
                    dataOutputStream2.writeShort(list.size());
                    for (int i = 0; i < list.size(); i++) {
                        byte[] bytes = list.get(i).getBytes();
                        int length = bytes.length;
                        ja.a(4, c, "write iter " + i + " dataLength = " + length);
                        dataOutputStream2.writeShort(length);
                        dataOutputStream2.write(bytes);
                    }
                    dataOutputStream2.writeShort(0);
                    z = true;
                    je.a(dataOutputStream2);
                } catch (Throwable th) {
                    th = th;
                    dataOutputStream = dataOutputStream2;
                    je.a(dataOutputStream);
                    throw th;
                }
            }
        } catch (Throwable th2) {
            th = th2;
        }
        return z;
    }
}
