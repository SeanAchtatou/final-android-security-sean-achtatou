package com.flurry.android.monolithic.sdk.impl;

import com.ideaworks3d.marmalade.s3eAndroidMarketBilling.s3eAndroidMarketBillingReceiver;
import java.util.ArrayList;
import org.apache.http.message.BasicNameValuePair;

class ho {
    /* access modifiers changed from: private */
    public hn a;
    /* access modifiers changed from: private */
    public String b;
    /* access modifiers changed from: private */
    public Object c = new Object();

    ho() {
    }

    public hn a(hm hmVar) {
        switch (hmVar.h()) {
            case 1:
                return c(hmVar);
            case 2:
                return d(hmVar);
            case s3eAndroidMarketBillingReceiver.S3E_ANDROIDMARKETBILLING_SECURITY_INVALID_NONCE /*32*/:
                return b(hmVar);
            default:
                return null;
        }
    }

    private hn c(hm hmVar) {
        boolean z;
        this.a = new hn(721, null);
        try {
            ArrayList arrayList = new ArrayList(hmVar.a());
            this.b = hmVar.c();
            String b2 = hmVar.b();
            String str = "v1/";
            if (hmVar.g() == hl.USER) {
                z = true;
                str = str + "user";
            } else if (hmVar.g() == hl.OBJECT) {
                str = str + "object/" + b2;
                z = false;
            } else {
                z = false;
            }
            if (this.b == null) {
                gr.a(z, str, arrayList, new hp(this));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            synchronized (this.c) {
                this.c.wait(30000);
            }
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
        return this.a;
    }

    private hn d(hm hmVar) {
        boolean z;
        this.a = new hn(721, null);
        this.b = hmVar.c();
        String b2 = hmVar.b();
        String str = "v1/";
        if (hmVar.g() == hl.USER) {
            z = true;
            str = str + "user";
        } else if (hmVar.g() == hl.OBJECT) {
            str = str + "object/" + b2;
            z = false;
        } else {
            z = false;
        }
        try {
            gr.c(z, str + "/" + this.b, null, new hq(this));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            synchronized (this.c) {
                this.c.wait(30000);
            }
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
        return this.a;
    }

    public hn b(hm hmVar) {
        boolean z;
        String str;
        this.a = new hn(721, null);
        String name = hmVar.a().get(0).getName();
        String value = hmVar.a().get(0).getValue();
        this.b = hmVar.c();
        String b2 = hmVar.b();
        ArrayList arrayList = new ArrayList();
        if (name != null && !name.equals("")) {
            arrayList.add(new BasicNameValuePair(name, "decrement:" + value));
        }
        if (hmVar.g() == hl.USER) {
            z = true;
            str = "" + "user";
        } else if (hmVar.g() == hl.OBJECT) {
            str = "" + "object/" + b2;
            z = false;
        } else {
            z = false;
            str = "";
        }
        try {
            gr.b(z, str + "/" + this.b, arrayList, new hr(this));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            synchronized (this.c) {
                this.c.wait(30000);
            }
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
        return this.a;
    }
}
