package com.flurry.android.monolithic.sdk.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

public class yv extends yh {
    protected LinkedHashSet<yg> a;

    public Collection<yg> a(xk xkVar, rf<?> rfVar, py pyVar) {
        HashMap hashMap = new HashMap();
        if (this.a != null) {
            Class<?> d = xkVar.d();
            Iterator<yg> it = this.a.iterator();
            while (it.hasNext()) {
                yg next = it.next();
                if (d.isAssignableFrom(next.a())) {
                    a(xh.b(next.a(), pyVar, rfVar), next, rfVar, pyVar, hashMap);
                }
            }
        }
        List<yg> a2 = pyVar.a((xg) xkVar);
        if (a2 != null) {
            for (yg next2 : a2) {
                a(xh.b(next2.a(), pyVar, rfVar), next2, rfVar, pyVar, hashMap);
            }
        }
        a(xh.b(xkVar.d(), pyVar, rfVar), new yg(xkVar.d(), null), rfVar, pyVar, hashMap);
        return new ArrayList(hashMap.values());
    }

    public Collection<yg> a(xh xhVar, rf<?> rfVar, py pyVar) {
        HashMap hashMap = new HashMap();
        if (this.a != null) {
            Class<?> d = xhVar.d();
            Iterator<yg> it = this.a.iterator();
            while (it.hasNext()) {
                yg next = it.next();
                if (d.isAssignableFrom(next.a())) {
                    a(xh.b(next.a(), pyVar, rfVar), next, rfVar, pyVar, hashMap);
                }
            }
        }
        a(xhVar, new yg(xhVar.d(), null), rfVar, pyVar, hashMap);
        return new ArrayList(hashMap.values());
    }

    /* access modifiers changed from: protected */
    public void a(xh xhVar, yg ygVar, rf<?> rfVar, py pyVar, HashMap<yg, yg> hashMap) {
        yg ygVar2;
        yg ygVar3;
        String g;
        if (ygVar.c() || (g = pyVar.g(xhVar)) == null) {
            ygVar2 = ygVar;
        } else {
            ygVar2 = new yg(ygVar.a(), g);
        }
        if (!hashMap.containsKey(ygVar2)) {
            hashMap.put(ygVar2, ygVar2);
            List<yg> a2 = pyVar.a((xg) xhVar);
            if (a2 != null && !a2.isEmpty()) {
                for (yg next : a2) {
                    xh b = xh.b(next.a(), pyVar, rfVar);
                    if (!next.c()) {
                        ygVar3 = new yg(next.a(), pyVar.g(b));
                    } else {
                        ygVar3 = next;
                    }
                    a(b, ygVar3, rfVar, pyVar, hashMap);
                }
            }
        } else if (ygVar2.c() && !hashMap.get(ygVar2).c()) {
            hashMap.put(ygVar2, ygVar2);
        }
    }
}
