package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

public abstract class qm {
    protected final qk a;
    protected final int b;

    public abstract qw a(afm afm, String str);

    public abstract qw a(ow owVar, pb pbVar, String str);

    public abstract qw a(Class<?> cls, pb pbVar);

    public abstract qw a(Class<?> cls, String str);

    public abstract qw a(Class<?> cls, String str, String str2);

    public abstract qw a(Class<?> cls, Throwable th);

    public abstract qw a(Object obj, String str);

    public abstract Object a(Object obj, qc qcVar, Object obj2);

    public abstract Calendar a(Date date);

    public abstract Date a(String str) throws IllegalArgumentException;

    public abstract void a(aeh aeh);

    public abstract boolean a(ow owVar, qu<?> quVar, Object obj, String str) throws IOException, oz;

    public abstract qw b(Class<?> cls);

    public abstract qw b(Class<?> cls, String str);

    public abstract qw c(Class<?> cls, String str);

    public abstract ow d();

    public abstract aeh g();

    public abstract adp h();

    protected qm(qk qkVar) {
        this.a = qkVar;
        this.b = qkVar.i;
    }

    public qk a() {
        return this.a;
    }

    public qq b() {
        return null;
    }

    public boolean a(ql qlVar) {
        return (this.b & qlVar.b()) != 0;
    }

    public on c() {
        return this.a.g();
    }

    public final aez e() {
        return this.a.h();
    }

    public afm a(Class<?> cls) {
        return this.a.b(cls);
    }

    public adk f() {
        return this.a.m();
    }

    public qw b(String str) {
        return qw.a(d(), str);
    }
}
