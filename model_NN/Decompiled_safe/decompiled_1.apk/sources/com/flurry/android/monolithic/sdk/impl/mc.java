package com.flurry.android.monolithic.sdk.impl;

import java.io.Flushable;
import java.io.IOException;
import java.nio.ByteBuffer;

public abstract class mc implements Flushable {
    public abstract void a() throws IOException;

    public abstract void a(double d) throws IOException;

    public abstract void a(float f) throws IOException;

    public abstract void a(int i) throws IOException;

    public abstract void a(long j) throws IOException;

    public abstract void a(nw nwVar) throws IOException;

    public abstract void a(ByteBuffer byteBuffer) throws IOException;

    public abstract void a(boolean z) throws IOException;

    public abstract void a(byte[] bArr, int i, int i2) throws IOException;

    public abstract void b() throws IOException;

    public abstract void b(int i) throws IOException;

    public abstract void b(long j) throws IOException;

    public abstract void b(byte[] bArr, int i, int i2) throws IOException;

    public abstract void c() throws IOException;

    public abstract void c(int i) throws IOException;

    public abstract void d() throws IOException;

    public abstract void e() throws IOException;

    public abstract void f() throws IOException;

    public void a(String str) throws IOException {
        a(new nw(str));
    }

    public void a(CharSequence charSequence) throws IOException {
        if (charSequence instanceof nw) {
            a((nw) charSequence);
        } else {
            a(charSequence.toString());
        }
    }

    public void a(byte[] bArr) throws IOException {
        a(bArr, 0, bArr.length);
    }

    public void b(byte[] bArr) throws IOException {
        b(bArr, 0, bArr.length);
    }
}
