package com.flurry.android.monolithic.sdk.impl;

import java.lang.reflect.Constructor;
import java.lang.reflect.Member;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

public final class xi extends xo {
    protected final Constructor<?> a;

    public xi(Constructor<?> constructor, xp xpVar, xp[] xpVarArr) {
        super(xpVar, xpVarArr);
        if (constructor == null) {
            throw new IllegalArgumentException("Null constructor not allowed");
        }
        this.a = constructor;
    }

    /* renamed from: e */
    public Constructor<?> a() {
        return this.a;
    }

    public String b() {
        return this.a.getName();
    }

    public Type c() {
        return d();
    }

    public Class<?> d() {
        return this.a.getDeclaringClass();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.xo.a(com.flurry.android.monolithic.sdk.impl.adj, java.lang.reflect.TypeVariable<?>[]):com.flurry.android.monolithic.sdk.impl.afm
     arg types: [com.flurry.android.monolithic.sdk.impl.adj, java.lang.reflect.TypeVariable[]]
     candidates:
      com.flurry.android.monolithic.sdk.impl.xi.a(java.lang.Object, java.lang.Object):void
      com.flurry.android.monolithic.sdk.impl.xo.a(int, com.flurry.android.monolithic.sdk.impl.xp):com.flurry.android.monolithic.sdk.impl.xn
      com.flurry.android.monolithic.sdk.impl.xo.a(int, java.lang.annotation.Annotation):void
      com.flurry.android.monolithic.sdk.impl.xk.a(java.lang.Object, java.lang.Object):void
      com.flurry.android.monolithic.sdk.impl.xo.a(com.flurry.android.monolithic.sdk.impl.adj, java.lang.reflect.TypeVariable<?>[]):com.flurry.android.monolithic.sdk.impl.afm */
    public afm a(adj adj) {
        return a(adj, (TypeVariable<?>[]) this.a.getTypeParameters());
    }

    public int f() {
        return this.a.getParameterTypes().length;
    }

    public Class<?> a(int i) {
        Class<?>[] parameterTypes = this.a.getParameterTypes();
        if (i >= parameterTypes.length) {
            return null;
        }
        return parameterTypes[i];
    }

    public Type b(int i) {
        Type[] genericParameterTypes = this.a.getGenericParameterTypes();
        if (i >= genericParameterTypes.length) {
            return null;
        }
        return genericParameterTypes[i];
    }

    public final Object g() throws Exception {
        return this.a.newInstance(new Object[0]);
    }

    public final Object a(Object[] objArr) throws Exception {
        return this.a.newInstance(objArr);
    }

    public final Object a(Object obj) throws Exception {
        return this.a.newInstance(obj);
    }

    public Class<?> h() {
        return this.a.getDeclaringClass();
    }

    public Member i() {
        return this.a;
    }

    public void a(Object obj, Object obj2) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Cannot call setValue() on constructor of " + h().getName());
    }

    public String toString() {
        return "[constructor for " + b() + ", annotations: " + this.b + "]";
    }
}
