package com.flurry.android.monolithic.sdk.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;

public class sr {
    protected final xq a;
    protected final HashMap<String, sw> b = new LinkedHashMap();
    protected List<ty> c;
    protected HashMap<String, sw> d;
    protected HashSet<String> e;
    protected th f;
    protected sv g;
    protected boolean h;

    public sr(xq xqVar) {
        this.a = xqVar;
    }

    public void a(sw swVar, boolean z) {
        this.b.put(swVar.c(), swVar);
    }

    public void a(sw swVar) {
        sw put = this.b.put(swVar.c(), swVar);
        if (put != null && put != swVar) {
            throw new IllegalArgumentException("Duplicate property '" + swVar.c() + "' for " + this.a.a());
        }
    }

    public void a(String str, sw swVar) {
        if (this.d == null) {
            this.d = new HashMap<>(4);
        }
        this.d.put(str, swVar);
        if (this.b != null) {
            this.b.remove(swVar.c());
        }
    }

    public void a(String str, afm afm, ado ado, xk xkVar, Object obj) {
        if (this.c == null) {
            this.c = new ArrayList();
        }
        this.c.add(new ty(str, afm, ado, xkVar, obj));
    }

    public void a(String str) {
        if (this.e == null) {
            this.e = new HashSet<>();
        }
        this.e.add(str);
    }

    public void a(qe qeVar) {
    }

    public void a(sv svVar) {
        if (this.g == null || svVar == null) {
            this.g = svVar;
            return;
        }
        throw new IllegalStateException("_anySetter already set to non-null");
    }

    public void a(boolean z) {
        this.h = z;
    }

    public void a(th thVar) {
        this.f = thVar;
    }

    public boolean b(String str) {
        return this.b.containsKey(str);
    }

    public qu<?> a(qc qcVar) {
        tj tjVar = new tj(this.b.values());
        tjVar.a();
        return new sp(this.a, qcVar, this.f, tjVar, this.d, this.e, this.h, this.g, this.c);
    }
}
