package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import com.flurry.android.impl.ads.FlurryAdModule;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public final class de implements co {
    private static final co a = new cs(a());

    public cn a_(Context context, FlurryAdModule flurryAdModule, m mVar, AdUnit adUnit) {
        return a.a_(context, flurryAdModule, mVar, adUnit);
    }

    private static Map<String, co> a() {
        HashMap hashMap = new HashMap();
        hashMap.put("Admob".toUpperCase(Locale.US), new df());
        hashMap.put("Millennial Media".toUpperCase(Locale.US), new dw());
        hashMap.put("InMobi".toUpperCase(Locale.US), new dm());
        hashMap.put("Mobclix".toUpperCase(Locale.US), new eb());
        hashMap.put("Jumptap".toUpperCase(Locale.US), new dr());
        return Collections.unmodifiableMap(hashMap);
    }
}
