package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.lang.ref.SoftReference;

public class op {
    static final int a = ox.a();
    static final int b = os.a();
    protected static final ThreadLocal<SoftReference<afn>> c = new ThreadLocal<>();
    protected afj d;
    protected afh e;
    protected pc f;
    protected int g;
    protected int h;
    protected pp i;
    protected pr j;
    protected pv k;

    public op() {
        this(null);
    }

    public op(pc pcVar) {
        this.d = afj.a();
        this.e = afh.a();
        this.g = a;
        this.h = b;
        this.f = pcVar;
    }

    public op a(ox oxVar) {
        this.g |= oxVar.c();
        return this;
    }

    public final boolean b(ox oxVar) {
        return (this.g & oxVar.c()) != 0;
    }

    public op a(pc pcVar) {
        this.f = pcVar;
        return this;
    }

    public pc a() {
        return this.f;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.op.a(java.lang.Object, boolean):com.flurry.android.monolithic.sdk.impl.pq
     arg types: [java.io.Reader, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.op.a(java.io.Writer, com.flurry.android.monolithic.sdk.impl.pq):com.flurry.android.monolithic.sdk.impl.or
      com.flurry.android.monolithic.sdk.impl.op.a(java.io.Reader, com.flurry.android.monolithic.sdk.impl.pq):com.flurry.android.monolithic.sdk.impl.ow
      com.flurry.android.monolithic.sdk.impl.op.a(java.lang.Object, boolean):com.flurry.android.monolithic.sdk.impl.pq */
    public ow a(Reader reader) throws IOException, ov {
        Reader reader2;
        pq a2 = a((Object) reader, false);
        if (this.j != null) {
            reader2 = this.j.a(a2, reader);
        } else {
            reader2 = reader;
        }
        return a(reader2, a2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.op.a(java.lang.Object, boolean):com.flurry.android.monolithic.sdk.impl.pq
     arg types: [java.io.Writer, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.op.a(java.io.Writer, com.flurry.android.monolithic.sdk.impl.pq):com.flurry.android.monolithic.sdk.impl.or
      com.flurry.android.monolithic.sdk.impl.op.a(java.io.Reader, com.flurry.android.monolithic.sdk.impl.pq):com.flurry.android.monolithic.sdk.impl.ow
      com.flurry.android.monolithic.sdk.impl.op.a(java.lang.Object, boolean):com.flurry.android.monolithic.sdk.impl.pq */
    public or a(Writer writer) throws IOException {
        Writer writer2;
        pq a2 = a((Object) writer, false);
        if (this.k != null) {
            writer2 = this.k.a(a2, writer);
        } else {
            writer2 = writer;
        }
        return a(writer2, a2);
    }

    /* access modifiers changed from: protected */
    public ow a(Reader reader, pq pqVar) throws IOException, ov {
        return new pm(pqVar, this.g, reader, this.f, this.d.a(b(ox.CANONICALIZE_FIELD_NAMES), b(ox.INTERN_FIELD_NAMES)));
    }

    /* access modifiers changed from: protected */
    public or a(Writer writer, pq pqVar) throws IOException {
        po poVar = new po(pqVar, this.h, this.f, writer);
        if (this.i != null) {
            poVar.a(this.i);
        }
        return poVar;
    }

    /* access modifiers changed from: protected */
    public pq a(Object obj, boolean z) {
        return new pq(b(), obj, z);
    }

    public afn b() {
        SoftReference softReference = c.get();
        afn afn = softReference == null ? null : (afn) softReference.get();
        if (afn != null) {
            return afn;
        }
        afn afn2 = new afn();
        c.set(new SoftReference(afn2));
        return afn2;
    }
}
