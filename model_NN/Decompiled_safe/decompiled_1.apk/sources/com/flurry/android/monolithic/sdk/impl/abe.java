package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.EnumMap;
import java.util.Map;

@rz
public class abe extends abc<EnumMap<? extends Enum<?>, ?>> implements rp {
    protected final boolean a;
    protected final aee b;
    protected final afm c;
    protected final qc d;
    protected ra<Object> e;
    protected final rx f;

    public /* bridge */ /* synthetic */ void a(Object obj, or orVar, ru ruVar) throws IOException, oq {
        a((EnumMap<? extends Enum<?>, ?>) ((EnumMap) obj), orVar, ruVar);
    }

    public /* bridge */ /* synthetic */ void a(Object obj, or orVar, ru ruVar, rx rxVar) throws IOException, oz {
        a((EnumMap<? extends Enum<?>, ?>) ((EnumMap) obj), orVar, ruVar, rxVar);
    }

    public abe(afm afm, boolean z, aee aee, rx rxVar, qc qcVar, ra<Object> raVar) {
        super(EnumMap.class, false);
        this.a = z || (afm != null && afm.u());
        this.c = afm;
        this.b = aee;
        this.f = rxVar;
        this.d = qcVar;
        this.e = raVar;
    }

    public abc<?> a(rx rxVar) {
        return new abe(this.c, this.a, this.b, rxVar, this.d, this.e);
    }

    public void a(EnumMap<? extends Enum<?>, ?> enumMap, or orVar, ru ruVar) throws IOException, oq {
        orVar.d();
        if (!enumMap.isEmpty()) {
            b(enumMap, orVar, ruVar);
        }
        orVar.e();
    }

    public void a(EnumMap<? extends Enum<?>, ?> enumMap, or orVar, ru ruVar, rx rxVar) throws IOException, oq {
        rxVar.b(enumMap, orVar);
        if (!enumMap.isEmpty()) {
            b(enumMap, orVar, ruVar);
        }
        rxVar.e(enumMap, orVar);
    }

    /* access modifiers changed from: protected */
    public void b(EnumMap<? extends Enum<?>, ?> enumMap, or orVar, ru ruVar) throws IOException, oq {
        ra<Object> a2;
        Class<?> cls;
        ra<Object> raVar;
        Class<?> cls2;
        ra<Object> raVar2;
        if (this.e != null) {
            a(enumMap, orVar, ruVar, this.e);
            return;
        }
        aee aee = this.b;
        Class<?> cls3 = null;
        ra<Object> raVar3 = null;
        aee aee2 = aee;
        for (Map.Entry next : enumMap.entrySet()) {
            Enum enumR = (Enum) next.getKey();
            if (aee2 == null) {
                aee2 = ((abf) ((abt) ruVar.a(enumR.getDeclaringClass(), this.d))).d();
            }
            orVar.a(aee2.a(enumR));
            Object value = next.getValue();
            if (value == null) {
                ruVar.a(orVar);
                cls2 = cls3;
                raVar2 = raVar3;
            } else {
                Class<?> cls4 = value.getClass();
                if (cls4 == cls3) {
                    raVar = raVar3;
                    Class<?> cls5 = cls3;
                    a2 = raVar3;
                    cls = cls5;
                } else {
                    a2 = ruVar.a(cls4, this.d);
                    cls = cls4;
                    raVar = a2;
                }
                try {
                    a2.a(value, orVar, ruVar);
                    cls2 = cls;
                    raVar2 = raVar;
                } catch (Exception e2) {
                    a(ruVar, e2, enumMap, ((Enum) next.getKey()).name());
                    cls2 = cls;
                    raVar2 = raVar;
                }
            }
            cls3 = cls2;
            raVar3 = raVar2;
        }
    }

    /* access modifiers changed from: protected */
    public void a(EnumMap<? extends Enum<?>, ?> enumMap, or orVar, ru ruVar, ra<Object> raVar) throws IOException, oq {
        aee aee = this.b;
        aee aee2 = aee;
        for (Map.Entry next : enumMap.entrySet()) {
            Enum enumR = (Enum) next.getKey();
            if (aee2 == null) {
                aee2 = ((abf) ((abt) ruVar.a(enumR.getDeclaringClass(), this.d))).d();
            }
            orVar.a(aee2.a(enumR));
            Object value = next.getValue();
            if (value == null) {
                ruVar.a(orVar);
            } else {
                try {
                    raVar.a(value, orVar, ruVar);
                } catch (Exception e2) {
                    a(ruVar, e2, enumMap, ((Enum) next.getKey()).name());
                }
            }
        }
    }

    public void a(ru ruVar) throws qw {
        if (this.a && this.e == null) {
            this.e = ruVar.a(this.c, this.d);
        }
    }
}
