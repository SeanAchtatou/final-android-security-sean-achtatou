package com.flurry.android.monolithic.sdk.impl;

final class ws extends we {
    private static final ws b = new ws(String.class);
    private static final ws c = new ws(Object.class);

    private ws(Class<?> cls) {
        super(cls);
    }

    public static ws a(Class<?> cls) {
        if (cls == String.class) {
            return b;
        }
        if (cls == Object.class) {
            return c;
        }
        return new ws(cls);
    }

    /* renamed from: c */
    public String b(String str, qm qmVar) throws qw {
        return str;
    }
}
