package com.flurry.android.monolithic.sdk.impl;

public final class tb {
    private final Object a;
    private final boolean b;
    private final Class<?> c;

    protected tb(afm afm, Object obj) {
        this.a = obj;
        this.b = afm.t();
        this.c = afm.p();
    }

    public Object a(qm qmVar) throws oz {
        if (!this.b || !qmVar.a(ql.FAIL_ON_NULL_FOR_PRIMITIVES)) {
            return this.a;
        }
        throw qmVar.b("Can not map JSON null into type " + this.c.getName() + " (set DeserializationConfig.Feature.FAIL_ON_NULL_FOR_PRIMITIVES to 'false' to allow)");
    }
}
