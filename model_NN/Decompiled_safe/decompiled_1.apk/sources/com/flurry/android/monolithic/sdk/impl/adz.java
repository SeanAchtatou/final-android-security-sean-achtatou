package com.flurry.android.monolithic.sdk.impl;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Constructor;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;

public final class adz {
    public static List<Class<?>> a(Class<?> cls, Class<?> cls2) {
        return a(cls, cls2, new ArrayList(8));
    }

    public static List<Class<?>> a(Class<?> cls, Class<?> cls2, List<Class<?>> list) {
        a(cls, cls2, list, false);
        return list;
    }

    private static void a(Class<?> cls, Class<?> cls2, Collection<Class<?>> collection, boolean z) {
        if (cls != cls2 && cls != null && cls != Object.class) {
            if (z) {
                if (!collection.contains(cls)) {
                    collection.add(cls);
                } else {
                    return;
                }
            }
            for (Class<?> a : cls.getInterfaces()) {
                a(a, cls2, collection, true);
            }
            a(cls.getSuperclass(), cls2, collection, true);
        }
    }

    public static String a(Class<?> cls) {
        if (cls.isAnnotation()) {
            return "annotation";
        }
        if (cls.isArray()) {
            return "array";
        }
        if (cls.isEnum()) {
            return "enum";
        }
        if (cls.isPrimitive()) {
            return "primitive";
        }
        return null;
    }

    public static String a(Class<?> cls, boolean z) {
        try {
            if (cls.getEnclosingMethod() != null) {
                return "local/anonymous";
            }
            if (z || cls.getEnclosingClass() == null || Modifier.isStatic(cls.getModifiers())) {
                return null;
            }
            return "non-static member class";
        } catch (NullPointerException | SecurityException e) {
        }
    }

    public static Class<?> b(Class<?> cls) {
        try {
            if (cls.getEnclosingMethod() != null) {
                return null;
            }
            if (!Modifier.isStatic(cls.getModifiers())) {
                return cls.getEnclosingClass();
            }
            return null;
        } catch (NullPointerException | SecurityException e) {
        }
    }

    public static boolean c(Class<?> cls) {
        if (Proxy.isProxyClass(cls)) {
            return true;
        }
        String name = cls.getName();
        if (name.startsWith("net.sf.cglib.proxy.") || name.startsWith("org.hibernate.proxy.")) {
            return true;
        }
        return false;
    }

    public static boolean d(Class<?> cls) {
        return (cls.getModifiers() & 1536) == 0;
    }

    public static boolean e(Class<?> cls) {
        if (cls.isArray()) {
            return true;
        }
        if (Collection.class.isAssignableFrom(cls)) {
            return true;
        }
        if (Map.class.isAssignableFrom(cls)) {
            return true;
        }
        return false;
    }

    public static boolean a(Method method) {
        if (Modifier.isStatic(method.getModifiers())) {
            return false;
        }
        Class<?>[] parameterTypes = method.getParameterTypes();
        if (parameterTypes != null && parameterTypes.length != 0) {
            return false;
        }
        if (Void.TYPE == method.getReturnType()) {
            return false;
        }
        return true;
    }

    public static Throwable a(Throwable th) {
        Throwable th2 = th;
        while (th2.getCause() != null) {
            th2 = th2.getCause();
        }
        return th2;
    }

    public static void b(Throwable th) {
        a(th, th.getMessage());
    }

    public static void a(Throwable th, String str) {
        if (th instanceof RuntimeException) {
            throw ((RuntimeException) th);
        } else if (th instanceof Error) {
            throw ((Error) th);
        } else {
            throw new IllegalArgumentException(str, th);
        }
    }

    public static void c(Throwable th) {
        b(a(th));
    }

    public static void b(Throwable th, String str) {
        a(a(th), str);
    }

    public static <T> T b(Class cls, boolean z) throws IllegalArgumentException {
        Constructor c = c(cls, z);
        if (c == null) {
            throw new IllegalArgumentException("Class " + cls.getName() + " has no default (no arg) constructor");
        }
        try {
            return c.newInstance(new Object[0]);
        } catch (Exception e) {
            b(e, "Failed to instantiate class " + cls.getName() + ", problem: " + e.getMessage());
            return null;
        }
    }

    public static <T> Constructor<T> c(Class<T> cls, boolean z) throws IllegalArgumentException {
        try {
            Constructor<T> declaredConstructor = cls.getDeclaredConstructor(new Class[0]);
            if (z) {
                a(declaredConstructor);
                return declaredConstructor;
            } else if (Modifier.isPublic(declaredConstructor.getModifiers())) {
                return declaredConstructor;
            } else {
                throw new IllegalArgumentException("Default constructor for " + cls.getName() + " is not accessible (non-public?): not allowed to try modify access via Reflection: can not instantiate type");
            }
        } catch (NoSuchMethodException e) {
            return null;
        } catch (Exception e2) {
            b(e2, "Failed to find default constructor of class " + cls.getName() + ", problem: " + e2.getMessage());
            return null;
        }
    }

    public static Object f(Class<?> cls) {
        if (cls == Integer.TYPE) {
            return 0;
        }
        if (cls == Long.TYPE) {
            return 0L;
        }
        if (cls == Boolean.TYPE) {
            return Boolean.FALSE;
        }
        if (cls == Double.TYPE) {
            return Double.valueOf(0.0d);
        }
        if (cls == Float.TYPE) {
            return Float.valueOf(0.0f);
        }
        if (cls == Byte.TYPE) {
            return (byte) 0;
        }
        if (cls == Short.TYPE) {
            return (short) 0;
        }
        if (cls == Character.TYPE) {
            return 0;
        }
        throw new IllegalArgumentException("Class " + cls.getName() + " is not a primitive type");
    }

    public static void a(Member member) {
        AccessibleObject accessibleObject = (AccessibleObject) member;
        try {
            accessibleObject.setAccessible(true);
        } catch (SecurityException e) {
            if (!accessibleObject.isAccessible()) {
                throw new IllegalArgumentException("Can not access " + member + " (from class " + member.getDeclaringClass().getName() + "; failed to set access: " + e.getMessage());
            }
        }
    }

    public static Class<? extends Enum<?>> a(EnumSet<?> enumSet) {
        if (!enumSet.isEmpty()) {
            return a((Enum) enumSet.iterator().next());
        }
        return aea.a.a(enumSet);
    }

    public static Class<? extends Enum<?>> a(EnumMap<?, ?> enumMap) {
        if (!enumMap.isEmpty()) {
            return a((Enum) enumMap.keySet().iterator().next());
        }
        return aea.a.a(enumMap);
    }

    public static Class<? extends Enum<?>> a(Enum<?> enumR) {
        Class cls = enumR.getClass();
        if (cls.getSuperclass() != Enum.class) {
            return cls.getSuperclass();
        }
        return cls;
    }

    public static Class<? extends Enum<?>> g(Class<?> cls) {
        if (cls.getSuperclass() != Enum.class) {
            return cls.getSuperclass();
        }
        return cls;
    }
}
