package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.nio.ByteBuffer;

public class mi extends mg implements mk {
    protected lx c;

    mi(mq mqVar, lx lxVar) throws IOException {
        super(mqVar);
        a(lxVar);
    }

    public mi a(lx lxVar) throws IOException {
        this.a.e();
        this.c = lxVar;
        return this;
    }

    public void b() throws IOException {
        this.a.a(mq.c);
        this.c.b();
    }

    public boolean c() throws IOException {
        this.a.a(mq.d);
        return this.c.c();
    }

    public int d() throws IOException {
        this.a.a(mq.e);
        return this.c.d();
    }

    public long e() throws IOException {
        this.a.a(mq.f);
        return this.c.e();
    }

    public float f() throws IOException {
        this.a.a(mq.g);
        return this.c.f();
    }

    public double g() throws IOException {
        this.a.a(mq.h);
        return this.c.g();
    }

    public nw a(nw nwVar) throws IOException {
        this.a.a(mq.i);
        return this.c.a(nwVar);
    }

    public String h() throws IOException {
        this.a.a(mq.i);
        return this.c.h();
    }

    public void i() throws IOException {
        this.a.a(mq.i);
        this.c.i();
    }

    public ByteBuffer a(ByteBuffer byteBuffer) throws IOException {
        this.a.a(mq.j);
        return this.c.a(byteBuffer);
    }

    public void j() throws IOException {
        this.a.a(mq.j);
        this.c.j();
    }

    private void b(int i) throws IOException {
        this.a.a(mq.k);
        mz mzVar = (mz) this.a.c();
        if (i != mzVar.A) {
            throw new jh("Incorrect length for fixed binary: expected " + mzVar.A + " but received " + i + " bytes.");
        }
    }

    public void b(byte[] bArr, int i, int i2) throws IOException {
        b(i2);
        this.c.b(bArr, i, i2);
    }

    public void a(int i) throws IOException {
        b(i);
        this.c.a(i);
    }

    /* access modifiers changed from: protected */
    public void a() throws IOException {
        this.a.a(mq.k);
        this.c.a(((mz) this.a.c()).A);
    }

    public int k() throws IOException {
        this.a.a(mq.l);
        mz mzVar = (mz) this.a.c();
        int k = this.c.k();
        if (k >= 0 && k < mzVar.A) {
            return k;
        }
        throw new jh("Enumeration out of range: max is " + mzVar.A + " but received " + k);
    }

    public long m() throws IOException {
        this.a.a(mq.n);
        long m = this.c.m();
        if (m == 0) {
            this.a.a(mq.o);
        }
        return m;
    }

    public long n() throws IOException {
        this.a.b();
        long n = this.c.n();
        if (n == 0) {
            this.a.a(mq.o);
        }
        return n;
    }

    public long o() throws IOException {
        this.a.a(mq.n);
        long o = this.c.o();
        while (o != 0) {
            while (true) {
                long j = o - 1;
                if (o <= 0) {
                    break;
                }
                this.a.f();
                o = j;
            }
            o = this.c.o();
        }
        this.a.a(mq.o);
        return 0;
    }

    public long p() throws IOException {
        this.a.a(mq.p);
        long p = this.c.p();
        if (p == 0) {
            this.a.a(mq.q);
        }
        return p;
    }

    public long q() throws IOException {
        this.a.b();
        long q = this.c.q();
        if (q == 0) {
            this.a.a(mq.q);
        }
        return q;
    }

    public long r() throws IOException {
        this.a.a(mq.p);
        long r = this.c.r();
        while (r != 0) {
            while (true) {
                long j = r - 1;
                if (r <= 0) {
                    break;
                }
                this.a.f();
                r = j;
            }
            r = this.c.r();
        }
        this.a.a(mq.q);
        return 0;
    }

    public int s() throws IOException {
        this.a.a(mq.m);
        int s = this.c.s();
        this.a.c(((ms) this.a.c()).a(s));
        return s;
    }

    public mq a(mq mqVar, mq mqVar2) throws IOException {
        return null;
    }
}
