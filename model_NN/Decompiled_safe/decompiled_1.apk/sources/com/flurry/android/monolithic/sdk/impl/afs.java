package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public class afs implements pd {
    protected pf a = new aft();
    protected pf b = new afu();
    protected boolean c = true;
    protected int d = 0;

    public void a(or orVar) throws IOException, oq {
        orVar.a(' ');
    }

    public void b(or orVar) throws IOException, oq {
        orVar.a('{');
        if (!this.b.a()) {
            this.d++;
        }
    }

    public void h(or orVar) throws IOException, oq {
        this.b.a(orVar, this.d);
    }

    public void d(or orVar) throws IOException, oq {
        if (this.c) {
            orVar.c(" : ");
        } else {
            orVar.a(':');
        }
    }

    public void c(or orVar) throws IOException, oq {
        orVar.a(',');
        this.b.a(orVar, this.d);
    }

    public void a(or orVar, int i) throws IOException, oq {
        if (!this.b.a()) {
            this.d--;
        }
        if (i > 0) {
            this.b.a(orVar, this.d);
        } else {
            orVar.a(' ');
        }
        orVar.a('}');
    }

    public void e(or orVar) throws IOException, oq {
        if (!this.a.a()) {
            this.d++;
        }
        orVar.a('[');
    }

    public void g(or orVar) throws IOException, oq {
        this.a.a(orVar, this.d);
    }

    public void f(or orVar) throws IOException, oq {
        orVar.a(',');
        this.a.a(orVar, this.d);
    }

    public void b(or orVar, int i) throws IOException, oq {
        if (!this.a.a()) {
            this.d--;
        }
        if (i > 0) {
            this.a.a(orVar, this.d);
        } else {
            orVar.a(' ');
        }
        orVar.a(']');
    }
}
