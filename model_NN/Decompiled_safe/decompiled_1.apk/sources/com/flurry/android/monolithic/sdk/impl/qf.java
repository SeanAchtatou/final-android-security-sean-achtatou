package com.flurry.android.monolithic.sdk.impl;

import com.flurry.android.monolithic.sdk.impl.qb;

public abstract class qf<T extends qb> {
    public abstract T a(qk qkVar, afm afm, qg qgVar);

    public abstract T a(rf<?> rfVar, afm afm, qg qgVar);

    public abstract T a(rq rqVar, afm afm, qg qgVar);

    public abstract T b(qk qkVar, afm afm, qg qgVar);

    protected qf() {
    }
}
