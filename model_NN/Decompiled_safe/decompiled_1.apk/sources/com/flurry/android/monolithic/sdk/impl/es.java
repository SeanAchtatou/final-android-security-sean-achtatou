package com.flurry.android.monolithic.sdk.impl;

class es implements Runnable {
    final /* synthetic */ int a;
    final /* synthetic */ String b;
    final /* synthetic */ String c;
    final /* synthetic */ em d;

    es(em emVar, int i, String str, String str2) {
        this.d = emVar;
        this.a = i;
        this.b = str;
        this.c = str2;
    }

    public void run() {
        fb d2;
        if (this.a == 200 && (d2 = eg.a().d()) != null) {
            d2.b();
        }
        this.d.i();
        if (!this.d.d.a(this.b, this.c)) {
            ja.a(6, em.f, "Internal error. Block wasn't deleted with id = " + this.b);
        }
        if (!this.d.e.remove(this.b)) {
            ja.a(6, em.f, "Internal error. Block with id = " + this.b + " was not in progress state");
        }
    }
}
