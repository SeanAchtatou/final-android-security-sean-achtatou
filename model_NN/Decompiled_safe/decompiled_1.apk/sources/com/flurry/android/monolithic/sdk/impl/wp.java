package com.flurry.android.monolithic.sdk.impl;

final class wp extends we {
    wp() {
        super(Integer.class);
    }

    /* renamed from: c */
    public Short b(String str, qm qmVar) throws qw {
        int a = a(str);
        if (a >= -32768 && a <= 32767) {
            return Short.valueOf((short) a);
        }
        throw qmVar.a(this.a, str, "overflow, value can not be represented as 16-bit value");
    }
}
