package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Map;

@rz
public class vb extends ug<Map<Object, Object>> implements ro {
    protected final afm a;
    protected final rc b;
    protected final qu<Object> c;
    protected final rw d;
    protected final th e;
    protected final boolean f;
    protected tr g;
    protected qu<Object> h;
    protected HashSet<String> i;

    public /* bridge */ /* synthetic */ Object a(ow owVar, qm qmVar, Object obj) throws IOException, oz {
        return a(owVar, qmVar, (Map<Object, Object>) ((Map) obj));
    }

    public vb(afm afm, th thVar, rc rcVar, qu<Object> quVar, rw rwVar) {
        super(Map.class);
        this.a = afm;
        this.b = rcVar;
        this.c = quVar;
        this.d = rwVar;
        this.e = thVar;
        if (thVar.j()) {
            this.g = new tr(thVar);
        } else {
            this.g = null;
        }
        this.f = thVar.h();
    }

    public void a(String[] strArr) {
        this.i = (strArr == null || strArr.length == 0) ? null : adp.a(strArr);
    }

    public void a(qk qkVar, qq qqVar) throws qw {
        if (this.e.i()) {
            afm l = this.e.l();
            if (l == null) {
                throw new IllegalArgumentException("Invalid delegate-creator definition for " + this.a + ": value instantiator (" + this.e.getClass().getName() + ") returned true for 'canCreateUsingDelegate()', but null for 'getDelegateType()'");
            }
            this.h = a(qkVar, qqVar, l, new qd(null, l, null, this.e.o()));
        }
        if (this.g != null) {
            for (sw next : this.g.a()) {
                if (!next.f()) {
                    this.g.a(next, a(qkVar, qqVar, next.a(), next));
                }
            }
        }
    }

    public qu<Object> d() {
        return this.c;
    }

    /* renamed from: b */
    public Map<Object, Object> a(ow owVar, qm qmVar) throws IOException, oz {
        if (this.g != null) {
            return c(owVar, qmVar);
        }
        if (this.h != null) {
            return (Map) this.e.a(this.h.a(owVar, qmVar));
        }
        if (!this.f) {
            throw qmVar.a(e(), "No default constructor found");
        }
        pb e2 = owVar.e();
        if (e2 == pb.START_OBJECT || e2 == pb.FIELD_NAME || e2 == pb.END_OBJECT) {
            Map<Object, Object> map = (Map) this.e.m();
            b(owVar, qmVar, map);
            return map;
        } else if (e2 == pb.VALUE_STRING) {
            return (Map) this.e.a(owVar.k());
        } else {
            throw qmVar.b(e());
        }
    }

    public Map<Object, Object> a(ow owVar, qm qmVar, Map<Object, Object> map) throws IOException, oz {
        pb e2 = owVar.e();
        if (e2 == pb.START_OBJECT || e2 == pb.FIELD_NAME) {
            b(owVar, qmVar, map);
            return map;
        }
        throw qmVar.b(e());
    }

    public Object a(ow owVar, qm qmVar, rw rwVar) throws IOException, oz {
        return rwVar.a(owVar, qmVar);
    }

    public final Class<?> e() {
        return this.a.p();
    }

    /* access modifiers changed from: protected */
    public final void b(ow owVar, qm qmVar, Map<Object, Object> map) throws IOException, oz {
        Object a2;
        pb e2 = owVar.e();
        if (e2 == pb.START_OBJECT) {
            e2 = owVar.b();
        }
        rc rcVar = this.b;
        qu<Object> quVar = this.c;
        rw rwVar = this.d;
        while (e2 == pb.FIELD_NAME) {
            String g2 = owVar.g();
            Object a3 = rcVar.a(g2, qmVar);
            pb b2 = owVar.b();
            if (this.i == null || !this.i.contains(g2)) {
                if (b2 == pb.VALUE_NULL) {
                    a2 = null;
                } else if (rwVar == null) {
                    a2 = quVar.a(owVar, qmVar);
                } else {
                    a2 = quVar.a(owVar, qmVar, rwVar);
                }
                map.put(a3, a2);
            } else {
                owVar.d();
            }
            e2 = owVar.b();
        }
    }

    public Map<Object, Object> c(ow owVar, qm qmVar) throws IOException, oz {
        Object a2;
        tr trVar = this.g;
        tw a3 = trVar.a(owVar, qmVar);
        pb e2 = owVar.e();
        if (e2 == pb.START_OBJECT) {
            e2 = owVar.b();
        }
        qu<Object> quVar = this.c;
        rw rwVar = this.d;
        while (e2 == pb.FIELD_NAME) {
            String g2 = owVar.g();
            pb b2 = owVar.b();
            if (this.i == null || !this.i.contains(g2)) {
                sw a4 = trVar.a(g2);
                if (a4 != null) {
                    if (a3.a(a4.j(), a4.a(owVar, qmVar))) {
                        owVar.b();
                        try {
                            Map<Object, Object> map = (Map) trVar.a(a3);
                            b(owVar, qmVar, map);
                            return map;
                        } catch (Exception e3) {
                            a(e3, this.a.p());
                            return null;
                        }
                    }
                } else {
                    Object a5 = this.b.a(owVar.g(), qmVar);
                    if (b2 == pb.VALUE_NULL) {
                        a2 = null;
                    } else if (rwVar == null) {
                        a2 = quVar.a(owVar, qmVar);
                    } else {
                        a2 = quVar.a(owVar, qmVar, rwVar);
                    }
                    a3.a(a5, a2);
                }
            } else {
                owVar.d();
            }
            e2 = owVar.b();
        }
        try {
            return (Map) trVar.a(a3);
        } catch (Exception e4) {
            this.a(e4, this.a.p());
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void a(Throwable th, Object obj) throws IOException {
        Throwable th2 = th;
        while ((th2 instanceof InvocationTargetException) && th2.getCause() != null) {
            th2 = th2.getCause();
        }
        if (th2 instanceof Error) {
            throw ((Error) th2);
        } else if (!(th2 instanceof IOException) || (th2 instanceof qw)) {
            throw qw.a(th2, obj, (String) null);
        } else {
            throw ((IOException) th2);
        }
    }
}
