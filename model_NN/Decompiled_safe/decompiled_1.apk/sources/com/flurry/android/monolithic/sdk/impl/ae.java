package com.flurry.android.monolithic.sdk.impl;

import com.flurry.android.impl.ads.FlurryAdModule;
import com.flurry.android.impl.ads.avro.protocol.v6.SdkAdEvent;
import com.flurry.android.impl.ads.avro.protocol.v6.SdkAdLog;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

public class ae {
    private final FlurryAdModule a;

    public ae(FlurryAdModule flurryAdModule) {
        this.a = flurryAdModule;
    }

    public List<SdkAdLog> a(List<m> list) {
        CopyOnWriteArrayList copyOnWriteArrayList = new CopyOnWriteArrayList();
        for (m next : list) {
            SdkAdLog sdkAdLog = new SdkAdLog();
            sdkAdLog.a(Long.valueOf(next.c()));
            sdkAdLog.a(next.b() == null ? "" : next.b());
            CopyOnWriteArrayList copyOnWriteArrayList2 = new CopyOnWriteArrayList();
            synchronized (next) {
                for (k next2 : next.d()) {
                    if (next2.b()) {
                        SdkAdEvent sdkAdEvent = new SdkAdEvent();
                        sdkAdEvent.a(next2.a());
                        sdkAdEvent.a(Long.valueOf(next2.c()));
                        Map<String, String> d = next2.d();
                        HashMap hashMap = new HashMap();
                        for (Map.Entry next3 : d.entrySet()) {
                            hashMap.put(next3.getKey(), next3.getValue());
                        }
                        sdkAdEvent.a(hashMap);
                        copyOnWriteArrayList2.add(sdkAdEvent);
                    }
                }
            }
            sdkAdLog.a(copyOnWriteArrayList2);
            copyOnWriteArrayList.add(sdkAdLog);
        }
        this.a.a(list);
        return copyOnWriteArrayList;
    }
}
