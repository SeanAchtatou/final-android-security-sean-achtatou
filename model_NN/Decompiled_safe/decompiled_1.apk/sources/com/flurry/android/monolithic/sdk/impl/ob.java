package com.flurry.android.monolithic.sdk.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class ob implements Iterable<og> {
    private final List<og> a = new LinkedList();
    private final Map<String, List<og>> b = new HashMap();

    public void a(og ogVar) {
        if (ogVar != null) {
            String lowerCase = ogVar.a().toLowerCase(Locale.US);
            Object obj = this.b.get(lowerCase);
            if (obj == null) {
                obj = new LinkedList();
                this.b.put(lowerCase, obj);
            }
            obj.add(ogVar);
            this.a.add(ogVar);
        }
    }

    public og a(String str) {
        if (str == null) {
            return null;
        }
        List list = this.b.get(str.toLowerCase(Locale.US));
        if (list == null || list.isEmpty()) {
            return null;
        }
        return (og) list.get(0);
    }

    public Iterator<og> iterator() {
        return Collections.unmodifiableList(this.a).iterator();
    }

    public String toString() {
        return this.a.toString();
    }
}
