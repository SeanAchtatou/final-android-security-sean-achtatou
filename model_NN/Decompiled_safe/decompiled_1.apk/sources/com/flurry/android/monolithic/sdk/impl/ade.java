package com.flurry.android.monolithic.sdk.impl;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class ade {
    protected final Type a;
    protected final Class<?> b;
    protected final ParameterizedType c;
    protected ade d;
    protected ade e;

    public ade(Type type) {
        this.a = type;
        if (type instanceof Class) {
            this.b = (Class) type;
            this.c = null;
        } else if (type instanceof ParameterizedType) {
            this.c = (ParameterizedType) type;
            this.b = (Class) this.c.getRawType();
        } else {
            throw new IllegalArgumentException("Type " + type.getClass().getName() + " can not be used to construct HierarchicType");
        }
    }

    private ade(Type type, Class<?> cls, ParameterizedType parameterizedType, ade ade, ade ade2) {
        this.a = type;
        this.b = cls;
        this.c = parameterizedType;
        this.d = ade;
        this.e = ade2;
    }

    public ade a() {
        ade a2 = this.d == null ? null : this.d.a();
        ade ade = new ade(this.a, this.b, this.c, a2, null);
        if (a2 != null) {
            a2.b(ade);
        }
        return ade;
    }

    public void a(ade ade) {
        this.d = ade;
    }

    public final ade b() {
        return this.d;
    }

    public void b(ade ade) {
        this.e = ade;
    }

    public final boolean c() {
        return this.c != null;
    }

    public final ParameterizedType d() {
        return this.c;
    }

    public final Class<?> e() {
        return this.b;
    }

    public String toString() {
        if (this.c != null) {
            return this.c.toString();
        }
        return this.b.getName();
    }
}
