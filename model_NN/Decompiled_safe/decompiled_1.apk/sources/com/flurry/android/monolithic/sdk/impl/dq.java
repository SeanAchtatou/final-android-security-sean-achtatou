package com.flurry.android.monolithic.sdk.impl;

import com.inmobi.androidsdk.IMAdInterstitial;
import com.inmobi.androidsdk.IMAdInterstitialListener;
import com.inmobi.androidsdk.IMAdRequest;
import java.util.Collections;

class dq implements IMAdInterstitialListener {
    final /* synthetic */ dp a;

    dq(dp dpVar) {
        this.a = dpVar;
    }

    public void onAdRequestFailed(IMAdInterstitial iMAdInterstitial, IMAdRequest.ErrorCode errorCode) {
        this.a.d(Collections.emptyMap());
        ja.a(3, dp.b, "InMobi imAdView ad request failed.");
    }

    public void onAdRequestLoaded(IMAdInterstitial iMAdInterstitial) {
        ja.a(3, dp.b, "InMobi Interstitial ad request completed.");
        if (IMAdInterstitial.State.READY.equals(iMAdInterstitial.getState())) {
            this.a.a(Collections.emptyMap());
            iMAdInterstitial.show();
        }
    }

    public void onDismissAdScreen(IMAdInterstitial iMAdInterstitial) {
        this.a.c(Collections.emptyMap());
        ja.a(3, dp.b, "InMobi Interstitial ad dismissed.");
    }

    public void onShowAdScreen(IMAdInterstitial iMAdInterstitial) {
        ja.a(3, dp.b, "InMobi Interstitial ad shown.");
    }

    public void onLeaveApplication(IMAdInterstitial iMAdInterstitial) {
        ja.a(3, dp.b, "InMobi onLeaveApplication");
    }
}
