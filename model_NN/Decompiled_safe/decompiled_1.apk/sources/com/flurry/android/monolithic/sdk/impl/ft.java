package com.flurry.android.monolithic.sdk.impl;

import android.text.TextUtils;
import com.flurry.android.impl.appcloud.AppCloudModule;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

public class ft extends fm {
    private static ft g = null;

    public ft() {
    }

    public ft(JSONObject jSONObject) {
        a(jSONObject);
    }

    public void d(String str) {
        this.d = str;
    }

    public void e(String str) {
        this.e = str;
    }

    public void f(String str) {
        this.f = str;
    }

    public static ft e() {
        return g;
    }

    public static ft f() {
        ft c;
        if (e() == null && (c = fy.c()) != null) {
            a(c);
            gr.a = fy.h();
            e().c(e().a);
        }
        return e();
    }

    public static void a(ft ftVar) {
        g = ftVar;
    }

    public static void a(String str, String str2, hz hzVar) throws Exception {
        ArrayList arrayList = new ArrayList();
        if (!TextUtils.equals(str, fy.f()) || !TextUtils.equals(str2, fy.g()) || TextUtils.isEmpty(fy.d())) {
            arrayList.add(new BasicNameValuePair("email", str));
            arrayList.add(new BasicNameValuePair("password", str2));
        } else {
            arrayList.add(new BasicNameValuePair("userid", fy.d()));
            arrayList.add(new BasicNameValuePair("password", str2));
        }
        a(arrayList, str2, hzVar);
    }

    static void b(String str, String str2, hz hzVar) throws Exception {
        ArrayList arrayList = new ArrayList();
        if (!TextUtils.equals(str, fy.e()) || !TextUtils.equals(str2, fy.g()) || TextUtils.isEmpty(fy.d())) {
            arrayList.add(new BasicNameValuePair("username", str));
            arrayList.add(new BasicNameValuePair("password", str2));
        } else {
            arrayList.add(new BasicNameValuePair("userid", fy.d()));
            arrayList.add(new BasicNameValuePair("password", str2));
        }
        a(arrayList, str2, hzVar);
    }

    static void a(List<NameValuePair> list, String str, hz hzVar) {
        if (!AppCloudModule.getInstance().e()) {
            hzVar.a(new hy(400, "Bad response"));
            return;
        }
        try {
            gr.c(false, "v1/user/login", list, new fu(str, hzVar));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(String str, String str2, String str3, boolean z, hz hzVar) {
        if (TextUtils.isEmpty(str)) {
            hzVar.a(new hy(400, "User name is not set."));
        } else if (TextUtils.isEmpty(str2)) {
            hzVar.a(new hy(400, "Password is not set."));
        } else if (TextUtils.isEmpty(str3)) {
            hzVar.a(new hy(400, "Email is not set."));
        } else if (!z) {
            try {
                b(str, str2, hzVar);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            b(str, str2, new fv(hzVar, str, str3, str2));
        }
    }

    public void a(hw hwVar) throws Exception {
        super.a(new fx(this, hwVar));
    }

    public void a(String str, float f, hx hxVar) {
        if (TextUtils.isEmpty(this.a)) {
            hxVar.a(new hy(400, "Please assign an object id."));
        } else {
            super.a(str, String.valueOf(f), hxVar);
        }
    }
}
