package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.nio.charset.Charset;

public class un extends um<Charset> {
    public un() {
        super(Charset.class);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public Charset a(String str, qm qmVar) throws IOException {
        return Charset.forName(str);
    }
}
