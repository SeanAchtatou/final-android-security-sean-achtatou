package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

public final class ack extends abq<AtomicBoolean> {
    public ack() {
        super(AtomicBoolean.class, false);
    }

    public void a(AtomicBoolean atomicBoolean, or orVar, ru ruVar) throws IOException, oq {
        orVar.a(atomicBoolean.get());
    }
}
