package com.flurry.android.monolithic.sdk.impl;

import java.util.GregorianCalendar;
import java.util.HashMap;

class tg {
    final HashMap<adb, qu<Object>> a = new HashMap<>();

    private tg() {
        Class<GregorianCalendar> cls = GregorianCalendar.class;
        a(new xc());
        wy wyVar = new wy();
        a(wyVar, String.class);
        a(wyVar, CharSequence.class);
        a(new ue());
        a(new vs(Boolean.class, null));
        a(new vt(Byte.class, null));
        a(new wb(Short.class, null));
        a(new vu(Character.class, null));
        a(new vx(Integer.class, null));
        a(new vy(Long.class, null));
        a(new vw(Float.class, null));
        a(new vv(Double.class, null));
        a(new vs(Boolean.TYPE, Boolean.FALSE));
        a(new vt(Byte.TYPE, (byte) 0));
        a(new wb(Short.TYPE, 0));
        a(new vu(Character.TYPE, 0));
        a(new vx(Integer.TYPE, 0));
        a(new vy(Long.TYPE, 0L));
        a(new vw(Float.TYPE, Float.valueOf(0.0f)));
        a(new vv(Double.TYPE, Double.valueOf(0.0d)));
        a(new vz());
        a(new vq());
        a(new vr());
        a(new ud());
        a(new uh());
        Class<GregorianCalendar> cls2 = GregorianCalendar.class;
        Class<GregorianCalendar> cls3 = GregorianCalendar.class;
        a(new ud(cls), cls);
        a(new wc());
        a(new xa());
        for (um<?> a2 : um.d()) {
            a(a2);
        }
        a(new wd());
        a(new tz());
        a(new xb());
        a(new uw());
    }

    public static HashMap<adb, qu<Object>> a() {
        return new tg().a;
    }

    private void a(vo<?> voVar) {
        a(voVar, voVar.f());
    }

    private void a(vo<?> voVar, Class<?> cls) {
        this.a.put(new adb(cls), voVar);
    }
}
