package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

final class kf extends LinkedHashMap<String, String> {
    private Set<String> a;

    public kf(Set<String> set) {
        super(1);
        this.a = set;
    }

    public void a(String str, String str2) {
        if (this.a.contains(str)) {
            throw new jg("Can't set reserved property: " + str);
        } else if (str2 == null) {
            throw new jg("Can't set a property to null: " + str);
        } else {
            String str3 = (String) get(str);
            if (str3 == null) {
                put(str, str2);
            } else if (!str3.equals(str2)) {
                throw new jg("Can't overwrite property: " + str);
            }
        }
    }

    public void a(or orVar) throws IOException {
        for (Map.Entry entry : entrySet()) {
            orVar.a((String) entry.getKey(), (String) entry.getValue());
        }
    }
}
