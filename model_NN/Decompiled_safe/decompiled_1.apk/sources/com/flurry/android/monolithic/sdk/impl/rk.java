package com.flurry.android.monolithic.sdk.impl;

import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

public class rk extends pc {
    protected static final qf<? extends qb> a = xr.i;
    protected static final py b = new xx();
    protected static final ye<?> c = yf.a();
    private static final afm n = adh.d((Class<?>) ou.class);
    protected final op d;
    protected yh e;
    protected adk f;
    protected qt g;
    protected rq h;
    protected ru i;
    protected rs j;
    protected qk k;
    protected qq l;
    protected final ConcurrentHashMap<afm, qu<Object>> m;

    public rk() {
        this(null, null, null);
    }

    public rk(op opVar) {
        this(opVar, null, null);
    }

    public rk(op opVar, ru ruVar, qq qqVar) {
        this(opVar, ruVar, qqVar, null, null);
    }

    public rk(op opVar, ru ruVar, qq qqVar, rq rqVar, qk qkVar) {
        ru ruVar2;
        qq qqVar2;
        this.m = new ConcurrentHashMap<>(64, 0.6f, 2);
        if (opVar == null) {
            this.d = new rj(this);
        } else {
            this.d = opVar;
            if (opVar.a() == null) {
                this.d.a(this);
            }
        }
        this.f = adk.a();
        this.h = rqVar != null ? rqVar : new rq(a, b, c, null, null, this.f, null);
        this.k = qkVar != null ? qkVar : new qk(a, b, c, null, null, this.f, null);
        if (ruVar == null) {
            ruVar2 = new zw();
        } else {
            ruVar2 = ruVar;
        }
        this.i = ruVar2;
        if (qqVar == null) {
            qqVar2 = new te();
        } else {
            qqVar2 = qqVar;
        }
        this.l = qqVar2;
        this.j = zi.e;
    }

    public rq a() {
        return this.h.a(this.e);
    }

    public qk b() {
        return this.k.a(this.e).a(this.h.i);
    }

    public aez c() {
        return this.k.h();
    }

    public ou a(ow owVar) throws IOException, oz {
        qk b2 = b();
        if (owVar.e() == null && owVar.b() == null) {
            return null;
        }
        ou ouVar = (ou) a(b2, owVar, n);
        return ouVar == null ? c().a() : ouVar;
    }

    public void a(or orVar, Object obj) throws IOException, oq, qw {
        rq a2 = a();
        if (!a2.a(rr.CLOSE_CLOSEABLE) || !(obj instanceof Closeable)) {
            this.i.a(a2, orVar, obj, this.j);
            if (a2.a(rr.FLUSH_AFTER_WRITE_VALUE)) {
                orVar.g();
                return;
            }
            return;
        }
        a(orVar, obj, a2);
    }

    public void a(or orVar, ou ouVar) throws IOException, oz {
        rq a2 = a();
        this.i.a(a2, orVar, ouVar, this.j);
        if (a2.a(rr.FLUSH_AFTER_WRITE_VALUE)) {
            orVar.g();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0026 A[SYNTHETIC, Splitter:B:14:0x0026] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void a(com.flurry.android.monolithic.sdk.impl.or r6, java.lang.Object r7, com.flurry.android.monolithic.sdk.impl.rq r8) throws java.io.IOException, com.flurry.android.monolithic.sdk.impl.oq, com.flurry.android.monolithic.sdk.impl.qw {
        /*
            r5 = this;
            r0 = r7
            java.io.Closeable r0 = (java.io.Closeable) r0
            r1 = r0
            com.flurry.android.monolithic.sdk.impl.ru r2 = r5.i     // Catch:{ all -> 0x0020 }
            com.flurry.android.monolithic.sdk.impl.rs r3 = r5.j     // Catch:{ all -> 0x0020 }
            r2.a(r8, r6, r7, r3)     // Catch:{ all -> 0x0020 }
            com.flurry.android.monolithic.sdk.impl.rr r2 = com.flurry.android.monolithic.sdk.impl.rr.FLUSH_AFTER_WRITE_VALUE     // Catch:{ all -> 0x0020 }
            boolean r2 = r8.a(r2)     // Catch:{ all -> 0x0020 }
            if (r2 == 0) goto L_0x0016
            r6.g()     // Catch:{ all -> 0x0020 }
        L_0x0016:
            r2 = 0
            r1.close()     // Catch:{ all -> 0x002e }
            if (r2 == 0) goto L_0x001f
            r2.close()     // Catch:{ IOException -> 0x002a }
        L_0x001f:
            return
        L_0x0020:
            r2 = move-exception
            r4 = r2
            r2 = r1
            r1 = r4
        L_0x0024:
            if (r2 == 0) goto L_0x0029
            r2.close()     // Catch:{ IOException -> 0x002c }
        L_0x0029:
            throw r1
        L_0x002a:
            r1 = move-exception
            goto L_0x001f
        L_0x002c:
            r2 = move-exception
            goto L_0x0029
        L_0x002e:
            r1 = move-exception
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.rk.a(com.flurry.android.monolithic.sdk.impl.or, java.lang.Object, com.flurry.android.monolithic.sdk.impl.rq):void");
    }

    /* access modifiers changed from: protected */
    public Object a(qk qkVar, ow owVar, afm afm) throws IOException, ov, qw {
        Object obj;
        pb b2 = b(owVar);
        if (b2 == pb.VALUE_NULL) {
            obj = a(qkVar, afm).b();
        } else if (b2 == pb.END_ARRAY || b2 == pb.END_OBJECT) {
            obj = null;
        } else {
            qm a2 = a(owVar, qkVar);
            qu<Object> a3 = a(qkVar, afm);
            if (qkVar.a(ql.UNWRAP_ROOT_VALUE)) {
                obj = a(owVar, afm, a2, a3);
            } else {
                obj = a3.a(owVar, a2);
            }
        }
        owVar.f();
        return obj;
    }

    /* access modifiers changed from: protected */
    public pb b(ow owVar) throws IOException, ov, qw {
        pb e2 = owVar.e();
        if (e2 != null || (e2 = owVar.b()) != null) {
            return e2;
        }
        throw new EOFException("No content to map to Object due to end of input");
    }

    /* access modifiers changed from: protected */
    public Object a(ow owVar, afm afm, qm qmVar, qu<Object> quVar) throws IOException, ov, qw {
        pw a2 = this.l.a(qmVar.a(), afm);
        if (owVar.e() != pb.START_OBJECT) {
            throw qw.a(owVar, "Current token not START_OBJECT (needed to unwrap root name '" + a2 + "'), but " + owVar.e());
        } else if (owVar.b() != pb.FIELD_NAME) {
            throw qw.a(owVar, "Current token not FIELD_NAME (to contain expected root name '" + a2 + "'), but " + owVar.e());
        } else {
            String g2 = owVar.g();
            if (!a2.a().equals(g2)) {
                throw qw.a(owVar, "Root name '" + g2 + "' does not match expected ('" + a2 + "') for type " + afm);
            }
            owVar.b();
            Object a3 = quVar.a(owVar, qmVar);
            if (owVar.b() == pb.END_OBJECT) {
                return a3;
            }
            throw qw.a(owVar, "Current token not END_OBJECT (to match wrapper object with root name '" + a2 + "'), but " + owVar.e());
        }
    }

    /* access modifiers changed from: protected */
    public qu<Object> a(qk qkVar, afm afm) throws qw {
        qu<Object> quVar = this.m.get(afm);
        if (quVar == null) {
            quVar = this.l.b(qkVar, afm, null);
            if (quVar == null) {
                throw new qw("Can not find a deserializer for type " + afm);
            }
            this.m.put(afm, quVar);
        }
        return quVar;
    }

    /* access modifiers changed from: protected */
    public qm a(ow owVar, qk qkVar) {
        return new td(qkVar, owVar, this.l, this.g);
    }
}
