package com.flurry.android.monolithic.sdk.impl;

import java.util.List;

public class zh {
    private static final zf[] f = new zf[0];
    protected final xq a;
    protected List<zf> b;
    protected zf[] c;
    protected zc d;
    protected Object e;

    public zh(xq xqVar) {
        this.a = xqVar;
    }

    public List<zf> a() {
        return this.b;
    }

    public void a(List<zf> list) {
        this.b = list;
    }

    public void a(zf[] zfVarArr) {
        this.c = zfVarArr;
    }

    public void a(zc zcVar) {
        this.d = zcVar;
    }

    public void a(Object obj) {
        this.e = obj;
    }

    public ra<?> b() {
        zf[] zfVarArr;
        if (this.b != null && !this.b.isEmpty()) {
            zfVarArr = (zf[]) this.b.toArray(new zf[this.b.size()]);
        } else if (this.d == null) {
            return null;
        } else {
            zfVarArr = f;
        }
        return new zg(this.a.a(), zfVarArr, this.c, this.d, this.e);
    }

    public zg c() {
        return zg.a(this.a.b());
    }
}
