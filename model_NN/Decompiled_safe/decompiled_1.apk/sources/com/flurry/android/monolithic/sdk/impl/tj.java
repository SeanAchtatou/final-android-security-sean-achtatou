package com.flurry.android.monolithic.sdk.impl;

import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;

public final class tj {
    private final tk[] a;
    private final int b;
    private final int c;

    public tj(Collection<sw> collection) {
        this.c = collection.size();
        int a2 = a(this.c);
        this.b = a2 - 1;
        tk[] tkVarArr = new tk[a2];
        for (sw next : collection) {
            String c2 = next.c();
            int hashCode = c2.hashCode() & this.b;
            tkVarArr[hashCode] = new tk(tkVarArr[hashCode], c2, next);
        }
        this.a = tkVarArr;
    }

    public void a() {
        int i = 0;
        tk[] tkVarArr = this.a;
        int length = tkVarArr.length;
        int i2 = 0;
        while (i < length) {
            int i3 = i2;
            tk tkVar = tkVarArr[i];
            while (tkVar != null) {
                tkVar.c.a(i3);
                tkVar = tkVar.a;
                i3++;
            }
            i++;
            i2 = i3;
        }
    }

    private static final int a(int i) {
        int i2 = 2;
        while (i2 < (i <= 32 ? i + i : (i >> 2) + i)) {
            i2 += i2;
        }
        return i2;
    }

    public int b() {
        return this.c;
    }

    public Iterator<sw> c() {
        return new tl(this.a);
    }

    public sw a(String str) {
        int hashCode = str.hashCode() & this.b;
        tk tkVar = this.a[hashCode];
        if (tkVar == null) {
            return null;
        }
        if (tkVar.b == str) {
            return tkVar.c;
        }
        do {
            tkVar = tkVar.a;
            if (tkVar == null) {
                return a(str, hashCode);
            }
        } while (tkVar.b != str);
        return tkVar.c;
    }

    public void a(sw swVar) {
        String c2 = swVar.c();
        int hashCode = c2.hashCode() & (this.a.length - 1);
        boolean z = false;
        tk tkVar = null;
        for (tk tkVar2 = this.a[hashCode]; tkVar2 != null; tkVar2 = tkVar2.a) {
            if (z || !tkVar2.b.equals(c2)) {
                tkVar = new tk(tkVar, tkVar2.b, tkVar2.c);
            } else {
                z = true;
            }
        }
        if (!z) {
            throw new NoSuchElementException("No entry '" + swVar + "' found, can't replace");
        }
        this.a[hashCode] = new tk(tkVar, c2, swVar);
    }

    public void b(sw swVar) {
        String c2 = swVar.c();
        int hashCode = c2.hashCode() & (this.a.length - 1);
        boolean z = false;
        tk tkVar = null;
        for (tk tkVar2 = this.a[hashCode]; tkVar2 != null; tkVar2 = tkVar2.a) {
            if (z || !tkVar2.b.equals(c2)) {
                tkVar = new tk(tkVar, tkVar2.b, tkVar2.c);
            } else {
                z = true;
            }
        }
        if (!z) {
            throw new NoSuchElementException("No entry '" + swVar + "' found, can't remove");
        }
        this.a[hashCode] = tkVar;
    }

    private sw a(String str, int i) {
        for (tk tkVar = this.a[i]; tkVar != null; tkVar = tkVar.a) {
            if (str.equals(tkVar.b)) {
                return tkVar.c;
            }
        }
        return null;
    }
}
