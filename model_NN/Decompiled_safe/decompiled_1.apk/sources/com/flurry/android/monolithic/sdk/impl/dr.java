package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import com.flurry.android.AdCreative;
import com.flurry.android.impl.ads.FlurryAdModule;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;
import java.util.ArrayList;
import java.util.List;

public final class dr extends cy {
    /* access modifiers changed from: protected */
    public String f() {
        return "Jumptap";
    }

    /* access modifiers changed from: protected */
    public List<cu> g() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new cu("JtAdTag", "1.1.10.4", "com.jumptap.adtag.JtAdInterstitial"));
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public List<ActivityInfo> j() {
        return new ArrayList();
    }

    /* access modifiers changed from: protected */
    public List<cu> k() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new cu("JtAdTag", "1.1.10.4", "com.jumptap.adtag.JtAdView"));
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public List<String> n() {
        ArrayList arrayList = new ArrayList();
        arrayList.add("com.flurry.jumptap.PUBLISHER_ID");
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public List<String> o() {
        ArrayList arrayList = new ArrayList();
        arrayList.add("android.permission.INTERNET");
        arrayList.add("android.permission.READ_PHONE_STATE");
        arrayList.add("android.permission.ACCESS_NETWORK_STATE");
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public cn a(Context context, FlurryAdModule flurryAdModule, m mVar, AdUnit adUnit, Bundle bundle) {
        if (context == null || flurryAdModule == null || mVar == null || adUnit == null || bundle == null) {
            return null;
        }
        return new du(context, flurryAdModule, mVar, adUnit, bundle);
    }

    /* access modifiers changed from: protected */
    public ac a(Context context, FlurryAdModule flurryAdModule, m mVar, AdCreative adCreative, Bundle bundle) {
        if (context == null || flurryAdModule == null || mVar == null || adCreative == null || bundle == null) {
            return null;
        }
        return new ds(context, flurryAdModule, mVar, adCreative, bundle);
    }
}
