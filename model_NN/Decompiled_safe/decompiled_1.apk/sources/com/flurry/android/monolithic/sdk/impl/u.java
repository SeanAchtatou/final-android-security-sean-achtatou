package com.flurry.android.monolithic.sdk.impl;

import android.content.DialogInterface;
import java.util.HashMap;

class u implements DialogInterface.OnClickListener {
    final /* synthetic */ bh a;
    final /* synthetic */ int b;
    final /* synthetic */ o c;

    u(o oVar, bh bhVar, int i) {
        this.c = oVar;
        this.a = bhVar;
        this.b = i;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        HashMap hashMap = new HashMap();
        hashMap.put("sourceEvent", this.a.a);
        this.c.a("userConfirmed", hashMap, this.c.d, this.c.c, this.c.e, this.b + 1);
        dialogInterface.dismiss();
        if (this.c.j != null && this.c.getCurrentBinding() == 3) {
            this.c.j.start();
        }
    }
}
