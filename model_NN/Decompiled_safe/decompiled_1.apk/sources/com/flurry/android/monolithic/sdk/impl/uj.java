package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.lang.reflect.Method;

public class uj extends wv<Object> {
    protected final Class<?> a;
    protected final Method b;

    public uj(Class<?> cls, xl xlVar) {
        super(Enum.class);
        this.a = cls;
        this.b = xlVar.a();
    }

    public Object a(ow owVar, qm qmVar) throws IOException, oz {
        pb e = owVar.e();
        if (e == pb.VALUE_STRING || e == pb.FIELD_NAME) {
            String k = owVar.k();
            try {
                return this.b.invoke(this.a, k);
            } catch (Exception e2) {
                adz.c(e2);
                return null;
            }
        } else {
            throw qmVar.b(this.a);
        }
    }
}
