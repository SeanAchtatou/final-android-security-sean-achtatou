package com.flurry.android.monolithic.sdk.impl;

import java.util.UUID;

final class wt extends we {
    protected wt() {
        super(UUID.class);
    }

    /* renamed from: c */
    public UUID b(String str, qm qmVar) throws IllegalArgumentException, qw {
        return UUID.fromString(str);
    }
}
