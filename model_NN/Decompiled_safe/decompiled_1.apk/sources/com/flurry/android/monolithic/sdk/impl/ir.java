package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.text.TextUtils;

public class ir {
    private static final String a = ir.class.getSimpleName();
    private static String b;
    private static String c;

    public static void a(String str) {
        b = str;
    }

    public static String a() {
        if (!TextUtils.isEmpty(b)) {
            return b;
        }
        if (!TextUtils.isEmpty(c)) {
            return c;
        }
        c = b();
        return c;
    }

    private static String b() {
        try {
            Context b2 = ia.a().b();
            PackageInfo packageInfo = b2.getPackageManager().getPackageInfo(b2.getPackageName(), 0);
            if (packageInfo.versionName != null) {
                return packageInfo.versionName;
            }
            if (packageInfo.versionCode != 0) {
                return Integer.toString(packageInfo.versionCode);
            }
            return "Unknown";
        } catch (Throwable th) {
            ja.a(6, a, "", th);
        }
    }
}
