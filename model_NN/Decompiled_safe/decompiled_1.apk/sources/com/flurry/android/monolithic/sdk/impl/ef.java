package com.flurry.android.monolithic.sdk.impl;

import com.mobclix.android.sdk.MobclixFullScreenAdView;
import com.mobclix.android.sdk.MobclixFullScreenAdViewListener;
import java.util.Collections;

class ef implements MobclixFullScreenAdViewListener {
    final /* synthetic */ ee a;

    ef(ee eeVar) {
        this.a = eeVar;
    }

    public void onDismissAd(MobclixFullScreenAdView mobclixFullScreenAdView) {
        this.a.c(Collections.emptyMap());
        ja.a(3, ee.b, "Mobclix Interstitial ad dismissed.");
    }

    public void onFailedLoad(MobclixFullScreenAdView mobclixFullScreenAdView, int i) {
        this.a.d(Collections.emptyMap());
        ja.a(3, ee.b, "Mobclix Interstitial ad failed to load.");
    }

    public void onFinishLoad(MobclixFullScreenAdView mobclixFullScreenAdView) {
        ja.a(3, ee.b, "Mobclix Interstitial ad successfully loaded.");
    }

    public void onPresentAd(MobclixFullScreenAdView mobclixFullScreenAdView) {
        this.a.a(Collections.emptyMap());
        ja.a(3, ee.b, "Mobclix Interstitial ad successfully shown.");
    }

    public String keywords() {
        ja.a(3, ee.b, "Mobclix keyword callback.");
        return null;
    }

    public String query() {
        ja.a(3, ee.b, "Mobclix query callback.");
        return null;
    }
}
