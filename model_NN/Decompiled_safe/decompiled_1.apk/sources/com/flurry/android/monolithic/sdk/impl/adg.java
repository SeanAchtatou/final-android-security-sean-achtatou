package com.flurry.android.monolithic.sdk.impl;

public final class adg extends adf {
    private adg(Class<?> cls, afm afm, afm afm2, Object obj, Object obj2) {
        super(cls, afm, afm2, obj, obj2);
    }

    public static adg a(Class<?> cls, afm afm, afm afm2) {
        return new adg(cls, afm, afm2, null, null);
    }

    /* access modifiers changed from: protected */
    public afm a(Class<?> cls) {
        return new adg(cls, this.a, this.b, this.f, this.g);
    }

    public afm b(Class<?> cls) {
        return cls == this.b.p() ? this : new adg(this.d, this.a, this.b.f(cls), this.f, this.g);
    }

    public afm c(Class<?> cls) {
        return cls == this.b.p() ? this : new adg(this.d, this.a, this.b.h(cls), this.f, this.g);
    }

    public afm d(Class<?> cls) {
        return cls == this.a.p() ? this : new adg(this.d, this.a.f(cls), this.b, this.f, this.g);
    }

    public afm e(Class<?> cls) {
        return cls == this.a.p() ? this : new adg(this.d, this.a.h(cls), this.b, this.f, this.g);
    }

    /* renamed from: g */
    public adg f(Object obj) {
        return new adg(this.d, this.a, this.b, this.f, obj);
    }

    /* renamed from: h */
    public adg e(Object obj) {
        return new adg(this.d, this.a, this.b.f(obj), this.f, this.g);
    }

    /* renamed from: i */
    public adg d(Object obj) {
        return new adg(this.d, this.a, this.b, obj, this.g);
    }

    public String toString() {
        return "[map type; class " + this.d.getName() + ", " + this.a + " -> " + this.b + "]";
    }
}
