package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

@rz
public class abo extends abw<Object[]> implements rp {
    protected final boolean a;
    protected final afm b;
    protected ra<Object> c;
    protected aal d = aal.a();

    public abo(afm afm, boolean z, rx rxVar, qc qcVar, ra<Object> raVar) {
        super(Object[].class, rxVar, qcVar);
        this.b = afm;
        this.a = z;
        this.c = raVar;
    }

    public abc<?> a(rx rxVar) {
        return new abo(this.b, this.a, rxVar, this.f, this.c);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0049, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x004a, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0050, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0051, code lost:
        r2 = 0;
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x006b, code lost:
        throw ((java.lang.Error) r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0070, code lost:
        throw com.flurry.android.monolithic.sdk.impl.qw.a(r7, r1, r2);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0049 A[ExcHandler: IOException (r0v9 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:9:0x0018] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x006c  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(java.lang.Object[] r8, com.flurry.android.monolithic.sdk.impl.or r9, com.flurry.android.monolithic.sdk.impl.ru r10) throws java.io.IOException, com.flurry.android.monolithic.sdk.impl.oq {
        /*
            r7 = this;
            int r0 = r8.length
            if (r0 != 0) goto L_0x0004
        L_0x0003:
            return
        L_0x0004:
            com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object> r1 = r7.c
            if (r1 == 0) goto L_0x000e
            com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object> r0 = r7.c
            r7.a(r8, r9, r10, r0)
            goto L_0x0003
        L_0x000e:
            com.flurry.android.monolithic.sdk.impl.rx r1 = r7.e
            if (r1 == 0) goto L_0x0016
            r7.b(r8, r9, r10)
            goto L_0x0003
        L_0x0016:
            r1 = 0
            r2 = 0
            com.flurry.android.monolithic.sdk.impl.aal r3 = r7.d     // Catch:{ IOException -> 0x0049, Exception -> 0x0050 }
            r6 = r2
            r2 = r1
            r1 = r6
        L_0x001d:
            if (r2 >= r0) goto L_0x0003
            r1 = r8[r2]     // Catch:{ IOException -> 0x0049, Exception -> 0x0071 }
            if (r1 != 0) goto L_0x0029
            r10.a(r9)     // Catch:{ IOException -> 0x0049, Exception -> 0x0071 }
        L_0x0026:
            int r2 = r2 + 1
            goto L_0x001d
        L_0x0029:
            java.lang.Class r4 = r1.getClass()     // Catch:{ IOException -> 0x0049, Exception -> 0x0071 }
            com.flurry.android.monolithic.sdk.impl.ra r5 = r3.a(r4)     // Catch:{ IOException -> 0x0049, Exception -> 0x0071 }
            if (r5 != 0) goto L_0x0073
            com.flurry.android.monolithic.sdk.impl.afm r5 = r7.b     // Catch:{ IOException -> 0x0049, Exception -> 0x0071 }
            boolean r5 = r5.e()     // Catch:{ IOException -> 0x0049, Exception -> 0x0071 }
            if (r5 == 0) goto L_0x004b
            com.flurry.android.monolithic.sdk.impl.afm r5 = r7.b     // Catch:{ IOException -> 0x0049, Exception -> 0x0071 }
            com.flurry.android.monolithic.sdk.impl.afm r4 = r10.a(r5, r4)     // Catch:{ IOException -> 0x0049, Exception -> 0x0071 }
            com.flurry.android.monolithic.sdk.impl.ra r4 = r7.a(r3, r4, r10)     // Catch:{ IOException -> 0x0049, Exception -> 0x0071 }
        L_0x0045:
            r4.a(r1, r9, r10)     // Catch:{ IOException -> 0x0049, Exception -> 0x0071 }
            goto L_0x0026
        L_0x0049:
            r0 = move-exception
            throw r0
        L_0x004b:
            com.flurry.android.monolithic.sdk.impl.ra r4 = r7.a(r3, r4, r10)     // Catch:{ IOException -> 0x0049, Exception -> 0x0071 }
            goto L_0x0045
        L_0x0050:
            r0 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
        L_0x0054:
            r7 = r0
        L_0x0055:
            boolean r0 = r7 instanceof java.lang.reflect.InvocationTargetException
            if (r0 == 0) goto L_0x0065
            java.lang.Throwable r0 = r7.getCause()
            if (r0 == 0) goto L_0x0065
            java.lang.Throwable r0 = r7.getCause()
            r7 = r0
            goto L_0x0055
        L_0x0065:
            boolean r0 = r7 instanceof java.lang.Error
            if (r0 == 0) goto L_0x006c
            java.lang.Error r7 = (java.lang.Error) r7
            throw r7
        L_0x006c:
            com.flurry.android.monolithic.sdk.impl.qw r0 = com.flurry.android.monolithic.sdk.impl.qw.a(r7, r1, r2)
            throw r0
        L_0x0071:
            r0 = move-exception
            goto L_0x0054
        L_0x0073:
            r4 = r5
            goto L_0x0045
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.abo.b(java.lang.Object[], com.flurry.android.monolithic.sdk.impl.or, com.flurry.android.monolithic.sdk.impl.ru):void");
    }

    public void a(Object[] objArr, or orVar, ru ruVar, ra<Object> raVar) throws IOException, oq {
        Throwable th;
        int length = objArr.length;
        rx rxVar = this.e;
        int i = 0;
        Object obj = null;
        while (i < length) {
            try {
                obj = objArr[i];
                if (obj == null) {
                    ruVar.a(orVar);
                } else if (rxVar == null) {
                    raVar.a(obj, orVar, ruVar);
                } else {
                    raVar.a(obj, orVar, ruVar, rxVar);
                }
                i++;
            } catch (IOException e) {
                throw e;
            } catch (Exception e2) {
                e = e2;
                Object obj2 = obj;
                while (true) {
                    th = e;
                    if ((th instanceof InvocationTargetException) && th.getCause() != null) {
                        e = th.getCause();
                    }
                }
                if (th instanceof Error) {
                    throw ((Error) th);
                }
                throw qw.a(th, obj2, i);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0028, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0029, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002a, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x002b, code lost:
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0043, code lost:
        throw ((java.lang.Error) r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0048, code lost:
        throw com.flurry.android.monolithic.sdk.impl.qw.a(r8, r1, r2);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0028 A[ExcHandler: IOException (r0v9 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:1:0x0005] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0044  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(java.lang.Object[] r9, com.flurry.android.monolithic.sdk.impl.or r10, com.flurry.android.monolithic.sdk.impl.ru r11) throws java.io.IOException, com.flurry.android.monolithic.sdk.impl.oq {
        /*
            r8 = this;
            int r0 = r9.length
            com.flurry.android.monolithic.sdk.impl.rx r1 = r8.e
            r2 = 0
            r3 = 0
            com.flurry.android.monolithic.sdk.impl.aal r4 = r8.d     // Catch:{ IOException -> 0x0028, Exception -> 0x002a }
            r7 = r3
            r3 = r2
            r2 = r7
        L_0x000a:
            if (r3 >= r0) goto L_0x0049
            r2 = r9[r3]     // Catch:{ IOException -> 0x0028, Exception -> 0x004a }
            if (r2 != 0) goto L_0x0016
            r11.a(r10)     // Catch:{ IOException -> 0x0028, Exception -> 0x004a }
        L_0x0013:
            int r3 = r3 + 1
            goto L_0x000a
        L_0x0016:
            java.lang.Class r5 = r2.getClass()     // Catch:{ IOException -> 0x0028, Exception -> 0x004a }
            com.flurry.android.monolithic.sdk.impl.ra r6 = r4.a(r5)     // Catch:{ IOException -> 0x0028, Exception -> 0x004a }
            if (r6 != 0) goto L_0x004e
            com.flurry.android.monolithic.sdk.impl.ra r5 = r8.a(r4, r5, r11)     // Catch:{ IOException -> 0x0028, Exception -> 0x004a }
        L_0x0024:
            r5.a(r2, r10, r11, r1)     // Catch:{ IOException -> 0x0028, Exception -> 0x004a }
            goto L_0x0013
        L_0x0028:
            r0 = move-exception
            throw r0
        L_0x002a:
            r0 = move-exception
            r1 = r3
        L_0x002c:
            r8 = r0
        L_0x002d:
            boolean r0 = r8 instanceof java.lang.reflect.InvocationTargetException
            if (r0 == 0) goto L_0x003d
            java.lang.Throwable r0 = r8.getCause()
            if (r0 == 0) goto L_0x003d
            java.lang.Throwable r0 = r8.getCause()
            r8 = r0
            goto L_0x002d
        L_0x003d:
            boolean r0 = r8 instanceof java.lang.Error
            if (r0 == 0) goto L_0x0044
            java.lang.Error r8 = (java.lang.Error) r8
            throw r8
        L_0x0044:
            com.flurry.android.monolithic.sdk.impl.qw r0 = com.flurry.android.monolithic.sdk.impl.qw.a(r8, r1, r2)
            throw r0
        L_0x0049:
            return
        L_0x004a:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x002c
        L_0x004e:
            r5 = r6
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.abo.b(java.lang.Object[], com.flurry.android.monolithic.sdk.impl.or, com.flurry.android.monolithic.sdk.impl.ru):void");
    }

    public void a(ru ruVar) throws qw {
        if (this.a && this.c == null) {
            this.c = ruVar.a(this.b, this.f);
        }
    }

    /* access modifiers changed from: protected */
    public final ra<Object> a(aal aal, Class<?> cls, ru ruVar) throws qw {
        aap a2 = aal.a(cls, ruVar, this.f);
        if (aal != a2.b) {
            this.d = a2.b;
        }
        return a2.a;
    }

    /* access modifiers changed from: protected */
    public final ra<Object> a(aal aal, afm afm, ru ruVar) throws qw {
        aap a2 = aal.a(afm, ruVar, this.f);
        if (aal != a2.b) {
            this.d = a2.b;
        }
        return a2.a;
    }
}
