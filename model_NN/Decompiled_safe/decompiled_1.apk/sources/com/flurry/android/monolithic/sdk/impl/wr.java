package com.flurry.android.monolithic.sdk.impl;

import java.lang.reflect.Method;

final class wr extends we {
    final Method b;

    public wr(Method method) {
        super(method.getDeclaringClass());
        this.b = method;
    }

    public Object b(String str, qm qmVar) throws Exception {
        return this.b.invoke(null, str);
    }
}
