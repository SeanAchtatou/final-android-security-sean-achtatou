package com.flurry.android.monolithic.sdk.impl;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;

final class au extends WebViewClient {
    final /* synthetic */ ar a;

    private au(ar arVar) {
        this.a = arVar;
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        boolean z;
        ja.a(3, this.a.a, "shouldOverrideUrlLoading: url = " + str);
        if (str == null || webView == null || webView != this.a.b) {
            return false;
        }
        if (this.a.l != null) {
            z = this.a.l.a(this.a, str, this.a.o);
        } else {
            z = false;
        }
        boolean unused = this.a.o = false;
        return z;
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        ja.a(3, this.a.a, "onPageStarted: url = " + str);
        if (str != null && webView != null && webView == this.a.b) {
            if (this.a.o) {
                boolean z = false;
                if (this.a.l != null) {
                    z = this.a.l.a(this.a, str, this.a.o);
                }
                if (z) {
                    webView.stopLoading();
                }
            }
            boolean unused = this.a.o = true;
        }
    }

    public void onPageFinished(WebView webView, String str) {
        ja.a(3, this.a.a, "onPageFinished: url = " + str);
        if (str != null && webView != null && webView == this.a.b) {
            boolean unused = this.a.o = false;
        }
    }
}
