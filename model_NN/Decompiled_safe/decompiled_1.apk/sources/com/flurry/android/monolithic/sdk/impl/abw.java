package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public abstract class abw<T> extends abc<T> {
    protected final rx e;
    protected final qc f;

    /* access modifiers changed from: protected */
    public abstract void b(Object obj, or orVar, ru ruVar) throws IOException, oq;

    protected abw(Class<T> cls, rx rxVar, qc qcVar) {
        super(cls);
        this.e = rxVar;
        this.f = qcVar;
    }

    public final void a(Object obj, or orVar, ru ruVar) throws IOException, oq {
        orVar.b();
        b(obj, orVar, ruVar);
        orVar.c();
    }

    public final void a(Object obj, or orVar, ru ruVar, rx rxVar) throws IOException, oq {
        rxVar.c(obj, orVar);
        b(obj, orVar, ruVar);
        rxVar.f(obj, orVar);
    }
}
