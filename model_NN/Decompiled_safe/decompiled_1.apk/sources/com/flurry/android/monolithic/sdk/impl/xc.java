package com.flurry.android.monolithic.sdk.impl;

import com.ideaworks3d.marmalade.s3eAndroidGooglePlayBilling.util.IabHelper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

@rz
public class xc extends vo<Object> {
    private static final Object[] a = new Object[0];

    public xc() {
        super(Object.class);
    }

    public Object a(ow owVar, qm qmVar) throws IOException, oz {
        switch (xd.a[owVar.e().ordinal()]) {
            case 1:
                return c(owVar, qmVar);
            case 2:
            case 4:
            default:
                throw qmVar.b(Object.class);
            case 3:
                return b(owVar, qmVar);
            case 5:
                return c(owVar, qmVar);
            case 6:
                return owVar.z();
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED /*7*/:
                return owVar.k();
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED /*8*/:
                if (qmVar.a(ql.USE_BIG_INTEGER_FOR_INTS)) {
                    return owVar.v();
                }
                return owVar.p();
            case 9:
                if (qmVar.a(ql.USE_BIG_DECIMAL_FOR_FLOATS)) {
                    return owVar.y();
                }
                return Double.valueOf(owVar.x());
            case 10:
                return Boolean.TRUE;
            case 11:
                return Boolean.FALSE;
            case 12:
                return null;
        }
    }

    public Object a(ow owVar, qm qmVar, rw rwVar) throws IOException, oz {
        switch (xd.a[owVar.e().ordinal()]) {
            case 1:
            case 3:
            case 5:
                return rwVar.d(owVar, qmVar);
            case 2:
            case 4:
            default:
                throw qmVar.b(Object.class);
            case 6:
                return owVar.z();
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED /*7*/:
                return owVar.k();
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED /*8*/:
                if (qmVar.a(ql.USE_BIG_INTEGER_FOR_INTS)) {
                    return owVar.v();
                }
                return Integer.valueOf(owVar.t());
            case 9:
                if (qmVar.a(ql.USE_BIG_DECIMAL_FOR_FLOATS)) {
                    return owVar.y();
                }
                return Double.valueOf(owVar.x());
            case 10:
                return Boolean.TRUE;
            case 11:
                return Boolean.FALSE;
            case 12:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public Object b(ow owVar, qm qmVar) throws IOException, oz {
        if (qmVar.a(ql.USE_JAVA_ARRAY_FOR_JSON_ARRAY)) {
            return d(owVar, qmVar);
        }
        if (owVar.b() == pb.END_ARRAY) {
            return new ArrayList(4);
        }
        aeh g = qmVar.g();
        int i = 0;
        Object[] a2 = g.a();
        int i2 = 0;
        while (true) {
            Object a3 = a(owVar, qmVar);
            i2++;
            if (i >= a2.length) {
                a2 = g.a(a2);
                i = 0;
            }
            int i3 = i + 1;
            a2[i] = a3;
            if (owVar.b() == pb.END_ARRAY) {
                ArrayList arrayList = new ArrayList(i2 + (i2 >> 3) + 1);
                g.a(a2, i3, arrayList);
                return arrayList;
            }
            i = i3;
        }
    }

    /* access modifiers changed from: protected */
    public Object c(ow owVar, qm qmVar) throws IOException, oz {
        pb e = owVar.e();
        if (e == pb.START_OBJECT) {
            e = owVar.b();
        }
        if (e != pb.FIELD_NAME) {
            return new LinkedHashMap(4);
        }
        String k = owVar.k();
        owVar.b();
        Object a2 = a(owVar, qmVar);
        if (owVar.b() != pb.FIELD_NAME) {
            LinkedHashMap linkedHashMap = new LinkedHashMap(4);
            linkedHashMap.put(k, a2);
            return linkedHashMap;
        }
        String k2 = owVar.k();
        owVar.b();
        Object a3 = a(owVar, qmVar);
        if (owVar.b() != pb.FIELD_NAME) {
            LinkedHashMap linkedHashMap2 = new LinkedHashMap(4);
            linkedHashMap2.put(k, a2);
            linkedHashMap2.put(k2, a3);
            return linkedHashMap2;
        }
        LinkedHashMap linkedHashMap3 = new LinkedHashMap();
        linkedHashMap3.put(k, a2);
        linkedHashMap3.put(k2, a3);
        do {
            String k3 = owVar.k();
            owVar.b();
            linkedHashMap3.put(k3, a(owVar, qmVar));
        } while (owVar.b() != pb.END_OBJECT);
        return linkedHashMap3;
    }

    /* access modifiers changed from: protected */
    public Object[] d(ow owVar, qm qmVar) throws IOException, oz {
        if (owVar.b() == pb.END_ARRAY) {
            return a;
        }
        aeh g = qmVar.g();
        Object[] a2 = g.a();
        int i = 0;
        while (true) {
            Object a3 = a(owVar, qmVar);
            if (i >= a2.length) {
                a2 = g.a(a2);
                i = 0;
            }
            int i2 = i + 1;
            a2[i] = a3;
            if (owVar.b() == pb.END_ARRAY) {
                return g.a(a2, i2);
            }
            i = i2;
        }
    }
}
