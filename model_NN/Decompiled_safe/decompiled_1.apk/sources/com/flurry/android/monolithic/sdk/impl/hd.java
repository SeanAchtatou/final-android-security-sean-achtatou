package com.flurry.android.monolithic.sdk.impl;

import java.util.HashMap;

public class hd {
    private static HashMap<String, gz> a = new HashMap<>();

    public void a(String str, String str2, fz fzVar) {
        if (a.containsKey(str)) {
            a.get(str).a(str2, fzVar);
            return;
        }
        gz gzVar = new gz(str);
        gzVar.a(str2, fzVar);
        a.put(str, gzVar);
    }

    public void a(String str, String str2, String str3) {
        if (a.containsKey(str)) {
            a.get(str).a(str2, str3);
        }
    }

    public boolean a(String str) {
        if (!a.containsKey(str)) {
            return false;
        }
        a.remove(str);
        return true;
    }

    public boolean a(String str, String str2) {
        if (!a.containsKey(str)) {
            return false;
        }
        gz gzVar = a.get(str);
        boolean a2 = gzVar.a(str2);
        if (gzVar.a() != 0) {
            return a2;
        }
        a.remove(str);
        return a2;
    }

    public boolean a(String str, fz fzVar) {
        if (!a.containsKey(str)) {
            return false;
        }
        gz gzVar = a.get(str);
        boolean a2 = gzVar.a(fzVar);
        if (gzVar.a() != 0) {
            return a2;
        }
        a.remove(str);
        return a2;
    }

    public int a() {
        return a.size();
    }

    public boolean b(String str) {
        if (!a.containsKey(str)) {
            a.put(str, new gz(str));
        }
        a.get(str).b();
        return true;
    }

    public void b() {
        if (a() > 0) {
            for (gz c : a.values()) {
                c.c();
            }
        }
    }

    public void c() {
        if (a() > 0) {
            for (gz d : a.values()) {
                d.d();
            }
        }
    }
}
