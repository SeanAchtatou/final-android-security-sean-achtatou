package com.flurry.android.monolithic.sdk.impl;

import java.util.Iterator;
import java.util.NoSuchElementException;

final class adq<T> implements Iterable<T>, Iterator<T> {
    private final T[] a;
    private int b = 0;

    public adq(T[] tArr) {
        this.a = tArr;
    }

    public boolean hasNext() {
        return this.b < this.a.length;
    }

    public T next() {
        if (this.b >= this.a.length) {
            throw new NoSuchElementException();
        }
        T[] tArr = this.a;
        int i = this.b;
        this.b = i + 1;
        return tArr[i];
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }

    public Iterator<T> iterator() {
        return this;
    }
}
