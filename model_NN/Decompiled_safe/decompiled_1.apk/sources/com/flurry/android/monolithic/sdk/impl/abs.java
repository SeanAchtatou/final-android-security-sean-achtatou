package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
public class abs extends abt<qz> {
    public static final abs a = new abs();

    protected abs() {
        super(qz.class);
    }

    public void a(qz qzVar, or orVar, ru ruVar) throws IOException, oq {
        qzVar.a(orVar, ruVar);
    }

    public final void a(qz qzVar, or orVar, ru ruVar, rx rxVar) throws IOException, oq {
        qzVar.a(orVar, ruVar, rxVar);
    }
}
