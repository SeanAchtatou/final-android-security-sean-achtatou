package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public final class aff extends afg {
    static final aff c = new aff("");
    final String d;

    public aff(String str) {
        this.d = str;
    }

    public static aff b(String str) {
        if (str == null) {
            return null;
        }
        if (str.length() == 0) {
            return c;
        }
        return new aff(str);
    }

    public boolean e() {
        return true;
    }

    public String h() {
        return this.d;
    }

    public String m() {
        return this.d;
    }

    public double a(double d2) {
        return pt.a(this.d, d2);
    }

    public final void a(or orVar, ru ruVar) throws IOException, oz {
        if (this.d == null) {
            orVar.f();
        } else {
            orVar.b(this.d);
        }
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        return ((aff) obj).d.equals(this.d);
    }

    public int hashCode() {
        return this.d.hashCode();
    }

    public String toString() {
        int length = this.d.length();
        StringBuilder sb = new StringBuilder((length >> 4) + length + 2);
        a(sb, this.d);
        return sb.toString();
    }

    protected static void a(StringBuilder sb, String str) {
        sb.append('\"');
        afr.a(sb, str);
        sb.append('\"');
    }
}
