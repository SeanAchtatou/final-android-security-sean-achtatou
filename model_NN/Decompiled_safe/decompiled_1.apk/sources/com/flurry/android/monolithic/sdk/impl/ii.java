package com.flurry.android.monolithic.sdk.impl;

public final class ii {
    private static final String a = ii.class.getSimpleName();

    public static ik a() {
        try {
            return (ik) Class.forName("com.flurry.android.impl.appcloud.AppCloudModule").getDeclaredMethod("getInstance", new Class[0]).invoke(null, new Object[0]);
        } catch (Exception e) {
            ja.a(5, a, "FlurryAdModule is not available:", e);
            return null;
        }
    }
}
