package com.flurry.android.monolithic.sdk.impl;

final class wl extends we {
    protected final aed<?> b;
    protected final xl c;

    protected wl(aed<?> aed, xl xlVar) {
        super(aed.a());
        this.b = aed;
        this.c = xlVar;
    }

    public Object b(String str, qm qmVar) throws qw {
        if (this.c != null) {
            try {
                return this.c.a(str);
            } catch (Exception e) {
                adz.c(e);
            }
        }
        Object a = this.b.a(str);
        if (a != null) {
            return a;
        }
        throw qmVar.a(this.a, str, "not one of values for Enum class");
    }
}
