package com.flurry.android.monolithic.sdk.impl;

import com.ideaworks3d.marmalade.s3eAndroidGooglePlayBilling.util.IabHelper;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class kx<D> implements lw<D> {
    private static final ThreadLocal<Map<ji, Map<ji, mh>>> f = new ky();
    private final kq a;
    private ji b;
    private ji c;
    private mh d;
    private final Thread e;

    public kx() {
        this(null, null, kq.a());
    }

    protected kx(ji jiVar, ji jiVar2, kq kqVar) {
        this.d = null;
        this.b = jiVar;
        this.c = jiVar2;
        this.a = kqVar;
        this.e = Thread.currentThread();
    }

    public kq a() {
        return this.a;
    }

    /* access modifiers changed from: protected */
    public final mh a(ji jiVar, ji jiVar2) throws IOException {
        Map map;
        Thread currentThread = Thread.currentThread();
        if (currentThread == this.e && this.d != null) {
            return this.d;
        }
        Map map2 = (Map) f.get().get(jiVar);
        if (map2 == null) {
            map = new nx();
            f.get().put(jiVar, map);
        } else {
            map = map2;
        }
        mh mhVar = (mh) map.get(jiVar2);
        if (mhVar == null) {
            mhVar = ly.a().a(ji.a(jiVar, jiVar2), jiVar2, null);
            map.put(jiVar2, mhVar);
        }
        if (currentThread != this.e) {
            return mhVar;
        }
        this.d = mhVar;
        return mhVar;
    }

    public D a(Object obj, lx lxVar) throws IOException {
        mh a2 = a(this.b, this.c);
        a2.a(lxVar);
        D a3 = a(obj, this.c, a2);
        a2.v();
        return a3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.kx.a(com.flurry.android.monolithic.sdk.impl.ji, com.flurry.android.monolithic.sdk.impl.lx):java.lang.Object
     arg types: [com.flurry.android.monolithic.sdk.impl.ji, com.flurry.android.monolithic.sdk.impl.mh]
     candidates:
      com.flurry.android.monolithic.sdk.impl.kx.a(com.flurry.android.monolithic.sdk.impl.ji, com.flurry.android.monolithic.sdk.impl.ji):com.flurry.android.monolithic.sdk.impl.mh
      com.flurry.android.monolithic.sdk.impl.kx.a(java.lang.Object, int):java.lang.Object
      com.flurry.android.monolithic.sdk.impl.kx.a(java.lang.Object, com.flurry.android.monolithic.sdk.impl.lx):D
      com.flurry.android.monolithic.sdk.impl.kx.a(java.lang.String, com.flurry.android.monolithic.sdk.impl.ji):java.lang.Object
      com.flurry.android.monolithic.sdk.impl.lw.a(java.lang.Object, com.flurry.android.monolithic.sdk.impl.lx):D
      com.flurry.android.monolithic.sdk.impl.kx.a(com.flurry.android.monolithic.sdk.impl.ji, com.flurry.android.monolithic.sdk.impl.lx):java.lang.Object */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.kx.a(java.lang.Object, com.flurry.android.monolithic.sdk.impl.ji, com.flurry.android.monolithic.sdk.impl.lx):java.lang.Object
     arg types: [java.lang.Object, com.flurry.android.monolithic.sdk.impl.ji, com.flurry.android.monolithic.sdk.impl.mh]
     candidates:
      com.flurry.android.monolithic.sdk.impl.kx.a(java.lang.Object, int, com.flurry.android.monolithic.sdk.impl.ji):java.lang.Object
      com.flurry.android.monolithic.sdk.impl.kx.a(java.lang.Object, com.flurry.android.monolithic.sdk.impl.ji, com.flurry.android.monolithic.sdk.impl.mh):java.lang.Object
      com.flurry.android.monolithic.sdk.impl.kx.a(java.lang.Object, long, java.lang.Object):void
      com.flurry.android.monolithic.sdk.impl.kx.a(java.lang.Object, java.lang.Object, java.lang.Object):void
      com.flurry.android.monolithic.sdk.impl.kx.a(java.lang.Object, com.flurry.android.monolithic.sdk.impl.ji, com.flurry.android.monolithic.sdk.impl.lx):java.lang.Object */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.kx.b(java.lang.Object, com.flurry.android.monolithic.sdk.impl.ji, com.flurry.android.monolithic.sdk.impl.lx):java.lang.Object
     arg types: [java.lang.Object, com.flurry.android.monolithic.sdk.impl.ji, com.flurry.android.monolithic.sdk.impl.mh]
     candidates:
      com.flurry.android.monolithic.sdk.impl.kx.b(java.lang.Object, com.flurry.android.monolithic.sdk.impl.ji, com.flurry.android.monolithic.sdk.impl.mh):java.lang.Object
      com.flurry.android.monolithic.sdk.impl.kx.b(java.lang.Object, com.flurry.android.monolithic.sdk.impl.ji, com.flurry.android.monolithic.sdk.impl.lx):java.lang.Object */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.kx.c(java.lang.Object, com.flurry.android.monolithic.sdk.impl.ji, com.flurry.android.monolithic.sdk.impl.lx):java.lang.Object
     arg types: [java.lang.Object, com.flurry.android.monolithic.sdk.impl.ji, com.flurry.android.monolithic.sdk.impl.mh]
     candidates:
      com.flurry.android.monolithic.sdk.impl.kx.c(java.lang.Object, com.flurry.android.monolithic.sdk.impl.ji, com.flurry.android.monolithic.sdk.impl.mh):java.lang.Object
      com.flurry.android.monolithic.sdk.impl.kx.c(java.lang.Object, com.flurry.android.monolithic.sdk.impl.ji, com.flurry.android.monolithic.sdk.impl.lx):java.lang.Object */
    /* access modifiers changed from: protected */
    public Object a(Object obj, ji jiVar, mh mhVar) throws IOException {
        switch (kz.a[jiVar.a().ordinal()]) {
            case 1:
                return b(obj, jiVar, mhVar);
            case 2:
                return a(jiVar, (lx) mhVar);
            case 3:
                return c(obj, jiVar, mhVar);
            case 4:
                return d(obj, jiVar, mhVar);
            case 5:
                return a(obj, jiVar.k().get(mhVar.s()), mhVar);
            case 6:
                return a(obj, jiVar, (lx) mhVar);
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED /*7*/:
                return b(obj, jiVar, (lx) mhVar);
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED /*8*/:
                return c(obj, mhVar);
            case 9:
                return c(obj, jiVar, (lx) mhVar);
            case 10:
                return Long.valueOf(mhVar.e());
            case 11:
                return Float.valueOf(mhVar.f());
            case 12:
                return Double.valueOf(mhVar.g());
            case 13:
                return Boolean.valueOf(mhVar.c());
            case 14:
                mhVar.b();
                return null;
            default:
                throw new jg("Unknown type: " + jiVar);
        }
    }

    /* access modifiers changed from: protected */
    public Object b(Object obj, ji jiVar, mh mhVar) throws IOException {
        Object d2 = this.a.d(obj, jiVar);
        Object a2 = this.a.a(d2, jiVar);
        for (js jsVar : mhVar.u()) {
            int b2 = jsVar.b();
            String a3 = jsVar.a();
            this.a.a(d2, a3, b2, a(obj != null ? this.a.b(d2, a3, b2, a2) : null, jsVar.c(), mhVar), a2);
        }
        return d2;
    }

    /* access modifiers changed from: protected */
    public Object a(ji jiVar, lx lxVar) throws IOException {
        return a(jiVar.c().get(lxVar.k()), jiVar);
    }

    /* access modifiers changed from: protected */
    public Object a(String str, ji jiVar) {
        return new ku(jiVar, str);
    }

    /* access modifiers changed from: protected */
    public Object c(Object obj, ji jiVar, mh mhVar) throws IOException {
        ji i = jiVar.i();
        long m = mhVar.m();
        if (m <= 0) {
            return a(obj, 0, jiVar);
        }
        Object a2 = a(obj, (int) m, jiVar);
        long j = m;
        long j2 = 0;
        do {
            for (long j3 = 0; j3 < j; j3++) {
                a(a2, j2 + j3, a(a(a2), i, mhVar));
            }
            j2 += j;
            j = mhVar.n();
        } while (j > 0);
        return a2;
    }

    /* access modifiers changed from: protected */
    public Object a(Object obj) {
        if (obj instanceof ko) {
            return ((ko) obj).b();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void a(Object obj, long j, Object obj2) {
        ((Collection) obj).add(obj2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.kx.b(java.lang.Object, com.flurry.android.monolithic.sdk.impl.ji, com.flurry.android.monolithic.sdk.impl.lx):java.lang.Object
     arg types: [?[OBJECT, ARRAY], com.flurry.android.monolithic.sdk.impl.ji, com.flurry.android.monolithic.sdk.impl.mh]
     candidates:
      com.flurry.android.monolithic.sdk.impl.kx.b(java.lang.Object, com.flurry.android.monolithic.sdk.impl.ji, com.flurry.android.monolithic.sdk.impl.mh):java.lang.Object
      com.flurry.android.monolithic.sdk.impl.kx.b(java.lang.Object, com.flurry.android.monolithic.sdk.impl.ji, com.flurry.android.monolithic.sdk.impl.lx):java.lang.Object */
    /* access modifiers changed from: protected */
    public Object d(Object obj, ji jiVar, mh mhVar) throws IOException {
        ji j = jiVar.j();
        long p = mhVar.p();
        Object a2 = a(obj, (int) p);
        if (p > 0) {
            do {
                for (int i = 0; ((long) i) < p; i++) {
                    a(a2, b((Object) null, jiVar, (lx) mhVar), a((Object) null, j, mhVar));
                }
                p = mhVar.q();
            } while (p > 0);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public void a(Object obj, Object obj2, Object obj3) {
        ((Map) obj).put(obj2, obj3);
    }

    /* access modifiers changed from: protected */
    public Object a(Object obj, ji jiVar, lx lxVar) throws IOException {
        ld ldVar = (ld) this.a.c(obj, jiVar);
        lxVar.b(ldVar.b(), 0, jiVar.l());
        return ldVar;
    }

    /* access modifiers changed from: protected */
    public Object a(Object obj, int i, ji jiVar) {
        if (!(obj instanceof Collection)) {
            return new ks(i, jiVar);
        }
        ((Collection) obj).clear();
        return obj;
    }

    /* access modifiers changed from: protected */
    public Object a(Object obj, int i) {
        if (!(obj instanceof Map)) {
            return new HashMap(i);
        }
        ((Map) obj).clear();
        return obj;
    }

    /* access modifiers changed from: protected */
    public Object b(Object obj, ji jiVar, lx lxVar) throws IOException {
        kq kqVar = this.a;
        kq kqVar2 = this.a;
        if ("String".equals(jiVar.a("avro.java.string"))) {
            return lxVar.h();
        }
        return b(obj, lxVar);
    }

    /* access modifiers changed from: protected */
    public Object b(Object obj, lx lxVar) throws IOException {
        return lxVar.a(obj instanceof nw ? (nw) obj : null);
    }

    /* access modifiers changed from: protected */
    public Object c(Object obj, lx lxVar) throws IOException {
        return lxVar.a(obj instanceof ByteBuffer ? (ByteBuffer) obj : null);
    }

    /* access modifiers changed from: protected */
    public Object c(Object obj, ji jiVar, lx lxVar) throws IOException {
        return Integer.valueOf(lxVar.d());
    }
}
