package com.flurry.android.monolithic.sdk.impl;

class mn extends nm {
    public ji a;

    public mn(ji jiVar, ji jiVar2) {
        super(jiVar);
        this.a = jiVar2;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof mn)) {
            return false;
        }
        mn mnVar = (mn) obj;
        return this.b == mnVar.b && this.a == mnVar.a;
    }

    public int hashCode() {
        return super.hashCode() + this.a.hashCode();
    }
}
