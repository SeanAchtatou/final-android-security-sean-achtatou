package com.flurry.android.monolithic.sdk.impl;

import android.text.TextUtils;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ic {
    private Map<String, Object> a = new HashMap();
    private Map<String, List<id>> b = new HashMap();

    ic() {
    }

    public synchronized void a(String str, Object obj) {
        if (!TextUtils.isEmpty(str)) {
            if (!a(obj, this.a.get(str))) {
                if (obj == null) {
                    this.a.remove(str);
                } else {
                    this.a.put(str, obj);
                }
                b(str, obj);
            }
        }
    }

    private boolean a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    private void b(String str, Object obj) {
        if (this.b.get(str) != null) {
            for (id a2 : this.b.get(str)) {
                a2.a(str, obj);
            }
        }
    }

    public synchronized Object a(String str) {
        return this.a.get(str);
    }

    public synchronized void a(String str, id idVar) {
        if (idVar != null) {
            Object obj = this.b.get(str);
            if (obj == null) {
                obj = new LinkedList();
            }
            obj.add(idVar);
            this.b.put(str, obj);
        }
    }

    public synchronized boolean b(String str, id idVar) {
        boolean remove;
        if (idVar == null) {
            remove = false;
        } else {
            List list = this.b.get(str);
            if (list == null) {
                remove = false;
            } else {
                remove = list.remove(idVar);
            }
        }
        return remove;
    }
}
