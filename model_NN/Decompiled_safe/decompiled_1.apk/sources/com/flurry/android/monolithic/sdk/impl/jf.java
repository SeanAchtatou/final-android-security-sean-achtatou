package com.flurry.android.monolithic.sdk.impl;

import java.io.PrintStream;
import java.io.PrintWriter;

public abstract class jf implements Runnable {
    private static final String a = jf.class.getSimpleName();
    PrintStream i;
    PrintWriter j;

    public abstract void a();

    public final void run() {
        try {
            a();
        } catch (Throwable th) {
            if (this.i != null) {
                th.printStackTrace(this.i);
            } else if (this.j != null) {
                th.printStackTrace(this.j);
            } else {
                th.printStackTrace();
            }
            ja.a(6, a, "", th);
        }
    }
}
