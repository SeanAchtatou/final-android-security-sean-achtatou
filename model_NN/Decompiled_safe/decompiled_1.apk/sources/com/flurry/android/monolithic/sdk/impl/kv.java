package com.flurry.android.monolithic.sdk.impl;

import java.util.Arrays;

public class kv implements ld, Comparable<kv> {
    private ji a;
    private byte[] b;

    public kv(ji jiVar) {
        a(jiVar);
    }

    protected kv() {
    }

    /* access modifiers changed from: protected */
    public void a(ji jiVar) {
        this.a = jiVar;
        this.b = new byte[jiVar.l()];
    }

    public ji a() {
        return this.a;
    }

    public byte[] b() {
        return this.b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return (obj instanceof ld) && Arrays.equals(this.b, ((ld) obj).b());
    }

    public int hashCode() {
        return Arrays.hashCode(this.b);
    }

    public String toString() {
        return Arrays.toString(this.b);
    }

    /* renamed from: a */
    public int compareTo(kv kvVar) {
        return lg.a(this.b, 0, this.b.length, kvVar.b, 0, kvVar.b.length);
    }
}
