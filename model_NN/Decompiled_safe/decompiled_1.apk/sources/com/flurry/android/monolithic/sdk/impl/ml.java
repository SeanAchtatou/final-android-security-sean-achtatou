package com.flurry.android.monolithic.sdk.impl;

import com.ideaworks3d.marmalade.s3eAndroidGooglePlayBilling.util.IabHelper;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ml extends nk {
    private static md a = new md().a(32);

    public final mq a(ji jiVar, ji jiVar2) throws IOException {
        return mq.a(a(jiVar, jiVar2, new HashMap()));
    }

    public mq a(ji jiVar, ji jiVar2, Map<nm, mq> map) throws IOException {
        kj a2 = jiVar.a();
        kj a3 = jiVar2.a();
        if (a2 != a3) {
            if (a2 != kj.UNION) {
                switch (mm.a[a3.ordinal()]) {
                    case 1:
                    case 2:
                    case 3:
                    case IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED /*7*/:
                    case IabHelper.BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED /*8*/:
                    case 10:
                    case 11:
                    case 12:
                    case 13:
                        break;
                    case 4:
                        switch (mm.a[a2.ordinal()]) {
                            case 3:
                                return mq.a(super.a(jiVar, map), mq.f);
                        }
                    case 5:
                        switch (mm.a[a2.ordinal()]) {
                            case 3:
                            case 4:
                                return mq.a(super.a(jiVar, map), mq.g);
                        }
                    case 6:
                        switch (mm.a[a2.ordinal()]) {
                            case 3:
                            case 4:
                            case 5:
                                return mq.a(super.a(jiVar, map), mq.h);
                        }
                    case 9:
                    default:
                        throw new RuntimeException("Unexpected schema type: " + a3);
                    case 14:
                        int b = b(jiVar2, jiVar);
                        if (b >= 0) {
                            return mq.b(new ni(b, a(jiVar, jiVar2.k().get(b), map)), mq.m);
                        }
                        break;
                }
            } else {
                return b(jiVar, jiVar2, map);
            }
        } else {
            switch (mm.a[a2.ordinal()]) {
                case 1:
                    return mq.c;
                case 2:
                    return mq.d;
                case 3:
                    return mq.e;
                case 4:
                    return mq.f;
                case 5:
                    return mq.g;
                case 6:
                    return mq.h;
                case IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED /*7*/:
                    return mq.i;
                case IabHelper.BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED /*8*/:
                    return mq.j;
                case 9:
                    if (jiVar.g().equals(jiVar2.g()) && jiVar.l() == jiVar2.l()) {
                        return mq.b(new mz(jiVar.l()), mq.k);
                    }
                case 10:
                    if (jiVar.g() == null || jiVar.g().equals(jiVar2.g())) {
                        return mq.b(a(jiVar.c(), jiVar2.c()), mq.l);
                    }
                case 11:
                    return mq.b(mq.a(mq.o, a(jiVar.i(), jiVar2.i(), map)), mq.n);
                case 12:
                    return mq.b(mq.a(mq.q, a(jiVar.j(), jiVar2.j(), map), mq.i), mq.p);
                case 13:
                    return c(jiVar, jiVar2, map);
                case 14:
                    return b(jiVar, jiVar2, map);
                default:
                    throw new jh("Unkown type for schema: " + a2);
            }
        }
        return mq.a("Found " + jiVar.a(true) + ", expecting " + jiVar2.a(true));
    }

    private mq b(ji jiVar, ji jiVar2, Map<nm, mq> map) throws IOException {
        List<ji> k = jiVar.k();
        int size = k.size();
        mq[] mqVarArr = new mq[size];
        String[] strArr = new String[size];
        int i = 0;
        for (ji next : k) {
            mqVarArr[i] = a(next, jiVar2, map);
            strArr[i] = next.g();
            i++;
        }
        return mq.b(mq.a(mqVarArr, strArr), new nj());
    }

    private mq c(ji jiVar, ji jiVar2, Map<nm, mq> map) throws IOException {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        mn mnVar = new mn(jiVar, jiVar2);
        mq mqVar = map.get(mnVar);
        if (mqVar != null) {
            return mqVar;
        }
        List<js> b = jiVar.b();
        List<js> b2 = jiVar2.b();
        js[] jsVarArr = new js[b2.size()];
        int i7 = 0;
        int size = b.size() + 1;
        Iterator<js> it = b.iterator();
        while (true) {
            i = i7;
            if (!it.hasNext()) {
                break;
            }
            js b3 = jiVar2.b(it.next().a());
            if (b3 != null) {
                jsVarArr[i] = b3;
                i7 = i + 1;
            } else {
                i7 = i;
            }
        }
        for (js next : b2) {
            if (jiVar.b(next.a()) != null) {
                i5 = size;
                i6 = i;
            } else if (next.e() == null) {
                mq a2 = mq.a("Found " + jiVar.a(true) + ", expecting " + jiVar2.a(true));
                map.put(mnVar, a2);
                return a2;
            } else {
                jsVarArr[i] = next;
                i5 = size + 3;
                i6 = i + 1;
            }
            i = i6;
            size = i5;
        }
        mq[] mqVarArr = new mq[size];
        int i8 = size - 1;
        mqVarArr[i8] = new mw(jsVarArr);
        mq b4 = mq.b(mqVarArr);
        map.put(mnVar, b4);
        Iterator<js> it2 = b.iterator();
        while (true) {
            i2 = i8;
            if (!it2.hasNext()) {
                break;
            }
            js next2 = it2.next();
            js b5 = jiVar2.b(next2.a());
            if (b5 == null) {
                i4 = i2 - 1;
                mqVarArr[i4] = new ng(a(next2.c(), next2.c(), map));
            } else {
                i4 = i2 - 1;
                mqVarArr[i4] = a(next2.c(), b5.c(), map);
            }
            i8 = i4;
        }
        for (js next3 : b2) {
            if (jiVar.b(next3.a()) == null) {
                int i9 = i2 - 1;
                mqVarArr[i9] = new mt(a(next3.c(), next3.e()));
                int i10 = i9 - 1;
                mqVarArr[i10] = a(next3.c(), next3.c(), map);
                i3 = i10 - 1;
                mqVarArr[i3] = mq.x;
            } else {
                i3 = i2;
            }
            i2 = i3;
        }
        return b4;
    }

    private static byte[] a(ji jiVar, ou ouVar) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        lr a2 = a.a(byteArrayOutputStream, null);
        a(a2, jiVar, ouVar);
        a2.flush();
        return byteArrayOutputStream.toByteArray();
    }

    public static void a(mc mcVar, ji jiVar, ou ouVar) throws IOException {
        switch (mm.a[jiVar.a().ordinal()]) {
            case 1:
                if (!ouVar.g()) {
                    throw new jh("Non-null default value for null type: " + ouVar);
                }
                mcVar.a();
                return;
            case 2:
                if (!ouVar.f()) {
                    throw new jh("Non-boolean default for boolean: " + ouVar);
                }
                mcVar.a(ouVar.i());
                return;
            case 3:
                if (!ouVar.c()) {
                    throw new jh("Non-numeric default value for int: " + ouVar);
                }
                mcVar.c(ouVar.j());
                return;
            case 4:
                if (!ouVar.c()) {
                    throw new jh("Non-numeric default value for long: " + ouVar);
                }
                mcVar.b(ouVar.k());
                return;
            case 5:
                if (!ouVar.c()) {
                    throw new jh("Non-numeric default value for float: " + ouVar);
                }
                mcVar.a((float) ouVar.l());
                return;
            case 6:
                if (!ouVar.c()) {
                    throw new jh("Non-numeric default value for double: " + ouVar);
                }
                mcVar.a(ouVar.l());
                return;
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED /*7*/:
                if (!ouVar.e()) {
                    throw new jh("Non-string default value for string: " + ouVar);
                }
                mcVar.a(ouVar.h());
                return;
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED /*8*/:
                if (!ouVar.e()) {
                    throw new jh("Non-string default value for bytes: " + ouVar);
                }
                mcVar.a(ouVar.h().getBytes("ISO-8859-1"));
                return;
            case 9:
                if (!ouVar.e()) {
                    throw new jh("Non-string default value for fixed: " + ouVar);
                }
                byte[] bytes = ouVar.h().getBytes("ISO-8859-1");
                if (bytes.length != jiVar.l()) {
                    byte[] bArr = new byte[jiVar.l()];
                    System.arraycopy(bytes, 0, bArr, 0, jiVar.l() > bytes.length ? bytes.length : jiVar.l());
                    bytes = bArr;
                }
                mcVar.b(bytes);
                return;
            case 10:
                mcVar.a(jiVar.c(ouVar.h()));
                return;
            case 11:
                mcVar.b();
                mcVar.a((long) ouVar.o());
                ji i = jiVar.i();
                Iterator<ou> it = ouVar.iterator();
                while (it.hasNext()) {
                    mcVar.c();
                    a(mcVar, i, it.next());
                }
                mcVar.d();
                return;
            case 12:
                mcVar.e();
                mcVar.a((long) ouVar.o());
                ji j = jiVar.j();
                Iterator<String> q = ouVar.q();
                while (q.hasNext()) {
                    mcVar.c();
                    String next = q.next();
                    mcVar.a(next);
                    a(mcVar, j, ouVar.a(next));
                }
                mcVar.f();
                return;
            case 13:
                for (js next2 : jiVar.b()) {
                    String a2 = next2.a();
                    ou a3 = ouVar.a(a2);
                    if (a3 == null) {
                        a3 = next2.e();
                    }
                    if (a3 == null) {
                        throw new jh("No default value for: " + a2);
                    }
                    a(mcVar, next2.c(), a3);
                }
                return;
            case 14:
                mcVar.b(0);
                a(mcVar, jiVar.k().get(0), ouVar);
                return;
            default:
                return;
        }
    }

    private static mq a(List<String> list, List<String> list2) {
        Object[] objArr = new Object[list.size()];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= objArr.length) {
                return new mu(list2.size(), objArr);
            }
            int indexOf = list2.indexOf(list.get(i2));
            objArr[i2] = indexOf == -1 ? "No match for " + list.get(i2) : new Integer(indexOf);
            i = i2 + 1;
        }
    }

    private static int b(ji jiVar, ji jiVar2) {
        kj a2 = jiVar2.a();
        int i = 0;
        for (ji next : jiVar.k()) {
            if (a2 == next.a()) {
                if (a2 != kj.RECORD && a2 != kj.ENUM && a2 != kj.FIXED) {
                    return i;
                }
                String g = jiVar2.g();
                String g2 = next.g();
                if ((g != null && g.equals(g2)) || (g == g2 && a2 == kj.RECORD)) {
                    return i;
                }
            }
            i++;
        }
        int i2 = 0;
        for (ji next2 : jiVar.k()) {
            switch (mm.a[a2.ordinal()]) {
                case 3:
                    switch (mm.a[next2.a().ordinal()]) {
                        case 4:
                        case 6:
                            return i2;
                    }
                case 4:
                case 5:
                    switch (mm.a[next2.a().ordinal()]) {
                        case 6:
                            return i2;
                    }
            }
            i2++;
        }
        return -1;
    }
}
