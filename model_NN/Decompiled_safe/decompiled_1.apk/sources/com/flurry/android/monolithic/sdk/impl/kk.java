package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

class kk extends ji {
    private final List<ji> f;
    private final Map<String, Integer> g = new HashMap();

    public kk(jx<ji> jxVar) {
        super(kj.UNION);
        this.f = jxVar.a();
        Iterator<ji> it = jxVar.iterator();
        int i = 0;
        while (it.hasNext()) {
            ji next = it.next();
            if (next.a() == kj.UNION) {
                throw new jg("Nested union: " + this);
            }
            String g2 = next.g();
            if (g2 == null) {
                throw new jg("Nameless in union:" + this);
            }
            int i2 = i + 1;
            if (this.g.put(g2, Integer.valueOf(i)) != null) {
                throw new jg("Duplicate in union:" + g2);
            }
            i = i2;
        }
    }

    public List<ji> k() {
        return this.f;
    }

    public Integer e(String str) {
        return this.g.get(str);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof kk)) {
            return false;
        }
        kk kkVar = (kk) obj;
        return c(kkVar) && this.f.equals(kkVar.f) && this.c.equals(kkVar.c);
    }

    /* access modifiers changed from: package-private */
    public int m() {
        int m = super.m();
        for (ji m2 : this.f) {
            m += m2.m();
        }
        return m;
    }

    public void a(String str, String str2) {
        throw new jg("Can't set properties on a union: " + this);
    }

    /* access modifiers changed from: package-private */
    public void a(kc kcVar, or orVar) throws IOException {
        orVar.b();
        for (ji a : this.f) {
            a.a(kcVar, orVar);
        }
        orVar.c();
    }
}
