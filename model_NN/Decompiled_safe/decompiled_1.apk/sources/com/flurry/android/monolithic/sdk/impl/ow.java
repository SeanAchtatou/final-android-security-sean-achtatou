package com.flurry.android.monolithic.sdk.impl;

import java.io.Closeable;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;

public abstract class ow implements Closeable {
    protected int a;
    protected pb b;
    protected pb c;

    public abstract pc a();

    public abstract byte[] a(on onVar) throws IOException, ov;

    public abstract pb b() throws IOException, ov;

    public abstract void close() throws IOException;

    public abstract ow d() throws IOException, ov;

    public abstract String g() throws IOException, ov;

    public abstract ot h();

    public abstract ot i();

    public abstract String k() throws IOException, ov;

    public abstract char[] l() throws IOException, ov;

    public abstract int m() throws IOException, ov;

    public abstract int n() throws IOException, ov;

    public abstract Number p() throws IOException, ov;

    public abstract oy q() throws IOException, ov;

    public abstract int t() throws IOException, ov;

    public abstract long u() throws IOException, ov;

    public abstract BigInteger v() throws IOException, ov;

    public abstract float w() throws IOException, ov;

    public abstract double x() throws IOException, ov;

    public abstract BigDecimal y() throws IOException, ov;

    protected ow() {
    }

    protected ow(int i) {
        this.a = i;
    }

    public boolean a(ox oxVar) {
        return (this.a & oxVar.c()) != 0;
    }

    public pb c() throws IOException, ov {
        pb b2 = b();
        if (b2 == pb.FIELD_NAME) {
            return b();
        }
        return b2;
    }

    public pb e() {
        return this.b;
    }

    public void f() {
        if (this.b != null) {
            this.c = this.b;
            this.b = null;
        }
    }

    public boolean j() {
        return e() == pb.START_ARRAY;
    }

    public boolean o() {
        return false;
    }

    public byte r() throws IOException, ov {
        int t = t();
        if (t >= -128 && t <= 127) {
            return (byte) t;
        }
        throw a("Numeric value (" + k() + ") out of range of Java byte");
    }

    public short s() throws IOException, ov {
        int t = t();
        if (t >= -32768 && t <= 32767) {
            return (short) t;
        }
        throw a("Numeric value (" + k() + ") out of range of Java short");
    }

    public Object z() throws IOException, ov {
        return null;
    }

    /* access modifiers changed from: protected */
    public ov a(String str) {
        return new ov(str, i());
    }
}
