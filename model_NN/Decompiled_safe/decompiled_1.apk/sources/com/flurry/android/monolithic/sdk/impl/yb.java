package com.flurry.android.monolithic.sdk.impl;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class yb {
    protected final rf<?> a;
    protected final boolean b;
    protected final afm c;
    protected final xh d;
    protected final ye<?> e;
    protected final py f;
    protected final LinkedHashMap<String, yc> g = new LinkedHashMap<>();
    protected LinkedList<yc> h = null;
    protected LinkedList<xl> i = null;
    protected LinkedList<xl> j = null;
    protected LinkedList<xl> k = null;
    protected Set<String> l;
    protected Set<String> m;
    protected LinkedHashMap<Object, xk> n;

    protected yb(rf<?> rfVar, boolean z, afm afm, xh xhVar) {
        this.a = rfVar;
        this.b = z;
        this.c = afm;
        this.d = xhVar;
        this.f = rfVar.b() ? this.a.a() : null;
        if (this.f == null) {
            this.e = this.a.e();
        } else {
            this.e = this.f.a(xhVar, this.a.e());
        }
    }

    public rf<?> a() {
        return this.a;
    }

    public afm b() {
        return this.c;
    }

    public xh c() {
        return this.d;
    }

    public List<qe> d() {
        return new ArrayList(this.g.values());
    }

    public Map<Object, xk> e() {
        return this.n;
    }

    public xl f() {
        if (this.k == null) {
            return null;
        }
        if (this.k.size() > 1) {
            a("Multiple value properties defined (" + this.k.get(0) + " vs " + this.k.get(1) + ")");
        }
        return this.k.get(0);
    }

    public xl g() {
        if (this.i == null) {
            return null;
        }
        if (this.i.size() > 1) {
            a("Multiple 'any-getters' defined (" + this.i.get(0) + " vs " + this.i.get(1) + ")");
        }
        return this.i.getFirst();
    }

    public xl h() {
        if (this.j == null) {
            return null;
        }
        if (this.j.size() > 1) {
            a("Multiple 'any-setters' defined (" + this.j.get(0) + " vs " + this.j.get(1) + ")");
        }
        return this.j.getFirst();
    }

    public Set<String> i() {
        return this.l;
    }

    public Set<String> j() {
        return this.m;
    }

    public yb k() {
        this.g.clear();
        m();
        o();
        n();
        p();
        q();
        r();
        rl j2 = this.a.j();
        if (j2 != null) {
            a(j2);
        }
        for (yc p : this.g.values()) {
            p.p();
        }
        for (yc a2 : this.g.values()) {
            a2.a(this.b);
        }
        l();
        return this;
    }

    /* access modifiers changed from: protected */
    public void l() {
        boolean booleanValue;
        AbstractMap linkedHashMap;
        String str;
        py a2 = this.a.a();
        Boolean i2 = a2.i(this.d);
        if (i2 == null) {
            booleanValue = this.a.d();
        } else {
            booleanValue = i2.booleanValue();
        }
        String[] h2 = a2.h(this.d);
        if (booleanValue || this.h != null || h2 != null) {
            int size = this.g.size();
            if (booleanValue) {
                linkedHashMap = new TreeMap();
            } else {
                linkedHashMap = new LinkedHashMap(size + size);
            }
            for (yc next : this.g.values()) {
                linkedHashMap.put(next.a(), next);
            }
            LinkedHashMap linkedHashMap2 = new LinkedHashMap(size + size);
            if (h2 != null) {
                for (String str2 : h2) {
                    yc ycVar = (yc) linkedHashMap.get(str2);
                    if (ycVar == null) {
                        Iterator<yc> it = this.g.values().iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            yc next2 = it.next();
                            if (str2.equals(next2.l())) {
                                yc ycVar2 = next2;
                                str = next2.a();
                                ycVar = ycVar2;
                                break;
                            }
                        }
                    }
                    str = str2;
                    if (ycVar != null) {
                        linkedHashMap2.put(str, ycVar);
                    }
                }
            }
            if (this.h != null) {
                Iterator<yc> it2 = this.h.iterator();
                while (it2.hasNext()) {
                    yc next3 = it2.next();
                    linkedHashMap2.put(next3.a(), next3);
                }
            }
            linkedHashMap2.putAll(linkedHashMap);
            this.g.clear();
            this.g.putAll(linkedHashMap2);
        }
    }

    /* access modifiers changed from: protected */
    public void m() {
        String c2;
        boolean z;
        boolean z2;
        py pyVar = this.f;
        for (xj next : this.d.l()) {
            String b2 = next.b();
            if (pyVar == null) {
                c2 = null;
            } else if (this.b) {
                c2 = pyVar.b(next);
            } else {
                c2 = pyVar.c(next);
            }
            if ("".equals(c2)) {
                c2 = b2;
            }
            if (c2 != null) {
                z = true;
            } else {
                z = false;
            }
            if (!z) {
                z = this.e.a(next);
            }
            if (pyVar == null || !pyVar.c((xk) next)) {
                z2 = false;
            } else {
                z2 = true;
            }
            b(b2).a(next, c2, z, z2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.yc.a(com.flurry.android.monolithic.sdk.impl.xn, java.lang.String, boolean, boolean):void
     arg types: [com.flurry.android.monolithic.sdk.impl.xn, java.lang.String, int, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.yc.a(com.flurry.android.monolithic.sdk.impl.xj, java.lang.String, boolean, boolean):void
      com.flurry.android.monolithic.sdk.impl.yc.a(com.flurry.android.monolithic.sdk.impl.xl, java.lang.String, boolean, boolean):void
      com.flurry.android.monolithic.sdk.impl.yc.a(com.flurry.android.monolithic.sdk.impl.xn, java.lang.String, boolean, boolean):void */
    /* access modifiers changed from: protected */
    public void n() {
        py pyVar = this.f;
        if (pyVar != null) {
            for (xi next : this.d.i()) {
                if (this.h == null) {
                    this.h = new LinkedList<>();
                }
                int f2 = next.f();
                for (int i2 = 0; i2 < f2; i2++) {
                    xn c2 = next.c(i2);
                    String a2 = pyVar.a(c2);
                    if (a2 != null) {
                        yc b2 = b(a2);
                        b2.a(c2, a2, true, false);
                        this.h.add(b2);
                    }
                }
            }
            for (xl next2 : this.d.j()) {
                if (this.h == null) {
                    this.h = new LinkedList<>();
                }
                int f3 = next2.f();
                for (int i3 = 0; i3 < f3; i3++) {
                    xn c3 = next2.c(i3);
                    String a3 = pyVar.a(c3);
                    if (a3 != null) {
                        yc b3 = b(a3);
                        b3.a(c3, a3, true, false);
                        this.h.add(b3);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void o() {
        String b2;
        String str;
        boolean z;
        String a2;
        String str2;
        boolean z2;
        py pyVar = this.f;
        for (xl next : this.d.k()) {
            int f2 = next.f();
            if (f2 == 0) {
                if (pyVar != null) {
                    if (pyVar.f(next)) {
                        if (this.i == null) {
                            this.i = new LinkedList<>();
                        }
                        this.i.add(next);
                    } else if (pyVar.c(next)) {
                        if (this.k == null) {
                            this.k = new LinkedList<>();
                        }
                        this.k.add(next);
                    }
                }
                String b3 = pyVar == null ? null : pyVar.b(next);
                if (b3 == null) {
                    a2 = ady.a(next, next.b());
                    if (a2 == null) {
                        a2 = ady.b(next, next.b());
                        if (a2 != null) {
                            str2 = b3;
                            z2 = this.e.b(next);
                        }
                    } else {
                        str2 = b3;
                        z2 = this.e.a(next);
                    }
                } else {
                    a2 = ady.a(next);
                    if (a2 == null) {
                        a2 = next.b();
                    }
                    if (b3.length() == 0) {
                        b3 = a2;
                    }
                    str2 = b3;
                    z2 = true;
                }
                b(a2).a(next, str2, z2, pyVar == null ? false : pyVar.c((xk) next));
            } else if (f2 == 1) {
                String d2 = pyVar == null ? null : pyVar.d(next);
                if (d2 == null) {
                    b2 = ady.b(next);
                    if (b2 != null) {
                        str = d2;
                        z = this.e.c(next);
                    }
                } else {
                    b2 = ady.b(next);
                    if (b2 == null) {
                        b2 = next.b();
                    }
                    if (d2.length() == 0) {
                        d2 = b2;
                    }
                    str = d2;
                    z = true;
                }
                b(b2).b(next, str, z, pyVar == null ? false : pyVar.c((xk) next));
            } else if (f2 == 2 && pyVar != null && pyVar.e(next)) {
                if (this.j == null) {
                    this.j = new LinkedList<>();
                }
                this.j.add(next);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void p() {
        py pyVar = this.f;
        if (pyVar != null) {
            for (xj next : this.d.l()) {
                a(pyVar.d((xk) next), next);
            }
            for (xl next2 : this.d.k()) {
                if (next2.f() == 1) {
                    a(pyVar.d((xk) next2), next2);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(Object obj, xk xkVar) {
        if (obj != null) {
            if (this.n == null) {
                this.n = new LinkedHashMap<>();
            }
            if (this.n.put(obj, xkVar) != null) {
                throw new IllegalArgumentException("Duplicate injectable value with id '" + String.valueOf(obj) + "' (of type " + (obj == null ? "[null]" : obj.getClass().getName()) + ")");
            }
        }
    }

    /* access modifiers changed from: protected */
    public void q() {
        Iterator<Map.Entry<String, yc>> it = this.g.entrySet().iterator();
        while (it.hasNext()) {
            yc ycVar = (yc) it.next().getValue();
            if (!ycVar.r()) {
                it.remove();
            } else {
                if (ycVar.s()) {
                    a(ycVar);
                    if (!ycVar.q()) {
                        it.remove();
                    } else {
                        ycVar.n();
                    }
                }
                ycVar.o();
            }
        }
    }

    private void a(yc ycVar) {
        if (!this.b) {
            String a2 = ycVar.a();
            this.l = a(this.l, a2);
            if (ycVar.t()) {
                this.m = a(this.m, a2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void r() {
        LinkedList linkedList;
        Iterator<Map.Entry<String, yc>> it = this.g.entrySet().iterator();
        LinkedList linkedList2 = null;
        while (true) {
            linkedList = linkedList2;
            if (!it.hasNext()) {
                break;
            }
            yc ycVar = (yc) it.next().getValue();
            String u = ycVar.u();
            if (u != null) {
                if (linkedList == null) {
                    linkedList = new LinkedList();
                }
                linkedList.add(ycVar.a(u));
                it.remove();
            }
            linkedList2 = linkedList;
        }
        if (linkedList != null) {
            Iterator it2 = linkedList.iterator();
            while (it2.hasNext()) {
                yc ycVar2 = (yc) it2.next();
                String a2 = ycVar2.a();
                yc ycVar3 = this.g.get(a2);
                if (ycVar3 == null) {
                    this.g.put(a2, ycVar2);
                } else {
                    ycVar3.b(ycVar2);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(rl rlVar) {
        yc ycVar;
        yc[] ycVarArr = (yc[]) this.g.values().toArray(new yc[this.g.size()]);
        this.g.clear();
        for (yc ycVar2 : ycVarArr) {
            String a2 = ycVar2.a();
            if (this.b) {
                if (ycVar2.b()) {
                    a2 = rlVar.a(this.a, ycVar2.g(), a2);
                } else if (ycVar2.d()) {
                    a2 = rlVar.a(this.a, ycVar2.i(), a2);
                }
            } else if (ycVar2.c()) {
                a2 = rlVar.b(this.a, ycVar2.h(), a2);
            } else if (ycVar2.e()) {
                a2 = rlVar.a(this.a, ycVar2.m(), a2);
            } else if (ycVar2.d()) {
                a2 = rlVar.a(this.a, ycVar2.i(), a2);
            } else if (ycVar2.b()) {
                a2 = rlVar.a(this.a, ycVar2.g(), a2);
            }
            if (!a2.equals(ycVar2.a())) {
                ycVar = ycVar2.a(a2);
            } else {
                ycVar = ycVar2;
            }
            yc ycVar3 = this.g.get(a2);
            if (ycVar3 == null) {
                this.g.put(a2, ycVar);
            } else {
                ycVar3.b(ycVar);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        throw new IllegalArgumentException("Problem with definition of " + this.d + ": " + str);
    }

    /* access modifiers changed from: protected */
    public yc b(String str) {
        yc ycVar = this.g.get(str);
        if (ycVar != null) {
            return ycVar;
        }
        yc ycVar2 = new yc(str);
        this.g.put(str, ycVar2);
        return ycVar2;
    }

    private Set<String> a(Set<String> set, String str) {
        Set<String> set2;
        if (set == null) {
            set2 = new HashSet<>();
        } else {
            set2 = set;
        }
        set2.add(str);
        return set2;
    }
}
