package com.flurry.android.monolithic.sdk.impl;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.LinearLayout;
import com.flurry.android.AdCreative;
import com.flurry.android.AdNetworkView;
import com.flurry.android.impl.ads.FlurryAdModule;
import com.mobclix.android.sdk.MobclixAdView;
import com.mobclix.android.sdk.MobclixIABRectangleMAdView;
import com.mobclix.android.sdk.MobclixMMABannerXLAdView;

public class ec extends AdNetworkView {
    /* access modifiers changed from: private */
    public static final String a = ec.class.getSimpleName();

    ec(Context context, FlurryAdModule flurryAdModule, m mVar, AdCreative adCreative, Bundle bundle) {
        super(context, flurryAdModule, mVar, adCreative);
        setFocusable(true);
    }

    private MobclixAdView a(Context context, int i, int i2) {
        if (i2 >= 320 && i >= 50) {
            ja.a(3, a, "Determined Mobclix AdSize as BANNER");
            return new MobclixMMABannerXLAdView((Activity) context);
        } else if (i2 < 300 || i < 250) {
            ja.a(3, a, "Could not find Mobclix AdSize that matches size");
            return null;
        } else {
            ja.a(3, a, "Determined Mobclix AdSize as IAB_RECT");
            return new MobclixIABRectangleMAdView((Activity) context);
        }
    }

    public void initLayout() {
        int i;
        int i2;
        MobclixAdView a2 = a(getContext(), getAdCreative().getHeight(), getAdCreative().getWidth());
        a2.addMobclixAdViewListener(new ed(this));
        if (a2 instanceof MobclixMMABannerXLAdView) {
            i = 50;
            i2 = 300;
        } else {
            i = 50;
            i2 = 300;
        }
        if (a2 instanceof MobclixIABRectangleMAdView) {
            i2 = 320;
            i = 250;
        }
        float f = getResources().getDisplayMetrics().density;
        a2.setLayoutParams(new LinearLayout.LayoutParams((int) ((((float) i2) * f) + 0.5f), (int) ((((float) i) * f) + 0.5f)));
        setGravity(17);
        addView(a2);
        a2.setRefreshTime(-1);
    }
}
