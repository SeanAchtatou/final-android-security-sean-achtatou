package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public abstract class qu<T> {
    public abstract T a(ow owVar, qm qmVar) throws IOException, oz;

    public T a(ow owVar, qm qmVar, T t) throws IOException, oz {
        throw new UnsupportedOperationException();
    }

    public Object a(ow owVar, qm qmVar, rw rwVar) throws IOException, oz {
        return rwVar.d(owVar, qmVar);
    }

    public qu<T> a() {
        return this;
    }

    public T b() {
        return null;
    }

    public T c() {
        return b();
    }
}
