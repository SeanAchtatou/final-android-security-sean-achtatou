package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import android.view.ViewGroup;
import com.flurry.android.impl.ads.FlurryAdModule;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public final class aq {
    private static final String a = aq.class.getSimpleName();
    private Map<String, cl> b = new HashMap();
    private Map<String, Stack<WeakReference<an>>> c = new HashMap();
    private final ScheduledExecutorService d = Executors.newSingleThreadScheduledExecutor();

    public synchronized List<an> a(Context context) {
        ArrayList arrayList;
        arrayList = new ArrayList();
        for (Stack<WeakReference<an>> it : this.c.values()) {
            Iterator it2 = it.iterator();
            while (it2.hasNext()) {
                an anVar = (an) ((WeakReference) it2.next()).get();
                if (anVar != null && (context == null || context == anVar.getContext())) {
                    arrayList.add(anVar);
                }
            }
        }
        return arrayList;
    }

    public synchronized void a(an anVar) {
        if (anVar != null) {
            String adSpace = anVar.getAdSpace();
            Stack stack = this.c.get(adSpace);
            if (stack != null) {
                Iterator it = stack.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    WeakReference weakReference = (WeakReference) it.next();
                    if (anVar == weakReference.get()) {
                        if (stack.remove(weakReference)) {
                            ja.a(3, a, "removed banner holder for adSpaceName: " + anVar.getAdSpace());
                        }
                    }
                }
                if (stack.size() == 0) {
                    this.c.remove(adSpace);
                }
            }
        }
    }

    public synchronized an a(String str) {
        an anVar;
        if (this.c.containsKey(str)) {
            Stack stack = this.c.get(str);
            anVar = stack.size() > 0 ? (an) ((WeakReference) stack.peek()).get() : null;
        } else {
            anVar = null;
        }
        return anVar;
    }

    private void a(String str, an anVar) {
        Stack stack = this.c.get(str);
        if (stack == null) {
            stack = new Stack();
            this.c.put(str, stack);
        }
        stack.push(new WeakReference(anVar));
    }

    public synchronized an a(FlurryAdModule flurryAdModule, Context context, ViewGroup viewGroup, String str) {
        an a2;
        a2 = a(str);
        if (a2 == null || !a2.getContext().equals(context)) {
            a2 = new an(flurryAdModule, context, str, viewGroup, this.d);
            a(str, a2);
        }
        return a2;
    }

    public synchronized void a(String str, cl clVar) {
        this.b.put(str, clVar);
    }

    public synchronized cl b(String str) {
        return this.b.remove(str);
    }

    public synchronized void c(String str) {
        this.b.remove(str);
    }

    public synchronized boolean d(String str) {
        return this.b.containsKey(str);
    }
}
