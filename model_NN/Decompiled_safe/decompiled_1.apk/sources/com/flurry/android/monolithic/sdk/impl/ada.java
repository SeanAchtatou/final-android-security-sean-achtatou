package com.flurry.android.monolithic.sdk.impl;

import java.lang.reflect.Array;
import java.lang.reflect.Type;

public final class ada extends adi {
    protected final afm a;
    protected final Object b;

    private ada(afm afm, Object obj, Object obj2, Object obj3) {
        super(obj.getClass(), afm.hashCode(), obj2, obj3);
        this.a = afm;
        this.b = obj;
    }

    public static ada a(afm afm, Object obj, Object obj2) {
        return new ada(afm, Array.newInstance(afm.p(), 0), null, null);
    }

    /* renamed from: a */
    public ada f(Object obj) {
        return obj == this.g ? this : new ada(this.a, this.b, this.f, obj);
    }

    /* renamed from: b */
    public ada e(Object obj) {
        return obj == this.a.o() ? this : new ada(this.a.f(obj), this.b, this.f, this.g);
    }

    /* renamed from: c */
    public ada d(Object obj) {
        return obj == this.f ? this : new ada(this.a, this.b, obj, this.g);
    }

    /* access modifiers changed from: protected */
    public String a() {
        return this.d.getName();
    }

    /* access modifiers changed from: protected */
    public afm a(Class<?> cls) {
        if (!cls.isArray()) {
            throw new IllegalArgumentException("Incompatible narrowing operation: trying to narrow " + toString() + " to class " + cls.getName());
        }
        return a(adk.a().a((Type) cls.getComponentType()), this.f, this.g);
    }

    public afm b(Class<?> cls) {
        return cls == this.a.p() ? this : a(this.a.f(cls), this.f, this.g);
    }

    public afm c(Class<?> cls) {
        return cls == this.a.p() ? this : a(this.a.h(cls), this.f, this.g);
    }

    public boolean b() {
        return true;
    }

    public boolean c() {
        return false;
    }

    public boolean d() {
        return true;
    }

    public boolean e() {
        return this.a.e();
    }

    public String a(int i) {
        if (i == 0) {
            return "E";
        }
        return null;
    }

    public boolean f() {
        return true;
    }

    public afm g() {
        return this.a;
    }

    public int h() {
        return 1;
    }

    public afm b(int i) {
        if (i == 0) {
            return this.a;
        }
        return null;
    }

    public String toString() {
        return "[array type, component type: " + this.a + "]";
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        return this.a.equals(((ada) obj).a);
    }
}
