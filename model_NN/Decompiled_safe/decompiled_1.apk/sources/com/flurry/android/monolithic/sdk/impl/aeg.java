package com.flurry.android.monolithic.sdk.impl;

public final class aeg<T> {
    final T a;
    final aeg<T> b;

    public aeg<T> a() {
        return this.b;
    }

    public T b() {
        return this.a;
    }
}
