package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import android.view.ViewGroup;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;

public final class cm extends cl {
    private final cn a;

    public cm(cn cnVar, AdUnit adUnit) {
        super(adUnit);
        this.a = cnVar;
    }

    public void a(Context context, ViewGroup viewGroup) {
        if (this.a != null) {
            this.a.a();
        }
    }
}
