package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

class ju extends kb {
    private final int i;

    public ju(ka kaVar, String str, int i2) {
        super(kj.FIXED, kaVar, str);
        if (i2 < 0) {
            throw new IllegalArgumentException("Invalid fixed size: " + i2);
        }
        this.i = i2;
    }

    public int l() {
        return this.i;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ju)) {
            return false;
        }
        ju juVar = (ju) obj;
        return c(juVar) && a(juVar) && this.i == juVar.i && this.c.equals(juVar.c);
    }

    /* access modifiers changed from: package-private */
    public int m() {
        return super.m() + this.i;
    }

    /* access modifiers changed from: package-private */
    public void a(kc kcVar, or orVar) throws IOException {
        if (!c(kcVar, orVar)) {
            orVar.d();
            orVar.a("type", "fixed");
            d(kcVar, orVar);
            if (e() != null) {
                orVar.a("doc", e());
            }
            orVar.a("size", this.i);
            this.c.a(orVar);
            a(orVar);
            orVar.e();
        }
    }
}
