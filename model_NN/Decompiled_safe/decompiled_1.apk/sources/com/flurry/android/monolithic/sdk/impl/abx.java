package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
public final class abx extends abw<boolean[]> {
    public abx() {
        super(boolean[].class, null, null);
    }

    public abc<?> a(rx rxVar) {
        return this;
    }

    /* renamed from: a */
    public void b(boolean[] zArr, or orVar, ru ruVar) throws IOException, oq {
        for (boolean a : zArr) {
            orVar.a(a);
        }
    }
}
