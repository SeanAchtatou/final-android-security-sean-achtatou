package com.flurry.android.monolithic.sdk.impl;

final class fv implements hz {
    final /* synthetic */ hz a;
    final /* synthetic */ String b;
    final /* synthetic */ String c;
    final /* synthetic */ String d;

    fv(hz hzVar, String str, String str2, String str3) {
        this.a = hzVar;
        this.b = str;
        this.c = str2;
        this.d = str3;
    }

    public void a(ft ftVar) {
        this.a.a(ftVar);
    }

    public void a(hy hyVar) {
        ft ftVar = new ft();
        if (hyVar.a() != 404 || !hyVar.b().equals("not found")) {
            this.a.a(hyVar);
            return;
        }
        ftVar.e(this.b);
        ftVar.d(this.c);
        ftVar.f(this.d);
        try {
            ftVar.a(new fw(this));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
