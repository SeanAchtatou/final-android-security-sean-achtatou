package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public final class afa extends afc {
    final long c;

    public afa(long j) {
        this.c = j;
    }

    public static afa a(long j) {
        return new afa(j);
    }

    public int j() {
        return (int) this.c;
    }

    public long k() {
        return this.c;
    }

    public double l() {
        return (double) this.c;
    }

    public String m() {
        return pu.a(this.c);
    }

    public final void a(or orVar, ru ruVar) throws IOException, oz {
        orVar.a(this.c);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        return ((afa) obj).c == this.c;
    }

    public int hashCode() {
        return ((int) this.c) ^ ((int) (this.c >> 32));
    }
}
