package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public final class aex extends afc {
    protected final double c;

    public aex(double d) {
        this.c = d;
    }

    public static aex b(double d) {
        return new aex(d);
    }

    public int j() {
        return (int) this.c;
    }

    public long k() {
        return (long) this.c;
    }

    public double l() {
        return this.c;
    }

    public String m() {
        return pu.a(this.c);
    }

    public final void a(or orVar, ru ruVar) throws IOException, oz {
        orVar.a(this.c);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        return ((aex) obj).c == this.c;
    }

    public int hashCode() {
        long doubleToLongBits = Double.doubleToLongBits(this.c);
        return ((int) (doubleToLongBits >> 32)) ^ ((int) doubleToLongBits);
    }
}
