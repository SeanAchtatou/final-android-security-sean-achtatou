package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.Date;

public class acq extends abt<Object> {
    static final acq a = new acq();

    public acq() {
        super(Object.class);
    }

    public void a(Object obj, or orVar, ru ruVar) throws IOException, oq {
        if (obj instanceof Date) {
            ruVar.b((Date) obj, orVar);
        } else {
            orVar.a(obj.toString());
        }
    }
}
