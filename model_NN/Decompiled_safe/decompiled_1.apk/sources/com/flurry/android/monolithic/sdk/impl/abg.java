package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.EnumSet;
import java.util.Iterator;

public class abg extends aay<EnumSet<? extends Enum<?>>> {
    public /* synthetic */ void b(Object obj, or orVar, ru ruVar) throws IOException, oq {
        a((EnumSet<? extends Enum<?>>) ((EnumSet) obj), orVar, ruVar);
    }

    public abg(afm afm, qc qcVar) {
        super(EnumSet.class, afm, true, null, qcVar, null);
    }

    public abc<?> a(rx rxVar) {
        return this;
    }

    public void a(EnumSet<? extends Enum<?>> enumSet, or orVar, ru ruVar) throws IOException, oq {
        ra<Object> raVar = this.d;
        Iterator it = enumSet.iterator();
        ra<Object> raVar2 = raVar;
        while (it.hasNext()) {
            Enum enumR = (Enum) it.next();
            if (raVar2 == null) {
                raVar2 = ruVar.a(enumR.getDeclaringClass(), this.e);
            }
            raVar2.a(enumR, orVar, ruVar);
        }
    }
}
