package com.flurry.android.monolithic.sdk.impl;

import com.ideaworks3d.marmalade.s3eAndroidGooglePlayBilling.util.IabHelper;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;

public final class agb extends pi {
    protected pc d;
    protected agc e;
    protected int f;
    protected pk g;
    protected boolean h;
    protected transient afq i;
    protected ot j = null;

    public agb(agc agc, pc pcVar) {
        super(0);
        this.e = agc;
        this.f = -1;
        this.d = pcVar;
        this.g = pk.a(-1, -1);
    }

    public void a(ot otVar) {
        this.j = otVar;
    }

    public pc a() {
        return this.d;
    }

    public void close() throws IOException {
        if (!this.h) {
            this.h = true;
        }
    }

    public pb b() throws IOException, ov {
        if (this.h || this.e == null) {
            return null;
        }
        int i2 = this.f + 1;
        this.f = i2;
        if (i2 >= 16) {
            this.f = 0;
            this.e = this.e.a();
            if (this.e == null) {
                return null;
            }
        }
        this.b = this.e.a(this.f);
        if (this.b == pb.FIELD_NAME) {
            Object A = A();
            this.g.a(A instanceof String ? (String) A : A.toString());
        } else if (this.b == pb.START_OBJECT) {
            this.g = this.g.c(-1, -1);
        } else if (this.b == pb.START_ARRAY) {
            this.g = this.g.b(-1, -1);
        } else if (this.b == pb.END_OBJECT || this.b == pb.END_ARRAY) {
            this.g = this.g.i();
            if (this.g == null) {
                this.g = pk.a(-1, -1);
            }
        }
        return this.b;
    }

    public ot h() {
        return i();
    }

    public ot i() {
        return this.j == null ? ot.a : this.j;
    }

    public String g() {
        return this.g.h();
    }

    public String k() {
        if (this.b == pb.VALUE_STRING || this.b == pb.FIELD_NAME) {
            Object A = A();
            if (A instanceof String) {
                return (String) A;
            }
            if (A == null) {
                return null;
            }
            return A.toString();
        } else if (this.b == null) {
            return null;
        } else {
            switch (aga.a[this.b.ordinal()]) {
                case IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED /*7*/:
                case IabHelper.BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED /*8*/:
                    Object A2 = A();
                    if (A2 == null) {
                        return null;
                    }
                    return A2.toString();
                default:
                    return this.b.a();
            }
        }
    }

    public char[] l() {
        String k = k();
        if (k == null) {
            return null;
        }
        return k.toCharArray();
    }

    public int m() {
        String k = k();
        if (k == null) {
            return 0;
        }
        return k.length();
    }

    public int n() {
        return 0;
    }

    public boolean o() {
        return false;
    }

    public BigInteger v() throws IOException, ov {
        Number p = p();
        if (p instanceof BigInteger) {
            return (BigInteger) p;
        }
        switch (q()) {
            case BIG_DECIMAL:
                return ((BigDecimal) p).toBigInteger();
            default:
                return BigInteger.valueOf(p.longValue());
        }
    }

    public BigDecimal y() throws IOException, ov {
        Number p = p();
        if (p instanceof BigDecimal) {
            return (BigDecimal) p;
        }
        switch (q()) {
            case INT:
            case LONG:
                return BigDecimal.valueOf(p.longValue());
            case BIG_INTEGER:
                return new BigDecimal((BigInteger) p);
            case BIG_DECIMAL:
            case FLOAT:
            default:
                return BigDecimal.valueOf(p.doubleValue());
        }
    }

    public double x() throws IOException, ov {
        return p().doubleValue();
    }

    public float w() throws IOException, ov {
        return p().floatValue();
    }

    public int t() throws IOException, ov {
        if (this.b == pb.VALUE_NUMBER_INT) {
            return ((Number) A()).intValue();
        }
        return p().intValue();
    }

    public long u() throws IOException, ov {
        return p().longValue();
    }

    public oy q() throws IOException, ov {
        Number p = p();
        if (p instanceof Integer) {
            return oy.INT;
        }
        if (p instanceof Long) {
            return oy.LONG;
        }
        if (p instanceof Double) {
            return oy.DOUBLE;
        }
        if (p instanceof BigDecimal) {
            return oy.BIG_DECIMAL;
        }
        if (p instanceof Float) {
            return oy.FLOAT;
        }
        if (p instanceof BigInteger) {
            return oy.BIG_INTEGER;
        }
        return null;
    }

    public final Number p() throws IOException, ov {
        B();
        return (Number) A();
    }

    public Object z() {
        if (this.b == pb.VALUE_EMBEDDED_OBJECT) {
            return A();
        }
        return null;
    }

    public byte[] a(on onVar) throws IOException, ov {
        if (this.b == pb.VALUE_EMBEDDED_OBJECT) {
            Object A = A();
            if (A instanceof byte[]) {
                return (byte[]) A;
            }
        }
        if (this.b != pb.VALUE_STRING) {
            throw a("Current token (" + this.b + ") not VALUE_STRING (or VALUE_EMBEDDED_OBJECT with byte[]), can not access as binary");
        }
        String k = k();
        if (k == null) {
            return null;
        }
        afq afq = this.i;
        if (afq == null) {
            afq = new afq(100);
            this.i = afq;
        } else {
            this.i.a();
        }
        a(k, afq, onVar);
        return afq.b();
    }

    /* access modifiers changed from: protected */
    public final Object A() {
        return this.e.b(this.f);
    }

    /* access modifiers changed from: protected */
    public final void B() throws ov {
        if (this.b == null || !this.b.c()) {
            throw a("Current token (" + this.b + ") not numeric, can not use numeric value accessors");
        }
    }

    /* access modifiers changed from: protected */
    public void H() throws ov {
        U();
    }
}
