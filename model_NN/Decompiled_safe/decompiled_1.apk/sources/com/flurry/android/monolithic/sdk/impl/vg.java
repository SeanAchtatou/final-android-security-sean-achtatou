package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
final class vg extends ve<byte[]> {
    public vg() {
        super(byte[].class);
    }

    /* renamed from: b */
    public byte[] a(ow owVar, qm qmVar) throws IOException, oz {
        byte b;
        pb e = owVar.e();
        if (e == pb.VALUE_STRING) {
            return owVar.a(qmVar.c());
        }
        if (e == pb.VALUE_EMBEDDED_OBJECT) {
            Object z = owVar.z();
            if (z == null) {
                return null;
            }
            if (z instanceof byte[]) {
                return (byte[]) z;
            }
        }
        if (!owVar.j()) {
            return c(owVar, qmVar);
        }
        ads b2 = qmVar.h().b();
        byte[] bArr = (byte[]) b2.a();
        int i = 0;
        while (true) {
            pb b3 = owVar.b();
            if (b3 == pb.END_ARRAY) {
                return (byte[]) b2.b(bArr, i);
            }
            if (b3 == pb.VALUE_NUMBER_INT || b3 == pb.VALUE_NUMBER_FLOAT) {
                b = owVar.r();
            } else if (b3 != pb.VALUE_NULL) {
                throw qmVar.b(this.q.getComponentType());
            } else {
                b = 0;
            }
            if (i >= bArr.length) {
                bArr = (byte[]) b2.a(bArr, i);
                i = 0;
            }
            bArr[i] = b;
            i++;
        }
    }

    private final byte[] c(ow owVar, qm qmVar) throws IOException, oz {
        byte b;
        if (owVar.e() == pb.VALUE_STRING && qmVar.a(ql.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT) && owVar.k().length() == 0) {
            return null;
        }
        if (!qmVar.a(ql.ACCEPT_SINGLE_VALUE_AS_ARRAY)) {
            throw qmVar.b(this.q);
        }
        pb e = owVar.e();
        if (e == pb.VALUE_NUMBER_INT || e == pb.VALUE_NUMBER_FLOAT) {
            b = owVar.r();
        } else if (e != pb.VALUE_NULL) {
            throw qmVar.b(this.q.getComponentType());
        } else {
            b = 0;
        }
        return new byte[]{b};
    }
}
