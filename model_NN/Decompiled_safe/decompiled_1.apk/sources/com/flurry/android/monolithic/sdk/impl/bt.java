package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import android.os.SystemClock;
import com.flurry.android.impl.ads.FlurryAdModule;
import java.lang.Thread;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

public class bt implements ih, Thread.UncaughtExceptionHandler {
    /* access modifiers changed from: private */
    public static final String b = bt.class.getSimpleName();
    private static bt c;
    Map<Context, Context> a = new WeakHashMap();
    private long d;
    private long e;
    private long f;
    /* access modifiers changed from: private */
    public Map<ie, ByteBuffer> g = new HashMap();
    private volatile boolean h = false;
    /* access modifiers changed from: private */
    public volatile boolean i = false;
    /* access modifiers changed from: private */
    public FlurryAdModule j;

    public bt() {
        is.a().a(this);
        this.j = FlurryAdModule.getInstance();
    }

    public static bt a() {
        if (c == null) {
            c = new bt();
        }
        return c;
    }

    public void f(Context context) {
        jc.a().b();
        in.a().b();
        in.a().c();
        if (this.a.put(context, context) != null) {
            ja.a(5, b, "onStartSession called with duplicate context, use a specific Activity or Service as context instead of using a global context");
        }
        if (!this.h) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            if (elapsedRealtime - this.f > ((Long) ib.a().a("ContinueSessionMillis")).longValue()) {
                this.e = elapsedRealtime;
                this.d = System.currentTimeMillis();
                this.j.a(context, this.d, this.e);
                this.j.a(new bu(this));
            } else {
                this.j.b(context);
            }
            this.h = true;
        }
    }

    public void g(Context context) {
        in.a().d();
        if (context != null && this.a.remove(context) == null) {
            ja.a(5, b, "onEndSession called without context from corresponding onStartSession");
        }
        if (this.h) {
            this.j.d(context);
        }
        if (this.h && this.a.isEmpty()) {
            this.f = SystemClock.elapsedRealtime();
            this.j.c(context);
            this.h = false;
        }
    }

    public void uncaughtException(Thread thread, Throwable th) {
        c();
    }

    private void c() {
        this.a.clear();
    }
}
