package com.flurry.android.monolithic.sdk.impl;

import java.util.List;
import java.util.Map;
import v2.com.playhaven.model.PHContent;

public abstract class mq {
    public static final mq c = new nh(PHContent.PARCEL_NULL);
    public static final mq d = new nh("boolean");
    public static final mq e = new nh("int");
    public static final mq f = new nh("long");
    public static final mq g = new nh("float");
    public static final mq h = new nh("double");
    public static final mq i = new nh("string");
    public static final mq j = new nh("bytes");
    public static final mq k = new nh("fixed");
    public static final mq l = new nh("enum");
    public static final mq m = new nh("union");
    public static final mq n = new nh("array-start");
    public static final mq o = new nh("array-end");
    public static final mq p = new nh("map-start");
    public static final mq q = new nh("map-end");
    public static final mq r = new nh("item-end");
    public static final mq s = new nh("field-action");
    public static final mq t = new my(false);
    public static final mq u = new my(true);
    public static final mq v = new my(true);
    public static final mq w = new my(true);
    public static final mq x = new my(true);
    public static final mq y = new nh("map-key-marker");
    public final na a;
    public final mq[] b;

    protected mq(na naVar) {
        this(naVar, null);
    }

    protected mq(na naVar, mq[] mqVarArr) {
        this.b = mqVarArr;
        this.a = naVar;
    }

    static mq a(mq... mqVarArr) {
        return new nd(mqVarArr);
    }

    static mq b(mq... mqVarArr) {
        return new ne(mqVarArr);
    }

    static mq a(mq mqVar, mq... mqVarArr) {
        return new nb(mqVar, mqVarArr);
    }

    static mq a(mq[] mqVarArr, String[] strArr) {
        return new ms(mqVarArr, strArr);
    }

    static mq a(String str) {
        return new mv(str);
    }

    static mq a(mq mqVar, mq mqVar2) {
        return new nc(mqVar, mqVar2);
    }

    public mq a(Map<ne, ne> map, Map<ne, List<mx>> map2) {
        return this;
    }

    public int a() {
        return 1;
    }

    static void a(mq[] mqVarArr, int i2, mq[] mqVarArr2, int i3, Map<ne, ne> map, Map<ne, List<mx>> map2) {
        int i4;
        int i5 = i3;
        int i6 = i2;
        while (i6 < mqVarArr.length) {
            mq a2 = mqVarArr[i6].a(map, map2);
            if (a2 instanceof ne) {
                mq[] mqVarArr3 = a2.b;
                List list = map2.get(a2);
                if (list == null) {
                    System.arraycopy(mqVarArr3, 0, mqVarArr2, i5, mqVarArr3.length);
                } else {
                    list.add(new mx(mqVarArr2, i5));
                }
                i4 = mqVarArr3.length + i5;
            } else {
                mqVarArr2[i5] = a2;
                i4 = i5 + 1;
            }
            i6++;
            i5 = i4;
        }
    }

    protected static int a(mq[] mqVarArr, int i2) {
        int i3 = 0;
        int i4 = i2;
        while (true) {
            int i5 = i3;
            if (i4 >= mqVarArr.length) {
                return i5;
            }
            if (mqVarArr[i4] instanceof ne) {
                i3 = ((ne) mqVarArr[i4]).a() + i5;
            } else {
                i3 = i5 + 1;
            }
            i4++;
        }
    }
}
