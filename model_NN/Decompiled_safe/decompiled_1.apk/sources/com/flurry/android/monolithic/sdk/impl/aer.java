package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.Arrays;

public final class aer extends afg {
    static final aer c = new aer(new byte[0]);
    final byte[] d;

    public aer(byte[] bArr) {
        this.d = bArr;
    }

    public static aer a(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        if (bArr.length == 0) {
            return c;
        }
        return new aer(bArr);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.on.a(byte[], boolean):java.lang.String
     arg types: [byte[], int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.on.a(java.lang.StringBuilder, int):void
      com.flurry.android.monolithic.sdk.impl.on.a(byte[], boolean):java.lang.String */
    public String m() {
        return oo.a().a(this.d, false);
    }

    public final void a(or orVar, ru ruVar) throws IOException, oz {
        orVar.a(this.d);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        return Arrays.equals(((aer) obj).d, this.d);
    }

    public int hashCode() {
        if (this.d == null) {
            return -1;
        }
        return this.d.length;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.on.a(byte[], boolean):java.lang.String
     arg types: [byte[], int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.on.a(java.lang.StringBuilder, int):void
      com.flurry.android.monolithic.sdk.impl.on.a(byte[], boolean):java.lang.String */
    public String toString() {
        return oo.a().a(this.d, true);
    }
}
