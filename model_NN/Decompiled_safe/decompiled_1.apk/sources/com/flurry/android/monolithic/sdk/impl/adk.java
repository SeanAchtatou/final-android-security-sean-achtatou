package com.flurry.android.monolithic.sdk.impl;

import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class adk {
    @Deprecated
    public static final adk a = new adk();
    private static final afm[] f = new afm[0];
    protected final adl[] b = null;
    protected final adm c = new adm(this);
    protected ade d;
    protected ade e;

    private adk() {
    }

    public static adk a() {
        return a;
    }

    public static afm b() {
        return a().c();
    }

    public static afm a(String str) throws IllegalArgumentException {
        return a.b(str);
    }

    public afm a(afm afm, Class<?> cls) {
        if (!(afm instanceof adh) || (!cls.isArray() && !Map.class.isAssignableFrom(cls) && !Collection.class.isAssignableFrom(cls))) {
            return afm.f(cls);
        }
        if (!afm.p().isAssignableFrom(cls)) {
            throw new IllegalArgumentException("Class " + cls.getClass().getName() + " not subtype of " + afm);
        }
        afm a2 = a(cls, new adj(this, afm.p()));
        Object n = afm.n();
        if (n != null) {
            a2 = a2.d(n);
        }
        Object o = afm.o();
        if (o != null) {
            return a2.f(o);
        }
        return a2;
    }

    public afm b(String str) throws IllegalArgumentException {
        return this.c.a(str);
    }

    public afm[] b(afm afm, Class<?> cls) {
        Class<?> p = afm.p();
        if (p != cls) {
            return a(p, cls, new adj(this, afm));
        }
        int h = afm.h();
        if (h == 0) {
            return null;
        }
        afm[] afmArr = new afm[h];
        for (int i = 0; i < h; i++) {
            afmArr[i] = afm.b(i);
        }
        return afmArr;
    }

    public afm[] a(Class<?> cls, Class<?> cls2) {
        return a(cls, cls2, new adj(this, cls));
    }

    public afm[] a(Class<?> cls, Class<?> cls2, adj adj) {
        ade c2 = c(cls, cls2);
        if (c2 == null) {
            throw new IllegalArgumentException("Class " + cls.getName() + " is not a subtype of " + cls2.getName());
        }
        adj adj2 = adj;
        while (c2.b() != null) {
            c2 = c2.b();
            Class<?> e2 = c2.e();
            adj adj3 = new adj(this, e2);
            if (c2.c()) {
                Type[] actualTypeArguments = c2.d().getActualTypeArguments();
                TypeVariable[] typeParameters = e2.getTypeParameters();
                int length = actualTypeArguments.length;
                for (int i = 0; i < length; i++) {
                    adj3.a(typeParameters[i].getName(), a.b(actualTypeArguments[i], adj2));
                }
            }
            adj2 = adj3;
        }
        if (!c2.c()) {
            return null;
        }
        return adj2.b();
    }

    public afm a(Type type) {
        return b(type, (adj) null);
    }

    public afm a(Type type, adj adj) {
        return b(type, adj);
    }

    public afm b(Type type, adj adj) {
        afm a2;
        adj adj2;
        if (type instanceof Class) {
            Class cls = (Class) type;
            if (adj == null) {
                adj2 = new adj(this, cls);
            } else {
                adj2 = adj;
            }
            a2 = a((Class<?>) cls, adj2);
        } else if (type instanceof ParameterizedType) {
            a2 = a((ParameterizedType) type, adj);
            adj2 = adj;
        } else if (type instanceof GenericArrayType) {
            a2 = a((GenericArrayType) type, adj);
            adj2 = adj;
        } else if (type instanceof TypeVariable) {
            a2 = a((TypeVariable<?>) ((TypeVariable) type), adj);
            adj2 = adj;
        } else if (type instanceof WildcardType) {
            a2 = a((WildcardType) type, adj);
            adj2 = adj;
        } else {
            throw new IllegalArgumentException("Unrecognized Type: " + type.toString());
        }
        if (this.b == null || a2.f()) {
            return a2;
        }
        afm afm = a2;
        for (adl a3 : this.b) {
            afm = a3.a(afm, type, adj2, this);
        }
        return afm;
    }

    public add b(Class<? extends Collection> cls, Class<?> cls2) {
        return add.a(cls, a((Type) cls2));
    }

    public adg a(Class<? extends Map> cls, Class<?> cls2, Class<?> cls3) {
        return adg.a(cls, a((Type) cls2), a((Type) cls3));
    }

    public afm a(Class<?> cls, afm[] afmArr) {
        TypeVariable[] typeParameters = cls.getTypeParameters();
        if (typeParameters.length != afmArr.length) {
            throw new IllegalArgumentException("Parameter type mismatch for " + cls.getName() + ": expected " + typeParameters.length + " parameters, was given " + afmArr.length);
        }
        String[] strArr = new String[typeParameters.length];
        int length = typeParameters.length;
        for (int i = 0; i < length; i++) {
            strArr[i] = typeParameters[i].getName();
        }
        return new adh(cls, strArr, afmArr, null, null);
    }

    public afm a(Class<?> cls) {
        return new adh(cls);
    }

    /* access modifiers changed from: protected */
    public afm a(Class<?> cls, adj adj) {
        if (cls.isArray()) {
            return ada.a(b(cls.getComponentType(), (adj) null), (Object) null, (Object) null);
        }
        if (cls.isEnum()) {
            return new adh(cls);
        }
        if (Map.class.isAssignableFrom(cls)) {
            return b(cls);
        }
        if (Collection.class.isAssignableFrom(cls)) {
            return c(cls);
        }
        return new adh(cls);
    }

    /* access modifiers changed from: protected */
    public afm a(Class<?> cls, List<afm> list) {
        if (cls.isArray()) {
            return ada.a(b(cls.getComponentType(), (adj) null), (Object) null, (Object) null);
        }
        if (cls.isEnum()) {
            return new adh(cls);
        }
        if (Map.class.isAssignableFrom(cls)) {
            if (list.size() <= 0) {
                return b(cls);
            }
            return adg.a(cls, list.get(0), list.size() >= 2 ? list.get(1) : c());
        } else if (Collection.class.isAssignableFrom(cls)) {
            if (list.size() >= 1) {
                return add.a(cls, list.get(0));
            }
            return c(cls);
        } else if (list.size() == 0) {
            return new adh(cls);
        } else {
            return a(cls, (afm[]) list.toArray(new afm[list.size()]));
        }
    }

    /* access modifiers changed from: protected */
    public afm a(ParameterizedType parameterizedType, adj adj) {
        afm[] afmArr;
        Class<Collection> cls = Collection.class;
        Class cls2 = (Class) parameterizedType.getRawType();
        Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
        int length = actualTypeArguments == null ? 0 : actualTypeArguments.length;
        if (length == 0) {
            afmArr = f;
        } else {
            afm[] afmArr2 = new afm[length];
            for (int i = 0; i < length; i++) {
                afmArr2[i] = b(actualTypeArguments[i], adj);
            }
            afmArr = afmArr2;
        }
        if (Map.class.isAssignableFrom(cls2)) {
            afm[] b2 = b(a(cls2, afmArr), Map.class);
            if (b2.length == 2) {
                return adg.a(cls2, b2[0], b2[1]);
            }
            throw new IllegalArgumentException("Could not find 2 type parameters for Map class " + cls2.getName() + " (found " + b2.length + ")");
        }
        Class<Collection> cls3 = Collection.class;
        if (cls.isAssignableFrom(cls2)) {
            Class<Collection> cls4 = Collection.class;
            afm[] b3 = b(a(cls2, afmArr), cls);
            if (b3.length == 1) {
                return add.a(cls2, b3[0]);
            }
            throw new IllegalArgumentException("Could not find 1 type parameter for Collection class " + cls2.getName() + " (found " + b3.length + ")");
        } else if (length == 0) {
            return new adh(cls2);
        } else {
            return a(cls2, afmArr);
        }
    }

    /* access modifiers changed from: protected */
    public afm a(GenericArrayType genericArrayType, adj adj) {
        return ada.a(b(genericArrayType.getGenericComponentType(), adj), (Object) null, (Object) null);
    }

    /* access modifiers changed from: protected */
    public afm a(TypeVariable<?> typeVariable, adj adj) {
        if (adj == null) {
            return c();
        }
        String name = typeVariable.getName();
        afm a2 = adj.a(name);
        if (a2 != null) {
            return a2;
        }
        Type[] bounds = typeVariable.getBounds();
        adj.b(name);
        return b(bounds[0], adj);
    }

    /* access modifiers changed from: protected */
    public afm a(WildcardType wildcardType, adj adj) {
        return b(wildcardType.getUpperBounds()[0], adj);
    }

    private afm b(Class<?> cls) {
        afm[] a2 = a(cls, (Class<?>) Map.class);
        if (a2 == null) {
            return adg.a(cls, c(), c());
        }
        if (a2.length == 2) {
            return adg.a(cls, a2[0], a2[1]);
        }
        throw new IllegalArgumentException("Strange Map type " + cls.getName() + ": can not determine type parameters");
    }

    private afm c(Class<?> cls) {
        afm[] a2 = a(cls, (Class<?>) Collection.class);
        if (a2 == null) {
            return add.a(cls, c());
        }
        if (a2.length == 1) {
            return add.a(cls, a2[0]);
        }
        throw new IllegalArgumentException("Strange Collection type " + cls.getName() + ": can not determine type parameters");
    }

    /* access modifiers changed from: protected */
    public afm c() {
        return new adh(Object.class);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.adk.a(java.lang.reflect.Type, java.lang.Class<?>):com.flurry.android.monolithic.sdk.impl.ade
     arg types: [java.lang.Class<?>, java.lang.Class<?>]
     candidates:
      com.flurry.android.monolithic.sdk.impl.adk.a(com.flurry.android.monolithic.sdk.impl.ade, java.lang.Class<?>):com.flurry.android.monolithic.sdk.impl.ade
      com.flurry.android.monolithic.sdk.impl.adk.a(com.flurry.android.monolithic.sdk.impl.afm, java.lang.Class<?>):com.flurry.android.monolithic.sdk.impl.afm
      com.flurry.android.monolithic.sdk.impl.adk.a(java.lang.Class<?>, com.flurry.android.monolithic.sdk.impl.adj):com.flurry.android.monolithic.sdk.impl.afm
      com.flurry.android.monolithic.sdk.impl.adk.a(java.lang.Class<?>, java.util.List<com.flurry.android.monolithic.sdk.impl.afm>):com.flurry.android.monolithic.sdk.impl.afm
      com.flurry.android.monolithic.sdk.impl.adk.a(java.lang.Class<?>, com.flurry.android.monolithic.sdk.impl.afm[]):com.flurry.android.monolithic.sdk.impl.afm
      com.flurry.android.monolithic.sdk.impl.adk.a(java.lang.reflect.GenericArrayType, com.flurry.android.monolithic.sdk.impl.adj):com.flurry.android.monolithic.sdk.impl.afm
      com.flurry.android.monolithic.sdk.impl.adk.a(java.lang.reflect.ParameterizedType, com.flurry.android.monolithic.sdk.impl.adj):com.flurry.android.monolithic.sdk.impl.afm
      com.flurry.android.monolithic.sdk.impl.adk.a(java.lang.reflect.Type, com.flurry.android.monolithic.sdk.impl.adj):com.flurry.android.monolithic.sdk.impl.afm
      com.flurry.android.monolithic.sdk.impl.adk.a(java.lang.reflect.TypeVariable<?>, com.flurry.android.monolithic.sdk.impl.adj):com.flurry.android.monolithic.sdk.impl.afm
      com.flurry.android.monolithic.sdk.impl.adk.a(java.lang.reflect.WildcardType, com.flurry.android.monolithic.sdk.impl.adj):com.flurry.android.monolithic.sdk.impl.afm
      com.flurry.android.monolithic.sdk.impl.adk.a(java.lang.Class<?>, java.lang.Class<?>):com.flurry.android.monolithic.sdk.impl.afm[]
      com.flurry.android.monolithic.sdk.impl.adk.a(java.lang.reflect.Type, java.lang.Class<?>):com.flurry.android.monolithic.sdk.impl.ade */
    /* access modifiers changed from: protected */
    public ade c(Class<?> cls, Class<?> cls2) {
        if (cls2.isInterface()) {
            return b(cls, cls2);
        }
        return a((Type) cls, cls2);
    }

    /* access modifiers changed from: protected */
    public ade a(Type type, Class<?> cls) {
        ade a2;
        ade ade = new ade(type);
        Class<?> e2 = ade.e();
        if (e2 == cls) {
            return ade;
        }
        Type genericSuperclass = e2.getGenericSuperclass();
        if (genericSuperclass == null || (a2 = a(genericSuperclass, cls)) == null) {
            return null;
        }
        a2.b(ade);
        ade.a(a2);
        return ade;
    }

    /* access modifiers changed from: protected */
    public ade b(Type type, Class<?> cls) {
        ade ade = new ade(type);
        Class<?> e2 = ade.e();
        if (e2 == cls) {
            return new ade(type);
        }
        if (e2 == HashMap.class && cls == Map.class) {
            return a(ade);
        }
        if (e2 == ArrayList.class && cls == List.class) {
            return b(ade);
        }
        return a(ade, cls);
    }

    /* access modifiers changed from: protected */
    public ade a(ade ade, Class<?> cls) {
        ade b2;
        Class<?> e2 = ade.e();
        Type[] genericInterfaces = e2.getGenericInterfaces();
        if (genericInterfaces != null) {
            for (Type b3 : genericInterfaces) {
                ade b4 = b(b3, cls);
                if (b4 != null) {
                    b4.b(ade);
                    ade.a(b4);
                    return ade;
                }
            }
        }
        Type genericSuperclass = e2.getGenericSuperclass();
        if (genericSuperclass == null || (b2 = b(genericSuperclass, cls)) == null) {
            return null;
        }
        b2.b(ade);
        ade.a(b2);
        return ade;
    }

    /* access modifiers changed from: protected */
    public synchronized ade a(ade ade) {
        if (this.d == null) {
            ade a2 = ade.a();
            a(a2, Map.class);
            this.d = a2.b();
        }
        ade a3 = this.d.a();
        ade.a(a3);
        a3.b(ade);
        return ade;
    }

    /* access modifiers changed from: protected */
    public synchronized ade b(ade ade) {
        if (this.e == null) {
            ade a2 = ade.a();
            a(a2, List.class);
            this.e = a2.b();
        }
        ade a3 = this.e.a();
        ade.a(a3);
        a3.b(ade);
        return ade;
    }
}
