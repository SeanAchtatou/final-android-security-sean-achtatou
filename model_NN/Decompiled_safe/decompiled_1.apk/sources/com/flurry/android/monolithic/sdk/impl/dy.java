package com.flurry.android.monolithic.sdk.impl;

import com.millennialmedia.android.MMAd;
import com.millennialmedia.android.MMException;
import com.millennialmedia.android.RequestListener;
import java.util.Collections;

class dy implements RequestListener {
    final /* synthetic */ dx a;

    dy(dx dxVar) {
        this.a = dxVar;
    }

    public void requestFailed(MMAd mMAd, MMException mMException) {
        this.a.onRenderFailed(Collections.emptyMap());
        ja.a(3, dx.a, String.format("Millennial MMAdView failed to load ad with error: %d %s.", Integer.valueOf(mMException.getCode()), mMException.getMessage()));
    }

    public void requestCompleted(MMAd mMAd) {
        this.a.onAdShown(Collections.emptyMap());
        ja.a(3, dx.a, "Millennial MMAdView returned ad." + System.currentTimeMillis());
    }

    public void MMAdOverlayLaunched(MMAd mMAd) {
        this.a.onAdClicked(Collections.emptyMap());
        ja.a(3, dx.a, "Millennial MMAdView banner overlay launched.");
    }

    public void MMAdRequestIsCaching(MMAd mMAd) {
        ja.a(3, dx.a, "Millennial MMAdView banner request is caching.");
    }
}
