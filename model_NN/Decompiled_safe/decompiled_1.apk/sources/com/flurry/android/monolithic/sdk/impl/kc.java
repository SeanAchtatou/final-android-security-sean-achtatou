package com.flurry.android.monolithic.sdk.impl;

import java.util.LinkedHashMap;

class kc extends LinkedHashMap<ka, ji> {
    /* access modifiers changed from: private */
    public String a;

    public String a() {
        return this.a;
    }

    public void a(String str) {
        this.a = str;
    }

    /* renamed from: a */
    public ji get(Object obj) {
        ka kaVar;
        if (obj instanceof String) {
            kj kjVar = ji.e.get((String) obj);
            if (kjVar != null) {
                return ji.a(kjVar);
            }
            kaVar = new ka((String) obj, this.a);
        } else {
            kaVar = (ka) obj;
        }
        return (ji) super.get(kaVar);
    }

    public void a(ji jiVar) {
        put(((kb) jiVar).f, jiVar);
    }

    /* renamed from: a */
    public ji put(ka kaVar, ji jiVar) {
        if (!containsKey(kaVar)) {
            return (ji) super.put(kaVar, jiVar);
        }
        throw new kl("Can't redefine: " + kaVar);
    }
}
