package com.flurry.android.monolithic.sdk.impl;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

class xu implements xz {
    private xu() {
    }

    public boolean a(Method method) {
        if (Modifier.isStatic(method.getModifiers())) {
            return false;
        }
        return method.getParameterTypes().length <= 2;
    }
}
