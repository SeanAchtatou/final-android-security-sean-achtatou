package com.flurry.android.monolithic.sdk.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

public class ol extends oi {
    private final File a;
    private final String b;
    private final String c;

    public ol(File file, String str, String str2, String str3) {
        super(str2);
        if (file == null) {
            throw new IllegalArgumentException("File may not be null");
        }
        this.a = file;
        if (str != null) {
            this.b = str;
        } else {
            this.b = file.getName();
        }
        this.c = str3;
    }

    public void a(OutputStream outputStream) throws IOException {
        if (outputStream == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        }
        FileInputStream fileInputStream = new FileInputStream(this.a);
        try {
            byte[] bArr = new byte[4096];
            while (true) {
                int read = fileInputStream.read(bArr);
                if (read != -1) {
                    outputStream.write(bArr, 0, read);
                } else {
                    outputStream.flush();
                    return;
                }
            }
        } finally {
            fileInputStream.close();
        }
    }

    public String d() {
        return "binary";
    }

    public String c() {
        return this.c;
    }

    public long e() {
        return this.a.length();
    }

    public String b() {
        return this.b;
    }
}
