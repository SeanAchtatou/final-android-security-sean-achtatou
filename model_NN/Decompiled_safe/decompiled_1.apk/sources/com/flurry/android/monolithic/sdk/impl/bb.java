package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import com.flurry.android.impl.ads.FlurryAdModule;
import com.flurry.android.impl.ads.avro.protocol.v6.AdFrame;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class bb implements am {
    static final String a = bb.class.getSimpleName();
    private static final Map<String, am> b = a();

    public ac a(Context context, FlurryAdModule flurryAdModule, m mVar, AdUnit adUnit) {
        if (context == null || flurryAdModule == null || mVar == null || adUnit == null) {
            return null;
        }
        String a2 = a(adUnit);
        if (a2 == null) {
            return null;
        }
        am a3 = a(a2);
        if (a3 == null) {
            ja.e(a, "Cannot create ad banner for type: " + a2);
            return null;
        }
        ja.a(3, a, "Creating ad banner for type: " + a2);
        return a3.a(context, flurryAdModule, mVar, adUnit);
    }

    private static Map<String, am> a() {
        HashMap hashMap = new HashMap();
        hashMap.put("FLURRY", new bd());
        hashMap.put("THIRD_PARTY", new dc());
        return Collections.unmodifiableMap(hashMap);
    }

    private static am a(String str) {
        return b.get(str);
    }

    private static String a(AdUnit adUnit) {
        if (adUnit == null) {
            return null;
        }
        List<AdFrame> d = adUnit.d();
        if (d == null || d.isEmpty()) {
            return null;
        }
        AdFrame adFrame = d.get(0);
        if (adFrame == null) {
            return null;
        }
        int intValue = adFrame.b().intValue();
        if (adUnit.e().intValue() == 1 || intValue == 2 || intValue == 1 || intValue == 3) {
            return "FLURRY";
        }
        if (intValue == 4) {
            return "THIRD_PARTY";
        }
        return null;
    }
}
