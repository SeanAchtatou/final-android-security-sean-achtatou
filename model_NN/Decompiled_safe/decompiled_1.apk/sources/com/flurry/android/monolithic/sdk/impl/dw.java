package com.flurry.android.monolithic.sdk.impl;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import com.flurry.android.AdCreative;
import com.flurry.android.impl.ads.FlurryAdModule;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;
import java.util.ArrayList;
import java.util.List;

public final class dw extends cy {
    /* access modifiers changed from: protected */
    public String f() {
        return "Millennial Media";
    }

    /* access modifiers changed from: protected */
    public List<cu> g() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new cu("MMAdView", "5.0.1", "com.millennialmedia.android.MMInterstitial"));
        return arrayList;
    }

    /* access modifiers changed from: protected */
    @TargetApi(13)
    public List<ActivityInfo> j() {
        ArrayList arrayList = new ArrayList();
        ActivityInfo activityInfo = new ActivityInfo();
        activityInfo.name = "com.millennialmedia.android.MMActivity";
        activityInfo.configChanges = 3248;
        arrayList.add(activityInfo);
        ActivityInfo activityInfo2 = new ActivityInfo();
        activityInfo2.name = "com.millennialmedia.android.VideoPlayer";
        activityInfo2.configChanges = 3248;
        arrayList.add(activityInfo2);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public List<String> h() {
        ArrayList arrayList = new ArrayList();
        arrayList.add("com.flurry.millennial.MYAPIDINTERSTITIAL");
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public List<cu> k() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new cu("MMAdView", "5.0.1", "com.millennialmedia.android.MMAdView"));
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public List<String> l() {
        ArrayList arrayList = new ArrayList();
        arrayList.add("com.flurry.millennial.MYAPID");
        arrayList.add("com.flurry.millennial.MYAPIDRECTANGLE");
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public List<String> n() {
        return new ArrayList();
    }

    /* access modifiers changed from: protected */
    @TargetApi(4)
    public List<String> o() {
        ArrayList arrayList = new ArrayList();
        arrayList.add("android.permission.INTERNET");
        arrayList.add("android.permission.WRITE_EXTERNAL_STORAGE");
        arrayList.add("android.permission.ACCESS_NETWORK_STATE");
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public cn a(Context context, FlurryAdModule flurryAdModule, m mVar, AdUnit adUnit, Bundle bundle) {
        if (context == null || flurryAdModule == null || mVar == null || adUnit == null || bundle == null) {
            return null;
        }
        return new dz(context, flurryAdModule, mVar, adUnit, bundle);
    }

    /* access modifiers changed from: protected */
    public ac a(Context context, FlurryAdModule flurryAdModule, m mVar, AdCreative adCreative, Bundle bundle) {
        if (context == null || flurryAdModule == null || mVar == null || adCreative == null || bundle == null) {
            return null;
        }
        return new dx(context, flurryAdModule, mVar, adCreative, bundle);
    }
}
