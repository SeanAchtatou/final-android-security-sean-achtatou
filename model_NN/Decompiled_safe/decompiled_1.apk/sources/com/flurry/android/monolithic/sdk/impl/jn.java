package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

class jn extends ji {
    private final ji f;

    public jn(ji jiVar) {
        super(kj.ARRAY);
        this.f = jiVar;
    }

    public ji i() {
        return this.f;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof jn)) {
            return false;
        }
        jn jnVar = (jn) obj;
        return c(jnVar) && this.f.equals(jnVar.f) && this.c.equals(jnVar.c);
    }

    /* access modifiers changed from: package-private */
    public int m() {
        return super.m() + this.f.m();
    }

    /* access modifiers changed from: package-private */
    public void a(kc kcVar, or orVar) throws IOException {
        orVar.d();
        orVar.a("type", "array");
        orVar.a("items");
        this.f.a(kcVar, orVar);
        this.c.a(orVar);
        orVar.e();
    }
}
