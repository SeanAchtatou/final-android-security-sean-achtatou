package com.flurry.android.monolithic.sdk.impl;

import android.content.DialogInterface;

class v implements DialogInterface.OnDismissListener {
    final /* synthetic */ o a;

    v(o oVar) {
        this.a = oVar;
    }

    public void onDismiss(DialogInterface dialogInterface) {
        ja.a(3, this.a.f, "extendedWebViewDialog.onDismiss()");
        if (this.a.l != null) {
            this.a.l.loadUrl("javascript:if(window.mraid){window.mraid.close();};");
        }
    }
}
