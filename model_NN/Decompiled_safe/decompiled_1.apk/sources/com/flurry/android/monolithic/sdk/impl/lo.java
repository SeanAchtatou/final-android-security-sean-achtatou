package com.flurry.android.monolithic.sdk.impl;

import com.flurry.android.Constants;
import java.io.EOFException;
import java.io.IOException;

class lo extends lp {
    private byte[] b;
    private int c;
    private int d;
    private boolean e;

    private lo(byte[] bArr, int i, int i2) {
        this.e = false;
        if (bArr.length < 16 || i2 < 16) {
            this.b = new byte[16];
            System.arraycopy(bArr, i, this.b, 0, i2);
            this.c = 0;
            this.d = i2;
            return;
        }
        this.b = bArr;
        this.c = i;
        this.d = i + i2;
    }

    /* access modifiers changed from: protected */
    public void a(int i, ll llVar) {
        byte[] unused = llVar.b = this.b;
        int unused2 = llVar.d = this.c;
        int unused3 = llVar.c = this.c;
        int unused4 = llVar.e = this.d;
        this.a = new ln(llVar);
    }

    /* access modifiers changed from: protected */
    public void a(long j) throws IOException {
        if (b(j) < j) {
            throw new EOFException();
        }
    }

    /* access modifiers changed from: protected */
    public long b(long j) throws IOException {
        this.d = this.a.c();
        this.c = this.a.b();
        long j2 = (long) (this.d - this.c);
        if (j2 >= j) {
            this.c = (int) (((long) this.c) + j);
            this.a.a(this.c);
            return j;
        }
        this.c = (int) (((long) this.c) + j2);
        this.a.a(this.c);
        return j2;
    }

    /* access modifiers changed from: protected */
    public void a(byte[] bArr, int i, int i2) throws IOException {
        if (b(bArr, i, i2) < i2) {
            throw new EOFException();
        }
    }

    /* access modifiers changed from: protected */
    public int b(byte[] bArr, int i, int i2) throws IOException {
        return 0;
    }

    /* access modifiers changed from: protected */
    public void a(byte[] bArr, int i, int i2, int i3) throws IOException {
        if (!this.e) {
            byte[] bArr2 = new byte[(i3 + 16)];
            System.arraycopy(bArr, i, bArr2, 0, i3);
            this.a.a(bArr2, 0, i3);
            this.e = true;
        }
    }

    public int read() throws IOException {
        this.d = this.a.c();
        this.c = this.a.b();
        if (this.c >= this.d) {
            return -1;
        }
        byte[] d2 = this.a.d();
        int i = this.c;
        this.c = i + 1;
        byte b2 = d2[i] & Constants.UNKNOWN;
        this.a.a(this.c);
        return b2;
    }

    public void close() throws IOException {
        this.a.a(this.a.c());
    }
}
