package com.flurry.android.monolithic.sdk.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

class jx<E> extends ArrayList<E> {
    private boolean a = false;

    public jx() {
    }

    public jx(int i) {
        super(i);
    }

    public jx(List list) {
        super(list);
    }

    public List<E> a() {
        this.a = true;
        return this;
    }

    private void b() {
        if (this.a) {
            throw new IllegalStateException();
        }
    }

    public boolean add(E e) {
        b();
        return super.add(e);
    }

    public boolean remove(Object obj) {
        b();
        return super.remove(obj);
    }

    public E remove(int i) {
        b();
        return super.remove(i);
    }

    public boolean addAll(Collection<? extends E> collection) {
        b();
        return super.addAll(collection);
    }

    public boolean addAll(int i, Collection<? extends E> collection) {
        b();
        return super.addAll(i, collection);
    }

    public boolean removeAll(Collection<?> collection) {
        b();
        return super.removeAll(collection);
    }

    public boolean retainAll(Collection<?> collection) {
        b();
        return super.retainAll(collection);
    }

    public void clear() {
        b();
        super.clear();
    }
}
