package com.flurry.android.monolithic.sdk.impl;

import java.util.Collection;

public abstract class abu<T extends Collection<?>> extends abt<T> {
    protected final qc b;

    protected abu(Class<?> cls, qc qcVar) {
        super(cls, false);
        this.b = qcVar;
    }
}
