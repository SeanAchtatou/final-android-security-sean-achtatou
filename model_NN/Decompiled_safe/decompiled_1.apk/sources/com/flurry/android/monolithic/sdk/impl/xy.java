package com.flurry.android.monolithic.sdk.impl;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public final class xy {
    static final Class<?>[] a = new Class[0];
    final String b;
    final Class<?>[] c;

    public xy(Method method) {
        this(method.getName(), method.getParameterTypes());
    }

    public xy(Constructor<?> constructor) {
        this("", constructor.getParameterTypes());
    }

    public xy(String str, Class<?>[] clsArr) {
        Class<?>[] clsArr2;
        this.b = str;
        if (clsArr == null) {
            clsArr2 = a;
        } else {
            clsArr2 = clsArr;
        }
        this.c = clsArr2;
    }

    public String toString() {
        return this.b + "(" + this.c.length + "-args)";
    }

    public int hashCode() {
        return this.b.hashCode() + this.c.length;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        xy xyVar = (xy) obj;
        if (!this.b.equals(xyVar.b)) {
            return false;
        }
        Class<?>[] clsArr = xyVar.c;
        int length = this.c.length;
        if (clsArr.length != length) {
            return false;
        }
        for (int i = 0; i < length; i++) {
            Class<?> cls = clsArr[i];
            Class<?> cls2 = this.c[i];
            if (cls != cls2 && !cls.isAssignableFrom(cls2) && !cls2.isAssignableFrom(cls)) {
                return false;
            }
        }
        return true;
    }
}
