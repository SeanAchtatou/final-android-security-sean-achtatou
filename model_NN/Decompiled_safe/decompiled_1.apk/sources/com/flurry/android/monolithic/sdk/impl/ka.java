package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

class ka {
    /* access modifiers changed from: private */
    public final String a;
    /* access modifiers changed from: private */
    public final String b;
    /* access modifiers changed from: private */
    public final String c;

    public ka(String str, String str2) {
        if (str == null) {
            this.c = null;
            this.b = null;
            this.a = null;
            return;
        }
        int lastIndexOf = str.lastIndexOf(46);
        if (lastIndexOf < 0) {
            this.b = str2;
            this.a = ji.h(str);
        } else {
            this.b = str.substring(0, lastIndexOf);
            this.a = ji.h(str.substring(lastIndexOf + 1, str.length()));
        }
        this.c = this.b == null ? this.a : this.b + "." + this.a;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ka)) {
            return false;
        }
        ka kaVar = (ka) obj;
        if (this.c == null) {
            return kaVar.c == null;
        }
        return this.c.equals(kaVar.c);
    }

    public int hashCode() {
        if (this.c == null) {
            return 0;
        }
        return this.c.hashCode();
    }

    public String toString() {
        return this.c;
    }

    public void a(kc kcVar, or orVar) throws IOException {
        if (this.a != null) {
            orVar.a("name", this.a);
        }
        if (this.b != null) {
            if (!this.b.equals(kcVar.a())) {
                orVar.a("namespace", this.b);
            }
            if (kcVar.a() == null) {
                kcVar.a(this.b);
            }
        }
    }

    public String a(String str) {
        return (this.b == null || this.b.equals(str)) ? this.a : this.c;
    }
}
