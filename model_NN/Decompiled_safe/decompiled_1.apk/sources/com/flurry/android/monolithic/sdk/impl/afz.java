package com.flurry.android.monolithic.sdk.impl;

import com.ideaworks3d.marmalade.s3eAndroidGooglePlayBilling.util.IabHelper;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;

public class afz extends or {
    protected static final int b = ox.a();
    protected pc c;
    protected int d = b;
    protected boolean e;
    protected agc f;
    protected agc g;
    protected int h;
    protected pl i = pl.g();

    public afz(pc pcVar) {
        this.c = pcVar;
        agc agc = new agc();
        this.g = agc;
        this.f = agc;
        this.h = 0;
    }

    public ow h() {
        return a(this.c);
    }

    public ow a(pc pcVar) {
        return new agb(this.f, pcVar);
    }

    public ow a(ow owVar) {
        agb agb = new agb(this.f, owVar.a());
        agb.a(owVar.h());
        return agb;
    }

    public void a(or orVar) throws IOException, oq {
        agc agc = this.f;
        int i2 = -1;
        while (true) {
            i2++;
            if (i2 >= 16) {
                agc a = agc.a();
                if (a != null) {
                    agc = a;
                    i2 = 0;
                } else {
                    return;
                }
            }
            pb a2 = agc.a(i2);
            if (a2 != null) {
                switch (aga.a[a2.ordinal()]) {
                    case 1:
                        orVar.d();
                        break;
                    case 2:
                        orVar.e();
                        break;
                    case 3:
                        orVar.b();
                        break;
                    case 4:
                        orVar.c();
                        break;
                    case 5:
                        Object b2 = agc.b(i2);
                        if (!(b2 instanceof pe)) {
                            orVar.a((String) b2);
                            break;
                        } else {
                            orVar.a((pe) b2);
                            break;
                        }
                    case 6:
                        Object b3 = agc.b(i2);
                        if (!(b3 instanceof pe)) {
                            orVar.b((String) b3);
                            break;
                        } else {
                            orVar.b((pe) b3);
                            break;
                        }
                    case IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED /*7*/:
                        Number number = (Number) agc.b(i2);
                        if (!(number instanceof BigInteger)) {
                            if (!(number instanceof Long)) {
                                orVar.b(number.intValue());
                                break;
                            } else {
                                orVar.a(number.longValue());
                                break;
                            }
                        } else {
                            orVar.a((BigInteger) number);
                            break;
                        }
                    case IabHelper.BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED /*8*/:
                        Object b4 = agc.b(i2);
                        if (b4 instanceof BigDecimal) {
                            orVar.a((BigDecimal) b4);
                            break;
                        } else if (b4 instanceof Float) {
                            orVar.a(((Float) b4).floatValue());
                            break;
                        } else if (b4 instanceof Double) {
                            orVar.a(((Double) b4).doubleValue());
                            break;
                        } else if (b4 == null) {
                            orVar.f();
                            break;
                        } else if (b4 instanceof String) {
                            orVar.e((String) b4);
                            break;
                        } else {
                            throw new oq("Unrecognized value type for VALUE_NUMBER_FLOAT: " + b4.getClass().getName() + ", can not serialize");
                        }
                    case 9:
                        orVar.a(true);
                        break;
                    case 10:
                        orVar.a(false);
                        break;
                    case 11:
                        orVar.f();
                        break;
                    case 12:
                        orVar.a(agc.b(i2));
                        break;
                    default:
                        throw new RuntimeException("Internal error: should never end up through this code path");
                }
            } else {
                return;
            }
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[TokenBuffer: ");
        ow h2 = h();
        int i2 = 0;
        while (true) {
            try {
                pb b2 = h2.b();
                if (b2 == null) {
                    break;
                }
                if (i2 < 100) {
                    if (i2 > 0) {
                        sb.append(", ");
                    }
                    sb.append(b2.toString());
                }
                i2++;
            } catch (IOException e2) {
                throw new IllegalStateException(e2);
            }
        }
        if (i2 >= 100) {
            sb.append(" ... (truncated ").append(i2 - 100).append(" entries)");
        }
        sb.append(']');
        return sb.toString();
    }

    public or a() {
        return this;
    }

    public void g() throws IOException {
    }

    public void close() throws IOException {
        this.e = true;
    }

    public final void b() throws IOException, oq {
        a(pb.START_ARRAY);
        this.i = this.i.h();
    }

    public final void c() throws IOException, oq {
        a(pb.END_ARRAY);
        pl j = this.i.j();
        if (j != null) {
            this.i = j;
        }
    }

    public final void d() throws IOException, oq {
        a(pb.START_OBJECT);
        this.i = this.i.i();
    }

    public final void e() throws IOException, oq {
        a(pb.END_OBJECT);
        pl j = this.i.j();
        if (j != null) {
            this.i = j;
        }
    }

    public final void a(String str) throws IOException, oq {
        a(pb.FIELD_NAME, str);
        this.i.a(str);
    }

    public void a(pe peVar) throws IOException, oq {
        a(pb.FIELD_NAME, peVar);
        this.i.a(peVar.a());
    }

    public void a(pw pwVar) throws IOException, oq {
        a(pb.FIELD_NAME, pwVar);
        this.i.a(pwVar.a());
    }

    public void b(String str) throws IOException, oq {
        if (str == null) {
            f();
        } else {
            a(pb.VALUE_STRING, str);
        }
    }

    public void a(char[] cArr, int i2, int i3) throws IOException, oq {
        b(new String(cArr, i2, i3));
    }

    public void b(pe peVar) throws IOException, oq {
        if (peVar == null) {
            f();
        } else {
            a(pb.VALUE_STRING, peVar);
        }
    }

    public void c(String str) throws IOException, oq {
        i();
    }

    public void b(char[] cArr, int i2, int i3) throws IOException, oq {
        i();
    }

    public void a(char c2) throws IOException, oq {
        i();
    }

    public void d(String str) throws IOException, oq {
        i();
    }

    public void b(int i2) throws IOException, oq {
        a(pb.VALUE_NUMBER_INT, Integer.valueOf(i2));
    }

    public void a(long j) throws IOException, oq {
        a(pb.VALUE_NUMBER_INT, Long.valueOf(j));
    }

    public void a(double d2) throws IOException, oq {
        a(pb.VALUE_NUMBER_FLOAT, Double.valueOf(d2));
    }

    public void a(float f2) throws IOException, oq {
        a(pb.VALUE_NUMBER_FLOAT, Float.valueOf(f2));
    }

    public void a(BigDecimal bigDecimal) throws IOException, oq {
        if (bigDecimal == null) {
            f();
        } else {
            a(pb.VALUE_NUMBER_FLOAT, bigDecimal);
        }
    }

    public void a(BigInteger bigInteger) throws IOException, oq {
        if (bigInteger == null) {
            f();
        } else {
            a(pb.VALUE_NUMBER_INT, bigInteger);
        }
    }

    public void e(String str) throws IOException, oq {
        a(pb.VALUE_NUMBER_FLOAT, str);
    }

    public void a(boolean z) throws IOException, oq {
        a(z ? pb.VALUE_TRUE : pb.VALUE_FALSE);
    }

    public void f() throws IOException, oq {
        a(pb.VALUE_NULL);
    }

    public void a(Object obj) throws IOException, oz {
        a(pb.VALUE_EMBEDDED_OBJECT, obj);
    }

    public void a(ou ouVar) throws IOException, oz {
        a(pb.VALUE_EMBEDDED_OBJECT, ouVar);
    }

    public void a(on onVar, byte[] bArr, int i2, int i3) throws IOException, oq {
        byte[] bArr2 = new byte[i3];
        System.arraycopy(bArr, i2, bArr2, 0, i3);
        a((Object) bArr2);
    }

    public void b(ow owVar) throws IOException, oz {
        switch (aga.a[owVar.e().ordinal()]) {
            case 1:
                d();
                return;
            case 2:
                e();
                return;
            case 3:
                b();
                return;
            case 4:
                c();
                return;
            case 5:
                a(owVar.g());
                return;
            case 6:
                if (owVar.o()) {
                    a(owVar.l(), owVar.n(), owVar.m());
                    return;
                } else {
                    b(owVar.k());
                    return;
                }
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED /*7*/:
                switch (aga.b[owVar.q().ordinal()]) {
                    case 1:
                        b(owVar.t());
                        return;
                    case 2:
                        a(owVar.v());
                        return;
                    default:
                        a(owVar.u());
                        return;
                }
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED /*8*/:
                switch (aga.b[owVar.q().ordinal()]) {
                    case 3:
                        a(owVar.y());
                        return;
                    case 4:
                        a(owVar.w());
                        return;
                    default:
                        a(owVar.x());
                        return;
                }
            case 9:
                a(true);
                return;
            case 10:
                a(false);
                return;
            case 11:
                f();
                return;
            case 12:
                a(owVar.z());
                return;
            default:
                throw new RuntimeException("Internal error: should never end up through this code path");
        }
    }

    public void c(ow owVar) throws IOException, oz {
        pb e2 = owVar.e();
        if (e2 == pb.FIELD_NAME) {
            a(owVar.g());
            e2 = owVar.b();
        }
        switch (aga.a[e2.ordinal()]) {
            case 1:
                d();
                while (owVar.b() != pb.END_OBJECT) {
                    c(owVar);
                }
                e();
                return;
            case 2:
            default:
                b(owVar);
                return;
            case 3:
                b();
                while (owVar.b() != pb.END_ARRAY) {
                    c(owVar);
                }
                c();
                return;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(pb pbVar) {
        agc a = this.g.a(this.h, pbVar);
        if (a == null) {
            this.h++;
            return;
        }
        this.g = a;
        this.h = 1;
    }

    /* access modifiers changed from: protected */
    public final void a(pb pbVar, Object obj) {
        agc a = this.g.a(this.h, pbVar, obj);
        if (a == null) {
            this.h++;
            return;
        }
        this.g = a;
        this.h = 1;
    }

    /* access modifiers changed from: protected */
    public void i() {
        throw new UnsupportedOperationException("Called operation not supported for TokenBuffer");
    }
}
