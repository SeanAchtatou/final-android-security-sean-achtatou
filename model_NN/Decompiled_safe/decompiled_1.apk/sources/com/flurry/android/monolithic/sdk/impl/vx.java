package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
public final class vx extends wa<Integer> {
    public vx(Class<Integer> cls, Integer num) {
        super(cls, num);
    }

    /* renamed from: b */
    public Integer a(ow owVar, qm qmVar) throws IOException, oz {
        return u(owVar, qmVar);
    }

    /* renamed from: b */
    public Integer a(ow owVar, qm qmVar, rw rwVar) throws IOException, oz {
        return u(owVar, qmVar);
    }
}
