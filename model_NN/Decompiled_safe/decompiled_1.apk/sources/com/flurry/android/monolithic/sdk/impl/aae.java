package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
public final class aae extends abq<Long> {
    static final aae a = new aae();

    public aae() {
        super(Long.class);
    }

    public void a(Long l, or orVar, ru ruVar) throws IOException, oq {
        orVar.a(l.longValue());
    }
}
