package com.flurry.android.monolithic.sdk.impl;

import android.util.Base64;
import java.util.List;
import org.apache.http.NameValuePair;

public class hh {
    public static String a(String str, String str2) {
        return "Basic " + Base64.encodeToString((str + ":" + str2).getBytes(), 10);
    }

    public static String a(List<NameValuePair> list) {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= list.size()) {
                return sb.toString();
            }
            sb.append(a(list.get(i2)) + "/");
            i = i2 + 1;
        }
    }

    public static String a(NameValuePair nameValuePair) {
        return (nameValuePair.getName() + "=" + nameValuePair.getValue()).replaceAll(" ", "%20");
    }
}
