package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.math.BigDecimal;

@rz
public class vq extends wv<BigDecimal> {
    public vq() {
        super(BigDecimal.class);
    }

    /* renamed from: b */
    public BigDecimal a(ow owVar, qm qmVar) throws IOException, oz {
        pb e = owVar.e();
        if (e == pb.VALUE_NUMBER_INT || e == pb.VALUE_NUMBER_FLOAT) {
            return owVar.y();
        }
        if (e == pb.VALUE_STRING) {
            String trim = owVar.k().trim();
            if (trim.length() == 0) {
                return null;
            }
            try {
                return new BigDecimal(trim);
            } catch (IllegalArgumentException e2) {
                throw qmVar.b(this.q, "not a valid representation");
            }
        } else {
            throw qmVar.a(this.q, e);
        }
    }
}
