package com.flurry.android.monolithic.sdk.impl;

public class ev extends jf {
    static int a = 15000;
    private String b;
    private String c;
    private String d;
    private byte[] e;
    private ex f;

    public ev(String str, String str2, String str3, byte[] bArr, ex exVar) {
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = bArr;
        this.f = exVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x006a A[SYNTHETIC, Splitter:B:8:0x006a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a() {
        /*
            r7 = this;
            r5 = 0
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            long r0 = r0.getId()
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            java.lang.Thread r1 = java.lang.Thread.currentThread()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "DataSender Sending Executor Thread, id = "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.setName(r0)
            org.apache.http.entity.ByteArrayEntity r0 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ Exception -> 0x007a, all -> 0x008a }
            byte[] r1 = r7.e     // Catch:{ Exception -> 0x007a, all -> 0x008a }
            r0.<init>(r1)     // Catch:{ Exception -> 0x007a, all -> 0x008a }
            java.lang.String r1 = "application/octet-stream"
            r0.setContentType(r1)     // Catch:{ Exception -> 0x007a, all -> 0x008a }
            org.apache.http.client.methods.HttpPost r1 = new org.apache.http.client.methods.HttpPost     // Catch:{ Exception -> 0x007a, all -> 0x008a }
            java.lang.String r2 = r7.b     // Catch:{ Exception -> 0x007a, all -> 0x008a }
            r1.<init>(r2)     // Catch:{ Exception -> 0x007a, all -> 0x008a }
            r1.setEntity(r0)     // Catch:{ Exception -> 0x007a, all -> 0x008a }
            org.apache.http.params.BasicHttpParams r0 = new org.apache.http.params.BasicHttpParams     // Catch:{ Exception -> 0x007a, all -> 0x008a }
            r0.<init>()     // Catch:{ Exception -> 0x007a, all -> 0x008a }
            r2 = 10000(0x2710, float:1.4013E-41)
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r0, r2)     // Catch:{ Exception -> 0x007a, all -> 0x008a }
            int r2 = com.flurry.android.monolithic.sdk.impl.ev.a     // Catch:{ Exception -> 0x007a, all -> 0x008a }
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r0, r2)     // Catch:{ Exception -> 0x007a, all -> 0x008a }
            org.apache.http.params.HttpParams r2 = r1.getParams()     // Catch:{ Exception -> 0x007a, all -> 0x008a }
            java.lang.String r3 = "http.protocol.expect-continue"
            r4 = 0
            r2.setBooleanParameter(r3, r4)     // Catch:{ Exception -> 0x007a, all -> 0x008a }
            org.apache.http.client.HttpClient r0 = com.flurry.android.monolithic.sdk.impl.iz.b(r0)     // Catch:{ Exception -> 0x007a, all -> 0x008a }
            org.apache.http.HttpResponse r1 = r0.execute(r1)     // Catch:{ Exception -> 0x00b8, all -> 0x00b1 }
            if (r0 == 0) goto L_0x00bf
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()
            r0.shutdown()
            r0 = r1
        L_0x0068:
            if (r0 == 0) goto L_0x0070
            org.apache.http.StatusLine r1 = r0.getStatusLine()     // Catch:{ Exception -> 0x00ac }
            if (r1 != 0) goto L_0x0096
        L_0x0070:
            com.flurry.android.monolithic.sdk.impl.ex r0 = r7.f     // Catch:{ Exception -> 0x00ac }
            java.lang.String r1 = r7.c     // Catch:{ Exception -> 0x00ac }
            java.lang.String r2 = r7.d     // Catch:{ Exception -> 0x00ac }
            r0.a(r1, r2)     // Catch:{ Exception -> 0x00ac }
        L_0x0079:
            return
        L_0x007a:
            r0 = move-exception
            r1 = r5
        L_0x007c:
            r0.printStackTrace()     // Catch:{ all -> 0x00b6 }
            if (r1 == 0) goto L_0x00bd
            org.apache.http.conn.ClientConnectionManager r0 = r1.getConnectionManager()
            r0.shutdown()
            r0 = r5
            goto L_0x0068
        L_0x008a:
            r0 = move-exception
            r1 = r5
        L_0x008c:
            if (r1 == 0) goto L_0x0095
            org.apache.http.conn.ClientConnectionManager r1 = r1.getConnectionManager()
            r1.shutdown()
        L_0x0095:
            throw r0
        L_0x0096:
            org.apache.http.StatusLine r0 = r0.getStatusLine()     // Catch:{ Exception -> 0x00ac }
            int r1 = r0.getStatusCode()     // Catch:{ Exception -> 0x00ac }
            java.lang.String r0 = r0.getReasonPhrase()     // Catch:{ Exception -> 0x00ac }
            com.flurry.android.monolithic.sdk.impl.ex r2 = r7.f     // Catch:{ Exception -> 0x00ac }
            java.lang.String r3 = r7.c     // Catch:{ Exception -> 0x00ac }
            java.lang.String r4 = r7.d     // Catch:{ Exception -> 0x00ac }
            r2.a(r1, r0, r3, r4)     // Catch:{ Exception -> 0x00ac }
            goto L_0x0079
        L_0x00ac:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0079
        L_0x00b1:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x008c
        L_0x00b6:
            r0 = move-exception
            goto L_0x008c
        L_0x00b8:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x007c
        L_0x00bd:
            r0 = r5
            goto L_0x0068
        L_0x00bf:
            r0 = r1
            goto L_0x0068
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.ev.a():void");
    }
}
