package com.flurry.android.monolithic.sdk.impl;

import com.flurry.android.Constants;
import java.util.Arrays;

public final class on {
    final String a;
    final boolean b;
    final char c;
    final int d;
    private final int[] e;
    private final char[] f;
    private final byte[] g;

    public on(String str, String str2, boolean z, char c2, int i) {
        this.e = new int[128];
        this.f = new char[64];
        this.g = new byte[64];
        this.a = str;
        this.b = z;
        this.c = c2;
        this.d = i;
        int length = str2.length();
        if (length != 64) {
            throw new IllegalArgumentException("Base64Alphabet length must be exactly 64 (was " + length + ")");
        }
        str2.getChars(0, length, this.f, 0);
        Arrays.fill(this.e, -1);
        for (int i2 = 0; i2 < length; i2++) {
            char c3 = this.f[i2];
            this.g[i2] = (byte) c3;
            this.e[c3] = i2;
        }
        if (z) {
            this.e[c2] = -2;
        }
    }

    public on(on onVar, String str, int i) {
        this(onVar, str, onVar.b, onVar.c, i);
    }

    public on(on onVar, String str, boolean z, char c2, int i) {
        this.e = new int[128];
        this.f = new char[64];
        this.g = new byte[64];
        this.a = str;
        byte[] bArr = onVar.g;
        System.arraycopy(bArr, 0, this.g, 0, bArr.length);
        char[] cArr = onVar.f;
        System.arraycopy(cArr, 0, this.f, 0, cArr.length);
        int[] iArr = onVar.e;
        System.arraycopy(iArr, 0, this.e, 0, iArr.length);
        this.b = z;
        this.c = c2;
        this.d = i;
    }

    public boolean a() {
        return this.b;
    }

    public boolean a(char c2) {
        return c2 == this.c;
    }

    public boolean a(int i) {
        return i == this.c;
    }

    public char b() {
        return this.c;
    }

    public int c() {
        return this.d;
    }

    public int b(char c2) {
        if (c2 <= 127) {
            return this.e[c2];
        }
        return -1;
    }

    public int a(int i, char[] cArr, int i2) {
        int i3 = i2 + 1;
        cArr[i2] = this.f[(i >> 18) & 63];
        int i4 = i3 + 1;
        cArr[i3] = this.f[(i >> 12) & 63];
        int i5 = i4 + 1;
        cArr[i4] = this.f[(i >> 6) & 63];
        int i6 = i5 + 1;
        cArr[i5] = this.f[i & 63];
        return i6;
    }

    public void a(StringBuilder sb, int i) {
        sb.append(this.f[(i >> 18) & 63]);
        sb.append(this.f[(i >> 12) & 63]);
        sb.append(this.f[(i >> 6) & 63]);
        sb.append(this.f[i & 63]);
    }

    public int a(int i, int i2, char[] cArr, int i3) {
        int i4 = i3 + 1;
        cArr[i3] = this.f[(i >> 18) & 63];
        int i5 = i4 + 1;
        cArr[i4] = this.f[(i >> 12) & 63];
        if (this.b) {
            int i6 = i5 + 1;
            cArr[i5] = i2 == 2 ? this.f[(i >> 6) & 63] : this.c;
            cArr[i6] = this.c;
            return i6 + 1;
        } else if (i2 != 2) {
            return i5;
        } else {
            int i7 = i5 + 1;
            cArr[i5] = this.f[(i >> 6) & 63];
            return i7;
        }
    }

    public void a(StringBuilder sb, int i, int i2) {
        sb.append(this.f[(i >> 18) & 63]);
        sb.append(this.f[(i >> 12) & 63]);
        if (this.b) {
            sb.append(i2 == 2 ? this.f[(i >> 6) & 63] : this.c);
            sb.append(this.c);
        } else if (i2 == 2) {
            sb.append(this.f[(i >> 6) & 63]);
        }
    }

    public String a(byte[] bArr, boolean z) {
        int length = bArr.length;
        StringBuilder sb = new StringBuilder((length >> 2) + length + (length >> 3));
        if (z) {
            sb.append('\"');
        }
        int i = length - 3;
        int c2 = c() >> 2;
        int i2 = 0;
        while (i2 <= i) {
            int i3 = i2 + 1;
            int i4 = i3 + 1;
            int i5 = i4 + 1;
            a(sb, (((bArr[i2] << 8) | (bArr[i3] & Constants.UNKNOWN)) << 8) | (bArr[i4] & Constants.UNKNOWN));
            int i6 = c2 - 1;
            if (i6 <= 0) {
                sb.append('\\');
                sb.append('n');
                i6 = c() >> 2;
            }
            c2 = i6;
            i2 = i5;
        }
        int i7 = length - i2;
        if (i7 > 0) {
            int i8 = i2 + 1;
            int i9 = bArr[i2] << 16;
            if (i7 == 2) {
                int i10 = i8 + 1;
                i9 |= (bArr[i8] & Constants.UNKNOWN) << 8;
            }
            a(sb, i9, i7);
        }
        if (z) {
            sb.append('\"');
        }
        return sb.toString();
    }

    public String toString() {
        return this.a;
    }
}
