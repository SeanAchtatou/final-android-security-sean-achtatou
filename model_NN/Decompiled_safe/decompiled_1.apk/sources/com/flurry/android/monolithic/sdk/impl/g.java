package com.flurry.android.monolithic.sdk.impl;

import com.flurry.android.FlurryWallet;
import com.flurry.android.FlurryWalletError;
import com.flurry.android.FlurryWalletOperationHandler;

public final class g implements hz {
    final /* synthetic */ String a;
    final /* synthetic */ float b;
    final /* synthetic */ FlurryWalletOperationHandler c;

    public g(String str, float f, FlurryWalletOperationHandler flurryWalletOperationHandler) {
        this.a = str;
        this.b = f;
        this.c = flurryWalletOperationHandler;
    }

    public void a(ft ftVar) {
        FlurryWallet.b(this.a, this.b, this.c);
    }

    public void a(hy hyVar) {
        this.c.onError(new FlurryWalletError(hyVar.a(), hyVar.b()));
    }
}
