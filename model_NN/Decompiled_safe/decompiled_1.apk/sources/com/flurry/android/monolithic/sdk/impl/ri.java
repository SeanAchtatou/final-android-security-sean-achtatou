package com.flurry.android.monolithic.sdk.impl;

import com.flurry.android.monolithic.sdk.impl.rh;
import com.flurry.android.monolithic.sdk.impl.ri;

abstract class ri<CFG extends rh, T extends ri<CFG, T>> extends rf<T> {
    protected int i;

    protected ri(qf<? extends qb> qfVar, py pyVar, ye<?> yeVar, yh yhVar, rl rlVar, adk adk, qs qsVar, int i2) {
        super(qfVar, pyVar, yeVar, yhVar, rlVar, adk, qsVar);
        this.i = i2;
    }

    protected ri(ri<CFG, T> riVar, rg rgVar, yh yhVar) {
        super(riVar, rgVar, yhVar);
        this.i = riVar.i;
    }

    static <F extends Enum<F> & rh> int d(Class cls) {
        int i2;
        Enum[] enumArr = (Enum[]) cls.getEnumConstants();
        int length = enumArr.length;
        int i3 = 0;
        int i4 = 0;
        while (i3 < length) {
            Enum enumR = enumArr[i3];
            if (((rh) enumR).a()) {
                i2 = ((rh) enumR).b() | i4;
            } else {
                i2 = i4;
            }
            i3++;
            i4 = i2;
        }
        return i4;
    }
}
