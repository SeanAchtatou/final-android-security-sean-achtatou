package com.flurry.android.monolithic.sdk.impl;

public final class add extends adc {
    private add(Class<?> cls, afm afm, Object obj, Object obj2) {
        super(cls, afm, obj, obj2);
    }

    /* access modifiers changed from: protected */
    public afm a(Class<?> cls) {
        return new add(cls, this.a, null, null);
    }

    public afm b(Class<?> cls) {
        return cls == this.a.p() ? this : new add(this.d, this.a.f(cls), this.f, this.g);
    }

    public afm c(Class<?> cls) {
        return cls == this.a.p() ? this : new add(this.d, this.a.h(cls), this.f, this.g);
    }

    public static add a(Class<?> cls, afm afm) {
        return new add(cls, afm, null, null);
    }

    /* renamed from: g */
    public add f(Object obj) {
        return new add(this.d, this.a, this.f, obj);
    }

    /* renamed from: h */
    public add e(Object obj) {
        return new add(this.d, this.a.f(obj), this.f, this.g);
    }

    /* renamed from: i */
    public add d(Object obj) {
        return new add(this.d, this.a, obj, this.g);
    }

    public String toString() {
        return "[collection type; class " + this.d.getName() + ", contains " + this.a + "]";
    }
}
