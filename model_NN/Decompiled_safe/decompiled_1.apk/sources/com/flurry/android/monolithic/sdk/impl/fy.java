package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

public class fy {
    public static void a() {
    }

    public static synchronized void a(String str) {
        synchronized (fy.class) {
            Context b = ia.a().b();
            SharedPreferences.Editor edit = b.getSharedPreferences("FLURRY_SHARED_PREFERENCES", 0).edit();
            edit.putString("flurry_last_user_id", str);
            edit.commit();
            if (!TextUtils.isEmpty(str)) {
                go.a(b, str);
            }
        }
    }

    public static synchronized void a(String str, String str2, String str3) {
        synchronized (fy.class) {
            SharedPreferences.Editor edit = ia.a().b().getSharedPreferences("FLURRY_SHARED_PREFERENCES", 0).edit();
            if (!TextUtils.isEmpty(str)) {
                edit.putString("flurry_last_user_name", str);
            }
            if (!TextUtils.isEmpty(str)) {
                edit.putString("flurry_last_user_email", str2);
            }
            if (!TextUtils.isEmpty(str3)) {
                edit.putString("flurry_last_user_session", str3);
            }
            edit.commit();
            ja.a(4, "AppCloudUserHelper", "saveLastLoggedInUserData, (Name, Email, Token) = ( " + str + " , " + str2 + " , " + str3 + " )");
        }
    }

    public static synchronized void b(String str) {
        synchronized (fy.class) {
            SharedPreferences.Editor edit = ia.a().b().getSharedPreferences("FLURRY_SHARED_PREFERENCES", 0).edit();
            edit.putString("flurry_last_user_pass", str);
            edit.commit();
        }
    }

    public static synchronized void b() {
        synchronized (fy.class) {
            SharedPreferences.Editor edit = ia.a().b().getSharedPreferences("FLURRY_SHARED_PREFERENCES", 0).edit();
            edit.putString("flurry_last_user_session", "");
            edit.commit();
        }
    }

    public static synchronized ft c() {
        ft ftVar;
        synchronized (fy.class) {
            ftVar = new ft();
            String d = d();
            if (!TextUtils.isEmpty(d)) {
                ftVar.b(d);
                String e = e();
                if (!TextUtils.isEmpty(e)) {
                    ftVar.e(e);
                }
                String f = f();
                if (!TextUtils.isEmpty(f)) {
                    ftVar.d(f);
                }
                ftVar.f = g();
            } else {
                ftVar = null;
            }
        }
        return ftVar;
    }

    public static synchronized String d() {
        String string;
        synchronized (fy.class) {
            string = ia.a().b().getSharedPreferences("FLURRY_SHARED_PREFERENCES", 0).getString("flurry_last_user_id", "");
        }
        return string;
    }

    public static synchronized String e() {
        String string;
        synchronized (fy.class) {
            string = ia.a().b().getSharedPreferences("FLURRY_SHARED_PREFERENCES", 0).getString("flurry_last_user_name", "");
        }
        return string;
    }

    public static synchronized String f() {
        String string;
        synchronized (fy.class) {
            string = ia.a().b().getSharedPreferences("FLURRY_SHARED_PREFERENCES", 0).getString("flurry_last_user_email", "");
        }
        return string;
    }

    public static synchronized String g() {
        String string;
        synchronized (fy.class) {
            string = ia.a().b().getSharedPreferences("FLURRY_SHARED_PREFERENCES", 0).getString("flurry_last_user_pass", "");
        }
        return string;
    }

    public static synchronized String h() {
        String string;
        synchronized (fy.class) {
            string = ia.a().b().getSharedPreferences("FLURRY_SHARED_PREFERENCES", 0).getString("flurry_last_user_session", "");
            ja.a(4, "AppCloudUserHelper", "User Session = " + string);
        }
        return string;
    }

    public static synchronized void i() {
        synchronized (fy.class) {
            ia.a().b().getSharedPreferences("FLURRY_SHARED_PREFERENCES", 0).edit().clear().commit();
        }
    }
}
