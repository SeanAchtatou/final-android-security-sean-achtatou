package com.flurry.android.monolithic.sdk.impl;

public enum oy {
    INT,
    LONG,
    BIG_INTEGER,
    FLOAT,
    DOUBLE,
    BIG_DECIMAL
}
