package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Date;

public abstract class ru {
    protected static final afm a = adk.a().a((Class<?>) Object.class);
    protected final rq b;
    protected final Class<?> c;

    public abstract ra<Object> a(afm afm, qc qcVar) throws qw;

    public abstract ra<Object> a(afm afm, boolean z, qc qcVar) throws qw;

    public abstract ra<Object> a(Class<?> cls, qc qcVar) throws qw;

    public abstract ra<Object> a(Class<?> cls, boolean z, qc qcVar) throws qw;

    public abstract void a(long j, or orVar) throws IOException, oz;

    public abstract void a(rq rqVar, or orVar, Object obj, rs rsVar) throws IOException, oq;

    public abstract void a(Date date, or orVar) throws IOException, oz;

    public abstract ra<Object> b(afm afm, qc qcVar) throws qw;

    public abstract void b(long j, or orVar) throws IOException, oz;

    public abstract void b(Date date, or orVar) throws IOException, oz;

    public abstract ra<Object> c();

    public abstract ra<Object> d();

    protected ru(rq rqVar) {
        this.b = rqVar;
        this.c = rqVar == null ? null : this.b.f();
    }

    public final boolean a(rr rrVar) {
        return this.b.a(rrVar);
    }

    public final Class<?> a() {
        return this.c;
    }

    public final zl b() {
        return this.b.h();
    }

    public afm a(Type type) {
        return this.b.m().a(type);
    }

    public afm a(afm afm, Class<?> cls) {
        return this.b.a(afm, cls);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.ru.a(java.lang.Class<?>, boolean, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object>
     arg types: [java.lang.Class<?>, int, ?[OBJECT, ARRAY]]
     candidates:
      com.flurry.android.monolithic.sdk.impl.ru.a(com.flurry.android.monolithic.sdk.impl.afm, boolean, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object>
      com.flurry.android.monolithic.sdk.impl.ru.a(java.lang.Class<?>, boolean, com.flurry.android.monolithic.sdk.impl.qc):com.flurry.android.monolithic.sdk.impl.ra<java.lang.Object> */
    public final void a(Object obj, or orVar) throws IOException, oz {
        if (obj == null) {
            d().a(null, orVar, this);
        } else {
            a(obj.getClass(), true, (qc) null).a(obj, orVar, this);
        }
    }

    public final void a(or orVar) throws IOException, oz {
        d().a(null, orVar, this);
    }
}
