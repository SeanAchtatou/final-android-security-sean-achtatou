package com.flurry.android.monolithic.sdk.impl;

import com.flurry.android.FlurryWalletError;
import com.flurry.android.FlurryWalletOperationHandler;

public final class fk implements hx {
    private final FlurryWalletOperationHandler a;

    public fk(FlurryWalletOperationHandler flurryWalletOperationHandler) {
        this.a = flurryWalletOperationHandler;
    }

    public void a() {
        if (this.a != null) {
            this.a.onOperationSucceed();
        }
    }

    public void a(hy hyVar) {
        if (this.a != null && hyVar != null) {
            this.a.onError(new FlurryWalletError(hyVar.a(), hyVar.b()));
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof fk)) {
            return false;
        }
        return this.a == ((fk) obj).a;
    }

    public int hashCode() {
        int i = 17 * 31;
        return (this.a == null ? 0 : this.a.hashCode()) + 527;
    }
}
