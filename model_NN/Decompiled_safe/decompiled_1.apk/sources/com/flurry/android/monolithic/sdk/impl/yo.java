package com.flurry.android.monolithic.sdk.impl;

import com.flurry.org.codehaus.jackson.annotate.JsonTypeInfo;
import java.io.IOException;

public class yo extends yk {
    protected final String a;

    public yo(afm afm, yi yiVar, qc qcVar, Class<?> cls, String str) {
        super(afm, yiVar, qcVar, cls);
        this.a = str;
    }

    public JsonTypeInfo.As a() {
        return JsonTypeInfo.As.PROPERTY;
    }

    public String b() {
        return this.a;
    }

    public Object a(ow owVar, qm qmVar) throws IOException, oz {
        ow owVar2;
        pb e = owVar.e();
        if (e == pb.START_OBJECT) {
            e = owVar.b();
        } else if (e == pb.START_ARRAY) {
            return a(owVar, qmVar, null);
        } else {
            if (e != pb.FIELD_NAME) {
                return a(owVar, qmVar, null);
            }
        }
        pb pbVar = e;
        afz afz = null;
        while (pbVar == pb.FIELD_NAME) {
            String g = owVar.g();
            owVar.b();
            if (this.a.equals(g)) {
                qu<Object> a2 = a(qmVar, owVar.k());
                if (afz != null) {
                    owVar2 = afx.a(afz.a(owVar), owVar);
                } else {
                    owVar2 = owVar;
                }
                owVar2.b();
                return a2.a(owVar2, qmVar);
            }
            if (afz == null) {
                afz = new afz(null);
            }
            afz.a(g);
            afz.c(owVar);
            pbVar = owVar.b();
        }
        return a(owVar, qmVar, afz);
    }

    /* access modifiers changed from: protected */
    public Object a(ow owVar, qm qmVar, afz afz) throws IOException, oz {
        ow owVar2;
        if (this.e != null) {
            qu<Object> a2 = a(qmVar);
            if (afz != null) {
                afz.e();
                owVar2 = afz.a(owVar);
                owVar2.b();
            } else {
                owVar2 = owVar;
            }
            return a2.a(owVar2, qmVar);
        }
        Object f = f(owVar, qmVar);
        if (f != null) {
            return f;
        }
        if (owVar.e() == pb.START_ARRAY) {
            return super.d(owVar, qmVar);
        }
        throw qmVar.a(owVar, pb.FIELD_NAME, "missing property '" + this.a + "' that is to contain type id  (for class " + c() + ")");
    }

    public Object d(ow owVar, qm qmVar) throws IOException, oz {
        if (owVar.e() == pb.START_ARRAY) {
            return super.b(owVar, qmVar);
        }
        return a(owVar, qmVar);
    }

    /* access modifiers changed from: protected */
    public Object f(ow owVar, qm qmVar) throws IOException, oz {
        Class<Boolean> cls = Boolean.class;
        switch (yp.a[owVar.e().ordinal()]) {
            case 1:
                if (this.c.p().isAssignableFrom(String.class)) {
                    return owVar.k();
                }
                break;
            case 2:
                if (this.c.p().isAssignableFrom(Integer.class)) {
                    return Integer.valueOf(owVar.t());
                }
                break;
            case 3:
                if (this.c.p().isAssignableFrom(Double.class)) {
                    return Double.valueOf(owVar.x());
                }
                break;
            case 4:
                Class<Boolean> cls2 = Boolean.class;
                if (this.c.p().isAssignableFrom(cls)) {
                    return Boolean.TRUE;
                }
                break;
            case 5:
                Class<Boolean> cls3 = Boolean.class;
                if (this.c.p().isAssignableFrom(cls)) {
                    return Boolean.FALSE;
                }
                break;
        }
        return null;
    }
}
