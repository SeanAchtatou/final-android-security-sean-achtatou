package com.flurry.android.monolithic.sdk.impl;

import android.content.Context;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class cw implements cx {
    private final List<cx> a;

    public cw() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new cv());
        arrayList.add(new da());
        arrayList.add(new ct());
        arrayList.add(new cz());
        this.a = Collections.unmodifiableList(arrayList);
    }

    public boolean a(Context context, db dbVar) {
        if (context == null || dbVar == null) {
            return false;
        }
        boolean z = true;
        for (cx a2 : this.a) {
            if (!a2.a(context, dbVar)) {
                z = false;
            }
        }
        return z;
    }
}
