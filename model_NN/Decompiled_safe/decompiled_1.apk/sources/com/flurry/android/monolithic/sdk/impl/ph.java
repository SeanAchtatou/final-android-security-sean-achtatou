package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import v2.com.playhaven.requests.base.PHAsyncRequest;

public abstract class ph extends pi {
    static final BigDecimal u = new BigDecimal(Long.MIN_VALUE);
    static final BigDecimal v = new BigDecimal(Long.MAX_VALUE);
    static final BigDecimal w = new BigDecimal(Long.MIN_VALUE);
    static final BigDecimal x = new BigDecimal(Long.MAX_VALUE);
    protected long A;
    protected double B;
    protected BigInteger C;
    protected BigDecimal D;
    protected boolean E;
    protected int F;
    protected int G;
    protected int H;
    protected final pq d;
    protected boolean e;
    protected int f = 0;
    protected int g = 0;
    protected long h = 0;
    protected int i = 1;
    protected int j = 0;
    protected long k = 0;
    protected int l = 1;
    protected int m = 0;
    protected pk n;
    protected pb o;
    protected final afy p;
    protected char[] q = null;
    protected boolean r = false;
    protected afq s = null;
    protected byte[] t;
    protected int y = 0;
    protected int z;

    /* access modifiers changed from: protected */
    public abstract boolean E() throws IOException;

    /* access modifiers changed from: protected */
    public abstract void F() throws IOException;

    protected ph(pq pqVar, int i2) {
        this.a = i2;
        this.d = pqVar;
        this.p = pqVar.c();
        this.n = pk.g();
    }

    public String g() throws IOException, ov {
        if (this.b == pb.START_OBJECT || this.b == pb.START_ARRAY) {
            return this.n.i().h();
        }
        return this.n.h();
    }

    public void close() throws IOException {
        if (!this.e) {
            this.e = true;
            try {
                F();
            } finally {
                G();
            }
        }
    }

    public ot h() {
        return new ot(this.d.a(), A(), B(), C());
    }

    public ot i() {
        return new ot(this.d.a(), (this.h + ((long) this.f)) - 1, this.i, (this.f - this.j) + 1);
    }

    public boolean o() {
        if (this.b == pb.VALUE_STRING) {
            return true;
        }
        if (this.b == pb.FIELD_NAME) {
            return this.r;
        }
        return false;
    }

    public final long A() {
        return this.k;
    }

    public final int B() {
        return this.l;
    }

    public final int C() {
        int i2 = this.m;
        return i2 < 0 ? i2 : i2 + 1;
    }

    /* access modifiers changed from: protected */
    public final void D() throws IOException {
        if (!E()) {
            S();
        }
    }

    /* access modifiers changed from: protected */
    public void G() throws IOException {
        this.p.a();
        char[] cArr = this.q;
        if (cArr != null) {
            this.q = null;
            this.d.c(cArr);
        }
    }

    /* access modifiers changed from: protected */
    public void H() throws ov {
        if (!this.n.b()) {
            c(": expected close marker for " + this.n.d() + " (from " + this.n.a(this.d.a()) + ")");
        }
    }

    /* access modifiers changed from: protected */
    public void a(int i2, char c) throws ov {
        d("Unexpected close marker '" + ((char) i2) + "': expected '" + c + "' (for " + this.n.d() + " starting at " + ("" + this.n.a(this.d.a())) + ")");
    }

    public afq I() {
        if (this.s == null) {
            this.s = new afq();
        } else {
            this.s.a();
        }
        return this.s;
    }

    /* access modifiers changed from: protected */
    public final pb a(boolean z2, int i2, int i3, int i4) {
        if (i3 >= 1 || i4 >= 1) {
            return b(z2, i2, i3, i4);
        }
        return a(z2, i2);
    }

    /* access modifiers changed from: protected */
    public final pb a(boolean z2, int i2) {
        this.E = z2;
        this.F = i2;
        this.G = 0;
        this.H = 0;
        this.y = 0;
        return pb.VALUE_NUMBER_INT;
    }

    /* access modifiers changed from: protected */
    public final pb b(boolean z2, int i2, int i3, int i4) {
        this.E = z2;
        this.F = i2;
        this.G = i3;
        this.H = i4;
        this.y = 0;
        return pb.VALUE_NUMBER_FLOAT;
    }

    /* access modifiers changed from: protected */
    public final pb a(String str, double d2) {
        this.p.a(str);
        this.B = d2;
        this.y = 8;
        return pb.VALUE_NUMBER_FLOAT;
    }

    public Number p() throws IOException, ov {
        if (this.y == 0) {
            a(0);
        }
        if (this.b == pb.VALUE_NUMBER_INT) {
            if ((this.y & 1) != 0) {
                return Integer.valueOf(this.z);
            }
            if ((this.y & 2) != 0) {
                return Long.valueOf(this.A);
            }
            if ((this.y & 4) != 0) {
                return this.C;
            }
            return this.D;
        } else if ((this.y & 16) != 0) {
            return this.D;
        } else {
            if ((this.y & 8) == 0) {
                U();
            }
            return Double.valueOf(this.B);
        }
    }

    public oy q() throws IOException, ov {
        if (this.y == 0) {
            a(0);
        }
        if (this.b == pb.VALUE_NUMBER_INT) {
            if ((this.y & 1) != 0) {
                return oy.INT;
            }
            if ((this.y & 2) != 0) {
                return oy.LONG;
            }
            return oy.BIG_INTEGER;
        } else if ((this.y & 16) != 0) {
            return oy.BIG_DECIMAL;
        } else {
            return oy.DOUBLE;
        }
    }

    public int t() throws IOException, ov {
        if ((this.y & 1) == 0) {
            if (this.y == 0) {
                a(1);
            }
            if ((this.y & 1) == 0) {
                J();
            }
        }
        return this.z;
    }

    public long u() throws IOException, ov {
        if ((this.y & 2) == 0) {
            if (this.y == 0) {
                a(2);
            }
            if ((this.y & 2) == 0) {
                K();
            }
        }
        return this.A;
    }

    public BigInteger v() throws IOException, ov {
        if ((this.y & 4) == 0) {
            if (this.y == 0) {
                a(4);
            }
            if ((this.y & 4) == 0) {
                L();
            }
        }
        return this.C;
    }

    public float w() throws IOException, ov {
        return (float) x();
    }

    public double x() throws IOException, ov {
        if ((this.y & 8) == 0) {
            if (this.y == 0) {
                a(8);
            }
            if ((this.y & 8) == 0) {
                M();
            }
        }
        return this.B;
    }

    public BigDecimal y() throws IOException, ov {
        if ((this.y & 16) == 0) {
            if (this.y == 0) {
                a(16);
            }
            if ((this.y & 16) == 0) {
                N();
            }
        }
        return this.D;
    }

    /* access modifiers changed from: protected */
    public void a(int i2) throws IOException, ov {
        if (this.b == pb.VALUE_NUMBER_INT) {
            char[] e2 = this.p.e();
            int d2 = this.p.d();
            int i3 = this.F;
            if (this.E) {
                d2++;
            }
            if (i3 <= 9) {
                int a = pt.a(e2, d2, i3);
                if (this.E) {
                    a = -a;
                }
                this.z = a;
                this.y = 1;
            } else if (i3 <= 18) {
                long b = pt.b(e2, d2, i3);
                if (this.E) {
                    b = -b;
                }
                if (i3 == 10) {
                    if (this.E) {
                        if (b >= -2147483648L) {
                            this.z = (int) b;
                            this.y = 1;
                            return;
                        }
                    } else if (b <= 2147483647L) {
                        this.z = (int) b;
                        this.y = 1;
                        return;
                    }
                }
                this.A = b;
                this.y = 2;
            } else {
                a(i2, e2, d2, i3);
            }
        } else if (this.b == pb.VALUE_NUMBER_FLOAT) {
            d(i2);
        } else {
            d("Current token (" + this.b + ") not numeric, can not use numeric value accessors");
        }
    }

    private final void d(int i2) throws IOException, ov {
        if (i2 == 16) {
            try {
                this.D = this.p.h();
                this.y = 16;
            } catch (NumberFormatException e2) {
                a("Malformed numeric value '" + this.p.f() + "'", e2);
            }
        } else {
            this.B = this.p.i();
            this.y = 8;
        }
    }

    private final void a(int i2, char[] cArr, int i3, int i4) throws IOException, ov {
        String f2 = this.p.f();
        try {
            if (pt.a(cArr, i3, i4, this.E)) {
                this.A = Long.parseLong(f2);
                this.y = 2;
                return;
            }
            this.C = new BigInteger(f2);
            this.y = 4;
        } catch (NumberFormatException e2) {
            a("Malformed numeric value '" + f2 + "'", e2);
        }
    }

    /* access modifiers changed from: protected */
    public void J() throws IOException, ov {
        if ((this.y & 2) != 0) {
            int i2 = (int) this.A;
            if (((long) i2) != this.A) {
                d("Numeric value (" + k() + ") out of range of int");
            }
            this.z = i2;
        } else if ((this.y & 4) != 0) {
            this.z = this.C.intValue();
        } else if ((this.y & 8) != 0) {
            if (this.B < -2.147483648E9d || this.B > 2.147483647E9d) {
                O();
            }
            this.z = (int) this.B;
        } else if ((this.y & 16) != 0) {
            if (w.compareTo(this.D) > 0 || x.compareTo(this.D) < 0) {
                O();
            }
            this.z = this.D.intValue();
        } else {
            U();
        }
        this.y |= 1;
    }

    /* access modifiers changed from: protected */
    public void K() throws IOException, ov {
        if ((this.y & 1) != 0) {
            this.A = (long) this.z;
        } else if ((this.y & 4) != 0) {
            this.A = this.C.longValue();
        } else if ((this.y & 8) != 0) {
            if (this.B < -9.223372036854776E18d || this.B > 9.223372036854776E18d) {
                P();
            }
            this.A = (long) this.B;
        } else if ((this.y & 16) != 0) {
            if (u.compareTo(this.D) > 0 || v.compareTo(this.D) < 0) {
                P();
            }
            this.A = this.D.longValue();
        } else {
            U();
        }
        this.y |= 2;
    }

    /* access modifiers changed from: protected */
    public void L() throws IOException, ov {
        if ((this.y & 16) != 0) {
            this.C = this.D.toBigInteger();
        } else if ((this.y & 2) != 0) {
            this.C = BigInteger.valueOf(this.A);
        } else if ((this.y & 1) != 0) {
            this.C = BigInteger.valueOf((long) this.z);
        } else if ((this.y & 8) != 0) {
            this.C = BigDecimal.valueOf(this.B).toBigInteger();
        } else {
            U();
        }
        this.y |= 4;
    }

    /* access modifiers changed from: protected */
    public void M() throws IOException, ov {
        if ((this.y & 16) != 0) {
            this.B = this.D.doubleValue();
        } else if ((this.y & 4) != 0) {
            this.B = this.C.doubleValue();
        } else if ((this.y & 2) != 0) {
            this.B = (double) this.A;
        } else if ((this.y & 1) != 0) {
            this.B = (double) this.z;
        } else {
            U();
        }
        this.y |= 8;
    }

    /* access modifiers changed from: protected */
    public void N() throws IOException, ov {
        if ((this.y & 8) != 0) {
            this.D = new BigDecimal(k());
        } else if ((this.y & 4) != 0) {
            this.D = new BigDecimal(this.C);
        } else if ((this.y & 2) != 0) {
            this.D = BigDecimal.valueOf(this.A);
        } else if ((this.y & 1) != 0) {
            this.D = BigDecimal.valueOf((long) this.z);
        } else {
            U();
        }
        this.y |= 16;
    }

    /* access modifiers changed from: protected */
    public void a(int i2, String str) throws ov {
        String str2 = "Unexpected character (" + c(i2) + ") in numeric value";
        if (str != null) {
            str2 = str2 + ": " + str;
        }
        d(str2);
    }

    /* access modifiers changed from: protected */
    public void b(String str) throws ov {
        d("Invalid numeric value: " + str);
    }

    /* access modifiers changed from: protected */
    public void O() throws IOException, ov {
        d("Numeric value (" + k() + ") out of range of int (" + Integer.MIN_VALUE + " - " + ((int) PHAsyncRequest.INFINITE_REDIRECTS) + ")");
    }

    /* access modifiers changed from: protected */
    public void P() throws IOException, ov {
        d("Numeric value (" + k() + ") out of range of long (" + Long.MIN_VALUE + " - " + Long.MAX_VALUE + ")");
    }

    /* access modifiers changed from: protected */
    public char Q() throws IOException, ov {
        throw new UnsupportedOperationException();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.ph.a(com.flurry.android.monolithic.sdk.impl.on, int, int):java.lang.IllegalArgumentException
     arg types: [com.flurry.android.monolithic.sdk.impl.on, char, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.ph.a(com.flurry.android.monolithic.sdk.impl.on, char, int):int
      com.flurry.android.monolithic.sdk.impl.pi.a(java.lang.String, com.flurry.android.monolithic.sdk.impl.afq, com.flurry.android.monolithic.sdk.impl.on):void
      com.flurry.android.monolithic.sdk.impl.ph.a(com.flurry.android.monolithic.sdk.impl.on, int, int):java.lang.IllegalArgumentException */
    /* access modifiers changed from: protected */
    public final int a(on onVar, char c, int i2) throws IOException, ov {
        if (c != '\\') {
            throw a(onVar, (int) c, i2);
        }
        char Q = Q();
        if (Q <= ' ' && i2 == 0) {
            return -1;
        }
        int b = onVar.b(Q);
        if (b >= 0) {
            return b;
        }
        throw a(onVar, (int) Q, i2);
    }

    /* access modifiers changed from: protected */
    public IllegalArgumentException a(on onVar, int i2, int i3) throws IllegalArgumentException {
        return a(onVar, i2, i3, (String) null);
    }

    /* access modifiers changed from: protected */
    public IllegalArgumentException a(on onVar, int i2, int i3, String str) throws IllegalArgumentException {
        String str2;
        if (i2 <= 32) {
            str2 = "Illegal white space character (code 0x" + Integer.toHexString(i2) + ") as character #" + (i3 + 1) + " of 4-char base64 unit: can only used between units";
        } else if (onVar.a(i2)) {
            str2 = "Unexpected padding character ('" + onVar.b() + "') as character #" + (i3 + 1) + " of 4-char base64 unit: padding only legal as 3rd or 4th character";
        } else if (!Character.isDefined(i2) || Character.isISOControl(i2)) {
            str2 = "Illegal character (code 0x" + Integer.toHexString(i2) + ") in base64 content";
        } else {
            str2 = "Illegal character '" + ((char) i2) + "' (code 0x" + Integer.toHexString(i2) + ") in base64 content";
        }
        if (str != null) {
            str2 = str2 + ": " + str;
        }
        return new IllegalArgumentException(str2);
    }
}
