package com.flurry.android.monolithic.sdk.impl;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.widget.Toast;
import com.flurry.android.AdCreative;
import com.flurry.android.FlurryFullscreenTakeoverActivity;
import com.flurry.android.impl.ads.FlurryAdModule;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.http.HttpResponse;

public class bi implements ci {
    /* access modifiers changed from: private */
    public static final String d = bi.class.getSimpleName();
    private static int e = 0;
    FlurryAdModule a;
    ce b;
    ck c;
    private boolean f = true;

    public bi(FlurryAdModule flurryAdModule) {
        this.a = flurryAdModule;
        this.c = new ck(flurryAdModule);
        this.b = ce.a();
    }

    public void a() {
        this.f = e((String) null);
    }

    public int a(i iVar) {
        return this.a.b().a().b(iVar.c.d.b().toString());
    }

    public void a(i iVar, cj cjVar, int i) {
        String str = null;
        if (iVar.c != null) {
            str = iVar.c.a;
        }
        ja.a(3, d, "performAction:action=" + iVar.a + ",params=" + iVar.b + ",triggering event=" + str);
        String str2 = iVar.a;
        if (i > 10) {
            ja.a(5, d, "Maximum depth for event/action loop exceeded when performing action:" + str2 + "," + iVar.b + ",triggered by:" + str);
        } else if (str2.equals("directOpen")) {
            b(iVar);
        } else if (str2.equals("delete")) {
            c(iVar);
        } else if (str2.equals("processRedirect")) {
            d(iVar);
        } else if (str2.equals("verifyUrl")) {
            b(iVar, cjVar, i);
        } else if (str2.equals("launchPackage")) {
            e(iVar);
        } else if (str2.equals("sendUrlAsync")) {
            f(iVar);
        } else if (str2.equals("sendAdLogs")) {
            g(iVar);
        } else if (str2.equals("logEvent")) {
            h(iVar);
        } else if (str2.equals("nextFrame")) {
            i(iVar);
        } else if (str2.equals("nextAdUnit")) {
            a(iVar, i);
        } else if (str2.equals("checkCap")) {
            c(iVar, cjVar, i);
        } else if (str2.equals("updateViewCount")) {
            j(iVar);
        } else {
            ja.a(5, d, "Unknown action:" + str2 + ",triggered by:" + str);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(i iVar) {
        Context context = iVar.c.c;
        m mVar = iVar.c.e;
        AdUnit adUnit = iVar.c.d;
        String obj = adUnit.b().toString();
        if (iVar.b.containsKey(FlurryFullscreenTakeoverActivity.EXTRA_KEY_URL)) {
            String str = iVar.b.get(FlurryFullscreenTakeoverActivity.EXTRA_KEY_URL);
            if (str.startsWith("market://")) {
                a(context, str, obj);
            } else if ("true".equals(iVar.b.get("native"))) {
                ja.a(2, d, "Explictly instructed to use native browser");
                b(context, a(mVar, adUnit, iVar, str), obj);
            } else {
                Intent intent = new Intent(ia.a().b(), FlurryFullscreenTakeoverActivity.class);
                intent.putExtra(FlurryFullscreenTakeoverActivity.EXTRA_KEY_URL, str);
                if (je.a(intent)) {
                    a(context, intent, obj);
                    return;
                }
                ja.a(6, d, "Can't start FlurryFullscreenTakeoverActivity, was it declared in the manifest? Falling back to default browser");
                b(context, str, obj);
            }
        } else {
            ja.a(6, d, "failed to perform directOpen action: no url in " + iVar.c.a);
        }
    }

    /* access modifiers changed from: package-private */
    public void c(i iVar) {
        int i;
        String obj = iVar.c.d.b().toString();
        if (iVar.b.containsKey("count")) {
            String str = iVar.b.get("count");
            try {
                i = Integer.parseInt(str);
            } catch (NumberFormatException e2) {
                ja.a(6, d, "caught NumberFormatException with count parameter in deleteAds:" + str);
                i = -1;
            }
            this.a.b().a(obj, i);
        } else if (iVar.b.containsKey("groupId")) {
            this.a.b().a(obj, iVar.b.get("groupId"));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.bi.a(android.content.Context, java.lang.String, boolean, com.flurry.android.impl.ads.avro.protocol.v6.AdUnit):void
     arg types: [android.content.Context, java.lang.String, int, com.flurry.android.impl.ads.avro.protocol.v6.AdUnit]
     candidates:
      com.flurry.android.monolithic.sdk.impl.bi.a(java.lang.String, int, int, boolean):org.apache.http.HttpResponse
      com.flurry.android.monolithic.sdk.impl.bi.a(com.flurry.android.monolithic.sdk.impl.m, com.flurry.android.impl.ads.avro.protocol.v6.AdUnit, com.flurry.android.monolithic.sdk.impl.i, java.lang.String):java.lang.String
      com.flurry.android.monolithic.sdk.impl.bi.a(android.content.Context, java.lang.String, boolean, com.flurry.android.impl.ads.avro.protocol.v6.AdUnit):void */
    /* access modifiers changed from: package-private */
    public void d(i iVar) {
        Context context = iVar.c.c;
        m mVar = iVar.c.e;
        AdUnit adUnit = iVar.c.d;
        String obj = adUnit.b().toString();
        if (iVar.b.containsKey(FlurryFullscreenTakeoverActivity.EXTRA_KEY_URL)) {
            String str = iVar.b.get(FlurryFullscreenTakeoverActivity.EXTRA_KEY_URL);
            if ("true".equals(iVar.b.get("native"))) {
                ja.a(2, d, "Explictly instructed to use native browser in pr.");
                b(context, a(mVar, adUnit, iVar, str), obj);
            } else if (str.startsWith("http")) {
                b(context, a(mVar, adUnit, iVar, str), true, adUnit);
            } else {
                a(context, str, false, adUnit);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(i iVar, cj cjVar, int i) {
        Context context = iVar.c.c;
        m mVar = iVar.c.e;
        AdUnit adUnit = iVar.c.d;
        if (iVar.b.containsKey(FlurryFullscreenTakeoverActivity.EXTRA_KEY_URL)) {
            cjVar.a(new bh(a(iVar.b.get(FlurryFullscreenTakeoverActivity.EXTRA_KEY_URL)) ? "urlVerified" : "urlNotVerified", Collections.emptyMap(), context, adUnit, mVar, iVar.c.f), this, i + 1);
        }
    }

    /* access modifiers changed from: package-private */
    public void e(i iVar) {
        Context context = iVar.c.c;
        AdUnit adUnit = iVar.c.d;
        if (iVar.b.containsKey("package")) {
            a(context, iVar.b.get("package"), adUnit);
        }
    }

    /* access modifiers changed from: package-private */
    public void f(i iVar) {
        m mVar = iVar.c.e;
        AdUnit adUnit = iVar.c.d;
        if (iVar.b.containsKey(FlurryFullscreenTakeoverActivity.EXTRA_KEY_URL)) {
            c(a(mVar, adUnit, iVar, iVar.b.get(FlurryFullscreenTakeoverActivity.EXTRA_KEY_URL)));
        }
    }

    /* access modifiers changed from: package-private */
    public void g(i iVar) {
        this.a.w();
    }

    /* access modifiers changed from: package-private */
    public void h(i iVar) {
        boolean z = iVar.b.containsKey("__sendToServer") && iVar.b.get("__sendToServer").equals("true");
        iVar.b.remove("__sendToServer");
        this.a.a(iVar.c.e, iVar.c.a, z, iVar.b);
    }

    /* access modifiers changed from: package-private */
    public void i(i iVar) {
    }

    /* access modifiers changed from: package-private */
    public void a(i iVar, int i) {
        e = a(iVar);
        if (i > e) {
            ja.a(5, d, "Maximum depth for event/action loop exceeded when performing next AdUnit:");
            return;
        }
        Context context = iVar.c.c;
        AdUnit adUnit = iVar.c.d;
        String obj = adUnit.b().toString();
        boolean containsKey = iVar.b.containsKey("delay");
        boolean a2 = a(adUnit, iVar.c.f);
        long j = 30;
        if (containsKey) {
            try {
                j = Long.parseLong(iVar.b.get("delay"));
            } catch (NumberFormatException e2) {
                ja.a(6, d, "caught NumberFormatException with delay parameter in nextAdUnit:" + iVar.b.get("delay"));
            }
        }
        an a3 = this.a.b().a(obj);
        if (a3 != null && containsKey && a2) {
            a3.a(1000 * j);
        } else if (a3 != null) {
            a3.post(new bj(this, context, obj, a3));
        } else if (iVar.c.a.equals("renderFailed")) {
            this.a.b().b(context, obj);
        } else {
            this.a.a(context, this.a.b().g(obj));
        }
    }

    /* access modifiers changed from: package-private */
    public void c(i iVar, cj cjVar, int i) {
        String str;
        Context context = iVar.c.c;
        m mVar = iVar.c.e;
        AdUnit adUnit = iVar.c.d;
        if (iVar.b.containsKey("idHash")) {
            String str2 = iVar.b.get("idHash");
            cd a2 = this.b.a(str2);
            if (a2 != null && this.b.a(a2.h())) {
                ja.a(4, d, "Discarding expired frequency cap info for idHash=" + str2);
                this.b.b(str2);
                a2 = null;
            }
            if (a2 == null || a2.c() < a2.e()) {
                str = "capNotExhausted";
            } else {
                ja.a(4, d, "Frequency cap exhausted for idHash=" + str2);
                str = "capExhausted";
            }
            cjVar.a(new bh(str, Collections.emptyMap(), context, adUnit, mVar, iVar.c.f), this, i + 1);
        }
    }

    /* access modifiers changed from: package-private */
    public void j(i iVar) {
        cd a2;
        if (iVar.b.containsKey("idHash") && (a2 = this.b.a(iVar.b.get("idHash"))) != null) {
            a2.d();
            ja.a(4, d, "updateViewCount:idHash=" + a2.b() + ",newCap=" + a2.e() + ",prevCap=" + a2.f() + ",views=" + a2.c());
            if (a2.c() > a2.e()) {
                ja.a(6, d, "FlurryAdAction: !! rendering a capped object: " + a2.b());
            }
        }
    }

    public void a(Context context, String str, String str2) {
        if (!str.startsWith("market://details?id=")) {
            ja.a(5, d, "Unexpected Google Play url scheme: " + str);
        } else if (!this.f) {
            b(context, "https://market.android.com/details?id=" + str.substring("market://details?id=".length()), str2);
        } else if (!b(context, str, str2)) {
            ja.a(6, d, "Cannot launch Google Play url " + str);
        }
    }

    public boolean b(Context context, String str, String str2) {
        return a(context, new Intent("android.intent.action.VIEW").setData(Uri.parse(str)), str2);
    }

    /* access modifiers changed from: package-private */
    public boolean a(String str) {
        Intent launchIntentForPackage = ia.a().c().getLaunchIntentForPackage(str);
        return launchIntentForPackage != null && je.a(launchIntentForPackage);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.bi.a(android.content.Context, java.lang.String, boolean, com.flurry.android.impl.ads.avro.protocol.v6.AdUnit):void
     arg types: [android.content.Context, java.lang.String, int, com.flurry.android.impl.ads.avro.protocol.v6.AdUnit]
     candidates:
      com.flurry.android.monolithic.sdk.impl.bi.a(java.lang.String, int, int, boolean):org.apache.http.HttpResponse
      com.flurry.android.monolithic.sdk.impl.bi.a(com.flurry.android.monolithic.sdk.impl.m, com.flurry.android.impl.ads.avro.protocol.v6.AdUnit, com.flurry.android.monolithic.sdk.impl.i, java.lang.String):java.lang.String
      com.flurry.android.monolithic.sdk.impl.bi.a(android.content.Context, java.lang.String, boolean, com.flurry.android.impl.ads.avro.protocol.v6.AdUnit):void */
    /* access modifiers changed from: package-private */
    public void a(Context context, String str, AdUnit adUnit) {
        Intent launchIntentForPackage = ia.a().c().getLaunchIntentForPackage(str);
        if (launchIntentForPackage == null || !je.a(launchIntentForPackage)) {
            a(context, "https://play.google.com/store/apps/details?id=" + str, false, adUnit);
        } else {
            a(context, launchIntentForPackage, adUnit.b().toString());
        }
    }

    public boolean a(Context context, Intent intent, String str) {
        if (context == null || intent == null || str == null || !je.a(intent)) {
            return false;
        }
        try {
            context.startActivity(a(intent, str));
            return true;
        } catch (ActivityNotFoundException e2) {
            ja.a(6, d, "Cannot launch Activity", e2);
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public Intent a(Intent intent, String str) {
        Intent intent2;
        if (je.b(intent)) {
            intent2 = new Intent(intent);
        } else {
            intent2 = new Intent(ia.a().b(), FlurryFullscreenTakeoverActivity.class);
            intent2.putExtra(FlurryFullscreenTakeoverActivity.EXTRA_KEY_TARGETINTENT, intent);
        }
        if (intent2 != null) {
            intent2.putExtra(FlurryFullscreenTakeoverActivity.EXTRA_KEY_ADSPACENAME, str);
        }
        return intent2;
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, String str, boolean z, AdUnit adUnit) {
        this.a.a(new bk(this, str, adUnit, context, z));
    }

    public String a(m mVar, AdUnit adUnit, i iVar, String str) {
        Pattern compile = Pattern.compile(".*?(%\\{\\w+\\}).*?");
        Matcher matcher = compile.matcher(str);
        String str2 = str;
        while (matcher.matches()) {
            String a2 = this.c.a(mVar, adUnit, iVar, str2, matcher.group(1));
            matcher = compile.matcher(a2);
            str2 = a2;
        }
        return str2;
    }

    /* access modifiers changed from: package-private */
    public void b(Context context, String str, boolean z, AdUnit adUnit) {
        this.a.a(new bl(this, str, context, z, adUnit));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.bi.a(java.lang.String, int, int, boolean):org.apache.http.HttpResponse
     arg types: [java.lang.String, int, int, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.bi.a(com.flurry.android.monolithic.sdk.impl.m, com.flurry.android.impl.ads.avro.protocol.v6.AdUnit, com.flurry.android.monolithic.sdk.impl.i, java.lang.String):java.lang.String
      com.flurry.android.monolithic.sdk.impl.bi.a(android.content.Context, java.lang.String, boolean, com.flurry.android.impl.ads.avro.protocol.v6.AdUnit):void
      com.flurry.android.monolithic.sdk.impl.bi.a(java.lang.String, int, int, boolean):org.apache.http.HttpResponse */
    /* access modifiers changed from: package-private */
    public String b(String str) {
        int i = 0;
        String str2 = str;
        while (i < 5) {
            if (Uri.parse(str2).getScheme().equals("http")) {
                if (!c() || b()) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e2) {
                        ja.a(3, d, e2.getMessage());
                    }
                } else {
                    HttpResponse a2 = a(str2, 10000, 15000, false);
                    if (a2 != null) {
                        int statusCode = a2.getStatusLine().getStatusCode();
                        if (ja.c() <= 3) {
                            Toast.makeText(ia.a().b(), "fFU HTTP Response Code: " + statusCode, 0).show();
                        }
                        if (statusCode == 200) {
                            ja.a(3, d, "Redirect URL found for: " + str2);
                            return str2;
                        } else if (statusCode < 300 || statusCode >= 400) {
                            ja.a(3, d, "Bad Response status code: " + statusCode);
                            return null;
                        } else {
                            ja.a(3, d, "NumRedirects: " + (i + 1));
                            if (a2.containsHeader("Location")) {
                                str2 = a2.getFirstHeader("Location").getValue();
                            }
                        }
                    } else {
                        continue;
                    }
                }
                i++;
            } else if (a(str2, "android.intent.action.VIEW")) {
                return str2;
            } else {
                if (TextUtils.isEmpty(str2) || !str2.startsWith("market://")) {
                    return null;
                }
                return str2;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void c(String str) {
        ja.a(3, d, "url after is: " + str);
        this.a.a(new bm(this, str));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.bi.a(java.lang.String, int, int, boolean):org.apache.http.HttpResponse
     arg types: [java.lang.String, int, int, int]
     candidates:
      com.flurry.android.monolithic.sdk.impl.bi.a(com.flurry.android.monolithic.sdk.impl.m, com.flurry.android.impl.ads.avro.protocol.v6.AdUnit, com.flurry.android.monolithic.sdk.impl.i, java.lang.String):java.lang.String
      com.flurry.android.monolithic.sdk.impl.bi.a(android.content.Context, java.lang.String, boolean, com.flurry.android.impl.ads.avro.protocol.v6.AdUnit):void
      com.flurry.android.monolithic.sdk.impl.bi.a(java.lang.String, int, int, boolean):org.apache.http.HttpResponse */
    /* access modifiers changed from: package-private */
    public boolean d(String str) {
        for (int i = 0; i < 5; i++) {
            ja.a(6, d, " sendURLUntilSuccess URL: " + str);
            if (!c() || b()) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e2) {
                    ja.a(3, d, e2.getMessage());
                }
            } else {
                HttpResponse a2 = a(str, 10000, 15000, true);
                int statusCode = a2.getStatusLine().getStatusCode();
                if (ja.c() <= 3) {
                    Toast.makeText(ia.a().b(), "sUUS HTTP Response Code: " + statusCode, 0).show();
                }
                if (a2 != null && statusCode == 200) {
                    ja.a(3, d, "URL hit succeeded for: " + str);
                    return true;
                }
            }
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x007e  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:16:0x0057=Splitter:B:16:0x0057, B:9:0x002e=Splitter:B:9:0x002e} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static org.apache.http.HttpResponse a(java.lang.String r6, int r7, int r8, boolean r9) {
        /*
            r5 = 0
            org.apache.http.client.methods.HttpGet r0 = new org.apache.http.client.methods.HttpGet     // Catch:{ UnknownHostException -> 0x002c, Exception -> 0x0055, all -> 0x007a }
            r0.<init>(r6)     // Catch:{ UnknownHostException -> 0x002c, Exception -> 0x0055, all -> 0x007a }
            org.apache.http.params.BasicHttpParams r1 = new org.apache.http.params.BasicHttpParams     // Catch:{ UnknownHostException -> 0x002c, Exception -> 0x0055, all -> 0x007a }
            r1.<init>()     // Catch:{ UnknownHostException -> 0x002c, Exception -> 0x0055, all -> 0x007a }
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r1, r7)     // Catch:{ UnknownHostException -> 0x002c, Exception -> 0x0055, all -> 0x007a }
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r1, r8)     // Catch:{ UnknownHostException -> 0x002c, Exception -> 0x0055, all -> 0x007a }
            java.lang.String r2 = "http.protocol.handle-redirects"
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r9)     // Catch:{ UnknownHostException -> 0x002c, Exception -> 0x0055, all -> 0x007a }
            r1.setParameter(r2, r3)     // Catch:{ UnknownHostException -> 0x002c, Exception -> 0x0055, all -> 0x007a }
            org.apache.http.client.HttpClient r1 = com.flurry.android.monolithic.sdk.impl.iz.b(r1)     // Catch:{ UnknownHostException -> 0x002c, Exception -> 0x0055, all -> 0x007a }
            org.apache.http.HttpResponse r0 = r1.execute(r0)     // Catch:{ UnknownHostException -> 0x008a, Exception -> 0x0088 }
            if (r1 == 0) goto L_0x002b
            org.apache.http.conn.ClientConnectionManager r1 = r1.getConnectionManager()
            r1.shutdown()
        L_0x002b:
            return r0
        L_0x002c:
            r0 = move-exception
            r1 = r5
        L_0x002e:
            java.lang.String r2 = com.flurry.android.monolithic.sdk.impl.bi.d     // Catch:{ all -> 0x0086 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0086 }
            r3.<init>()     // Catch:{ all -> 0x0086 }
            java.lang.String r4 = "Unknown host: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0086 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0086 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x0086 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0086 }
            com.flurry.android.monolithic.sdk.impl.ja.a(r2, r0)     // Catch:{ all -> 0x0086 }
            if (r1 == 0) goto L_0x0053
            org.apache.http.conn.ClientConnectionManager r0 = r1.getConnectionManager()
            r0.shutdown()
        L_0x0053:
            r0 = r5
            goto L_0x002b
        L_0x0055:
            r0 = move-exception
            r1 = r5
        L_0x0057:
            java.lang.String r2 = com.flurry.android.monolithic.sdk.impl.bi.d     // Catch:{ all -> 0x0086 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0086 }
            r3.<init>()     // Catch:{ all -> 0x0086 }
            java.lang.String r4 = "Failed to hit URL: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0086 }
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ all -> 0x0086 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0086 }
            com.flurry.android.monolithic.sdk.impl.ja.a(r2, r3, r0)     // Catch:{ all -> 0x0086 }
            if (r1 == 0) goto L_0x0078
            org.apache.http.conn.ClientConnectionManager r0 = r1.getConnectionManager()
            r0.shutdown()
        L_0x0078:
            r0 = r5
            goto L_0x002b
        L_0x007a:
            r0 = move-exception
            r1 = r5
        L_0x007c:
            if (r1 == 0) goto L_0x0085
            org.apache.http.conn.ClientConnectionManager r1 = r1.getConnectionManager()
            r1.shutdown()
        L_0x0085:
            throw r0
        L_0x0086:
            r0 = move-exception
            goto L_0x007c
        L_0x0088:
            r0 = move-exception
            goto L_0x0057
        L_0x008a:
            r0 = move-exception
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.monolithic.sdk.impl.bi.a(java.lang.String, int, int, boolean):org.apache.http.HttpResponse");
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return FlurryAdModule.e();
    }

    /* access modifiers changed from: package-private */
    public boolean a(AdUnit adUnit, int i) {
        if (adUnit == null || adUnit.d().size() <= 0) {
            return false;
        }
        return adUnit.d().get(i).e().e().toString().equals(AdCreative.kFormatBanner);
    }

    /* access modifiers changed from: package-private */
    public boolean e(String str) {
        String str2;
        String packageName = ia.a().b().getPackageName();
        if (str == null) {
            str2 = "market://details?id=" + packageName;
        } else {
            str2 = str;
        }
        return a(str2, "android.intent.action.VIEW");
    }

    /* access modifiers changed from: package-private */
    public boolean a(String str, String str2) {
        Intent intent = new Intent(str2);
        intent.setData(Uri.parse(str));
        return je.a(intent);
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return jc.a().c();
    }
}
