package com.flurry.android.monolithic.sdk.impl;

import com.flurry.android.FlurryFullscreenTakeoverActivity;
import com.flurry.android.impl.appcloud.AppCloudModule;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.json.JSONTokener;

public class hf extends he {
    protected static final String a = hf.class.getSimpleName();

    public hf(HashMap<String, Object> hashMap) {
        super(hashMap);
    }

    public void a() {
        String str;
        JSONObject jSONObject;
        b("PostOperation Thread");
        try {
            this.f = (fr) this.h.get("del");
            this.g = (fs) this.h.get("del_internal");
            String a2 = a((String) this.h.get(FlurryFullscreenTakeoverActivity.EXTRA_KEY_URL));
            HttpPost httpPost = new HttpPost(a2);
            ja.a(4, a, "Put operation URL = " + a2);
            if (AppCloudModule.c()) {
                this.d = iz.a(new BasicHttpParams());
                this.e = new BasicHttpContext();
                this.e.setAttribute("http.cookie-store", new BasicCookieStore());
                String str2 = (String) this.d.execute(httpPost, new BasicResponseHandler(), this.e);
                JSONTokener jSONTokener = new JSONTokener(str2);
                jSONTokener.skipPast("(");
                JSONObject jSONObject2 = (JSONObject) jSONTokener.nextValue();
                str = str2;
                jSONObject = jSONObject2;
            } else {
                this.d = iz.b(new BasicHttpParams());
                a(httpPost, this.h);
                httpPost.setEntity(a((List) this.h.get("data")));
                String str3 = EntityUtils.toString(this.d.execute(this.c, httpPost, new BasicHttpContext()).getEntity()).toString();
                str = str3;
                jSONObject = new JSONObject(str3);
            }
            ja.a(4, a, "responseText = " + str);
            a(jSONObject);
            if (this.d != null) {
                this.d.getConnectionManager().shutdown();
            }
        } catch (Exception e) {
            ja.a(6, a, "Exception during communication with server: ", e);
            a((JSONObject) null);
            if (this.d != null) {
                this.d.getConnectionManager().shutdown();
            }
        } catch (Throwable th) {
            if (this.d != null) {
                this.d.getConnectionManager().shutdown();
            }
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public void a(JSONObject jSONObject) {
        try {
            fq fqVar = new fq(jSONObject);
            if (this.g != null) {
                this.g.a(fqVar, this.f, gx.METHOD_POST, this.h);
            } else {
                this.f.a(fqVar);
            }
        } catch (Exception e) {
            ja.a(6, a, "Exception in onPostExecute: ", e);
        }
    }

    private oh a(List<NameValuePair> list) throws Exception {
        oh ohVar = new oh(oe.BROWSER_COMPATIBLE);
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getName().equals("file")) {
                String[] split = list.get(i).getValue().split("/");
                ohVar.a("file", new ol(new File(list.get(i).getValue()), split[split.length - 1], "application/octet-stream", "utf-8"));
            } else {
                String name = list.get(i).getName();
                if (!(name == "filename" || name == "file")) {
                    ohVar.a(name, new om(list.get(i).getValue()));
                }
            }
        }
        return ohVar;
    }
}
