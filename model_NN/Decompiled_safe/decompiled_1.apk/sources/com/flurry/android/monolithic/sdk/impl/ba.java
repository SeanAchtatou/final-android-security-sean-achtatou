package com.flurry.android.monolithic.sdk.impl;

import android.webkit.WebChromeClient;

final class ba implements ax {
    final /* synthetic */ ar a;
    private WebChromeClient.CustomViewCallback b;

    public ba(ar arVar, WebChromeClient.CustomViewCallback customViewCallback) {
        this.a = arVar;
        this.b = customViewCallback;
    }

    public void a() {
        if (this.b != null) {
            this.b.onCustomViewHidden();
        }
    }
}
