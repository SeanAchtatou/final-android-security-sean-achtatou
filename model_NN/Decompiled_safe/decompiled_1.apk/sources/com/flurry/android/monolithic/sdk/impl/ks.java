package com.flurry.android.monolithic.sdk.impl;

import java.util.AbstractList;
import java.util.Iterator;
import v2.com.playhaven.model.PHContent;

public class ks<T> extends AbstractList<T> implements ko<T>, Comparable<ko<T>> {
    private static final Object[] a = new Object[0];
    private final ji b;
    /* access modifiers changed from: private */
    public int c;
    /* access modifiers changed from: private */
    public Object[] d = a;

    public ks(int i, ji jiVar) {
        if (jiVar == null || !kj.ARRAY.equals(jiVar.a())) {
            throw new jg("Not an array schema: " + jiVar);
        }
        this.b = jiVar;
        if (i != 0) {
            this.d = new Object[i];
        }
    }

    public ji a() {
        return this.b;
    }

    public int size() {
        return this.c;
    }

    public void clear() {
        this.c = 0;
    }

    public Iterator<T> iterator() {
        return new kt(this);
    }

    public T get(int i) {
        if (i < this.c) {
            return this.d[i];
        }
        throw new IndexOutOfBoundsException("Index " + i + " out of bounds.");
    }

    public boolean add(T t) {
        if (this.c == this.d.length) {
            Object[] objArr = new Object[(((this.c * 3) / 2) + 1)];
            System.arraycopy(this.d, 0, objArr, 0, this.c);
            this.d = objArr;
        }
        Object[] objArr2 = this.d;
        int i = this.c;
        this.c = i + 1;
        objArr2[i] = t;
        return true;
    }

    public void add(int i, T t) {
        if (i > this.c || i < 0) {
            throw new IndexOutOfBoundsException("Index " + i + " out of bounds.");
        }
        if (this.c == this.d.length) {
            Object[] objArr = new Object[(((this.c * 3) / 2) + 1)];
            System.arraycopy(this.d, 0, objArr, 0, this.c);
            this.d = objArr;
        }
        System.arraycopy(this.d, i, this.d, i + 1, this.c - i);
        this.d[i] = t;
        this.c++;
    }

    public T set(int i, T t) {
        if (i >= this.c) {
            throw new IndexOutOfBoundsException("Index " + i + " out of bounds.");
        }
        T t2 = this.d[i];
        this.d[i] = t;
        return t2;
    }

    public T remove(int i) {
        if (i >= this.c) {
            throw new IndexOutOfBoundsException("Index " + i + " out of bounds.");
        }
        T t = this.d[i];
        this.c--;
        System.arraycopy(this.d, i + 1, this.d, i, this.c - i);
        this.d[this.c] = null;
        return t;
    }

    public T b() {
        if (this.c < this.d.length) {
            return this.d[this.c];
        }
        return null;
    }

    /* renamed from: a */
    public int compareTo(ko<T> koVar) {
        return kq.a().a(this, koVar, a());
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("[");
        int i = 0;
        Iterator it = iterator();
        while (it.hasNext()) {
            Object next = it.next();
            stringBuffer.append(next == null ? PHContent.PARCEL_NULL : next.toString());
            i++;
            if (i < size()) {
                stringBuffer.append(", ");
            }
        }
        stringBuffer.append("]");
        return stringBuffer.toString();
    }
}
