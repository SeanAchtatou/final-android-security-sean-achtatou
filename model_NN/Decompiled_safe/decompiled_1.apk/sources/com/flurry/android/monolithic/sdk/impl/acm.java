package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;

public final class acm extends abq<AtomicLong> {
    public acm() {
        super(AtomicLong.class, false);
    }

    public void a(AtomicLong atomicLong, or orVar, ru ruVar) throws IOException, oq {
        orVar.a(atomicLong.get());
    }
}
