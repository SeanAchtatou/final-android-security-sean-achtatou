package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public abstract class sw implements qc {
    protected final String a;
    protected final afm b;
    protected final ado c;
    protected qu<Object> d;
    protected rw e;
    protected tb f;
    protected String g;
    protected int h = -1;

    public abstract sw a(qu<Object> quVar);

    public abstract void a(ow owVar, qm qmVar, Object obj) throws IOException, oz;

    public abstract void a(Object obj, Object obj2) throws IOException;

    public abstract xk b();

    protected sw(String str, afm afm, rw rwVar, ado ado) {
        if (str == null || str.length() == 0) {
            this.a = "";
        } else {
            this.a = afv.a.a(str);
        }
        this.b = afm;
        this.c = ado;
        this.e = rwVar;
    }

    protected sw(sw swVar) {
        this.a = swVar.a;
        this.b = swVar.b;
        this.c = swVar.c;
        this.d = swVar.d;
        this.e = swVar.e;
        this.f = swVar.f;
        this.g = swVar.g;
        this.h = swVar.h;
    }

    protected sw(sw swVar, qu<Object> quVar) {
        this.a = swVar.a;
        this.b = swVar.b;
        this.c = swVar.c;
        this.e = swVar.e;
        this.g = swVar.g;
        this.h = swVar.h;
        this.d = quVar;
        if (quVar == null) {
            this.f = null;
            return;
        }
        Object b2 = quVar.b();
        this.f = b2 == null ? null : new tb(this.b, b2);
    }

    public void a(String str) {
        this.g = str;
    }

    public void a(int i) {
        if (this.h != -1) {
            throw new IllegalStateException("Property '" + c() + "' already had index (" + this.h + "), trying to assign " + i);
        }
        this.h = i;
    }

    public final String c() {
        return this.a;
    }

    public afm a() {
        return this.b;
    }

    @Deprecated
    public String d() {
        return this.a;
    }

    public String e() {
        return this.g;
    }

    public boolean f() {
        return this.d != null;
    }

    public boolean g() {
        return this.e != null;
    }

    public qu<Object> h() {
        return this.d;
    }

    public rw i() {
        return this.e;
    }

    public int j() {
        return this.h;
    }

    public Object k() {
        return null;
    }

    public final Object a(ow owVar, qm qmVar) throws IOException, oz {
        if (owVar.e() == pb.VALUE_NULL) {
            if (this.f == null) {
                return null;
            }
            return this.f.a(qmVar);
        } else if (this.e != null) {
            return this.d.a(owVar, qmVar, this.e);
        } else {
            return this.d.a(owVar, qmVar);
        }
    }

    /* access modifiers changed from: protected */
    public void a(Exception exc, Object obj) throws IOException {
        if (exc instanceof IllegalArgumentException) {
            String name = obj == null ? "[NULL]" : obj.getClass().getName();
            StringBuilder append = new StringBuilder("Problem deserializing property '").append(d());
            append.append("' (expected type: ").append(a());
            append.append("; actual type: ").append(name).append(")");
            String message = exc.getMessage();
            if (message != null) {
                append.append(", problem: ").append(message);
            } else {
                append.append(" (no error message provided)");
            }
            throw new qw(append.toString(), null, exc);
        }
        a(exc);
    }

    /* access modifiers changed from: protected */
    public IOException a(Exception exc) throws IOException {
        if (exc instanceof IOException) {
            throw ((IOException) exc);
        } else if (exc instanceof RuntimeException) {
            throw ((RuntimeException) exc);
        } else {
            Throwable th = exc;
            while (th.getCause() != null) {
                th = th.getCause();
            }
            throw new qw(th.getMessage(), null, th);
        }
    }

    public String toString() {
        return "[property '" + c() + "']";
    }
}
