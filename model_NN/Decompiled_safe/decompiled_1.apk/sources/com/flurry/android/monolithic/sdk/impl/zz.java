package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

@rz
public final class zz extends abm<Boolean> {
    final boolean a;

    public zz(boolean z) {
        super(Boolean.class);
        this.a = z;
    }

    public void a(Boolean bool, or orVar, ru ruVar) throws IOException, oq {
        orVar.a(bool.booleanValue());
    }
}
