package com.flurry.android.monolithic.sdk.impl;

import java.lang.reflect.Modifier;

public abstract class afm {
    protected final Class<?> d;
    protected final int e;
    protected Object f = null;
    protected Object g = null;

    /* access modifiers changed from: protected */
    public abstract afm a(Class<?> cls);

    public abstract afm b(Class<?> cls);

    public abstract afm c(Class<?> cls);

    public abstract afm e(Object obj);

    public abstract boolean equals(Object obj);

    public abstract afm f(Object obj);

    public abstract boolean f();

    public abstract String m();

    public abstract String toString();

    protected afm(Class<?> cls, int i) {
        this.d = cls;
        this.e = cls.getName().hashCode() + i;
    }

    public afm d(Object obj) {
        j(obj);
        return this;
    }

    @Deprecated
    public void j(Object obj) {
        if (obj == null || this.f == null) {
            this.f = obj;
            return;
        }
        throw new IllegalStateException("Trying to reset value handler for type [" + toString() + "]; old handler of type " + this.f.getClass().getName() + ", new handler of type " + obj.getClass().getName());
    }

    public afm f(Class<?> cls) {
        if (cls == this.d) {
            return this;
        }
        a(cls, this.d);
        afm a = a(cls);
        if (this.f != a.n()) {
            a = a.d(this.f);
        }
        if (this.g != a.o()) {
            return a.f(this.g);
        }
        return a;
    }

    public afm g(Class<?> cls) {
        if (cls == this.d) {
            return this;
        }
        afm a = a(cls);
        if (this.f != a.n()) {
            a = a.d(this.f);
        }
        if (this.g != a.o()) {
            return a.f(this.g);
        }
        return a;
    }

    public afm h(Class<?> cls) {
        if (cls == this.d) {
            return this;
        }
        a(this.d, cls);
        return i(cls);
    }

    /* access modifiers changed from: protected */
    public afm i(Class<?> cls) {
        return a(cls);
    }

    public final Class<?> p() {
        return this.d;
    }

    public boolean c() {
        return Modifier.isAbstract(this.d.getModifiers());
    }

    public boolean d() {
        if ((this.d.getModifiers() & 1536) == 0) {
            return true;
        }
        if (this.d.isPrimitive()) {
            return true;
        }
        return false;
    }

    public boolean q() {
        return Throwable.class.isAssignableFrom(this.d);
    }

    public boolean b() {
        return false;
    }

    public final boolean r() {
        return this.d.isEnum();
    }

    public final boolean s() {
        return this.d.isInterface();
    }

    public final boolean t() {
        return this.d.isPrimitive();
    }

    public final boolean u() {
        return Modifier.isFinal(this.d.getModifiers());
    }

    public boolean i() {
        return false;
    }

    public boolean j() {
        return false;
    }

    public boolean e() {
        return h() > 0;
    }

    public afm k() {
        return null;
    }

    public afm g() {
        return null;
    }

    public int h() {
        return 0;
    }

    public afm b(int i) {
        return null;
    }

    public String a(int i) {
        return null;
    }

    public <T> T n() {
        return this.f;
    }

    public <T> T o() {
        return this.g;
    }

    /* access modifiers changed from: protected */
    public void a(Class<?> cls, Class<?> cls2) {
        if (!this.d.isAssignableFrom(cls)) {
            throw new IllegalArgumentException("Class " + cls.getName() + " is not assignable to " + this.d.getName());
        }
    }

    public final int hashCode() {
        return this.e;
    }
}
