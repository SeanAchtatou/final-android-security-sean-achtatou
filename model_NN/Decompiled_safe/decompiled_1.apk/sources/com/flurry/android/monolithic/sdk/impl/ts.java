package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;

public abstract class ts {
    public final ts a;
    public final Object b;

    public abstract void a(Object obj) throws IOException, oz;

    protected ts(ts tsVar, Object obj) {
        this.a = tsVar;
        this.b = obj;
    }
}
