package com.flurry.android.monolithic.sdk.impl;

import com.flurry.org.codehaus.jackson.annotate.JsonTypeInfo;
import java.io.IOException;

public abstract class rw {
    public abstract JsonTypeInfo.As a();

    public abstract Object a(ow owVar, qm qmVar) throws IOException, oz;

    public abstract Object b(ow owVar, qm qmVar) throws IOException, oz;

    public abstract String b();

    public abstract Object c(ow owVar, qm qmVar) throws IOException, oz;

    public abstract Object d(ow owVar, qm qmVar) throws IOException, oz;
}
