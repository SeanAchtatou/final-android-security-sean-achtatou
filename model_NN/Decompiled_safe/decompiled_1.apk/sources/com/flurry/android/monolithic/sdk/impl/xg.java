package com.flurry.android.monolithic.sdk.impl;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Type;

public abstract class xg {
    public abstract <A extends Annotation> A a(Class cls);

    public abstract AnnotatedElement a();

    public abstract String b();

    public abstract Type c();

    public abstract Class<?> d();

    protected xg() {
    }

    public final <A extends Annotation> boolean b(Class cls) {
        return a(cls) != null;
    }

    public afm a(adj adj) {
        return adj.a(c());
    }
}
