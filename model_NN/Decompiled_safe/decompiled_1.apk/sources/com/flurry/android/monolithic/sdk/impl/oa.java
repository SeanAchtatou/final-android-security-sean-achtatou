package com.flurry.android.monolithic.sdk.impl;

public class oa {
    private final String a;
    private final ob b;
    private final oj c;

    public oa(String str, oj ojVar) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        } else if (ojVar == null) {
            throw new IllegalArgumentException("Body may not be null");
        } else {
            this.a = str;
            this.c = ojVar;
            this.b = new ob();
            a(ojVar);
            b(ojVar);
            c(ojVar);
        }
    }

    public String a() {
        return this.a;
    }

    public oj b() {
        return this.c;
    }

    public ob c() {
        return this.b;
    }

    public void a(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("Field name may not be null");
        }
        this.b.a(new og(str, str2));
    }

    /* access modifiers changed from: protected */
    public void a(oj ojVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("form-data; name=\"");
        sb.append(a());
        sb.append("\"");
        if (ojVar.b() != null) {
            sb.append("; filename=\"");
            sb.append(ojVar.b());
            sb.append("\"");
        }
        a("Content-Disposition", sb.toString());
    }

    /* access modifiers changed from: protected */
    public void b(oj ojVar) {
        StringBuilder sb = new StringBuilder();
        sb.append(ojVar.a());
        if (ojVar.c() != null) {
            sb.append("; charset=");
            sb.append(ojVar.c());
        }
        a("Content-Type", sb.toString());
    }

    /* access modifiers changed from: protected */
    public void c(oj ojVar) {
        a("Content-Transfer-Encoding", ojVar.d());
    }
}
