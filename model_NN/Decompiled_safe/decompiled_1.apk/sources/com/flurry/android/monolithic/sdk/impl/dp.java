package com.flurry.android.monolithic.sdk.impl;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import com.flurry.android.impl.ads.FlurryAdModule;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;
import com.inmobi.androidsdk.IMAdInterstitial;
import com.inmobi.androidsdk.IMAdInterstitialListener;
import com.inmobi.androidsdk.IMAdRequest;
import java.lang.reflect.Method;

public class dp extends cr {
    /* access modifiers changed from: private */
    public static final String b = dp.class.getSimpleName();
    private final String c;
    private final boolean d;
    private final Method e = g();

    public dp(Context context, FlurryAdModule flurryAdModule, m mVar, AdUnit adUnit, Bundle bundle) {
        super(context, flurryAdModule, mVar, adUnit);
        this.c = bundle.getString("com.flurry.inmobi.MY_APP_ID");
        this.d = bundle.getBoolean("com.flurry.inmobi.test");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.monolithic.sdk.impl.dp.a(com.inmobi.androidsdk.IMAdInterstitial, com.inmobi.androidsdk.IMAdInterstitialListener):void
     arg types: [com.inmobi.androidsdk.IMAdInterstitial, com.flurry.android.monolithic.sdk.impl.dq]
     candidates:
      com.flurry.android.monolithic.sdk.impl.cr.a(java.lang.String, java.util.Map<java.lang.String, java.lang.String>):void
      com.flurry.android.monolithic.sdk.impl.dp.a(com.inmobi.androidsdk.IMAdInterstitial, com.inmobi.androidsdk.IMAdInterstitialListener):void */
    public void a() {
        IMAdInterstitial iMAdInterstitial = new IMAdInterstitial((Activity) b(), this.c);
        a(iMAdInterstitial, (IMAdInterstitialListener) new dq(this));
        IMAdRequest iMAdRequest = new IMAdRequest();
        if (this.d) {
            ja.a(3, b, "InMobi Interstitial set to Test Mode.");
            iMAdRequest.setTestMode(true);
        }
        iMAdInterstitial.loadNewAd(iMAdRequest);
    }

    private void a(IMAdInterstitial iMAdInterstitial, IMAdInterstitialListener iMAdInterstitialListener) {
        if (this.e != null && iMAdInterstitial != null) {
            try {
                this.e.invoke(iMAdInterstitial, iMAdInterstitialListener);
            } catch (Exception e2) {
            }
        }
    }

    private static Method g() {
        for (Method method : IMAdInterstitial.class.getMethods()) {
            String name = method.getName();
            if (name.equals("setIMAdInterstitialListener") || name.equals("setImAdInterstitialListener")) {
                return method;
            }
        }
        return null;
    }
}
