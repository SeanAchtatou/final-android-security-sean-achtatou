package com.flurry.android.monolithic.sdk.impl;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.params.HttpParams;

public final class iz {
    private static SchemeRegistry a;

    private static synchronized SchemeRegistry a() {
        SchemeRegistry schemeRegistry;
        synchronized (iz.class) {
            if (a != null) {
                schemeRegistry = a;
            } else {
                a = new SchemeRegistry();
                a.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
                a.register(new Scheme("https", new ix(), 443));
                schemeRegistry = a;
            }
        }
        return schemeRegistry;
    }

    public static HttpClient a(HttpParams httpParams) {
        return new DefaultHttpClient(httpParams);
    }

    public static HttpClient b(HttpParams httpParams) {
        return new DefaultHttpClient(new SingleClientConnManager(httpParams, a()), httpParams);
    }
}
