package com.flurry.android.monolithic.sdk.impl;

import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.flurry.android.FlurryFullscreenTakeoverActivity;

public final class c implements aw {
    final /* synthetic */ FlurryFullscreenTakeoverActivity a;
    private View b;
    private int c;
    private ax d;
    private FrameLayout e;

    private c(FlurryFullscreenTakeoverActivity flurryFullscreenTakeoverActivity) {
        this.a = flurryFullscreenTakeoverActivity;
    }

    public void a(ar arVar, View view, ax axVar) {
        a(arVar, view, this.a.getRequestedOrientation(), axVar);
    }

    public void a(ar arVar, View view, int i, ax axVar) {
        if (this.b != null) {
            a(arVar);
        }
        this.b = view;
        this.c = this.a.getRequestedOrientation();
        this.d = axVar;
        this.e = new FrameLayout(this.a);
        this.e.setBackgroundColor(-16777216);
        this.e.addView(this.b, new FrameLayout.LayoutParams(-1, -1, 17));
        ((ViewGroup) this.a.getWindow().getDecorView()).addView(this.e, -1, -1);
        this.a.setRequestedOrientation(i);
    }

    public void a(ar arVar) {
        if (this.b != null) {
            ((ViewGroup) this.a.getWindow().getDecorView()).removeView(this.e);
            this.e.removeView(this.b);
            if (this.d != null) {
                this.d.a();
            }
            this.a.setRequestedOrientation(this.c);
            this.d = null;
            this.e = null;
            this.b = null;
        }
    }
}
