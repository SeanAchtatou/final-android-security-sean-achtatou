package com.flurry.android.monolithic.sdk.impl;

import java.io.IOException;
import java.util.Collection;

@rz
public class uf extends ug<Collection<Object>> implements ro {
    protected final afm a;
    protected final qu<Object> b;
    protected final rw c;
    protected final th d;
    protected qu<Object> e;

    public /* bridge */ /* synthetic */ Object a(ow owVar, qm qmVar, Object obj) throws IOException, oz {
        return a(owVar, qmVar, (Collection<Object>) ((Collection) obj));
    }

    public uf(afm afm, qu<Object> quVar, rw rwVar, th thVar) {
        super(afm.p());
        this.a = afm;
        this.b = quVar;
        this.c = rwVar;
        this.d = thVar;
    }

    public void a(qk qkVar, qq qqVar) throws qw {
        if (this.d.i()) {
            afm l = this.d.l();
            if (l == null) {
                throw new IllegalArgumentException("Invalid delegate-creator definition for " + this.a + ": value instantiator (" + this.d.getClass().getName() + ") returned true for 'canCreateUsingDelegate()', but null for 'getDelegateType()'");
            }
            this.e = a(qkVar, qqVar, l, new qd(null, l, null, this.d.o()));
        }
    }

    public qu<Object> d() {
        return this.b;
    }

    /* renamed from: b */
    public Collection<Object> a(ow owVar, qm qmVar) throws IOException, oz {
        if (this.e != null) {
            return (Collection) this.d.a(this.e.a(owVar, qmVar));
        }
        if (owVar.e() == pb.VALUE_STRING) {
            String k = owVar.k();
            if (k.length() == 0) {
                return (Collection) this.d.a(k);
            }
        }
        return a(owVar, qmVar, (Collection<Object>) ((Collection) this.d.m()));
    }

    public Collection<Object> a(ow owVar, qm qmVar, Collection<Object> collection) throws IOException, oz {
        Object a2;
        if (!owVar.j()) {
            return b(owVar, qmVar, collection);
        }
        qu<Object> quVar = this.b;
        rw rwVar = this.c;
        while (true) {
            pb b2 = owVar.b();
            if (b2 == pb.END_ARRAY) {
                return collection;
            }
            if (b2 == pb.VALUE_NULL) {
                a2 = null;
            } else if (rwVar == null) {
                a2 = quVar.a(owVar, qmVar);
            } else {
                a2 = quVar.a(owVar, qmVar, rwVar);
            }
            collection.add(a2);
        }
    }

    public Object a(ow owVar, qm qmVar, rw rwVar) throws IOException, oz {
        return rwVar.b(owVar, qmVar);
    }

    private final Collection<Object> b(ow owVar, qm qmVar, Collection<Object> collection) throws IOException, oz {
        Object a2;
        if (!qmVar.a(ql.ACCEPT_SINGLE_VALUE_AS_ARRAY)) {
            throw qmVar.b(this.a.p());
        }
        qu<Object> quVar = this.b;
        rw rwVar = this.c;
        if (owVar.e() == pb.VALUE_NULL) {
            a2 = null;
        } else if (rwVar == null) {
            a2 = quVar.a(owVar, qmVar);
        } else {
            a2 = quVar.a(owVar, qmVar, rwVar);
        }
        collection.add(a2);
        return collection;
    }
}
