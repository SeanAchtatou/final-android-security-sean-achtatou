package com.flurry.org.codehaus.jackson.annotate;

public enum JsonMethod {
    GETTER,
    SETTER,
    CREATOR,
    FIELD,
    IS_GETTER,
    NONE,
    ALL
}
