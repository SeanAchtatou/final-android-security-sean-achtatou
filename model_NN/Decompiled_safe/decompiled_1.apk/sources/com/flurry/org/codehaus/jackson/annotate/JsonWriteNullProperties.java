package com.flurry.org.codehaus.jackson.annotate;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE, ElementType.METHOD, ElementType.FIELD})
@Deprecated
@JacksonAnnotation
@Retention(RetentionPolicy.RUNTIME)
public @interface JsonWriteNullProperties {
    boolean value() default true;
}
