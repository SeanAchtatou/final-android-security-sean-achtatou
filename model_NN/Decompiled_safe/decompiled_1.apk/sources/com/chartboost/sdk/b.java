package com.chartboost.sdk;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import com.chartboost.sdk.Libraries.a;
import com.chartboost.sdk.Libraries.e;
import com.chartboost.sdk.impl.a;
import com.chartboost.sdk.impl.s;
import com.flurry.android.FlurryFullscreenTakeoverActivity;
import org.json.JSONObject;

public abstract class b {
    public a a = null;
    public c b = null;
    public a c = null;
    public a d = null;
    protected int e = 0;
    protected JSONObject f;
    protected com.chartboost.sdk.impl.a g;
    private int h;
    private int i;
    private int j;
    private C0003b k;

    public interface a {
        void a();
    }

    public interface c {
        void a(String str, JSONObject jSONObject);
    }

    /* access modifiers changed from: protected */
    public abstract C0003b a(Context context);

    /* renamed from: com.chartboost.sdk.b$b  reason: collision with other inner class name */
    public abstract class C0003b extends RelativeLayout implements s.a {
        protected boolean a = false;

        /* access modifiers changed from: protected */
        public abstract void a(int i, int i2);

        public C0003b(Context context) {
            super(context);
            setFocusableInTouchMode(true);
            requestFocus();
            setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
        }

        /* access modifiers changed from: protected */
        public void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);
            if (!this.a) {
                if (Chartboost.sharedChartboost().getForcedOrientationDifference().isOdd()) {
                    b(h, w);
                } else {
                    b(w, h);
                }
            }
        }

        private boolean b(int i, int i2) {
            try {
                a(i, i2);
                return true;
            } catch (Exception e) {
                return false;
            }
        }

        public void a() {
            a((Activity) getContext());
        }

        public void b() {
        }

        /* JADX WARNING: Removed duplicated region for block: B:13:0x004f  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean a(android.app.Activity r5) {
            /*
                r4 = this;
                int r0 = r4.getWidth()     // Catch:{ Exception -> 0x0057 }
                int r1 = r4.getHeight()     // Catch:{ Exception -> 0x0057 }
                if (r0 == 0) goto L_0x000c
                if (r1 != 0) goto L_0x005b
            L_0x000c:
                android.view.Window r0 = r5.getWindow()     // Catch:{ Exception -> 0x0057 }
                r1 = 16908290(0x1020002, float:2.3877235E-38)
                android.view.View r0 = r0.findViewById(r1)     // Catch:{ Exception -> 0x0057 }
                if (r0 != 0) goto L_0x0021
                android.view.Window r0 = r5.getWindow()     // Catch:{ Exception -> 0x0057 }
                android.view.View r0 = r0.getDecorView()     // Catch:{ Exception -> 0x0057 }
            L_0x0021:
                int r1 = r0.getWidth()     // Catch:{ Exception -> 0x0057 }
                int r0 = r0.getHeight()     // Catch:{ Exception -> 0x0057 }
            L_0x0029:
                if (r1 == 0) goto L_0x002d
                if (r0 != 0) goto L_0x0041
            L_0x002d:
                android.util.DisplayMetrics r0 = new android.util.DisplayMetrics
                r0.<init>()
                android.view.WindowManager r1 = r5.getWindowManager()
                android.view.Display r1 = r1.getDefaultDisplay()
                r1.getMetrics(r0)
                int r1 = r0.widthPixels
                int r0 = r0.heightPixels
            L_0x0041:
                com.chartboost.sdk.Chartboost r2 = com.chartboost.sdk.Chartboost.sharedChartboost()
                com.chartboost.sdk.Libraries.CBOrientation$Difference r2 = r2.getForcedOrientationDifference()
                boolean r2 = r2.isOdd()
                if (r2 == 0) goto L_0x0052
                r3 = r1
                r1 = r0
                r0 = r3
            L_0x0052:
                boolean r0 = r4.b(r1, r0)
                return r0
            L_0x0057:
                r0 = move-exception
                r0 = 0
                r1 = r0
                goto L_0x0029
            L_0x005b:
                r3 = r1
                r1 = r0
                r0 = r3
                goto L_0x0029
            */
            throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.b.C0003b.a(android.app.Activity):boolean");
        }
    }

    public b(com.chartboost.sdk.impl.a aVar) {
        this.g = aVar;
        this.k = null;
    }

    public void a(JSONObject jSONObject) {
        this.i = 0;
        this.j = 0;
        this.h = 0;
        this.f = jSONObject.optJSONObject("assets");
        if (this.f == null && this.d != null) {
            this.d.a();
        }
    }

    /* access modifiers changed from: protected */
    public void a(String str, e.b bVar) {
        a(str, bVar, false);
    }

    /* access modifiers changed from: protected */
    public void a(String str, e.b bVar, boolean z) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("paramNoMemoryCache", z);
        a(this.f, str, bVar, bundle);
    }

    /* access modifiers changed from: protected */
    public void a(JSONObject jSONObject, String str, e.b bVar, Bundle bundle) {
        JSONObject optJSONObject = jSONObject.optJSONObject(str);
        if (optJSONObject != null) {
            this.j++;
            e.a().a(optJSONObject.optString(FlurryFullscreenTakeoverActivity.EXTRA_KEY_URL), optJSONObject.optString("checksum"), bVar, null, bundle);
            return;
        }
        a((a.C0000a) null);
    }

    /* access modifiers changed from: protected */
    public void a(a.C0000a aVar) {
        if (aVar != null) {
            this.h++;
        }
        this.i++;
        if (this.i == this.e && !a() && this.d != null) {
            this.d.a();
        }
    }

    public boolean a() {
        if (this.h != this.j) {
            return false;
        }
        if (this.c != null) {
            this.c.a();
        }
        return true;
    }

    public boolean b() {
        if (this.g.c != a.b.CBImpressionStateWaitingForDisplay) {
            return false;
        }
        Chartboost.sharedChartboost().a(this.g);
        Activity b2 = Chartboost.sharedChartboost().b();
        if (b2 == null) {
            this.k = null;
            return false;
        }
        this.k = a(b2);
        if (this.k.a(b2)) {
            return true;
        }
        this.k = null;
        return false;
    }

    public void c() {
        e();
        this.c = null;
        this.d = null;
        this.b = null;
        this.a = null;
        this.f = null;
    }

    public C0003b d() {
        return this.k;
    }

    public void e() {
        if (this.k != null) {
            this.k.b();
        }
        this.k = null;
    }
}
