package com.chartboost.sdk.impl;

import java.io.Serializable;

public class az implements Serializable {
    private final String a;

    public String a() {
        return this.a;
    }

    public boolean equals(Object o) {
        Object obj;
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (o instanceof az) {
            obj = ((az) o).a;
        } else if (!(o instanceof String)) {
            return false;
        } else {
            obj = (String) o;
        }
        if (this.a == null ? obj != null : !this.a.equals(obj)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        if (this.a != null) {
            return this.a.hashCode();
        }
        return 0;
    }

    public String toString() {
        return this.a;
    }
}
