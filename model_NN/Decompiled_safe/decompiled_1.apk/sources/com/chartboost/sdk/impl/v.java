package com.chartboost.sdk.impl;

import android.content.Context;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import com.chartboost.sdk.b;
import org.json.JSONObject;

public class v extends com.chartboost.sdk.b {
    private String h = null;

    public class a extends b.C0003b {
        public WebView c;

        public a(Context context, String str) {
            super(context);
            setFocusable(false);
            this.c = new b(context);
            this.c.setWebViewClient(new c(v.this));
            addView(this.c);
            this.c.loadDataWithBaseURL("file:///android_asset/", str, "text/html", "utf-8", null);
        }

        /* access modifiers changed from: protected */
        public void a(int i, int i2) {
        }
    }

    public v(a aVar) {
        super(aVar);
    }

    /* access modifiers changed from: protected */
    public b.C0003b a(Context context) {
        return new a(context, this.h);
    }

    public void a(JSONObject jSONObject) {
        String optString = jSONObject.optString("html");
        if (optString != null) {
            this.h = optString;
            a();
        }
    }

    private class b extends WebView {
        public b(Context context) {
            super(context);
            setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            setBackgroundColor(0);
            getSettings().setJavaScriptEnabled(true);
        }

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            if ((keyCode == 4 || keyCode == 3) && v.this.a != null) {
                v.this.a.a();
            }
            return super.onKeyDown(keyCode, event);
        }
    }

    private class c extends WebViewClient {
        private v b;

        public c(v vVar) {
            this.b = vVar;
        }

        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if (this.b != null && this.b.c != null) {
                this.b.c.a();
            }
        }

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            if (this.b.d != null) {
                this.b.d.a();
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:40:0x00aa  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean shouldOverrideUrlLoading(android.webkit.WebView r8, java.lang.String r9) {
            /*
                r7 = this;
                r6 = 0
                r3 = 3
                r5 = 4
                r4 = 0
                java.lang.String r0 = "UTF-8"
                java.lang.String r0 = ""
                java.net.URI r0 = new java.net.URI     // Catch:{ Exception -> 0x0039 }
                r0.<init>(r9)     // Catch:{ Exception -> 0x0039 }
                java.lang.String r0 = r0.getScheme()     // Catch:{ Exception -> 0x0039 }
                java.lang.String r1 = "chartboost"
                boolean r0 = r0.equals(r1)
                if (r0 == 0) goto L_0x0061
                java.lang.String r0 = "/"
                java.lang.String[] r0 = r9.split(r0)
                int r1 = r0.length
                java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
                int r2 = r1.intValue()
                if (r2 >= r3) goto L_0x0049
                com.chartboost.sdk.impl.v r0 = r7.b
                com.chartboost.sdk.b$a r0 = r0.a
                if (r0 == 0) goto L_0x0037
                com.chartboost.sdk.impl.v r0 = r7.b
                com.chartboost.sdk.b$a r0 = r0.a
                r0.a()
            L_0x0037:
                r0 = r4
            L_0x0038:
                return r0
            L_0x0039:
                r0 = move-exception
                com.chartboost.sdk.impl.v r0 = r7.b
                com.chartboost.sdk.b$a r0 = r0.a
                if (r0 == 0) goto L_0x0047
                com.chartboost.sdk.impl.v r0 = r7.b
                com.chartboost.sdk.b$a r0 = r0.a
                r0.a()
            L_0x0047:
                r0 = r4
                goto L_0x0038
            L_0x0049:
                r2 = 2
                r2 = r0[r2]
                java.lang.String r3 = "close"
                boolean r3 = r2.equals(r3)
                if (r3 == 0) goto L_0x0063
                com.chartboost.sdk.impl.v r0 = r7.b
                com.chartboost.sdk.b$a r0 = r0.a
                if (r0 == 0) goto L_0x0061
                com.chartboost.sdk.impl.v r0 = r7.b
                com.chartboost.sdk.b$a r0 = r0.a
                r0.a()
            L_0x0061:
                r0 = 1
                goto L_0x0038
            L_0x0063:
                java.lang.String r3 = "link"
                boolean r2 = r2.equals(r3)
                if (r2 == 0) goto L_0x0061
                int r2 = r1.intValue()
                if (r2 >= r5) goto L_0x0080
                com.chartboost.sdk.impl.v r0 = r7.b
                com.chartboost.sdk.b$a r0 = r0.a
                if (r0 == 0) goto L_0x007e
                com.chartboost.sdk.impl.v r0 = r7.b
                com.chartboost.sdk.b$a r0 = r0.a
                r0.a()
            L_0x007e:
                r0 = r4
                goto L_0x0038
            L_0x0080:
                r2 = 3
                r2 = r0[r2]     // Catch:{ Exception -> 0x00b2 }
                java.lang.String r3 = "UTF-8"
                java.lang.String r2 = java.net.URLDecoder.decode(r2, r3)     // Catch:{ Exception -> 0x00b2 }
                int r1 = r1.intValue()     // Catch:{ Exception -> 0x00ba }
                if (r1 >= r5) goto L_0x00bd
                org.json.JSONTokener r1 = new org.json.JSONTokener     // Catch:{ Exception -> 0x00ba }
                r3 = 4
                r0 = r0[r3]     // Catch:{ Exception -> 0x00ba }
                java.lang.String r3 = "UTF-8"
                java.lang.String r0 = java.net.URLDecoder.decode(r0, r3)     // Catch:{ Exception -> 0x00ba }
                r1.<init>(r0)     // Catch:{ Exception -> 0x00ba }
                org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ Exception -> 0x00ba }
                r0.<init>(r1)     // Catch:{ Exception -> 0x00ba }
                r1 = r0
                r0 = r2
            L_0x00a4:
                com.chartboost.sdk.impl.v r2 = r7.b
                com.chartboost.sdk.b$c r2 = r2.b
                if (r2 == 0) goto L_0x0061
                com.chartboost.sdk.impl.v r2 = r7.b
                com.chartboost.sdk.b$c r2 = r2.b
                r2.a(r0, r1)
                goto L_0x0061
            L_0x00b2:
                r0 = move-exception
                r1 = r6
            L_0x00b4:
                r0.printStackTrace()
                r0 = r1
                r1 = r6
                goto L_0x00a4
            L_0x00ba:
                r0 = move-exception
                r1 = r2
                goto L_0x00b4
            L_0x00bd:
                r0 = r2
                r1 = r6
                goto L_0x00a4
            */
            throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.impl.v.c.shouldOverrideUrlLoading(android.webkit.WebView, java.lang.String):boolean");
        }
    }
}
