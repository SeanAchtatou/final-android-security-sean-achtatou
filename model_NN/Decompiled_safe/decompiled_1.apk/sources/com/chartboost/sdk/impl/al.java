package com.chartboost.sdk.impl;

import java.lang.reflect.Array;
import java.util.Date;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Pattern;

public class al implements ai {
    protected ap a;

    public byte[] a(aj ajVar) {
        ao aoVar = new ao();
        a(aoVar);
        b(ajVar);
        a();
        return aoVar.c();
    }

    public void a(ap apVar) {
        if (this.a != null) {
            throw new IllegalStateException("in the middle of something");
        }
        this.a = apVar;
    }

    public void a() {
        this.a = null;
    }

    /* access modifiers changed from: protected */
    public boolean a(String str, aj ajVar) {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean a(String str, Object obj) {
        return false;
    }

    public int b(aj ajVar) {
        return b((String) null, ajVar);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00a7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int b(java.lang.String r11, com.chartboost.sdk.impl.aj r12) {
        /*
            r10 = this;
            r5 = 3
            r9 = 0
            java.lang.String r8 = "_id"
            if (r12 != 0) goto L_0x000e
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            java.lang.String r1 = "can't save a null object"
            r0.<init>(r1)
            throw r0
        L_0x000e:
            com.chartboost.sdk.impl.ap r0 = r10.a
            int r2 = r0.a()
            boolean r0 = r12 instanceof java.util.List
            if (r0 == 0) goto L_0x00f2
            r0 = 4
        L_0x0019:
            boolean r1 = r10.a(r11, r12)
            if (r1 == 0) goto L_0x0027
            com.chartboost.sdk.impl.ap r0 = r10.a
            int r0 = r0.a()
            int r0 = r0 - r2
        L_0x0026:
            return r0
        L_0x0027:
            if (r11 == 0) goto L_0x002c
            r10.a(r0, r11)
        L_0x002c:
            com.chartboost.sdk.impl.ap r1 = r10.a
            int r3 = r1.a()
            com.chartboost.sdk.impl.ap r1 = r10.a
            r1.c(r9)
            r1 = 0
            if (r0 != r5) goto L_0x00a5
            if (r11 != 0) goto L_0x00a5
            r4 = 1
        L_0x003d:
            if (r0 != r5) goto L_0x00ef
            if (r4 == 0) goto L_0x0054
            java.lang.String r0 = "_id"
            boolean r0 = r12.b(r8)
            if (r0 == 0) goto L_0x0054
            java.lang.String r0 = "_id"
            java.lang.String r0 = "_id"
            java.lang.Object r0 = r12.a(r8)
            r10.b(r8, r0)
        L_0x0054:
            java.lang.String r0 = "_transientFields"
            java.lang.Object r0 = r12.a(r0)
            boolean r5 = r0 instanceof java.util.List
            if (r5 == 0) goto L_0x00ef
            java.util.List r0 = (java.util.List) r0
            r5 = r0
        L_0x0061:
            boolean r0 = r12 instanceof java.util.Map
            if (r0 == 0) goto L_0x00a7
            java.util.Map r12 = (java.util.Map) r12
            java.util.Set r0 = r12.entrySet()
            java.util.Iterator r6 = r0.iterator()
        L_0x006f:
            boolean r0 = r6.hasNext()
            if (r0 == 0) goto L_0x00d5
            java.lang.Object r0 = r6.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            if (r4 == 0) goto L_0x008b
            java.lang.Object r1 = r0.getKey()
            java.lang.String r1 = (java.lang.String) r1
            java.lang.String r7 = "_id"
            boolean r1 = r1.equals(r8)
            if (r1 != 0) goto L_0x006f
        L_0x008b:
            if (r5 == 0) goto L_0x0097
            java.lang.Object r1 = r0.getKey()
            boolean r1 = r5.contains(r1)
            if (r1 != 0) goto L_0x006f
        L_0x0097:
            java.lang.Object r1 = r0.getKey()
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r0 = r0.getValue()
            r10.b(r1, r0)
            goto L_0x006f
        L_0x00a5:
            r4 = r9
            goto L_0x003d
        L_0x00a7:
            java.util.Set r0 = r12.keySet()
            java.util.Iterator r1 = r0.iterator()
        L_0x00af:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x00d5
            java.lang.Object r0 = r1.next()
            java.lang.String r0 = (java.lang.String) r0
            if (r4 == 0) goto L_0x00c5
            java.lang.String r6 = "_id"
            boolean r6 = r0.equals(r8)
            if (r6 != 0) goto L_0x00af
        L_0x00c5:
            if (r5 == 0) goto L_0x00cd
            boolean r6 = r5.contains(r0)
            if (r6 != 0) goto L_0x00af
        L_0x00cd:
            java.lang.Object r6 = r12.a(r0)
            r10.b(r0, r6)
            goto L_0x00af
        L_0x00d5:
            com.chartboost.sdk.impl.ap r0 = r10.a
            r0.write(r9)
            com.chartboost.sdk.impl.ap r0 = r10.a
            com.chartboost.sdk.impl.ap r1 = r10.a
            int r1 = r1.a()
            int r1 = r1 - r3
            r0.a(r3, r1)
            com.chartboost.sdk.impl.ap r0 = r10.a
            int r0 = r0.a()
            int r0 = r0 - r2
            goto L_0x0026
        L_0x00ef:
            r5 = r1
            goto L_0x0061
        L_0x00f2:
            r0 = r5
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.impl.al.b(java.lang.String, com.chartboost.sdk.impl.aj):int");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.impl.al.b(java.lang.String, com.chartboost.sdk.impl.aj):int
     arg types: [java.lang.String, com.chartboost.sdk.impl.am]
     candidates:
      com.chartboost.sdk.impl.al.b(java.lang.String, java.lang.Object):void
      com.chartboost.sdk.impl.al.b(java.lang.String, com.chartboost.sdk.impl.aj):int */
    /* access modifiers changed from: protected */
    public void b(String str, Object obj) {
        if (!str.equals("_transientFields")) {
            if (!str.equals("$where") || !(obj instanceof String)) {
                Object a2 = ag.a(obj);
                if (a2 == null) {
                    a(str);
                } else if (a2 instanceof Date) {
                    a(str, (Date) a2);
                } else if (a2 instanceof Number) {
                    a(str, (Number) a2);
                } else if (a2 instanceof Character) {
                    a(str, a2.toString());
                } else if (a2 instanceof String) {
                    a(str, a2.toString());
                } else if (a2 instanceof ay) {
                    a(str, (ay) a2);
                } else if (a2 instanceof aj) {
                    b(str, (aj) a2);
                } else if (a2 instanceof Boolean) {
                    a(str, (Boolean) a2);
                } else if (a2 instanceof Pattern) {
                    a(str, (Pattern) a2);
                } else if (a2 instanceof Map) {
                    a(str, (Map) a2);
                } else if (a2 instanceof Iterable) {
                    a(str, (Iterable) a2);
                } else if (a2 instanceof byte[]) {
                    a(str, (byte[]) a2);
                } else if (a2 instanceof at) {
                    a(str, (at) a2);
                } else if (a2 instanceof UUID) {
                    a(str, (UUID) a2);
                } else if (a2.getClass().isArray()) {
                    c(str, a2);
                } else if (a2 instanceof az) {
                    a(str, (az) a2);
                } else if (a2 instanceof as) {
                    a(str, (as) a2);
                } else if (a2 instanceof av) {
                    a(str, (av) a2);
                } else if (a2 instanceof au) {
                    a(str, (au) a2);
                } else if (a2 instanceof z) {
                    am amVar = new am();
                    amVar.a("$ref", ((z) a2).b());
                    amVar.a("$id", ((z) a2).a());
                    b(str, (aj) amVar);
                } else if (a2 instanceof ax) {
                    d(str);
                } else if (a2 instanceof aw) {
                    e(str);
                } else if (!a(str, a2)) {
                    throw new IllegalArgumentException("can't serialize " + a2.getClass());
                }
            } else {
                a((byte) 13, str);
                b(obj.toString());
            }
        }
    }

    private void c(String str, Object obj) {
        a((byte) 4, str);
        int a2 = this.a.a();
        this.a.c(0);
        int length = Array.getLength(obj);
        for (int i = 0; i < length; i++) {
            b(String.valueOf(i), Array.get(obj, i));
        }
        this.a.write(0);
        this.a.a(a2, this.a.a() - a2);
    }

    private void a(String str, Iterable iterable) {
        a((byte) 4, str);
        int a2 = this.a.a();
        this.a.c(0);
        int i = 0;
        for (Object b : iterable) {
            b(String.valueOf(i), b);
            i++;
        }
        this.a.write(0);
        this.a.a(a2, this.a.a() - a2);
    }

    private void a(String str, Map map) {
        a((byte) 3, str);
        int a2 = this.a.a();
        this.a.c(0);
        for (Map.Entry entry : map.entrySet()) {
            b(entry.getKey().toString(), entry.getValue());
        }
        this.a.write(0);
        this.a.a(a2, this.a.a() - a2);
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        a((byte) 10, str);
    }

    /* access modifiers changed from: protected */
    public void a(String str, as asVar) {
        a((byte) 17, str);
        this.a.c(asVar.b());
        this.a.c(asVar.a());
    }

    /* access modifiers changed from: protected */
    public void a(String str, av avVar) {
        a((byte) 15, str);
        int a2 = this.a.a();
        this.a.c(0);
        b(avVar.a());
        b(avVar.b());
        this.a.a(a2, this.a.a() - a2);
    }

    /* access modifiers changed from: protected */
    public void a(String str, au auVar) {
        a((byte) 13, str);
        this.a.a();
        b(auVar.a());
    }

    /* access modifiers changed from: protected */
    public void a(String str, Boolean bool) {
        a((byte) 8, str);
        this.a.write(bool.booleanValue() ? 1 : 0);
    }

    /* access modifiers changed from: protected */
    public void a(String str, Date date) {
        a((byte) 9, str);
        this.a.a(date.getTime());
    }

    /* access modifiers changed from: protected */
    public void a(String str, Number number) {
        if ((number instanceof Integer) || (number instanceof Short) || (number instanceof Byte) || (number instanceof AtomicInteger)) {
            a((byte) 16, str);
            this.a.c(number.intValue());
        } else if ((number instanceof Long) || (number instanceof AtomicLong)) {
            a((byte) 18, str);
            this.a.a(number.longValue());
        } else if ((number instanceof Float) || (number instanceof Double)) {
            a((byte) 1, str);
            this.a.a(number.doubleValue());
        } else {
            throw new IllegalArgumentException("can't serialize " + number.getClass());
        }
    }

    /* access modifiers changed from: protected */
    public void a(String str, byte[] bArr) {
        a(str, 0, bArr);
    }

    /* access modifiers changed from: protected */
    public void a(String str, at atVar) {
        a(str, atVar.a(), atVar.b());
    }

    private void a(String str, int i, byte[] bArr) {
        a((byte) 5, str);
        int length = bArr.length;
        if (i == 2) {
            length += 4;
        }
        this.a.c(length);
        this.a.write(i);
        if (i == 2) {
            this.a.c(length - 4);
        }
        int a2 = this.a.a();
        this.a.write(bArr);
        ae.a(this.a.a() - a2, bArr.length);
    }

    /* access modifiers changed from: protected */
    public void a(String str, UUID uuid) {
        a((byte) 5, str);
        this.a.c(16);
        this.a.write(3);
        this.a.a(uuid.getMostSignificantBits());
        this.a.a(uuid.getLeastSignificantBits());
    }

    /* access modifiers changed from: protected */
    public void a(String str, az azVar) {
        a(str, azVar.a(), (byte) 14);
    }

    /* access modifiers changed from: protected */
    public void a(String str, String str2) {
        a(str, str2, (byte) 2);
    }

    private void a(String str, String str2, byte b) {
        a(b, str);
        b(str2);
    }

    /* access modifiers changed from: protected */
    public void a(String str, ay ayVar) {
        a((byte) 7, str);
        this.a.d(ayVar.c());
        this.a.d(ayVar.d());
        this.a.d(ayVar.e());
    }

    private void a(String str, Pattern pattern) {
        a((byte) 11, str);
        c(pattern.pattern());
        c(ag.a(pattern.flags()));
    }

    private void d(String str) {
        a((byte) -1, str);
    }

    private void e(String str) {
        a(Byte.MAX_VALUE, str);
    }

    /* access modifiers changed from: protected */
    public void a(byte b, String str) {
        this.a.write(b);
        c(str);
    }

    /* access modifiers changed from: protected */
    public void b(String str) {
        int a2 = this.a.a();
        this.a.c(0);
        this.a.a(a2, c(str));
    }

    /* access modifiers changed from: protected */
    public int c(String str) {
        int length = str.length();
        int i = 0;
        int i2 = 0;
        while (i < length) {
            int codePointAt = Character.codePointAt(str, i);
            if (codePointAt < 128) {
                this.a.write((byte) codePointAt);
                i2++;
            } else if (codePointAt < 2048) {
                this.a.write((byte) ((codePointAt >> 6) + 192));
                this.a.write((byte) ((codePointAt & 63) + 128));
                i2 += 2;
            } else if (codePointAt < 65536) {
                this.a.write((byte) ((codePointAt >> 12) + 224));
                this.a.write((byte) (((codePointAt >> 6) & 63) + 128));
                this.a.write((byte) ((codePointAt & 63) + 128));
                i2 += 3;
            } else {
                this.a.write((byte) ((codePointAt >> 18) + 240));
                this.a.write((byte) (((codePointAt >> 12) & 63) + 128));
                this.a.write((byte) (((codePointAt >> 6) & 63) + 128));
                this.a.write((byte) ((codePointAt & 63) + 128));
                i2 += 4;
            }
            i += Character.charCount(codePointAt);
        }
        this.a.write(0);
        return i2 + 1;
    }
}
