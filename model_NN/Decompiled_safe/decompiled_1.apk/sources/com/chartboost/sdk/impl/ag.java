package com.chartboost.sdk.impl;

import com.ideaworks3d.marmalade.S3EVideoView;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class ag {
    private static boolean a = false;
    private static boolean b = false;
    static final Logger f = Logger.getLogger("org.bson.BSON");
    static bd<List<an>> g = new bd<>();
    static bd<List<an>> h = new bd<>();
    protected static Charset i = Charset.forName("UTF-8");
    static ThreadLocal<ai> j = new ThreadLocal<ai>() {
        /* access modifiers changed from: protected */
        /* renamed from: a */
        public ai initialValue() {
            return new al();
        }
    };
    static ThreadLocal<ah> k = new ThreadLocal<ah>() {
        /* access modifiers changed from: protected */
        /* renamed from: a */
        public ah initialValue() {
            return new ak();
        }
    };

    public static String a(int i2) {
        StringBuilder sb = new StringBuilder();
        int i3 = i2;
        for (a aVar : a.values()) {
            if ((aVar.j & i3) > 0) {
                sb.append(aVar.k);
                i3 -= aVar.j;
            }
        }
        if (i3 <= 0) {
            return sb.toString();
        }
        throw new IllegalArgumentException("some flags could not be recognized.");
    }

    private enum a {
        CANON_EQ(128, 'c', "Pattern.CANON_EQ"),
        UNIX_LINES(1, 'd', "Pattern.UNIX_LINES"),
        GLOBAL(S3EVideoView.S3E_VIDEO_MAX_VOLUME, 'g', null),
        CASE_INSENSITIVE(2, 'i', null),
        MULTILINE(8, 'm', null),
        DOTALL(32, 's', "Pattern.DOTALL"),
        LITERAL(16, 't', "Pattern.LITERAL"),
        UNICODE_CASE(64, 'u', "Pattern.UNICODE_CASE"),
        COMMENTS(4, 'x', null);
        
        private static final Map<Character, a> m = new HashMap();
        public final int j;
        public final char k;
        public final String l;

        static {
            for (a aVar : values()) {
                m.put(Character.valueOf(aVar.k), aVar);
            }
        }

        private a(int i, char c, String str) {
            this.j = i;
            this.k = c;
            this.l = str;
        }
    }

    public static Object a(Object obj) {
        if (!a()) {
            return obj;
        }
        if (g.a() == 0 || obj == null) {
            return obj;
        }
        List<an> a2 = g.a((Object) obj.getClass());
        if (a2 == null) {
            return obj;
        }
        Object obj2 = obj;
        for (an a3 : a2) {
            obj2 = a3.a(obj2);
        }
        return obj2;
    }

    private static boolean a() {
        return a || b;
    }
}
