package com.chartboost.sdk.impl;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import com.chartboost.sdk.Chartboost;
import java.net.URI;
import java.net.URISyntaxException;

public class m {
    public a a;

    public interface a {
        void a(String str);
    }

    public m(a aVar) {
        this.a = aVar;
    }

    public void a(final String str, final Context context) {
        try {
            String scheme = new URI(str).getScheme();
            if (scheme != null) {
                if (scheme.equals("http") || scheme.equals("https")) {
                    new AsyncTask<Void, Void, String>() {
                        /* JADX WARNING: Removed duplicated region for block: B:14:0x0027  */
                        /* JADX WARNING: Removed duplicated region for block: B:19:0x0030  */
                        /* renamed from: a */
                        /* Code decompiled incorrectly, please refer to instructions dump. */
                        public java.lang.String doInBackground(java.lang.Void... r5) {
                            /*
                                r4 = this;
                                r2 = 0
                                java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x0023, all -> 0x002c }
                                java.lang.String r1 = r3     // Catch:{ Exception -> 0x0023, all -> 0x002c }
                                r0.<init>(r1)     // Catch:{ Exception -> 0x0023, all -> 0x002c }
                                java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x0023, all -> 0x002c }
                                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0023, all -> 0x002c }
                                r1 = 0
                                r0.setInstanceFollowRedirects(r1)     // Catch:{ Exception -> 0x0039, all -> 0x0034 }
                                java.lang.String r1 = "Location"
                                java.lang.String r1 = r0.getHeaderField(r1)     // Catch:{ Exception -> 0x0039, all -> 0x0034 }
                                if (r1 != 0) goto L_0x001c
                                java.lang.String r1 = r3     // Catch:{ Exception -> 0x0039, all -> 0x0034 }
                            L_0x001c:
                                if (r0 == 0) goto L_0x0021
                                r0.disconnect()
                            L_0x0021:
                                r0 = r1
                            L_0x0022:
                                return r0
                            L_0x0023:
                                r0 = move-exception
                                r0 = r2
                            L_0x0025:
                                if (r0 == 0) goto L_0x002a
                                r0.disconnect()
                            L_0x002a:
                                r0 = r2
                                goto L_0x0022
                            L_0x002c:
                                r0 = move-exception
                                r1 = r2
                            L_0x002e:
                                if (r1 == 0) goto L_0x0033
                                r1.disconnect()
                            L_0x0033:
                                throw r0
                            L_0x0034:
                                r1 = move-exception
                                r3 = r1
                                r1 = r0
                                r0 = r3
                                goto L_0x002e
                            L_0x0039:
                                r1 = move-exception
                                goto L_0x0025
                            */
                            throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.impl.m.AnonymousClass1.doInBackground(java.lang.Void[]):java.lang.String");
                        }

                        /* renamed from: a */
                        public void onPostExecute(String str) {
                            m.this.b(str, context);
                        }
                    }.execute(new Void[0]);
                } else {
                    b(str, context);
                }
            }
        } catch (URISyntaxException e) {
        }
    }

    /* access modifiers changed from: private */
    public void b(String str, Context context) {
        Context context2;
        if (this.a != null) {
            this.a.a(str);
        }
        if (context == null) {
            context2 = Chartboost.sharedChartboost().getContext();
        } else {
            context2 = context;
        }
        if (context2 != null) {
            try {
                Intent intent = new Intent("android.intent.action.VIEW");
                if (!(context2 instanceof Activity)) {
                    intent.addFlags(268435456);
                }
                intent.setData(Uri.parse(str));
                context2.startActivity(intent);
            } catch (Exception e) {
                try {
                    if (str.startsWith("market://")) {
                        String str2 = "http://market.android.com/" + str.substring(9);
                        Intent intent2 = new Intent("android.intent.action.VIEW");
                        if (!(context2 instanceof Activity)) {
                            intent2.addFlags(268435456);
                        }
                        intent2.setData(Uri.parse(str2));
                        context2.startActivity(intent2);
                    }
                } catch (Exception e2) {
                }
            }
        }
    }
}
