package com.chartboost.sdk.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

abstract class ba<K, V, M extends Map<K, V>> implements Serializable, ConcurrentMap<K, V> {
    /* access modifiers changed from: private */
    public volatile M a;
    /* access modifiers changed from: private */
    public final transient Lock b = new ReentrantLock();
    private final h<K, V> c;

    /* access modifiers changed from: package-private */
    public abstract <N extends Map<? extends K, ? extends V>> M a(N n);

    protected <N extends Map<? extends K, ? extends V>> ba(N n, h.a aVar) {
        this.a = (Map) bb.a("delegate", a((Map) bb.a("map", n)));
        this.c = ((h.a) bb.a("viewType", aVar)).a(this);
    }

    public final void clear() {
        this.b.lock();
        try {
            b(a(Collections.emptyMap()));
        } finally {
            this.b.unlock();
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: M
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final V remove(java.lang.Object r3) {
        /*
            r2 = this;
            java.util.concurrent.locks.Lock r0 = r2.b
            r0.lock()
            M r0 = r2.a     // Catch:{ all -> 0x002b }
            boolean r0 = r0.containsKey(r3)     // Catch:{ all -> 0x002b }
            if (r0 != 0) goto L_0x0014
            r0 = 0
            java.util.concurrent.locks.Lock r1 = r2.b
            r1.unlock()
        L_0x0013:
            return r0
        L_0x0014:
            java.util.Map r0 = r2.a()     // Catch:{ all -> 0x002b }
            java.lang.Object r1 = r0.remove(r3)     // Catch:{ all -> 0x0026 }
            r2.b(r0)     // Catch:{ all -> 0x002b }
            java.util.concurrent.locks.Lock r0 = r2.b
            r0.unlock()
            r0 = r1
            goto L_0x0013
        L_0x0026:
            r1 = move-exception
            r2.b(r0)     // Catch:{ all -> 0x002b }
            throw r1     // Catch:{ all -> 0x002b }
        L_0x002b:
            r0 = move-exception
            java.util.concurrent.locks.Lock r1 = r2.b
            r1.unlock()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.impl.ba.remove(java.lang.Object):java.lang.Object");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: M
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean remove(java.lang.Object r3, java.lang.Object r4) {
        /*
            r2 = this;
            java.util.concurrent.locks.Lock r0 = r2.b
            r0.lock()
            M r0 = r2.a     // Catch:{ all -> 0x0031 }
            boolean r0 = r0.containsKey(r3)     // Catch:{ all -> 0x0031 }
            if (r0 == 0) goto L_0x002a
            M r0 = r2.a     // Catch:{ all -> 0x0031 }
            java.lang.Object r0 = r0.get(r3)     // Catch:{ all -> 0x0031 }
            boolean r0 = r2.a(r4, r0)     // Catch:{ all -> 0x0031 }
            if (r0 == 0) goto L_0x002a
            java.util.Map r0 = r2.a()     // Catch:{ all -> 0x0031 }
            r0.remove(r3)     // Catch:{ all -> 0x0031 }
            r2.b(r0)     // Catch:{ all -> 0x0031 }
            r0 = 1
            java.util.concurrent.locks.Lock r1 = r2.b
            r1.unlock()
        L_0x0029:
            return r0
        L_0x002a:
            r0 = 0
            java.util.concurrent.locks.Lock r1 = r2.b
            r1.unlock()
            goto L_0x0029
        L_0x0031:
            r0 = move-exception
            java.util.concurrent.locks.Lock r1 = r2.b
            r1.unlock()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.impl.ba.remove(java.lang.Object, java.lang.Object):boolean");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: M
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean replace(K r3, V r4, V r5) {
        /*
            r2 = this;
            java.util.concurrent.locks.Lock r0 = r2.b
            r0.lock()
            M r0 = r2.a     // Catch:{ all -> 0x0031 }
            boolean r0 = r0.containsKey(r3)     // Catch:{ all -> 0x0031 }
            if (r0 == 0) goto L_0x0019
            M r0 = r2.a     // Catch:{ all -> 0x0031 }
            java.lang.Object r0 = r0.get(r3)     // Catch:{ all -> 0x0031 }
            boolean r0 = r2.a(r4, r0)     // Catch:{ all -> 0x0031 }
            if (r0 != 0) goto L_0x0020
        L_0x0019:
            r0 = 0
            java.util.concurrent.locks.Lock r1 = r2.b
            r1.unlock()
        L_0x001f:
            return r0
        L_0x0020:
            java.util.Map r0 = r2.a()     // Catch:{ all -> 0x0031 }
            r0.put(r3, r5)     // Catch:{ all -> 0x0031 }
            r2.b(r0)     // Catch:{ all -> 0x0031 }
            r0 = 1
            java.util.concurrent.locks.Lock r1 = r2.b
            r1.unlock()
            goto L_0x001f
        L_0x0031:
            r0 = move-exception
            java.util.concurrent.locks.Lock r1 = r2.b
            r1.unlock()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.impl.ba.replace(java.lang.Object, java.lang.Object, java.lang.Object):boolean");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: M
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public V replace(K r3, V r4) {
        /*
            r2 = this;
            java.util.concurrent.locks.Lock r0 = r2.b
            r0.lock()
            M r0 = r2.a     // Catch:{ all -> 0x002b }
            boolean r0 = r0.containsKey(r3)     // Catch:{ all -> 0x002b }
            if (r0 != 0) goto L_0x0014
            r0 = 0
            java.util.concurrent.locks.Lock r1 = r2.b
            r1.unlock()
        L_0x0013:
            return r0
        L_0x0014:
            java.util.Map r0 = r2.a()     // Catch:{ all -> 0x002b }
            java.lang.Object r1 = r0.put(r3, r4)     // Catch:{ all -> 0x0026 }
            r2.b(r0)     // Catch:{ all -> 0x002b }
            java.util.concurrent.locks.Lock r0 = r2.b
            r0.unlock()
            r0 = r1
            goto L_0x0013
        L_0x0026:
            r1 = move-exception
            r2.b(r0)     // Catch:{ all -> 0x002b }
            throw r1     // Catch:{ all -> 0x002b }
        L_0x002b:
            r0 = move-exception
            java.util.concurrent.locks.Lock r1 = r2.b
            r1.unlock()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.impl.ba.replace(java.lang.Object, java.lang.Object):java.lang.Object");
    }

    public final V put(K key, V value) {
        Map a2;
        this.b.lock();
        try {
            a2 = a();
            V put = a2.put(key, value);
            b(a2);
            this.b.unlock();
            return put;
        } catch (Throwable th) {
            this.b.unlock();
            throw th;
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: M
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public V putIfAbsent(K r3, V r4) {
        /*
            r2 = this;
            java.util.concurrent.locks.Lock r0 = r2.b
            r0.lock()
            M r0 = r2.a     // Catch:{ all -> 0x0024 }
            boolean r0 = r0.containsKey(r3)     // Catch:{ all -> 0x0024 }
            if (r0 != 0) goto L_0x002b
            java.util.Map r0 = r2.a()     // Catch:{ all -> 0x0024 }
            java.lang.Object r1 = r0.put(r3, r4)     // Catch:{ all -> 0x001f }
            r2.b(r0)     // Catch:{ all -> 0x0024 }
            java.util.concurrent.locks.Lock r0 = r2.b
            r0.unlock()
            r0 = r1
        L_0x001e:
            return r0
        L_0x001f:
            r1 = move-exception
            r2.b(r0)     // Catch:{ all -> 0x0024 }
            throw r1     // Catch:{ all -> 0x0024 }
        L_0x0024:
            r0 = move-exception
            java.util.concurrent.locks.Lock r1 = r2.b
            r1.unlock()
            throw r0
        L_0x002b:
            M r0 = r2.a     // Catch:{ all -> 0x0024 }
            java.lang.Object r0 = r0.get(r3)     // Catch:{ all -> 0x0024 }
            java.util.concurrent.locks.Lock r1 = r2.b
            r1.unlock()
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.impl.ba.putIfAbsent(java.lang.Object, java.lang.Object):java.lang.Object");
    }

    public final void putAll(Map<? extends K, ? extends V> t) {
        this.b.lock();
        try {
            Map a2 = a();
            a2.putAll(t);
            b(a2);
        } finally {
            this.b.unlock();
        }
    }

    /* access modifiers changed from: protected */
    public M a() {
        this.b.lock();
        try {
            return a((Map) this.a);
        } finally {
            this.b.unlock();
        }
    }

    /* access modifiers changed from: protected */
    public void b(M m) {
        this.a = m;
    }

    public final Set<Map.Entry<K, V>> entrySet() {
        return this.c.b();
    }

    public final Set<K> keySet() {
        return this.c.a();
    }

    public final Collection<V> values() {
        return this.c.c();
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: M
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final boolean containsKey(java.lang.Object r2) {
        /*
            r1 = this;
            M r0 = r1.a
            boolean r0 = r0.containsKey(r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.impl.ba.containsKey(java.lang.Object):boolean");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: M
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final boolean containsValue(java.lang.Object r2) {
        /*
            r1 = this;
            M r0 = r1.a
            boolean r0 = r0.containsValue(r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.impl.ba.containsValue(java.lang.Object):boolean");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: M
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final V get(java.lang.Object r2) {
        /*
            r1 = this;
            M r0 = r1.a
            java.lang.Object r0 = r0.get(r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.impl.ba.get(java.lang.Object):java.lang.Object");
    }

    public final boolean isEmpty() {
        return this.a.isEmpty();
    }

    public final int size() {
        return this.a.size();
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: M
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final boolean equals(java.lang.Object r2) {
        /*
            r1 = this;
            M r0 = r1.a
            boolean r0 = r0.equals(r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.impl.ba.equals(java.lang.Object):boolean");
    }

    public final int hashCode() {
        return this.a.hashCode();
    }

    public String toString() {
        return this.a.toString();
    }

    private class d extends a<K> implements Set<K> {
        private d() {
        }

        /* access modifiers changed from: package-private */
        public Collection<K> a() {
            return ba.this.a.keySet();
        }

        public void clear() {
            ba.this.b.lock();
            try {
                Map a2 = ba.this.a();
                a2.keySet().clear();
                ba.this.b(a2);
            } finally {
                ba.this.b.unlock();
            }
        }

        public boolean remove(Object o) {
            return ba.this.remove(o) != null;
        }

        public boolean removeAll(Collection<?> c) {
            Map a2;
            ba.this.b.lock();
            try {
                a2 = ba.this.a();
                boolean removeAll = a2.keySet().removeAll(c);
                ba.this.b(a2);
                ba.this.b.unlock();
                return removeAll;
            } catch (Throwable th) {
                ba.this.b.unlock();
                throw th;
            }
        }

        public boolean retainAll(Collection<?> c) {
            Map a2;
            ba.this.b.lock();
            try {
                a2 = ba.this.a();
                boolean retainAll = a2.keySet().retainAll(c);
                ba.this.b(a2);
                ba.this.b.unlock();
                return retainAll;
            } catch (Throwable th) {
                ba.this.b.unlock();
                throw th;
            }
        }
    }

    private final class g extends a<V> {
        private g() {
        }

        /* access modifiers changed from: package-private */
        public Collection<V> a() {
            return ba.this.a.values();
        }

        public void clear() {
            ba.this.b.lock();
            try {
                Map a2 = ba.this.a();
                a2.values().clear();
                ba.this.b(a2);
            } finally {
                ba.this.b.unlock();
            }
        }

        public boolean remove(Object o) {
            Map a2;
            ba.this.b.lock();
            try {
                if (!contains(o)) {
                    ba.this.b.unlock();
                    return false;
                }
                a2 = ba.this.a();
                boolean remove = a2.values().remove(o);
                ba.this.b(a2);
                ba.this.b.unlock();
                return remove;
            } catch (Throwable th) {
                ba.this.b.unlock();
                throw th;
            }
        }

        public boolean removeAll(Collection<?> c) {
            Map a2;
            ba.this.b.lock();
            try {
                a2 = ba.this.a();
                boolean removeAll = a2.values().removeAll(c);
                ba.this.b(a2);
                ba.this.b.unlock();
                return removeAll;
            } catch (Throwable th) {
                ba.this.b.unlock();
                throw th;
            }
        }

        public boolean retainAll(Collection<?> c) {
            Map a2;
            ba.this.b.lock();
            try {
                a2 = ba.this.a();
                boolean retainAll = a2.values().retainAll(c);
                ba.this.b(a2);
                ba.this.b.unlock();
                return retainAll;
            } catch (Throwable th) {
                ba.this.b.unlock();
                throw th;
            }
        }
    }

    private class b extends a<Map.Entry<K, V>> implements Set<Map.Entry<K, V>> {
        private b() {
        }

        /* access modifiers changed from: package-private */
        public Collection<Map.Entry<K, V>> a() {
            return ba.this.a.entrySet();
        }

        public void clear() {
            ba.this.b.lock();
            try {
                Map a2 = ba.this.a();
                a2.entrySet().clear();
                ba.this.b(a2);
            } finally {
                ba.this.b.unlock();
            }
        }

        public boolean remove(Object o) {
            Map a2;
            ba.this.b.lock();
            try {
                if (!contains(o)) {
                    ba.this.b.unlock();
                    return false;
                }
                a2 = ba.this.a();
                boolean remove = a2.entrySet().remove(o);
                ba.this.b(a2);
                ba.this.b.unlock();
                return remove;
            } catch (Throwable th) {
                ba.this.b.unlock();
                throw th;
            }
        }

        public boolean removeAll(Collection<?> c) {
            Map a2;
            ba.this.b.lock();
            try {
                a2 = ba.this.a();
                boolean removeAll = a2.entrySet().removeAll(c);
                ba.this.b(a2);
                ba.this.b.unlock();
                return removeAll;
            } catch (Throwable th) {
                ba.this.b.unlock();
                throw th;
            }
        }

        public boolean retainAll(Collection<?> c) {
            Map a2;
            ba.this.b.lock();
            try {
                a2 = ba.this.a();
                boolean retainAll = a2.entrySet().retainAll(c);
                ba.this.b(a2);
                ba.this.b.unlock();
                return retainAll;
            } catch (Throwable th) {
                ba.this.b.unlock();
                throw th;
            }
        }
    }

    private static class f<T> implements Iterator<T> {
        private final Iterator<T> a;

        public f(Iterator<T> it) {
            this.a = it;
        }

        public boolean hasNext() {
            return this.a.hasNext();
        }

        public T next() {
            return this.a.next();
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    protected static abstract class a<E> implements Collection<E> {
        /* access modifiers changed from: package-private */
        public abstract Collection<E> a();

        protected a() {
        }

        public final boolean contains(Object o) {
            return a().contains(o);
        }

        public final boolean containsAll(Collection<?> c) {
            return a().containsAll(c);
        }

        public final Iterator<E> iterator() {
            return new f(a().iterator());
        }

        public final boolean isEmpty() {
            return a().isEmpty();
        }

        public final int size() {
            return a().size();
        }

        public final Object[] toArray() {
            return a().toArray();
        }

        public final <T> T[] toArray(T[] a) {
            return a().toArray(a);
        }

        public int hashCode() {
            return a().hashCode();
        }

        public boolean equals(Object obj) {
            return a().equals(obj);
        }

        public String toString() {
            return a().toString();
        }

        public final boolean add(E e) {
            throw new UnsupportedOperationException();
        }

        public final boolean addAll(Collection<? extends E> collection) {
            throw new UnsupportedOperationException();
        }
    }

    private boolean a(Object obj, Object obj2) {
        if (obj == null) {
            return obj2 == null;
        }
        return obj.equals(obj2);
    }

    public static abstract class h<K, V> {

        public enum a {
            STABLE {
                /* access modifiers changed from: package-private */
                public <K, V, M extends Map<K, V>> h<K, V> a(ba<K, V, M> baVar) {
                    baVar.getClass();
                    return new c();
                }
            },
            LIVE {
                /* access modifiers changed from: package-private */
                public <K, V, M extends Map<K, V>> h<K, V> a(ba<K, V, M> baVar) {
                    baVar.getClass();
                    return new e();
                }
            };

            /* access modifiers changed from: package-private */
            public abstract <K, V, M extends Map<K, V>> h<K, V> a(ba<K, V, M> baVar);
        }

        /* access modifiers changed from: package-private */
        public abstract Set<K> a();

        /* access modifiers changed from: package-private */
        public abstract Set<Map.Entry<K, V>> b();

        /* access modifiers changed from: package-private */
        public abstract Collection<V> c();

        h() {
        }
    }

    final class c extends h<K, V> implements Serializable {
        c() {
        }

        public Set<K> a() {
            return Collections.unmodifiableSet(ba.this.a.keySet());
        }

        public Set<Map.Entry<K, V>> b() {
            return Collections.unmodifiableSet(ba.this.a.entrySet());
        }

        public Collection<V> c() {
            return Collections.unmodifiableCollection(ba.this.a.values());
        }
    }

    final class e extends h<K, V> implements Serializable {
        private final transient ba<K, V, M>.d b = new d();
        private final transient ba<K, V, M>.b c = new b();
        private final transient ba<K, V, M>.g d = new g();

        e() {
        }

        public Set<K> a() {
            return this.b;
        }

        public Set<Map.Entry<K, V>> b() {
            return this.c;
        }

        public Collection<V> c() {
            return this.d;
        }
    }
}
