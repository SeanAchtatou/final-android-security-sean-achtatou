package com.chartboost.sdk.Libraries;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.widget.ImageView;
import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.Libraries.a;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;

public class e {
    private static e a = null;
    private d b = new d(Chartboost.sharedChartboost().getContext());
    /* access modifiers changed from: private */
    public a c = new a();

    public interface b {
        void a(a.C0000a aVar, Bundle bundle);
    }

    public static synchronized e a() {
        e eVar;
        synchronized (e.class) {
            if (a == null) {
                a = new e();
            }
            eVar = a;
        }
        return eVar;
    }

    private e() {
    }

    public void b() {
        this.b.a();
        this.c.a();
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x002a  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0046  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.String r10, java.lang.String r11, com.chartboost.sdk.Libraries.e.b r12, android.widget.ImageView r13, android.os.Bundle r14) {
        /*
            r9 = this;
            r7 = 1
            r6 = 0
            r0 = 0
            if (r14 == 0) goto L_0x0039
            java.lang.String r1 = "paramNoMemoryCache"
            boolean r1 = r14.getBoolean(r1)
        L_0x000b:
            if (r1 != 0) goto L_0x0013
            com.chartboost.sdk.Libraries.a r2 = r9.c     // Catch:{ Exception -> 0x003d }
            com.chartboost.sdk.Libraries.a$a r0 = r2.a(r11)     // Catch:{ Exception -> 0x003d }
        L_0x0013:
            if (r0 != 0) goto L_0x0028
            com.chartboost.sdk.Libraries.a$a r0 = r9.a(r11)     // Catch:{ Exception -> 0x0058 }
            if (r0 == 0) goto L_0x0028
            if (r1 == 0) goto L_0x003b
            r2 = r6
        L_0x001e:
            r0.a(r2)     // Catch:{ Exception -> 0x0058 }
            if (r1 != 0) goto L_0x0028
            com.chartboost.sdk.Libraries.a r1 = r9.c     // Catch:{ Exception -> 0x0058 }
            r1.a(r11, r0)     // Catch:{ Exception -> 0x0058 }
        L_0x0028:
            if (r0 == 0) goto L_0x0046
            if (r13 == 0) goto L_0x0033
            android.graphics.Bitmap r1 = r0.b()
            r13.setImageBitmap(r1)
        L_0x0033:
            if (r12 == 0) goto L_0x0038
            r12.a(r0, r14)
        L_0x0038:
            return
        L_0x0039:
            r1 = r6
            goto L_0x000b
        L_0x003b:
            r2 = r7
            goto L_0x001e
        L_0x003d:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x0041:
            r0.printStackTrace()
            r0 = r1
            goto L_0x0028
        L_0x0046:
            com.chartboost.sdk.Libraries.e$a r0 = new com.chartboost.sdk.Libraries.e$a
            r1 = r9
            r2 = r13
            r3 = r12
            r4 = r11
            r5 = r14
            r0.<init>(r2, r3, r4, r5)
            java.lang.String[] r1 = new java.lang.String[r7]
            r1[r6] = r10
            r0.execute(r1)
            goto L_0x0038
        L_0x0058:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.Libraries.e.a(java.lang.String, java.lang.String, com.chartboost.sdk.Libraries.e$b, android.widget.ImageView, android.os.Bundle):void");
    }

    class a extends AsyncTask<String, Void, a.C0000a> {
        private String b;
        private final WeakReference<ImageView> c;
        private b d;
        private String e;
        private Bundle f;

        public a(ImageView imageView, b bVar, String str, Bundle bundle) {
            this.c = new WeakReference<>(imageView);
            c cVar = new c(this);
            if (imageView != null) {
                imageView.setImageDrawable(cVar);
            }
            this.e = str;
            this.d = bVar;
            this.f = bundle;
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0027  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0034  */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x00a7 A[SYNTHETIC, Splitter:B:38:0x00a7] */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public com.chartboost.sdk.Libraries.a.C0000a doInBackground(java.lang.String... r13) {
            /*
                r12 = this;
                r10 = 1
                r6 = 0
                r8 = 0
                java.lang.String r9 = "CBWebImageCache"
                r0 = r13[r8]
                r12.b = r0
                android.os.Bundle r0 = r12.f
                if (r0 == 0) goto L_0x0029
                android.os.Bundle r0 = r12.f
                java.lang.String r1 = "paramNoMemoryCache"
                boolean r0 = r0.getBoolean(r1)
            L_0x0015:
                com.chartboost.sdk.Libraries.e r1 = com.chartboost.sdk.Libraries.e.this     // Catch:{ Exception -> 0x002d }
                java.lang.String r2 = r12.e     // Catch:{ Exception -> 0x002d }
                com.chartboost.sdk.Libraries.a$a r1 = r1.a(r2)     // Catch:{ Exception -> 0x002d }
                if (r1 == 0) goto L_0x0025
                if (r0 == 0) goto L_0x002b
                r2 = r8
            L_0x0022:
                r1.a(r2)     // Catch:{ Exception -> 0x0134 }
            L_0x0025:
                if (r1 == 0) goto L_0x0034
                r0 = r1
            L_0x0028:
                return r0
            L_0x0029:
                r0 = r8
                goto L_0x0015
            L_0x002b:
                r2 = r10
                goto L_0x0022
            L_0x002d:
                r1 = move-exception
                r2 = r6
            L_0x002f:
                r1.printStackTrace()
                r1 = r2
                goto L_0x0025
            L_0x0034:
                org.apache.http.client.HttpClient r2 = com.chartboost.sdk.impl.j.b()
                org.apache.http.client.methods.HttpGet r3 = new org.apache.http.client.methods.HttpGet
                java.lang.String r4 = r12.b
                r3.<init>(r4)
                org.apache.http.HttpResponse r2 = r2.execute(r3)     // Catch:{ IOException -> 0x012d, IllegalStateException -> 0x00e6, Exception -> 0x0103 }
                org.apache.http.StatusLine r4 = r2.getStatusLine()     // Catch:{ IOException -> 0x012d, IllegalStateException -> 0x00e6, Exception -> 0x0103 }
                int r4 = r4.getStatusCode()     // Catch:{ IOException -> 0x012d, IllegalStateException -> 0x00e6, Exception -> 0x0103 }
                r5 = 200(0xc8, float:2.8E-43)
                if (r4 == r5) goto L_0x0071
                java.lang.String r0 = "ImageDownloader"
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x012d, IllegalStateException -> 0x00e6, Exception -> 0x0103 }
                java.lang.String r5 = "Error "
                r2.<init>(r5)     // Catch:{ IOException -> 0x012d, IllegalStateException -> 0x00e6, Exception -> 0x0103 }
                java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ IOException -> 0x012d, IllegalStateException -> 0x00e6, Exception -> 0x0103 }
                java.lang.String r4 = " while retrieving bitmap from "
                java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ IOException -> 0x012d, IllegalStateException -> 0x00e6, Exception -> 0x0103 }
                java.lang.String r4 = r12.b     // Catch:{ IOException -> 0x012d, IllegalStateException -> 0x00e6, Exception -> 0x0103 }
                java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ IOException -> 0x012d, IllegalStateException -> 0x00e6, Exception -> 0x0103 }
                java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x012d, IllegalStateException -> 0x00e6, Exception -> 0x0103 }
                android.util.Log.w(r0, r2)     // Catch:{ IOException -> 0x012d, IllegalStateException -> 0x00e6, Exception -> 0x0103 }
                r0 = r6
                goto L_0x0028
            L_0x0071:
                org.apache.http.HttpEntity r2 = r2.getEntity()     // Catch:{ IOException -> 0x012d, IllegalStateException -> 0x00e6, Exception -> 0x0103 }
                if (r2 == 0) goto L_0x011d
                java.io.InputStream r4 = r2.getContent()     // Catch:{ all -> 0x00d7 }
                com.chartboost.sdk.Libraries.e r5 = com.chartboost.sdk.Libraries.e.this     // Catch:{ all -> 0x012f }
                java.lang.String r6 = r12.e     // Catch:{ all -> 0x012f }
                com.chartboost.sdk.Libraries.e$e r7 = new com.chartboost.sdk.Libraries.e$e     // Catch:{ all -> 0x012f }
                r7.<init>(r4)     // Catch:{ all -> 0x012f }
                r5.a(r6, r7)     // Catch:{ all -> 0x012f }
                com.chartboost.sdk.Libraries.e r5 = com.chartboost.sdk.Libraries.e.this     // Catch:{ Exception -> 0x00d1 }
                java.lang.String r6 = r12.e     // Catch:{ Exception -> 0x00d1 }
                com.chartboost.sdk.Libraries.a$a r1 = r5.a(r6)     // Catch:{ Exception -> 0x00d1 }
                if (r1 == 0) goto L_0x013a
                if (r0 == 0) goto L_0x00cf
                r5 = r8
            L_0x0094:
                r1.a(r5)     // Catch:{ Exception -> 0x00d1 }
                if (r0 != 0) goto L_0x013a
                com.chartboost.sdk.Libraries.e r0 = com.chartboost.sdk.Libraries.e.this     // Catch:{ Exception -> 0x00d1 }
                com.chartboost.sdk.Libraries.a r0 = r0.c     // Catch:{ Exception -> 0x00d1 }
                java.lang.String r5 = r12.e     // Catch:{ Exception -> 0x00d1 }
                r0.a(r5, r1)     // Catch:{ Exception -> 0x00d1 }
                r0 = r1
            L_0x00a5:
                if (r4 == 0) goto L_0x00aa
                r4.close()     // Catch:{ IOException -> 0x00af, IllegalStateException -> 0x012b, Exception -> 0x0123 }
            L_0x00aa:
                r2.consumeContent()     // Catch:{ IOException -> 0x00af, IllegalStateException -> 0x012b, Exception -> 0x0123 }
                goto L_0x0028
            L_0x00af:
                r1 = move-exception
                r11 = r1
                r1 = r0
                r0 = r11
            L_0x00b3:
                r3.abort()
                java.lang.String r2 = "CBWebImageCache"
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                java.lang.String r3 = "I/O error while retrieving bitmap from "
                r2.<init>(r3)
                java.lang.String r3 = r12.b
                java.lang.StringBuilder r2 = r2.append(r3)
                java.lang.String r2 = r2.toString()
                android.util.Log.w(r9, r2, r0)
                r0 = r1
                goto L_0x0028
            L_0x00cf:
                r5 = r10
                goto L_0x0094
            L_0x00d1:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ all -> 0x012f }
                r0 = r1
                goto L_0x00a5
            L_0x00d7:
                r0 = move-exception
                r4 = r1
                r1 = r6
            L_0x00da:
                if (r1 == 0) goto L_0x00df
                r1.close()     // Catch:{ IOException -> 0x00e3, IllegalStateException -> 0x0128, Exception -> 0x0120 }
            L_0x00df:
                r2.consumeContent()     // Catch:{ IOException -> 0x00e3, IllegalStateException -> 0x0128, Exception -> 0x0120 }
                throw r0     // Catch:{ IOException -> 0x00e3, IllegalStateException -> 0x0128, Exception -> 0x0120 }
            L_0x00e3:
                r0 = move-exception
                r1 = r4
                goto L_0x00b3
            L_0x00e6:
                r0 = move-exception
                r0 = r1
            L_0x00e8:
                r3.abort()
                java.lang.String r1 = "CBWebImageCache"
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                java.lang.String r2 = "Incorrect URL: "
                r1.<init>(r2)
                java.lang.String r2 = r12.b
                java.lang.StringBuilder r1 = r1.append(r2)
                java.lang.String r1 = r1.toString()
                android.util.Log.w(r9, r1)
                goto L_0x0028
            L_0x0103:
                r0 = move-exception
            L_0x0104:
                r3.abort()
                java.lang.String r2 = "CBWebImageCache"
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                java.lang.String r3 = "Error while retrieving bitmap from "
                r2.<init>(r3)
                java.lang.String r3 = r12.b
                java.lang.StringBuilder r2 = r2.append(r3)
                java.lang.String r2 = r2.toString()
                android.util.Log.w(r9, r2, r0)
            L_0x011d:
                r0 = r1
                goto L_0x0028
            L_0x0120:
                r0 = move-exception
                r1 = r4
                goto L_0x0104
            L_0x0123:
                r1 = move-exception
                r11 = r1
                r1 = r0
                r0 = r11
                goto L_0x0104
            L_0x0128:
                r0 = move-exception
                r0 = r4
                goto L_0x00e8
            L_0x012b:
                r1 = move-exception
                goto L_0x00e8
            L_0x012d:
                r0 = move-exception
                goto L_0x00b3
            L_0x012f:
                r0 = move-exception
                r11 = r4
                r4 = r1
                r1 = r11
                goto L_0x00da
            L_0x0134:
                r2 = move-exception
                r11 = r2
                r2 = r1
                r1 = r11
                goto L_0x002f
            L_0x013a:
                r0 = r1
                goto L_0x00a5
            */
            throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.Libraries.e.a.doInBackground(java.lang.String[]):com.chartboost.sdk.Libraries.a$a");
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(a.C0000a aVar) {
            if (!isCancelled()) {
                if (aVar != null) {
                    try {
                        e.this.c.a(this.e, aVar);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
                if (this.c != null) {
                    ImageView imageView = this.c.get();
                    if (this == e.b(imageView)) {
                        imageView.setImageBitmap(aVar.b());
                    }
                }
                if (this.d != null) {
                    this.d.a(aVar, this.f);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public static a b(ImageView imageView) {
        if (imageView != null) {
            Drawable drawable = imageView.getDrawable();
            if (drawable instanceof c) {
                return ((c) drawable).a();
            }
        }
        return null;
    }

    static class c extends BitmapDrawable {
        private final WeakReference<a> a;

        public c(a aVar) {
            this.a = new WeakReference<>(aVar);
        }

        public a a() {
            return this.a.get();
        }
    }

    /* access modifiers changed from: protected */
    public a.C0000a a(String str) throws IOException {
        Bitmap bitmap;
        File a2 = this.b.a(String.valueOf(str) + ".png");
        if (!a2.exists()) {
            return null;
        }
        BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(a2));
        long length = a2.length();
        if (length > 2147483647L) {
            throw new IOException("Cannot read files larger than 2147483647 bytes");
        }
        int i = (int) length;
        byte[] bArr = new byte[i];
        bufferedInputStream.read(bArr, 0, i);
        bufferedInputStream.close();
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
        BitmapFactory.Options options2 = new BitmapFactory.Options();
        options2.inJustDecodeBounds = false;
        options2.inDither = false;
        options2.inPurgeable = true;
        options2.inInputShareable = true;
        options2.inTempStorage = new byte[32768];
        options2.inSampleSize = 1;
        while (true) {
            if (options2.inSampleSize >= 64) {
                bitmap = null;
                break;
            }
            try {
                bitmap = BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options2);
                break;
            } catch (OutOfMemoryError e) {
                options2.inSampleSize *= 2;
            } catch (Exception e2) {
                return null;
            }
        }
        return new a.C0000a(bitmap, options2.inSampleSize);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0043 A[SYNTHETIC, Splitter:B:24:0x0043] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(java.lang.String r6, com.chartboost.sdk.Libraries.e.C0001e r7) throws java.io.IOException {
        /*
            r5 = this;
            r4 = 0
            com.chartboost.sdk.Libraries.e$d r0 = r5.b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = java.lang.String.valueOf(r6)
            r1.<init>(r2)
            java.lang.String r2 = ".png"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            java.io.File r0 = r0.a(r1)
            r1 = 0
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0050, all -> 0x0040 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x0050, all -> 0x0040 }
            r0 = 4096(0x1000, float:5.74E-42)
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x0037, all -> 0x004d }
        L_0x0024:
            int r1 = r7.read(r0)     // Catch:{ Exception -> 0x0037, all -> 0x004d }
            r3 = -1
            if (r1 != r3) goto L_0x0032
            if (r2 == 0) goto L_0x0030
            r2.close()     // Catch:{ Exception -> 0x0047 }
        L_0x0030:
            r0 = 1
        L_0x0031:
            return r0
        L_0x0032:
            r3 = 0
            r2.write(r0, r3, r1)     // Catch:{ Exception -> 0x0037, all -> 0x004d }
            goto L_0x0024
        L_0x0037:
            r0 = move-exception
            r0 = r2
        L_0x0039:
            if (r0 == 0) goto L_0x003e
            r0.close()     // Catch:{ Exception -> 0x0049 }
        L_0x003e:
            r0 = r4
            goto L_0x0031
        L_0x0040:
            r0 = move-exception
        L_0x0041:
            if (r1 == 0) goto L_0x0046
            r1.close()     // Catch:{ Exception -> 0x004b }
        L_0x0046:
            throw r0
        L_0x0047:
            r0 = move-exception
            goto L_0x0030
        L_0x0049:
            r0 = move-exception
            goto L_0x003e
        L_0x004b:
            r1 = move-exception
            goto L_0x0046
        L_0x004d:
            r0 = move-exception
            r1 = r2
            goto L_0x0041
        L_0x0050:
            r0 = move-exception
            r0 = r1
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.Libraries.e.a(java.lang.String, com.chartboost.sdk.Libraries.e$e):boolean");
    }

    /* renamed from: com.chartboost.sdk.Libraries.e$e  reason: collision with other inner class name */
    static class C0001e extends FilterInputStream {
        public C0001e(InputStream inputStream) {
            super(inputStream);
        }

        public long skip(long n) throws IOException {
            long j = 0;
            while (j < n) {
                long skip = this.in.skip(n - j);
                if (skip == 0) {
                    if (read() < 0) {
                        break;
                    }
                    skip = 1;
                }
                j += skip;
            }
            return j;
        }
    }

    private static class d {
        private File a = null;

        public d(Context context) {
            try {
                if ((context.checkCallingOrSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE") == 0) && Environment.getExternalStorageState().equals("mounted")) {
                    this.a = context.getExternalFilesDir("cache");
                }
                if (this.a != null && !this.a.exists()) {
                    this.a.mkdirs();
                }
            } catch (Exception e) {
                this.a = null;
            }
            if (this.a == null) {
                this.a = context.getCacheDir();
                if (!this.a.exists()) {
                    this.a.mkdirs();
                }
            }
        }

        public File a(String str) {
            return new File(this.a, str);
        }

        public void a() {
            try {
                File[] listFiles = this.a.listFiles();
                if (listFiles != null) {
                    for (File delete : listFiles) {
                        delete.delete();
                    }
                }
            } catch (Exception e) {
            }
        }
    }
}
