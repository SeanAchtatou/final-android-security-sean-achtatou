package com.vungle.sdk;

import com.vungle.sdk.ah;
import com.vungle.sdk.aj;

/* compiled from: vungle */
class ak implements ah.a {
    aj.a a;
    final /* synthetic */ aj b;

    ak(aj ajVar) {
        this.b = ajVar;
    }

    public ah.a a(aj.a aVar) {
        this.a = aVar;
        return this;
    }

    public void a(ah.c cVar) {
        as.a("RequestAd", "Acquired advert:");
        as.a("RequestAd", " -- Directory:  " + String.valueOf(cVar.a()));
        as.a("RequestAd", " -- PreRoll:    " + String.valueOf(cVar.c()));
        as.a("RequestAd", " -- PostRoll:   " + String.valueOf(cVar.d()));
        as.a("RequestAd", " -- Video:      " + String.valueOf(cVar.b()));
        as.a("RequestAd", " -- Expiration: " + String.valueOf(cVar.e()));
        ai.j = true;
        ai.k = false;
        this.a.a(d.k);
    }

    public void a() {
        as.c("RequestAd", "Failed to acquire advert.");
        ai.j = false;
        ai.k = false;
        this.a.a(d.k);
    }
}
