package com.vungle.sdk;

import android.content.Context;
import android.webkit.WebView;
import android.widget.FrameLayout;

/* compiled from: vungle */
class o {
    private FrameLayout a;
    private ag b;
    private WebView c;

    public o(Context context) {
        this.a = new FrameLayout(context);
        this.b = new ag(context);
        this.c = new WebView(context);
        this.a.addView(this.b);
        this.b.addView(this.c);
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.b.getLayoutParams();
        layoutParams.gravity = 17;
        layoutParams.width = -1;
        layoutParams.height = -1;
        this.b.setLayoutParams(layoutParams);
    }

    public FrameLayout a() {
        return this.a;
    }

    public WebView b() {
        return this.c;
    }
}
