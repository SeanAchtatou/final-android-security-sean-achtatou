package com.vungle.sdk;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;
import java.io.IOException;

/* compiled from: vungle */
class m {
    private static Bitmap h = null;
    private static Bitmap i = null;
    private static Bitmap j = null;
    RelativeLayout a;
    VideoView b;
    ProgressBar c;
    RelativeLayout d;
    TextView e;
    ImageView f;
    ImageView g;

    private void a(Context context) {
        if (h == null) {
            try {
                h = BitmapFactory.decodeStream(context.getAssets().open("vunglepub_sdk_close.png"));
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        if (i == null) {
            try {
                i = BitmapFactory.decodeStream(context.getAssets().open("vunglepub_sdk_mute.png"));
            } catch (IOException e3) {
                e3.printStackTrace();
            }
        }
        if (j == null) {
            try {
                j = BitmapFactory.decodeStream(context.getAssets().open("vunglepub_sdk_unmute.png"));
            } catch (IOException e4) {
                e4.printStackTrace();
            }
        }
    }

    private void a(ImageView imageView, float f2) {
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        imageView.setMinimumWidth((int) (42.0f * f2));
        imageView.setMinimumHeight((int) (42.0f * f2));
    }

    public m(Context context) {
        float f2 = context.getResources().getDisplayMetrics().density;
        a(context);
        this.a = new RelativeLayout(context);
        this.b = new VideoView(context);
        this.c = new ProgressBar(context);
        this.d = new RelativeLayout(context);
        this.e = new TextView(context);
        this.f = new ImageView(context);
        this.g = new ImageView(context);
        this.a.addView(this.b);
        this.a.addView(this.c);
        this.a.addView(this.d);
        this.d.addView(this.e);
        this.d.addView(this.f);
        this.a.addView(this.g);
        a(this.f, f2);
        a(this.g, f2);
        this.f.setImageBitmap(h);
        this.g.setImageBitmap(i);
        this.e.setTextAppearance(context, 16973892);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.c.getLayoutParams();
        layoutParams.addRule(13);
        this.c.setLayoutParams(layoutParams);
        RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.b.getLayoutParams();
        layoutParams2.addRule(13);
        this.b.setLayoutParams(layoutParams2);
        RelativeLayout.LayoutParams layoutParams3 = (RelativeLayout.LayoutParams) this.d.getLayoutParams();
        layoutParams3.addRule(10);
        layoutParams3.addRule(14);
        this.d.setLayoutParams(layoutParams3);
        RelativeLayout.LayoutParams layoutParams4 = (RelativeLayout.LayoutParams) this.e.getLayoutParams();
        layoutParams4.addRule(9);
        layoutParams4.addRule(15);
        layoutParams4.setMargins(Math.round(f2 * 5.0f), 0, 0, 0);
        this.e.setLayoutParams(layoutParams4);
        RelativeLayout.LayoutParams layoutParams5 = (RelativeLayout.LayoutParams) this.f.getLayoutParams();
        layoutParams5.addRule(11);
        layoutParams5.addRule(15);
        this.f.setLayoutParams(layoutParams5);
        RelativeLayout.LayoutParams layoutParams6 = (RelativeLayout.LayoutParams) this.g.getLayoutParams();
        layoutParams6.addRule(11);
        layoutParams6.addRule(12);
        this.g.setLayoutParams(layoutParams6);
    }

    public void a(boolean z) {
        this.g.setImageBitmap(z ? i : j);
    }

    public RelativeLayout a() {
        return this.a;
    }

    public ProgressBar b() {
        return this.c;
    }

    public TextView c() {
        return this.e;
    }

    public ImageView d() {
        return this.f;
    }

    public ImageView e() {
        return this.g;
    }

    public VideoView f() {
        return this.b;
    }
}
