package com.vungle.sdk;

import com.vungle.sdk.x;

/* compiled from: vungle */
class r extends x.g {
    final /* synthetic */ VungleAdvert a;

    r(VungleAdvert vungleAdvert) {
        this.a = vungleAdvert;
    }

    public void a(long j, long j2) {
        as.a("Video-callback", "Video Completion. Len: " + (((double) j2) / 1000.0d));
        av.d().a(j);
        av.d().c(j2);
        av.d().b(j2);
        av.c().b().add(VungleAdvert.f());
        int unused = this.a.g = 3;
        this.a.e();
    }

    public void a(long j, long j2, long j3) {
        as.a("Video-callback", "Video Cancelled. Watched " + (((double) j2) / 1000.0d) + " of " + (((double) j3) / 1000.0d));
        av.d().a(j);
        av.d().c(j2);
        av.d().b(j3);
        av.c().b().add(VungleAdvert.f());
        int unused = this.a.g = 3;
        this.a.e();
    }

    public void a(long j) {
        as.a("Video-callback", "Video Error. :(");
        av.c().a("videoerror");
        int unused = this.a.g = 3;
        this.a.e();
    }
}
