package com.vungle.sdk;

import android.content.SharedPreferences;
import com.flurry.android.FlurryFullscreenTakeoverActivity;
import com.vungle.sdk.VunglePub;

/* compiled from: vungle */
class d {
    public static String A = "pre_";
    public static String B = "";
    public static String C = "OK";
    public static String D = "DownloadStats";
    public static String E = "id";
    public static String F = FlurryFullscreenTakeoverActivity.EXTRA_KEY_URL;
    public static String G = "app_id";
    public static String H = "chk";
    public static String I = "campaign";
    public static String J = "postBundle";
    public static String K = "preBundle";
    public static String L = "expiry";
    public static String M = "callToActionUrl";
    public static String N = "showClose";
    public static String O = "countdown";
    public static String P = "md5";
    public static String Q = "sleep";
    public static String R = "videoHeight";
    public static String S = "videoWidth";
    public static String T = "size";
    public static String U = "asyncThreshold";
    public static String V = "retryCount";
    public static String W = "VUNGLE_PUB_APP_INFO";
    public static String X = "IsVgAppInstalled";
    public static String Y = "VgLastViewedTime";
    public static String Z = "VgAdDelayDuration";
    public static int a = 0;
    public static String aa = "ACTIVITY_LAUNCH_MODE";
    public static String ab = "webPath";
    public static String ac = "delay";
    public static int ad = 0;
    private static int ae = 0;
    private static String[] af = {"http://api.vungle.com", "http://acceptance.vungle.com", "http://localhost:3000"};
    private static String[] ag = {"/api/v1/requestAd", "/api/v2/requestAd"};
    private static String[] ah = {"/api/v1/reportAd", "/api/v2/reportAd"};
    private static String[] ai = {"/api/v1/new", "/api/v2/new"};
    private static String[] aj = {"/api/v1/sessionStart", "/api/v2/sessionStart"};
    private static String[] ak = {"/api/v1/sessionEnd", "/api/v2/sessionEnd"};
    private static String[] al = {"/api/v1/unfilled", "/api/v2/unfilled"};
    private static int am = 0;
    private static long an = 0;
    private static VunglePub.EventListener ao = null;
    private static Object ap = new Object();
    public static int b = 0;
    public static int c = 1;
    public static int d = 2;
    public static int e = 3;
    public static int f = 3;
    public static int g = 30000;
    public static int h = 0;
    public static int i = 1;
    public static int j = 45;
    public static int k = 1;
    public static int l = 2;
    public static int m = 1000;
    public static int n = 500;
    public static int o = 101;
    public static int p = 102;
    public static long q = 15000;
    public static long r = 15000;
    public static long s = 1000;
    public static String t = "Vungle";
    public static String u = "VungleException";
    public static String v = "Stats";
    public static String w = "VungleDownload";
    public static String x = "Vungle_Media";
    public static String y = "adPayload";
    public static String z = "post_";

    public static void a(VunglePub.EventListener eventListener) {
        synchronized (ap) {
            ao = eventListener;
        }
    }

    private static String k() {
        return af[0];
    }

    public static String a() {
        return k() + ag[0];
    }

    public static String b() {
        return k() + ah[0];
    }

    public static String c() {
        return k() + ai[0];
    }

    public static String d() {
        return k() + aj[0];
    }

    public static String e() {
        return k() + ak[0];
    }

    public static String f() {
        return k() + al[0];
    }

    public static int g() {
        return am;
    }

    public static void a(int i2) {
        as.b("AD_DELAY", "Set to " + i2 + " seconds.");
        am = i2;
        SharedPreferences.Editor edit = ai.e().getSharedPreferences(W, 0).edit();
        edit.putInt(Z, i2);
        edit.commit();
    }

    public static long h() {
        return an;
    }

    public static void a(long j2) {
        an = j2;
        SharedPreferences.Editor edit = ai.e().getSharedPreferences(W, 0).edit();
        edit.putLong(Y, j2);
        edit.commit();
    }

    public static void i() {
        as.a("VungleAdStart", "Ad started");
        try {
            synchronized (ap) {
                if (ao != null) {
                    e eVar = new e(ao);
                    as.a("User Callback", "Sending onVungleAdStart.");
                    new Thread(eVar, "AdStartedCallbackThread").start();
                }
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public static void j() {
        as.a("VungleAdEnd", "Ad ended");
        try {
            synchronized (ap) {
                if (ao != null) {
                    f fVar = new f(ao);
                    as.a("User Callback", "Sending onVungleAdEnd.");
                    new Thread(fVar, "AdEndCallbackThread").start();
                }
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public static void a(double d2, double d3) {
        as.a("VungleAdView", "End of ad. User viewed: " + d2 + " / " + d3 + " seconds.");
        try {
            synchronized (ap) {
                if (ao != null) {
                    a a2 = new g(ao).a(d2, d3);
                    as.a("User Callback", "Sending onVungleView.");
                    new Thread(a2, "AdViewCallbackThread").start();
                }
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    /* compiled from: vungle */
    private static abstract class a implements Runnable {
        protected VunglePub.EventListener a;

        public a(VunglePub.EventListener eventListener) {
            this.a = eventListener;
        }
    }
}
