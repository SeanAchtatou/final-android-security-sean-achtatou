package com.vungle.sdk;

import android.content.Context;
import com.flurry.android.AdCreative;
import com.vungle.sdk.af;

/* compiled from: vungle */
class w extends af {
    /* access modifiers changed from: private */
    public b d;

    /* compiled from: vungle */
    public interface b {
        void a();

        void a(String str);

        void b();

        void c();
    }

    public w(Context context, String str, Object obj) {
        super(context, str, obj);
        this.d = (b) obj;
    }

    /* access modifiers changed from: protected */
    public af.a d() {
        return new a();
    }

    /* access modifiers changed from: protected */
    public void a(Context context) {
        o oVar = new o(context);
        this.b = oVar.a();
        this.a = oVar.b();
    }

    /* compiled from: vungle */
    private class a extends af.a {
        private a() {
            super();
        }

        public boolean a(String str, String str2) {
            if (str.equalsIgnoreCase("close")) {
                w.this.d.b();
                return true;
            } else if (str.equalsIgnoreCase("watch")) {
                w.this.d.a();
                return true;
            } else if (str.equalsIgnoreCase("download")) {
                w.this.d.c();
                return true;
            } else if (!str.equalsIgnoreCase(AdCreative.kFormatCustom)) {
                return false;
            } else {
                w.this.d.a(str2);
                return true;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void e() {
        this.d.a();
    }
}
