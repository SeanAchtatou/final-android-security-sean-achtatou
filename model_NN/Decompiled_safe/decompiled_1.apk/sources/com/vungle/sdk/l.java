package com.vungle.sdk;

import android.content.Context;
import android.widget.ViewFlipper;

/* compiled from: vungle */
class l extends ViewFlipper {
    public l(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        try {
            super.onDetachedFromWindow();
        } catch (IllegalArgumentException e) {
            stopFlipping();
        }
    }
}
