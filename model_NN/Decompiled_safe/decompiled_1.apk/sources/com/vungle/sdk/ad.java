package com.vungle.sdk;

import android.content.DialogInterface;
import com.vungle.sdk.x;

/* compiled from: vungle */
class ad implements DialogInterface.OnClickListener {
    final /* synthetic */ x.a a;

    ad(x.a aVar) {
        this.a = aVar;
    }

    public void onClick(DialogInterface dialog, int which) {
        long currentPosition = (long) x.this.g.getCurrentPosition();
        long duration = (long) x.this.g.getDuration();
        as.a("Callback", "Cancel Vid");
        x.this.g.stopPlayback();
        x.this.g();
        x.this.i();
        x.this.A.a(x.this.x, currentPosition, duration);
    }
}
