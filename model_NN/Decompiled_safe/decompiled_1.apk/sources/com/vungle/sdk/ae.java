package com.vungle.sdk;

import android.content.DialogInterface;
import com.vungle.sdk.x;

/* compiled from: vungle */
class ae implements DialogInterface.OnCancelListener {
    final /* synthetic */ x.a a;

    ae(x.a aVar) {
        this.a = aVar;
    }

    public void onCancel(DialogInterface dialog) {
        as.a("Callback", "Dismiss Dialog");
        x.this.g.start();
        boolean unused = this.a.b = false;
    }
}
