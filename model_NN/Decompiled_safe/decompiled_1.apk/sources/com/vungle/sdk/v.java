package com.vungle.sdk;

import android.content.Context;
import android.os.Bundle;
import com.flurry.android.AdCreative;
import com.vungle.sdk.af;

/* compiled from: vungle */
class v extends af {
    /* access modifiers changed from: private */
    public b d;

    /* compiled from: vungle */
    public interface b {
        void a();

        void a(String str);

        void b();

        void c();
    }

    public v(Context context, Bundle bundle, Object obj) {
        super(context, bundle, obj);
        this.d = (b) obj;
    }

    public v(Context context, String str, Object obj) {
        super(context, str, obj);
        this.d = (b) obj;
    }

    /* access modifiers changed from: protected */
    public af.a d() {
        return new a();
    }

    /* access modifiers changed from: protected */
    public void a(Context context) {
        n nVar = new n(context);
        this.b = nVar.a();
        this.a = nVar.b();
    }

    /* compiled from: vungle */
    private class a extends af.a {
        private a() {
            super();
        }

        public boolean a(String str, String str2) {
            if (str.equalsIgnoreCase("close")) {
                v.this.d.a();
                return true;
            } else if (str.equalsIgnoreCase("download")) {
                v.this.d.b();
                return true;
            } else if (str.equalsIgnoreCase("replay")) {
                v.this.d.c();
                return true;
            } else if (!str.equalsIgnoreCase(AdCreative.kFormatCustom)) {
                return false;
            } else {
                v.this.d.a(str2);
                return true;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void e() {
        this.d.a();
    }
}
