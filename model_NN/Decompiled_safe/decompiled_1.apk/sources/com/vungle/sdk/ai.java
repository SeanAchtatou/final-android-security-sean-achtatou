package com.vungle.sdk;

import android.content.Context;

/* compiled from: vungle */
class ai {
    public static float A = 1.0f;
    public static int B = d.b;
    public static int C = 0;
    public static boolean D = false;
    public static String E = null;
    public static boolean F = true;
    private static Context G;
    private static boolean H = false;
    private static int I = 3;
    private static int J = 5;
    private static int K = -1;
    private static long L = d.q;
    private static long M = d.q;
    private static long N = ((long) d.g);
    public static String a = "";
    public static String b = "";
    public static String c = "";
    public static String d = "";
    public static String e = "";
    public static String f = "";
    public static String g = "";
    public static String h = "";
    public static boolean i = false;
    public static boolean j = false;
    public static Boolean k = false;
    public static Boolean l = false;
    public static boolean m = false;
    public static boolean n = false;
    public static boolean o = false;
    public static boolean p = true;
    public static boolean q = true;
    public static boolean r = false;
    public static boolean s = true;
    public static boolean t = false;
    public static boolean u = false;
    public static float v = 960.0f;
    public static float w = 540.0f;
    public static float x = 960.0f;
    public static float y = 540.0f;
    public static float z = 1.0f;

    public static long a() {
        if (L < d.q) {
            L = 300000;
        }
        return L;
    }

    public static void a(long j2) {
        if (j2 < d.q) {
            L = 300000;
        }
        L = j2;
    }

    public static long b() {
        return M;
    }

    public static int c() {
        return I;
    }

    public static void a(int i2) {
        I = i2;
    }

    public static void b(long j2) {
        N = j2;
    }

    public static boolean d() {
        return H;
    }

    public static void a(boolean z2) {
        if (H && !z2) {
            as.b("setVideoAdInFocus()", "Resetting last viewed time.");
            d.a(System.currentTimeMillis());
            d.j();
        } else if (!H && z2) {
            d.i();
        }
        H = z2;
    }

    public static Context e() {
        return G;
    }

    public static void a(Context context) {
        G = context.getApplicationContext();
    }
}
