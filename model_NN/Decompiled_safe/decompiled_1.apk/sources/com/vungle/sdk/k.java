package com.vungle.sdk;

import android.net.Uri;

/* compiled from: vungle */
class k {

    /* compiled from: vungle */
    private static class a implements Runnable {
        private String a;
        private String b;
        private String c;

        public a(String str, String str2, String str3) {
            this.a = str;
            this.b = str2;
            this.c = str3;
        }

        public void run() {
            Uri.Builder buildUpon = Uri.parse(this.a).buildUpon();
            buildUpon.appendQueryParameter("isu", this.c);
            buildUpon.appendQueryParameter("ut", String.valueOf(System.currentTimeMillis() / 1000));
            buildUpon.appendQueryParameter("app_id", this.b);
            Uri build = buildUpon.build();
            as.b("UnfilledAdRequest", "Sending to: " + build);
            ar.a(build.toString(), "");
        }
    }

    public static void a(String str, String str2) {
        new Thread(new a(d.f(), str, str2), "UnfilledAdRequestThread").start();
    }
}
