package com.vungle.sdk;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import com.vungle.sdk.u;
import com.vungle.sdk.v;
import java.io.File;

/* compiled from: vungle */
public class VungleAdvertTest extends Activity {
    private String a = null;
    private View b = null;
    private v c = null;

    public void onCreate(Bundle b2) {
        super.onCreate(b2);
        as.b("testBundle", "onCreate" + (b2 == null ? "(null)" : "(bundle)"));
        if (b2 != null) {
            this.c = new v(this, b2, new a());
            this.b = this.c.a();
            setContentView(this.b);
            return;
        }
        if (getIntent().hasExtra("bundle_path")) {
            this.a = getIntent().getStringExtra("bundle_path");
        }
        try {
            this.c = new v(this, this.a, new a());
            this.b = this.c.a();
            setContentView(this.b);
        } catch (u.a e) {
            as.c("testBundle", "Failed to load html file: " + this.a);
            String b3 = ah.b();
            if (b3 != null) {
                ah.a(new File(b3, "__POST_BUNDLE_TEMP_DIR__"));
                ai.a(false);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle out) {
        super.onSaveInstanceState(out);
        this.c.a(out);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        VunglePub.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        VunglePub.onResume();
    }

    public void onBackPressed() {
        super.onBackPressed();
        Toast.makeText(this, "Back button pressed.", 1).show();
        ah.a(new File(ah.b(), "__POST_BUNDLE_TEMP_DIR__"));
        ai.a(false);
        finish();
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        Toast.makeText(this, "'" + str + "' event triggered.", 1).show();
        ah.a(new File(ah.b(), "__POST_BUNDLE_TEMP_DIR__"));
        ai.a(false);
        finish();
    }

    /* compiled from: vungle */
    private class a implements v.b {
        private a() {
        }

        public void a() {
            VungleAdvertTest.this.a("CLOSE");
        }

        public void b() {
            VungleAdvertTest.this.a("DOWNLOAD");
        }

        public void c() {
            VungleAdvertTest.this.a("REPLAY");
        }

        public void a(String str) {
            VungleAdvertTest.this.a("CUSTOM:" + str);
        }
    }
}
