package com.vungle.sdk;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.webkit.JsPromptResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.vungle.sdk.u;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: vungle */
abstract class af extends u {
    protected WebView a;
    protected View b;
    protected Object c = null;

    /* access modifiers changed from: protected */
    public abstract a d();

    /* access modifiers changed from: protected */
    public abstract void e();

    private void a(WebView webView, Object obj) {
        WebSettings settings = webView.getSettings();
        settings.setBuiltInZoomControls(false);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setSavePassword(true);
        settings.setSaveFormData(true);
        settings.setJavaScriptEnabled(true);
        settings.setLoadsImagesAutomatically(true);
        webView.setBackgroundColor(0);
        webView.setBackgroundResource(0);
        webView.setWebChromeClient(d());
        webView.setWebViewClient(new b());
    }

    public af(Context context, Bundle bundle, Object obj) {
        this.c = obj;
        a(context);
        a(this.a, obj);
        this.a.restoreState(bundle);
        ((Activity) context).setRequestedOrientation(-1);
    }

    public af(Context context, String str, Object obj) {
        if (str == null || str.length() == 0 || (!str.endsWith(".html") && !str.endsWith(".htm"))) {
            throw new u.a();
        }
        as.b("StageWeb", "Using post-roll: " + str);
        this.c = obj;
        a(context);
        a(this.a, obj);
        ((Activity) context).setRequestedOrientation(-1);
        this.a.loadUrl(str);
    }

    public View a() {
        return this.b;
    }

    public void b() {
    }

    public void c() {
    }

    public void a(Bundle bundle) {
        this.a.saveState(bundle);
    }

    /* compiled from: vungle */
    public abstract class a extends WebChromeClient {
        public abstract boolean a(String str, String str2);

        public a() {
        }

        public boolean onJsPrompt(WebView view, String url, String message, String defaultValue, JsPromptResult result) {
            if (!message.startsWith("showToast")) {
                return false;
            }
            try {
                JSONObject jSONObject = new JSONObject(message.substring("showToast".length()));
                JSONArray jSONArray = jSONObject.getJSONArray("params");
                String string = jSONObject.getString("method");
                if (!string.equalsIgnoreCase("showToast")) {
                    as.d("StageWeb", "Incorrect function primitive: " + string);
                    result.confirm(null);
                    return true;
                } else if (af.this.c == null) {
                    as.d("StageWeb", "No handler provided.");
                    result.confirm(null);
                    return true;
                } else {
                    String lowerCase = jSONArray.getString(0).trim().toLowerCase();
                    if (!a(lowerCase, jSONArray.length() > 1 ? jSONArray.getString(1) : null)) {
                        as.d("StageWeb", "Invalid event triggered: " + lowerCase);
                    }
                    result.confirm("{\"result\":0}");
                    return true;
                }
            } catch (JSONException e) {
                as.a("JsPrompt", "Callback executed with a bad JSON format.", e);
            }
        }
    }

    /* compiled from: vungle */
    public class b extends WebViewClient {
        public b() {
        }

        public void onReceivedError(WebView v, int code, String desc, String url) {
            as.c("StageWeb", "Failed with Error " + code + ": " + desc);
            af.this.e();
        }

        public void onPageFinished(WebView wv, String url) {
            if (!url.toLowerCase().startsWith("javascript:")) {
                String str = "javascript:function actionClicked(t,u){ var r = prompt('showToast'+JSON.stringify({method:'showToast',params:(u?[t,u]:[t])}));if(r&&typeof r === 'string'){return JSON.parse(r).result;}}; function noTapHighlight() { var l=document.getElementsByTagName('*');for(var i=0; i<l.length; i++) {l[i].style.webkitTapHighlightColor='rgba(0,0,0,0)';}};noTapHighlight();";
                String b = ax.b(ai.e());
                if (b != null) {
                    str = str + "var _device_id = \"" + b.replace("\"", "\\\"") + "\";";
                }
                af.this.a.loadUrl(str);
            }
        }
    }
}
