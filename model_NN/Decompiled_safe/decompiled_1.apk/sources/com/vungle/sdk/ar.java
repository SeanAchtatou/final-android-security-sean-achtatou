package com.vungle.sdk;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/* compiled from: vungle */
class ar {
    public static String a(String str, String str2) {
        return a(str, str2, false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0112  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0160  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(java.lang.String r6, java.lang.String r7, boolean r8) {
        /*
            r0 = 0
            java.lang.String r1 = com.vungle.sdk.d.t     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            r2.<init>()     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            java.lang.String r3 = "Request URL : "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            java.lang.String r2 = r2.toString()     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            com.vungle.sdk.as.b(r1, r2)     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            java.lang.String r1 = com.vungle.sdk.d.t     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            r2.<init>()     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            java.lang.String r3 = "Post Data : "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            java.lang.String r2 = r2.toString()     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            com.vungle.sdk.as.b(r1, r2)     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            java.net.URL r1 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            r1.<init>(r6)     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            java.net.URLConnection r6 = r1.openConnection()     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            java.net.HttpURLConnection r6 = (java.net.HttpURLConnection) r6     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            java.lang.String r1 = "POST"
            r6.setRequestMethod(r1)     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            r1 = 1
            r6.setDoOutput(r1)     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            r1 = 0
            r6.setUseCaches(r1)     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            java.lang.String r1 = "Content-Type"
            java.lang.String r2 = "application/json"
            r6.setRequestProperty(r1, r2)     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            java.lang.String r1 = "User-Agent"
            java.lang.String r2 = "VungleDroid/1.2.2"
            r6.setRequestProperty(r1, r2)     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            java.lang.String r1 = "X-VUNGLE-TIMEZONE"
            java.lang.String r2 = com.vungle.sdk.ax.c()     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            r6.setRequestProperty(r1, r2)     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            java.lang.String r1 = "X-VUNGLE-LANGUAGE"
            java.lang.String r2 = com.vungle.sdk.ax.b()     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            r6.setRequestProperty(r1, r2)     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            java.lang.String r1 = "X-VUNGLE-BUNDLE-ID"
            android.content.Context r2 = com.vungle.sdk.ai.e()     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            java.lang.String r2 = r2.getPackageName()     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            r6.setRequestProperty(r1, r2)     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            java.lang.String r1 = "PackageName"
            android.content.Context r2 = com.vungle.sdk.ai.e()     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            java.lang.String r2 = r2.getPackageName()     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            com.vungle.sdk.as.a(r1, r2)     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            if (r8 == 0) goto L_0x00aa
            java.lang.String r1 = "X-VUNG-AUTHORIZATION"
            java.lang.String r2 = com.vungle.sdk.ax.d(r7)     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            r6.setRequestProperty(r1, r2)     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            java.lang.String r1 = "X-VUNG-DATE"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            r2.<init>()     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            java.lang.String r3 = ""
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            java.lang.String r2 = r2.toString()     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            r6.setRequestProperty(r1, r2)     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
        L_0x00aa:
            java.io.OutputStream r1 = r6.getOutputStream()     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            byte[] r2 = r7.getBytes()     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            r1.write(r2)     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            java.lang.String r1 = com.vungle.sdk.d.t     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            r2.<init>()     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            java.lang.String r3 = "POST Response Code : "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            int r3 = r6.getResponseCode()     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            java.lang.String r2 = r2.toString()     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            com.vungle.sdk.as.b(r1, r2)     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            int r1 = r6.getResponseCode()     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            r2 = 200(0xc8, float:2.8E-43)
            if (r1 != r2) goto L_0x0110
            java.lang.Object r6 = r6.getContent()     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            java.io.InputStream r6 = (java.io.InputStream) r6     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            r1.<init>()     // Catch:{ MalformedURLException -> 0x0165, IOException -> 0x013e }
            r0 = 1024(0x400, float:1.435E-42)
            char[] r0 = new char[r0]     // Catch:{ MalformedURLException -> 0x0107, IOException -> 0x0163 }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ all -> 0x0100 }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ all -> 0x0100 }
            java.lang.String r4 = "UTF-8"
            r3.<init>(r6, r4)     // Catch:{ all -> 0x0100 }
            r2.<init>(r3)     // Catch:{ all -> 0x0100 }
        L_0x00f4:
            int r3 = r2.read(r0)     // Catch:{ all -> 0x0100 }
            r4 = -1
            if (r3 == r4) goto L_0x0117
            r4 = 0
            r1.append(r0, r4, r3)     // Catch:{ all -> 0x0100 }
            goto L_0x00f4
        L_0x0100:
            r0 = move-exception
            if (r6 == 0) goto L_0x0106
            r6.close()     // Catch:{ MalformedURLException -> 0x0107, IOException -> 0x0163 }
        L_0x0106:
            throw r0     // Catch:{ MalformedURLException -> 0x0107, IOException -> 0x0163 }
        L_0x0107:
            r0 = move-exception
        L_0x0108:
            java.lang.String r2 = com.vungle.sdk.d.u
            java.lang.String r3 = "MalformedURLException"
            com.vungle.sdk.as.a(r2, r3, r0)
            r0 = r1
        L_0x0110:
            if (r0 == 0) goto L_0x0160
            java.lang.String r0 = r0.toString()
        L_0x0116:
            return r0
        L_0x0117:
            java.lang.String r0 = com.vungle.sdk.d.t     // Catch:{ all -> 0x0100 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0100 }
            r2.<init>()     // Catch:{ all -> 0x0100 }
            java.lang.String r3 = "POST Response : "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0100 }
            java.lang.String r3 = r1.toString()     // Catch:{ all -> 0x0100 }
            java.lang.String r3 = r3.trim()     // Catch:{ all -> 0x0100 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0100 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0100 }
            com.vungle.sdk.as.b(r0, r2)     // Catch:{ all -> 0x0100 }
            if (r6 == 0) goto L_0x015e
            r6.close()     // Catch:{ MalformedURLException -> 0x0107, IOException -> 0x0163 }
            r0 = r1
            goto L_0x0110
        L_0x013e:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0142:
            java.lang.String r2 = com.vungle.sdk.d.u
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "IOException: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r0 = r0.toString()
            com.vungle.sdk.as.d(r2, r0)
        L_0x015e:
            r0 = r1
            goto L_0x0110
        L_0x0160:
            java.lang.String r0 = com.vungle.sdk.d.B
            goto L_0x0116
        L_0x0163:
            r0 = move-exception
            goto L_0x0142
        L_0x0165:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0108
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.sdk.ar.a(java.lang.String, java.lang.String, boolean):java.lang.String");
    }

    public static ArrayList<String> a(String str) {
        InputStream inputStream;
        ArrayList<String> arrayList = new ArrayList<>();
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setConnectTimeout(30000);
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.connect();
            as.b(d.t, "GET Response Code : " + httpURLConnection.getResponseCode());
            arrayList.add(0, httpURLConnection.getResponseMessage());
            if (httpURLConnection.getResponseCode() == 200) {
                inputStream = httpURLConnection.getInputStream();
                StringBuilder sb = new StringBuilder();
                char[] cArr = new char[1024];
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                while (true) {
                    int read = bufferedReader.read(cArr);
                    if (read == -1) {
                        break;
                    }
                    sb.append(cArr, 0, read);
                }
                arrayList.add(1, sb.toString());
                as.b(d.t, "GET Response : " + sb.toString());
                if (inputStream != null) {
                    inputStream.close();
                }
            }
        } catch (MalformedURLException e) {
            as.a(d.u, "MalformedURLException", e);
        } catch (IOException e2) {
            as.d(d.u, "IOException: " + e2.getMessage());
        } catch (Throwable th) {
            if (inputStream != null) {
                inputStream.close();
            }
            throw th;
        }
        return arrayList;
    }
}
