package com.vungle.sdk;

import android.app.Activity;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.widget.ViewFlipper;
import com.vungle.sdk.u;

/* compiled from: vungle */
public class VungleAdvert extends Activity {
    u a = null;
    u b = null;
    private boolean c = false;
    private ViewFlipper d = null;
    private int e = 0;
    private Boolean[] f = {false, false};
    /* access modifiers changed from: private */
    public int g;
    private long h = Long.MIN_VALUE;
    /* access modifiers changed from: private */
    public boolean i = false;
    /* access modifiers changed from: private */
    public boolean j = false;

    /* access modifiers changed from: private */
    public void c() {
        if (!d()) {
            this.g++;
        }
    }

    private String a(String str) {
        if (str == null) {
            return null;
        }
        return "file://" + str;
    }

    private boolean d() {
        return this.g >= 4;
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        String str2;
        if (str == null) {
            str2 = av.a().a().g();
        } else {
            str2 = str;
        }
        runOnUiThread(new p(this).a(str2));
    }

    private u a(int i2) {
        switch (i2) {
            case 0:
                return null;
            case 1:
                ah e2 = ah.e();
                if (e2 == null) {
                    return null;
                }
                try {
                    return new w(this, a(e2.a.c()), new q(this));
                } catch (u.a e3) {
                    return null;
                }
            case 2:
                try {
                    x xVar = new x(this, ah.e().a.b(), av.a().a().j(), av.a().a().h(), new r(this));
                    xVar.a(ai.F);
                    return xVar;
                } catch (u.a e4) {
                    return null;
                }
            case 3:
                String a2 = a(ah.e().a.d());
                try {
                    as.b("post-roll url", a2);
                    return new v(this, a2, new s(this));
                } catch (u.a e5) {
                    return null;
                }
            case 4:
                return null;
            default:
                as.d("getViewForStage", "Invalid stage code.");
                return null;
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        as.a("showStage", "Showing stage for: " + this.g);
        u uVar = this.a;
        this.a = a(this.g);
        if (this.a != null) {
            this.b = uVar;
            if (this.f[this.e].booleanValue()) {
                this.d.removeViewAt(this.e);
            }
            this.f[this.e] = true;
            this.d.addView(this.a.a(), this.e);
            this.d.setDisplayedChild(this.e);
            this.e = 1 - this.e;
        } else if (d()) {
            a();
            finish();
        } else {
            c();
            e();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            as.a("VungleAdvert", "onCreate()");
            getWindow().addFlags(128);
            if (savedInstanceState != null) {
                this.g = savedInstanceState.getInt("stage");
                this.h = savedInstanceState.getLong("start");
            } else {
                this.g = 0;
                this.h = SystemClock.elapsedRealtime();
                c();
            }
            ai.a(true);
            this.d = new l(this);
            this.d.setInAnimation(this, 17432578);
            this.d.setOutAnimation(this, 17432577);
            this.d.setAnimateFirstView(true);
            this.d.getInAnimation().setAnimationListener(new t(this));
            requestWindowFeature(1);
            setContentView(this.d);
            if (ax.g(ai.e()) != null) {
                this.g = 4;
            }
            e();
        } catch (Throwable th) {
            av.a(th);
            this.g = 4;
            a();
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle out) {
        super.onSaveInstanceState(out);
        out.putInt("stage", this.g);
        out.putLong("start", this.h);
    }

    public void onBackPressed() {
        as.b("onBackPressed", "back button pressed");
        a();
        super.onBackPressed();
    }

    /* access modifiers changed from: protected */
    public void a() {
        if (!this.c) {
            long elapsedRealtime = SystemClock.elapsedRealtime() - this.h;
            this.c = true;
            av.c().b(elapsedRealtime);
            ai.a(false);
            ax.d();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        as.a("VungleAdvert", "onResume");
        if (this.a != null && !this.i && this.j) {
            as.b("VungleAdvert", "Resume --> onShow");
            this.a.b();
        }
        this.i = true;
        VunglePub.onResume();
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        as.a("VungleAdvert", "onWindowFocusChanged(" + hasFocus + ")");
        if (this.a != null && this.i && !this.j && hasFocus) {
            as.b("VungleAdvert", "FC --> onShow");
            this.a.b();
        }
        this.j = hasFocus;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        as.a("VungleAdvert", "onPause");
        if (this.a != null) {
            as.b("VungleAdvert", "Pause --> onHide");
            this.a.c();
        }
        this.i = false;
        VunglePub.onPause();
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (this.g != 2) {
            return super.onKeyDown(keyCode, event);
        }
        if (keyCode == 25 || keyCode == 24 || keyCode == 91) {
            return super.onKeyDown(keyCode, event);
        }
        if (((ai.t && !ai.D) || (ai.u && ai.D)) && keyCode == 4) {
            av.c().a("back");
            ((x) this.a).e();
        }
        return true;
    }

    /* access modifiers changed from: private */
    public static i f() {
        i iVar = new i();
        iVar.a(av.d().a());
        iVar.b(av.d().b());
        iVar.c(av.d().c());
        av.a(new i());
        return iVar;
    }
}
