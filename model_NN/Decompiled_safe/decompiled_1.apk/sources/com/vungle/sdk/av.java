package com.vungle.sdk;

import android.util.Log;
import java.util.ArrayList;

/* compiled from: vungle */
class av {
    private static j a;
    private static i b;
    private static String c;
    private static h d;
    private static ArrayList<String> e = null;
    private static c f = null;

    public static j a() {
        return a;
    }

    public static ArrayList<String> b() {
        return e;
    }

    public static void a(ArrayList<String> arrayList) {
        e = arrayList;
    }

    public static void a(String str) {
        if (e == null) {
            e = new ArrayList<>(0);
        }
        e.add(str);
    }

    public static h c() {
        return d;
    }

    public static void a(h hVar) {
        d = hVar;
    }

    public static i d() {
        return b;
    }

    public static void a(i iVar) {
        b = iVar;
    }

    public static c e() {
        return f;
    }

    public static void a(c cVar) {
        f = cVar;
    }

    public static void a(j jVar) {
        a = jVar;
    }

    public static String f() {
        String str = c;
        c = null;
        return str;
    }

    public static void b(String str) {
        c = str;
    }

    public static void a(Throwable th) {
        b(Log.getStackTraceString(th));
    }
}
