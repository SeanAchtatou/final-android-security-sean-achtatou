package com.vungle.sdk;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

/* compiled from: vungle */
class p implements Runnable {
    String a = null;
    final /* synthetic */ VungleAdvert b;

    p(VungleAdvert vungleAdvert) {
        this.b = vungleAdvert;
    }

    public Runnable a(String str) {
        this.a = str;
        return this;
    }

    public void run() {
        String str = this.a;
        if (str == null || str.length() == 0) {
            as.c("VungleAdvertCTA", "CTA is either null or empty.");
            return;
        }
        as.a("VungleAdvertCTA", "Dispatching CTA: " + str);
        try {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
            intent.addFlags(268435456);
            this.b.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            if (str.toLowerCase().startsWith("amzn://")) {
                as.c("VungleAdvertCTA", "Clicked, but Amazon Appstore was not available.");
                Toast.makeText(this.b, "Amazon AppStore needs to be installed for this link to work.", 1).show();
                return;
            }
            as.c("VungleAdvertCTA", "Clicked, but could not find an activity to service the CTA.");
            Toast.makeText(this.b, "We encountered an error while trying to open that link. Sorry 'bout that! :X", 1).show();
        }
    }
}
