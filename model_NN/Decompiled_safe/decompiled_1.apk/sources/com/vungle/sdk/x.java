package com.vungle.sdk;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;
import com.vungle.sdk.u;
import java.util.Timer;
import java.util.TimerTask;

/* compiled from: vungle */
class x extends u {
    private static final boolean c = (Build.VERSION.SDK_INT < 14);
    /* access modifiers changed from: private */
    public g A = null;
    private a B = null;
    /* access modifiers changed from: private */
    public Handler C = new y(this);
    m a;
    boolean b = false;
    /* access modifiers changed from: private */
    public Context d = null;
    private View e = null;
    /* access modifiers changed from: private */
    public ProgressBar f = null;
    /* access modifiers changed from: private */
    public VideoView g = null;
    /* access modifiers changed from: private */
    public ImageView h = null;
    private ImageView i = null;
    private Timer j = null;
    private AudioManager k = null;
    /* access modifiers changed from: private */
    public TextView l = null;
    private String m;
    /* access modifiers changed from: private */
    public boolean n = false;
    private boolean o = false;
    /* access modifiers changed from: private */
    public boolean p = true;
    /* access modifiers changed from: private */
    public Object q = new Object();
    /* access modifiers changed from: private */
    public boolean r = false;
    /* access modifiers changed from: private */
    public boolean s = false;
    /* access modifiers changed from: private */
    public boolean t = false;
    private boolean u = false;
    /* access modifiers changed from: private */
    public int v;
    /* access modifiers changed from: private */
    public int w;
    /* access modifiers changed from: private */
    public long x;
    /* access modifiers changed from: private */
    public AlertDialog y;
    /* access modifiers changed from: private */
    public boolean z = false;

    /* compiled from: vungle */
    public static abstract class g {
        public abstract void a(long j);

        public abstract void a(long j, long j2);

        public abstract void a(long j, long j2, long j3);
    }

    /* access modifiers changed from: protected */
    public void a(Context context) {
        this.a = new m(context);
        this.e = this.a.a();
        this.f = this.a.b();
        this.g = this.a.f();
        this.h = this.a.d();
        this.i = this.a.e();
        this.l = this.a.c();
    }

    public x(Context context, String str, int i2, int i3, g gVar) {
        this.d = context;
        if (str == null || str.length() == 0) {
            throw new u.a();
        }
        try {
            a(context);
            this.k = (AudioManager) context.getSystemService("audio");
            this.x = System.currentTimeMillis();
            this.m = str;
            this.v = i2;
            this.w = i3;
            this.A = gVar;
            ((Activity) context).setRequestedOrientation(ai.C);
            this.e.setBackgroundColor(-16777216);
            this.f.setVisibility(0);
            this.h.getDrawable().setAlpha(0);
            this.l.setTextColor(0);
            this.g.setVisibility(4);
            this.n = !ai.s;
            f();
            this.g.setOnPreparedListener(new e(this, null));
            this.g.setOnCompletionListener(new b(this, null));
            this.g.setOnErrorListener(new c(this, null));
            this.i.setOnClickListener(new d(this, null));
            this.B = new a(this, null);
            this.h.setOnClickListener(this.B);
        } catch (Throwable th) {
            av.a(th);
            throw new u.a();
        }
    }

    public View a() {
        return this.e;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b() {
        /*
            r6 = this;
            java.lang.String r0 = "VungleAdvertVideo"
            java.lang.String r1 = "onShow() start"
            com.vungle.sdk.as.b(r0, r1)
            r0 = 0
            r6.r = r0     // Catch:{ Throwable -> 0x0030 }
            boolean r0 = r6.u     // Catch:{ Throwable -> 0x0030 }
            if (r0 == 0) goto L_0x000f
        L_0x000e:
            return
        L_0x000f:
            r0 = 1
            r6.u = r0     // Catch:{ Throwable -> 0x0030 }
            android.app.AlertDialog r0 = r6.y     // Catch:{ Throwable -> 0x0030 }
            if (r0 != 0) goto L_0x000e
            com.vungle.sdk.x$a r0 = r6.B     // Catch:{ Throwable -> 0x0030 }
            r0.a()     // Catch:{ Throwable -> 0x0030 }
            r0 = 0
            r6.b = r0     // Catch:{ Throwable -> 0x0030 }
            boolean r0 = r6.o     // Catch:{ Throwable -> 0x0030 }
            if (r0 != 0) goto L_0x0038
            android.widget.VideoView r0 = r6.g     // Catch:{ Throwable -> 0x0030 }
            r1 = 0
            r0.setVisibility(r1)     // Catch:{ Throwable -> 0x0030 }
            android.widget.VideoView r0 = r6.g     // Catch:{ Throwable -> 0x0030 }
            java.lang.String r1 = r6.m     // Catch:{ Throwable -> 0x0030 }
            r0.setVideoPath(r1)     // Catch:{ Throwable -> 0x0030 }
            goto L_0x000e
        L_0x0030:
            r0 = move-exception
            com.vungle.sdk.av.a(r0)
            r6.j()
            goto L_0x000e
        L_0x0038:
            java.lang.Object r0 = r6.q     // Catch:{ Throwable -> 0x0030 }
            monitor-enter(r0)     // Catch:{ Throwable -> 0x0030 }
            android.widget.TextView r1 = r6.l     // Catch:{ all -> 0x006b }
            r2 = 0
            r1.setTextColor(r2)     // Catch:{ all -> 0x006b }
            android.widget.TextView r1 = r6.l     // Catch:{ all -> 0x006b }
            r2 = 1073741824(0x40000000, float:2.0)
            r3 = 0
            r4 = 0
            r5 = 0
            r1.setShadowLayer(r2, r3, r4, r5)     // Catch:{ all -> 0x006b }
            android.widget.ImageView r1 = r6.h     // Catch:{ all -> 0x006b }
            android.graphics.drawable.Drawable r1 = r1.getDrawable()     // Catch:{ all -> 0x006b }
            r2 = 0
            r1.setAlpha(r2)     // Catch:{ all -> 0x006b }
            r1 = 0
            r6.s = r1     // Catch:{ all -> 0x006b }
            r1 = 0
            r6.t = r1     // Catch:{ all -> 0x006b }
            monitor-exit(r0)     // Catch:{ all -> 0x006b }
            android.widget.VideoView r0 = r6.g     // Catch:{ Throwable -> 0x0030 }
            r1 = 0
            r0.seekTo(r1)     // Catch:{ Throwable -> 0x0030 }
            android.widget.VideoView r0 = r6.g     // Catch:{ Throwable -> 0x0030 }
            r0.start()     // Catch:{ Throwable -> 0x0030 }
            r6.h()     // Catch:{ Throwable -> 0x0030 }
            goto L_0x000e
        L_0x006b:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x006b }
            throw r1     // Catch:{ Throwable -> 0x0030 }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.sdk.x.b():void");
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c() {
        /*
            r6 = this;
            boolean r0 = r6.u     // Catch:{ Throwable -> 0x0076 }
            if (r0 != 0) goto L_0x0005
        L_0x0004:
            return
        L_0x0005:
            r0 = 0
            r6.u = r0     // Catch:{ Throwable -> 0x0076 }
            android.app.AlertDialog r0 = r6.y     // Catch:{ Throwable -> 0x0076 }
            if (r0 == 0) goto L_0x0019
            android.app.AlertDialog r0 = r6.y     // Catch:{ Throwable -> 0x0076 }
            r0.dismiss()     // Catch:{ Throwable -> 0x0076 }
            android.app.AlertDialog r0 = r6.y     // Catch:{ Throwable -> 0x0076 }
            r0.hide()     // Catch:{ Throwable -> 0x0076 }
            r0 = 0
            r6.y = r0     // Catch:{ Throwable -> 0x0076 }
        L_0x0019:
            android.widget.VideoView r0 = r6.g     // Catch:{ Throwable -> 0x0076 }
            if (r0 == 0) goto L_0x0022
            android.widget.VideoView r0 = r6.g     // Catch:{ Throwable -> 0x0076 }
            r0.pause()     // Catch:{ Throwable -> 0x0076 }
        L_0x0022:
            java.lang.Object r1 = r6.q     // Catch:{ Throwable -> 0x0076 }
            monitor-enter(r1)     // Catch:{ Throwable -> 0x0076 }
            r0 = 1
            r6.r = r0     // Catch:{ all -> 0x0073 }
            android.widget.TextView r0 = r6.l     // Catch:{ all -> 0x0073 }
            android.view.animation.Animation r0 = r0.getAnimation()     // Catch:{ all -> 0x0073 }
            android.view.animation.AlphaAnimation r0 = (android.view.animation.AlphaAnimation) r0     // Catch:{ all -> 0x0073 }
            if (r0 == 0) goto L_0x0035
            r0.cancel()     // Catch:{ all -> 0x0073 }
        L_0x0035:
            android.widget.TextView r0 = r6.l     // Catch:{ all -> 0x0073 }
            r2 = 0
            r0.setAnimation(r2)     // Catch:{ all -> 0x0073 }
            android.widget.ImageView r0 = r6.h     // Catch:{ all -> 0x0073 }
            android.view.animation.Animation r0 = r0.getAnimation()     // Catch:{ all -> 0x0073 }
            android.view.animation.AlphaAnimation r0 = (android.view.animation.AlphaAnimation) r0     // Catch:{ all -> 0x0073 }
            if (r0 == 0) goto L_0x0048
            r0.cancel()     // Catch:{ all -> 0x0073 }
        L_0x0048:
            android.widget.ImageView r0 = r6.h     // Catch:{ all -> 0x0073 }
            r2 = 0
            r0.setAnimation(r2)     // Catch:{ all -> 0x0073 }
            android.widget.TextView r0 = r6.l     // Catch:{ all -> 0x0073 }
            r2 = 0
            r0.setTextColor(r2)     // Catch:{ all -> 0x0073 }
            android.widget.TextView r0 = r6.l     // Catch:{ all -> 0x0073 }
            r2 = 1073741824(0x40000000, float:2.0)
            r3 = 0
            r4 = 0
            r5 = 0
            r0.setShadowLayer(r2, r3, r4, r5)     // Catch:{ all -> 0x0073 }
            android.widget.ImageView r0 = r6.h     // Catch:{ all -> 0x0073 }
            android.graphics.drawable.Drawable r0 = r0.getDrawable()     // Catch:{ all -> 0x0073 }
            r2 = 0
            r0.setAlpha(r2)     // Catch:{ all -> 0x0073 }
            r6.i()     // Catch:{ all -> 0x0073 }
            r0 = 0
            r6.s = r0     // Catch:{ all -> 0x0073 }
            r0 = 0
            r6.t = r0     // Catch:{ all -> 0x0073 }
            monitor-exit(r1)     // Catch:{ all -> 0x0073 }
            goto L_0x0004
        L_0x0073:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0073 }
            throw r0     // Catch:{ Throwable -> 0x0076 }
        L_0x0076:
            r0 = move-exception
            com.vungle.sdk.av.a(r0)
            r6.j()
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.sdk.x.c():void");
    }

    public void a(boolean z2) {
        this.p = z2;
    }

    /* access modifiers changed from: private */
    public void f() {
        this.k.setStreamMute(3, this.n);
        this.a.a(this.n);
    }

    /* access modifiers changed from: private */
    public void g() {
        this.z = true;
        if (c) {
            ((RelativeLayout) this.e).removeView(this.g);
            this.g = null;
        } else {
            this.g.setVisibility(4);
        }
        if (this.n) {
            this.k.setStreamMute(3, false);
        }
    }

    /* compiled from: vungle */
    private class e implements MediaPlayer.OnPreparedListener {
        private e() {
        }

        /* synthetic */ e(x xVar, y yVar) {
            this();
        }

        public void onPrepared(MediaPlayer in_mp) {
            as.b("VungleAdvertVideo", "onPrepared called");
            x.this.f.setVisibility(4);
            long unused = x.this.x = System.currentTimeMillis();
            x.this.g.setBackgroundDrawable(null);
            if (((Activity) x.this.d).hasWindowFocus()) {
                x.this.g.start();
                x.this.h();
            }
        }
    }

    /* compiled from: vungle */
    private class b implements MediaPlayer.OnCompletionListener {
        private b() {
        }

        /* synthetic */ b(x xVar, y yVar) {
            this();
        }

        public void onCompletion(MediaPlayer mp) {
            as.b("VungleAdvertVideo", "onComplete called");
            if (!x.this.z) {
                x.this.i();
                x.this.g();
                x.this.A.a(x.this.x, (long) mp.getDuration());
            }
        }
    }

    /* compiled from: vungle */
    private class c implements MediaPlayer.OnErrorListener {
        private c() {
        }

        /* synthetic */ c(x xVar, y yVar) {
            this();
        }

        public boolean onError(MediaPlayer mp, int what, int extra) {
            if (x.this.z) {
                return false;
            }
            as.c("OnErrorListener", "Error encountered during video playback: what(" + what + ") extra(" + extra + ")");
            x.this.i();
            x.this.g();
            x.this.A.a(x.this.x);
            return true;
        }
    }

    /* compiled from: vungle */
    private class a implements View.OnClickListener {
        /* access modifiers changed from: private */
        public boolean b;

        private a() {
            this.b = false;
        }

        /* synthetic */ a(x xVar, y yVar) {
            this();
        }

        public void a() {
            this.b = false;
        }

        public void onClick(View arg0) {
            if (!x.this.z && !this.b) {
                this.b = true;
                long currentPosition = (long) x.this.g.getCurrentPosition();
                long duration = (long) x.this.g.getDuration();
                if (ai.D && x.this.p && ((double) currentPosition) / 1000.0d >= ((double) x.this.w)) {
                    x.this.g.pause();
                    AlertDialog.Builder builder = new AlertDialog.Builder(x.this.d);
                    builder.setTitle("Confirm Cancel");
                    builder.setMessage("Stopping this video early will prevent you from earning your reward. Continue?");
                    builder.setPositiveButton("Keep Watching", new ac(this));
                    builder.setNegativeButton("Cancel Video", new ad(this));
                    builder.setOnCancelListener(new ae(this));
                    AlertDialog unused = x.this.y = builder.show();
                } else if (x.this.p && ((double) currentPosition) / 1000.0d >= ((double) x.this.w)) {
                    x.this.g.stopPlayback();
                    x.this.g();
                    x.this.i();
                    x.this.A.a(x.this.x, currentPosition, duration);
                }
            }
        }
    }

    /* compiled from: vungle */
    private class d implements View.OnClickListener {
        private d() {
        }

        /* synthetic */ d(x xVar, y yVar) {
            this();
        }

        public void onClick(View arg0) {
            boolean unused = x.this.n = !x.this.n;
            av.c().a(x.this.n ? "muted" : "un-muted");
            x.this.f();
        }
    }

    /* compiled from: vungle */
    private class f extends TimerTask {
        private f() {
        }

        /* synthetic */ f(x xVar, y yVar) {
            this();
        }

        public void run() {
            x.this.C.sendEmptyMessage(66);
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        if (this.j == null) {
            this.j = new Timer();
            this.j.schedule(new f(this, null), 0, 100);
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        if (this.j != null) {
            this.j.cancel();
            this.j.purge();
            this.j = null;
        }
    }

    private void j() {
        g();
        i();
        this.A.a(this.x);
    }

    public void d() {
        if (!this.z) {
            long currentPosition = (long) this.g.getCurrentPosition();
            long duration = (long) this.g.getDuration();
            this.g.stopPlayback();
            g();
            i();
            this.A.a(this.x, currentPosition, duration);
        }
    }

    public void e() {
        if (!this.b) {
            this.b = true;
            if (!ai.D) {
                d();
                return;
            }
            this.g.pause();
            AlertDialog.Builder builder = new AlertDialog.Builder(this.d);
            builder.setTitle("Confirm Cancel");
            builder.setMessage("Stopping this video early will prevent you from earning your reward. Continue?");
            builder.setPositiveButton("Keep Watching", new z(this));
            builder.setNegativeButton("Cancel Video", new aa(this));
            builder.setOnCancelListener(new ab(this));
            this.y = builder.show();
        }
    }
}
