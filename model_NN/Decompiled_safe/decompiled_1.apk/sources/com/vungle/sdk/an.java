package com.vungle.sdk;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.util.Timer;

/* compiled from: vungle */
class an {
    public static final int[] a = {10, 30, 30, 60, 60, 120};
    private int b;
    private int c;
    /* access modifiers changed from: private */
    public int d;
    /* access modifiers changed from: private */
    public String e;
    /* access modifiers changed from: private */
    public String f;
    /* access modifiers changed from: private */
    public a g;
    /* access modifiers changed from: private */
    public String h;
    /* access modifiers changed from: private */
    public b i;
    /* access modifiers changed from: private */
    public String j;

    /* compiled from: vungle */
    public interface b {
        void a(String str);

        void c();
    }

    private boolean e() {
        int i2 = this.b + 1;
        this.b = i2;
        return i2 <= 3;
    }

    /* access modifiers changed from: private */
    public boolean f() {
        this.d = a[Math.min(this.c, a.length - 1)];
        int i2 = this.c + 1;
        this.c = i2;
        return i2 <= 33;
    }

    public an(String str, String str2) {
        this(str, str2, null);
    }

    public an(String str, String str2, String str3) {
        this.b = 0;
        this.c = 0;
        this.d = a[0];
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = null;
        this.e = str;
        this.f = str2;
        this.h = str3;
    }

    public void a(b bVar) {
        this.i = bVar;
    }

    public void a(String str) {
        this.j = str;
    }

    public void a() {
        this.g = new a();
        this.g.a(this.e, this.f, this.h);
    }

    /* compiled from: vungle */
    private class a {
        private boolean b;
        private boolean c;
        private String d;
        private String e;
        /* access modifiers changed from: private */
        public String f;
        private boolean g;

        private a() {
            this.b = false;
            this.c = true;
            this.e = null;
            this.f = null;
            this.g = false;
        }

        public void a(String str, String str2, String str3) {
            new Thread(new ao(this, str, str2, str3), "VungleDownloadThread").start();
        }

        /* access modifiers changed from: protected */
        public Boolean a(String... strArr) {
            try {
                URL url = new URL(strArr[0]);
                String[] split = url.getFile().split(File.separator);
                this.f = split[split.length - 1];
                try {
                    this.f = URLDecoder.decode(this.f, "UTF-8");
                } catch (UnsupportedEncodingException e2) {
                    as.a("DownloadFile", "Failed to decode filename.", e2);
                }
                if (an.this.j != null) {
                    this.f = an.this.j + this.f;
                }
                File file = new File(strArr[1], this.f);
                this.d = file.getAbsolutePath();
                if (strArr.length >= 3) {
                    this.e = strArr[2];
                }
                URLConnection openConnection = url.openConnection();
                openConnection.setConnectTimeout(5000);
                openConnection.setReadTimeout(5000);
                openConnection.connect();
                BufferedInputStream bufferedInputStream = new BufferedInputStream(openConnection.getInputStream());
                as.a("DownloadFile", "Download started on: " + this.f);
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                byte[] bArr = new byte[2048];
                while (true) {
                    int read = bufferedInputStream.read(bArr);
                    if (read != -1) {
                        fileOutputStream.write(bArr, 0, read);
                    } else {
                        fileOutputStream.flush();
                        fileOutputStream.close();
                        bufferedInputStream.close();
                        as.a("DownloadFile", "Downloaded ended on: " + this.f);
                        return true;
                    }
                }
            } catch (Exception e3) {
                as.d("DownloadFile", "Problem encountered during file download: " + e3.getMessage());
                this.g = true;
                return false;
            }
        }

        /* access modifiers changed from: protected */
        public void a(Boolean bool) {
            this.b = true;
            if (bool.booleanValue() && this.e != null) {
                if (ax.a(this.d).equalsIgnoreCase(this.e)) {
                    as.a("DownloadFile", "File (" + this.f + ") passes md5: " + this.e);
                } else {
                    as.a("DownloadFile", "File (" + this.f + ") fails md5 check, but we don't care.");
                }
            }
            if (!bool.booleanValue()) {
                File file = new File(this.d);
                if (file.exists()) {
                    file.delete();
                }
                this.d = null;
                if (this.g && an.this.f()) {
                    as.a("DownloadFile", "Waiting for " + an.this.d + " seconds before retrying...");
                    new Timer().schedule(new ap(this), (long) (an.this.d * 1000));
                } else if (0 != 0) {
                    a unused = an.this.g = new a();
                    an.this.g.a(an.this.e, an.this.f, an.this.h);
                } else if (an.this.i != null) {
                    an.this.i.c();
                }
            } else if (an.this.i != null) {
                an.this.i.a(this.d);
            }
            this.c = bool.booleanValue();
        }

        public boolean a() {
            return b() && this.c;
        }

        public boolean b() {
            return this.b;
        }

        public String c() {
            if (a()) {
                return this.d;
            }
            return null;
        }
    }

    public boolean b() {
        if (!e()) {
            return false;
        }
        as.a("DownloadFile", "Downloading file (" + this.g.f + ") again... Retry #" + this.b);
        this.g = new a();
        this.g.a(this.e, this.f, this.h);
        return true;
    }

    public boolean c() {
        if (this.g != null) {
            return this.g.a();
        }
        return false;
    }

    public String d() {
        if (this.g != null) {
            return this.g.c();
        }
        return null;
    }
}
