package com.vungle.sdk;

import com.flurry.android.AdCreative;
import com.vungle.sdk.v;

/* compiled from: vungle */
class s implements v.b {
    final /* synthetic */ VungleAdvert a;

    s(VungleAdvert vungleAdvert) {
        this.a = vungleAdvert;
    }

    public void a() {
        as.a("PostRoll-callback", "'Close' button clicked.");
        av.c().a("close");
        int unused = this.a.g = 4;
        this.a.e();
    }

    public void b() {
        as.a("PostRoll-callback", "'Download' button clicked.");
        int unused = this.a.g = 4;
        this.a.b((String) null);
        av.c().a("download");
        this.a.e();
    }

    public void c() {
        as.a("PostRoll-callback", "'Replay' button clicked.");
        av.c().a("replay");
        int unused = this.a.g = 2;
        this.a.e();
    }

    public void a(String str) {
        as.a("PostRoll-callback", "'Custom' button clicked.");
        int unused = this.a.g = 4;
        this.a.b(str);
        av.c().a(AdCreative.kFormatCustom);
        this.a.e();
    }
}
