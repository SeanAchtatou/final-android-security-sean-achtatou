package com.vungle.sdk;

import android.content.DialogInterface;

/* compiled from: vungle */
class aa implements DialogInterface.OnClickListener {
    final /* synthetic */ x a;

    aa(x xVar) {
        this.a = xVar;
    }

    public void onClick(DialogInterface dialog, int which) {
        long currentPosition = (long) this.a.g.getCurrentPosition();
        long duration = (long) this.a.g.getDuration();
        as.a("Callback", "Cancel Vid");
        this.a.g.stopPlayback();
        this.a.g();
        this.a.i();
        this.a.A.a(this.a.x, currentPosition, duration);
    }
}
