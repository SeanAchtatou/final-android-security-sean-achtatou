package com.vungle.sdk;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import java.util.ArrayList;

/* compiled from: vungle */
public final class VunglePub {
    private static VunglePub a = null;
    private static String b = null;

    /* compiled from: vungle */
    public interface EventListener {
        void onVungleAdEnd();

        void onVungleAdStart();

        void onVungleView(double d, double d2);
    }

    private static synchronized boolean a() {
        boolean z;
        synchronized (VunglePub.class) {
            if (a == null) {
                as.c("VunglePub", "VunglePub was not initialized.");
                z = false;
            } else {
                z = true;
            }
        }
        return z;
    }

    private VunglePub(Context ctx, String vungleAppId, int age, int gender) {
        ai.a(ctx);
        av.a(new j(vungleAppId));
        av.a().b(age);
        av.a().a(gender);
        d();
    }

    private void b() {
        SharedPreferences sharedPreferences = ai.e().getSharedPreferences(d.W, 0);
        long j = sharedPreferences.getLong(d.Y, 0);
        int i = sharedPreferences.getInt(d.Z, 0);
        if (j > System.currentTimeMillis()) {
            i = 0;
            j = 0;
        }
        d.a(i);
        d.a(j);
        as.a("VunglePub", "Restored LastViewed to: " + j);
        as.a("VunglePub", "Restored AdDelay to:    " + i);
    }

    private void c() {
        int i;
        int i2;
        Display defaultDisplay;
        WindowManager windowManager = (WindowManager) ai.e().getSystemService("window");
        if (windowManager == null || (defaultDisplay = windowManager.getDefaultDisplay()) == null) {
            i = 270;
            i2 = 480;
        } else {
            i = defaultDisplay.getWidth();
            i2 = defaultDisplay.getHeight();
        }
        ai.x = (float) i;
        ai.y = (float) i2;
    }

    private void d() {
        c();
        av.a(new i());
        av.a(new h());
        if (!ax.b(ai.e(), d.W, d.X, false) && ax.f(ai.e())) {
            new aj(ai.e()).d();
        }
        av.a(new ArrayList(0));
        ax.h(ai.e());
        b();
        as.a("VunglePub", "SDK initialized -- VungleDroid/1.2.2");
    }

    private static void e() {
        if (av.b() != null && !av.b().isEmpty()) {
            new aj(ai.e()).b();
        }
    }

    public static synchronized void init(Context context, String appId) {
        synchronized (VunglePub.class) {
            init(context, appId, 0, -1);
        }
    }

    public static synchronized void init(Context context, String vungleAppId, int age, int gender) {
        synchronized (VunglePub.class) {
            if (a == null) {
                as.b("VunglePub", "Detected first call.");
                ai.i = true;
                a = new VunglePub(context, vungleAppId, age, gender);
                aj.e();
                e();
            }
        }
    }

    public static synchronized void onResume() {
        synchronized (VunglePub.class) {
            if (a()) {
                aw.a();
                au.a();
            }
        }
    }

    public static synchronized void onPause() {
        synchronized (VunglePub.class) {
            if (a()) {
                aw.b();
                au.b();
            }
        }
    }

    public static synchronized void setSoundEnabled(boolean isSoundEnabled) {
        synchronized (VunglePub.class) {
            if (a()) {
                ai.s = isSoundEnabled;
            }
        }
    }

    public static synchronized void setBackButtonEnabled(boolean isBackButtonEnabled) {
        synchronized (VunglePub.class) {
            if (a()) {
                ai.t = isBackButtonEnabled;
            }
        }
    }

    public static synchronized void setIncentivizedBackButtonEnabled(boolean isBackButtonEnabled) {
        synchronized (VunglePub.class) {
            if (a()) {
                ai.u = isBackButtonEnabled;
            }
        }
    }

    public static synchronized boolean getSoundEnabled() {
        boolean z;
        synchronized (VunglePub.class) {
            if (!a()) {
                z = false;
            } else {
                z = ai.s;
            }
        }
        return z;
    }

    public static synchronized void setAutoRotation(boolean shouldAutoRotate) {
        synchronized (VunglePub.class) {
            ai.r = shouldAutoRotate;
        }
    }

    private static boolean a(Boolean bool, String str, boolean z) {
        boolean z2;
        boolean z3;
        if (!a()) {
            return false;
        }
        ai.D = bool.booleanValue();
        ai.E = str;
        ai.F = z;
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - d.h() < ((long) (d.g() * 1000))) {
            as.b("DisplayAdvert", "Suppressing a video play due to server delay setting");
            as.b("DisplayAdvert", "Time Diff: " + (((double) (currentTimeMillis - d.h())) / 1000.0d));
            as.b("DisplayAdvert", "Cur Delay: " + d.g());
            z2 = true;
            z3 = false;
        } else if (!ai.j) {
            aj.e();
            z2 = false;
            z3 = false;
        } else if (!ai.n) {
            z3 = f();
            z2 = false;
        } else {
            z2 = false;
            z3 = false;
        }
        if (!z3 && !z2 && !ai.d()) {
            k.a(av.a().b(), ax.b(ai.e()));
            aj.e();
        }
        return z3;
    }

    public static synchronized boolean displayAdvert() {
        boolean a2;
        synchronized (VunglePub.class) {
            a2 = a(false, null, true);
        }
        return a2;
    }

    public static synchronized boolean displayIncentivizedAdvert(boolean showCloseButton) {
        boolean a2;
        synchronized (VunglePub.class) {
            a2 = a(true, null, showCloseButton);
        }
        return a2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0009, code lost:
        if (r3.length() == 0) goto L_0x000b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized boolean displayIncentivizedAdvert(java.lang.String r3, boolean r4) {
        /*
            java.lang.Class<com.vungle.sdk.VunglePub> r0 = com.vungle.sdk.VunglePub.class
            monitor-enter(r0)
            if (r3 == 0) goto L_0x000b
            int r1 = r3.length()     // Catch:{ all -> 0x001e }
            if (r1 != 0) goto L_0x0013
        L_0x000b:
            java.lang.String r1 = "VunglePub"
            java.lang.String r2 = "displayIncentivizedAdvert() provided a null/empty name parameter."
            com.vungle.sdk.as.a(r1, r2)     // Catch:{ all -> 0x001e }
            r3 = 0
        L_0x0013:
            r1 = 1
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)     // Catch:{ all -> 0x001e }
            boolean r1 = a(r1, r3, r4)     // Catch:{ all -> 0x001e }
            monitor-exit(r0)
            return r1
        L_0x001e:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.sdk.VunglePub.displayIncentivizedAdvert(java.lang.String, boolean):boolean");
    }

    public static synchronized boolean isVideoAvailable() {
        boolean z;
        synchronized (VunglePub.class) {
            if (!a()) {
                b = "VunglePub not initialized.";
                z = false;
            } else if (System.currentTimeMillis() - d.h() < ((long) (d.g() * 1000))) {
                b = "Ad views are too close together. (Within the delay time provided by the adserver)";
                z = false;
            } else {
                String g = ax.g(ai.e());
                if (g != null) {
                    b = g + " not currently available.";
                    z = false;
                } else if (!ai.j) {
                    b = "Advert has not been fully downloaded yet.";
                    z = false;
                } else {
                    b = "Advert is available.";
                    z = true;
                }
            }
        }
        return z;
    }

    public static synchronized boolean isVideoAvailable(boolean debug) {
        boolean isVideoAvailable;
        synchronized (VunglePub.class) {
            isVideoAvailable = isVideoAvailable();
            if (debug) {
                Log.i("Vungle", b);
                as.b("isVideoAvailable", b);
                b = null;
            }
        }
        return isVideoAvailable;
    }

    private static synchronized boolean f() {
        boolean g;
        synchronized (VunglePub.class) {
            as.b("playVideo", "video ready: " + ai.j);
            as.b("playVideo", "ad in focus: " + ai.d());
            if (!ai.j) {
                as.a("playVideo", "No video available");
                g = false;
            } else if (ai.d()) {
                as.a("playVideo", "Video ad is already visible");
                g = false;
            } else if (ax.c(av.a().a().b())) {
                g = Build.VERSION.SDK_INT < 11 ? false : !ax.f(ai.e()) ? false : g();
            } else if (ax.g(ai.e()) != null) {
                g = false;
            } else {
                as.b("playVideo", "Chose to display video");
                as.b("playVideo", " -- delay: " + d.g());
                as.b("playVideo", " -- start: " + d.h());
                as.b("playVideo", " -- focus: " + ai.d());
                g = g();
            }
        }
        return g;
    }

    /* compiled from: vungle */
    public static final class Gender {
        public static final int FEMALE = 1;
        public static final int MALE = 0;
        public static final int UNKNOWN = -1;

        public static String toString(int gender) {
            switch (gender) {
                case 0:
                    return "male";
                case 1:
                    return "female";
                default:
                    return "unknown";
            }
        }
    }

    public static synchronized void setEventListener(EventListener el) {
        synchronized (VunglePub.class) {
            if (a()) {
                d.a(el);
            }
        }
    }

    private static synchronized boolean g() {
        boolean z;
        synchronized (VunglePub.class) {
            av.c().a(System.currentTimeMillis());
            ai.j = false;
            Intent intent = new Intent(ai.e(), VungleAdvert.class);
            intent.addFlags(268435456);
            ai.e().startActivity(intent);
            ah e = ah.e();
            if (e == null) {
                z = false;
            } else {
                e.c();
                z = true;
            }
        }
        return z;
    }

    public static final synchronized String getVersionString() {
        synchronized (VunglePub.class) {
        }
        return "VungleDroid/1.2.2";
    }
}
