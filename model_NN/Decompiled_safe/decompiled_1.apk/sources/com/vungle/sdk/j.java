package com.vungle.sdk;

import android.media.AudioManager;
import android.os.Build;
import com.flurry.android.AdCreative;
import com.flurry.android.FlurryFullscreenTakeoverActivity;
import com.vungle.sdk.VunglePub;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: vungle */
class j {
    boolean a = false;
    private int b;
    private int c;
    private String d;
    private String e;
    private final String f;
    private String g;
    private b h = null;
    private String i = "";
    private a j;

    public j(String str) {
        this.i = str;
        this.d = ax.c(ai.e());
        this.f = ax.a(ai.e());
        this.e = ax.a();
        this.h = new b();
    }

    public void a(int i2) {
        this.b = i2;
    }

    public void b(int i2) {
        this.c = i2;
    }

    public void a(a aVar) {
        this.j = aVar;
    }

    public a a() {
        return this.j;
    }

    public String b() {
        return this.i;
    }

    private String c() {
        this.g = ax.d(ai.e());
        return this.g;
    }

    private double d() {
        AudioManager audioManager = (AudioManager) ai.e().getSystemService("audio");
        return ((double) audioManager.getStreamVolume(3)) / ((double) audioManager.getStreamMaxVolume(3));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public String a(JSONObject jSONObject) {
        try {
            jSONObject.put("campaign", this.j.c().replace('_', '|'));
            jSONObject.put("app_id", this.j.e());
            jSONObject.put(FlurryFullscreenTakeoverActivity.EXTRA_KEY_URL, this.j.b());
            if (this.j.a() != null) {
                jSONObject.put("id", this.j.a());
            }
            jSONObject.put("incentivized", ai.D);
            if (ai.D && ai.E != null) {
                jSONObject.put("name", ai.E);
            }
            return b(jSONObject);
        } catch (JSONException e2) {
            as.a(d.u, "JSONException", e2);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    public String b(JSONObject jSONObject) {
        JSONObject jSONObject2;
        if (jSONObject == null) {
            jSONObject2 = new JSONObject();
        } else {
            jSONObject2 = jSONObject;
        }
        try {
            JSONObject jSONObject3 = new JSONObject();
            JSONObject jSONObject4 = new JSONObject();
            JSONObject jSONObject5 = new JSONObject();
            JSONObject jSONObject6 = new JSONObject();
            jSONObject3.put("osVersion", Build.VERSION.RELEASE);
            jSONObject3.put("platform", "android");
            this.a = ah.a();
            jSONObject3.put("isSdCardAvailable", this.a ? 1 : 0);
            ax.i(ai.e());
            jSONObject6.put(AdCreative.kFixWidth, (double) ai.x);
            jSONObject6.put(AdCreative.kFixHeight, (double) ai.y);
            jSONObject3.put("dim", jSONObject6);
            jSONObject3.put("connection", c());
            jSONObject3.put("volume", d());
            jSONObject3.put("soundEnabled", ai.s);
            jSONObject3.put("mac", this.f);
            jSONObject3.put("model", ax.e());
            if ("NATIVE".equals("CORONA")) {
                jSONObject3.put("corona", true);
            }
            String e2 = ax.e(ai.e());
            if (e2 != null && e2.length() > 0) {
                jSONObject3.put("networkOperator", e2);
            }
            if (!ax.b(this.e)) {
                jSONObject3.put("serial", this.e);
            }
            jSONObject4.put("gender", VunglePub.Gender.toString(this.b));
            jSONObject4.put("age", this.c);
            jSONObject5.put("lat", this.h.a);
            jSONObject5.put("long", this.h.b);
            jSONObject4.put("location", jSONObject5);
            jSONObject2.put("deviceInfo", jSONObject3);
            jSONObject2.put("demo", jSONObject4);
            jSONObject2.put("pubAppId", this.i);
            String f2 = av.f();
            if (f2 != null) {
                jSONObject2.put("lastError", f2);
            }
            jSONObject2.put("isu", this.d);
            as.b("QA", "requestJSON : " + jSONObject2.toString(3));
        } catch (JSONException e3) {
            as.a(d.u, "JSONException", e3);
        }
        return jSONObject2.toString();
    }
}
