package com.vungle.sdk;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;

/* compiled from: vungle */
class ag extends FrameLayout {
    private final double a = 1.0d;
    private final double b = 1.0d;

    public ag(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int i;
        int i2;
        int size = View.MeasureSpec.getSize(widthMeasureSpec);
        int size2 = View.MeasureSpec.getSize(heightMeasureSpec);
        double d = (((double) size) * 1.0d) / 1.0d;
        if (d > ((double) size2)) {
            int i3 = size2;
            i = (int) ((((double) size2) * 1.0d) / 1.0d);
            i2 = i3;
        } else {
            i = size;
            i2 = (int) d;
        }
        super.onMeasure(View.MeasureSpec.makeMeasureSpec(i, 1073741824), View.MeasureSpec.makeMeasureSpec(i2, 1073741824));
    }
}
