package com.vungle.sdk;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: vungle */
class h {
    private JSONArray a;
    private long b;
    private long c;
    private long d;
    private ArrayList<i> e;

    public h() {
        this.a = null;
        this.c = 0;
        this.e = null;
        this.a = new JSONArray();
        this.e = new ArrayList<>(0);
    }

    public String a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("clickedThrough", this.a);
            jSONObject.put("adStartTime", this.b);
            jSONObject.put("adDuration", this.c);
            jSONObject.put("ttDownload", this.d);
            if (!this.e.isEmpty() && this.e != null) {
                JSONArray jSONArray = new JSONArray();
                int size = this.e.size();
                for (int i = 0; i < size; i++) {
                    jSONArray.put(this.e.get(i).d());
                }
                jSONObject.put("plays", jSONArray);
            }
            j a2 = av.a();
            if (a2 != null) {
                return a2.a(jSONObject);
            }
        } catch (JSONException e2) {
            as.a(d.u, "JSONException", e2);
        }
        return null;
    }

    public ArrayList<i> b() {
        return this.e;
    }

    public void a(String str) {
        if (this.a == null) {
            this.a = new JSONArray();
        }
        this.a.put(str);
    }

    public void a(long j) {
        this.b = j;
    }

    public void b(long j) {
        this.c = j;
    }

    public void c(long j) {
        this.d = j;
    }
}
