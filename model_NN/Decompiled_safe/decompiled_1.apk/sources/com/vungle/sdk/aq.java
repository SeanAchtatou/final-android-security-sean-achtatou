package com.vungle.sdk;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/* compiled from: vungle */
abstract class aq {
    public static boolean a(String str, String str2) {
        File file = new File(str2);
        if (!file.exists()) {
            file.mkdirs();
            if (!file.isDirectory()) {
                return false;
            }
        }
        if (!file.isDirectory()) {
            return false;
        }
        try {
            File file2 = new File(str);
            if (!file2.exists()) {
                as.c("unzipInto", "Provided zip-file does not exist.");
                return false;
            }
            ZipFile zipFile = new ZipFile(file2);
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            while (entries.hasMoreElements()) {
                ZipEntry zipEntry = (ZipEntry) entries.nextElement();
                if (zipEntry.isDirectory()) {
                    File file3 = new File(str2, zipEntry.getName());
                    file3.mkdirs();
                    if (!file3.isDirectory()) {
                        return false;
                    }
                } else {
                    InputStream inputStream = zipFile.getInputStream(zipEntry);
                    FileOutputStream fileOutputStream = new FileOutputStream(new File(str2, zipEntry.getName()));
                    byte[] bArr = new byte[2048];
                    while (true) {
                        int read = inputStream.read(bArr, 0, 2048);
                        if (read < 0) {
                            break;
                        }
                        fileOutputStream.write(bArr, 0, read);
                    }
                    inputStream.close();
                    fileOutputStream.close();
                }
            }
            return true;
        } catch (IOException e) {
            as.c("unzipInto", "IO Exception during decompression: " + e.getMessage());
            return false;
        }
    }
}
