package com.vungle.sdk;

import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: vungle */
abstract class aw {
    private static double a = 0.0d;
    private static double b = 0.0d;
    private static Timer c = null;
    private static boolean d = false;

    public static synchronized void a() {
        synchronized (aw.class) {
            if (d) {
                if (c != null) {
                    c.cancel();
                    c = null;
                }
            } else if (c == null) {
                c = new Timer();
                c.schedule(new c(), 10000);
                a = ((double) System.currentTimeMillis()) / 1000.0d;
            }
        }
    }

    public static synchronized void b() {
        synchronized (aw.class) {
            if (d) {
                if (c == null) {
                    c = new Timer();
                    c.schedule(new a(), 10000);
                    b = ((double) System.currentTimeMillis()) / 1000.0d;
                }
            } else if (c != null) {
                c.cancel();
                c = null;
            }
        }
    }

    /* access modifiers changed from: private */
    public static synchronized void e() {
        synchronized (aw.class) {
            d = true;
            c = null;
            as.a("VungleSession", "Sending SESSION START.");
            g();
        }
    }

    /* access modifiers changed from: private */
    public static synchronized void f() {
        synchronized (aw.class) {
            d = false;
            c = null;
            as.a("VungleSession", "Sending SESSION END.");
            h();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.vungle.sdk.aw.a(boolean, java.lang.String):java.lang.String
     arg types: [int, java.lang.String]
     candidates:
      com.vungle.sdk.aw.a(java.lang.String, java.lang.String):void
      com.vungle.sdk.aw.a(boolean, java.lang.String):java.lang.String */
    private static void g() {
        a(d.d(), a(false, av.a().b()));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.vungle.sdk.aw.a(boolean, java.lang.String):java.lang.String
     arg types: [int, java.lang.String]
     candidates:
      com.vungle.sdk.aw.a(java.lang.String, java.lang.String):void
      com.vungle.sdk.aw.a(boolean, java.lang.String):java.lang.String */
    private static void h() {
        a(d.e(), a(true, av.a().b()));
    }

    private static void a(String str, String str2) {
        b bVar = new b();
        bVar.a = str;
        bVar.b = str2;
        new Thread(bVar, "VungleSessionThread").start();
    }

    private static String a(boolean z, String str) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("start", a);
            if (z) {
                jSONObject.put("end", b);
            }
            jSONObject.put("isu", ax.b(ai.e()));
            jSONObject.put("pubAppId", str);
            return jSONObject.toString();
        } catch (JSONException e) {
            as.a(d.u, "JSONException", e);
            return null;
        }
    }

    /* compiled from: vungle */
    private static class b implements Runnable {
        public String a;
        public String b;

        private b() {
        }

        public void run() {
            as.b("SessionAsync", "POST-ing to: " + this.a + " with: " + this.b);
            ar.a(this.a, this.b);
        }
    }

    /* compiled from: vungle */
    private static class c extends TimerTask {
        private c() {
        }

        public void run() {
            aw.e();
        }
    }

    /* compiled from: vungle */
    private static class a extends TimerTask {
        private a() {
        }

        public void run() {
            aw.f();
        }
    }
}
