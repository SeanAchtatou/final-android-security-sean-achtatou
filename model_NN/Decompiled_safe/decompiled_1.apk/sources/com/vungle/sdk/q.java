package com.vungle.sdk;

import com.flurry.android.AdCreative;
import com.vungle.sdk.w;

/* compiled from: vungle */
class q implements w.b {
    final /* synthetic */ VungleAdvert a;

    q(VungleAdvert vungleAdvert) {
        this.a = vungleAdvert;
    }

    public void a() {
        as.a("PreRoll-callback", "'Watch' button clicked.");
        av.c().a("watch");
        this.a.c();
        this.a.e();
    }

    public void b() {
        as.a("PreRoll-callback", "'Close' button clicked.");
        av.c().a("close");
        int unused = this.a.g = 4;
        this.a.e();
    }

    public void c() {
        as.a("PreRoll-callback", "'Download' button clicked.");
        int unused = this.a.g = 4;
        this.a.b((String) null);
        av.c().a("download");
        this.a.e();
    }

    public void a(String str) {
        as.a("PreRoll-callback", "'Custom' button clicked.");
        int unused = this.a.g = 4;
        this.a.b(str);
        av.c().a(AdCreative.kFormatCustom);
        this.a.e();
    }
}
