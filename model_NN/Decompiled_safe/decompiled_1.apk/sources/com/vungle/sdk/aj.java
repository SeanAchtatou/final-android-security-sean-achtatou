package com.vungle.sdk;

import android.content.Context;
import android.net.Uri;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONObject;

/* compiled from: vungle */
class aj {
    /* access modifiers changed from: private */
    public boolean a = false;
    private TimerTask b = null;
    private final Context c;
    private int d = -1;
    /* access modifiers changed from: private */
    public int e = -1;
    private int f = 0;
    private long g = -1;
    private String h = null;
    private b i = null;

    /* compiled from: vungle */
    public interface a {
        void a(int i);
    }

    public aj(Context context) {
        this.c = context;
    }

    private void f() {
        double d2 = 0.0d;
        ArrayList<i> b2 = av.c().b();
        Iterator<i> it = b2.iterator();
        double d3 = 0.0d;
        while (it.hasNext()) {
            i next = it.next();
            d3 = (double) next.b();
            if (((double) next.c()) > d2) {
                d2 = (double) next.c();
            }
        }
        if (b2.size() > 0) {
            d.a(d2 / 1000.0d, d3 / 1000.0d);
        }
    }

    public void a() {
        if (ai.i) {
            as.b(d.t, "prepareReportAdData()");
            av.a(av.c().a());
            f();
            av.a(new h());
            if (ax.f(this.c)) {
                b();
                return;
            }
            this.e = 5;
            j();
        }
    }

    public void b() {
        if (this.b != null) {
            this.b.cancel();
        }
        this.b = null;
        if (ax.f(this.c)) {
            as.b(d.t, "Starting ReportAd Asynchronously");
            if (ai.i) {
                this.i = new b(this, 1);
                this.i.a();
                return;
            }
            as.b("VungleConnectionHandler", "State has somehow gotten invalid...");
            return;
        }
        this.e = 5;
        j();
    }

    /* access modifiers changed from: private */
    public void g() {
        try {
            ArrayList<String> b2 = av.b();
            as.a("VungleConnectionHandler", "Starting a metric data flush...");
            if (!ax.f(ai.e())) {
                as.b("VungleConnectionHandler", "  -- Skipping due to network state");
            } else if (b2 != null && !b2.isEmpty()) {
                Iterator<String> it = b2.iterator();
                while (it.hasNext()) {
                    String next = it.next();
                    if (ar.a(d.b(), next).contains(d.C)) {
                        as.b("VungleConnectionHandler", "  -- Sent: " + next);
                        it.remove();
                    }
                }
                av.a(b2);
            } else {
                return;
            }
            a(b2);
        } catch (Throwable th) {
            av.a(th);
        }
    }

    private void a(ArrayList<String> arrayList) {
        if (arrayList != null && !arrayList.isEmpty()) {
            this.e = 5;
            j();
        }
    }

    public void c() {
        if (!ai.m || ai.i) {
            ai.j = false;
            if (this.b != null) {
                this.b.cancel();
            }
            this.b = null;
            if (ax.g(this.c) == null) {
                as.b(d.t, "Starting AdRequest Asynchronously");
                if (ai.i) {
                    this.d = 0;
                    this.i = new b(this, 0);
                    this.i.a();
                    return;
                }
                return;
            }
            this.e = 4;
            j();
        }
    }

    /* access modifiers changed from: private */
    public void a(a aVar) {
        ax.i(this.c);
        as.b("requestAdPayLoad()", "enter");
        this.h = null;
        if (ai.q) {
            ai.q = false;
            ah e2 = ah.e();
            if (e2 != null) {
                this.h = e2.d();
                if (this.h != null) {
                    as.b("RequestAd", "RequestAd packet restored from cache.");
                }
            }
        }
        if (this.h == null) {
            this.h = ar.a(d.a(), av.a().b((JSONObject) null));
        }
        if (at.a(this.h) == d.l) {
            aVar.a(d.l);
            return;
        }
        ai.j = false;
        ai.k = true;
        ah.e().a(av.a(), new ak(this).a(aVar));
    }

    /* access modifiers changed from: private */
    public void h() {
        boolean z;
        String i2 = i();
        as.b(d.t, "GET Request URL : " + i2);
        if (!ax.b(i2)) {
            ArrayList<String> a2 = ar.a(i2);
            if (a2 != null) {
                Iterator<String> it = a2.iterator();
                if (it.hasNext() && d.C.equals(it.next())) {
                    z = true;
                    ax.a(this.c, d.W, d.X, z);
                    as.b(d.t, "Install report response: " + a2.get(0));
                }
            }
            z = false;
            ax.a(this.c, d.W, d.X, z);
            as.b(d.t, "Install report response: " + a2.get(0));
        }
        synchronized (ai.l) {
            ai.l = false;
        }
    }

    private String i() {
        Uri.Builder buildUpon = Uri.parse(d.c()).buildUpon();
        buildUpon.appendQueryParameter("isu", ax.c(this.c));
        buildUpon.appendQueryParameter("app_id", av.a().b());
        String a2 = ax.a(this.c);
        if (!(a2 == null || a2.length() == 0)) {
            buildUpon.appendQueryParameter("ma", a2);
        }
        String a3 = ax.a();
        if (!ax.b(a3)) {
            buildUpon.appendQueryParameter("serial", a3);
        }
        as.b("trackInstall", "Request url: " + buildUpon.build().toString());
        return buildUpon.build().toString();
    }

    public void d() {
        synchronized (ai.l) {
            if (!ai.k.booleanValue()) {
                ai.l = true;
                this.i = new b(this, 3);
                this.i.a();
            }
        }
    }

    /* compiled from: vungle */
    private static class b {
        WeakReference<aj> a;
        int b;
        private Thread c = new Thread(new a(this, null), "VungleNetworkTaskThread");

        public b(aj ajVar, int i) {
            this.a = new WeakReference<>(ajVar);
            this.b = i;
            this.c.setDaemon(true);
        }

        public void a() {
            this.c.start();
        }

        /* compiled from: vungle */
        private class a implements Runnable {
            private a() {
            }

            /* synthetic */ a(b bVar, ak akVar) {
                this();
            }

            /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
            public void run() {
                try {
                    aj ajVar = b.this.a.get();
                    if (ajVar == null) {
                        as.d("VungleNetworkTask", "VungleConnectionHandler object was collected.");
                        return;
                    }
                    switch (b.this.b) {
                        case 0:
                            as.b("VungleNetworkTask", "Performing requestAd...");
                            ajVar.a(new am(this, ajVar));
                            return;
                        case 1:
                            as.b("VungleNetworkTask", "Flushing metric queue...");
                            ajVar.g();
                            return;
                        case 2:
                        default:
                            as.c("VungleNetworkTask", "Unexpected event type: " + b.this.b);
                            return;
                        case 3:
                            as.b("VungleNetworkTask", "Tracking an installation...");
                            ajVar.h();
                            return;
                    }
                    av.a(th);
                } catch (Throwable th) {
                    av.a(th);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        this.b = new al(this);
        k();
    }

    private void k() {
        long l;
        this.f++;
        if (this.f > ai.c()) {
            this.a = false;
            ai.k = false;
            return;
        }
        if (this.a) {
            this.a = false;
            l = ai.a();
        } else {
            l = l();
        }
        if (l > -1) {
            new Timer().schedule(this.b, l);
            as.b(d.t, "Sleep Timer Starts for: " + (l / 1000) + " Seconds");
            as.b(d.t, "Timer Type " + this.e);
        } else if (av.e() != null) {
            av.a((c) null);
        }
    }

    private long l() {
        if (this.g <= 0) {
            this.g = ai.b();
        } else {
            this.g *= 2;
        }
        as.b(d.t, "Retry Count : " + this.f);
        as.b(d.t, "Current Sleep Time : " + this.g);
        return this.g;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0014, code lost:
        com.vungle.sdk.ai.k = true;
        new com.vungle.sdk.aj(com.vungle.sdk.ai.e()).c();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void e() {
        /*
            java.lang.Boolean r0 = com.vungle.sdk.ai.k
            monitor-enter(r0)
            boolean r1 = com.vungle.sdk.ai.d()     // Catch:{ all -> 0x0028 }
            if (r1 != 0) goto L_0x0013
            java.lang.Boolean r1 = com.vungle.sdk.ai.k     // Catch:{ all -> 0x0028 }
            boolean r1 = r1.booleanValue()     // Catch:{ all -> 0x0028 }
            if (r1 == 0) goto L_0x0013
            monitor-exit(r0)     // Catch:{ all -> 0x0028 }
        L_0x0012:
            return
        L_0x0013:
            monitor-exit(r0)     // Catch:{ all -> 0x0028 }
            r0 = 1
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            com.vungle.sdk.ai.k = r0
            com.vungle.sdk.aj r0 = new com.vungle.sdk.aj
            android.content.Context r1 = com.vungle.sdk.ai.e()
            r0.<init>(r1)
            r0.c()
            goto L_0x0012
        L_0x0028:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0028 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.sdk.aj.e():void");
    }
}
