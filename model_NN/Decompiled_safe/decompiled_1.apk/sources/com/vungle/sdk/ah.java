package com.vungle.sdk;

import android.os.Environment;
import android.os.SystemClock;
import com.vungle.sdk.an;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

/* compiled from: vungle */
class ah {
    private static volatile ah b = null;
    public c a;
    /* access modifiers changed from: private */
    public ArrayList<c> c = new ArrayList<>();
    /* access modifiers changed from: private */
    public long d = Long.MIN_VALUE;
    /* access modifiers changed from: private */
    public Boolean e = false;
    private String f = null;
    private long g = Long.MIN_VALUE;

    /* compiled from: vungle */
    public interface a {
        void a();

        void a(c cVar);
    }

    /* compiled from: vungle */
    private class e extends Exception {
        private e() {
        }
    }

    public static boolean a() {
        return "mounted".equals(Environment.getExternalStorageState());
    }

    public static String b() {
        if (ai.e() == null) {
            as.c("VungleCache", "Cache being initialized before context is set.");
        }
        File externalCacheDir = ai.e().getExternalCacheDir();
        if (externalCacheDir == null) {
            return null;
        }
        return externalCacheDir.getAbsolutePath() + File.separator + ".VungleCacheDir";
    }

    public static boolean a(File file) {
        if (file.isDirectory()) {
            String[] list = file.list();
            for (String file2 : list) {
                if (!a(new File(file, file2))) {
                    return false;
                }
            }
        }
        return file.delete();
    }

    /* compiled from: vungle */
    public class c {
        /* access modifiers changed from: private */
        public String b;
        private String c;
        private String d;
        private String e;
        private long f;

        public c(String str, String str2, String str3, String str4, long j) {
            this.b = str;
            this.c = str2;
            this.d = str3;
            this.e = str4;
            this.f = j;
        }

        public String a() {
            return ah.b() + File.separator + this.b;
        }

        public String b() {
            if (this.c == null) {
                return null;
            }
            return a() + File.separator + this.c;
        }

        public String c() {
            if (this.d == null) {
                return null;
            }
            return a() + File.separator + "pre" + File.separator + this.d;
        }

        public String d() {
            if (this.e == null) {
                return null;
            }
            return a() + File.separator + "post" + File.separator + this.e;
        }

        public long e() {
            return this.f;
        }
    }

    private void a(c cVar) {
        a(new File(cVar.a()));
    }

    /* access modifiers changed from: private */
    public void f() {
        int i;
        long j;
        as.a("VungleCache", "Pruning entries...");
        long currentTimeMillis = System.currentTimeMillis() / 1000;
        Iterator<c> it = this.c.iterator();
        while (it.hasNext()) {
            c next = it.next();
            if (this.a != null && this.a.b.equals(next.b)) {
                as.a("VungleCache", "Skipping the current ad for expiration pruning...");
            } else if (next.e() < currentTimeMillis) {
                as.a("VungleCache", "Removing expired campaign: " + next.b);
                a(next);
                it.remove();
            }
        }
        int size = this.c.size() - 10;
        if (size > 0) {
            int i2 = 0;
            while (i2 < size) {
                long j2 = Long.MAX_VALUE;
                int i3 = -1;
                int i4 = 0;
                while (i4 < this.c.size()) {
                    long e2 = currentTimeMillis - this.c.get(i4).e();
                    if (this.a != null && this.a.b.equals(this.c.get(i4).b)) {
                        as.a("VungleCache", "Skipping current in cases where we want to remove an extra campaign...");
                    }
                    if (e2 < j2) {
                        i = i4;
                        j = e2;
                    } else {
                        i = i3;
                        j = j2;
                    }
                    i4++;
                    j2 = j;
                    i3 = i;
                }
                if (i3 >= 0) {
                    c cVar = this.c.get(i3);
                    as.a("VungleCache", "Removing extra campaign: " + cVar.b);
                    a(cVar);
                    this.c.remove(i3);
                    i2++;
                } else {
                    return;
                }
            }
        }
    }

    private boolean c(String str) {
        String lowerCase = str.toLowerCase();
        for (String endsWith : new String[]{".mp4", ".avi", ".3gp", ".webm"}) {
            if (lowerCase.endsWith(endsWith)) {
                return true;
            }
        }
        return false;
    }

    public static String a(String str) {
        File file = new File(str);
        String[] strArr = {".htm", ".html"};
        if (!file.isDirectory()) {
            return null;
        }
        for (String lowerCase : file.list()) {
            String lowerCase2 = lowerCase.toLowerCase();
            for (String endsWith : strArr) {
                if (lowerCase2.endsWith(endsWith)) {
                    return lowerCase2;
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    public c d(String str) {
        long j;
        String str2;
        String str3 = null;
        File file = new File(str);
        if (!file.isDirectory()) {
            a(file);
            return null;
        }
        String[] split = str.split(File.separator);
        String str4 = split[split.length - 1];
        String[] list = file.list();
        int length = list.length;
        long j2 = Long.MIN_VALUE;
        String str5 = null;
        String str6 = null;
        int i = 0;
        while (i < length) {
            String str7 = list[i];
            if (str7.equalsIgnoreCase("pre")) {
                j = j2;
                str2 = a(str + File.separator + "pre");
                str7 = str3;
            } else if (str7.equalsIgnoreCase("post")) {
                str5 = a(str + File.separator + "post");
                str7 = str3;
                j = j2;
                str2 = str6;
            } else if (str7.equalsIgnoreCase("expire")) {
                try {
                    Scanner scanner = new Scanner(new File(str, str7));
                    if (scanner.hasNextLong()) {
                        j2 = scanner.nextLong();
                    }
                    scanner.close();
                    str7 = str3;
                    j = j2;
                    str2 = str6;
                } catch (FileNotFoundException e2) {
                    as.c("VungleCache", "Failed to read expiration value, due to missing value. :/");
                    str7 = str3;
                    j = j2;
                    str2 = str6;
                }
            } else if (c(str7)) {
                j = j2;
                str2 = str6;
            } else {
                str7 = str3;
                j = j2;
                str2 = str6;
            }
            i++;
            str3 = str7;
            str6 = str2;
            j2 = j;
        }
        if (str4 != null && j2 != Long.MIN_VALUE && ((str6 != null && str5 != null && str3 != null) || ((str6 == null && str5 != null && str3 != null) || ((str6 != null && str5 == null && str3 == null) || (str6 == null && str5 != null && str3 == null))))) {
            return new c(str4, str3, str6, str5, j2);
        }
        as.a("VungleCache", "PRE " + str6 + "|POST " + str5 + "|VID " + str3);
        as.a("VungleCache", "Malformed cache directory detected. Removing: " + file);
        a(file);
        return null;
    }

    private ah() {
        String b2 = b();
        if (b2 == null) {
            throw new e();
        }
        File file = new File(b2);
        file.mkdirs();
        if (!file.isDirectory()) {
            as.c("VungleCache", "Failed to create cache directory structure.");
            return;
        }
        for (String str : file.list()) {
            File file2 = new File(b2, str);
            if (file2.isDirectory()) {
                c d2 = d(file2.getAbsolutePath());
                if (d2 != null) {
                    this.c.add(d2);
                }
            } else if (file2.isFile() && str.equals("last_request")) {
                as.b("VungleCache", "RequestAd Cache -- request file found");
                try {
                    FileReader fileReader = new FileReader(file2);
                    BufferedReader bufferedReader = new BufferedReader(fileReader);
                    String readLine = bufferedReader.readLine();
                    String str2 = "";
                    while (true) {
                        String readLine2 = bufferedReader.readLine();
                        if (readLine2 == null) {
                            break;
                        }
                        str2 = str2 + readLine2;
                    }
                    this.g = Long.parseLong(readLine);
                    this.f = str2;
                    bufferedReader.close();
                    fileReader.close();
                } catch (Throwable th) {
                    file2.delete();
                    this.f = null;
                    this.g = Long.MIN_VALUE;
                }
            }
        }
        f();
    }

    /* compiled from: vungle */
    private class b implements an.b {
        private Integer b;
        private boolean c;
        private d d;

        private b() {
            this.b = 0;
            this.c = true;
            this.d = null;
        }

        public void a(d dVar) {
            this.d = dVar;
        }

        public void a() {
            synchronized (this.b) {
                Integer valueOf = Integer.valueOf(this.b.intValue() + 1);
                this.b = valueOf;
                if (valueOf.intValue() == 3) {
                    d();
                }
            }
        }

        public void b() {
            synchronized (this.b) {
                Integer num = this.b;
                this.b = Integer.valueOf(this.b.intValue() - 1);
            }
        }

        public void a(String str) {
            a();
        }

        public void c() {
            synchronized (this.b) {
                this.c = false;
                a();
            }
        }

        private void d() {
            if (this.c) {
                this.d.a();
            } else {
                this.d.b();
            }
        }
    }

    /* compiled from: vungle */
    private class d {
        private an b = null;
        private an c = null;
        private an d = null;
        private b e;
        private a f;
        private String g;

        public d(String str, b bVar, a aVar) {
            this.e = bVar;
            this.f = aVar;
            this.g = str;
        }

        public void a(an anVar) {
            this.b = anVar;
        }

        public void b(an anVar) {
            this.c = anVar;
        }

        public void c(an anVar) {
            this.d = anVar;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.vungle.sdk.ah.a(com.vungle.sdk.ah, java.lang.Boolean):java.lang.Boolean
         arg types: [com.vungle.sdk.ah, int]
         candidates:
          com.vungle.sdk.ah.a(com.vungle.sdk.ah, java.lang.String):com.vungle.sdk.ah$c
          com.vungle.sdk.ah.a(com.vungle.sdk.j, com.vungle.sdk.ah$a):void
          com.vungle.sdk.ah.a(com.vungle.sdk.ah, java.lang.Boolean):java.lang.Boolean */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x009c  */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x00ee A[SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a() {
            /*
                r8 = this;
                r3 = 0
                r7 = 1
                r6 = 0
                java.lang.String r5 = "VungleCache"
                com.vungle.sdk.an r0 = r8.b
                if (r0 == 0) goto L_0x0175
                com.vungle.sdk.an r0 = r8.b
                boolean r0 = r0.c()
                if (r0 == 0) goto L_0x0175
                java.io.File r0 = new java.io.File
                com.vungle.sdk.an r1 = r8.b
                java.lang.String r1 = r1.d()
                r0.<init>(r1)
            L_0x001c:
                com.vungle.sdk.an r1 = r8.c
                if (r1 == 0) goto L_0x0172
                com.vungle.sdk.an r1 = r8.c
                boolean r1 = r1.c()
                if (r1 == 0) goto L_0x0172
                java.io.File r1 = new java.io.File
                com.vungle.sdk.an r2 = r8.c
                java.lang.String r2 = r2.d()
                r1.<init>(r2)
            L_0x0033:
                com.vungle.sdk.an r2 = r8.d
                if (r2 == 0) goto L_0x016f
                com.vungle.sdk.an r2 = r8.d
                boolean r2 = r2.c()
                if (r2 == 0) goto L_0x016f
                java.io.File r2 = new java.io.File
                com.vungle.sdk.an r3 = r8.d
                java.lang.String r3 = r3.d()
                r2.<init>(r3)
            L_0x004a:
                if (r0 == 0) goto L_0x016b
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r4 = r0.getParent()
                java.lang.StringBuilder r3 = r3.append(r4)
                java.lang.String r4 = java.io.File.separator
                java.lang.StringBuilder r3 = r3.append(r4)
                java.lang.String r4 = "pre"
                java.lang.StringBuilder r3 = r3.append(r4)
                java.lang.String r3 = r3.toString()
                java.lang.String r0 = r0.getAbsolutePath()
                boolean r0 = com.vungle.sdk.aq.a(r0, r3)
                if (r0 != 0) goto L_0x016b
                java.lang.String r0 = "VungleCache"
                java.lang.String r0 = "Unzip of pre-roll failed."
                com.vungle.sdk.as.c(r5, r0)
                java.io.File r0 = new java.io.File
                r0.<init>(r3)
                com.vungle.sdk.ah.a(r0)
                com.vungle.sdk.an r0 = r8.b
                boolean r0 = r0.b()
                if (r0 == 0) goto L_0x0101
                java.lang.String r0 = "VungleCache"
                java.lang.String r0 = "Attempting re-download of pre-roll..."
                com.vungle.sdk.as.a(r5, r0)
                com.vungle.sdk.ah$b r0 = r8.e
                r0.b()
                r0 = r6
            L_0x0097:
                r3 = r6
            L_0x0098:
                if (r1 == 0) goto L_0x009a
            L_0x009a:
                if (r2 == 0) goto L_0x0168
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r4 = r2.getParent()
                java.lang.StringBuilder r1 = r1.append(r4)
                java.lang.String r4 = java.io.File.separator
                java.lang.StringBuilder r1 = r1.append(r4)
                java.lang.String r4 = "post"
                java.lang.StringBuilder r1 = r1.append(r4)
                java.lang.String r1 = r1.toString()
                java.lang.String r2 = r2.getAbsolutePath()
                boolean r2 = com.vungle.sdk.aq.a(r2, r1)
                if (r2 != 0) goto L_0x0168
                java.lang.String r2 = "VungleCache"
                java.lang.String r2 = "Unzip of post-roll failed."
                com.vungle.sdk.as.c(r5, r2)
                java.io.File r2 = new java.io.File
                r2.<init>(r1)
                com.vungle.sdk.ah.a(r2)
                com.vungle.sdk.an r1 = r8.d
                boolean r1 = r1.b()
                if (r1 == 0) goto L_0x0103
                java.lang.String r1 = "VungleCache"
                java.lang.String r1 = "Attempting re-download of post-roll..."
                com.vungle.sdk.as.a(r5, r1)
                com.vungle.sdk.ah$b r1 = r8.e
                r1.b()
            L_0x00e6:
                r1 = r6
            L_0x00e7:
                com.vungle.sdk.ah r2 = com.vungle.sdk.ah.this
                java.lang.Boolean r2 = r2.e
                monitor-enter(r2)
                com.vungle.sdk.ah r3 = com.vungle.sdk.ah.this     // Catch:{ all -> 0x0105 }
                r4 = 0
                java.lang.Boolean r4 = java.lang.Boolean.valueOf(r4)     // Catch:{ all -> 0x0105 }
                java.lang.Boolean unused = r3.e = r4     // Catch:{ all -> 0x0105 }
                monitor-exit(r2)     // Catch:{ all -> 0x0105 }
                if (r1 != 0) goto L_0x0108
                if (r0 == 0) goto L_0x0100
                r8.b()
            L_0x0100:
                return
            L_0x0101:
                r0 = r7
                goto L_0x0097
            L_0x0103:
                r0 = r7
                goto L_0x00e6
            L_0x0105:
                r0 = move-exception
                monitor-exit(r2)     // Catch:{ all -> 0x0105 }
                throw r0
            L_0x0108:
                long r0 = android.os.SystemClock.elapsedRealtime()
                com.vungle.sdk.ah r2 = com.vungle.sdk.ah.this
                long r2 = r2.d
                long r0 = r0 - r2
                com.vungle.sdk.h r2 = com.vungle.sdk.av.c()
                r2.c(r0)
                com.vungle.sdk.ah r0 = com.vungle.sdk.ah.this
                java.lang.String r1 = r8.g
                com.vungle.sdk.ah$c r0 = r0.d(r1)
                if (r0 == 0) goto L_0x0162
                java.lang.String r1 = "VungleCache"
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "New campaign ("
                java.lang.StringBuilder r1 = r1.append(r2)
                java.lang.String r2 = r0.b
                java.lang.StringBuilder r1 = r1.append(r2)
                java.lang.String r2 = ") successfully added."
                java.lang.StringBuilder r1 = r1.append(r2)
                java.lang.String r1 = r1.toString()
                com.vungle.sdk.as.a(r5, r1)
                com.vungle.sdk.ah r1 = com.vungle.sdk.ah.this
                r1.a = r0
                com.vungle.sdk.ah r1 = com.vungle.sdk.ah.this
                java.util.ArrayList r1 = r1.c
                r1.add(r0)
                com.vungle.sdk.ah r0 = com.vungle.sdk.ah.this
                r0.f()
                com.vungle.sdk.ah$a r0 = r8.f
                com.vungle.sdk.ah r1 = com.vungle.sdk.ah.this
                com.vungle.sdk.ah$c r1 = r1.a
                r0.a(r1)
                goto L_0x0100
            L_0x0162:
                com.vungle.sdk.ah$a r0 = r8.f
                r0.a()
                goto L_0x0100
            L_0x0168:
                r1 = r3
                goto L_0x00e7
            L_0x016b:
                r0 = r6
                r3 = r7
                goto L_0x0098
            L_0x016f:
                r2 = r3
                goto L_0x004a
            L_0x0172:
                r1 = r3
                goto L_0x0033
            L_0x0175:
                r0 = r3
                goto L_0x001c
            */
            throw new UnsupportedOperationException("Method not decompiled: com.vungle.sdk.ah.d.a():void");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.vungle.sdk.ah.a(com.vungle.sdk.ah, java.lang.Boolean):java.lang.Boolean
         arg types: [com.vungle.sdk.ah, int]
         candidates:
          com.vungle.sdk.ah.a(com.vungle.sdk.ah, java.lang.String):com.vungle.sdk.ah$c
          com.vungle.sdk.ah.a(com.vungle.sdk.j, com.vungle.sdk.ah$a):void
          com.vungle.sdk.ah.a(com.vungle.sdk.ah, java.lang.Boolean):java.lang.Boolean */
        public void b() {
            String[] split = this.g.split(File.separator);
            as.c("VungleCache", "Failed to acquire campaign: " + split[split.length - 1]);
            ah.a(new File(this.g));
            synchronized (ah.this.e) {
                Boolean unused = ah.this.e = (Boolean) false;
            }
            this.f.a();
        }
    }

    public void a(j jVar, a aVar) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        a a2 = jVar.a();
        String c2 = a2.c();
        Iterator<c> it = this.c.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            c next = it.next();
            if (next.b.equalsIgnoreCase(c2)) {
                as.a("VungleCache", "Found similar campaign in cache.");
                String b2 = next.b();
                String b3 = a2.b();
                boolean z5 = b3 == null || b3.length() == 0;
                if (b2 == null || b2.length() == 0) {
                    z = true;
                } else {
                    z = false;
                }
                if (z5 != z) {
                    as.a("VungleCache", "Mismatch with cached video.");
                    z2 = false;
                } else {
                    z2 = true;
                }
                String c3 = next.c();
                String f2 = a2.f();
                if ((c3 == null || c3.length() == 0) != (f2 == null || f2.length() == 0)) {
                    as.a("VungleCache", "Mismatch with cached pre-roll.");
                    z2 = false;
                }
                String d2 = next.d();
                String d3 = a2.d();
                if (d2 == null || d2.length() == 0) {
                    z3 = true;
                } else {
                    z3 = false;
                }
                if (d3 == null || d3.length() == 0) {
                    z4 = true;
                } else {
                    z4 = false;
                }
                if (z3 != z4) {
                    as.a("VungleCache", "Mismastch with cached post-roll.");
                    z2 = false;
                }
                if (!z2) {
                    as.a("VungleCache", "Removing outdated cache entry for: " + c2);
                    a(next);
                    it.remove();
                } else {
                    as.a("VungleCache", "Cache already has data for: " + c2);
                    File file = new File(next.a(), "expire");
                    file.delete();
                    try {
                        FileWriter fileWriter = new FileWriter(file);
                        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
                        bufferedWriter.write(String.valueOf(a2.m()));
                        bufferedWriter.close();
                        fileWriter.close();
                    } catch (IOException e2) {
                        as.a("VungleCache", "Exception encountered while writing expiration file.", e2);
                    }
                    this.a = next;
                    aVar.a(next);
                    return;
                }
            }
        }
        b(jVar, aVar);
    }

    private void b(j jVar, a aVar) {
        a a2 = jVar.a();
        String f2 = a2.f();
        String d2 = a2.d();
        String c2 = a2.c();
        String b2 = a2.b();
        String i = a2.i();
        this.d = SystemClock.elapsedRealtime();
        synchronized (this.e) {
            this.e = true;
        }
        File file = new File(b(), c2);
        file.mkdirs();
        if (!file.isDirectory()) {
            as.b("insertAdvert", "Failed to create base directory for campaign.");
            synchronized (this.e) {
                this.e = false;
            }
            return;
        }
        File file2 = new File(file.getAbsoluteFile(), "expire");
        if (file2.exists()) {
            file2.delete();
        }
        try {
            FileWriter fileWriter = new FileWriter(file2);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(String.valueOf(a2.m()));
            bufferedWriter.close();
            fileWriter.close();
            b bVar = new b();
            d dVar = new d(file.getAbsolutePath(), bVar, aVar);
            bVar.a(dVar);
            if (f2 == null || f2.length() <= 0) {
                bVar.a();
            } else {
                an anVar = new an(f2, file.getAbsolutePath());
                anVar.a(bVar);
                anVar.a("pre_");
                dVar.a(anVar);
                anVar.a();
            }
            if (b2 == null || b2.length() <= 0) {
                bVar.a();
            } else {
                an anVar2 = new an(b2, file.getAbsolutePath(), i);
                anVar2.a(bVar);
                dVar.b(anVar2);
                anVar2.a();
            }
            if (d2 == null || d2.length() <= 0) {
                bVar.a();
                return;
            }
            an anVar3 = new an(d2, file.getAbsolutePath());
            anVar3.a(bVar);
            anVar3.a("post_");
            dVar.c(anVar3);
            anVar3.a();
        } catch (IOException e2) {
            as.a("insertAdvert", "Exception encountered while writing expiration file.", e2);
            synchronized (this.e) {
                this.e = false;
            }
        }
    }

    public synchronized void b(String str) {
        as.b("VungleCache", "RequestAd Cache -- sent to disk");
        try {
            FileWriter fileWriter = new FileWriter(new File(b(), "last_request"));
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(Long.toString(System.currentTimeMillis()) + "\n");
            bufferedWriter.write(str);
            bufferedWriter.close();
            fileWriter.close();
        } catch (IOException e2) {
            as.d("VungleCache", "Failed to place requestAd into file:" + e2.toString());
            c();
        }
        return;
    }

    public synchronized void c() {
        as.b("VungleCache", "RequestAd Cache -- invalidate!");
        new File(b(), "last_request").delete();
        this.f = null;
        this.g = Long.MIN_VALUE;
    }

    public synchronized String d() {
        String str;
        as.b("VungleCache", "RequestAd Cache -- get last one");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.f == null || currentTimeMillis - this.g >= 86400000) {
            c();
            str = null;
        } else {
            str = this.f;
        }
        return str;
    }

    public static ah e() {
        if (b == null) {
            synchronized (ah.class) {
                if (b == null) {
                    try {
                        b = new ah();
                    } catch (e e2) {
                        as.d("VungleCache", "External storage is either not present, or mounted. Cannot currently initialize.");
                        b = null;
                    }
                }
            }
        }
        return b;
    }
}
