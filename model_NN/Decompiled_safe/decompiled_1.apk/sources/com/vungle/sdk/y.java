package com.vungle.sdk;

import android.os.Handler;

/* compiled from: vungle */
class y extends Handler {
    final /* synthetic */ x a;

    y(x xVar) {
        this.a = xVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void handleMessage(android.os.Message r9) {
        /*
            r8 = this;
            r6 = 4652007308841189376(0x408f400000000000, double:1000.0)
            com.vungle.sdk.x r0 = r8.a
            java.lang.Object r0 = r0.q
            monitor-enter(r0)
            com.vungle.sdk.x r1 = r8.a     // Catch:{ all -> 0x005b }
            android.widget.VideoView r1 = r1.g     // Catch:{ all -> 0x005b }
            if (r1 == 0) goto L_0x001c
            com.vungle.sdk.x r1 = r8.a     // Catch:{ all -> 0x005b }
            boolean r1 = r1.r     // Catch:{ all -> 0x005b }
            if (r1 == 0) goto L_0x001e
        L_0x001c:
            monitor-exit(r0)     // Catch:{ all -> 0x005b }
        L_0x001d:
            return
        L_0x001e:
            com.vungle.sdk.x r1 = r8.a     // Catch:{ all -> 0x005b }
            boolean r1 = r1.s     // Catch:{ all -> 0x005b }
            if (r1 != 0) goto L_0x0034
            com.vungle.sdk.x r1 = r8.a     // Catch:{ all -> 0x005b }
            android.widget.ImageView r1 = r1.h     // Catch:{ all -> 0x005b }
            android.graphics.drawable.Drawable r1 = r1.getDrawable()     // Catch:{ all -> 0x005b }
            r2 = 0
            r1.setAlpha(r2)     // Catch:{ all -> 0x005b }
        L_0x0034:
            com.vungle.sdk.x r1 = r8.a     // Catch:{ all -> 0x005b }
            boolean r1 = r1.t     // Catch:{ all -> 0x005b }
            if (r1 != 0) goto L_0x0054
            com.vungle.sdk.x r1 = r8.a     // Catch:{ all -> 0x005b }
            android.widget.TextView r1 = r1.l     // Catch:{ all -> 0x005b }
            r2 = 0
            r1.setTextColor(r2)     // Catch:{ all -> 0x005b }
            com.vungle.sdk.x r1 = r8.a     // Catch:{ all -> 0x005b }
            android.widget.TextView r1 = r1.l     // Catch:{ all -> 0x005b }
            r2 = 1073741824(0x40000000, float:2.0)
            r3 = 0
            r4 = 0
            r5 = 0
            r1.setShadowLayer(r2, r3, r4, r5)     // Catch:{ all -> 0x005b }
        L_0x0054:
            int r1 = r9.what     // Catch:{ all -> 0x005b }
            switch(r1) {
                case 66: goto L_0x005e;
                default: goto L_0x0059;
            }     // Catch:{ all -> 0x005b }
        L_0x0059:
            monitor-exit(r0)     // Catch:{ all -> 0x005b }
            goto L_0x001d
        L_0x005b:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x005b }
            throw r1
        L_0x005e:
            com.vungle.sdk.x r1 = r8.a     // Catch:{ all -> 0x005b }
            android.widget.VideoView r1 = r1.g     // Catch:{ all -> 0x005b }
            int r1 = r1.getCurrentPosition()     // Catch:{ all -> 0x005b }
            double r1 = (double) r1     // Catch:{ all -> 0x005b }
            double r1 = r1 / r6
            com.vungle.sdk.x r3 = r8.a     // Catch:{ all -> 0x005b }
            boolean r3 = r3.p     // Catch:{ all -> 0x005b }
            if (r3 == 0) goto L_0x00b9
            com.vungle.sdk.x r3 = r8.a     // Catch:{ all -> 0x005b }
            int r3 = r3.w     // Catch:{ all -> 0x005b }
            double r3 = (double) r3     // Catch:{ all -> 0x005b }
            int r3 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r3 <= 0) goto L_0x00b9
            com.vungle.sdk.x r3 = r8.a     // Catch:{ all -> 0x005b }
            boolean r3 = r3.s     // Catch:{ all -> 0x005b }
            if (r3 != 0) goto L_0x00b9
            android.view.animation.AlphaAnimation r3 = new android.view.animation.AlphaAnimation     // Catch:{ all -> 0x005b }
            r4 = 0
            r5 = 1065353216(0x3f800000, float:1.0)
            r3.<init>(r4, r5)     // Catch:{ all -> 0x005b }
            r4 = 1000(0x3e8, double:4.94E-321)
            r3.setDuration(r4)     // Catch:{ all -> 0x005b }
            com.vungle.sdk.x r4 = r8.a     // Catch:{ all -> 0x005b }
            android.widget.ImageView r4 = r4.h     // Catch:{ all -> 0x005b }
            r4.startAnimation(r3)     // Catch:{ all -> 0x005b }
            com.vungle.sdk.x r3 = r8.a     // Catch:{ all -> 0x005b }
            r4 = 1
            boolean unused = r3.s = r4     // Catch:{ all -> 0x005b }
            com.vungle.sdk.x r3 = r8.a     // Catch:{ all -> 0x005b }
            android.widget.ImageView r3 = r3.h     // Catch:{ all -> 0x005b }
            android.graphics.drawable.Drawable r3 = r3.getDrawable()     // Catch:{ all -> 0x005b }
            r4 = 255(0xff, float:3.57E-43)
            r3.setAlpha(r4)     // Catch:{ all -> 0x005b }
            com.vungle.sdk.x r3 = r8.a     // Catch:{ all -> 0x005b }
            android.widget.ImageView r3 = r3.h     // Catch:{ all -> 0x005b }
            r3.bringToFront()     // Catch:{ all -> 0x005b }
        L_0x00b9:
            com.vungle.sdk.x r3 = r8.a     // Catch:{ all -> 0x005b }
            boolean r3 = r3.t     // Catch:{ all -> 0x005b }
            if (r3 != 0) goto L_0x010a
            com.vungle.sdk.x r3 = r8.a     // Catch:{ all -> 0x005b }
            int r3 = r3.v     // Catch:{ all -> 0x005b }
            double r3 = (double) r3     // Catch:{ all -> 0x005b }
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 <= 0) goto L_0x010a
            android.view.animation.AlphaAnimation r1 = new android.view.animation.AlphaAnimation     // Catch:{ all -> 0x005b }
            r2 = 0
            r3 = 1065353216(0x3f800000, float:1.0)
            r1.<init>(r2, r3)     // Catch:{ all -> 0x005b }
            r2 = 1000(0x3e8, double:4.94E-321)
            r1.setDuration(r2)     // Catch:{ all -> 0x005b }
            com.vungle.sdk.x r2 = r8.a     // Catch:{ all -> 0x005b }
            android.widget.TextView r2 = r2.l     // Catch:{ all -> 0x005b }
            r2.startAnimation(r1)     // Catch:{ all -> 0x005b }
            com.vungle.sdk.x r1 = r8.a     // Catch:{ all -> 0x005b }
            r2 = 1
            boolean unused = r1.t = r2     // Catch:{ all -> 0x005b }
            com.vungle.sdk.x r1 = r8.a     // Catch:{ all -> 0x005b }
            android.widget.TextView r1 = r1.l     // Catch:{ all -> 0x005b }
            r2 = -1
            r1.setTextColor(r2)     // Catch:{ all -> 0x005b }
            com.vungle.sdk.x r1 = r8.a     // Catch:{ all -> 0x005b }
            android.widget.TextView r1 = r1.l     // Catch:{ all -> 0x005b }
            r2 = 1073741824(0x40000000, float:2.0)
            r3 = 0
            r4 = 0
            r5 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r1.setShadowLayer(r2, r3, r4, r5)     // Catch:{ all -> 0x005b }
            com.vungle.sdk.x r1 = r8.a     // Catch:{ all -> 0x005b }
            android.widget.TextView r1 = r1.l     // Catch:{ all -> 0x005b }
            r1.bringToFront()     // Catch:{ all -> 0x005b }
        L_0x010a:
            com.vungle.sdk.x r1 = r8.a     // Catch:{ all -> 0x005b }
            android.widget.VideoView r1 = r1.g     // Catch:{ all -> 0x005b }
            int r1 = r1.getDuration()     // Catch:{ all -> 0x005b }
            com.vungle.sdk.x r2 = r8.a     // Catch:{ all -> 0x005b }
            android.widget.VideoView r2 = r2.g     // Catch:{ all -> 0x005b }
            int r2 = r2.getCurrentPosition()     // Catch:{ all -> 0x005b }
            int r1 = r1 - r2
            double r1 = (double) r1     // Catch:{ all -> 0x005b }
            double r1 = r1 / r6
            long r1 = java.lang.Math.round(r1)     // Catch:{ all -> 0x005b }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x005b }
            r3.<init>()     // Catch:{ all -> 0x005b }
            java.lang.String r4 = "Video ends in "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x005b }
            java.lang.StringBuilder r3 = r3.append(r1)     // Catch:{ all -> 0x005b }
            r4 = 1
            int r1 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r1 != 0) goto L_0x014f
            java.lang.String r1 = " second"
        L_0x013c:
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ all -> 0x005b }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x005b }
            com.vungle.sdk.x r2 = r8.a     // Catch:{ all -> 0x005b }
            android.widget.TextView r2 = r2.l     // Catch:{ all -> 0x005b }
            r2.setText(r1)     // Catch:{ all -> 0x005b }
            goto L_0x0059
        L_0x014f:
            java.lang.String r1 = " seconds"
            goto L_0x013c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.sdk.y.handleMessage(android.os.Message):void");
    }
}
