package com.vungle.sdk;

import android.content.DialogInterface;

/* compiled from: vungle */
class ab implements DialogInterface.OnCancelListener {
    final /* synthetic */ x a;

    ab(x xVar) {
        this.a = xVar;
    }

    public void onCancel(DialogInterface dialog) {
        as.a("Callback", "Dismiss Dialog");
        this.a.g.start();
        this.a.b = false;
    }
}
