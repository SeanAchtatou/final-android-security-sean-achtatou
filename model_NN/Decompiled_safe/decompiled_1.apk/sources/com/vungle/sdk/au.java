package com.vungle.sdk;

/* compiled from: vungle */
class au {
    /* access modifiers changed from: private */
    public static int a = 10000;
    private static Thread b = null;
    private static a c;

    public static synchronized void a() {
        synchronized (au.class) {
            if (b == null) {
                c = new a();
                b = new Thread(c, "VungleSDKStatusThread");
                b.setPriority(1);
                b.setDaemon(true);
                b.start();
            }
        }
    }

    public static synchronized void b() {
        synchronized (au.class) {
            if (b != null) {
                c.a();
                c = null;
                b = null;
            }
        }
    }

    /* compiled from: vungle */
    private static class a implements Runnable {
        private boolean a;

        private a() {
            this.a = true;
        }

        public void a() {
            this.a = false;
        }

        public void run() {
            while (this.a) {
                if (!ai.d() && !ai.j && !ai.k.booleanValue()) {
                    aj.e();
                }
                try {
                    Thread.sleep((long) au.a);
                } catch (InterruptedException e) {
                }
            }
        }
    }
}
