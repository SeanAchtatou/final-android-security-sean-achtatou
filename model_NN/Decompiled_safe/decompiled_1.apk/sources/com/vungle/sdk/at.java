package com.vungle.sdk;

import android.os.Build;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: vungle */
class at {
    public static int a(String str) {
        int i = d.l;
        boolean z = false;
        try {
            if (ax.b(str)) {
                return i;
            }
            JSONObject jSONObject = new JSONObject(str);
            as.b("QA", "responseJSON : " + jSONObject.toString(3));
            if (!jSONObject.isNull(d.Q)) {
                ai.a(((long) jSONObject.getInt(d.Q)) * d.s);
                return d.l;
            }
            if (!jSONObject.isNull(d.K) && !jSONObject.isNull(d.F) && !jSONObject.isNull(d.J)) {
                z = true;
            } else if (!jSONObject.isNull(d.F) && !jSONObject.isNull(d.J)) {
                z = true;
            } else if (!jSONObject.isNull(d.K)) {
                z = true;
            } else if (!jSONObject.isNull(d.J)) {
                z = true;
            }
            if (!z) {
                return d.l;
            }
            as.a("VungleParser", "Syncing requestAd to disk...");
            ah e = ah.e();
            if (e != null) {
                e.b(str);
            }
            return a(jSONObject);
        } catch (JSONException e2) {
            as.a(d.u, "JSONException", e2);
            int i2 = d.l;
            av.a().a((a) null);
            return i2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    private static int a(JSONObject jSONObject) {
        boolean z;
        a aVar = new a();
        aVar.a(jSONObject.getInt(d.N));
        aVar.b(jSONObject.getInt(d.O));
        aVar.i(jSONObject.getString(d.H));
        aVar.d(jSONObject.getString(d.G));
        aVar.c(jSONObject.getString(d.M));
        aVar.e(jSONObject.getString(d.I).replace('|', '_'));
        if (!jSONObject.isNull(d.J)) {
            aVar.f(jSONObject.getString(d.J));
        } else {
            aVar.f(d.B);
        }
        if (!jSONObject.isNull(d.E)) {
            aVar.a(jSONObject.getString(d.E));
        } else {
            aVar.a((String) null);
        }
        if (!jSONObject.isNull(d.F)) {
            aVar.b(jSONObject.getString(d.F));
        } else {
            aVar.b(d.B);
        }
        if (!jSONObject.isNull(d.K)) {
            aVar.g(jSONObject.getString(d.K));
        } else {
            aVar.g(d.B);
        }
        if (!jSONObject.isNull(d.T)) {
            aVar.a(jSONObject.getLong(d.T));
        }
        if (!jSONObject.isNull(d.R)) {
            aVar.d(jSONObject.getInt(d.R));
        }
        if (!jSONObject.isNull(d.S)) {
            aVar.c(jSONObject.getInt(d.S));
        }
        if (!jSONObject.isNull(d.P)) {
            aVar.h(jSONObject.getString(d.P));
        }
        if (!jSONObject.isNull(d.L)) {
            aVar.c(Long.parseLong(jSONObject.getString(d.L)));
        }
        if (!jSONObject.isNull(d.V)) {
            aVar.e(jSONObject.getInt(d.V));
        } else {
            aVar.e(d.f);
        }
        if (!jSONObject.isNull(d.U)) {
            aVar.b(((long) jSONObject.getInt(d.U)) * d.s);
        } else {
            aVar.b((long) d.g);
        }
        if (!jSONObject.isNull(d.ac)) {
            as.b("VungleParser", "New delay value provided by server.");
            int i = jSONObject.getInt(d.ac);
            aVar.f(i);
            d.a(i);
        } else {
            aVar.f(d.ad);
        }
        if (Build.VERSION.SDK_INT >= 9) {
            z = true;
        } else {
            z = false;
        }
        if (z && ai.r) {
            ai.C = 10;
        } else if (aVar.l() > aVar.k()) {
            ai.C = z ? 7 : 1;
        } else {
            ai.C = z ? 6 : 0;
        }
        int i2 = d.k;
        if (av.a() != null) {
            av.a().a(aVar);
        }
        return i2;
    }
}
