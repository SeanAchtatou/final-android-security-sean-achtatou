package com.vungle.sdk;

import android.view.animation.Animation;

/* compiled from: vungle */
class t implements Animation.AnimationListener {
    final /* synthetic */ VungleAdvert a;

    t(VungleAdvert vungleAdvert) {
        this.a = vungleAdvert;
    }

    public void onAnimationEnd(Animation animation) {
        if (this.a.a != null && this.a.j && this.a.i) {
            this.a.a.b();
        }
        if (this.a.b != null) {
            this.a.b.c();
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }
}
