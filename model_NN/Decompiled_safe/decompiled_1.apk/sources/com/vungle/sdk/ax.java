package com.vungle.sdk;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Base64;
import java.io.File;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.TimeZone;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: vungle */
class ax {
    public static String a(String str, String str2) {
        try {
            Mac instance = Mac.getInstance("HmacSHA256");
            instance.init(new SecretKeySpec(str2.getBytes(), instance.getAlgorithm()));
            return Base64.encodeToString(instance.doFinal(str.getBytes()), 8);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return str;
        } catch (InvalidKeyException e2) {
            e2.printStackTrace();
            return str;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x002d A[SYNTHETIC, Splitter:B:18:0x002d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] e(java.lang.String r6) {
        /*
            r0 = 0
            r1 = 1024(0x400, float:1.435E-42)
            byte[] r1 = new byte[r1]
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ all -> 0x0027 }
            r2.<init>(r6)     // Catch:{ all -> 0x0027 }
            java.lang.String r0 = "MD5"
            java.security.MessageDigest r0 = java.security.MessageDigest.getInstance(r0)     // Catch:{ all -> 0x005e }
        L_0x0010:
            int r3 = r2.read(r1)     // Catch:{ all -> 0x005e }
            if (r3 <= 0) goto L_0x001a
            r4 = 0
            r0.update(r1, r4, r3)     // Catch:{ all -> 0x005e }
        L_0x001a:
            r4 = -1
            if (r3 != r4) goto L_0x0010
            byte[] r0 = r0.digest()     // Catch:{ all -> 0x005e }
            if (r2 == 0) goto L_0x0026
            r2.close()     // Catch:{ FileNotFoundException -> 0x0059, NoSuchAlgorithmException -> 0x003b, IOException -> 0x0048 }
        L_0x0026:
            return r0
        L_0x0027:
            r2 = move-exception
            r5 = r2
            r2 = r0
            r0 = r5
        L_0x002b:
            if (r2 == 0) goto L_0x0030
            r2.close()     // Catch:{ FileNotFoundException -> 0x0031, NoSuchAlgorithmException -> 0x0057, IOException -> 0x0055 }
        L_0x0030:
            throw r0     // Catch:{ FileNotFoundException -> 0x0031, NoSuchAlgorithmException -> 0x0057, IOException -> 0x0055 }
        L_0x0031:
            r0 = move-exception
        L_0x0032:
            java.lang.String r2 = com.vungle.sdk.d.u
            java.lang.String r3 = "FileNotFoundException"
            com.vungle.sdk.as.a(r2, r3, r0)
            r0 = r1
            goto L_0x0026
        L_0x003b:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x003f:
            java.lang.String r2 = com.vungle.sdk.d.u
            java.lang.String r3 = "NoSuchAlgorithmException"
            com.vungle.sdk.as.a(r2, r3, r0)
            r0 = r1
            goto L_0x0026
        L_0x0048:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x004c:
            java.lang.String r2 = com.vungle.sdk.d.u
            java.lang.String r3 = "IOException"
            com.vungle.sdk.as.a(r2, r3, r0)
            r0 = r1
            goto L_0x0026
        L_0x0055:
            r0 = move-exception
            goto L_0x004c
        L_0x0057:
            r0 = move-exception
            goto L_0x003f
        L_0x0059:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0032
        L_0x005e:
            r0 = move-exception
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.sdk.ax.e(java.lang.String):byte[]");
    }

    public static String a(String str) {
        byte[] e = e(str);
        int length = e.length;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            sb.append(Character.forDigit((e[i] >> 4) & 15, 16));
            sb.append(Character.forDigit(e[i] & 15, 16));
        }
        return sb.toString();
    }

    public static boolean b(String str) {
        return str == null || d.B.equals(str);
    }

    public static String a(Context context) {
        try {
            return ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress();
        } catch (SecurityException e) {
            return null;
        }
    }

    public static String a() {
        String str = d.B;
        if (Build.VERSION.SDK_INT < 9) {
            return str;
        }
        try {
            return Build.SERIAL;
        } catch (NoSuchFieldError e) {
            return d.B;
        }
    }

    public static String b(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), "android_id");
    }

    public static String c(Context context) {
        String b = b(context);
        return (b == null || b.length() == 0) ? a() : b;
    }

    public static String b() {
        as.b(d.t, Locale.getDefault().getISO3Language());
        return Locale.getDefault().getISO3Language();
    }

    public static String c() {
        as.b(d.t, TimeZone.getDefault().getID());
        return TimeZone.getDefault().getID();
    }

    public static String d(Context context) {
        String str = d.B;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null || connectivityManager.getActiveNetworkInfo() == null) {
            return str;
        }
        switch (connectivityManager.getActiveNetworkInfo().getType()) {
            case 0:
                return "mobile";
            case 1:
            case 6:
                return "wifi";
            default:
                return str;
        }
    }

    public static String e(Context context) {
        try {
            String networkOperatorName = ((TelephonyManager) context.getSystemService("phone")).getNetworkOperatorName();
            if (networkOperatorName == null || networkOperatorName.length() == 0) {
                return null;
            }
            return networkOperatorName;
        } catch (Throwable th) {
            av.a(th);
            return null;
        }
    }

    public static boolean f(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null) {
            return activeNetworkInfo.isConnected();
        }
        return false;
    }

    public static String g(Context context) {
        if (!f(context)) {
            as.a("VungleUtil", "Not connected to network.");
            return "Network";
        } else if (ah.a()) {
            return null;
        } else {
            as.a("VungleUtil", "External storage unavailable.");
            return "External Storage";
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0026 A[SYNTHETIC, Splitter:B:17:0x0026] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x002b A[Catch:{ FileNotFoundException -> 0x0034, IOException -> 0x0063 }] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0030 A[Catch:{ FileNotFoundException -> 0x0034, IOException -> 0x0063 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(android.content.Context r6, java.lang.String r7, boolean r8) {
        /*
            r4 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0034, IOException -> 0x0063 }
            r0.<init>()     // Catch:{ FileNotFoundException -> 0x0034, IOException -> 0x0063 }
            if (r8 == 0) goto L_0x003f
            java.io.FileInputStream r1 = r6.openFileInput(r7)     // Catch:{ all -> 0x004a }
        L_0x000c:
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ all -> 0x006c }
            r2.<init>(r1)     // Catch:{ all -> 0x006c }
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ all -> 0x0071 }
            r3.<init>(r2)     // Catch:{ all -> 0x0071 }
        L_0x0016:
            java.lang.String r4 = r3.readLine()     // Catch:{ all -> 0x0020 }
            if (r4 == 0) goto L_0x004f
            r0.append(r4)     // Catch:{ all -> 0x0020 }
            goto L_0x0016
        L_0x0020:
            r0 = move-exception
            r5 = r3
            r3 = r1
            r1 = r5
        L_0x0024:
            if (r1 == 0) goto L_0x0029
            r1.close()     // Catch:{ FileNotFoundException -> 0x0034, IOException -> 0x0063 }
        L_0x0029:
            if (r2 == 0) goto L_0x002e
            r2.close()     // Catch:{ FileNotFoundException -> 0x0034, IOException -> 0x0063 }
        L_0x002e:
            if (r3 == 0) goto L_0x0033
            r3.close()     // Catch:{ FileNotFoundException -> 0x0034, IOException -> 0x0063 }
        L_0x0033:
            throw r0     // Catch:{ FileNotFoundException -> 0x0034, IOException -> 0x0063 }
        L_0x0034:
            r0 = move-exception
            java.lang.String r1 = com.vungle.sdk.d.u
            java.lang.String r2 = "FileNotFoundException"
            com.vungle.sdk.as.a(r1, r2, r0)
        L_0x003c:
            java.lang.String r0 = com.vungle.sdk.d.B
        L_0x003e:
            return r0
        L_0x003f:
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ all -> 0x004a }
            java.io.File r2 = new java.io.File     // Catch:{ all -> 0x004a }
            r2.<init>(r7)     // Catch:{ all -> 0x004a }
            r1.<init>(r2)     // Catch:{ all -> 0x004a }
            goto L_0x000c
        L_0x004a:
            r0 = move-exception
            r1 = r4
            r2 = r4
            r3 = r4
            goto L_0x0024
        L_0x004f:
            if (r3 == 0) goto L_0x0054
            r3.close()     // Catch:{ FileNotFoundException -> 0x0034, IOException -> 0x0063 }
        L_0x0054:
            if (r2 == 0) goto L_0x0059
            r2.close()     // Catch:{ FileNotFoundException -> 0x0034, IOException -> 0x0063 }
        L_0x0059:
            if (r1 == 0) goto L_0x005e
            r1.close()     // Catch:{ FileNotFoundException -> 0x0034, IOException -> 0x0063 }
        L_0x005e:
            java.lang.String r0 = r0.toString()     // Catch:{ FileNotFoundException -> 0x0034, IOException -> 0x0063 }
            goto L_0x003e
        L_0x0063:
            r0 = move-exception
            java.lang.String r1 = com.vungle.sdk.d.u
            java.lang.String r2 = "IOException"
            com.vungle.sdk.as.a(r1, r2, r0)
            goto L_0x003c
        L_0x006c:
            r0 = move-exception
            r2 = r4
            r3 = r1
            r1 = r4
            goto L_0x0024
        L_0x0071:
            r0 = move-exception
            r3 = r1
            r1 = r4
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.sdk.ax.a(android.content.Context, java.lang.String, boolean):java.lang.String");
    }

    public static void h(Context context) {
        as.b(d.t, "Call to readMetricDataQueue( Context context )");
        ArrayList arrayList = new ArrayList(0);
        if (new File(context.getFilesDir().getAbsolutePath() + File.separator + "MetricDataQueue").exists()) {
            String a = a(context, "MetricDataQueue", true);
            if (b(a)) {
                av.a(arrayList);
                as.b(d.t, "No pending metric data requests found");
                return;
            }
            try {
                JSONArray jSONArray = new JSONArray(a);
                int length = jSONArray.length();
                as.b(d.t, length + " Pending metric data requests found");
                for (int i = 0; i < length; i++) {
                    arrayList.add(jSONArray.get(i).toString());
                }
                av.a(arrayList);
                File file = new File(context.getFilesDir().getAbsoluteFile() + File.separator + "MetricDataQueue");
                if (file.exists()) {
                    as.b(d.t, "Deleting : " + file.getAbsolutePath());
                    file.delete();
                }
            } catch (JSONException e) {
                as.a(d.u, "JSONException", e);
                File file2 = new File(context.getFilesDir().getAbsoluteFile() + File.separator + "MetricDataQueue");
                if (file2.exists()) {
                    as.b(d.t, "Deleting : " + file2.getAbsolutePath());
                    file2.delete();
                }
            } catch (Throwable th) {
                File file3 = new File(context.getFilesDir().getAbsoluteFile() + File.separator + "MetricDataQueue");
                if (file3.exists()) {
                    as.b(d.t, "Deleting : " + file3.getAbsolutePath());
                    file3.delete();
                }
                throw th;
            }
        }
    }

    public static void i(Context context) {
        int i = context.getResources().getConfiguration().orientation;
        if (i == 2 || i == 3) {
            if (ai.y > ai.x) {
                f();
            }
        } else if ((i == 1 || i == 0) && ai.x > ai.y) {
            f();
        }
    }

    private static void f() {
        float f = ai.x;
        ai.x = ai.y;
        ai.y = f;
    }

    public static boolean c(String str) {
        if (b(str)) {
            return false;
        }
        if (".m3u8".equalsIgnoreCase(str.substring(str.length() - 5, str.length())) || ".m3u".equalsIgnoreCase(str.substring(str.length() - 4, str.length()))) {
            return true;
        }
        return false;
    }

    public static void a(Context context, String str, String str2, boolean z) {
        SharedPreferences.Editor edit = context.getSharedPreferences(str, 0).edit();
        if (edit != null) {
            edit.putBoolean(str2, z);
            edit.commit();
        }
    }

    public static boolean b(Context context, String str, String str2, boolean z) {
        return context.getSharedPreferences(str, 0).getBoolean(str2, z);
    }

    public static void d() {
        ai.k = true;
        new aj(ai.e()).a();
        new aj(ai.e()).c();
    }

    public static String d(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            String string = jSONObject.getString("campaign");
            String string2 = jSONObject.getString("pubAppId");
            return String.format("pubAppId=%s,SignedHeaders=host;x-vung-date;x-vung-target,Signature=%s", string2, a(str, string + string2 + System.currentTimeMillis()));
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String e() {
        String str;
        String str2;
        try {
            StringBuilder append = new StringBuilder().append("");
            if (Build.MANUFACTURER != null) {
                str = Build.MANUFACTURER;
            } else {
                str = "";
            }
            StringBuilder append2 = new StringBuilder().append(append.append(str).toString() + ",");
            if (Build.MODEL != null) {
                str2 = Build.MODEL;
            } else {
                str2 = "";
            }
            return append2.append(str2).toString();
        } catch (Throwable th) {
            return "<Unknown>";
        }
    }
}
