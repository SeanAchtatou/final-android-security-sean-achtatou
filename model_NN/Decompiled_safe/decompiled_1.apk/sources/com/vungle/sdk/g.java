package com.vungle.sdk;

import com.vungle.sdk.VunglePub;
import com.vungle.sdk.d;

/* compiled from: vungle */
final class g extends d.a {
    private double b;
    private double c;

    g(VunglePub.EventListener eventListener) {
        super(eventListener);
    }

    public d.a a(double d, double d2) {
        this.b = d;
        this.c = d2;
        return this;
    }

    public void run() {
        this.a.onVungleView(this.b, this.c);
    }
}
