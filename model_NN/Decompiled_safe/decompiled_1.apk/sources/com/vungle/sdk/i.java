package com.vungle.sdk;

import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: vungle */
class i {
    private long a;
    private long b;
    private long c;

    i() {
    }

    public long a() {
        return this.a;
    }

    public void a(long j) {
        this.a = j;
    }

    public long b() {
        return this.b;
    }

    public void b(long j) {
        this.b = j;
    }

    public long c() {
        return this.c;
    }

    public void c(long j) {
        this.c = j;
    }

    public JSONObject d() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("startTime", this.a);
            jSONObject.put("videoLength", this.b);
            jSONObject.put("videoViewed", this.c);
        } catch (JSONException e) {
            as.a(d.u, "JSONException", e);
        }
        return jSONObject;
    }
}
