package com.vungle.sdk;

import android.content.Context;
import android.webkit.WebView;
import android.widget.FrameLayout;

/* compiled from: vungle */
class n {
    private FrameLayout a;
    private WebView b;

    public n(Context context) {
        this.a = new FrameLayout(context);
        this.b = new WebView(context);
        this.a.addView(this.b);
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.b.getLayoutParams();
        layoutParams.gravity = 17;
        layoutParams.width = -1;
        layoutParams.height = -1;
        this.b.setLayoutParams(layoutParams);
    }

    public FrameLayout a() {
        return this.a;
    }

    public WebView b() {
        return this.b;
    }
}
