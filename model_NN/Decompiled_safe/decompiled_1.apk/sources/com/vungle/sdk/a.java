package com.vungle.sdk;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/* compiled from: vungle */
class a {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e = d.B;
    private String f;
    private String g = d.B;
    private String h;
    private String i;
    private int j = 3;
    private int k;
    private long l = 0;
    private long m = 0;
    private int n = 0;
    private int o = 0;
    private long p = -1;
    private int q = -1;
    private long r = 0;
    private int s = 0;

    a() {
    }

    public String a() {
        return this.a;
    }

    public void a(String str) {
        this.a = str;
    }

    public String b() {
        return this.b;
    }

    public void b(String str) {
        this.b = str;
        if (!ax.b(this.b)) {
            ai.g = this.b.substring(this.b.lastIndexOf(File.separator) + 1, this.b.length());
            try {
                as.b(d.w, "before encoding: " + ai.g);
                ai.g = URLDecoder.decode(ai.g, "UTF-8");
                as.b(d.w, "After encoding: " + ai.g);
            } catch (UnsupportedEncodingException e2) {
                as.a(d.u, "UnsupportedEncodingException", e2);
            }
        }
    }

    public void c(String str) {
        this.c = str;
    }

    public void d(String str) {
        this.d = str;
    }

    public String c() {
        return this.f;
    }

    public void e(String str) {
        this.f = str;
    }

    public String d() {
        return this.h;
    }

    public void f(String str) {
        this.h = str;
        if (!ax.b(this.h)) {
            ai.d = this.h.substring(this.h.lastIndexOf(File.separator) + 1, this.h.lastIndexOf("."));
        }
    }

    public String e() {
        return this.d;
    }

    public String f() {
        return this.g;
    }

    public void g(String str) {
        this.g = str;
        if (!ax.b(this.g)) {
            ai.e = this.g.substring(this.g.lastIndexOf(File.separator) + 1, this.g.lastIndexOf("."));
            return;
        }
        this.g = d.B;
        ai.e = d.B;
    }

    public String g() {
        return this.c;
    }

    public int h() {
        return this.j;
    }

    public void a(int i2) {
        this.j = i2;
    }

    public String i() {
        return this.e;
    }

    public void h(String str) {
        this.e = str;
    }

    public void i(String str) {
        this.i = str;
    }

    public int j() {
        return this.k;
    }

    public void b(int i2) {
        this.k = i2;
    }

    public void a(long j2) {
        this.l = j2;
    }

    public int k() {
        return this.n;
    }

    public void c(int i2) {
        this.n = i2;
    }

    public int l() {
        return this.o;
    }

    public void d(int i2) {
        this.o = i2;
    }

    public void b(long j2) {
        if (j2 == 0 || j2 >= ((long) d.g)) {
            this.p = j2;
        } else {
            this.p = (long) d.g;
        }
        ai.b(this.p);
    }

    public void e(int i2) {
        if (i2 > 0) {
            this.q = i2;
        } else {
            this.q = d.f;
        }
        ai.a(this.q);
    }

    public long m() {
        return this.m;
    }

    public void c(long j2) {
        this.m = j2;
    }

    public void f(int i2) {
        this.s = i2;
    }
}
