package com.ideaworks3d.marmalade;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.os.Handler;
import android.os.PowerManager;
import android.os.Vibrator;
import android.text.method.PasswordTransformationMethod;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.widget.EditText;
import com.vungle.sdk.VunglePub;
import java.util.Locale;
import javax.microedition.khronos.opengles.GL;

public class LoaderView extends SurfaceView implements SurfaceHolder.Callback, DialogInterface.OnClickListener, DialogInterface.OnDismissListener {
    private static int[] g_PixelsLast;
    private static LoaderGL m_LoaderGL = new LoaderGL();
    private final Runnable m_BacklightOn = new Runnable() {
        public void run() {
            ((PowerManager) LoaderView.this.m_LoaderActivity.getSystemService("power")).newWakeLock(26, getClass().getName()).acquire(3000);
        }
    };
    private EditText m_EditText;
    private String m_ErrorBody;
    private Dialog m_ErrorDialog = null;
    private int m_ErrorRtn;
    public boolean m_ErrorRunning = false;
    private String m_ErrorTitle;
    private int m_ErrorType;
    private Bitmap m_FullScreenBitmap;
    public GL m_GL;
    Handler m_Handler = new Handler();
    public int m_Height;
    private Dialog m_InputDialog = null;
    private String m_InputTextDefault;
    private int m_InputTextFlags;
    private String m_InputTextResult = null;
    public boolean m_InputTextRunning = false;
    private String m_InputTextTitle;
    public LoaderActivity m_LoaderActivity;
    public LoaderKeyboard m_LoaderKeyboard;
    public boolean m_NewlyCreated;
    private Paint m_Paint = new Paint();
    private int[] m_Pixels;
    /* access modifiers changed from: private */
    public final Runnable m_RunOnOSThread = new Runnable() {
        public void run() {
            synchronized (LoaderView.this.m_RunOnOSThread) {
                LoaderView.this.runOnOSThreadNative();
                LoaderView.this.m_RunOnOSThread.notify();
            }
        }
    };
    private final Runnable m_ShowError = new Runnable() {
        public void run() {
            LoaderView.this.showErrorReal();
        }
    };
    private final Runnable m_ShowInputText = new Runnable() {
        public void run() {
            LoaderView.this.showInputTextReal();
        }
    };
    private SurfaceHolder m_SurfaceHolder;
    private boolean m_UseGL = false;
    private Vibrator m_Vibrator;
    private int m_VideoState = 0;
    /* access modifiers changed from: private */
    public S3EVideoView m_VideoView = null;
    public int m_Width;

    /* access modifiers changed from: private */
    public native void runOnOSThreadNative();

    private native void setInputText(String str);

    private native void setPixelsNative(int i, int i2, int[] iArr, boolean z);

    private native void videoStoppedNotify();

    public LoaderView(LoaderActivity loaderActivity, boolean z) {
        super(loaderActivity);
        this.m_UseGL = z;
        this.m_LoaderActivity = loaderActivity;
        this.m_Vibrator = (Vibrator) this.m_LoaderActivity.getSystemService("vibrator");
        this.m_SurfaceHolder = getHolder();
        this.m_SurfaceHolder.addCallback(this);
        this.m_LoaderKeyboard = new LoaderKeyboard(this);
        if (z) {
            this.m_SurfaceHolder.setType(2);
            int[] iArr = new int[1];
            if (LoaderAPI.s3eConfigGetInt("GL", "AndroidSurfaceHolder", iArr) == 0) {
                this.m_SurfaceHolder.setFormat(iArr[0]);
            }
        } else {
            this.m_SurfaceHolder.setType(1);
        }
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus();
        setId(1983274);
    }

    public int getCurrentOrientation() {
        if (Build.VERSION.SDK_INT < 9) {
            switch (getResources().getConfiguration().orientation) {
                case 1:
                    return 1;
                case 2:
                    return 0;
                default:
                    return -1;
            }
        } else {
            Display defaultDisplay = ((WindowManager) this.m_LoaderActivity.getSystemService("window")).getDefaultDisplay();
            boolean z = defaultDisplay.getWidth() >= defaultDisplay.getHeight();
            switch (defaultDisplay.getRotation()) {
                case 0:
                    return z ? 0 : 1;
                case 1:
                    return z ? 0 : 9;
                case 2:
                    return z ? 8 : 9;
                case 3:
                    return z ? 8 : 1;
                default:
                    return -1;
            }
        }
    }

    private void backlightOn() {
        this.m_Handler.post(this.m_BacklightOn);
    }

    public boolean onKeyPreIme(int i, KeyEvent keyEvent) {
        return this.m_LoaderKeyboard.onKeyPreIme(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
    }

    public void onDismiss(DialogInterface dialogInterface) {
        if (dialogInterface == this.m_InputDialog) {
            setInputText(this.m_InputTextResult);
            this.m_InputTextRunning = false;
            this.m_InputDialog = null;
        } else if (dialogInterface == this.m_ErrorDialog) {
            this.m_ErrorDialog = null;
            this.m_ErrorRunning = false;
        }
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        if (dialogInterface == this.m_InputDialog) {
            if (i == -1) {
                this.m_InputTextResult = this.m_EditText.getText().toString();
            }
            dialogInterface.dismiss();
        } else if (dialogInterface == this.m_ErrorDialog) {
            switch (i) {
                case -3:
                    this.m_ErrorRtn = 2;
                    break;
                case -2:
                    this.m_ErrorRtn = 1;
                    break;
                case VunglePub.Gender.UNKNOWN /*-1*/:
                    this.m_ErrorRtn = 0;
                    break;
            }
            dialogInterface.dismiss();
        }
    }

    public boolean onKeyEvent(int i, int i2, KeyEvent keyEvent) {
        if (this.m_InputTextRunning && i == 4) {
            this.m_InputTextRunning = false;
            return true;
        } else if (dispatchKeyEvent(keyEvent)) {
            return true;
        } else {
            return this.m_LoaderKeyboard.onKeyEvent(i, i2, keyEvent);
        }
    }

    public void runOnOSThread() {
        synchronized (this.m_RunOnOSThread) {
            this.m_Handler.post(this.m_RunOnOSThread);
            try {
                this.m_RunOnOSThread.wait();
            } catch (InterruptedException e) {
            }
        }
    }

    private class VideoRunner implements Runnable {
        public static final int PAUSE = 2;
        public static final int PLAY = 1;
        public static final int RESUME = 3;
        public static final int STOP = 4;
        public static final int VOLUME = 5;
        private int m_Action;
        private String m_File;
        private boolean m_Fullscreen;
        private int m_Height;
        private long m_Offset;
        private int m_Repeats;
        private int m_Return;
        private long m_Size;
        private int m_Volume;
        private int m_Width;
        private int m_X;
        private int m_Y;

        private VideoRunner() {
        }

        /* access modifiers changed from: package-private */
        public void play(String str, int i, int i2, int i3, int i4, int i5, int i6, boolean z, long j, long j2) {
            this.m_Action = 1;
            this.m_File = str;
            this.m_Volume = i;
            this.m_Repeats = i2;
            this.m_X = i3;
            this.m_Y = i4;
            this.m_Width = i5;
            this.m_Height = i6;
            this.m_Fullscreen = z;
            this.m_Offset = j;
            this.m_Size = j2;
        }

        /* access modifiers changed from: package-private */
        public void setState(int i) {
            switch (i) {
                case 0:
                    this.m_Action = 4;
                    return;
                case 1:
                    this.m_Action = 3;
                    return;
                case 2:
                    this.m_Action = 2;
                    return;
                default:
                    return;
            }
        }

        /* access modifiers changed from: package-private */
        public void setVolume(int i) {
            this.m_Action = 5;
            this.m_Volume = i;
        }

        public synchronized int runOnUiThread(boolean z) {
            int i;
            try {
                if (Thread.currentThread() == LoaderView.this.m_LoaderActivity.getMainLooper().getThread()) {
                    run();
                } else {
                    LoaderView.this.m_Handler.post(this);
                    if (z) {
                        wait();
                    }
                }
                if (z) {
                    i = this.m_Return;
                } else {
                    i = 0;
                }
            } catch (InterruptedException e) {
                i = -1;
            }
            return i;
        }

        public synchronized void run() {
            if (this.m_Action == 1) {
                if (LoaderView.this.m_VideoView != null) {
                    LoaderView.this.m_VideoView.videoStop();
                    LoaderView.this.m_VideoView.videoRemoveView();
                    S3EVideoView unused = LoaderView.this.m_VideoView = null;
                }
                S3EVideoView unused2 = LoaderView.this.m_VideoView = new S3EVideoView(LoaderView.this.m_LoaderActivity);
                LoaderView.this.m_VideoView.videoAddView(this.m_Fullscreen, this.m_X, this.m_Y, this.m_Width, this.m_Height);
                LoaderView.this.m_VideoView.videoSetVolume(this.m_Volume);
                this.m_Return = LoaderView.this.m_VideoView.videoPlay(this.m_File, this.m_Repeats, this.m_Offset, this.m_Size);
            } else if (LoaderView.this.m_VideoView != null) {
                switch (this.m_Action) {
                    case 2:
                        LoaderView.this.m_VideoView.videoPause();
                        break;
                    case 3:
                        LoaderView.this.m_VideoView.videoResume();
                        break;
                    case 4:
                        LoaderView.this.m_VideoView.videoStop();
                        LoaderView.this.m_VideoView.videoRemoveView();
                        S3EVideoView unused3 = LoaderView.this.m_VideoView = null;
                        break;
                    case 5:
                        LoaderView.this.m_VideoView.videoSetVolume(this.m_Volume);
                        break;
                }
                this.m_Return = 0;
            }
            notify();
        }
    }

    public void enableRespondingToRotation() {
        this.m_LoaderActivity.LoaderThread().onSplashFinished();
    }

    public int videoPlay(String str, int i, int i2, int i3, int i4, int i5, int i6, boolean z, long j, long j2) {
        this.m_VideoState = 1;
        VideoRunner videoRunner = new VideoRunner();
        videoRunner.play(str, i, i2, i3, i4, i5, i6, z, j, j2);
        return videoRunner.runOnUiThread(true);
    }

    public int videoPause() {
        if (this.m_VideoState != 1) {
            return -1;
        }
        this.m_VideoState = 2;
        VideoRunner videoRunner = new VideoRunner();
        videoRunner.setState(2);
        return videoRunner.runOnUiThread(false);
    }

    public int videoResume() {
        if (this.m_VideoState != 2) {
            return -1;
        }
        this.m_VideoState = 1;
        VideoRunner videoRunner = new VideoRunner();
        videoRunner.setState(1);
        return videoRunner.runOnUiThread(false);
    }

    private boolean videoIsPlaying() {
        return videoGetStatus() == 1;
    }

    public void videoStop() {
        boolean z;
        if (this.m_VideoState != 0) {
            VideoRunner videoRunner = new VideoRunner();
            videoRunner.setState(0);
            videoRunner.runOnUiThread(true);
            z = videoIsPlaying();
            this.m_VideoState = 0;
        } else {
            z = false;
        }
        if (z && !videoIsPlaying()) {
            videoStopped();
        }
    }

    public void videoSetVolume(int i) {
        VideoRunner videoRunner = new VideoRunner();
        videoRunner.setVolume(i);
        videoRunner.runOnUiThread(false);
    }

    public int videoGetStatus() {
        return this.m_VideoState;
    }

    public int videoGetPosition() {
        if (this.m_VideoView != null) {
            return this.m_VideoView.videoGetPosition();
        }
        return 0;
    }

    public void videoStopped() {
        if (this.m_VideoView != null) {
            this.m_VideoView.videoRemoveView();
            this.m_VideoView = null;
        }
        this.m_VideoState = 0;
        videoStoppedNotify();
    }

    /* access modifiers changed from: private */
    public void showErrorReal() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.m_LoaderActivity);
        this.m_ErrorRtn = 0;
        builder.setTitle(this.m_ErrorTitle);
        builder.setMessage(this.m_ErrorBody);
        builder.setPositiveButton("Continue", this);
        if (this.m_ErrorType > 0) {
            builder.setNegativeButton("Stop", this);
        }
        if (this.m_ErrorType > 1) {
            builder.setNeutralButton("Ignore", this);
        }
        this.m_ErrorDialog = builder.create();
        this.m_ErrorDialog.setOnDismissListener(this);
        this.m_ErrorDialog.show();
    }

    public int showError(String str, String str2, int i) {
        int i2;
        if (this.m_LoaderActivity.isFinishing()) {
            return 0;
        }
        synchronized (this.m_ShowError) {
            this.m_ErrorTitle = str;
            this.m_ErrorBody = str2;
            this.m_ErrorType = i;
            this.m_ErrorRtn = 0;
            this.m_ErrorRunning = true;
            this.m_Handler.post(this.m_ShowError);
            while (this.m_ErrorRunning) {
                LoaderAPI.s3eDeviceYield(20);
            }
            i2 = this.m_ErrorRtn;
        }
        return i2;
    }

    public void doneInputText(DialogInterface dialogInterface, int i) {
    }

    public void showInputTextReal() {
        this.m_EditText = new EditText(this.m_LoaderActivity);
        this.m_EditText.setText(this.m_InputTextDefault);
        if ((this.m_InputTextFlags & 1) != 0) {
            this.m_EditText.setInputType(128);
            this.m_EditText.setTransformationMethod(new PasswordTransformationMethod());
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this.m_LoaderActivity);
        builder.setTitle(this.m_InputTextTitle);
        builder.setView(this.m_EditText);
        builder.setPositiveButton("OK", this);
        builder.setNegativeButton("Cancel", this);
        this.m_InputDialog = builder.create();
        this.m_InputTextRunning = true;
        this.m_InputTextResult = null;
        this.m_InputDialog.setOnDismissListener(this);
        this.m_InputDialog.show();
    }

    public void getInputString(String str, String str2, int i) {
        this.m_InputTextTitle = str;
        this.m_InputTextDefault = str2;
        this.m_InputTextFlags = i;
        this.m_Handler.post(this.m_ShowInputText);
    }

    public void vibrateStart(long j) {
        this.m_Vibrator.vibrate(j);
    }

    public void vibrateStop() {
        this.m_Vibrator.cancel();
    }

    public boolean vibrateAvailable() {
        try {
            return ((Boolean) this.m_Vibrator.getClass().getMethod("hasVibrator", new Class[0]).invoke(this.m_Vibrator, new Object[0])).booleanValue();
        } catch (Exception e) {
            return true;
        }
    }

    public String getLocale() {
        return Locale.getDefault().toString();
    }

    public boolean glInit(int i) {
        if (this.m_UseGL && !m_LoaderGL.started()) {
            this.m_GL = m_LoaderGL.startGL(this.m_SurfaceHolder, i);
        }
        return this.m_UseGL;
    }

    public void glReInit() {
        if (this.m_UseGL && m_LoaderGL.started()) {
            this.m_GL = m_LoaderGL.restartGL(this.m_SurfaceHolder);
        }
    }

    public void glTerm() {
        if (this.m_UseGL && m_LoaderGL.started()) {
            m_LoaderGL.stopGL();
            this.m_GL = null;
        }
    }

    public void glSwapBuffers() {
        if (m_LoaderGL.started()) {
            m_LoaderGL.swap();
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.m_LoaderActivity.LoaderThread() != null) {
            return this.m_LoaderActivity.LoaderThread().onTouchEvent(motionEvent);
        }
        return false;
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (Build.VERSION.SDK_INT < 5 || LoaderAPI.s3eConfigGet("AndroidIgnoreBackKeyFromPointerDevice", 0) == 0 || keyEvent.getKeyCode() != 4) {
            return super.dispatchKeyEvent(keyEvent);
        }
        if ((keyEvent.getDevice().getId() & 255 & 2) == 2) {
            return true;
        }
        return super.dispatchKeyEvent(keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        this.m_NewlyCreated = true;
    }

    public synchronized void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        LoaderThread LoaderThread = this.m_LoaderActivity.LoaderThread();
        if (LoaderThread != null) {
            LoaderThread.suspendForSurfaceChange();
            this.m_Width = 0;
            this.m_Height = 0;
            if (m_LoaderGL.started()) {
                m_LoaderGL.stopGL();
                this.m_GL = null;
            }
            this.m_FullScreenBitmap = null;
            setPixelsNative(0, 0, null, false);
            this.m_Pixels = null;
            LoaderThread.resumeAfterSurfaceChange();
            notify();
        }
    }

    public synchronized void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        LoaderThread LoaderThread = this.m_LoaderActivity.LoaderThread();
        if (this.m_Pixels == null || !LoaderThread.skipSurfaceChange()) {
            LoaderThread.suspendForSurfaceChange();
            this.m_Width = i2;
            this.m_Height = i3;
            if (this.m_FullScreenBitmap != null) {
                this.m_FullScreenBitmap.recycle();
                this.m_FullScreenBitmap = null;
            }
            this.m_FullScreenBitmap = Bitmap.createBitmap(this.m_Width, this.m_Height, Bitmap.Config.RGB_565);
            if (this.m_Pixels == null || this.m_Pixels.length != this.m_Width * this.m_Height) {
                if (g_PixelsLast == null || g_PixelsLast.length < this.m_Width * this.m_Height) {
                    this.m_Pixels = new int[(this.m_Width * this.m_Height)];
                    g_PixelsLast = this.m_Pixels;
                } else {
                    this.m_Pixels = g_PixelsLast;
                }
            }
            setPixelsNative(this.m_Width, this.m_Height, this.m_Pixels, this.m_NewlyCreated);
            this.m_NewlyCreated = false;
            LoaderThread.resumeAfterSurfaceChange();
            notify();
        } else {
            notify();
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean waitForSurface() {
        /*
            r2 = this;
            r1 = 0
            monitor-enter(r2)
            int[] r0 = r2.m_Pixels     // Catch:{ all -> 0x0017 }
            if (r0 != 0) goto L_0x0009
            r2.wait()     // Catch:{ InterruptedException -> 0x0010 }
        L_0x0009:
            int[] r0 = r2.m_Pixels     // Catch:{ all -> 0x0017 }
            if (r0 != 0) goto L_0x0014
            monitor-exit(r2)     // Catch:{ all -> 0x0017 }
            r0 = r1
        L_0x000f:
            return r0
        L_0x0010:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0017 }
            r0 = r1
            goto L_0x000f
        L_0x0014:
            monitor-exit(r2)     // Catch:{ all -> 0x0017 }
            r0 = 1
            goto L_0x000f
        L_0x0017:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0017 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ideaworks3d.marmalade.LoaderView.waitForSurface():boolean");
    }

    public void doDraw() {
        Canvas canvas;
        try {
            if (this.m_FullScreenBitmap == null) {
                if (0 != 0) {
                    this.m_SurfaceHolder.unlockCanvasAndPost(null);
                }
            } else if (!this.m_UseGL) {
                this.m_FullScreenBitmap.setPixels(this.m_Pixels, 0, this.m_Width, 0, 0, this.m_Width, this.m_Height);
                Canvas lockCanvas = this.m_SurfaceHolder.lockCanvas();
                if (lockCanvas != null) {
                    try {
                        lockCanvas.drawBitmap(this.m_FullScreenBitmap, 0.0f, 0.0f, this.m_Paint);
                        if (lockCanvas != null) {
                            this.m_SurfaceHolder.unlockCanvasAndPost(lockCanvas);
                        }
                    } catch (Throwable th) {
                        Throwable th2 = th;
                        canvas = lockCanvas;
                        th = th2;
                        if (canvas != null) {
                            this.m_SurfaceHolder.unlockCanvasAndPost(canvas);
                        }
                        throw th;
                    }
                } else if (lockCanvas != null) {
                    this.m_SurfaceHolder.unlockCanvasAndPost(lockCanvas);
                }
            } else if (0 != 0) {
                this.m_SurfaceHolder.unlockCanvasAndPost(null);
            }
        } catch (Throwable th3) {
            th = th3;
            canvas = null;
        }
    }
}
