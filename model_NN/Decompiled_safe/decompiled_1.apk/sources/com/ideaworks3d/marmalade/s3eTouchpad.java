package com.ideaworks3d.marmalade;

import android.content.res.Configuration;
import android.os.Build;
import android.view.InputDevice;
import android.view.MotionEvent;
import android.view.ViewParent;

class s3eTouchpad {
    static final int S3E_TOUCHPAD_AVAILABLE = 0;
    static final int S3E_TOUCHPAD_HEIGHT = 2;
    static final int S3E_TOUCHPAD_WIDTH = 1;
    private static int m_Height = 0;
    private static int m_Width = 0;
    private int inputDeviceId = -1;
    private boolean processPositionEvents;

    public static native void onMotionEvent(int i, int i2, int i3, int i4);

    s3eTouchpad() {
    }

    public static boolean onTouchEvent(MotionEvent motionEvent) {
        if (1048584 != motionEvent.getSource()) {
            return false;
        }
        int action = motionEvent.getAction();
        int i = (65280 & action) >>> 8;
        if (action == 2) {
            int pointerCount = motionEvent.getPointerCount();
            for (int i2 = 0; i2 < pointerCount; i2++) {
                onMotionEvent(motionEvent.getPointerId(i2), action + 4, (int) motionEvent.getX(i2), m_Height - ((int) motionEvent.getY(i2)));
            }
        } else if (action == 0 || action == 1) {
            onMotionEvent(motionEvent.getPointerId(0), action + 4, (int) motionEvent.getX(), m_Height - ((int) motionEvent.getY()));
        } else {
            int pointerId = motionEvent.getPointerId(i);
            int i3 = action & 255;
            if (i3 == 6 || i3 == 5) {
                onMotionEvent(pointerId, (i3 - 5) + 4, (int) motionEvent.getX(i), m_Height - ((int) motionEvent.getY(i)));
            }
        }
        return true;
    }

    public boolean s3eTouchpadInit() {
        for (int i : InputDevice.getDeviceIds()) {
            InputDevice device = InputDevice.getDevice(i);
            if ((device.getSources() & 1048584) > 0) {
                InputDevice.MotionRange motionRange = device.getMotionRange(0);
                InputDevice.MotionRange motionRange2 = device.getMotionRange(1);
                if (!(motionRange == null || motionRange2 == null)) {
                    m_Width = (int) motionRange.getMax();
                    m_Height = (int) motionRange2.getMax();
                    if (m_Width > 0 && m_Height > 0) {
                        if (!setProcessPositionEvents(true)) {
                            return false;
                        }
                        this.inputDeviceId = i;
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public void s3eTouchpadTerminate() {
        if (this.processPositionEvents) {
            setProcessPositionEvents(false);
        }
    }

    public int s3eTouchpadGetInt(int i) {
        int i2;
        if (i == 0) {
            LoaderAPI.trace("Touchpad GetInt S3E_TOUCHPAD_AVAILABLE");
            if (this.inputDeviceId == -1) {
                return 0;
            }
            Configuration configuration = LoaderActivity.m_Activity.getResources().getConfiguration();
            try {
                LoaderAPI.trace("Android build: " + Build.ID);
                LoaderAPI.trace("Checking for legacy Xperia Play build ID: " + "3.0.A.2.");
                if (Build.ID.startsWith("3.0.A.2.") && Integer.parseInt(Build.ID.substring("3.0.A.2.".length())) <= 181) {
                    LoaderAPI.trace("Found... Using legacy Configuration enum");
                    return configuration.hardKeyboardHidden != 2 ? 1 : 0;
                }
            } catch (NumberFormatException e) {
                LoaderAPI.trace("Error parsing build ID");
            }
            if (configuration.navigationHidden != 2) {
                i2 = 1;
            } else {
                i2 = 0;
            }
            LoaderAPI.trace("Touchpad available: " + i2);
            return i2;
        } else if (i == 1) {
            return m_Width;
        } else {
            if (i == 2) {
                return m_Height;
            }
            return 0;
        }
    }

    public boolean setProcessPositionEvents(boolean z) {
        if (Integer.parseInt(Build.VERSION.SDK) >= 9) {
            try {
                ViewParent parent = LoaderActivity.m_Activity.getWindow().getDecorView().getRootView().getParent();
                Class.forName("android.view.ViewRoot").getMethod("setProcessPositionEvents", Boolean.TYPE).invoke(parent, Boolean.valueOf(z));
                return true;
            } catch (Exception e) {
            }
        }
        return false;
    }
}
