package com.ideaworks3d.marmalade;

public class SuspendResumeEvent {
    public final EventType eventType;

    public enum EventType {
        SUSPEND,
        RESUME,
        SHUTDOWN
    }

    public SuspendResumeEvent(EventType eventType2) {
        this.eventType = eventType2;
    }
}
