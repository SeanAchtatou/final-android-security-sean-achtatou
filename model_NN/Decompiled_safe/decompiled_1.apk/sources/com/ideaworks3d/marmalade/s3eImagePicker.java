package com.ideaworks3d.marmalade;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

class s3eImagePicker {
    static final String[] CURSOR_TYPE = {"_data", "_id"};
    static final int S3E_IMAGEPICKER_FORMAT_ANY = 0;
    static final int S3E_IMAGEPICKER_FORMAT_ANYIMAGE = 6;
    static final int S3E_IMAGEPICKER_FORMAT_ANYVIDEO = 5;
    static final int S3E_IMAGEPICKER_FORMAT_BMP = 3;
    static final int S3E_IMAGEPICKER_FORMAT_GIF = 4;
    static final int S3E_IMAGEPICKER_FORMAT_JPG = 1;
    static final int S3E_IMAGEPICKER_FORMAT_PNG = 2;
    static final int S3E_IMAGEPICKER_FORMAT_UNKNOWN = 7;
    /* access modifiers changed from: private */
    public volatile boolean waitingForFile;

    public native boolean ImagePicker_addResultString(String str);

    s3eImagePicker() {
    }

    public String s3eImagePickerToFile(int i, int i2) {
        Intent intent = new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        switch (i) {
            case 0:
                intent = new Intent("android.intent.action.PICK");
                intent.setType("*/*");
                break;
            case 1:
            case 2:
            case 3:
            case 4:
            case 6:
                intent.setType("image/*");
                break;
            case 5:
                intent = new Intent("android.intent.action.PICK", MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                intent.setType("video/*");
                break;
            default:
                return null;
        }
        Intent ExecuteIntent = LoaderActivity.m_Activity.ExecuteIntent(intent);
        if (ExecuteIntent == null) {
            return null;
        }
        File convertUriToFile = convertUriToFile(ExecuteIntent.getData());
        if (convertUriToFile == null) {
            return null;
        }
        String str = "raw://" + convertUriToFile.getPath();
        if (!ImagePicker_addResultString(str)) {
            return null;
        }
        return str;
    }

    private boolean CopyStream(InputStream inputStream, OutputStream outputStream) {
        try {
            byte[] bArr = new byte[1024];
            int i = 0;
            while (true) {
                int read = inputStream.read(bArr, 0, 1024);
                if (read == -1) {
                    break;
                }
                outputStream.write(bArr, 0, read);
                i += read;
            }
            if (i != 0) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    /* access modifiers changed from: private */
    public File getPicasaFile(Uri uri) {
        File cacheDir;
        InputStream inputStream;
        if (Environment.getExternalStorageState().equals("mounted")) {
            cacheDir = new File(LoaderActivity.m_Activity.getExternalCacheDir(), "Picasa");
        } else {
            cacheDir = LoaderActivity.m_Activity.getCacheDir();
        }
        if (!cacheDir.exists()) {
            cacheDir.mkdirs();
        }
        try {
            File createTempFile = File.createTempFile("img", ".jpg", cacheDir);
            if (uri.toString().startsWith("content://com.android.gallery3d") || uri.toString().startsWith("content://com.google.android.gallery3d")) {
                inputStream = LoaderActivity.m_Activity.getContentResolver().openInputStream(uri);
            } else {
                inputStream = new URL(uri.toString()).openStream();
            }
            FileOutputStream fileOutputStream = new FileOutputStream(createTempFile);
            boolean CopyStream = CopyStream(inputStream, fileOutputStream);
            fileOutputStream.close();
            if (CopyStream) {
                return createTempFile;
            }
            createTempFile.delete();
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    private File convertUriToFile(final Uri uri) {
        final File[] fileArr = new File[1];
        Cursor query = LoaderActivity.m_Activity.getContentResolver().query(uri, new String[]{"_data", "_display_name"}, null, null, null);
        if (query != null) {
            query.moveToFirst();
            int columnIndex = query.getColumnIndex("_data");
            if (!uri.toString().startsWith("content://com.android.gallery3d") && !uri.toString().startsWith("content://com.google.android.gallery3d")) {
                String string = query.getString(columnIndex);
                query.close();
                return new File(string);
            } else if (query.getColumnIndex("_display_name") != -1) {
                this.waitingForFile = true;
                LoaderActivity.m_Activity.ShowProgressDialog();
                new Thread(new Runnable() {
                    public void run() {
                        fileArr[0] = s3eImagePicker.this.getPicasaFile(uri);
                        boolean unused = s3eImagePicker.this.waitingForFile = false;
                    }
                }).start();
            }
        } else if (uri != null && uri.toString().length() > 0) {
            this.waitingForFile = true;
            LoaderActivity.m_Activity.ShowProgressDialog();
            new Thread(new Runnable() {
                public void run() {
                    fileArr[0] = s3eImagePicker.this.getPicasaFile(uri);
                    boolean unused = s3eImagePicker.this.waitingForFile = false;
                }
            }).start();
        }
        while (this.waitingForFile) {
            LoaderAPI.s3eDeviceYield(1);
        }
        LoaderActivity.m_Activity.HideProgressDialog();
        return fileArr[0];
    }
}
