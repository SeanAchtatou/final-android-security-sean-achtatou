package com.ideaworks3d.marmalade;

public interface SuspendResumeListener {
    void onSuspendResumeEvent(SuspendResumeEvent suspendResumeEvent);
}
