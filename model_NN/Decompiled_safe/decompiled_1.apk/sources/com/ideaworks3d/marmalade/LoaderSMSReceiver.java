package com.ideaworks3d.marmalade;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

public class LoaderSMSReceiver extends BroadcastReceiver {
    static final String ACTION = "android.provider.Telephony.SMS_RECEIVED";

    public static native void onReceiveCallback(String str, String str2, long j);

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(ACTION)) {
            new StringBuilder();
            Bundle extras = intent.getExtras();
            if (extras != null) {
                Object[] objArr = (Object[]) extras.get("pdus");
                SmsMessage[] smsMessageArr = new SmsMessage[objArr.length];
                for (int i = 0; i < objArr.length; i++) {
                    smsMessageArr[i] = SmsMessage.createFromPdu((byte[]) objArr[i]);
                }
                for (SmsMessage smsMessage : smsMessageArr) {
                    onReceiveCallback(smsMessage.getDisplayOriginatingAddress(), smsMessage.getDisplayMessageBody(), smsMessage.getTimestampMillis());
                }
            }
        }
    }
}
