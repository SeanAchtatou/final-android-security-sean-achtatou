package com.ideaworks3d.marmalade;

import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class LoaderAPI {
    public static final int S3E_RESULT_ERROR = 1;
    public static final int S3E_RESULT_SUCCESS = 0;
    private static Stack<View.OnKeyListener> m_KeyListeners = new Stack<>();
    private static List<SuspendResumeListener> m_Listeners = new ArrayList();

    public static native int s3eConfigGet(String str, int i);

    public static native int s3eConfigGetInt(String str, String str2, int[] iArr);

    public static native int s3eConfigGetString(String str, String str2, String[] strArr);

    public static native void s3eDebugTraceLine(String str);

    public static native void s3eDeviceYield(int i);

    public static void traceChan(String str, String str2) {
        trace(str + ": " + str2);
    }

    public static void trace(String str) {
        if (LoaderActivity.m_Activity != null) {
            s3eDebugTraceLine(str);
        } else {
            Log.i("MARMALADE", str);
        }
    }

    public static String getStackTrace(Throwable th) {
        StringWriter stringWriter = new StringWriter();
        th.printStackTrace(new PrintWriter(stringWriter));
        return stringWriter.toString();
    }

    public static String getStackTrace() {
        try {
            throw new Exception("Tracer");
        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            e.printStackTrace(new PrintWriter(stringWriter));
            return stringWriter.toString();
        }
    }

    public static LoaderActivity getActivity() {
        return LoaderActivity.m_Activity;
    }

    public static View getMainView() {
        return LoaderActivity.m_Activity.m_View;
    }

    public static FrameLayout getFrameLayout() {
        return LoaderActivity.m_Activity.m_FrameLayout;
    }

    public static void addSuspendResumeListener(SuspendResumeListener suspendResumeListener) {
        if (!m_Listeners.contains(suspendResumeListener)) {
            m_Listeners.add(suspendResumeListener);
        }
    }

    public static boolean removeSuspendResumeListener(SuspendResumeListener suspendResumeListener) {
        return m_Listeners.remove(suspendResumeListener);
    }

    public static void notifySuspendResumeListeners(SuspendResumeEvent suspendResumeEvent) {
        for (SuspendResumeListener onSuspendResumeEvent : m_Listeners) {
            onSuspendResumeEvent.onSuspendResumeEvent(suspendResumeEvent);
        }
    }

    public static void pushKeyListener(View.OnKeyListener onKeyListener) {
        m_KeyListeners.push(onKeyListener);
        getMainView().setOnKeyListener(onKeyListener);
    }

    public static View.OnKeyListener popKeyListener() {
        View.OnKeyListener pop = m_KeyListeners.pop();
        getMainView().setOnKeyListener(pop);
        return pop;
    }
}
