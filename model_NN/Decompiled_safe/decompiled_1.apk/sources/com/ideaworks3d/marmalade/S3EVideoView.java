package com.ideaworks3d.marmalade;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.VideoView;

public class S3EVideoView extends VideoView implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {
    public static final int S3E_VIDEO_FAILED = 3;
    public static final int S3E_VIDEO_MAX_VOLUME = 256;
    public static final int S3E_VIDEO_PAUSED = 2;
    public static final int S3E_VIDEO_PLAYING = 1;
    public static final int S3E_VIDEO_STOPPED = 0;
    private boolean m_Fullscreen;
    private int m_Height;
    private LoaderActivity m_LoaderActivity;
    private MediaPlayer m_MediaPlayer;
    private String m_Path;
    private int m_Repeats;
    private Uri m_Uri;
    private float m_Volume = 1.0f;
    private int m_Width;

    public S3EVideoView(LoaderActivity loaderActivity) {
        super(loaderActivity);
        this.m_LoaderActivity = loaderActivity;
        setOnPreparedListener(this);
        setOnCompletionListener(this);
        setOnErrorListener(this);
    }

    public int videoGetPosition() {
        try {
            return getCurrentPosition();
        } catch (IllegalStateException e) {
            return 0;
        }
    }

    public int videoPlay(String str, int i, long j, long j2) {
        this.m_Repeats = i;
        if (j2 == 0) {
            this.m_Path = str;
            setVideoPath(this.m_Path);
            return 0;
        }
        this.m_Uri = Uri.parse(VFSProvider.ASSET_URI + "/" + str + "/" + j + "/" + j2);
        setVideoURI(this.m_Uri);
        return 0;
    }

    public void videoPause() {
        pause();
    }

    public void videoResume() {
        start();
    }

    public void videoStop() {
        this.m_MediaPlayer = null;
        stopPlayback();
    }

    private static boolean requiresSeparateWindow() {
        return Integer.parseInt(Build.VERSION.SDK) <= 4;
    }

    public void videoAddView(boolean z, int i, int i2, int i3, int i4) {
        this.m_Fullscreen = z;
        this.m_Width = i3;
        this.m_Height = i4;
        if (requiresSeparateWindow()) {
            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
            layoutParams.flags |= 8;
            layoutParams.flags |= 512;
            layoutParams.flags |= 1024;
            layoutParams.gravity = 51;
            layoutParams.x = i;
            layoutParams.y = i2;
            layoutParams.width = i3;
            layoutParams.height = i4;
            this.m_LoaderActivity.getWindow().getWindowManager().addView(this, layoutParams);
            return;
        }
        if (z) {
            FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(-1, -1, 17);
            this.m_Width = 0;
            this.m_Height = 0;
            this.m_LoaderActivity.m_FrameLayout.addView(this, layoutParams2);
        } else {
            RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(i3, i4);
            layoutParams3.leftMargin = i;
            layoutParams3.topMargin = i2;
            this.m_LoaderActivity.m_TopLevel.addView(this, layoutParams3);
        }
        try {
            getClass().getMethod("setZOrderOnTop", Boolean.TYPE).invoke(this, true);
        } catch (Exception e) {
        }
    }

    public void videoRemoveView() {
        if (requiresSeparateWindow()) {
            this.m_LoaderActivity.getWindow().getWindowManager().removeViewImmediate(this);
        } else if (this.m_Fullscreen) {
            this.m_LoaderActivity.m_FrameLayout.removeView(this);
        } else {
            this.m_LoaderActivity.m_TopLevel.removeView(this);
        }
    }

    public void videoSetVolume(int i) {
        this.m_Volume = ((float) i) / 256.0f;
        if (this.m_MediaPlayer != null) {
            this.m_MediaPlayer.setVolume(this.m_Volume, this.m_Volume);
        }
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        this.m_MediaPlayer = mediaPlayer;
        this.m_MediaPlayer.setVolume(this.m_Volume, this.m_Volume);
        start();
    }

    public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        this.m_LoaderActivity.m_View.videoStopped();
        return true;
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        this.m_MediaPlayer = null;
        this.m_Repeats--;
        if (this.m_Repeats <= 0) {
            videoStop();
            this.m_LoaderActivity.m_View.videoStopped();
            return;
        }
        stopPlayback();
        if (this.m_Uri != null) {
            setVideoURI(this.m_Uri);
        } else {
            setVideoPath(this.m_Path);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.m_LoaderActivity.LoaderThread() == null) {
            return false;
        }
        int[] iArr = new int[2];
        getLocationOnScreen(iArr);
        motionEvent.offsetLocation((float) iArr[0], (float) iArr[1]);
        return this.m_LoaderActivity.LoaderThread().onTouchEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.m_Width != 0 && this.m_Height != 0) {
            setMeasuredDimension(this.m_Width, this.m_Height);
        }
    }
}
