package com.ideaworks3d.marmalade;

import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import java.lang.reflect.Method;

public class LoaderActivity extends Activity {
    private static final int INTENT_CODE = 123456;
    public static LoaderActivity m_Activity;
    public boolean m_ActivityPaused = false;
    private Intent m_Data;
    public FrameLayout m_FrameLayout;
    public boolean m_IgnoreFocusLoss = false;
    private volatile boolean m_IntentBlocking;
    private LoaderThread m_LoaderThread;
    private Handler m_ProgressDialogHandler;
    public RelativeLayout m_TopLevel;
    private Method m_TouchpadEvent = null;
    public LoaderView m_View;
    private OrientationEventListener m_orientationEventListener = null;

    interface CursorCompleteListener {
        void cursorLoadComplete(Cursor cursor);
    }

    /* access modifiers changed from: private */
    public native void onOrientationChangedNative();

    private class DelayedResumeTask extends AsyncTask<Void, Void, Void> {
        private DelayedResumeTask() {
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void voidR) {
            if (!LoaderActivity.this.isScreenOn()) {
                return;
            }
            if (!LoaderActivity.this.isScreenLocked()) {
                LoaderActivity.this.onResume();
            } else {
                LoaderActivity.this.startDelayedResume();
            }
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... voidArr) {
            try {
                synchronized (this) {
                    wait(300);
                }
                return null;
            } catch (InterruptedException e) {
                return null;
            }
        }
    }

    public LoaderThread LoaderThread() {
        return this.m_LoaderThread;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (m_Activity != null) {
        }
        String packageName = getPackageName();
        int lastIndexOf = packageName.lastIndexOf(46);
        if (lastIndexOf >= 0) {
            packageName = packageName.substring(lastIndexOf + 1);
        }
        System.loadLibrary(packageName);
        m_Activity = this;
        if (Integer.parseInt(Build.VERSION.SDK) >= 9) {
            try {
                this.m_TouchpadEvent = Class.forName("com.ideaworks3d.marmalade.s3eTouchpad").getMethod("onTouchEvent", MotionEvent.class);
            } catch (Exception e) {
                this.m_TouchpadEvent = null;
            }
        }
        this.m_ProgressDialogHandler = new ProgressDialogHandler();
        this.m_TopLevel = new RelativeLayout(this);
        this.m_FrameLayout = new FrameLayout(this);
        this.m_TopLevel.addView(this.m_FrameLayout);
        createView(false);
        setContentView(this.m_TopLevel);
        this.m_orientationEventListener = new OrientationEventListener(this, 3) {
            public void onOrientationChanged(int i) {
                LoaderActivity.this.onOrientationChangedNative();
            }
        };
        this.m_orientationEventListener.enable();
        if (!this.m_orientationEventListener.canDetectOrientation()) {
        }
    }

    public void createView(boolean z) {
        if (this.m_View != null) {
            this.m_FrameLayout.removeView(this.m_View);
            this.m_View = null;
            System.gc();
        }
        this.m_View = new LoaderView(this, z);
        this.m_FrameLayout.addView(this.m_View, 0);
        if (this.m_LoaderThread != null) {
            this.m_LoaderThread.setView(this.m_View);
        }
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (configuration.hardKeyboardHidden == 1) {
            this.m_View.m_LoaderKeyboard.hardKeyboardConfigurationChanged(true);
        } else if (configuration.hardKeyboardHidden == 2) {
            this.m_View.m_LoaderKeyboard.hardKeyboardConfigurationChanged(false);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        startLoader();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.m_ActivityPaused = false;
        if (!isScreenLocked()) {
            this.m_orientationEventListener.enable();
            if (this.m_LoaderThread != null) {
                this.m_LoaderThread.onResume();
            }
        } else if (isScreenOn()) {
            startDelayedResume();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (this.m_LoaderThread != null) {
            this.m_LoaderThread.onPause();
        }
        this.m_ActivityPaused = true;
        this.m_orientationEventListener.disable();
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (m_Activity != this) {
            super.onDestroy();
            return;
        }
        if (isFinishing() && this.m_LoaderThread != null) {
            this.m_LoaderThread.soundStop();
            this.m_LoaderThread.audioStopAll();
            this.m_LoaderThread.onDestroy();
            this.m_LoaderThread = null;
            m_Activity = null;
        }
        this.m_orientationEventListener.disable();
        super.onDestroy();
    }

    public void onLowMemory() {
        if (this.m_LoaderThread != null) {
            this.m_LoaderThread.onLowMemory();
        }
    }

    /* access modifiers changed from: private */
    public boolean isScreenLocked() {
        return ((KeyguardManager) getSystemService("keyguard")).inKeyguardRestrictedInputMode();
    }

    /* access modifiers changed from: private */
    public boolean isScreenOn() {
        return ((PowerManager) getSystemService("power")).isScreenOn();
    }

    /* access modifiers changed from: private */
    public void startDelayedResume() {
        new DelayedResumeTask().execute(new Void[0]);
    }

    private void startLoader() {
        this.m_LoaderThread = LoaderThread.getInstance(this, getAssets(), getFilesDir(), this.m_View);
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        if (this.m_TouchpadEvent != null) {
            try {
                if (((Boolean) this.m_TouchpadEvent.invoke(null, motionEvent)).booleanValue()) {
                    return true;
                }
            } catch (Exception e) {
            }
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    private boolean onKeyEvent(int i, int i2, KeyEvent keyEvent) {
        if (this.m_View != null) {
            return this.m_View.onKeyEvent(i, i2, keyEvent);
        }
        return false;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (onKeyEvent(i, 1, keyEvent)) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (onKeyEvent(i, 0, keyEvent)) {
            return true;
        }
        return super.onKeyUp(i, keyEvent);
    }

    public boolean getIgnoreFocusLoss() {
        return this.m_IgnoreFocusLoss;
    }

    public void setIgnoreFocusLoss(boolean z) {
        this.m_IgnoreFocusLoss = z;
    }

    public Intent ExecuteIntent(final Intent intent) {
        this.m_Data = null;
        this.m_IntentBlocking = true;
        if (this.m_LoaderThread == null) {
            return null;
        }
        LoaderThread().runOnOSThread(new Runnable() {
            public void run() {
                try {
                    LoaderActivity.this.startActivityForResult(intent, LoaderActivity.INTENT_CODE);
                } catch (Exception e) {
                    LoaderAPI.trace("Could not start activity: " + e.getMessage());
                }
            }
        });
        while (this.m_IntentBlocking) {
            LoaderAPI.s3eDeviceYield(20);
        }
        return this.m_Data;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        Intent intent2;
        super.onActivityResult(i, i2, intent);
        if (i == INTENT_CODE) {
            if (i2 != -1) {
                LoaderAPI.trace("Intent cancelled");
                this.m_Data = null;
            } else {
                if (intent == null) {
                    intent2 = new Intent();
                } else {
                    intent2 = intent;
                }
                this.m_Data = intent2;
            }
            this.m_IntentBlocking = false;
        }
    }

    public void getCursor(final Uri uri, final String[] strArr, final CursorCompleteListener cursorCompleteListener) {
        LoaderThread().runOnOSThread(new Runnable() {
            public void run() {
                if (Integer.parseInt(Build.VERSION.SDK) >= 11) {
                    CursorLoaderHelper.getCursor(uri, strArr, cursorCompleteListener);
                    return;
                }
                cursorCompleteListener.cursorLoadComplete(LoaderActivity.this.managedQuery(uri, strArr, null, null, null));
            }
        });
    }

    public void ShowProgressDialog() {
        if (this.m_ProgressDialogHandler != null) {
            Message message = new Message();
            message.what = 0;
            this.m_ProgressDialogHandler.sendMessage(message);
        }
    }

    public void HideProgressDialog() {
        if (this.m_ProgressDialogHandler != null) {
            Message message = new Message();
            message.what = 1;
            this.m_ProgressDialogHandler.sendMessage(message);
        }
    }
}
