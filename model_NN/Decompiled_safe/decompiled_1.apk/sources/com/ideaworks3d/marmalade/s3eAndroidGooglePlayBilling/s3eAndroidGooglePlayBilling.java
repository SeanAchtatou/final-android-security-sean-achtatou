package com.ideaworks3d.marmalade.s3eAndroidGooglePlayBilling;

import android.content.Intent;
import android.util.Log;
import com.ideaworks3d.marmalade.LoaderAPI;
import com.ideaworks3d.marmalade.s3eAndroidGooglePlayBilling.util.IabHelper;
import com.ideaworks3d.marmalade.s3eAndroidGooglePlayBilling.util.IabResult;
import com.ideaworks3d.marmalade.s3eAndroidGooglePlayBilling.util.Inventory;
import com.ideaworks3d.marmalade.s3eAndroidGooglePlayBilling.util.Purchase;
import com.ideaworks3d.marmalade.s3eAndroidGooglePlayBilling.util.SkuDetails;
import java.util.Arrays;
import java.util.List;

class s3eAndroidGooglePlayBilling {
    private static final String TAG = "s3eAndroidGooglePlayBilling";
    public static IabHelper mHelper;
    public static final IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult iabResult, Purchase purchase) {
            Log.d(s3eAndroidGooglePlayBilling.TAG, "Purchase finished: " + iabResult + ", purchase: " + purchase);
            if (iabResult.isFailure()) {
                s3eAndroidGooglePlayBilling.safe_native_PURCHASE_CALLBACK(iabResult, null);
                return;
            }
            Log.d(s3eAndroidGooglePlayBilling.TAG, "Purchase successful.");
            s3eAndroidGooglePlayBilling.safe_native_PURCHASE_CALLBACK(iabResult, purchase);
        }
    };
    public static boolean m_SendRequest = false;
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult iabResult) {
            Log.d(s3eAndroidGooglePlayBilling.TAG, "Consumption finished. Purchase: " + purchase + ", result: " + iabResult);
            s3eAndroidGooglePlayBilling.safe_native_CONSUME_CALLBACK(iabResult);
        }
    };
    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult iabResult, Inventory inventory) {
            Log.d(s3eAndroidGooglePlayBilling.TAG, "Query inventory finished.");
            if (!s3eAndroidGooglePlayBilling.this.s3eAndroidGooglePlayBillingAvailable) {
                s3eAndroidGooglePlayBilling.safe_native_RESTORE_CALLBACK(new IabResult(3, "Android Market Billing is not available, did you call s3eAndroidGooglePlayBillingStart?"), null);
                return;
            }
            Log.d(s3eAndroidGooglePlayBilling.TAG, "Query inventory was successful.");
            s3eAndroidGooglePlayBilling.safe_native_RESTORE_CALLBACK(iabResult, inventory);
        }
    };
    IabHelper.QueryProductsFinishedListener mGotSkusListener = new IabHelper.QueryProductsFinishedListener() {
        public void onQuerySkusFinished(IabResult iabResult, Inventory inventory) {
            Log.d(s3eAndroidGooglePlayBilling.TAG, "Query products finished.");
            if (iabResult.isFailure()) {
                s3eAndroidGooglePlayBilling.safe_native_LIST_PRODUCTS_CALLBACK(iabResult, null);
                return;
            }
            Log.d(s3eAndroidGooglePlayBilling.TAG, "Query products was successful.");
            s3eAndroidGooglePlayBilling.safe_native_LIST_PRODUCTS_CALLBACK(iabResult, inventory);
        }
    };
    /* access modifiers changed from: private */
    public boolean s3eAndroidGooglePlayBillingAvailable = false;

    public static native void native_CONSUME_CALLBACK(int i, String str);

    public static native void native_LIST_PRODUCTS_CALLBACK(int i, String str, S3eBillingItemInfo[] s3eBillingItemInfoArr);

    public static native void native_PURCHASE_CALLBACK(int i, String str, S3eBillingPurchase s3eBillingPurchase);

    public static native void native_RESTORE_CALLBACK(int i, String str, S3eBillingPurchase[] s3eBillingPurchaseArr);

    s3eAndroidGooglePlayBilling() {
    }

    public int s3eAndroidGooglePlayBillingStart(String str) {
        Log.d(TAG, "s3eAndroidGooglePlayBillingStart called.");
        if (str == null) {
            Log.d(TAG, "ERROR: No public key sent.");
            return 1;
        }
        Log.d(TAG, "Creating IAB helper.");
        mHelper = new IabHelper(LoaderAPI.getActivity(), str);
        mHelper.enableDebugLogging(true);
        Log.d(TAG, "Starting setup.");
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult iabResult) {
                Log.d(s3eAndroidGooglePlayBilling.TAG, "Setup finished.");
                if (!iabResult.isSuccess()) {
                    Log.d(s3eAndroidGooglePlayBilling.TAG, "Problem setting up in-app billing: " + iabResult);
                } else {
                    boolean unused = s3eAndroidGooglePlayBilling.this.s3eAndroidGooglePlayBillingAvailable = true;
                }
            }
        });
        return 0;
    }

    public void s3eAndroidGooglePlayBillingStop() {
        Log.d(TAG, "s3eAndroidGooglePlayBillingStop called.");
        if (mHelper != null) {
            mHelper.dispose();
        } else {
            Log.d(TAG, "s3eAndroidGooglePlayBillingStop called without having been successfully started.");
        }
        mHelper = null;
        this.s3eAndroidGooglePlayBillingAvailable = false;
    }

    public int s3eAndroidGooglePlayBillingIsSupported() {
        return this.s3eAndroidGooglePlayBillingAvailable ? 0 : 1;
    }

    public void s3eAndroidGooglePlayBillingRequestPurchase(String str, boolean z, String str2) {
        Log.d(TAG, "s3eAndroidGooglePlayBillingRequestPurchase called for: " + str);
        if (!this.s3eAndroidGooglePlayBillingAvailable) {
            safe_native_PURCHASE_CALLBACK(new IabResult(3, "Android Market Billing is not available, did you call s3eAndroidGooglePlayBillingStart?"), null);
            return;
        }
        m_SendRequest = true;
        Intent intent = new Intent(LoaderAPI.getActivity(), PurchaseProxy.class);
        intent.putExtra("productID", str);
        intent.putExtra("inApp", z);
        intent.putExtra("developerPayLoad", str2);
        LoaderAPI.getActivity().startActivity(intent);
    }

    public void s3eAndroidGooglePlayBillingRequestProductInformation(String[] strArr, String[] strArr2) {
        List list;
        List list2;
        Log.d(TAG, "s3eAndroidGooglePlayBillingRequestProductInformation called for:");
        if (strArr != null) {
            Log.d(TAG, "inApp: " + strArr.toString());
        }
        if (strArr2 != null) {
            Log.d(TAG, "subs: " + strArr2.toString());
        }
        if (!this.s3eAndroidGooglePlayBillingAvailable) {
            safe_native_LIST_PRODUCTS_CALLBACK(new IabResult(3, "Android Market Billing is not available, did you call s3eAndroidGooglePlayBillingStart?"), null);
            return;
        }
        if (strArr != null) {
            list = Arrays.asList(strArr);
        } else {
            list = null;
        }
        if (strArr2 != null) {
            list2 = Arrays.asList(strArr2);
        } else {
            list2 = null;
        }
        mHelper.querySkusAsync(list, list2, this.mGotSkusListener);
    }

    public void s3eAndroidGooglePlayBillingRestoreTransactions() {
        Log.d(TAG, "s3eAndroidGooglePlayBillingRestoreTransactions called.");
        if (!this.s3eAndroidGooglePlayBillingAvailable) {
            safe_native_RESTORE_CALLBACK(new IabResult(3, "Android Market Billing is not available, did you call s3eAndroidGooglePlayBillingStart?"), null);
        } else {
            mHelper.queryInventoryAsync(false, null, this.mGotInventoryListener);
        }
    }

    public void s3eAndroidGooglePlayBillingConsumeItem(String str) {
        Log.d(TAG, "s3eAndroidGooglePlayBillingConsumeItem called for: " + str);
        if (!this.s3eAndroidGooglePlayBillingAvailable) {
            safe_native_CONSUME_CALLBACK(new IabResult(3, "Android Market Billing is not available, did you call s3eAndroidGooglePlayBillingStart?"));
            return;
        }
        mHelper.consumeAsync(new Purchase(str), this.mConsumeFinishedListener);
    }

    public static class S3eBillingPurchase {
        public String m_DeveloperPayload;
        public String m_JSON;
        public String m_OrderID;
        public String m_PackageID;
        public String m_ProductId;
        public int m_PurchaseState;
        public long m_PurchaseTime;
        public String m_PurchaseToken;
        public String m_Signature;

        public S3eBillingPurchase(Purchase purchase) {
            this.m_OrderID = purchase.getOrderId();
            this.m_PackageID = purchase.getPackageName();
            this.m_ProductId = purchase.getSku();
            this.m_PurchaseTime = purchase.getPurchaseTime();
            this.m_PurchaseState = purchase.getPurchaseState();
            this.m_PurchaseToken = purchase.getToken();
            this.m_DeveloperPayload = purchase.getDeveloperPayload();
            this.m_JSON = purchase.getOriginalJson();
            this.m_Signature = purchase.getSignature();
        }
    }

    public static class S3eBillingItemInfo {
        public String m_Description;
        public String m_Price;
        public String m_ProductID;
        public String m_Title;
        public String m_Type;

        public S3eBillingItemInfo(SkuDetails skuDetails) {
            this.m_ProductID = skuDetails.getSku();
            this.m_Type = skuDetails.getType();
            this.m_Price = skuDetails.getPrice();
            this.m_Title = skuDetails.getTitle();
            this.m_Description = skuDetails.getDescription();
        }
    }

    /* access modifiers changed from: private */
    public static void safe_native_PURCHASE_CALLBACK(IabResult iabResult, Purchase purchase) {
        S3eBillingPurchase s3eBillingPurchase = null;
        if (purchase != null) {
            try {
                s3eBillingPurchase = new S3eBillingPurchase(purchase);
            } catch (UnsatisfiedLinkError e) {
                Log.v(TAG, "No native handlers installed for safe_native_PURCHASE_CALLBACK, we received " + iabResult.getResponse() + " " + iabResult.getMessage());
                return;
            }
        }
        native_PURCHASE_CALLBACK(iabResult.getResponse(), iabResult.getMessage(), s3eBillingPurchase);
    }

    /* access modifiers changed from: private */
    public static void safe_native_LIST_PRODUCTS_CALLBACK(IabResult iabResult, Inventory inventory) {
        if (inventory != null) {
            try {
                List<SkuDetails> allSkus = inventory.getAllSkus();
                S3eBillingItemInfo[] s3eBillingItemInfoArr = new S3eBillingItemInfo[allSkus.size()];
                for (int i = 0; i < allSkus.size(); i++) {
                    s3eBillingItemInfoArr[i] = new S3eBillingItemInfo(allSkus.get(i));
                }
                native_LIST_PRODUCTS_CALLBACK(iabResult.getResponse(), iabResult.getMessage(), s3eBillingItemInfoArr);
            } catch (UnsatisfiedLinkError e) {
                Log.v(TAG, "No native handlers installed for native_LIST_PRODUCTS_CALLBACK, we received " + iabResult.getResponse() + " " + iabResult.getMessage());
            }
        } else {
            native_LIST_PRODUCTS_CALLBACK(iabResult.getResponse(), iabResult.getMessage(), null);
        }
    }

    /* access modifiers changed from: private */
    public static void safe_native_RESTORE_CALLBACK(IabResult iabResult, Inventory inventory) {
        if (inventory != null) {
            try {
                List<Purchase> allPurchases = inventory.getAllPurchases();
                S3eBillingPurchase[] s3eBillingPurchaseArr = new S3eBillingPurchase[allPurchases.size()];
                for (int i = 0; i < allPurchases.size(); i++) {
                    s3eBillingPurchaseArr[i] = new S3eBillingPurchase(allPurchases.get(i));
                }
                native_RESTORE_CALLBACK(iabResult.getResponse(), iabResult.getMessage(), s3eBillingPurchaseArr);
            } catch (UnsatisfiedLinkError e) {
                Log.v(TAG, "No native handlers installed for native_LIST_PRODUCTS_CALLBACK, we received " + iabResult.getResponse() + " " + iabResult.getMessage());
            }
        } else {
            native_RESTORE_CALLBACK(iabResult.getResponse(), iabResult.getMessage(), null);
        }
    }

    /* access modifiers changed from: private */
    public static void safe_native_CONSUME_CALLBACK(IabResult iabResult) {
        try {
            native_CONSUME_CALLBACK(iabResult.getResponse(), iabResult.getMessage());
        } catch (UnsatisfiedLinkError e) {
            Log.v(TAG, "No native handlers installed for native_LIST_PRODUCTS_CALLBACK, we received " + iabResult.getResponse() + " " + iabResult.getMessage());
        }
    }
}
