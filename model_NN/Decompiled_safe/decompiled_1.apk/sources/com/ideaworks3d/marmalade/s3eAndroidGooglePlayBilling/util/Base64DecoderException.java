package com.ideaworks3d.marmalade.s3eAndroidGooglePlayBilling.util;

public class Base64DecoderException extends Exception {
    private static final long serialVersionUID = 1;

    public Base64DecoderException() {
    }

    public Base64DecoderException(String str) {
        super(str);
    }
}
