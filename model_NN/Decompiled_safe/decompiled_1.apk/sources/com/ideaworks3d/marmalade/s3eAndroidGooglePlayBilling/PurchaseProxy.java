package com.ideaworks3d.marmalade.s3eAndroidGooglePlayBilling;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.ideaworks3d.marmalade.s3eAndroidGooglePlayBilling.util.IabHelper;
import com.ideaworks3d.marmalade.s3eAndroidGooglePlayBilling.util.IabResult;
import com.ideaworks3d.marmalade.s3eAndroidGooglePlayBilling.util.Purchase;

public class PurchaseProxy extends Activity {
    static final int RC_REQUEST = 3147;
    private static final String TAG = "PurchaseProxy";
    public IabHelper.OnIabPurchaseFinishedListener mProxyPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult iabResult, Purchase purchase) {
            Log.d(PurchaseProxy.TAG, "onIabPurchaseFinished");
            s3eAndroidGooglePlayBilling.mPurchaseFinishedListener.onIabPurchaseFinished(iabResult, purchase);
            Log.d(PurchaseProxy.TAG, "PurchaseProxy Activity - Closing Activity");
            PurchaseProxy.this.finish();
        }
    };

    /* access modifiers changed from: protected */
    public void onPause() {
        Log.d(TAG, "Proxy OnPause");
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        Log.d(TAG, "Proxy onResume");
        super.onResume();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (s3eAndroidGooglePlayBilling.m_SendRequest) {
            s3eAndroidGooglePlayBilling.m_SendRequest = false;
            Intent intent = getIntent();
            String stringExtra = intent.getStringExtra("productID");
            String stringExtra2 = intent.getStringExtra("developerPayLoad");
            if (intent.getBooleanExtra("inApp", true)) {
                s3eAndroidGooglePlayBilling.mHelper.launchPurchaseFlow(this, stringExtra, RC_REQUEST, this.mProxyPurchaseFinishedListener, stringExtra2);
            } else {
                s3eAndroidGooglePlayBilling.mHelper.launchSubscriptionPurchaseFlow(this, stringExtra, RC_REQUEST, this.mProxyPurchaseFinishedListener, stringExtra2);
            }
        }
        Log.d(TAG, "PurchaseProxy Activity has been restarted with Helper in flight - notifying IabHelper");
        s3eAndroidGooglePlayBilling.mHelper.SetPurchaseListener(this.mProxyPurchaseFinishedListener);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        Log.d(TAG, "onActivityResult(" + i + "," + i2 + "," + intent);
        if (!s3eAndroidGooglePlayBilling.mHelper.handleActivityResult(i, i2, intent)) {
            super.onActivityResult(i, i2, intent);
        } else {
            Log.d(TAG, "onActivityResult forwarded by PurchaseProxy. Closing down.");
        }
    }
}
