package com.ideaworks3d.marmalade.s3eAndroidGooglePlayBilling.util;

import android.text.TextUtils;
import android.util.Log;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

public class Security {
    private static final String KEY_FACTORY_ALGORITHM = "RSA";
    private static final String SIGNATURE_ALGORITHM = "SHA1withRSA";
    private static final String TAG = "IABUtil/Security";

    public static boolean verifyPurchase(String str, String str2, String str3) {
        if (str2 == null) {
            Log.e(TAG, "data is null");
            return false;
        }
        if (!TextUtils.isEmpty(str3)) {
            PublicKey generatePublicKey = generatePublicKey(str);
            if (generatePublicKey == null) {
                return false;
            }
            if (!verify(generatePublicKey, str2, str3)) {
                Log.w(TAG, "signature does not match data.");
                return false;
            }
        }
        return true;
    }

    public static PublicKey generatePublicKey(String str) {
        try {
            return KeyFactory.getInstance(KEY_FACTORY_ALGORITHM).generatePublic(new X509EncodedKeySpec(Base64.decode(str)));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (InvalidKeySpecException e2) {
            Log.e(TAG, "Invalid key specification." + e2.getMessage());
            return null;
        } catch (Base64DecoderException e3) {
            Log.e(TAG, "Base64 decoding failed." + e3.getMessage());
            return null;
        }
    }

    public static boolean verify(PublicKey publicKey, String str, String str2) {
        try {
            Signature instance = Signature.getInstance(SIGNATURE_ALGORITHM);
            instance.initVerify(publicKey);
            instance.update(str.getBytes());
            if (instance.verify(Base64.decode(str2))) {
                return true;
            }
            Log.e(TAG, "Signature verification failed.");
            return false;
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "NoSuchAlgorithmException." + e.getMessage());
            return false;
        } catch (InvalidKeyException e2) {
            Log.e(TAG, "Invalid key specification." + e2.getMessage());
            return false;
        } catch (SignatureException e3) {
            Log.e(TAG, "Signature exception." + e3.getMessage());
            return false;
        } catch (Base64DecoderException e4) {
            Log.e(TAG, "Base64 decoding failed." + e4.getMessage());
            return false;
        }
    }
}
