package com.ideaworks3d.marmalade;

import android.view.SurfaceHolder;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL;

class LoaderGL {
    private static final int EGL_CONTEXT_CLIENT_VERSION = 12440;
    private static final int EGL_DEPTH_ENCODING_NV = 12514;
    private boolean m_DoneInit;
    EGL10 m_Egl;
    EGLConfig[] m_EglConfigs;
    EGLContext m_EglContext;
    EGLDisplay m_EglDisplay;
    EGLSurface m_EglSurface;
    private int m_GLVersion;
    private boolean m_Started;

    LoaderGL() {
    }

    public boolean started() {
        return this.m_Started;
    }

    public void init() {
        this.m_Egl = (EGL10) EGLContext.getEGL();
        this.m_EglDisplay = this.m_Egl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
        this.m_Egl.eglInitialize(this.m_EglDisplay, new int[2]);
        chooseConfigs();
        int[] iArr = null;
        if (this.m_GLVersion == 2) {
            iArr = new int[]{EGL_CONTEXT_CLIENT_VERSION, this.m_GLVersion, 12344};
        }
        this.m_EglContext = this.m_Egl.eglCreateContext(this.m_EglDisplay, this.m_EglConfigs[0], EGL10.EGL_NO_CONTEXT, iArr);
        this.m_DoneInit = true;
    }

    public GL startGL(SurfaceHolder surfaceHolder, int i) {
        if (i != 0) {
            this.m_GLVersion = i;
        }
        if (!this.m_DoneInit) {
            init();
        }
        this.m_EglSurface = this.m_Egl.eglCreateWindowSurface(this.m_EglDisplay, this.m_EglConfigs[0], surfaceHolder, null);
        this.m_Egl.eglMakeCurrent(this.m_EglDisplay, this.m_EglSurface, this.m_EglSurface, this.m_EglContext);
        this.m_Started = true;
        return this.m_EglContext.getGL();
    }

    private class ConfigSetting {
        private int mDefault;
        private String mName;
        private int mSetting;
        private int mValue;

        public ConfigSetting(String str, int i) {
            Construct(str, i, -1);
        }

        public ConfigSetting(String str, int i, int i2) {
            Construct(str, i, i2);
        }

        private void Construct(String str, int i, int i2) {
            this.mName = str;
            this.mSetting = i;
            this.mDefault = i2;
            int[] iArr = {-1};
            if (LoaderAPI.s3eConfigGetInt("GL", str, iArr) == 0) {
                this.mValue = iArr[0];
            } else {
                this.mValue = i2;
            }
        }

        public String GetName() {
            return this.mName;
        }

        public int GetSetting() {
            return this.mSetting;
        }

        public int GetValue() {
            return this.mValue;
        }

        public int GetDefault() {
            return this.mDefault;
        }

        public void SetValueToDefault() {
            this.mValue = this.mDefault;
        }

        public boolean HasValue() {
            return this.mValue != -1;
        }
    }

    private static int CountSettingsWithValue(ConfigSetting[] configSettingArr) {
        int i = 0;
        for (ConfigSetting HasValue : configSettingArr) {
            if (HasValue.HasValue()) {
                i++;
            }
        }
        return i;
    }

    private static int CopySettingsWithValues(ConfigSetting[] configSettingArr, int[] iArr) {
        int i = 0;
        for (ConfigSetting configSetting : configSettingArr) {
            if (configSetting.HasValue()) {
                iArr[i * 2] = configSetting.GetSetting();
                iArr[(i * 2) + 1] = configSetting.GetValue();
                i++;
            }
        }
        return i;
    }

    private int[] CreateSpecFromSettings(ConfigSetting[] configSettingArr) {
        int CountSettingsWithValue = CountSettingsWithValue(configSettingArr);
        if (this.m_GLVersion == 2) {
            CountSettingsWithValue++;
        }
        int[] iArr = new int[((CountSettingsWithValue * 2) + 1)];
        int CopySettingsWithValues = CopySettingsWithValues(configSettingArr, iArr);
        if (this.m_GLVersion == 2) {
            iArr[CopySettingsWithValues * 2] = 12352;
            iArr[(CopySettingsWithValues * 2) + 1] = 4;
            CopySettingsWithValues++;
        }
        iArr[CopySettingsWithValues * 2] = 12344;
        return iArr;
    }

    private void chooseConfigs() {
        ConfigSetting[] configSettingArr = {new ConfigSetting("EGL_BUFFER_SIZE", 12320), new ConfigSetting("EGL_DEPTH_ENCODING_NV", EGL_DEPTH_ENCODING_NV), new ConfigSetting("EGL_DEPTH_SIZE", 12325), new ConfigSetting("EGL_SURFACE_TYPE", 12339), new ConfigSetting("EGL_RED_SIZE", 12324), new ConfigSetting("EGL_GREEN_SIZE", 12323), new ConfigSetting("EGL_BLUE_SIZE", 12322), new ConfigSetting("EGL_ALPHA_SIZE", 12321), new ConfigSetting("EGL_STENCIL_SIZE", 12326), new ConfigSetting("EGL_SAMPLE_BUFFERS", 12338), new ConfigSetting("EGL_SAMPLES", 12337)};
        int[] iArr = new int[1];
        int i = 0;
        while (true) {
            int[] CreateSpecFromSettings = CreateSpecFromSettings(configSettingArr);
            this.m_Egl.eglChooseConfig(this.m_EglDisplay, CreateSpecFromSettings, null, 0, iArr);
            if (iArr[0] != 0) {
                int i2 = iArr[0];
                this.m_EglConfigs = new EGLConfig[i2];
                this.m_Egl.eglChooseConfig(this.m_EglDisplay, CreateSpecFromSettings, this.m_EglConfigs, i2, iArr);
                EGLConfig eGLConfig = this.m_EglConfigs[0];
                findConfigAttrib(this.m_Egl, this.m_EglDisplay, eGLConfig, 12324, 0);
                findConfigAttrib(this.m_Egl, this.m_EglDisplay, eGLConfig, 12323, 0);
                findConfigAttrib(this.m_Egl, this.m_EglDisplay, eGLConfig, 12322, 0);
                findConfigAttrib(this.m_Egl, this.m_EglDisplay, eGLConfig, 12321, 0);
                findConfigAttrib(this.m_Egl, this.m_EglDisplay, eGLConfig, 12325, 0);
                findConfigAttrib(this.m_Egl, this.m_EglDisplay, eGLConfig, 12352, 0);
                findConfigAttrib(this.m_Egl, this.m_EglDisplay, eGLConfig, 12327, 12344);
                return;
            } else if (i < configSettingArr.length) {
                configSettingArr[i].SetValueToDefault();
                i++;
            } else {
                throw new RuntimeException("Failed to choose an EGL config");
            }
        }
    }

    private int findConfigAttrib(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig, int i, int i2) {
        int[] iArr = new int[1];
        if (egl10.eglGetConfigAttrib(eGLDisplay, eGLConfig, i, iArr)) {
            return iArr[0];
        }
        return i2;
    }

    public void swap() {
        this.m_Egl.eglSwapBuffers(this.m_EglDisplay, this.m_EglSurface);
    }

    public GL restartGL(SurfaceHolder surfaceHolder) {
        stop();
        return startGL(surfaceHolder, this.m_GLVersion);
    }

    public void stopGL() {
        stop();
        term();
    }

    private void stop() {
        this.m_Started = false;
        if (this.m_EglSurface != null) {
            this.m_Egl.eglMakeCurrent(this.m_EglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
            this.m_Egl.eglDestroySurface(this.m_EglDisplay, this.m_EglSurface);
            this.m_EglSurface = null;
        }
    }

    public void term() {
        if (this.m_EglContext != null) {
            this.m_Egl.eglDestroyContext(this.m_EglDisplay, this.m_EglContext);
            this.m_EglContext = null;
        }
        if (this.m_EglDisplay != null) {
            this.m_Egl.eglTerminate(this.m_EglDisplay);
            this.m_EglDisplay = null;
        }
        this.m_DoneInit = false;
    }
}
