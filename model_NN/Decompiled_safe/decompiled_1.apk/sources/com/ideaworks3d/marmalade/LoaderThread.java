package com.ideaworks3d.marmalade;

import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.ClipboardManager;
import android.view.Display;
import android.view.MotionEvent;
import android.view.WindowManager;
import com.ideaworks3d.marmalade.SuspendResumeEvent;
import com.ideaworks3d.marmalade.s3eAndroidGooglePlayBilling.util.IabHelper;
import java.io.File;
import java.lang.reflect.Method;

public class LoaderThread extends Thread implements SensorEventListener {
    static LoaderThread g_Singleton;
    private Sensor m_Accelerometer;
    private int m_AppDoingInitTerm;
    /* access modifiers changed from: private */
    public AssetManager m_Assets;
    /* access modifiers changed from: private */
    public int m_BatteryLevel;
    private BroadcastReceiver m_BatteryLevelReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            int unused = LoaderThread.this.m_BatteryLevel = intent.getIntExtra("level", 0);
            boolean access$700 = LoaderThread.this.m_ChargerConnected;
            boolean unused2 = LoaderThread.this.m_ChargerConnected = intent.getIntExtra("plugged", 0) != 0;
            if (access$700 != LoaderThread.this.m_ChargerConnected) {
                LoaderThread.this.chargerStateChanged(LoaderThread.this.m_ChargerConnected);
            }
        }
    };
    private Boolean m_BatteryLevelReceiverRegistered = false;
    /* access modifiers changed from: private */
    public boolean m_ChargerConnected;
    private Sensor m_Compass;
    /* access modifiers changed from: private */
    public final Runnable m_CreateView = new Runnable() {
        public void run() {
            LoaderThread.this.m_Loader.createView(LoaderThread.this.m_UseGL);
            synchronized (LoaderThread.this.m_CreateView) {
                LoaderThread.this.m_CreateView.notify();
            }
        }
    };
    private boolean m_DidSuspendForSurfaceChange;
    private File m_FileRoot;
    private int m_FixScreenOrientation;
    private Handler m_Handler = new Handler();
    private boolean m_IgnoreResizeWhilePaused;
    /* access modifiers changed from: private */
    public LoaderActivity m_Loader;
    private LoaderSMSReceiver m_LoaderSMSReceiver;
    private LoaderLocation m_Location;
    private MediaPlayerManager m_MediaPlayerManager;
    private MulticastLockFacade m_MulticastLock = null;
    private boolean m_NetworkCheckEnabled = false;
    private BroadcastReceiver m_NetworkCheckReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo == null || !activeNetworkInfo.isAvailable() || !activeNetworkInfo.isConnected()) {
                LoaderThread.this.networkCheckChanged(false);
            } else {
                LoaderThread.this.networkCheckChanged(true);
            }
        }
    };
    private Integer m_OnTouchWait = new Integer(0);
    private Sensor m_Orientation;
    private Boolean m_Paused = false;
    private Boolean m_ResumeInProgress = false;
    private final Runnable m_RunOnOSThread = new Runnable() {
        public void run() {
            LoaderThread.this.runOnOSTickNative();
        }
    };
    private SensorManager m_SensorManager;
    private boolean m_SkipNextChange;
    private SoundPlayer m_SoundPlayer;
    private SoundRecord m_SoundRecord;
    private int m_StartScreenOrientation;
    private Boolean m_Started = false;
    private Boolean m_Terminating = false;
    private Method m_TouchEvent = null;
    private boolean m_TouchMulti = false;
    /* access modifiers changed from: private */
    public boolean m_UseGL = false;
    private Boolean m_VideoIsPaused = false;
    private LoaderView m_View;
    private Method m_getRotation;
    private boolean m_splashFinished = false;

    private enum AudioState {
        None,
        Idle,
        Initialized,
        Stopped,
        Prepared,
        Started,
        Paused,
        PlaybackCompleted,
        Error
    }

    /* access modifiers changed from: private */
    public native void audioStoppedNotify(int i);

    /* access modifiers changed from: private */
    public native void chargerStateChanged(boolean z);

    private native void initNative();

    private native void lowMemoryWarning();

    /* access modifiers changed from: private */
    public native void networkCheckChanged(boolean z);

    private native void onAccelNative(float f, float f2, float f3);

    private native void onCompassNative(int i, float f, float f2, float f3);

    private native void resumeAppThreads();

    private native void runNative(String str, String str2);

    private native void runOnOSThreadNative(Runnable runnable);

    /* access modifiers changed from: private */
    public native void runOnOSTickNative();

    private native void setViewNative(LoaderView loaderView);

    private native void shutdownNative();

    private native boolean signalResume(boolean z);

    private native boolean signalSuspend(boolean z);

    private native void suspendAppThreads();

    public native void onMotionEvent(int i, int i2, int i3, int i4);

    public void suspendForSurfaceChange() {
        if (this.m_AppDoingInitTerm == 0 && !this.m_ResumeInProgress.booleanValue()) {
            suspendAppThreads();
            this.m_DidSuspendForSurfaceChange = true;
        }
    }

    public void resumeAfterSurfaceChange() {
        if (this.m_DidSuspendForSurfaceChange) {
            this.m_DidSuspendForSurfaceChange = false;
            resumeAppThreads();
        }
    }

    public boolean skipSurfaceChange() {
        if (this.m_IgnoreResizeWhilePaused) {
            if (this.m_Paused.booleanValue() && this.m_FixScreenOrientation != 4) {
                this.m_SkipNextChange = true;
                return true;
            } else if (!this.m_Paused.booleanValue() && this.m_SkipNextChange) {
                this.m_SkipNextChange = false;
                return true;
            }
        }
        return false;
    }

    private class MediaPlayerManager {
        private Boolean[] m_AudioIsPaused = new Boolean[16];
        /* access modifiers changed from: private */
        public int[] m_AudioPlayRepeats = new int[16];
        private AudioState[] m_AudioState = new AudioState[16];
        private int[] m_AudioVolume = new int[16];
        /* access modifiers changed from: private */
        public MediaPlayer[] m_MediaPlayer = new MediaPlayer[16];
        private MediaPlayerListener[] m_MediaPlayerListener = new MediaPlayerListener[16];
        private final int m_NumAudioChannels = 16;

        public MediaPlayerManager() {
            for (int i = 0; i < 16; i++) {
                this.m_MediaPlayer[i] = null;
                this.m_MediaPlayerListener[i] = new MediaPlayerListener(i);
                this.m_AudioState[i] = AudioState.None;
                this.m_AudioIsPaused[i] = false;
                this.m_AudioPlayRepeats[i] = 0;
                this.m_AudioVolume[i] = 100;
            }
        }

        private class MediaPlayerListener implements MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener {
            int m_channel;

            MediaPlayerListener(int i) {
                this.m_channel = i;
            }

            public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
                MediaPlayerManager.this.audioChangeState(AudioState.Error, this.m_channel);
                MediaPlayerManager.this.m_MediaPlayer[this.m_channel].reset();
                MediaPlayerManager.this.audioChangeState(AudioState.Idle, this.m_channel);
                MediaPlayerManager.this.audioStopped(this.m_channel);
                return true;
            }

            public void onCompletion(MediaPlayer mediaPlayer) {
                int[] access$300 = MediaPlayerManager.this.m_AudioPlayRepeats;
                int i = this.m_channel;
                access$300[i] = access$300[i] - 1;
                if (MediaPlayerManager.this.m_AudioPlayRepeats[this.m_channel] == 0) {
                    MediaPlayerManager.this.audioChangeState(AudioState.PlaybackCompleted, this.m_channel);
                    MediaPlayerManager.this.audioStop(this.m_channel);
                    return;
                }
                try {
                    MediaPlayerManager.this.m_MediaPlayer[this.m_channel].start();
                    MediaPlayerManager.this.audioChangeState(AudioState.Started, this.m_channel);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
            }
        }

        public int audioGetNumChannels() {
            return 16;
        }

        /* access modifiers changed from: private */
        public void audioStopped(int i) {
            LoaderThread.this.audioStoppedNotify(i);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:22:0x00b3, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x00b4, code lost:
            audioChangeState(com.ideaworks3d.marmalade.LoaderThread.AudioState.Error, r15);
            r0.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x00f5, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x00f6, code lost:
            audioChangeState(com.ideaworks3d.marmalade.LoaderThread.AudioState.Error, r15);
            r0.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
            return -1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
            return -2;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x00f5 A[ExcHandler: IOException (r0v2 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:3:0x0028] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public int audioPlay(java.lang.String r9, int r10, long r11, long r13, int r15) {
            /*
                r8 = this;
                r8.audioStop(r15)
                android.media.MediaPlayer[] r0 = r8.m_MediaPlayer
                r0 = r0[r15]
                if (r0 != 0) goto L_0x0028
                android.media.MediaPlayer[] r0 = r8.m_MediaPlayer
                android.media.MediaPlayer r1 = new android.media.MediaPlayer
                r1.<init>()
                r0[r15] = r1
                android.media.MediaPlayer[] r0 = r8.m_MediaPlayer
                r0 = r0[r15]
                com.ideaworks3d.marmalade.LoaderThread$MediaPlayerManager$MediaPlayerListener[] r1 = r8.m_MediaPlayerListener
                r1 = r1[r15]
                r0.setOnErrorListener(r1)
                android.media.MediaPlayer[] r0 = r8.m_MediaPlayer
                r0 = r0[r15]
                com.ideaworks3d.marmalade.LoaderThread$MediaPlayerManager$MediaPlayerListener[] r1 = r8.m_MediaPlayerListener
                r1 = r1[r15]
                r0.setOnCompletionListener(r1)
            L_0x0028:
                android.media.MediaPlayer[] r0 = r8.m_MediaPlayer     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                r0 = r0[r15]     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                r0.reset()     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                com.ideaworks3d.marmalade.LoaderThread$AudioState r0 = com.ideaworks3d.marmalade.LoaderThread.AudioState.Idle     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                r8.audioChangeState(r0, r15)     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                r0 = 0
                int r0 = (r13 > r0 ? 1 : (r13 == r0 ? 0 : -1))
                if (r0 <= 0) goto L_0x0099
                java.io.File r0 = new java.io.File     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                r0.<init>(r9)     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                r1 = 268435456(0x10000000, float:2.5243549E-29)
                android.os.ParcelFileDescriptor r1 = android.os.ParcelFileDescriptor.open(r0, r1)     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                android.content.res.AssetFileDescriptor r0 = new android.content.res.AssetFileDescriptor     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                r2 = r11
                r4 = r13
                r0.<init>(r1, r2, r4)     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                android.media.MediaPlayer[] r2 = r8.m_MediaPlayer     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                r2 = r2[r15]     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                java.io.FileDescriptor r3 = r0.getFileDescriptor()     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                long r4 = r0.getStartOffset()     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                long r6 = r0.getLength()     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                r2.setDataSource(r3, r4, r6)     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                r0.close()     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                r1.close()     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
            L_0x0065:
                com.ideaworks3d.marmalade.LoaderThread$AudioState r0 = com.ideaworks3d.marmalade.LoaderThread.AudioState.Initialized     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                r8.audioChangeState(r0, r15)     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                android.media.MediaPlayer[] r0 = r8.m_MediaPlayer     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                r0 = r0[r15]     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                r0.prepare()     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                com.ideaworks3d.marmalade.LoaderThread$AudioState r0 = com.ideaworks3d.marmalade.LoaderThread.AudioState.Prepared     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                r8.audioChangeState(r0, r15)     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                r8.audioSetVolumeInternal(r15)
                int[] r0 = r8.m_AudioPlayRepeats
                r0[r15] = r10
                android.media.MediaPlayer[] r0 = r8.m_MediaPlayer
                r0 = r0[r15]
                int[] r1 = r8.m_AudioPlayRepeats
                r1 = r1[r15]
                if (r1 != 0) goto L_0x0100
                r1 = 1
            L_0x0088:
                r0.setLooping(r1)
                android.media.MediaPlayer[] r0 = r8.m_MediaPlayer     // Catch:{ IllegalStateException -> 0x0102 }
                r0 = r0[r15]     // Catch:{ IllegalStateException -> 0x0102 }
                r0.start()     // Catch:{ IllegalStateException -> 0x0102 }
                com.ideaworks3d.marmalade.LoaderThread$AudioState r0 = com.ideaworks3d.marmalade.LoaderThread.AudioState.Started
                r8.audioChangeState(r0, r15)
                r0 = 0
            L_0x0098:
                return r0
            L_0x0099:
                java.lang.String r0 = "http://"
                int r0 = r9.indexOf(r0)     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                r1 = -1
                if (r0 != r1) goto L_0x00ab
                java.lang.String r0 = "https://"
                int r0 = r9.indexOf(r0)     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                r1 = -1
                if (r0 == r1) goto L_0x00be
            L_0x00ab:
                android.media.MediaPlayer[] r0 = r8.m_MediaPlayer     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                r0 = r0[r15]     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                r0.setDataSource(r9)     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                goto L_0x0065
            L_0x00b3:
                r0 = move-exception
                com.ideaworks3d.marmalade.LoaderThread$AudioState r1 = com.ideaworks3d.marmalade.LoaderThread.AudioState.Error
                r8.audioChangeState(r1, r15)
                r0.printStackTrace()
                r0 = -1
                goto L_0x0098
            L_0x00be:
                java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x00d2, IOException -> 0x00f5 }
                r0.<init>(r9)     // Catch:{ FileNotFoundException -> 0x00d2, IOException -> 0x00f5 }
                android.media.MediaPlayer[] r1 = r8.m_MediaPlayer     // Catch:{ FileNotFoundException -> 0x00d2, IOException -> 0x00f5 }
                r1 = r1[r15]     // Catch:{ FileNotFoundException -> 0x00d2, IOException -> 0x00f5 }
                java.io.FileDescriptor r2 = r0.getFD()     // Catch:{ FileNotFoundException -> 0x00d2, IOException -> 0x00f5 }
                r1.setDataSource(r2)     // Catch:{ FileNotFoundException -> 0x00d2, IOException -> 0x00f5 }
                r0.close()     // Catch:{ FileNotFoundException -> 0x00d2, IOException -> 0x00f5 }
                goto L_0x0065
            L_0x00d2:
                r0 = move-exception
                com.ideaworks3d.marmalade.LoaderThread r0 = com.ideaworks3d.marmalade.LoaderThread.this     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                android.content.res.AssetManager r0 = r0.m_Assets     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                android.content.res.AssetFileDescriptor r6 = r0.openFd(r9)     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                android.media.MediaPlayer[] r0 = r8.m_MediaPlayer     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                r0 = r0[r15]     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                java.io.FileDescriptor r1 = r6.getFileDescriptor()     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                long r2 = r6.getStartOffset()     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                long r4 = r6.getLength()     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                r0.setDataSource(r1, r2, r4)     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                r6.close()     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00f5 }
                goto L_0x0065
            L_0x00f5:
                r0 = move-exception
                com.ideaworks3d.marmalade.LoaderThread$AudioState r1 = com.ideaworks3d.marmalade.LoaderThread.AudioState.Error
                r8.audioChangeState(r1, r15)
                r0.printStackTrace()
                r0 = -2
                goto L_0x0098
            L_0x0100:
                r1 = 0
                goto L_0x0088
            L_0x0102:
                r0 = move-exception
                r0.printStackTrace()
                r0 = -2
                goto L_0x0098
            */
            throw new UnsupportedOperationException("Method not decompiled: com.ideaworks3d.marmalade.LoaderThread.MediaPlayerManager.audioPlay(java.lang.String, int, long, long, int):int");
        }

        public void doPause() {
            boolean z;
            for (int i = 0; i < 16; i++) {
                Boolean[] boolArr = this.m_AudioIsPaused;
                if (this.m_AudioState[i] == AudioState.Paused) {
                    z = true;
                } else {
                    z = false;
                }
                boolArr[i] = Boolean.valueOf(z);
                if (!this.m_AudioIsPaused[i].booleanValue()) {
                    audioPause(i);
                }
            }
        }

        public void doResume() {
            for (int i = 0; i < 16; i++) {
                if (!this.m_AudioIsPaused[i].booleanValue()) {
                    audioResume(i);
                }
            }
        }

        public int audioPause(int i) {
            if (this.m_AudioState[i] != AudioState.Started) {
                return -1;
            }
            if (this.m_MediaPlayer[i] == null) {
                return -1;
            }
            try {
                this.m_MediaPlayer[i].pause();
                audioChangeState(AudioState.Paused, i);
                return 0;
            } catch (IllegalStateException e) {
                return -1;
            }
        }

        public int audioResume(int i) {
            if (this.m_AudioState[i] != AudioState.Paused) {
                return -1;
            }
            if (this.m_MediaPlayer[i] == null) {
                return -1;
            }
            try {
                this.m_MediaPlayer[i].start();
                audioChangeState(AudioState.Started, i);
                return 0;
            } catch (IllegalStateException e) {
                return -1;
            }
        }

        public void audioStopAll() {
            for (int i = 0; i < 16; i++) {
                audioStop(i);
            }
        }

        public void audioStop(int i) {
            AudioState audioState = this.m_AudioState[i];
            if (this.m_MediaPlayer[i] == null) {
                return;
            }
            if (audioState == AudioState.Started || audioState == AudioState.Paused || audioState == AudioState.PlaybackCompleted) {
                try {
                    this.m_MediaPlayer[i].stop();
                    audioChangeState(AudioState.Stopped, i);
                } catch (IllegalStateException e) {
                }
                this.m_MediaPlayer[i].reset();
                audioChangeState(AudioState.Idle, i);
                audioStopped(i);
            }
        }

        public boolean audioIsPlaying(int i) {
            return this.m_MediaPlayer[i] != null && this.m_AudioState[i] == AudioState.Started;
        }

        public int audioGetStatus(int i) {
            switch (this.m_AudioState[i]) {
                case Started:
                    return 1;
                case Paused:
                    return 2;
                case Error:
                    return 3;
                default:
                    return 0;
            }
        }

        public int audioGetPosition(int i) {
            if (this.m_AudioState[i] == AudioState.Started || this.m_AudioState[i] == AudioState.Paused) {
                return this.m_MediaPlayer[i].getCurrentPosition();
            }
            return 0;
        }

        public int audioGetDuration(int i) {
            if (this.m_MediaPlayer[i] == null || this.m_AudioState[i] == AudioState.Error || this.m_AudioState[i] == AudioState.Idle || this.m_AudioState[i] == AudioState.Initialized) {
                return 0;
            }
            return this.m_MediaPlayer[i].getDuration();
        }

        public void audioSetPosition(int i, int i2) {
            this.m_MediaPlayer[i2].seekTo(i);
        }

        public void audioSetVolume(int i, int i2) {
            this.m_AudioVolume[i2] = i;
            audioSetVolumeInternal(i2);
        }

        private void audioSetVolumeInternal(int i) {
            if (this.m_MediaPlayer[i] != null && this.m_AudioState[i] != AudioState.Error) {
                float f = ((float) this.m_AudioVolume[i]) / 100.0f;
                this.m_MediaPlayer[i].setVolume(f, f);
            }
        }

        /* access modifiers changed from: private */
        public void audioChangeState(AudioState audioState, int i) {
            this.m_AudioState[i] = audioState;
        }
    }

    public void audioStopAll() {
        this.m_MediaPlayerManager.audioStopAll();
    }

    public void soundSetVolume(int i) {
        this.m_SoundPlayer.setVolume(i);
    }

    public boolean getStarted() {
        return this.m_Started.booleanValue();
    }

    private LoaderThread(LoaderActivity loaderActivity, AssetManager assetManager, File file) {
        this.m_Assets = assetManager;
        this.m_Loader = loaderActivity;
        this.m_StartScreenOrientation = 1;
        this.m_FixScreenOrientation = loaderActivity.getRequestedOrientation();
        this.m_FileRoot = file;
        this.m_SoundPlayer = new SoundPlayer();
        this.m_MediaPlayerManager = new MediaPlayerManager();
        this.m_Loader.registerReceiver(this.m_BatteryLevelReceiver, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        this.m_BatteryLevelReceiverRegistered = true;
        this.m_SensorManager = (SensorManager) this.m_Loader.getSystemService("sensor");
        try {
            File file2 = new File(this.m_FileRoot, "makeramdirectory.txt");
            if (file2.mkdirs()) {
            }
            file2.delete();
        } catch (Exception e) {
        }
        if (Build.VERSION.SDK_INT >= 5) {
            try {
                this.m_TouchEvent = Class.forName("com.ideaworks3d.marmalade.MultiTouch").getMethod("onTouchEvent", LoaderThread.class, MotionEvent.class);
            } catch (Exception e2) {
                this.m_TouchEvent = null;
            }
        }
        initNative();
    }

    public void setView(LoaderView loaderView) {
        if (this.m_View != null) {
            synchronized (this.m_View) {
                if (this.m_View != null) {
                    this.m_View.notify();
                }
            }
        }
        this.m_View = loaderView;
        this.m_StartScreenOrientation = this.m_View.getCurrentOrientation();
        doFixOrientation();
        setViewNative(this.m_View);
    }

    public static LoaderThread getInstance(LoaderActivity loaderActivity, AssetManager assetManager, File file, LoaderView loaderView) {
        if (g_Singleton == null) {
            g_Singleton = new LoaderThread(loaderActivity, assetManager, file);
            g_Singleton.setView(loaderView);
            g_Singleton.start();
        } else {
            synchronized (g_Singleton.m_View) {
                g_Singleton.m_Loader = loaderActivity;
                g_Singleton.setView(loaderView);
            }
        }
        return g_Singleton;
    }

    private int translateS3eOrientation(int i) {
        boolean z = Build.VERSION.SDK_INT < 9;
        switch (i) {
            case 0:
                return 4;
            case 1:
                return z ? 1 : 7;
            case 2:
                return z ? 0 : 6;
            case 3:
                return 1;
            case 4:
                return 0;
            default:
                return 4;
        }
    }

    private void fixOrientation(int i) {
        this.m_FixScreenOrientation = translateS3eOrientation(i);
        if (LoaderAPI.s3eConfigGet("AndroidIgnoreResizeWhilePaused", 0) != 0) {
            this.m_IgnoreResizeWhilePaused = true;
        }
        doFixOrientation();
    }

    public int getOrientation() {
        Display defaultDisplay = ((WindowManager) this.m_Loader.getSystemService("window")).getDefaultDisplay();
        if (this.m_getRotation == null) {
            Class<?> cls = defaultDisplay.getClass();
            try {
                this.m_getRotation = cls.getMethod("getRotation", new Class[0]);
            } catch (NoSuchMethodException e) {
                try {
                    this.m_getRotation = cls.getMethod("getOrientation", new Class[0]);
                } catch (NoSuchMethodException e2) {
                }
            }
        }
        try {
            return ((Integer) this.m_getRotation.invoke(defaultDisplay, new Object[0])).intValue();
        } catch (Exception e3) {
            return 0;
        }
    }

    public void onSplashFinished() {
        this.m_splashFinished = true;
        doFixOrientation();
    }

    private int extendSplashOrientation(int i) {
        if (Build.VERSION.SDK_INT < 9) {
            return i;
        }
        switch (i) {
            case 0:
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED /*8*/:
                return 6;
            case 1:
            case 9:
                return 7;
            default:
                return i;
        }
    }

    private int translateSplashOrientation(int i) {
        switch (i) {
            case 4:
            case 10:
                return this.m_splashFinished ? i : extendSplashOrientation(this.m_StartScreenOrientation);
            default:
                return i;
        }
    }

    public void doFixOrientation() {
        this.m_Loader.setRequestedOrientation(translateSplashOrientation(this.m_FixScreenOrientation));
    }

    private void touchSetWait(int i) {
        this.m_OnTouchWait = new Integer(i);
    }

    public void onResume() {
        this.m_Paused = false;
        this.m_ResumeInProgress = true;
        if (!signalResume(false)) {
            this.m_ResumeInProgress = false;
        }
    }

    public void runRunnable(Runnable runnable) {
        runnable.run();
    }

    public void runOnOSThread(Runnable runnable) {
        runOnOSThreadNative(runnable);
    }

    public void runOnOSSignal() {
        this.m_Handler.post(this.m_RunOnOSThread);
    }

    public void onPause() {
        if (!this.m_Terminating.booleanValue() && this.m_Started.booleanValue() && !this.m_Paused.booleanValue()) {
            this.m_Paused = true;
            signalSuspend(true);
        }
    }

    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        int type = sensorEvent.sensor.getType();
        if (type == 1) {
            onAccelNative(sensorEvent.values[0], sensorEvent.values[1], sensorEvent.values[2]);
        } else if (type == 2) {
            onCompassNative(0, sensorEvent.values[0], sensorEvent.values[1], sensorEvent.values[2]);
        } else if (type == 3) {
            onCompassNative(1, sensorEvent.values[0], sensorEvent.values[1], sensorEvent.values[2]);
        }
    }

    public void accelStart() {
        if (this.m_Accelerometer == null) {
            this.m_Accelerometer = this.m_SensorManager.getDefaultSensor(1);
            this.m_SensorManager.registerListener(this, this.m_Accelerometer, 1);
        }
    }

    public void accelStop() {
        if (this.m_Accelerometer != null) {
            this.m_SensorManager.unregisterListener(this, this.m_Accelerometer);
            this.m_Accelerometer = null;
        }
    }

    public boolean smsStart() {
        if (this.m_LoaderSMSReceiver != null || Integer.parseInt(Build.VERSION.SDK) <= 3) {
            return false;
        }
        this.m_LoaderSMSReceiver = new LoaderSMSReceiver();
        this.m_Loader.registerReceiver(this.m_LoaderSMSReceiver, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
        return true;
    }

    public void smsStop() {
        if (this.m_LoaderSMSReceiver != null) {
            this.m_Loader.unregisterReceiver(this.m_LoaderSMSReceiver);
            this.m_LoaderSMSReceiver = null;
        }
    }

    public void compassStart() {
        if (this.m_Compass == null) {
            this.m_Compass = this.m_SensorManager.getDefaultSensor(2);
            this.m_Orientation = this.m_SensorManager.getDefaultSensor(3);
            this.m_SensorManager.registerListener(this, this.m_Orientation, 1);
            this.m_SensorManager.registerListener(this, this.m_Compass, 1);
        }
    }

    public void compassStop() {
        if (this.m_Compass != null) {
            this.m_SensorManager.unregisterListener(this, this.m_Compass);
            this.m_SensorManager.unregisterListener(this, this.m_Orientation);
            this.m_Compass = null;
        }
    }

    public int soundInit(int i, boolean z, int i2) {
        return this.m_SoundPlayer.init(i, z, i2);
    }

    public void soundStart() {
        this.m_SoundPlayer.start();
    }

    public void soundStop() {
        this.m_SoundPlayer.stop();
    }

    public int recordStart(int i) {
        if (this.m_SoundRecord != null) {
            return 0;
        }
        this.m_SoundRecord = new SoundRecord();
        return this.m_SoundRecord.start(i);
    }

    public int recordStop() {
        if (this.m_SoundRecord == null) {
            return 1;
        }
        int stop = this.m_SoundRecord.stop();
        this.m_SoundRecord = null;
        return stop;
    }

    public String clipboardGet() {
        ClipboardManager clipboardManager = (ClipboardManager) this.m_Loader.getSystemService("clipboard");
        if (clipboardManager == null || !clipboardManager.hasText()) {
            return null;
        }
        return clipboardManager.getText().toString();
    }

    public void clipboardSet(String str) {
        ClipboardManager clipboardManager = (ClipboardManager) this.m_Loader.getSystemService("clipboard");
        if (clipboardManager != null) {
            clipboardManager.setText(str);
        }
    }

    public boolean networkCheckStart() {
        if (this.m_NetworkCheckEnabled) {
            return true;
        }
        this.m_NetworkCheckEnabled = true;
        this.m_Loader.registerReceiver(this.m_NetworkCheckReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        return true;
    }

    public boolean networkCheckStop() {
        if (!this.m_NetworkCheckEnabled) {
            return true;
        }
        this.m_NetworkCheckEnabled = false;
        if (this.m_NetworkCheckReceiver != null) {
            this.m_Loader.unregisterReceiver(this.m_NetworkCheckReceiver);
        }
        return true;
    }

    public int getBatteryLevel() {
        return this.m_BatteryLevel;
    }

    public boolean chargerIsConnected() {
        return this.m_ChargerConnected;
    }

    private void doSuspend() {
        LoaderAPI.notifySuspendResumeListeners(new SuspendResumeEvent(SuspendResumeEvent.EventType.SUSPEND));
        this.m_MediaPlayerManager.doPause();
        this.m_VideoIsPaused = Boolean.valueOf(this.m_View.videoGetStatus() == 2);
        if (!this.m_VideoIsPaused.booleanValue()) {
            this.m_View.videoPause();
        }
        this.m_SoundPlayer.pause();
        if (this.m_SoundRecord != null) {
            this.m_SoundRecord.stop();
        }
        if (this.m_BatteryLevelReceiverRegistered.booleanValue()) {
            if (this.m_BatteryLevelReceiver != null) {
                this.m_Loader.unregisterReceiver(this.m_BatteryLevelReceiver);
            }
            this.m_BatteryLevelReceiverRegistered = false;
        }
        if (this.m_NetworkCheckEnabled && this.m_NetworkCheckReceiver != null) {
            this.m_Loader.unregisterReceiver(this.m_NetworkCheckReceiver);
        }
        if (this.m_Location != null) {
            this.m_Location.locationStop();
        }
        if (this.m_Accelerometer != null) {
            this.m_SensorManager.unregisterListener(this, this.m_Accelerometer);
        }
        if (this.m_Compass != null) {
            this.m_SensorManager.unregisterListener(this, this.m_Compass);
            this.m_SensorManager.unregisterListener(this, this.m_Orientation);
        }
        this.m_AppDoingInitTerm++;
        this.m_View.glTerm();
        this.m_AppDoingInitTerm--;
    }

    private void doResume() {
        this.m_AppDoingInitTerm++;
        waitForView();
        this.m_UseGL = this.m_View.glInit(0);
        this.m_AppDoingInitTerm--;
        if (this.m_Accelerometer != null) {
            this.m_SensorManager.registerListener(this, this.m_Accelerometer, 1);
        }
        if (this.m_Compass != null) {
            this.m_SensorManager.registerListener(this, this.m_Orientation, 1);
            this.m_SensorManager.registerListener(this, this.m_Compass, 1);
        }
        if (this.m_Location != null) {
            this.m_Location.locationStart(this.m_Loader);
        }
        if (!this.m_BatteryLevelReceiverRegistered.booleanValue()) {
            this.m_Loader.registerReceiver(this.m_BatteryLevelReceiver, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            this.m_BatteryLevelReceiverRegistered = true;
        }
        if (this.m_NetworkCheckEnabled) {
            this.m_Loader.registerReceiver(this.m_NetworkCheckReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }
        this.m_SoundPlayer.resume();
        this.m_MediaPlayerManager.doResume();
        if (!this.m_VideoIsPaused.booleanValue()) {
            this.m_View.videoResume();
        }
        if (this.m_SoundRecord != null) {
            this.m_SoundRecord.start(-1);
        }
        this.m_ResumeInProgress = false;
        LoaderAPI.notifySuspendResumeListeners(new SuspendResumeEvent(SuspendResumeEvent.EventType.RESUME));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        setViewNative(r2.m_View);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x001e, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void waitForView() {
        /*
            r2 = this;
        L_0x0000:
            com.ideaworks3d.marmalade.LoaderView r0 = r2.m_View
            monitor-enter(r0)
            java.lang.Boolean r1 = r2.m_Terminating     // Catch:{ all -> 0x0015 }
            boolean r1 = r1.booleanValue()     // Catch:{ all -> 0x0015 }
            if (r1 != 0) goto L_0x0018
            com.ideaworks3d.marmalade.LoaderView r1 = r2.m_View     // Catch:{ all -> 0x0015 }
            boolean r1 = r1.waitForSurface()     // Catch:{ all -> 0x0015 }
            if (r1 != 0) goto L_0x0018
            monitor-exit(r0)     // Catch:{ all -> 0x0015 }
            goto L_0x0000
        L_0x0015:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0015 }
            throw r1
        L_0x0018:
            com.ideaworks3d.marmalade.LoaderView r1 = r2.m_View     // Catch:{ all -> 0x0015 }
            r2.setViewNative(r1)     // Catch:{ all -> 0x0015 }
            monitor-exit(r0)     // Catch:{ all -> 0x0015 }
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ideaworks3d.marmalade.LoaderThread.waitForView():void");
    }

    private void glInit(int i) {
        if (!this.m_UseGL) {
            this.m_UseGL = true;
            this.m_AppDoingInitTerm++;
            if (!this.m_Paused.booleanValue() && !this.m_Terminating.booleanValue()) {
                synchronized (this.m_CreateView) {
                    this.m_Handler.post(this.m_CreateView);
                    try {
                        this.m_CreateView.wait();
                    } catch (InterruptedException e) {
                    }
                }
                waitForView();
            }
            this.m_View.glInit(i);
            this.m_AppDoingInitTerm--;
        }
    }

    private void glTerm() {
        if (this.m_UseGL) {
            this.m_AppDoingInitTerm++;
            this.m_View.glTerm();
            this.m_UseGL = false;
            if (!this.m_Paused.booleanValue() && !this.m_Terminating.booleanValue()) {
                synchronized (this.m_CreateView) {
                    this.m_Handler.post(this.m_CreateView);
                    try {
                        this.m_CreateView.wait();
                    } catch (InterruptedException e) {
                    }
                }
            }
            if (!this.m_Paused.booleanValue()) {
                waitForView();
            }
            this.m_AppDoingInitTerm--;
        }
    }

    private void glReInit() {
        if (this.m_UseGL) {
            this.m_AppDoingInitTerm++;
            this.m_View.glReInit();
            this.m_AppDoingInitTerm--;
        }
    }

    public void run() {
        waitForView();
        this.m_Started = true;
        runNative(this.m_FileRoot.getAbsolutePath(), this.m_Loader.getPackageResourcePath());
        shuttingDown(false);
    }

    private String getPrivateExternalDir() {
        File file;
        if (!Environment.getExternalStorageState().equals("mounted")) {
            return null;
        }
        if (Build.VERSION.SDK_INT >= 8) {
            try {
                file = (File) Context.class.getMethod("getExternalFilesDir", String.class).invoke(this.m_Loader, null);
            } catch (Exception e) {
                file = null;
            }
        } else {
            file = null;
        }
        if (file != null) {
            return file.getAbsolutePath();
        }
        return null;
    }

    private String getRstDir() {
        if (!Environment.getExternalStorageState().equals("mounted")) {
            return null;
        }
        File externalStorageDirectory = Environment.getExternalStorageDirectory();
        if (externalStorageDirectory != null) {
            return externalStorageDirectory.getAbsolutePath();
        }
        return null;
    }

    private String getCacheDir() {
        File cacheDir = this.m_Loader.getCacheDir();
        if (cacheDir != null) {
            return cacheDir.getAbsolutePath();
        }
        return null;
    }

    private String getTmpDir() {
        if (Build.VERSION.SDK_INT >= 8) {
            try {
                File file = (File) Context.class.getMethod("getExternalCacheDir", new Class[0]).invoke(this.m_Loader, new Object[0]);
                if (file != null) {
                    return file.getAbsolutePath();
                }
                return null;
            } catch (Exception e) {
            }
        }
        return null;
    }

    public boolean hasMultitouch() {
        PackageManager packageManager = this.m_Loader.getPackageManager();
        try {
            return ((Boolean) PackageManager.class.getMethod("hasSystemFeature", String.class).invoke(packageManager, "android.hardware.touchscreen.multitouch")).booleanValue();
        } catch (Exception e) {
            return false;
        }
    }

    private void shuttingDown(boolean z) {
        if (!this.m_Terminating.booleanValue()) {
            this.m_Started = false;
            this.m_Terminating = true;
            g_Singleton = null;
            if (z) {
                shutdownNative();
            }
            smsStop();
            accelStop();
            LoaderAPI.notifySuspendResumeListeners(new SuspendResumeEvent(SuspendResumeEvent.EventType.SHUTDOWN));
            if (!this.m_Loader.isFinishing()) {
                if (this.m_BatteryLevelReceiverRegistered.booleanValue()) {
                    if (this.m_BatteryLevelReceiver != null) {
                        this.m_Loader.unregisterReceiver(this.m_BatteryLevelReceiver);
                    }
                    this.m_BatteryLevelReceiverRegistered = false;
                }
                networkCheckStop();
                this.m_Loader.finish();
            }
        }
    }

    public void onDestroy() {
        shuttingDown(true);
        interrupt();
        try {
            join();
        } catch (InterruptedException e) {
        }
    }

    public void onLowMemory() {
        lowMemoryWarning();
    }

    public boolean locationStart() {
        if (this.m_Location != null) {
            return false;
        }
        this.m_Location = new LoaderLocation();
        return this.m_Location.locationStart(this.m_Loader);
    }

    public boolean locationStop() {
        if (this.m_Location == null) {
            return false;
        }
        boolean locationStop = this.m_Location.locationStop();
        this.m_Location = null;
        return locationStop;
    }

    public boolean locationGpsData() {
        if (this.m_Location != null) {
            return this.m_Location.locationGpsData();
        }
        return false;
    }

    public String getDeviceModel() {
        return Build.MODEL;
    }

    public String getDeviceId() {
        String deviceId = ((TelephonyManager) this.m_Loader.getSystemService("phone")).getDeviceId();
        if (deviceId != null && deviceId != "") {
            return deviceId;
        }
        String string = Settings.Secure.getString(this.m_Loader.getContentResolver(), "android_id");
        if (string != null && string != "9774d56d682e549c") {
            return string;
        }
        if (Build.VERSION.SDK_INT >= 9) {
            try {
                String str = (String) Build.class.getField("SERIAL").get(null);
                if (str != null) {
                    return str;
                }
            } catch (Exception e) {
            }
        }
        return null;
    }

    public String getDeviceIMSI() {
        return ((TelephonyManager) this.m_Loader.getSystemService("phone")).getSubscriberId();
    }

    public String getDeviceNumber() {
        return ((TelephonyManager) this.m_Loader.getSystemService("phone")).getLine1Number();
    }

    public boolean getSilentMode() {
        if (((AudioManager) this.m_Loader.getSystemService("audio")).getRingerMode() == 2) {
            return false;
        }
        return true;
    }

    public boolean launchBrowser(String str) {
        try {
            Uri parse = Uri.parse(str);
            Intent intent = new Intent();
            intent.setData(parse);
            if (!str.startsWith("vfstore")) {
                intent.setAction("android.intent.action.VIEW");
            }
            intent.addFlags(337641472);
            this.m_Loader.startActivity(intent);
            return true;
        } catch (ActivityNotFoundException e) {
            return false;
        }
    }

    public boolean sendEmail(String str, String str2, String str3) {
        try {
            String[] split = str.split(",\\s*?");
            Intent intent = new Intent();
            intent.setAction("android.intent.action.SEND");
            intent.setType("text/xml");
            intent.putExtra("android.intent.extra.EMAIL", split);
            intent.putExtra("android.intent.extra.SUBJECT", str2);
            intent.putExtra("android.intent.extra.TEXT", str3);
            intent.addFlags(337641472);
            this.m_Loader.startActivity(intent);
            return true;
        } catch (ActivityNotFoundException e) {
            return false;
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int i;
        if (this.m_TouchEvent != null) {
            try {
                this.m_TouchEvent.invoke(null, this, motionEvent);
            } catch (Exception e) {
            }
        } else {
            int x = (int) motionEvent.getX();
            int y = (int) motionEvent.getY();
            switch (motionEvent.getAction() & 255) {
                case 0:
                    i = 1;
                    break;
                case 1:
                    i = 2;
                    break;
                case 2:
                    i = 3;
                    break;
                default:
                    i = 0;
                    break;
            }
            onMotionEvent(0, i + 3, x, y);
        }
        if (this.m_OnTouchWait.intValue() > 0) {
            synchronized (this.m_OnTouchWait) {
                try {
                    this.m_OnTouchWait.wait((long) this.m_OnTouchWait.intValue());
                } catch (InterruptedException e2) {
                }
            }
        }
        return true;
    }

    public int getNetworkType() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.m_Loader.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            return -1;
        }
        return activeNetworkInfo.getType();
    }

    public int getNetworkSubType() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.m_Loader.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            return -1;
        }
        return activeNetworkInfo.getSubtype();
    }

    public boolean acquireMulticastLock() {
        if (Integer.parseInt(Build.VERSION.SDK) <= 3) {
            return false;
        }
        if (this.m_MulticastLock == null) {
            try {
                this.m_MulticastLock = new MulticastLockFacade((WifiManager) this.m_Loader.getSystemService("wifi"));
            } catch (Exception e) {
                return false;
            }
        }
        if (this.m_MulticastLock == null) {
            return false;
        }
        this.m_MulticastLock.acquire();
        return this.m_MulticastLock.isHeld();
    }

    public boolean releaseMulticastLock() {
        if (this.m_MulticastLock == null) {
            return false;
        }
        this.m_MulticastLock.release();
        this.m_MulticastLock = null;
        return true;
    }

    class MulticastLockFacade {
        private Object m_multiCastLockReal = null;

        public MulticastLockFacade(WifiManager wifiManager) throws Exception {
            try {
                this.m_multiCastLockReal = wifiManager.getClass().getMethod("createMulticastLock", String.class).invoke(wifiManager, "Marmalade");
            } catch (Exception e) {
                throw e;
            }
        }

        public boolean isHeld() {
            if (this.m_multiCastLockReal == null) {
                return false;
            }
            try {
                return ((Boolean) this.m_multiCastLockReal.getClass().getMethod("isHeld", null).invoke(this.m_multiCastLockReal, null)).booleanValue();
            } catch (Exception e) {
                return false;
            }
        }

        public void release() {
            if (this.m_multiCastLockReal != null) {
                try {
                    this.m_multiCastLockReal.getClass().getMethod("release", null).invoke(this.m_multiCastLockReal, null);
                } catch (Exception e) {
                }
            }
        }

        public void acquire() {
            if (this.m_multiCastLockReal != null) {
                try {
                    this.m_multiCastLockReal.getClass().getMethod("acquire", null).invoke(this.m_multiCastLockReal, null);
                } catch (Exception e) {
                }
            }
        }
    }
}
