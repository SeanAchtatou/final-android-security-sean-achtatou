package com.ideaworks3d.marmalade;

import android.app.ProgressDialog;
import android.os.Handler;
import android.os.Message;

/* compiled from: LoaderActivity */
class ProgressDialogHandler extends Handler {
    public static final int PROGRESS_FINISH = 1;
    public static final int PROGRESS_START = 0;
    private ProgressDialog progressDialog;

    ProgressDialogHandler() {
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 0:
                this.progressDialog = ProgressDialog.show(LoaderActivity.m_Activity, "", "", true, false);
                return;
            case 1:
                if (this.progressDialog != null && this.progressDialog.isShowing()) {
                    this.progressDialog.dismiss();
                    this.progressDialog = null;
                    return;
                }
                return;
            default:
                return;
        }
    }
}
