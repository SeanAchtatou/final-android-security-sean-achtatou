package com.ideaworks3d.marmalade;

import android.media.AudioRecord;

public class SoundRecord implements AudioRecord.OnRecordPositionUpdateListener {
    private AudioRecord m_AudioRecord = null;
    private int m_BufSize;
    private short[] m_Buffer;
    private int m_Frequency;
    private int m_Period;

    private native void recordAudio(short[] sArr, int i, int i2);

    public int start(int i) {
        if (this.m_AudioRecord != null) {
            return 0;
        }
        if (i != -1) {
            this.m_Frequency = i;
        }
        this.m_BufSize = AudioRecord.getMinBufferSize(this.m_Frequency, 2, 2);
        if (this.m_BufSize == -2) {
            this.m_Frequency = 8000;
            this.m_BufSize = AudioRecord.getMinBufferSize(this.m_Frequency, 2, 2);
            if (this.m_BufSize == -2) {
                return 0;
            }
        }
        if (this.m_BufSize <= 4096) {
            this.m_BufSize *= 2;
        }
        this.m_Period = this.m_BufSize / 4;
        this.m_Buffer = new short[this.m_BufSize];
        this.m_AudioRecord = new AudioRecord(1, this.m_Frequency, 2, 2, this.m_BufSize);
        this.m_AudioRecord.setRecordPositionUpdateListener(this);
        this.m_AudioRecord.setPositionNotificationPeriod(this.m_Period);
        try {
            this.m_AudioRecord.startRecording();
            recordAudio(this.m_Buffer, this.m_AudioRecord.read(this.m_Buffer, 0, this.m_Period), this.m_Frequency);
            return this.m_Frequency;
        } catch (IllegalStateException e) {
            return 0;
        }
    }

    public void onMarkerReached(AudioRecord audioRecord) {
    }

    public void onPeriodicNotification(AudioRecord audioRecord) {
        recordAudio(this.m_Buffer, this.m_AudioRecord.read(this.m_Buffer, 0, this.m_Period), this.m_Frequency);
    }

    public int stop() {
        if (this.m_AudioRecord == null) {
            return 1;
        }
        this.m_AudioRecord.setRecordPositionUpdateListener(null);
        this.m_AudioRecord.stop();
        this.m_AudioRecord.release();
        this.m_AudioRecord = null;
        return 0;
    }
}
