package com.ideaworks3d.marmalade;

import android.graphics.Bitmap;
import android.os.Handler;
import android.view.View;

public class s3eTest {
    /* access modifiers changed from: private */
    public Handler m_Handler;
    Thread thread;

    public s3eTest() {
        LoaderActivity.m_Activity.LoaderThread().runOnOSThread(new Runnable() {
            public void run() {
                Handler unused = s3eTest.this.m_Handler = new Handler();
            }
        });
    }

    public void PostSuspend() {
        LoaderAPI.trace("PostSuspend");
        this.m_Handler.post(new Runnable() {
            public void run() {
                LoaderActivity.m_Activity.onPause();
            }
        });
    }

    public void PostResume() {
        LoaderAPI.trace("PostResume");
        this.m_Handler.post(new Runnable() {
            public void run() {
                LoaderActivity.m_Activity.onResume();
            }
        });
    }

    public void PostSetFocus(final boolean z) {
        LoaderAPI.trace("PostSetFocus");
        this.m_Handler.post(new Runnable() {
            public void run() {
                LoaderActivity.m_Activity.m_View.onWindowFocusChanged(z);
            }
        });
    }

    public void PostSuspendResume(final int i, final int i2) {
        this.thread = new Thread(new Runnable() {
            public void run() {
                int i = 0;
                while (i < i2) {
                    try {
                        s3eTest.this.PostSuspend();
                        s3eTest.this.PostSetFocus(false);
                        Thread.sleep((long) i);
                        s3eTest.this.PostResume();
                        s3eTest.this.PostSetFocus(true);
                        i++;
                    } catch (InterruptedException e) {
                        return;
                    } finally {
                        s3eTest.this.PostResume();
                        s3eTest.this.thread = null;
                    }
                }
            }
        });
        this.thread.start();
    }

    public boolean QueryThreadFinished() {
        if (this.thread == null) {
            return true;
        }
        return false;
    }

    public void PostRotate(final int i, final int i2) {
        LoaderAPI.trace("PostRotate");
        this.m_Handler.post(new Runnable() {
            public void run() {
                LoaderActivity.m_Activity.m_View.surfaceChanged(null, 0, i, i2);
            }
        });
    }

    public int[] TakeScreenShot(int[] iArr) {
        LoaderAPI.trace("TakeScreenShot");
        View findViewById = LoaderAPI.getActivity().getWindow().getDecorView().findViewById(16908290);
        findViewById.setDrawingCacheEnabled(true);
        Bitmap createBitmap = Bitmap.createBitmap(findViewById.getDrawingCache());
        int width = createBitmap.getWidth();
        int height = createBitmap.getHeight();
        int[] iArr2 = new int[(width * height)];
        createBitmap.getPixels(iArr2, 0, width, 0, 0, width, height);
        iArr[0] = width;
        iArr[1] = height;
        findViewById.setDrawingCacheEnabled(false);
        return iArr2;
    }
}
