package com.ideaworks3d.marmalade;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import com.ideaworks3d.marmalade.LoaderActivity;

public class CursorLoaderHelper {
    public static boolean getCursor(Uri uri, String[] strArr, final LoaderActivity.CursorCompleteListener cursorCompleteListener) {
        try {
            CursorLoader cursorLoader = new CursorLoader(LoaderAPI.getActivity(), uri, strArr, null, null, null);
            cursorLoader.registerListener(0, new Loader.OnLoadCompleteListener<Cursor>() {
                public /* bridge */ /* synthetic */ void onLoadComplete(Loader loader, Object obj) {
                    onLoadComplete((Loader<Cursor>) loader, (Cursor) obj);
                }

                public void onLoadComplete(Loader<Cursor> loader, Cursor cursor) {
                    LoaderAPI.trace("CursorLoader onLoadComplete");
                    cursorCompleteListener.cursorLoadComplete(cursor);
                }
            });
            cursorLoader.startLoading();
        } catch (Exception e) {
            LoaderAPI.trace("Could not create cursorLoader " + e);
            LoaderAPI.trace(e.getMessage());
        }
        return false;
    }
}
