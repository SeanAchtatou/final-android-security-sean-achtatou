package com.ideaworks3d.marmalade;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import java.io.File;
import java.io.FileNotFoundException;

public class VFSProvider extends ContentProvider {
    public static final Uri ASSET_URI = Uri.parse("content://zzzze38b27a1f1145f4ff2a7d14685f60a14.VFSProvider");
    private static final String AUTHORITY = "zzzze38b27a1f1145f4ff2a7d14685f60a14.VFSProvider";
    private static final String CONTENT_PREFIX = "content://";
    private static final String EXP_PATH = "/Android/obb/";

    public boolean onCreate() {
        return true;
    }

    public AssetFileDescriptor openAssetFile(Uri uri, String str) throws FileNotFoundException {
        String encodedPath = uri.getEncodedPath();
        if (encodedPath.startsWith("/")) {
            encodedPath = encodedPath.substring(1);
        }
        return getAssetFileDescriptor(encodedPath);
    }

    public ParcelFileDescriptor openFile(Uri uri, String str) throws FileNotFoundException {
        AssetFileDescriptor openAssetFile = openAssetFile(uri, str);
        if (openAssetFile != null) {
            return openAssetFile.getParcelFileDescriptor();
        }
        return null;
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        return null;
    }

    public AssetFileDescriptor getAssetFileDescriptor(String str) {
        String[] split = str.split("/");
        if (split.length < 3) {
            return null;
        }
        try {
            long parseLong = Long.parseLong(split[split.length - 2]);
            long parseLong2 = Long.parseLong(split[split.length - 1]);
            String str2 = split[0];
            for (int i = 1; i < split.length - 2; i++) {
                str2 = str2 + "/" + split[i];
            }
            return new AssetFileDescriptor(ParcelFileDescriptor.open(new File(str2), 268435456), parseLong, parseLong2);
        } catch (NumberFormatException e) {
            return null;
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        return 0;
    }

    public int delete(Uri uri, String str, String[] strArr) {
        return 0;
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        return null;
    }

    public String getType(Uri uri) {
        return "vnd.android.cursor.item/asset";
    }
}
