package com.ideaworks3d.marmalade;

import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.view.KeyEvent;
import android.view.inputmethod.InputMethodManager;
import com.ideaworks3d.marmalade.SuspendResumeEvent;

public class LoaderKeyboard implements SuspendResumeListener {
    /* access modifiers changed from: private */
    public SoftInputReceiver m_Receiver;
    /* access modifiers changed from: private */
    public LoaderView m_View;
    /* access modifiers changed from: private */
    public boolean m_onScreenKeyboard = false;
    /* access modifiers changed from: private */
    public boolean m_pausing = false;

    private native boolean onKeyEventNative(int i, int i2, int i3);

    private native void setCharInputEnabledNative(boolean z);

    private class SoftInputReceiver extends ResultReceiver {
        public SoftInputReceiver(Handler handler) {
            super(handler);
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        /* access modifiers changed from: protected */
        public void onReceiveResult(int i, Bundle bundle) {
            boolean z;
            switch (i) {
                case 0:
                case 2:
                    if (!LoaderKeyboard.this.m_onScreenKeyboard && !LoaderKeyboard.this.m_pausing) {
                        z = true;
                        break;
                    }
                    z = false;
                    break;
                case 1:
                case 3:
                    if (LoaderKeyboard.this.m_onScreenKeyboard && !LoaderKeyboard.this.m_pausing) {
                        z = true;
                        break;
                    }
                    z = false;
                    break;
                default:
                    z = false;
                    break;
            }
            if (z) {
                ((InputMethodManager) LoaderActivity.m_Activity.getSystemService("input_method")).toggleSoftInput(0, 0);
            }
        }
    }

    public LoaderKeyboard(LoaderView loaderView) {
        this.m_View = loaderView;
        this.m_Receiver = new SoftInputReceiver(this.m_View.m_Handler);
        LoaderAPI.addSuspendResumeListener(this);
    }

    public void onSuspendResumeEvent(SuspendResumeEvent suspendResumeEvent) {
        if (suspendResumeEvent.eventType == SuspendResumeEvent.EventType.SUSPEND && this.m_onScreenKeyboard) {
            this.m_pausing = true;
            setShowOnScreenKeyboard(false);
            this.m_onScreenKeyboard = true;
        }
        if (suspendResumeEvent.eventType == SuspendResumeEvent.EventType.RESUME) {
            this.m_pausing = false;
            if (this.m_onScreenKeyboard) {
                setShowOnScreenKeyboard(this.m_onScreenKeyboard);
            }
        }
    }

    public boolean onKeyEvent(int i, int i2, KeyEvent keyEvent) {
        return onKeyEventNative(i, keyEvent.getUnicodeChar(), i2);
    }

    public boolean onKeyPreIme(int i, KeyEvent keyEvent) {
        if (i != 4 || !this.m_onScreenKeyboard) {
            return false;
        }
        setCharInputEnabledNative(false);
        setShowOnScreenKeyboard(false);
        return true;
    }

    public void hardKeyboardConfigurationChanged(boolean z) {
        if (!z && this.m_onScreenKeyboard) {
            setShowOnScreenKeyboard(true);
        }
    }

    public void setShowOnScreenKeyboard(final boolean z) {
        this.m_onScreenKeyboard = z;
        final InputMethodManager inputMethodManager = (InputMethodManager) LoaderActivity.m_Activity.getSystemService("input_method");
        LoaderActivity.m_Activity.LoaderThread().runOnOSThread(new Runnable() {
            public void run() {
                if (z) {
                    LoaderKeyboard.this.m_View.requestFocus();
                    if (!inputMethodManager.showSoftInput(LoaderKeyboard.this.m_View, 2, LoaderKeyboard.this.m_Receiver)) {
                        inputMethodManager.toggleSoftInput(0, 0);
                    }
                } else if (!inputMethodManager.hideSoftInputFromWindow(LoaderKeyboard.this.m_View.getWindowToken(), 0, LoaderKeyboard.this.m_Receiver)) {
                    inputMethodManager.toggleSoftInput(0, 0);
                }
            }
        });
    }

    public boolean getShowOnScreenKeyboard() {
        return this.m_onScreenKeyboard;
    }

    public int getKeyboardInfo() {
        Configuration configuration = this.m_View.getResources().getConfiguration();
        int i = 0;
        if (configuration.keyboard == 2 && configuration.hardKeyboardHidden != 2) {
            i = 0 | 1;
        }
        if (configuration.keyboard == 3 && configuration.hardKeyboardHidden != 2) {
            i |= 2;
        }
        if (configuration.navigation >= 2) {
            try {
                if (((Integer) configuration.getClass().getField("navigationHidden").get(configuration)).intValue() == 2) {
                    return i;
                }
            } catch (Exception e) {
            }
            if (!Build.MODEL.equals("Zeus") || configuration.hardKeyboardHidden != 2) {
                return i | 4;
            }
        }
        return i;
    }
}
