package com.ideaworks3d.marmalade.s3eAndroidMarketBilling;

import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.android.vending.billing.IMarketBillingService;
import com.ideaworks3d.marmalade.LoaderActivity;
import com.ideaworks3d.marmalade.s3eAndroidGooglePlayBilling.util.IabHelper;
import java.security.SecureRandom;
import java.util.HashSet;

class s3eAndroidMarketBilling extends Service implements ServiceConnection {
    public static final int S3E_ANDROIDMARKETBILLING_PURCHASE_STATE_CANCELLED = 1;
    public static final int S3E_ANDROIDMARKETBILLING_PURCHASE_STATE_PURCHASED = 0;
    public static final int S3E_ANDROIDMARKETBILLING_PURCHASE_STATE_REFUNDED = 2;
    public static final int S3E_ANDROIDMARKETBILLING_RESULT_BILLING_UNAVAILABLE = 3;
    public static final int S3E_ANDROIDMARKETBILLING_RESULT_DEVELOPER_ERROR = 5;
    public static final int S3E_ANDROIDMARKETBILLING_RESULT_ERROR = 6;
    public static final int S3E_ANDROIDMARKETBILLING_RESULT_ITEM_UNAVAILABLE = 4;
    public static final int S3E_ANDROIDMARKETBILLING_RESULT_OK = 0;
    public static final int S3E_ANDROIDMARKETBILLING_RESULT_SERVICE_UNAVAILABLE = 2;
    public static final int S3E_ANDROIDMARKETBILLING_RESULT_USER_CANCELED = 1;
    public static final String TAG = "s3eAndroidMarketBilling";
    private static final HashSet<Long> m_Nonces = new HashSet<>();
    private static final SecureRandom m_SecureRandom = new SecureRandom();
    static String m_pubKey;
    public IMarketBillingService m_Service;

    /* access modifiers changed from: package-private */
    public Bundle makeRequestBundle(String str) {
        Bundle bundle = new Bundle();
        bundle.putString("BILLING_REQUEST", str);
        bundle.putInt("API_VERSION", 1);
        bundle.putString("PACKAGE_NAME", LoaderActivity.m_Activity.getPackageName());
        return bundle;
    }

    static void putNonce(Bundle bundle) {
        long nextLong;
        do {
            nextLong = m_SecureRandom.nextLong();
        } while (m_Nonces.contains(Long.valueOf(nextLong)));
        m_Nonces.add(Long.valueOf(nextLong));
        bundle.putLong("NONCE", nextLong);
    }

    static boolean checkNonce(long j) {
        boolean contains = m_Nonces.contains(Long.valueOf(j));
        m_Nonces.remove(Long.valueOf(j));
        return contains;
    }

    /* access modifiers changed from: protected */
    public int extractReqId(Bundle bundle, long[] jArr) {
        Log.i("s3eAndroidMarketBilling", "Returning " + bundle.getInt(IabHelper.RESPONSE_CODE) + ", outReq=" + bundle.getLong("REQUEST_ID"));
        jArr[0] = bundle.getLong("REQUEST_ID");
        return bundle.getInt(IabHelper.RESPONSE_CODE);
    }

    public s3eAndroidMarketBilling() {
        attachBaseContext(LoaderActivity.m_Activity);
        bindService(new Intent("com.android.vending.billing.MarketBillingService.BIND"), this, 1);
    }

    public void s3eAndroidMarketBillingUnbind() {
        try {
            unbindService(this);
        } catch (IllegalArgumentException e) {
        }
    }

    public int s3eAndroidMarketBillingIsSupported() {
        if (this.m_Service == null) {
            Log.i("s3eAndroidMarketBilling", "s3eAndroidMarketBillingIsSupported service is not registered");
            return 2;
        }
        try {
            return this.m_Service.sendBillingRequest(makeRequestBundle("CHECK_BILLING_SUPPORTED")).getInt(IabHelper.RESPONSE_CODE);
        } catch (RemoteException e) {
            Log.i("s3eAndroidMarketBilling", "s3eAndroidMarketBillingIsSupported Remote Exception");
            return 6;
        }
    }

    public int s3eAndroidMarketBillingRequestPurchase(String str, String str2, long[] jArr) {
        Log.i("s3eAndroidMarketBilling", "s3eAndroidMarketBillingRequestPurchase(" + str + "," + str2 + ")");
        if (this.m_Service == null) {
            return 2;
        }
        try {
            Bundle makeRequestBundle = makeRequestBundle("REQUEST_PURCHASE");
            makeRequestBundle.putString("ITEM_ID", str);
            makeRequestBundle.putString("DEVELOPER_PAYLOAD", str2);
            Bundle sendBillingRequest = this.m_Service.sendBillingRequest(makeRequestBundle);
            LoaderActivity.m_Activity.startIntentSender(((PendingIntent) sendBillingRequest.getParcelable("PURCHASE_INTENT")).getIntentSender(), new Intent(), 0, 0, 0);
            return extractReqId(sendBillingRequest, jArr);
        } catch (IntentSender.SendIntentException e) {
            return 6;
        } catch (RemoteException e2) {
            return 6;
        }
    }

    public int s3eAndroidMarketBillingGetPurchaseInformation(String[] strArr, long[] jArr) {
        Log.i("s3eAndroidMarketBilling", "s3eAndroidMarketBillingGetPurchaseInformation(" + strArr + "={" + strArr[0] + "...})");
        if (this.m_Service == null) {
            return 2;
        }
        try {
            Bundle makeRequestBundle = makeRequestBundle("GET_PURCHASE_INFORMATION");
            putNonce(makeRequestBundle);
            makeRequestBundle.putStringArray("NOTIFY_IDS", strArr);
            return extractReqId(this.m_Service.sendBillingRequest(makeRequestBundle), jArr);
        } catch (RemoteException e) {
            return 6;
        }
    }

    public int s3eAndroidMarketBillingConfirmNotifications(String[] strArr, long[] jArr) {
        Log.i("s3eAndroidMarketBilling", "s3eAndroidMarketBillingConfirmNotifications(" + strArr + "={" + strArr[0] + "...})");
        if (this.m_Service == null) {
            return 2;
        }
        try {
            Bundle makeRequestBundle = makeRequestBundle("CONFIRM_NOTIFICATIONS");
            makeRequestBundle.putStringArray("NOTIFY_IDS", strArr);
            return extractReqId(this.m_Service.sendBillingRequest(makeRequestBundle), jArr);
        } catch (RemoteException e) {
            return 6;
        }
    }

    public int s3eAndroidMarketBillingRestoreTransactions(long[] jArr) {
        Log.i("s3eAndroidMarketBilling", "s3eAndroidMarketBillingRestoreTransactions");
        if (this.m_Service == null) {
            return 2;
        }
        try {
            Bundle makeRequestBundle = makeRequestBundle("RESTORE_TRANSACTIONS");
            putNonce(makeRequestBundle);
            return extractReqId(this.m_Service.sendBillingRequest(makeRequestBundle), jArr);
        } catch (RemoteException e) {
            return 6;
        }
    }

    public void s3eAndroidMarketBillingSetPublicKey(String str) {
        Log.i("s3eAndroidMarketBilling", "s3eAndroidMarketBillingSetPublicKey(got key=" + (str != null) + ")");
        m_pubKey = str;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        this.m_Service = IMarketBillingService.Stub.asInterface(iBinder);
    }

    public void onServiceDisconnected(ComponentName componentName) {
        this.m_Service = null;
    }
}
