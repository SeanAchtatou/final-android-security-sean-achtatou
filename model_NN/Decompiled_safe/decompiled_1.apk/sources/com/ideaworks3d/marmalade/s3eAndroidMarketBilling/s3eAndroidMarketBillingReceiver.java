package com.ideaworks3d.marmalade.s3eAndroidMarketBilling;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class s3eAndroidMarketBillingReceiver extends BroadcastReceiver {
    public static final int S3E_ANDROIDMARKETBILLING_SECURITY_INVALID_NONCE = 32;
    public static final int S3E_ANDROIDMARKETBILLING_SECURITY_INVALID_SIGNATURE = 2;
    public static final int S3E_ANDROIDMARKETBILLING_SECURITY_NO_NONCE = 16;
    public static final int S3E_ANDROIDMARKETBILLING_SECURITY_NO_SIGNATURE = 1;
    public static final int S3E_ANDROIDMARKETBILLING_SECURITY_UNCHECKED_SIGNATURE = 4;
    public static final int S3E_ANDROIDMARKETBILLING_SECURITY_VALID = 0;
    public static final String TAG = "s3eAndroidMarketBilling";

    private static native void native_onInAppNotifyCallback(String str);

    private static native void native_onPurchaseStateChangedCallback(Order[] orderArr, int i);

    private static native void native_onResponseCodeCallback(long j, int i);

    public void onReceive(Context context, Intent intent) {
        Log.i("s3eAndroidMarketBilling", "Got intent " + intent);
        String action = intent.getAction();
        if (action.equals("com.android.vending.billing.RESPONSE_CODE")) {
            onResponseCode(intent);
        } else if (action.equals("com.android.vending.billing.IN_APP_NOTIFY")) {
            onInAppNotify(intent);
        } else if (action.equals("com.android.vending.billing.PURCHASE_STATE_CHANGED")) {
            onPurchaseStateChanged(intent);
        }
    }

    public void onResponseCode(Intent intent) {
        Bundle extras = intent.getExtras();
        long j = extras.getLong("request_id");
        int i = extras.getInt("response_code");
        Log.i("s3eAndroidMarketBilling", "Got response code intent: reqId=" + j + ", respCode=" + i);
        Log.i("s3eAndroidMarketBilling", "Enqueuing native callback...");
        try {
            native_onResponseCodeCallback(j, i);
        } catch (UnsatisfiedLinkError e) {
            Log.i("s3eAndroidMarketBilling", "Failed to send Responce Code callback");
            Log.i("s3eAndroidMarketBilling", "Application is either not running or s3eAndroidMarketBilling extension was not initialised");
        }
    }

    public void onInAppNotify(Intent intent) {
        String string = intent.getExtras().getString("notification_id");
        Log.i("s3eAndroidMarketBilling", "Got In-App Notify: ID=" + string);
        Log.i("s3eAndroidMarketBilling", "Enqueuing native callback...");
        try {
            native_onInAppNotifyCallback(string);
        } catch (UnsatisfiedLinkError e) {
            Log.i("s3eAndroidMarketBilling", "Failed to send App Notify callback");
            Log.i("s3eAndroidMarketBilling", "Application is either not running or s3eAndroidMarketBilling extension was not initialised");
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0062 A[Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0093 A[Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0152 A[SYNTHETIC, Splitter:B:31:0x0152] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onPurchaseStateChanged(android.content.Intent r11) {
        /*
            r10 = this;
            r5 = 0
            java.lang.String r8 = "s3eAndroidMarketBilling"
            android.os.Bundle r0 = r11.getExtras()
            java.lang.String r1 = "inapp_signed_data"
            java.lang.String r1 = r0.getString(r1)
            java.lang.String r2 = "inapp_signature"
            java.lang.String r0 = r0.getString(r2)
            java.lang.String r2 = "s3eAndroidMarketBilling"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Got purchase state change: JSON="
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r1)
            java.lang.String r3 = "\nSignature:"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r2 = r2.toString()
            android.util.Log.i(r8, r2)
            if (r1 != 0) goto L_0x003f
            java.lang.String r0 = "s3eAndroidMarketBilling"
            java.lang.String r0 = "Discarded"
            android.util.Log.i(r8, r0)
        L_0x003e:
            return
        L_0x003f:
            if (r0 == 0) goto L_0x0049
            java.lang.String r2 = ""
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x00d9
        L_0x0049:
            r0 = r5 | 1
        L_0x004b:
            org.json.JSONTokener r2 = new org.json.JSONTokener     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            r2.<init>(r1)     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            java.lang.Object r10 = r2.nextValue()     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            org.json.JSONObject r10 = (org.json.JSONObject) r10     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            java.lang.String r1 = "nonce"
            long r1 = r10.optLong(r1)     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            r3 = 0
            int r3 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r3 != 0) goto L_0x0152
            r0 = r0 | 16
        L_0x0064:
            java.lang.String r1 = "s3eAndroidMarketBilling"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            r2.<init>()     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            java.lang.String r3 = "Security issues: 0x"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            java.lang.String r3 = java.lang.Integer.toHexString(r0)     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            java.lang.String r2 = r2.toString()     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            android.util.Log.i(r1, r2)     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            java.lang.String r1 = "orders"
            org.json.JSONArray r1 = r10.optJSONArray(r1)     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            int r2 = r1.length()     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            com.ideaworks3d.marmalade.s3eAndroidMarketBilling.s3eAndroidMarketBillingReceiver$Order[] r2 = new com.ideaworks3d.marmalade.s3eAndroidMarketBilling.s3eAndroidMarketBillingReceiver.Order[r2]     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            r3 = r5
        L_0x008d:
            int r4 = r1.length()     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            if (r3 >= r4) goto L_0x015c
            org.json.JSONObject r4 = r1.optJSONObject(r3)     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            com.ideaworks3d.marmalade.s3eAndroidMarketBilling.s3eAndroidMarketBillingReceiver$Order r5 = new com.ideaworks3d.marmalade.s3eAndroidMarketBilling.s3eAndroidMarketBillingReceiver$Order     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            r5.<init>()     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            java.lang.String r6 = "notificationId"
            java.lang.String r6 = r4.optString(r6)     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            r5.m_NotificationID = r6     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            java.lang.String r6 = "orderId"
            java.lang.String r6 = r4.optString(r6)     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            r5.m_OrderID = r6     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            java.lang.String r6 = "packageName"
            java.lang.String r6 = r4.optString(r6)     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            r5.m_PackageName = r6     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            java.lang.String r6 = "productId"
            java.lang.String r6 = r4.optString(r6)     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            r5.m_ProductID = r6     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            java.lang.String r6 = "purchaseTime"
            long r6 = r4.optLong(r6)     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            r5.m_PurchaseTime = r6     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            java.lang.String r6 = "purchaseState"
            int r6 = r4.optInt(r6)     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            r5.m_PurchaseState = r6     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            java.lang.String r6 = "developerPayload"
            java.lang.String r4 = r4.optString(r6)     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            r5.m_DeveloperPayload = r4     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            r2[r3] = r5     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            int r3 = r3 + 1
            goto L_0x008d
        L_0x00d9:
            java.lang.String r2 = com.ideaworks3d.marmalade.s3eAndroidMarketBilling.s3eAndroidMarketBilling.m_pubKey
            if (r2 != 0) goto L_0x00e8
            java.lang.String r0 = "s3eAndroidMarketBilling"
            java.lang.String r0 = "No public key supplied. Won't verify signature."
            android.util.Log.i(r8, r0)
            r0 = r5 | 4
            goto L_0x004b
        L_0x00e8:
            java.lang.String r2 = "s3eAndroidMarketBilling"
            java.lang.String r2 = "Verifying signature..."
            android.util.Log.i(r8, r2)
            java.lang.String r2 = com.ideaworks3d.marmalade.s3eAndroidMarketBilling.s3eAndroidMarketBilling.m_pubKey
            byte[] r2 = android.util.Base64.decode(r2, r5)
            java.lang.String r3 = "RSA"
            java.security.KeyFactory r3 = java.security.KeyFactory.getInstance(r3)     // Catch:{ GeneralSecurityException -> 0x0181 }
            java.security.spec.X509EncodedKeySpec r4 = new java.security.spec.X509EncodedKeySpec     // Catch:{ GeneralSecurityException -> 0x0181 }
            r4.<init>(r2)     // Catch:{ GeneralSecurityException -> 0x0181 }
            java.security.PublicKey r2 = r3.generatePublic(r4)     // Catch:{ GeneralSecurityException -> 0x0181 }
            java.lang.String r3 = "SHA1withRSA"
            java.security.Signature r3 = java.security.Signature.getInstance(r3)     // Catch:{ GeneralSecurityException -> 0x0181 }
            r3.initVerify(r2)     // Catch:{ GeneralSecurityException -> 0x0181 }
            byte[] r2 = r1.getBytes()     // Catch:{ GeneralSecurityException -> 0x0181 }
            r3.update(r2)     // Catch:{ GeneralSecurityException -> 0x0181 }
            r2 = 0
            byte[] r0 = android.util.Base64.decode(r0, r2)     // Catch:{ GeneralSecurityException -> 0x0181 }
            boolean r0 = r3.verify(r0)     // Catch:{ GeneralSecurityException -> 0x0181 }
            if (r0 != 0) goto L_0x0184
            java.lang.String r0 = "s3eAndroidMarketBilling"
            java.lang.String r2 = "Signature is invalid"
            android.util.Log.i(r0, r2)     // Catch:{ GeneralSecurityException -> 0x0181 }
            r0 = r5 | 2
        L_0x0128:
            java.lang.String r2 = "s3eAndroidMarketBilling"
            java.lang.String r3 = "Signature is valid"
            android.util.Log.i(r2, r3)     // Catch:{ GeneralSecurityException -> 0x0131 }
            goto L_0x004b
        L_0x0131:
            r2 = move-exception
            r9 = r2
            r2 = r0
            r0 = r9
        L_0x0135:
            r2 = r2 | 4
            java.lang.String r3 = "s3eAndroidMarketBilling"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "GeneralSecurityException:"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r0 = r0.toString()
            android.util.Log.i(r8, r0)
            r0 = r2
            goto L_0x004b
        L_0x0152:
            boolean r1 = com.ideaworks3d.marmalade.s3eAndroidMarketBilling.s3eAndroidMarketBilling.checkNonce(r1)     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            if (r1 != 0) goto L_0x0064
            r0 = r0 | 32
            goto L_0x0064
        L_0x015c:
            native_onPurchaseStateChangedCallback(r2, r0)     // Catch:{ UnsatisfiedLinkError -> 0x0161 }
            goto L_0x003e
        L_0x0161:
            r0 = move-exception
            java.lang.String r0 = "s3eAndroidMarketBilling"
            java.lang.String r1 = "Failed to send Purchase State Changed callback"
            android.util.Log.i(r0, r1)     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            java.lang.String r0 = "s3eAndroidMarketBilling"
            java.lang.String r1 = "Application is either not running or s3eAndroidMarketBilling extension was not initialised"
            android.util.Log.i(r0, r1)     // Catch:{ JSONException -> 0x0172, ClassCastException -> 0x0175 }
            goto L_0x003e
        L_0x0172:
            r0 = move-exception
            goto L_0x003e
        L_0x0175:
            r0 = move-exception
            java.lang.String r1 = "s3eAndroidMarketBilling"
            java.lang.String r0 = r0.toString()
            android.util.Log.i(r8, r0)
            goto L_0x003e
        L_0x0181:
            r0 = move-exception
            r2 = r5
            goto L_0x0135
        L_0x0184:
            r0 = r5
            goto L_0x0128
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ideaworks3d.marmalade.s3eAndroidMarketBilling.s3eAndroidMarketBillingReceiver.onPurchaseStateChanged(android.content.Intent):void");
    }

    static class Order {
        public String m_DeveloperPayload;
        public String m_NotificationID;
        public String m_OrderID;
        public String m_PackageName;
        public String m_ProductID;
        public int m_PurchaseState;
        public long m_PurchaseTime;

        Order() {
        }
    }
}
