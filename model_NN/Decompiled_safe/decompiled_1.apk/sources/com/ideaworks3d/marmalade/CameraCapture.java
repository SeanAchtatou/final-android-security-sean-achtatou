package com.ideaworks3d.marmalade;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import com.ideaworks3d.marmalade.LoaderActivity;
import java.io.File;

/* compiled from: s3eCameraCapture */
class CameraCapture {
    static final int S3E_CAMERACAPTURE_BMP = 3;
    static final int S3E_CAMERACAPTURE_JPG = 1;
    static final int S3E_CAMERACAPTURE_PNG = 2;
    static final int S3E_CAMERACAPTURE_VIDEO = 4;
    private static int images = 0;
    /* access modifiers changed from: private */
    public static volatile boolean m_WaitingForFile;
    private static int videos = 0;

    CameraCapture() {
    }

    private int CheckCamera() {
        Camera open;
        if (Build.VERSION.SDK_INT >= 9) {
            open = Camera.open(0);
        } else {
            open = Camera.open();
        }
        if (open == null) {
            return 0;
        }
        open.release();
        return 1;
    }

    public int s3eCameraCaptureIsFormatSupported(int i) {
        if (1 != i && 4 != i) {
            return 0;
        }
        try {
            return CheckCamera();
        } catch (RuntimeException e) {
            return 0;
        }
    }

    public String s3eCameraCaptureToFile(int i) {
        File file;
        if (i == 4) {
            file = startCaptureVideo();
        } else if (i == 1) {
            file = startCaptureImage();
        } else {
            file = null;
        }
        if (file == null) {
            return null;
        }
        return "raw://" + file.getPath();
    }

    private File startCaptureVideo() {
        Intent intent = new Intent("android.media.action.VIDEO_CAPTURE");
        intent.putExtra("android.intent.extra.videoQuality", 1);
        Intent ExecuteIntent = LoaderActivity.m_Activity.ExecuteIntent(intent);
        if (ExecuteIntent == null || ExecuteIntent.getData() == null) {
            return null;
        }
        return convertUriToFile(ExecuteIntent.getData(), new String[]{"_data", "_id"});
    }

    private File startCaptureImage() {
        Uri fromFile;
        ContentValues contentValues = new ContentValues();
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        StringBuilder append = new StringBuilder().append("New image ");
        int i = videos;
        videos = i + 1;
        contentValues.put("title", append.append(i).toString());
        contentValues.put("description", "Image captured by s3eCamera");
        try {
            fromFile = LoaderActivity.m_Activity.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
        } catch (UnsupportedOperationException e) {
            StringBuilder append2 = new StringBuilder().append("/sdcard/image");
            int i2 = images;
            images = i2 + 1;
            fromFile = Uri.fromFile(new File(append2.append(i2).toString()));
        }
        intent.putExtra("output", fromFile);
        intent.putExtra("android.intent.extra.videoQuality", 1);
        LoaderAPI.trace("Executing Camera Capture intent");
        if (LoaderActivity.m_Activity.ExecuteIntent(intent) == null) {
            LoaderAPI.trace("Activity returned null, assuming cancelled");
            return null;
        }
        LoaderAPI.trace("Retrieving image url");
        return convertUriToFile(fromFile, new String[]{"_data", "_id"});
    }

    public static File convertUriToFile(Uri uri, String[] strArr) {
        final File[] fileArr = new File[1];
        m_WaitingForFile = true;
        LoaderActivity.m_Activity.getCursor(uri, strArr, new LoaderActivity.CursorCompleteListener() {
            public void cursorLoadComplete(Cursor cursor) {
                try {
                    int columnIndexOrThrow = cursor.getColumnIndexOrThrow("_data");
                    if (cursor.moveToFirst()) {
                        fileArr[0] = new File(cursor.getString(columnIndexOrThrow));
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                    LoaderAPI.trace("Finished waiting for file cursor");
                    boolean unused = CameraCapture.m_WaitingForFile = false;
                } catch (RuntimeException e) {
                    LoaderAPI.trace("Exception in convertUriToFile: " + e + " " + e.getMessage());
                    if (cursor != null) {
                        cursor.close();
                    }
                    LoaderAPI.trace("Finished waiting for file cursor");
                    boolean unused2 = CameraCapture.m_WaitingForFile = false;
                } catch (Throwable th) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    LoaderAPI.trace("Finished waiting for file cursor");
                    boolean unused3 = CameraCapture.m_WaitingForFile = false;
                    throw th;
                }
            }
        });
        while (m_WaitingForFile) {
            LoaderAPI.s3eDeviceYield(1);
        }
        return fileArr[0];
    }
}
