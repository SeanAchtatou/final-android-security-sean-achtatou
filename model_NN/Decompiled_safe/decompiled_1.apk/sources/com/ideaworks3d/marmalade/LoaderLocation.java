package com.ideaworks3d.marmalade;

import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class LoaderLocation {
    /* access modifiers changed from: private */
    public GpsStatus m_GpsStatus;
    /* access modifiers changed from: private */
    public LocationUpdateHandler m_LocationListener;
    /* access modifiers changed from: private */
    public LocationManager m_LocationManager;
    /* access modifiers changed from: private */
    public int m_LocationUpdateDistance = 2;
    /* access modifiers changed from: private */
    public int m_LocationUpdateInterval = 5000;

    private native void locationSatellite(int i, float f, float f2, int i2, float f3, boolean z);

    /* access modifiers changed from: private */
    public native void locationUpdate(int i, long j, double d, double d2, double d3, float f, float f2, float f3);

    class LocationUpdateHandler implements LocationListener, GpsStatus.Listener {
        LocationUpdateHandler() {
        }

        public void onGpsStatusChanged(int i) {
            if (LoaderLocation.this.m_LocationManager != null) {
                if (LoaderLocation.this.m_GpsStatus == null) {
                    GpsStatus unused = LoaderLocation.this.m_GpsStatus = LoaderLocation.this.m_LocationManager.getGpsStatus(null);
                } else {
                    LoaderLocation.this.m_LocationManager.getGpsStatus(LoaderLocation.this.m_GpsStatus);
                }
            }
        }

        public void onLocationChanged(Location location) {
            if (location != null) {
                LoaderLocation.this.locationUpdate(location.getProvider().equals("gps") ? 1 : 3, location.getTime(), location.getLatitude(), location.getLongitude(), location.getAltitude(), location.getAccuracy(), location.hasBearing() ? location.getBearing() : -1.0f, location.getSpeed());
            }
        }

        public void onProviderDisabled(String str) {
        }

        public void onProviderEnabled(String str) {
        }

        public void onStatusChanged(String str, int i, Bundle bundle) {
        }
    }

    public boolean locationStart(LoaderActivity loaderActivity) {
        int i;
        Location location;
        if (this.m_LocationManager != null) {
            return false;
        }
        this.m_LocationManager = (LocationManager) loaderActivity.getSystemService("location");
        if (this.m_LocationManager == null) {
            return false;
        }
        int[] iArr = {5000};
        int[] iArr2 = {2};
        if (LoaderAPI.s3eConfigGetInt("s3e", "LocUpdateInterval", iArr) == 0) {
            this.m_LocationUpdateInterval = iArr[0];
        }
        if (LoaderAPI.s3eConfigGetInt("s3e", "AndroidLocUpdateDistance", iArr2) == 0) {
            this.m_LocationUpdateDistance = iArr2[0];
        }
        loaderActivity.LoaderThread().runOnOSThread(new Runnable() {
            public void run() {
                LocationUpdateHandler unused = LoaderLocation.this.m_LocationListener = new LocationUpdateHandler();
                LoaderLocation.this.m_LocationManager.requestLocationUpdates("gps", (long) LoaderLocation.this.m_LocationUpdateInterval, (float) LoaderLocation.this.m_LocationUpdateDistance, LoaderLocation.this.m_LocationListener);
                LoaderLocation.this.m_LocationManager.addGpsStatusListener(LoaderLocation.this.m_LocationListener);
            }
        });
        Location lastKnownLocation = this.m_LocationManager.getLastKnownLocation("gps");
        if (lastKnownLocation == null) {
            i = 3;
            location = this.m_LocationManager.getLastKnownLocation("network");
        } else {
            Location location2 = lastKnownLocation;
            i = 1;
            location = location2;
        }
        if (location != null) {
            locationUpdate(i, location.getTime(), location.getLatitude(), location.getLongitude(), location.getAltitude(), location.getAccuracy(), location.hasBearing() ? location.getBearing() : -1.0f, location.getSpeed());
        }
        return true;
    }

    public boolean locationStop() {
        if (this.m_LocationManager == null) {
            return false;
        }
        this.m_LocationManager.removeGpsStatusListener(this.m_LocationListener);
        this.m_LocationManager.removeUpdates(this.m_LocationListener);
        this.m_LocationListener = null;
        this.m_LocationManager = null;
        return true;
    }

    public boolean locationGpsData() {
        int i = 0;
        if (this.m_GpsStatus == null) {
            return false;
        }
        for (GpsSatellite next : this.m_GpsStatus.getSatellites()) {
            locationSatellite(i, next.getAzimuth(), next.getElevation(), next.getPrn(), next.getSnr(), next.usedInFix());
            i++;
        }
        return true;
    }
}
