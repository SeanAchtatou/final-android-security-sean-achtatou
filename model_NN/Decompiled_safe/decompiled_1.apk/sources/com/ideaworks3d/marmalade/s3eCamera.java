package com.ideaworks3d.marmalade;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Build;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;
import com.ideaworks3d.marmalade.SuspendResumeEvent;
import java.lang.reflect.Method;
import java.util.List;

class s3eCamera implements Camera.PreviewCallback, SuspendResumeListener {
    static final int S3E_CAMERA_AUTO_FOCUS = 3;
    static final int S3E_CAMERA_AVAILABLE = 0;
    static final int S3E_CAMERA_BRIGHTNESS = 4;
    static final int S3E_CAMERA_BUSY = 2;
    static final int S3E_CAMERA_CONTRAST = 5;
    static final int S3E_CAMERA_PIXEL_TYPE_NV21 = 4097;
    static final int S3E_CAMERA_PIXEL_TYPE_RGB565 = 1058;
    static final int S3E_CAMERA_QUALITY = 6;
    static final int S3E_CAMERA_STATUS = 1;
    static final int S3E_CAMERA_TYPE = 7;
    int m_AutoFocus = 0;
    private byte[] m_Buffer;
    /* access modifiers changed from: private */
    public Camera m_Camera;
    int m_Hint = 0;
    private boolean m_NeedsRemovePreview = false;
    private Preview m_Preview;
    int m_Quality = 0;
    private Camera.Size m_Size;
    int m_Type = 0;
    private LoaderView m_View;
    Method m_addCallbackBuffer;
    Method m_setPreviewCallbackWithBuffer;
    private boolean running = false;

    private native void previewCallback(byte[] bArr, int i, int i2, int i3, int i4);

    public s3eCamera() {
        LoaderAPI.addSuspendResumeListener(this);
    }

    /* access modifiers changed from: private */
    public boolean OpenCamera(int i) {
        if (this.m_Camera != null) {
            return true;
        }
        if (Build.VERSION.SDK_INT >= 9) {
            this.m_Camera = Camera.open(i);
        } else {
            if (i != 0) {
                LoaderAPI.trace("SDK9 method [android.hardware.Camera.open(int)] was not found on the device.");
            }
            this.m_Camera = Camera.open();
        }
        return this.m_Camera != null;
    }

    /* access modifiers changed from: private */
    public void CloseCamera() {
        if (this.m_Camera != null) {
            this.m_Camera.setPreviewCallback(null);
            this.m_Camera.stopPreview();
            this.m_Camera.release();
            this.m_Camera = null;
        }
    }

    public int s3eCameraGetInt(int i) {
        if (i == 1) {
            if (this.m_Camera == null) {
                return 0;
            }
            return 1;
        } else if (i != 0) {
            return 0;
        } else {
            try {
                return ((Boolean) PackageManager.class.getMethod("hasSystemFeature", new Class[]{String.class}).invoke(LoaderActivity.m_Activity.getPackageManager(), new Object[]{"android.hardware.camera"})).booleanValue() ? 1 : 0;
            } catch (Exception e) {
                return 1;
            }
        }
    }

    public int s3eCameraSetInt(int i, int i2) {
        if (i == 7) {
            this.m_Type = i2;
        }
        if (!(i == 3 && this.m_Camera == null)) {
        }
        return 0;
    }

    /* JADX INFO: finally extract failed */
    public boolean s3eCameraIsFormatSupported(int i) {
        if (i == S3E_CAMERA_PIXEL_TYPE_NV21) {
            return true;
        }
        if (i == S3E_CAMERA_PIXEL_TYPE_RGB565) {
            try {
                if (!OpenCamera(0)) {
                    CloseCamera();
                    return false;
                }
                Camera.Parameters parameters = this.m_Camera.getParameters();
                List list = (List) parameters.getClass().getMethod("getSupportedPreviewFormats", new Class[0]).invoke(parameters, new Object[0]);
                Log.i("CameraView", "Supported formats: " + list);
                if (list.contains(4)) {
                    CloseCamera();
                    return true;
                }
                CloseCamera();
            } catch (Exception e) {
                CloseCamera();
                return false;
            } catch (Throwable th) {
                CloseCamera();
                throw th;
            }
        }
        return false;
    }

    public void onPreviewFrame(byte[] bArr, Camera camera) {
        previewCallback(bArr, camera.getParameters().getPreviewFormat(), this.m_Size.width, this.m_Size.height, LoaderActivity.m_Activity.LoaderThread().getOrientation());
        if (this.m_addCallbackBuffer != null) {
            try {
                this.m_addCallbackBuffer.invoke(this.m_Camera, this.m_Buffer);
            } catch (Exception e) {
            }
        }
    }

    public int s3eCameraStart(int i, int i2, int i3) {
        Class<Camera> cls = Camera.class;
        this.m_Hint = i;
        this.m_Quality = i3;
        this.m_View = LoaderActivity.m_Activity.m_View;
        try {
            this.m_setPreviewCallbackWithBuffer = Camera.class.getMethod("setPreviewCallbackWithBuffer", Camera.PreviewCallback.class);
            this.m_addCallbackBuffer = Camera.class.getMethod("addCallbackBuffer", byte[].class);
            if (!OpenCamera(this.m_Type)) {
                throw new RuntimeException("Can't open the camera.");
            }
            setCameraParameters();
            LoaderAPI.trace("Creating image buffer");
            this.m_Buffer = new byte[(this.m_Size.width * this.m_Size.height * 2)];
            LoaderAPI.trace("Invoking preview methods");
            this.m_addCallbackBuffer.invoke(this.m_Camera, this.m_Buffer);
            this.m_setPreviewCallbackWithBuffer.invoke(this.m_Camera, this);
            if (Build.VERSION.SDK_INT >= 11) {
                LoaderActivity.m_Activity.runOnUiThread(new Runnable() {
                    public void run() {
                        s3eCamera.this.createPreview();
                    }
                });
            }
            this.m_Camera.startPreview();
            this.running = true;
            Log.i("CameraView", "done");
            return 0;
        } catch (Exception e) {
            LoaderAPI.getStackTrace(e);
            if (this.m_setPreviewCallbackWithBuffer == null) {
                LoaderActivity.m_Activity.runOnUiThread(new Runnable() {
                    public void run() {
                        s3eCamera.this.createPreview();
                    }
                });
            }
            this.running = true;
            return 0;
        }
    }

    public int s3eCameraStop() {
        this.running = false;
        CloseCamera();
        if (this.m_setPreviewCallbackWithBuffer != null) {
            this.m_Buffer = null;
        }
        if (this.m_NeedsRemovePreview) {
            LoaderActivity.m_Activity.runOnUiThread(new Runnable() {
                public void run() {
                    s3eCamera.this.removePreview();
                }
            });
        }
        return 0;
    }

    private Camera.Size getOptimalPreviewSize(List<Camera.Size> list, int i, int i2) {
        Camera.Size size = null;
        double d = Double.MAX_VALUE;
        for (Camera.Size next : list) {
            Log.i("CameraView", "Available size: " + next.width + "x" + next.height);
            double abs = (double) (Math.abs(next.height - i2) + Math.abs(next.width - i));
            if (abs < d) {
                d = abs;
                size = next;
            }
        }
        return size;
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x00d7  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00fc  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0123  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setCameraParameters() {
        /*
            r9 = this;
            java.lang.String r7 = "x"
            java.lang.String r6 = "CameraView"
            java.lang.String r0 = "Setting camera parameters"
            com.ideaworks3d.marmalade.LoaderAPI.trace(r0)
            r1 = 0
            android.hardware.Camera r0 = r9.m_Camera
            android.hardware.Camera$Parameters r2 = r0.getParameters()
            java.lang.Class r3 = r2.getClass()     // Catch:{ Exception -> 0x00f1 }
            java.lang.String r0 = "getSupportedPreviewSizes"
            r4 = 0
            java.lang.Class[] r4 = new java.lang.Class[r4]     // Catch:{ Exception -> 0x00f1 }
            java.lang.reflect.Method r0 = r3.getMethod(r0, r4)     // Catch:{ Exception -> 0x00f1 }
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x00f1 }
            java.lang.Object r0 = r0.invoke(r2, r4)     // Catch:{ Exception -> 0x00f1 }
            java.util.List r0 = (java.util.List) r0     // Catch:{ Exception -> 0x00f1 }
            java.lang.String r1 = "getSupportedPreviewFormats"
            r4 = 0
            java.lang.Class[] r4 = new java.lang.Class[r4]     // Catch:{ Exception -> 0x0130 }
            java.lang.reflect.Method r1 = r3.getMethod(r1, r4)     // Catch:{ Exception -> 0x0130 }
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x0130 }
            java.lang.Object r1 = r1.invoke(r2, r3)     // Catch:{ Exception -> 0x0130 }
            java.util.List r1 = (java.util.List) r1     // Catch:{ Exception -> 0x0130 }
            java.lang.String r3 = "CameraView"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0130 }
            r4.<init>()     // Catch:{ Exception -> 0x0130 }
            java.lang.String r5 = "Supported formats: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0130 }
            java.lang.StringBuilder r4 = r4.append(r1)     // Catch:{ Exception -> 0x0130 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0130 }
            android.util.Log.i(r3, r4)     // Catch:{ Exception -> 0x0130 }
            r3 = 4
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0130 }
            boolean r1 = r1.contains(r3)     // Catch:{ Exception -> 0x0130 }
            if (r1 == 0) goto L_0x005f
            r1 = 4
            r2.setPreviewFormat(r1)     // Catch:{ Exception -> 0x0130 }
        L_0x005f:
            java.lang.String r1 = "Setting preview size"
            com.ideaworks3d.marmalade.LoaderAPI.trace(r1)
            int r1 = r9.m_Hint
            r3 = 2
            if (r1 != r3) goto L_0x00fc
            com.ideaworks3d.marmalade.LoaderView r1 = r9.m_View
            int r1 = r1.m_Width
            com.ideaworks3d.marmalade.LoaderView r3 = r9.m_View
            int r3 = r3.m_Height
            r8 = r3
            r3 = r1
            r1 = r8
        L_0x0074:
            if (r0 == 0) goto L_0x0123
            java.lang.String r4 = "CameraView"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Ideal size: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r3)
            java.lang.String r5 = "x"
            java.lang.StringBuilder r4 = r4.append(r7)
            java.lang.StringBuilder r4 = r4.append(r1)
            java.lang.String r4 = r4.toString()
            android.util.Log.i(r6, r4)
            android.hardware.Camera$Size r0 = r9.getOptimalPreviewSize(r0, r3, r1)
            r9.m_Size = r0
            java.lang.String r0 = "CameraView"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Got size: "
            java.lang.StringBuilder r0 = r0.append(r1)
            android.hardware.Camera$Size r1 = r9.m_Size
            int r1 = r1.width
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "x"
            java.lang.StringBuilder r0 = r0.append(r7)
            android.hardware.Camera$Size r1 = r9.m_Size
            int r1 = r1.height
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            android.util.Log.i(r6, r0)
            android.hardware.Camera$Size r0 = r9.m_Size
            int r0 = r0.width
            android.hardware.Camera$Size r1 = r9.m_Size
            int r1 = r1.height
            r2.setPreviewSize(r0, r1)
        L_0x00d3:
            int r0 = r9.m_Type
            if (r0 == 0) goto L_0x00e5
            java.lang.String r0 = "Setting camera ID"
            com.ideaworks3d.marmalade.LoaderAPI.trace(r0)
            java.lang.String r0 = "camera-id"
            int r1 = r9.m_Type
            int r1 = r1 + 1
            r2.set(r0, r1)
        L_0x00e5:
            android.hardware.Camera$Size r0 = r2.getPreviewSize()
            r9.m_Size = r0
            android.hardware.Camera r0 = r9.m_Camera     // Catch:{ Exception -> 0x0127 }
            r0.setParameters(r2)     // Catch:{ Exception -> 0x0127 }
        L_0x00f0:
            return
        L_0x00f1:
            r0 = move-exception
            r0 = r1
        L_0x00f3:
            java.lang.String r1 = "CameraView"
            java.lang.String r1 = "Could not request alternative preview format (OS Version < 2.0?)"
            android.util.Log.i(r6, r1)
            goto L_0x005f
        L_0x00fc:
            int r1 = r9.m_Hint
            r3 = 1
            if (r1 != r3) goto L_0x0112
            com.ideaworks3d.marmalade.LoaderView r1 = r9.m_View
            int r1 = r1.m_Width
            int r1 = r1 / 2
            com.ideaworks3d.marmalade.LoaderView r3 = r9.m_View
            int r3 = r3.m_Height
            int r3 = r3 / 2
            r8 = r3
            r3 = r1
            r1 = r8
            goto L_0x0074
        L_0x0112:
            com.ideaworks3d.marmalade.LoaderView r1 = r9.m_View
            int r1 = r1.m_Width
            int r1 = r1 / 4
            com.ideaworks3d.marmalade.LoaderView r3 = r9.m_View
            int r3 = r3.m_Height
            int r3 = r3 / 4
            r8 = r3
            r3 = r1
            r1 = r8
            goto L_0x0074
        L_0x0123:
            r2.setPreviewSize(r3, r1)
            goto L_0x00d3
        L_0x0127:
            r0 = move-exception
            java.lang.String r0 = "CameraView"
            java.lang.String r0 = "Exception setting requested preview size"
            android.util.Log.i(r6, r0)
            goto L_0x00f0
        L_0x0130:
            r1 = move-exception
            goto L_0x00f3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ideaworks3d.marmalade.s3eCamera.setCameraParameters():void");
    }

    public void onSuspendResumeEvent(SuspendResumeEvent suspendResumeEvent) {
        if (this.running) {
            if (suspendResumeEvent.eventType == SuspendResumeEvent.EventType.RESUME) {
                s3eCameraStart(this.m_Hint, S3E_CAMERA_PIXEL_TYPE_NV21, this.m_Quality);
            }
            if (suspendResumeEvent.eventType == SuspendResumeEvent.EventType.SUSPEND) {
                s3eCameraStop();
                this.running = true;
            }
            if (suspendResumeEvent.eventType == SuspendResumeEvent.EventType.SHUTDOWN) {
                s3eCameraStop();
            }
        }
    }

    public int createPreview() {
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.flags |= 8;
        layoutParams.flags |= 16;
        layoutParams.flags |= 512;
        layoutParams.flags |= 1024;
        layoutParams.gravity = 85;
        layoutParams.x = 0;
        layoutParams.y = 0;
        layoutParams.width = 1;
        layoutParams.height = 1;
        this.m_Preview = new Preview(LoaderActivity.m_Activity);
        this.m_NeedsRemovePreview = true;
        LoaderActivity.m_Activity.getWindow().getWindowManager().addView(this.m_Preview, layoutParams);
        return 0;
    }

    public int removePreview() {
        if (this.m_Preview == null || !this.m_NeedsRemovePreview) {
            return 1;
        }
        this.m_NeedsRemovePreview = false;
        LoaderActivity.m_Activity.getWindow().getWindowManager().removeViewImmediate(this.m_Preview);
        this.m_Preview = null;
        return 0;
    }

    class Preview extends SurfaceView implements SurfaceHolder.Callback {
        Preview(Context context) {
            super(context);
            SurfaceHolder holder = getHolder();
            holder.addCallback(this);
            holder.setType(3);
        }

        public void surfaceCreated(SurfaceHolder surfaceHolder) {
            if (s3eCamera.this.OpenCamera(0)) {
                try {
                    s3eCamera.this.m_Camera.setPreviewDisplay(surfaceHolder);
                    s3eCamera.this.m_Camera.setPreviewCallback(s3eCamera.this);
                } catch (Exception e) {
                    s3eCamera.this.CloseCamera();
                }
            }
        }

        public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
            s3eCamera.this.CloseCamera();
        }

        public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
            s3eCamera.this.setCameraParameters();
            s3eCamera.this.m_Camera.startPreview();
        }
    }
}
