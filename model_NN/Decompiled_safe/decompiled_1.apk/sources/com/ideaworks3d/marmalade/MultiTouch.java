package com.ideaworks3d.marmalade;

import android.view.MotionEvent;

class MultiTouch {
    private static final int POINTER_DOWN = 1;
    private static final int POINTER_MOVE = 3;
    private static final int POINTER_UP = 2;
    private static final int TOUCH_DOWN = 4;
    private static final int TOUCH_MOVE = 6;
    private static final int TOUCH_UP = 5;

    MultiTouch() {
    }

    public static boolean onTouchEvent(LoaderThread loaderThread, MotionEvent motionEvent) {
        int i;
        int action = motionEvent.getAction() & 255;
        if (action == 2) {
            int pointerCount = motionEvent.getPointerCount();
            for (int i2 = 0; i2 < pointerCount; i2++) {
                loaderThread.onMotionEvent(motionEvent.getPointerId(i2), 6, (int) motionEvent.getX(i2), (int) motionEvent.getY(i2));
            }
        } else {
            if (action == 0 || action == 5) {
                i = 4;
            } else if (action == 1 || action == 3 || action == 6) {
                i = 5;
            } else {
                i = 0;
            }
            if (i != 0) {
                int action2 = (motionEvent.getAction() & 65280) >>> 8;
                loaderThread.onMotionEvent(motionEvent.getPointerId(action2), i, (int) motionEvent.getX(action2), (int) motionEvent.getY(action2));
            }
        }
        return true;
    }
}
