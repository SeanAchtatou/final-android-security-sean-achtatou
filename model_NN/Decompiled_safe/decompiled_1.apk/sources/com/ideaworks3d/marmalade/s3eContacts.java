package com.ideaworks3d.marmalade;

import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;
import java.util.HashMap;
import v2.com.playhaven.requests.base.PHAsyncRequest;

class s3eContacts {
    private final int S3E_CONTACTS_FIELD_ADDRESS = 10;
    private final int S3E_CONTACTS_FIELD_CITY = 11;
    private final int S3E_CONTACTS_FIELD_COUNTRY = 13;
    private final int S3E_CONTACTS_FIELD_DISPLAY_NAME = 0;
    private final int S3E_CONTACTS_FIELD_EMAIL_ADDR = 8;
    private final int S3E_CONTACTS_FIELD_FIRST_NAME = 1;
    private final int S3E_CONTACTS_FIELD_FORMATTED_ADDR = 19;
    private final int S3E_CONTACTS_FIELD_HOME_PHONE = 5;
    private final int S3E_CONTACTS_FIELD_HONORIFIC_PREFIX = 16;
    private final int S3E_CONTACTS_FIELD_HONORIFIC_SUFFIX = 17;
    private final int S3E_CONTACTS_FIELD_IM = 25;
    private final int S3E_CONTACTS_FIELD_LAST_NAME = 2;
    private final int S3E_CONTACTS_FIELD_MIDDLE_NAME = 3;
    private final int S3E_CONTACTS_FIELD_MOBILE_PHONE = 6;
    private final int S3E_CONTACTS_FIELD_NICKNAME = 4;
    private final int S3E_CONTACTS_FIELD_NOTE = 36;
    private final int S3E_CONTACTS_FIELD_ORGANISATION = 32;
    private final int S3E_CONTACTS_FIELD_ORGANISATION_DEPT = 33;
    private final int S3E_CONTACTS_FIELD_ORGANISATION_TITLE = 34;
    private final int S3E_CONTACTS_FIELD_POSTAL_CODE = 12;
    private final int S3E_CONTACTS_FIELD_REGION = 20;
    private final int S3E_CONTACTS_FIELD_URL = 9;
    private final int S3E_CONTACTS_FIELD_WORK_PHONE = 7;
    private final int S3E_CONTACTS_UNLIMITED_MAX_ENTRIES = PHAsyncRequest.INFINITE_REDIRECTS;
    private int[] contactsMap = null;
    private final HashMap<Integer, FieldItem> m_ContactFields = InitContactFieldMap();
    private String m_QueryToSearch = new String();

    s3eContacts() {
    }

    class FieldItem {
        public int fieldType;
        public String fieldTypeColumn;
        public int maxEntries;
        public String mimeType;
        public String searchColumn;

        public FieldItem(String str, String str2, int i) {
            this.mimeType = null;
            this.searchColumn = null;
            this.maxEntries = 0;
            this.fieldTypeColumn = null;
            this.fieldType = 0;
            this.mimeType = str;
            this.searchColumn = str2;
            this.maxEntries = i;
        }

        public FieldItem(s3eContacts s3econtacts, String str, String str2, int i, String str3, int i2) {
            this(str, str2, i);
            this.fieldTypeColumn = str3;
            this.fieldType = i2;
        }
    }

    /* access modifiers changed from: package-private */
    public HashMap<Integer, FieldItem> InitContactFieldMap() {
        HashMap<Integer, FieldItem> hashMap = new HashMap<>();
        hashMap.put(4, new FieldItem("vnd.android.cursor.item/nickname", "data1", 1));
        hashMap.put(8, new FieldItem("vnd.android.cursor.item/email_v2", "data1", PHAsyncRequest.INFINITE_REDIRECTS));
        hashMap.put(25, new FieldItem("vnd.android.cursor.item/im", "data1", PHAsyncRequest.INFINITE_REDIRECTS));
        hashMap.put(36, new FieldItem("vnd.android.cursor.item/note", "data1", 1));
        hashMap.put(9, new FieldItem("vnd.android.cursor.item/website", "data1", PHAsyncRequest.INFINITE_REDIRECTS));
        hashMap.put(19, new FieldItem("vnd.android.cursor.item/postal-address_v2", "data1", PHAsyncRequest.INFINITE_REDIRECTS));
        hashMap.put(10, new FieldItem("vnd.android.cursor.item/postal-address_v2", "data1", PHAsyncRequest.INFINITE_REDIRECTS));
        hashMap.put(11, new FieldItem("vnd.android.cursor.item/postal-address_v2", "data7", 1));
        hashMap.put(20, new FieldItem("vnd.android.cursor.item/postal-address_v2", "data8", 1));
        hashMap.put(12, new FieldItem("vnd.android.cursor.item/postal-address_v2", "data9", 1));
        hashMap.put(13, new FieldItem("vnd.android.cursor.item/postal-address_v2", "data10", 1));
        hashMap.put(0, new FieldItem("vnd.android.cursor.item/name", "data1", 1));
        hashMap.put(1, new FieldItem("vnd.android.cursor.item/name", "data2", 1));
        hashMap.put(2, new FieldItem("vnd.android.cursor.item/name", "data3", 1));
        hashMap.put(17, new FieldItem("vnd.android.cursor.item/name", "data6", 1));
        hashMap.put(32, new FieldItem("vnd.android.cursor.item/organization", "data1", 1));
        hashMap.put(33, new FieldItem("vnd.android.cursor.item/organization", "data5", 1));
        hashMap.put(34, new FieldItem("vnd.android.cursor.item/organization", "data4", 1));
        hashMap.put(5, new FieldItem(this, "vnd.android.cursor.item/phone_v2", "data1", PHAsyncRequest.INFINITE_REDIRECTS, "data2", 1));
        hashMap.put(6, new FieldItem(this, "vnd.android.cursor.item/phone_v2", "data1", PHAsyncRequest.INFINITE_REDIRECTS, "data2", 2));
        hashMap.put(7, new FieldItem(this, "vnd.android.cursor.item/phone_v2", "data1", PHAsyncRequest.INFINITE_REDIRECTS, "data2", 3));
        return hashMap;
    }

    private String CreateSearchQuery(String str, String str2, String str3, String str4, int i) {
        String str5 = "((mimetype = '" + str2 + "') AND (" + str3 + " LIKE '%" + str + "%')";
        if (str4 != null) {
            str5 = str5 + "AND ( " + str4 + " = '" + i + "' )";
        }
        return str5 + ")";
    }

    public String QueryForContactRow(int i, String str, String str2, int i2) {
        String str3 = "mimetype = '" + str + "'" + " AND " + "contact_id" + " = " + i;
        if (str2 != null) {
            str3 = str3 + " AND " + str2 + " = " + i2;
        }
        Log.v("myapp", "query for row: " + str3);
        return str3;
    }

    private void AddQueryes(String str, int i) {
        FieldItem fieldItem = this.m_ContactFields.get(Integer.valueOf(i));
        if (fieldItem != null) {
            if (this.m_QueryToSearch.length() > 0) {
                this.m_QueryToSearch += " OR ";
            }
            this.m_QueryToSearch += CreateSearchQuery(str, fieldItem.mimeType, fieldItem.searchColumn, fieldItem.fieldTypeColumn, fieldItem.fieldType);
        }
    }

    private boolean UpdateUIDMap(String str) {
        String str2;
        Log.v("myapp", "update contacts with query: " + str);
        this.contactsMap = new int[0];
        boolean z = str == null;
        Uri uri = z ? ContactsContract.RawContacts.CONTENT_URI : ContactsContract.Data.CONTENT_URI;
        String str3 = z ? "contact_id" : "contact_id";
        if (z) {
            str2 = "deleted = 0 ";
        } else {
            str2 = str;
        }
        try {
            Cursor query = LoaderActivity.m_Activity.getContentResolver().query(uri, new String[]{str3}, str2, null, null);
            query.moveToFirst();
            if (query == null) {
                return false;
            }
            int count = query.getCount();
            this.contactsMap = new int[count];
            Log.v("myapp", "cursor: number of records: " + count);
            int i = 0;
            while (i < count) {
                this.contactsMap[i] = query.getInt(query.getColumnIndex(str3));
                i++;
                query.moveToNext();
            }
            query.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean contactsSimpleSearch(String str, int[] iArr, boolean z) {
        this.m_QueryToSearch = "";
        for (int AddQueryes : iArr) {
            AddQueryes(str, AddQueryes);
        }
        if (this.m_QueryToSearch == "") {
            return false;
        }
        return UpdateUIDMap(this.m_QueryToSearch);
    }

    public boolean contactsUpdate() {
        return UpdateUIDMap(null);
    }

    public int contactsGetNumRecords() {
        if (this.contactsMap != null) {
            return this.contactsMap.length;
        }
        return -1;
    }

    public int contactsGetUID(int i) {
        if (this.contactsMap == null || i < 0 || i >= this.contactsMap.length) {
            return -1;
        }
        return this.contactsMap[i];
    }

    public Cursor contactsGetData(int i, int i2) {
        FieldItem fieldItem = this.m_ContactFields.get(Integer.valueOf(i2));
        if (fieldItem == null) {
            return null;
        }
        return LoaderActivity.m_Activity.getContentResolver().query(ContactsContract.Data.CONTENT_URI, new String[]{fieldItem.searchColumn}, QueryForContactRow(i, fieldItem.mimeType, fieldItem.fieldTypeColumn, fieldItem.fieldType), null, null);
    }

    public int contactsGetMaxNumEntries(int i) {
        FieldItem fieldItem = this.m_ContactFields.get(Integer.valueOf(i));
        if (fieldItem == null) {
            return 0;
        }
        return fieldItem.maxEntries;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0035, code lost:
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x003d, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x003e, code lost:
        r4 = r1;
        r1 = null;
        r0 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0041, code lost:
        if (r1 != null) goto L_0x0043;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0043, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0046, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0047, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0048, code lost:
        r4 = r1;
        r1 = r0;
        r0 = r4;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0034 A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:1:0x0002] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0047 A[ExcHandler: all (r1v7 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:7:0x000f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int contactsGetNumEntries(int r6, int r7) {
        /*
            r5 = this;
            r3 = 0
            r0 = 0
            android.database.Cursor r0 = r5.contactsGetData(r6, r7)     // Catch:{ Exception -> 0x0034, all -> 0x003d }
            if (r0 != 0) goto L_0x000f
            if (r0 == 0) goto L_0x000d
            r0.close()
        L_0x000d:
            r0 = r3
        L_0x000e:
            return r0
        L_0x000f:
            int r1 = r0.getCount()     // Catch:{ Exception -> 0x0034, all -> 0x0047 }
            if (r7 != 0) goto L_0x002d
            r2 = 1
            if (r1 != r2) goto L_0x002d
            r2 = 0
            boolean r2 = r0.moveToPosition(r2)     // Catch:{ Exception -> 0x004c, all -> 0x0047 }
            if (r2 == 0) goto L_0x002d
            r2 = 0
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x004c, all -> 0x0047 }
            if (r2 != 0) goto L_0x002d
            if (r0 == 0) goto L_0x002b
            r0.close()
        L_0x002b:
            r0 = r3
            goto L_0x000e
        L_0x002d:
            if (r0 == 0) goto L_0x004e
            r0.close()
            r0 = r1
            goto L_0x000e
        L_0x0034:
            r1 = move-exception
            r1 = r3
        L_0x0036:
            if (r0 == 0) goto L_0x004e
            r0.close()
            r0 = r1
            goto L_0x000e
        L_0x003d:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0041:
            if (r1 == 0) goto L_0x0046
            r1.close()
        L_0x0046:
            throw r0
        L_0x0047:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0041
        L_0x004c:
            r2 = move-exception
            goto L_0x0036
        L_0x004e:
            r0 = r1
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ideaworks3d.marmalade.s3eContacts.contactsGetNumEntries(int, int):int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x002d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String contactsGetField(int r5, int r6, int r7) {
        /*
            r4 = this;
            r2 = 0
            android.database.Cursor r0 = r4.contactsGetData(r5, r6)     // Catch:{ Exception -> 0x0020, all -> 0x0029 }
            if (r0 != 0) goto L_0x000e
            if (r0 == 0) goto L_0x000c
            r0.close()
        L_0x000c:
            r0 = r2
        L_0x000d:
            return r0
        L_0x000e:
            boolean r1 = r0.moveToPosition(r7)     // Catch:{ Exception -> 0x0036, all -> 0x0031 }
            if (r1 == 0) goto L_0x003c
            r1 = 0
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Exception -> 0x0036, all -> 0x0031 }
        L_0x0019:
            if (r0 == 0) goto L_0x003a
            r0.close()
            r0 = r1
            goto L_0x000d
        L_0x0020:
            r0 = move-exception
            r0 = r2
        L_0x0022:
            if (r0 == 0) goto L_0x0038
            r0.close()
            r0 = r2
            goto L_0x000d
        L_0x0029:
            r0 = move-exception
            r1 = r2
        L_0x002b:
            if (r1 == 0) goto L_0x0030
            r1.close()
        L_0x0030:
            throw r0
        L_0x0031:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
            goto L_0x002b
        L_0x0036:
            r1 = move-exception
            goto L_0x0022
        L_0x0038:
            r0 = r2
            goto L_0x000d
        L_0x003a:
            r0 = r1
            goto L_0x000d
        L_0x003c:
            r1 = r2
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ideaworks3d.marmalade.s3eContacts.contactsGetField(int, int, int):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x00f0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean contactsSetField(java.lang.String r12, int r13, int r14, int r15) {
        /*
            r11 = this;
            r10 = 1
            r9 = 0
            r8 = 0
            java.lang.String r1 = "_id"
            java.lang.String r1 = " value: "
            java.util.HashMap<java.lang.Integer, com.ideaworks3d.marmalade.s3eContacts$FieldItem> r1 = r11.m_ContactFields
            java.lang.Integer r2 = java.lang.Integer.valueOf(r14)
            java.lang.Object r1 = r1.get(r2)
            r0 = r1
            com.ideaworks3d.marmalade.s3eContacts$FieldItem r0 = (com.ideaworks3d.marmalade.s3eContacts.FieldItem) r0
            r7 = r0
            if (r7 != 0) goto L_0x0019
            r1 = r9
        L_0x0018:
            return r1
        L_0x0019:
            com.ideaworks3d.marmalade.LoaderActivity r1 = com.ideaworks3d.marmalade.LoaderActivity.m_Activity     // Catch:{ Exception -> 0x00f6, all -> 0x00ec }
            android.content.ContentResolver r1 = r1.getContentResolver()     // Catch:{ Exception -> 0x00f6, all -> 0x00ec }
            android.net.Uri r2 = android.provider.ContactsContract.Data.CONTENT_URI     // Catch:{ Exception -> 0x00f6, all -> 0x00ec }
            r3 = 3
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ Exception -> 0x00f6, all -> 0x00ec }
            r4 = 0
            java.lang.String r5 = "_id"
            r3[r4] = r5     // Catch:{ Exception -> 0x00f6, all -> 0x00ec }
            r4 = 1
            java.lang.String r5 = "contact_id"
            r3[r4] = r5     // Catch:{ Exception -> 0x00f6, all -> 0x00ec }
            r4 = 2
            java.lang.String r5 = "mimetype"
            r3[r4] = r5     // Catch:{ Exception -> 0x00f6, all -> 0x00ec }
            java.lang.String r4 = r7.mimeType     // Catch:{ Exception -> 0x00f6, all -> 0x00ec }
            java.lang.String r5 = r7.fieldTypeColumn     // Catch:{ Exception -> 0x00f6, all -> 0x00ec }
            int r6 = r7.fieldType     // Catch:{ Exception -> 0x00f6, all -> 0x00ec }
            java.lang.String r4 = r11.QueryForContactRow(r13, r4, r5, r6)     // Catch:{ Exception -> 0x00f6, all -> 0x00ec }
            r5 = 0
            r6 = 0
            android.database.Cursor r2 = r1.query(r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x00f6, all -> 0x00ec }
            android.content.ContentValues r3 = new android.content.ContentValues     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            r3.<init>()     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            java.lang.String r4 = r7.searchColumn     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            r3.put(r4, r12)     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            java.lang.String r4 = "mimetype"
            java.lang.String r5 = r7.mimeType     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            r3.put(r4, r5)     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            java.lang.String r4 = "raw_contact_id"
            java.lang.Integer r5 = java.lang.Integer.valueOf(r13)     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            r3.put(r4, r5)     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            java.lang.String r4 = r7.fieldTypeColumn     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            if (r4 == 0) goto L_0x006c
            java.lang.String r4 = r7.fieldTypeColumn     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            int r5 = r7.fieldType     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            r3.put(r4, r5)     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
        L_0x006c:
            if (r2 == 0) goto L_0x0074
            int r4 = r2.getCount()     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            if (r15 != r4) goto L_0x00a5
        L_0x0074:
            java.lang.String r4 = "myapp"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            r5.<init>()     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            java.lang.String r6 = "insert row. type: "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            java.lang.String r6 = r7.mimeType     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            java.lang.String r6 = " value: "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            java.lang.StringBuilder r5 = r5.append(r12)     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            android.util.Log.v(r4, r5)     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            android.net.Uri r4 = android.provider.ContactsContract.Data.CONTENT_URI     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            r1.insert(r4, r3)     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
        L_0x009d:
            if (r2 == 0) goto L_0x00fc
            r2.close()
            r1 = r10
            goto L_0x0018
        L_0x00a5:
            java.lang.String r4 = "myapp"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            r5.<init>()     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            java.lang.String r6 = "update row. type: "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            java.lang.String r6 = r7.mimeType     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            java.lang.String r6 = " value: "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            java.lang.StringBuilder r5 = r5.append(r12)     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            android.util.Log.v(r4, r5)     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            r2.moveToPosition(r15)     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            android.net.Uri r4 = android.provider.ContactsContract.Data.CONTENT_URI     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            java.lang.String r5 = "_id"
            int r5 = r2.getColumnIndex(r5)     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            java.lang.String r5 = r2.getString(r5)     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            android.net.Uri r4 = android.net.Uri.withAppendedPath(r4, r5)     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            r5 = 0
            r6 = 0
            r1.update(r4, r3, r5, r6)     // Catch:{ Exception -> 0x00e2, all -> 0x00f4 }
            goto L_0x009d
        L_0x00e2:
            r1 = move-exception
            r1 = r2
        L_0x00e4:
            if (r1 == 0) goto L_0x00f9
            r1.close()
            r1 = r9
            goto L_0x0018
        L_0x00ec:
            r1 = move-exception
            r2 = r8
        L_0x00ee:
            if (r2 == 0) goto L_0x00f3
            r2.close()
        L_0x00f3:
            throw r1
        L_0x00f4:
            r1 = move-exception
            goto L_0x00ee
        L_0x00f6:
            r1 = move-exception
            r1 = r8
            goto L_0x00e4
        L_0x00f9:
            r1 = r9
            goto L_0x0018
        L_0x00fc:
            r1 = r10
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ideaworks3d.marmalade.s3eContacts.contactsSetField(java.lang.String, int, int, int):boolean");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public int contactsCreate() {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("aggregation_mode", (Integer) 3);
            return Integer.parseInt(LoaderActivity.m_Activity.getContentResolver().insert(ContactsContract.RawContacts.CONTENT_URI, contentValues).getLastPathSegment());
        } catch (Exception e) {
            return -1;
        }
    }

    public boolean contactsDelete(int i) {
        try {
            if (LoaderActivity.m_Activity.getContentResolver().delete(ContentUris.withAppendedId(ContactsContract.RawContacts.CONTENT_URI, (long) i), null, null) != 0) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }
}
