package com.ideaworks3d.marmalade;

import android.media.AudioTrack;
import android.os.SystemClock;

public class SoundPlayer implements AudioTrack.OnPlaybackPositionUpdateListener {
    private static final int PERIODS_IN_BUFFER = 4;
    private int m_BufSize;
    private int m_FrameSize;
    private boolean m_NeedsPrime = false;
    private int m_Period;
    private short[] m_SampleData;
    private int m_SampleRate = 0;
    private boolean m_Stereo = false;
    private AudioTrack m_Track;
    private int m_Volume;

    private native void generateAudio(short[] sArr, int i);

    /* access modifiers changed from: package-private */
    public int init(int i, boolean z, int i2) {
        int i3;
        this.m_SampleRate = i2;
        this.m_Stereo = z;
        this.m_Volume = i;
        if (this.m_SampleRate == 0) {
            this.m_SampleRate = AudioTrack.getNativeOutputSampleRate(3);
        }
        if (this.m_Stereo) {
            i3 = 3;
        } else {
            i3 = 2;
        }
        this.m_FrameSize = 2;
        if (this.m_Stereo) {
            this.m_FrameSize *= 2;
        }
        this.m_BufSize = AudioTrack.getMinBufferSize(this.m_SampleRate, i3, 2);
        this.m_Period = (this.m_BufSize / this.m_FrameSize) / 4;
        try {
            this.m_Track = new AudioTrack(3, this.m_SampleRate, i3, 2, this.m_BufSize, 1);
            applyVolume();
            if (this.m_Track.getState() != 1) {
                return 0;
            }
            this.m_SampleData = new short[this.m_BufSize];
            this.m_Track.setPlaybackPositionUpdateListener(this);
            this.m_Track.setPositionNotificationPeriod(this.m_Period);
            this.m_NeedsPrime = true;
            return this.m_SampleRate;
        } catch (IllegalArgumentException e) {
            return 0;
        }
    }

    /* access modifiers changed from: package-private */
    public void start() {
        if (this.m_Track == null) {
            init(this.m_Volume, this.m_Stereo, this.m_SampleRate);
        }
        this.m_Track.play();
        if (this.m_NeedsPrime) {
            this.m_NeedsPrime = false;
            writeSamples(this.m_Period * 4);
        }
    }

    public void onMarkerReached(AudioTrack audioTrack) {
    }

    private boolean writeSamples(int i) {
        int i2;
        if (this.m_Track == null || this.m_SampleData == null) {
            return false;
        }
        generateAudio(this.m_SampleData, i);
        if (this.m_Stereo) {
            i2 = i * 2;
        } else {
            i2 = i;
        }
        if (this.m_Track == null || this.m_SampleData == null) {
            return false;
        }
        long uptimeMillis = SystemClock.uptimeMillis();
        this.m_Track.write(this.m_SampleData, 0, i2);
        if (SystemClock.uptimeMillis() - uptimeMillis > 1) {
        }
        return true;
    }

    public synchronized void onPeriodicNotification(AudioTrack audioTrack) {
        if (!writeSamples(this.m_Period)) {
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void stop() {
        if (this.m_Track != null) {
            this.m_Track.setPlaybackPositionUpdateListener(null);
            this.m_Track.stop();
            this.m_Track.flush();
            this.m_Track.release();
            this.m_Track = null;
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void pause() {
        if (this.m_Track != null) {
            if (this.m_Track.getPlayState() == 3) {
                this.m_Track.pause();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void resume() {
        if (this.m_Track != null) {
            if (this.m_Track.getPlayState() == 2) {
                start();
                writeSamples((this.m_Period * 4) / 2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void setVolume(int i) {
        this.m_Volume = i;
        if (this.m_Track != null) {
            applyVolume();
        }
    }

    private synchronized void applyVolume() {
        float maxVolume = (((float) this.m_Volume) * AudioTrack.getMaxVolume()) / 100.0f;
        this.m_Track.setStereoVolume(maxVolume, maxVolume);
    }
}
