package v2.com.playhaven.configuration;

/**
 * Auto-generated file, contains project and version control state info.
 */
public final class Version {
  public static String PLUGIN_BUILD_TIME = "2013-04-15 18:17";

  public static String PROJECT_NAME = "Playhaven API";
  public static String PROJECT_GROUP = "com.playhaven.androidsdk";
  public static String PROJECT_ARTIFACT = "playhaven";
  public static String PROJECT_VERSION = "1.12.5";
  public static String SOURCE_VERSION = "v1.12.4-7-g2517056";
  public static String SOURCE_BRANCH = "1.12.5-release";
  public static Boolean SOURCE_DIRTY = false;
  public static String SOURCE_WORKING_COPY = "/Volumes/Data/jenkins/workspace/sdk-android-legacy";

  public static String SERVER_BUILD_PLAN = "N/A";
  public static String SERVER_BUILD_ID = "N/A";
  public static String SERVER_BUILD_TIME = "N/A";

  public static String BANNER_FULL = "Build info: Playhaven API version 1.12.5 built on 2013-04-15 18:17 source version v1.12.4-7-g2517056, build id: N/A-N/A.";
  public static String BANNER = "1.12.5-v1.12.4-7-g2517056 2013-04-15 18:17 ";
}
