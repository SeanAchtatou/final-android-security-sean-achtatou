
function loadFileSafe(filename, safeEnvironment)
{
    local untrustedFunction, message = loadfile(filename);
    
    if not untrustedFunction then
        return nil, message;
    end
    setfenv(untrustedFunction, safeEnvironment);
    return pcall(untrustedFunction);
}

function parseShopObject(shopObjectDefinition)
{
	local shopObject;

	if shopObjectDefinition.type == nil or shopObjectDefinition.type == "item" then
		shopObject = parseShopItem(shopObjectDefinition);
	elseif shopObjectDefinition.type == "group" then
		shopObject = parseShopGroup(shopObjectDefinition);
	elseif shopObjectDefinition.type == "stack" then
		shopObject = parseShopStack(shopObjectDefinition);
	else
		error("Invalid shop object type");
	end

	shopObject:setKey(shopObjectDefinition.key);
	shopObject:setDescription(shopObjectDefinition.description || (shopObjectDefinition.key .. "Description"));
	shopObject:setShopImagePath(shopObjectDefinition.shopImagePath);
}

function parseShopGroup(shopGroupDefinition)
{
	shopGroup = ShopGroup:create();

	for shopObjectDefinition in shopGroupDefinition.objects do
		shopGroup:getShopObjects():addObject(parseShopObject(shopObjectDefinition));
	end

	return shopGroup;
}

function parseShopStack(shopStackDefinition)
{
	shopStack = ShopStack:create();

	for shopItemDefinition in shopStack.items do
		shopStack:getShopItems():addObject(parseShopItem(shopItemDefinition));
	end

	return shopStack;
}

function parseShopItem(shopItemDefinition)
{
	return ShopItem:create();
}

function loadShop()
{
    local shopEnvironment = {};
    local shop = loadFileSafe("Data/shop_items.lua", shopEnvironment);
    
    return parseShopObject(shop);
}