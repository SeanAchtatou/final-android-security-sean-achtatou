require 'Scripts/safe_load.lua'

function parseReconquer(definition)
    local reconquer = ReconquerData:create();

    reconquer:setDeltaSecondsMin(definition.deltaSecondsMin);
    reconquer:setDeltaSecondsMax(definition.deltaSecondsMax);
    reconquer:setProbability(definition.probability);

    return reconquer;
end

local reconquers = CCArray:create();

local definitions = loadFileSafe("Scripts/Data/reconquers.lua", {});
for key, definition in pairs(definitions) do
    reconquers:addObject(parseReconquer(definition));
end

return reconquers;