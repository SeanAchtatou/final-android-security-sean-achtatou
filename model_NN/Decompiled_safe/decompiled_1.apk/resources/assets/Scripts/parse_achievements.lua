require 'Scripts/safe_load.lua'

function parseAchievement(achievementDefinition)
    local achievement = AchievementData:create();

    achievement:setEventTrackerType(achievementDefinition.eventTrackerType);
    achievement:setGameEventType(achievementDefinition.gameEventType);
    achievement:setThreshold(achievementDefinition.threshold);

    if achievementDefinition.achievementIdAndroid ~= nil then
        achievement:setAchievementIdAndroid(achievementDefinition.achievementIdAndroid);
    end

    if achievementDefinition.achievementIdBlackBerry ~= nil then
        achievement:setAchievementIdBlackBerry(achievementDefinition.achievementIdBlackBerry);
    end

    if achievementDefinition.achievementIdIOS ~= nil then
        achievement:setAchievementIdIOS(achievementDefinition.achievementIdIOS);
    end

    if achievementDefinition.achievementIdWindowsPhone ~= nil then
        achievement:setAchievementIdWindowsPhone(achievementDefinition.achievementIdWindowsPhone);
    end

    return achievement;
end

local achievements = CCArray:create();

local achievementDefinitions = loadFileSafe("Scripts/Data/achievements.lua",
    {
        EventTrackerType_Counter=EventTrackerType_Counter,
        EventTrackerType_Threshold=EventTrackerType_Threshold,

        GameEventType_AchievementsUnlocked=GameEventType_AchievementsUnlocked,
        GameEventType_BoosterPurchase=GameEventType_BoosterPurchase,
        GameEventType_Dodge=GameEventType_Dodge,
        GameEventType_HeroMaxCombo=GameEventType_HeroMaxCombo,
        GameEventType_HeroMaxScore=GameEventType_HeroMaxScore,
        GameEventType_LevelCleared=GameEventType_LevelCleared,
        GameEventType_LifeRefillPurchase=GameEventType_LifeRefillPurchase,
        GameEventType_NeighbourhoodCollection=GameEventType_NeighbourhoodCollection,
        GameEventType_NeighbourhoodsCompleted=GameEventType_NeighbourhoodsCompleted,
        GameEventType_PickupHealthMeat=GameEventType_PickupHealthMeat,
        GameEventType_PropDestroyed=GameEventType_PropDestroyed,
        GameEventType_Punch=GameEventType_Punch,
        GameEventType_RevivePurchase=GameEventType_RevivePurchase,
        GameEventType_SpecialAttack=GameEventType_SpecialAttack,
        GameEventType_SpecialAttackPurchase=GameEventType_SpecialAttackPurchase,
        GameEventType_StatUpgradePurchase=GameEventType_StatUpgradePurchase,
        GameEventType_Throw=GameEventType_Throw,
        GameEventType_TutorialShow=GameEventType_TutorialShow,
        GameEventType_Uppercut=GameEventType_Uppercut,
    }
);

for key, achievementDefinition in pairs(achievementDefinitions) do
    achievements:addObject(parseAchievement(achievementDefinition));
end

return achievements;