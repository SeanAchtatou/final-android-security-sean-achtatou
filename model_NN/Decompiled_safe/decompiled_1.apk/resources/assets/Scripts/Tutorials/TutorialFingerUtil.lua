local SWIPE_DISTANCE = 60.0;

local ANCHOR_POINT_FINGER = ccp(0.6, 0.2);

function createTapRing()
    local ring = CCSprite:create("Images/UI/HUD/tutorial_ring.png");
    ring:setScale(0.025);
    ring:setOpacity(0);
    
    return ring;
end

function createOneFinger()
    local finger = CCSprite:create("Images/UI/HUD/tutorial_hand_one_finger.png");
    finger:setAnchorPoint(ANCHOR_POINT_FINGER);
    finger:setScale(0.5);

    return finger;
end

function createTwoFingers()
    local fingers = CCSprite:create("Images/UI/HUD/tutorial_hand_two_fingers.png");
    fingers:setAnchorPoint(ANCHOR_POINT_FINGER);
    fingers:setScale(0.5);

    return fingers;
end

function createStreak()
    local streak = CCSprite:create("Images/UI/HUD/tutorial_streak.png");
    streak:setScaleX(0.05);
    streak:setScaleY(0.5);
    streak:setOpacity(0);

    return streak;
end

function createStreakSwipeActionLoop(offset)
    local actionLoopStreak = CCArray:create();
    actionLoopStreak:addObject(CCDelayTime:create(1.0));
    actionLoopStreak:addObject(CCFadeTo:create(0.05, 128));
    actionLoopStreak:addObject(CCSpawn:createWithTwoActions(CCMoveBy:create(0.15, offset), CCScaleTo:create(0.15, 0.45, 0.5)));
    actionLoopStreak:addObject(CCFadeTo:create(0.05, 0));
    actionLoopStreak:addObject(CCDelayTime:create(0.25));
    actionLoopStreak:addObject(CCSpawn:createWithTwoActions(CCMoveBy:create(0.25, ccpNeg(offset)), CCScaleTo:create(0.25, 0.05, 0.5)));

    return actionLoopStreak;
end

function createFingerSwipeActionLoop(offset)
    local actionLoopFinger = CCArray:create();
    actionLoopFinger:addObject(CCDelayTime:create(1.0));
    actionLoopFinger:addObject(CCRotateBy:create(0.05, -20.0));
    actionLoopFinger:addObject(CCMoveBy:create(0.15, offset));
    actionLoopFinger:addObject(CCRotateBy:create(0.05, 20.0));
    actionLoopFinger:addObject(CCDelayTime:create(0.25));
    actionLoopFinger:addObject(CCMoveBy:create(0.25, ccpNeg(offset)));

    return actionLoopFinger;
end

function oneFingerTap(tutorialStepNode)
    
    local ring = createTapRing();
    ring:setPosition(ccp(344, 47));
    tutorialStepNode:addChild(ring);

    local finger = createOneFinger();
    finger:setPosition(ccp(360, 20));
    tutorialStepNode:addChild(finger);

    local actionLoopRing = CCArray:create();
    actionLoopRing:addObject(CCDelayTime:create(1.05));
    actionLoopRing:addObject(CCFadeTo:create(0.0, 196));
    actionLoopRing:addObject(CCSpawn:createWithTwoActions(CCEaseSineOut:create(CCScaleTo:create(0.45, 0.175)), CCEaseSineOut:create(CCFadeTo:create(0.45, 0))));
    actionLoopRing:addObject(CCScaleTo:create(0.0, 0.025));

    local actionLoopFinger = CCArray:create();
    actionLoopFinger:addObject(CCDelayTime:create(1.0));
    actionLoopFinger:addObject(CCRotateBy:create(0.05, -20.0));
    actionLoopFinger:addObject(CCDelayTime:create(0.15));
    actionLoopFinger:addObject(CCRotateBy:create(0.05, 20.0));
    actionLoopFinger:addObject(CCDelayTime:create(0.25));

    local ringAction = CCRepeatForever:create(CCSequence:create(actionLoopRing));
    ring:runAction(ringAction);

    local fingerAction = CCRepeatForever:create(CCSequence:create(actionLoopFinger));
    finger:runAction(fingerAction);
end

function oneFingerDoubleTap(tutorialStepNode)

    local ring1 = createTapRing();
    ring1:setPosition(ccp(344, 47));
    tutorialStepNode:addChild(ring1);

    local ring2 = createTapRing();
    ring2:setPosition(ccp(344, 47));
    tutorialStepNode:addChild(ring2);

    local finger = createOneFinger();
    finger:setPosition(ccp(360, 20));
    tutorialStepNode:addChild(finger);

    local actionLoopRing1 = CCArray:create();
    actionLoopRing1:addObject(CCDelayTime:create(1.05));
    actionLoopRing1:addObject(CCFadeTo:create(0.0, 196));
    actionLoopRing1:addObject(CCSpawn:createWithTwoActions(CCEaseSineOut:create(CCScaleTo:create(0.45, 0.175)), CCEaseSineOut:create(CCFadeTo:create(0.45, 0))));
    actionLoopRing1:addObject(CCScaleTo:create(0.0, 0.025));
    actionLoopRing1:addObject(CCDelayTime:create(0.25));

    local actionLoopRing2 = CCArray:create();
    actionLoopRing2:addObject(CCDelayTime:create(1.3));
    actionLoopRing2:addObject(CCFadeTo:create(0.0, 196));
    actionLoopRing2:addObject(CCSpawn:createWithTwoActions(CCEaseSineOut:create(CCScaleTo:create(0.45, 0.175)), CCEaseSineOut:create(CCFadeTo:create(0.45, 0))));
    actionLoopRing2:addObject(CCScaleTo:create(0.0, 0.025));

    local actionLoopFinger = CCArray:create();
    actionLoopFinger:addObject(CCDelayTime:create(1.0));
    actionLoopFinger:addObject(CCRotateBy:create(0.05, -20.0));
    actionLoopFinger:addObject(CCDelayTime:create(0.15));
    actionLoopFinger:addObject(CCRotateBy:create(0.05, 20.0));
    actionLoopFinger:addObject(CCRotateBy:create(0.05, -20.0));
    actionLoopFinger:addObject(CCDelayTime:create(0.15));
    actionLoopFinger:addObject(CCRotateBy:create(0.05, 20.0));
    actionLoopFinger:addObject(CCDelayTime:create(0.25));

    local ring1Action = CCRepeatForever:create(CCSequence:create(actionLoopRing1));
    ring1:runAction(ring1Action);

    local ring2Action = CCRepeatForever:create(CCSequence:create(actionLoopRing2));
    ring2:runAction(ring2Action);

    local fingerAction = CCRepeatForever:create(CCSequence:create(actionLoopFinger));
    finger:runAction(fingerAction);
end

function oneFingerSwipeLeft(tutorialStepNode)

    local ring = createTapRing();
    ring:setPosition(ccp(404, 47));
    tutorialStepNode:addChild(ring);

    local streak = createStreak();
    streak:setAnchorPoint(ccp(0.0, 0.5));
    streak:setPosition(ccp(404, 47));
    streak:setFlipX(true);
    tutorialStepNode:addChild(streak);

    local finger = createOneFinger();
    finger:setPosition(ccp(420, 20));
    tutorialStepNode:addChild(finger);

    local actionLoopRing = CCArray:create();
    actionLoopRing:addObject(CCDelayTime:create(1.05));
    actionLoopRing:addObject(CCFadeTo:create(0.0, 196));
    actionLoopRing:addObject(CCSpawn:createWithTwoActions(CCEaseSineOut:create(CCScaleTo:create(0.45, 0.175)), CCEaseSineOut:create(CCFadeTo:create(0.45, 0))));
    actionLoopRing:addObject(CCScaleTo:create(0.0, 0.025));
    actionLoopRing:addObject(CCDelayTime:create(0.25));

    local swipeOffset = ccp(-SWIPE_DISTANCE, 0);
    local actionLoopStreak = createStreakSwipeActionLoop(swipeOffset);
    local actionLoopFinger = createFingerSwipeActionLoop(swipeOffset);

    local ringAction = CCRepeatForever:create(CCSequence:create(actionLoopRing));
    ring:runAction(ringAction);

    local streakAction = CCRepeatForever:create(CCSequence:create(actionLoopStreak));
    streak:runAction(streakAction);

    local fingerAction = CCRepeatForever:create(CCSequence:create(actionLoopFinger));
    finger:runAction(fingerAction);
end

function oneFingerSwipeRight(tutorialStepNode)

    local ring = createTapRing();
    ring:setPosition(ccp(344, 47));
    tutorialStepNode:addChild(ring);

    local streak = createStreak();
    streak:setAnchorPoint(ccp(1.0, 0.5));
    streak:setPosition(ccp(344, 47));
    tutorialStepNode:addChild(streak);

    local finger = createOneFinger();
    finger:setPosition(ccp(360, 20));
    tutorialStepNode:addChild(finger);

    local actionLoopRing = CCArray:create();
    actionLoopRing:addObject(CCDelayTime:create(1.05));
    actionLoopRing:addObject(CCFadeTo:create(0.0, 196));
    actionLoopRing:addObject(CCSpawn:createWithTwoActions(CCEaseSineOut:create(CCScaleTo:create(0.45, 0.175)), CCEaseSineOut:create(CCFadeTo:create(0.45, 0))));
    actionLoopRing:addObject(CCScaleTo:create(0.0, 0.025));
    actionLoopRing:addObject(CCDelayTime:create(0.25));

    local swipeOffset = ccp(SWIPE_DISTANCE, 0);
    local actionLoopStreak = createStreakSwipeActionLoop(swipeOffset);
    local actionLoopFinger = createFingerSwipeActionLoop(swipeOffset);

    local ringAction = CCRepeatForever:create(CCSequence:create(actionLoopRing));
    ring:runAction(ringAction);

    local streakAction = CCRepeatForever:create(CCSequence:create(actionLoopStreak));
    streak:runAction(streakAction);

    local fingerAction = CCRepeatForever:create(CCSequence:create(actionLoopFinger));
    finger:runAction(fingerAction);
end

function oneFingerSwipeUp(tutorialStepNode)

    local ring = createTapRing();
    ring:setPosition(ccp(344, 47));
    tutorialStepNode:addChild(ring);

    local streak = createStreak();
    streak:setAnchorPoint(ccp(1.0, 0.5));
    streak:setRotation(-90.0);
    streak:setPosition(ccp(344, 47));
    tutorialStepNode:addChild(streak);

    local finger = createOneFinger();
    finger:setPosition(ccp(360, 20));
    tutorialStepNode:addChild(finger);

    local actionLoopRing = CCArray:create();
    actionLoopRing:addObject(CCDelayTime:create(1.05));
    actionLoopRing:addObject(CCFadeTo:create(0.0, 196));
    actionLoopRing:addObject(CCSpawn:createWithTwoActions(CCEaseSineOut:create(CCScaleTo:create(0.45, 0.175)), CCEaseSineOut:create(CCFadeTo:create(0.45, 0))));
    actionLoopRing:addObject(CCScaleTo:create(0.0, 0.025));
    actionLoopRing:addObject(CCDelayTime:create(0.25));

    local swipeOffset = ccp(0, SWIPE_DISTANCE);
    local actionLoopStreak = createStreakSwipeActionLoop(swipeOffset);
    local actionLoopFinger = createFingerSwipeActionLoop(swipeOffset);

    local ringAction = CCRepeatForever:create(CCSequence:create(actionLoopRing));
    ring:runAction(ringAction);

    local streakAction = CCRepeatForever:create(CCSequence:create(actionLoopStreak));
    streak:runAction(streakAction);

    local fingerAction = CCRepeatForever:create(CCSequence:create(actionLoopFinger));
    finger:runAction(fingerAction);
end

function oneFingerSwipeCircle(tutorialStepNode)

    local ring = createTapRing();
    ring:setPosition(ccp(369, 47));
    tutorialStepNode:addChild(ring);

    local streak = createStreak();
    streak:setAnchorPoint(ccp(1.0, 0.5));
    streak:setRotation(30.0);
    streak:setPosition(ccp(0, -25));

    local circle1 = CCNode:create();
    circle1:setPosition(ccp(369, 72));
    circle1:addChild(streak);

    tutorialStepNode:addChild(circle1);

    local finger = createOneFinger();
    finger:setPosition(ccp(0, -25));

    local circle2 = CCNode:create();
    circle2:setPosition(ccp(385, 45));
    circle2:addChild(finger);

    tutorialStepNode:addChild(circle2);

    local actionLoopRing = CCArray:create();
    actionLoopRing:addObject(CCDelayTime:create(1.05));
    actionLoopRing:addObject(CCFadeTo:create(0.0, 196));
    actionLoopRing:addObject(CCSpawn:createWithTwoActions(CCEaseSineOut:create(CCScaleTo:create(0.45, 0.175)), CCEaseSineOut:create(CCFadeTo:create(0.45, 0))));
    actionLoopRing:addObject(CCScaleTo:create(0.0, 0.025));
    actionLoopRing:addObject(CCDelayTime:create(0.25));

    local actionLoopCircle1 = CCArray:create();
    actionLoopCircle1:addObject(CCDelayTime:create(1.05));
    actionLoopCircle1:addObject(CCRotateBy:create(0.65, -360.0));
    actionLoopCircle1:addObject(CCDelayTime:create(0.05));

    local actionLoopStreak = CCArray:create();
    actionLoopStreak:addObject(CCDelayTime:create(1.0));
    actionLoopStreak:addObject(CCFadeTo:create(0.05, 128));
    actionLoopStreak:addObject(CCSpawn:createWithTwoActions(CCDelayTime:create(0.65), CCScaleTo:create(0.15, 0.25, 0.5)));
    actionLoopStreak:addObject(CCSpawn:createWithTwoActions(CCFadeTo:create(0.05, 0), CCScaleTo:create(0.05, 0.05, 0.5)));

    local actionLoopCircle2 = CCArray:create();
    actionLoopCircle2:addObject(CCDelayTime:create(1.05));
    actionLoopCircle2:addObject(CCRotateBy:create(0.65, -360.0));
    actionLoopCircle2:addObject(CCDelayTime:create(0.05));

    local actionLoopFinger = CCArray:create();
    actionLoopFinger:addObject(CCDelayTime:create(1.0));
    actionLoopFinger:addObject(CCRotateBy:create(0.05, -20.0));
    actionLoopFinger:addObject(CCRotateBy:create(0.65, 360.0));
    actionLoopFinger:addObject(CCRotateBy:create(0.05, 20.0));

    local ringAction = CCRepeatForever:create(CCSequence:create(actionLoopRing));
    ring:runAction(ringAction);

    local circleAction1 = CCRepeatForever:create(CCSequence:create(actionLoopCircle1));
    circle1:runAction(circleAction1);

    local streakAction = CCRepeatForever:create(CCSequence:create(actionLoopStreak));
    streak:runAction(streakAction);

    local circleAction2 = CCRepeatForever:create(CCSequence:create(actionLoopCircle2));
    circle2:runAction(circleAction2);

    local fingerAction = CCRepeatForever:create(CCSequence:create(actionLoopFinger));
    finger:runAction(fingerAction);
end

function twoFingerSwipeRight(tutorialStepNode)

    local ring1 = createTapRing();
    ring1:setPosition(ccp(342, 43));
    tutorialStepNode:addChild(ring1);

    local ring2 = createTapRing();
    ring2:setPosition(ccp(351, 47));
    tutorialStepNode:addChild(ring2);

    local streak1 = createStreak();
    streak1:setAnchorPoint(ccp(1.0, 0.5));
    streak1:setPosition(ccp(342, 43));
    tutorialStepNode:addChild(streak1);

    local streak2 = createStreak();
    streak2:setAnchorPoint(ccp(1.0, 0.5));
    streak2:setPosition(ccp(351, 47));
    tutorialStepNode:addChild(streak2);

    local fingers = createTwoFingers();
    fingers:setPosition(ccp(360, 20));
    tutorialStepNode:addChild(fingers);

    local actionLoopRing1 = CCArray:create();
    actionLoopRing1:addObject(CCDelayTime:create(1.05));
    actionLoopRing1:addObject(CCFadeTo:create(0.0, 196));
    actionLoopRing1:addObject(CCSpawn:createWithTwoActions(CCEaseSineOut:create(CCScaleTo:create(0.45, 0.175)), CCEaseSineOut:create(CCFadeTo:create(0.45, 0))));
    actionLoopRing1:addObject(CCScaleTo:create(0.0, 0.025));
    actionLoopRing1:addObject(CCDelayTime:create(0.25));

    local actionLoopRing2 = CCArray:create();
    actionLoopRing2:addObject(CCDelayTime:create(1.05));
    actionLoopRing2:addObject(CCFadeTo:create(0.0, 195));
    actionLoopRing2:addObject(CCSpawn:createWithTwoActions(CCEaseSineOut:create(CCScaleTo:create(0.45, 0.175)), CCEaseSineOut:create(CCFadeTo:create(0.45, 0))));
    actionLoopRing2:addObject(CCScaleTo:create(0.0, 0.025));
    actionLoopRing2:addObject(CCDelayTime:create(0.25));

    local actionLoopStreak1 = CCArray:create();
    actionLoopStreak1:addObject(CCDelayTime:create(1.0));
    actionLoopStreak1:addObject(CCFadeTo:create(0.05, 128));
    actionLoopStreak1:addObject(CCSpawn:createWithTwoActions(CCMoveBy:create(0.15, ccp(SWIPE_DISTANCE, 0)), CCScaleTo:create(0.15, 0.45, 0.5)));
    actionLoopStreak1:addObject(CCFadeTo:create(0.05, 0));
    actionLoopStreak1:addObject(CCDelayTime:create(0.25));
    actionLoopStreak1:addObject(CCSpawn:createWithTwoActions(CCMoveBy:create(0.25, ccp(-SWIPE_DISTANCE, 0)), CCScaleTo:create(0.25, 0.05, 0.5)));

    local actionLoopStreak2 = CCArray:create();
    actionLoopStreak2:addObject(CCDelayTime:create(1.0));
    actionLoopStreak2:addObject(CCFadeTo:create(0.05, 127));
    actionLoopStreak2:addObject(CCSpawn:createWithTwoActions(CCMoveBy:create(0.15, ccp(SWIPE_DISTANCE, 0)), CCScaleTo:create(0.15, 0.45, 0.5)));
    actionLoopStreak2:addObject(CCFadeTo:create(0.05, 0));
    actionLoopStreak2:addObject(CCDelayTime:create(0.25));
    actionLoopStreak2:addObject(CCSpawn:createWithTwoActions(CCMoveBy:create(0.25, ccp(-SWIPE_DISTANCE, 0)), CCScaleTo:create(0.25, 0.05, 0.5)));

    local actionLoopFinger = CCArray:create();
    actionLoopFinger:addObject(CCDelayTime:create(1.0));
    actionLoopFinger:addObject(CCRotateBy:create(0.05, -20.0));
    actionLoopFinger:addObject(CCMoveBy:create(0.15, ccp(SWIPE_DISTANCE, 0)));
    actionLoopFinger:addObject(CCRotateBy:create(0.05, 20.0));
    actionLoopFinger:addObject(CCDelayTime:create(0.25));
    actionLoopFinger:addObject(CCMoveBy:create(0.25, ccp(-SWIPE_DISTANCE, 0)));

    local ringAction1 = CCRepeatForever:create(CCSequence:create(actionLoopRing1));
    ring1:runAction(ringAction1);

    local ringAction2 = CCRepeatForever:create(CCSequence:create(actionLoopRing2));
    ring2:runAction(ringAction2);

    local streakAction1 = CCRepeatForever:create(CCSequence:create(actionLoopStreak1));
    streak1:runAction(streakAction1);

    local streakAction2 = CCRepeatForever:create(CCSequence:create(actionLoopStreak2));
    streak2:runAction(streakAction2);

    local fingerAction = CCRepeatForever:create(CCSequence:create(actionLoopFinger));
    fingers:runAction(fingerAction);
end