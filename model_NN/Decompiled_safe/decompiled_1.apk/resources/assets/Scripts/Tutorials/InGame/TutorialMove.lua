require 'Scripts/Tutorials/TutorialFingerUtil.lua'

tutorialStep = UITutorialStep:create(false);
tutorialStep:addCompletionEvent(GameEventType_NextBattlezone);
tutorialStep:addCompletionEvent(GameEventType_LevelEnd);

coachPanel = CoachPanel:create("TutorialMove", true);
coachPanel:setPosition(ccp(5, 5));
tutorialStep:addChild(coachPanel, ZOrder_Behind);

oneFingerTap(tutorialStep);

Level:getCurrentLevel():getHUD():addChild(tutorialStep);

coroutine.yield(tutorialStep);