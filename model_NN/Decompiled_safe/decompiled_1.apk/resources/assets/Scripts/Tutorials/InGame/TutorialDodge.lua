require 'Scripts/Tutorials/TutorialFingerUtil.lua'

tutorialStep = UITutorialStep:create(false);
tutorialStep:addCompletionEvent(GameEventType_NextBattlezone);
tutorialStep:addCompletionEvent(GameEventType_LevelEnd);

coachPanel = CoachPanel:create("TutorialDodge", true);
coachPanel:setPosition(ccp(5, 5));
tutorialStep:addChild(coachPanel, ZOrder_Behind);

oneFingerDoubleTap(tutorialStep);

Level:getCurrentLevel():getHUD():addChild(tutorialStep);

coroutine.yield(tutorialStep);