tutorialStep = UITutorialStep:create(false);
tutorialStep:addCompletionEvent(GameEventType_NextBattlezone);
tutorialStep:addCompletionEvent(GameEventType_LevelEnd);

coachPanel = CoachPanel:create("TutorialThrow2", true);
coachPanel:setPosition(ccp(5, 5));
tutorialStep:addChild(coachPanel, ZOrder_Behind);

Level:getCurrentLevel():getHUD():addChild(tutorialStep);

coroutine.yield(tutorialStep);