nextButtonPrompt = UITutorialPrompt:create(UITutorialTag_MapNextButton, 0);

coachPanel = CoachPanel:create("TutorialMap1", false);
coachPanel:setPosition(ccp(10, 190));
nextButtonPrompt:addChild(coachPanel, ZOrder_Behind);

coroutine.yield(nextButtonPrompt);


mapLevelItemPrompt = UITutorialPrompt:create(UITutorialTag_MapLevelItemButton, 1);

coachPanel = CoachPanel:create("TutorialMap2", false);
coachPanel:setPosition(ccp(10, 10));
mapLevelItemPrompt:addChild(coachPanel, ZOrder_Behind);

mapLevelItemPrompt:setLightRayScale(0.333);
coroutine.yield(mapLevelItemPrompt);


fightButtonPrompt = UITutorialPrompt:create(UITutorialTag_FightButton, 0);

coachPanel = CoachPanel:create("TutorialMap3", false);
coachPanel:setPosition(ccp(10, 190));
fightButtonPrompt:addChild(coachPanel, ZOrder_Behind);

fightButtonPrompt:setLightRayOffset(ccp(-5, 0));
coroutine.yield(fightButtonPrompt);