nextButtonPrompt = UITutorialPrompt:create(UITutorialTag_MapNextButton, 0);

coachPanel = CoachPanel:create("TutorialNeighbourhoodCollection1", false);
coachPanel:setPosition(ccp(10, 190));
nextButtonPrompt:addChild(coachPanel, ZOrder_Behind);

coroutine.yield(nextButtonPrompt);


collectButtonPrompt = UITutorialPrompt:create(UITutorialTag_NeighbourhoodCollectButton, 0);

coachPanel = CoachPanel:create("TutorialNeighbourhoodCollection2", false);
coachPanel:setPosition(ccp(10, 10));
collectButtonPrompt:addChild(coachPanel, ZOrder_Behind);

collectButtonPrompt:setLightRayScale(0.4);
coroutine.yield(collectButtonPrompt);