revivePrompt = UITutorialPrompt:create(UITutorialTag_ReviveButton, 0);

GameEvents:notify(NULL, GameEventType_ReviveTutorial, NULL);

coachPanel = CoachPanel:create("TutorialRevive1", false);
coachPanel:setPosition(ccp(10, 125));
revivePrompt:addChild(coachPanel, ZOrder_Behind);

revivePrompt:setLightRayScale(0.6);
coroutine.yield(revivePrompt);