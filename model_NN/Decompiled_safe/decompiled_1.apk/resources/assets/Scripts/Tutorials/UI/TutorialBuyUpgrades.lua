
-- Check if first damage upgrade hasn't already been purchased.
if not BAMFGame:sharedGame():getPlayerProfile():getActiveHeroProfile():getInventory():hasAmount("Power1", 1) then

    trainingButtonPrompt = UITutorialPrompt:create(UITutorialTag_TrainingButton, 0);

    coachPanel = CoachPanel:create("TutorialBuyUpgrades1", false);
    coachPanel:setPosition(ccp(10, 10));
    trainingButtonPrompt:addChild(coachPanel, ZOrder_Behind);

    coroutine.yield(trainingButtonPrompt);
else

    nextButtonPrompt = UITutorialPrompt:create(UITutorialTag_MapNextButton, 0);

    coachPanel = CoachPanel:create("TutorialBuyUpgrades2", false);
    coachPanel:setPosition(ccp(10, 190));
    nextButtonPrompt:addChild(coachPanel, ZOrder_Behind);

    coroutine.yield(nextButtonPrompt);
end