boosterButtonPrompt = UITutorialPrompt:create(UITutorialTag_BoosterBuyButton, 1);

coachPanel = CoachPanel:create("TutorialBuffs1", false);
coachPanel:setPosition(ccp(10, 240));
boosterButtonPrompt:addChild(coachPanel, ZOrder_Behind);

boosterButtonPrompt:setLightRayScale(0.75);
coroutine.yield(boosterButtonPrompt);


fightButtonPrompt = UITutorialPrompt:create(UITutorialTag_BoosterShopFightButton, 0);

coachPanel = CoachPanel:create("TutorialBuffs2", false);
coachPanel:setPosition(ccp(10, 150));
fightButtonPrompt:addChild(coachPanel, ZOrder_Behind);

fightButtonPrompt:setLightRayOffset(ccp(-5, 0));
coroutine.yield(fightButtonPrompt);