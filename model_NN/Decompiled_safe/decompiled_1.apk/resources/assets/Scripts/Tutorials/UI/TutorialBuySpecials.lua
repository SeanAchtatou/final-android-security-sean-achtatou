skillTabPrompt = UITutorialPrompt:create(UITutorialTag_SkillTabButton, 0);

coachPanel = CoachPanel:create("TutorialBuySpecials1", false);
coachPanel:setPosition(ccp(10, 190));
skillTabPrompt:addChild(coachPanel, ZOrder_Behind);

coroutine.yield(skillTabPrompt);


node = UITutorialPrompt:findTutorialTag(UITutorialTag_SkillItemBuyButton, 0)

-- Check if Fistnado special hasn't already been purchased.
if node:isVisible() then
    skillItemBuyPrompt = UITutorialPrompt:create(UITutorialTag_SkillItemBuyButton, 0);

    coachPanel = CoachPanel:create("TutorialBuySpecials2", false);
    coachPanel:setPosition(ccp(10, 10));
    skillItemBuyPrompt:addChild(coachPanel, ZOrder_Behind);

    coroutine.yield(skillItemBuyPrompt);


    nextButtonPrompt = UITutorialPrompt:create(UITutorialTag_MapNextButton, 0);

    coachPanel = CoachPanel:create("TutorialBuySpecials3", false);
    coachPanel:setPosition(ccp(10, 190));
    nextButtonPrompt:addChild(coachPanel, ZOrder_Behind);

    coroutine.yield(nextButtonPrompt);
else
    nextButtonPrompt = UITutorialPrompt:create(UITutorialTag_MapNextButton, 0);

    coachPanel = CoachPanel:create("TutorialBuySpecials4", false);
    coachPanel:setPosition(ccp(10, 190));
    nextButtonPrompt:addChild(coachPanel, ZOrder_Behind);

    coroutine.yield(nextButtonPrompt);
end