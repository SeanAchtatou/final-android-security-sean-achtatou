require 'Scripts/safe_load.lua'

function parseTutorial(tutorialDefinition)
    local tutorialData = UITutorialData:create(tutorialDefinition.luaPath);

    tutorialData:setLevelOrderRequired(tutorialDefinition.levelOrderRequired);
    tutorialData:setTutorialTrigger(tutorialDefinition.tutorialTrigger);

    if tutorialDefinition.coinBonus ~= nil then
        tutorialData:setCoinBonus(tutorialDefinition.coinBonus);
    end

    if tutorialDefinition.filter ~= nil then
        tutorialData:setFilter(tutorialDefinition.filter);
    end

    if tutorialDefinition.permanent ~= nil then
        tutorialData:setPermanent(tutorialDefinition.permanent);
    end

    if tutorialDefinition.toothBonus ~= nil then
        tutorialData:setToothBonus(tutorialDefinition.toothBonus);
    end

    return tutorialData;
end

local tutorials = CCArray:create();

local tutorialDefinitions = loadFileSafe("Scripts/Data/tutorials.lua",
    {
        GameEventType_BattlezoneStart=GameEventType_BattlezoneStart,
        GameEventType_BoosterShopEnter=GameEventType_BoosterShopEnter,
        GameEventType_GymEnter=GameEventType_GymEnter,
        GameEventType_Revive=GameEventType_Revive,
        GameEventType_LevelEnd=GameEventType_LevelEnd,
        GameEventType_NoMoreLives=GameEventType_NoMoreLives,
    }
);

for key, tutorialDefinition in pairs(tutorialDefinitions) do
    tutorials:addObject(parseTutorial(tutorialDefinition));
end

return tutorials;