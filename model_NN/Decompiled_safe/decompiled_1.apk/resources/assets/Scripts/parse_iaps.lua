require 'Scripts/safe_load.lua'

function parseIAP(iapDefinition)
    local iap = IAPData:create();

    iap:setIconPath(iapDefinition.iconPath);
    iap:setAmount(iapDefinition.amount);
    iap:setConsumable(iapDefinition.consumable);
    iap:setItemKey(iapDefinition.itemKey);
    iap:setProductTitleKey(iapDefinition.titleKey);
    iap:setProductDescriptionKey(iapDefinition.descriptionKey);
    iap:setValueUSD(iapDefinition.valueUSD);

    if iapDefinition.productIdAndroid ~= nil then
        iap:setProductIdAndroid(iapDefinition.productIdAndroid);
    end

    if iapDefinition.productIdBlackBerry ~= nil then
        iap:setProductIdBlackBerry(iapDefinition.productIdBlackBerry);
    end

    if iapDefinition.productIdIOS ~= nil then
        iap:setProductIdIOS(iapDefinition.productIdIOS);
    end

    if iapDefinition.productIdWindowsPhone ~= nil then
        iap:setProductIdWindowsPhone(iapDefinition.productIdWindowsPhone);
    end

    if iapDefinition.bestValue ~= nil then
        iap:setBestValue(iapDefinition.bestValue);
    end

    if iapDefinition.formattedPrice ~= nil then
        iap:setFormattedPrice(iapDefinition.formattedPrice);
    end

    if iapDefinition.freePercentage ~= nil then
        iap:setFreePercentage(iapDefinition.freePercentage);
    end

    if iapDefinition.iconOffset ~= nil then
        iap:setIconOffset(iapDefinition.iconOffset);
    end

    if iapDefinition.shineMaskPath ~= nil then
        iap:setShineMaskPath(iapDefinition.shineMaskPath);
    end

    if iapDefinition.lightRayOffset ~= nil then
        iap:setLightRayOffset(iapDefinition.lightRayOffset);
    end

    if iapDefinition.lightRayScale ~= nil then
        iap:setLightRayScale(iapDefinition.lightRayScale);
    end

    if iapDefinition.mostPopular ~= nil then
        iap:setMostPopular(iapDefinition.mostPopular);
    end

    return iap;
end

local iaps = CCArray:create();

local iapDefinitions = loadFileSafe("Scripts/Data/iaps.lua", {ccp=ccp});
for key, iapDefinition in pairs(iapDefinitions) do
    iaps:addObject(parseIAP(iapDefinition));
end

return iaps;