require 'Scripts/safe_load.lua'

function parseLevel(levelDefinition)
    local level = LevelData:create(levelDefinition.key);

    level:setCoinBonus(levelDefinition.coinBonus);
    level:setLuaPath(levelDefinition.luaPath);
    level:setMapPosition(levelDefinition.mapPosition);
    level:setNeighbourhoodKey(levelDefinition.neighbourhoodKey);
    level:setOrder(levelDefinition.order);
    level:setSoundKey(levelDefinition.soundKey);
    level:setStarThreshold(0, levelDefinition.starThreshold1);
    level:setStarThreshold(1, levelDefinition.starThreshold2);
    level:setStarThreshold(2, levelDefinition.starThreshold3);

    if levelDefinition.healthMeatDropProbability ~= nil then
        level:setHealthMeatDropProbability(levelDefinition.healthMeatDropProbability);
    end

    if levelDefinition.mode ~= nil then
        level:setLevelMode(levelDefinition.mode);
    end

    if levelDefinition.sourceLevelKey ~= nil then
        level:setSourceLevelKey(levelDefinition.sourceLevelKey);
    end

    if levelDefinition.timeLimitInSeconds ~= nil then
        level:setTimeLimitInSeconds(levelDefinition.timeLimitInSeconds);
    end

    if levelDefinition.unlockRequirement ~= nil then
	    level:setUnlockRequirement(levelDefinition.unlockRequirement);
    end

    return level;
end

local levels = CCArray:create();

local levelDefinitions = loadFileSafe("Scripts/Data/levels.lua", 
    {
        ccp=ccp,
        LevelRequirement=LevelRequirement,
        LevelMode_Survival=LevelMode_Survival,
        LevelMode_Timed=LevelMode_Timed,
    }
);
for key, levelDefinition in pairs(levelDefinitions) do
    levels:addObject(parseLevel(levelDefinition));
end

return levels;