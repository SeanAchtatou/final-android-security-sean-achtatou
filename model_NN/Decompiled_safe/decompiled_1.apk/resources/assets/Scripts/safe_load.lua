function loadFileSafe(filename, safeEnvironment)
    local untrustedFunction, message = loadfile(filename);
    
    if not untrustedFunction then
        return nil, message;
    end
    setfenv(untrustedFunction, safeEnvironment);
    return untrustedFunction();
end