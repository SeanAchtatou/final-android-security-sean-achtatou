-- TODO: Use safe sandboxed environment
--require 'Scripts/safe_load.lua'

boosters = ShopGroup:create("Boosters")

item = ShopItem:create("BoosterHealth")
item:setDescription("BoosterHealthDescription")
item:setShopImagePath("Images/UI/HUD/icon_heart.png")
item:getBuyCosts():addCost(ToothCost:create(5))
healthTrigger = PassiveTrigger:create()
healthTrigger:addEffect(MultiplyHealth:create(2.0))
item:getTriggerDispatcher():addPassiveTrigger(healthTrigger)
boosters:getShopObjects():addObject(item)

item = ShopItem:create("BoosterDamage")
item:setDescription("BoosterDamageDescription")
item:setShopImagePath("Images/UI/HUD/icon_damage.png")
item:getBuyCosts():addCost(ToothCost:create(10))
damageTrigger = PassiveTrigger:create()
damageTrigger:addEffect(MultiplyDamage:create(2.0))
item:getTriggerDispatcher():addPassiveTrigger(damageTrigger)
boosters:getShopObjects():addObject(item)

item = ShopItem:create("BoosterCombo")
item:setDescription("BoosterComboDescription")
item:setShopImagePath("Images/UI/HUD/icon_combo.png")
item:getBuyCosts():addCost(ToothCost:create(8))
comboTrigger = PassiveTrigger:create()
comboTrigger:addEffect(MultiplyComboSpecialCount:create(2))
item:getTriggerDispatcher():addPassiveTrigger(comboTrigger)
boosters:getShopObjects():addObject(item)

item = ShopItem:create("BoosterMagnet")
item:setDescription("BoosterMagnetDescription")
item:setShopImagePath("Images/UI/HUD/icon_magnet.png")
item:getBuyCosts():addCost(ToothCost:create(2))
magnetTrigger = PassiveTrigger:create()
magnetTrigger:addEffect(AddPickupRadius:create(500.0))
item:getTriggerDispatcher():addPassiveTrigger(magnetTrigger)
boosters:getShopObjects():addObject(item)

return boosters
