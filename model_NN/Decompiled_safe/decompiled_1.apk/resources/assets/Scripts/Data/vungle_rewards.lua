-- TODO: Use safe sandboxed environment
--require 'Scripts/safe_load.lua'

vungleRewards = CCArray:create()

data = VungleRewardData:create()
data:setShopItemKey("Coin")
data:setAmount(10)
data:setProbabilityWeight(40)
vungleRewards:addObject(data)

data = VungleRewardData:create()
data:setShopItemKey("Coin")
data:setAmount(25)
data:setProbabilityWeight(30)
vungleRewards:addObject(data)

data = VungleRewardData:create()
data:setShopItemKey("Coin")
data:setAmount(50)
data:setProbabilityWeight(20)
vungleRewards:addObject(data)

data = VungleRewardData:create()
data:setShopItemKey("Tooth")
data:setAmount(1)
data:setProbabilityWeight(9)
vungleRewards:addObject(data)

data = VungleRewardData:create()
data:setShopItemKey("Tooth")
data:setAmount(3)
data:setProbabilityWeight(1)
vungleRewards:addObject(data)

return vungleRewards
