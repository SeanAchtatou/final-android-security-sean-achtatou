return
{
    {
        key="Neighbourhood1",
        collectionMapPosition=ccp(741, 1120),
        collectionTimeMs=(60 * 60 * 1000),
        imagePath="Images/UI/Map/map_redlight01.png",
        mapPosition=ccp(191, 795),
        soundKey="Env_RedLight",
        toothReward=1,
        zOrder=4
    },
    {
        key="Neighbourhood2",
        coinReward=30,
        collectionMapPosition=ccp(911, 818),
        collectionTimeMs=(10 * 60 * 1000),
        imagePath="Images/UI/Map/map_portside01.png",
        mapPosition=ccp(448, 632),
        soundKey="Env_Port",
        zOrder=4
    },
    {
        key="Neighbourhood3",
        coinReward=45,
        collectionMapPosition=ccp(758, 412),
        collectionTimeMs=(15 * 60 * 1000),
        imagePath="Images/UI/Map/map_underground01.png",
        mapPosition=ccp(472, 240),
        soundKey="Env_Metro",
        zOrder=2
    },
    {
        key="Neighbourhood4",
        coinReward=60,
        collectionMapPosition=ccp(1271, 956),
        collectionTimeMs=(20 * 60 * 1000),
        imagePath="Images/UI/Map/map_portside02.png",
        mapPosition=ccp(851, 601),
        soundKey="Env_Port",
        zOrder=4
    },
    {
        key="Neighbourhood5",
        coinReward=90,
        collectionMapPosition=ccp(1171, 1297),
        collectionTimeMs=(30 * 60 * 1000),
        imagePath="Images/UI/Map/map_redlight02.png",
        mapPosition=ccp(798, 1048),
        soundKey="Env_RedLight",
        zOrder=4
    },
    {
        key="Neighbourhood6",
        coinReward=120,
        collectionMapPosition=ccp(1671, 825),
        collectionTimeMs=(40 * 60 * 1000),
        imagePath="Images/UI/Map/map_portside03.png",
        mapPosition=ccp(1318, 606),
        soundKey="Env_Port",
        zOrder=4
    },
    {
        key="Neighbourhood7",
        coinReward=180,
        collectionMapPosition=ccp(1560, 431),
        collectionTimeMs=(60 * 60 * 1000),
        imagePath="Images/UI/Map/map_underground02.png",
        mapPosition=ccp(1098, 294),
        soundKey="Env_Metro",
        zOrder=2
    },
    {
        key="Neighbourhood8",
        coinReward=240,
        collectionMapPosition=ccp(1822, 1066),
        collectionTimeMs=(80 * 60 * 1000),
        imagePath="Images/UI/Map/map_redlight03.png",
        mapPosition=ccp(1541, 650),
        soundKey="Env_RedLight",
        zOrder=4
    },
    {
        key="Neighbourhood9",
        coinReward=360,
        collectionMapPosition=ccp(2145, 441),
        collectionTimeMs=(2 * 60 * 60 * 1000),
        imagePath="Images/UI/Map/map_underground03.png",
        mapPosition=ccp(1809, 181),
        soundKey="Env_Metro",
        zOrder=2
    },
    {
        key="Neighbourhood10",
        collectionMapPosition=ccp(2400, 700),
        collectionTimeMs=(5 * 60 * 60 * 1000),
        imagePath="Images/UI/Map/map_portside04.png",
        mapPosition=ccp(1918, 700),
        soundKey="Env_Port",
        toothReward=5,
        zOrder=4
    },
    {
        key="Neighbourhood11",
        coinReward=540,
        collectionMapPosition=ccp(2500, 700),
        collectionTimeMs=(3 * 60 * 60 * 1000),
        imagePath="Images/UI/Map/map_redlight04.png",
        mapPosition=ccp(2495, 645),
        soundKey="Env_RedLight",
        zOrder=4
    },
    {
        key="Neighbourhood12",
        coinReward=720,
        collectionMapPosition=ccp(2600, 400),
        collectionTimeMs=(4 * 60 * 60 * 1000),
        imagePath="Images/UI/Map/map_underground04.png",
        mapPosition=ccp(2441, 187),
        soundKey="Env_Metro",
        zOrder=2
    }
}