return
{
    -- INTRODUCTION ACHIEVEMENTS
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.firstlevelclear",

        eventTrackerType=EventTrackerType_Counter,
        gameEventType=GameEventType_LevelCleared,
        threshold=1
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.firstupgradepurchase",

        eventTrackerType=EventTrackerType_Counter,
        gameEventType=GameEventType_StatUpgradePurchase,
        threshold=1
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.firstspecialpurchase",

        eventTrackerType=EventTrackerType_Counter,
        gameEventType=GameEventType_SpecialAttackPurchase,
        threshold=1
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.firstneighbourhoodcollect",

        eventTrackerType=EventTrackerType_Counter,
        gameEventType=GameEventType_NeighbourhoodCollection,
        threshold=1
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.firstboosterpurchase",

        eventTrackerType=EventTrackerType_Counter,
        gameEventType=GameEventType_BoosterPurchase,
        threshold=1
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.firstrevivepurchase",

        eventTrackerType=EventTrackerType_Counter,
        gameEventType=GameEventType_RevivePurchase,
        threshold=1
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.firstliferefillpurchase",

        eventTrackerType=EventTrackerType_Counter,
        gameEventType=GameEventType_LifeRefillPurchase,
        threshold=1
    },
    -- STORY ACHIEVEMENTS
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.completeneighbourhood1",

        eventTrackerType=EventTrackerType_Threshold,
        gameEventType=GameEventType_NeighbourhoodsCompleted,
        threshold=1
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.completeneighbourhood2",

        eventTrackerType=EventTrackerType_Threshold,
        gameEventType=GameEventType_NeighbourhoodsCompleted,
        threshold=2
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.completeneighbourhood3",

        eventTrackerType=EventTrackerType_Threshold,
        gameEventType=GameEventType_NeighbourhoodsCompleted,
        threshold=3
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.completeneighbourhood4",

        eventTrackerType=EventTrackerType_Threshold,
        gameEventType=GameEventType_NeighbourhoodsCompleted,
        threshold=4
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.completeneighbourhood5",

        eventTrackerType=EventTrackerType_Threshold,
        gameEventType=GameEventType_NeighbourhoodsCompleted,
        threshold=5
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.completeneighbourhood6",

        eventTrackerType=EventTrackerType_Threshold,
        gameEventType=GameEventType_NeighbourhoodsCompleted,
        threshold=6
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.completeneighbourhood7",

        eventTrackerType=EventTrackerType_Threshold,
        gameEventType=GameEventType_NeighbourhoodsCompleted,
        threshold=7
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.completeneighbourhood8",

        eventTrackerType=EventTrackerType_Threshold,
        gameEventType=GameEventType_NeighbourhoodsCompleted,
        threshold=8
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.completeneighbourhood9",

        eventTrackerType=EventTrackerType_Threshold,
        gameEventType=GameEventType_NeighbourhoodsCompleted,
        threshold=9
    },
    -- PROGRESS ACHIEVEMENTS
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.smashprops1",

        eventTrackerType=EventTrackerType_Counter,
        gameEventType=GameEventType_PropDestroyed,
        threshold=50
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.smashprops2",

        eventTrackerType=EventTrackerType_Counter,
        gameEventType=GameEventType_PropDestroyed,
        threshold=500
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.smashprops3",

        eventTrackerType=EventTrackerType_Counter,
        gameEventType=GameEventType_PropDestroyed,
        threshold=5000
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.collectneighbourhoods1",

        eventTrackerType=EventTrackerType_Counter,
        gameEventType=GameEventType_NeighbourhoodCollection,
        threshold=10
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.collectneighbourhoods2",

        eventTrackerType=EventTrackerType_Counter,
        gameEventType=GameEventType_NeighbourhoodCollection,
        threshold=100
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.collectneighbourhoods3",

        eventTrackerType=EventTrackerType_Counter,
        gameEventType=GameEventType_NeighbourhoodCollection,
        threshold=1000
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.dodgeenemies1",

        eventTrackerType=EventTrackerType_Counter,
        gameEventType=GameEventType_Dodge,
        threshold=50
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.dodgeenemies2",

        eventTrackerType=EventTrackerType_Counter,
        gameEventType=GameEventType_Dodge,
        threshold=500
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.dodgeenemies3",

        eventTrackerType=EventTrackerType_Counter,
        gameEventType=GameEventType_Dodge,
        threshold=5000
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.throwenemies1",

        eventTrackerType=EventTrackerType_Counter,
        gameEventType=GameEventType_Throw,
        threshold=50
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.throwenemies2",

        eventTrackerType=EventTrackerType_Counter,
        gameEventType=GameEventType_Throw,
        threshold=500
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.throwenemies3",

        eventTrackerType=EventTrackerType_Counter,
        gameEventType=GameEventType_Throw,
        threshold=5000
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.uppercutenemies1",

        eventTrackerType=EventTrackerType_Counter,
        gameEventType=GameEventType_Uppercut,
        threshold=50
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.uppercutenemies2",

        eventTrackerType=EventTrackerType_Counter,
        gameEventType=GameEventType_Uppercut,
        threshold=500
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.uppercutenemies3",

        eventTrackerType=EventTrackerType_Counter,
        gameEventType=GameEventType_Uppercut,
        threshold=5000
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.punchenemies1",

        eventTrackerType=EventTrackerType_Counter,
        gameEventType=GameEventType_Punch,
        threshold=200
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.punchenemies2",

        eventTrackerType=EventTrackerType_Counter,
        gameEventType=GameEventType_Punch,
        threshold=2000
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.punchenemies3",

        eventTrackerType=EventTrackerType_Counter,
        gameEventType=GameEventType_Punch,
        threshold=20000
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.activatespecial1",

        eventTrackerType=EventTrackerType_Counter,
        gameEventType=GameEventType_SpecialAttack,
        threshold=20
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.activatespecial2",

        eventTrackerType=EventTrackerType_Counter,
        gameEventType=GameEventType_SpecialAttack,
        threshold=200
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.activatespecial3",

        eventTrackerType=EventTrackerType_Counter,
        gameEventType=GameEventType_SpecialAttack,
        threshold=2000
    },
    -- MISCELLANEOUS SKILL ACHIEVEMENTS
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.eatstreetmeat",

        eventTrackerType=EventTrackerType_Counter,
        gameEventType=GameEventType_PickupHealthMeat,
        threshold=10
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.coachlisten",

        eventTrackerType=EventTrackerType_Counter,
        gameEventType=GameEventType_TutorialShow,
        threshold=10
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.heromaxcombo",

        eventTrackerType=EventTrackerType_Threshold,
        gameEventType=GameEventType_HeroMaxCombo,
        threshold=100
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.heromaxscore",

        eventTrackerType=EventTrackerType_Threshold,
        gameEventType=GameEventType_HeroMaxScore,
        threshold=1000000
    },
    {
        achievementIdIOS="com.doublestallion.bamf.achievement.achievementunlocked",

        eventTrackerType=EventTrackerType_Threshold,
        gameEventType=GameEventType_AchievementsUnlocked,
        threshold=1
    },
}
