-- TODO: Use safe sandboxed environment
--require 'Scripts/safe_load.lua'

AdMediator:purgeAdMediator()
adMediator = AdMediator:sharedAdMediator()
playhaven = Playhaven:sharedPlayhaven()
chartboost = Chartboost:sharedChartboost()

--[[
placement = AdPlacementData:new()
placement:setAdvertiser(chartboost)
placement:setPlacementKey("more_games")
placement:setWeight(1)
adMediator:addPlacement(GameEventType_MoreGamesPressed, placement)
--]]

placement = AdPlacementData:new()
placement:setAdvertiser(playhaven)
placement:setPlacementKey("more_games")
placement:setWeight(1)
adMediator:addPlacement(GameEventType_MoreGamesPressed, placement)

placement = AdPlacementData:new()
placement:setAdvertiser(playhaven)
placement:setPlacementKey("main_menu")
placement:setWeight(1)
adMediator:addPlacement(GameEventType_MainMenuOpen, placement)

--[[
placement = AdPlacementData:new()
placement:setAdvertiser(playhaven)
placement:setPlacementKey("pause_menu")
placement:setWeight(1)
adMediator:addPlacement(GameEventType_GamePaused, placement)
--]]

placement = AdPlacementData:new()
placement:setAdvertiser(playhaven)
placement:setPlacementKey("store_open")
placement:setWeight(1)
adMediator:addPlacement(GameEventType_GymEnter, placement)

placement = AdPlacementData:new()
placement:setAdvertiser(playhaven)
placement:setPlacementKey("level_cleared")
placement:setWeight(1)
adMediator:addPlacement(GameEventType_LevelCleared, placement)

placement = AdPlacementData:new()
placement:setAdvertiser(playhaven)
placement:setPlacementKey("level_failed")
placement:setWeight(1)
adMediator:addPlacement(GameEventType_LevelFailed, placement)