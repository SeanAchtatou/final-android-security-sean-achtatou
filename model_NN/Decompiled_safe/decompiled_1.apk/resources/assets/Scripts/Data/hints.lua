-- TODO: Use safe sandboxed environment
--require 'Scripts/safe_load.lua'

hintKeys = CCArray:create()

hintKeys:addObject(CCString:create("HintThrowChicken"));
hintKeys:addObject(CCString:create("HintAvoidBrute"));
hintKeys:addObject(CCString:create("HintThrowFatty"));
hintKeys:addObject(CCString:create("HintInvincibleRoll"));
hintKeys:addObject(CCString:create("HintUseSpecial"));
hintKeys:addObject(CCString:create("HintHighScore"));
hintKeys:addObject(CCString:create("HintNeighbourhoodCollect"));
hintKeys:addObject(CCString:create("HintDamageBoost"));
hintKeys:addObject(CCString:create("HintSpecialBoost"));
hintKeys:addObject(CCString:create("HintMagnetBoost"));
hintKeys:addObject(CCString:create("HintLivesQuit"));
hintKeys:addObject(CCString:create("HintMullets"));
hintKeys:addObject(CCString:create("HintMovember"));
hintKeys:addObject(CCString:create("HintJabUpper"));
hintKeys:addObject(CCString:create("HintUppercut"));
hintKeys:addObject(CCString:create("HintSpecialBar"));
hintKeys:addObject(CCString:create("HintCheat"));

return hintKeys
