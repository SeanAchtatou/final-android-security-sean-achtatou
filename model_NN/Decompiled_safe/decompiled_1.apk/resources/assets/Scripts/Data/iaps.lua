return
{
    {
        -- // TODO: Make final Android IAPs and remove the "testing" before launch.
        productIdAndroid="com.doublestallion.bamf.coindoubler",
        productIdIOS="com.doublestallion.bamf.coindoubler",
        -- // TODO: Create these in the Windows Phone Dev Center.
        productIdWindowsPhone="com.doublestallion.bamf.testing.coindoubler",

        titleKey="CoinDoubler",
        descriptionKey="CoinDoublerDescription",
        formattedPrice="$4.99",
        valueUSD=5,
        iconOffset=ccp(0.0, 5.0),
        iconPath="Images/UI/IAP/shop_coindoubler.png",
        shineMaskPath="Images/UI/IAP/shop_coindoubler_mask.png",
        itemKey="CoinDoubler",
        consumable=false,
        amount=1,
    },
    {
        -- // TODO: Make final Android IAPs and remove the "testing" before launch.
        productIdAndroid="com.doublestallion.bamf.toothpacktier2",
        productIdIOS="com.doublestallion.bamf.toothpacktier2",
        -- // TODO: Create these in the Windows Phone Dev Center.
        productIdWindowsPhone="com.doublestallion.bamf.testing.toothpacktier2",

        titleKey="ToothPackTier2",
        descriptionKey="ToothPackTier2Description",
        formattedPrice="$1.99",
        valueUSD=2,
        iconOffset=ccp(0.0, 0.0),
        iconPath="Images/UI/IAP/shop_teeth01.png",
        shineMaskPath="Images/UI/IAP/shop_teeth01.png",
        itemKey="Tooth",
        consumable=true,
        amount=20,
    },
    {
        -- // TODO: Make final Android IAPs and remove the "testing" before launch.
        productIdAndroid="com.doublestallion.bamf.toothpacktier5",
        productIdIOS="com.doublestallion.bamf.toothpacktier5",
        -- // TODO: Create these in the Windows Phone Dev Center.
        productIdWindowsPhone="com.doublestallion.bamf.testing.toothpacktier5",

        titleKey="ToothPackTier5",
        descriptionKey="ToothPackTier5Description",
        formattedPrice="$4.99",
        valueUSD=5,
        freePercentage=10,
        iconOffset=ccp(0.0, -4.5),
        iconPath="Images/UI/IAP/shop_teeth02.png",
        shineMaskPath="Images/UI/IAP/shop_teeth02.png",
        itemKey="Tooth",
        consumable=true,
        amount=55,
    },
    {
        -- // TODO: Make final Android IAPs and remove the "testing" before launch.
        productIdAndroid="com.doublestallion.bamf.toothpacktier10",
        productIdIOS="com.doublestallion.bamf.toothpacktier10",
        -- // TODO: Create these in the Windows Phone Dev Center.
        productIdWindowsPhone="com.doublestallion.bamf.testing.toothpacktier10",

        titleKey="ToothPackTier10",
        descriptionKey="ToothPackTier10Description",
        formattedPrice="$9.99",
        valueUSD=10,
        freePercentage=15,
        iconOffset=ccp(0.0, -12.0),
        iconPath="Images/UI/IAP/shop_teeth03.png",
        shineMaskPath="Images/UI/IAP/shop_teeth03.png",
        mostPopular=true,
        itemKey="Tooth",
        consumable=true,
        amount=115,
    },
    {
        -- // TODO: Make final Android IAPs and remove the "testing" before launch.
        productIdAndroid="com.doublestallion.bamf.toothpacktier20",
        productIdIOS="com.doublestallion.bamf.toothpacktier20",
        -- // TODO: Create these in the Windows Phone Dev Center.
        productIdWindowsPhone="com.doublestallion.bamf.testing.toothpacktier20",

        titleKey="ToothPackTier20",
        descriptionKey="ToothPackTier20Description",
        formattedPrice="$19.99",
        valueUSD=20,
        freePercentage=25,
        iconOffset=ccp(0.0, -17.5),
        iconPath="Images/UI/IAP/shop_teeth04.png",
        shineMaskPath="Images/UI/IAP/shop_teeth04_mask.png",
        lightRayOffset=ccp(0.0, -17.5),
        lightRayScale=0.79,
        itemKey="Tooth",
        consumable=true,
        amount=250,
    },
    {
        -- // TODO: Make final Android IAPs and remove the "testing" before launch.
        productIdAndroid="com.doublestallion.bamf.toothpacktier50",
        productIdIOS="com.doublestallion.bamf.toothpacktier50",
        -- // TODO: Create these in the Windows Phone Dev Center.
        productIdWindowsPhone="com.doublestallion.bamf.testing.toothpacktier50",

        titleKey="ToothPackTier50",
        descriptionKey="ToothPackTier50Description",
        formattedPrice="$49.99",
        valueUSD=50,
        freePercentage=50,
        iconOffset=ccp(0.0, -15.0),
        iconPath="Images/UI/IAP/shop_teeth05.png",
        shineMaskPath="Images/UI/IAP/shop_teeth05_mask.png",
        lightRayOffset=ccp(0.0, -20.0),
        lightRayScale=0.9,
        itemKey="Tooth",
        consumable=true,
        amount=750,
    },
    {
        -- // TODO: Make final Android IAPs and remove the "testing" before launch.
        productIdAndroid="com.doublestallion.bamf.toothpacktier100",
        productIdIOS="com.doublestallion.bamf.toothpacktier100",
        -- // TODO: Create these in the Windows Phone Dev Center.
        productIdWindowsPhone="com.doublestallion.bamf.testing.toothpacktier100",

        titleKey="ToothPackTier100",
        bestValue=true,
        descriptionKey="ToothPackTier100Description",
        formattedPrice="$99.99",
        valueUSD=100,
        freePercentage=65,
        iconOffset=ccp(0.0, -11.0),
        iconPath="Images/UI/IAP/shop_teeth06.png",
        shineMaskPath="Images/UI/IAP/shop_teeth06_mask.png",
        lightRayOffset=ccp(0.0, -20.0),
        lightRayScale=1.0,
        itemKey="Tooth",
        consumable=true,
        amount=1650,
    },
}
