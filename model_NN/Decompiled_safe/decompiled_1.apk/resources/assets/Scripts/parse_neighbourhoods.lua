require 'Scripts/safe_load.lua'

function parseNeighbourhood(neighbourhoodDefinition)
	local neighbourhood = NeighbourhoodData:create(neighbourhoodDefinition.key);

    neighbourhood:setCollectionMapPosition(neighbourhoodDefinition.collectionMapPosition);
    neighbourhood:setCollectionTimeMs(neighbourhoodDefinition.collectionTimeMs);
    neighbourhood:setImagePath(neighbourhoodDefinition.imagePath);
    neighbourhood:setMapPosition(neighbourhoodDefinition.mapPosition);
    neighbourhood:setSoundKey(neighbourhoodDefinition.soundKey);
    neighbourhood:setZOrder(neighbourhoodDefinition.zOrder);

    if neighbourhoodDefinition.coinReward ~= nil then
        neighbourhood:setCoinReward(neighbourhoodDefinition.coinReward);
    end

    if neighbourhoodDefinition.toothReward ~= nil then
        neighbourhood:setToothReward(neighbourhoodDefinition.toothReward);
    end

	return neighbourhood;
end

local levels = CCArray:create();

local neighbourhoodDefinitions = loadFileSafe("Scripts/Data/neighbourhoods.lua", {ccc3=ccc3, ccp=ccp});
for key, neighbourhoodDefinition in pairs(neighbourhoodDefinitions) do
    levels:addObject(parseNeighbourhood(neighbourhoodDefinition));
end

return levels;