function createRoom (multiplier)
    print("Loading Room" .. multiplier)
    room = Room:create()

    backgroundPath = "Images/Levels/Port" .. (((multiplier - 1) % 5) + 1) .. ".jpg"

    room:setBackground(backgroundPath)
    room:setEnterLocation(CCPoint(-40, 100))
    room:setExitLocation(CCPoint(520, 100))

    battlezone = Battlezone:create()
    room:addChild(battlezone)

    wave = BattlezoneWave:create()
    wave:setMaxEnemiesOnScreen(EnemyType_Boomer, (((multiplier - 1) / 2) + 1))
    wave:setMaxEnemiesOnScreen(EnemyType_Thug, ((multiplier + 1) / 2 + 1))
    battlezone:addChild(wave)

    enemySpawner = EnemySpawner:create("Thug1", CCPoint(-40.0, 10.0), CCPoint(-40.0, 150.0))
    enemySpawner:setMaxSpawnCount(1 * multiplier)
    wave:addChild(enemySpawner)

    enemySpawner = EnemySpawner:create("Boomer1", CCPoint(520.0, 10.0), CCPoint(520.0, 150.0))
    enemySpawner:setMaxSpawnCount(1 * multiplier)
    wave:addChild(enemySpawner)

    enemySpawner = EnemySpawner:create("Thug1", CCPoint(520.0, 10.0), CCPoint(520.0, 150.0))
    enemySpawner:setMaxSpawnCount(1 * multiplier)
    wave:addChild(enemySpawner)

    coroutine.yield(room)
end

counter = 1;
while true do
    createRoom(counter)
    counter = counter + 1
end