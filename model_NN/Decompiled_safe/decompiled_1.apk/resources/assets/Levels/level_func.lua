function loadRoom(filename)
    local reader = CCBReader:new(CCNodeLoaderLibrary:sharedCCNodeLoaderLibrary())
	print("Loading " .. filename)
	local room = reader:readNodeGraphFromFile(filename)
	reader:release();
	
	return room;
end