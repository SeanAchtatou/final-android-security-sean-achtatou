print("Loading Room")
room = Room:create()

room:setEnterLocation(CCPoint(-40, 100))
room:setExitLocation(CCPoint(520, 100))

battlezone = Battlezone:create()

wave = BattlezoneWave:create()
wave:setMaxEnemiesOnScreen(EnemyType_Boomer, 1)
wave:setMaxEnemiesOnScreen(EnemyType_Brute, 1)
wave:setMaxEnemiesOnScreen(EnemyType_Fatty, 1)
wave:setMaxEnemiesOnScreen(EnemyType_Thug, 1)

enemySpawner = EnemySpawner:create("Thug1", CCPoint(520.0, 10.0), CCPoint(520.0, 150.0))
enemySpawner:setMaxSpawnCount(2)
wave:addChild(enemySpawner)

enemySpawner = EnemySpawner:create("Thug2", CCPoint(520.0, 10.0), CCPoint(520.0, 150.0))
enemySpawner:setMaxSpawnCount(2)
wave:addChild(enemySpawner)

battlezone:addChild(wave)


-- Create a prop that must be killed
prop = Prop:create()
prop:setName("Finger")
prop:setSoundKey("Finger")
prop:setPosition(300, 100)
finger = CCSprite:create("Images/UI/HUD/tutorial_hand_one_finger.png");
finger:setScale(0.5);
prop:addChild(finger)
battlezone:addChild(prop)

room:addChild(battlezone)

coroutine.yield(room)