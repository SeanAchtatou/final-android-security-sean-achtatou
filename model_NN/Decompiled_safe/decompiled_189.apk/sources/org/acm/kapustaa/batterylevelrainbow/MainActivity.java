package org.acm.kapustaa.batterylevelrainbow;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import org.acm.kapustaa.batterylevelrainbow.NumberPickerDialog;

public class MainActivity extends Activity implements NumberPickerDialog.OnNumberSetListener {
    protected static final int NUMBER_PICKER_DIALOG = 1;
    protected static boolean appVisible = false;
    protected static int oldLevel = Integer.MIN_VALUE;
    private int currentValue = 0;
    /* access modifiers changed from: private */
    public int itemId;
    private MenuItem mMenuItem = null;
    private int maxValue = Integer.MAX_VALUE;
    private int minValue = Integer.MIN_VALUE;
    /* access modifiers changed from: private */
    public int newValue;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appVisible = true;
        BatteryStatusService.appVisible = true;
        setContentView((int) R.layout.main);
        ((Button) findViewById(R.id.app_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainActivity.this.startStatusService();
            }
        });
        if (!processIntent(getIntent(), true)) {
            updateGraphic();
            startStatusService();
        }
    }

    public void onStart() {
        super.onStart();
    }

    public void onResume() {
        super.onResume();
        appVisible = true;
        BatteryStatusService.appVisible = true;
    }

    public void onPause() {
        super.onPause();
        appVisible = false;
        BatteryStatusService.appVisible = false;
    }

    public void onStop() {
        super.onStop();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onNewIntent(Intent intent) {
        if (!processIntent(intent, false)) {
            setIntent(intent);
            super.onNewIntent(intent);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        menu.findItem(R.id.update_status).setEnabled(true);
        menu.findItem(R.id.stop_status).setEnabled(true);
        menu.findItem(R.id.pause_widget).setEnabled(true);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        if (BatteryStatusService.widgetActive) {
            menu.findItem(R.id.pause_widget).setEnabled(true);
        } else {
            menu.findItem(R.id.pause_widget).setEnabled(false);
        }
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.update_status /*2131230745*/:
                startStatusService();
                return true;
            case R.id.stop_status /*2131230746*/:
                if (!BatteryStatusService.widgetActive) {
                    runMethodInService(2, this);
                }
                finish();
                return true;
            case R.id.pause_widget /*2131230747*/:
                BatteryStatusService.pauseWidget(this);
                return true;
            default:
                if (!setPreferences(id, item)) {
                    return super.onOptionsItemSelected(item);
                }
                startStatusService();
                return true;
        }
    }

    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return createNumberPickerDialog();
            default:
                return null;
        }
    }

    public Dialog onCreateDialog(int id, Bundle args) {
        switch (id) {
            case 1:
                return createNumberPickerDialog();
            default:
                return null;
        }
    }

    public void onPrepareDialog(int id, Dialog dialog) {
        switch (id) {
            case 1:
                prepareNumberPickerDialog(dialog, this.mMenuItem, this.minValue, this.maxValue, this.currentValue);
                return;
            default:
                return;
        }
    }

    public void onPrepareDialog(int id, Dialog dialog, Bundle args) {
        switch (id) {
            case 1:
                prepareNumberPickerDialog(dialog, this.mMenuItem, this.minValue, this.maxValue, this.currentValue);
                return;
            default:
                return;
        }
    }

    public void onNumberSet(int newVal) {
        new UpdateIntPreference(newVal, this.mMenuItem.getItemId()).execute(this);
    }

    /* access modifiers changed from: protected */
    public boolean recalcStatus(Context context, int number) {
        boolean z;
        boolean z2;
        int level = BatteryStatusService.getBatteryLevel(BatteryStatusService.currPercent);
        oldLevel = BatteryStatusService.currLevel;
        boolean updateBatteryStatus = BatteryStatusService.updateBatteryStatus(BatteryStatusService.currPercent, BatteryStatusService.currStatus, level, context, number);
        if (BatteryStatusService.currPercent < BatteryStatusService.currLimit) {
            z = true;
        } else {
            z = false;
        }
        if (BatteryStatusService.currPercent < BatteryStatusService.notifyPercent) {
            z2 = true;
        } else {
            z2 = false;
        }
        boolean refreshNeeded = updateBatteryStatus | (z ^ z2);
        if (refreshNeeded) {
            runMethodInService(3, context);
        }
        return refreshNeeded;
    }

    private boolean processIntent(Intent intent, boolean create) {
        if (intent.getAction() != null) {
            return false;
        }
        if (intent.getIntExtra("org.acm.kapustaa.batteryrainbowapp.METHOD", 0) == 1) {
            updateGraphic();
            if (create) {
                startStatusService();
            }
        } else {
            if (create) {
                updateGraphic();
            }
            startStatusService();
        }
        return true;
    }

    private boolean setPreferences(int id, MenuItem item) {
        switch (id) {
            case R.id.notify_percent /*2131230749*/:
                updateIntPreference(item, 0, 101, BatteryStatusService.notifyPercent);
                return true;
            case R.id.percent1 /*2131230750*/:
                updateIntPreference(item, 0, BatteryStatusService.splitPercent2, BatteryStatusService.splitPercent1);
                return true;
            case R.id.percent2 /*2131230751*/:
                updateIntPreference(item, BatteryStatusService.splitPercent1, BatteryStatusService.splitPercent3, BatteryStatusService.splitPercent2);
                return true;
            case R.id.percent3 /*2131230752*/:
                updateIntPreference(item, BatteryStatusService.splitPercent2, BatteryStatusService.splitPercent4, BatteryStatusService.splitPercent3);
                return true;
            case R.id.percent4 /*2131230753*/:
                updateIntPreference(item, BatteryStatusService.splitPercent3, BatteryStatusService.splitPercent5, BatteryStatusService.splitPercent4);
                return true;
            case R.id.percent5 /*2131230754*/:
                updateIntPreference(item, BatteryStatusService.splitPercent4, 101, BatteryStatusService.splitPercent5);
                return true;
            default:
                return false;
        }
    }

    private void updateIntPreference(MenuItem item, int min, int max, int current) {
        this.mMenuItem = item;
        this.minValue = min;
        this.maxValue = max;
        this.currentValue = current;
        showDialog(1);
    }

    private void runMethodInService(int method, Context context) {
        Intent intent = new Intent(context, BatteryStatusService.class);
        intent.putExtra("org.acm.kapustaa.batteryrainbowapp.METHOD", method);
        startService(intent);
    }

    /* access modifiers changed from: private */
    public void startStatusService() {
        startService(new Intent(this, BatteryStatusService.class));
    }

    /* access modifiers changed from: private */
    public void updateGraphic() {
        int colorRid;
        int colorRid2;
        int percent = BatteryStatusService.currPercent;
        int status = BatteryStatusService.currStatus;
        int level = BatteryStatusService.currLevel;
        Resources rsc = getResources();
        TextView percent1Text = (TextView) findViewById(R.id.text_percent1);
        if (percent1Text != null) {
            percent1Text.setText(String.valueOf(BatteryStatusService.splitPercent1) + "%");
        }
        TextView percent2Text = (TextView) findViewById(R.id.text_percent2);
        if (percent2Text != null) {
            percent2Text.setText(String.valueOf(BatteryStatusService.splitPercent2) + "%");
        }
        TextView percent3Text = (TextView) findViewById(R.id.text_percent3);
        if (percent3Text != null) {
            percent3Text.setText(String.valueOf(BatteryStatusService.splitPercent3) + "%");
        }
        TextView percent4Text = (TextView) findViewById(R.id.text_percent4);
        if (percent4Text != null) {
            percent4Text.setText(String.valueOf(BatteryStatusService.splitPercent4) + "%");
        }
        TextView percent5Text = (TextView) findViewById(R.id.text_percent5);
        if (percent5Text != null) {
            percent5Text.setText(String.valueOf(BatteryStatusService.splitPercent5) + "%");
        }
        TextView notifyPercentText = (TextView) findViewById(R.id.text_notify_percent);
        if (notifyPercentText != null) {
            notifyPercentText.setText(String.valueOf(BatteryStatusService.notifyPercent) + "%");
            if (BatteryStatusService.notifyPercent > percent) {
                colorRid2 = R.color.red;
            } else {
                colorRid2 = R.color.blue;
            }
            notifyPercentText.setTextColor(rsc.getColor(colorRid2));
        }
        ImageView levelImage = (ImageView) findViewById(R.id.main_level);
        if (levelImage != null) {
            levelImage.setImageDrawable(rsc.getDrawable(BatteryStatusService.getLevelGraphic(level)));
        }
        int drawableRid = BatteryStatusService.getStatusGraphic(status);
        if ((status == 5) && (level > BatteryStatusService.splitPercent5)) {
            colorRid = R.color.orange;
            drawableRid = R.drawable.graphic_gray;
        } else if (drawableRid == R.color.light_gray) {
            drawableRid = R.color.brown;
            colorRid = R.color.brown;
        } else {
            colorRid = BatteryStatusService.getStatusTextColor(status);
        }
        ImageView statusImage = (ImageView) findViewById(R.id.main_status);
        if (statusImage != null) {
            statusImage.setImageDrawable(rsc.getDrawable(drawableRid));
        }
        int color = rsc.getColor(colorRid);
        TextView percentText = (TextView) findViewById(R.id.text_percent);
        if (percentText != null) {
            percentText.setText(String.valueOf(percent) + "%");
            percentText.setTextColor(color);
        }
        TextView statusText = (TextView) findViewById(R.id.text_status);
        if (statusText != null) {
            statusText.setText(BatteryStatusService.getStatusText(status, this));
            statusText.setTextColor(color);
        }
    }

    private Dialog createNumberPickerDialog() {
        NumberPickerDialog dialog = new NumberPickerDialog(this, -1);
        dialog.setOnNumberSetListener(this);
        return dialog;
    }

    private void prepareNumberPickerDialog(Dialog dialog, MenuItem item, int min, int max, int current) {
        ((NumberPickerDialog) dialog).setNumberPicker(item, min, max, current);
    }

    /* access modifiers changed from: private */
    public void setIntPreference(Context context, int item, int value) {
        switch (item) {
            case R.id.notify_percent /*2131230749*/:
                BatteryStatusService.notifyPercent = value;
                runMethodInService(4, context);
                return;
            case R.id.percent1 /*2131230750*/:
                BatteryStatusService.splitPercent1 = value;
                runMethodInService(5, context);
                return;
            case R.id.percent2 /*2131230751*/:
                BatteryStatusService.splitPercent2 = value;
                runMethodInService(6, context);
                return;
            case R.id.percent3 /*2131230752*/:
                BatteryStatusService.splitPercent3 = value;
                runMethodInService(7, context);
                return;
            case R.id.percent4 /*2131230753*/:
                BatteryStatusService.splitPercent4 = value;
                runMethodInService(8, context);
                return;
            case R.id.percent5 /*2131230754*/:
                BatteryStatusService.splitPercent5 = value;
                runMethodInService(9, context);
                return;
            default:
                return;
        }
    }

    protected class UpdateIntPreference extends AsyncTask<Context, Void, Boolean> {
        protected UpdateIntPreference(int value, int id) {
            MainActivity.this.newValue = value;
            MainActivity.this.itemId = id;
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(Context... args) {
            Boolean valueOf;
            Context context = args[0];
            int number = BatteryStatusService.getNextNumber();
            synchronized (BatteryStatusService.class) {
                MainActivity.this.setIntPreference(context, MainActivity.this.itemId, MainActivity.this.newValue);
                valueOf = Boolean.valueOf(MainActivity.this.recalcStatus(context, number));
            }
            return valueOf;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean dummy) {
            MainActivity.this.updateGraphic();
            MainActivity.this.dismissDialog(1);
        }
    }
}
