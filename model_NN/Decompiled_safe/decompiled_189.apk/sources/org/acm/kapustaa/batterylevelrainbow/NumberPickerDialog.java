package org.acm.kapustaa.batterylevelrainbow;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class NumberPickerDialog extends AlertDialog implements DialogInterface.OnClickListener {
    private int mInitialValue;
    private OnNumberSetListener mListener;
    private NumberPicker mNumberPicker;

    public interface OnNumberSetListener {
        void onNumberSet(int i);
    }

    public NumberPickerDialog(Context context, int theme) {
        super(context, theme);
        setButton(-1, context.getString(R.string.dialog_set_number), this);
        setButton(-2, context.getString(R.string.dialog_cancel), (DialogInterface.OnClickListener) null);
        View view = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.number_picker_pref, (ViewGroup) null);
        setView(view);
        this.mNumberPicker = (NumberPicker) view.findViewById(R.id.pref_num_picker);
    }

    public void setNumberPicker(MenuItem item, int minValue, int maxValue, int initialValue) {
        ImageView iconImage = (ImageView) findViewById(R.id.pref_icon);
        if (iconImage != null) {
            iconImage.setImageDrawable(item.getIcon());
        }
        TextView titleText = (TextView) findViewById(R.id.pref_title);
        if (titleText != null) {
            titleText.setText(item.getTitleCondensed());
        }
        this.mInitialValue = initialValue;
        this.mNumberPicker.setRange(minValue, maxValue);
        this.mNumberPicker.setCurrent(this.mInitialValue);
    }

    public void setOnNumberSetListener(OnNumberSetListener listener) {
        this.mListener = listener;
    }

    public void onClick(DialogInterface dialog, int which) {
        if (this.mListener != null) {
            this.mListener.onNumberSet(this.mNumberPicker.getCurrent());
        }
    }
}
