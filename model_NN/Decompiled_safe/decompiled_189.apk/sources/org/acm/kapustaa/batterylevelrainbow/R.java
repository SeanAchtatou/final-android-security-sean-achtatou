package org.acm.kapustaa.batterylevelrainbow;

public final class R {

    public static final class attr {
        public static final int defaultValue = 2130771970;
        public static final int endRange = 2130771969;
        public static final int maxValue = 2130771971;
        public static final int startRange = 2130771968;
    }

    public static final class color {
        public static final int black = 2131034120;
        public static final int blue = 2131034117;
        public static final int brown = 2131034119;
        public static final int dark_gray = 2131034123;
        public static final int green = 2131034116;
        public static final int light_gray = 2131034121;
        public static final int medium_gray = 2131034122;
        public static final int orange = 2131034114;
        public static final int red = 2131034113;
        public static final int transparent = 2131034112;
        public static final int violet = 2131034118;
        public static final int white = 2131034124;
        public static final int yellow = 2131034115;
    }

    public static final class drawable {
        public static final int graphic0 = 2130837504;
        public static final int graphic1 = 2130837505;
        public static final int graphic2 = 2130837506;
        public static final int graphic3 = 2130837507;
        public static final int graphic4 = 2130837508;
        public static final int graphic5 = 2130837509;
        public static final int graphic6 = 2130837510;
        public static final int graphic_full = 2130837511;
        public static final int graphic_gray = 2130837512;
        public static final int ic_stat_notify0 = 2130837513;
        public static final int ic_stat_notify1 = 2130837514;
        public static final int ic_stat_notify10 = 2130837515;
        public static final int ic_stat_notify11 = 2130837516;
        public static final int ic_stat_notify12 = 2130837517;
        public static final int ic_stat_notify13 = 2130837518;
        public static final int ic_stat_notify14 = 2130837519;
        public static final int ic_stat_notify15 = 2130837520;
        public static final int ic_stat_notify16 = 2130837521;
        public static final int ic_stat_notify17 = 2130837522;
        public static final int ic_stat_notify18 = 2130837523;
        public static final int ic_stat_notify19 = 2130837524;
        public static final int ic_stat_notify2 = 2130837525;
        public static final int ic_stat_notify20 = 2130837526;
        public static final int ic_stat_notify21 = 2130837527;
        public static final int ic_stat_notify22 = 2130837528;
        public static final int ic_stat_notify23 = 2130837529;
        public static final int ic_stat_notify24 = 2130837530;
        public static final int ic_stat_notify25 = 2130837531;
        public static final int ic_stat_notify3 = 2130837532;
        public static final int ic_stat_notify4 = 2130837533;
        public static final int ic_stat_notify5 = 2130837534;
        public static final int ic_stat_notify6 = 2130837535;
        public static final int ic_stat_notify7 = 2130837536;
        public static final int ic_stat_notify8 = 2130837537;
        public static final int ic_stat_notify9 = 2130837538;
        public static final int icon = 2130837539;
        public static final int landscape_graphic = 2130837540;
        public static final int landscape_small_graphic = 2130837541;
        public static final int menu_icon_pause = 2130837542;
        public static final int menu_icon_pause_disabled = 2130837543;
        public static final int menu_icon_pause_enabled = 2130837544;
        public static final int menu_icon_percent = 2130837545;
        public static final int menu_icon_percent1 = 2130837546;
        public static final int menu_icon_percent2 = 2130837547;
        public static final int menu_icon_percent3 = 2130837548;
        public static final int menu_icon_percent4 = 2130837549;
        public static final int menu_icon_percent5 = 2130837550;
        public static final int menu_icon_stop = 2130837551;
        public static final int portrait_graphic = 2130837552;
        public static final int portrait_small_graphic = 2130837553;
        public static final int timepicker_down_btn = 2130837554;
        public static final int timepicker_down_disabled = 2130837555;
        public static final int timepicker_down_disabled_focused = 2130837556;
        public static final int timepicker_down_normal = 2130837557;
        public static final int timepicker_down_pressed = 2130837558;
        public static final int timepicker_down_selected = 2130837559;
        public static final int timepicker_input = 2130837560;
        public static final int timepicker_input_disabled = 2130837561;
        public static final int timepicker_input_normal = 2130837562;
        public static final int timepicker_input_pressed = 2130837563;
        public static final int timepicker_input_selected = 2130837564;
        public static final int timepicker_up_btn = 2130837565;
        public static final int timepicker_up_disabled = 2130837566;
        public static final int timepicker_up_disabled_focused = 2130837567;
        public static final int timepicker_up_normal = 2130837568;
        public static final int timepicker_up_pressed = 2130837569;
        public static final int timepicker_up_selected = 2130837570;
    }

    public static final class id {
        public static final int app_button = 2131230721;
        public static final int decrement = 2131230735;
        public static final int increment = 2131230733;
        public static final int landscape_main = 2131230732;
        public static final int landscape_small_layout = 2131230744;
        public static final int main_level = 2131230723;
        public static final int main_status = 2131230722;
        public static final int notify_percent = 2131230749;
        public static final int pause_widget = 2131230747;
        public static final int percent1 = 2131230750;
        public static final int percent2 = 2131230751;
        public static final int percent3 = 2131230752;
        public static final int percent4 = 2131230753;
        public static final int percent5 = 2131230754;
        public static final int portrait_main = 2131230720;
        public static final int portrait_small_layout = 2131230739;
        public static final int pref_icon = 2131230736;
        public static final int pref_num_picker = 2131230738;
        public static final int pref_title = 2131230737;
        public static final int preferences = 2131230748;
        public static final int small_image = 2131230740;
        public static final int small_level = 2131230742;
        public static final int small_status = 2131230741;
        public static final int small_text = 2131230743;
        public static final int stop_status = 2131230746;
        public static final int text_notify_percent = 2131230731;
        public static final int text_percent = 2131230724;
        public static final int text_percent1 = 2131230726;
        public static final int text_percent2 = 2131230727;
        public static final int text_percent3 = 2131230728;
        public static final int text_percent4 = 2131230729;
        public static final int text_percent5 = 2131230730;
        public static final int text_status = 2131230725;
        public static final int timepicker_input = 2131230734;
        public static final int update_status = 2131230745;
    }

    public static final class layout {
        public static final int main = 2130903040;
        public static final int number_picker = 2130903041;
        public static final int number_picker_pref = 2130903042;
        public static final int widget_small = 2130903043;
    }

    public static final class menu {
        public static final int main_menu = 2131165184;
    }

    public static final class string {
        public static final int app_name = 2131099648;
        public static final int battery_charge_is_low = 2131099650;
        public static final int battery_is_not_charging = 2131099656;
        public static final int battery_is_not_full = 2131099658;
        public static final int battery_low = 2131099649;
        public static final int battery_not_charging = 2131099657;
        public static final int battery_not_full = 2131099659;
        public static final int battery_status_is_normal = 2131099662;
        public static final int battery_status_is_unknown = 2131099660;
        public static final int battery_status_normal = 2131099663;
        public static final int battery_status_unknown = 2131099661;
        public static final int battery_updates_are_stopped = 2131099666;
        public static final int battery_updates_stopped = 2131099665;
        public static final int charging = 2131099653;
        public static final int dialog_cancel = 2131099668;
        public static final int dialog_set_number = 2131099667;
        public static final int discharging = 2131099652;
        public static final int foreground_service_started = 2131099664;
        public static final int fully_charged = 2131099654;
        public static final int not_charging = 2131099651;
        public static final int notify = 2131099675;
        public static final int notify_percentage = 2131099674;
        public static final int pause_widget = 2131099672;
        public static final int percent_1 = 2131099677;
        public static final int percent_2 = 2131099679;
        public static final int percent_3 = 2131099681;
        public static final int percent_4 = 2131099683;
        public static final int percent_5 = 2131099685;
        public static final int preferences = 2131099673;
        public static final int split_percentage_1 = 2131099676;
        public static final int split_percentage_2 = 2131099678;
        public static final int split_percentage_3 = 2131099680;
        public static final int split_percentage_4 = 2131099682;
        public static final int split_percentage_5 = 2131099684;
        public static final int stop_updates = 2131099671;
        public static final int unknown_status = 2131099655;
        public static final int update = 2131099670;
        public static final int update_status = 2131099669;
    }

    public static final class styleable {
        public static final int[] numberpicker = {R.attr.startRange, R.attr.endRange, R.attr.defaultValue, R.attr.maxValue};
        public static final int numberpicker_defaultValue = 2;
        public static final int numberpicker_endRange = 1;
        public static final int numberpicker_maxValue = 3;
        public static final int numberpicker_startRange = 0;
    }

    public static final class xml {
        public static final int widget_provider = 2130968576;
    }
}
