package org.acm.kapustaa.batterylevelrainbow;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class BatteryStatusService extends IntentService {
    protected static final int CHECK_FOR_NOTIFY_METHOD = 3;
    private static final int HALF_MAX_INT = 1073741824;
    private static final int MAX_SCALE = 10683998;
    public static final String PREFS_NAME = "BatteryRainbowAndGray";
    protected static final int SET_NOTIFY_PERCENT_METHOD = 4;
    protected static final int SET_SPLIT_PERCENT_1_METHOD = 5;
    protected static final int SET_SPLIT_PERCENT_2_METHOD = 6;
    protected static final int SET_SPLIT_PERCENT_3_METHOD = 7;
    protected static final int SET_SPLIT_PERCENT_4_METHOD = 8;
    protected static final int SET_SPLIT_PERCENT_5_METHOD = 9;
    protected static final int STOP_SERVICE_METHOD = 2;
    protected static final int UPDATE_GRAPHIC_METHOD = 1;
    protected static boolean appReady = false;
    protected static boolean appVisible = false;
    protected static int currLevel = 3;
    protected static int currLimit = Integer.MAX_VALUE;
    private static int currMsgNbr = 0;
    private static int currNote = 0;
    protected static int currPercent = 0;
    protected static int currStatus = 1;
    private static int foregroundLevel = 0;
    protected static boolean gotPreferences = false;
    private static NotificationManager mNM;
    private static final Class<?>[] mSetForegroundSignature = {Boolean.TYPE};
    private static final Class<?>[] mStartForegroundSignature = {Integer.TYPE, Notification.class};
    private static final Class<?>[] mStopForegroundSignature = {Boolean.TYPE};
    protected static int notifyPercent = 50;
    protected static int numberStamp = 0;
    protected static boolean preferencesChanged = false;
    protected static boolean rcvrRegistered = false;
    protected static BatteryStatusService serviceContext = null;
    private static boolean serviceEnabled = false;
    protected static int splitPercent1 = 10;
    protected static int splitPercent2 = 30;
    protected static int splitPercent3 = 50;
    protected static int splitPercent4 = 70;
    protected static int splitPercent5 = 90;
    private static int updatedNumber = 0;
    protected static boolean widgetActive = false;
    private BroadcastReceiver batteryState = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("android.intent.action.BATTERY_CHANGED")) {
                int number = BatteryStatusService.getNextNumber();
                synchronized (BatteryStatusService.class) {
                    BatteryStatusService.this.updateStatus(context, intent, number);
                }
            }
        }
    };
    private Method mSetForeground;
    private Object[] mSetForegroundArgs = new Object[1];
    private Method mStartForeground;
    private Object[] mStartForegroundArgs = new Object[2];
    private Method mStopForeground;
    private Object[] mStopForegroundArgs = new Object[1];

    public BatteryStatusService() {
        super("BatteryStatusService");
    }

    public void onCreate() {
        boolean z;
        boolean z2;
        mNM = (NotificationManager) getSystemService("notification");
        try {
            this.mStartForeground = getClass().getMethod("startForeground", mStartForegroundSignature);
        } catch (NoSuchMethodException e) {
            this.mStartForeground = null;
        }
        try {
            this.mStopForeground = getClass().getMethod("stopForeground", mStopForegroundSignature);
        } catch (NoSuchMethodException e2) {
            this.mStopForeground = null;
        }
        if (this.mStartForeground != null) {
            z = true;
        } else {
            z = false;
        }
        if (this.mStopForeground != null) {
            z2 = true;
        } else {
            z2 = false;
        }
        if (z && z2) {
            foregroundLevel = 2;
            Notification notification = getNotification(0, 0, MainActivity.class, SET_SPLIT_PERCENT_2_METHOD);
            this.mStartForegroundArgs[0] = 3;
            this.mStartForegroundArgs[1] = notification;
            invokeMethod(this.mStartForeground, this.mStartForegroundArgs);
            currNote = 3;
        } else {
            try {
                this.mSetForeground = getClass().getMethod("setForeground", mSetForegroundSignature);
            } catch (NoSuchMethodException e3) {
                this.mSetForeground = null;
            }
            if (this.mSetForeground != null) {
                foregroundLevel = 1;
                this.mSetForegroundArgs[0] = Boolean.TRUE;
                invokeMethod(this.mSetForeground, this.mSetForegroundArgs);
            } else {
                foregroundLevel = 0;
            }
        }
        if (!serviceEnabled) {
            appVisible = MainActivity.appVisible;
            widgetActive = WidgetReceiver.widgetActive;
        }
        if (!gotPreferences) {
            initPreferences(this);
        }
        serviceEnabled = true;
    }

    public void onStart(Intent intent, int startId) {
        processIntent(intent);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        processIntent(intent);
        return 1;
    }

    public IBinder onBind(Intent arg0) {
        return null;
    }

    public void onHandleIntent(Intent intent) {
    }

    public void onDestroy() {
        stopStatus();
        if (currNote != 0) {
            cancelNotify(currNote);
        }
        if (serviceEnabled) {
            Notification notification = getNotification(0, 0, MainActivity.class, SET_SPLIT_PERCENT_2_METHOD);
            int noteId = foregroundLevel + 1;
            mNM.notify(noteId, notification);
            currNote = noteId;
        }
        if (foregroundLevel == 2) {
            this.mStopForegroundArgs[0] = Boolean.FALSE;
            invokeMethod(this.mStopForeground, this.mStopForegroundArgs);
        } else if (foregroundLevel == 1) {
            this.mSetForegroundArgs[0] = Boolean.FALSE;
            invokeMethod(this.mSetForeground, this.mSetForegroundArgs);
        }
    }

    /* access modifiers changed from: package-private */
    public void invokeMethod(Method method, Object[] args) {
        try {
            method.invoke(this, args);
        } catch (InvocationTargetException e) {
            Log.w("BatteryRainbowApp", "Unable to invoke method", e);
        } catch (IllegalAccessException e2) {
            Log.w("BatteryRainbowApp", "Unable to invoke method", e2);
        }
    }

    /* access modifiers changed from: protected */
    public void cancelNotify(int noteId) {
        mNM.cancel(noteId);
        currNote = 0;
    }

    protected static int getBatteryLevel(int percent) {
        if (percent < splitPercent3) {
            if (percent < splitPercent1) {
                if (percent < 0) {
                    return 0;
                }
                return 1;
            } else if (percent < splitPercent2) {
                return 2;
            } else {
                return 3;
            }
        } else if (percent < splitPercent5) {
            if (percent < splitPercent4) {
                return SET_NOTIFY_PERCENT_METHOD;
            }
            return SET_SPLIT_PERCENT_1_METHOD;
        } else if (percent > 100) {
            return SET_SPLIT_PERCENT_3_METHOD;
        } else {
            return SET_SPLIT_PERCENT_2_METHOD;
        }
    }

    protected static int getLevelGraphic(int level) {
        if (level < SET_NOTIFY_PERCENT_METHOD) {
            if (level < 2) {
                return level < 1 ? R.drawable.graphic0 : R.drawable.graphic1;
            }
            if (level < 3) {
                return R.drawable.graphic2;
            }
            return R.drawable.graphic3;
        } else if (level >= SET_SPLIT_PERCENT_2_METHOD) {
            return level < SET_SPLIT_PERCENT_3_METHOD ? R.drawable.graphic6 : R.drawable.graphic0;
        } else {
            if (level < SET_SPLIT_PERCENT_1_METHOD) {
                return R.drawable.graphic4;
            }
            return R.drawable.graphic5;
        }
    }

    protected static int getStatusGraphic(int status) {
        if (status == 3) {
            return R.color.black;
        }
        if (status == 2) {
            return R.color.white;
        }
        if (status == SET_SPLIT_PERCENT_1_METHOD) {
            return R.drawable.graphic_full;
        }
        if (status == SET_NOTIFY_PERCENT_METHOD) {
            return R.color.light_gray;
        }
        return R.color.medium_gray;
    }

    protected static String getStatusText(int status, Context context) {
        if (status == 3) {
            return context.getString(R.string.discharging);
        }
        if (status == 2) {
            return context.getString(R.string.charging);
        }
        if (status == SET_SPLIT_PERCENT_1_METHOD) {
            return context.getString(R.string.fully_charged);
        }
        if (status == SET_NOTIFY_PERCENT_METHOD) {
            return context.getString(R.string.not_charging);
        }
        return String.valueOf(context.getString(R.string.unknown_status)) + " (" + status + ")";
    }

    protected static int getStatusTextColor(int status) {
        if (status == 3) {
            return R.color.black;
        }
        if (status == 2) {
            return R.color.blue;
        }
        if (status == SET_SPLIT_PERCENT_1_METHOD) {
            return R.color.green;
        }
        if (status == SET_NOTIFY_PERCENT_METHOD) {
            return R.color.brown;
        }
        return R.color.red;
    }

    /* access modifiers changed from: protected */
    public void checkForNotify(int percent, int status, int level, int noteId) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6;
        boolean z7;
        boolean z8;
        int msgNbr;
        boolean z9;
        boolean z10;
        boolean z11;
        if (percent < notifyPercent) {
            z = true;
        } else {
            z = false;
        }
        if (status == 3) {
            z2 = true;
        } else {
            z2 = false;
        }
        if (status == SET_NOTIFY_PERCENT_METHOD) {
            z3 = true;
        } else {
            z3 = false;
        }
        boolean batteryLow = z & (z2 | z3);
        if (percent < splitPercent5) {
            z4 = true;
        } else {
            z4 = false;
        }
        if (status == SET_SPLIT_PERCENT_1_METHOD) {
            z5 = true;
        } else {
            z5 = false;
        }
        boolean batteryUnfilled = z4 & z5;
        if (batteryLow) {
            msgNbr = 3;
        } else if (batteryUnfilled) {
            msgNbr = SET_SPLIT_PERCENT_1_METHOD;
        } else if (status == SET_NOTIFY_PERCENT_METHOD) {
            msgNbr = SET_NOTIFY_PERCENT_METHOD;
        } else {
            if (status != 2) {
                z6 = true;
            } else {
                z6 = false;
            }
            if (status != 3) {
                z7 = true;
            } else {
                z7 = false;
            }
            boolean z12 = z6 & z7;
            if (status != SET_SPLIT_PERCENT_1_METHOD) {
                z8 = true;
            } else {
                z8 = false;
            }
            if (z12 && z8) {
                msgNbr = 1;
            } else {
                msgNbr = 2;
            }
        }
        if (status != currStatus) {
            z9 = true;
        } else {
            z9 = false;
        }
        boolean z13 = z9 | (level != currLevel);
        if (percent != currPercent) {
            z10 = true;
        } else {
            z10 = false;
        }
        boolean z14 = z13 | z10;
        if (percent < currLimit) {
            z11 = true;
        } else {
            z11 = false;
        }
        if (z14 || (z11 ^ (percent < notifyPercent))) {
            buildNotify(percent, status, level, noteId, msgNbr);
        }
    }

    protected static boolean updateBatteryStatus(int percent, int status, int level, Context context, int number) {
        boolean z;
        boolean z2;
        boolean refreshNeeded;
        if (currPercent != percent) {
            z = true;
        } else {
            z = false;
        }
        boolean z3 = z | (currStatus != status);
        if (currLevel != level) {
            z2 = true;
        } else {
            z2 = false;
        }
        if (z3 || z2) {
            refreshNeeded = true;
        } else {
            refreshNeeded = false;
        }
        currPercent = percent;
        currStatus = status;
        currLevel = level;
        updatedNumber = number;
        return refreshNeeded;
    }

    protected static void initPreferences(Context context) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6;
        boolean z7;
        boolean z8;
        boolean z9;
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        notifyPercent = settings.getInt("notifyPercent", -1);
        splitPercent1 = settings.getInt("splitPercent1", -1);
        splitPercent2 = settings.getInt("splitPercent2", -1);
        splitPercent3 = settings.getInt("splitPercent3", -1);
        splitPercent4 = settings.getInt("splitPercent4", -1);
        splitPercent5 = settings.getInt("splitPercent5", -1);
        if ((notifyPercent < 0) || (notifyPercent > 101)) {
            notifyPercent = 50;
        }
        if (splitPercent1 < 0) {
            z = true;
        } else {
            z = false;
        }
        if (z || (splitPercent1 > 101)) {
            if (10 > splitPercent2) {
                z9 = true;
            } else {
                z9 = false;
            }
            if (z9 && (splitPercent2 >= 0)) {
                splitPercent1 = splitPercent2;
            } else {
                splitPercent1 = 10;
            }
        }
        if (splitPercent5 < 0) {
            z2 = true;
        } else {
            z2 = false;
        }
        if (z2 || (splitPercent5 > 101)) {
            if (90 >= splitPercent4) {
                z8 = true;
            } else {
                z8 = false;
            }
            if (z8 || (splitPercent4 < 0)) {
                splitPercent5 = 90;
            } else {
                splitPercent5 = splitPercent4;
            }
        }
        if (splitPercent2 < 0) {
            z3 = true;
        } else {
            z3 = false;
        }
        if (z3 || (splitPercent2 > 101)) {
            if (30 < splitPercent1) {
                splitPercent2 = splitPercent1;
            } else {
                if (30 > splitPercent3) {
                    z7 = true;
                } else {
                    z7 = false;
                }
                if (z7 && (splitPercent3 >= 0)) {
                    splitPercent2 = splitPercent3;
                } else {
                    splitPercent2 = 30;
                }
            }
        } else if (splitPercent2 < splitPercent1) {
            splitPercent2 = splitPercent1;
        }
        if (splitPercent4 < 0) {
            z4 = true;
        } else {
            z4 = false;
        }
        if (z4 || (splitPercent4 > 101)) {
            if (70 > splitPercent5) {
                splitPercent4 = splitPercent5;
            } else {
                if (70 >= splitPercent3) {
                    z6 = true;
                } else {
                    z6 = false;
                }
                if (z6 || (splitPercent3 < 0)) {
                    splitPercent4 = 70;
                } else {
                    splitPercent4 = splitPercent3;
                }
            }
        } else if (splitPercent4 > splitPercent5) {
            splitPercent4 = splitPercent5;
        }
        if (splitPercent3 < 0) {
            z5 = true;
        } else {
            z5 = false;
        }
        if (z5 || (splitPercent3 > 101)) {
            if (50 < splitPercent2) {
                splitPercent3 = splitPercent2;
            } else if (50 > splitPercent4) {
                splitPercent3 = splitPercent4;
            } else {
                splitPercent3 = 50;
            }
        } else if (splitPercent3 < splitPercent2) {
            splitPercent3 = splitPercent2;
        } else if (splitPercent3 > splitPercent4) {
            splitPercent3 = splitPercent4;
        }
        gotPreferences = true;
    }

    protected static void setPreferenceMethod(Context context, int preference) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, 0).edit();
        switch (preference) {
            case SET_NOTIFY_PERCENT_METHOD /*4*/:
                editor.putInt("notifyPercent", notifyPercent);
                break;
            case SET_SPLIT_PERCENT_1_METHOD /*5*/:
                editor.putInt("splitPercent1", splitPercent1);
                break;
            case SET_SPLIT_PERCENT_2_METHOD /*6*/:
                editor.putInt("splitPercent2", splitPercent2);
                break;
            case SET_SPLIT_PERCENT_3_METHOD /*7*/:
                editor.putInt("splitPercent3", splitPercent3);
                break;
            case SET_SPLIT_PERCENT_4_METHOD /*8*/:
                editor.putInt("splitPercent4", splitPercent4);
                break;
            case SET_SPLIT_PERCENT_5_METHOD /*9*/:
                editor.putInt("splitPercent5", splitPercent5);
                break;
            default:
                return;
        }
        editor.commit();
    }

    protected static void buildGraphic(int percent, int status, int level, Context context) {
        RemoteViews widgetViews = setupWidget(context);
        setupGraphic(widgetViews, percent, status, level, context);
        finishWidget(widgetViews, context);
    }

    protected static void pauseWidget(Context context) {
        WidgetReceiver.widgetActive = false;
        widgetActive = false;
        RemoteViews widgetViews = setupWidget(context);
        widgetViews.setTextViewText(R.id.small_text, "Paused");
        widgetViews.setInt(R.id.small_text, "setTextColor", context.getResources().getColor(R.color.red));
        finishWidget(widgetViews, context);
        WidgetReceiver.oldPercent = Integer.MIN_VALUE;
        WidgetReceiver.oldStatus = Integer.MIN_VALUE;
        WidgetReceiver.oldLevel = Integer.MIN_VALUE;
    }

    protected static RemoteViews setupWidget(Context context) {
        return new RemoteViews(context.getPackageName(), (int) R.layout.widget_small);
    }

    protected static void setupGraphic(RemoteViews widgetViews, int percent, int status, int level, Context context) {
        boolean z;
        boolean z2;
        int colorRid;
        int drawable = getStatusGraphic(status);
        if (status == SET_SPLIT_PERCENT_1_METHOD) {
            z = true;
        } else {
            z = false;
        }
        if (level > splitPercent5) {
            z2 = true;
        } else {
            z2 = false;
        }
        if (z && z2) {
            drawable = R.drawable.graphic_gray;
            colorRid = R.color.orange;
        } else if (drawable == R.color.light_gray) {
            drawable = R.color.brown;
            colorRid = R.color.brown;
        } else {
            colorRid = getStatusTextColor(status);
        }
        widgetViews.setImageViewResource(R.id.small_status, drawable);
        widgetViews.setImageViewResource(R.id.small_level, getLevelGraphic(level));
        widgetViews.setTextViewText(R.id.small_text, String.valueOf(percent) + "%");
        widgetViews.setInt(R.id.small_text, "setTextColor", serviceContext.getResources().getColor(colorRid));
    }

    protected static void finishWidget(RemoteViews widgetViews, Context context) {
        widgetViews.setOnClickPendingIntent(R.id.small_image, PendingIntent.getBroadcast(context, 0, new Intent(context, WidgetReceiver.class), 0));
        AppWidgetManager.getInstance(context).updateAppWidget(new ComponentName(context, WidgetReceiver.class), widgetViews);
    }

    protected static synchronized int getNextNumber() {
        int i;
        synchronized (BatteryStatusService.class) {
            if (numberStamp <= HALF_MAX_INT) {
                numberStamp++;
            } else {
                numberStamp = -1073741824;
            }
            i = numberStamp;
        }
        return i;
    }

    /* access modifiers changed from: private */
    public void updateStatus(Context context, Intent intent, int number) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6;
        boolean z7;
        boolean z8;
        boolean z9;
        int stamp = updatedNumber;
        boolean z10 = (number > 0) & (stamp >= 0) & (number > stamp);
        if (number > 0) {
            z = true;
        } else {
            z = false;
        }
        boolean z11 = z10 | (z & (stamp < 0) & (number - stamp < HALF_MAX_INT));
        if (number <= 0) {
            z2 = true;
        } else {
            z2 = false;
        }
        boolean z12 = z11 | (z2 & (stamp >= 0) & (stamp - number > HALF_MAX_INT));
        if (number <= 0) {
            z3 = true;
        } else {
            z3 = false;
        }
        if (z12 || (z3 & (stamp < 0) & (number > stamp))) {
            int status = intent.getIntExtra("status", 1);
            int percent = getBatteryPercent(intent);
            int level = getBatteryLevel(percent);
            if (status != currStatus) {
                z4 = true;
            } else {
                z4 = false;
            }
            boolean z13 = z4 | (level != currLevel);
            if (percent != currPercent) {
                z5 = true;
            } else {
                z5 = false;
            }
            boolean z14 = z13 | z5;
            if (percent < currLimit) {
                z6 = true;
            } else {
                z6 = false;
            }
            if (z14 || (z6 ^ (percent < notifyPercent))) {
                checkForNotify(percent, status, level, foregroundLevel + 1);
                boolean refreshNeeded = updateBatteryStatus(percent, status, level, context, number);
                if (appVisible && refreshNeeded) {
                    runMethodInActivity(1, context);
                }
                if (widgetActive) {
                    if (percent != WidgetReceiver.oldPercent) {
                        z7 = true;
                    } else {
                        z7 = false;
                    }
                    if (status != WidgetReceiver.oldStatus) {
                        z8 = true;
                    } else {
                        z8 = false;
                    }
                    boolean z15 = z7 | z8;
                    if (level != WidgetReceiver.oldLevel) {
                        z9 = true;
                    } else {
                        z9 = false;
                    }
                    if (z15 || z9) {
                        buildGraphic(percent, status, level, context);
                        WidgetReceiver.oldPercent = percent;
                        WidgetReceiver.oldStatus = status;
                        WidgetReceiver.oldLevel = level;
                    }
                }
            }
        }
    }

    private void processIntent(Intent intent) {
        int method = intent.getIntExtra("org.acm.kapustaa.batteryrainbowapp.METHOD", 0);
        switch (method) {
            case 2:
                stopStatus();
            case 3:
                checkForNotify(currPercent, currStatus, MainActivity.oldLevel, 0);
            case SET_NOTIFY_PERCENT_METHOD /*4*/:
            case SET_SPLIT_PERCENT_1_METHOD /*5*/:
            case SET_SPLIT_PERCENT_2_METHOD /*6*/:
            case SET_SPLIT_PERCENT_3_METHOD /*7*/:
            case SET_SPLIT_PERCENT_4_METHOD /*8*/:
            case SET_SPLIT_PERCENT_5_METHOD /*9*/:
                setPreferenceMethod(this, method);
                break;
        }
        startStatus();
    }

    private void buildNotify(int percent, int status, int level, int noteId, int msgNbr) {
        Notification notification = getNotification(status, level, MainActivity.class, msgNbr);
        if (noteId > 0) {
            mNM.notify(noteId, notification);
            currNote = noteId;
        } else {
            mNM.notify(currNote, notification);
        }
        currLimit = notifyPercent;
    }

    private Notification getNotification(int status, int level, Class<?> cls, int msgNbr) {
        Notification note;
        String title;
        String descr;
        if (msgNbr == SET_SPLIT_PERCENT_2_METHOD) {
            note = new Notification(R.drawable.ic_stat_notify24, "", System.currentTimeMillis());
        } else {
            note = new Notification(getStatusIcon(status, level), "", System.currentTimeMillis());
        }
        note.flags |= 34;
        if (msgNbr >= 3) {
            if (msgNbr > SET_SPLIT_PERCENT_1_METHOD) {
                if (msgNbr > SET_SPLIT_PERCENT_2_METHOD) {
                    title = getString(R.string.battery_status_unknown);
                    descr = getString(R.string.battery_status_is_unknown);
                } else {
                    title = getString(R.string.battery_updates_stopped);
                    descr = getString(R.string.battery_updates_are_stopped);
                }
            } else if (msgNbr == SET_NOTIFY_PERCENT_METHOD) {
                title = getString(R.string.battery_not_charging);
                descr = getString(R.string.battery_is_not_charging);
            } else {
                title = getString(R.string.battery_low);
                descr = getString(R.string.battery_charge_is_low);
            }
        } else if (msgNbr <= 1) {
            title = getString(R.string.battery_status_unknown);
            descr = getString(R.string.battery_status_is_unknown);
        } else if (msgNbr > 2) {
            title = getString(R.string.battery_not_full);
            descr = getString(R.string.battery_is_not_full);
        } else {
            title = getString(R.string.battery_status_normal);
            descr = getString(R.string.battery_status_is_normal);
        }
        Intent intent = new Intent(this, cls);
        intent.addFlags(335544320);
        intent.putExtra("org.acm.kapustaa.batteryrainbowapp.METHOD", "updateGraphic");
        PendingIntent pi = PendingIntent.getActivity(this, 0, intent, 268435456);
        if (msgNbr != currMsgNbr) {
            note.tickerText = descr;
            currMsgNbr = msgNbr;
        }
        note.setLatestEventInfo(this, title, descr, pi);
        return note;
    }

    private void runMethodInActivity(int method, Context context) {
        Intent intnt = new Intent(context, MainActivity.class);
        intnt.addFlags(335544320);
        intnt.putExtra("org.acm.kapustaa.batteryrainbowapp.METHOD", method);
        startActivity(intnt);
    }

    private void startStatus() {
        if (rcvrRegistered) {
            unregisterReceiver(this.batteryState);
        } else {
            serviceContext = this;
        }
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.BATTERY_CHANGED");
        registerReceiver(this.batteryState, intentFilter);
        rcvrRegistered = true;
    }

    private void stopStatus() {
        serviceEnabled = false;
        if (rcvrRegistered) {
            unregisterReceiver(this.batteryState);
            rcvrRegistered = false;
        }
        pauseWidget(this);
        stopSelf();
    }

    private static int getBatteryPercent(Intent intent) {
        boolean z;
        int steps = intent.getIntExtra("level", 0);
        int scale = intent.getIntExtra("steps", 0);
        if (scale < 0) {
            scale = -scale;
            steps = -steps;
        }
        if (scale == 0) {
            z = true;
        } else {
            z = false;
        }
        if (z || (scale == 100)) {
            return steps;
        }
        if (scale <= MAX_SCALE) {
            return ((steps * 200) + scale) / (scale + scale);
        }
        return (int) (((100.0d * ((double) steps)) / ((double) scale)) + 0.5d);
    }

    private static int getStatusIcon(int status, int level) {
        if (status == 3) {
            if (level < SET_NOTIFY_PERCENT_METHOD) {
                if (level < 2) {
                    return level < 1 ? R.drawable.ic_stat_notify24 : R.drawable.ic_stat_notify6;
                }
                if (level < 3) {
                    return R.drawable.ic_stat_notify7;
                }
                return R.drawable.ic_stat_notify8;
            } else if (level >= SET_SPLIT_PERCENT_2_METHOD) {
                return level < SET_SPLIT_PERCENT_3_METHOD ? R.drawable.ic_stat_notify11 : R.drawable.ic_stat_notify24;
            } else {
                if (level < SET_SPLIT_PERCENT_1_METHOD) {
                    return R.drawable.ic_stat_notify9;
                }
                return R.drawable.ic_stat_notify10;
            }
        } else if (status == 2) {
            if (level < SET_NOTIFY_PERCENT_METHOD) {
                if (level < 2) {
                    return level < 1 ? R.drawable.ic_stat_notify24 : R.drawable.ic_stat_notify12;
                }
                if (level < 3) {
                    return R.drawable.ic_stat_notify13;
                }
                return R.drawable.ic_stat_notify14;
            } else if (level >= SET_SPLIT_PERCENT_2_METHOD) {
                return level < SET_SPLIT_PERCENT_3_METHOD ? R.drawable.ic_stat_notify17 : R.drawable.ic_stat_notify24;
            } else {
                if (level < SET_SPLIT_PERCENT_1_METHOD) {
                    return R.drawable.ic_stat_notify15;
                }
                return R.drawable.ic_stat_notify16;
            }
        } else if (status == SET_SPLIT_PERCENT_1_METHOD) {
            if (level < SET_NOTIFY_PERCENT_METHOD) {
                if (level < 2) {
                    return level < 1 ? R.drawable.ic_stat_notify24 : R.drawable.ic_stat_notify18;
                }
                if (level < 3) {
                    return R.drawable.ic_stat_notify9;
                }
                return R.drawable.ic_stat_notify20;
            } else if (level >= SET_SPLIT_PERCENT_2_METHOD) {
                return level < SET_SPLIT_PERCENT_3_METHOD ? R.drawable.ic_stat_notify23 : R.drawable.ic_stat_notify24;
            } else {
                if (level < SET_SPLIT_PERCENT_1_METHOD) {
                    return R.drawable.ic_stat_notify21;
                }
                return R.drawable.ic_stat_notify22;
            }
        } else if (status != SET_NOTIFY_PERCENT_METHOD) {
            return R.drawable.ic_stat_notify24;
        } else {
            if (level < SET_NOTIFY_PERCENT_METHOD) {
                if (level < 2) {
                    return level < 1 ? R.drawable.ic_stat_notify24 : R.drawable.ic_stat_notify0;
                }
                if (level < 3) {
                    return R.drawable.ic_stat_notify1;
                }
                return R.drawable.ic_stat_notify2;
            } else if (level >= SET_SPLIT_PERCENT_2_METHOD) {
                return level < SET_SPLIT_PERCENT_3_METHOD ? R.drawable.ic_stat_notify5 : R.drawable.ic_stat_notify24;
            } else {
                if (level < SET_SPLIT_PERCENT_1_METHOD) {
                    return R.drawable.ic_stat_notify3;
                }
                return R.drawable.ic_stat_notify4;
            }
        }
    }
}
