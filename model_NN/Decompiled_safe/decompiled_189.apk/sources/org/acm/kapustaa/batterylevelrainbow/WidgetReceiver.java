package org.acm.kapustaa.batterylevelrainbow;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;

public class WidgetReceiver extends AppWidgetProvider {
    protected static int oldLevel = Integer.MIN_VALUE;
    protected static int oldPercent = Integer.MIN_VALUE;
    protected static int oldStatus = Integer.MIN_VALUE;
    protected static boolean widgetActive = false;

    public void onEnabled(Context context) {
        if (!BatteryStatusService.gotPreferences) {
            BatteryStatusService.initPreferences(context);
        }
        BatteryStatusService.pauseWidget(context);
        context.startService(new Intent(context, BatteryStatusService.class));
    }

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction() == null) {
            displayStatusUpdates(context);
        } else {
            super.onReceive(context, intent);
        }
    }

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        if (widgetActive) {
            displayStatusUpdates(context);
        } else {
            BatteryStatusService.pauseWidget(context);
        }
    }

    public void onDisabled(Context context) {
        widgetActive = false;
        BatteryStatusService.widgetActive = false;
        super.onDisabled(context);
    }

    private void displayStatusUpdates(Context context) {
        widgetActive = true;
        BatteryStatusService.widgetActive = true;
        BatteryStatusService.buildGraphic(BatteryStatusService.currPercent, BatteryStatusService.currStatus, BatteryStatusService.currLevel, context);
        context.startService(new Intent(context, BatteryStatusService.class));
    }
}
