package com.gau.go.launcherex.theme.windows7;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Process;
import android.view.KeyEvent;

public class classic extends Activity {
    private CustomAlertDialog mDialog;

    class CustomAlertDialog extends AlertDialog {
        public CustomAlertDialog(Context context) {
            super(context);
        }

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            boolean ret = super.onKeyDown(keyCode, event);
            classic.this.finish();
            return ret;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        if (isExistSkin("com.gau.go.launcherex")) {
            startGOLauncher("com.gau.go.launcherex");
            finish();
            return;
        }
        this.mDialog = new CustomAlertDialog(this);
        this.mDialog.setTitle((int) R.string.dialogtitle);
        this.mDialog.setMessage(getResources().getString(R.string.dialogcontent));
        this.mDialog.setButton(-1, getResources().getString(R.string.dialogok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent EMarketintent = new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pname:com.gau.go.launcherex"));
                EMarketintent.setPackage("com.android.vending");
                EMarketintent.setFlags(268435456);
                try {
                    classic.this.startActivity(EMarketintent);
                } catch (ActivityNotFoundException e) {
                    Intent browserIntent = new Intent("android.intent.action.VIEW", Uri.parse("http://61.145.124.93/soft/3GHeart/com.gau.go.launcherex.apk"));
                    browserIntent.setFlags(268435456);
                    try {
                        classic.this.startActivity(browserIntent);
                    } catch (ActivityNotFoundException e2) {
                        e2.printStackTrace();
                    }
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
                classic.this.finish();
            }
        });
        this.mDialog.show();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        Process.killProcess(Process.myPid());
    }

    private boolean isExistSkin(String packageName) {
        try {
            createPackageContext(packageName, 2);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private void startGOLauncher(String packageName) {
        Intent launchIntent = getPackageManager().getLaunchIntentForPackage(packageName);
        if (launchIntent != null) {
            try {
                startActivity(launchIntent);
            } catch (ActivityNotFoundException e) {
            }
        }
    }
}
