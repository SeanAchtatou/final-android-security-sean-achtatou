package com.gau.go.launcherex.theme.windows7;

public final class R {

    public static final class array {
        public static final int backgroundlist = 2130968577;
        public static final int dock_backgroundlist = 2130968578;
        public static final int wallpaperlist = 2130968576;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int allapp = 2130837504;
        public static final int allapp_selected = 2130837505;
        public static final int appfunc_folder_close_up = 2130837506;
        public static final int appfunc_folder_open = 2130837507;
        public static final int appfunc_folderback = 2130837508;
        public static final int appfunc_movetodesk = 2130837509;
        public static final int appfunc_rename = 2130837510;
        public static final int appfunc_screennow = 2130837511;
        public static final int appfunc_screenother = 2130837512;
        public static final int appfunc_tab_current_v = 2130837513;
        public static final int appfunc_tab_focused_v = 2130837514;
        public static final int appfunchome = 2130837515;
        public static final int clearhistory = 2130837516;
        public static final int clearhistory_selected = 2130837517;
        public static final int d_add = 2130837518;
        public static final int d_browser = 2130837519;
        public static final int d_calendar = 2130837520;
        public static final int d_camera = 2130837521;
        public static final int d_contacts = 2130837522;
        public static final int d_del = 2130837523;
        public static final int d_home = 2130837524;
        public static final int d_market = 2130837525;
        public static final int d_messaging = 2130837526;
        public static final int d_music = 2130837527;
        public static final int d_phone = 2130837528;
        public static final int d_settings = 2130837529;
        public static final int default_wallpaper = 2130837530;
        public static final int default_wallpaper_2 = 2130837531;
        public static final int default_wallpaper_2_thumb = 2130837532;
        public static final int default_wallpaper_thumb = 2130837533;
        public static final int dock = 2130837534;
        public static final int dock_thumb = 2130837535;
        public static final int folder_back = 2130837536;
        public static final int folder_back1 = 2130837537;
        public static final int folder_back1_documents = 2130837538;
        public static final int folder_back1_links = 2130837539;
        public static final int folder_back1_music = 2130837540;
        public static final int folder_back1_photos = 2130837541;
        public static final int folder_back1_settings = 2130837542;
        public static final int folder_back1_tasks = 2130837543;
        public static final int folder_back1_videos = 2130837544;
        public static final int folder_back1_windows = 2130837545;
        public static final int folder_back_contacts = 2130837546;
        public static final int folder_back_documents = 2130837547;
        public static final int folder_back_downloads = 2130837548;
        public static final int folder_back_favorites = 2130837549;
        public static final int folder_back_games = 2130837550;
        public static final int folder_back_links = 2130837551;
        public static final int folder_back_music = 2130837552;
        public static final int folder_back_photos = 2130837553;
        public static final int folder_back_pictures = 2130837554;
        public static final int folder_back_user = 2130837555;
        public static final int folder_back_videos = 2130837556;
        public static final int folder_close_light = 2130837557;
        public static final int folder_close_up = 2130837558;
        public static final int folder_open = 2130837559;
        public static final int folderclose = 2130837560;
        public static final int folderopenback = 2130837561;
        public static final int funbg = 2130837562;
        public static final int funbg_thumb = 2130837563;
        public static final int history = 2130837564;
        public static final int history_selected = 2130837565;
        public static final int homebg = 2130837566;
        public static final int icon = 2130837567;
        public static final int icon_001 = 2130837568;
        public static final int icon_002 = 2130837569;
        public static final int icon_003 = 2130837570;
        public static final int icon_004 = 2130837571;
        public static final int icon_005 = 2130837572;
        public static final int icon_006 = 2130837573;
        public static final int icon_007 = 2130837574;
        public static final int icon_008 = 2130837575;
        public static final int icon_009 = 2130837576;
        public static final int icon_010 = 2130837577;
        public static final int icon_011 = 2130837578;
        public static final int icon_012 = 2130837579;
        public static final int icon_013 = 2130837580;
        public static final int icon_014 = 2130837581;
        public static final int icon_015 = 2130837582;
        public static final int icon_016 = 2130837583;
        public static final int icon_017 = 2130837584;
        public static final int icon_018 = 2130837585;
        public static final int icon_019 = 2130837586;
        public static final int icon_020 = 2130837587;
        public static final int icon_021 = 2130837588;
        public static final int icon_022 = 2130837589;
        public static final int icon_023 = 2130837590;
        public static final int icon_024 = 2130837591;
        public static final int icon_025 = 2130837592;
        public static final int icon_026 = 2130837593;
        public static final int icon_027 = 2130837594;
        public static final int icon_028 = 2130837595;
        public static final int icon_029 = 2130837596;
        public static final int icon_030 = 2130837597;
        public static final int icon_browser = 2130837598;
        public static final int icon_calculator = 2130837599;
        public static final int icon_calendar = 2130837600;
        public static final int icon_camcorder = 2130837601;
        public static final int icon_camera = 2130837602;
        public static final int icon_clock = 2130837603;
        public static final int icon_contacts = 2130837604;
        public static final int icon_downloads = 2130837605;
        public static final int icon_email = 2130837606;
        public static final int icon_equalizer = 2130837607;
        public static final int icon_facebook = 2130837608;
        public static final int icon_filemanager = 2130837609;
        public static final int icon_gallery = 2130837610;
        public static final int icon_gmail = 2130837611;
        public static final int icon_googlesearch = 2130837612;
        public static final int icon_gtalk = 2130837613;
        public static final int icon_maps = 2130837614;
        public static final int icon_market = 2130837615;
        public static final int icon_messaging = 2130837616;
        public static final int icon_music = 2130837617;
        public static final int icon_opera = 2130837618;
        public static final int icon_pdf = 2130837619;
        public static final int icon_phone = 2130837620;
        public static final int icon_places = 2130837621;
        public static final int icon_settings = 2130837622;
        public static final int icon_skype = 2130837623;
        public static final int icon_terminal = 2130837624;
        public static final int icon_torch = 2130837625;
        public static final int icon_twitter = 2130837626;
        public static final int icon_winamp = 2130837627;
        public static final int icon_youtube = 2130837628;
        public static final int iconback = 2130837629;
        public static final int iconupon = 2130837630;
        public static final int menu_add = 2130837631;
        public static final int menu_add_folder = 2130837632;
        public static final int menu_back = 2130837633;
        public static final int menu_background = 2130837634;
        public static final int menu_desk_setting = 2130837635;
        public static final int menu_expendbar = 2130837636;
        public static final int menu_feedback = 2130837637;
        public static final int menu_go_market = 2130837638;
        public static final int menu_hide_app = 2130837639;
        public static final int menu_lock_screen = 2130837640;
        public static final int menu_more = 2130837641;
        public static final int menu_recommend = 2130837642;
        public static final int menu_screen_setting = 2130837643;
        public static final int menu_setting = 2130837644;
        public static final int menu_share = 2130837645;
        public static final int menu_sort = 2130837646;
        public static final int menu_theme_settting = 2130837647;
        public static final int menu_unlock_screen = 2130837648;
        public static final int menu_wallpaper = 2130837649;
        public static final int run = 2130837650;
        public static final int run_selected = 2130837651;
        public static final int screennow = 2130837652;
        public static final int screenother = 2130837653;
        public static final int shortcut_light_iconbg = 2130837654;
        public static final int tabbg = 2130837655;
        public static final int themepreview = 2130837656;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class string {
        public static final int app_name = 2131034113;
        public static final int dialogcontent = 2131034115;
        public static final int dialogok = 2131034116;
        public static final int dialogtitle = 2131034114;
        public static final int hello = 2131034112;
        public static final int theme_info = 2131034118;
        public static final int theme_title = 2131034117;
    }
}
