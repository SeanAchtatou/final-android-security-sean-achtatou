package com.admob.android.ads;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import com.admob.android.ads.InterstitialAd;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: AssetDownloader */
public final class u implements h {
    public Hashtable<String, Bitmap> a = new Hashtable<>();
    public HashSet<e> b = new HashSet<>();
    public s c = null;
    public WeakReference<Context> d;
    private a e;

    /* compiled from: AssetDownloader */
    public interface a {
        void k();

        void l();
    }

    public u(a aVar) {
        this.e = aVar;
        this.d = null;
    }

    public final boolean a() {
        return this.b == null || this.b.size() == 0;
    }

    public final void b() {
        if (this.b != null) {
            synchronized (this.b) {
                Iterator<e> it = this.b.iterator();
                while (it.hasNext()) {
                    it.next().f();
                }
            }
        }
    }

    public final void a(JSONObject jSONObject, String str) throws JSONException {
        if (this.b != null) {
            synchronized (this.b) {
                if (jSONObject != null) {
                    Iterator<String> keys = jSONObject.keys();
                    while (keys.hasNext()) {
                        String next = keys.next();
                        JSONObject jSONObject2 = jSONObject.getJSONObject(next);
                        String string = jSONObject2.getString("u");
                        if (!(jSONObject2.optInt("c", 0) == 1) || this.c == null) {
                            a(string, next, str, false);
                        } else {
                            Bitmap a2 = this.c.a(next);
                            if (a2 != null) {
                                this.a.put(next, a2);
                            } else {
                                a(string, next, str, true);
                            }
                        }
                    }
                }
            }
        }
    }

    private void a(String str, String str2, String str3, boolean z) {
        e a2 = g.a(str, str2, str3, this);
        if (z) {
            a2.a((Object) true);
        }
        this.b.add(a2);
    }

    public final void a(e eVar, Exception exc) {
        String str;
        String str2;
        String str3;
        String str4;
        if (exc != null) {
            if (InterstitialAd.c.a(AdManager.LOG, 3)) {
                if (eVar != null) {
                    String b2 = eVar.b();
                    URL c2 = eVar.c();
                    if (c2 != null) {
                        String url = c2.toString();
                        str4 = b2;
                        str3 = url;
                    } else {
                        str4 = b2;
                        str3 = null;
                    }
                } else {
                    str3 = null;
                    str4 = null;
                }
                Log.d(AdManager.LOG, "Failed downloading assets for ad: " + str4 + " " + str3, exc);
            }
        } else if (InterstitialAd.c.a(AdManager.LOG, 3)) {
            if (eVar != null) {
                String b3 = eVar.b();
                URL c3 = eVar.c();
                if (c3 != null) {
                    String url2 = c3.toString();
                    str2 = b3;
                    str = url2;
                } else {
                    str2 = b3;
                    str = null;
                }
            } else {
                str = null;
                str2 = null;
            }
            Log.d(AdManager.LOG, "Failed downloading assets for ad: " + str2 + " " + str);
        }
        c();
    }

    public final void a(e eVar) {
        String b2 = eVar.b();
        byte[] a2 = eVar.a();
        if (a2 != null) {
            Bitmap bitmap = null;
            try {
                bitmap = BitmapFactory.decodeByteArray(a2, 0, a2.length);
            } catch (Throwable th) {
                if (InterstitialAd.c.a(AdManager.LOG, 6)) {
                    Log.e(AdManager.LOG, "couldn't create a Bitmap", th);
                }
            }
            if (bitmap != null) {
                Object g = eVar.g();
                if ((g instanceof Boolean) && ((Boolean) g).booleanValue()) {
                    this.c.a(b2, bitmap);
                }
                this.a.put(b2, bitmap);
                if (this.b != null) {
                    synchronized (this.b) {
                        this.b.remove(eVar);
                    }
                    if (a() && this.e != null) {
                        this.e.k();
                        return;
                    }
                    return;
                }
                return;
            }
            if (InterstitialAd.c.a(AdManager.LOG, 3)) {
                Log.d(AdManager.LOG, "Failed reading asset(" + b2 + ") as a bitmap.");
            }
            c();
            return;
        }
        if (InterstitialAd.c.a(AdManager.LOG, 3)) {
            Log.d(AdManager.LOG, "Failed reading asset(" + b2 + ") for ad");
        }
        c();
    }

    public final void c() {
        if (this.b != null) {
            synchronized (this.b) {
                Iterator<e> it = this.b.iterator();
                while (it.hasNext()) {
                    it.next().e();
                }
                this.b.clear();
                this.b = null;
            }
        }
        d();
        if (this.e != null) {
            this.e.l();
        }
    }

    public final void d() {
        if (this.a != null) {
            this.a.clear();
            this.a = null;
        }
    }
}
