package com.admob.android.ads;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.admob.android.ads.InterstitialAd;
import com.admob.android.ads.u;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: Ad */
public final class j implements u.a {
    private static final int a = Color.rgb(102, 102, 102);
    private static final Rect b = new Rect(0, 0, 0, 0);
    private static final PointF c;
    private static final PointF d;
    private static final PointF e = new PointF(0.5f, 0.5f);
    private static final Matrix f = new Matrix();
    private static final RectF g = new RectF(0.0f, 0.0f, 0.0f, 0.0f);
    private static float h = -1.0f;
    private static Handler i = null;
    private c A;
    private double B;
    private double C;
    private q D;
    private b E;
    private boolean F;
    private s G;
    private String j;
    private boolean k;
    private boolean l;
    private Vector<String> m = new Vector<>();
    private Rect n;
    private long o = 0;
    private int p;
    private int q;
    private WeakReference<m> r;
    private k s;
    private int t;
    private int u;
    private int v;
    private JSONObject w;
    private u x;
    private int y;
    private Vector<Bitmap> z;

    /* compiled from: Ad */
    public interface c {
        void a();
    }

    static /* synthetic */ void a(j jVar, JSONArray jSONArray) {
        if (jVar.s != null) {
            try {
                jVar.s.setPadding(0, 0, 0, 0);
                ArrayList arrayList = new ArrayList();
                int i2 = 0;
                while (i2 < jSONArray.length()) {
                    View b2 = jVar.b(jSONArray.getJSONObject(i2));
                    if (b2 == null) {
                        jVar.r();
                        return;
                    } else {
                        arrayList.add(b2);
                        i2++;
                    }
                }
                long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
                for (int i3 = 0; i3 < arrayList.size(); i3++) {
                    View view = (View) arrayList.get(i3);
                    Animation animation = view.getAnimation();
                    if (animation != null) {
                        animation.setStartTime(currentAnimationTimeMillis);
                    }
                    jVar.s.addView(view);
                }
                jVar.s.invalidate();
                jVar.s.requestLayout();
                if (!jVar.F) {
                    jVar.s.b();
                }
                jVar.x.d();
                if (jVar.o()) {
                    jVar.q();
                }
            } catch (JSONException e2) {
                jVar.r();
            }
        }
    }

    /* compiled from: Ad */
    public enum a {
        CLICK_TO_MAP("map"),
        CLICK_TO_VIDEO("video"),
        CLICK_TO_APP("app"),
        CLICK_TO_BROWSER("url"),
        CLICK_TO_CALL("call"),
        CLICK_TO_MUSIC("itunes"),
        CLICK_TO_CANVAS("canvas"),
        CLICK_TO_CONTACT("contact"),
        CLICK_TO_INTERACTIVE_VIDEO("movie"),
        CLICK_TO_FULLSCREEN_BROWSER("screen");
        
        private String k;

        private a(String str) {
            this.k = str;
        }

        public final String toString() {
            return this.k;
        }

        public static a a(String str) {
            for (a aVar : values()) {
                if (aVar.toString().equals(str)) {
                    return aVar;
                }
            }
            return null;
        }
    }

    /* compiled from: Ad */
    public enum b {
        VIEW("view"),
        INTERSTITIAL("full_screen"),
        BAR("bar");
        
        private String d;

        private b(String str) {
            this.d = str;
        }

        public final String toString() {
            return this.d;
        }
    }

    static {
        PointF pointF = new PointF(0.0f, 0.0f);
        c = pointF;
        d = pointF;
    }

    public static void a(Handler handler) {
        i = handler;
    }

    public final q a() {
        return this.D;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, float):float
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, int):int
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, android.graphics.Matrix):android.graphics.Matrix
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, android.graphics.PointF):android.graphics.PointF
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, android.graphics.Rect):android.graphics.Rect
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, android.graphics.RectF):android.graphics.RectF
      com.admob.android.ads.j.a(int, int, android.view.View):com.admob.android.ads.aj
      com.admob.android.ads.j.a(float, float, android.view.View):com.admob.android.ads.ap
      com.admob.android.ads.j.a(android.widget.ImageView, android.graphics.Bitmap, org.json.JSONObject):void
      com.admob.android.ads.j.a(org.json.JSONObject, android.view.animation.Animation, android.view.animation.AnimationSet):void
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, float):float */
    public static j a(m mVar, Context context, JSONObject jSONObject, int i2, int i3, int i4, k kVar, b bVar) {
        boolean a2;
        if (jSONObject == null || jSONObject.length() == 0) {
            return null;
        }
        j jVar = new j();
        jVar.E = bVar;
        jVar.a(mVar);
        jVar.t = i2;
        jVar.u = i3;
        jVar.v = i4;
        jVar.s = kVar;
        jVar.G = s.a(context);
        jVar.x.c = jVar.G;
        if (jVar.E == b.INTERSTITIAL) {
            float a3 = a(jSONObject, "timeout", 0.0f);
            if (a3 > 0.0f) {
                h = a3;
                SharedPreferences.Editor edit = context.getSharedPreferences("admob_prefs", 2).edit();
                edit.putFloat("timeout", h);
                edit.commit();
            }
            jVar.D.a(context, jSONObject, jVar.x);
            jVar.D.a.l = true;
            a aVar = jVar.D.a.a;
            if (aVar == a.CLICK_TO_FULLSCREEN_BROWSER || aVar == a.CLICK_TO_INTERACTIVE_VIDEO) {
                jVar.x.b();
                if (jVar.x.a()) {
                    jVar.k();
                }
                a2 = true;
            } else {
                a2 = false;
            }
        } else {
            a2 = jVar.a(context, jSONObject);
        }
        if (!a2) {
            return null;
        }
        return jVar;
    }

    protected j() {
        a((m) null);
        this.j = null;
        this.n = null;
        this.p = -1;
        this.q = -1;
        this.A = null;
        this.u = -1;
        this.t = -1;
        this.v = -16777216;
        this.x = new u(this);
        this.y = 0;
        this.z = new Vector<>();
        this.B = -1.0d;
        this.C = -1.0d;
        this.D = new q();
        this.E = b.BAR;
        h = 5.0f;
        this.F = false;
        this.G = null;
    }

    public final double b() {
        return this.B;
    }

    private void a(String str) {
        if (str != null && !"".equals(str)) {
            this.m.add(str);
        }
    }

    public final int a(int i2) {
        double d2 = (double) i2;
        if (this.C > 0.0d) {
            d2 *= this.C;
        }
        return (int) d2;
    }

    public static int a(int i2, double d2) {
        double d3 = (double) i2;
        if (d2 > 0.0d) {
            d3 *= d2;
        }
        return (int) d3;
    }

    /* access modifiers changed from: package-private */
    public final Rect a(Rect rect) {
        Rect rect2 = new Rect(rect);
        if (this.C > 0.0d) {
            rect2.left = a(rect.left);
            rect2.top = a(rect.top);
            rect2.right = a(rect.right);
            rect2.bottom = a(rect.bottom);
        }
        return rect2;
    }

    public final k c() {
        return this.s;
    }

    public final void a(k kVar) {
        this.s = kVar;
    }

    public final long d() {
        return this.o;
    }

    private void a(m mVar) {
        this.r = new WeakReference<>(mVar);
    }

    public final boolean e() {
        return this.k;
    }

    public final void a(c cVar) {
        this.A = cVar;
    }

    public final int f() {
        return this.p;
    }

    public final int g() {
        return this.q;
    }

    public final Rect h() {
        if (this.n == null) {
            this.n = new Rect(0, 0, this.p, this.q);
        }
        return this.n;
    }

    /* access modifiers changed from: package-private */
    public final void i() {
        Iterator<Bitmap> it = this.z.iterator();
        while (it.hasNext()) {
            Bitmap next = it.next();
            if (next != null) {
                next.recycle();
            }
        }
        this.z.clear();
        if (this.D != null) {
            this.D.a();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0029  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0064  */
    /* JADX WARNING: Removed duplicated region for block: B:26:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(org.json.JSONObject r5) {
        /*
            r4 = this;
            r1 = 4
            java.lang.String r3 = "AdMobSDK"
            boolean r0 = r4.l
            if (r0 == 0) goto L_0x003a
            java.lang.String r0 = "AdMobSDK"
            boolean r0 = com.admob.android.ads.InterstitialAd.c.a(r3, r1)
            if (r0 == 0) goto L_0x0016
            java.lang.String r0 = "AdMobSDK"
            java.lang.String r0 = "Ad clicked again.  Stats on admob.com will only reflect the first click."
            android.util.Log.i(r3, r0)
        L_0x0016:
            r1 = 0
            com.admob.android.ads.k r0 = r4.s
            if (r0 == 0) goto L_0x0075
            com.admob.android.ads.k r0 = r4.s
            android.content.Context r0 = r0.getContext()
            boolean r2 = r0 instanceof android.app.Activity
            if (r2 == 0) goto L_0x0075
            android.app.Activity r0 = (android.app.Activity) r0
        L_0x0027:
            if (r0 == 0) goto L_0x0064
            com.admob.android.ads.q r1 = r4.D
            com.admob.android.ads.k r2 = r4.s
            r1.a(r0, r2)
        L_0x0030:
            com.admob.android.ads.j$c r0 = r4.A
            if (r0 == 0) goto L_0x0039
            com.admob.android.ads.j$c r0 = r4.A
            r0.a()
        L_0x0039:
            return
        L_0x003a:
            r0 = 1
            r4.l = r0
            java.lang.String r0 = "AdMobSDK"
            boolean r0 = com.admob.android.ads.InterstitialAd.c.a(r3, r1)
            if (r0 == 0) goto L_0x004c
            java.lang.String r0 = "AdMobSDK"
            java.lang.String r0 = "Ad clicked."
            android.util.Log.i(r3, r0)
        L_0x004c:
            com.admob.android.ads.k r0 = r4.s
            if (r0 == 0) goto L_0x0016
            com.admob.android.ads.k r0 = r4.s
            android.content.Context r0 = r0.getContext()
            com.admob.android.ads.q r1 = r4.D
            java.lang.String r0 = com.admob.android.ads.AdManager.getUserId(r0)
            com.admob.android.ads.r r1 = r1.a
            java.util.Vector<com.admob.android.ads.w> r1 = r1.c
            com.admob.android.ads.q.a(r1, r5, r0)
            goto L_0x0016
        L_0x0064:
            java.lang.String r0 = "AdMobSDK"
            r0 = 3
            boolean r0 = com.admob.android.ads.InterstitialAd.c.a(r3, r0)
            if (r0 == 0) goto L_0x0030
            java.lang.String r0 = "AdMobSDK"
            java.lang.String r0 = "Context null, not able to finish click."
            android.util.Log.d(r3, r0)
            goto L_0x0030
        L_0x0075:
            r0 = r1
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.admob.android.ads.j.a(org.json.JSONObject):void");
    }

    /* access modifiers changed from: package-private */
    public final void j() {
        if (this.s != null) {
            Context context = this.s.getContext();
            Iterator<String> it = this.m.iterator();
            while (it.hasNext()) {
                g.a(it.next(), "impression_request", AdManager.getUserId(context)).f();
            }
        }
    }

    public final String toString() {
        String str = this.j;
        if (str == null) {
            return "";
        }
        return str;
    }

    public final boolean equals(Object obj) {
        if (obj instanceof j) {
            return toString().equals(((j) obj).toString());
        }
        return false;
    }

    public final int hashCode() {
        return toString().hashCode();
    }

    private boolean a(Context context, JSONObject jSONObject) {
        JSONObject optJSONObject = jSONObject.optJSONObject("o");
        if (optJSONObject != null) {
            this.D.a(context, optJSONObject, (u) null);
        } else {
            this.j = jSONObject.optString("text", null);
            String optString = jSONObject.optString("6", null);
            this.D.a.b = jSONObject.optString("8", null);
            this.D.a.a = a.a(optString);
            JSONArray optJSONArray = jSONObject.optJSONArray("ac");
            if (optJSONArray != null) {
                this.D.a(context, optJSONArray);
            }
            JSONObject optJSONObject2 = jSONObject.optJSONObject("ac");
            if (optJSONObject2 != null) {
                this.D.a(context, optJSONObject2);
            }
        }
        String optString2 = jSONObject.optString("jsonp_url", null);
        String optString3 = jSONObject.optString("tracking_url", null);
        this.D.a.a(optString2, true);
        this.D.a.a(optString3, false);
        if (jSONObject.has("refreshInterval")) {
            this.B = jSONObject.optDouble("refreshInterval");
        }
        if (jSONObject.has("density")) {
            this.C = jSONObject.optDouble("density");
        } else {
            this.C = (double) k.d();
        }
        PointF a2 = a(jSONObject, "d", (PointF) null);
        if (a2 == null) {
            a2 = new PointF(320.0f, 48.0f);
        }
        if (a2.x < 0.0f || a2.y < 0.0f) {
            return false;
        }
        this.p = (int) a2.x;
        this.q = (int) a2.y;
        String optString4 = jSONObject.optString("cpm_url", null);
        if (optString4 != null) {
            this.k = true;
            a(optString4);
        }
        String optString5 = jSONObject.optString("tracking_pixel", null);
        if (optString5 != null) {
            try {
                new URL(optString5);
            } catch (MalformedURLException e2) {
                try {
                    optString5 = URLEncoder.encode(optString5, "UTF-8");
                } catch (UnsupportedEncodingException e3) {
                }
            }
        }
        if (optString5 != null) {
            a(optString5);
        }
        JSONObject optJSONObject3 = jSONObject.optJSONObject("markup");
        if (this.D.a.a == a.CLICK_TO_CANVAS && !(this.s.getContext() instanceof Activity)) {
            r();
            return false;
        } else if (optJSONObject3 == null) {
            return false;
        } else {
            q qVar = this.D;
            if (!(qVar.a.c != null && qVar.a.c.size() > 0)) {
                if (InterstitialAd.c.a(AdManager.LOG, 6)) {
                    Log.e(AdManager.LOG, "Bad response:  didn't get clickURLString.  erroring out.");
                }
                return false;
            }
            this.w = optJSONObject3;
            try {
                this.x.a(this.w.optJSONObject("$"), this.s != null ? AdManager.getUserId(this.s.getContext()) : null);
                p();
                double optDouble = this.w.optDouble("itid");
                if (optDouble > 0.0d) {
                    this.o = (long) (optDouble * 1000.0d);
                }
            } catch (JSONException e4) {
                if (InterstitialAd.c.a(AdManager.LOG, 6)) {
                    Log.e(AdManager.LOG, "Could not read in the flex ad.", e4);
                }
            }
            this.x.b();
            if (this.x.a()) {
                k();
            }
            return true;
        }
    }

    public final void a(boolean z2) {
        this.y--;
        if (!z2) {
            this.x.c();
        } else if (o()) {
            q();
        }
    }

    private boolean o() {
        return this.y <= 0;
    }

    private void p() {
        Rect rect = new Rect(0, 0, this.p, this.q);
        if (this.w.has("ta")) {
            try {
                JSONArray jSONArray = this.w.getJSONArray("ta");
                int i2 = jSONArray.getInt(0);
                int i3 = jSONArray.getInt(1);
                Rect rect2 = new Rect(i2, i3, jSONArray.getInt(2) + i2, jSONArray.getInt(3) + i3);
                if (Math.abs(rect2.width()) >= 44 && Math.abs(rect2.height()) >= 44) {
                    rect = rect2;
                }
            } catch (JSONException e2) {
                if (InterstitialAd.c.a(AdManager.LOG, 3)) {
                    Log.d(AdManager.LOG, "could not read in the touchable area for the ad.");
                }
            }
        }
        this.n = rect;
    }

    public final void k() {
        if (this.D != null) {
            if (this.D.c()) {
                this.D.a(this.x.a);
            }
            this.D.b();
        }
        if (this.w != null) {
            JSONObject jSONObject = this.w;
            this.w = null;
            try {
                JSONArray optJSONArray = jSONObject.optJSONArray("v");
                if (optJSONArray != null) {
                    d dVar = new d(this, optJSONArray);
                    if (i != null) {
                        i.post(dVar);
                    }
                } else {
                    r();
                }
            } catch (JSONException e2) {
                if (InterstitialAd.c.a(AdManager.LOG, 3)) {
                    Log.d(AdManager.LOG, "couldn't construct the views.", e2);
                }
            }
            u uVar = this.x;
            if (uVar.b != null) {
                synchronized (uVar.b) {
                    uVar.b.clear();
                    uVar.b = null;
                }
            }
        } else if (o()) {
            q();
        }
    }

    public final void l() {
        this.w = null;
        if (InterstitialAd.c.a(AdManager.LOG, 4)) {
            Log.i(AdManager.LOG, "assetsDidFailToLoad()");
        }
        r();
    }

    private void q() {
        m mVar = this.r.get();
        if (mVar != null) {
            mVar.a(this);
        }
        if (this.G != null) {
            s.a();
        }
    }

    public final boolean m() {
        return this.F;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, float):float
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, int):int
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, android.graphics.Matrix):android.graphics.Matrix
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, android.graphics.PointF):android.graphics.PointF
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, android.graphics.Rect):android.graphics.Rect
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, android.graphics.RectF):android.graphics.RectF
      com.admob.android.ads.j.a(int, int, android.view.View):com.admob.android.ads.aj
      com.admob.android.ads.j.a(float, float, android.view.View):com.admob.android.ads.ap
      com.admob.android.ads.j.a(android.widget.ImageView, android.graphics.Bitmap, org.json.JSONObject):void
      com.admob.android.ads.j.a(org.json.JSONObject, android.view.animation.Animation, android.view.animation.AnimationSet):void
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, float):float */
    private View b(JSONObject jSONObject) {
        boolean z2;
        af afVar;
        boolean z3;
        af afVar2;
        Typeface typeface;
        try {
            if (this.s == null || jSONObject == null) {
                return null;
            }
            String string = jSONObject.getString("t");
            Rect a2 = a(a(jSONObject, "f", b));
            if ("l".equals(string)) {
                if (this.s != null) {
                    String string2 = jSONObject.getString("x");
                    float a3 = a(jSONObject, "fs", 13.0f);
                    JSONArray optJSONArray = jSONObject.optJSONArray("fa");
                    Typeface typeface2 = Typeface.DEFAULT;
                    if (optJSONArray != null) {
                        int i2 = 0;
                        for (int i3 = 0; i3 < optJSONArray.length(); i3++) {
                            String string3 = optJSONArray.getString(i3);
                            if ("b".equals(string3)) {
                                i2 |= 1;
                            } else if ("i".equals(string3)) {
                                i2 |= 2;
                            } else if ("m".equals(string3)) {
                                typeface2 = Typeface.MONOSPACE;
                            } else if ("s".equals(string3)) {
                                typeface2 = Typeface.SERIF;
                            } else if ("ss".equals(string3)) {
                                typeface2 = Typeface.SANS_SERIF;
                            }
                        }
                        typeface = Typeface.create(typeface2, i2);
                    } else {
                        typeface = typeface2;
                    }
                    int i4 = this.t;
                    if (jSONObject.has("fco")) {
                        int a4 = a(jSONObject, "fco", i4);
                        if (a4 != i4) {
                            i4 = a4;
                        }
                    } else {
                        i4 = jSONObject.optInt("fc", 0) == 1 ? this.u : this.t;
                    }
                    boolean optBoolean = jSONObject.optBoolean("afstfw", true);
                    float a5 = a(jSONObject, "mfs", 8.0f);
                    int optInt = jSONObject.optInt("nol", 1);
                    af afVar3 = new af(this.s.getContext(), k.d());
                    afVar3.b = optBoolean;
                    afVar3.a = afVar3.c * a5;
                    afVar3.setBackgroundColor(0);
                    afVar3.setText(string2);
                    afVar3.setTextColor(i4);
                    afVar3.setTextSize(a3);
                    afVar3.setTypeface(typeface);
                    afVar3.setLines(optInt);
                    afVar2 = afVar3;
                } else {
                    afVar2 = null;
                }
                z3 = true;
                afVar = afVar2;
                z2 = true;
            } else if ("bg".equals(string)) {
                z3 = false;
                afVar = a(jSONObject, a2);
                z2 = false;
            } else if ("i".equals(string)) {
                ImageView imageView = null;
                if (this.s != null) {
                    imageView = null;
                    String string4 = jSONObject.getString("$");
                    if (string4 != null) {
                        u uVar = this.x;
                        Bitmap bitmap = uVar.a != null ? uVar.a.get(string4) : null;
                        if (bitmap != null) {
                            ImageView imageView2 = new ImageView(this.s.getContext());
                            imageView2.setScaleType(ImageView.ScaleType.FIT_XY);
                            if (jSONObject.optBoolean("b", false)) {
                                a(imageView2, bitmap, jSONObject);
                                imageView = imageView2;
                            } else {
                                this.z.add(bitmap);
                                imageView2.setImageBitmap(bitmap);
                                imageView = imageView2;
                            }
                        } else {
                            if (InterstitialAd.c.a(AdManager.LOG, 6)) {
                                Log.e(AdManager.LOG, "couldn't find Bitmap " + string4);
                            }
                            imageView = null;
                        }
                    } else if (InterstitialAd.c.a(AdManager.LOG, 3)) {
                        Log.d(AdManager.LOG, "Could not find asset name " + jSONObject);
                    }
                }
                z3 = true;
                afVar = imageView;
                z2 = true;
            } else if ("P".equals(string)) {
                z3 = true;
                afVar = this.s != null ? new View(this.s.getContext()) : null;
                z2 = true;
            } else if ("wv".equals(string)) {
                z3 = true;
                afVar = d(jSONObject);
                z2 = true;
            } else {
                z2 = true;
                afVar = null;
                z3 = true;
            }
            if (afVar != null) {
                if (z3) {
                    afVar.setBackgroundColor(a(jSONObject, "bgc", 0));
                } else if (z2) {
                    afVar.setBackgroundDrawable(null);
                }
                PointF a6 = a(jSONObject, "ap", e);
                ah c2 = ah.c(afVar);
                c2.b = a6;
                afVar.setTag(c2);
                AnimationSet animationSet = null;
                JSONArray optJSONArray2 = jSONObject.optJSONArray("a");
                JSONObject optJSONObject = jSONObject.optJSONObject("ag");
                if (optJSONArray2 != null) {
                    animationSet = a(optJSONArray2, optJSONObject, afVar, a2);
                }
                String optString = jSONObject.optString("ut", null);
                if (!(afVar == null || optString == null)) {
                    afVar.setTag(ah.c(afVar));
                }
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(a2.width(), a2.height());
                layoutParams.addRule(9);
                layoutParams.addRule(10);
                layoutParams.setMargins(a2.left, a2.top, 0, 0);
                afVar.setLayoutParams(layoutParams);
                if (animationSet != null) {
                    afVar.setAnimation(animationSet);
                }
                if (jSONObject.optBoolean("cav") && this.s != null) {
                    this.s.a(afVar, layoutParams);
                }
                return afVar;
            }
            if (InterstitialAd.c.a(AdManager.LOG, 6)) {
                Log.e(AdManager.LOG, "created a null view.");
            }
            return null;
        } catch (JSONException e2) {
            if (InterstitialAd.c.a(AdManager.LOG, 6)) {
                Log.e(AdManager.LOG, "exception while trying to create a flex view.", e2);
            }
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, float):float
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, int):int
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, android.graphics.Matrix):android.graphics.Matrix
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, android.graphics.PointF):android.graphics.PointF
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, android.graphics.Rect):android.graphics.Rect
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, android.graphics.RectF):android.graphics.RectF
      com.admob.android.ads.j.a(int, int, android.view.View):com.admob.android.ads.aj
      com.admob.android.ads.j.a(float, float, android.view.View):com.admob.android.ads.ap
      com.admob.android.ads.j.a(android.widget.ImageView, android.graphics.Bitmap, org.json.JSONObject):void
      com.admob.android.ads.j.a(org.json.JSONObject, android.view.animation.Animation, android.view.animation.AnimationSet):void
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, float):float */
    private AnimationSet a(JSONArray jSONArray, JSONObject jSONObject, View view, Rect rect) throws JSONException {
        Animation animation;
        AnimationSet animationSet = new AnimationSet(false);
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= jSONArray.length()) {
                break;
            }
            JSONObject jSONObject2 = jSONArray.getJSONObject(i3);
            Animation animation2 = null;
            String optString = jSONObject2.optString("t", null);
            int a2 = (int) (((double) a(jSONObject2, "d", 0.25f)) * 1000.0d);
            if ("B".equals(optString)) {
                String optString2 = jSONObject2.optString("kp", null);
                String optString3 = jSONObject2.optString("vt", null);
                if (optString2 == null || optString3 == null) {
                    if (InterstitialAd.c.a(AdManager.LOG, 6)) {
                        Log.e(AdManager.LOG, "Could not read basic animation: keyPath(" + optString2 + ") or valueType(" + optString3 + ") is null.");
                        animation = null;
                    }
                    animation = null;
                } else if ("position".equals(optString2) && "P".equals(optString3)) {
                    animation = a(a(jSONObject2, "fv", c), a(jSONObject2, "tv", d), view, rect);
                } else if ("opacity".equals(optString2) && "F".equals(optString3)) {
                    animation = a(a(jSONObject2, "fv", 0.0f), a(jSONObject2, "tv", 0.0f));
                } else if ("transform".equals(optString2) && "AT".equals(optString3)) {
                    a(jSONObject2, "fv", f);
                    a(jSONObject2, "fv", f);
                    animation = a(view, rect, jSONObject2, jSONObject2.getJSONArray("tfv"), jSONObject2.getJSONArray("ttv"));
                } else if ("bounds".equals(optString2) && "R".equals(optString3)) {
                    animation = a(a(jSONObject2, "fv", g), a(jSONObject2, "tv", g), (View) null, rect);
                } else if ("zPosition".equals(optString2) && "F".equals(optString3)) {
                    animation = a(a(jSONObject2, "fv", 0.0f), a(jSONObject2, "tv", 0.0f), view);
                } else if (!"backgroundColor".equals(optString2) || !"C".equals(optString3)) {
                    if (InterstitialAd.c.a(AdManager.LOG, 6)) {
                        Log.e(AdManager.LOG, "Could not read basic animation: could not interpret keyPath(" + optString2 + ") and valueType(" + optString3 + ") combination.");
                    }
                    animation = null;
                } else {
                    animation = a(a(jSONObject2, "fv", 0), a(jSONObject2, "tv", 0), view);
                }
                if (animation != null) {
                    Interpolator a3 = a(jSONObject2.optString("tf", null), -1, -1, -1);
                    if (a3 == null) {
                        a3 = null;
                    }
                    if (a3 != null) {
                        animation.setInterpolator(a3);
                    }
                }
                animation2 = animation;
            } else if ("K".equals(optString)) {
                animation2 = a(jSONObject2, view, rect, (long) a2);
            }
            if (animation2 != null) {
                animation2.setDuration((long) a2);
                a(jSONObject2, animation2, animationSet);
                animationSet.addAnimation(animation2);
                animation2.getDuration();
            }
            i2 = i3 + 1;
        }
        if (jSONObject != null) {
            a(jSONObject, animationSet, (AnimationSet) null);
        }
        return animationSet;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, float):float
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, int):int
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, android.graphics.Matrix):android.graphics.Matrix
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, android.graphics.PointF):android.graphics.PointF
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, android.graphics.Rect):android.graphics.Rect
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, android.graphics.RectF):android.graphics.RectF
      com.admob.android.ads.j.a(int, int, android.view.View):com.admob.android.ads.aj
      com.admob.android.ads.j.a(float, float, android.view.View):com.admob.android.ads.ap
      com.admob.android.ads.j.a(android.widget.ImageView, android.graphics.Bitmap, org.json.JSONObject):void
      com.admob.android.ads.j.a(org.json.JSONObject, android.view.animation.Animation, android.view.animation.AnimationSet):void
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, float):float */
    private static int c(JSONObject jSONObject) {
        int a2 = (int) a(jSONObject, "rc", 1.0f);
        if (a2 > 0) {
            return a2 - 1;
        }
        return a2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, float):float
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, int):int
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, android.graphics.Matrix):android.graphics.Matrix
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, android.graphics.PointF):android.graphics.PointF
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, android.graphics.Rect):android.graphics.Rect
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, android.graphics.RectF):android.graphics.RectF
      com.admob.android.ads.j.a(int, int, android.view.View):com.admob.android.ads.aj
      com.admob.android.ads.j.a(float, float, android.view.View):com.admob.android.ads.ap
      com.admob.android.ads.j.a(android.widget.ImageView, android.graphics.Bitmap, org.json.JSONObject):void
      com.admob.android.ads.j.a(org.json.JSONObject, android.view.animation.Animation, android.view.animation.AnimationSet):void
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, float):float */
    private void a(JSONObject jSONObject, Animation animation, AnimationSet animationSet) {
        float a2 = a(jSONObject, "bt", 0.0f);
        float a3 = a(jSONObject, "to", 0.0f);
        int c2 = c(jSONObject);
        boolean optBoolean = jSONObject.optBoolean("ar", false);
        String optString = jSONObject.optString("fm", "r");
        int i2 = (int) (((double) (a2 + 0.0f + a3)) * 1000.0d);
        float a4 = 1.0f / a(jSONObject, "s", 1.0f);
        a(animation, c2, i2, a4, optString, optBoolean);
        if (animationSet != null) {
            a(animationSet, c2, i2, a4, optString, optBoolean);
        }
    }

    private static void a(Animation animation, int i2, int i3, float f2, String str, boolean z2) {
        if (z2) {
            animation.setRepeatMode(2);
        }
        animation.setRepeatCount(i2);
        animation.setStartOffset((long) i3);
        animation.startNow();
        animation.scaleCurrentDuration(f2);
        a(str, animation);
    }

    private static void a(String str, Animation animation) {
        if (str != null && animation != null) {
            Class<?> cls = animation.getClass();
            try {
                Method method = cls.getMethod("setFillEnabled", Boolean.TYPE);
                if (method != null) {
                    method.invoke(animation, true);
                }
            } catch (Exception e2) {
            }
            if ("b".equals(str)) {
                animation.setFillBefore(true);
                animation.setFillAfter(false);
            } else if ("fb".equals(str) || "r".equals(str)) {
                animation.setFillBefore(true);
                animation.setFillAfter(true);
            } else if ("f".equals(str)) {
                animation.setFillBefore(false);
                animation.setFillAfter(true);
            } else if ("r".equals(str)) {
                animation.setFillBefore(false);
                animation.setFillAfter(false);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.admob.android.ads.an.<init>(float[], float[], float, float, float, boolean):void
     arg types: [float[], float[], float, float, int, int]
     candidates:
      com.admob.android.ads.an.<init>(float, float, float, float, float, boolean):void
      com.admob.android.ads.an.<init>(float[], float[], float, float, float, boolean):void */
    private static Animation a(View view, Rect rect, JSONObject jSONObject, JSONArray jSONArray, JSONArray jSONArray2) throws JSONException {
        String optString = jSONObject.optString("tt", null);
        if (optString != null) {
            if ("t".equals(optString)) {
                return a(e(jSONArray), e(jSONArray2), view, rect);
            }
            if ("r".equals(optString)) {
                float[] b2 = b(jSONArray);
                float[] b3 = b(jSONArray2);
                if (b2 == null || b3 == null || Arrays.equals(b2, b3)) {
                    return null;
                }
                PointF a2 = a(new RectF(rect), ah.b(view));
                return new an(b2, b3, a2.x, a2.y, 0.0f, false);
            } else if ("sc".equals(optString)) {
                float[] b4 = b(jSONArray);
                float[] b5 = b(jSONArray2);
                PointF b6 = ah.b(view);
                return new ScaleAnimation(b4[0], b5[0], b4[1], b5[1], 1, b6.x, 1, b6.y);
            } else if ("sk".equals(optString)) {
                float[] b7 = b(jSONArray);
                float[] b8 = b(jSONArray2);
                if (b7 == null || b8 == null || Arrays.equals(b7, b8)) {
                    return null;
                }
                return new ao(b7, b8, a(new RectF(rect), ah.b(view)));
            } else {
                "p".equals(optString);
            }
        }
        return null;
    }

    private static ak a(float f2, float f3) {
        if (f2 != f3) {
            return new ak(f2, f3);
        }
        return null;
    }

    private static am a(PointF pointF, PointF pointF2, View view, Rect rect) {
        if (pointF.equals(pointF2)) {
            return null;
        }
        PointF b2 = ah.b(view);
        float width = (((float) rect.width()) * b2.x) + ((float) rect.left);
        float height = (b2.y * ((float) rect.height())) + ((float) rect.top);
        pointF.x -= width;
        pointF.y -= height;
        pointF2.x -= width;
        pointF2.y -= height;
        return new am(0, pointF.x, 0, pointF2.x, 0, pointF.y, 0, pointF2.y);
    }

    private AnimationSet a(JSONObject jSONObject, View view, Rect rect, long j2) throws JSONException {
        String string = jSONObject.getString("vt");
        float[] b2 = b(jSONObject, "kt");
        JSONArray jSONArray = jSONObject.getJSONArray("vs");
        String[] a2 = a(jSONObject, "tfs");
        JSONArray optJSONArray = jSONObject.optJSONArray("ttvs");
        int length = b2.length;
        int length2 = jSONArray.length();
        int length3 = a2.length;
        if (!(length == length2 && length2 == length3 + 1) && ((double) b2[0]) == 0.0d && ((double) b2[length - 1]) == 1.0d) {
            if (InterstitialAd.c.a(AdManager.LOG, 6)) {
                Log.e(AdManager.LOG, "keyframe animations were invalid: numKeyTimes=" + length + " numKeyValues=" + length2 + " numKeyFunctions=" + length3 + " keyTimes[0]=" + b2[0] + " keyTimes[" + (length - 1) + "]=" + b2[length - 1]);
            }
            return null;
        }
        AnimationSet animationSet = new AnimationSet(false);
        String string2 = jSONObject.getString("kp");
        int c2 = c(jSONObject);
        for (int i2 = 0; i2 < length - 1; i2++) {
            Animation a3 = a(i2, string2, string, b2, jSONArray, a2, j2, view, rect, jSONObject, optJSONArray);
            if (a3 != null) {
                a3.setRepeatCount(c2);
                animationSet.addAnimation(a3);
            }
        }
        a(jSONObject.optString("fm", "r"), animationSet);
        return animationSet;
    }

    private static Animation a(int i2, String str, String str2, float[] fArr, JSONArray jSONArray, String[] strArr, long j2, View view, Rect rect, JSONObject jSONObject, JSONArray jSONArray2) {
        Animation animation;
        int i3 = i2 + 1;
        float f2 = fArr[i2];
        float f3 = fArr[i3];
        if (str == null || str2 == null) {
            if (InterstitialAd.c.a(AdManager.LOG, 6)) {
                Log.e(AdManager.LOG, "Could not read keyframe animation: keyPath(" + str + ") or valueType(" + str2 + ") is null.");
                animation = null;
            }
            animation = null;
        } else {
            try {
                if ("position".equals(str) && "P".equals(str2)) {
                    animation = a(e(jSONArray.getJSONArray(i2)), e(jSONArray.getJSONArray(i3)), view, rect);
                } else if ("opacity".equals(str) && "F".equals(str2)) {
                    animation = a((float) jSONArray.getDouble(i2), (float) jSONArray.getDouble(i3));
                } else if ("bounds".equals(str) && "R".equals(str2)) {
                    animation = a(d(jSONArray.getJSONArray(i2)), d(jSONArray.getJSONArray(i3)), view, rect);
                } else if ("zPosition".equals(str) && "F".equals(str2)) {
                    animation = a((float) jSONArray.getDouble(i2), (float) jSONArray.getDouble(i3), view);
                } else if (!"backgroundColor".equals(str) || !"C".equals(str2)) {
                    if (!"transform".equals(str) || !"AT".equals(str2)) {
                        if (InterstitialAd.c.a(AdManager.LOG, 6)) {
                            Log.e(AdManager.LOG, "Could not read keyframe animation: could not interpret keyPath(" + str + ") and valueType(" + str2 + ") combination.");
                        }
                    } else if (jSONArray2 != null) {
                        a(jSONArray.getJSONArray(i2));
                        a(jSONArray.getJSONArray(i3));
                        animation = a(view, rect, jSONObject, jSONArray2.getJSONArray(i2), jSONArray2.getJSONArray(i3));
                    }
                    animation = null;
                } else {
                    animation = a(c(jSONArray.getJSONArray(i2)), c(jSONArray.getJSONArray(i3)), view);
                }
            } catch (JSONException e2) {
            }
        }
        if (animation != null) {
            animation.setDuration(j2);
            Interpolator a2 = a(strArr[i2], (long) ((int) (((float) j2) * f2)), (long) ((int) ((f3 - f2) * ((float) j2))), j2);
            if (a2 != null) {
                animation.setInterpolator(a2);
            }
        }
        return animation;
    }

    private static aj a(int i2, int i3, View view) {
        if (i2 != i3) {
            return new aj(i2, i3, view);
        }
        return null;
    }

    private static ap a(float f2, float f3, View view) {
        if (f2 != f3) {
            return new ap(f2, f3, view);
        }
        return null;
    }

    private static al a(RectF rectF, RectF rectF2, View view, Rect rect) {
        if (rectF.equals(rectF2)) {
            return null;
        }
        PointF a2 = a(rectF, ah.b(view));
        float width = (float) rect.width();
        float height = (float) rect.height();
        return new al(rectF.width() / width, rectF2.width() / width, rectF.height() / height, rectF2.height() / height, a2.x, a2.y);
    }

    private static PointF a(RectF rectF, PointF pointF) {
        float width = rectF.width();
        float height = rectF.height();
        return new PointF((width * pointF.x) + rectF.left, (height * pointF.y) + rectF.top);
    }

    /* compiled from: Ad */
    static class d implements Runnable {
        private WeakReference<j> a;
        private JSONArray b;

        public d(j jVar, JSONArray jSONArray) {
            this.a = new WeakReference<>(jVar);
            this.b = jSONArray;
        }

        public final void run() {
            try {
                j jVar = this.a.get();
                if (jVar != null) {
                    j.a(jVar, this.b);
                }
            } catch (Exception e) {
                if (InterstitialAd.c.a(AdManager.LOG, 6)) {
                    Log.e(AdManager.LOG, "exception caught in Ad$ViewAdd.run(), " + e.getMessage());
                    e.printStackTrace();
                }
                j jVar2 = this.a.get();
                if (jVar2 != null) {
                    jVar2.r();
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, float):float
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, int):int
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, android.graphics.Matrix):android.graphics.Matrix
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, android.graphics.PointF):android.graphics.PointF
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, android.graphics.Rect):android.graphics.Rect
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, android.graphics.RectF):android.graphics.RectF
      com.admob.android.ads.j.a(int, int, android.view.View):com.admob.android.ads.aj
      com.admob.android.ads.j.a(float, float, android.view.View):com.admob.android.ads.ap
      com.admob.android.ads.j.a(android.widget.ImageView, android.graphics.Bitmap, org.json.JSONObject):void
      com.admob.android.ads.j.a(org.json.JSONObject, android.view.animation.Animation, android.view.animation.AnimationSet):void
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, float):float */
    private View a(JSONObject jSONObject, Rect rect) {
        if (this.s == null) {
            return null;
        }
        float a2 = a(jSONObject, "ia", 0.5f);
        float a3 = a(jSONObject, "epy", 0.4375f);
        int a4 = a(jSONObject, "bc", this.v);
        try {
            Bitmap createBitmap = Bitmap.createBitmap(rect.width(), rect.height(), Bitmap.Config.ARGB_8888);
            if (createBitmap == null) {
                return null;
            }
            this.z.add(createBitmap);
            a(new Canvas(createBitmap), rect, a4, -1, (int) (a2 * 255.0f), a3);
            View view = new View(this.s.getContext());
            view.setBackgroundDrawable(new BitmapDrawable(createBitmap));
            return view;
        } catch (Throwable th) {
            return null;
        }
    }

    public static void a(Canvas canvas, Rect rect, int i2, int i3, int i4, float f2) {
        int height = ((int) (((float) rect.height()) * f2)) + rect.top;
        Rect rect2 = new Rect(rect.left, rect.top, rect.right, height);
        Paint paint = new Paint();
        paint.setColor(-1);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawRect(rect2, paint);
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{Color.argb(i4, Color.red(i2), Color.green(i2), Color.blue(i2)), i2});
        gradientDrawable.setBounds(rect2);
        gradientDrawable.draw(canvas);
        Rect rect3 = new Rect(rect.left, height, rect.right, rect.bottom);
        Paint paint2 = new Paint();
        paint2.setColor(i2);
        paint2.setStyle(Paint.Style.FILL);
        canvas.drawRect(rect3, paint2);
    }

    private View d(JSONObject jSONObject) {
        if (this.s == null) {
            return null;
        }
        String optString = jSONObject.optString("u");
        String optString2 = jSONObject.optString("html");
        String optString3 = jSONObject.optString("base");
        this.y++;
        z zVar = new z(this.s.getContext(), this);
        if (optString != null && !optString.equals("")) {
            zVar.b(optString);
            zVar.loadUrl(optString);
        } else if (optString2 == null || optString2.equals("") || optString3 == null || optString3.equals("")) {
            a(false);
            return null;
        } else {
            zVar.loadDataWithBaseURL(optString3, optString2, null, null, null);
        }
        JSONObject optJSONObject = jSONObject.optJSONObject("d");
        if (optJSONObject != null) {
            zVar.a = optJSONObject;
        }
        AdView adView = this.s.b;
        JSONObject jSONObject2 = new JSONObject();
        try {
            jSONObject2.put("ptc", b(adView.getPrimaryTextColor()));
            jSONObject2.put("stc", b(adView.getSecondaryTextColor()));
            jSONObject2.put("bgc", b(adView.getBackgroundColor()));
        } catch (JSONException e2) {
        }
        zVar.b = jSONObject2;
        zVar.b();
        this.F = true;
        return zVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, float):float
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, int):int
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, android.graphics.Matrix):android.graphics.Matrix
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, android.graphics.PointF):android.graphics.PointF
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, android.graphics.Rect):android.graphics.Rect
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, android.graphics.RectF):android.graphics.RectF
      com.admob.android.ads.j.a(int, int, android.view.View):com.admob.android.ads.aj
      com.admob.android.ads.j.a(float, float, android.view.View):com.admob.android.ads.ap
      com.admob.android.ads.j.a(android.widget.ImageView, android.graphics.Bitmap, org.json.JSONObject):void
      com.admob.android.ads.j.a(org.json.JSONObject, android.view.animation.Animation, android.view.animation.AnimationSet):void
      com.admob.android.ads.j.a(org.json.JSONObject, java.lang.String, float):float */
    private void a(ImageView imageView, Bitmap bitmap, JSONObject jSONObject) {
        Bitmap bitmap2;
        float a2 = a(jSONObject, "bw", 0.5f);
        int a3 = a(jSONObject, "bdc", a);
        float a4 = a(jSONObject, "br", 6.5f);
        if (a2 < 1.0f) {
            a2 = 1.0f;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        try {
            Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            if (createBitmap != null) {
                createBitmap.eraseColor(0);
                Canvas canvas = new Canvas(createBitmap);
                canvas.setDrawFilter(new PaintFlagsDrawFilter(0, 1));
                float f2 = a4 + a2;
                Path path = new Path();
                RectF rectF = new RectF(0.0f, 0.0f, (float) width, (float) height);
                path.addRoundRect(rectF, f2, f2, Path.Direction.CCW);
                canvas.clipPath(path, Region.Op.REPLACE);
                canvas.drawBitmap(bitmap, 0.0f, 0.0f, new Paint(3));
                canvas.clipRect(rectF, Region.Op.REPLACE);
                Paint paint = new Paint(1);
                paint.setStrokeWidth(a2);
                paint.setColor(a3);
                paint.setStyle(Paint.Style.STROKE);
                Path path2 = new Path();
                float f3 = a2 / 2.0f;
                path2.addRoundRect(new RectF(f3, f3, ((float) width) - f3, ((float) height) - f3), a4, a4, Path.Direction.CCW);
                canvas.drawPath(path2, paint);
                if (bitmap != null) {
                    bitmap.recycle();
                }
                bitmap2 = createBitmap;
                this.z.add(bitmap2);
                imageView.setImageBitmap(bitmap2);
            }
        } catch (Throwable th) {
            bitmap2 = bitmap;
        }
    }

    /* access modifiers changed from: private */
    public void r() {
        m mVar = this.r.get();
        if (mVar != null) {
            mVar.a();
        }
        if (this.G != null) {
            s.a();
        }
    }

    private static Interpolator a(String str, long j2, long j3, long j4) {
        AccelerateInterpolator accelerateInterpolator = "i".equals(str) ? new AccelerateInterpolator() : "o".equals(str) ? new DecelerateInterpolator() : "io".equals(str) ? new AccelerateDecelerateInterpolator() : "l".equals(str) ? new LinearInterpolator() : null;
        if (accelerateInterpolator == null || j2 == -1 || j3 == -1 || j4 == -1) {
            return accelerateInterpolator;
        }
        return new ai(accelerateInterpolator, j2, j3, j4);
    }

    private static Matrix a(JSONArray jSONArray) {
        float[] b2 = b(jSONArray);
        if (b2 == null || b2.length != 9) {
            return null;
        }
        Matrix matrix = new Matrix();
        matrix.setValues(b2);
        return matrix;
    }

    private static Matrix a(JSONObject jSONObject, String str, Matrix matrix) {
        float[] b2 = b(jSONObject, str);
        if (b2 == null || b2.length != 9) {
            return matrix;
        }
        Matrix matrix2 = new Matrix();
        matrix2.setValues(b2);
        return matrix2;
    }

    private static String[] a(JSONObject jSONObject, String str) {
        JSONArray optJSONArray = jSONObject.optJSONArray(str);
        if (optJSONArray == null) {
            return null;
        }
        int length = optJSONArray.length();
        try {
            String[] strArr = new String[length];
            for (int i2 = 0; i2 < length; i2++) {
                strArr[i2] = optJSONArray.getString(i2);
            }
            return strArr;
        } catch (JSONException e2) {
            return null;
        }
    }

    private static float[] b(JSONArray jSONArray) {
        int length = jSONArray.length();
        try {
            float[] fArr = new float[length];
            for (int i2 = 0; i2 < length; i2++) {
                fArr[i2] = (float) jSONArray.getDouble(i2);
            }
            return fArr;
        } catch (JSONException e2) {
            return null;
        }
    }

    private static float[] b(JSONObject jSONObject, String str) {
        JSONArray optJSONArray = jSONObject.optJSONArray(str);
        if (optJSONArray == null) {
            return null;
        }
        return b(optJSONArray);
    }

    private static float a(JSONObject jSONObject, String str, float f2) {
        return (float) jSONObject.optDouble(str, (double) f2);
    }

    private static JSONArray b(int i2) {
        JSONArray jSONArray = new JSONArray();
        jSONArray.put(Color.red(i2));
        jSONArray.put(Color.green(i2));
        jSONArray.put(Color.blue(i2));
        jSONArray.put(Color.alpha(i2));
        return jSONArray;
    }

    private static int c(JSONArray jSONArray) throws JSONException {
        return Color.argb((int) (jSONArray.getDouble(3) * 255.0d), (int) (jSONArray.getDouble(0) * 255.0d), (int) (jSONArray.getDouble(1) * 255.0d), (int) (jSONArray.getDouble(2) * 255.0d));
    }

    private static int a(JSONObject jSONObject, String str, int i2) {
        if (jSONObject == null || !jSONObject.has(str)) {
            return i2;
        }
        try {
            JSONArray jSONArray = jSONObject.getJSONArray(str);
            return Color.argb((int) (jSONArray.getDouble(3) * 255.0d), (int) (jSONArray.getDouble(0) * 255.0d), (int) (jSONArray.getDouble(1) * 255.0d), (int) (jSONArray.getDouble(2) * 255.0d));
        } catch (Exception e2) {
            return i2;
        }
    }

    private static RectF d(JSONArray jSONArray) throws JSONException {
        float f2 = (float) jSONArray.getDouble(0);
        float f3 = (float) jSONArray.getDouble(1);
        return new RectF(f2, f3, ((float) jSONArray.getDouble(2)) + f2, ((float) jSONArray.getDouble(3)) + f3);
    }

    private static RectF a(JSONObject jSONObject, String str, RectF rectF) {
        if (jSONObject == null || !jSONObject.has(str)) {
            return rectF;
        }
        try {
            return d(jSONObject.getJSONArray(str));
        } catch (JSONException e2) {
            return rectF;
        }
    }

    private static Rect a(JSONObject jSONObject, String str, Rect rect) {
        if (jSONObject == null || !jSONObject.has(str)) {
            return rect;
        }
        try {
            JSONArray jSONArray = jSONObject.getJSONArray(str);
            int i2 = (int) jSONArray.getDouble(0);
            int i3 = (int) jSONArray.getDouble(1);
            return new Rect(i2, i3, ((int) jSONArray.getDouble(2)) + i2, ((int) jSONArray.getDouble(3)) + i3);
        } catch (JSONException e2) {
            return rect;
        }
    }

    private static PointF e(JSONArray jSONArray) throws JSONException {
        return new PointF((float) jSONArray.getDouble(0), (float) jSONArray.getDouble(1));
    }

    private static PointF a(JSONObject jSONObject, String str, PointF pointF) {
        if (jSONObject == null || !jSONObject.has(str)) {
            return pointF;
        }
        try {
            return e(jSONObject.getJSONArray(str));
        } catch (JSONException e2) {
            return pointF;
        }
    }

    public static float a(Context context) {
        if (h < 0.0f) {
            h = context.getSharedPreferences("admob_prefs", 2).getFloat("timeout", 5.0f);
        }
        return h;
    }

    public static float n() {
        return h;
    }
}
