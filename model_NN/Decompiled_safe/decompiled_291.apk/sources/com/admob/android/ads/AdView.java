package com.admob.android.ads;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.admob.android.ads.InterstitialAd;
import com.admob.android.ads.j;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;

public class AdView extends RelativeLayout {
    private static Boolean a;
    private static Handler s = null;
    /* access modifiers changed from: private */
    public k b;
    /* access modifiers changed from: private */
    public int c;
    private boolean d;
    private d e;
    private int f;
    private int g;
    private int h;
    /* access modifiers changed from: private */
    public String i;
    /* access modifiers changed from: private */
    public String j;
    /* access modifiers changed from: private */
    public AdListener k;
    private boolean l;
    private boolean m;
    /* access modifiers changed from: private */
    public boolean n;
    /* access modifiers changed from: private */
    public long o;
    private a p;
    private j.b q;
    private f r;

    /* compiled from: MyParcelableUtil */
    public static class a implements m {
        private WeakReference<AdView> a;

        public a() {
        }

        public static ArrayList<Bundle> a(Vector<? extends n> vector) {
            if (vector == null) {
                return null;
            }
            ArrayList<Bundle> arrayList = new ArrayList<>();
            Iterator<? extends n> it = vector.iterator();
            while (it.hasNext()) {
                n nVar = (n) it.next();
                if (nVar == null) {
                    arrayList.add(null);
                } else {
                    arrayList.add(nVar.a());
                }
            }
            return arrayList;
        }

        public static Bundle a(n nVar) {
            if (nVar == null) {
                return null;
            }
            return nVar.a();
        }

        public a(AdView adView) {
            this.a = new WeakReference<>(adView);
        }

        public final void a() {
            AdView adView = this.a.get();
            if (adView != null) {
                AdView.f(adView);
            }
        }

        public final void a(j jVar) {
            AdView adView = this.a.get();
            if (adView != null) {
                synchronized (adView) {
                    if (adView.b == null || !jVar.equals(adView.b.c())) {
                        if (InterstitialAd.c.a(AdManager.LOG, 4)) {
                            Log.i(AdManager.LOG, "Ad returned (" + (SystemClock.uptimeMillis() - adView.o) + " ms):  " + jVar);
                        }
                        adView.getContext();
                        adView.a(jVar, jVar.c());
                    } else if (InterstitialAd.c.a(AdManager.LOG, 3)) {
                        Log.d(AdManager.LOG, "Received the same ad we already had.  Discarding it.");
                    }
                }
            }
        }
    }

    static /* synthetic */ void a(AdView adView, j jVar) {
        if (adView.k == null) {
            return;
        }
        if (adView.b == null || adView.b.getParent() == null) {
            try {
                adView.k.onReceiveAd(adView);
            } catch (Exception e2) {
                Log.w(AdManager.LOG, "Unhandled exception raised in your AdListener.onReceiveAd.", e2);
            }
        } else {
            try {
                adView.k.onReceiveRefreshedAd(adView);
            } catch (Exception e3) {
                Log.w(AdManager.LOG, "Unhandled exception raised in your AdListener.onReceiveRefreshedAd.", e3);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.admob.android.ads.an.<init>(float, float, float, float, float, boolean):void
     arg types: [int, int, float, float, float, int]
     candidates:
      com.admob.android.ads.an.<init>(float[], float[], float, float, float, boolean):void
      com.admob.android.ads.an.<init>(float, float, float, float, float, boolean):void */
    static /* synthetic */ void b(AdView adView, final k kVar) {
        kVar.setVisibility(8);
        an anVar = new an(0.0f, -90.0f, ((float) adView.getWidth()) / 2.0f, ((float) adView.getHeight()) / 2.0f, -0.4f * ((float) adView.getWidth()), true);
        anVar.setDuration(700);
        anVar.setFillAfter(true);
        anVar.setInterpolator(new AccelerateInterpolator());
        anVar.setAnimationListener(new Animation.AnimationListener() {
            public final void onAnimationStart(Animation animation) {
            }

            public final void onAnimationEnd(Animation animation) {
                AdView.this.post(new g(kVar, AdView.this));
            }

            public final void onAnimationRepeat(Animation animation) {
            }
        });
        adView.startAnimation(anVar);
    }

    static /* synthetic */ a c(AdView adView) {
        if (adView.p == null) {
            adView.p = new a(adView);
        }
        return adView.p;
    }

    static /* synthetic */ void f(AdView adView) {
        if (adView.k != null) {
            s.post(new c(adView));
        }
    }

    static class f {
        public static final f a = new f(com.madhouse.android.ads.AdView.AD_MEASURE_320, 48);
        private int b;
        private int c;

        private f(int i, int i2) {
            this.b = i;
            this.c = i2;
        }

        public final String toString() {
            return String.valueOf(this.b) + "x" + String.valueOf(this.c);
        }

        static {
            new f(com.madhouse.android.ads.AdView.AD_MEASURE_320, 270);
            new f(748, 110);
            new f(488, 80);
        }
    }

    public AdView(Activity activity) {
        this(activity, null, 0);
    }

    public AdView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public AdView(Context context, AttributeSet attributeSet, int i2) {
        this(context, attributeSet, i2, f.a);
    }

    public AdView(Context context, AttributeSet attributeSet, int i2, f fVar) {
        super(context, attributeSet, i2);
        int i3;
        int i4;
        int i5;
        this.m = true;
        if (a == null) {
            a = new Boolean(isInEditMode());
        }
        if (s == null && !a.booleanValue()) {
            Handler handler = new Handler();
            s = handler;
            j.a(handler);
        }
        this.r = fVar;
        if (fVar != f.a) {
            this.q = j.b.VIEW;
        }
        setDescendantFocusability(262144);
        setClickable(true);
        setLongClickable(false);
        setGravity(17);
        if (attributeSet != null) {
            String str = "http://schemas.android.com/apk/res/" + context.getPackageName();
            if (attributeSet.getAttributeBooleanValue(str, "testing", false) && InterstitialAd.c.a(AdManager.LOG, 5)) {
                Log.w(AdManager.LOG, "AdView's \"testing\" XML attribute has been deprecated and will be ignored.  Please delete it from your XML layout and use AdManager.setTestDevices instead.");
            }
            int attributeUnsignedIntValue = attributeSet.getAttributeUnsignedIntValue(str, "backgroundColor", -16777216);
            int attributeUnsignedIntValue2 = attributeSet.getAttributeUnsignedIntValue(str, "textColor", -1);
            if (attributeUnsignedIntValue2 >= 0) {
                setTextColor(attributeUnsignedIntValue2);
            }
            int attributeUnsignedIntValue3 = attributeSet.getAttributeUnsignedIntValue(str, "primaryTextColor", -1);
            int attributeUnsignedIntValue4 = attributeSet.getAttributeUnsignedIntValue(str, "secondaryTextColor", -1);
            this.i = attributeSet.getAttributeValue(str, "keywords");
            setRequestInterval(attributeSet.getAttributeIntValue(str, "refreshInterval", 0));
            boolean attributeBooleanValue = attributeSet.getAttributeBooleanValue(str, "isGoneWithoutAd", false);
            if (attributeBooleanValue) {
                setGoneWithoutAd(attributeBooleanValue);
            }
            i3 = attributeUnsignedIntValue4;
            int i6 = attributeUnsignedIntValue3;
            i5 = attributeUnsignedIntValue;
            i4 = i6;
        } else {
            i3 = -1;
            i4 = -1;
            i5 = -16777216;
        }
        setBackgroundColor(i5);
        setPrimaryTextColor(i4);
        setSecondaryTextColor(i3);
        this.b = null;
        this.p = null;
        if (a.booleanValue()) {
            TextView textView = new TextView(context, attributeSet, i2);
            textView.setBackgroundColor(getBackgroundColor());
            textView.setTextColor(getPrimaryTextColor());
            textView.setPadding(10, 10, 10, 10);
            textView.setTextSize(16.0f);
            textView.setGravity(16);
            textView.setText("Ads by AdMob");
            addView(textView, new RelativeLayout.LayoutParams(-1, -1));
            return;
        }
        c();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        if (InterstitialAd.c.a(AdManager.LOG, 3)) {
            Log.d(AdManager.LOG, "AdView size is " + measuredWidth + " by " + measuredHeight);
        }
        if (a.booleanValue()) {
            return;
        }
        if (((float) ((int) (((float) measuredWidth) / k.d()))) <= 310.0f) {
            if (InterstitialAd.c.a(AdManager.LOG, 3)) {
                Log.d(AdManager.LOG, "We need to have a minimum width of 320 device independent pixels to show an ad.");
            }
            try {
                this.b.setVisibility(8);
            } catch (NullPointerException e2) {
            }
        } else {
            try {
                int visibility = this.b.getVisibility();
                this.b.setVisibility(super.getVisibility());
                if (visibility != 0 && this.b.getVisibility() == 0) {
                    a(this.b);
                }
            } catch (NullPointerException e3) {
            }
        }
    }

    static class c implements Runnable {
        private WeakReference<AdView> a;

        public c(AdView adView) {
            this.a = new WeakReference<>(adView);
        }

        public final void run() {
            AdView adView = this.a.get();
            if (adView == null) {
                return;
            }
            if ((adView.b == null || adView.b.getParent() == null) && adView.k != null) {
                try {
                    adView.k.onFailedToReceiveAd(adView);
                } catch (Exception e) {
                    Log.w(AdManager.LOG, "Unhandled exception raised in your AdListener.onFailedToReceiveAd.", e);
                }
            } else {
                try {
                    adView.k.onFailedToReceiveRefreshedAd(adView);
                } catch (Exception e2) {
                    Log.w(AdManager.LOG, "Unhandled exception raised in your AdListener.onFailedToReceiveRefreshedAd.", e2);
                }
            }
        }
    }

    public void requestFreshAd() {
        if (!this.d) {
            long uptimeMillis = (SystemClock.uptimeMillis() - this.o) / 1000;
            if (uptimeMillis <= 0 || uptimeMillis >= 13) {
                if (e()) {
                    c();
                }
            } else if (InterstitialAd.c.a(AdManager.LOG, 3)) {
                Log.d(AdManager.LOG, "Ignoring requestFreshAd.  Called " + uptimeMillis + " seconds since last refresh.  " + "Refreshes must be at least " + 13 + " apart.");
            }
        } else if (InterstitialAd.c.a(AdManager.LOG, 3)) {
            Log.d(AdManager.LOG, "Request interval overridden by the server.  Ignoring requestFreshAd.");
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        c.a(getContext());
        if (this.m || super.getVisibility() == 0) {
            if (!this.n) {
                this.n = true;
                this.o = SystemClock.uptimeMillis();
                new b(this).start();
            } else if (InterstitialAd.c.a(AdManager.LOG, 5)) {
                Log.w(AdManager.LOG, "Ignoring requestFreshAd() because we are requesting an ad right now already.");
            }
        } else if (InterstitialAd.c.a(AdManager.LOG, 5)) {
            Log.w(AdManager.LOG, "Cannot requestFreshAd() when the AdView is not visible.  Call AdView.setVisibility(View.VISIBLE) first.");
        }
    }

    static class b extends Thread {
        private WeakReference<AdView> a;

        public b(AdView adView) {
            this.a = new WeakReference<>(adView);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.admob.android.ads.AdView.a(com.admob.android.ads.AdView, boolean):boolean
         arg types: [com.admob.android.ads.AdView, int]
         candidates:
          com.admob.android.ads.AdView.a(com.admob.android.ads.AdView, com.admob.android.ads.j):void
          com.admob.android.ads.AdView.a(com.admob.android.ads.AdView, com.admob.android.ads.k):void
          com.admob.android.ads.AdView.a(com.admob.android.ads.j, com.admob.android.ads.k):void
          com.admob.android.ads.AdView.a(com.admob.android.ads.AdView, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.admob.android.ads.AdView.b(com.admob.android.ads.AdView, boolean):void
         arg types: [com.admob.android.ads.AdView, int]
         candidates:
          com.admob.android.ads.AdView.b(com.admob.android.ads.AdView, com.admob.android.ads.k):void
          com.admob.android.ads.AdView.b(com.admob.android.ads.AdView, boolean):void */
        public final void run() {
            AdView adView = this.a.get();
            if (adView != null) {
                try {
                    Context context = adView.getContext();
                    if (b.a(AdView.c(adView), context, adView.i, adView.j, adView.getPrimaryTextColor(), adView.getSecondaryTextColor(), adView.getBackgroundColor(), new k(null, context, adView), (int) (((float) adView.getMeasuredWidth()) / k.d()), adView.a(), null, adView.b()) == null) {
                        AdView.f(adView);
                    }
                } catch (Exception e) {
                    if (InterstitialAd.c.a(AdManager.LOG, 6)) {
                        Log.e(AdManager.LOG, "Unhandled exception requesting a fresh ad.", e);
                    }
                    AdView.f(adView);
                } finally {
                    boolean unused = adView.n = false;
                    adView.a(true);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(j jVar, k kVar) {
        int visibility = super.getVisibility();
        double b2 = jVar.b();
        if (b2 >= 0.0d) {
            this.d = true;
            setRequestInterval((int) b2);
            a(true);
        } else {
            this.d = false;
        }
        boolean z = this.m;
        if (z) {
            this.m = false;
        }
        kVar.a(jVar);
        kVar.setVisibility(visibility);
        kVar.setGravity(17);
        jVar.a(kVar);
        kVar.setLayoutParams(new RelativeLayout.LayoutParams(jVar.a(jVar.f()), jVar.a(jVar.g())));
        s.post(new e(this, kVar, visibility, z));
    }

    static class e implements Runnable {
        private WeakReference<AdView> a;
        private WeakReference<k> b;
        private int c;
        private boolean d;

        public e(AdView adView, k kVar, int i, boolean z) {
            this.a = new WeakReference<>(adView);
            this.b = new WeakReference<>(kVar);
            this.c = i;
            this.d = z;
        }

        public final void run() {
            try {
                AdView adView = this.a.get();
                k kVar = this.b.get();
                if (adView != null && kVar != null) {
                    adView.addView(kVar);
                    AdView.a(adView, kVar.c());
                    if (this.c != 0) {
                        k unused = adView.b = kVar;
                    } else if (this.d) {
                        adView.a(kVar);
                    } else {
                        AdView.b(adView, kVar);
                    }
                }
            } catch (Exception e) {
                if (InterstitialAd.c.a(AdManager.LOG, 6)) {
                    Log.e(AdManager.LOG, "Unhandled exception placing AdContainer into AdView.", e);
                }
            }
        }
    }

    public int getRequestInterval() {
        return this.c / 1000;
    }

    public void setRequestInterval(int i2) {
        int i3 = i2 * 1000;
        if (this.c != i3) {
            if (i2 > 0) {
                if (i2 < 13) {
                    if (InterstitialAd.c.a(AdManager.LOG, 5)) {
                        Log.w(AdManager.LOG, "AdView.setRequestInterval(" + i2 + ") seconds must be >= " + 13);
                    }
                    i3 = 13000;
                } else if (i2 > 600) {
                    if (InterstitialAd.c.a(AdManager.LOG, 5)) {
                        Log.w(AdManager.LOG, "AdView.setRequestInterval(" + i2 + ") seconds must be <= " + 600);
                    }
                    i3 = 600000;
                }
            }
            this.c = i3;
            if (i2 <= 0) {
                d();
            }
            if (InterstitialAd.c.a(AdManager.LOG, 4)) {
                Log.i(AdManager.LOG, "Requesting fresh ads every " + i2 + " seconds.");
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        synchronized (this) {
            if (z) {
                if (this.c > 0 && getVisibility() == 0) {
                    int i2 = this.c;
                    d();
                    if (e()) {
                        this.e = new d(this);
                        s.postDelayed(this.e, (long) i2);
                        if (InterstitialAd.c.a(AdManager.LOG, 3)) {
                            Log.d(AdManager.LOG, "Ad refresh scheduled for " + i2 + " from now.");
                        }
                    }
                }
            }
            if (!z || this.c == 0) {
                d();
            }
        }
    }

    private void d() {
        if (this.e != null) {
            this.e.a = true;
            this.e = null;
            if (InterstitialAd.c.a(AdManager.LOG, 2)) {
                Log.v(AdManager.LOG, "Cancelled an ad refresh scheduled for the future.");
            }
        }
    }

    private boolean e() {
        j c2;
        if (this.b == null || (c2 = this.b.c()) == null || !c2.e() || this.b.h() >= 120) {
            return true;
        }
        if (InterstitialAd.c.a(AdManager.LOG, 3)) {
            Log.d(AdManager.LOG, "Cannot refresh CPM ads.  Ignoring request to refresh the ad.");
        }
        return false;
    }

    static class d implements Runnable {
        boolean a;
        private WeakReference<AdView> b;

        public d(AdView adView) {
            this.b = new WeakReference<>(adView);
        }

        public final void run() {
            try {
                AdView adView = this.b.get();
                if (!this.a && adView != null) {
                    if (InterstitialAd.c.a(AdManager.LOG, 3)) {
                        int h = adView.c / 1000;
                        if (InterstitialAd.c.a(AdManager.LOG, 3)) {
                            Log.d(AdManager.LOG, "Requesting a fresh ad because a request interval passed (" + h + " seconds).");
                        }
                    }
                    adView.c();
                }
            } catch (Exception e) {
                if (InterstitialAd.c.a(AdManager.LOG, 6)) {
                    Log.e(AdManager.LOG, "exception caught in RefreshHandler.run(), " + e.getMessage());
                }
            }
        }
    }

    public void onWindowFocusChanged(boolean z) {
        a(z);
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i2) {
        a(i2 == 0);
    }

    @Deprecated
    public void setTextColor(int i2) {
        if (InterstitialAd.c.a(AdManager.LOG, 5)) {
            Log.w(AdManager.LOG, "Calling the deprecated method setTextColor!  Please use setPrimaryTextColor and setSecondaryTextColor instead.");
        }
        setPrimaryTextColor(i2);
        setSecondaryTextColor(i2);
    }

    @Deprecated
    public int getTextColor() {
        if (InterstitialAd.c.a(AdManager.LOG, 5)) {
            Log.w(AdManager.LOG, "Calling the deprecated method getTextColor!  Please use getPrimaryTextColor and getSecondaryTextColor instead.");
        }
        return getPrimaryTextColor();
    }

    public int getPrimaryTextColor() {
        return this.g;
    }

    public void setPrimaryTextColor(int i2) {
        this.g = -16777216 | i2;
    }

    public int getSecondaryTextColor() {
        return this.h;
    }

    public void setSecondaryTextColor(int i2) {
        this.h = -16777216 | i2;
    }

    public void setBackgroundColor(int i2) {
        this.f = -16777216 | i2;
        invalidate();
    }

    public int getBackgroundColor() {
        return this.f;
    }

    public String getKeywords() {
        return this.i;
    }

    public void setKeywords(String str) {
        this.i = str;
    }

    public String getSearchQuery() {
        return this.j;
    }

    public void setSearchQuery(String str) {
        this.j = str;
    }

    @Deprecated
    public void setGoneWithoutAd(boolean z) {
        if (InterstitialAd.c.a(AdManager.LOG, 5)) {
            Log.w(AdManager.LOG, "Deprecated method setGoneWithoutAd was called.  See JavaDoc for instructions to remove.");
        }
    }

    @Deprecated
    public boolean isGoneWithoutAd() {
        if (!InterstitialAd.c.a(AdManager.LOG, 5)) {
            return false;
        }
        Log.w(AdManager.LOG, "Deprecated method isGoneWithoutAd was called.  See JavaDoc for instructions to remove.");
        return false;
    }

    public void setVisibility(int i2) {
        boolean z;
        if (super.getVisibility() != i2) {
            synchronized (this) {
                int childCount = getChildCount();
                for (int i3 = 0; i3 < childCount; i3++) {
                    getChildAt(i3).setVisibility(i2);
                }
                super.setVisibility(i2);
                invalidate();
            }
        }
        if (i2 == 0) {
            z = true;
        } else {
            z = false;
        }
        a(z);
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        if (z) {
            setVisibility(0);
        } else {
            setVisibility(8);
        }
    }

    public void setAdListener(AdListener adListener) {
        synchronized (this) {
            this.k = adListener;
        }
    }

    public AdListener getAdListener() {
        return this.k;
    }

    public boolean hasAd() {
        return (this.b == null || this.b.c() == null) ? false : true;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        this.l = true;
        a(true);
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.l = false;
        a(false);
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: private */
    public void a(k kVar) {
        this.b = kVar;
        if (this.l) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(233);
            alphaAnimation.startNow();
            alphaAnimation.setFillAfter(true);
            alphaAnimation.setInterpolator(new AccelerateInterpolator());
            startAnimation(alphaAnimation);
        }
    }

    static final class g implements Runnable {
        private WeakReference<AdView> a;
        private WeakReference<k> b;

        public g(k kVar, AdView adView) {
            this.b = new WeakReference<>(kVar);
            this.a = new WeakReference<>(adView);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.admob.android.ads.an.<init>(float, float, float, float, float, boolean):void
         arg types: [int, int, float, float, float, int]
         candidates:
          com.admob.android.ads.an.<init>(float[], float[], float, float, float, boolean):void
          com.admob.android.ads.an.<init>(float, float, float, float, float, boolean):void */
        public final void run() {
            try {
                final AdView adView = this.a.get();
                final k kVar = this.b.get();
                if (adView != null && kVar != null) {
                    final k a2 = adView.b;
                    if (a2 != null) {
                        a2.setVisibility(8);
                    }
                    kVar.setVisibility(0);
                    an anVar = new an(90.0f, 0.0f, ((float) adView.getWidth()) / 2.0f, ((float) adView.getHeight()) / 2.0f, -0.4f * ((float) adView.getWidth()), false);
                    anVar.setDuration(700);
                    anVar.setFillAfter(true);
                    anVar.setInterpolator(new DecelerateInterpolator());
                    anVar.setAnimationListener(new Animation.AnimationListener(this) {
                        public final void onAnimationStart(Animation animation) {
                        }

                        public final void onAnimationEnd(Animation animation) {
                            if (a2 != null) {
                                adView.removeView(a2);
                            }
                            k unused = adView.b = kVar;
                            if (a2 != null) {
                                a2.e();
                            }
                        }

                        public final void onAnimationRepeat(Animation animation) {
                        }
                    });
                    adView.startAnimation(anVar);
                }
            } catch (Exception e) {
                if (InterstitialAd.c.a(AdManager.LOG, 6)) {
                    Log.e(AdManager.LOG, "exception caught in SwapViews.run(), " + e.getMessage());
                }
            }
        }
    }

    public void cleanup() {
        if (this.b != null) {
            this.b.e();
            this.b = null;
        }
    }

    /* access modifiers changed from: package-private */
    public final j.b a() {
        return this.q;
    }

    /* access modifiers changed from: package-private */
    public final f b() {
        return this.r;
    }
}
