package com.madhouse.android.ads;

import I.I;
import android.content.Context;

public class AdManager {
    public static final String USER_GENDER_FEMALE = "female";
    public static final String USER_GENDER_MALE = "male";
    private static String _;
    private static String __;
    private static String ___;
    private static boolean ____ = true;

    private AdManager() {
    }

    protected static final String _() {
        return _;
    }

    protected static final String __() {
        return __;
    }

    protected static final String ___() {
        return ___;
    }

    protected static final boolean ____() {
        return ____;
    }

    public static final void setApplicationId(Context context, String str) {
        if (str != null && str.length() == 16 && _ == null) {
            byte[] _2 = m._(context, I.I(442));
            if (_2 == null) {
                m._(context, I.I(442), str.getBytes());
                _ = str;
                return;
            }
            _ = new String(_2);
        }
    }

    public static final void setEmbedBrowserEnable(boolean z) {
        ____ = z;
    }

    public static final void setUserAge(String str) {
        __ = str;
    }

    public static final void setUserGender(String str) {
        ___ = str;
    }
}
