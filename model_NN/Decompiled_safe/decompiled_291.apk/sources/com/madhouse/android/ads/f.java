package com.madhouse.android.ads;

import I.I;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.webkit.URLUtil;
import android.widget.RelativeLayout;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

final class f {
    private static boolean _ = false;

    private f() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    protected static final Bitmap _(Context context, int i, int i2, float f) {
        Bitmap decodeResource = BitmapFactory.decodeResource(context.getResources(), i);
        if (decodeResource != null) {
            int width = decodeResource.getWidth();
            int height = decodeResource.getHeight();
            int[] iArr = new int[(width * height)];
            int red = Color.red(-1);
            int green = Color.green(-1);
            int blue = Color.blue(-1);
            decodeResource.getPixels(iArr, 0, width, 0, 0, width, height);
            for (int i3 = 0; i3 < height; i3++) {
                for (int i4 = 0; i4 < width; i4++) {
                    int alpha = Color.alpha(iArr[(i3 * width) + i4]);
                    if (alpha > 0) {
                        iArr[(i3 * width) + i4] = Color.argb(alpha, red, green, blue);
                    }
                }
            }
            Bitmap createBitmap = Bitmap.createBitmap(iArr, width, height, Bitmap.Config.ARGB_8888);
            if (createBitmap != decodeResource) {
                decodeResource.recycle();
            }
            Matrix matrix = new Matrix();
            matrix.postRotate((float) i2);
            matrix.postScale(f, f);
            decodeResource = Bitmap.createBitmap(createBitmap, 0, 0, createBitmap.getHeight(), createBitmap.getWidth(), matrix, true);
            if (createBitmap != decodeResource) {
                createBitmap.recycle();
            }
        }
        return decodeResource;
    }

    private static String _() {
        String I2;
        String I3;
        try {
            I2 = Build.BRAND;
        } catch (Exception e) {
            I2 = I.I(542);
        }
        try {
            I3 = Build.MODEL;
        } catch (Exception e2) {
            I3 = I.I(542);
        }
        if (I3.toLowerCase().indexOf(I2.toLowerCase()) != -1) {
            return I3;
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(I2).append(I.I(576)).append(I3);
        return stringBuffer.toString();
    }

    protected static final String _(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService(I.I(589))).getDeviceId();
        } catch (Exception e) {
            return null;
        }
    }

    protected static final String _(Context context, String str) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null) {
                return applicationInfo.metaData.getString(str);
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    protected static final String _(String str) {
        try {
            String bigInteger = new BigInteger(1, MessageDigest.getInstance(I.I(595)).digest(str.getBytes())).toString(16);
            while (bigInteger.length() < 32) {
                bigInteger = I.I(599) + bigInteger;
            }
            return bigInteger;
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    protected static final void _(boolean z) {
        _ = z;
    }

    protected static final int __(String str) {
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    private static String __() {
        String I2;
        String I3;
        try {
            I2 = System.getProperty(I.I(549));
        } catch (Exception e) {
            I2 = I.I(542);
        }
        try {
            I3 = System.getProperty(I.I(557));
        } catch (Exception e2) {
            I3 = I.I(542);
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(I.I(568)).append(I.I(536)).append(I2).append(I3).append(I.I(540));
        return stringBuffer.toString();
    }

    protected static final String __(Context context) {
        return Settings.System.getString(context.getContentResolver(), I.I(578));
    }

    protected static final void __(Context context, String str) {
        if (str == null) {
            return;
        }
        if (URLUtil.isHttpsUrl(str) || !AdManager.____()) {
            Intent intent = new Intent(I.I(304), Uri.parse(str));
            intent.addFlags(268435456);
            context.startActivity(intent);
        } else if (!_) {
            _____ _____ = new _____(context);
            ((Activity) context).addContentView(_____, new RelativeLayout.LayoutParams(-1, -1));
            _____._(str);
        }
    }

    private static String ___() {
        try {
            return Build.VERSION.RELEASE;
        } catch (Exception e) {
            return I.I(542);
        }
    }

    protected static final String ___(Context context) {
        StringBuffer stringBuffer = new StringBuffer();
        StringBuffer append = stringBuffer.append(I.I(519)).append(I.I(524)).append(_()).append(I.I(524)).append(__()).append(I.I(524)).append(___()).append(I.I(524));
        StringBuffer stringBuffer2 = new StringBuffer();
        new DisplayMetrics();
        DisplayMetrics displayMetrics = context.getApplicationContext().getResources().getDisplayMetrics();
        stringBuffer2.append(I.I(536)).append(displayMetrics.widthPixels).append(I.I(538)).append(displayMetrics.heightPixels).append(I.I(538)).append(displayMetrics.density).append(I.I(540));
        append.append(stringBuffer2.toString()).append(I.I(524)).append(I.I(526));
        char[] charArray = stringBuffer.toString().toCharArray();
        stringBuffer.delete(0, stringBuffer.length());
        for (char c : charArray) {
            if (c != ' ') {
                stringBuffer.append(c);
            }
        }
        return stringBuffer.toString();
    }

    protected static final void ___(Context context, String str) {
        if (str != null) {
            String str2 = null;
            if (str != null) {
                str2 = str.replaceFirst(I.I(454), I.I(481));
                if (str.equals(str2)) {
                    str2 = str.replaceFirst(I.I(491), I.I(481));
                }
            }
            Intent intent = new Intent(I.I(304), Uri.parse(str2));
            intent.addFlags(268435456);
            try {
                context.startActivity(intent);
            } catch (ActivityNotFoundException e) {
                __(context, str);
            }
        }
    }

    protected static final boolean ___(String str) {
        return str != null && (str.startsWith(I.I(481)) || str.startsWith(I.I(454)) || str.startsWith(I.I(491)));
    }
}
