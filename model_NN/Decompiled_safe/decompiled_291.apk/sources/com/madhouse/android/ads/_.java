package com.madhouse.android.ads;

import I.I;
import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.Uri;

final class _ {
    private _() {
    }

    protected static final l _(Context context) {
        l lVar = new l();
        int __ = __(context);
        if (__ == 0 || __ == 2) {
            lVar._ = __;
            return lVar;
        }
        try {
            Cursor query = context.getContentResolver().query(Uri.parse(I.I(1634)), null, null, null, null);
            query.moveToFirst();
            if (query.getColumnIndex(I.I(1673)) != -1 || query.getColumnIndex(I.I(1679)) != -1 || query.getColumnIndex(I.I(1685)) != -1 || query.getColumnIndex(I.I(1691)) != -1) {
                if (query != null) {
                    query.close();
                }
                lVar._ = __;
                return lVar;
            } else if (query.getInt(query.getColumnIndex(I.I(1698))) > 0) {
                lVar._ = __;
                lVar.__ = query.getString(query.getColumnIndex(I.I(1698)));
                lVar.___ = query.getString(query.getColumnIndex(I.I(1704)));
                if (query != null) {
                    query.close();
                }
                return lVar;
            } else if (query.getInt(query.getColumnIndex(I.I(1709))) > 0) {
                lVar._ = __;
                lVar.__ = query.getString(query.getColumnIndex(I.I(1709)));
                lVar.___ = query.getString(query.getColumnIndex(I.I(1718)));
                if (query != null) {
                    query.close();
                }
                return lVar;
            } else {
                if (query != null) {
                    query.close();
                }
                lVar._ = __;
                return lVar;
            }
        } catch (Exception e) {
            lVar._ = __;
            return lVar;
        }
    }

    private static int __(Context context) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(I.I(1726));
            if (connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected()) {
                if (connectivityManager.getActiveNetworkInfo().getType() == 0) {
                    return 1;
                }
                if (connectivityManager.getActiveNetworkInfo().getType() == 1) {
                    return 2;
                }
            }
            return 0;
        } catch (Exception e) {
            return 0;
        }
    }
}
