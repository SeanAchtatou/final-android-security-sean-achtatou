package com.madhouse.android.ads;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URISyntaxException;

final class eeee extends View {
    private int $;
    private byte[] $$;
    private String $$$;
    private String $$$$;
    private Bitmap $$$$$;
    int _;
    int __;
    private int ___;
    private int ____;
    private int _____;
    private Paint a;
    private Matrix aa;
    private __ aaa;
    private Context aaaa;

    protected eeee(__ __2, Context context) {
        this(__2, context, null, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.madhouse.android.ads.eeeee._(java.lang.String, boolean):byte[]
     arg types: [java.lang.String, int]
     candidates:
      com.madhouse.android.ads.eeeee._(java.lang.String, int):java.lang.String
      com.madhouse.android.ads.eeeee._(java.lang.String, boolean):byte[] */
    private eeee(__ __2, Context context, AttributeSet attributeSet, int i) {
        super(context, null, 0);
        this.aaa = __2;
        this.aaaa = context;
        this._ = f.__(__2.____);
        this.__ = f.__(__2._____);
        new DisplayMetrics();
        DisplayMetrics displayMetrics = context.getApplicationContext().getResources().getDisplayMetrics();
        this.___ = displayMetrics.widthPixels;
        this.____ = displayMetrics.heightPixels;
        if (__2 != null) {
            setFocusable(true);
            setClickable(true);
            String str = __2.$;
            String str2 = __2.___;
            if (str != null && str.length() > 0 && str2 != null && str2.length() == 8) {
                this.$$ = null;
                if (__2.$$$$) {
                    this.$$ = e._(this.aaaa, str2);
                    if (this.$$ == null) {
                        this.$$ = eeeee._(str, false);
                        e._(this.aaaa, str2, this.$$);
                    }
                } else {
                    this.$$ = eeeee._(str, false);
                }
                if (this.$$ != null) {
                    this.a = new Paint();
                    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(this.$$);
                    if (this.$$$$$ != null && !this.$$$$$.isRecycled()) {
                        this.$$$$$.recycle();
                    }
                    this.$$$$$ = BitmapFactory.decodeStream(byteArrayInputStream);
                    this._____ = this.$$$$$.getWidth();
                    this.$ = this.$$$$$.getHeight();
                    this.aa = new Matrix();
                    if (this.___ <= this.____) {
                        float f = ((float) this._) / ((float) this._____);
                        this.aa.postScale(f, f);
                    } else {
                        float f2 = ((float) this.____) / ((float) this.$);
                        this.aa.postScale(f2, f2);
                    }
                    try {
                        byteArrayInputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                return;
            }
        }
        invalidate();
    }

    private void __() {
        if (this.aaa.$$$ != null) {
            this.$$$ = eeeee._(this.aaa.$$$);
        } else if (this.aaa.$$ != null) {
            this.$$$$ = this.aaa.$$;
        }
        if (this.$$$ != null) {
            try {
                f.___(this.aaaa, this.$$$);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        } else if (this.$$$$ != null) {
            try {
                f.__(this.aaaa, this.$$$$);
            } catch (URISyntaxException e2) {
                e2.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void _() {
        if (!this.$$$$$.isRecycled()) {
            this.$$$$$.recycle();
            this.$$$$$ = null;
        }
    }

    public final boolean dispatchTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 0) {
            setPressed(true);
        } else if (action == 2) {
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            int left = getLeft();
            int top = getTop();
            int right = getRight();
            int bottom = getBottom();
            if (x < ((float) left) || x > ((float) right) || y < ((float) top) || y > ((float) bottom)) {
                setPressed(false);
            } else {
                setPressed(true);
            }
        } else if (action == 1) {
            if (isPressed()) {
                __();
            }
            setPressed(false);
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    public final boolean dispatchTrackballEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            setPressed(true);
        } else if (motionEvent.getAction() == 1) {
            if (hasFocus()) {
                __();
            }
            setPressed(false);
        }
        return super.dispatchTrackballEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        try {
            super.onDraw(canvas);
            if (this.$$$$$ != null && this.aa != null && this.a != null) {
                canvas.concat(this.aa);
                if (this.___ <= this.____) {
                    canvas.drawBitmap(this.$$$$$, 0.0f, (float) ((this.____ - this.$) / 2), this.a);
                } else {
                    canvas.drawBitmap(this.$$$$$, (float) ((this.___ / 2) - ((int) (((double) this.___) * 0.12875d))), 0.0f, this.a);
                }
            }
        } catch (Exception e) {
            if (this.$$$$$ != null && !this.$$$$$.isRecycled()) {
                this.$$$$$.recycle();
            }
        }
    }

    public final boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 66 || i == 23) {
            setPressed(true);
        }
        return super.onKeyDown(i, keyEvent);
    }

    public final boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (i == 66 || i == 23) {
            __();
        }
        setPressed(false);
        return super.onKeyUp(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        int i3 = 0;
        int size = View.MeasureSpec.getSize(i2);
        if (size != 0) {
            i3 = View.MeasureSpec.getSize(i);
        }
        setMeasuredDimension(i3, size);
    }

    public final void setPressed(boolean z) {
        if (isPressed() != z) {
            super.setPressed(z);
            invalidate();
        }
    }
}
