package com.CrystalSelfDefend5;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.widget.ImageButton;
import com.admob.android.ads.AdManager;
import com.admob.android.ads.AdView;
import com.admob.android.ads.SimpleAdListener;

public class AnimationActivity extends Activity {
    public String First_String = "type1_";
    public AdView ad;
    /* access modifiers changed from: private */
    public GameView mGameView;
    public int num = 0;
    private View.OnFocusChangeListener onFocusC = new View.OnFocusChangeListener() {
        public void onFocusChange(View v, boolean state) {
            Log.e("OnonFocusChange", "=" + v + state);
        }
    };
    /* access modifiers changed from: private */
    public ImageButton play;
    private View.OnClickListener play_listener = new View.OnClickListener() {
        public void onClick(View v) {
            if (AnimationActivity.this.mGameView.frameAnimation.isRunning()) {
                AnimationActivity.this.play.setImageResource(R.drawable.play);
            } else {
                AnimationActivity.this.play.setImageResource(R.drawable.stop);
            }
            AnimationActivity.this.mGameView.startAnimation();
        }
    };
    private ImageButton slowDown;
    private View.OnClickListener slowDown_listener = new View.OnClickListener() {
        public void onClick(View v) {
            AnimationActivity.this.play.setImageResource(R.drawable.play);
            AnimationActivity.this.mGameView.slowDown();
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.mGameView = (GameView) findViewById(R.id.selfDefendView);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (!"null".equals(extras.getString("first_String")) || extras.getString("first_String") != null) {
                Log.d("first_String", "=" + extras.getString("first_String"));
                this.mGameView.First_String = extras.getString("first_String");
            }
            if (!"null".equals(extras.getString("num")) || extras.getString("num") != null) {
                this.mGameView.num = Integer.parseInt(extras.getString("num"));
            }
        }
        findButton();
        setButtonListener();
        this.mGameView.setOnFocusChangeListener(this.onFocusC);
        this.ad = (AdView) findViewById(R.id.admobad_in);
        AdManager.setPublisherId("a14df043c9f1d3f");
        setAd();
        this.ad.setAdListener(new LunarLanderListener(this, null));
    }

    public void setAd() {
        this.ad.setVisibility(0);
        AlphaAnimation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(400);
        animation.setFillAfter(true);
        animation.setInterpolator(new AccelerateInterpolator());
        this.ad.startAnimation(animation);
    }

    private class LunarLanderListener extends SimpleAdListener {
        private LunarLanderListener() {
        }

        /* synthetic */ LunarLanderListener(AnimationActivity animationActivity, LunarLanderListener lunarLanderListener) {
            this();
        }

        public void onFailedToReceiveAd(AdView adView) {
            super.onFailedToReceiveAd(adView);
            Log.d("ad", "=" + AnimationActivity.this.ad);
            AnimationActivity.this.ad.requestFreshAd();
            Log.d("Lunar", "onFailedToReceiveAd");
        }

        public void onFailedToReceiveRefreshedAd(AdView adView) {
            super.onFailedToReceiveRefreshedAd(adView);
            AnimationActivity.this.ad.requestFreshAd();
            Log.d("Lunar", "onFailedToReceiveRefreshedAd");
        }

        public void onReceiveAd(AdView adView) {
            super.onReceiveAd(adView);
            Log.d("Lunar", "onReceiveAd");
            AnimationActivity.this.setAd();
            AnimationActivity.this.ad.setRequestInterval(60);
        }

        public void onReceiveRefreshedAd(AdView adView) {
            super.onReceiveRefreshedAd(adView);
            AnimationActivity.this.setAd();
            AnimationActivity.this.ad.requestFreshAd();
            Log.d("Lunar", "onReceiveRefreshedAd");
        }
    }

    private void findButton() {
        this.play = (ImageButton) findViewById(R.id.button_play);
        this.slowDown = (ImageButton) findViewById(R.id.button_slowDown);
    }

    private void setButtonListener() {
        this.play.setOnClickListener(this.play_listener);
        this.slowDown.setOnClickListener(this.slowDown_listener);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (this.mGameView == null) {
            return false;
        }
        this.mGameView.onKeyUp(keyCode, event);
        return true;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (this.mGameView == null) {
            return false;
        }
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        finish();
        return true;
    }
}
