package com.CrystalSelfDefend5;

public final class R {

    public static final class attr {
        public static final int adBannerAnimation = 2130771978;
        public static final int adInterval = 2130771976;
        public static final int adMeasure = 2130771977;
        public static final int adPosition = 2130771975;
        public static final int backgroundColor = 2130771969;
        public static final int debug = 2130771974;
        public static final int keywords = 2130771972;
        public static final int primaryTextColor = 2130771970;
        public static final int refreshInterval = 2130771973;
        public static final int secondaryTextColor = 2130771971;
        public static final int tileSize = 2130771968;
    }

    public static final class drawable {
        public static final int bg1 = 2130837504;
        public static final int borders = 2130837505;
        public static final int bordersstretched = 2130837506;
        public static final int button_1 = 2130837507;
        public static final int icon = 2130837508;
        public static final int play = 2130837509;
        public static final int slowdown = 2130837510;
        public static final int stop = 2130837511;
        public static final int type1_1 = 2130837512;
        public static final int type1_10 = 2130837513;
        public static final int type1_11 = 2130837514;
        public static final int type1_12 = 2130837515;
        public static final int type1_13 = 2130837516;
        public static final int type1_14 = 2130837517;
        public static final int type1_15 = 2130837518;
        public static final int type1_16 = 2130837519;
        public static final int type1_17 = 2130837520;
        public static final int type1_18 = 2130837521;
        public static final int type1_19 = 2130837522;
        public static final int type1_2 = 2130837523;
        public static final int type1_20 = 2130837524;
        public static final int type1_21 = 2130837525;
        public static final int type1_22 = 2130837526;
        public static final int type1_23 = 2130837527;
        public static final int type1_24 = 2130837528;
        public static final int type1_3 = 2130837529;
        public static final int type1_4 = 2130837530;
        public static final int type1_5 = 2130837531;
        public static final int type1_6 = 2130837532;
        public static final int type1_7 = 2130837533;
        public static final int type1_8 = 2130837534;
        public static final int type1_9 = 2130837535;
        public static final int type2_1 = 2130837536;
        public static final int type2_10 = 2130837537;
        public static final int type2_11 = 2130837538;
        public static final int type2_12 = 2130837539;
        public static final int type2_13 = 2130837540;
        public static final int type2_14 = 2130837541;
        public static final int type2_15 = 2130837542;
        public static final int type2_16 = 2130837543;
        public static final int type2_17 = 2130837544;
        public static final int type2_18 = 2130837545;
        public static final int type2_19 = 2130837546;
        public static final int type2_2 = 2130837547;
        public static final int type2_20 = 2130837548;
        public static final int type2_21 = 2130837549;
        public static final int type2_22 = 2130837550;
        public static final int type2_23 = 2130837551;
        public static final int type2_24 = 2130837552;
        public static final int type2_3 = 2130837553;
        public static final int type2_4 = 2130837554;
        public static final int type2_5 = 2130837555;
        public static final int type2_6 = 2130837556;
        public static final int type2_7 = 2130837557;
        public static final int type2_8 = 2130837558;
        public static final int type2_9 = 2130837559;
        public static final int type3_1 = 2130837560;
        public static final int type3_10 = 2130837561;
        public static final int type3_11 = 2130837562;
        public static final int type3_12 = 2130837563;
        public static final int type3_13 = 2130837564;
        public static final int type3_14 = 2130837565;
        public static final int type3_15 = 2130837566;
        public static final int type3_16 = 2130837567;
        public static final int type3_17 = 2130837568;
        public static final int type3_18 = 2130837569;
        public static final int type3_19 = 2130837570;
        public static final int type3_2 = 2130837571;
        public static final int type3_20 = 2130837572;
        public static final int type3_21 = 2130837573;
        public static final int type3_22 = 2130837574;
        public static final int type3_23 = 2130837575;
        public static final int type3_24 = 2130837576;
        public static final int type3_25 = 2130837577;
        public static final int type3_26 = 2130837578;
        public static final int type3_27 = 2130837579;
        public static final int type3_28 = 2130837580;
        public static final int type3_29 = 2130837581;
        public static final int type3_3 = 2130837582;
        public static final int type3_30 = 2130837583;
        public static final int type3_31 = 2130837584;
        public static final int type3_32 = 2130837585;
        public static final int type3_33 = 2130837586;
        public static final int type3_34 = 2130837587;
        public static final int type3_35 = 2130837588;
        public static final int type3_36 = 2130837589;
        public static final int type3_4 = 2130837590;
        public static final int type3_5 = 2130837591;
        public static final int type3_6 = 2130837592;
        public static final int type3_7 = 2130837593;
        public static final int type3_8 = 2130837594;
        public static final int type3_9 = 2130837595;
        public static final int type4_1 = 2130837596;
        public static final int type4_10 = 2130837597;
        public static final int type4_11 = 2130837598;
        public static final int type4_12 = 2130837599;
        public static final int type4_13 = 2130837600;
        public static final int type4_14 = 2130837601;
        public static final int type4_15 = 2130837602;
        public static final int type4_16 = 2130837603;
        public static final int type4_17 = 2130837604;
        public static final int type4_18 = 2130837605;
        public static final int type4_19 = 2130837606;
        public static final int type4_2 = 2130837607;
        public static final int type4_20 = 2130837608;
        public static final int type4_21 = 2130837609;
        public static final int type4_22 = 2130837610;
        public static final int type4_23 = 2130837611;
        public static final int type4_24 = 2130837612;
        public static final int type4_3 = 2130837613;
        public static final int type4_4 = 2130837614;
        public static final int type4_5 = 2130837615;
        public static final int type4_6 = 2130837616;
        public static final int type4_7 = 2130837617;
        public static final int type4_8 = 2130837618;
        public static final int type4_9 = 2130837619;
        public static final int type5_1 = 2130837620;
        public static final int type5_10 = 2130837621;
        public static final int type5_11 = 2130837622;
        public static final int type5_12 = 2130837623;
        public static final int type5_13 = 2130837624;
        public static final int type5_14 = 2130837625;
        public static final int type5_15 = 2130837626;
        public static final int type5_16 = 2130837627;
        public static final int type5_17 = 2130837628;
        public static final int type5_18 = 2130837629;
        public static final int type5_19 = 2130837630;
        public static final int type5_2 = 2130837631;
        public static final int type5_20 = 2130837632;
        public static final int type5_21 = 2130837633;
        public static final int type5_22 = 2130837634;
        public static final int type5_23 = 2130837635;
        public static final int type5_24 = 2130837636;
        public static final int type5_3 = 2130837637;
        public static final int type5_4 = 2130837638;
        public static final int type5_5 = 2130837639;
        public static final int type5_6 = 2130837640;
        public static final int type5_7 = 2130837641;
        public static final int type5_8 = 2130837642;
        public static final int type5_9 = 2130837643;
        public static final int type6_1 = 2130837644;
        public static final int type6_10 = 2130837645;
        public static final int type6_11 = 2130837646;
        public static final int type6_12 = 2130837647;
        public static final int type6_13 = 2130837648;
        public static final int type6_14 = 2130837649;
        public static final int type6_15 = 2130837650;
        public static final int type6_16 = 2130837651;
        public static final int type6_17 = 2130837652;
        public static final int type6_18 = 2130837653;
        public static final int type6_19 = 2130837654;
        public static final int type6_2 = 2130837655;
        public static final int type6_20 = 2130837656;
        public static final int type6_21 = 2130837657;
        public static final int type6_22 = 2130837658;
        public static final int type6_23 = 2130837659;
        public static final int type6_24 = 2130837660;
        public static final int type6_25 = 2130837661;
        public static final int type6_3 = 2130837662;
        public static final int type6_4 = 2130837663;
        public static final int type6_5 = 2130837664;
        public static final int type6_6 = 2130837665;
        public static final int type6_7 = 2130837666;
        public static final int type6_8 = 2130837667;
        public static final int type6_9 = 2130837668;
        public static final int type7_1 = 2130837669;
        public static final int type7_10 = 2130837670;
        public static final int type7_11 = 2130837671;
        public static final int type7_12 = 2130837672;
        public static final int type7_13 = 2130837673;
        public static final int type7_14 = 2130837674;
        public static final int type7_15 = 2130837675;
        public static final int type7_16 = 2130837676;
        public static final int type7_17 = 2130837677;
        public static final int type7_18 = 2130837678;
        public static final int type7_19 = 2130837679;
        public static final int type7_2 = 2130837680;
        public static final int type7_20 = 2130837681;
        public static final int type7_21 = 2130837682;
        public static final int type7_22 = 2130837683;
        public static final int type7_23 = 2130837684;
        public static final int type7_24 = 2130837685;
        public static final int type7_3 = 2130837686;
        public static final int type7_4 = 2130837687;
        public static final int type7_5 = 2130837688;
        public static final int type7_6 = 2130837689;
        public static final int type7_7 = 2130837690;
        public static final int type7_8 = 2130837691;
        public static final int type7_9 = 2130837692;
        public static final int type8_1 = 2130837693;
        public static final int type8_10 = 2130837694;
        public static final int type8_11 = 2130837695;
        public static final int type8_12 = 2130837696;
        public static final int type8_13 = 2130837697;
        public static final int type8_14 = 2130837698;
        public static final int type8_15 = 2130837699;
        public static final int type8_16 = 2130837700;
        public static final int type8_17 = 2130837701;
        public static final int type8_18 = 2130837702;
        public static final int type8_19 = 2130837703;
        public static final int type8_2 = 2130837704;
        public static final int type8_20 = 2130837705;
        public static final int type8_21 = 2130837706;
        public static final int type8_22 = 2130837707;
        public static final int type8_23 = 2130837708;
        public static final int type8_24 = 2130837709;
        public static final int type8_25 = 2130837710;
        public static final int type8_26 = 2130837711;
        public static final int type8_3 = 2130837712;
        public static final int type8_4 = 2130837713;
        public static final int type8_5 = 2130837714;
        public static final int type8_6 = 2130837715;
        public static final int type8_7 = 2130837716;
        public static final int type8_8 = 2130837717;
        public static final int type8_9 = 2130837718;
    }

    public static final class id {
        public static final int ad1 = 2131034121;
        public static final int admobad_entry = 2131034122;
        public static final int admobad_in = 2131034128;
        public static final int button_play = 2131034126;
        public static final int button_slowDown = 2131034125;
        public static final int container = 2131034123;
        public static final int mainLayout = 2131034112;
        public static final int scoreBoard = 2131034124;
        public static final int selfDefendView = 2131034127;
        public static final int type_1 = 2131034113;
        public static final int type_2 = 2131034114;
        public static final int type_3 = 2131034115;
        public static final int type_4 = 2131034116;
        public static final int type_5 = 2131034117;
        public static final int type_6 = 2131034118;
        public static final int type_7 = 2131034119;
        public static final int type_8 = 2131034120;
    }

    public static final class layout {
        public static final int entry = 2130903040;
        public static final int main = 2130903041;
    }

    public static final class string {
        public static final int app_name = 2130968577;
        public static final int hello = 2130968576;
    }

    public static final class styleable {
        public static final int[] TileView = {R.attr.tileSize};
        public static final int TileView_tileSize = 0;
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
        public static final int[] com_madhouse_android_ads_AdView = {R.attr.debug, R.attr.adPosition, R.attr.adInterval, R.attr.adMeasure, R.attr.adBannerAnimation};
        public static final int com_madhouse_android_ads_AdView_adBannerAnimation = 4;
        public static final int com_madhouse_android_ads_AdView_adInterval = 2;
        public static final int com_madhouse_android_ads_AdView_adMeasure = 3;
        public static final int com_madhouse_android_ads_AdView_adPosition = 1;
        public static final int com_madhouse_android_ads_AdView_debug = 0;
    }
}
