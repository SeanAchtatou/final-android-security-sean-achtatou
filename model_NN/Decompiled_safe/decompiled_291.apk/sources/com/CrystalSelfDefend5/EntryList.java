package com.CrystalSelfDefend5;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import com.admob.android.ads.AdManager;
import com.admob.android.ads.AdView;
import com.admob.android.ads.SimpleAdListener;

public class EntryList extends Activity {
    static final int REQUEST_CODE = 1;
    public AdView ad;
    private Button type_1;
    private View.OnClickListener type_1_listener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(EntryList.this, AnimationActivity.class);
            intent.putExtra("num", "24");
            intent.putExtra("first_String", "type1_");
            EntryList.this.startActivityForResult(intent, 1);
        }
    };
    private Button type_2;
    private View.OnClickListener type_2_listener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(EntryList.this, AnimationActivity.class);
            intent.putExtra("num", "24");
            intent.putExtra("first_String", "type2_");
            EntryList.this.startActivityForResult(intent, 1);
        }
    };
    private Button type_3;
    private View.OnClickListener type_3_listener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(EntryList.this, AnimationActivity.class);
            intent.putExtra("num", "36");
            intent.putExtra("first_String", "type3_");
            EntryList.this.startActivityForResult(intent, 1);
        }
    };
    private Button type_4;
    private View.OnClickListener type_4_listener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(EntryList.this, AnimationActivity.class);
            intent.putExtra("num", "24");
            intent.putExtra("first_String", "type4_");
            EntryList.this.startActivityForResult(intent, 1);
        }
    };
    private Button type_5;
    private View.OnClickListener type_5_listener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(EntryList.this, AnimationActivity.class);
            intent.putExtra("num", "24");
            intent.putExtra("first_String", "type5_");
            EntryList.this.startActivityForResult(intent, 1);
        }
    };
    private Button type_6;
    private View.OnClickListener type_6_listener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(EntryList.this, AnimationActivity.class);
            intent.putExtra("num", "24");
            intent.putExtra("first_String", "type6_");
            EntryList.this.startActivityForResult(intent, 1);
        }
    };
    private Button type_7;
    private View.OnClickListener type_7_listener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(EntryList.this, AnimationActivity.class);
            intent.putExtra("num", "24");
            intent.putExtra("first_String", "type7_");
            EntryList.this.startActivityForResult(intent, 1);
        }
    };
    private Button type_8;
    private View.OnClickListener type_8_listener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(EntryList.this, AnimationActivity.class);
            intent.putExtra("num", "26");
            intent.putExtra("first_String", "type8_");
            EntryList.this.startActivityForResult(intent, 1);
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.entry);
        findButton();
        setButtonListener();
        this.ad = (AdView) findViewById(R.id.admobad_entry);
        AdManager.setPublisherId("a14df043c9f1d3f");
        setAd();
        this.ad.setAdListener(new LunarLanderListener(this, null));
    }

    public void setAd() {
        this.ad.setVisibility(0);
        AlphaAnimation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(400);
        animation.setFillAfter(true);
        animation.setInterpolator(new AccelerateInterpolator());
        this.ad.startAnimation(animation);
    }

    private class LunarLanderListener extends SimpleAdListener {
        private LunarLanderListener() {
        }

        /* synthetic */ LunarLanderListener(EntryList entryList, LunarLanderListener lunarLanderListener) {
            this();
        }

        public void onFailedToReceiveAd(AdView adView) {
            super.onFailedToReceiveAd(adView);
            Log.d("ad", "=" + EntryList.this.ad);
            EntryList.this.ad.requestFreshAd();
            Log.d("Lunar", "onFailedToReceiveAd");
        }

        public void onFailedToReceiveRefreshedAd(AdView adView) {
            super.onFailedToReceiveRefreshedAd(adView);
            EntryList.this.ad.requestFreshAd();
            Log.d("Lunar", "onFailedToReceiveRefreshedAd");
        }

        public void onReceiveAd(AdView adView) {
            super.onReceiveAd(adView);
            Log.d("Lunar", "onReceiveAd");
            EntryList.this.setAd();
            EntryList.this.ad.setRequestInterval(60);
        }

        public void onReceiveRefreshedAd(AdView adView) {
            super.onReceiveRefreshedAd(adView);
            EntryList.this.setAd();
            Log.d("Lunar", "onReceiveRefreshedAd");
            EntryList.this.ad.requestFreshAd();
        }
    }

    private void findButton() {
        this.type_1 = (Button) findViewById(R.id.type_1);
        this.type_1.setBackgroundResource(R.drawable.button_1);
        this.type_1.setText("SKILLS ONE");
        this.type_2 = (Button) findViewById(R.id.type_2);
        this.type_2.setBackgroundResource(R.drawable.button_1);
        this.type_2.setText("SKILLS TWO");
        this.type_3 = (Button) findViewById(R.id.type_3);
        this.type_3.setBackgroundResource(R.drawable.button_1);
        this.type_3.setText("SKILLS Three");
        this.type_4 = (Button) findViewById(R.id.type_4);
        this.type_4.setBackgroundResource(R.drawable.button_1);
        this.type_4.setText("SKILLS Four");
        this.type_5 = (Button) findViewById(R.id.type_5);
        this.type_5.setBackgroundResource(R.drawable.button_1);
        this.type_5.setText("SKILLS Five");
        this.type_6 = (Button) findViewById(R.id.type_6);
        this.type_6.setBackgroundResource(R.drawable.button_1);
        this.type_6.setText("SKILLS Six");
        this.type_7 = (Button) findViewById(R.id.type_7);
        this.type_7.setBackgroundResource(R.drawable.button_1);
        this.type_7.setText("SKILLS Seven");
        this.type_8 = (Button) findViewById(R.id.type_8);
        this.type_8.setBackgroundResource(R.drawable.button_1);
        this.type_8.setText("SKILLS Eghit");
    }

    private void setButtonListener() {
        this.type_1.setOnClickListener(this.type_1_listener);
        this.type_2.setOnClickListener(this.type_2_listener);
        this.type_3.setOnClickListener(this.type_3_listener);
        this.type_4.setOnClickListener(this.type_4_listener);
        this.type_5.setOnClickListener(this.type_5_listener);
        this.type_6.setOnClickListener(this.type_6_listener);
        this.type_7.setOnClickListener(this.type_7_listener);
        this.type_8.setOnClickListener(this.type_8_listener);
    }
}
