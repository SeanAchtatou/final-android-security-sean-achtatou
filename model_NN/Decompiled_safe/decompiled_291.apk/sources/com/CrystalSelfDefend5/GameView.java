package com.CrystalSelfDefend5;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import com.madhouse.android.ads.AdView;

public class GameView extends View {
    public String First_String = "type1_";
    public AnimationDrawable frameAnimation = null;
    Drawable mBitAnimation = null;
    Context mContext = null;
    public int num = 0;

    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        this.frameAnimation = new AnimationDrawable();
        for (int i = 1; i <= this.num; i++) {
            this.mBitAnimation = getResources().getDrawable(getResources().getIdentifier(String.valueOf(this.First_String) + i, "drawable", this.mContext.getPackageName()));
            this.frameAnimation.addFrame(this.mBitAnimation, 150);
        }
        this.frameAnimation.setOneShot(false);
        this.frameAnimation.setBounds(40, 20, 280, AdView.RETRUNCODE_OK);
        setBackgroundDrawable(this.frameAnimation);
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case 19:
                this.frameAnimation.start();
                return true;
            case 20:
                this.frameAnimation.stop();
                return true;
            default:
                return true;
        }
    }

    public void startAnimation() {
        if (this.frameAnimation.isRunning()) {
            this.frameAnimation.stop();
            return;
        }
        this.frameAnimation = new AnimationDrawable();
        for (int i = 1; i <= this.num; i++) {
            this.mBitAnimation = getResources().getDrawable(getResources().getIdentifier(String.valueOf(this.First_String) + i, "drawable", this.mContext.getPackageName()));
            this.frameAnimation.addFrame(this.mBitAnimation, 150);
        }
        this.frameAnimation.setOneShot(false);
        this.frameAnimation.setBounds(40, 20, 280, AdView.RETRUNCODE_OK);
        setBackgroundDrawable(this.frameAnimation);
        this.frameAnimation.start();
    }

    public void slowDown() {
        this.frameAnimation = new AnimationDrawable();
        for (int i = 1; i <= this.num; i++) {
            this.mBitAnimation = getResources().getDrawable(getResources().getIdentifier(String.valueOf(this.First_String) + i, "drawable", this.mContext.getPackageName()));
            this.frameAnimation.addFrame(this.mBitAnimation, 350);
        }
        this.frameAnimation.setOneShot(false);
        this.frameAnimation.setBounds(40, 20, 280, AdView.RETRUNCODE_OK);
        setBackgroundDrawable(this.frameAnimation);
        this.frameAnimation.start();
    }
}
