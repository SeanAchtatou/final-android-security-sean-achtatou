package com.madhouse.android.ads;

import android.content.DialogInterface;
import android.webkit.JsPromptResult;

final class d implements DialogInterface.OnClickListener {
    private final /* synthetic */ JsPromptResult _;

    d(c cVar, JsPromptResult jsPromptResult) {
        this._ = jsPromptResult;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this._.cancel();
    }
}
