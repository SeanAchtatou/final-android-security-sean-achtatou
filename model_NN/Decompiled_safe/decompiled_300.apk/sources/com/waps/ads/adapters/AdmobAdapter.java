package com.waps.ads.adapters;

import android.app.Activity;
import android.util.Log;
import android.view.ViewGroup;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.waps.ads.AdGroupLayout;
import com.waps.ads.AdGroupTargeting;
import com.waps.ads.a.a;
import com.waps.ads.b.c;
import com.waps.ads.f;
import java.text.SimpleDateFormat;

public class AdmobAdapter extends a implements AdListener {
    public AdmobAdapter(AdGroupLayout adGroupLayout, c cVar) {
        super(adGroupLayout, cVar);
    }

    /* access modifiers changed from: protected */
    public String birthdayForAdGroupTargeting() {
        if (AdGroupTargeting.getBirthDate() != null) {
            return new SimpleDateFormat("yyyyMMdd").format(AdGroupTargeting.getBirthDate().getTime());
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public AdRequest.Gender genderForAdGroupTargeting() {
        switch (a.a[AdGroupTargeting.getGender().ordinal()]) {
            case 1:
                return AdRequest.Gender.MALE;
            case 2:
                return AdRequest.Gender.FEMALE;
            default:
                return null;
        }
    }

    public void handle() {
        Activity activity;
        AdGroupLayout adGroupLayout = (AdGroupLayout) this.c.get();
        if (adGroupLayout != null && (activity = (Activity) adGroupLayout.a.get()) != null) {
            AdView adView = new AdView(activity, AdSize.BANNER, this.d.e);
            adView.setAdListener(this);
            adView.loadAd(requestForAdGroupLayout(adGroupLayout));
        }
    }

    /* access modifiers changed from: protected */
    public void log(String str) {
        Log.d("AdGroup_SDK", "GoogleAdapter " + str);
    }

    public void onDismissScreen(Ad ad) {
    }

    public void onFailedToReceiveAd(Ad ad, AdRequest.ErrorCode errorCode) {
        log("failure (" + errorCode + ")");
        ad.setAdListener((AdListener) null);
        AdGroupLayout adGroupLayout = (AdGroupLayout) this.c.get();
        if (adGroupLayout != null) {
            adGroupLayout.rollover();
        }
    }

    public void onLeaveApplication(Ad ad) {
    }

    public void onPresentScreen(Ad ad) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.waps.ads.f.<init>(com.waps.ads.AdGroupLayout, android.view.ViewGroup):void
     arg types: [com.waps.ads.AdGroupLayout, com.google.ads.AdView]
     candidates:
      com.waps.ads.f.<init>(com.waps.ads.AdGroupLayout, android.view.View):void
      com.waps.ads.f.<init>(com.waps.ads.AdGroupLayout, android.view.ViewGroup):void */
    public void onReceiveAd(Ad ad) {
        log("success");
        AdGroupLayout adGroupLayout = (AdGroupLayout) this.c.get();
        if (adGroupLayout != null) {
            if (!(ad instanceof AdView)) {
                log("invalid AdView");
                return;
            }
            adGroupLayout.j.resetRollover();
            adGroupLayout.b.post(new f(adGroupLayout, (ViewGroup) ((AdView) ad)));
            adGroupLayout.rotateThreadedDelayed();
        }
    }

    /* access modifiers changed from: protected */
    public AdRequest requestForAdGroupLayout(AdGroupLayout adGroupLayout) {
        AdRequest adRequest = new AdRequest();
        adRequest.setTesting(AdGroupTargeting.getTestMode());
        adRequest.setGender(genderForAdGroupTargeting());
        adRequest.setBirthday(birthdayForAdGroupTargeting());
        if (adGroupLayout.d.j == 1) {
            adRequest.setLocation(adGroupLayout.j.e);
        }
        adRequest.setKeywords(AdGroupTargeting.getKeywordSet());
        return adRequest;
    }
}
