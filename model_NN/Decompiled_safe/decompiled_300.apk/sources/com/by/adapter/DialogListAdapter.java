package com.by.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.by.bean.UploadPac;
import com.by.pacbell.R;
import java.util.List;

public class DialogListAdapter extends BaseAdapter {
    private Context context;
    private List<UploadPac> list;
    private LayoutInflater mInflater;

    private class ListHolder {
        TextView appName;

        private ListHolder() {
        }

        /* synthetic */ ListHolder(DialogListAdapter dialogListAdapter, ListHolder listHolder) {
            this();
        }
    }

    public DialogListAdapter(Context c) {
        this.context = c;
    }

    public void setList(List<UploadPac> myparklist) {
        this.list = myparklist;
        this.mInflater = (LayoutInflater) this.context.getSystemService("layout_inflater");
    }

    public int getCount() {
        return this.list.size();
    }

    public Object getItem(int index) {
        return this.list.get(index);
    }

    public long getItemId(int index) {
        return (long) index;
    }

    public View getView(int index, View convertView, ViewGroup parent) {
        ListHolder holder;
        if (convertView == null) {
            convertView = this.mInflater.inflate((int) R.layout.show_dialog_list, (ViewGroup) null);
            holder = new ListHolder(this, null);
            holder.appName = (TextView) convertView.findViewById(R.id.dialog_name);
            convertView.setTag(holder);
        } else {
            holder = (ListHolder) convertView.getTag();
        }
        UploadPac uploadInfo = this.list.get(index);
        if (uploadInfo != null) {
            System.out.println("------------" + uploadInfo.getFile_name() + ",holder.appname==" + holder.appName);
            holder.appName.setText(uploadInfo.getFile_name());
        }
        return convertView;
    }
}
