package com.by.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.admogo.util.AdMogoUtil;
import com.by.bean.Ring;
import com.by.pacbell.R;
import java.util.List;

public class GridAdapter extends BaseAdapter {
    private String ImgUrl;
    private Context context;
    private List<Ring> list;
    private LayoutInflater mInflater;

    public class GridHolder {
        public ImageView appImage;
        public TextView appName;

        public GridHolder() {
        }
    }

    public GridAdapter(Context c) {
        this.context = c;
    }

    public void setList(List<Ring> myparklist) {
        this.list = myparklist;
        this.mInflater = (LayoutInflater) this.context.getSystemService("layout_inflater");
    }

    public int getCount() {
        return this.list.size();
    }

    public Object getItem(int index) {
        return this.list.get(index);
    }

    public long getItemId(int index) {
        return (long) index;
    }

    public View getView(int index, View convertView, ViewGroup parent) {
        GridHolder holder;
        if (convertView == null) {
            convertView = this.mInflater.inflate((int) R.layout.grid_item, (ViewGroup) null);
            holder = new GridHolder();
            holder.appImage = (ImageView) convertView.findViewById(R.id.itemImage);
            holder.appName = (TextView) convertView.findViewById(R.id.itemText);
            convertView.setTag(holder);
            holder.appImage.startAnimation(AnimationUtils.loadAnimation(this.context, R.anim.scalestyle));
        } else {
            holder = (GridHolder) convertView.getTag();
        }
        Ring parkInfo = this.list.get(index);
        if (parkInfo != null) {
            holder.appName.setText(parkInfo.getRingName());
            switch (parkInfo.getRingId()) {
                case 1:
                    holder.appImage.setImageResource(R.drawable.ertong);
                    break;
                case 2:
                    holder.appImage.setImageResource(R.drawable.ganenfumu);
                    break;
                case 3:
                    holder.appImage.setImageResource(R.drawable.guoqing);
                    break;
                case 4:
                    holder.appImage.setImageResource(R.drawable.jimo);
                    break;
                case 5:
                    holder.appImage.setImageResource(R.drawable.qixi);
                    break;
                case 6:
                    holder.appImage.setImageResource(R.drawable.shengdan);
                    break;
                case 7:
                    holder.appImage.setImageResource(R.drawable.xinchun);
                    break;
                case 8:
                    holder.appImage.setImageResource(R.drawable.zhongqiu);
                    break;
                case AdMogoUtil.NETWORK_TYPE_CUSTOM:
                    holder.appImage.setImageResource(R.drawable.wangluo);
                    break;
                case AdMogoUtil.NETWORK_TYPE_ADMOGO:
                    holder.appImage.setImageResource(R.drawable.edir);
                    break;
            }
        }
        return convertView;
    }
}
