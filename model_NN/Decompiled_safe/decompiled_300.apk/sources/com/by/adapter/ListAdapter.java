package com.by.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.by.bean.RingSecList;
import com.by.pacbell.R;
import java.util.List;

public class ListAdapter extends BaseAdapter {
    private String ImgUrl;
    private Context context;
    private List<RingSecList> list;
    private LayoutInflater mInflater;

    private class ListHolder {
        ImageView appImage;
        TextView appName;

        private ListHolder() {
        }

        /* synthetic */ ListHolder(ListAdapter listAdapter, ListHolder listHolder) {
            this();
        }
    }

    public ListAdapter(Context c) {
        this.context = c;
    }

    public void setList(List<RingSecList> myparklist) {
        this.list = myparklist;
        this.mInflater = (LayoutInflater) this.context.getSystemService("layout_inflater");
    }

    public int getCount() {
        return this.list.size();
    }

    public Object getItem(int index) {
        return this.list.get(index);
    }

    public long getItemId(int index) {
        return (long) index;
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public View getView(int index, View convertView, ViewGroup parent) {
        ListHolder holder;
        if (convertView == null) {
            convertView = this.mInflater.inflate((int) R.layout.otherlist_show, (ViewGroup) null);
            holder = new ListHolder(this, null);
            holder.appImage = (ImageView) convertView.findViewById(R.id.Title_ImgId);
            holder.appName = (TextView) convertView.findViewById(R.id.Title_name);
            convertView.setTag(holder);
        } else {
            holder = (ListHolder) convertView.getTag();
        }
        RingSecList ringsecInfo = this.list.get(index);
        System.out.println("------------list.get(index)==" + this.list.get(index).getRingsecName());
        if (ringsecInfo != null) {
            holder.appName.setText(ringsecInfo.getRingsecName());
        }
        return convertView;
    }
}
