package com.by.bean;

public class Ring {
    private int ringId;
    private String ringName;

    public Ring() {
    }

    public Ring(int ringId2, String ringName2) {
        this.ringId = ringId2;
        this.ringName = ringName2;
    }

    public int getRingId() {
        return this.ringId;
    }

    public void setRingId(int ringId2) {
        this.ringId = ringId2;
    }

    public String getRingName() {
        return this.ringName;
    }

    public void setRingName(String ringName2) {
        this.ringName = ringName2;
    }
}
