package com.by.bean;

public class RingSecList {
    private int ringkeyId;
    private int ringsecId;
    private String ringsecName;
    private int ringsecNext;
    private int ringsecPre;
    private String ringsecmp3Url;

    public int getRingsecId() {
        return this.ringsecId;
    }

    public void setRingsecId(int ringsecId2) {
        this.ringsecId = ringsecId2;
    }

    public String getRingsecName() {
        return this.ringsecName;
    }

    public void setRingsecName(String ringsecName2) {
        this.ringsecName = ringsecName2;
    }

    public int getRingsecNext() {
        return this.ringsecNext;
    }

    public void setRingsecNext(int ringsecNext2) {
        this.ringsecNext = ringsecNext2;
    }

    public String getRingsecmp3Url() {
        return this.ringsecmp3Url;
    }

    public void setRingsecmp3Url(String ringsecmp3Url2) {
        this.ringsecmp3Url = ringsecmp3Url2;
    }

    public int getRingsecPre() {
        return this.ringsecPre;
    }

    public void setRingsecPre(int ringsecPre2) {
        this.ringsecPre = ringsecPre2;
    }

    public int getRingkeyId() {
        return this.ringkeyId;
    }

    public void setRingkeyId(int ringkeyId2) {
        this.ringkeyId = ringkeyId2;
    }

    public RingSecList(int ringsecId2, String ringsecName2, int ringsecNext2, String ringsecmp3Url2, int ringsecPre2, int ringkeyId2) {
        this.ringsecId = ringsecId2;
        this.ringsecName = ringsecName2;
        this.ringsecNext = ringsecNext2;
        this.ringsecmp3Url = ringsecmp3Url2;
        this.ringsecPre = ringsecPre2;
        this.ringkeyId = ringkeyId2;
    }

    public RingSecList() {
    }
}
