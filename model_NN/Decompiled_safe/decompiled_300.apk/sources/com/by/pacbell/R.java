package com.by.pacbell;

public final class R {

    public static final class anim {
        public static final int scalestyle = 2130968576;
        public static final int shape = 2130968577;
    }

    public static final class attr {
        public static final int adBannerAnimation = 2130771972;
        public static final int adInterval = 2130771970;
        public static final int adMeasure = 2130771971;
        public static final int adPosition = 2130771969;
        public static final int background = 2130771978;
        public static final int backgroundColor = 2130771973;
        public static final int debug = 2130771968;
        public static final int isTesting = 2130771982;
        public static final int keywords = 2130771975;
        public static final int primaryText = 2130771979;
        public static final int primaryTextColor = 2130771974;
        public static final int refresh = 2130771981;
        public static final int refreshInterval = 2130771977;
        public static final int secondaryTextColor = 2130771980;
        public static final int spots = 2130771976;
    }

    public static final class drawable {
        public static final int black = 2130837524;
        public static final int download = 2130837504;
        public static final int edir = 2130837505;
        public static final int ertong = 2130837506;
        public static final int font_kuang = 2130837507;
        public static final int ganenfumu = 2130837508;
        public static final int guoqing = 2130837509;
        public static final int icon = 2130837510;
        public static final int jimo = 2130837511;
        public static final int list_bg = 2130837512;
        public static final int local_music = 2130837513;
        public static final int main_bg = 2130837514;
        public static final int musicplay_bg = 2130837515;
        public static final int qixi = 2130837516;
        public static final int shengdan = 2130837517;
        public static final int small_title = 2130837518;
        public static final int up_load = 2130837519;
        public static final int wangluo = 2130837520;
        public static final int white = 2130837523;
        public static final int xinchun = 2130837521;
        public static final int zhongqiu = 2130837522;
    }

    public static final class id {
        public static final int Edit_Name = 2131099672;
        public static final int Title_ImgId = 2131099663;
        public static final int Title_name = 2131099664;
        public static final int ad1 = 2131099654;
        public static final int admogo_layout = 2131099655;
        public static final int currenttime = 2131099648;
        public static final int dialog_list = 2131099670;
        public static final int dialog_name = 2131099669;
        public static final int downbutton = 2131099665;
        public static final int gridlist = 2131099660;
        public static final int itemImage = 2131099656;
        public static final int itemText = 2131099657;
        public static final int linbottom = 2131099668;
        public static final int local_mp3 = 2131099658;
        public static final int next = 2131099651;
        public static final int other_list = 2131099661;
        public static final int pause = 2131099650;
        public static final int prev = 2131099649;
        public static final int progress = 2131099653;
        public static final int set_ring = 2131099666;
        public static final int showName_describute = 2131099671;
        public static final int title = 2131099662;
        public static final int titleTextView = 2131099667;
        public static final int totaltime = 2131099652;
        public static final int upload_File = 2131099673;
        public static final int uploadbutton = 2131099659;
    }

    public static final class layout {
        public static final int audio_player = 2130903040;
        public static final int com_add_adwo = 2130903041;
        public static final int com_add_mogo = 2130903042;
        public static final int common_ad_bottom = 2130903043;
        public static final int common_ad_top = 2130903044;
        public static final int grid_item = 2130903045;
        public static final int main = 2130903046;
        public static final int other_list = 2130903047;
        public static final int otherlist_show = 2130903048;
        public static final int player_activity = 2130903049;
        public static final int show_dialog_list = 2130903050;
        public static final int showlistdialog = 2130903051;
    }

    public static final class string {
        public static final int app_name = 2131034113;
        public static final int descr = 2131034115;
        public static final int durationformatlong = 2131034119;
        public static final int durationformatshort = 2131034118;
        public static final int exit = 2131034117;
        public static final int hello = 2131034112;
        public static final int other_list = 2131034114;
        public static final int shezhi = 2131034116;
    }

    public static final class styleable {
        public static final int[] cn_domob_android_ads_DomobAdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.keywords, R.attr.spots, R.attr.refreshInterval};
        public static final int cn_domob_android_ads_DomobAdView_backgroundColor = 0;
        public static final int cn_domob_android_ads_DomobAdView_keywords = 2;
        public static final int cn_domob_android_ads_DomobAdView_primaryTextColor = 1;
        public static final int cn_domob_android_ads_DomobAdView_refreshInterval = 4;
        public static final int cn_domob_android_ads_DomobAdView_spots = 3;
        public static final int[] com_adwo_adsdk_AdwoAdView = {R.attr.background, R.attr.primaryText, R.attr.secondaryTextColor, R.attr.refresh, R.attr.isTesting};
        public static final int com_adwo_adsdk_AdwoAdView_background = 0;
        public static final int com_adwo_adsdk_AdwoAdView_isTesting = 4;
        public static final int com_adwo_adsdk_AdwoAdView_primaryText = 1;
        public static final int com_adwo_adsdk_AdwoAdView_refresh = 3;
        public static final int com_adwo_adsdk_AdwoAdView_secondaryTextColor = 2;
        public static final int[] com_madhouse_android_ads_AdView = {R.attr.debug, R.attr.adPosition, R.attr.adInterval, R.attr.adMeasure, R.attr.adBannerAnimation};
        public static final int com_madhouse_android_ads_AdView_adBannerAnimation = 4;
        public static final int com_madhouse_android_ads_AdView_adInterval = 2;
        public static final int com_madhouse_android_ads_AdView_adMeasure = 3;
        public static final int com_madhouse_android_ads_AdView_adPosition = 1;
        public static final int com_madhouse_android_ads_AdView_debug = 0;
    }
}
