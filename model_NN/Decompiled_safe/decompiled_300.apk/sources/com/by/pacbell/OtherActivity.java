package com.by.pacbell;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.by.adapter.ListAdapter;
import com.by.bean.RingSecList;
import com.by.constant.Constant;
import com.by.download.DownloadTitleTask;
import com.by.utils.FileUtils;
import java.util.List;

public class OtherActivity extends Activity {
    static final int REQUEST_CODE = 0;
    ListView other_list;
    String parkid = "";
    public List<RingSecList> ringseclist;

    public void work(List<RingSecList> result) {
        this.ringseclist = result;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.other_list);
        bindsWidget();
        this.parkid = getIntent().getStringExtra("tabsId");
        System.out.println("park================id=" + this.parkid);
        if (this.parkid == null || this.parkid.equals("0")) {
            this.ringseclist = new FileUtils().getMp3Files(Constant.PATHMp3);
            System.out.println("----------------ringseclist===" + this.ringseclist.size());
            ListAdapter adapter = new ListAdapter(this);
            adapter.setList(this.ringseclist);
            this.other_list.setAdapter((android.widget.ListAdapter) adapter);
        } else {
            int parkId = Integer.parseInt(this.parkid);
            new DownloadTitleTask(this, this.other_list, this.ringseclist).execute(new StringBuilder(String.valueOf(parkId)).toString());
        }
        this.other_list.setOnItemClickListener(new otherItemOnclick());
    }

    private void bindsWidget() {
        this.other_list = (ListView) findViewById(R.id.other_list);
    }

    class otherItemOnclick implements AdapterView.OnItemClickListener {
        otherItemOnclick() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int postion, long arg3) {
            if (OtherActivity.this.parkid == null || !OtherActivity.this.parkid.equals("0")) {
                RingSecList ringlist = OtherActivity.this.ringseclist.get(postion);
                Intent inten = new Intent(OtherActivity.this, MusicPlayer.class);
                String title_name = ringlist.getRingsecName();
                String title_fileurl = ringlist.getRingsecmp3Url();
                int pre = ringlist.getRingsecPre();
                int next = ringlist.getRingsecNext();
                inten.putExtra("title_name", title_name);
                inten.putExtra("title_fileurl", title_fileurl);
                inten.putExtra("pre", pre);
                inten.putExtra("next", next);
                inten.putExtra("parkid", OtherActivity.this.parkid);
                OtherActivity.this.startActivity(inten);
                return;
            }
            RingSecList ringlist2 = OtherActivity.this.ringseclist.get(postion);
            Intent inten2 = new Intent(OtherActivity.this, MusicPlayer.class);
            String title_name2 = ringlist2.getRingsecName();
            String title_fileurl2 = ringlist2.getRingsecmp3Url();
            inten2.putExtra("title_name", title_name2);
            inten2.putExtra("title_fileurl", title_fileurl2);
            OtherActivity.this.startActivity(inten2);
        }
    }
}
