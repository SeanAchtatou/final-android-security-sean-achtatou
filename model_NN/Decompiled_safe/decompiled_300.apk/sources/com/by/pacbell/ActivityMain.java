package com.by.pacbell;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.by.adapter.DialogListAdapter;
import com.by.bean.Ring;
import com.by.bean.UploadPac;
import com.by.download.DownloadTask;
import com.by.task.UpfileTask;
import com.by.utils.FileUtils;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ActivityMain extends Activity {
    public String PacurlName = "";
    EditText eidt;
    GridView gridView;
    ListView listview;
    ImageButton local_mp3;
    List<UploadPac> mypaclist;
    List<Ring> ringlist = new ArrayList();
    public String root = "";
    TextView showName_describute;
    Button upload;
    ImageButton upload_mp3;

    public void work(List<Ring> result) {
        this.ringlist = result;
    }

    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.gridView = (GridView) findViewById(R.id.gridlist);
        new DownloadTask(this, this.gridView).execute(new String[0]);
        this.gridView.setOnItemClickListener(new mainClickListener());
        this.local_mp3 = (ImageButton) findViewById(R.id.local_mp3);
        this.local_mp3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                ActivityMain.this.IntentTest(ActivityMain.this, OtherActivity.class, "tabsId", "0");
            }
        });
        this.upload_mp3 = (ImageButton) findViewById(R.id.uploadbutton);
        this.upload_mp3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                ActivityMain.this.ShowMessageDialog();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void ShowMessageDialog() {
        View textEntryView = LayoutInflater.from(this).inflate((int) R.layout.showlistdialog, (ViewGroup) null);
        this.listview = (ListView) textEntryView.findViewById(R.id.dialog_list);
        this.eidt = (EditText) textEntryView.findViewById(R.id.Edit_Name);
        this.upload = (Button) textEntryView.findViewById(R.id.upload_File);
        this.showName_describute = (TextView) textEntryView.findViewById(R.id.showName_describute);
        if (this.PacurlName != "") {
            this.PacurlName = "";
        }
        Updatelist(this.listview, "/");
        this.listview.setOnItemClickListener(new pacNameListViewListener());
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setIcon((int) R.drawable.icon);
        builder.setTitle("文件列表");
        builder.setView(textEntryView);
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                ActivityMain.this.setTitle("");
            }
        });
        builder.show();
    }

    class pacNameListViewListener implements AdapterView.OnItemClickListener {
        pacNameListViewListener() {
        }

        public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
            ActivityMain.this.root = ActivityMain.this.mypaclist.get(position).getFile_name();
            System.out.println("-----------------" + ActivityMain.this.root);
            if (ActivityMain.this.root.endsWith(".mp3")) {
                ActivityMain.this.root = String.valueOf(ActivityMain.this.PacurlName) + "/" + ActivityMain.this.root;
                ActivityMain.this.mypaclist = ActivityMain.this.Updatelist(ActivityMain.this.listview, ActivityMain.this.root);
                if (ActivityMain.this.mypaclist != null && ActivityMain.this.mypaclist.size() == 0) {
                    Toast.makeText(ActivityMain.this, "您可以上传MP3文件了", 0).show();
                    ActivityMain.this.listview.setVisibility(8);
                    ActivityMain.this.upload.setVisibility(0);
                    ActivityMain.this.showName_describute.setVisibility(0);
                    ActivityMain.this.eidt.setVisibility(0);
                    ActivityMain.this.upload.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View arg0) {
                            String pacName = ActivityMain.this.eidt.getText().toString();
                            if (pacName == "") {
                                Toast.makeText(ActivityMain.this, "描述不能为空，可能会丢失数据", 0);
                                return;
                            }
                            new UpfileTask(ActivityMain.this).execute(ActivityMain.this.root, pacName);
                        }
                    });
                    return;
                }
                return;
            }
            ActivityMain.this.root = String.valueOf(ActivityMain.this.PacurlName) + "/" + ActivityMain.this.root;
            System.out.println("root==========" + ActivityMain.this.root);
            File isfile = new File(String.valueOf(String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + File.separator) + ActivityMain.this.root);
            if (isfile.isDirectory()) {
                if (isfile.listFiles().length == 0) {
                    Toast.makeText(ActivityMain.this, "文件夹已为空", 0).show();
                    return;
                }
                ActivityMain.this.mypaclist = ActivityMain.this.Updatelist(ActivityMain.this.listview, ActivityMain.this.root);
                ActivityMain.this.PacurlName = ActivityMain.this.root;
            } else if (isfile.isFile()) {
                Toast.makeText(ActivityMain.this, "已经找到文件了，但不支持上传文件格式", 0).show();
            }
        }
    }

    public List<UploadPac> Updatelist(ListView l, String root2) {
        this.mypaclist = new FileUtils().getSdFiles(root2);
        DialogListAdapter adapter = new DialogListAdapter(this);
        adapter.setList(this.mypaclist);
        l.setAdapter((ListAdapter) adapter);
        return this.mypaclist;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        finish();
        return super.onKeyDown(keyCode, event);
    }

    class mainClickListener implements AdapterView.OnItemClickListener {
        mainClickListener() {
        }

        public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
            ActivityMain.this.IntentTest(ActivityMain.this, OtherActivity.class, "tabsId", new StringBuilder(String.valueOf(ActivityMain.this.ringlist.get(position).getRingId())).toString());
        }
    }

    /* access modifiers changed from: private */
    public void IntentTest(ActivityMain a, Class<OtherActivity> class2, String strkey, String strvalue) {
        Intent intent = new Intent();
        intent.setClass(a, class2);
        intent.putExtra(strkey, strvalue);
        startActivity(intent);
    }
}
