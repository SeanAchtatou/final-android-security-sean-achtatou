package com.by.pacbell;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import com.by.bean.RingSecList;
import com.by.constant.Constant;
import com.by.doamin.CheckWork;
import com.by.download.DownLoadMp3Task;
import com.by.download.HttpDownloader;
import com.by.server.ServerRingSecList;
import com.by.utils.FileUtils;
import com.by.utils.MusicUtils;
import com.by.utils.Utils;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class MusicPlayer extends Activity {
    static final int RESULT_BACK = 1;
    public static Map<String, String> local = new HashMap();
    Bitmap bm;
    CheckWork checked = new CheckWork();
    /* access modifiers changed from: private */
    public int curPosition;
    /* access modifiers changed from: private */
    public TextView currenttime;
    private Button downbutton;
    private int failplaysize;
    int failsizee = 0;
    /* access modifiers changed from: private */
    public String fileurl;
    ImageButton mPauseButton;
    private MediaPlayer.OnPreparedListener mediaPreparedListener = new MediaPlayer.OnPreparedListener() {
        public void onPrepared(MediaPlayer mp) {
            MusicPlayer.this.progress.setMax(MusicPlayer.this.myMediaPlayer.getDuration());
            MusicPlayer.this.myHandler.sendEmptyMessage(1);
            MusicPlayer.this.myMediaPlayer.start();
            MusicPlayer.this.setPauseButtonImage();
            MusicPlayer.this.totaltime.setText(MusicUtils.makeTimeString(MusicPlayer.this, (long) (MusicPlayer.this.myMediaPlayer.getDuration() / 1000)));
        }
    };
    /* access modifiers changed from: private */
    public Handler myHandler;
    /* access modifiers changed from: private */
    public MediaPlayer myMediaPlayer;
    /* access modifiers changed from: private */
    public String name;
    /* access modifiers changed from: private */
    public int next_Page;
    private ImageButton next_btn;
    /* access modifiers changed from: private */
    public int pre_Page;
    private ImageButton prev_btn;
    /* access modifiers changed from: private */
    public SeekBar progress;
    private SeekBar.OnSeekBarChangeListener seekbarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (fromUser) {
                MusicPlayer.this.myMediaPlayer.seekTo(progress);
            }
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    };
    private Button set_ringbutton;
    /* access modifiers changed from: private */
    public TextView titleTextView;
    TextView title_back = null;
    /* access modifiers changed from: private */
    public TextView totaltime;
    private Uri uri;
    /* access modifiers changed from: private */
    public String url;

    /* access modifiers changed from: private */
    public void doPauseResume() {
        try {
            if (this.myMediaPlayer != null) {
                if (this.myMediaPlayer.isPlaying()) {
                    this.myMediaPlayer.pause();
                } else {
                    this.myMediaPlayer.start();
                }
                setPauseButtonImage();
            }
        } catch (Exception e) {
        }
    }

    public void init() {
        this.myHandler = new Handler() {
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 1:
                        try {
                            countTime();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        MusicPlayer.this.progress.setProgress(MusicPlayer.this.curPosition);
                        MusicPlayer.this.myHandler.sendEmptyMessageDelayed(1, 1000);
                        return;
                    default:
                        return;
                }
            }

            private void countTime() {
                if (MusicPlayer.this.myMediaPlayer != null) {
                    if (MusicPlayer.this.myMediaPlayer.getCurrentPosition() != 0) {
                        MusicPlayer.this.curPosition = MusicPlayer.this.myMediaPlayer.getCurrentPosition();
                    }
                    MusicPlayer.this.currenttime.setText(MusicUtils.makeTimeString(MusicPlayer.this, (long) (MusicPlayer.this.curPosition / 1000)));
                    if (MusicPlayer.this.myMediaPlayer.isPlaying()) {
                        MusicPlayer.this.currenttime.setVisibility(0);
                        return;
                    }
                    MusicPlayer.this.currenttime.setVisibility(MusicPlayer.this.currenttime.getVisibility() == 4 ? 0 : 4);
                }
            }
        };
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        bindViews();
        bindevents();
        Intent itent = getIntent();
        this.name = itent.getStringExtra("title_name");
        this.fileurl = itent.getStringExtra("title_fileurl");
        this.pre_Page = itent.getIntExtra("pre", 0);
        this.next_Page = itent.getIntExtra("next", 0);
        if (this.fileurl != null) {
            playAudio(this.fileurl, this.name);
            init();
            return;
        }
        Toast.makeText(this, "服务端获取数据失败！", 0);
    }

    public void bindViews() {
        setContentView((int) R.layout.player_activity);
        this.progress = (SeekBar) findViewById(R.id.progress);
        this.currenttime = (TextView) findViewById(R.id.currenttime);
        this.totaltime = (TextView) findViewById(R.id.totaltime);
        this.titleTextView = (TextView) findViewById(R.id.titleTextView);
        this.mPauseButton = (ImageButton) findViewById(R.id.pause);
        this.prev_btn = (ImageButton) findViewById(R.id.prev);
        this.next_btn = (ImageButton) findViewById(R.id.next);
        this.downbutton = (Button) findViewById(R.id.downbutton);
        this.set_ringbutton = (Button) findViewById(R.id.set_ring);
    }

    private void bindevents() {
        this.prev_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (MusicPlayer.this.pre_Page == -1) {
                    Toast.makeText(MusicPlayer.this, "已经到首页", 0).show();
                    return;
                }
                try {
                    RingSecList item = ServerRingSecList.FindListId(Integer.valueOf(MusicPlayer.this.pre_Page));
                    if (item == null) {
                        Toast.makeText(MusicPlayer.this, "请求服务器失败，请在尝试一次", 0).show();
                        return;
                    }
                    MusicPlayer.this.fileurl = item.getRingsecmp3Url();
                    MusicPlayer.this.pre_Page = item.getRingsecPre();
                    MusicPlayer.this.next_Page = item.getRingsecNext();
                    MusicPlayer.this.name = item.getRingsecName();
                    MusicPlayer.this.playAudio(MusicPlayer.this.fileurl, MusicPlayer.this.name);
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(MusicPlayer.this, "系统没找到相关内容", 0).show();
                }
            }
        });
        this.next_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (MusicPlayer.this.next_Page == -1) {
                    Toast.makeText(MusicPlayer.this, "已经到最后一页", 0).show();
                    return;
                }
                try {
                    RingSecList item = ServerRingSecList.FindListId(Integer.valueOf(MusicPlayer.this.next_Page));
                    if (item == null) {
                        Toast.makeText(MusicPlayer.this, "请求服务器失败，请在尝试一次", 0).show();
                        return;
                    }
                    MusicPlayer.this.fileurl = item.getRingsecmp3Url();
                    MusicPlayer.this.pre_Page = item.getRingsecPre();
                    MusicPlayer.this.next_Page = item.getRingsecNext();
                    MusicPlayer.this.name = item.getRingsecName();
                    MusicPlayer.this.playAudio(MusicPlayer.this.fileurl, MusicPlayer.this.name);
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(MusicPlayer.this, "系统没找到相关内容", 0).show();
                }
            }
        });
        this.mPauseButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MusicPlayer.this.doPauseResume();
            }
        });
        this.progress.setOnSeekBarChangeListener(this.seekbarChangeListener);
        this.downbutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (new File(String.valueOf(Constant.baoyiringmp3) + MusicPlayer.this.name + ".mp3").exists()) {
                    MusicPlayer.this.fileurl = MusicPlayer.local.get(MusicPlayer.this.name);
                    MusicPlayer.this.buildDialog1(MusicPlayer.this, MusicPlayer.this.fileurl, MusicPlayer.this.name).show();
                    return;
                }
                new DownLoadMp3Task(MusicPlayer.this).execute(MusicPlayer.this.fileurl, MusicPlayer.this.name);
                MusicPlayer.local.put(MusicPlayer.this.name, MusicPlayer.this.fileurl);
            }
        });
        this.set_ringbutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FileUtils fileutils = new FileUtils();
                System.out.println("fileurl=====" + MusicPlayer.this.name);
                if (Boolean.valueOf(fileutils.isFileExist(String.valueOf(MusicPlayer.this.name) + ".mp3", Constant.PATHMp3)).booleanValue()) {
                    MusicPlayer.this.buildDialog2(MusicPlayer.this, String.valueOf(MusicPlayer.this.name) + ".mp3").show();
                } else {
                    MusicPlayer.this.showDialog();
                }
            }
        });
    }

    public void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("对不起,下载之后才可以设置铃声");
        builder.setNegativeButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    /* access modifiers changed from: private */
    public Dialog buildDialog2(Context context, final String musicName) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon((int) R.drawable.icon);
        builder.setTitle("确定将" + musicName + "设置为手机铃声么？");
        builder.setPositiveButton("设置手机铃声", new DialogInterface.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
            public void onClick(DialogInterface dialog, int whichButton) {
                String SDCard = Environment.getExternalStorageDirectory().getAbsolutePath();
                String path = String.valueOf(SDCard) + File.separator + Constant.PATHMp3;
                File file = new File(String.valueOf(SDCard) + File.separator + "media");
                if (!file.exists()) {
                    file.mkdir();
                    File file2 = new File(String.valueOf(SDCard) + File.separator + "media/audio");
                    if (!file2.exists()) {
                        file2.mkdir();
                    }
                    File file3 = new File(String.valueOf(SDCard) + File.separator + "media/audio/ringtones");
                    if (!file3.exists()) {
                        file3.mkdir();
                    }
                } else {
                    File file4 = new File(String.valueOf(SDCard) + File.separator + "media/audio");
                    if (!file4.exists()) {
                        file4.mkdir();
                        File file5 = new File(String.valueOf(SDCard) + File.separator + "media/audio/ringtones");
                        if (!file5.exists()) {
                            file5.mkdir();
                        }
                    } else {
                        File file6 = new File(String.valueOf(SDCard) + File.separator + "media/audio/ringtones");
                        if (!file6.exists()) {
                            file6.mkdir();
                        }
                    }
                }
                if (new FileUtils().readSDCard() / 1024 < 200) {
                    Toast.makeText(MusicPlayer.this, "对不起，剩余空间不足！", 1).show();
                    return;
                }
                String musicrealname = String.valueOf(Utils.getMD5Str(musicName).toLowerCase()) + ".mp3";
                int copyFile = new HttpDownloader().copyFile(String.valueOf(path) + musicName, "media/audio/ringtones", musicrealname);
                try {
                    File file7 = new FileUtils().creatSDFile(musicrealname, "media/audio/ringtones");
                    ContentValues values = new ContentValues();
                    values.put("_data", file7.getAbsolutePath());
                    values.put("is_ringtone", (Boolean) true);
                    RingtoneManager.setActualDefaultRingtoneUri(MusicPlayer.this, 1, MusicPlayer.this.getContentResolver().insert(MediaStore.Audio.Media.getContentUriForPath(file7.getAbsolutePath()), values));
                    Toast.makeText(MusicPlayer.this, "已将" + musicName + "设置为手机铃声", 1).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        return builder.create();
    }

    /* access modifiers changed from: private */
    public void playAudio(String u, String n) {
        this.fileurl = u;
        this.name = n;
        String files = String.valueOf(Constant.baoyiringmp3) + this.name + ".mp3";
        if (new File(files).exists()) {
            this.url = "file://" + files;
            this.titleTextView.setText(this.name);
            setPath(this.url);
            this.prev_btn.setVisibility(8);
            this.next_btn.setVisibility(8);
            return;
        }
        this.url = String.valueOf(Constant.server_mp3_url) + this.fileurl;
        this.titleTextView.setText(this.name);
        new PlayTask(this).execute(this.url);
    }

    /* access modifiers changed from: private */
    public Dialog buildDialog1(Context t, final String fileurl1, final String musicName) {
        AlertDialog.Builder builder = new AlertDialog.Builder(t);
        builder.setIcon((int) R.drawable.download);
        builder.setTitle("确定重新下载" + musicName + "吗？");
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                new DownLoadMp3Task(MusicPlayer.this).execute(fileurl1, musicName);
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        return builder.create();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            relasePlayer();
            Bundle extras = getIntent().getExtras();
            Intent intent = new Intent();
            intent.putExtras(extras);
            setResult(-1, intent);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void relasePlayer() {
        if (this.myMediaPlayer != null) {
            this.myMediaPlayer.reset();
            this.myMediaPlayer.release();
            this.myMediaPlayer = null;
        }
    }

    public void setPath(String uriString) {
        System.out.println("---------uristring========" + uriString);
        try {
            this.uri = Uri.parse(uriString);
            relasePlayer();
            this.myMediaPlayer = new MediaPlayer();
            this.myMediaPlayer.setOnPreparedListener(this.mediaPreparedListener);
            this.myMediaPlayer.setDataSource(this, this.uri);
            this.myMediaPlayer.prepare();
            this.failplaysize = 0;
        } catch (Exception e) {
            this.failplaysize++;
            if (this.failplaysize < 2) {
                setPath(uriString);
            } else {
                Toast.makeText(this, "Mp3文件加载失败", 1).show();
                throw new RuntimeException("缓冲失败");
            }
        }
    }

    /* access modifiers changed from: private */
    public void setPauseButtonImage() {
        try {
            if (this.myMediaPlayer == null || !this.myMediaPlayer.isPlaying()) {
                this.mPauseButton.setImageResource(17301540);
            } else {
                this.mPauseButton.setImageResource(17301539);
            }
        } catch (Exception e) {
        }
    }

    public class PlayTask extends AsyncTask<String, String, Void> {
        Context curcontext;
        private int isdown;

        public PlayTask(Context context) {
            this.curcontext = context;
        }

        public void onPreExecute() {
            MusicPlayer.this.titleTextView.setText("正在为你缓冲铃声,请稍候！");
            super.onPreExecute();
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            super.onCancelled();
        }

        public Void doInBackground(String... params) {
            MusicPlayer.this.url = params[0];
            try {
                MusicPlayer.this.setPath(MusicPlayer.this.url);
                this.isdown = 100;
                return null;
            } catch (Exception e) {
                this.isdown = -100;
                e.printStackTrace();
                publishProgress("请检查网络是否连接正常！");
                try {
                    Thread.sleep(1500);
                    return null;
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                    return null;
                }
            }
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(String... aa) {
            MusicPlayer.this.titleTextView.setText(aa[0]);
        }

        public void onPostExecute(Void url) {
            super.onPostExecute((Object) url);
            MusicPlayer.this.titleTextView.setText(MusicPlayer.this.name);
        }
    }
}
