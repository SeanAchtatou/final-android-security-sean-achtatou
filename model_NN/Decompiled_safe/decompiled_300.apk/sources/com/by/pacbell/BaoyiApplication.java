package com.by.pacbell;

import android.app.Application;
import android.content.pm.PackageManager;
import com.by.constant.Constant;
import com.by.utils.DiskMp3Cache;
import com.by.utils.DiskTextCache;
import java.io.File;

public class BaoyiApplication extends Application {
    public static String TAG = "baoyi";
    private static BaoyiApplication instance;
    private DiskMp3Cache disMp3Cache;
    private DiskTextCache diskTextCache;

    public static BaoyiApplication getInstance() {
        return instance;
    }

    public DiskMp3Cache getDisMp3Cache() {
        return this.disMp3Cache;
    }

    public void setDisMp3Cache(DiskMp3Cache disMp3Cache2) {
        this.disMp3Cache = disMp3Cache2;
    }

    public DiskTextCache getDiskTextCache() {
        return this.diskTextCache;
    }

    public String getVersion() {
        try {
            return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "0.0.0";
        }
    }

    private void inticacheDiskNovel() {
        File disk = new File(Constant.baoyiringtext);
        if (!disk.exists() && !disk.isDirectory()) {
            if (disk.mkdirs()) {
                System.out.println(" ok:创建txt成功！ ");
            } else {
                System.out.println(" err:创建txt失败！ ");
            }
        }
    }

    private void inticacheDiskMp3() {
        File disk = new File(Constant.baoyiringmp3);
        if (!disk.exists() && !disk.isDirectory()) {
            if (disk.mkdirs()) {
                System.out.println(" ok:创建MP3文件夹成功！ ");
            } else {
                System.out.println(" err:创建MP3文件夹成功！ ");
            }
        }
    }

    public void onCreate() {
        super.onCreate();
        inticacheDiskNovel();
        inticacheDiskMp3();
        instance = this;
        this.diskTextCache = new DiskTextCache();
        this.disMp3Cache = new DiskMp3Cache();
    }
}
