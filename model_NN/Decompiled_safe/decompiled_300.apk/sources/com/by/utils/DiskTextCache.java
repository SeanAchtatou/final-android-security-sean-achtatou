package com.by.utils;

import android.util.Log;
import com.by.constant.Constant;
import com.by.pacbell.BaoyiApplication;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.WeakHashMap;

public class DiskTextCache extends WeakHashMap<String, String> {
    private static final String charset = "utf-8";

    public String get(Object key) {
        File fileitem = new File(String.valueOf(Constant.baoyiringtext) + Utils.getMD5Str(key.toString()) + ".txt");
        if (!fileitem.exists()) {
            Log.d(BaoyiApplication.TAG, "缓存中没有读取txt");
            return null;
        }
        try {
            ByteBuffer buffers = DataUtil.readToByteBuffer(new FileInputStream(fileitem));
            if (charset == 0) {
                return Charset.forName(DataUtil.defaultCharset).decode(buffers).toString().replace("\r\n", "\n");
            }
            String text = Charset.forName(charset).decode(buffers).toString().replace("\r\n", "\n");
            buffers.rewind();
            return text;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String putOverride(String key, String value) {
        File fileitem = new File(String.valueOf(Constant.baoyiringtext) + Utils.getMD5Str(key) + ".txt");
        if (key.startsWith("http")) {
            downfiles(key, value, fileitem);
        } else {
            writeText(value, fileitem);
        }
        return value;
    }

    public String put(String key, String value) {
        File fileitem = new File(String.valueOf(Constant.baoyiringtext) + Utils.getMD5Str(key) + ".txt");
        if (fileitem.exists() && fileitem.length() > 100) {
            Log.d(BaoyiApplication.TAG, "磁盘中存在该文件");
        } else if (key.startsWith("http")) {
            downfiles(key, value, fileitem);
        } else {
            writeText(value, fileitem);
        }
        return value;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0025 A[SYNTHETIC, Splitter:B:20:0x0025] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x002b A[SYNTHETIC, Splitter:B:24:0x002b] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x003d A[SYNTHETIC, Splitter:B:33:0x003d] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0043 A[SYNTHETIC, Splitter:B:37:0x0043] */
    /* JADX WARNING: Removed duplicated region for block: B:60:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void writeText(java.lang.String r7, java.io.File r8) {
        /*
            r6 = this;
            r3 = 0
            r1 = 0
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x001e }
            r4.<init>(r8)     // Catch:{ Exception -> 0x001e }
            java.io.OutputStreamWriter r2 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x0063, all -> 0x005c }
            java.lang.String r5 = "UTF-8"
            r2.<init>(r4, r5)     // Catch:{ Exception -> 0x0063, all -> 0x005c }
            r2.write(r7)     // Catch:{ Exception -> 0x0067, all -> 0x005f }
            if (r2 == 0) goto L_0x006e
            r2.close()     // Catch:{ IOException -> 0x0052 }
        L_0x0016:
            r1 = 0
        L_0x0017:
            if (r4 == 0) goto L_0x006c
            r4.close()     // Catch:{ IOException -> 0x0057 }
        L_0x001c:
            r3 = 0
        L_0x001d:
            return
        L_0x001e:
            r5 = move-exception
            r0 = r5
        L_0x0020:
            r0.printStackTrace()     // Catch:{ all -> 0x003a }
            if (r1 == 0) goto L_0x0029
            r1.close()     // Catch:{ IOException -> 0x0030 }
        L_0x0028:
            r1 = 0
        L_0x0029:
            if (r3 == 0) goto L_0x001d
            r3.close()     // Catch:{ IOException -> 0x0035 }
        L_0x002e:
            r3 = 0
            goto L_0x001d
        L_0x0030:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0028
        L_0x0035:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x002e
        L_0x003a:
            r5 = move-exception
        L_0x003b:
            if (r1 == 0) goto L_0x0041
            r1.close()     // Catch:{ IOException -> 0x0048 }
        L_0x0040:
            r1 = 0
        L_0x0041:
            if (r3 == 0) goto L_0x0047
            r3.close()     // Catch:{ IOException -> 0x004d }
        L_0x0046:
            r3 = 0
        L_0x0047:
            throw r5
        L_0x0048:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0040
        L_0x004d:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0046
        L_0x0052:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0016
        L_0x0057:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x001c
        L_0x005c:
            r5 = move-exception
            r3 = r4
            goto L_0x003b
        L_0x005f:
            r5 = move-exception
            r1 = r2
            r3 = r4
            goto L_0x003b
        L_0x0063:
            r5 = move-exception
            r0 = r5
            r3 = r4
            goto L_0x0020
        L_0x0067:
            r5 = move-exception
            r0 = r5
            r1 = r2
            r3 = r4
            goto L_0x0020
        L_0x006c:
            r3 = r4
            goto L_0x001d
        L_0x006e:
            r1 = r2
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.by.utils.DiskTextCache.writeText(java.lang.String, java.io.File):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x001b  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0023  */
    /* JADX WARNING: Removed duplicated region for block: B:23:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void writeByFileOutputStream(java.lang.String r4, java.lang.String r5) throws java.io.IOException {
        /*
            r1 = 0
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0014 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x0014 }
            byte[] r3 = r5.getBytes()     // Catch:{ Exception -> 0x002b, all -> 0x0028 }
            r2.write(r3)     // Catch:{ Exception -> 0x002b, all -> 0x0028 }
            if (r2 == 0) goto L_0x002f
            r2.close()
            r1 = 0
        L_0x0013:
            return
        L_0x0014:
            r3 = move-exception
            r0 = r3
        L_0x0016:
            r0.printStackTrace()     // Catch:{ all -> 0x0020 }
            if (r1 == 0) goto L_0x0013
            r1.close()
            r1 = 0
            goto L_0x0013
        L_0x0020:
            r3 = move-exception
        L_0x0021:
            if (r1 == 0) goto L_0x0027
            r1.close()
            r1 = 0
        L_0x0027:
            throw r3
        L_0x0028:
            r3 = move-exception
            r1 = r2
            goto L_0x0021
        L_0x002b:
            r3 = move-exception
            r0 = r3
            r1 = r2
            goto L_0x0016
        L_0x002f:
            r1 = r2
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: com.by.utils.DiskTextCache.writeByFileOutputStream(java.lang.String, java.lang.String):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0025  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x002b  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:36:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void writeByOutputStreamWrite(java.lang.String r6, java.lang.String r7) throws java.io.IOException {
        /*
            r3 = 0
            r1 = 0
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x001e }
            r2.<init>(r6)     // Catch:{ Exception -> 0x001e }
            java.io.OutputStreamWriter r4 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x0045, all -> 0x003e }
            java.lang.String r5 = "UTF-8"
            r4.<init>(r2, r5)     // Catch:{ Exception -> 0x0045, all -> 0x003e }
            r4.write(r7)     // Catch:{ Exception -> 0x0049, all -> 0x0041 }
            if (r4 == 0) goto L_0x0050
            r4.close()
            r3 = 0
        L_0x0017:
            if (r2 == 0) goto L_0x004e
            r2.close()
            r1 = 0
        L_0x001d:
            return
        L_0x001e:
            r5 = move-exception
            r0 = r5
        L_0x0020:
            r0.printStackTrace()     // Catch:{ all -> 0x0030 }
            if (r3 == 0) goto L_0x0029
            r3.close()
            r3 = 0
        L_0x0029:
            if (r1 == 0) goto L_0x001d
            r1.close()
            r1 = 0
            goto L_0x001d
        L_0x0030:
            r5 = move-exception
        L_0x0031:
            if (r3 == 0) goto L_0x0037
            r3.close()
            r3 = 0
        L_0x0037:
            if (r1 == 0) goto L_0x003d
            r1.close()
            r1 = 0
        L_0x003d:
            throw r5
        L_0x003e:
            r5 = move-exception
            r1 = r2
            goto L_0x0031
        L_0x0041:
            r5 = move-exception
            r1 = r2
            r3 = r4
            goto L_0x0031
        L_0x0045:
            r5 = move-exception
            r0 = r5
            r1 = r2
            goto L_0x0020
        L_0x0049:
            r5 = move-exception
            r0 = r5
            r1 = r2
            r3 = r4
            goto L_0x0020
        L_0x004e:
            r1 = r2
            goto L_0x001d
        L_0x0050:
            r3 = r4
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.by.utils.DiskTextCache.writeByOutputStreamWrite(java.lang.String, java.lang.String):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.WeakHashMap.put(java.lang.Object, java.lang.Object):V}
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.by.utils.DiskTextCache.put(java.lang.String, java.lang.String):java.lang.String
      ClspMth{java.util.WeakHashMap.put(java.lang.Object, java.lang.Object):V} */
    private void downfiles(String key, String value, File fileitem) {
        Log.d(BaoyiApplication.TAG, "网络图片" + key);
        Log.d(BaoyiApplication.TAG, "缓存文件到磁盘");
        try {
            URLConnection conn = new URL(key).openConnection();
            conn.setDoInput(true);
            int length = conn.getContentLength();
            InputStream is = conn.getInputStream();
            System.out.println("is============" + is);
            if (length != -1) {
                FileOutputStream output = new FileOutputStream(fileitem);
                byte[] buffer = new byte[6144];
                while (true) {
                    int temp1 = is.read(buffer);
                    if (temp1 == -1) {
                        output.flush();
                        return;
                    }
                    output.write(buffer, 0, temp1);
                }
            }
        } catch (MalformedURLException e) {
            super.put((Object) key, (Object) value);
        } catch (IOException e2) {
            super.put((Object) key, (Object) value);
        }
    }
}
