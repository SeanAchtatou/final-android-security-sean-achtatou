package com.by.utils;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;
import com.by.doamin.CheckWork;
import com.by.pacbell.BaoyiApplication;
import com.by.pacbell.R;
import java.io.File;
import java.util.Formatter;
import java.util.Locale;

public class MusicUtils {
    private static StringBuilder sFormatBuilder = new StringBuilder();
    private static Formatter sFormatter = new Formatter(sFormatBuilder, Locale.getDefault());
    private static final Object[] sTimeArgs = new Object[5];

    public static String makeTimeString(Context context, long secs) {
        int i;
        if (secs < 3600) {
            i = R.string.durationformatshort;
        } else {
            i = R.string.durationformatlong;
        }
        String durationformat = context.getString(i);
        sFormatBuilder.setLength(0);
        Object[] timeArgs = sTimeArgs;
        timeArgs[0] = Long.valueOf(secs / 3600);
        timeArgs[1] = Long.valueOf(secs / 60);
        timeArgs[2] = Long.valueOf((secs / 60) % 60);
        timeArgs[3] = Long.valueOf(secs);
        timeArgs[4] = Long.valueOf(secs % 60);
        return sFormatter.format(durationformat, timeArgs).toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    public static void setMyRingtone1(Context context, File file, CheckWork check) {
        ContentValues values = new ContentValues();
        values.put("_data", file.getAbsolutePath());
        values.put("title", file.getName());
        values.put("mime_type", "audio/mp3");
        values.put("artist", "宝亿铃声");
        values.put("is_ringtone", Boolean.valueOf(check.isIsringtones()));
        values.put("is_notification", Boolean.valueOf(check.isIsnotifications()));
        values.put("is_alarm", Boolean.valueOf(check.isIsalarms()));
        values.put("is_music", (Boolean) false);
        Uri newUri = context.getContentResolver().insert(MediaStore.Audio.Media.getContentUriForPath(file.getAbsolutePath()), values);
        if (check.isIsalarms()) {
            RingtoneManager.setActualDefaultRingtoneUri(context, 4, newUri);
        }
        if (check.isIsnotifications()) {
            RingtoneManager.setActualDefaultRingtoneUri(context, 2, newUri);
        }
        if (check.isIsringtones()) {
            RingtoneManager.setActualDefaultRingtoneUri(context, 1, newUri);
        }
        Toast.makeText(context, "设置铃声成功", 0).show();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    public static void setMyRingtone(Context context, File file) {
        ContentResolver resolver = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put("_data", file.getAbsolutePath());
        values.put("title", file.getName());
        values.put("_size", Long.valueOf(file.length()));
        values.put("mime_type", "audio/mp3");
        values.put("artist", "宝亿铃声");
        values.put("is_ringtone", (Boolean) true);
        values.put("is_notification", (Boolean) false);
        values.put("is_alarm", (Boolean) false);
        values.put("is_music", (Boolean) false);
        Uri uri = MediaStore.Audio.Media.getContentUriForPath(file.getAbsolutePath());
        String where = "title='" + file.getName() + "'";
        String[] cols = {"_id", "_data", "title"};
        context.getContentResolver().update(uri, values, where, cols);
        Cursor cursor = query(context, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, cols, where, null, null);
        if (cursor != null) {
            try {
                if (cursor.getCount() == 1) {
                    cursor.moveToFirst();
                    Settings.System.putString(resolver, "ringtone", uri.toString());
                    Toast.makeText(context, "设置成功", 0).show();
                }
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        }
        if (cursor != null) {
            cursor.close();
        }
    }

    public static void setRingtone(Context context, long id) {
        ContentResolver resolver = context.getContentResolver();
        Uri ringUri = ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, id);
        try {
            ContentValues values = new ContentValues(2);
            values.put("is_ringtone", "1");
            values.put("is_alarm", "1");
            resolver.update(ringUri, values, null, null);
            Context context2 = context;
            Cursor cursor = query(context2, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, new String[]{"_id", "_data", "title"}, "_id=" + id, null, null);
            if (cursor != null) {
                try {
                    if (cursor.getCount() == 1) {
                        cursor.moveToFirst();
                        Settings.System.putString(resolver, "ringtone", ringUri.toString());
                        Toast.makeText(context, "", 0).show();
                    }
                } catch (Throwable th) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (cursor != null) {
                cursor.close();
            }
        } catch (UnsupportedOperationException e) {
            Log.e(BaoyiApplication.TAG, "不能设置铃声id" + id);
        }
    }

    public static Cursor query(Context context, Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder, int limit) {
        try {
            ContentResolver resolver = context.getContentResolver();
            if (resolver == null) {
                return null;
            }
            if (limit > 0) {
                uri = uri.buildUpon().appendQueryParameter("limit", new StringBuilder().append(limit).toString()).build();
            }
            return resolver.query(uri, projection, selection, selectionArgs, sortOrder);
        } catch (UnsupportedOperationException e) {
            return null;
        }
    }

    public static Cursor query(Context context, Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        return query(context, uri, projection, selection, selectionArgs, sortOrder, 0);
    }
}
