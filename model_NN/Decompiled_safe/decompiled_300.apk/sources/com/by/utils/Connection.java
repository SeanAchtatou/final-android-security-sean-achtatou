package com.by.utils;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.Map;

public interface Connection {

    public interface Base<T extends Base> {
        T cookie(String str, String str2);

        String cookie(String str);

        Map<String, String> cookies();

        boolean hasCookie(String str);

        boolean hasHeader(String str);

        T header(String str, String str2);

        String header(String str);

        Map<String, String> headers();

        T method(Method method);

        Method method();

        T removeCookie(String str);

        T removeHeader(String str);

        T url(URL url);

        URL url();
    }

    public interface KeyVal {
        KeyVal key(String str);

        String key();

        KeyVal value(String str);

        String value();
    }

    public enum Method {
        GET,
        POST
    }

    public interface Request extends Base<Request> {
        Request data(KeyVal keyVal);

        Collection<KeyVal> data();

        Request followRedirects(boolean z);

        boolean followRedirects();

        int timeout();

        Request timeout(int i);
    }

    public interface Response extends Base<Response> {
        String body();

        byte[] bodyAsBytes();

        String charset();

        String contentType();

        int statusCode();

        String statusMessage();
    }

    Connection cookie(String str, String str2);

    Connection data(String str, String str2);

    Connection data(Map<String, String> map);

    Connection data(String... strArr);

    Response execute() throws IOException;

    Connection followRedirects(boolean z);

    Connection header(String str, String str2);

    Connection method(Method method);

    Connection referrer(String str);

    Request request();

    Connection request(Request request);

    Response response();

    Connection response(Response response);

    Connection timeout(int i);

    Connection url(String str);

    Connection url(URL url);

    Connection userAgent(String str);
}
