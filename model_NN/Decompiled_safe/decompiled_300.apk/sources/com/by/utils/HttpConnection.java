package com.by.utils;

import com.by.utils.Connection;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

public class HttpConnection implements Connection {
    private Connection.Request req = new Request(null);
    private Connection.Response res = new Response();

    public static Connection connect(String url) {
        Connection con = new HttpConnection();
        con.url(url);
        return con;
    }

    public static Connection connect(URL url) {
        Connection con = new HttpConnection();
        con.url(url);
        return con;
    }

    private HttpConnection() {
    }

    public Connection url(URL url) {
        this.req.url(url);
        return this;
    }

    public Connection url(String url) {
        Validate.notEmpty(url, "Must supply a valid URL");
        try {
            this.req.url(new URL(url));
            return this;
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException("Malformed URL: " + url, e);
        }
    }

    public Connection userAgent(String userAgent) {
        Validate.notNull(userAgent, "User agent must not be null");
        this.req.header("User-Agent", userAgent);
        return this;
    }

    public Connection timeout(int millis) {
        this.req.timeout(millis);
        return this;
    }

    public Connection followRedirects(boolean followRedirects) {
        this.req.followRedirects(followRedirects);
        return this;
    }

    public Connection referrer(String referrer) {
        Validate.notNull(referrer, "Referrer must not be null");
        this.req.header("Referer", referrer);
        return this;
    }

    public Connection method(Connection.Method method) {
        this.req.method(method);
        return this;
    }

    public Connection data(String key, String value) {
        this.req.data(KeyVal.create(key, value));
        return this;
    }

    public Connection data(Map<String, String> data) {
        Validate.notNull(data, "Data map must not be null");
        for (Map.Entry<String, String> entry : data.entrySet()) {
            this.req.data(KeyVal.create((String) entry.getKey(), (String) entry.getValue()));
        }
        return this;
    }

    public Connection data(String... keyvals) {
        Validate.notNull(keyvals, "Data key value pairs must not be null");
        Validate.isTrue(keyvals.length % 2 == 0, "Must supply an even number of key value pairs");
        for (int i = 0; i < keyvals.length; i += 2) {
            String key = keyvals[i];
            String value = keyvals[i + 1];
            Validate.notEmpty(key, "Data key must not be empty");
            Validate.notNull(value, "Data value must not be null");
            this.req.data(KeyVal.create(key, value));
        }
        return this;
    }

    public Connection header(String name, String value) {
        this.req.header(name, value);
        return this;
    }

    public Connection cookie(String name, String value) {
        this.req.cookie(name, value);
        return this;
    }

    public Connection.Response execute() throws IOException {
        this.res = Response.execute(this.req);
        return this.res;
    }

    public Connection.Request request() {
        return this.req;
    }

    public Connection request(Connection.Request request) {
        this.req = request;
        return this;
    }

    public Connection.Response response() {
        return this.res;
    }

    public Connection response(Connection.Response response) {
        this.res = response;
        return this;
    }

    private static abstract class Base<T extends Connection.Base> implements Connection.Base<T> {
        Map<String, String> cookies;
        Map<String, String> headers;
        Connection.Method method;
        URL url;

        private Base() {
            this.headers = new LinkedHashMap();
            this.cookies = new LinkedHashMap();
        }

        /* synthetic */ Base(Base base) {
            this();
        }

        public URL url() {
            return this.url;
        }

        public T url(URL url2) {
            Validate.notNull(url2, "URL must not be null");
            this.url = url2;
            return this;
        }

        public Connection.Method method() {
            return this.method;
        }

        public T method(Connection.Method method2) {
            Validate.notNull(method2, "Method must not be null");
            this.method = method2;
            return this;
        }

        public String header(String name) {
            Validate.notNull(name, "Header name must not be null");
            return getHeaderCaseInsensitive(name);
        }

        public T header(String name, String value) {
            Validate.notEmpty(name, "Header name must not be empty");
            Validate.notNull(value, "Header value must not be null");
            removeHeader(name);
            this.headers.put(name, value);
            return this;
        }

        public boolean hasHeader(String name) {
            Validate.notEmpty(name, "Header name must not be empty");
            return getHeaderCaseInsensitive(name) != null;
        }

        public T removeHeader(String name) {
            Validate.notEmpty(name, "Header name must not be empty");
            Map.Entry<String, String> entry = scanHeaders(name);
            if (entry != null) {
                this.headers.remove(entry.getKey());
            }
            return this;
        }

        public Map<String, String> headers() {
            return this.headers;
        }

        private String getHeaderCaseInsensitive(String name) {
            Map.Entry<String, String> entry;
            Validate.notNull(name, "Header name must not be null");
            String value = this.headers.get(name);
            if (value == null) {
                value = this.headers.get(name.toLowerCase());
            }
            if (value != null || (entry = scanHeaders(name)) == null) {
                return value;
            }
            return entry.getValue();
        }

        private Map.Entry<String, String> scanHeaders(String name) {
            String lc = name.toLowerCase();
            for (Map.Entry<String, String> entry : this.headers.entrySet()) {
                if (((String) entry.getKey()).toLowerCase().equals(lc)) {
                    return entry;
                }
            }
            return null;
        }

        public String cookie(String name) {
            Validate.notNull(name, "Cookie name must not be null");
            return this.cookies.get(name);
        }

        public T cookie(String name, String value) {
            Validate.notEmpty(name, "Cookie name must not be empty");
            Validate.notNull(value, "Cookie value must not be null");
            this.cookies.put(name, value);
            return this;
        }

        public boolean hasCookie(String name) {
            Validate.notEmpty("Cookie name must not be empty");
            return this.cookies.containsKey(name);
        }

        public T removeCookie(String name) {
            Validate.notEmpty("Cookie name must not be empty");
            this.cookies.remove(name);
            return this;
        }

        public Map<String, String> cookies() {
            return this.cookies;
        }
    }

    public static class Request extends Base<Connection.Request> implements Connection.Request {
        private Collection<Connection.KeyVal> data;
        private boolean followRedirects;
        private int timeoutMilliseconds;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.by.utils.HttpConnection.Base.cookie(java.lang.String, java.lang.String):T
         arg types: [java.lang.String, java.lang.String]
         candidates:
          com.by.utils.HttpConnection.Base.cookie(java.lang.String, java.lang.String):T
          com.by.utils.Connection.Base.cookie(java.lang.String, java.lang.String):T
          com.by.utils.Connection.Base.cookie(java.lang.String, java.lang.String):T
          com.by.utils.HttpConnection.Base.cookie(java.lang.String, java.lang.String):T */
        public /* bridge */ /* synthetic */ Connection.Request cookie(String str, String str2) {
            return super.cookie(str, str2);
        }

        public /* bridge */ /* synthetic */ String cookie(String str) {
            return super.cookie(str);
        }

        public /* bridge */ /* synthetic */ Map cookies() {
            return super.cookies();
        }

        public /* bridge */ /* synthetic */ boolean hasCookie(String str) {
            return super.hasCookie(str);
        }

        public /* bridge */ /* synthetic */ boolean hasHeader(String str) {
            return super.hasHeader(str);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.by.utils.HttpConnection.Base.header(java.lang.String, java.lang.String):T
         arg types: [java.lang.String, java.lang.String]
         candidates:
          com.by.utils.HttpConnection.Base.header(java.lang.String, java.lang.String):T
          com.by.utils.Connection.Base.header(java.lang.String, java.lang.String):T
          com.by.utils.Connection.Base.header(java.lang.String, java.lang.String):T
          com.by.utils.HttpConnection.Base.header(java.lang.String, java.lang.String):T */
        public /* bridge */ /* synthetic */ Connection.Request header(String str, String str2) {
            return super.header(str, str2);
        }

        public /* bridge */ /* synthetic */ String header(String str) {
            return super.header(str);
        }

        public /* bridge */ /* synthetic */ Map headers() {
            return super.headers();
        }

        public /* bridge */ /* synthetic */ Connection.Method method() {
            return super.method();
        }

        public /* bridge */ /* synthetic */ Connection.Request method(Connection.Method method) {
            return super.method(method);
        }

        public /* bridge */ /* synthetic */ Connection.Request removeCookie(String str) {
            return super.removeCookie(str);
        }

        public /* bridge */ /* synthetic */ Connection.Request removeHeader(String str) {
            return super.removeHeader(str);
        }

        public /* bridge */ /* synthetic */ Connection.Request url(URL url) {
            return super.url(url);
        }

        public /* bridge */ /* synthetic */ URL url() {
            return super.url();
        }

        private Request() {
            super(null);
            this.timeoutMilliseconds = 3000;
            this.followRedirects = true;
            this.data = new ArrayList();
            this.method = Connection.Method.GET;
            this.headers.put("Accept-Encoding", "gzip");
        }

        /* synthetic */ Request(Request request) {
            this();
        }

        public int timeout() {
            return this.timeoutMilliseconds;
        }

        public Request timeout(int millis) {
            Validate.isTrue(millis >= 0, "Timeout milliseconds must be 0 (infinite) or greater");
            this.timeoutMilliseconds = millis;
            return this;
        }

        public boolean followRedirects() {
            return this.followRedirects;
        }

        public Connection.Request followRedirects(boolean followRedirects2) {
            this.followRedirects = followRedirects2;
            return this;
        }

        public Request data(Connection.KeyVal keyval) {
            Validate.notNull(keyval, "Key val must not be null");
            this.data.add(keyval);
            return this;
        }

        public Collection<Connection.KeyVal> data() {
            return this.data;
        }
    }

    public static class Response extends Base<Connection.Response> implements Connection.Response {
        private static final int MAX_REDIRECTS = 20;
        private ByteBuffer byteData;
        private String charset;
        private String contentType;
        private boolean executed = false;
        private int numRedirects = 0;
        private int statusCode;
        private String statusMessage;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.by.utils.HttpConnection.Base.cookie(java.lang.String, java.lang.String):T
         arg types: [java.lang.String, java.lang.String]
         candidates:
          com.by.utils.HttpConnection.Base.cookie(java.lang.String, java.lang.String):T
          com.by.utils.Connection.Base.cookie(java.lang.String, java.lang.String):T
          com.by.utils.Connection.Base.cookie(java.lang.String, java.lang.String):T
          com.by.utils.HttpConnection.Base.cookie(java.lang.String, java.lang.String):T */
        public /* bridge */ /* synthetic */ Connection.Response cookie(String str, String str2) {
            return super.cookie(str, str2);
        }

        public /* bridge */ /* synthetic */ String cookie(String str) {
            return super.cookie(str);
        }

        public /* bridge */ /* synthetic */ Map cookies() {
            return super.cookies();
        }

        public /* bridge */ /* synthetic */ boolean hasCookie(String str) {
            return super.hasCookie(str);
        }

        public /* bridge */ /* synthetic */ boolean hasHeader(String str) {
            return super.hasHeader(str);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.by.utils.HttpConnection.Base.header(java.lang.String, java.lang.String):T
         arg types: [java.lang.String, java.lang.String]
         candidates:
          com.by.utils.HttpConnection.Base.header(java.lang.String, java.lang.String):T
          com.by.utils.Connection.Base.header(java.lang.String, java.lang.String):T
          com.by.utils.Connection.Base.header(java.lang.String, java.lang.String):T
          com.by.utils.HttpConnection.Base.header(java.lang.String, java.lang.String):T */
        public /* bridge */ /* synthetic */ Connection.Response header(String str, String str2) {
            return super.header(str, str2);
        }

        public /* bridge */ /* synthetic */ String header(String str) {
            return super.header(str);
        }

        public /* bridge */ /* synthetic */ Map headers() {
            return super.headers();
        }

        public /* bridge */ /* synthetic */ Connection.Method method() {
            return super.method();
        }

        public /* bridge */ /* synthetic */ Connection.Response method(Connection.Method method) {
            return super.method(method);
        }

        public /* bridge */ /* synthetic */ Connection.Response removeCookie(String str) {
            return super.removeCookie(str);
        }

        public /* bridge */ /* synthetic */ Connection.Response removeHeader(String str) {
            return super.removeHeader(str);
        }

        public /* bridge */ /* synthetic */ Connection.Response url(URL url) {
            return super.url(url);
        }

        public /* bridge */ /* synthetic */ URL url() {
            return super.url();
        }

        Response() {
            super(null);
        }

        private Response(Response previousResponse) throws IOException {
            super(null);
            if (previousResponse != null) {
                this.numRedirects = previousResponse.numRedirects + 1;
                if (this.numRedirects >= 20) {
                    throw new IOException(String.format("Too many redirects occurred trying to load URL %s", previousResponse.url()));
                }
            }
        }

        static Response execute(Connection.Request req) throws IOException {
            return execute(req, null);
        }

        static Response execute(Connection.Request req, Response previousResponse) throws IOException {
            boolean z;
            InputStream inStream;
            Validate.notNull(req, "Request must not be null");
            String protocol = req.url().getProtocol();
            if (protocol.equals("http") || protocol.equals("https")) {
                z = true;
            } else {
                z = false;
            }
            Validate.isTrue(z, "Only http & https protocols supported");
            if (req.method() == Connection.Method.GET && req.data().size() > 0) {
                serialiseRequestUrl(req);
            }
            HttpURLConnection conn = createConnection(req);
            conn.connect();
            if (req.method() == Connection.Method.POST) {
                writePost(req.data(), conn.getOutputStream());
            }
            int status = conn.getResponseCode();
            boolean needsRedirect = false;
            if (status != 200) {
                if (status == 302 || status == 301 || status == 303) {
                    needsRedirect = true;
                } else {
                    throw new IOException(String.valueOf(status) + " error loading URL " + req.url().toString());
                }
            }
            Response res = new Response(previousResponse);
            res.setupFromConnection(conn, previousResponse);
            if (!needsRedirect || !req.followRedirects()) {
                InputStream inStream2 = null;
                try {
                    if (!res.hasHeader("Content-Encoding") || !res.header("Content-Encoding").equalsIgnoreCase("gzip")) {
                        inStream = new BufferedInputStream(conn.getInputStream());
                    } else {
                        inStream = new BufferedInputStream(new GZIPInputStream(conn.getInputStream()));
                    }
                    res.byteData = DataUtil.readToByteBuffer(inStream2);
                    res.charset = DataUtil.getCharsetFromContentType(res.contentType);
                    res.executed = true;
                    return res;
                } finally {
                    if (inStream2 != null) {
                        inStream2.close();
                    }
                }
            } else {
                req.url(new URL(req.url(), res.header("Location")));
                for (Map.Entry<String, String> cookie : res.cookies.entrySet()) {
                    req.cookie((String) cookie.getKey(), (String) cookie.getValue());
                }
                return execute(req, res);
            }
        }

        public int statusCode() {
            return this.statusCode;
        }

        public String statusMessage() {
            return this.statusMessage;
        }

        public String charset() {
            return this.charset;
        }

        public String contentType() {
            return this.contentType;
        }

        public String body() {
            String body;
            Validate.isTrue(this.executed, "Request must be executed (with .execute(), .get(), or .post() before getting response body");
            if (this.charset == null) {
                body = Charset.forName(DataUtil.defaultCharset).decode(this.byteData).toString();
            } else {
                body = Charset.forName(this.charset).decode(this.byteData).toString();
            }
            this.byteData.rewind();
            return body;
        }

        public byte[] bodyAsBytes() {
            Validate.isTrue(this.executed, "Request must be executed (with .execute(), .get(), or .post() before getting response body");
            return this.byteData.array();
        }

        private static HttpURLConnection createConnection(Connection.Request req) throws IOException {
            HttpURLConnection conn = (HttpURLConnection) req.url().openConnection();
            conn.setRequestMethod(req.method().name());
            conn.setInstanceFollowRedirects(false);
            conn.setConnectTimeout(req.timeout());
            conn.setReadTimeout(req.timeout());
            if (req.method() == Connection.Method.POST) {
                conn.setDoOutput(true);
            }
            if (req.cookies().size() > 0) {
                conn.addRequestProperty("Cookie", getRequestCookieString(req));
            }
            for (Map.Entry<String, String> header : req.headers().entrySet()) {
                conn.addRequestProperty((String) header.getKey(), (String) header.getValue());
            }
            return conn;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.by.utils.HttpConnection.Response.cookie(java.lang.String, java.lang.String):com.by.utils.Connection$Response
         arg types: [java.lang.String, java.lang.String]
         candidates:
          com.by.utils.HttpConnection.Response.cookie(java.lang.String, java.lang.String):com.by.utils.Connection$Response
          com.by.utils.HttpConnection.Response.cookie(java.lang.String, java.lang.String):com.by.utils.Connection$Response */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.by.utils.HttpConnection.Response.header(java.lang.String, java.lang.String):com.by.utils.Connection$Response
         arg types: [java.lang.String, java.lang.String]
         candidates:
          com.by.utils.HttpConnection.Response.header(java.lang.String, java.lang.String):com.by.utils.Connection$Response
          com.by.utils.HttpConnection.Response.header(java.lang.String, java.lang.String):com.by.utils.Connection$Response */
        private void setupFromConnection(HttpURLConnection conn, Connection.Response previousResponse) throws IOException {
            this.method = Connection.Method.valueOf(conn.getRequestMethod());
            this.url = conn.getURL();
            this.statusCode = conn.getResponseCode();
            this.statusMessage = conn.getResponseMessage();
            this.contentType = conn.getContentType();
            for (Map.Entry<String, List<String>> entry : conn.getHeaderFields().entrySet()) {
                String name = (String) entry.getKey();
                if (name != null) {
                    List<String> values = entry.getValue();
                    if (name.equalsIgnoreCase("Set-Cookie")) {
                        for (String value : values) {
                            TokenQueue cd = new TokenQueue(value);
                            cookie(cd.chompTo("=").trim(), cd.consumeTo(";").trim());
                        }
                    } else if (!values.isEmpty()) {
                        header(name, (String) values.get(0));
                    }
                }
            }
            if (previousResponse != null) {
                for (Map.Entry<String, String> prevCookie : previousResponse.cookies().entrySet()) {
                    if (!hasCookie((String) prevCookie.getKey())) {
                        cookie((String) prevCookie.getKey(), (String) prevCookie.getValue());
                    }
                }
            }
        }

        private static void writePost(Collection<Connection.KeyVal> data, OutputStream outputStream) throws IOException {
            OutputStreamWriter w = new OutputStreamWriter(outputStream, DataUtil.defaultCharset);
            boolean first = true;
            for (Connection.KeyVal keyVal : data) {
                if (!first) {
                    w.append('&');
                } else {
                    first = false;
                }
                w.write(URLEncoder.encode(keyVal.key(), DataUtil.defaultCharset));
                w.write(61);
                w.write(URLEncoder.encode(keyVal.value(), DataUtil.defaultCharset));
            }
            w.close();
        }

        private static String getRequestCookieString(Connection.Request req) {
            StringBuilder sb = new StringBuilder();
            boolean first = true;
            for (Map.Entry<String, String> cookie : req.cookies().entrySet()) {
                if (!first) {
                    sb.append("; ");
                } else {
                    first = false;
                }
                sb.append((String) cookie.getKey()).append('=').append((String) cookie.getValue());
            }
            return sb.toString();
        }

        private static void serialiseRequestUrl(Connection.Request req) throws IOException {
            URL in = req.url();
            StringBuilder url = new StringBuilder();
            boolean first = true;
            url.append(in.getProtocol()).append("://").append(in.getAuthority()).append(in.getPath()).append("?");
            if (in.getQuery() != null) {
                url.append(in.getQuery());
                first = false;
            }
            for (Connection.KeyVal keyVal : req.data()) {
                if (!first) {
                    url.append('&');
                } else {
                    first = false;
                }
                url.append(URLEncoder.encode(keyVal.key(), DataUtil.defaultCharset)).append('=').append(URLEncoder.encode(keyVal.value(), DataUtil.defaultCharset));
            }
            req.url(new URL(url.toString()));
            req.data().clear();
        }
    }

    public static class KeyVal implements Connection.KeyVal {
        private String key;
        private String value;

        public static KeyVal create(String key2, String value2) {
            Validate.notEmpty(key2, "Data key must not be empty");
            Validate.notNull(value2, "Data value must not be null");
            return new KeyVal(key2, value2);
        }

        private KeyVal(String key2, String value2) {
            this.key = key2;
            this.value = value2;
        }

        public KeyVal key(String key2) {
            Validate.notEmpty(key2, "Data key must not be empty");
            this.key = key2;
            return this;
        }

        public String key() {
            return this.key;
        }

        public KeyVal value(String value2) {
            Validate.notNull(value2, "Data value must not be null");
            this.value = value2;
            return this;
        }

        public String value() {
            return this.value;
        }

        public String toString() {
            return String.valueOf(this.key) + "=" + this.value;
        }
    }
}
