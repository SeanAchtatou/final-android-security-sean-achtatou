package com.by.utils;

import android.os.Environment;
import android.os.StatFs;
import com.by.bean.RingSecList;
import com.by.bean.UploadPac;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {
    private String SDPATH = (String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + File.separator);

    public String getSDPATH() {
        return this.SDPATH;
    }

    public File creatSDFile(String fileName, String dir) throws IOException {
        File file = new File(String.valueOf(this.SDPATH) + dir + File.separator + fileName);
        file.createNewFile();
        return file;
    }

    public long getFileSize(String fileName, String path) {
        return new File(String.valueOf(this.SDPATH) + path + File.separator + fileName).length();
    }

    public File creatSDDir(String dirName) {
        File dir = new File(String.valueOf(this.SDPATH) + dirName + File.separator);
        dir.mkdir();
        return dir;
    }

    public boolean isFileExist(String fileName, String path) {
        return new File(String.valueOf(this.SDPATH) + path + fileName).exists();
    }

    public File write2SDFromInput(String path, String fileName, InputStream input) {
        Exception e;
        File file = null;
        OutputStream output = null;
        try {
            creatSDDir(path);
            file = creatSDFile(fileName, path);
            OutputStream output2 = new FileOutputStream(file);
            try {
                byte[] buffer = new byte[6144];
                while (true) {
                    int temp = input.read(buffer);
                    if (temp == -1) {
                        break;
                    }
                    output2.write(buffer, 0, temp);
                }
                output2.flush();
                try {
                    output2.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            } catch (Exception e3) {
                e = e3;
                output = output2;
            } catch (Throwable th) {
                th = th;
                output = output2;
                try {
                    output.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
                throw th;
            }
        } catch (Exception e5) {
            e = e5;
            try {
                e.printStackTrace();
                try {
                    output.close();
                } catch (IOException e6) {
                    e6.printStackTrace();
                }
                return file;
            } catch (Throwable th2) {
                th = th2;
                output.close();
                throw th;
            }
        }
        return file;
    }

    public List<RingSecList> getMp3Files(String path) {
        List<RingSecList> mp3Infos = new ArrayList<>();
        File[] files = new File(String.valueOf(this.SDPATH) + File.separator + path).listFiles();
        for (int i = 0; i < files.length; i++) {
            RingSecList ringlist = new RingSecList();
            if (files[i].getName().endsWith("mp3")) {
                ringlist.setRingsecmp3Url(files[i].getName());
                ringlist.setRingsecName(files[i].getName().replace(".mp3", ""));
                ringlist.setRingsecPre(-100);
                ringlist.setRingsecNext(-100);
                mp3Infos.add(ringlist);
            }
        }
        return mp3Infos;
    }

    public List<UploadPac> getSdFiles(String root) {
        List<UploadPac> mp3Infos_Name = new ArrayList<>();
        File[] files = new File(String.valueOf(this.SDPATH) + root).listFiles();
        System.out.println("root======" + root);
        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                if (files[i].isFile()) {
                    UploadPac upload = new UploadPac();
                    upload.setFile_name(files[i].getName());
                    mp3Infos_Name.add(upload);
                } else {
                    UploadPac upload1 = new UploadPac();
                    upload1.setFile_name(files[i].getName());
                    mp3Infos_Name.add(upload1);
                    getSdFiles(files[i].getName());
                }
            }
        }
        return mp3Infos_Name;
    }

    public long readSDCard() {
        if (!"mounted".equals(Environment.getExternalStorageState())) {
            return 0;
        }
        StatFs sf = new StatFs(Environment.getExternalStorageDirectory().getPath());
        long blockSize = (long) sf.getBlockSize();
        long blockCount = (long) sf.getBlockCount();
        return ((long) sf.getAvailableBlocks()) * blockSize;
    }
}
