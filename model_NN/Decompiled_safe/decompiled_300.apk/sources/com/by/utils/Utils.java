package com.by.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.os.StatFs;
import android.util.DisplayMetrics;
import android.util.Log;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Enumeration;

public class Utils {
    public static boolean isWifi(Context mContext) {
        NetworkInfo activeNetInfo = ((ConnectivityManager) mContext.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetInfo == null || activeNetInfo.getType() != 1) {
            return false;
        }
        return true;
    }

    public static boolean getHeight(Context mContext) {
        new DisplayMetrics();
        DisplayMetrics dm = mContext.getApplicationContext().getResources().getDisplayMetrics();
        int i = dm.widthPixels;
        int i2 = dm.heightPixels;
        return false;
    }

    public void i(Bitmap bitmap, Context mContext) {
        new DisplayMetrics();
        int screenHeight = mContext.getApplicationContext().getResources().getDisplayMetrics().heightPixels;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = false;
        int be = (int) (((float) options.outHeight) / ((float) screenHeight));
        if (be <= 0) {
            be = 1;
        }
        options.inSampleSize = be;
        Bitmap bitmap2 = BitmapFactory.decodeStream(new ByteArrayInputStream(null), new Rect(), options);
        int w = bitmap2.getWidth();
        System.out.println(String.valueOf(w) + "   " + bitmap2.getHeight());
    }

    public static String getLocalIpAddress() {
        try {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            while (en.hasMoreElements()) {
                Enumeration<InetAddress> enumIpAddr = en.nextElement().getInetAddresses();
                while (true) {
                    if (enumIpAddr.hasMoreElements()) {
                        InetAddress inetAddress = enumIpAddr.nextElement();
                        if (!inetAddress.isLoopbackAddress()) {
                            return inetAddress.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (SocketException e) {
            Log.e("WifiPreference IpAddress", e.toString());
        }
        return null;
    }

    public static String getMD5Str(String str) {
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(str.getBytes(DataUtil.defaultCharset));
        } catch (NoSuchAlgorithmException e) {
            System.out.println("NoSuchAlgorithmException caught!");
            System.exit(-1);
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        byte[] byteArray = messageDigest.digest();
        StringBuffer md5StrBuff = new StringBuffer();
        for (int i = 0; i < byteArray.length; i++) {
            if (Integer.toHexString(byteArray[i] & 255).length() == 1) {
                md5StrBuff.append("0").append(Integer.toHexString(byteArray[i] & 255));
            } else {
                md5StrBuff.append(Integer.toHexString(byteArray[i] & 255));
            }
        }
        return md5StrBuff.substring(8, 24).toString().toUpperCase();
    }

    public static void readSDCard() {
        if ("mounted".equals(Environment.getExternalStorageState())) {
            StatFs sf = new StatFs(Environment.getExternalStorageDirectory().getPath());
            long blockSize = (long) sf.getBlockSize();
            long blockCount = (long) sf.getBlockCount();
            long availCount = (long) sf.getAvailableBlocks();
            Log.d("", "block大小:" + blockSize + ",block数目:" + blockCount + ",总大小:" + ((blockSize * blockCount) / 1024) + "KB");
            Log.d("", "可用的block数目：:" + availCount + ",剩余空间:" + ((availCount * blockSize) / 1024) + "KB");
        }
    }

    /* JADX INFO: Multiple debug info for r0v1 java.io.File: [D('size' long), D('sdcardDir' java.io.File)] */
    /* JADX INFO: Multiple debug info for r0v8 long: [D('size' long), D('availCount' long)] */
    public static long readSDCardMB() {
        if (!"mounted".equals(Environment.getExternalStorageState())) {
            return 0;
        }
        StatFs sf = new StatFs(Environment.getExternalStorageDirectory().getPath());
        long blockSize = (long) sf.getBlockSize();
        long blockCount = (long) sf.getBlockCount();
        long availCount = (long) sf.getAvailableBlocks();
        Log.d("", "block大小:" + blockSize + ",block数目:" + blockCount + ",总大小:" + ((blockCount * blockSize) / 1024) + "KB");
        Log.d("", "可用的block数目：:" + availCount + ",剩余空间:" + ((availCount * blockSize) / 1024) + "KB");
        return (availCount * blockSize) / 1048576;
    }

    public static void readSystem() {
        StatFs sf = new StatFs(Environment.getRootDirectory().getPath());
        long blockSize = (long) sf.getBlockSize();
        long blockCount = (long) sf.getBlockCount();
        long availCount = (long) sf.getAvailableBlocks();
        Log.d("", "block大小:" + blockSize + ",block数目:" + blockCount + ",总大小:" + ((blockSize * blockCount) / 1024) + "KB");
        Log.d("", "可用的block数目：:" + availCount + ",可用大小:" + ((availCount * blockSize) / 1024) + "KB");
    }
}
