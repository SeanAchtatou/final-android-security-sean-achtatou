package com.by.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import com.by.constant.Constant;
import com.by.pacbell.BaoyiApplication;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.WeakHashMap;

public class DiskMp3Cache extends WeakHashMap<String, Bitmap> {
    public Bitmap get(Object key) {
        File fileitem = new File(String.valueOf(Constant.baoyiringmp3) + Utils.getMD5Str(key.toString()) + ".mp3");
        Bitmap image = null;
        if (!fileitem.exists()) {
            Log.d(BaoyiApplication.TAG, "缓存中没有mp3文件");
            return (Bitmap) super.get(key);
        }
        try {
            image = BitmapFactory.decodeStream(new FileInputStream(fileitem));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (image == null) {
            return (Bitmap) super.get(key);
        }
        return image;
    }
}
