package com.by.server;

import com.by.bean.Ring;
import com.by.constant.Constant;
import com.by.pacbell.BaoyiApplication;
import com.by.utils.Connection;
import com.by.utils.HttpConnection;
import com.by.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ServerRing {
    private static Map<String, String> caches = new HashMap();

    public static List<Ring> findRingAll() {
        String url = String.valueOf(Constant.server) + "Servlet_FindRingAll";
        String key = Utils.getMD5Str(url);
        String messages = caches.get(key);
        if (messages == null) {
            try {
                Connection conn = HttpConnection.connect(url);
                conn.method(Connection.Method.GET);
                String messages2 = conn.execute().body();
                conn.timeout(10000);
                caches.put(key, messages2);
                BaoyiApplication.getInstance().getDiskTextCache().put(key, messages2);
                messages = caches.get(key);
            } catch (Exception e) {
            }
        }
        if (messages == null) {
            messages = BaoyiApplication.getInstance().getDiskTextCache().get((Object) key);
        }
        new ArrayList();
        return (List) new Gson().fromJson(messages, new TypeToken<LinkedList<Ring>>() {
        }.getType());
    }
}
