package com.by.server;

import com.by.bean.RingSecList;
import com.by.constant.Constant;
import com.by.pacbell.BaoyiApplication;
import com.by.utils.Connection;
import com.by.utils.HttpConnection;
import com.by.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ServerRingSecList {
    private static Map<String, String> caches = new HashMap();

    public static List<RingSecList> findByTypePid(Integer id) {
        String url = String.valueOf(Constant.server) + "Servlet_FindRingSecListTypeAll";
        String key = Utils.getMD5Str(String.valueOf(url) + id);
        String messages = caches.get(key);
        if (messages == null) {
            try {
                Connection conn = HttpConnection.connect(url);
                conn.data("ringseckeyid", id.toString());
                conn.method(Connection.Method.GET);
                conn.timeout(10000);
                String messages2 = conn.execute().body();
                caches.put(key, messages2);
                BaoyiApplication.getInstance().getDiskTextCache().put(key, messages2);
                System.out.println("---------------------------------null-----1" + messages2);
                messages = caches.get(key);
            } catch (Exception e) {
            }
        }
        if (messages == null) {
            messages = BaoyiApplication.getInstance().getDiskTextCache().get((Object) key);
            System.out.println("---------------------------------null-----2" + messages);
        } else if (id.intValue() == Constant.updateId) {
            messages = null;
            try {
                Connection conn2 = HttpConnection.connect(url);
                conn2.data("ringseckeyid", id.toString());
                conn2.method(Connection.Method.GET);
                messages = conn2.execute().body();
                caches.put(key, messages);
                BaoyiApplication.getInstance().getDiskTextCache().put(key, messages);
                System.out.println("---------------------------------null-----1" + messages);
            } catch (Exception e2) {
            }
        }
        new ArrayList();
        return (List) new Gson().fromJson(messages, new TypeToken<LinkedList<RingSecList>>() {
        }.getType());
    }

    public static RingSecList FindListId(Integer id) {
        String url = String.valueOf(Constant.server) + "Servlet_FindRingSecListById";
        String key = Utils.getMD5Str(String.valueOf(url) + id);
        String messages = caches.get(key);
        if (messages == null) {
            try {
                Connection conn = HttpConnection.connect(url);
                conn.data("ringsecid", id.toString());
                conn.method(Connection.Method.GET);
                conn.timeout(10000);
                String messages2 = conn.execute().body();
                caches.put(key, messages2);
                BaoyiApplication.getInstance().getDiskTextCache().put(key, messages2);
                messages = caches.get(key);
            } catch (Exception e) {
            }
        }
        if (messages == null) {
            messages = BaoyiApplication.getInstance().getDiskTextCache().get((Object) key);
        }
        return (RingSecList) new Gson().fromJson(messages, RingSecList.class);
    }
}
