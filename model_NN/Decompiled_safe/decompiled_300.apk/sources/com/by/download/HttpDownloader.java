package com.by.download;

import android.os.Environment;
import com.by.utils.FileUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpDownloader {
    private String SDPATH;
    private URL url = null;

    /* JADX WARNING: Unknown top exception splitter block from list: {B:23:0x0048=Splitter:B:23:0x0048, B:15:0x003a=Splitter:B:15:0x003a} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String download(java.lang.String r9) {
        /*
            r8 = this;
            java.lang.StringBuffer r4 = new java.lang.StringBuffer
            r4.<init>()
            r3 = 0
            r0 = 0
            java.net.URL r6 = new java.net.URL     // Catch:{ MalformedURLException -> 0x006b, IOException -> 0x0046 }
            r6.<init>(r9)     // Catch:{ MalformedURLException -> 0x006b, IOException -> 0x0046 }
            r8.url = r6     // Catch:{ MalformedURLException -> 0x006b, IOException -> 0x0046 }
            java.net.URL r6 = r8.url     // Catch:{ MalformedURLException -> 0x006b, IOException -> 0x0046 }
            java.net.URLConnection r5 = r6.openConnection()     // Catch:{ MalformedURLException -> 0x006b, IOException -> 0x0046 }
            java.net.HttpURLConnection r5 = (java.net.HttpURLConnection) r5     // Catch:{ MalformedURLException -> 0x006b, IOException -> 0x0046 }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ MalformedURLException -> 0x006b, IOException -> 0x0046 }
            java.io.InputStreamReader r6 = new java.io.InputStreamReader     // Catch:{ MalformedURLException -> 0x006b, IOException -> 0x0046 }
            java.io.InputStream r7 = r5.getInputStream()     // Catch:{ MalformedURLException -> 0x006b, IOException -> 0x0046 }
            r6.<init>(r7)     // Catch:{ MalformedURLException -> 0x006b, IOException -> 0x0046 }
            r1.<init>(r6)     // Catch:{ MalformedURLException -> 0x006b, IOException -> 0x0046 }
        L_0x0024:
            java.lang.String r3 = r1.readLine()     // Catch:{ MalformedURLException -> 0x0037, IOException -> 0x0067, all -> 0x0064 }
            if (r3 != 0) goto L_0x0033
            r1.close()     // Catch:{ IOException -> 0x005e }
            r0 = r1
        L_0x002e:
            java.lang.String r6 = r4.toString()
            return r6
        L_0x0033:
            r4.append(r3)     // Catch:{ MalformedURLException -> 0x0037, IOException -> 0x0067, all -> 0x0064 }
            goto L_0x0024
        L_0x0037:
            r6 = move-exception
            r2 = r6
            r0 = r1
        L_0x003a:
            r2.printStackTrace()     // Catch:{ all -> 0x0054 }
            r0.close()     // Catch:{ IOException -> 0x0041 }
            goto L_0x002e
        L_0x0041:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x002e
        L_0x0046:
            r6 = move-exception
            r2 = r6
        L_0x0048:
            r2.printStackTrace()     // Catch:{ all -> 0x0054 }
            r0.close()     // Catch:{ IOException -> 0x004f }
            goto L_0x002e
        L_0x004f:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x002e
        L_0x0054:
            r6 = move-exception
        L_0x0055:
            r0.close()     // Catch:{ IOException -> 0x0059 }
        L_0x0058:
            throw r6
        L_0x0059:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0058
        L_0x005e:
            r2 = move-exception
            r2.printStackTrace()
            r0 = r1
            goto L_0x002e
        L_0x0064:
            r6 = move-exception
            r0 = r1
            goto L_0x0055
        L_0x0067:
            r6 = move-exception
            r2 = r6
            r0 = r1
            goto L_0x0048
        L_0x006b:
            r6 = move-exception
            r2 = r6
            goto L_0x003a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.by.download.HttpDownloader.download(java.lang.String):java.lang.String");
    }

    public int downFile(String urlStr, String path, String fileName) {
        InputStream inputStream = null;
        try {
            FileUtils fileUtils = new FileUtils();
            long Remainingspace = fileUtils.readSDCard();
            if (fileUtils.isFileExist(fileName, path)) {
                try {
                    inputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return 1;
            } else if (Remainingspace / 1024 < 200) {
                try {
                    inputStream.close();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                return 4;
            } else {
                inputStream = getInputStreamFromUrl(urlStr);
                if (fileUtils.write2SDFromInput(path, fileName, inputStream) == null) {
                    try {
                        inputStream.close();
                    } catch (Exception e3) {
                        e3.printStackTrace();
                    }
                    return -1;
                }
                try {
                    inputStream.close();
                } catch (Exception e4) {
                    e4.printStackTrace();
                }
                return 0;
            }
        } catch (Exception e5) {
            e5.printStackTrace();
            try {
                inputStream.close();
            } catch (Exception e6) {
                e6.printStackTrace();
            }
            return -1;
        } catch (Throwable th) {
            try {
                inputStream.close();
            } catch (Exception e7) {
                e7.printStackTrace();
            }
            throw th;
        }
    }

    public int ReFile(String urlStr, String path, String fileName) {
        InputStream inputStream = null;
        try {
            FileUtils fileUtils = new FileUtils();
            long Remainingspace = fileUtils.readSDCard();
            if (!fileUtils.isFileExist(fileName, path)) {
                try {
                    inputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return 2;
            } else if (Remainingspace < fileUtils.getFileSize(fileName, path)) {
                try {
                    inputStream.close();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                return 4;
            } else {
                this.SDPATH = String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + File.separator;
                new File(String.valueOf(this.SDPATH) + path + File.separator + fileName).delete();
                inputStream = getInputStreamFromUrl(urlStr);
                if (fileUtils.write2SDFromInput(path, fileName, inputStream) == null) {
                    try {
                        inputStream.close();
                    } catch (Exception e3) {
                        e3.printStackTrace();
                    }
                    return -1;
                }
                try {
                    inputStream.close();
                } catch (Exception e4) {
                    e4.printStackTrace();
                }
                return 0;
            }
        } catch (Exception e5) {
            e5.printStackTrace();
            try {
                inputStream.close();
            } catch (Exception e6) {
                e6.printStackTrace();
            }
            return -1;
        } catch (Throwable th) {
            try {
                inputStream.close();
            } catch (Exception e7) {
                e7.printStackTrace();
            }
            throw th;
        }
    }

    public int copyFile(String urlStr, String path, String fileName) {
        Exception e;
        InputStream inputStream = null;
        try {
            FileUtils fileUtils = new FileUtils();
            if (fileUtils.isFileExist(fileName, path)) {
                try {
                    inputStream.close();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                return 1;
            }
            InputStream inputStream2 = new FileInputStream(new File(urlStr));
            try {
                OutputStream output = new FileOutputStream(fileUtils.creatSDFile(fileName, path));
                byte[] buffer = new byte[4096];
                while (true) {
                    int temp = inputStream2.read(buffer);
                    if (temp == -1) {
                        break;
                    }
                    output.write(buffer, 0, temp);
                }
                output.flush();
                try {
                    inputStream2.close();
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
                return 0;
            } catch (Exception e4) {
                e = e4;
                inputStream = inputStream2;
            } catch (Throwable th) {
                th = th;
                inputStream = inputStream2;
                try {
                    inputStream.close();
                } catch (Exception e5) {
                    e5.printStackTrace();
                }
                throw th;
            }
        } catch (Exception e6) {
            e = e6;
        }
        try {
            e.printStackTrace();
            try {
                inputStream.close();
            } catch (Exception e7) {
                e7.printStackTrace();
            }
            return -1;
        } catch (Throwable th2) {
            th = th2;
            inputStream.close();
            throw th;
        }
        return -1;
    }

    public int DelFile(String urlStr) {
        try {
            new File(urlStr).delete();
            return 3;
        } catch (Exception e) {
            e.printStackTrace();
            return -2;
        }
    }

    public InputStream getInputStreamFromUrl(String urlStr) throws IOException {
        this.url = new URL(urlStr);
        return ((HttpURLConnection) this.url.openConnection()).getInputStream();
    }
}
