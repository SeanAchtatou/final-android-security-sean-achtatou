package com.by.download;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.ListView;
import android.widget.Toast;
import com.by.adapter.ListAdapter;
import com.by.bean.RingSecList;
import com.by.pacbell.OtherActivity;
import com.by.server.ServerRingSecList;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class DownloadTitleTask extends AsyncTask<String, Integer, List<RingSecList>> {
    OtherActivity curcontext;
    private ProgressDialog dialog = null;
    List<RingSecList> info;
    ListView listview;

    /* access modifiers changed from: protected */
    public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        onPostExecute((List<RingSecList>) ((List) obj));
    }

    public DownloadTitleTask(OtherActivity context, ListView l, List<RingSecList> list) {
        this.curcontext = context;
        this.listview = l;
        this.info = list;
    }

    public DownloadTitleTask() {
    }

    public void onPreExecute() {
        showLoadSongDialog();
        this.dialog.show();
    }

    private void showLoadSongDialog() {
        this.dialog = new ProgressDialog(this.curcontext);
        this.dialog.setProgressStyle(0);
        this.dialog.setTitle("正在 加载章节内容");
        this.dialog.setMessage("请稍等！");
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        super.onCancelled();
    }

    public List<RingSecList> doInBackground(String... params) {
        this.info = ServerRingSecList.findByTypePid(Integer.valueOf(Integer.parseInt(params[0])));
        System.out.println("info==============" + this.info);
        return this.info;
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(Integer... aa) {
    }

    public InputStream getInputStreamFromUrl(String urlStr) throws IOException {
        return ((HttpURLConnection) new URL(urlStr).openConnection()).getInputStream();
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(List<RingSecList> result) {
        ListAdapter adapter = new ListAdapter(this.curcontext);
        if (result != null) {
            adapter.setList(result);
            this.listview.setAdapter((android.widget.ListAdapter) adapter);
        }
        this.dialog.dismiss();
        if (result != null && result.size() == 0) {
            Toast.makeText(this.curcontext, "获取数据失败,请再次尝试", 0).show();
        }
        if (result == null) {
            Toast.makeText(this.curcontext, "请求服务器数据失败，请重新尝试一次", 0).show();
        }
        if (result != null) {
            this.curcontext.work(result);
        }
    }
}
