package com.by.download;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.Toast;
import com.by.adapter.GridAdapter;
import com.by.bean.Ring;
import com.by.pacbell.ActivityMain;
import com.by.pacbell.BaoyiApplication;
import com.by.server.ServerRing;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class DownloadTask extends AsyncTask<String, Integer, List<Ring>> {
    BaoyiApplication baoyi_online = new BaoyiApplication();
    ActivityMain curcontext;
    private ProgressDialog dialog = null;
    GridView gridview;
    List<Ring> info;
    private Ring park;

    /* access modifiers changed from: protected */
    public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        onPostExecute((List<Ring>) ((List) obj));
    }

    public DownloadTask(ActivityMain context, GridView g) {
        this.curcontext = context;
        this.gridview = g;
    }

    public DownloadTask() {
    }

    public void onPreExecute() {
        showLoadSongDialog();
        this.dialog.show();
    }

    private void showLoadSongDialog() {
        this.dialog = new ProgressDialog(this.curcontext);
        this.dialog.setProgressStyle(0);
        this.dialog.setTitle("正在加载文本内容，请稍等");
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        super.onCancelled();
    }

    public List<Ring> doInBackground(String... params) {
        this.info = ServerRing.findRingAll();
        System.out.println("info==============" + this.info);
        return this.info;
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(Integer... aa) {
    }

    public InputStream getInputStreamFromUrl(String urlStr) throws IOException {
        return ((HttpURLConnection) new URL(urlStr).openConnection()).getInputStream();
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(List<Ring> result) {
        GridAdapter adapter = new GridAdapter(this.curcontext);
        if (result != null) {
            adapter.setList(result);
            this.gridview.setAdapter((ListAdapter) adapter);
        }
        this.dialog.dismiss();
        if (result != null && result.size() == 0) {
            Toast.makeText(this.curcontext, "获取数据失败,请再次尝试", 0).show();
        }
        if (result == null) {
            Toast.makeText(this.curcontext, "请求数据失败,请再次尝试", 0).show();
        }
        if (result != null) {
            this.curcontext.work(result);
        }
    }
}
