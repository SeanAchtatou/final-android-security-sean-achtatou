package com.by.download;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;
import com.by.constant.Constant;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class DownLoadMp3Task extends AsyncTask<String, Integer, Void> {
    Context curcontext;
    private int isdown;
    private ProgressDialog progressDialog = null;

    public DownLoadMp3Task(Context context) {
        this.curcontext = context;
    }

    public void onPreExecute() {
        this.progressDialog = ProgressDialog.show(this.curcontext, "下载铃音", "正在下载铃音,请稍候！");
        super.onPreExecute();
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        super.onCancelled();
    }

    public Void doInBackground(String... params) {
        try {
            this.isdown = -100;
            String path = String.valueOf(Constant.baoyiringmp3) + params[1] + ".mp3";
            URLConnection conn = new URL(String.valueOf(Constant.server_mp3_url) + params[0]).openConnection();
            conn.setDoInput(true);
            int length = conn.getContentLength();
            InputStream is = conn.getInputStream();
            if (length != -1) {
                FileOutputStream output = new FileOutputStream(new File(path));
                byte[] buffer = new byte[6144];
                int downsize = 0;
                while (true) {
                    int count = is.read(buffer);
                    if (count == -1) {
                        break;
                    }
                    output.write(buffer, 0, count);
                    downsize += count;
                    publishProgress(Integer.valueOf((int) ((((float) downsize) / ((float) length)) * 100.0f)));
                }
                output.flush();
            }
            this.isdown = 100;
            return null;
        } catch (Exception e) {
            this.isdown = -100;
            e.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(Integer... aa) {
        this.progressDialog.setTitle("下载铃音:" + aa[0] + "%");
    }

    public InputStream getInputStreamFromUrl(String urlStr) throws IOException {
        return ((HttpURLConnection) new URL(urlStr).openConnection()).getInputStream();
    }

    public void onPostExecute(Void url) {
        super.onPostExecute((Object) url);
        this.progressDialog.dismiss();
        if (this.isdown > 0) {
            Toast.makeText(this.curcontext, "下载铃声成功", 0).show();
        } else {
            Toast.makeText(this.curcontext, "下载铃声失败", 0).show();
        }
    }
}
