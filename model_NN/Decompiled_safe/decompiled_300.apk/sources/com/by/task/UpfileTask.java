package com.by.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;
import com.by.constant.Constant;
import com.by.utils.DataUtil;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;

public class UpfileTask extends AsyncTask<String, String, Void> {
    Context curcontext;
    private int isdown;
    private String pacName;
    private ProgressDialog progressDialog = null;
    private String sdcardurl;

    public UpfileTask(Context context) {
        this.curcontext = context;
    }

    public Void doInBackground(String... params) {
        HttpClient httpclient = new DefaultHttpClient(new BasicHttpParams());
        this.sdcardurl = params[0];
        this.pacName = params[1];
        System.out.println("param[0]====" + params[0] + ",param[1]==" + params[1]);
        try {
            HttpPost httppost = new HttpPost(String.valueOf(Constant.server) + "UpFile");
            MultipartEntity entity = new MultipartEntity();
            entity.addPart("contents", new StringBody(this.pacName, Charset.forName(DataUtil.defaultCharset)));
            entity.addPart("fileitem", new FileBody(new File(String.valueOf(Constant.sdCard) + this.sdcardurl), "a", "utf-8"));
            httppost.setEntity(entity);
            publishProgress("准备上传音乐");
            httpclient.execute(httppost);
            publishProgress("上传音乐成功");
            this.isdown = 100;
            return null;
        } catch (UnsupportedEncodingException e) {
            UnsupportedEncodingException e2 = e;
            this.isdown = -100;
            publishProgress(e2.getMessage());
            return null;
        } catch (IOException e3) {
            publishProgress(e3.getMessage());
            this.isdown = -100;
            return null;
        } finally {
            httpclient.getConnectionManager().shutdown();
        }
    }

    public InputStream getInputStreamFromUrl(String urlStr) throws IOException {
        return ((HttpURLConnection) new URL(urlStr).openConnection()).getInputStream();
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        super.onCancelled();
    }

    public void onPostExecute(Void url) {
        super.onPostExecute((Object) url);
        this.progressDialog.dismiss();
        if (this.isdown > 0) {
            Toast.makeText(this.curcontext, "上传音乐成功 ", 0).show();
        } else {
            Toast.makeText(this.curcontext, "上传音乐失败", 0).show();
        }
    }

    public void onPreExecute() {
        this.progressDialog = ProgressDialog.show(this.curcontext, "上传音乐", "正在上传音乐,请稍候！");
        super.onPreExecute();
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(String... aa) {
        this.progressDialog.setMessage(aa[0]);
    }
}
