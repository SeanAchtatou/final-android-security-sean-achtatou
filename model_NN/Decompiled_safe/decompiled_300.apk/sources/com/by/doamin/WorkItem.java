package com.by.doamin;

public class WorkItem {
    private Integer categories;
    private String fileurl;
    private Integer hits;
    private Integer id;
    private String name;
    private Integer next;
    private Integer previous;
    private Integer sortnum;
    private String tag;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id2) {
        this.id = id2;
    }

    public Integer getHits() {
        return this.hits;
    }

    public void setHits(Integer hits2) {
        this.hits = hits2;
    }

    public Integer getSortnum() {
        return this.sortnum;
    }

    public void setSortnum(Integer sortnum2) {
        this.sortnum = sortnum2;
    }

    public Integer getCategories() {
        return this.categories;
    }

    public void setCategories(Integer categories2) {
        this.categories = categories2;
    }

    public Integer getNext() {
        return this.next;
    }

    public void setNext(Integer next2) {
        this.next = next2;
    }

    public Integer getPrevious() {
        return this.previous;
    }

    public void setPrevious(Integer previous2) {
        this.previous = previous2;
    }

    public String getFileurl() {
        return this.fileurl;
    }

    public void setFileurl(String fileurl2) {
        this.fileurl = fileurl2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getTag() {
        return this.tag;
    }

    public void setTag(String tag2) {
        this.tag = tag2;
    }
}
