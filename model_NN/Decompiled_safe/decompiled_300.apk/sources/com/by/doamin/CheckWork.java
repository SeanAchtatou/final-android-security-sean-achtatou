package com.by.doamin;

public class CheckWork {
    private boolean isalarms;
    private boolean isnotifications;
    private boolean isringtones;

    public boolean isIsringtones() {
        return this.isringtones;
    }

    public void setIsringtones(boolean isringtones2) {
        this.isringtones = isringtones2;
    }

    public boolean isIsalarms() {
        return this.isalarms;
    }

    public void setIsalarms(boolean isalarms2) {
        this.isalarms = isalarms2;
    }

    public boolean isIsnotifications() {
        return this.isnotifications;
    }

    public void setIsnotifications(boolean isnotifications2) {
        this.isnotifications = isnotifications2;
    }
}
