package com.admogo.adapters;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import com.admogo.AdMogoLayout;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import com.vpon.adon.android.AdListener;
import com.vpon.adon.android.AdOnPlatform;
import com.vpon.adon.android.AdView;

public class VponCNAdapter extends AdMogoAdapter implements AdListener {
    private Activity activity;

    public VponCNAdapter(AdMogoLayout adMogoLayout, Ration ration) {
        super(adMogoLayout, ration);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{com.admogo.AdMogoLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void}
     arg types: [com.vpon.adon.android.AdView, android.view.ViewGroup$LayoutParams]
     candidates:
      ClspMth{android.view.ViewGroup.addView(android.view.View, int):void}
      SimpleMethodDetails{com.admogo.AdMogoLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void} */
    public void handle() {
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null) {
            this.activity = adMogoLayout.activityReference.get();
            if (this.activity != null) {
                try {
                    AdView adView = new AdView(this.activity);
                    adView.setLicenseKey(this.ration.key, AdOnPlatform.CN, false);
                    adView.setAdListener(this);
                    adMogoLayout.addView((View) adView, new ViewGroup.LayoutParams(-2, -2));
                } catch (IllegalArgumentException e) {
                    adMogoLayout.rollover();
                }
            }
        }
    }

    public void onRecevieAd(AdView adView) {
        adView.setAdListener((AdListener) null);
        if (!this.activity.isFinishing()) {
            Log.d(AdMogoUtil.ADMOGO, "Vpon success");
            AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
            if (adMogoLayout != null) {
                adMogoLayout.adMogoManager.resetRollover();
                adMogoLayout.handler.post(new AdMogoLayout.ViewAdRunnable(adMogoLayout, adView, 30));
                adMogoLayout.rotateThreadedDelayed();
            }
        }
    }

    public void onFailedToRecevieAd(AdView adView) {
        adView.setAdListener((AdListener) null);
        if (!this.activity.isFinishing()) {
            Log.d(AdMogoUtil.ADMOGO, "Vpon failure");
            AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
            if (adMogoLayout != null) {
                adMogoLayout.rollover();
            }
        }
    }

    public void finish() {
        Log.d(AdMogoUtil.ADMOGO, "Vpon Finished");
    }
}
