package com.admogo.util;

import android.app.Activity;
import android.util.DisplayMetrics;

public class AdMogoUtil extends Activity {
    public static final String ADMOGO = "AdMogo SDK";
    public static final int CUSTOM_TYPE_BANNER = 1;
    public static final int CUSTOM_TYPE_ICON = 2;
    public static final int MOGO_TYPE_BANNER = 1;
    public static final int MOGO_TYPE_ICON = 2;
    public static final int NETWORK_TYPE_4THSCREEN = 13;
    public static final int NETWORK_TYPE_ADCHINA = 21;
    public static final int NETWORK_TYPE_ADMOB = 1;
    public static final int NETWORK_TYPE_ADMOGO = 10;
    public static final int NETWORK_TYPE_ADSENSE = 14;
    public static final int NETWORK_TYPE_ADTOUCH = 28;
    public static final int NETWORK_TYPE_ADWO = 33;
    public static final int NETWORK_TYPE_AIRAD = 32;
    public static final int NETWORK_TYPE_APPMEDIA = 36;
    public static final int NETWORK_TYPE_CASEE = 25;
    public static final int NETWORK_TYPE_CUSTOM = 9;
    public static final int NETWORK_TYPE_DOMOB = 29;
    public static final int NETWORK_TYPE_DOUBLECLICK = 15;
    public static final int NETWORK_TYPE_EVENT = 17;
    public static final int NETWORK_TYPE_GENERIC = 16;
    public static final int NETWORK_TYPE_GREYSTRIP = 7;
    public static final int NETWORK_TYPE_INMOBI = 18;
    public static final int NETWORK_TYPE_JUMPTAP = 2;
    public static final int NETWORK_TYPE_LIVERAIL = 5;
    public static final int NETWORK_TYPE_LSENSE = 34;
    public static final int NETWORK_TYPE_MDOTM = 12;
    public static final int NETWORK_TYPE_MEDIALETS = 4;
    public static final int NETWORK_TYPE_MILLENNIAL = 6;
    public static final int NETWORK_TYPE_MOBCLIX = 11;
    public static final int NETWORK_TYPE_MOGO = 27;
    public static final int NETWORK_TYPE_QUATTRO = 8;
    public static final int NETWORK_TYPE_SMAATO = 35;
    public static final int NETWORK_TYPE_SMART = 26;
    public static final int NETWORK_TYPE_VIDEOEGG = 3;
    public static final int NETWORK_TYPE_VPON = 30;
    public static final int NETWORK_TYPE_WIYUN = 22;
    public static final int NETWORK_TYPE_WOOBOO = 23;
    public static final int NETWORK_TYPE_YOUMI = 24;
    public static final int NETWORK_TYPE_ZESTADZ = 20;
    public static final int VERSION = 274;
    private static double density = -1.0d;
    public static final String urlClick = "http://clk.adsmogo.com/exclick.ashx?appid=%s&nid=%s&type=%d&uuid=%s&country=%s&appver=%d&client=2&nt=%s&os=%s&dn=%s&adtype=%s";
    public static final String urlConfig = "http://cfg.adsmogo.com/GetInfo.ashx?appid=%s&appver=%d&client=2&country=%s&nt=%s&os=%s&dn=%s&adtype=%s&size=%s";
    public static final String urlCustom = "http://cus.adsmogo.com/custom.ashx?appid=%s&nid=%s&uuid=%s&country=%s&appver=%d&client=2&nt=%s&os=%s&dn=%s";
    public static final String urlImpression = "http://imp.adsmogo.com/exmet.ashx?appid=%s&nid=%s&type=%d&uuid=%s&country=%s&appver=%d&client=2&nt=%s&os=%s&dn=%s&adtype=%s";
    public static final String urlMogo = "http://www.adsmogo.com/adserv.php?appid=%s&nid=%s&uuid=%s&country=%s&ad_type=%d&gender=%s&birthday=%s&keywords=%s&appver=%d&client=2&nt=%s&os=%s&dn=%s";
    public static final String urlNull = "http://blk.adsmogo.com/blank.ashx?appid=%s&nid=%s&type=%s&country=%s&appver=%s&nt=%s&os=%s&dn=%s&adtype=%s";
    public static final String urlRequest = "http://req.adsmogo.com/exrequest.ashx?appid=%s&nid=%s&country=%s&appver=%d&type=%d&client=2&nt=%s&os=%s&dn=%s&adtype=%s";

    public static String convertToHex(byte[] data) {
        StringBuffer buf = new StringBuffer();
        for (byte element : data) {
            int halfbyte = (element >>> 4) & 15;
            int two_halfs = 0;
            while (true) {
                if (halfbyte < 0 || halfbyte > 9) {
                    buf.append((char) ((halfbyte - 10) + 97));
                } else {
                    buf.append((char) (halfbyte + 48));
                }
                halfbyte = element & 15;
                int two_halfs2 = two_halfs + 1;
                if (two_halfs >= 1) {
                    break;
                }
                two_halfs = two_halfs2;
            }
        }
        return buf.toString();
    }

    public static double getDensity(Activity activity) {
        if (density == -1.0d) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            density = (double) displayMetrics.density;
        }
        return density;
    }

    public static int convertToScreenPixels(int dipPixels, double density2) {
        return (int) convertToScreenPixels((double) dipPixels, density2);
    }

    public static double convertToScreenPixels(double dipPixels, double density2) {
        return density2 > 0.0d ? dipPixels * density2 : dipPixels;
    }
}
