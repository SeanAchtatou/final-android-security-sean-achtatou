package cn.domob.android.ads;

import android.content.Context;
import android.util.Log;

final class e extends Thread {
    private DomobAdView a;

    public e(DomobAdView domobAdView) {
        this.a = domobAdView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.domob.android.ads.DomobAdView.a(cn.domob.android.ads.DomobAdView, boolean):void
     arg types: [cn.domob.android.ads.DomobAdView, int]
     candidates:
      cn.domob.android.ads.DomobAdView.a(cn.domob.android.ads.DomobAdView, cn.domob.android.ads.DomobAdBuilder):cn.domob.android.ads.DomobAdBuilder
      cn.domob.android.ads.DomobAdView.a(cn.domob.android.ads.DomobAdEngine, cn.domob.android.ads.DomobAdBuilder):void
      cn.domob.android.ads.DomobAdView.a(cn.domob.android.ads.DomobAdView, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.domob.android.ads.DomobAdView.b(cn.domob.android.ads.DomobAdView, boolean):void
     arg types: [cn.domob.android.ads.DomobAdView, int]
     candidates:
      cn.domob.android.ads.DomobAdView.b(cn.domob.android.ads.DomobAdView, cn.domob.android.ads.DomobAdBuilder):void
      cn.domob.android.ads.DomobAdView.b(cn.domob.android.ads.DomobAdView, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.domob.android.ads.DomobAdView.c(cn.domob.android.ads.DomobAdView, boolean):void
     arg types: [cn.domob.android.ads.DomobAdView, int]
     candidates:
      cn.domob.android.ads.DomobAdView.c(cn.domob.android.ads.DomobAdView, cn.domob.android.ads.DomobAdBuilder):void
      cn.domob.android.ads.DomobAdView.c(cn.domob.android.ads.DomobAdView, boolean):void */
    public final void run() {
        boolean z;
        boolean z2;
        if (this.a == null) {
            Log.e("DomobSDK", "DomobGetDcThread exit because adview is null.");
            return;
        }
        if (Log.isLoggable("DomobSDK", 3)) {
            Log.d("DomobSDK", "DomobGetDcThread start");
        }
        Context context = this.a.getContext();
        try {
            f fVar = new f();
            c a2 = fVar.a(context);
            if (a2 == null) {
                if (Log.isLoggable("DomobSDK", 3)) {
                    Log.d("DomobSDK", "detector is null.");
                }
                b a3 = fVar.a();
                if (a3 != null) {
                    int e = a3.e();
                    if (e < 200 || e >= 300) {
                        if (Log.isLoggable("DomobSDK", 3)) {
                            Log.d("DomobSDK", "connection return error:" + e + ", try again!");
                        }
                        z2 = false;
                    } else {
                        DomobAdView.a(this.a, false);
                        z2 = true;
                    }
                } else {
                    z2 = false;
                }
            } else {
                if (Log.isLoggable("DomobSDK", 3)) {
                    Log.d("DomobSDK", "success to get detector.");
                }
                int a4 = a2.a();
                if (a4 == 0) {
                    z = false;
                } else {
                    z = true;
                }
                if (a4 != 1) {
                    this.a.setRequestInterval(a4);
                }
                this.a.a(a2.b(), a2.c(), a2.d());
                DomobAdView.a(this.a, false);
                z2 = z;
            }
            if (DomobAdManager.h(context) == null) {
                Log.w("DomobSDK", "CID is null, continue detecting!");
                DomobAdView.a(this.a, true);
                z2 = false;
            }
            DomobAdView.b(this.a, false);
            if (z2) {
                if (Log.isLoggable("DomobSDK", 3)) {
                    Log.d("DomobSDK", "request ad without delay.");
                }
                DomobAdView.a(this.a);
                return;
            }
            DomobAdView.c(this.a, true);
        } catch (Exception e2) {
            if (DomobAdManager.getPublisherId(context) == null) {
                Log.e("DomobSDK", "Please set your publisher ID first!");
            } else {
                Log.e("DomobSDK", "Unkown exception happened!");
            }
            e2.printStackTrace();
        }
    }
}
