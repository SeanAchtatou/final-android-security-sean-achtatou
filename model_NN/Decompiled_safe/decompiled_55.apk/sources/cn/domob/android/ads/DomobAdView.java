package cn.domob.android.ads;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;
import cn.domob.android.ads.DomobAdEngine;

public class DomobAdView extends RelativeLayout {
    static final Handler a = new Handler();
    private static Boolean b = null;
    /* access modifiers changed from: private */
    public DomobAdBuilder c;
    private String d;
    private DomobAdEngine.RecvHandler e;
    private int f;
    private int g;
    private long h;
    private long i;
    private long j;
    private String k;
    private DomobAdListener l;
    private boolean m;
    private boolean n;
    private boolean o;
    /* access modifiers changed from: private */
    public boolean p;
    /* access modifiers changed from: private */
    public boolean q;
    private long r;
    private long s;
    /* access modifiers changed from: private */
    public int t;
    /* access modifiers changed from: private */
    public boolean u;
    /* access modifiers changed from: private */
    public Context v;
    private boolean w;
    private boolean x;
    private c y;

    public DomobAdView(Activity activity) {
        this(activity, null, 0);
    }

    public DomobAdView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public DomobAdView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.i = -16777216;
        this.j = -1;
        this.p = true;
        this.w = false;
        this.x = false;
        Log.i("DomobSDK", "current version is 20110503");
        if (Log.isLoggable("DomobSDK", 3)) {
            Log.d("DomobSDK", "DomobAdView!");
        }
        setDescendantFocusability(262144);
        setClickable(true);
        setLongClickable(false);
        setGravity(17);
        this.f = 20000;
        if (attributeSet != null) {
            String str = "http://schemas.android.com/apk/res/" + context.getPackageName();
            int attributeUnsignedIntValue = attributeSet.getAttributeUnsignedIntValue(str, "backgroundColor", -1);
            if (attributeUnsignedIntValue != -1) {
                setBackgroundColor(attributeUnsignedIntValue);
            }
            int attributeUnsignedIntValue2 = attributeSet.getAttributeUnsignedIntValue(str, "primaryTextColor", -1);
            if (attributeUnsignedIntValue2 != -1) {
                setPrimaryTextColor(attributeUnsignedIntValue2);
            }
            this.d = attributeSet.getAttributeValue(str, "keywords");
            this.k = attributeSet.getAttributeValue(str, "spots");
            this.f = a(attributeSet.getAttributeIntValue(str, "refreshInterval", 20)) * 1000;
            if (Log.isLoggable("DomobSDK", 3)) {
                Log.d("DomobSDK", "setDefaultInterval:" + this.f);
            }
        }
        this.v = context;
        this.c = null;
        this.e = null;
        if (b == null) {
            b = Boolean.valueOf(b(context));
        }
        if (!a(context)) {
            this.q = false;
            this.r = 0;
            this.s = 0;
            setRequestInterval(1);
        }
        this.m = false;
        this.o = true;
        this.h = 0;
        this.u = true;
        this.t = 0;
        if (this.q) {
            long uptimeMillis = SystemClock.uptimeMillis();
            if (Log.isLoggable("DomobSDK", 3)) {
                Log.d("DomobSDK", "mDisabled = " + this.q + " | mDisabledTime = " + this.r + " | mDisabledTimestamp = " + this.s);
                Log.d("DomobSDK", "curr timestamp = " + uptimeMillis);
            }
            if (uptimeMillis - this.s >= this.r * 1000) {
                if (Log.isLoggable("DomobSDK", 3)) {
                    Log.d("DomobSDK", "send detector because ad is disabled.");
                }
                e();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0029  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean a(android.content.Context r7) {
        /*
            r6 = this;
            r3 = 1
            r4 = 0
            r0 = 0
            cn.domob.android.ads.DBHelper r1 = cn.domob.android.ads.DBHelper.a(r7)     // Catch:{ Exception -> 0x0074 }
            android.database.Cursor r0 = r1.b()     // Catch:{ Exception -> 0x0074 }
            if (r0 == 0) goto L_0x0013
            int r2 = r0.getCount()     // Catch:{ Exception -> 0x006a }
            if (r2 != 0) goto L_0x002d
        L_0x0013:
            java.lang.String r2 = "DomobSDK"
            r3 = 3
            boolean r2 = android.util.Log.isLoggable(r2, r3)     // Catch:{ Exception -> 0x006a }
            if (r2 == 0) goto L_0x0023
            java.lang.String r2 = "DomobSDK"
            java.lang.String r3 = "no data in conf db, initialize now."
            android.util.Log.d(r2, r3)     // Catch:{ Exception -> 0x006a }
        L_0x0023:
            r1.c()     // Catch:{ Exception -> 0x006a }
            r1 = r4
        L_0x0027:
            if (r0 == 0) goto L_0x002c
            r0.close()
        L_0x002c:
            return r1
        L_0x002d:
            r0.moveToFirst()     // Catch:{ Exception -> 0x006a }
            java.lang.String r1 = "_dis_flag"
            int r1 = r0.getColumnIndexOrThrow(r1)     // Catch:{ Exception -> 0x006a }
            int r1 = r0.getInt(r1)     // Catch:{ Exception -> 0x006a }
            if (r1 != 0) goto L_0x0066
            r1 = 0
            r6.q = r1     // Catch:{ Exception -> 0x006a }
        L_0x003f:
            java.lang.String r1 = "_dis_time"
            int r1 = r0.getColumnIndexOrThrow(r1)     // Catch:{ Exception -> 0x006a }
            long r1 = r0.getLong(r1)     // Catch:{ Exception -> 0x006a }
            r6.r = r1     // Catch:{ Exception -> 0x006a }
            java.lang.String r1 = "_dis_timestamp"
            int r1 = r0.getColumnIndexOrThrow(r1)     // Catch:{ Exception -> 0x006a }
            long r1 = r0.getLong(r1)     // Catch:{ Exception -> 0x006a }
            r6.s = r1     // Catch:{ Exception -> 0x006a }
            java.lang.String r1 = "_interval"
            int r1 = r0.getColumnIndexOrThrow(r1)     // Catch:{ Exception -> 0x006a }
            int r1 = r0.getInt(r1)     // Catch:{ Exception -> 0x006a }
            r6.setRequestInterval(r1)     // Catch:{ Exception -> 0x006a }
            r1 = r3
            goto L_0x0027
        L_0x0066:
            r1 = 1
            r6.q = r1     // Catch:{ Exception -> 0x006a }
            goto L_0x003f
        L_0x006a:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x006e:
            r0.printStackTrace()
            r0 = r1
            r1 = r4
            goto L_0x0027
        L_0x0074:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x006e
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.domob.android.ads.DomobAdView.a(android.content.Context):boolean");
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int i2;
        DomobAdEngine b2;
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (Log.isLoggable("DomobSDK", 3)) {
            Log.d("DomobSDK", "DomobAdView onMeasure");
        }
        int size = View.MeasureSpec.getSize(widthMeasureSpec);
        int mode = View.MeasureSpec.getMode(widthMeasureSpec);
        int size2 = View.MeasureSpec.getSize(heightMeasureSpec);
        int mode2 = View.MeasureSpec.getMode(heightMeasureSpec);
        if (!(mode == Integer.MIN_VALUE || mode == 1073741824)) {
            size = DomobAdManager.e(getContext());
        }
        if (Log.isLoggable("DomobSDK", 3)) {
            Log.d("DomobSDK", "DomobAdView width is :" + size);
        }
        if (mode2 == 1073741824) {
            if (Log.isLoggable("DomobSDK", 3)) {
                Log.d("DomobSDK", "heightMeasureSpec is android.view.View.MeasureSpec.EXACTLY!");
            }
            i2 = size2;
        } else if (this.c == null || (b2 = this.c.b()) == null) {
            i2 = 0;
        } else {
            i2 = b2.a(b2.c());
            if (mode2 == Integer.MIN_VALUE && size2 < i2) {
                Log.e("DomobSDK", "Cannot display ad because its container is too small.  The ad is " + i2 + " pixels tall but is only given " + size2 + " at most to draw into.  Please make your view containing DomobAdView taller.");
                i2 = 0;
            }
        }
        if (Log.isLoggable("DomobSDK", 3)) {
            Log.d("DomobSDK", "DomobAdView height is :" + i2);
        }
        setMeasuredDimension(size, i2);
        if (this.p && !b.booleanValue()) {
            if (Log.isLoggable("DomobSDK", 3)) {
                Log.d("DomobSDK", "request the first ad!");
            }
            f();
        }
    }

    protected static void a(DomobAdView domobAdView, boolean z) {
        domobAdView.o = z;
    }

    public void requestFreshAd() {
        long uptimeMillis = (SystemClock.uptimeMillis() - this.h) / 1000;
        if (uptimeMillis < 20) {
            Log.w("DomobSDK", "Ignoring requestFreshAd.  Called " + uptimeMillis + " seconds since last refresh.  " + "Refreshes must be at least " + 20 + " apart.");
        } else {
            f();
        }
    }

    protected static void a(DomobAdView domobAdView) {
        if (domobAdView != null) {
            domobAdView.f();
        }
    }

    private void e() {
        new e(this).start();
    }

    private void f() {
        if (this.q && (!DomobAdManager.isTestMode(this.v) || !DomobAdManager.isTestAllowed(this.v))) {
            Log.w("DomobSDK", "AD has been disabled on web server, ignore doRefresh()");
        } else if (!this.p && super.getVisibility() != 0) {
            Log.w("DomobSDK", "Cannot doRefresh() when the DomobAdView is not visible. Call DomobAdView.setVisibility(View.VISIBLE) first.");
        } else if (this.m) {
            Log.w("DomobSDK", "Ignoring doRefresh() because we are requesting an ad right now already.");
        } else {
            this.m = true;
            this.h = SystemClock.uptimeMillis();
            if (this.o) {
                if (Log.isLoggable("DomobSDK", 3)) {
                    Log.d("DomobSDK", "send detector!");
                }
                e();
                return;
            }
            if (Log.isLoggable("DomobSDK", 3)) {
                Log.d("DomobSDK", "doRefresh now!");
            }
            new d(this).start();
        }
    }

    public int getRequestInterval() {
        return this.g / 1000;
    }

    public void setRequestInterval(int requestInterval) {
        if (requestInterval == 1) {
            if (Log.isLoggable("DomobSDK", 3)) {
                Log.d("DomobSDK", "use default interval.");
            }
            this.g = this.f;
        } else if (requestInterval == 0) {
            Log.w("DomobSDK", "refreshInterval is 0, AD will not be refreshed any more.");
            this.g = requestInterval * 1000;
            g();
        } else {
            this.g = a(requestInterval) * 1000;
        }
        if (Log.isLoggable("DomobSDK", 3)) {
            Log.d("DomobSDK", "setRequestInterval:" + this.g);
        }
    }

    private static int a(int i2) {
        if (i2 < 20) {
            return 20;
        }
        if (i2 > 600) {
            return 600;
        }
        return i2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00ec, code lost:
        if (r4.g == 0) goto L_0x00ee;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(boolean r5) {
        /*
            r4 = this;
            monitor-enter(r4)
            java.lang.String r0 = "DomobSDK"
            r1 = 3
            boolean r0 = android.util.Log.isLoggable(r0, r1)     // Catch:{ all -> 0x00e5 }
            if (r0 == 0) goto L_0x0078
            java.lang.String r0 = "DomobSDK"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00e5 }
            java.lang.String r2 = "schedule for a fresh ad?"
            r1.<init>(r2)     // Catch:{ all -> 0x00e5 }
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ all -> 0x00e5 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00e5 }
            android.util.Log.d(r0, r1)     // Catch:{ all -> 0x00e5 }
            java.lang.String r0 = "DomobSDK"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00e5 }
            java.lang.String r2 = "check WindowsVisibility:"
            r1.<init>(r2)     // Catch:{ all -> 0x00e5 }
            int r2 = r4.t     // Catch:{ all -> 0x00e5 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00e5 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00e5 }
            android.util.Log.d(r0, r1)     // Catch:{ all -> 0x00e5 }
            java.lang.String r0 = "DomobSDK"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00e5 }
            java.lang.String r2 = "check WindowsFocus:"
            r1.<init>(r2)     // Catch:{ all -> 0x00e5 }
            boolean r2 = r4.u     // Catch:{ all -> 0x00e5 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00e5 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00e5 }
            android.util.Log.d(r0, r1)     // Catch:{ all -> 0x00e5 }
            java.lang.String r0 = "DomobSDK"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00e5 }
            java.lang.String r2 = "getVisibility:"
            r1.<init>(r2)     // Catch:{ all -> 0x00e5 }
            int r2 = r4.getVisibility()     // Catch:{ all -> 0x00e5 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00e5 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00e5 }
            android.util.Log.d(r0, r1)     // Catch:{ all -> 0x00e5 }
            java.lang.String r0 = "DomobSDK"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00e5 }
            java.lang.String r2 = "mInterval:"
            r1.<init>(r2)     // Catch:{ all -> 0x00e5 }
            int r2 = r4.g     // Catch:{ all -> 0x00e5 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00e5 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00e5 }
            android.util.Log.d(r0, r1)     // Catch:{ all -> 0x00e5 }
        L_0x0078:
            r0 = 0
            int r1 = r4.g     // Catch:{ all -> 0x00e5 }
            if (r1 > 0) goto L_0x0085
            int r1 = r4.g     // Catch:{ all -> 0x00e5 }
            if (r1 != 0) goto L_0x0086
            boolean r1 = r4.p     // Catch:{ all -> 0x00e5 }
            if (r1 == 0) goto L_0x0086
        L_0x0085:
            r0 = 1
        L_0x0086:
            if (r5 == 0) goto L_0x00e8
            if (r0 == 0) goto L_0x00e8
            int r0 = r4.getVisibility()     // Catch:{ all -> 0x00e5 }
            if (r0 != 0) goto L_0x00e8
            int r0 = r4.t     // Catch:{ all -> 0x00e5 }
            if (r0 != 0) goto L_0x00e8
            boolean r0 = r4.u     // Catch:{ all -> 0x00e5 }
            if (r0 == 0) goto L_0x00e8
            r4.g()     // Catch:{ all -> 0x00e5 }
            cn.domob.android.ads.DomobAdView$c r0 = new cn.domob.android.ads.DomobAdView$c     // Catch:{ all -> 0x00e5 }
            r0.<init>(r4)     // Catch:{ all -> 0x00e5 }
            r4.y = r0     // Catch:{ all -> 0x00e5 }
            int r0 = r4.g     // Catch:{ all -> 0x00e5 }
            if (r0 != 0) goto L_0x00da
            android.os.Handler r0 = cn.domob.android.ads.DomobAdView.a     // Catch:{ all -> 0x00e5 }
            cn.domob.android.ads.DomobAdView$c r1 = r4.y     // Catch:{ all -> 0x00e5 }
            r2 = 20000(0x4e20, double:9.8813E-320)
            r0.postDelayed(r1, r2)     // Catch:{ all -> 0x00e5 }
        L_0x00af:
            java.lang.String r0 = "DomobSDK"
            r1 = 3
            boolean r0 = android.util.Log.isLoggable(r0, r1)     // Catch:{ all -> 0x00e5 }
            if (r0 == 0) goto L_0x00d8
            java.lang.String r0 = "DomobSDK"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00e5 }
            r1.<init>()     // Catch:{ all -> 0x00e5 }
            java.lang.String r2 = "Ad refresh scheduled for "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00e5 }
            int r2 = r4.g     // Catch:{ all -> 0x00e5 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00e5 }
            java.lang.String r2 = " from now."
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00e5 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00e5 }
            android.util.Log.d(r0, r1)     // Catch:{ all -> 0x00e5 }
        L_0x00d8:
            monitor-exit(r4)     // Catch:{ all -> 0x00e5 }
            return
        L_0x00da:
            android.os.Handler r0 = cn.domob.android.ads.DomobAdView.a     // Catch:{ all -> 0x00e5 }
            cn.domob.android.ads.DomobAdView$c r1 = r4.y     // Catch:{ all -> 0x00e5 }
            int r2 = r4.g     // Catch:{ all -> 0x00e5 }
            long r2 = (long) r2     // Catch:{ all -> 0x00e5 }
            r0.postDelayed(r1, r2)     // Catch:{ all -> 0x00e5 }
            goto L_0x00af
        L_0x00e5:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x00e8:
            if (r5 == 0) goto L_0x00ee
            int r0 = r4.g     // Catch:{ all -> 0x00e5 }
            if (r0 != 0) goto L_0x00d8
        L_0x00ee:
            java.lang.String r0 = "DomobSDK"
            r1 = 3
            boolean r0 = android.util.Log.isLoggable(r0, r1)     // Catch:{ all -> 0x00e5 }
            if (r0 == 0) goto L_0x00fe
            java.lang.String r0 = "DomobSDK"
            java.lang.String r1 = "just cancel the previous request!"
            android.util.Log.d(r0, r1)     // Catch:{ all -> 0x00e5 }
        L_0x00fe:
            r4.g()     // Catch:{ all -> 0x00e5 }
            goto L_0x00d8
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.domob.android.ads.DomobAdView.a(boolean):void");
    }

    private void g() {
        if (a != null) {
            if (this.y != null) {
                this.y.a = true;
                a.removeCallbacks(this.y);
                this.y = null;
            }
            if (Log.isLoggable("DomobSDK", 3)) {
                Log.d("DomobSDK", "Cancelled an ad refresh scheduled.");
            }
        }
    }

    class c implements Runnable {
        boolean a;

        /* synthetic */ c(DomobAdView domobAdView) {
            this((byte) 0);
        }

        private c(byte b2) {
        }

        public final void run() {
            try {
                DomobAdView domobAdView = DomobAdView.this;
                if (!this.a && domobAdView != null) {
                    int b2 = DomobAdView.b(domobAdView) / 1000;
                    if (Log.isLoggable("DomobSDK", 3)) {
                        Log.d("DomobSDK", "Requesting a fresh ad because a request interval passed (" + b2 + " seconds).");
                    }
                    DomobAdView.a(domobAdView);
                }
            } catch (Exception e) {
                Log.e("DomobSDK", "exception caught in RefreshThread.run()!");
                e.printStackTrace();
            }
        }
    }

    public void onWindowFocusChanged(boolean flag) {
        if (Log.isLoggable("DomobSDK", 3)) {
            Log.d("DomobSDK", "onWindowFocusChanged:" + flag);
        }
        this.u = flag;
        a(flag);
        super.onWindowFocusChanged(flag);
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int v2) {
        if (Log.isLoggable("DomobSDK", 3)) {
            Log.d("DomobSDK", "onWindowVisibilityChanged:" + v2);
        }
        this.t = v2;
        a(v2 == 0);
        super.onWindowVisibilityChanged(v2);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        if (Log.isLoggable("DomobSDK", 3)) {
            Log.d("DomobSDK", "onAttachedToWindow");
        }
        this.n = true;
        a(true);
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        if (Log.isLoggable("DomobSDK", 3)) {
            Log.d("DomobSDK", "onDetachedFromWindow");
        }
        this.n = false;
        this.p = true;
        if (this.v != null) {
            long c2 = f.c();
            if (Log.isLoggable("DomobSDK", 3)) {
                Log.d("DomobSDK", "The avg req time is:" + c2);
            }
            if (c2 != -1) {
                DBHelper a2 = DBHelper.a(this.v);
                ContentValues contentValues = new ContentValues();
                contentValues.put("_avg_time", Long.valueOf(c2));
                a2.a(contentValues);
            }
        }
        a(false);
        if (this.c != null) {
            this.c.c();
        }
        f.b();
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public final long a() {
        return this.j;
    }

    public void setPrimaryTextColor(int color) {
        this.j = (long) (-16777216 | color);
        this.x = true;
    }

    /* access modifiers changed from: protected */
    public final long b() {
        return this.i;
    }

    public void setBackgroundColor(int color) {
        this.i = (long) (-16777216 | color);
        invalidate();
        this.w = true;
    }

    public String getKeywords() {
        return this.d;
    }

    public void setKeywords(String s2) {
        if (s2 == null || s2.length() <= 200) {
            this.d = s2;
        } else {
            Log.e("DomobSDK", "The length of keywords cannot exceed 200!");
        }
    }

    public String getSpots() {
        return this.k;
    }

    public void setSpots(String s2) {
        if (s2 == null || s2.length() <= 200) {
            this.k = s2;
        } else {
            Log.e("DomobSDK", "The length of spots cannot exceed 200!");
        }
    }

    public void setVisibility(int v2) {
        if (super.getVisibility() != v2) {
            synchronized (this) {
                int childCount = getChildCount();
                for (int i2 = 0; i2 < childCount; i2++) {
                    getChildAt(i2).setVisibility(v2);
                }
                super.setVisibility(v2);
                invalidate();
            }
        }
        if (Log.isLoggable("DomobSDK", 3)) {
            Log.d("DomobSDK", "setVisibility:" + v2);
        }
        a(v2 == 0);
    }

    /* access modifiers changed from: protected */
    public final void a(boolean z, long j2, long j3) {
        if (this.q != z) {
            this.q = z;
            if (this.q) {
                Log.w("DomobSDK", "AD is disabled on web server.");
                this.r = j2;
                this.s = j3;
            } else {
                this.r = 0;
                this.s = 0;
            }
            if (a != null) {
                a.post(new e(this));
            }
        }
    }

    public void setAdListener(DomobAdListener adlistener) {
        synchronized (this) {
            this.l = adlistener;
        }
    }

    public DomobAdListener getAdListener() {
        return this.l;
    }

    public boolean hasAd() {
        return (this.c == null || this.c.b() == null) ? false : true;
    }

    public void cleanup() {
        if (this.c != null) {
            if (Log.isLoggable("DomobSDK", 3)) {
                Log.d("DomobSDK", "clear the ad.");
            }
            this.c.c();
            this.c = null;
        }
    }

    private static boolean b(Context context) {
        try {
            if (Class.forName("org.json.JSONException") != null) {
                return false;
            }
        } catch (ClassNotFoundException e2) {
            e2.printStackTrace();
        }
        return DomobAdManager.b(context) == null;
    }

    protected static DomobAdBuilder a(DomobAdView domobAdView, DomobAdBuilder domobAdBuilder) {
        domobAdView.c = domobAdBuilder;
        return domobAdBuilder;
    }

    protected static int b(DomobAdView domobAdView) {
        return domobAdView.g;
    }

    protected static DomobAdListener c(DomobAdView domobAdView) {
        return domobAdView.l;
    }

    protected static String d(DomobAdView domobAdView) {
        if (domobAdView.d == null || domobAdView.d.length() <= 200) {
            return domobAdView.d;
        }
        Log.e("DomobSDK", "The length of keywords cannot exceed 200!");
        return null;
    }

    protected static String e(DomobAdView domobAdView) {
        if (domobAdView.k == null || domobAdView.k.length() <= 200) {
            return domobAdView.k;
        }
        Log.e("DomobSDK", "The length of spots cannot exceed 200!");
        return null;
    }

    protected static void b(DomobAdView domobAdView, boolean z) {
        if (domobAdView != null) {
            domobAdView.m = false;
        }
    }

    protected static void c(DomobAdView domobAdView, boolean z) {
        if (domobAdView != null) {
            domobAdView.a(true);
        }
    }

    protected static DomobAdEngine.RecvHandler f(DomobAdView domobAdView) {
        if (domobAdView == null) {
            return null;
        }
        if (domobAdView.e == null) {
            domobAdView.e = new DomobAdEngine.RecvHandler(domobAdView);
        }
        return domobAdView.e;
    }

    protected static void g(DomobAdView domobAdView) {
        if (domobAdView != null && domobAdView.l != null && a != null) {
            Handler handler = a;
            domobAdView.getClass();
            handler.post(new b(domobAdView));
        }
    }

    class b implements Runnable {
        private DomobAdView a;

        public b(DomobAdView domobAdView) {
            this.a = domobAdView;
        }

        public final void run() {
            if (this.a != null) {
                try {
                    DomobAdView.c(this.a).onFailedToReceiveFreshAd(this.a);
                } catch (Exception e) {
                    Log.e("DomobSDK", "Unhandled exception raised in onFailedToReceiveRefreshedAd.");
                    e.printStackTrace();
                }
            }
        }
    }

    protected static void h(DomobAdView domobAdView) {
        if (domobAdView != null && domobAdView.l != null) {
            try {
                domobAdView.l.onReceivedFreshAd(domobAdView);
            } catch (Exception e2) {
                Log.e("DomobSDK", "Unhandled exception raised in onReceivedFreshAd.");
                e2.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(DomobAdEngine domobAdEngine, DomobAdBuilder domobAdBuilder) {
        if (Log.isLoggable("DomobSDK", 3)) {
            Log.d("DomobSDK", "construct ad view");
        }
        if (domobAdEngine == null || domobAdBuilder == null) {
            Log.w("DomobSDK", "failed to construct view!");
            return;
        }
        boolean z = this.p;
        this.p = false;
        domobAdBuilder.a(domobAdEngine);
        int visibility = getVisibility();
        if (Log.isLoggable("DomobSDK", 3)) {
            Log.d("DomobSDK", "this.getVisibility():" + visibility);
        }
        domobAdBuilder.setVisibility(visibility);
        domobAdBuilder.setGravity(17);
        domobAdEngine.a(domobAdBuilder);
        domobAdBuilder.setLayoutParams(new RelativeLayout.LayoutParams(domobAdEngine.a(domobAdEngine.b()), domobAdEngine.a(domobAdEngine.c())));
        a.post(new d(domobAdBuilder, visibility, z));
    }

    class e implements Runnable {
        /* synthetic */ e(DomobAdView domobAdView) {
            this((byte) 0);
        }

        private e(byte b) {
        }

        public final void run() {
            if (DomobAdView.this == null) {
                return;
            }
            if (!DomobAdView.this.q) {
                DomobAdView.this.setVisibility(0);
            } else if (!DomobAdManager.isTestMode(DomobAdView.this.v) || !DomobAdManager.isTestAllowed(DomobAdView.this.v)) {
                DomobAdView.this.setVisibility(8);
            }
        }
    }

    class d implements Runnable {
        private DomobAdView a;
        private DomobAdBuilder b;
        private int c;
        private boolean d;

        public d(DomobAdBuilder domobAdBuilder, int i, boolean z) {
            this.a = DomobAdView.this;
            this.b = domobAdBuilder;
            this.c = i;
            this.d = z;
        }

        public final void run() {
            try {
                if (!(this.a == null || this.b == null)) {
                    if (Log.isLoggable("DomobSDK", 3)) {
                        Log.d("DomobSDK", "show ad!");
                        Log.d("DomobSDK", "WindowVisibility:" + DomobAdView.this.t);
                        Log.d("DomobSDK", "WindowFocus:" + DomobAdView.this.u);
                    }
                    if (DomobAdView.this.t == 0 && DomobAdView.this.u) {
                        this.b.setVisibility(4);
                        this.a.addView(this.b);
                        DomobAdView.h(this.a);
                        if (this.c == 0) {
                            if (this.d) {
                                DomobAdView.b(this.a, this.b);
                                return;
                            }
                            String a2 = this.b.a();
                            if (a2 == null || a2.equals("fr2l")) {
                                DomobAdView.c(this.a, this.b);
                                return;
                            }
                            return;
                        }
                    }
                }
            } catch (Exception e2) {
                Log.e("DomobSDK", "Unhandled exception in showAdThread.run()");
                e2.printStackTrace();
            }
            if (this.b != null) {
                if (Log.isLoggable("DomobSDK", 3)) {
                    Log.d("DomobSDK", "error or view is invisible, clear resources!");
                }
                this.a.removeView(this.b);
                this.b.c();
                this.b = null;
                if (this.d) {
                    if (Log.isLoggable("DomobSDK", 3)) {
                        Log.d("DomobSDK", "reset mNoAd");
                    }
                    DomobAdView.this.p = true;
                }
            }
        }
    }

    protected static void b(DomobAdView domobAdView, DomobAdBuilder domobAdBuilder) {
        if (Log.isLoggable("DomobSDK", 3)) {
            Log.d("DomobSDK", "startAlphaAnimation");
        }
        DomobAdBuilder domobAdBuilder2 = domobAdView.c;
        domobAdBuilder.setVisibility(0);
        domobAdView.c = domobAdBuilder;
        if (domobAdBuilder2 != null) {
            domobAdView.removeView(domobAdBuilder2);
            domobAdBuilder2.c();
        }
        if (domobAdView.n) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(233);
            alphaAnimation.startNow();
            alphaAnimation.setFillAfter(true);
            alphaAnimation.setInterpolator(new AccelerateInterpolator());
            domobAdView.startAnimation(alphaAnimation);
        }
    }

    protected static void c(DomobAdView domobAdView, DomobAdBuilder domobAdBuilder) {
        if (Log.isLoggable("DomobSDK", 3)) {
            Log.d("DomobSDK", "startRotate3dAnimation");
        }
        j jVar = new j(0.0f, -90.0f, ((float) domobAdView.getWidth()) / 2.0f, ((float) domobAdView.getHeight()) / 2.0f, 0.0f, true);
        jVar.setDuration(500);
        jVar.setFillAfter(true);
        jVar.setInterpolator(new AccelerateInterpolator());
        domobAdView.getClass();
        jVar.setAnimationListener(new a(domobAdView, domobAdView, domobAdBuilder));
        domobAdView.startAnimation(jVar);
    }

    class a implements Animation.AnimationListener {
        private DomobAdBuilder a;
        private DomobAdView b;

        a(DomobAdView domobAdView, DomobAdView domobAdView2, DomobAdBuilder domobAdBuilder) {
            this.b = domobAdView2;
            this.a = domobAdBuilder;
        }

        public final void onAnimationStart(Animation animation) {
        }

        public final void onAnimationEnd(Animation animation) {
            if (Log.isLoggable("DomobSDK", 3)) {
                Log.d("DomobSDK", "DomobAdView onAnimationEnd.");
            }
            if (this.a != null) {
                String a2 = this.a.a();
                if (a2 == null || a2.equals("fr2l")) {
                    DomobAdBuilder m = this.b.c;
                    this.a.setVisibility(0);
                    DomobAdView.a(this.b, this.a);
                    if (m != null) {
                        this.b.removeView(m);
                        m.c();
                    }
                    j jVar = new j(90.0f, 0.0f, ((float) this.b.getWidth()) / 2.0f, ((float) this.b.getHeight()) / 2.0f, 0.0f, false);
                    jVar.setDuration(500);
                    jVar.setFillAfter(true);
                    jVar.setInterpolator(new DecelerateInterpolator());
                    this.b.startAnimation(jVar);
                }
            }
        }

        public final void onAnimationRepeat(Animation animation) {
        }
    }

    /* access modifiers changed from: protected */
    public final boolean c() {
        return this.w;
    }

    /* access modifiers changed from: protected */
    public final boolean d() {
        return this.x;
    }
}
