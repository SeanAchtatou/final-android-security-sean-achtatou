package cn.domob.android.ads;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.CycleInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

final class DomobAdEngine implements a {
    private static final int a = Color.rgb(102, 102, 102);
    private Rect b = null;
    private DomobAdBuilder c;
    private float d = -1.0f;
    private boolean e;
    private RecvHandler f = null;
    private e g = null;
    private long h = -1;
    private long i = -8682592;
    private boolean j;
    private boolean k;
    private int l = -1;
    private int m = -1;
    private Vector<View> n = null;
    private Vector<Bitmap> o = new Vector<>();
    private Vector<d> p = new Vector<>();
    private Vector<Intent> q = new Vector<>();
    private HashSet<b> r = new HashSet<>();
    private Hashtable<String, byte[]> s = new Hashtable<>();
    private Hashtable<String, String> t = new Hashtable<>();
    private String u = null;
    private JSONObject v;

    interface e {
        void f();
    }

    class a implements a {
        a(DomobAdEngine domobAdEngine) {
        }

        a() {
        }

        protected static b a(String str, String str2, String str3, String str4, a aVar, int i, String str5) {
            return new k(str, str2, str3, str4, aVar, 30000, null, str5);
        }

        protected static b a(String str, String str2, String str3, String str4, a aVar, String str5) {
            return a(str, str2, str3, str4, aVar, 30000, str5);
        }

        protected static b a(String str, String str2, String str3, String str4, a aVar) {
            return a(str, str2, str3, str4, aVar, 30000, null);
        }

        public void a(b bVar) {
        }
    }

    static class d {
        public String a;

        d() {
        }

        public d(String str, boolean z) {
            this.a = str;
        }
    }

    protected DomobAdEngine() {
    }

    protected static DomobAdEngine a(RecvHandler recvHandler, JSONObject jSONObject, long j2, long j3, DomobAdBuilder domobAdBuilder, boolean z, boolean z2) {
        if (jSONObject == null || jSONObject.length() == 0) {
            Log.e("DomobSDK", "failed to init engine, jsonobject is null!");
            return null;
        }
        if (Log.isLoggable("DomobSDK", 3)) {
            Log.d("DomobSDK", "init engine now.");
        }
        DomobAdEngine domobAdEngine = new DomobAdEngine();
        domobAdEngine.f = recvHandler;
        domobAdEngine.h = j2;
        domobAdEngine.i = j3;
        domobAdEngine.c = domobAdBuilder;
        domobAdEngine.j = z;
        domobAdEngine.k = z2;
        if (!domobAdEngine.c(jSONObject)) {
            return null;
        }
        if (!Log.isLoggable("DomobSDK", 3)) {
            return domobAdEngine;
        }
        Log.d("DomobSDK", "success to parse ad response.");
        return domobAdEngine;
    }

    /* access modifiers changed from: protected */
    public final void a(JSONObject jSONObject) {
        if (this.e) {
            Log.w("DomobSDK", "already clicked, ignore it.");
        } else {
            if (Log.isLoggable("DomobSDK", 3)) {
                Log.d("DomobSDK", "report clicked now.");
            }
            this.e = true;
            if (!(this.c == null || jSONObject == null)) {
                Context context = this.c.getContext();
                Iterator<d> it = this.p.iterator();
                while (it.hasNext()) {
                    d next = it.next();
                    a aVar = new a(this);
                    String a2 = f.a(context, jSONObject.toString());
                    if (Log.isLoggable("DomobSDK", 3)) {
                        Log.d("DomobSDK", "click report:" + a2);
                    }
                    b a3 = a.a(next.a, "click_tracking", f.b(context), DomobAdManager.b(context), aVar, a2);
                    a3.a(context);
                    a3.b();
                }
            }
        }
        g();
        if (this.g != null) {
            if (Log.isLoggable("DomobSDK", 3)) {
                Log.d("DomobSDK", "show click animation.");
            }
            this.g.f();
        }
    }

    private boolean f() {
        boolean z = this.r == null || this.r.size() == 0;
        if (!z && Log.isLoggable("DomobSDK", 3)) {
            Log.d("DomobSDK", "there are " + this.r.size() + " connection left.");
        }
        return z;
    }

    private void g() {
        Context context;
        if (this.c != null && (context = this.c.getContext()) != null) {
            if (Log.isLoggable("DomobSDK", 3)) {
                Log.d("DomobSDK", "do click action now.");
            }
            PackageManager packageManager = context.getPackageManager();
            Iterator<Intent> it = this.q.iterator();
            while (it.hasNext()) {
                Intent next = it.next();
                if (packageManager.resolveActivity(next, 65536) != null) {
                    try {
                        context.startActivity(next);
                        return;
                    } catch (Exception e2) {
                        Log.e("DomobSDK", "error happened in doAction!");
                        e2.printStackTrace();
                    }
                } else {
                    Log.e("DomobSDK", "activity cannot be resolved, intent.getAction() =  " + next.getAction());
                }
            }
        }
    }

    private void b(JSONObject jSONObject) {
        String str;
        String str2;
        String str3;
        if (jSONObject != null) {
            String optString = jSONObject.optString("a", null);
            if (optString == null) {
                Log.e("DomobSDK", "invalid action type!");
                return;
            }
            String optString2 = jSONObject.optString("d", null);
            if (DomobAdManager.ACTION_MAP.equals(optString)) {
                if (optString2 == null) {
                    Log.e("DomobSDK", "map data is null!");
                    return;
                }
                str = "android.intent.action.VIEW";
                str2 = "geo:" + optString2;
                str3 = null;
            } else if (DomobAdManager.ACTION_SMS.equals(optString)) {
                if (optString2 == null) {
                    Log.e("DomobSDK", "smsto is null!");
                    return;
                }
                str = "android.intent.action.SENDTO";
                str2 = "smsto:" + optString2;
                str3 = "sms_body";
            } else if (DomobAdManager.ACTION_MAIL.equals(optString)) {
                if (optString2 == null) {
                    Log.e("DomobSDK", "mailto is null!");
                    return;
                }
                str = "android.intent.action.SENDTO";
                str2 = "mailto:" + optString2;
                str3 = "android.intent.extra.TEXT";
            } else if (DomobAdManager.ACTION_URL.equals(optString) || DomobAdManager.ACTION_MARKET.equals(optString)) {
                if (optString2 == null) {
                    Log.e("DomobSDK", "url is null!");
                    return;
                }
                str = "android.intent.action.VIEW";
                str2 = optString2;
                str3 = null;
            } else if (DomobAdManager.ACTION_CALL.equals(optString)) {
                if (optString2 == null) {
                    Log.e("DomobSDK", "tel number is null!");
                    return;
                }
                str = "android.intent.action.DIAL";
                str2 = "tel:" + optString2;
                str3 = null;
            } else if (!DomobAdManager.ACTION_VIDEO.equals(optString) && !DomobAdManager.ACTION_AUDIO.equals(optString)) {
                Log.e("DomobSDK", "unknown action type!");
                return;
            } else if (optString2 == null) {
                Log.e("DomobSDK", "url is null!");
                return;
            } else {
                str = "android.intent.action.VIEW";
                str2 = optString2;
                str3 = null;
            }
            int optInt = jSONObject.optInt(DomobAdManager.GENDER_FEMALE, 268435456);
            String optString3 = jSONObject.optString("b", null);
            Uri parse = Uri.parse(str2);
            if (parse == null) {
                Log.e("DomobSDK", "intent uri is null");
            } else if (Log.isLoggable("DomobSDK", 3)) {
                Log.d("DomobSDK", "intent uri:" + parse.toString());
            }
            Intent intent = new Intent(str, parse);
            if (optInt != 0) {
                intent.addFlags(optInt);
            }
            if (!(optString3 == null || str3 == null)) {
                intent.putExtra(str3, optString3);
            }
            String optString4 = jSONObject.optString("s", null);
            if (DomobAdManager.ACTION_MAIL.equals(optString) && optString4 != null) {
                intent.putExtra("android.intent.extra.SUBJECT", optString4);
            }
            this.q.add(intent);
        }
    }

    /* access modifiers changed from: protected */
    public final int a(int i2) {
        return (int) (((double) this.d) <= 0.0d ? (float) i2 : ((float) i2) * this.d);
    }

    /* access modifiers changed from: protected */
    public final DomobAdBuilder a() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public final void a(DomobAdBuilder domobAdBuilder) {
        this.c = domobAdBuilder;
        if (this.g == null) {
            this.g = domobAdBuilder;
        }
    }

    /* access modifiers changed from: protected */
    public final int b() {
        return this.l;
    }

    /* access modifiers changed from: protected */
    public final int c() {
        return this.m;
    }

    /* access modifiers changed from: protected */
    public final Rect d() {
        if (this.b == null) {
            this.b = new Rect(0, 0, a(this.l), a(this.m));
        }
        if (Log.isLoggable("DomobSDK", 3)) {
            Log.d("DomobSDK", "getRect :" + this.b.left + "," + this.b.top + "," + this.b.right + "," + this.b.bottom);
        }
        return this.b;
    }

    /* access modifiers changed from: protected */
    public final void e() {
        if (Log.isLoggable("DomobSDK", 3)) {
            Log.d("DomobSDK", "clear engine resources.");
        }
        if (this.q != null) {
            this.q.clear();
            this.q = null;
        }
        if (this.p != null) {
            this.p.clear();
            this.p = null;
        }
        if (this.o != null) {
            Iterator<Bitmap> it = this.o.iterator();
            while (it.hasNext()) {
                Bitmap next = it.next();
                if (next != null) {
                    next.recycle();
                }
            }
            this.o.clear();
            this.o = null;
        }
        if (this.c != null) {
            this.c.getContext().getSystemService("window");
        }
    }

    private boolean c(JSONObject jSONObject) {
        String optString = jSONObject.optString("rp_url", null);
        if (optString == null) {
            Log.e("DomobSDK", "there is no jsonpurl which is required!");
            return false;
        }
        if (Log.isLoggable("DomobSDK", 3)) {
            Log.d("DomobSDK", "parse response now.");
        }
        if (optString != null && !optString.equals("")) {
            this.p.add(new d(optString, false));
        }
        this.u = jSONObject.optString("identifier", null);
        if (this.u == null || this.u.equals("nullad")) {
            Log.e("DomobSDK", "ad identifier is null, ignore it!");
            return false;
        }
        this.d = DomobAdBuilder.d();
        PointF a2 = a(jSONObject, "d", (PointF) null);
        if (a2 == null) {
            a2 = new PointF(320.0f, 48.0f);
        }
        if (a2.x < 0.0f || a2.y < 0.0f) {
            return false;
        }
        this.l = (int) a2.x;
        this.m = (int) a2.y;
        JSONObject optJSONObject = jSONObject.optJSONObject("ac");
        if (optJSONObject != null) {
            b(optJSONObject);
        }
        JSONArray optJSONArray = jSONObject.optJSONArray("ac");
        if (optJSONArray != null) {
            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                try {
                    b(optJSONArray.getJSONObject(i2));
                } catch (JSONException e2) {
                    e2.printStackTrace();
                }
            }
        }
        JSONObject optJSONObject2 = jSONObject.optJSONObject("markup");
        if (optJSONObject2 == null) {
            Log.e("DomobSDK", "there is no markup which is required!");
            return false;
        }
        this.v = optJSONObject2;
        try {
            h();
            i();
            if (f()) {
                j();
            }
            return true;
        } catch (Exception e3) {
            e3.printStackTrace();
            return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x003a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean a(cn.domob.android.ads.DBHelper r11, java.lang.String r12, java.lang.String r13) {
        /*
            r10 = this;
            r8 = 1
            r7 = 3
            r0 = 0
            r1 = 0
            android.net.Uri r2 = cn.domob.android.ads.DBHelper.a     // Catch:{ Exception -> 0x0084 }
            android.database.Cursor r1 = r11.b(r2, r13)     // Catch:{ Exception -> 0x0084 }
            if (r1 == 0) goto L_0x003e
            int r2 = r1.getCount()     // Catch:{ Exception -> 0x0084 }
            if (r2 <= 0) goto L_0x003e
            r1.moveToFirst()     // Catch:{ Exception -> 0x0084 }
            java.lang.String r2 = "_image"
            int r2 = r1.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x0084 }
            byte[] r2 = r1.getBlob(r2)     // Catch:{ Exception -> 0x0084 }
            if (r2 == 0) goto L_0x009d
            java.lang.String r3 = "DomobSDK"
            r4 = 3
            boolean r3 = android.util.Log.isLoggable(r3, r4)     // Catch:{ Exception -> 0x0084 }
            if (r3 == 0) goto L_0x0031
            java.lang.String r3 = "DomobSDK"
            java.lang.String r4 = "success to load from resources DB."
            android.util.Log.d(r3, r4)     // Catch:{ Exception -> 0x0084 }
        L_0x0031:
            java.util.Hashtable<java.lang.String, byte[]> r3 = r10.s     // Catch:{ Exception -> 0x0084 }
            r3.put(r12, r2)     // Catch:{ Exception -> 0x0084 }
            r0 = r1
            r1 = r8
        L_0x0038:
            if (r0 == 0) goto L_0x003d
            r0.close()
        L_0x003d:
            return r1
        L_0x003e:
            android.content.Context r2 = r11.a()     // Catch:{ Exception -> 0x0084 }
            if (r2 == 0) goto L_0x009d
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0084 }
            r3.<init>()     // Catch:{ Exception -> 0x0084 }
            android.content.res.AssetManager r2 = r2.getAssets()     // Catch:{ Exception -> 0x0084 }
            java.io.InputStream r2 = r2.open(r13)     // Catch:{ Exception -> 0x0084 }
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeStream(r2)     // Catch:{ Exception -> 0x0084 }
            android.graphics.Bitmap$CompressFormat r4 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ Exception -> 0x0084 }
            r5 = 100
            r2.compress(r4, r5, r3)     // Catch:{ Exception -> 0x0084 }
            byte[] r4 = r3.toByteArray()     // Catch:{ Exception -> 0x0084 }
            if (r4 == 0) goto L_0x0078
            java.lang.String r5 = "DomobSDK"
            r6 = 3
            boolean r5 = android.util.Log.isLoggable(r5, r6)     // Catch:{ Exception -> 0x0084 }
            if (r5 == 0) goto L_0x0072
            java.lang.String r5 = "DomobSDK"
            java.lang.String r6 = "success to load from preload resources."
            android.util.Log.d(r5, r6)     // Catch:{ Exception -> 0x0084 }
        L_0x0072:
            java.util.Hashtable<java.lang.String, byte[]> r5 = r10.s     // Catch:{ Exception -> 0x0084 }
            r5.put(r12, r4)     // Catch:{ Exception -> 0x0084 }
            r0 = r8
        L_0x0078:
            r3.close()     // Catch:{ Exception -> 0x0084 }
            if (r2 == 0) goto L_0x009d
            r2.recycle()     // Catch:{ Exception -> 0x0084 }
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0038
        L_0x0084:
            r2 = move-exception
            r9 = r2
            r2 = r0
            r0 = r9
            java.lang.String r3 = "DomobSDK"
            java.lang.String r4 = "cannot load it from res this time."
            android.util.Log.i(r3, r4)
            java.lang.String r3 = "DomobSDK"
            boolean r3 = android.util.Log.isLoggable(r3, r7)
            if (r3 == 0) goto L_0x009a
            r0.printStackTrace()
        L_0x009a:
            r0 = r1
            r1 = r2
            goto L_0x0038
        L_0x009d:
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.domob.android.ads.DomobAdEngine.a(cn.domob.android.ads.DBHelper, java.lang.String, java.lang.String):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0047  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean b(cn.domob.android.ads.DBHelper r8, java.lang.String r9, java.lang.String r10) {
        /*
            r7 = this;
            r0 = 0
            r1 = 0
            android.net.Uri r2 = cn.domob.android.ads.DBHelper.b     // Catch:{ Exception -> 0x0072 }
            android.database.Cursor r1 = r8.b(r2, r10)     // Catch:{ Exception -> 0x0072 }
            if (r1 == 0) goto L_0x004b
            int r2 = r1.getCount()     // Catch:{ Exception -> 0x007c }
            if (r2 <= 0) goto L_0x004b
            r1.moveToFirst()     // Catch:{ Exception -> 0x007c }
            java.lang.String r2 = "_image"
            int r2 = r1.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x007c }
            byte[] r2 = r1.getBlob(r2)     // Catch:{ Exception -> 0x007c }
            if (r2 == 0) goto L_0x0081
            java.lang.String r3 = "DomobSDK"
            r4 = 3
            boolean r3 = android.util.Log.isLoggable(r3, r4)     // Catch:{ Exception -> 0x007c }
            if (r3 == 0) goto L_0x003c
            java.lang.String r3 = "DomobSDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x007c }
            java.lang.String r5 = "load image from cache:"
            r4.<init>(r5)     // Catch:{ Exception -> 0x007c }
            java.lang.StringBuilder r4 = r4.append(r10)     // Catch:{ Exception -> 0x007c }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x007c }
            android.util.Log.d(r3, r4)     // Catch:{ Exception -> 0x007c }
        L_0x003c:
            java.util.Hashtable<java.lang.String, byte[]> r3 = r7.s     // Catch:{ Exception -> 0x007c }
            r3.put(r9, r2)     // Catch:{ Exception -> 0x007c }
            r0 = 1
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x0045:
            if (r0 == 0) goto L_0x004a
            r0.close()
        L_0x004a:
            return r1
        L_0x004b:
            java.lang.String r2 = "DomobSDK"
            r3 = 3
            boolean r2 = android.util.Log.isLoggable(r2, r3)     // Catch:{ Exception -> 0x007c }
            if (r2 == 0) goto L_0x0081
            java.lang.String r2 = "DomobSDK"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x007c }
            java.lang.String r4 = "fail to load image:"
            r3.<init>(r4)     // Catch:{ Exception -> 0x007c }
            java.lang.StringBuilder r3 = r3.append(r10)     // Catch:{ Exception -> 0x007c }
            java.lang.String r4 = " from cache"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x007c }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x007c }
            android.util.Log.d(r2, r3)     // Catch:{ Exception -> 0x007c }
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0045
        L_0x0072:
            r2 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
        L_0x0076:
            r1.printStackTrace()
            r1 = r0
            r0 = r2
            goto L_0x0045
        L_0x007c:
            r2 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
            goto L_0x0076
        L_0x0081:
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0045
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.domob.android.ads.DomobAdEngine.b(cn.domob.android.ads.DBHelper, java.lang.String, java.lang.String):boolean");
    }

    private void h() throws JSONException {
        JSONObject optJSONObject = this.v.optJSONObject("$");
        if (optJSONObject != null) {
            Iterator<String> keys = optJSONObject.keys();
            if (this.r != null) {
                synchronized (this.r) {
                    if (this.c != null) {
                        Context context = this.c.getContext();
                        DBHelper a2 = DBHelper.a(context);
                        while (keys.hasNext()) {
                            String next = keys.next();
                            String optString = optJSONObject.getJSONObject(next).optString("u", null);
                            if (!(next == null || optString == null)) {
                                int lastIndexOf = optString.lastIndexOf(47);
                                if (lastIndexOf > 0 && lastIndexOf + 1 < optString.length()) {
                                    String substring = optString.substring(lastIndexOf + 1);
                                    if (Log.isLoggable("DomobSDK", 3)) {
                                        Log.d("DomobSDK", "image name:" + substring);
                                    }
                                    this.t.put(next, substring);
                                    if (next.startsWith("def_")) {
                                        if (a(a2, next, substring)) {
                                            if (Log.isLoggable("DomobSDK", 3)) {
                                                Log.d("DomobSDK", "load " + substring + " from resources.");
                                            }
                                        }
                                    } else if (b(a2, next, substring)) {
                                        if (Log.isLoggable("DomobSDK", 3)) {
                                            Log.d("DomobSDK", "load " + substring + " from cache.");
                                        }
                                    }
                                }
                                if (Log.isLoggable("DomobSDK", 3)) {
                                    Log.d("DomobSDK", "need download " + next + " from server " + optString);
                                }
                                b a3 = a.a(optString, next, f.b(context), DomobAdManager.b(context), this);
                                a3.a(context);
                                this.r.add(a3);
                            }
                        }
                    }
                }
            }
        }
    }

    private void i() {
        if (this.r != null) {
            synchronized (this.r) {
                Iterator<b> it = this.r.iterator();
                while (it.hasNext()) {
                    it.next().b();
                }
            }
        }
    }

    private void j() {
        g gVar;
        String str;
        Typeface typeface;
        int i2;
        if (this.v == null) {
            Log.e("DomobSDK", "can not build view without markup!");
            return;
        }
        boolean z = true;
        try {
            if (Log.isLoggable("DomobSDK", 3)) {
                Log.d("DomobSDK", "build view with markup.");
            }
            JSONArray jSONArray = this.v.getJSONArray("v");
            if (jSONArray != null) {
                b bVar = new b(this.c, this);
                int i3 = 0;
                while (true) {
                    if (i3 >= jSONArray.length()) {
                        break;
                    }
                    JSONObject jSONObject = jSONArray.getJSONObject(i3);
                    Vector vector = null;
                    String str2 = null;
                    String string = jSONObject.getString("t");
                    Rect a2 = a(jSONObject, DomobAdManager.GENDER_FEMALE, this.b);
                    if ("l".equals(string)) {
                        if (Log.isLoggable("DomobSDK", 3)) {
                            Log.d("DomobSDK", "build text view.");
                        }
                        if (this.c != null) {
                            float a3 = a(jSONObject, "fs", 14.0f);
                            JSONArray optJSONArray = jSONObject.optJSONArray("fa");
                            JSONArray optJSONArray2 = jSONObject.optJSONArray("x");
                            Typeface typeface2 = Typeface.DEFAULT;
                            if (optJSONArray != null) {
                                int i4 = 0;
                                for (int i5 = 0; i5 < optJSONArray.length(); i5++) {
                                    String string2 = optJSONArray.getString(i5);
                                    if (Log.isLoggable("DomobSDK", 3)) {
                                        Log.d("DomobSDK", "fa:[" + i5 + "] = " + string2);
                                    }
                                    if ("b".equals(string2)) {
                                        i4 |= 1;
                                    } else if ("i".equals(string2)) {
                                        i4 |= 2;
                                    } else if (DomobAdManager.GENDER_MALE.equals(string2)) {
                                        typeface2 = Typeface.MONOSPACE;
                                    } else if ("s".equals(string2)) {
                                        typeface2 = Typeface.SERIF;
                                    } else if ("ss".equals(string2)) {
                                        typeface2 = Typeface.SANS_SERIF;
                                    }
                                }
                                typeface = Typeface.create(typeface2, i4);
                            } else {
                                typeface = typeface2;
                            }
                            String optString = jSONObject.optString("fc", null);
                            int i6 = (int) this.h;
                            if (optString == null || this.k) {
                                i2 = i6;
                            } else {
                                i2 = (int) Long.parseLong(optString, 16);
                            }
                            boolean optBoolean = jSONObject.optBoolean("dnv", true);
                            float a4 = a(jSONObject, "mfs", 12.0f);
                            if (jSONObject.has("at")) {
                                str2 = jSONObject.getString("at");
                            }
                            if (optJSONArray2 != null) {
                                if (optJSONArray2.length() > 1) {
                                    vector = new Vector();
                                }
                                g gVar2 = null;
                                for (int i7 = 0; i7 < optJSONArray2.length(); i7++) {
                                    String string3 = optJSONArray2.getString(i7);
                                    g gVar3 = new g(this.c.getContext(), DomobAdBuilder.d());
                                    gVar3.b = optBoolean;
                                    gVar3.a = gVar3.c * a4;
                                    gVar3.setText(string3);
                                    gVar3.setTextColor(i2);
                                    gVar3.setTextSize(1, a3);
                                    gVar3.setTypeface(typeface);
                                    gVar3.setSingleLine();
                                    gVar3.setEllipsize(TextUtils.TruncateAt.END);
                                    if (vector != null) {
                                        vector.add(gVar3);
                                    } else {
                                        gVar2 = gVar3;
                                    }
                                }
                                str = str2;
                                gVar = gVar2;
                            } else {
                                String str3 = str2;
                                gVar = null;
                                str = str3;
                            }
                        }
                        gVar = null;
                        str = null;
                    } else if ("bg".equals(string)) {
                        if (Log.isLoggable("DomobSDK", 3)) {
                            Log.d("DomobSDK", "build background view.");
                        }
                        gVar = a(jSONObject, a2);
                        str = null;
                    } else {
                        if ("i".equals(string)) {
                            if (Log.isLoggable("DomobSDK", 3)) {
                                Log.d("DomobSDK", "build image view.");
                            }
                            gVar = d(jSONObject);
                            str = null;
                        }
                        gVar = null;
                        str = null;
                    }
                    if (gVar == null && vector == null) {
                        Log.d("DomobSDK", "failed to build view");
                        z = false;
                    } else {
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(a2.width(), a2.height());
                        layoutParams.addRule(9);
                        layoutParams.addRule(10);
                        layoutParams.setMargins(a2.left, a2.top, 0, 0);
                        if (vector != null) {
                            Iterator it = vector.iterator();
                            while (it.hasNext()) {
                                bVar.a.add(a((View) it.next(), layoutParams, str));
                            }
                        } else if (gVar != null) {
                            bVar.a.add(a(gVar, layoutParams, str));
                            if (jSONObject.optBoolean("cav") && this.c != null) {
                                this.c.a(gVar);
                            }
                        }
                        z = true;
                    }
                    if (!z) {
                        Log.e("DomobSDK", "failed to build view, callback!");
                        if (this.f != null) {
                            this.f.failed();
                        }
                    } else {
                        i3++;
                    }
                }
                if (z) {
                    if (this.c != null) {
                        this.c.a(this.v.optString("tat", null));
                    }
                    DomobAdView.a.post(bVar);
                }
            } else if (this.f != null) {
                Log.e("DomobSDK", "can not build view because jason array of views is null!");
                this.f.failed();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            z = false;
        }
        if (!z) {
            Log.d("DomobSDK", "failed to build view, clear all bmps and views!");
            e();
        }
        if (this.r != null) {
            this.r.clear();
            this.r = null;
        }
        if (this.s != null) {
            this.s.clear();
            this.s = null;
        }
        if (this.t != null) {
            this.t.clear();
            this.t = null;
        }
    }

    private static c a(View view, RelativeLayout.LayoutParams layoutParams, String str) {
        c cVar = new c();
        cVar.a = view;
        cVar.b = layoutParams;
        cVar.c = str;
        return cVar;
    }

    private View a(JSONObject jSONObject, Rect rect) throws JSONException {
        if (this.c != null) {
            try {
                float a2 = a(jSONObject, "ia", 1.0f);
                float a3 = a(jSONObject, "epy", 1.0f);
                int i2 = (int) this.i;
                String optString = jSONObject.optString("bc", null);
                if (optString != null && !optString.equals("") && !this.j) {
                    i2 = (int) Long.parseLong(optString, 16);
                    if (Log.isLoggable("DomobSDK", 3)) {
                        Log.d("DomobSDK", "bgcolor is:" + i2);
                    }
                }
                Bitmap createBitmap = Bitmap.createBitmap(rect.width(), rect.height(), Bitmap.Config.ARGB_8888);
                this.o.add(createBitmap);
                Canvas canvas = new Canvas(createBitmap);
                int height = rect.top + ((int) (a3 * ((float) rect.height())));
                Rect rect2 = new Rect(rect.left, rect.top, rect.right, height);
                Paint paint = new Paint();
                paint.setColor(-1);
                paint.setStyle(Paint.Style.FILL);
                canvas.drawRect(rect2, paint);
                GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{Color.argb((int) (a2 * 255.0f), Color.red(i2), Color.green(i2), Color.blue(i2)), i2});
                gradientDrawable.setBounds(rect2);
                gradientDrawable.draw(canvas);
                Rect rect3 = new Rect(rect.left, height, rect.right, rect.bottom);
                Paint paint2 = new Paint();
                paint2.setColor(i2);
                paint2.setStyle(Paint.Style.FILL);
                canvas.drawRect(rect3, paint2);
                View view = new View(this.c.getContext());
                view.setBackgroundDrawable(new BitmapDrawable(createBitmap));
                return view;
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        return null;
    }

    private View d(JSONObject jSONObject) throws JSONException {
        ImageView imageView = null;
        if (this.c != null) {
            DBHelper a2 = DBHelper.a(this.c.getContext());
            String string = jSONObject.getString("$");
            if (string != null) {
                if (string.startsWith("def_")) {
                    if (Log.isLoggable("DomobSDK", 3)) {
                        Log.d("DomobSDK", "load " + string + " from resources.");
                    }
                    if (!a(a2, string, String.valueOf(string) + ".png")) {
                        if (Log.isLoggable("DomobSDK", 3)) {
                            Log.d("DomobSDK", "failed to load image from resources, try the backup.");
                        }
                        String optString = jSONObject.optString("def", null);
                        if (optString == null) {
                            Log.e("DomobSDK", "no defined backup default resources!");
                            return null;
                        } else if (!a(a2, string, String.valueOf(optString) + ".png")) {
                            Log.e("DomobSDK", "failed to load backup default resources!");
                            return null;
                        }
                    }
                }
                byte[] bArr = this.s.get(string);
                if (bArr != null) {
                    try {
                        Bitmap decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
                        if (decodeByteArray == null) {
                            Log.e("DomobSDK", "failed to decode Bitmap!");
                            return null;
                        }
                        ImageView imageView2 = new ImageView(this.c.getContext());
                        try {
                            imageView2.setScaleType(ImageView.ScaleType.FIT_XY);
                            if (jSONObject.optBoolean("b", false)) {
                                float a3 = a(jSONObject, "bw", 0.5f);
                                int a4 = (int) a(jSONObject, "bdc", (float) a);
                                float a5 = a(jSONObject, "br", 6.5f);
                                if (a3 < 1.0f) {
                                    a3 = 1.0f;
                                }
                                int width = decodeByteArray.getWidth();
                                int height = decodeByteArray.getHeight();
                                if (Log.isLoggable("DomobSDK", 3)) {
                                    Log.d("DomobSDK", "img size(widthxhight):" + width + "x" + height);
                                }
                                Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                                createBitmap.eraseColor(0);
                                this.o.add(createBitmap);
                                Canvas canvas = new Canvas(createBitmap);
                                canvas.setDrawFilter(new PaintFlagsDrawFilter(0, 1));
                                float f2 = a3 + a5;
                                Path path = new Path();
                                RectF rectF = new RectF(0.0f, 0.0f, (float) width, (float) height);
                                path.addRoundRect(rectF, f2, f2, Path.Direction.CCW);
                                canvas.clipPath(path, Region.Op.REPLACE);
                                canvas.drawBitmap(decodeByteArray, 0.0f, 0.0f, new Paint(3));
                                canvas.clipRect(rectF, Region.Op.REPLACE);
                                Paint paint = new Paint(1);
                                paint.setStrokeWidth(a3);
                                paint.setColor(a4);
                                paint.setStyle(Paint.Style.STROKE);
                                float f3 = a3 / 2.0f;
                                Path path2 = new Path();
                                path2.addRoundRect(new RectF(f3, f3, ((float) width) - f3, ((float) height) - f3), a5, a5, Path.Direction.CCW);
                                canvas.drawPath(path2, paint);
                                if (decodeByteArray != null) {
                                    decodeByteArray.recycle();
                                }
                                imageView2.setImageBitmap(createBitmap);
                                return imageView2;
                            }
                            this.o.add(decodeByteArray);
                            imageView2.setImageBitmap(decodeByteArray);
                            return imageView2;
                        } catch (Throwable th) {
                            th = th;
                            imageView = imageView2;
                            th.printStackTrace();
                            return imageView;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        th.printStackTrace();
                        return imageView;
                    }
                }
            } else {
                Log.e("DomobSDK", "can not create an imageView without $");
            }
        }
        return null;
    }

    private static float a(JSONObject jSONObject, String str, float f2) {
        return (float) jSONObject.optDouble(str, (double) f2);
    }

    private Rect a(JSONObject jSONObject, String str, Rect rect) {
        Rect rect2;
        if (jSONObject == null || !jSONObject.has(str)) {
            return rect;
        }
        try {
            JSONArray jSONArray = jSONObject.getJSONArray(str);
            int i2 = (int) jSONArray.getDouble(0);
            int i3 = (int) jSONArray.getDouble(1);
            Rect rect3 = new Rect(a(i2), a(i3), a(((int) jSONArray.getDouble(2)) + i2), a(((int) jSONArray.getDouble(3)) + i3));
            try {
                if (!Log.isLoggable("DomobSDK", 3)) {
                    return rect3;
                }
                Log.d("DomobSDK", "getRect :" + rect3.left + "," + rect3.top + "," + rect3.right + "," + rect3.bottom);
                return rect3;
            } catch (JSONException e2) {
                e = e2;
                rect2 = rect3;
                e.printStackTrace();
                return rect2;
            }
        } catch (JSONException e3) {
            e = e3;
            rect2 = rect;
            e.printStackTrace();
            return rect2;
        }
    }

    private static PointF a(JSONObject jSONObject, String str, PointF pointF) {
        if (jSONObject != null && jSONObject.has(str)) {
            try {
                JSONArray jSONArray = jSONObject.getJSONArray(str);
                return new PointF((float) jSONArray.getDouble(0), (float) jSONArray.getDouble(1));
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
        return pointF;
    }

    static class b implements Runnable {
        Vector<c> a = new Vector<>();
        private DomobAdBuilder b;
        private DomobAdEngine c;

        public b(DomobAdBuilder domobAdBuilder, DomobAdEngine domobAdEngine) {
            this.b = domobAdBuilder;
            this.c = domobAdEngine;
        }

        public final void run() {
            h hVar;
            AnimationSet animationSet;
            try {
                if (Log.isLoggable("DomobSDK", 3)) {
                    Log.d("DomobSDK", "AddViewThread run");
                }
                if (this.b != null) {
                    this.b.setPadding(0, 0, 0, 0);
                    Iterator<c> it = this.a.iterator();
                    RelativeLayout.LayoutParams layoutParams = null;
                    h hVar2 = null;
                    while (it.hasNext()) {
                        c next = it.next();
                        if (next.a == null || next.b == null) {
                            if (Log.isLoggable("DomobSDK", 3)) {
                                Log.d("DomobSDK", "Basic View element is null, continue.");
                            }
                        } else if (next.c != null) {
                            if (hVar2 == null) {
                                hVar = new h(this.b.getContext());
                                if ("l2r".equals(next.c)) {
                                    TranslateAnimation translateAnimation = new TranslateAnimation(-100.0f, 0.0f, 0.0f, 0.0f);
                                    translateAnimation.setDuration(3000);
                                    translateAnimation.setInterpolator(new AccelerateInterpolator());
                                    translateAnimation.setFillAfter(true);
                                    AnimationSet animationSet2 = new AnimationSet(true);
                                    animationSet2.addAnimation(translateAnimation);
                                    animationSet = animationSet2;
                                } else if ("r2l".equals(next.c)) {
                                    TranslateAnimation translateAnimation2 = new TranslateAnimation(100.0f, 0.0f, 0.0f, 0.0f);
                                    translateAnimation2.setDuration(3000);
                                    translateAnimation2.setInterpolator(new AccelerateInterpolator());
                                    translateAnimation2.setFillAfter(true);
                                    AnimationSet animationSet3 = new AnimationSet(true);
                                    animationSet3.addAnimation(translateAnimation2);
                                    animationSet = animationSet3;
                                } else if ("t2b".equals(next.c)) {
                                    TranslateAnimation translateAnimation3 = new TranslateAnimation(0.0f, 0.0f, -50.0f, 0.0f);
                                    translateAnimation3.setDuration(2500);
                                    translateAnimation3.setInterpolator(new AccelerateInterpolator());
                                    translateAnimation3.setFillAfter(true);
                                    AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
                                    alphaAnimation.setDuration(500);
                                    alphaAnimation.setInterpolator(new AccelerateInterpolator());
                                    alphaAnimation.setFillAfter(true);
                                    AnimationSet animationSet4 = new AnimationSet(true);
                                    animationSet4.addAnimation(translateAnimation3);
                                    animationSet4.addAnimation(alphaAnimation);
                                    animationSet = animationSet4;
                                } else if ("b2t".equals(next.c)) {
                                    TranslateAnimation translateAnimation4 = new TranslateAnimation(0.0f, 0.0f, 50.0f, 0.0f);
                                    translateAnimation4.setDuration(2500);
                                    translateAnimation4.setInterpolator(new AccelerateInterpolator());
                                    translateAnimation4.setFillAfter(true);
                                    AlphaAnimation alphaAnimation2 = new AlphaAnimation(0.0f, 1.0f);
                                    alphaAnimation2.setDuration(500);
                                    alphaAnimation2.setInterpolator(new AccelerateInterpolator());
                                    alphaAnimation2.setFillAfter(true);
                                    AnimationSet animationSet5 = new AnimationSet(true);
                                    animationSet5.addAnimation(translateAnimation4);
                                    animationSet5.addAnimation(alphaAnimation2);
                                    animationSet = animationSet5;
                                } else if ("shx".equals(next.c)) {
                                    TranslateAnimation translateAnimation5 = new TranslateAnimation(30.0f, 50.0f, 0.0f, 0.0f);
                                    translateAnimation5.setDuration(3000);
                                    translateAnimation5.setInterpolator(new CycleInterpolator(2.0f));
                                    translateAnimation5.setFillAfter(true);
                                    AnimationSet animationSet6 = new AnimationSet(false);
                                    animationSet6.addAnimation(translateAnimation5);
                                    animationSet = animationSet6;
                                } else if ("shy".equals(next.c)) {
                                    TranslateAnimation translateAnimation6 = new TranslateAnimation(0.0f, 0.0f, -3.0f, 10.0f);
                                    translateAnimation6.setDuration(3000);
                                    translateAnimation6.setInterpolator(new CycleInterpolator(2.0f));
                                    translateAnimation6.setFillAfter(true);
                                    AnimationSet animationSet7 = new AnimationSet(false);
                                    animationSet7.addAnimation(translateAnimation6);
                                    animationSet = animationSet7;
                                } else {
                                    animationSet = null;
                                }
                                hVar.setInAnimation(animationSet);
                            } else {
                                hVar = hVar2;
                            }
                            hVar.addView(next.a, next.b);
                            hVar2 = hVar;
                            layoutParams = next.b;
                        } else {
                            this.b.addView(next.a, next.b);
                        }
                    }
                    if (hVar2 != null) {
                        if (hVar2.getChildCount() > 1) {
                            hVar2.setFlipInterval(10000);
                        } else {
                            hVar2.setFlipInterval(600000);
                        }
                        hVar2.startFlipping();
                        this.b.addView(hVar2, layoutParams);
                    }
                    this.b.invalidate();
                    this.b.requestLayout();
                }
                if (this.c != null) {
                    DomobAdEngine.a(this.c);
                }
            } catch (Exception e) {
                Log.e("DomobSDK", "failed to add view into builder!");
                e.printStackTrace();
                if (this.c != null) {
                    this.c.e();
                }
            }
            if (this.a != null) {
                this.a.clear();
            }
        }
    }

    static void a(DomobAdEngine domobAdEngine) {
        if (domobAdEngine.f != null) {
            domobAdEngine.f.received(domobAdEngine);
        }
    }

    public static class RecvHandler {
        private DomobAdView a = null;

        public RecvHandler(DomobAdView domobAdView) {
            this.a = domobAdView;
        }

        public void failed() {
            if (this.a != null) {
                DomobAdView.g(this.a);
            }
        }

        public void received(DomobAdEngine engine) {
            if (this.a != null) {
                synchronized (this.a) {
                    if (Log.isLoggable("DomobSDK", 3)) {
                        Log.d("DomobSDK", "new ad, construct it.");
                    }
                    this.a.a(engine, engine.a());
                }
            }
        }
    }

    static class c {
        public View a;
        public RelativeLayout.LayoutParams b;
        public String c;

        c() {
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00a9  */
    /* JADX WARNING: Removed duplicated region for block: B:52:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(cn.domob.android.ads.b r8) {
        /*
            r7 = this;
            r2 = 3
            java.lang.String r0 = r8.c()
            byte[] r1 = r8.d()
            if (r1 == 0) goto L_0x00d7
            java.util.Hashtable<java.lang.String, byte[]> r2 = r7.s
            r2.put(r0, r1)
            cn.domob.android.ads.DomobAdBuilder r2 = r7.c
            if (r2 == 0) goto L_0x0037
            java.util.Hashtable<java.lang.String, java.lang.String> r2 = r7.t
            java.lang.Object r0 = r2.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            if (r0 == 0) goto L_0x0037
            cn.domob.android.ads.DomobAdBuilder r2 = r7.c
            android.content.Context r2 = r2.getContext()
            cn.domob.android.ads.DBHelper r2 = cn.domob.android.ads.DBHelper.a(r2)
            java.lang.String r3 = "def_"
            boolean r3 = r0.startsWith(r3)
            if (r3 == 0) goto L_0x004e
            long r3 = java.lang.System.currentTimeMillis()
            cn.domob.android.ads.DBHelper.a(r2, r0, r1, r3)
        L_0x0037:
            java.util.HashSet<cn.domob.android.ads.b> r0 = r7.r
            if (r0 == 0) goto L_0x0044
            java.util.HashSet<cn.domob.android.ads.b> r0 = r7.r
            monitor-enter(r0)
            java.util.HashSet<cn.domob.android.ads.b> r1 = r7.r     // Catch:{ all -> 0x00ff }
            r1.remove(r8)     // Catch:{ all -> 0x00ff }
            monitor-exit(r0)     // Catch:{ all -> 0x00ff }
        L_0x0044:
            boolean r0 = r7.f()
            if (r0 == 0) goto L_0x004d
            r7.j()
        L_0x004d:
            return
        L_0x004e:
            r3 = 0
            android.net.Uri r4 = cn.domob.android.ads.DBHelper.b     // Catch:{ Exception -> 0x00d0 }
            android.database.Cursor r3 = r2.b(r4, r0)     // Catch:{ Exception -> 0x00d0 }
            if (r3 == 0) goto L_0x005d
            int r4 = r3.getCount()     // Catch:{ Exception -> 0x0102 }
            if (r4 != 0) goto L_0x00ad
        L_0x005d:
            android.net.Uri r4 = cn.domob.android.ads.DBHelper.b     // Catch:{ Exception -> 0x0102 }
            r2.a(r4)     // Catch:{ Exception -> 0x0102 }
            java.lang.String r4 = "DomobSDK"
            r5 = 3
            boolean r4 = android.util.Log.isLoggable(r4, r5)     // Catch:{ Exception -> 0x0102 }
            if (r4 == 0) goto L_0x0085
            java.lang.String r4 = "DomobSDK"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0102 }
            java.lang.String r6 = "save image:"
            r5.<init>(r6)     // Catch:{ Exception -> 0x0102 }
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ Exception -> 0x0102 }
            java.lang.String r6 = " to cache!"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0102 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0102 }
            android.util.Log.d(r4, r5)     // Catch:{ Exception -> 0x0102 }
        L_0x0085:
            android.content.ContentValues r4 = new android.content.ContentValues     // Catch:{ Exception -> 0x0102 }
            r4.<init>()     // Catch:{ Exception -> 0x0102 }
            java.lang.String r5 = "_name"
            r4.put(r5, r0)     // Catch:{ Exception -> 0x0102 }
            java.lang.String r0 = "_image"
            r4.put(r0, r1)     // Catch:{ Exception -> 0x0102 }
            java.lang.String r0 = "_date"
            long r5 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0102 }
            java.lang.Long r1 = java.lang.Long.valueOf(r5)     // Catch:{ Exception -> 0x0102 }
            r4.put(r0, r1)     // Catch:{ Exception -> 0x0102 }
            android.net.Uri r0 = cn.domob.android.ads.DBHelper.b     // Catch:{ Exception -> 0x0102 }
            r2.a(r0, r4)     // Catch:{ Exception -> 0x0102 }
            r0 = r3
        L_0x00a7:
            if (r0 == 0) goto L_0x0037
            r0.close()
            goto L_0x0037
        L_0x00ad:
            java.lang.String r1 = "DomobSDK"
            r2 = 3
            boolean r1 = android.util.Log.isLoggable(r1, r2)     // Catch:{ Exception -> 0x0102 }
            if (r1 == 0) goto L_0x0105
            java.lang.String r1 = "DomobSDK"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0102 }
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ Exception -> 0x0102 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x0102 }
            java.lang.String r0 = " is already in cache."
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Exception -> 0x0102 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0102 }
            android.util.Log.d(r1, r0)     // Catch:{ Exception -> 0x0102 }
            r0 = r3
            goto L_0x00a7
        L_0x00d0:
            r0 = move-exception
            r1 = r3
        L_0x00d2:
            r0.printStackTrace()
            r0 = r1
            goto L_0x00a7
        L_0x00d7:
            java.lang.String r1 = "DomobSDK"
            boolean r1 = android.util.Log.isLoggable(r1, r2)
            if (r1 == 0) goto L_0x0037
            java.lang.String r1 = "DomobSDK"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Failed reading asset("
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r2 = ") for ad"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            android.util.Log.d(r1, r0)
            goto L_0x0037
        L_0x00ff:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        L_0x0102:
            r0 = move-exception
            r1 = r3
            goto L_0x00d2
        L_0x0105:
            r0 = r3
            goto L_0x00a7
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.domob.android.ads.DomobAdEngine.a(cn.domob.android.ads.b):void");
    }
}
