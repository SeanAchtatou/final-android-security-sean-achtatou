package cn.domob.android.ads;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import cn.domob.android.ads.DomobAdEngine;
import java.net.URLEncoder;
import java.util.Locale;
import org.json.JSONObject;
import org.json.JSONTokener;

final class f {
    private static String a = "http://r.domob.cn/a/";
    private static int b = 0;
    private static String c = null;
    private static long d = 0;
    private static long e = 0;
    private static boolean f = false;
    private b g;

    protected f() {
        if (Log.isLoggable("DomobSDK", 3)) {
            Log.d("DomobSDK", "AD Url:" + a + " | Req Count:" + b);
        }
        this.g = null;
    }

    /* access modifiers changed from: protected */
    public final b a() {
        return this.g;
    }

    protected static void b() {
        a = "http://r.domob.cn/a/";
        b = 0;
        d = 0;
        e = 0;
        f = false;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x01d7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final cn.domob.android.ads.DomobAdEngine a(cn.domob.android.ads.DomobAdEngine.RecvHandler r16, android.content.Context r17, java.lang.String r18, java.lang.String r19, long r20, long r22, cn.domob.android.ads.DomobAdBuilder r24, int r25, int r26, boolean r27, boolean r28) {
        /*
            r15 = this;
            cn.domob.android.ads.DomobAdManager.a(r17)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "1"
            r0 = r17
            r1 = r3
            r2 = r4
            a(r0, r1, r2)
            long r4 = java.lang.System.currentTimeMillis()
            java.lang.String r6 = "ts"
            java.lang.String r7 = java.lang.String.valueOf(r4)
            a(r3, r6, r7)
            java.lang.String r6 = "so"
            java.lang.String r7 = cn.domob.android.ads.DomobAdManager.d(r17)
            a(r3, r6, r7)
            if (r25 <= 0) goto L_0x0052
            java.lang.String r6 = "DomobSDK"
            r7 = 3
            boolean r6 = android.util.Log.isLoggable(r6, r7)
            if (r6 == 0) goto L_0x0049
            java.lang.String r6 = "DomobSDK"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r8 = "handset screen width is "
            r7.<init>(r8)
            r0 = r7
            r1 = r25
            java.lang.StringBuilder r7 = r0.append(r1)
            java.lang.String r7 = r7.toString()
            android.util.Log.d(r6, r7)
        L_0x0049:
            java.lang.String r6 = "sw"
            java.lang.String r7 = java.lang.String.valueOf(r25)
            a(r3, r6, r7)
        L_0x0052:
            if (r26 <= 0) goto L_0x007d
            java.lang.String r6 = "DomobSDK"
            r7 = 3
            boolean r6 = android.util.Log.isLoggable(r6, r7)
            if (r6 == 0) goto L_0x0074
            java.lang.String r6 = "DomobSDK"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r8 = "handset screen height is "
            r7.<init>(r8)
            r0 = r7
            r1 = r26
            java.lang.StringBuilder r7 = r0.append(r1)
            java.lang.String r7 = r7.toString()
            android.util.Log.d(r6, r7)
        L_0x0074:
            java.lang.String r6 = "sh"
            java.lang.String r7 = java.lang.String.valueOf(r26)
            a(r3, r6, r7)
        L_0x007d:
            java.lang.String r6 = "sd"
            float r7 = cn.domob.android.ads.DomobAdBuilder.d()
            java.lang.String r7 = java.lang.String.valueOf(r7)
            a(r3, r6, r7)
            boolean r6 = cn.domob.android.ads.DomobAdManager.isTestAllowed(r17)
            if (r6 == 0) goto L_0x00a6
            boolean r6 = cn.domob.android.ads.DomobAdManager.isTestMode(r17)
            if (r6 == 0) goto L_0x00a6
            java.lang.String r6 = "m"
            java.lang.String r7 = "test"
            a(r3, r6, r7)
            java.lang.String r6 = "test_action"
            java.lang.String r7 = cn.domob.android.ads.DomobAdManager.getTestAction()
            a(r3, r6, r7)
        L_0x00a6:
            java.lang.String r6 = "k"
            r0 = r3
            r1 = r6
            r2 = r18
            a(r0, r1, r2)
            java.lang.String r6 = "spot"
            r0 = r3
            r1 = r6
            r2 = r19
            a(r0, r1, r2)
            java.lang.String r6 = "dim"
            java.lang.String r7 = "320x48"
            a(r3, r6, r7)
            java.lang.String r6 = cn.domob.android.ads.DomobAdManager.g(r17)
            if (r6 == 0) goto L_0x00d3
            java.lang.String r7 = "d[coord]"
            a(r3, r7, r6)
            java.lang.String r6 = "d[coord_timestamp]"
            java.lang.String r7 = cn.domob.android.ads.DomobAdManager.a()
            a(r3, r6, r7)
        L_0x00d3:
            java.lang.String r6 = "d[pc]"
            java.lang.String r7 = cn.domob.android.ads.DomobAdManager.getPostalCode()
            a(r3, r6, r7)
            java.lang.String r6 = "d[dob]"
            java.lang.String r7 = cn.domob.android.ads.DomobAdManager.b()
            a(r3, r6, r7)
            java.lang.String r6 = "d[gender]"
            java.lang.String r7 = cn.domob.android.ads.DomobAdManager.getGender()
            a(r3, r6, r7)
            java.lang.String r6 = "pb[identifier]"
            java.lang.String r7 = cn.domob.android.ads.DomobAdManager.i(r17)
            a(r3, r6, r7)
            java.lang.String r6 = "pb[version]"
            int r7 = cn.domob.android.ads.DomobAdManager.j(r17)
            java.lang.String r7 = java.lang.String.valueOf(r7)
            a(r3, r6, r7)
            int r6 = cn.domob.android.ads.f.b
            int r6 = r6 + 1
            cn.domob.android.ads.f.b = r6
            r7 = 1
            if (r6 != r7) goto L_0x010f
            cn.domob.android.ads.f.d = r4
        L_0x010f:
            java.lang.String r6 = "stat[reqs]"
            int r7 = cn.domob.android.ads.f.b
            java.lang.String r7 = java.lang.String.valueOf(r7)
            a(r3, r6, r7)
            long r6 = cn.domob.android.ads.f.d
            long r6 = r4 - r6
            java.lang.String r8 = "stat[time]"
            java.lang.String r6 = java.lang.String.valueOf(r6)
            a(r3, r8, r6)
            cn.domob.android.ads.f.d = r4
            java.lang.String r9 = r3.toString()
            java.lang.String r3 = "DomobSDK"
            r4 = 3
            boolean r3 = android.util.Log.isLoggable(r3, r4)
            if (r3 == 0) goto L_0x0156
            java.lang.String r3 = "DomobSDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "ad url:"
            r4.<init>(r5)
            java.lang.String r5 = cn.domob.android.ads.f.a
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = "\nad req:"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r9)
            java.lang.String r4 = r4.toString()
            android.util.Log.d(r3, r4)
        L_0x0156:
            long r12 = java.lang.System.currentTimeMillis()
            r14 = 0
            java.lang.String r3 = cn.domob.android.ads.f.a
            r4 = 0
            java.lang.String r5 = b(r17)
            java.lang.String r6 = cn.domob.android.ads.DomobAdManager.b(r17)
            r7 = 0
            r8 = 30000(0x7530, float:4.2039E-41)
            cn.domob.android.ads.b r3 = cn.domob.android.ads.DomobAdEngine.a.a(r3, r4, r5, r6, r7, r8, r9)
            r15.g = r3
            cn.domob.android.ads.b r3 = r15.g
            r0 = r3
            r1 = r17
            r0.a(r1)
            cn.domob.android.ads.b r3 = r15.g
            boolean r3 = r3.a()
            if (r3 == 0) goto L_0x0205
            cn.domob.android.ads.b r3 = r15.g
            byte[] r3 = r3.d()
            java.lang.String r4 = new java.lang.String
            r4.<init>(r3)
            java.lang.String r3 = ""
            boolean r3 = r4.equals(r3)
            if (r3 != 0) goto L_0x01fe
            java.lang.String r3 = "DomobSDK"
            r5 = 3
            boolean r3 = android.util.Log.isLoggable(r3, r5)
            if (r3 == 0) goto L_0x01af
            java.lang.String r3 = "DomobSDK"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "ad resp:"
            r5.<init>(r6)
            java.lang.StringBuilder r5 = r5.append(r4)
            java.lang.String r5 = r5.toString()
            android.util.Log.d(r3, r5)
        L_0x01af:
            org.json.JSONTokener r3 = new org.json.JSONTokener
            r3.<init>(r4)
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ Exception -> 0x01f1 }
            r4.<init>(r3)     // Catch:{ Exception -> 0x01f1 }
            r3 = r16
            r5 = r20
            r7 = r22
            r9 = r24
            r10 = r27
            r11 = r28
            cn.domob.android.ads.DomobAdEngine r3 = cn.domob.android.ads.DomobAdEngine.a(r3, r4, r5, r7, r9, r10, r11)     // Catch:{ Exception -> 0x01f1 }
        L_0x01c9:
            long r4 = java.lang.System.currentTimeMillis()
            long r4 = r4 - r12
            java.lang.String r6 = "DomobSDK"
            r7 = 3
            boolean r6 = android.util.Log.isLoggable(r6, r7)
            if (r6 == 0) goto L_0x01eb
            java.lang.String r6 = "DomobSDK"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r8 = "ad response time is:"
            r7.<init>(r8)
            java.lang.StringBuilder r7 = r7.append(r4)
            java.lang.String r7 = r7.toString()
            android.util.Log.d(r6, r7)
        L_0x01eb:
            long r6 = cn.domob.android.ads.f.e
            long r4 = r4 + r6
            cn.domob.android.ads.f.e = r4
            return r3
        L_0x01f1:
            r3 = move-exception
            java.lang.String r4 = "DomobSDK"
            java.lang.String r5 = "failed to init ad engine!"
            android.util.Log.e(r4, r5)
            r3.printStackTrace()
            r3 = r14
            goto L_0x01c9
        L_0x01fe:
            java.lang.String r3 = "DomobSDK"
            java.lang.String r4 = "ad resp is empty!"
            android.util.Log.i(r3, r4)
        L_0x0205:
            r3 = r14
            goto L_0x01c9
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.domob.android.ads.f.a(cn.domob.android.ads.DomobAdEngine$RecvHandler, android.content.Context, java.lang.String, java.lang.String, long, long, cn.domob.android.ads.DomobAdBuilder, int, int, boolean, boolean):cn.domob.android.ads.DomobAdEngine");
    }

    /* access modifiers changed from: protected */
    public final c a(Context context) {
        c cVar;
        if (f) {
            if (Log.isLoggable("DomobSDK", 3)) {
                Log.d("DomobSDK", "ignore, it is detecting now");
            }
            return null;
        }
        f = true;
        String c2 = c(context);
        if (Log.isLoggable("DomobSDK", 3)) {
            Log.d("DomobSDK", "detector req:" + c2);
        }
        this.g = DomobAdEngine.a.a(a, null, b(context), DomobAdManager.b(context), null, 30000, c2);
        this.g.a(context);
        if (this.g.a()) {
            String str = new String(this.g.d());
            if (!str.equals("")) {
                if (Log.isLoggable("DomobSDK", 3)) {
                    Log.d("DomobSDK", "detector resp:" + str);
                }
                try {
                    cVar = c.a(context, new JSONObject(new JSONTokener(str)));
                } catch (Exception e2) {
                    Log.e("DomobSDK", "failed to init detector!");
                    cVar = null;
                }
                f = false;
                return cVar;
            }
            Log.i("DomobSDK", "detector resp is empty!");
        }
        cVar = null;
        f = false;
        return cVar;
    }

    private static void a(Context context, StringBuilder sb, String str) {
        String str2;
        sb.append("rt").append("=").append(str);
        String publisherId = DomobAdManager.getPublisherId(context);
        if (publisherId == null) {
            throw new IllegalStateException("Publisher ID is not set!");
        }
        a(sb, "ipb", publisherId);
        String b2 = b(context);
        if (b2 != null) {
            a(sb, "ua", b2);
        } else {
            a(sb, "ua", "unknown");
        }
        String language = Locale.getDefault().getLanguage();
        if (language != null) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(language.toLowerCase());
            String country = Locale.getDefault().getCountry();
            if (country != null) {
                stringBuffer.append("-");
                stringBuffer.append(country.toLowerCase());
            }
            str2 = stringBuffer.toString();
        } else {
            str2 = "zh-cn";
        }
        a(sb, "l", str2);
        a(sb, DomobAdManager.GENDER_FEMALE, "jsonp");
        a(sb, "e", "UTF-8");
        a(sb, "sdk", "1");
        a(sb, "v", "20110503-android-20101220");
        a(sb, "idv", DomobAdManager.b(context));
        String c2 = DomobAdManager.c(context);
        if (c2 != null) {
            if (Log.isLoggable("DomobSDK", 3)) {
                Log.d("DomobSDK", "current network type:" + c2);
            }
            a(sb, "network", c2);
        }
        String h = DomobAdManager.h(context);
        if (h != null) {
            if (Log.isLoggable("DomobSDK", 3)) {
                Log.d("DomobSDK", "CID:" + h);
            }
            a(sb, "cid", h);
        }
    }

    protected static String b(Context context) {
        if (c == null) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("android");
            stringBuffer.append(",");
            stringBuffer.append(",");
            if (Build.VERSION.RELEASE.length() > 0) {
                stringBuffer.append(Build.VERSION.RELEASE);
            } else {
                stringBuffer.append("1.5");
            }
            stringBuffer.append(",");
            stringBuffer.append(",");
            String str = Build.MODEL;
            if (str.length() > 0) {
                stringBuffer.append(str);
            }
            stringBuffer.append(",");
            String networkOperatorName = ((TelephonyManager) context.getSystemService("phone")).getNetworkOperatorName();
            if (networkOperatorName != null) {
                stringBuffer.append(networkOperatorName);
            }
            stringBuffer.append(",");
            stringBuffer.append(",");
            stringBuffer.append(",");
            c = stringBuffer.toString();
            if (Log.isLoggable("DomobSDK", 3)) {
                Log.d("DomobSDK", "getUserAgent:" + c);
            }
        }
        return c;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x003d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String c(android.content.Context r7) {
        /*
            cn.domob.android.ads.DomobAdManager.a(r7)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "4"
            a(r7, r0, r1)
            long r1 = java.lang.System.currentTimeMillis()
            java.lang.String r3 = "ts"
            java.lang.String r1 = java.lang.String.valueOf(r1)
            a(r0, r3, r1)
            r1 = 0
            cn.domob.android.ads.DBHelper r2 = cn.domob.android.ads.DBHelper.a(r7)     // Catch:{ Exception -> 0x008d }
            android.database.Cursor r1 = r2.b()     // Catch:{ Exception -> 0x008d }
            if (r1 == 0) goto L_0x002b
            int r2 = r1.getCount()     // Catch:{ Exception -> 0x0084 }
            if (r2 != 0) goto L_0x0045
        L_0x002b:
            java.lang.String r2 = "DomobSDK"
            r3 = 3
            boolean r2 = android.util.Log.isLoggable(r2, r3)     // Catch:{ Exception -> 0x0084 }
            if (r2 == 0) goto L_0x003b
            java.lang.String r2 = "DomobSDK"
            java.lang.String r3 = "conf db is empty!"
            android.util.Log.d(r2, r3)     // Catch:{ Exception -> 0x0084 }
        L_0x003b:
            if (r1 == 0) goto L_0x0040
            r1.close()
        L_0x0040:
            java.lang.String r0 = r0.toString()
            return r0
        L_0x0045:
            r1.moveToFirst()     // Catch:{ Exception -> 0x0084 }
            java.lang.String r2 = "_conf_ver"
            int r2 = r1.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x0084 }
            java.lang.String r2 = r1.getString(r2)     // Catch:{ Exception -> 0x0084 }
            java.lang.String r3 = "lm[config]"
            a(r0, r3, r2)     // Catch:{ Exception -> 0x0084 }
            java.lang.String r2 = "_res_ver"
            int r2 = r1.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x0084 }
            java.lang.String r2 = r1.getString(r2)     // Catch:{ Exception -> 0x0084 }
            java.lang.String r3 = "lm[res]"
            a(r0, r3, r2)     // Catch:{ Exception -> 0x0084 }
            long r2 = c()     // Catch:{ Exception -> 0x0084 }
            r4 = -1
            int r4 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r4 != 0) goto L_0x007a
            java.lang.String r2 = "_avg_time"
            int r2 = r1.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x0084 }
            long r2 = r1.getLong(r2)     // Catch:{ Exception -> 0x0084 }
        L_0x007a:
            java.lang.String r4 = "avg"
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ Exception -> 0x0084 }
            a(r0, r4, r2)     // Catch:{ Exception -> 0x0084 }
            goto L_0x003b
        L_0x0084:
            r2 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
        L_0x0088:
            r1.printStackTrace()
            r1 = r2
            goto L_0x003b
        L_0x008d:
            r2 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
            goto L_0x0088
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.domob.android.ads.f.c(android.content.Context):java.lang.String");
    }

    protected static long c() {
        if (b > 0) {
            return e / ((long) b);
        }
        return -1;
    }

    protected static String a(Context context, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("jstr").append("=").append(str);
        String b2 = b(context);
        if (b2 != null) {
            a(sb, "ua", b2);
        } else {
            a(sb, "ua", "unknown");
        }
        String h = DomobAdManager.h(context);
        if (h != null) {
            if (Log.isLoggable("DomobSDK", 3)) {
                Log.d("DomobSDK", "CID:" + h);
            }
            a(sb, "cid", h);
        }
        return sb.toString();
    }

    private static void a(StringBuilder sb, String str, String str2) {
        if (str2 != null && str2.length() != 0) {
            try {
                sb.append("&").append(URLEncoder.encode(str, "UTF-8")).append("=").append(URLEncoder.encode(str2, "UTF-8"));
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    static void a(String str) {
        if (str != null) {
            if (Log.isLoggable("DomobSDK", 3)) {
                Log.d("DomobSDK", "set ad url:" + str);
            }
            a = str;
        }
    }

    static String d() {
        return a;
    }
}
