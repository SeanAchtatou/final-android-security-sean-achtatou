package cn.domob.android.ads;

import android.content.Context;
import android.util.Log;

final class d extends Thread {
    private DomobAdView a;

    public d(DomobAdView domobAdView) {
        this.a = domobAdView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.domob.android.ads.DomobAdView.b(cn.domob.android.ads.DomobAdView, boolean):void
     arg types: [cn.domob.android.ads.DomobAdView, int]
     candidates:
      cn.domob.android.ads.DomobAdView.b(cn.domob.android.ads.DomobAdView, cn.domob.android.ads.DomobAdBuilder):void
      cn.domob.android.ads.DomobAdView.b(cn.domob.android.ads.DomobAdView, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.domob.android.ads.DomobAdView.c(cn.domob.android.ads.DomobAdView, boolean):void
     arg types: [cn.domob.android.ads.DomobAdView, int]
     candidates:
      cn.domob.android.ads.DomobAdView.c(cn.domob.android.ads.DomobAdView, cn.domob.android.ads.DomobAdBuilder):void
      cn.domob.android.ads.DomobAdView.c(cn.domob.android.ads.DomobAdView, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.domob.android.ads.DomobAdView.a(cn.domob.android.ads.DomobAdView, boolean):void
     arg types: [cn.domob.android.ads.DomobAdView, int]
     candidates:
      cn.domob.android.ads.DomobAdView.a(cn.domob.android.ads.DomobAdView, cn.domob.android.ads.DomobAdBuilder):cn.domob.android.ads.DomobAdBuilder
      cn.domob.android.ads.DomobAdView.a(cn.domob.android.ads.DomobAdEngine, cn.domob.android.ads.DomobAdBuilder):void
      cn.domob.android.ads.DomobAdView.a(cn.domob.android.ads.DomobAdView, boolean):void */
    public final void run() {
        if (this.a == null) {
            Log.e("DomobSDK", "GetAdThread exit because adview is null!");
            return;
        }
        Context context = this.a.getContext();
        try {
            DomobAdBuilder domobAdBuilder = new DomobAdBuilder(null, context, this.a);
            int measuredWidth = (int) (((float) this.a.getMeasuredWidth()) / DomobAdBuilder.d());
            if (((float) measuredWidth) <= 310.0f) {
                Log.e("DomobSDK", "Ignoring request because we need to have a minimum width of 320 device independent pixels to show an ad.");
                if (Log.isLoggable("DomobSDK", 3)) {
                    Log.d("DomobSDK", "width = " + measuredWidth);
                }
                DomobAdView.g(this.a);
            } else {
                f fVar = new f();
                if (fVar.a(DomobAdView.f(this.a), context, DomobAdView.d(this.a), DomobAdView.e(this.a), this.a.a(), this.a.b(), domobAdBuilder, DomobAdManager.e(context), DomobAdManager.f(context), this.a.c(), this.a.d()) == null) {
                    DomobAdView.g(this.a);
                    b a2 = fVar.a();
                    if (a2 != null) {
                        int e = a2.e();
                        if (e < 200 || e >= 300) {
                            if (Log.isLoggable("DomobSDK", 3)) {
                                Log.d("DomobSDK", "connection return error:" + e + ", try detector next time.");
                            }
                            DomobAdView.a(this.a, true);
                        } else if (Log.isLoggable("DomobSDK", 3)) {
                            Log.d("DomobSDK", "connection is OK, continue ad request next time.");
                        }
                    }
                }
            }
            DomobAdView.b(this.a, false);
            DomobAdView.c(this.a, true);
        } catch (Exception e2) {
            if (DomobAdManager.getPublisherId(context) == null) {
                Log.e("DomobSDK", "Please set your publisher ID first!");
            } else {
                Log.e("DomobSDK", "Unkown exception happened!");
            }
            e2.printStackTrace();
        }
    }
}
