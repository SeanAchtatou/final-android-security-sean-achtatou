package org.zryu.android.andpa;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int about = 2130837504;
        public static final int aboutbutton = 2130837505;
        public static final int aotr_bg = 2130837506;
        public static final int backbutton = 2130837507;
        public static final int food_bg = 2130837508;
        public static final int food_bg4 = 2130837509;
        public static final int help = 2130837510;
        public static final int helpbutton = 2130837511;
        public static final int icon = 2130837512;
        public static final int pause = 2130837513;
        public static final int roach_e = 2130837514;
        public static final int roach_n = 2130837515;
        public static final int roach_ne = 2130837516;
        public static final int roach_nw = 2130837517;
        public static final int roach_s = 2130837518;
        public static final int roach_se = 2130837519;
        public static final int roach_sw = 2130837520;
        public static final int roach_w = 2130837521;
        public static final int scoreboard = 2130837522;
        public static final int startbutton = 2130837523;
        public static final int status = 2130837524;
    }

    public static final class id {
        public static final int AdLinearLayout = 2131034113;
        public static final int game = 2131034112;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class string {
        public static final int app_name = 2130968576;
        public static final int menu_pause = 2130968579;
        public static final int menu_resume = 2130968580;
        public static final int menu_start = 2130968577;
        public static final int menu_stop = 2130968578;
    }
}
