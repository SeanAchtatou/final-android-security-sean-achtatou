package org.zryu.android.andpa;

public class Mode {
    public static final int STATE_GAMEOVER = 4;
    public static final int STATE_PAUSE = 1;
    public static final int STATE_READY = 2;
    public static final int STATE_RUNNING = 3;
    public static final int VIEW_ABOUT = 6;
    public static final int VIEW_GAMEOVER = 3;
    public static final int VIEW_HELP = 5;
    public static final int VIEW_PAUSE = 4;
    public static final int VIEW_RUNNING = 2;
    public static final int VIEW_STARTUP = 1;
    private int preView;
    private int state;
    private int view;

    public int getState() {
        return this.state;
    }

    public void setState(int state2) {
        this.state = state2;
    }

    public boolean isRunning() {
        return this.state == 3;
    }

    public boolean isPaused() {
        return this.state == 1;
    }

    public boolean isReady() {
        return this.state == 2;
    }

    public boolean isGameOver() {
        return this.state == 4;
    }

    public int getView() {
        return this.view;
    }

    public void setView(int view2) {
        this.view = view2;
    }

    public int getPreView() {
        return this.preView;
    }

    public void setPreView(int preView2) {
        this.preView = preView2;
    }
}
