package org.zryu.android.andpa;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.madhouse.android.ads.AdView;
import java.util.ArrayList;

class GameView extends SurfaceView implements SurfaceHolder.Callback {
    /* access modifiers changed from: private */
    public boolean isSurfaceCreated = false;
    /* access modifiers changed from: private */
    public Context mContext;
    private GameThread thread;

    class GameThread extends Thread {
        public static final int SIZE = 20;
        private Bitmap aboutB;
        private Bitmap aboutBG;
        private Bitmap backB;
        private Canvas c = null;
        private Bitmap helpB;
        private Bitmap helpBG;
        private int mCanvasHeight;
        private int mCanvasWidth;
        private ArrayList<Roach> mColony;
        private Handler mHandler;
        private long mLastTime;
        private Mode mMode = new Mode();
        private ArrayList<Bitmap> mRoachImgs;
        private boolean mRun = false;
        private SurfaceHolder mSurfaceHolder;
        private MotherRoach mrs;
        private Bitmap pauseBG;
        private Bitmap playBG;
        private Scorer score = new Scorer();
        private Bitmap scoreBG;
        private Bitmap startB;
        private Bitmap startBG;
        private Bitmap stats;
        private Paint textPaint;

        public GameThread(SurfaceHolder surfaceHolder, Context context, Handler handler) {
            this.mSurfaceHolder = surfaceHolder;
            this.mHandler = handler;
            GameView.this.mContext = context;
            this.startBG = BitmapFactory.decodeResource(GameView.this.mContext.getResources(), R.drawable.aotr_bg);
            this.playBG = BitmapFactory.decodeResource(GameView.this.mContext.getResources(), R.drawable.food_bg);
            this.pauseBG = BitmapFactory.decodeResource(GameView.this.mContext.getResources(), R.drawable.pause);
            this.helpBG = BitmapFactory.decodeResource(GameView.this.mContext.getResources(), R.drawable.help);
            this.aboutBG = BitmapFactory.decodeResource(GameView.this.mContext.getResources(), R.drawable.about);
            this.scoreBG = BitmapFactory.decodeResource(GameView.this.mContext.getResources(), R.drawable.scoreboard);
            this.stats = BitmapFactory.decodeResource(GameView.this.mContext.getResources(), R.drawable.status);
            this.startB = BitmapFactory.decodeResource(GameView.this.mContext.getResources(), R.drawable.startbutton);
            this.helpB = BitmapFactory.decodeResource(GameView.this.mContext.getResources(), R.drawable.helpbutton);
            this.aboutB = BitmapFactory.decodeResource(GameView.this.mContext.getResources(), R.drawable.aboutbutton);
            this.backB = BitmapFactory.decodeResource(GameView.this.mContext.getResources(), R.drawable.backbutton);
            this.textPaint = new Paint();
            createRoachImgs();
            createColony();
            Log.i("app msg", "colony created");
            this.mMode.setView(1);
            this.mMode.setPreView(1);
        }

        public void createRoachImgs() {
            this.mRoachImgs = new ArrayList<>();
            this.mRoachImgs.add(BitmapFactory.decodeResource(GameView.this.mContext.getResources(), R.drawable.roach_n));
            this.mRoachImgs.add(BitmapFactory.decodeResource(GameView.this.mContext.getResources(), R.drawable.roach_ne));
            this.mRoachImgs.add(BitmapFactory.decodeResource(GameView.this.mContext.getResources(), R.drawable.roach_e));
            this.mRoachImgs.add(BitmapFactory.decodeResource(GameView.this.mContext.getResources(), R.drawable.roach_se));
            this.mRoachImgs.add(BitmapFactory.decodeResource(GameView.this.mContext.getResources(), R.drawable.roach_s));
            this.mRoachImgs.add(BitmapFactory.decodeResource(GameView.this.mContext.getResources(), R.drawable.roach_sw));
            this.mRoachImgs.add(BitmapFactory.decodeResource(GameView.this.mContext.getResources(), R.drawable.roach_w));
            this.mRoachImgs.add(BitmapFactory.decodeResource(GameView.this.mContext.getResources(), R.drawable.roach_nw));
        }

        public void createColony() {
            this.mColony = new ArrayList<>();
            for (int i = 0; i < 20; i++) {
                this.mColony.add(new Roach(this.mRoachImgs, -100.0f, -100.0f, 0));
            }
        }

        public void doStart() {
            synchronized (this.mSurfaceHolder) {
                Log.i("app msg", "canvas width: " + this.mCanvasWidth + " canvas height: " + this.mCanvasHeight);
                this.mMode.setState(4);
                this.mrs = new MotherRoach(this.mColony, this.score, this.mMode);
                this.mrs.start();
                this.score.reset();
                this.mLastTime = System.currentTimeMillis() + 100;
                this.mMode.setView(2);
                this.mMode.setPreView(2);
                this.mMode.setState(3);
            }
        }

        public void pause() {
            synchronized (this.mSurfaceHolder) {
                if (this.mMode.isRunning()) {
                    this.mMode.setView(4);
                    setState(1);
                }
            }
        }

        /* JADX INFO: finally extract failed */
        public void run() {
            while (this.mRun) {
                if (GameView.this.isSurfaceCreated) {
                    this.c = null;
                    try {
                        this.c = this.mSurfaceHolder.lockCanvas(null);
                        if (this.c != null) {
                            synchronized (this.mSurfaceHolder) {
                                if (this.mMode.isRunning()) {
                                    updateGame();
                                }
                                doDraw(this.c);
                            }
                        }
                        if (this.c != null) {
                            this.mSurfaceHolder.unlockCanvasAndPost(this.c);
                        }
                    } catch (Throwable th) {
                        if (this.c != null) {
                            this.mSurfaceHolder.unlockCanvasAndPost(this.c);
                        }
                        throw th;
                    }
                }
            }
        }

        public void setRunning(boolean b) {
            this.mRun = b;
        }

        public void setState(int mode) {
            synchronized (this.mSurfaceHolder) {
                setState(mode, null);
            }
        }

        public void setState(int mode, CharSequence message) {
            synchronized (this.mSurfaceHolder) {
                this.mMode.setState(mode);
            }
        }

        public void setSurfaceSize(int width, int height) {
            synchronized (this.mSurfaceHolder) {
                this.mCanvasWidth = width;
                this.mCanvasHeight = height;
            }
        }

        public void unpause() {
            synchronized (this.mSurfaceHolder) {
                if (this.mMode.isPaused()) {
                    this.mLastTime = System.currentTimeMillis() + 100;
                    this.mMode.setView(this.mMode.getPreView());
                    setState(3);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public boolean doTouch(MotionEvent event) {
            synchronized (this.mSurfaceHolder) {
                if (this.mMode.getView() == 2 && this.mMode.isRunning() && event.getAction() == 0) {
                    for (int i = 0; i < 20; i++) {
                        Roach roach = this.mColony.get(i);
                        if (event.getX() > roach.getX() && event.getX() < roach.getX() + ((float) roach.getImg().getWidth()) && event.getY() > roach.getY() && event.getY() < roach.getY() + ((float) roach.getImg().getWidth())) {
                            roach.setState(2);
                            this.score.addHits();
                        }
                    }
                } else if (this.mMode.getView() == 1 && event.getAction() == 0) {
                    if (event.getX() > ((float) 5) && event.getX() < ((float) (this.startB.getWidth() + 5)) && event.getY() > ((float) 315) && event.getY() < ((float) (this.startB.getHeight() + 315))) {
                        doStart();
                    } else if (event.getX() > ((float) 5) && event.getX() < ((float) (this.helpB.getWidth() + 5)) && event.getY() > ((float) (this.startB.getHeight() + 315)) && event.getY() < ((float) (this.startB.getHeight() + 315 + this.helpB.getHeight()))) {
                        this.mMode.setView(5);
                        this.mMode.setPreView(5);
                    } else if (event.getX() > ((float) 5) && event.getX() < ((float) (this.aboutB.getWidth() + 5)) && event.getY() > ((float) (this.startB.getHeight() + 315 + this.helpB.getHeight())) && event.getY() < ((float) (this.startB.getHeight() + 315 + this.helpB.getHeight() + this.aboutB.getHeight()))) {
                        this.mMode.setView(6);
                        this.mMode.setPreView(6);
                    }
                } else if ((this.mMode.getView() == 3 || this.mMode.getView() == 6 || this.mMode.getView() == 5) && event.getAction() == 0 && event.getX() > 120.0f && event.getX() < ((float) (this.backB.getWidth() + 120)) && event.getY() > 400.0f && event.getY() < ((float) (this.backB.getWidth() + AdView.RETRUNCODE_NOADS))) {
                    this.mMode.setView(1);
                    this.mMode.setPreView(1);
                }
            }
            return true;
        }

        private void doDraw(Canvas canvas) {
            canvas.drawARGB(255, 0, 0, 0);
            if (this.mMode.getView() == 1) {
                canvas.drawBitmap(this.startBG, 0.0f, 0.0f, new Paint());
                canvas.drawBitmap(this.startB, 5.0f, (float) 315, new Paint());
                canvas.drawBitmap(this.helpB, 5.0f, (float) (this.startB.getHeight() + 315), new Paint());
                canvas.drawBitmap(this.aboutB, 5.0f, (float) (this.startB.getHeight() + 315 + this.helpB.getHeight()), new Paint());
            } else if (this.mMode.getView() == 2) {
                canvas.drawBitmap(this.playBG, 0.0f, 0.0f, new Paint());
                for (int i = 0; i < 20; i++) {
                    canvas.drawBitmap(this.mColony.get(i).getImg(), this.mColony.get(i).getX(), this.mColony.get(i).getY(), new Paint());
                }
                canvas.drawBitmap(this.stats, 0.0f, 410.0f, new Paint());
                this.textPaint.setTextSize(30.0f);
                this.textPaint.setColor(-1);
                this.textPaint.setTextAlign(Paint.Align.LEFT);
                canvas.drawText(new StringBuilder(String.valueOf(this.score.getHits())).toString(), 60.0f, 445.0f, this.textPaint);
                if (this.score.getMisses() > 6) {
                    this.textPaint.setColor(-65536);
                }
                canvas.drawText(new StringBuilder(String.valueOf(this.score.getMisses())).toString(), 275.0f, 445.0f, this.textPaint);
            } else if (this.mMode.getView() == 3) {
                canvas.drawBitmap(this.scoreBG, 0.0f, 0.0f, new Paint());
                this.textPaint.setColor(-1);
                this.textPaint.setTextSize(60.0f);
                this.textPaint.setTextAlign(Paint.Align.CENTER);
                canvas.drawText(new StringBuilder(String.valueOf(this.score.getHits())).toString(), 160.0f, 250.0f, this.textPaint);
                this.textPaint.setTextSize(12.0f);
                canvas.drawText(gameOverMsg(), 160.0f, 285.0f, this.textPaint);
                canvas.drawBitmap(this.backB, 120.0f, 400.0f, new Paint());
            } else if (this.mMode.getView() == 4) {
                canvas.drawBitmap(this.pauseBG, 0.0f, 0.0f, new Paint());
            } else if (this.mMode.getView() == 5) {
                canvas.drawBitmap(this.helpBG, 0.0f, 0.0f, new Paint());
                canvas.drawBitmap(this.backB, 120.0f, 400.0f, new Paint());
            } else if (this.mMode.getView() == 6) {
                canvas.drawBitmap(this.aboutBG, 0.0f, 0.0f, new Paint());
                canvas.drawBitmap(this.backB, 120.0f, 400.0f, new Paint());
            }
        }

        private String gameOverMsg() {
            if (this.score.getHits() <= 30) {
                return "WIMP!";
            }
            if (this.score.getHits() > 30 && this.score.getHits() <= 50) {
                return "Is that all you got?";
            }
            if (this.score.getHits() > 50 && this.score.getHits() <= 80) {
                return "Your fingers need more exercise!";
            }
            if (this.score.getHits() > 80 && this.score.getHits() <= 100) {
                return "Get your fingers dirtier!";
            }
            if (this.score.getHits() > 100 && this.score.getHits() <= 130) {
                return "Splat 'em good!";
            }
            if (this.score.getHits() > 130 && this.score.getHits() <= 150) {
                return "Crunchy eh?";
            }
            if (this.score.getHits() > 150 && this.score.getHits() <= 180) {
                return "Splat the s*** out of 'em!";
            }
            if (this.score.getHits() <= 180 || this.score.getHits() > 200) {
                return "OUR SAVIOUR!";
            }
            return "BAYGON!";
        }

        private void updateGame() {
            long now = System.currentTimeMillis();
            if (this.mLastTime <= now) {
                double d = ((double) (now - this.mLastTime)) / 1000.0d;
                this.mLastTime = now;
            }
        }
    }

    public void setThread(GameThread thread2) {
        this.thread = thread2;
    }

    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        getHolder().addCallback(this);
        setFocusable(true);
    }

    public GameThread getThread() {
        return this.thread;
    }

    public boolean onTouchEvent(MotionEvent event) {
        return this.thread.doTouch(event);
    }

    public void onWindowFocusChanged(boolean hasWindowFocus) {
        if (!hasWindowFocus) {
            this.thread.pause();
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        this.thread.setSurfaceSize(width, height);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        this.isSurfaceCreated = true;
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        this.thread.setRunning(false);
        while (retry) {
            try {
                this.thread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
    }
}
