package org.zryu.android.andpa;

import java.util.Random;

public class RoachGuide extends Thread {
    private float finX;
    private float finY;
    private Mode mode;
    private int n;
    private Roach r;
    private Scorer score;
    private int speed = 20;

    public RoachGuide(Roach r2, float finX2, float finY2, int n2, Scorer score2, Mode mode2) {
        this.r = r2;
        this.finX = finX2;
        this.finY = finY2;
        this.n = n2;
        this.score = score2;
        this.mode = mode2;
    }

    public void run() {
        synchronized (this) {
            this.r.setState(1);
            switch (this.n) {
                case 0:
                    this.r.setImg("s");
                    while (true) {
                        if (this.r.getY() < this.finY && this.score.getMisses() < 10) {
                            if (this.mode.isRunning()) {
                                if (this.r.isClicked()) {
                                    break;
                                } else {
                                    sleep();
                                    this.r.setY(this.r.getY() + 1.0f);
                                }
                            } else if (this.mode.isGameOver()) {
                                break;
                            }
                        }
                    }
                case 1:
                    this.r.setImg("n");
                    while (true) {
                        if (this.r.getY() > this.finY && this.score.getMisses() < 10) {
                            if (this.mode.isRunning()) {
                                if (this.r.isClicked()) {
                                    break;
                                } else {
                                    sleep();
                                    this.r.setY(this.r.getY() - 1.0f);
                                }
                            } else if (this.mode.isGameOver()) {
                                break;
                            }
                        }
                    }
                case 2:
                    this.r.setImg("e");
                    while (true) {
                        if (this.r.getX() < this.finX && this.score.getMisses() < 10) {
                            if (this.mode.isRunning()) {
                                if (this.r.isClicked()) {
                                    break;
                                } else {
                                    sleep();
                                    this.r.setX(this.r.getX() + 1.0f);
                                }
                            } else if (this.mode.isGameOver()) {
                                break;
                            }
                        }
                    }
                case 3:
                    this.r.setImg("w");
                    while (true) {
                        if (this.r.getX() > this.finX && this.score.getMisses() < 10) {
                            if (this.mode.isRunning()) {
                                if (this.r.isClicked()) {
                                    break;
                                } else {
                                    sleep();
                                    this.r.setX(this.r.getX() - 1.0f);
                                }
                            } else if (this.mode.isGameOver()) {
                                break;
                            }
                        }
                    }
                case 4:
                    if (this.r.getX() > this.finX) {
                        this.r.setImg("sw");
                        while (true) {
                            if (this.r.getY() < this.finY && this.score.getMisses() < 10 && isInBounds(this.r)) {
                                if (this.mode.isRunning()) {
                                    if (this.r.isClicked()) {
                                        break;
                                    } else {
                                        sleep();
                                        this.r.setX(this.r.getX() - 1.0f);
                                        this.r.setY(this.r.getY() + 1.0f);
                                    }
                                } else if (this.mode.isGameOver()) {
                                    break;
                                }
                            }
                        }
                    } else {
                        this.r.setImg("se");
                        while (true) {
                            if (this.r.getY() < this.finY && this.score.getMisses() < 10 && isInBounds(this.r)) {
                                if (this.mode.isRunning()) {
                                    if (this.r.isClicked()) {
                                        break;
                                    } else {
                                        sleep();
                                        this.r.setX(this.r.getX() + 1.0f);
                                        this.r.setY(this.r.getY() + 1.0f);
                                    }
                                } else if (this.mode.isGameOver()) {
                                    break;
                                }
                            }
                        }
                    }
                case 5:
                    if (this.r.getX() > this.finX) {
                        this.r.setImg("nw");
                        while (true) {
                            if (this.r.getY() > this.finY && this.score.getMisses() < 10 && isInBounds(this.r)) {
                                if (this.mode.isRunning()) {
                                    if (this.r.isClicked()) {
                                        break;
                                    } else {
                                        sleep();
                                        this.r.setX(this.r.getX() - 1.0f);
                                        this.r.setY(this.r.getY() - 1.0f);
                                    }
                                } else if (this.mode.isGameOver()) {
                                    break;
                                }
                            }
                        }
                    } else {
                        this.r.setImg("ne");
                        while (true) {
                            if (this.r.getY() > this.finY && this.score.getMisses() < 10 && isInBounds(this.r)) {
                                if (this.mode.isRunning()) {
                                    if (this.r.isClicked()) {
                                        break;
                                    } else {
                                        sleep();
                                        this.r.setX(this.r.getX() + 1.0f);
                                        this.r.setY(this.r.getY() - 1.0f);
                                    }
                                } else if (this.mode.isGameOver()) {
                                    break;
                                }
                            }
                        }
                    }
                case 6:
                    if (this.r.getY() > this.finY) {
                        this.r.setImg("nw");
                        while (true) {
                            if (this.r.getX() > this.finX && this.score.getMisses() < 10 && isInBounds(this.r)) {
                                if (this.mode.isRunning()) {
                                    if (this.r.isClicked()) {
                                        break;
                                    } else {
                                        sleep();
                                        this.r.setX(this.r.getX() - 1.0f);
                                        this.r.setY(this.r.getY() - 1.0f);
                                    }
                                } else if (this.mode.isGameOver()) {
                                    break;
                                }
                            }
                        }
                    } else {
                        this.r.setImg("sw");
                        while (true) {
                            if (this.r.getX() > this.finX && this.score.getMisses() < 10 && isInBounds(this.r)) {
                                if (this.mode.isRunning()) {
                                    if (this.r.isClicked()) {
                                        break;
                                    } else {
                                        sleep();
                                        this.r.setX(this.r.getX() - 1.0f);
                                        this.r.setY(this.r.getY() + 1.0f);
                                    }
                                } else if (this.mode.isGameOver()) {
                                    break;
                                }
                            }
                        }
                    }
                case 7:
                    if (this.r.getY() > this.finY) {
                        this.r.setImg("ne");
                        while (true) {
                            if (this.r.getX() < this.finX && this.score.getMisses() < 10 && isInBounds(this.r)) {
                                if (this.mode.isRunning()) {
                                    if (this.r.isClicked()) {
                                        break;
                                    } else {
                                        sleep();
                                        this.r.setX(this.r.getX() + 1.0f);
                                        this.r.setY(this.r.getY() - 1.0f);
                                    }
                                } else if (this.mode.isGameOver()) {
                                    break;
                                }
                            }
                        }
                    } else {
                        this.r.setImg("se");
                        while (this.r.getX() < this.finX && this.score.getMisses() < 10 && isInBounds(this.r)) {
                            if (this.mode.isRunning()) {
                                if (this.r.isClicked()) {
                                    break;
                                } else {
                                    sleep();
                                    this.r.setX(this.r.getX() + 1.0f);
                                    this.r.setY(this.r.getY() + 1.0f);
                                }
                            } else if (this.mode.isGameOver()) {
                                break;
                            }
                        }
                    }
                    break;
            }
            cleanup();
        }
    }

    private void cleanup() {
        if (this.r.isVisible() && this.score.getMisses() < 10) {
            this.score.addMisses();
        }
        this.r.setX(-100.0f);
        this.r.setY(-100.0f);
        this.r.setState(0);
    }

    private void sleep() {
        int x = this.score.getHits() / 10;
        if (x == 0) {
            x = 1;
        }
        this.speed = generateRandInt((20 - x) + 5);
        try {
            Thread.sleep((long) this.speed);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setSpeed(int speed2) {
        this.speed = speed2;
    }

    public int generateRandInt(int max) {
        return new Random().nextInt(max);
    }

    public boolean isInBounds(Roach r2) {
        return r2.getX() >= -80.0f && r2.getX() <= 400.0f && r2.getY() >= -80.0f && r2.getY() <= 490.0f;
    }
}
