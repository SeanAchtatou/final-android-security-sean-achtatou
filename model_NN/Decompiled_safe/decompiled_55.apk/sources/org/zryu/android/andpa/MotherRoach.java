package org.zryu.android.andpa;

import android.util.Log;
import java.util.ArrayList;
import java.util.Random;

public class MotherRoach extends Thread {
    public static final float LEFTBOUND = -80.0f;
    public static final float LOWERBOUND = 490.0f;
    public static final int MAXCHOICES = 7;
    public static final int MAXMISSES = 10;
    public static final float RIGHTBOUND = 400.0f;
    public static final int STATE_RUNNING = 4;
    public static final float UPPERBOUND = -80.0f;
    private ArrayList<Roach> colony;
    private Mode mode;
    private Scorer score;

    public MotherRoach(ArrayList<Roach> colony2, Scorer score2, Mode mode2) {
        this.colony = colony2;
        this.score = score2;
        this.mode = mode2;
    }

    public void run() {
        synchronized (this) {
            while (this.score.getMisses() < 10) {
                if (this.mode.isRunning()) {
                    int i = generateRandInt(this.colony.size());
                    int sleepingTime = generateRandInt(30) * 100;
                    if (this.score.getHits() > 30) {
                        sleepingTime = generateRandInt(90000 / this.score.getHits());
                    }
                    if (this.colony.get(i).isHidden()) {
                        createRoach(this.colony.get(i), generateRandInt(7));
                    }
                    try {
                        Thread.sleep((long) sleepingTime);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else if (this.mode.isGameOver()) {
                    break;
                }
            }
            this.mode.setState(4);
            this.mode.setView(3);
            this.mode.setPreView(3);
            Log.i("app msg", "MotherRoach dead");
        }
    }

    public void createRoach(Roach r, int n) {
        float xa = 100.0f + generateRandFloat(8);
        float xb = generateRandFloat(8) + 100.0f;
        float ya = generateRandFloat(15) + 100.0f;
        float yb = generateRandFloat(15) + 100.0f;
        switch (n) {
            case 0:
                r.setX(xa);
                r.setY(-80.0f);
                new RoachGuide(r, xa, 490.0f, n, this.score, this.mode).start();
                return;
            case 1:
                r.setX(xa);
                r.setY(490.0f);
                new RoachGuide(r, xa, -80.0f, n, this.score, this.mode).start();
                return;
            case 2:
                r.setX(-80.0f);
                r.setY(ya);
                new RoachGuide(r, 400.0f, ya, n, this.score, this.mode).start();
                return;
            case 3:
                r.setX(400.0f);
                r.setY(ya);
                new RoachGuide(r, -80.0f, ya, n, this.score, this.mode).start();
                return;
            case 4:
                r.setX(xa);
                r.setY(-80.0f);
                new RoachGuide(r, xb, 490.0f, n, this.score, this.mode).start();
                return;
            case 5:
                r.setX(xa);
                r.setY(490.0f);
                new RoachGuide(r, xb, -80.0f, n, this.score, this.mode).start();
                return;
            case 6:
                r.setX(400.0f);
                r.setY(ya);
                new RoachGuide(r, -80.0f, yb, n, this.score, this.mode).start();
                return;
            case 7:
                r.setX(-80.0f);
                r.setY(ya);
                new RoachGuide(r, 400.0f, yb, n, this.score, this.mode).start();
                return;
            default:
                return;
        }
    }

    public float generateRandFloat(int max) {
        return (float) (new Random().nextInt(max) * 10);
    }

    public int generateRandInt(int max) {
        return new Random().nextInt(max);
    }

    public void destroy() {
        this.score.setMisses(10);
    }
}
