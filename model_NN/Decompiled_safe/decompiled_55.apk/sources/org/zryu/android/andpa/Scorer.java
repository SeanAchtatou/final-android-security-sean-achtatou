package org.zryu.android.andpa;

public class Scorer {
    private int hits = 0;
    private int misses = 0;

    public int getHits() {
        return this.hits;
    }

    public void setHits(int hits2) {
        this.hits = hits2;
    }

    public int getMisses() {
        return this.misses;
    }

    public void setMisses(int misses2) {
        this.misses = misses2;
    }

    public void addHits() {
        this.hits++;
    }

    public void addMisses() {
        this.misses++;
    }

    public void reset() {
        this.hits = 0;
        this.misses = 0;
    }
}
