package org.zryu.android.andpa;

import android.util.Log;

public class Cleaner extends Thread {
    SharedVars sv;

    public Cleaner(SharedVars sv2) {
        this.sv = sv2;
    }

    public void run() {
        for (int i = 0; i < 10; i++) {
            Log.i("app msg", "Consumer got " + this.sv.getValue());
            try {
                Thread.sleep((long) ((int) (Math.random() * 10000.0d)));
            } catch (InterruptedException e) {
            }
        }
    }
}
