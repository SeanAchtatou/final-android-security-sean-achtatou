package org.zryu.android.andpa;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.widget.LinearLayout;
import com.gfan.sdk.statistics.Collector;
import com.madhouse.android.ads.AdView;
import org.zryu.android.andpa.GameView;

public class GameTemplate extends Activity {
    private static final int MENU_PAUSE = 1;
    private static final int MENU_RESUME = 2;
    private static final int MENU_START = 3;
    private static final int MENU_STOP = 4;
    private GameView.GameThread mGameThread;
    private GameView mGameView;

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 3, 0, (int) R.string.menu_start);
        menu.add(0, 4, 0, (int) R.string.menu_stop);
        menu.add(0, 1, 0, (int) R.string.menu_pause);
        menu.add(0, 2, 0, (int) R.string.menu_resume);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                this.mGameThread.pause();
                return true;
            case 2:
                this.mGameThread.unpause();
                return true;
            case 3:
                this.mGameThread.doStart();
                return true;
            case 4:
                this.mGameThread.setState(4);
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        Collector.onError(this);
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.main);
        this.mGameView = (GameView) findViewById(R.id.game);
        SurfaceHolder holder = this.mGameView.getHolder();
        holder.addCallback(this.mGameView);
        GameView gameView = this.mGameView;
        gameView.getClass();
        this.mGameThread = new GameView.GameThread(holder, this, null);
        this.mGameView.setThread(this.mGameThread);
        this.mGameThread.setState(2);
        this.mGameThread.setRunning(true);
        this.mGameThread.start();
        Log.w(getClass().getName(), "SIS is null");
        addAdView();
    }

    private void addAdView() {
        ((LinearLayout) findViewById(R.id.AdLinearLayout)).addView(new AdView(this, null, 0, "90004938", 30, 0, false));
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        Collector.onPause(this);
        super.onPause();
        this.mGameView.getThread().pause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        Collector.onResume(this);
        super.onResume();
    }
}
