package org.zryu.android.andpa;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;

public class SharedVars {
    int[] array = new int[10];
    Canvas canvas;
    Bitmap img;
    int top;

    public SharedVars(Canvas canvas2, Bitmap img2) {
        this.canvas = canvas2;
        this.img = img2;
    }

    public void insertValue(int value) {
        synchronized (this) {
            if (this.top < this.array.length) {
                this.array[this.top] = value;
                this.top++;
                this.canvas.drawBitmap(this.img, 10.0f, (float) value, new Paint());
                try {
                    if (this.top < 0 || this.top >= 10) {
                        Log.i("app msg", "array full");
                        wait();
                    } else {
                        notify();
                    }
                } catch (InterruptedException e) {
                }
            }
        }
    }

    public int getValue() {
        int i;
        synchronized (this) {
            if (this.top <= 0) {
                try {
                    wait();
                } catch (InterruptedException e) {
                }
            }
            this.top--;
            notify();
            i = this.array[this.top];
        }
        return i;
    }
}
