package org.zryu.android.andpa;

import android.graphics.Bitmap;
import java.util.ArrayList;

public class Roach {
    public static final int ROACH_CLICKED = 2;
    public static final int ROACH_HIDDEN = 0;
    public static final int ROACH_VISIBLE = 1;
    private Bitmap img;
    private ArrayList<Bitmap> roachImgs;
    private int state;
    private float x;
    private float y;

    public Roach(ArrayList<Bitmap> roachImgs2, float x2, float y2, int state2) {
        this.roachImgs = roachImgs2;
        this.x = x2;
        this.y = y2;
        this.state = state2;
        this.img = roachImgs2.get(0);
    }

    public float getX() {
        return this.x;
    }

    public void setX(float x2) {
        this.x = x2;
    }

    public float getY() {
        return this.y;
    }

    public void setY(float y2) {
        this.y = y2;
    }

    public int getState() {
        return this.state;
    }

    public void setState(int state2) {
        this.state = state2;
    }

    public Bitmap getImg() {
        return this.img;
    }

    public void setImg(String code) {
        if (code.equals("n")) {
            this.img = this.roachImgs.get(0);
        } else if (code.equals("ne")) {
            this.img = this.roachImgs.get(1);
        } else if (code.equals("e")) {
            this.img = this.roachImgs.get(2);
        } else if (code.equals("se")) {
            this.img = this.roachImgs.get(3);
        } else if (code.equals("s")) {
            this.img = this.roachImgs.get(4);
        } else if (code.equals("sw")) {
            this.img = this.roachImgs.get(5);
        } else if (code.equals("w")) {
            this.img = this.roachImgs.get(6);
        } else if (code.equals("nw")) {
            this.img = this.roachImgs.get(7);
        } else {
            this.img = this.roachImgs.get(0);
        }
    }

    public boolean isClicked() {
        return this.state == 2;
    }

    public boolean isHidden() {
        return this.state == 0;
    }

    public boolean isVisible() {
        return this.state == 1;
    }
}
