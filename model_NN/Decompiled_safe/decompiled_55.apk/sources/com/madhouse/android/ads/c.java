package com.madhouse.android.ads;

import I.I;
import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.EditText;
import java.util.Timer;

final class c extends WebChromeClient {
    private /* synthetic */ bbbb _;

    c(bbbb bbbb) {
        this._ = bbbb;
    }

    public final boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this._._);
        builder.setTitle(I.I(421));
        builder.setMessage(str2);
        builder.setPositiveButton(17039370, new cc(this, jsResult));
        builder.setCancelable(false);
        builder.create();
        builder.show();
        return true;
    }

    public final boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this._._);
        builder.setTitle(I.I(413));
        builder.setMessage(str2);
        builder.setPositiveButton(17039370, new ccc(this, jsResult));
        builder.setNeutralButton(17039360, new cccc(this, jsResult));
        builder.setCancelable(false);
        builder.create();
        builder.show();
        return true;
    }

    public final boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this._._);
        builder.setTitle(I.I(406));
        EditText editText = new EditText(this._._);
        editText.setSingleLine();
        editText.setText(str3);
        builder.setView(editText);
        builder.setPositiveButton(17039370, new ccccc(this, jsPromptResult, editText));
        builder.setNeutralButton(17039360, new d(this, jsPromptResult));
        return super.onJsPrompt(webView, str, str2, str3, jsPromptResult);
    }

    public final void onReceivedIcon(WebView webView, Bitmap bitmap) {
        super.onReceivedIcon(webView, bitmap);
        b bVar = this._.__.__;
        if (bVar.__ != null && bitmap != null) {
            bVar.__.setImageBitmap(bitmap);
            bVar.__.setVisibility(0);
        }
    }

    public final void onReceivedTitle(WebView webView, String str) {
        b bVar = this._.__.__;
        if (bVar._ != null) {
            bb bbVar = bVar._;
            bbVar.$$$ = str;
            bbVar._ = bbVar.$$$$.measureText(str);
            bbVar.__ = (float) bbVar.getWidth();
            bbVar.____ = (float) bbVar.getPaddingLeft();
            bbVar._____ = bbVar.getTextSize() + ((bbVar.___ - bbVar.getTextSize()) / 4.0f);
            bbVar.$ = 0.0f;
            new Timer().schedule(new bbb(bbVar), 3000);
        }
        super.onReceivedTitle(webView, str);
    }
}
