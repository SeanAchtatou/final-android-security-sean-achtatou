package com.madhouse.android.ads;

import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

final class gg implements Animation.AnimationListener {
    final /* synthetic */ AdView _;
    private final /* synthetic */ ____ __;

    gg(AdView adView, ____ ____) {
        this._ = adView;
        this.__ = ____;
    }

    public final void onAnimationEnd(Animation animation) {
        this.__.setVisibility(0);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(350);
        alphaAnimation.startNow();
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        alphaAnimation.setAnimationListener(new ggg(this, this.__));
        this.__.startAnimation(alphaAnimation);
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
