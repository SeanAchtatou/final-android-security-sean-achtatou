package com.gfan.sdk.statistics;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.Iterator;
import java.util.Vector;

public class ConnectDBUtil {
    private static Vector<ConnectDBUtil> connects = new Vector<>();
    private SQLiteDatabase db = this.mDatabaseHelper.getWritableDatabase();
    private DatabaseHelper mDatabaseHelper;

    private ConnectDBUtil(Context context) {
        this.mDatabaseHelper = new DatabaseHelper(context);
    }

    public static synchronized ConnectDBUtil getConnection(Context context) {
        ConnectDBUtil connectDBUtil;
        synchronized (ConnectDBUtil.class) {
            Iterator<ConnectDBUtil> it = connects.iterator();
            while (true) {
                if (it.hasNext()) {
                    ConnectDBUtil connect = it.next();
                    if (!connect.db.isOpen()) {
                        connect.db = connect.mDatabaseHelper.getWritableDatabase();
                        connectDBUtil = connect;
                        break;
                    }
                } else {
                    ConnectDBUtil connect2 = new ConnectDBUtil(context);
                    connects.add(connect2);
                    connectDBUtil = connect2;
                    break;
                }
            }
        }
        return connectDBUtil;
    }

    public synchronized void close() {
        if (this.mDatabaseHelper != null) {
            this.mDatabaseHelper.close();
        }
    }

    public synchronized Cursor AppSelect(long appStartTime) {
        return this.db.query("appbase", new String[]{"clickname", "clickcount", "app_start_time"}, "app_start_time=" + appStartTime, null, null, null, null);
    }

    public synchronized Cursor AppSelectClickname(String clickname, long appStartTime) {
        return this.db.query("appbase", new String[]{"clickcount"}, "clickname ='" + clickname + "' and app_start_time=" + appStartTime, null, null, null, null);
    }

    public synchronized long AppInsert(String clickname, long appStartTime, int clickcount) {
        ContentValues cv;
        cv = new ContentValues();
        cv.put("clickname", clickname);
        cv.put("clickcount", Integer.valueOf(clickcount));
        cv.put("app_start_time", Long.valueOf(appStartTime));
        return this.db.insert("appbase", null, cv);
    }

    public synchronized long AppUpdate(String clickname, long appStartTime, int count) {
        ContentValues cv;
        cv = new ContentValues();
        cv.put("clickcount", Integer.valueOf(count));
        return (long) this.db.update("appbase", cv, "clickname = '" + clickname + "' and app_start_time=" + appStartTime, null);
    }

    public synchronized boolean AppClear(long appStartTime) {
        return this.db.delete("appbase", new StringBuilder("app_start_time=").append(appStartTime).toString(), null) > 0;
    }

    public synchronized Cursor BackupAppInfoSelect() {
        return this.db.query("app_backup_base", null, null, null, null, null, null);
    }

    public synchronized long BackupAppInfoInsert(String version, long starttime, long endtime, long timesum, String mac, String cpidmac, String opid, String sdkVersion, String sdkType) {
        ContentValues cv;
        cv = new ContentValues();
        cv.put("version", version);
        cv.put("starttime", Long.valueOf(starttime));
        cv.put("endtime", Long.valueOf(endtime));
        cv.put("timesum", Long.valueOf(timesum));
        cv.put("mac", mac);
        cv.put("cpidmac", cpidmac);
        cv.put("opid", opid);
        cv.put("sdk_version", sdkVersion);
        cv.put("sdk_type", sdkType);
        return this.db.insert("app_backup_base", null, cv);
    }

    public synchronized int BackupAppInfoClear(long id) {
        return this.db.delete("app_backup_base", "id=" + id, null);
    }

    public synchronized Cursor ridSelect() {
        return this.db.query("ridbase", null, null, null, null, null, null);
    }

    public synchronized long ridInsert(String rid) {
        long insert;
        Cursor cursor = this.db.query("ridbase", null, "rid='" + rid + "'", null, null, null, null);
        if (cursor.getCount() > 0) {
            cursor.close();
            insert = -1;
        } else {
            cursor.close();
            ContentValues cv = new ContentValues();
            cv.put("rid", rid);
            insert = this.db.insert("ridbase", null, cv);
        }
        return insert;
    }

    public synchronized Cursor BackupStartInfoSelect() {
        return this.db.query("app_backup_start", null, null, null, null, null, null);
    }

    public synchronized long BackupStartInfoInsert(String version, long starttime, String mac, String cpidmac, String opid, String sdkVersion, String sdkType) {
        ContentValues cv;
        cv = new ContentValues();
        cv.put("version", version);
        cv.put("starttime", Long.valueOf(starttime));
        cv.put("mac", mac);
        cv.put("cpidmac", cpidmac);
        cv.put("opid", opid);
        cv.put("sdk_version", sdkVersion);
        cv.put("sdk_type", sdkType);
        return this.db.insert("app_backup_start", null, cv);
    }

    public synchronized int BackupStartInfoClear(long id) {
        return this.db.delete("app_backup_start", "id=" + id, null);
    }
}
