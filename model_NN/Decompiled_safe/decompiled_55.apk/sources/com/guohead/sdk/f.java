package com.guohead.sdk;

import android.util.Log;
import com.guohead.sdk.util.GuoheAdUtil;

final class f extends Thread {
    private /* synthetic */ GuoheAdLayout a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    f(GuoheAdLayout guoheAdLayout, String str) {
        super(str);
        this.a = guoheAdLayout;
    }

    public final void run() {
        try {
            Log.d(GuoheAdUtil.GUOHEAD, "Will call rotateAd() in " + this.a.extra.cycleTime + " seconds");
            Thread.sleep((long) (this.a.extra.cycleTime * 1000));
        } catch (InterruptedException e) {
            Log.e(GuoheAdUtil.GUOHEAD, "Caught InterruptedException in rotateThreadedDelayed()", e);
        }
        this.a.rotateAd();
    }
}
