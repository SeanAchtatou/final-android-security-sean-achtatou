package com.guohead.sdk;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;
import com.guohead.sdk.adapters.GuoheAdAdapter;
import com.guohead.sdk.obj.Custom;
import com.guohead.sdk.obj.Extra;
import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.GuoheAdUtil;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GuoheAdManager {
    private Extra a;
    private List b;
    private int c = 0;
    private Context d;
    public SharedPreferences data;
    private Iterator e;
    public String keyGuoheAd;
    public String localeString;

    public GuoheAdManager(Context context, String str) {
        Log.i(GuoheAdUtil.GUOHEAD, "Creating guoheAdManager...");
        this.d = context;
        this.keyGuoheAd = str;
        GuoheAdUtil.init(context);
        while (this.a == null) {
            fetchConfig();
            if (this.a == null) {
                try {
                    Log.d(GuoheAdUtil.GUOHEAD, "Sleeping for 30 seconds");
                    Thread.sleep(30000);
                } catch (InterruptedException e2) {
                    Log.e(GuoheAdUtil.GUOHEAD, "Thread unable to sleep", e2);
                }
            }
        }
        Log.i(GuoheAdUtil.GUOHEAD, "Finished creating guoheAdManager");
    }

    private static String a(InputStream inputStream) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream), 8192);
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    sb.append(readLine + "\n");
                } else {
                    try {
                        inputStream.close();
                        return sb.toString();
                    } catch (IOException e2) {
                        Log.e(GuoheAdUtil.GUOHEAD, "Caught IOException in convertStreamToString()", e2);
                        return null;
                    }
                }
            } catch (IOException e3) {
                Log.e(GuoheAdUtil.GUOHEAD, "Caught IOException in convertStreamToString()", e3);
                try {
                    inputStream.close();
                    return null;
                } catch (IOException e4) {
                    Log.e(GuoheAdUtil.GUOHEAD, "Caught IOException in convertStreamToString()", e4);
                    return null;
                }
            } catch (Throwable th) {
                try {
                    inputStream.close();
                    throw th;
                } catch (IOException e5) {
                    Log.e(GuoheAdUtil.GUOHEAD, "Caught IOException in convertStreamToString()", e5);
                    return null;
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x008e, code lost:
        android.util.Log.e(com.guohead.sdk.util.GuoheAdUtil.GUOHEAD, "Unable to parse response from JSON. This may or may not be fatal.", r0);
        r5.a = new com.guohead.sdk.obj.Extra();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x009d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x009e, code lost:
        android.util.Log.e(com.guohead.sdk.util.GuoheAdUtil.GUOHEAD, "Unable to parse response from JSON. This may or may not be fatal.", r0);
        r5.a = new com.guohead.sdk.obj.Extra();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x008d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x009d A[ExcHandler: NullPointerException (r0v0 'e' java.lang.NullPointerException A[CUSTOM_DECLARE]), Splitter:B:0:0x0000] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(java.lang.String r6) {
        /*
            r5 = this;
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ JSONException -> 0x008d, NullPointerException -> 0x009d }
            r0.<init>(r6)     // Catch:{ JSONException -> 0x008d, NullPointerException -> 0x009d }
            java.lang.String r1 = "extra"
            org.json.JSONObject r1 = r0.getJSONObject(r1)     // Catch:{ JSONException -> 0x008d, NullPointerException -> 0x009d }
            com.guohead.sdk.obj.Extra r2 = new com.guohead.sdk.obj.Extra     // Catch:{ JSONException -> 0x008d, NullPointerException -> 0x009d }
            r2.<init>()     // Catch:{ JSONException -> 0x008d, NullPointerException -> 0x009d }
            java.lang.String r3 = "cycle_time"
            int r3 = r1.getInt(r3)     // Catch:{ JSONException -> 0x0084, NullPointerException -> 0x009d }
            r2.cycleTime = r3     // Catch:{ JSONException -> 0x0084, NullPointerException -> 0x009d }
            java.lang.String r3 = "location_on"
            int r3 = r1.getInt(r3)     // Catch:{ JSONException -> 0x0084, NullPointerException -> 0x009d }
            r2.locationOn = r3     // Catch:{ JSONException -> 0x0084, NullPointerException -> 0x009d }
            java.lang.String r3 = "transition"
            int r3 = r1.getInt(r3)     // Catch:{ JSONException -> 0x0084, NullPointerException -> 0x009d }
            r2.transition = r3     // Catch:{ JSONException -> 0x0084, NullPointerException -> 0x009d }
            java.lang.String r3 = "background_color_rgb"
            org.json.JSONObject r3 = r1.getJSONObject(r3)     // Catch:{ JSONException -> 0x0084, NullPointerException -> 0x009d }
            java.lang.String r4 = "red"
            int r4 = r3.getInt(r4)     // Catch:{ JSONException -> 0x0084, NullPointerException -> 0x009d }
            r2.bgRed = r4     // Catch:{ JSONException -> 0x0084, NullPointerException -> 0x009d }
            java.lang.String r4 = "green"
            int r4 = r3.getInt(r4)     // Catch:{ JSONException -> 0x0084, NullPointerException -> 0x009d }
            r2.bgGreen = r4     // Catch:{ JSONException -> 0x0084, NullPointerException -> 0x009d }
            java.lang.String r4 = "blue"
            int r4 = r3.getInt(r4)     // Catch:{ JSONException -> 0x0084, NullPointerException -> 0x009d }
            r2.bgBlue = r4     // Catch:{ JSONException -> 0x0084, NullPointerException -> 0x009d }
            java.lang.String r4 = "alpha"
            int r3 = r3.getInt(r4)     // Catch:{ JSONException -> 0x0084, NullPointerException -> 0x009d }
            int r3 = r3 * 255
            r2.bgAlpha = r3     // Catch:{ JSONException -> 0x0084, NullPointerException -> 0x009d }
            java.lang.String r3 = "text_color_rgb"
            org.json.JSONObject r1 = r1.getJSONObject(r3)     // Catch:{ JSONException -> 0x0084, NullPointerException -> 0x009d }
            java.lang.String r3 = "red"
            int r3 = r1.getInt(r3)     // Catch:{ JSONException -> 0x0084, NullPointerException -> 0x009d }
            r2.fgRed = r3     // Catch:{ JSONException -> 0x0084, NullPointerException -> 0x009d }
            java.lang.String r3 = "green"
            int r3 = r1.getInt(r3)     // Catch:{ JSONException -> 0x0084, NullPointerException -> 0x009d }
            r2.fgGreen = r3     // Catch:{ JSONException -> 0x0084, NullPointerException -> 0x009d }
            java.lang.String r3 = "blue"
            int r3 = r1.getInt(r3)     // Catch:{ JSONException -> 0x0084, NullPointerException -> 0x009d }
            r2.fgBlue = r3     // Catch:{ JSONException -> 0x0084, NullPointerException -> 0x009d }
            java.lang.String r3 = "alpha"
            int r1 = r1.getInt(r3)     // Catch:{ JSONException -> 0x0084, NullPointerException -> 0x009d }
            int r1 = r1 * 255
            r2.fgAlpha = r1     // Catch:{ JSONException -> 0x0084, NullPointerException -> 0x009d }
        L_0x0078:
            r5.a = r2     // Catch:{ JSONException -> 0x008d, NullPointerException -> 0x009d }
            java.lang.String r1 = "rations"
            org.json.JSONArray r0 = r0.getJSONArray(r1)     // Catch:{ JSONException -> 0x008d, NullPointerException -> 0x009d }
            r5.a(r0)     // Catch:{ JSONException -> 0x008d, NullPointerException -> 0x009d }
        L_0x0083:
            return
        L_0x0084:
            r1 = move-exception
            java.lang.String r3 = "Guohead SDK"
            java.lang.String r4 = "Exception in parsing config.extra JSON. This may or may not be fatal."
            android.util.Log.e(r3, r4, r1)     // Catch:{ JSONException -> 0x008d, NullPointerException -> 0x009d }
            goto L_0x0078
        L_0x008d:
            r0 = move-exception
            java.lang.String r1 = "Guohead SDK"
            java.lang.String r2 = "Unable to parse response from JSON. This may or may not be fatal."
            android.util.Log.e(r1, r2, r0)
            com.guohead.sdk.obj.Extra r0 = new com.guohead.sdk.obj.Extra
            r0.<init>()
            r5.a = r0
            goto L_0x0083
        L_0x009d:
            r0 = move-exception
            java.lang.String r1 = "Guohead SDK"
            java.lang.String r2 = "Unable to parse response from JSON. This may or may not be fatal."
            android.util.Log.e(r1, r2, r0)
            com.guohead.sdk.obj.Extra r0 = new com.guohead.sdk.obj.Extra
            r0.<init>()
            r5.a = r0
            goto L_0x0083
        */
        throw new UnsupportedOperationException("Method not decompiled: com.guohead.sdk.GuoheAdManager.a(java.lang.String):void");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private void a(JSONArray jSONArray) {
        int i = 0;
        ArrayList arrayList = new ArrayList();
        this.c = 0;
        while (i < jSONArray.length()) {
            try {
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                if (jSONObject != null) {
                    Ration ration = new Ration();
                    ration.appid = jSONObject.getString("appid");
                    ration.nid = jSONObject.getString("nid");
                    ration.type = jSONObject.getInt("type");
                    ration.name = jSONObject.getString("nname");
                    ration.weight = jSONObject.getInt("weight");
                    ration.priority = jSONObject.getInt("priority");
                    Log.i("===>", "===>ration: " + ration.weight);
                    switch (ration.type) {
                        case 1:
                            String string = jSONObject.getString("key");
                            if (string.contains("|;|")) {
                                String[] split = string.split("\\|;\\|");
                                ration.key = split[0];
                                if (split.length > 1) {
                                    ration.key2 = split[1];
                                }
                            } else {
                                ration.key = jSONObject.getString("key");
                            }
                            this.c += ration.weight;
                            arrayList.add(ration);
                            break;
                        case 6:
                            String[] split2 = jSONObject.getString("key").split("\\|;\\|");
                            ration.key = split2[0];
                            ration.key2 = split2[1];
                            this.c += ration.weight;
                            arrayList.add(ration);
                            break;
                        case GuoheAdUtil.NETWORK_TYPE_CUSTOM /*9*/:
                        case GuoheAdUtil.NETWORK_TYPE_ADTOUCH /*91*/:
                            this.c += ration.weight;
                            arrayList.add(ration);
                            break;
                        case GuoheAdUtil.NETWORK_TYPE_EVENT /*17*/:
                            ration.key = jSONObject.getString("key");
                            this.c += ration.weight;
                            arrayList.add(ration);
                            break;
                        case GuoheAdUtil.NETWORK_TYPE_DOMOB /*92*/:
                            String[] split3 = jSONObject.getString("key").split("\\|;\\|");
                            ration.key = split3[0];
                            ration.key2 = split3[1];
                            this.c += ration.weight;
                            arrayList.add(ration);
                            break;
                        case GuoheAdUtil.NETWORK_TYPE_WIYUN /*93*/:
                            String string2 = jSONObject.getString("key");
                            if (string2.contains("|;|")) {
                                String[] split4 = string2.split("\\|;\\|");
                                ration.key = split4[0];
                                ration.key2 = split4[1];
                            } else {
                                ration.key = jSONObject.getString("key");
                            }
                            this.c += ration.weight;
                            arrayList.add(ration);
                            break;
                        case GuoheAdUtil.NETWORK_TYPE_MADHOUSE /*94*/:
                            String[] split5 = jSONObject.getString("key").split("\\|;\\|");
                            ration.key = split5[0];
                            ration.key2 = split5[1];
                            ration.key3 = split5[2];
                            this.c += ration.weight;
                            arrayList.add(ration);
                            break;
                        case GuoheAdUtil.NETWORK_TYPE_YOUMI /*95*/:
                        case GuoheAdUtil.NETWORK_TYPE_WOOBOO /*96*/:
                            String[] split6 = jSONObject.getString("key").split("\\|;\\|");
                            ration.key = split6[0];
                            ration.key2 = split6[1];
                            ration.appVersion = GuoheAdUtil.getAppVersion(this.d);
                            this.c += ration.weight;
                            arrayList.add(ration);
                            break;
                        case GuoheAdUtil.NETWORK_TYPE_ADCHINA /*97*/:
                            String[] split7 = jSONObject.getString("key").split("\\|;\\|");
                            ration.key = split7[0];
                            ration.key2 = split7[1];
                            this.c += ration.weight;
                            arrayList.add(ration);
                            break;
                        case GuoheAdUtil.NETWORK_TYPE_CASEE /*98*/:
                            ration.key = jSONObject.getString("key");
                            this.c += ration.weight;
                            arrayList.add(ration);
                            break;
                        default:
                            Log.w(GuoheAdUtil.GUOHEAD, "Don't know how to fetch key for unexpected ration type: " + ration.type);
                            break;
                    }
                }
                i++;
            } catch (JSONException e2) {
                Log.e(GuoheAdUtil.GUOHEAD, "JSONException in parsing config.rations JSON. This may or may not be fatal.", e2);
            }
        }
        Collections.sort(arrayList);
        this.b = arrayList;
        this.e = this.b.iterator();
    }

    private Custom b(String str) {
        Log.d(GuoheAdUtil.GUOHEAD, "Received custom jsonString: " + str);
        Custom custom = new Custom();
        try {
            JSONObject jSONObject = new JSONObject(str);
            custom.type = jSONObject.getInt("ad_type");
            custom.imageLink = jSONObject.getString("img_url");
            custom.link = jSONObject.getString("redirect_url");
            custom.description = jSONObject.getString("ad_text");
            custom.image = c(custom.imageLink);
            return custom;
        } catch (JSONException e2) {
            Log.e(GuoheAdUtil.GUOHEAD, "Caught JSONException in parseCustomJsonString()", e2);
            return null;
        }
    }

    private static Drawable c(String str) {
        try {
            return Drawable.createFromStream((InputStream) new URL(str).getContent(), "src");
        } catch (Exception e2) {
            Log.e(GuoheAdUtil.GUOHEAD, "Unable to fetchImage(): ", e2);
            return null;
        }
    }

    public static void finish(Context context) {
        try {
            GuoheAdAdapter.finish(context);
        } catch (Exception e2) {
            Log.v(GuoheAdUtil.GUOHEAD, "Finish Exception: " + e2.toString());
        }
    }

    public void HttpfetchConfig() {
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            String format = String.format(GuoheAdUtil.urlConfig, this.keyGuoheAd, GuoheAdUtil.deviceIDHash, URLEncoder.encode(GuoheAdUtil.model), URLEncoder.encode(GuoheAdUtil.SDK), GuoheAdUtil.network, GuoheAdUtil.ghVersion);
            Log.i(GuoheAdUtil.GUOHEAD, "=======> request to server: " + format);
            HttpResponse execute = defaultHttpClient.execute(new HttpGet(format));
            Log.d(GuoheAdUtil.GUOHEAD, execute.getStatusLine().toString());
            HttpEntity entity = execute.getEntity();
            if (entity != null) {
                String a2 = a(entity.getContent());
                a(a2);
                Log.d(GuoheAdUtil.GUOHEAD, "Received jsonString: " + a2);
                this.data.edit().putString("GHConfig", a2).commit();
            }
        } catch (ClientProtocolException e2) {
            Log.e(GuoheAdUtil.GUOHEAD, "Caught ClientProtocolException in fetchConfig()", e2);
        } catch (IOException e3) {
            Log.e(GuoheAdUtil.GUOHEAD, "Caught IOException in fetchConfig()", e3);
        } catch (Exception e4) {
            Log.e(GuoheAdUtil.GUOHEAD, "Caught Exception in HttpfetchConfig()", e4);
        }
    }

    public void fetchConfig() {
        String str = "{}";
        try {
            this.data = this.d.getSharedPreferences("GHData", 0);
            if (this.data != null) {
                str = this.data.getString("GHConfig", "{}");
            }
            if (str != null && str.length() > 2) {
                a(str);
                Log.d(GuoheAdUtil.GUOHEAD, "Readed jsonString: " + str);
            }
            HttpfetchConfig();
        } catch (Exception e2) {
            Log.e(GuoheAdUtil.GUOHEAD, "Caught Exception in fetchConfig()", e2);
        }
    }

    public Custom getCustom(Ration ration) {
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        String format = String.format(GuoheAdUtil.urlCustom, ration.appid, ration.nid, GuoheAdUtil.deviceIDHash, GuoheAdUtil.locale, URLEncoder.encode(GuoheAdUtil.model), URLEncoder.encode(GuoheAdUtil.SDK), GuoheAdUtil.network, GuoheAdUtil.ghVersion);
        HttpGet httpGet = new HttpGet(format);
        Log.i(GuoheAdUtil.GUOHEAD, "=======> request to server: " + format);
        try {
            HttpResponse execute = defaultHttpClient.execute(httpGet);
            Log.d(GuoheAdUtil.GUOHEAD, execute.getStatusLine().toString());
            HttpEntity entity = execute.getEntity();
            if (entity != null) {
                return b(a(entity.getContent()));
            }
        } catch (ClientProtocolException e2) {
            Log.e(GuoheAdUtil.GUOHEAD, "Caught ClientProtocolException in getCustom()", e2);
        } catch (IOException e3) {
            Log.e(GuoheAdUtil.GUOHEAD, "Caught IOException in getCustom()", e3);
        }
        return null;
    }

    public Extra getExtra() {
        if (this.c > 0) {
            return this.a;
        }
        Log.i(GuoheAdUtil.GUOHEAD, "Sum of ration weights is 0 - no ads to be shown");
        return null;
    }

    public Location getLocation() {
        if (this.d == null) {
            return null;
        }
        if (this.d.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) {
            return ((LocationManager) this.d.getSystemService("location")).getLastKnownLocation("gps");
        }
        if (this.d.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
            return ((LocationManager) this.d.getSystemService("location")).getLastKnownLocation("network");
        }
        return null;
    }

    public Ration getRation() {
        int nextInt = new Random().nextInt(this.c) + 1;
        int i = 0;
        Ration ration = null;
        for (Ration ration2 : this.b) {
            int i2 = ration2.weight + i;
            if (i2 >= nextInt) {
                return ration2;
            }
            i = i2;
            ration = ration2;
        }
        return ration;
    }

    public Ration getRollover() {
        if (this.e == null) {
            return null;
        }
        if (this.e.hasNext()) {
            return (Ration) this.e.next();
        }
        return null;
    }

    public void resetRollover() {
        this.e = this.b.iterator();
    }
}
