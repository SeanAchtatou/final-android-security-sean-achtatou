package com.guohead.sdk;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.guohead.sdk.adapters.GuoheAdAdapter;
import com.guohead.sdk.obj.Custom;
import com.guohead.sdk.obj.Extra;
import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.GuoheAdUtil;
import java.util.Timer;

public class GuoheAdLayout extends RelativeLayout implements GuoheAdStateListener {
    public String ClickedLink = "http://www.guohead.com";
    /* access modifiers changed from: private */
    public Runnable a;
    public Ration activeRation;
    public final Activity activity;
    private boolean b;
    private boolean c;
    public Custom custom;
    /* access modifiers changed from: private */
    public String d;
    public Extra extra;
    public GuoheAdInterface guoheAdInterface;
    public GuoheAdStateListener guoheAdListener;
    public GuoheAdManager guoheAdManager;
    public Handler handler;
    public Ration nextRation;
    public ViewGroup nextView;
    public RelativeLayout superView;
    public Ration tempRation;
    public Runnable viewRunnable;

    public interface GuoheAdInterface {
        void guoheAdGeneric();
    }

    public GuoheAdLayout(Activity activity2) {
        super(activity2);
        this.d = GuoheAdUtil.getAppKey(activity2);
        this.activity = activity2;
        this.superView = this;
        this.b = true;
        this.c = true;
        this.handler = new Handler();
        this.a = new a(this);
        this.viewRunnable = new c(this);
        new d(this, GuoheAdUtil.GUOHEAD, activity2).start();
        setHorizontalScrollBarEnabled(false);
        setVerticalScrollBarEnabled(false);
    }

    private void a() {
        new j(this, GuoheAdUtil.GUOHEAD).start();
        new Timer().schedule(new i(this), 1000);
        if (this.guoheAdListener != null) {
            this.guoheAdListener.onClick();
        }
    }

    static /* synthetic */ void a(GuoheAdLayout guoheAdLayout) {
        guoheAdLayout.tempRation = guoheAdLayout.nextRation;
        if (guoheAdLayout.nextRation == null) {
            Log.e(GuoheAdUtil.GUOHEAD, "nextRation is null!");
            guoheAdLayout.rotateThreadedDelayed();
            return;
        }
        Log.d(GuoheAdUtil.GUOHEAD, String.format("Showing ad:\n\tnid: %s\n\tname: %s\n\ttype: %d\n\tkey: %s\n\tkey2: %s\n\tkey3: %s", guoheAdLayout.nextRation.nid, guoheAdLayout.nextRation.name, Integer.valueOf(guoheAdLayout.nextRation.type), guoheAdLayout.nextRation.key, guoheAdLayout.nextRation.key2, guoheAdLayout.nextRation.key3));
        try {
            GuoheAdAdapter.handle(guoheAdLayout, guoheAdLayout.nextRation);
        } catch (Throwable th) {
            Log.w(GuoheAdUtil.GUOHEAD, "Caught an exception in adapter:", th);
            guoheAdLayout.rolloverThreaded();
        }
    }

    public void countImpressionThreaded() {
        Log.d(GuoheAdUtil.GUOHEAD, "Sending metrics request for impression");
        new h(this, GuoheAdUtil.GUOHEAD).start();
        if (this.guoheAdListener != null) {
            this.guoheAdListener.onRefreshAd();
        }
    }

    public void onClick() {
    }

    public void onFail() {
        new b(this, GuoheAdUtil.GUOHEAD).start();
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                if (this.activeRation == null) {
                    return false;
                }
                if (this.activeRation.type == 9) {
                    if (this.custom == null || this.custom.link == null) {
                        Log.w(GuoheAdUtil.GUOHEAD, "In onInterceptTouchEvent(), but custom or custom.link is null");
                    } else if (this.custom.type != 3) {
                        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(this.custom.link));
                        intent.addFlags(268435456);
                        try {
                            this.activity.startActivity(intent);
                        } catch (Exception e) {
                            Log.w(GuoheAdUtil.GUOHEAD, "Could not handle click to " + this.custom.link, e);
                        }
                    }
                }
                if (!TextUtils.isEmpty(this.activeRation.key2)) {
                    if (this.activeRation.type == 94) {
                        if (Boolean.getBoolean(this.activeRation.key3)) {
                            return false;
                        }
                    } else if (Boolean.getBoolean(this.activeRation.key2)) {
                        return false;
                    }
                }
                a();
                return false;
            default:
                return false;
        }
    }

    public void onReceiveAd() {
    }

    public void onRefreshAd() {
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i) {
        if (i == 0) {
            this.b = true;
            if (!this.c) {
                this.c = true;
                rotateThreadedNow();
                return;
            }
            return;
        }
        this.b = false;
    }

    public void pushSubView(ViewGroup viewGroup) {
        this.superView.removeAllViews();
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(13);
        this.superView.addView(viewGroup, layoutParams);
        this.activeRation = this.nextRation;
        countImpressionThreaded();
    }

    public void rollover() {
        this.nextRation = this.guoheAdManager.getRollover();
        this.handler.post(this.a);
    }

    public void rolloverThreaded() {
        new g(this, GuoheAdUtil.GUOHEAD).start();
    }

    public void rotateAd() {
        if (!this.b) {
            this.c = false;
            return;
        }
        Log.i(GuoheAdUtil.GUOHEAD, "Rotating Ad");
        try {
            this.nextRation = this.guoheAdManager.getRation();
            this.handler.post(this.a);
        } catch (Exception e) {
            Log.v(GuoheAdUtil.GUOHEAD, "Rotating Ad Exception");
        }
        if (this.guoheAdListener != null) {
            this.guoheAdListener.onReceiveAd();
        }
    }

    public void rotateThreadedDelayed() {
        new f(this, GuoheAdUtil.GUOHEAD).start();
    }

    public void rotateThreadedNow() {
        new e(this, GuoheAdUtil.GUOHEAD).start();
    }

    public void setGuoheAdInterface(GuoheAdInterface guoheAdInterface2) {
        this.guoheAdInterface = guoheAdInterface2;
    }

    public void setListener(GuoheAdStateListener guoheAdStateListener) {
        this.guoheAdListener = guoheAdStateListener;
    }
}
