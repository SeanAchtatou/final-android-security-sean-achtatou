package com.guohead.sdk;

public interface GuoheAdStateListener {
    void onClick();

    void onFail();

    void onReceiveAd();

    void onRefreshAd();
}
