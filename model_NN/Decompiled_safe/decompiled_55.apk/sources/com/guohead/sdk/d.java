package com.guohead.sdk;

import android.app.Activity;
import android.util.Log;
import com.guohead.sdk.util.GuoheAdUtil;

final class d extends Thread {
    private /* synthetic */ Activity a;
    private /* synthetic */ GuoheAdLayout b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    d(GuoheAdLayout guoheAdLayout, String str, Activity activity) {
        super(str);
        this.b = guoheAdLayout;
        this.a = activity;
    }

    public final void run() {
        this.b.guoheAdManager = new GuoheAdManager(this.a, this.b.d);
        this.b.extra = this.b.guoheAdManager.getExtra();
        if (this.b.extra == null) {
            Log.e(GuoheAdUtil.GUOHEAD, "Unable to get configuration info or bad info, exiting GuoheAd");
        } else {
            this.b.rotateAd();
        }
    }
}
