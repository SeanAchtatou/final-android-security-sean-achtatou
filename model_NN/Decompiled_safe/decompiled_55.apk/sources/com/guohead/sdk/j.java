package com.guohead.sdk;

import android.util.Log;
import com.guohead.sdk.util.GuoheAdUtil;
import java.io.IOException;
import java.net.URLEncoder;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

final class j extends Thread {
    private /* synthetic */ GuoheAdLayout a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    j(GuoheAdLayout guoheAdLayout, String str) {
        super(str);
        this.a = guoheAdLayout;
    }

    public final void run() {
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        String format = String.format(GuoheAdUtil.urlClick, this.a.activeRation.appid, this.a.activeRation.nid, Integer.valueOf(this.a.activeRation.type), GuoheAdUtil.deviceIDHash, GuoheAdUtil.locale, URLEncoder.encode(GuoheAdUtil.model), URLEncoder.encode(GuoheAdUtil.SDK), GuoheAdUtil.network, GuoheAdUtil.ghVersion);
        Log.d(GuoheAdUtil.GUOHEAD, "++++++++++Click: " + format);
        try {
            defaultHttpClient.execute(new HttpGet(format));
        } catch (ClientProtocolException e) {
            Log.e(GuoheAdUtil.GUOHEAD, "Caught ClientProtocolException in countClickThreaded()", e);
        } catch (IOException e2) {
            Log.e(GuoheAdUtil.GUOHEAD, "Caught IOException in countClickThreaded()", e2);
        }
    }
}
