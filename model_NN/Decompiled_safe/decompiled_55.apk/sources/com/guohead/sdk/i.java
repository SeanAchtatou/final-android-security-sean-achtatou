package com.guohead.sdk;

import android.util.Log;
import com.guohead.sdk.util.GuoheAdUtil;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.TimerTask;

final class i extends TimerTask {
    private /* synthetic */ GuoheAdLayout a;

    i(GuoheAdLayout guoheAdLayout) {
        this.a = guoheAdLayout;
    }

    public final void run() {
        try {
            StringBuilder sb = new StringBuilder();
            try {
                ArrayList arrayList = new ArrayList();
                arrayList.add("logcat");
                arrayList.add("-d");
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec((String[]) arrayList.toArray(new String[0])).getInputStream()));
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    } else if (readLine.contains("act=android.intent.action.VIEW dat=")) {
                        String str = readLine.split(" ")[7];
                        sb.append(str.substring(4, str.length()));
                        sb.append(" ");
                    }
                }
            } catch (IOException e) {
                Log.e("LOG", "CollectLogTask.doInBackground failed", e);
            }
            if (sb != null) {
                int max = Math.max(sb.length() - 100000, 0);
                if (max > 0) {
                    sb.delete(0, max);
                }
                String[] split = sb.toString().split(" ");
                int length = split.length;
                this.a.ClickedLink = split[length - 1];
            }
        } catch (Exception e2) {
            Log.e(GuoheAdUtil.GUOHEAD, e2.toString());
        }
    }
}
