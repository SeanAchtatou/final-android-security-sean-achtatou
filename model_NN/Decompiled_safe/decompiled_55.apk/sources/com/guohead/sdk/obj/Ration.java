package com.guohead.sdk.obj;

public class Ration implements Comparable {
    public String appVersion = "v1.0";
    public String appid = "";
    public String key = "";
    public String key2 = "";
    public String key3 = "";
    public String name = "";
    public String nid = "";
    public int priority = 0;
    public String sdkVersion = "";
    public int type = 0;
    public int weight = 0;

    public int compareTo(Ration ration) {
        int i = ration.priority;
        if (this.priority < i) {
            return -1;
        }
        return this.priority > i ? 1 : 0;
    }
}
