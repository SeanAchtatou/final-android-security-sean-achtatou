package com.guohead.sdk.util;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

public class GuoheAdUtil {
    public static final int CUSTOM_TYPE_BANNER = 1;
    public static final int CUSTOM_TYPE_CHOICE = 3;
    public static final int CUSTOM_TYPE_ICON = 2;
    public static final String GUOHEAD = "Guohead SDK";
    public static final int NETWORK_TYPE_4THSCREEN = 13;
    public static final int NETWORK_TYPE_ADCHINA = 97;
    public static final int NETWORK_TYPE_ADMOB = 1;
    public static final int NETWORK_TYPE_ADSENSE = 14;
    public static final int NETWORK_TYPE_ADTOUCH = 91;
    public static final int NETWORK_TYPE_CASEE = 98;
    public static final int NETWORK_TYPE_CUSTOM = 9;
    public static final int NETWORK_TYPE_DOMOB = 92;
    public static final int NETWORK_TYPE_DOUBLECLICK = 15;
    public static final int NETWORK_TYPE_EVENT = 17;
    public static final int NETWORK_TYPE_GENERIC = 16;
    public static final int NETWORK_TYPE_GREYSTRIP = 7;
    public static final int NETWORK_TYPE_GUOHEAD = 10;
    public static final int NETWORK_TYPE_JUMPTAP = 2;
    public static final int NETWORK_TYPE_LIVERAIL = 5;
    public static final int NETWORK_TYPE_MADHOUSE = 94;
    public static final int NETWORK_TYPE_MDOTM = 12;
    public static final int NETWORK_TYPE_MEDIALETS = 4;
    public static final int NETWORK_TYPE_MILLENNIAL = 6;
    public static final int NETWORK_TYPE_MOBCLIX = 11;
    public static final int NETWORK_TYPE_QUATTRO = 8;
    public static final int NETWORK_TYPE_VIDEOEGG = 3;
    public static final int NETWORK_TYPE_WIYUN = 93;
    public static final int NETWORK_TYPE_WOOBOO = 96;
    public static final int NETWORK_TYPE_YOUMI = 95;
    public static String SDK = "Unknow SDK";
    public static String appKey = "00000000000000000000000000000000";
    public static String appVersion = "Unknow App Version";
    public static String deviceIDHash = "00000000000000000000000000000000";
    public static final String ghVersion = "v1.5.0";
    public static String locale = "zh_CN";
    public static final String locationString = "&location=%f,%f&location_timestamp=%d";
    public static String model = "Unknow Device";
    public static String network = "UnknowModel";
    public static final String urlClick = "http://mob.guohead.com/exclick.php?appid=%s&nid=%s&type=%d&uuid=%s&country_code=%s&client=2&model=%s&sdk=%s&network=%s&ghver=%s";
    public static final String urlConfig = "http://mob.guohead.com/getInfo.php?appid=%s&uuid=%s&client=2&model=%s&sdk=%s&network=%s&ghver=%s";
    public static final String urlCustom = "http://mob.guohead.com/custom.php?appid=%s&nid=%s&uuid=%s&country_code=%s&client=2&model=%s&sdk=%s&network=%s&ghver=%s";
    public static final String urlFail = "http://mob.guohead.com/exfail.php?appid=%s&nid=%s&type=%d&uuid=%s&country_code=%s&client=2&model=%s&sdk=%s&network=%s&ghver=%s";
    public static final String urlImpression = "http://mob.guohead.com/exmet.php?appid=%s&nid=%s&type=%d&uuid=%s&country_code=%s&client=2&model=%s&sdk=%s&network=%s&ghver=%s";

    public static String convertToHex(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < bArr.length; i++) {
            byte b = (bArr[i] >>> 4) & 15;
            int i2 = 0;
            while (true) {
                if (b < 0 || b > 9) {
                    stringBuffer.append((char) ((b - 10) + 97));
                } else {
                    stringBuffer.append((char) (b + 48));
                }
                b = bArr[i] & 15;
                int i3 = i2 + 1;
                if (i2 > 0) {
                    break;
                }
                i2 = i3;
            }
        }
        return stringBuffer.toString();
    }

    public static String getAppKey(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo.metaData.containsKey("GH_APPKEY")) {
                String string = applicationInfo.metaData.getString("GH_APPKEY");
                appKey = string;
                if (string == null || appKey.length() != 32) {
                    appKey = "00000000000000000000000000000000";
                    Log.e(GUOHEAD, "Guohead Key's length does not equals 32 characters");
                }
                return appKey;
            }
            throw new RuntimeException("GH_APPKEY not found,please set the meta-data GH_APPKEY in AndroidManifest.xml");
        } catch (Exception e) {
            Log.v(GUOHEAD, e.toString());
        }
    }

    public static String getAppVersion(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "1.0";
        }
    }

    public static String getNetworkAccessMode(Context context) {
        try {
            if (context.getPackageManager().checkPermission("android.permission.ACCESS_NETWORK_STATE", context.getPackageName()) != 0) {
                return "Unknown";
            }
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager == null) {
                return "Unknown";
            }
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            String lowerCase = activeNetworkInfo.getTypeName().toLowerCase();
            String lowerCase2 = !lowerCase.equals("wifi") ? activeNetworkInfo.getExtraInfo().toLowerCase() : lowerCase;
            return lowerCase2 == null ? "Unknown" : lowerCase2;
        } catch (Exception e) {
            Log.v(GUOHEAD, "getNetworkAccessMode Error");
            return "Unknown";
        }
    }

    public static void init(Context context) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            StringBuffer stringBuffer = new StringBuffer("android_id");
            stringBuffer.append("GuoheAd");
            deviceIDHash = convertToHex(instance.digest(stringBuffer.toString().getBytes()));
            Log.d(GUOHEAD, "Hashed device ID is: " + deviceIDHash);
            model = Build.MODEL;
            SDK = Build.VERSION.RELEASE;
            appKey = getAppKey(context);
            appVersion = getAppVersion(context);
            locale = Locale.getDefault().toString();
            network = getNetworkAccessMode(context);
        } catch (NoSuchAlgorithmException e) {
            Log.e(GUOHEAD, e.toString());
        }
    }
}
