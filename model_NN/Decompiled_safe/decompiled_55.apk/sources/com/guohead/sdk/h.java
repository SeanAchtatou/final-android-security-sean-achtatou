package com.guohead.sdk;

import android.util.Log;
import com.guohead.sdk.util.GuoheAdUtil;
import java.io.IOException;
import java.net.URLEncoder;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

final class h extends Thread {
    private /* synthetic */ GuoheAdLayout a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    h(GuoheAdLayout guoheAdLayout, String str) {
        super(str);
        this.a = guoheAdLayout;
    }

    public final void run() {
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        String format = String.format(GuoheAdUtil.urlImpression, this.a.activeRation.appid, this.a.activeRation.nid, Integer.valueOf(this.a.activeRation.type), GuoheAdUtil.deviceIDHash, GuoheAdUtil.locale, URLEncoder.encode(GuoheAdUtil.model), URLEncoder.encode(GuoheAdUtil.SDK), GuoheAdUtil.network, GuoheAdUtil.ghVersion);
        Log.d(GuoheAdUtil.GUOHEAD, "++++++++++Impression: " + format);
        try {
            defaultHttpClient.execute(new HttpGet(format));
        } catch (ClientProtocolException e) {
            Log.e(GuoheAdUtil.GUOHEAD, "Caught ClientProtocolException in countImpressionThreaded()", e);
        } catch (IOException e2) {
            Log.e(GuoheAdUtil.GUOHEAD, "Caught IOException in countImpressionThreaded()", e2);
        }
    }
}
