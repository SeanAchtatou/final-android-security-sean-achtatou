package com.guohead.sdk;

import android.util.Log;
import com.guohead.sdk.util.GuoheAdUtil;
import java.io.IOException;
import java.net.URLEncoder;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

final class b extends Thread {
    private /* synthetic */ GuoheAdLayout a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    b(GuoheAdLayout guoheAdLayout, String str) {
        super(str);
        this.a = guoheAdLayout;
    }

    public final void run() {
        if (this.a.tempRation == null) {
            Log.d("===========> ", " tempRation null la!");
            return;
        }
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        String format = String.format(GuoheAdUtil.urlFail, this.a.tempRation.appid, this.a.tempRation.nid, Integer.valueOf(this.a.tempRation.type), GuoheAdUtil.deviceIDHash, GuoheAdUtil.locale, URLEncoder.encode(GuoheAdUtil.model), URLEncoder.encode(GuoheAdUtil.SDK), GuoheAdUtil.network, GuoheAdUtil.ghVersion);
        Log.d(GuoheAdUtil.GUOHEAD, "++++++++++Failure: " + format);
        try {
            defaultHttpClient.execute(new HttpGet(format));
        } catch (ClientProtocolException e) {
            Log.e(GuoheAdUtil.GUOHEAD, "Caught ClientProtocolException in countClickThreaded()", e);
        } catch (IOException e2) {
            Log.e(GuoheAdUtil.GUOHEAD, "Caught IOException in countClickThreaded()", e2);
        }
        this.a.tempRation = null;
    }
}
