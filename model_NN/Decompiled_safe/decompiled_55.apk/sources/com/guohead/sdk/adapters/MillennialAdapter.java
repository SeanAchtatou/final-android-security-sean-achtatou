package com.guohead.sdk.adapters;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import com.guohead.sdk.GuoheAdLayout;
import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.GuoheAdUtil;
import com.millennialmedia.android.MMAdView;
import java.util.Hashtable;

public class MillennialAdapter extends GuoheAdAdapter implements MMAdView.MMAdListener {
    public MMAdView adView;

    public MillennialAdapter(GuoheAdLayout guoheAdLayout, Ration ration) {
        super(guoheAdLayout, ration);
    }

    public void MMAdClickedToNewBrowser(MMAdView mMAdView) {
        Log.d(GuoheAdUtil.GUOHEAD, "Millennial Ad clicked, new browser launched");
    }

    public void MMAdClickedToOverlay(MMAdView mMAdView) {
        Log.d(GuoheAdUtil.GUOHEAD, "Millennial Ad Clicked to overlay");
    }

    public void MMAdFailed(MMAdView mMAdView) {
        Log.d(GuoheAdUtil.GUOHEAD, "Millennial failure");
        mMAdView.setListener((MMAdView.MMAdListener) null);
        this.guoheAdLayout.removeView(mMAdView);
        this.guoheAdLayout.rollover();
    }

    public void MMAdOverlayLaunched(MMAdView mMAdView) {
        Log.d(GuoheAdUtil.GUOHEAD, "Millennial Ad Overlay Launched");
    }

    public void MMAdReturned(MMAdView mMAdView) {
        Log.d(GuoheAdUtil.GUOHEAD, "Millennial success");
        mMAdView.setListener((MMAdView.MMAdListener) null);
        this.guoheAdLayout.activity.runOnUiThread(new o(this, mMAdView));
    }

    public void finish() {
        this.adView.halt();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{com.guohead.sdk.GuoheAdLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void}
     arg types: [com.millennialmedia.android.MMAdView, android.view.ViewGroup$LayoutParams]
     candidates:
      ClspMth{android.view.ViewGroup.addView(android.view.View, int):void}
      SimpleMethodDetails{com.guohead.sdk.GuoheAdLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void} */
    public void handle() {
        int i = this.guoheAdLayout.extra.cycleTime;
        int i2 = i < 30 ? 30 : i;
        Hashtable hashtable = new Hashtable();
        if (!TextUtils.isEmpty(this.ration.key2)) {
            hashtable.put("height", "53");
            hashtable.put("width", "320");
            hashtable.put("keywords", this.ration.key2);
        }
        this.adView = new MMAdView(this.guoheAdLayout.activity, this.ration.key, "MMBannerAdTop", i2, false, hashtable);
        this.adView.setListener(this);
        this.adView.callForAd();
        this.adView.setHorizontalScrollBarEnabled(false);
        this.adView.setVerticalScrollBarEnabled(false);
        this.guoheAdLayout.addView((View) this.adView, new ViewGroup.LayoutParams(-2, -2));
    }
}
