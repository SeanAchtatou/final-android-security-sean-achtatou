package com.guohead.sdk.adapters;

final class l implements Runnable {
    private /* synthetic */ DomobAdapter a;

    l(DomobAdapter domobAdapter) {
        this.a = domobAdapter;
    }

    public final void run() {
        this.a.guoheAdLayout.removeView(this.a.a);
        this.a.a.setVisibility(0);
        this.a.guoheAdLayout.guoheAdManager.resetRollover();
        this.a.guoheAdLayout.nextView = this.a.a;
        this.a.guoheAdLayout.handler.post(this.a.guoheAdLayout.viewRunnable);
        this.a.guoheAdLayout.rotateThreadedDelayed();
    }
}
