package com.guohead.sdk.adapters;

import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.guohead.sdk.GuoheAdLayout;
import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.GuoheAdUtil;
import com.madhouse.android.ads.AdView;

public class CustomAdapter extends GuoheAdAdapter {
    /* access modifiers changed from: private */
    public Runnable a;

    public CustomAdapter(GuoheAdLayout guoheAdLayout, Ration ration) {
        super(guoheAdLayout, ration);
    }

    public void displayCustom() {
        switch (this.guoheAdLayout.custom.type) {
            case 1:
                Log.d(GuoheAdUtil.GUOHEAD, "Serving custom type: banner");
                RelativeLayout relativeLayout = new RelativeLayout(this.guoheAdLayout.activity);
                if (this.guoheAdLayout.custom.image != null) {
                    ImageView imageView = new ImageView(this.guoheAdLayout.activity);
                    imageView.setImageDrawable(this.guoheAdLayout.custom.image);
                    imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    relativeLayout.addView(imageView, new RelativeLayout.LayoutParams(-2, -2));
                    this.guoheAdLayout.pushSubView(relativeLayout);
                    break;
                } else {
                    this.guoheAdLayout.rotateThreadedNow();
                    return;
                }
            case 2:
                Log.d(GuoheAdUtil.GUOHEAD, "Serving custom type: icon");
                RelativeLayout relativeLayout2 = new RelativeLayout(this.guoheAdLayout.activity);
                if (this.guoheAdLayout.custom.image != null) {
                    relativeLayout2.setLayoutParams(new FrameLayout.LayoutParams((int) AdView.AD_MEASURE_320, 50));
                    ImageView imageView2 = new ImageView(this.guoheAdLayout.activity);
                    int rgb = Color.rgb(this.guoheAdLayout.extra.bgRed, this.guoheAdLayout.extra.bgGreen, this.guoheAdLayout.extra.bgBlue);
                    imageView2.setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-1, rgb, rgb, rgb}));
                    relativeLayout2.addView(imageView2, new RelativeLayout.LayoutParams(-1, -1));
                    ImageView imageView3 = new ImageView(this.guoheAdLayout.activity);
                    imageView3.setImageDrawable(this.guoheAdLayout.custom.image);
                    imageView3.setId(10);
                    imageView3.setPadding(4, 0, 7, 0);
                    imageView3.setScaleType(ImageView.ScaleType.CENTER);
                    relativeLayout2.addView(imageView3, new RelativeLayout.LayoutParams(-2, -1));
                    TextView textView = new TextView(this.guoheAdLayout.activity);
                    textView.setMaxLines(2);
                    textView.setPadding(0, 0, 4, 0);
                    textView.setText(this.guoheAdLayout.custom.description);
                    textView.setTypeface(Typeface.DEFAULT_BOLD, 1);
                    textView.setTextColor(Color.rgb(this.guoheAdLayout.extra.fgRed, this.guoheAdLayout.extra.fgGreen, this.guoheAdLayout.extra.fgBlue));
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
                    layoutParams.addRule(1, imageView3.getId());
                    layoutParams.addRule(10);
                    layoutParams.addRule(12);
                    layoutParams.addRule(15);
                    layoutParams.addRule(13);
                    layoutParams.setMargins(0, 0, 5, 0);
                    textView.setGravity(16);
                    relativeLayout2.addView(textView, layoutParams);
                    this.guoheAdLayout.pushSubView(relativeLayout2);
                    break;
                } else {
                    this.guoheAdLayout.rotateThreadedNow();
                    return;
                }
            case 3:
                Log.d(GuoheAdUtil.GUOHEAD, "Serving custom type: choice");
                RelativeLayout relativeLayout3 = new RelativeLayout(this.guoheAdLayout.activity);
                if (this.guoheAdLayout.custom.image != null) {
                    relativeLayout3.setLayoutParams(new RelativeLayout.LayoutParams((int) AdView.AD_MEASURE_320, 50));
                    ImageView imageView4 = new ImageView(this.guoheAdLayout.activity);
                    int rgb2 = Color.rgb(this.guoheAdLayout.extra.bgRed, this.guoheAdLayout.extra.bgGreen, this.guoheAdLayout.extra.bgBlue);
                    imageView4.setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-1, rgb2, rgb2, rgb2}));
                    relativeLayout3.addView(imageView4, new RelativeLayout.LayoutParams(-1, -1));
                    ImageView imageView5 = new ImageView(this.guoheAdLayout.activity);
                    imageView5.setImageDrawable(this.guoheAdLayout.custom.image);
                    imageView5.setId(10);
                    imageView5.setPadding(4, 3, 6, 0);
                    imageView5.setScaleType(ImageView.ScaleType.CENTER);
                    relativeLayout3.addView(imageView5, new RelativeLayout.LayoutParams(-2, -1));
                    ImageView imageView6 = new ImageView(this.guoheAdLayout.activity);
                    imageView6.setImageDrawable(this.guoheAdLayout.custom.image);
                    imageView6.setId(12);
                    imageView6.setPadding(5, 3, 5, 0);
                    imageView6.setScaleType(ImageView.ScaleType.CENTER);
                    RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -1);
                    layoutParams2.addRule(15);
                    layoutParams2.addRule(11);
                    relativeLayout3.addView(imageView6, layoutParams2);
                    imageView6.setOnClickListener(new i(this));
                    TextView textView2 = new TextView(this.guoheAdLayout.activity);
                    textView2.setMaxLines(2);
                    textView2.setId(11);
                    textView2.setText(this.guoheAdLayout.custom.description);
                    textView2.setTypeface(Typeface.DEFAULT_BOLD, 1);
                    textView2.setTextColor(Color.rgb(this.guoheAdLayout.extra.fgRed, this.guoheAdLayout.extra.fgGreen, this.guoheAdLayout.extra.fgBlue));
                    RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -1);
                    layoutParams3.addRule(1, imageView5.getId());
                    layoutParams3.addRule(0, imageView6.getId());
                    layoutParams3.addRule(15);
                    layoutParams3.addRule(13);
                    textView2.setGravity(16);
                    relativeLayout3.addView(textView2, layoutParams3);
                    this.guoheAdLayout.pushSubView(relativeLayout3);
                    break;
                } else {
                    this.guoheAdLayout.rotateThreadedNow();
                    return;
                }
            default:
                Log.w(GuoheAdUtil.GUOHEAD, "Unknown custom type!");
                this.guoheAdLayout.rotateThreadedNow();
                return;
        }
        this.guoheAdLayout.rotateThreadedDelayed();
    }

    public void finish() {
    }

    public void handle() {
        this.a = new g(this);
        new h(this).start();
    }
}
