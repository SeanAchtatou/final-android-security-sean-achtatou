package com.guohead.sdk.adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.ViewGroup;
import com.guohead.sdk.GuoheAdLayout;
import com.guohead.sdk.obj.Extra;
import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.GuoheAdUtil;
import net.youmi.android.AdListener;
import net.youmi.android.AdManager;
import net.youmi.android.AdView;

public class YouMiAdapter extends GuoheAdAdapter implements AdListener {
    /* access modifiers changed from: private */
    public AdView a;

    public YouMiAdapter(GuoheAdLayout guoheAdLayout, Ration ration) {
        super(guoheAdLayout, ration);
    }

    public void finish() {
        Log.i(GuoheAdUtil.GUOHEAD, getClass().getSimpleName() + "==> finish ");
        this.a.onDestroy();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{net.youmi.android.AdView.<init>(android.content.Context, int, int, int):void}
     arg types: [android.app.Activity, int, int, int]
     candidates:
      net.youmi.android.AdView.<init>(android.app.Activity, int, int, int):void
      SimpleMethodDetails{net.youmi.android.AdView.<init>(android.content.Context, int, int, int):void} */
    public void handle() {
        Extra extra = this.guoheAdLayout.extra;
        int rgb = Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue);
        int rgb2 = Color.rgb(extra.fgRed, extra.fgGreen, extra.fgBlue);
        Log.i(GuoheAdUtil.GUOHEAD, rgb + ", " + rgb2);
        int i = extra.cycleTime;
        if (i < 30) {
            i = 30;
        }
        AdManager.init(this.ration.key, this.ration.key2, i, false, this.ration.appVersion);
        this.a = new AdView((Context) this.guoheAdLayout.activity, rgb, rgb2, 255);
        this.a.setAdListener(this);
        this.guoheAdLayout.addView(this.a, new ViewGroup.LayoutParams(-2, -2));
    }

    public void onConnectFailed() {
        Log.d(GuoheAdUtil.GUOHEAD, "==========> YouMi failure");
        this.a.setAdListener(null);
        this.guoheAdLayout.activity.runOnUiThread(new t(this));
    }

    public void onReceiveAd() {
        Log.d(GuoheAdUtil.GUOHEAD, "========> YouMi success");
        this.a.setAdListener(null);
        this.guoheAdLayout.activity.runOnUiThread(new u(this));
    }
}
