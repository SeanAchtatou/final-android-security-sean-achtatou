package com.guohead.sdk.adapters;

import android.graphics.Color;
import android.util.Log;
import android.view.ViewGroup;
import cn.domob.android.ads.DomobAdListener;
import cn.domob.android.ads.DomobAdManager;
import cn.domob.android.ads.DomobAdView;
import com.guohead.sdk.GuoheAdLayout;
import com.guohead.sdk.obj.Extra;
import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.GuoheAdUtil;

public class DomobAdapter extends GuoheAdAdapter implements DomobAdListener {
    /* access modifiers changed from: private */
    public DomobAdView a;

    public DomobAdapter(GuoheAdLayout guoheAdLayout, Ration ration) {
        super(guoheAdLayout, ration);
    }

    public void finish() {
        Log.i(GuoheAdUtil.GUOHEAD, getClass().getSimpleName() + "==> finish ");
    }

    public void handle() {
        this.a = new DomobAdView(this.guoheAdLayout.activity);
        try {
            DomobAdManager.setPublisherId(this.ration.key);
            this.a.setAdListener(this);
            Extra extra = this.guoheAdLayout.extra;
            int rgb = Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue);
            int rgb2 = Color.rgb(extra.fgRed, extra.fgGreen, extra.fgBlue);
            int i = extra.cycleTime;
            if (i < 30) {
                i = 30;
            }
            this.a.setRequestInterval(i);
            this.a.setBackgroundColor(rgb);
            this.a.setPrimaryTextColor(rgb2);
            this.a.setKeywords(this.ration.key2);
            DomobAdManager.setIsTestMode(false);
            this.guoheAdLayout.addView(this.a, new ViewGroup.LayoutParams(-2, -2));
            this.a.requestFreshAd();
        } catch (IllegalArgumentException e) {
            this.guoheAdLayout.rollover();
        }
    }

    public void onFailedToReceiveFreshAd(DomobAdView domobAdView) {
        Log.d(GuoheAdUtil.GUOHEAD, "==========> onFailedToReceiveAd");
        notifyOnFail();
        this.a.setAdListener(null);
        clearAdStateListener();
        this.guoheAdLayout.activity.runOnUiThread(new k(this));
    }

    public void onReceivedFreshAd(DomobAdView domobAdView) {
        Log.d(GuoheAdUtil.GUOHEAD, "========> Wiyun success");
        this.a.setAdListener(null);
        clearAdStateListener();
        this.guoheAdLayout.activity.runOnUiThread(new l(this));
    }
}
