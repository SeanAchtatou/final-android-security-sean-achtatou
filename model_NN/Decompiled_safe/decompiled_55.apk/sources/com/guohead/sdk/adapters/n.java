package com.guohead.sdk.adapters;

import android.util.Log;
import com.guohead.sdk.util.GuoheAdUtil;

final class n implements Runnable {
    private /* synthetic */ MadHouseAdapter a;

    n(MadHouseAdapter madHouseAdapter) {
        this.a = madHouseAdapter;
    }

    public final void run() {
        Log.i(GuoheAdUtil.GUOHEAD, "-----> run thread");
        this.a.guoheAdLayout.removeView(this.a.a);
        this.a.a.setVisibility(0);
        this.a.guoheAdLayout.guoheAdManager.resetRollover();
        this.a.guoheAdLayout.nextView = this.a.a;
        this.a.guoheAdLayout.handler.post(this.a.guoheAdLayout.viewRunnable);
        this.a.guoheAdLayout.rotateThreadedDelayed();
    }
}
