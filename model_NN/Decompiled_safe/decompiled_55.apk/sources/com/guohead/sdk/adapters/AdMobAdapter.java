package com.guohead.sdk.adapters;

import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import com.admob.android.ads.AdListener;
import com.admob.android.ads.AdManager;
import com.admob.android.ads.AdView;
import com.guohead.sdk.GuoheAdLayout;
import com.guohead.sdk.obj.Extra;
import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.GuoheAdUtil;

public class AdMobAdapter extends GuoheAdAdapter implements AdListener {
    public AdMobAdapter(GuoheAdLayout guoheAdLayout, Ration ration) {
        super(guoheAdLayout, ration);
    }

    public void finish() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{com.guohead.sdk.GuoheAdLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void}
     arg types: [com.admob.android.ads.AdView, android.view.ViewGroup$LayoutParams]
     candidates:
      ClspMth{android.view.ViewGroup.addView(android.view.View, int):void}
      SimpleMethodDetails{com.guohead.sdk.GuoheAdLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void} */
    public void handle() {
        try {
            AdManager.setPublisherId(this.ration.key);
            Extra extra = this.guoheAdLayout.extra;
            int rgb = Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue);
            int rgb2 = Color.rgb(extra.fgRed, extra.fgGreen, extra.fgBlue);
            int i = extra.cycleTime;
            AdView adView = new AdView(this.guoheAdLayout.activity);
            adView.setAdListener(this);
            adView.setBackgroundColor(rgb);
            adView.setPrimaryTextColor(rgb2);
            adView.setKeywords(this.ration.key2);
            adView.setRequestInterval(i);
            adView.setVisibility(4);
            this.guoheAdLayout.addView((View) adView, new ViewGroup.LayoutParams(-2, -2));
        } catch (IllegalArgumentException e) {
            this.guoheAdLayout.rollover();
        }
    }

    public void onFailedToReceiveAd(AdView adView) {
        Log.d(GuoheAdUtil.GUOHEAD, "=======> AdMob failure");
        adView.setAdListener((AdListener) null);
        this.guoheAdLayout.removeView(adView);
        this.guoheAdLayout.rolloverThreaded();
    }

    public void onFailedToReceiveRefreshedAd(AdView adView) {
    }

    public void onReceiveAd(AdView adView) {
        Log.d(GuoheAdUtil.GUOHEAD, "========> AdMob success");
        adView.setAdListener((AdListener) null);
        this.guoheAdLayout.removeView(adView);
        adView.setVisibility(0);
        this.guoheAdLayout.guoheAdManager.resetRollover();
        this.guoheAdLayout.nextView = adView;
        this.guoheAdLayout.handler.post(this.guoheAdLayout.viewRunnable);
        this.guoheAdLayout.rotateThreadedDelayed();
    }

    public void onReceiveRefreshedAd(AdView adView) {
    }
}
