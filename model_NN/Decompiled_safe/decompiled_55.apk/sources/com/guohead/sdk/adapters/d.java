package com.guohead.sdk.adapters;

final class d implements Runnable {
    private /* synthetic */ AdTouchAdapter a;

    d(AdTouchAdapter adTouchAdapter) {
        this.a = adTouchAdapter;
    }

    public final void run() {
        this.a.guoheAdLayout.removeView(this.a.a);
        this.a.a.setVisibility(0);
        this.a.guoheAdLayout.guoheAdManager.resetRollover();
        this.a.guoheAdLayout.nextView = this.a.a;
        this.a.guoheAdLayout.handler.post(this.a.guoheAdLayout.viewRunnable);
        this.a.guoheAdLayout.rotateThreadedDelayed();
    }
}
