package com.guohead.sdk.adapters;

import android.content.Context;
import android.util.Log;
import com.guohead.sdk.GuoheAdLayout;
import com.guohead.sdk.GuoheAdStateListener;
import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.GuoheAdUtil;

public abstract class GuoheAdAdapter {
    private static GuoheAdAdapter b;
    private GuoheAdStateListener a;
    protected final GuoheAdLayout guoheAdLayout;
    protected Ration ration;

    public GuoheAdAdapter(GuoheAdLayout guoheAdLayout2, Ration ration2) {
        this.guoheAdLayout = guoheAdLayout2;
        this.a = guoheAdLayout2;
        this.ration = ration2;
    }

    private static GuoheAdAdapter a(GuoheAdLayout guoheAdLayout2, Ration ration2) {
        try {
            switch (ration2.type) {
                case 1:
                    return Class.forName("com.admob.android.ads.AdView") != null ? a("com.guohead.sdk.adapters.AdMobAdapter", guoheAdLayout2, ration2) : a(ration2);
                case 6:
                    return Class.forName("com.millennialmedia.android.MMAdView") != null ? a("com.guohead.sdk.adapters.MillennialAdapter", guoheAdLayout2, ration2) : a(ration2);
                case 8:
                    return Class.forName("com.qwapi.adclient.android.view.QWAdView") != null ? a("com.guohead.sdk.adapters.QuattroAdapter", guoheAdLayout2, ration2) : a(ration2);
                case GuoheAdUtil.NETWORK_TYPE_CUSTOM /*9*/:
                    return new CustomAdapter(guoheAdLayout2, ration2);
                case GuoheAdUtil.NETWORK_TYPE_EVENT /*17*/:
                    return new EventAdapter(guoheAdLayout2, ration2);
                case GuoheAdUtil.NETWORK_TYPE_ADTOUCH /*91*/:
                    return Class.forName("com.energysource.szj.embeded.AdView") != null ? a("com.guohead.sdk.adapters.AdTouchAdapter", guoheAdLayout2, ration2) : a(ration2);
                case GuoheAdUtil.NETWORK_TYPE_DOMOB /*92*/:
                    return Class.forName("cn.domob.android.ads.DomobAdView") != null ? a("com.guohead.sdk.adapters.DomobAdapter", guoheAdLayout2, ration2) : a(ration2);
                case GuoheAdUtil.NETWORK_TYPE_WIYUN /*93*/:
                    return Class.forName("com.wiyun.ad.AdView") != null ? a("com.guohead.sdk.adapters.WiyunAdapter", guoheAdLayout2, ration2) : a(ration2);
                case GuoheAdUtil.NETWORK_TYPE_MADHOUSE /*94*/:
                    return Class.forName("com.madhouse.android.ads.AdView") != null ? a("com.guohead.sdk.adapters.MadHouseAdapter", guoheAdLayout2, ration2) : a(ration2);
                case GuoheAdUtil.NETWORK_TYPE_YOUMI /*95*/:
                    return Class.forName("net.youmi.android.AdView") != null ? a("com.guohead.sdk.adapters.YouMiAdapter", guoheAdLayout2, ration2) : a(ration2);
                case GuoheAdUtil.NETWORK_TYPE_WOOBOO /*96*/:
                    return Class.forName("com.wooboo.adlib_android.WoobooAdView") != null ? a("com.guohead.sdk.adapters.WooBooAdapter", guoheAdLayout2, ration2) : a(ration2);
                case GuoheAdUtil.NETWORK_TYPE_ADCHINA /*97*/:
                    return Class.forName("com.adchina.android.ads.AdView") != null ? a("com.guohead.sdk.adapters.AdChinaAdapter", guoheAdLayout2, ration2) : a(ration2);
                case GuoheAdUtil.NETWORK_TYPE_CASEE /*98*/:
                    return Class.forName("com.casee.adsdk.CaseeAdView") != null ? a("com.guohead.sdk.adapters.CaseeAdapter", guoheAdLayout2, ration2) : a(ration2);
                default:
                    return a(ration2);
            }
        } catch (ClassNotFoundException e) {
            return a(ration2);
        } catch (VerifyError e2) {
            Log.e("GuoheAd", "YYY - Caught VerifyError", e2);
            return a(ration2);
        }
    }

    private static GuoheAdAdapter a(Ration ration2) {
        Log.w(GuoheAdUtil.GUOHEAD, "Unsupported ration type: " + ration2.type);
        return null;
    }

    private static GuoheAdAdapter a(String str, GuoheAdLayout guoheAdLayout2, Ration ration2) {
        try {
            return (GuoheAdAdapter) Class.forName(str).getConstructor(GuoheAdLayout.class, Ration.class).newInstance(guoheAdLayout2, ration2);
        } catch (Exception e) {
            Log.e("GuoheAdAdapter", "ERROR: " + e.getMessage());
            return null;
        }
    }

    public static void finish(Context context) {
        Log.i(GuoheAdUtil.GUOHEAD, "================> finish execute.");
        if (b != null) {
            b.a = null;
        }
        b.finish();
    }

    public static void handle(GuoheAdLayout guoheAdLayout2, Ration ration2) {
        if (b != null) {
            b.finish();
        }
        GuoheAdAdapter a2 = a(guoheAdLayout2, ration2);
        b = a2;
        if (a2 != null) {
            Log.d(GuoheAdUtil.GUOHEAD, "Valid adapter, calling handle()");
            b.handle();
            return;
        }
        throw new Exception("Invalid adapter");
    }

    /* access modifiers changed from: protected */
    public void clearAdStateListener() {
        this.a = null;
    }

    public abstract void finish();

    public abstract void handle();

    /* access modifiers changed from: protected */
    public void notifyOnClick() {
        if (this.a != null) {
            this.a.onClick();
        }
    }

    /* access modifiers changed from: protected */
    public void notifyOnFail() {
        if (this.a != null) {
            this.a.onFail();
        }
    }
}
