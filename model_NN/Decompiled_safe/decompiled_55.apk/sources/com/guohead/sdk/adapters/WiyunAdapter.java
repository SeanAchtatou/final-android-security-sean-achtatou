package com.guohead.sdk.adapters;

import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import com.guohead.sdk.GuoheAdLayout;
import com.guohead.sdk.obj.Extra;
import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.GuoheAdUtil;
import com.wiyun.ad.AdView;

public class WiyunAdapter extends GuoheAdAdapter implements AdView.AdListener {
    /* access modifiers changed from: private */
    public AdView a;

    public WiyunAdapter(GuoheAdLayout guoheAdLayout, Ration ration) {
        super(guoheAdLayout, ration);
    }

    public void finish() {
        Log.i(GuoheAdUtil.GUOHEAD, getClass().getSimpleName() + "==> finish ");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{com.guohead.sdk.GuoheAdLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void}
     arg types: [com.wiyun.ad.AdView, android.view.ViewGroup$LayoutParams]
     candidates:
      ClspMth{android.view.ViewGroup.addView(android.view.View, int):void}
      SimpleMethodDetails{com.guohead.sdk.GuoheAdLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void} */
    public void handle() {
        this.a = new AdView(this.guoheAdLayout.activity);
        try {
            this.a.setResId(this.ration.key);
            this.a.setListener(this);
            this.a.setTestAdType(2);
            Extra extra = this.guoheAdLayout.extra;
            int rgb = Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue);
            int rgb2 = Color.rgb(extra.fgRed, extra.fgGreen, extra.fgBlue);
            int i = extra.cycleTime;
            if (i < 30) {
                i = 30;
            }
            this.a.setRefreshInterval(i);
            this.a.setBackgroundColor(rgb);
            this.a.setTextColor(rgb2);
            this.guoheAdLayout.addView((View) this.a, new ViewGroup.LayoutParams(-2, -2));
            this.a.requestAd();
        } catch (IllegalArgumentException e) {
            this.guoheAdLayout.rollover();
        }
    }

    public void onAdClicked() {
    }

    public void onAdLoadFailed() {
        Log.d(GuoheAdUtil.GUOHEAD, "==========> onFailedToReceiveAd");
        notifyOnFail();
        this.a.setListener((AdView.AdListener) null);
        clearAdStateListener();
        this.guoheAdLayout.activity.runOnUiThread(new p(this));
    }

    public void onAdLoaded() {
        Log.d(GuoheAdUtil.GUOHEAD, "========> Wiyun success");
        this.a.setListener((AdView.AdListener) null);
        clearAdStateListener();
        this.guoheAdLayout.activity.runOnUiThread(new q(this));
    }

    public void onExitButtonClicked() {
    }
}
