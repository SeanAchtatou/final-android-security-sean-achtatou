package com.guohead.sdk.adapters;

import com.casee.adsdk.CaseeAdView;

final class f implements Runnable {
    private /* synthetic */ CaseeAdView a;
    private /* synthetic */ CaseeAdapter b;

    f(CaseeAdapter caseeAdapter, CaseeAdView caseeAdView) {
        this.b = caseeAdapter;
        this.a = caseeAdView;
    }

    public final void run() {
        this.b.guoheAdLayout.removeView(this.a);
        this.a.setVisibility(0);
        this.b.guoheAdLayout.guoheAdManager.resetRollover();
        this.b.guoheAdLayout.nextView = this.a;
        this.b.guoheAdLayout.handler.post(this.b.guoheAdLayout.viewRunnable);
        this.b.guoheAdLayout.rotateThreadedDelayed();
    }
}
