package com.guohead.sdk.adapters;

import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import com.casee.adsdk.CaseeAdView;
import com.guohead.sdk.GuoheAdLayout;
import com.guohead.sdk.obj.Extra;
import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.GuoheAdUtil;

public class CaseeAdapter extends GuoheAdAdapter implements CaseeAdView.AdListener {
    public CaseeAdView cav;
    public int refreshTime;

    public CaseeAdapter(GuoheAdLayout guoheAdLayout, Ration ration) {
        super(guoheAdLayout, ration);
    }

    public void finish() {
        Log.i(GuoheAdUtil.GUOHEAD, getClass().getSimpleName() + "==> finish ");
        if (this.cav != null) {
            this.cav.onUnshown();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{com.guohead.sdk.GuoheAdLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void}
     arg types: [com.casee.adsdk.CaseeAdView, android.view.ViewGroup$LayoutParams]
     candidates:
      ClspMth{android.view.ViewGroup.addView(android.view.View, int):void}
      SimpleMethodDetails{com.guohead.sdk.GuoheAdLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void} */
    public void handle() {
        Extra extra = this.guoheAdLayout.extra;
        int rgb = Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue);
        int rgb2 = Color.rgb(extra.fgRed, extra.fgGreen, extra.fgBlue);
        this.refreshTime = extra.cycleTime * 1000;
        this.cav = new CaseeAdView(this.guoheAdLayout.activity, (AttributeSet) null, rgb2, this.ration.key, false, this.refreshTime, rgb, rgb2, false);
        this.guoheAdLayout.addView((View) this.cav, new ViewGroup.LayoutParams(-1, -2));
    }

    public void onFailedToReceiveAd(CaseeAdView caseeAdView) {
        Log.d(GuoheAdUtil.GUOHEAD, "=======> CaseeAd failure");
        caseeAdView.setListener((CaseeAdView.AdListener) null);
        this.guoheAdLayout.activity.runOnUiThread(new e(this, caseeAdView));
    }

    public void onFailedToReceiveRefreshAd(CaseeAdView caseeAdView) {
    }

    public void onReceiveAd(CaseeAdView caseeAdView) {
        Log.d(GuoheAdUtil.GUOHEAD, "======> CaseeAd success");
        caseeAdView.setListener((CaseeAdView.AdListener) null);
        this.guoheAdLayout.activity.runOnUiThread(new f(this, caseeAdView));
    }

    public void onReceiveRefreshAd(CaseeAdView caseeAdView) {
    }
}
