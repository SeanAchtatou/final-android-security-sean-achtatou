package com.guohead.sdk.adapters;

import android.graphics.Color;
import android.util.Log;
import android.view.ViewGroup;
import com.guohead.sdk.GuoheAdLayout;
import com.guohead.sdk.obj.Extra;
import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.GuoheAdUtil;
import com.madhouse.android.ads.AdListener;
import com.madhouse.android.ads.AdManager;
import com.madhouse.android.ads.AdView;

public class MadHouseAdapter extends GuoheAdAdapter implements AdListener {
    /* access modifiers changed from: private */
    public AdView a;

    public MadHouseAdapter(GuoheAdLayout guoheAdLayout, Ration ration) {
        super(guoheAdLayout, ration);
    }

    public void finish() {
    }

    public void handle() {
        Extra extra = this.guoheAdLayout.extra;
        int rgb = Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue);
        int i = extra.cycleTime;
        int i2 = i < 30 ? 30 : i;
        AdManager.setApplicationId(this.guoheAdLayout.getContext(), this.ration.key);
        this.a = new AdView(this.guoheAdLayout.activity, null, 0, this.ration.key2, i2, Boolean.parseBoolean(this.ration.key3));
        this.a.setBackgroundColor(rgb);
        this.a.setListener(this);
        this.guoheAdLayout.addView(this.a, new ViewGroup.LayoutParams(-2, -2));
    }

    public void onAdEvent(int i) {
        Log.d(GuoheAdUtil.GUOHEAD, "====> onAdEvent: " + i);
    }

    public void onAdStatus(int i) {
        Log.d(GuoheAdUtil.GUOHEAD, "====> onAdStatus: " + i);
        this.a.setListener(null);
        if (i != 200) {
            notifyOnFail();
            clearAdStateListener();
            this.guoheAdLayout.activity.runOnUiThread(new m(this));
            return;
        }
        clearAdStateListener();
        this.guoheAdLayout.activity.runOnUiThread(new n(this));
    }
}
