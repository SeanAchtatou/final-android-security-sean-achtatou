package com.guohead.sdk.adapters;

import com.wooboo.adlib_android.WoobooAdView;

final class s implements Runnable {
    private /* synthetic */ WoobooAdView a;
    private /* synthetic */ WooBooAdapter b;

    s(WooBooAdapter wooBooAdapter, WoobooAdView woobooAdView) {
        this.b = wooBooAdapter;
        this.a = woobooAdView;
    }

    public final void run() {
        this.b.guoheAdLayout.removeView(this.a);
        this.a.setVisibility(0);
        this.b.guoheAdLayout.guoheAdManager.resetRollover();
        this.b.guoheAdLayout.nextView = this.a;
        this.b.guoheAdLayout.handler.post(this.b.guoheAdLayout.viewRunnable);
        this.b.guoheAdLayout.rotateThreadedDelayed();
    }
}
