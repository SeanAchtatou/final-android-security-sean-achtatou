package com.guohead.sdk.adapters;

import com.adchina.android.ads.AdView;

final class b implements Runnable {
    private /* synthetic */ AdView a;
    private /* synthetic */ AdChinaAdapter b;

    b(AdChinaAdapter adChinaAdapter, AdView adView) {
        this.b = adChinaAdapter;
        this.a = adView;
    }

    public final void run() {
        this.b.guoheAdLayout.removeView(this.a);
        this.a.setVisibility(0);
        this.b.guoheAdLayout.guoheAdManager.resetRollover();
        this.b.guoheAdLayout.nextView = this.a;
        this.b.guoheAdLayout.handler.post(this.b.guoheAdLayout.viewRunnable);
        this.b.guoheAdLayout.rotateThreadedDelayed();
    }
}
