package com.guohead.sdk.adapters;

import android.graphics.Color;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import com.adchina.android.ads.AdListener;
import com.adchina.android.ads.AdManager;
import com.adchina.android.ads.AdView;
import com.guohead.sdk.GuoheAdLayout;
import com.guohead.sdk.obj.Extra;
import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.GuoheAdUtil;

public class AdChinaAdapter extends GuoheAdAdapter implements AdListener {
    private AdView a;

    public AdChinaAdapter(GuoheAdLayout guoheAdLayout, Ration ration) {
        super(guoheAdLayout, ration);
    }

    public boolean OnRecvSms(AdView adView, String str) {
        return false;
    }

    public void finish() {
        Log.i(GuoheAdUtil.GUOHEAD, getClass().getSimpleName() + "==> finish ");
        this.a.stop();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{com.guohead.sdk.GuoheAdLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void}
     arg types: [com.adchina.android.ads.AdView, android.view.ViewGroup$LayoutParams]
     candidates:
      ClspMth{android.view.ViewGroup.addView(android.view.View, int):void}
      SimpleMethodDetails{com.guohead.sdk.GuoheAdLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void} */
    public void handle() {
        Display defaultDisplay = this.guoheAdLayout.activity.getWindowManager().getDefaultDisplay();
        int width = defaultDisplay.getWidth();
        AdManager.setResolution(width + "x" + defaultDisplay.getHeight());
        AdManager.setAdspaceId(this.ration.key);
        boolean z = true;
        try {
            z = Boolean.parseBoolean(this.ration.key2);
        } catch (Exception e) {
        }
        Log.d("guohead SDK", Boolean.toString(z));
        AdManager.setDebugMode(z);
        this.a = new AdView(this.guoheAdLayout.activity);
        this.a.setAdListener(this);
        Extra extra = this.guoheAdLayout.extra;
        this.a.setBackgroundColor(Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue));
        this.guoheAdLayout.addView((View) this.a, new ViewGroup.LayoutParams(-2, -2));
        this.a.start();
    }

    public void onFailedToReceiveAd(AdView adView) {
        Log.d(GuoheAdUtil.GUOHEAD, "==========> onFailedToReceiveAd");
        notifyOnFail();
        adView.setAdListener((AdListener) null);
        clearAdStateListener();
        this.guoheAdLayout.activity.runOnUiThread(new a(this, adView));
    }

    public void onFailedToRefreshAd(AdView adView) {
        Log.d(GuoheAdUtil.GUOHEAD, "==========> onFailedToRefreshAd");
    }

    public void onReceiveAd(AdView adView) {
        adView.setAdListener((AdListener) null);
        clearAdStateListener();
        this.guoheAdLayout.activity.runOnUiThread(new b(this, adView));
    }

    public void onRefreshAd(AdView adView) {
        Log.d(GuoheAdUtil.GUOHEAD, "========> onRefreshAd");
    }
}
