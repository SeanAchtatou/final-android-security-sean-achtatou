package com.guohead.sdk.adapters;

import android.graphics.Color;
import android.util.Log;
import android.widget.RelativeLayout;
import com.guohead.sdk.GuoheAdLayout;
import com.guohead.sdk.obj.Extra;
import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.GuoheAdUtil;
import com.wooboo.adlib_android.AdListener;
import com.wooboo.adlib_android.WoobooAdView;

public class WooBooAdapter extends GuoheAdAdapter implements AdListener {
    private WoobooAdView a;

    public WooBooAdapter(GuoheAdLayout guoheAdLayout, Ration ration) {
        super(guoheAdLayout, ration);
    }

    public void finish() {
        Log.i(GuoheAdUtil.GUOHEAD, getClass().getSimpleName() + "==> finish ");
    }

    public void handle() {
        Extra extra = this.guoheAdLayout.extra;
        int rgb = Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue);
        int rgb2 = Color.rgb(extra.fgRed, extra.fgGreen, extra.fgBlue);
        int i = extra.cycleTime;
        this.a = new WoobooAdView(this.guoheAdLayout.activity, this.ration.key, rgb, rgb2, Boolean.parseBoolean(this.ration.key2), i < 20 ? 20 : i);
        this.a.setListener(this);
        this.a.setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
        this.guoheAdLayout.addView(this.a);
    }

    public void onFailedToReceiveAd(WoobooAdView woobooAdView) {
        Log.d(GuoheAdUtil.GUOHEAD, "==========> WoobooAdView failure");
        woobooAdView.setListener((AdListener) null);
        this.guoheAdLayout.activity.runOnUiThread(new r(this, woobooAdView));
    }

    public void onNewAd() {
    }

    public void onReceiveAd(WoobooAdView woobooAdView) {
        Log.d(GuoheAdUtil.GUOHEAD, "========> WoobooAdView success");
        woobooAdView.setListener((AdListener) null);
        this.guoheAdLayout.activity.runOnUiThread(new s(this, woobooAdView));
    }
}
