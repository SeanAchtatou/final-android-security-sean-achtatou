package com.guohead.sdk.adapters;

import android.util.Log;
import com.guohead.sdk.GuoheAdLayout;
import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.GuoheAdUtil;

public class EventAdapter extends GuoheAdAdapter {
    public EventAdapter(GuoheAdLayout guoheAdLayout, Ration ration) {
        super(guoheAdLayout, ration);
    }

    public void finish() {
    }

    public void handle() {
        Log.d(GuoheAdUtil.GUOHEAD, "Event notification request initiated");
        if (this.guoheAdLayout.guoheAdInterface != null) {
            String str = this.ration.key;
            if (str == null) {
                Log.w(GuoheAdUtil.GUOHEAD, "Event key is null");
            }
            int indexOf = str.indexOf("|;|");
            if (indexOf < 0) {
                Log.w(GuoheAdUtil.GUOHEAD, "Event key separator not found");
            }
            try {
                this.guoheAdLayout.guoheAdInterface.getClass().getMethod(str.substring(indexOf + 3), null).invoke(this.guoheAdLayout.guoheAdInterface, null);
            } catch (Exception e) {
                Log.e(GuoheAdUtil.GUOHEAD, "Caught exception in handle()", e);
            }
        } else {
            Log.w(GuoheAdUtil.GUOHEAD, "Event notification would be sent, but no interface is listening");
        }
        this.guoheAdLayout.guoheAdManager.resetRollover();
        this.guoheAdLayout.rotateThreadedDelayed();
    }
}
