package com.guohead.sdk.adapters;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import com.energysource.szj.embeded.AdListener;
import com.energysource.szj.embeded.AdManager;
import com.energysource.szj.embeded.AdView;
import com.guohead.sdk.GuoheAdLayout;
import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.GuoheAdUtil;

public class AdTouchAdapter extends GuoheAdAdapter implements AdListener {
    /* access modifiers changed from: private */
    public AdView a;

    public AdTouchAdapter(GuoheAdLayout guoheAdLayout, Ration ration) {
        super(guoheAdLayout, ration);
    }

    public void failedReceiveAd(AdView adView) {
        Log.d(GuoheAdUtil.GUOHEAD, "==========> onFailedToReceiveAd");
        notifyOnFail();
        AdManager.setAdListener(this);
        clearAdStateListener();
        this.guoheAdLayout.activity.runOnUiThread(new c(this));
    }

    public void finish() {
        Log.i(GuoheAdUtil.GUOHEAD, getClass().getSimpleName() + "==> finish ");
        AdManager.destoryAd();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{com.guohead.sdk.GuoheAdLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void}
     arg types: [com.energysource.szj.embeded.AdView, android.view.ViewGroup$LayoutParams]
     candidates:
      ClspMth{android.view.ViewGroup.addView(android.view.View, int):void}
      SimpleMethodDetails{com.guohead.sdk.GuoheAdLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void} */
    public void handle() {
        try {
            this.a = new AdView(this.guoheAdLayout.activity, 0);
            this.a.setAnimationCacheEnabled(true);
            AdManager.openDebug();
            AdManager.initAd(this.guoheAdLayout.activity, this.guoheAdLayout.activity.getPackageName());
            AdManager.openPermissionJudge();
            AdManager.addAd(102, 2101, 81, 50, 0);
            AdManager.setAdListener(this);
            this.guoheAdLayout.addView((View) this.a, new ViewGroup.LayoutParams(-2, -2));
        } catch (IllegalArgumentException e) {
            this.guoheAdLayout.rollover();
        }
    }

    public void receiveAd(AdView adView) {
        Log.d(GuoheAdUtil.GUOHEAD, "========> Wiyun success");
        AdManager.setAdListener(this);
        clearAdStateListener();
        this.guoheAdLayout.activity.runOnUiThread(new d(this));
    }
}
