package com.guohead.sdk.adapters;

import com.millennialmedia.android.MMAdView;

final class o implements Runnable {
    private /* synthetic */ MMAdView a;
    private /* synthetic */ MillennialAdapter b;

    o(MillennialAdapter millennialAdapter, MMAdView mMAdView) {
        this.b = millennialAdapter;
        this.a = mMAdView;
    }

    public final void run() {
        this.b.guoheAdLayout.guoheAdManager.resetRollover();
        this.b.guoheAdLayout.nextView = this.a;
        this.b.guoheAdLayout.activeRation = this.b.ration;
        this.b.guoheAdLayout.rotateThreadedDelayed();
        this.b.guoheAdLayout.countImpressionThreaded();
    }
}
