package com.l.adlib_android;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.Display;
import android.widget.RelativeLayout;
import com.madhouse.android.ads.AdView;
import java.util.ArrayList;
import java.util.List;

public class ad extends RelativeLayout implements AdListener {
    private final String a = "IMG";
    private final String b = "TEXT";
    private List c = new ArrayList();
    private Context d;
    private boolean e;
    private boolean f;
    private ahead g;
    private List h;
    private AdListener i;

    public ad(Context context) {
        super(context);
    }

    public ad(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public ad(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
    }

    public ad(Context context, ahead ahead, List list, boolean z, boolean z2) {
        super(context);
        this.d = context;
        this.g = ahead;
        this.h = list;
        this.f = z2;
        this.e = z;
        setBackgroundDrawable(util.getGradientDrawable(new int[]{config.c, config.d}));
        Display defaultDisplay = ((Activity) this.d).getWindowManager().getDefaultDisplay();
        int i2 = this.d.getResources().getConfiguration().orientation;
        config.j = i2;
        int height = i2 == 2 ? defaultDisplay.getHeight() : config.j == 1 ? defaultDisplay.getWidth() : AdView.AD_MEASURE_320;
        if (this.e) {
            config.l = (float) defaultDisplay.getWidth();
        } else {
            config.l = (float) height;
        }
        config.m = (0.15f * ((float) height)) + 2.0f;
        config.p = config.l / ((float) height);
        setLayoutParams(new RelativeLayout.LayoutParams((int) config.l, (int) config.m));
        config.n = config.l / 640.0f;
        config.o = config.m / 96.0f;
        a(list);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    private void a(List list) {
        this.c.clear();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < list.size()) {
                abody abody = (abody) list.get(i3);
                this.c.add(abody.getAction().toString());
                int width = (int) (((float) (((double) abody.getWidth()) / 100.0d)) * config.l);
                int i4 = i3 + 256;
                image image = null;
                if (abody.getType().equalsIgnoreCase("IMG")) {
                    boolean equals = abody.getFooterCaption().trim().equals("");
                    image image2 = new image(this.d, util.decodeBase64strToBitmap(abody.getContent()), abody.getFooterCaption(), equals ? 0 : 15, equals ? 0 : 5, equals ? 0 : 15, equals ? 0 : 5);
                    image2.setOnClickAdListener(this);
                    image = image2;
                } else if (abody.getType().equalsIgnoreCase("TEXT")) {
                    text text = new text(this.d, abody.getTitle(), abody.getMessage(), 0, 0, 0, 0);
                    text.setOnClickAdListener(this);
                    image = text;
                }
                if (image != null) {
                    image.setId(i4);
                    RelativeLayout.LayoutParams layoutParams = util.getLayoutParams(width, -1);
                    if (i3 > 0) {
                        layoutParams.addRule(1, i4 - 1);
                    }
                    addView(image, layoutParams);
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    public void OnClickAd(int i2) {
        if (this.i != null) {
            this.i.OnClickAd(i2);
        }
    }

    public List getActions() {
        return this.c;
    }

    public List getBodyList() {
        return this.h;
    }

    public boolean getDeleteState() {
        return this.f;
    }

    public ahead getHead() {
        return this.g;
    }

    public void setDeleteState(boolean z) {
        this.f = z;
    }

    public void setonClickAdListener(AdListener adListener) {
        this.i = adListener;
    }
}
