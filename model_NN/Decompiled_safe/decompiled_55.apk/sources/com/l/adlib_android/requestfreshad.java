package com.l.adlib_android;

import android.content.Context;
import android.os.Handler;
import com.madhouse.android.ads.AdView;
import java.io.File;
import java.io.InputStream;
import java.util.List;

public class requestfreshad extends Thread {
    public static boolean a = false;
    public static Context b;
    public static locationmanager c;
    public static Handler d;
    private final int e = 100;

    private static void a(int i) {
        d.sendMessage(d.obtainMessage(i));
        util.Log("sendMessage..." + String.valueOf(i));
    }

    public void run() {
        ahead ParserXmlAdHead;
        String str;
        a = true;
        try {
            if (aqueue.getLength() < 3) {
                String str2 = "";
                util.Log("requestHead...");
                double[] locationData = c.getLocationData();
                param param = new param();
                param.setMid(config.a);
                param.setUid(device.a);
                param.setOS((byte) 0);
                param.setModel(device.b);
                param.setSdk(device.c);
                param.setOperator(device.d);
                param.setConnectionType(device.e);
                param.setLocation((byte) ((int) locationData[0]));
                param.setLat(locationData[1]);
                param.setLng(locationData[2]);
                param.setLastPosition(Integer.parseInt(util.getSettingsparams(b, "lastposition")));
                InputStream PostData = net.PostData("http://show.lsense.cn/AdShowService.asmx/GetAd", util.getPostHeadDataParams(param));
                if (PostData == null) {
                    util.Log("Enqueue requestHead...null");
                    ahead ahead = new ahead();
                    ahead.setDef("def");
                    ahead.setFileName(util.getSettingsparams(b, "filename"));
                    ahead.setAdId(util.getSettingsparams(b, "adid").trim() == "" ? 0 : Integer.parseInt(util.getSettingsparams(b, "adid").trim()));
                    ahead.setTurns(util.getSettingsparams(b, "turns").trim() == "" ? 1 : Integer.parseInt(util.getSettingsparams(b, "turns").trim()));
                    ahead.setLastPosition(util.getSettingsparams(b, "lastposition").trim() == "" ? -1 : Integer.parseInt(util.getSettingsparams(b, "lastposition").trim()));
                    ahead.setShowTime(util.getSettingsparams(b, "showtime").trim() == "" ? 20 : Integer.parseInt(util.getSettingsparams(b, "showtime").trim()));
                    String ReadfromFile = util.ReadfromFile(b, String.valueOf(util.getSettingsparams(b, "adid")) + ".default");
                    util.Log("ad from default log file...");
                    String str3 = ReadfromFile;
                    ParserXmlAdHead = ahead;
                    str2 = str3;
                } else {
                    ParserXmlAdHead = xparse.ParserXmlAdHead(util.StreamToString(PostData));
                    util.Log("adHead:" + ParserXmlAdHead.toString());
                    util.Log("Enqueue adHead:" + ParserXmlAdHead.getAdId());
                    if (ParserXmlAdHead != null) {
                        ParserXmlAdHead.setFileName(util.getFileNameFromUri(ParserXmlAdHead.getUri()));
                        ParserXmlAdHead.setDef("");
                        File contains = afile.contains(String.valueOf(ParserXmlAdHead.getFileName()));
                        if (contains != null) {
                            str2 = util.ReadfromFile(b, contains.getName());
                            contains.setLastModified(System.currentTimeMillis());
                            util.Log("ad from log file..." + ParserXmlAdHead.getAdId());
                        } else {
                            InputStream requestDataFromNet = net.getRequestDataFromNet(ParserXmlAdHead.getUri().trim());
                            if (requestDataFromNet != null) {
                                str2 = util.StreamToString(requestDataFromNet);
                                if (ParserXmlAdHead.getTurns() >= 100) {
                                    str = String.valueOf(ParserXmlAdHead.getFileName()) + ".default";
                                    ParserXmlAdHead.setTurns(ParserXmlAdHead.getTurns() - 100);
                                    util.setSettingsparams(b, "adid", String.valueOf(ParserXmlAdHead.getAdId()));
                                    util.setSettingsparams(b, "showtime", String.valueOf(ParserXmlAdHead.getShowTime()));
                                    util.setSettingsparams(b, "turns", String.valueOf(ParserXmlAdHead.getTurns()));
                                    util.setSettingsparams(b, "filename", ParserXmlAdHead.getFileName());
                                    util.Log("aqueue.LENGTH:" + aqueue.getLength());
                                    ParserXmlAdHead.setDef("def");
                                } else {
                                    str = String.valueOf(ParserXmlAdHead.getFileName()) + ".ad";
                                    afile.setFile(str);
                                }
                                util.SavetoFile(b, str, str2);
                                util.Log("ad from server..." + ParserXmlAdHead.getAdId());
                            } else {
                                util.Log("requestBody...null");
                            }
                        }
                    }
                }
                if (!str2.trim().equals("")) {
                    util.setSettingsparams(b, "lastposition", String.valueOf(ParserXmlAdHead.getLastPosition()));
                    if (!ParserXmlAdHead.getDef().equalsIgnoreCase("def") || aqueue.getLength() <= 0) {
                        List ParserXmlAdBody = xparse.ParserXmlAdBody(str2);
                        if (ParserXmlAdBody != null) {
                            aqueue.Enqueue(new adata(ParserXmlAdHead, ParserXmlAdBody));
                            util.Log("Enqueue..." + String.valueOf(ParserXmlAdHead.getAdId()) + ".." + String.valueOf(aqueue.getLength()));
                            a(AdView.RETRUNCODE_OK);
                            util.setAdListenerEx();
                        } else {
                            a(300);
                        }
                    } else {
                        a(AdView.RETRUNCODE_OK);
                        util.setAdListenerEx();
                    }
                } else {
                    a(300);
                }
            } else {
                a(AdView.RETRUNCODE_OK);
            }
        } catch (Exception e2) {
            a(300);
        } finally {
            a = false;
        }
    }
}
