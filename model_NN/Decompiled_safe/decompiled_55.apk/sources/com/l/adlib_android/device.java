package com.l.adlib_android;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Build;
import android.telephony.TelephonyManager;

public class device {
    public static long a;
    public static String b;
    public static String c;
    public static byte d;
    public static byte e;

    public static void Init(Context context) {
        b = Build.MODEL;
        c = Build.VERSION.RELEASE;
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        a = Long.parseLong(telephonyManager.getDeviceId());
        String networkOperatorName = telephonyManager.getNetworkOperatorName();
        if (networkOperatorName.contains("移动")) {
            d = 0;
        } else if (networkOperatorName.contains("联通")) {
            d = 1;
        } else if (networkOperatorName.contains("电信")) {
            d = 2;
        } else {
            d = -1;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager.getActiveNetworkInfo() != null) {
            String typeName = connectivityManager.getActiveNetworkInfo().getTypeName();
            if (typeName.equalsIgnoreCase("wifi")) {
                e = 1;
                return;
            } else if (typeName.equalsIgnoreCase("mobile")) {
                e = 2;
                return;
            }
        }
        e = -1;
    }

    public static String _toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("os:0\n");
        sb.append("uid:" + a + "\n");
        sb.append("model:" + b + "\n");
        sb.append("sdk:" + c + "\n");
        sb.append("operator:" + ((int) d) + "\n");
        sb.append("connectiontype:" + ((int) e) + "\n");
        return sb.toString();
    }
}
