package com.l.adlib_android;

import java.text.SimpleDateFormat;

public class ahead {
    static SimpleDateFormat a = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
    private int b;
    private int c;
    private int d;
    private int e;
    private int f;
    private String g;
    private String h;
    private String i;

    public int getAdId() {
        return this.b;
    }

    public int getAdType() {
        return this.e;
    }

    public String getDef() {
        return this.h;
    }

    public String getFileName() {
        return this.i;
    }

    public int getLastPosition() {
        return this.f;
    }

    public int getShowTime() {
        return this.c;
    }

    public int getTurns() {
        return this.d;
    }

    public String getUri() {
        return this.g;
    }

    public void setAdId(int i2) {
        this.b = i2;
    }

    public void setAdType(int i2) {
        this.e = i2;
    }

    public void setDef(String str) {
        this.h = str;
    }

    public void setFileName(String str) {
        this.i = str;
    }

    public void setLastPosition(int i2) {
        this.f = i2;
    }

    public void setShowTime(int i2) {
        this.c = i2;
    }

    public void setTurns(int i2) {
        this.d = i2;
    }

    public void setUri(String str) {
        this.g = str;
    }

    public String toString() {
        return "AdId:" + String.valueOf(this.b) + "\n" + "ShowTime:" + String.valueOf(this.c) + "\n" + "Turns:" + String.valueOf(this.d) + "\n" + "AdType:" + String.valueOf(this.e) + "\n" + "LastPosition:" + String.valueOf(this.f) + "\n" + "Uri:" + this.g;
    }
}
