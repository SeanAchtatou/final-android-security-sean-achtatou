package com.l.adlib_android;

public class param {
    private int a;
    private long b;
    private byte c = 0;
    private String d;
    private String e;
    private byte f = -1;
    private String g = "1.0";
    private byte h;
    private byte i;
    private double j;
    private double k;
    private int l = -1;
    private byte m = 1;

    public byte getAdType() {
        return this.m;
    }

    public String getClientVar() {
        return this.g;
    }

    public byte getConnectionType() {
        return this.h;
    }

    public int getLastPosition() {
        return this.l;
    }

    public double getLat() {
        return this.j;
    }

    public double getLng() {
        return this.k;
    }

    public byte getLocation() {
        return this.i;
    }

    public int getMid() {
        return this.a;
    }

    public String getModel() {
        return this.d;
    }

    public byte getOS() {
        return this.c;
    }

    public byte getOperator() {
        return this.f;
    }

    public String getSdk() {
        return this.e;
    }

    public long getUid() {
        return this.b;
    }

    public void setAdType(byte b2) {
        this.m = b2;
    }

    public void setClientVar(String str) {
        this.g = str;
    }

    public void setConnectionType(byte b2) {
        this.h = b2;
    }

    public void setLastPosition(int i2) {
        this.l = i2;
    }

    public void setLat(double d2) {
        this.j = d2;
    }

    public void setLng(double d2) {
        this.k = d2;
    }

    public void setLocation(byte b2) {
        this.i = b2;
    }

    public void setMid(int i2) {
        this.a = i2;
    }

    public void setModel(String str) {
        this.d = str;
    }

    public void setOS(byte b2) {
        this.c = b2;
    }

    public void setOperator(byte b2) {
        this.f = b2;
    }

    public void setSdk(String str) {
        this.e = str;
    }

    public void setUid(long j2) {
        this.b = j2;
    }
}
