package com.l.adlib_android;

import android.os.Handler;
import android.os.Message;
import com.madhouse.android.ads.AdView;

final class a extends Handler {
    final /* synthetic */ AdView a;

    a(AdView adView) {
        this.a = adView;
    }

    public final void handleMessage(Message message) {
        super.handleMessage(message);
        switch (message.what) {
            case AdView.RETRUNCODE_OK /*200*/:
                AdView.i = aqueue.Dequeue();
                if (AdView.i != null) {
                    AdView.i.setCurrentTimeMillis(System.currentTimeMillis());
                    this.a.b(AdView.i);
                    this.a.k.postDelayed(this.a, (long) (AdView.i.getAdHead().getShowTime() * 1000));
                    util.Log("getShowTime:" + String.valueOf(AdView.i.getAdHead().getShowTime()));
                    return;
                }
                util.Log("getAdQueue: Null");
                this.a.k.postDelayed(this.a, 20000);
                return;
            case 300:
                this.a.k.postDelayed(this.a, 20000);
                return;
            default:
                return;
        }
    }
}
