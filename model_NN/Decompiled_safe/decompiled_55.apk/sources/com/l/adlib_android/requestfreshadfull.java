package com.l.adlib_android;

import android.content.Context;
import android.os.Handler;
import com.madhouse.android.ads.AdView;
import java.io.InputStream;

public class requestfreshadfull extends Thread {
    public static Context a;
    public static locationmanager b;
    public static Handler c;

    private static void a(int i) {
        c.sendMessage(c.obtainMessage(i));
        util.Log("sendMessage..." + String.valueOf(i));
    }

    public void run() {
        ahead ahead = null;
        try {
            util.Log("requestHead...");
            double[] locationData = b.getLocationData();
            param param = new param();
            param.setMid(config.a);
            param.setUid(device.a);
            param.setOS((byte) 0);
            param.setModel(device.b);
            param.setSdk(device.c);
            param.setOperator(device.d);
            param.setConnectionType(device.e);
            param.setLocation((byte) ((int) locationData[0]));
            param.setLat(locationData[1]);
            param.setLng(locationData[2]);
            param.setLastPosition(Integer.parseInt(util.getSettingsparams(a, "lastpositionfull")));
            param.setAdType((byte) 2);
            InputStream PostData = net.PostData("http://show.lsense.cn/AdShowService.asmx/GetAd", util.getPostHeadDataParams(param));
            if (PostData == null) {
                util.Log("Enqueue requestHeadfull...null");
            } else {
                ahead = xparse.ParserXmlAdHead(util.StreamToString(PostData));
                util.Log("Enqueue adHead:" + ahead.toString());
                if (ahead != null) {
                    InputStream requestDataFromNet = net.getRequestDataFromNet(ahead.getUri().trim());
                    if (requestDataFromNet != null) {
                        AdViewFull.a = xparse.ParserXmlAdBody(util.StreamToString(requestDataFromNet));
                    } else {
                        util.Log("requestBody...null");
                    }
                }
            }
            if (AdViewFull.a.size() > 0) {
                util.setAdListenerEx();
                util.setSettingsparams(a, "lastpositionfull", String.valueOf(ahead.getLastPosition()));
                a(AdView.RETRUNCODE_OK);
                return;
            }
            a(300);
        } catch (Exception e) {
            a(300);
        }
    }
}
