package com.l.adlib_android;

import java.util.List;

public class adata {
    private ahead a;
    private List b;
    private boolean c;
    private long d;

    public adata() {
    }

    public adata(ahead ahead, List list) {
        this.a = ahead;
        this.b = list;
    }

    public List getAdBody() {
        return this.b;
    }

    public ahead getAdHead() {
        return this.a;
    }

    public long getCurrentTimeMillis() {
        return this.d;
    }

    public boolean getInBuffer() {
        return this.c;
    }

    public void setAdBody(List list) {
        this.b = list;
    }

    public void setAdHead(ahead ahead) {
        this.a = ahead;
    }

    public void setCurrentTimeMillis(long j) {
        this.d = j;
    }

    public void setInBuffer(boolean z) {
        this.c = z;
    }
}
