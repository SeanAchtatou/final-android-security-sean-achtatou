package com.l.adlib_android;

import android.util.Xml;
import java.util.List;
import org.xml.sax.SAXException;

public class xparse {
    public static List ParserXmlAdBody(String str) {
        h hVar = new h();
        try {
            Xml.parse(str, hVar);
            return hVar.a();
        } catch (SAXException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static ahead ParserXmlAdHead(String str) {
        i iVar = new i();
        try {
            Xml.parse(str, iVar);
            return iVar.a();
        } catch (SAXException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
