package com.l.adlib_android;

import java.util.ArrayList;
import java.util.List;

public class aqueue {
    public static int a = 0;
    private static List b = new ArrayList();

    public static synchronized void Deleted() {
        synchronized (aqueue.class) {
            b.remove(0);
            if (a > 0) {
                a--;
            } else {
                a = 0;
            }
        }
    }

    public static synchronized adata Dequeue() {
        adata adata;
        boolean z;
        synchronized (aqueue.class) {
            if (getLength() <= 0) {
                adata = null;
            } else {
                if (a >= b.size()) {
                    a = 0;
                }
                adata = (adata) b.get(a);
                adata.getAdHead().setTurns(adata.getAdHead().getTurns() - 1);
                int i = a;
                if (adata.getAdHead().getTurns() <= 0) {
                    b.remove(a);
                    adata.setInBuffer(true);
                    z = true;
                } else {
                    a++;
                    adata.setInBuffer(false);
                    z = false;
                }
                util.Log("position:" + String.valueOf(i) + " . size:" + String.valueOf(b.size()) + " . turns:" + String.valueOf(adata.getAdHead().getTurns()) + " . showtime:" + String.valueOf(adata.getAdHead().getShowTime()) + " . adid:" + String.valueOf(adata.getAdHead().getAdId()) + " . lastposition:" + String.valueOf(adata.getAdHead().getLastPosition()) + " . del:" + Boolean.toString(z));
            }
        }
        return adata;
    }

    public static synchronized void Enqueue(adata adata) {
        synchronized (aqueue.class) {
            b.add(adata);
        }
    }

    public static int getLength() {
        return b.size();
    }
}
