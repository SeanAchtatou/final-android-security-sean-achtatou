package com.l.adlib_android;

import android.view.animation.DecelerateInterpolator;

final class c implements Runnable {
    final /* synthetic */ AdView a;

    public c(AdView adView) {
        this.a = adView;
    }

    public final void run() {
        float width = ((float) this.a.e.getWidth()) / 2.0f;
        float height = ((float) this.a.e.getHeight()) / 2.0f;
        if (this.a.d.size() > 0) {
            ((ad) this.a.d.get(0)).setVisibility(0);
        }
        Rotate3dAnimation rotate3dAnimation = new Rotate3dAnimation(90.0f, 0.0f, width, height, 310.0f, false);
        rotate3dAnimation.setDuration(700);
        rotate3dAnimation.setFillAfter(true);
        rotate3dAnimation.setInterpolator(new DecelerateInterpolator());
        this.a.e.startAnimation(rotate3dAnimation);
    }
}
