package com.l.adlib_android;

import java.io.File;
import java.util.Comparator;

final class f implements Comparator {
    f() {
    }

    public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
        int lastModified = (int) (((File) obj).lastModified() - ((File) obj2).lastModified());
        if (lastModified == 0) {
            return 0;
        }
        return lastModified > 0 ? 1 : -1;
    }
}
