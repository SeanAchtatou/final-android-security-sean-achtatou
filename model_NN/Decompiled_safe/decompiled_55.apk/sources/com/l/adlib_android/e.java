package com.l.adlib_android;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

final class e extends BaseAdapter {
    final /* synthetic */ AdViewFull a;
    private Context b;

    public e(AdViewFull adViewFull, Context context) {
        this.a = adViewFull;
        this.b = context;
    }

    public final int getCount() {
        return this.a.c.size();
    }

    public final Object getItem(int i) {
        return Integer.valueOf(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        ImageView imageView = new ImageView(this.b);
        imageView.setImageBitmap((Bitmap) this.a.c.get(i));
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        imageView.setLayoutParams(new Gallery.LayoutParams(-1, -1));
        return imageView;
    }
}
