package com.l.adlib_android;

public class abody {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;
    private int i;

    public String getAction() {
        return this.g;
    }

    public String getActionData() {
        return this.h;
    }

    public String getContent() {
        return this.b;
    }

    public String getFeeUri() {
        return this.f;
    }

    public String getFooterCaption() {
        return this.c;
    }

    public String getMessage() {
        return this.e;
    }

    public String getTitle() {
        return this.d;
    }

    public String getType() {
        return this.a;
    }

    public int getWidth() {
        return this.i;
    }

    public void setAction(String str) {
        this.g = str;
    }

    public void setActionData(String str) {
        this.h = str;
    }

    public void setContent(String str) {
        this.b = str;
    }

    public void setFeeUri(String str) {
        this.f = str;
    }

    public void setFooterCaption(String str) {
        this.c = str;
    }

    public void setMessage(String str) {
        this.e = str;
    }

    public void setTitle(String str) {
        this.d = str;
    }

    public void setType(String str) {
        this.a = str;
    }

    public void setWidth(int i2) {
        this.i = i2;
    }
}
