package com.l.adlib_android;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

public class net {
    public static InputStream PostData(String str, String str2) {
        InputStream inputStream;
        Exception exc;
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 30000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 60000);
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
            HttpPost httpPost = new HttpPost(str);
            if (str2 != null) {
                httpPost.setEntity(new StringEntity(str2, "UTF-8"));
                httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
            }
            HttpResponse execute = defaultHttpClient.execute(httpPost);
            if (execute.getStatusLine().getStatusCode() == 200) {
                InputStream content = execute.getEntity().getContent();
                try {
                    defaultHttpClient.getConnectionManager().shutdown();
                    return content;
                } catch (Exception e) {
                    Exception exc2 = e;
                    inputStream = content;
                    exc = exc2;
                }
            } else {
                util.setAdListenerEx(String.valueOf(execute.getStatusLine().getStatusCode()));
                return null;
            }
        } catch (Exception e2) {
            Exception exc3 = e2;
            inputStream = null;
            exc = exc3;
        }
        exc.printStackTrace();
        util.setAdListenerEx(exc.getMessage().toString());
        return inputStream;
    }

    public static InputStream PostData(String str, Map map) {
        InputStream inputStream = null;
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 30000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 60000);
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
            HttpPost httpPost = new HttpPost(str);
            ArrayList arrayList = new ArrayList();
            if (map != null) {
                for (String str2 : map.keySet()) {
                    arrayList.add(new BasicNameValuePair(str2, (String) map.get(str2)));
                }
                httpPost.setEntity(new UrlEncodedFormEntity(arrayList, "UTF-8"));
            }
            HttpResponse execute = defaultHttpClient.execute(httpPost);
            if (execute.getStatusLine().getStatusCode() == 200) {
                InputStream content = execute.getEntity().getContent();
                try {
                    defaultHttpClient.getConnectionManager().shutdown();
                    return content;
                } catch (Exception e) {
                    Exception exc = e;
                    inputStream = content;
                    e = exc;
                }
            } else {
                util.setAdListenerEx(String.valueOf(execute.getStatusLine().getStatusCode()));
                return null;
            }
        } catch (Exception e2) {
            e = e2;
        }
        e.printStackTrace();
        util.setAdListenerEx(e.getMessage().toString());
        return inputStream;
    }

    public static InputStream getRequestDataFromNet(String str) {
        util.Log("requestBody..." + str);
        try {
            int lastIndexOf = str.lastIndexOf("/");
            URLConnection openConnection = new URL(String.valueOf(str.substring(0, lastIndexOf)) + "/" + URLEncoder.encode(str.substring(lastIndexOf + 1))).openConnection();
            openConnection.setConnectTimeout(30000);
            openConnection.setReadTimeout(60000);
            HttpURLConnection httpURLConnection = (HttpURLConnection) openConnection;
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode == 200) {
                return httpURLConnection.getInputStream();
            }
            util.setAdListenerEx(String.valueOf(responseCode));
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            util.setAdListenerEx(e.getMessage().toString());
            return null;
        }
    }

    public static InputStream requestFeeUri(String str) {
        util.Log("requestFeeUri..." + str);
        try {
            URLConnection openConnection = new URL(str).openConnection();
            openConnection.setConnectTimeout(30000);
            openConnection.setReadTimeout(60000);
            HttpURLConnection httpURLConnection = (HttpURLConnection) openConnection;
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode == 200) {
                return httpURLConnection.getInputStream();
            }
            util.setAdListenerEx(String.valueOf(responseCode));
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            util.setAdListenerEx(e.getMessage().toString());
            return null;
        }
    }

    public Bitmap getNetBitmap(String str) {
        Bitmap bitmap = null;
        try {
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
                httpURLConnection.setDoInput(true);
                httpURLConnection.setConnectTimeout(30000);
                httpURLConnection.connect();
                InputStream inputStream = httpURLConnection.getInputStream();
                bitmap = BitmapFactory.decodeStream(inputStream);
                inputStream.close();
                httpURLConnection.disconnect();
                return bitmap;
            } catch (IOException e) {
                e.printStackTrace();
                util.Log(e.getMessage().toString());
                return bitmap;
            }
        } catch (MalformedURLException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public String httpRequestGet() {
        URL url = null;
        String str = "";
        try {
            url = new URL("");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        if (url == null) {
            return str;
        }
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            url.openConnection();
            InputStreamReader inputStreamReader = new InputStreamReader(httpURLConnection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    inputStreamReader.close();
                    httpURLConnection.disconnect();
                    return str;
                }
                str = String.valueOf(str) + readLine + "\n";
            }
        } catch (IOException e2) {
            return str;
        }
    }
}
