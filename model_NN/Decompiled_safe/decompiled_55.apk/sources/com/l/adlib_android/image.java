package com.l.adlib_android;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class image extends RelativeLayout implements View.OnClickListener {
    private final int a = 4096;
    private final float b = 9.0f;
    private Context c;
    private int d;
    private ImageView e;
    private TextView f;
    private int g;
    private int h;
    private double i = 0.7d;
    private final int j;
    private final int k;
    private final int l;
    private final int m;
    private AdListener n;

    public image(Context context, Bitmap bitmap, String str, int i2, int i3, int i4, int i5) {
        super(context);
        setOnClickListener(this);
        this.c = context;
        this.d = 4096;
        this.j = (int) (config.n * ((float) i2));
        this.k = (int) (config.o * ((float) i3));
        this.l = (int) (config.n * ((float) i4));
        this.m = (int) (config.o * ((float) i5));
        if (bitmap != null) {
            this.e = new ImageView(this.c);
            int i6 = this.d + 1;
            this.d = i6;
            this.g = i6;
            this.e.setId(this.g);
            this.e.setScaleType(ImageView.ScaleType.FIT_XY);
            this.e.setPadding(this.j, this.k, this.l, this.m);
            this.e.setImageBitmap(bitmap);
            this.i = str.trim().equals("") ? 1.0d : this.i;
            addView(this.e, util.getLayoutParams(-1, (int) (this.i * ((double) config.m))));
        }
        if (!str.trim().equals("")) {
            this.f = new TextView(this.c);
            int i7 = this.d + 1;
            this.d = i7;
            this.h = i7;
            this.f.setId(this.h);
            this.f.setTextSize(2, 9.0f);
            this.f.setTextScaleX(config.p);
            this.f.setTypeface(Typeface.create(Typeface.DEFAULT, 1));
            this.f.setText(str);
            RelativeLayout.LayoutParams layoutParams = util.getLayoutParams(-2, -2);
            layoutParams.addRule(3, this.g);
            layoutParams.addRule(14);
            addView(this.f, layoutParams);
        }
    }

    public void onClick(View view) {
        if (this.n != null) {
            this.n.OnClickAd(view.getId());
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            setBackgroundDrawable(util.getGradientDrawable(new int[]{Color.rgb(155, 225, 35), Color.rgb(75, 120, 15)}));
        } else if (motionEvent.getAction() == 1) {
            setBackgroundColor(0);
        }
        return super.onTouchEvent(motionEvent);
    }

    public void setOnClickAdListener(AdListener adListener) {
        this.n = adListener;
    }
}
