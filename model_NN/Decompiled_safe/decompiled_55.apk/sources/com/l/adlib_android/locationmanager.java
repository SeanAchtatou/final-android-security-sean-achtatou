package com.l.adlib_android;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;

public class locationmanager implements LocationListener {
    private final long a = 2000;
    private final float b = 0.0f;
    private Context c;
    private Location d = null;
    private LocationManager e = null;
    private TelephonyManager f = null;

    public locationmanager(Context context) {
        this.c = context;
        this.e = (LocationManager) this.c.getSystemService("location");
        this.f = (TelephonyManager) this.c.getSystemService("phone");
        requestLocationUpdates();
    }

    public double[] getLocationData() {
        double[] dArr = new double[3];
        dArr[0] = -1.0d;
        if (this.d != null) {
            dArr[0] = 1.0d;
            dArr[1] = this.d.getLatitude();
            dArr[2] = this.d.getLongitude();
        } else {
            GsmCellLocation gsmCellLocation = (GsmCellLocation) this.f.getCellLocation();
            if (gsmCellLocation != null) {
                dArr[0] = 2.0d;
                dArr[1] = (double) gsmCellLocation.getCid();
                dArr[2] = (double) gsmCellLocation.getLac();
            }
        }
        return dArr;
    }

    public void onLocationChanged(Location location) {
        if (location != null) {
            this.d = location;
        }
    }

    public void onProviderDisabled(String str) {
    }

    public void onProviderEnabled(String str) {
    }

    public void onStatusChanged(String str, int i, Bundle bundle) {
    }

    public void removeUpdates() {
        this.e.removeUpdates(this);
    }

    public void requestLocationUpdates() {
        for (String next : this.e.getProviders(true)) {
            if (next.equalsIgnoreCase("gps") || next.equalsIgnoreCase("network")) {
                util.Log(next);
                this.e.requestLocationUpdates(next, 2000, 0.0f, this);
            }
        }
    }
}
