package com.l.adlib_android;

import android.view.View;
import android.view.animation.Animation;
import android.widget.RelativeLayout;

final class b implements Animation.AnimationListener {
    final /* synthetic */ AdView a;

    /* synthetic */ b(AdView adView) {
        this(adView, (byte) 0);
    }

    private b(AdView adView, byte b) {
        this.a = adView;
    }

    public final void onAnimationEnd(Animation animation) {
        ((RelativeLayout) this.a.e).removeView((View) this.a.d.get(0));
        this.a.d.remove(0);
        this.a.e.post(new c(this.a));
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
