package com.l.adlib_android;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.widget.RelativeLayout;
import cn.domob.android.ads.DomobAdManager;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.List;

public class AdView extends RelativeLayout implements AdListener, Runnable {
    /* access modifiers changed from: private */
    public static adata i = null;
    private static requestfreshad j = null;
    private final String a;
    private final int b;
    private final int c;
    /* access modifiers changed from: private */
    public List d;
    /* access modifiers changed from: private */
    public View e;
    private Context f;
    private boolean g;
    private locationmanager h;
    /* access modifiers changed from: private */
    public Handler k;

    public AdView(Context context) {
        this(context, null);
    }

    public AdView(Context context, int i2, int i3, int i4, int i5, int i6, boolean z) {
        super(context);
        this.a = "http://schemas.android.com/apk/res/";
        this.b = 700;
        this.c = 20;
        this.d = new ArrayList();
        this.g = true;
        this.k = new a(this);
        this.f = context;
        config.a = i2;
        config.c = i3;
        config.d = i4;
        config.e = i5;
        config.f = i6;
        config.h = z;
        b();
    }

    public AdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = "http://schemas.android.com/apk/res/";
        this.b = 700;
        this.c = 20;
        this.d = new ArrayList();
        this.g = true;
        this.k = new a(this);
        this.f = context;
        config.a = meta.getInt(context, "appkey");
        if (attributeSet != null) {
            String str = "http://schemas.android.com/apk/res/" + context.getPackageName();
            config.f = attributeSet.getAttributeIntValue(str, "backgroundTransparent", config.f);
            config.h = attributeSet.getAttributeBooleanValue(str, "autoFit", config.h);
            String attributeValue = attributeSet.getAttributeValue(str, "textColor");
            if (attributeValue != null) {
                config.e = Color.parseColor(attributeValue);
            }
            String attributeValue2 = attributeSet.getAttributeValue(str, "startBackgroundColor");
            if (attributeValue2 != null) {
                config.c = Color.parseColor(attributeValue2);
            }
            String attributeValue3 = attributeSet.getAttributeValue(str, "endBackgroundColor");
            if (attributeValue3 != null) {
                config.d = Color.parseColor(attributeValue3);
            }
        }
        b();
    }

    private void b() {
        util.Log("init");
        setBackgroundColor(0);
        setLayoutParams(util.getLayoutParams(-1, -2));
        config.i = "/data/data/" + this.f.getApplicationContext().getPackageName() + "/files";
        device.Init(this.f);
        afile.getFiles();
        this.e = this;
        util.Log("initEx");
        this.k.removeCallbacks(this);
        this.g = true;
        this.h = new locationmanager(this.f);
        if (i != null) {
            b(i);
            util.Log("get ad from buffer..." + String.valueOf(i.getAdHead().getAdId()));
        } else {
            this.k.post(this);
        }
        requestfreshad.b = this.f;
        requestfreshad.d = this.k;
        requestfreshad.c = this.h;
    }

    /* access modifiers changed from: private */
    public void b(adata adata) {
        util.Log("showFreshAdID:" + String.valueOf(adata.getAdHead().getAdId()));
        ad adVar = new ad(this.f, adata.getAdHead(), adata.getAdBody(), config.h, adata.getInBuffer());
        adVar.setonClickAdListener(this);
        adVar.setVisibility(0);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) adVar.getLayoutParams();
        layoutParams.addRule(14);
        int size = this.d.size();
        if (size > 0) {
            adVar.setVisibility(4);
        }
        this.d.add(adVar);
        addView(adVar, layoutParams);
        if (size > 0) {
            Rotate3dAnimation rotate3dAnimation = new Rotate3dAnimation(0.0f, -90.0f, ((float) getWidth()) / 2.0f, ((float) getHeight()) / 2.0f, 310.0f, true);
            rotate3dAnimation.setDuration(700);
            rotate3dAnimation.setFillAfter(true);
            rotate3dAnimation.setInterpolator(new AccelerateInterpolator());
            rotate3dAnimation.setAnimationListener(new b(this));
            startAnimation(rotate3dAnimation);
        }
    }

    public void OnClickAd(int i2) {
        Intent intent;
        if (this.d.size() > 0) {
            abody abody = (abody) ((ad) this.d.get(0)).getBodyList().get(i2 - 256);
            String trim = abody.getAction().toString().trim();
            String trim2 = abody.getActionData().toString().trim();
            String trim3 = abody.getFeeUri().toString().trim();
            if (!trim3.trim().equals("")) {
                String replace = trim3.replace("key=", "key=" + util.Encrypt(((ad) this.d.get(0)).getHead().getAdId(), config.a, device.a));
                util.Log("newFeeUri:" + replace);
                new fee(replace).start();
            }
            if (trim.equalsIgnoreCase("tel")) {
                intent = new Intent("android.intent.action.CALL", Uri.parse("tel:" + trim2));
            } else if (trim.equalsIgnoreCase(DomobAdManager.ACTION_SMS)) {
                if (trim2.trim().equalsIgnoreCase("")) {
                    Intent intent2 = new Intent("android.intent.action.VIEW");
                    intent2.setType("vnd.android-dir/mms-sms");
                    intent = intent2;
                } else {
                    intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + trim2));
                }
            } else if (trim.equalsIgnoreCase(DomobAdManager.ACTION_VIDEO)) {
                Intent intent3 = new Intent("android.intent.action.VIEW");
                intent3.setDataAndType(Uri.parse("http://" + trim2), "video/*");
                intent = intent3;
            } else if (trim.equalsIgnoreCase(DomobAdManager.ACTION_AUDIO)) {
                Intent intent4 = new Intent("android.intent.action.VIEW");
                intent4.setDataAndType(Uri.parse("http://" + trim2), "audio/mp3");
                intent = intent4;
            } else {
                intent = !trim2.trim().equalsIgnoreCase("") ? new Intent("android.intent.action.VIEW", Uri.parse("http://" + trim2)) : null;
            }
            if (intent != null) {
                util.Log("DeleteState:" + String.valueOf(((ad) this.d.get(0)).getDeleteState()));
                if (!((ad) this.d.get(0)).getDeleteState()) {
                    ((ad) this.d.get(0)).setDeleteState(true);
                    i.setInBuffer(true);
                    aqueue.Deleted();
                }
                this.f.startActivity(intent);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        util.Log("onDetachedFromWindow");
        this.k.removeCallbacks(this);
        requestfreshad.a = false;
        this.g = false;
        this.h.removeUpdates();
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i2) {
        long j2;
        super.onWindowVisibilityChanged(i2);
        switch (i2) {
            case 0:
                this.k.removeCallbacks(this);
                this.g = true;
                this.h = new locationmanager(this.f);
                if (i != null) {
                    if (i != null) {
                        long currentTimeMillis = (System.currentTimeMillis() - i.getCurrentTimeMillis()) - ((long) (i.getAdHead().getShowTime() * 1000));
                        j2 = currentTimeMillis < 0 ? Math.abs(currentTimeMillis) : 2000;
                    } else {
                        j2 = 0;
                    }
                    this.k.postDelayed(this, j2);
                    util.Log("diff:" + String.valueOf(j2));
                } else {
                    this.k.post(this);
                }
                util.Log("WindowVisibility(VISIBLE):" + String.valueOf(i2));
                return;
            case 4:
                util.Log("WindowVisibility(INVISIBLE):" + String.valueOf(i2));
                return;
            case 8:
                this.h.removeUpdates();
                util.Log("WindowVisibility(GONE):" + String.valueOf(i2));
                this.k.removeCallbacks(this);
                requestfreshad.a = false;
                this.g = false;
                return;
            default:
                return;
        }
    }

    public void run() {
        util.Log("run...");
        if (!this.g) {
            return;
        }
        if (j == null) {
            requestfreshad requestfreshad = new requestfreshad();
            j = requestfreshad;
            requestfreshad.start();
            util.Log("run start requestfreshad one...");
            return;
        }
        util.Log("thread state:" + j.getState().toString());
        if (j.getState() == Thread.State.TERMINATED) {
            requestfreshad requestfreshad2 = new requestfreshad();
            j = requestfreshad2;
            requestfreshad2.start();
            util.Log("run start requestfreshad...");
        }
    }

    public void setOnAdListenerEx(AdListenerEx adListenerEx) {
        util.a = adListenerEx;
    }
}
