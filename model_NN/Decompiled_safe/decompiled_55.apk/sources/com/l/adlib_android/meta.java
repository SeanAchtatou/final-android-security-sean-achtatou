package com.l.adlib_android;

import android.content.Context;
import android.content.pm.PackageManager;

public class meta {
    private static Object a(Context context, String str) {
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.get(str);
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }

    public static Object get(Context context, String str) {
        return a(context, str);
    }

    public static Boolean getBoolean(Context context, String str) {
        return (Boolean) a(context, str);
    }

    public static int getInt(Context context, String str) {
        return ((Integer) a(context, str)).intValue();
    }

    public static String getString(Context context, String str) {
        return (String) a(context, str);
    }
}
