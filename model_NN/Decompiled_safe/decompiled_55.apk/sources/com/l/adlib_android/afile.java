package com.l.adlib_android;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class afile {
    private static int a = 10;
    private static List b = new ArrayList();
    private static final Comparator c = new f();

    public static File contains(String str) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= b.size()) {
                return null;
            }
            File file = (File) b.get(i2);
            if (file.getName().equalsIgnoreCase(String.valueOf(str.trim()) + ".ad")) {
                return file;
            }
            i = i2 + 1;
        }
    }

    public static void getFiles() {
        b.clear();
        File[] listFiles = new File(config.i).listFiles(new g());
        if (listFiles != null) {
            for (File file : listFiles) {
                b.add(file);
                util.Log(file.getName());
            }
        }
    }

    public static void setFile(String str) {
        File file = new File(String.valueOf(config.i) + "/" + str);
        if (b.size() >= a) {
            Collections.sort(b, c);
            ((File) b.get(0)).delete();
            b.set(0, file);
            return;
        }
        b.add(file);
    }
}
