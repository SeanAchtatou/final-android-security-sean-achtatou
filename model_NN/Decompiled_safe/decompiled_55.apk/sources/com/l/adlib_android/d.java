package com.l.adlib_android;

import android.os.Handler;
import android.os.Message;
import android.widget.SpinnerAdapter;
import com.madhouse.android.ads.AdView;

final class d extends Handler {
    final /* synthetic */ AdViewFull a;

    d(AdViewFull adViewFull) {
        this.a = adViewFull;
    }

    public final void handleMessage(Message message) {
        super.handleMessage(message);
        switch (message.what) {
            case AdView.RETRUNCODE_OK /*200*/:
                AdViewFull.a(this.a);
                this.a.h.setAdapter((SpinnerAdapter) new e(this.a, this.a.f));
                if (this.a.c.size() <= 0) {
                    this.a.i.setVisibility(4);
                    break;
                } else {
                    this.a.i.setVisibility(0);
                    break;
                }
        }
        if (this.a.j != null) {
            this.a.j.dismiss();
        }
    }
}
