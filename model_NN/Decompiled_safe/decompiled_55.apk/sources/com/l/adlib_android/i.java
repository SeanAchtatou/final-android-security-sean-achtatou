package com.l.adlib_android;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

final class i extends DefaultHandler {
    private ahead a;
    private StringBuilder b;

    i() {
    }

    public final ahead a() {
        return this.a;
    }

    public final void characters(char[] cArr, int i, int i2) {
        super.characters(cArr, i, i2);
        this.b.append(cArr, i, i2);
    }

    public final void endElement(String str, String str2, String str3) {
        super.endElement(str, str2, str3);
        if (this.a != null) {
            if (str2.equalsIgnoreCase("adid")) {
                this.a.setAdId(Integer.parseInt(this.b.toString()));
            } else if (str2.equalsIgnoreCase("showtime")) {
                this.a.setShowTime(Integer.parseInt(this.b.toString()));
            } else if (str2.equalsIgnoreCase("turns")) {
                this.a.setTurns(Integer.parseInt(this.b.toString()));
            } else if (str2.equalsIgnoreCase("adtype")) {
                this.a.setAdType(Integer.parseInt(this.b.toString()));
            } else if (str2.equalsIgnoreCase("uri")) {
                this.a.setUri(this.b.toString());
            } else if (str2.equalsIgnoreCase("lastposition")) {
                this.a.setLastPosition(Integer.parseInt(this.b.toString()));
            } else {
                str2.equalsIgnoreCase("adhead");
            }
            this.b.setLength(0);
        }
    }

    public final void startDocument() {
        super.startDocument();
        this.b = new StringBuilder();
    }

    public final void startElement(String str, String str2, String str3, Attributes attributes) {
        super.startElement(str, str2, str3, attributes);
        if (str2.equalsIgnoreCase("adhead")) {
            this.a = new ahead();
        }
    }
}
