package net.youmi.android;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;

final class en extends Drawable {
    private int A;
    private int B = Color.argb(80, 0, 0, 0);
    private float C = 3.0f;
    private float D = 1.0f;
    private Path E = new Path();
    private Path F = new Path();
    private Path G = new Path();
    private Path H = new Path();

    /* renamed from: I  reason: collision with root package name */
    private Paint f0I = new Paint();
    private Shader J;
    private Shader K;
    private int L = 0;
    private int M = 2;
    private ca N;
    int a;
    int b;
    float c = 3.0f;
    float d = 1.2f;
    float e = 4.0f;
    private String f = "有米";
    private float g = 9.0f;
    private float h = 0.8f;
    private float i = 0.0f;
    private float j = 0.0f;
    private float k = 0.0f;
    private float l = 0.0f;
    private float m = 0.0f;
    private float n = 0.0f;
    private Rect o = new Rect();
    private float p = 5.0f;
    private float q = 5.0f;
    private RectF r = new RectF(0.0f, 0.0f, 320.0f, 50.0f);
    private RectF s = new RectF(this.r);
    private RectF t = new RectF(this.r);
    private int u = Color.argb(230, 250, 250, 250);
    private int v = Color.argb(230, 234, 234, 234);
    private int w = Color.rgb(217, 217, 217);
    private int x = Color.rgb(115, 115, 115);
    private int y = 255;
    private int z;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, int, int, int, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, int, float, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    en(int i2, int i3, ca caVar) {
        this.a = i2;
        this.b = i3;
        this.N = caVar;
        b();
        this.r.set(0.0f, 0.0f, (float) this.a, (float) this.b);
        this.s.set(0.0f, 0.0f, (float) this.a, (float) this.b);
        this.t.set(0.0f, 0.0f, (float) this.a, (float) this.b);
        this.t.inset(this.d, this.d);
        this.J = new LinearGradient(0.0f, 0.0f, 1.0f, 1.0f, this.u, this.v, Shader.TileMode.CLAMP);
        this.K = new LinearGradient(0.0f, 0.0f, (float) (this.N.a(5) + 0), (float) this.b, Color.argb(8, 255, 255, 255), Color.argb(50, 255, 255, 255), Shader.TileMode.CLAMP);
        this.E.addRect(this.s, Path.Direction.CW);
        this.E.close();
        this.f0I.reset();
        this.f0I.setTextSize(this.g);
        this.o.set(0, 0, this.a, this.b);
        this.f0I.getTextBounds(this.f, 0, this.f.length(), this.o);
        this.i = (((float) this.o.width()) * 1.5f) + (this.p * 2.0f) + (this.q * 2.0f);
        this.j = (((float) this.o.height()) + (this.h * 2.0f)) - this.d;
        float f2 = this.t.left;
        float f3 = this.t.right;
        float f4 = this.t.top;
        float f5 = this.t.bottom;
        RectF rectF = new RectF();
        if (this.L == 0) {
            if (this.M == 2) {
                this.i = ((float) this.o.width()) * 1.1f;
                this.j = ((float) this.o.height()) + (this.h * 2.0f);
                this.z = Math.round((((float) this.a) - this.i) - this.d);
                this.A = Math.round(this.j + f4);
                float f6 = this.j * 0.5f;
                this.k = f3 - ((this.i + this.d) * 0.5f);
                this.l = ((0.5f * f4) + this.j) - this.h;
                this.m = this.k;
                this.n = this.l;
                this.n -= this.N.a(1.0f);
                this.F.moveTo(f3 - this.e, f5);
                RectF rectF2 = new RectF();
                rectF2.set(f3 - (this.e * 2.0f), f5 - (this.e * 2.0f), f3, f5);
                this.F.arcTo(rectF2, 90.0f, -90.0f);
                this.F.lineTo(f3, this.j + f4 + f6);
                rectF.set((f3 - ((float) this.o.width())) - ((this.i - ((float) this.o.width())) * 0.4f), (f4 - this.d) + this.h, ((float) this.o.width()) + 2.0f, (float) this.o.height());
                float f7 = f3 - (this.i * 0.5f);
                float f8 = this.j + f4;
                rectF2.set(f3 - this.j, this.j + f4, f3, (this.j * 2.0f) + f4);
                this.F.arcTo(rectF2, 360.0f, -90.0f);
                this.F.lineTo(f3 - this.i, this.j + f4);
                float f9 = (this.j * 0.5f) + f4;
                float f10 = f3 - this.i;
                rectF2.set((f3 - this.i) - f6, f4, (f3 - this.i) + f6, this.j + f4);
                this.F.arcTo(rectF2, 90.0f, 90.0f);
                float f11 = (f3 - this.i) - this.p;
                rectF2.set((f3 - this.i) - (this.j * 1.5f), f4, (f3 - this.i) - f6, this.j + f4);
                this.F.arcTo(rectF2, 360.0f, -90.0f);
                this.F.lineTo(this.e + f2, f4);
                rectF2.set(f2, f4, (this.e * 2.0f) + f2, (this.e * 2.0f) + f4);
                this.F.arcTo(rectF2, 270.0f, -90.0f);
                this.F.lineTo(f2, f5 - this.e);
                rectF2.set(f2, f5 - (this.e * 2.0f), (this.e * 2.0f) + f2, f5);
                this.F.arcTo(rectF2, 180.0f, -90.0f);
                this.F.lineTo(f3 - this.e, f5);
            }
        } else if (this.L == 1) {
        }
        this.F.close();
        this.F.setFillType(Path.FillType.INVERSE_EVEN_ODD);
        RectF rectF3 = new RectF(this.t);
        rectF3.inset(-10.0f, -10.0f);
        this.G.addRect(rectF3, Path.Direction.CW);
        this.G.addPath(this.F);
        this.G.close();
        this.H.moveTo(0.0f, 0.0f);
        this.H.lineTo((float) this.a, 0.0f);
        this.H.cubicTo(((float) this.a) * 0.5f, ((float) this.b) * 0.25f, ((float) this.a) * 0.25f, ((float) this.b) * 0.5f, 0.0f, (float) this.b);
        this.H.lineTo(0.0f, 0.0f);
        this.H.close();
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.z;
    }

    /* access modifiers changed from: protected */
    public void b() {
        this.g = this.N.i().a();
        this.c = this.N.a(this.c);
        this.d = this.N.a(this.d);
        this.e = this.N.a(this.e);
        this.h = this.N.a(this.h);
        this.p = this.N.a(this.p);
        this.q = this.N.a(this.q);
        this.C = this.N.a(this.C);
        this.D = this.N.a(this.D);
    }

    public void draw(Canvas canvas) {
        this.f0I.reset();
        this.f0I.setShadowLayer(this.C, this.D, this.D, this.B);
        this.G.setFillType(Path.FillType.WINDING);
        canvas.drawPath(this.G, this.f0I);
        this.f0I.reset();
        this.f0I.setShader(this.J);
        this.f0I.setAntiAlias(true);
        canvas.drawPath(this.F, this.f0I);
        this.f0I.reset();
        this.f0I.setStrokeWidth(1.0f);
        this.f0I.setStyle(Paint.Style.STROKE);
        this.f0I.setColor(-1);
        this.f0I.setAntiAlias(true);
        canvas.drawPath(this.F, this.f0I);
        this.f0I.setStrokeWidth(0.5f);
        this.f0I.setColor(-7829368);
        canvas.drawRect(this.s, this.f0I);
        this.f0I.reset();
        this.f0I.setTextAlign(Paint.Align.CENTER);
        this.f0I.setTextSize(this.g);
        this.f0I.setColor(-1);
        this.f0I.setAntiAlias(true);
        canvas.drawText(this.f, this.k, this.l, this.f0I);
        this.f0I.setColor(this.x);
        canvas.drawText(this.f, this.m, this.n, this.f0I);
        this.f0I.reset();
        this.f0I.setShader(this.K);
        this.f0I.setAntiAlias(true);
        canvas.drawPath(this.H, this.f0I);
    }

    public int getOpacity() {
        return 255 - this.y;
    }

    public void setAlpha(int i2) {
        this.y = i2;
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }
}
