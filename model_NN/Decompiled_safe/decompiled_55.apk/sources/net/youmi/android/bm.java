package net.youmi.android;

import android.app.Activity;
import android.view.animation.Animation;
import android.widget.RelativeLayout;

class bm extends RelativeLayout implements bb {
    final /* synthetic */ cx a;
    private aw b;
    private Activity c;
    private AdView d;
    private em e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bm(cx cxVar, Activity activity, AdView adView) {
        super(activity);
        this.a = cxVar;
        this.c = activity;
        this.d = adView;
    }

    private void e() {
        if (this.b == null) {
            this.b = new aw(this.c, null);
            RelativeLayout.LayoutParams a2 = h.a(this.d.getAdWidth(), this.d.getAdHeight());
            this.b.setClickable(false);
            this.b.setScrollContainer(false);
            addView(this.b, a2);
        }
    }

    public void a() {
        setVisibility(8);
    }

    public void a(Animation animation) {
    }

    public boolean a(cu cuVar) {
        em j;
        try {
            e();
            if (!(cuVar == null || (j = cuVar.j()) == null)) {
                this.e = j;
                return true;
            }
        } catch (Exception e2) {
            f.a(e2);
        }
        return false;
    }

    public void b() {
        e();
        try {
            this.b.a(this.e);
            setVisibility(0);
        } catch (Exception e2) {
        }
    }

    public void c() {
        setVisibility(0);
    }

    public void d() {
        e();
        try {
            this.b.a(this.e);
            setVisibility(0);
        } catch (Exception e2) {
        }
    }
}
