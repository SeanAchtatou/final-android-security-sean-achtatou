package net.youmi.android;

import android.content.Context;
import android.content.Intent;

class p implements Runnable {
    final /* synthetic */ eo a;
    private final /* synthetic */ Intent b;
    private final /* synthetic */ Context c;

    p(eo eoVar, Intent intent, Context context) {
        this.a = eoVar;
        this.b = intent;
        this.c = context;
    }

    public void run() {
        try {
            String schemeSpecificPart = this.b.getData().getSchemeSpecificPart();
            if (schemeSpecificPart != null) {
                k.a(this.c, schemeSpecificPart, this.b.getAction());
            }
        } catch (Exception e) {
        }
    }
}
