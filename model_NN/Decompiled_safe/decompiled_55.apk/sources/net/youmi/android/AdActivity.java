package net.youmi.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import com.guohead.sdk.util.GuoheAdUtil;

public final class AdActivity extends Activity {
    cu a;
    private ff b;
    private ca c;

    static Intent a(Context context) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), AdActivity.class.getName());
        intent.putExtra("D780FBF4215247bcBB1AC0AD33C474FE", "FEDC335110C04414AF100EA25C26A70D");
        return intent;
    }

    static void a(Activity activity) {
        Intent intent = new Intent(activity, AdActivity.class);
        intent.putExtra("D780FBF4215247bcBB1AC0AD33C474FE", "FD7C4B12A60F415dBE8C580A137F5F1C");
        activity.startActivity(intent);
    }

    static void a(Activity activity, String[] strArr, String str, String str2) {
        if (strArr != null) {
            try {
                if (strArr.length != 0) {
                    Intent intent = new Intent(activity, AdActivity.class);
                    intent.putExtra("D780FBF4215247bcBB1AC0AD33C474FE", "DB9C288EF60A40d4897665843327626E");
                    intent.putExtra("EB80F3291A8E469c962CA133BDC549D7", strArr);
                    intent.putExtra("172C94EDC717477aBF600D7898A64A8E", str);
                    intent.putExtra("D50EF1926ADD471892E72BCE6D7E032C", str2);
                    activity.startActivity(intent);
                }
            } catch (Exception e) {
            }
        }
    }

    private void b() {
        String[] stringArrayExtra;
        cu cuVar;
        Intent intent = getIntent();
        String stringExtra = intent.getStringExtra("D780FBF4215247bcBB1AC0AD33C474FE");
        if (stringExtra.equals("A821718FB7F248b590F3721F6576D289")) {
            this.b = new ex(this, this.c);
            requestWindowFeature(2);
            setProgressBarIndeterminate(false);
            setContentView(this.b.b());
            return;
        }
        if (stringExtra.equals("FD7C4B12A60F415dBE8C580A137F5F1C") && (cuVar = dp.a) != null) {
            this.a = cuVar;
            switch (cuVar.a()) {
                case GuoheAdUtil.NETWORK_TYPE_MOBCLIX:
                    if (cuVar.i() == null) {
                        a();
                        break;
                    } else {
                        ex exVar = new ex(this, this.c);
                        requestWindowFeature(2);
                        setProgressBarIndeterminate(false);
                        this.b = exVar;
                        setContentView(this.b.b());
                        exVar.a(cuVar.i());
                        return;
                    }
            }
        }
        if (stringExtra.equals("FEDC335110C04414AF100EA25C26A70D")) {
            setTitle("下载管理");
            this.b = new fg(this, this.c);
            setContentView(this.b.b());
        } else if (!stringExtra.equals("DB9C288EF60A40d4897665843327626E") || (stringArrayExtra = intent.getStringArrayExtra("EB80F3291A8E469c962CA133BDC549D7")) == null || stringArrayExtra.length <= 0) {
            a();
        } else {
            requestWindowFeature(1);
            getWindow().setFlags(1024, 1024);
            this.b = new cf(this, this.c, stringArrayExtra, intent.getStringExtra("172C94EDC717477aBF600D7898A64A8E"), intent.getStringExtra("D50EF1926ADD471892E72BCE6D7E032C"));
            setContentView(this.b.b());
        }
    }

    static void b(Activity activity) {
        Intent intent = new Intent(activity, AdActivity.class);
        intent.putExtra("D780FBF4215247bcBB1AC0AD33C474FE", "FEDC335110C04414AF100EA25C26A70D");
        activity.startActivity(intent);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        try {
            finish();
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.c = ca.a(this);
        b();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            try {
                if (this.b != null) {
                    this.b.b_();
                    return true;
                }
            } catch (Exception e) {
            }
        }
        return super.onKeyDown(i, keyEvent);
    }
}
