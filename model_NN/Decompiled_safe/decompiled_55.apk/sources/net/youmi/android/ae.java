package net.youmi.android;

import com.madhouse.android.ads.AdView;

class ae {
    final /* synthetic */ y a;
    private int b = 0;
    private int c = 0;
    private int d = 5;
    private int e = 0;
    private int f = 0;
    private float g = 0.0f;

    ae(y yVar, ca caVar, y yVar2) {
        this.a = yVar;
        if (!caVar.c()) {
            switch (caVar.e()) {
                case 120:
                    this.b = 18;
                    this.c = 21;
                    this.e = 27;
                    this.g = 14.0f;
                    break;
                case 160:
                    this.b = 24;
                    this.c = 28;
                    this.e = 36;
                    this.g = 16.0f;
                    break;
                case AdView.AD_MEASURE_240:
                    this.b = 36;
                    this.c = 42;
                    this.e = 54;
                    this.g = 23.0f;
                    break;
                case AdView.AD_MEASURE_320:
                    this.b = 48;
                    this.c = 56;
                    this.e = 72;
                    this.g = 28.0f;
                    break;
                default:
                    this.b = 24;
                    this.c = 28;
                    this.e = 36;
                    this.g = 16.0f;
                    break;
            }
        } else {
            this.b = 24;
            this.c = 28;
            this.e = 36;
            this.g = 16.0f;
        }
        this.d = caVar.a(5);
        this.f = (this.e - this.c) / 2;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public int c() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public int d() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public int e() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public float f() {
        return this.g;
    }
}
