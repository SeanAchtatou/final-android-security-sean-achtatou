package net.youmi.android;

import com.madhouse.android.ads.AdView;

class af {
    final /* synthetic */ y a;
    private int b = 0;
    private int c = 0;

    af(y yVar, ca caVar, y yVar2) {
        this.a = yVar;
        if (!caVar.c()) {
            switch (caVar.e()) {
                case 120:
                    this.c = 27;
                    break;
                case 160:
                    this.c = 36;
                    break;
                case AdView.AD_MEASURE_240:
                    this.c = 54;
                    break;
                case AdView.AD_MEASURE_320:
                    this.c = 72;
                    break;
                default:
                    this.c = 36;
                    break;
            }
        } else {
            this.c = 36;
        }
        this.b = (yVar2.a() - this.c) / 2;
        if (this.b < 0) {
            this.b = 0;
        }
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.c;
    }
}
