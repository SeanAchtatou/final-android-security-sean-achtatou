package net.youmi.android;

import android.app.Activity;
import android.widget.FrameLayout;

class dt extends FrameLayout {
    static int e = -1;
    Activity a;
    ca b;
    g c;
    cb d;
    protected int f;
    private int g;
    private int h = -1;

    public dt(Activity activity, co coVar, ca caVar) {
        super(activity);
        int i = e + 1;
        e = i;
        this.f = i;
        this.a = activity;
        this.b = caVar;
        this.c = new g(this.a, coVar, this.b);
        addView(this.c);
        this.d = new cb(this.a);
        addView(this.d);
    }

    /* access modifiers changed from: package-private */
    public void a(int i) {
        this.h = i;
    }

    /* access modifiers changed from: package-private */
    public void a(ci ciVar) {
        this.g = 0;
        if (this.d != null) {
            this.d.setVisibility(8);
        }
        this.c.a(ciVar);
    }

    /* access modifiers changed from: package-private */
    public void a(m mVar) {
        this.g = 1;
        if (this.c != null) {
            this.c.setVisibility(8);
        }
        this.d.a(mVar);
    }
}
