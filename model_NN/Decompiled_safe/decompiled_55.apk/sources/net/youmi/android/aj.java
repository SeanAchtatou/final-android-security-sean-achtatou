package net.youmi.android;

import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;

class aj {
    private static Animation a;
    private static Animation b;
    private static Animation c;
    private static Animation d;

    aj() {
    }

    private static Animation a(int i) {
        AnimationSet animationSet = new AnimationSet(true);
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) i);
        translateAnimation.setDuration(300);
        animationSet.addAnimation(translateAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(300);
        animationSet.addAnimation(alphaAnimation);
        return animationSet;
    }

    static Animation a(ca caVar) {
        if (c == null) {
            c = e(caVar);
        }
        return c;
    }

    static Animation b(ca caVar) {
        if (d == null) {
            d = a(caVar.a().a());
        }
        return d;
    }

    static Animation c(ca caVar) {
        if (a == null) {
            a = f(caVar);
        }
        return a;
    }

    static Animation d(ca caVar) {
        if (b == null) {
            b = g(caVar);
        }
        return b;
    }

    private static Animation e(ca caVar) {
        AnimationSet animationSet = new AnimationSet(true);
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, (float) (-caVar.a().a()), 0.0f);
        translateAnimation.setDuration(300);
        animationSet.addAnimation(translateAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(300);
        animationSet.addAnimation(alphaAnimation);
        return animationSet;
    }

    private static Animation f(ca caVar) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(600);
        return alphaAnimation;
    }

    private static Animation g(ca caVar) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(600);
        return alphaAnimation;
    }
}
