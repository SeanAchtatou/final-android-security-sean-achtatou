package net.youmi.android;

import android.app.Activity;
import android.widget.RelativeLayout;
import android.widget.TextView;

class g extends RelativeLayout {
    Activity a;
    ca b;
    co c;
    TextView d;
    TextView e;
    dk f;

    public g(Activity activity, co coVar, ca caVar) {
        super(activity);
        this.a = activity;
        this.c = coVar;
        this.b = caVar;
        a();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.d = new TextView(this.a);
        this.d.setId(1013);
        this.e = new TextView(this.a);
        this.e.setId(1014);
        this.f = new dk(this.a, this.b);
        this.f.setId(1016);
        RelativeLayout.LayoutParams a2 = h.a();
        a2.addRule(9);
        a2.addRule(10);
        RelativeLayout.LayoutParams a3 = h.a();
        a3.addRule(9);
        a3.addRule(3, this.d.getId());
        RelativeLayout.LayoutParams b2 = h.b((this.b.d() * 80) / 100);
        b2.addRule(12);
        b2.addRule(3, this.e.getId());
        b2.setMargins(0, 10, 0, 10);
        addView(this.f, b2);
        addView(this.d, a2);
        addView(this.e, a3);
    }

    /* access modifiers changed from: package-private */
    public void a(ci ciVar) {
        if (ciVar != null) {
            if (ciVar.c.exists()) {
                this.d.setText(ciVar.a);
                long length = ciVar.c.length();
                if (ciVar.d > 0) {
                    this.e.setText(String.valueOf(length) + "/" + ciVar.d);
                    dk dkVar = this.f;
                    dkVar.a((int) ((length * 100) / ciVar.d));
                } else {
                    this.e.setText(new StringBuilder(String.valueOf(length)).toString());
                    this.f.setVisibility(4);
                }
            } else {
                this.c.a();
            }
        }
        setVisibility(0);
    }
}
