package net.youmi.android;

import android.content.Context;
import org.apache.http.HttpHost;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

abstract class dh implements dn {
    private static String h;
    protected String a;
    protected String b;
    protected long c;
    protected long d = 0;
    protected Context e;
    protected int f = -1;
    protected cc g;

    dh() {
    }

    static String d() {
        if (h == null) {
            h = "Mozilla/5.0(Linux;U;" + eh.c() + ";" + eh.a() + ";" + eh.b() + ") AppleWebkit/533.1(KHTML,like Gecko) Version/4.0 Mobile Safari/533.1";
        }
        return h;
    }

    /* access modifiers changed from: protected */
    public abstract int a();

    public int a(Context context, String str) {
        try {
            this.e = context;
            this.a = str;
            this.b = str;
            int a2 = a();
            return a2 != 0 ? a2 : b();
        } catch (Exception e2) {
            return 4;
        }
    }

    /* access modifiers changed from: protected */
    public void a(long j) {
        this.d = j;
        this.f = 5;
        i();
    }

    /* access modifiers changed from: protected */
    public abstract int b();

    /* access modifiers changed from: protected */
    public HttpParams e() {
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpClientParams.setRedirecting(basicHttpParams, true);
        HttpProtocolParams.setUserAgent(basicHttpParams, d());
        HttpProtocolParams.setContentCharset(basicHttpParams, "utf-8");
        HttpProtocolParams.setHttpElementCharset(basicHttpParams, "utf-8");
        if (s.a(this.e).equals("cmwap")) {
            basicHttpParams.setParameter("http.route.default-proxy", new HttpHost("10.0.0.172", 80, "http"));
        }
        return basicHttpParams;
    }

    /* access modifiers changed from: protected */
    public DefaultHttpClient f() {
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(e());
        defaultHttpClient.setRedirectHandler(new cw(this));
        return defaultHttpClient;
    }

    public int g() {
        return this.f;
    }

    public String h() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public void i() {
        if (this.g != null) {
            this.g.a(this);
        }
    }
}
