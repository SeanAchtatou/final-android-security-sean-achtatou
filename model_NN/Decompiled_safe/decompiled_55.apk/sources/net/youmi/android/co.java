package net.youmi.android;

import android.app.Activity;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import java.util.ArrayList;

class co extends LinearLayout {
    Activity a;
    ca b;
    boolean c = false;
    boolean d = false;
    fa e;
    Cdo f;
    ArrayList g;
    ArrayList h;
    ExpandableListView i;
    Runnable j = new ac(this);

    public co(Activity activity, ca caVar) {
        super(activity);
        this.a = activity;
        this.b = caVar;
        this.h = r.b();
        this.g = r.a();
        b();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.h = r.b();
        this.g = r.a();
        if (this.g == null) {
            r.e(this.a);
        } else if (this.g.size() == 0) {
            r.e(this.a);
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        this.i = new ExpandableListView(this.a);
        this.f = new Cdo(this, this.a, this, this.b, this.i, this.h, this.g);
        this.i.setAdapter(this.f);
        addView(this.i, new ViewGroup.LayoutParams(-1, -1));
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.e = new fa(new ad(this), -1);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        try {
            this.e.c();
        } catch (Exception e2) {
        }
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z) {
            try {
                this.e.b();
            } catch (Exception e2) {
            }
        } else {
            this.e.a();
        }
    }
}
