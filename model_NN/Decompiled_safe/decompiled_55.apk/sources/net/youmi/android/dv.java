package net.youmi.android;

import com.madhouse.android.ads.AdView;

final class dv {
    static final dv a = new dv(AdView.AD_MEASURE_240, 38);
    static final dv b = new dv(AdView.AD_MEASURE_320, 50);
    static final dv c = new dv(AdView.AD_MEASURE_480, 75);
    static final dv d = new dv(AdView.AD_MEASURE_640, 100);
    private int e;
    private int f;

    dv(int i, int i2) {
        this.e = i;
        this.f = i2;
    }

    public int a() {
        return this.e;
    }

    public int b() {
        return this.f;
    }
}
