package com.vpon.adon.android.webClientHandler;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.vpon.adon.android.entity.Ad;

public class TelHandler extends WebClientHandler {
    public TelHandler(WebClientHandler next) {
        super(next);
    }

    public boolean handle(Context context, Ad ad, String url) {
        if (!url.startsWith("tel:")) {
            return doNext(context, ad, url);
        }
        context.startActivity(new Intent("android.intent.action.DIAL", Uri.parse(url)));
        return true;
    }
}
