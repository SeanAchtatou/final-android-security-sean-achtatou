package com.mobclick.android;

import android.content.Context;
import org.json.JSONObject;

final class l implements Runnable {
    private static final Object a = new Object();
    private MobclickAgent b = MobclickAgent.a;
    private Context c;
    private JSONObject d;

    l(MobclickAgent mobclickAgent, Context context, JSONObject jSONObject) {
        this.c = context;
        this.d = jSONObject;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r4 = this;
            org.json.JSONObject r0 = r4.d     // Catch:{ Exception -> 0x0027 }
            java.lang.String r1 = "type"
            java.lang.String r0 = r0.getString(r1)     // Catch:{ Exception -> 0x0027 }
            java.lang.String r1 = "update"
            if (r0 != r1) goto L_0x0016
            com.mobclick.android.MobclickAgent r0 = r4.b     // Catch:{ Exception -> 0x0027 }
            android.content.Context r1 = r4.c     // Catch:{ Exception -> 0x0027 }
            org.json.JSONObject r2 = r4.d     // Catch:{ Exception -> 0x0027 }
            r0.c(r1, r2)     // Catch:{ Exception -> 0x0027 }
        L_0x0015:
            return
        L_0x0016:
            java.lang.Object r0 = com.mobclick.android.l.a     // Catch:{ Exception -> 0x0027 }
            monitor-enter(r0)     // Catch:{ Exception -> 0x0027 }
            com.mobclick.android.MobclickAgent r1 = r4.b     // Catch:{ all -> 0x0024 }
            android.content.Context r2 = r4.c     // Catch:{ all -> 0x0024 }
            org.json.JSONObject r3 = r4.d     // Catch:{ all -> 0x0024 }
            r1.e(r2, r3)     // Catch:{ all -> 0x0024 }
            monitor-exit(r0)     // Catch:{ all -> 0x0024 }
            goto L_0x0015
        L_0x0024:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0024 }
            throw r1     // Catch:{ Exception -> 0x0027 }
        L_0x0027:
            r0 = move-exception
            java.lang.String r1 = "MobclickAgent"
            java.lang.String r2 = "Exception occurred when sending message."
            android.util.Log.e(r1, r2)
            r0.printStackTrace()
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclick.android.l.run():void");
    }
}
