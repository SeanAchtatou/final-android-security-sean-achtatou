package com.mobclick.android;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class r {
    public static String a(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return null;
        }
        if (activeNetworkInfo.getType() == 1) {
            return null;
        }
        String extraInfo = activeNetworkInfo.getExtraInfo();
        Log.i("TAG", "net type:" + extraInfo);
        if (extraInfo.equals("cmnet")) {
            return null;
        }
        if (extraInfo.equals("cmwap") || extraInfo.equals("3gwap")) {
            return "10.0.0.172";
        }
        if (extraInfo.equals("3gnet")) {
            return null;
        }
        if (extraInfo.equals("uninet")) {
            return null;
        }
        if (extraInfo.equals("uniwap")) {
            return null;
        }
        if (extraInfo.equals("ctwap")) {
            return "10.0.0.200";
        }
        return null;
    }
}
