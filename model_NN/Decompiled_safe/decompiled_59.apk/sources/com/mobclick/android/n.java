package com.mobclick.android;

import android.content.Context;
import java.text.SimpleDateFormat;
import java.util.Date;

public class n {
    private static String[] A = {"Gender", "Male", "Female"};
    private static String B = "应用程序有新版本更新";
    private static String C = "New version found";
    private static String D = "最新版本: ";
    private static String E = "Latest version: ";
    private static String F = "（提示：非WIFI环境）";
    private static String G = "(Warning: Not WIFI Condition)";
    private static String H = "立即更新";
    private static String I = "Update now";
    private static String J = "应用更新";
    private static String K = "App updating";
    private static String L = "正在更新应用程序...";
    private static String M = "Updating application...";
    private static String N = "以后再说";
    private static String O = "Not now";
    public static String a = "last_send_time";
    public static final String b = "MobclickAgent";
    public static final String c = "http://www.umeng.com/app_logs";
    public static final String d = "http://www.umeng.com/api/check_app_update";
    public static final String e = "http://www.umeng.co/app_logs";
    public static final String f = "http://www.umeng.co/api/check_app_update";
    public static boolean g = true;
    public static String h = "未联网";
    public static String i = "Please make sure you are connected to internet, update failed";
    public static final String j = "正在更新中....";
    public static final String k = "Is updating....";
    public static final String l = "最新版本已下载，是否安装？";
    public static final String m = "The lastest version has been downloaded, install now ?";
    private static String n = "用户反馈";
    private static String o = "Feedback";
    private static String p = "欢迎您提出宝贵的意见和建议，您留下的每个字都將用来改善我们的服务";
    private static String q = "Any comments and suggestions are welcome, we believe every word you write will benefit us";
    private static String r = "请输入您的反馈意见（字数500以内）";
    private static String s = "Input your suggestions here";
    private static String t = "提交反馈";
    private static String u = "Submit suggestions";
    private static String v = "请正确选择年龄和性别再提交";
    private static String w = "Please fill in a correct age and gender before submitting";
    private static String[] x = {"年龄", "18岁以下", "18-24岁", "25-30岁", "31-35岁", "36-40岁", "41-50岁", "51-59岁", "60岁及以上"};
    private static String[] y = {"Age", "<18", "18~24", "25~30", "31~35", "36~40", "41~50", "51~59", ">=60"};
    private static String[] z = {"性别", "男", "女"};

    public static String A() {
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    }

    public static int a(Date date, Date date2) {
        Date date3;
        Date date4;
        if (date.after(date2)) {
            date3 = date;
            date4 = date2;
        } else {
            date3 = date2;
            date4 = date;
        }
        return (int) ((date3.getTime() - date4.getTime()) / 86400000);
    }

    public static String a() {
        return n;
    }

    public static String a(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? n : o;
    }

    public static Date a(String str) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(str);
        } catch (Exception e2) {
            return null;
        }
    }

    public static String b() {
        return o;
    }

    public static String b(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? p : q;
    }

    public static String c() {
        return p;
    }

    public static String c(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? r : s;
    }

    public static String d() {
        return q;
    }

    public static String d(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? t : u;
    }

    public static String e() {
        return r;
    }

    public static String e(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? v : w;
    }

    public static String f() {
        return s;
    }

    public static String[] f(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? x : y;
    }

    public static String g() {
        return t;
    }

    public static String[] g(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? z : A;
    }

    public static String h() {
        return u;
    }

    public static String h(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? B : C;
    }

    public static String i() {
        return v;
    }

    public static String i(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? D : E;
    }

    public static String j() {
        return w;
    }

    public static String j(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? F : G;
    }

    public static String k(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? H : I;
    }

    public static String[] k() {
        return x;
    }

    public static String l(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? J : K;
    }

    public static String[] l() {
        return y;
    }

    public static String m(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? L : M;
    }

    public static String[] m() {
        return z;
    }

    public static String n(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? N : O;
    }

    public static String[] n() {
        return A;
    }

    public static String o() {
        return B;
    }

    public static String o(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? j : k;
    }

    public static String p() {
        return C;
    }

    public static String p(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? l : m;
    }

    public static String q() {
        return D;
    }

    public static String r() {
        return E;
    }

    public static String s() {
        return H;
    }

    public static String t() {
        return I;
    }

    public static String u() {
        return J;
    }

    public static String v() {
        return K;
    }

    public static String w() {
        return L;
    }

    public static String x() {
        return M;
    }

    public static String y() {
        return N;
    }

    public static String z() {
        return O;
    }
}
