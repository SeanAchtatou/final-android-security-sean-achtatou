package com.mobclick.android;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import javax.microedition.khronos.opengles.GL10;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MobclickAgent implements e {
    public static boolean LOCATION_OPEN = true;
    /* access modifiers changed from: private */
    public static final MobclickAgent a = new MobclickAgent();
    private static final String b = "Android";
    private static final String c = "Android";
    private static final String d = "2.2";
    private static final long e = 30000;
    public static boolean enableCacheInUpdate = true;
    private static final int f = 8;
    private static int g = 1;
    private static String j = "";
    private static String k = "";
    private static boolean l = true;
    private static UmengUpdateListener m = null;
    private static UmengFeedbackListener n = null;
    private static boolean o = true;
    private static JSONObject p = null;
    public static boolean testMode = false;
    public static boolean updateAutoPopup = true;
    private Context h;
    private final Handler i;

    private MobclickAgent() {
        HandlerThread handlerThread = new HandlerThread(n.b);
        handlerThread.start();
        this.i = new Handler(handlerThread.getLooper());
    }

    private static String a(Context context) {
        String str = null;
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null) {
                String string = applicationInfo.metaData.getString("UMENG_APPKEY");
                if (string != null) {
                    str = string;
                } else {
                    Log.i(n.b, "Could not read UMENG_APPKEY meta-data from AndroidManifest.xml.");
                }
            }
        } catch (Exception e2) {
            Log.i(n.b, "Could not read UMENG_APPKEY meta-data from AndroidManifest.xml.", e2);
            e2.printStackTrace();
        }
        return str.trim();
    }

    private String a(Context context, SharedPreferences sharedPreferences) {
        Long valueOf = Long.valueOf(System.currentTimeMillis());
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putLong("start_millis", valueOf.longValue());
        edit.putLong("end_millis", -1);
        edit.commit();
        return sharedPreferences.getString("session_id", null);
    }

    private String a(Context context, String str, SharedPreferences sharedPreferences) {
        c(context, sharedPreferences);
        long currentTimeMillis = System.currentTimeMillis();
        String str2 = String.valueOf(str) + String.valueOf(currentTimeMillis);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("appkey", str);
        edit.putString("session_id", str2);
        edit.putLong("start_millis", currentTimeMillis);
        edit.putLong("end_millis", -1);
        edit.putLong("duration", 0);
        edit.putString("activities", "");
        edit.commit();
        b(context, sharedPreferences);
        return str2;
    }

    private static String a(Context context, JSONObject jSONObject, String str, boolean z) {
        Log.i(n.b, jSONObject.toString());
        HttpPost httpPost = new HttpPost(str);
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 10000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 30000);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
        try {
            String a2 = r.a(context);
            if (a2 != null) {
                Log.i("TAG", "Proxy IP:" + a2);
                defaultHttpClient.getParams().setParameter("http.route.default-proxy", new HttpHost(a2, 80));
            }
            String jSONObject2 = jSONObject.toString();
            if (!n.g || z) {
                ArrayList arrayList = new ArrayList(1);
                arrayList.add(new BasicNameValuePair("content", jSONObject2));
                httpPost.setEntity(new UrlEncodedFormEntity(arrayList, "UTF-8"));
            } else {
                byte[] b2 = o.b("content=" + jSONObject2);
                httpPost.addHeader("Content-Encoding", "deflate");
                httpPost.setEntity(new InputStreamEntity(new ByteArrayInputStream(b2), (long) o.b));
            }
            SharedPreferences.Editor edit = j(context).edit();
            Date date = new Date();
            HttpResponse execute = defaultHttpClient.execute(httpPost);
            long time = new Date().getTime() - date.getTime();
            if (execute.getStatusLine().getStatusCode() == 200) {
                Log.i(n.b, "Sent message to " + str);
                edit.putLong("req_time", time);
                edit.commit();
                HttpEntity entity = execute.getEntity();
                if (entity != null) {
                    return a(entity.getContent());
                }
                return null;
            }
            edit.putLong("req_time", -1);
            return null;
        } catch (ClientProtocolException e2) {
            Log.i(n.b, "ClientProtocolException,Failed to send message.", e2);
            e2.printStackTrace();
            return null;
        } catch (IOException e3) {
            Log.i(n.b, "IOException,Failed to send message.", e3);
            e3.printStackTrace();
            return null;
        }
    }

    private static String a(InputStream inputStream) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream), 8192);
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    try {
                        inputStream.close();
                        return sb.toString();
                    } catch (IOException e2) {
                        Log.e(n.b, "Caught IOException in convertStreamToString()", e2);
                        e2.printStackTrace();
                        return null;
                    }
                } else {
                    sb.append(String.valueOf(readLine) + "\n");
                }
            } catch (IOException e3) {
                Log.e(n.b, "Caught IOException in convertStreamToString()", e3);
                e3.printStackTrace();
                try {
                    inputStream.close();
                    return null;
                } catch (IOException e4) {
                    Log.e(n.b, "Caught IOException in convertStreamToString()", e4);
                    e4.printStackTrace();
                    return null;
                }
            } catch (Throwable th) {
                try {
                    inputStream.close();
                    throw th;
                } catch (IOException e5) {
                    Log.e(n.b, "Caught IOException in convertStreamToString()", e5);
                    e5.printStackTrace();
                    return null;
                }
            }
        }
    }

    private void a(Context context, SharedPreferences sharedPreferences, String str, String str2, int i2) {
        String string = sharedPreferences.getString("session_id", "");
        String b2 = b();
        String str3 = b2.split(" ")[0];
        String str4 = b2.split(" ")[1];
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("type", "event");
            jSONObject.put("session_id", string);
            jSONObject.put("date", str3);
            jSONObject.put("time", str4);
            jSONObject.put("tag", str);
            jSONObject.put("label", str2);
            jSONObject.put("acc", i2);
            this.i.post(new l(this, context, jSONObject));
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    private synchronized void a(Context context, String str) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("type", "update");
            jSONObject.put("appkey", str);
            int i2 = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
            String packageName = context.getPackageName();
            jSONObject.put("version_code", i2);
            jSONObject.put("package", packageName);
            jSONObject.put("sdk_version", d);
            String b2 = o.b(context);
            String a2 = o.a(b2);
            if (b2 == null || b2.equals("")) {
                Log.e(n.b, "No device id");
            }
            jSONObject.put("idmd5", a2);
            jSONObject.put("channel", b(context));
            this.i.post(new l(this, context, jSONObject));
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return;
    }

    /* access modifiers changed from: private */
    public synchronized void a(Context context, String str, String str2) {
        this.h = context;
        SharedPreferences l2 = l(context);
        if (l2 != null) {
            if (a(l2)) {
                Log.i(n.b, "Start new session: " + a(context, str, l2));
            } else {
                Log.i(n.b, "Extend current session: " + a(context, l2));
            }
        }
    }

    /* access modifiers changed from: private */
    public synchronized void a(Context context, String str, String str2, String str3, int i2) {
        SharedPreferences l2 = l(context);
        if (l2 != null) {
            a(context, l2, str2, str3, i2);
        }
    }

    private synchronized void a(Context context, JSONObject jSONObject) {
        if (jSONObject != null) {
            b(context, jSONObject);
        }
    }

    private boolean a(SharedPreferences sharedPreferences) {
        return testMode || System.currentTimeMillis() - sharedPreferences.getLong("end_millis", -1) > e;
    }

    private static boolean a(String str, Context context) {
        if (context.getPackageManager().checkPermission("android.permission.ACCESS_NETWORK_STATE", context.getPackageName()) == 0 && !o(context)) {
            return false;
        }
        if (str == "update" || str == "feedback") {
            return true;
        }
        if (g == 3) {
            if (str == "flush") {
                return true;
            }
        } else if (str == "error") {
            return true;
        } else {
            if (g == 1 && str == "launch") {
                return true;
            }
            if (g == 2 && str == "terminate") {
                return true;
            }
            if (g == 0) {
                return true;
            }
            if (g == 4) {
                String string = k(context).getString(n.A(), "false");
                Log.i(n.b, "Log has been sent today: " + string + ";type:" + str);
                return !string.equals("true") && str.equals("launch");
            } else if (g == 5) {
                return p(context)[0].equals("Wi-Fi");
            }
        }
        return false;
    }

    private static AlertDialog b(Context context, File file) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(n.h(context)).setMessage(n.p(context)).setCancelable(false).setPositiveButton(n.k(context), new g(context, file)).setNegativeButton(n.n(context), new h());
        AlertDialog create = builder.create();
        create.setCancelable(true);
        return create;
    }

    private static String b() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
    }

    private static String b(Context context) {
        Object obj;
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo == null || applicationInfo.metaData == null || (obj = applicationInfo.metaData.get("UMENG_CHANNEL")) == null) {
                return "Unknown";
            }
            String obj2 = obj.toString();
            if (obj2 != null) {
                return obj2;
            }
            Log.i(n.b, "Could not read UMENG_CHANNEL meta-data from AndroidManifest.xml.");
            return "Unknown";
        } catch (Exception e2) {
            Log.i(n.b, "Could not read UMENG_CHANNEL meta-data from AndroidManifest.xml.", e2);
            e2.printStackTrace();
            return "Unknown";
        }
    }

    private void b(Context context, SharedPreferences sharedPreferences) {
        String string = sharedPreferences.getString("session_id", null);
        if (string == null) {
            Log.e(n.b, "Missing session_id, ignore message");
            return;
        }
        String b2 = b();
        String str = b2.split(" ")[0];
        String str2 = b2.split(" ")[1];
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("type", "launch");
            jSONObject.put("session_id", string);
            jSONObject.put("date", str);
            jSONObject.put("time", str2);
            this.i.post(new l(this, context, jSONObject));
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public synchronized void b(Context context, String str) {
        String c2 = c(context);
        if (c2 != "" && c2.length() <= 10240) {
            c(context, c2);
        }
    }

    private void b(Context context, JSONObject jSONObject) {
        String b2 = b();
        String str = b2.split(" ")[0];
        String str2 = b2.split(" ")[1];
        try {
            jSONObject.put("type", "feedback");
            jSONObject.put("date", str);
            jSONObject.put("time", str2);
            this.i.post(new l(this, context, jSONObject));
        } catch (JSONException e2) {
            n.onFeedbackReturned(FeedbackStatus.FAILED);
            e2.printStackTrace();
        }
    }

    private static String c(Context context) {
        String str = "";
        try {
            String packageName = context.getPackageName();
            ArrayList arrayList = new ArrayList();
            arrayList.add("logcat");
            arrayList.add("-d");
            arrayList.add("-v");
            arrayList.add("raw");
            arrayList.add("-s");
            arrayList.add("AndroidRuntime:E");
            arrayList.add("-p");
            arrayList.add(packageName);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec((String[]) arrayList.toArray(new String[arrayList.size()])).getInputStream()), 1024);
            boolean z = false;
            String str2 = "";
            boolean z2 = false;
            for (String readLine = bufferedReader.readLine(); readLine != null; readLine = bufferedReader.readLine()) {
                if (readLine.indexOf("thread attach failed") < 0) {
                    str2 = String.valueOf(str2) + readLine + 10;
                }
                if (!z2 && readLine.toLowerCase().indexOf("exception") >= 0) {
                    z2 = true;
                }
                z = (z || readLine.indexOf(packageName) < 0) ? z : true;
            }
            if (str2.length() > 0 && z2 && z) {
                str = str2;
            }
            try {
                Runtime.getRuntime().exec("logcat -c");
                return str;
            } catch (Exception e2) {
                Log.e(n.b, "Failed to clear log");
                e2.printStackTrace();
                return str;
            }
        } catch (Exception e3) {
            Exception exc = e3;
            String str3 = str;
            Log.e(n.b, "Failed to catch error log");
            exc.printStackTrace();
            return str3;
        }
    }

    private void c(Context context, SharedPreferences sharedPreferences) {
        String string = sharedPreferences.getString("session_id", null);
        if (string == null) {
            Log.w(n.b, "Missing session_id, ignore message");
            return;
        }
        Long valueOf = Long.valueOf(sharedPreferences.getLong("duration", -1));
        if (valueOf.longValue() <= 0) {
            valueOf = 0L;
        }
        String b2 = b();
        String str = b2.split(" ")[0];
        String str2 = b2.split(" ")[1];
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("type", "terminate");
            jSONObject.put("session_id", string);
            jSONObject.put("date", str);
            jSONObject.put("time", str2);
            jSONObject.put("duration", String.valueOf(valueOf.longValue() / 1000));
            if (l) {
                String[] split = sharedPreferences.getString("activities", "").split(";");
                JSONArray jSONArray = new JSONArray();
                for (String jSONArray2 : split) {
                    jSONArray.put(new JSONArray(jSONArray2));
                }
                jSONObject.put("activities", jSONArray);
            }
            this.i.post(new l(this, context, jSONObject));
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public static void c(Context context, File file) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.parse("file://" + file.getAbsolutePath()), "application/vnd.android.package-archive");
        context.startActivity(intent);
    }

    private void c(Context context, String str) {
        String b2 = b();
        String str2 = b2.split(" ")[0];
        String str3 = b2.split(" ")[1];
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("type", "error");
            jSONObject.put("context", str);
            jSONObject.put("date", str2);
            jSONObject.put("time", str3);
            this.i.post(new l(this, context, jSONObject));
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mobclick.android.MobclickAgent.a(android.content.Context, org.json.JSONObject, java.lang.String, boolean):java.lang.String
     arg types: [android.content.Context, org.json.JSONObject, java.lang.String, int]
     candidates:
      com.mobclick.android.MobclickAgent.a(com.mobclick.android.MobclickAgent, android.content.Context, java.lang.String, java.lang.String):void
      com.mobclick.android.MobclickAgent.a(android.content.Context, org.json.JSONObject, java.lang.String, boolean):java.lang.String */
    /* access modifiers changed from: private */
    public void c(Context context, JSONObject jSONObject) {
        if (a("update", context)) {
            String a2 = a(context, jSONObject, n.d, true);
            Log.i(n.b, "return message from " + a2);
            if (a2 == null) {
                a2 = a(context, jSONObject, n.f, true);
            }
            if (a2 != null) {
                d(context, a2);
            } else if (m != null) {
                m.onUpdateReturned(UpdateStatus.Timeout);
            }
        } else if (m != null) {
            m.onUpdateReturned(UpdateStatus.No);
        }
    }

    private static AlertDialog d(Context context, JSONObject jSONObject) {
        try {
            String string = jSONObject.has("version") ? jSONObject.getString("version") : "";
            String string2 = jSONObject.has("update_log") ? jSONObject.getString("update_log") : "";
            String string3 = jSONObject.has("path") ? jSONObject.getString("path") : "";
            String str = "";
            if (!p(context)[0].equals("Wi-Fi")) {
                str = String.valueOf(n.j(context)) + "\n";
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(n.h(context)).setMessage(String.valueOf(str) + n.i(context) + string + "\n" + string2).setCancelable(false).setPositiveButton(n.k(context), new i(context, string3, string)).setNegativeButton(n.n(context), new j());
            AlertDialog create = builder.create();
            create.setCancelable(true);
            return create;
        } catch (Exception e2) {
            Log.e(n.b, "Fail to create update dialog box.", e2);
            e2.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: private */
    public synchronized void d(Context context) {
        if (this.h != context) {
            Log.e(n.b, "onPause() called without context from corresponding onResume()");
        } else {
            this.h = context;
            SharedPreferences l2 = l(context);
            if (l2 != null) {
                long j2 = l2.getLong("start_millis", -1);
                if (j2 == -1) {
                    Log.e(n.b, "onEndSession called before onStartSession");
                } else {
                    long currentTimeMillis = System.currentTimeMillis();
                    long j3 = currentTimeMillis - j2;
                    long j4 = l2.getLong("duration", 0);
                    SharedPreferences.Editor edit = l2.edit();
                    if (l) {
                        String string = l2.getString("activities", "");
                        String name = context.getClass().getName();
                        if (!"".equals(string)) {
                            string = String.valueOf(string) + ";";
                        }
                        edit.remove("activities");
                        edit.putString("activities", String.valueOf(string) + "[" + name + "," + (j3 / 1000) + "]");
                    }
                    edit.putLong("start_millis", -1);
                    edit.putLong("end_millis", currentTimeMillis);
                    edit.putLong("duration", j3 + j4);
                    edit.commit();
                }
            }
        }
    }

    private void d(Context context, String str) {
        try {
            p = new JSONObject(str);
            if (p.getString("update").equals("Yes")) {
                if (m != null) {
                    m.onUpdateReturned(UpdateStatus.Yes);
                }
                if (updateAutoPopup) {
                    showUpdateDialog(context);
                }
            } else if (m != null) {
                m.onUpdateReturned(UpdateStatus.No);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private synchronized void e(Context context) {
        f(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mobclick.android.MobclickAgent.a(android.content.Context, org.json.JSONObject, java.lang.String, boolean):java.lang.String
     arg types: [android.content.Context, org.json.JSONObject, java.lang.String, int]
     candidates:
      com.mobclick.android.MobclickAgent.a(com.mobclick.android.MobclickAgent, android.content.Context, java.lang.String, java.lang.String):void
      com.mobclick.android.MobclickAgent.a(android.content.Context, org.json.JSONObject, java.lang.String, boolean):java.lang.String */
    /* access modifiers changed from: private */
    public void e(Context context, JSONObject jSONObject) {
        JSONObject i2 = i(context);
        if (i2 == null) {
            Log.e(n.b, "Fail to construct message header");
            return;
        }
        JSONObject g2 = g(context);
        JSONObject jSONObject2 = new JSONObject();
        try {
            String string = jSONObject.getString("type");
            if (string != null) {
                if (string != "flush") {
                    jSONObject.remove("type");
                    if (g2 == null) {
                        g2 = new JSONObject();
                        JSONArray jSONArray = new JSONArray();
                        jSONArray.put(jSONObject);
                        g2.put(string, jSONArray);
                    } else if (g2.isNull(string)) {
                        JSONArray jSONArray2 = new JSONArray();
                        jSONArray2.put(jSONObject);
                        g2.put(string, jSONArray2);
                    } else {
                        g2.getJSONArray(string).put(jSONObject);
                    }
                }
                if (g2 == null) {
                    Log.w(n.b, "No cache message to flush");
                    return;
                }
                jSONObject2.put("header", i2);
                jSONObject2.put("body", g2);
                if (a(string, context)) {
                    String a2 = a(context, jSONObject2, n.c, false);
                    if (a2 == null) {
                        a2 = a(context, jSONObject2, n.e, false);
                    }
                    if (a2 != null) {
                        Log.i(n.b, "send message succeed, clear cache");
                        if (string.equals("feedback")) {
                            n.onFeedbackReturned(FeedbackStatus.SUCCEED);
                        }
                        h(context);
                        if (g == 4) {
                            SharedPreferences.Editor edit = k(context).edit();
                            edit.putString(n.A(), "true");
                            edit.commit();
                            return;
                        }
                        return;
                    } else if (string.equals("feedback")) {
                        n.onFeedbackReturned(FeedbackStatus.FAILED);
                    }
                } else if (string.equals("feedback")) {
                    n.onFeedbackReturned(FeedbackStatus.DISCONNECT);
                }
                f(context, g2);
            }
        } catch (JSONException e2) {
            Log.e(n.b, "Fail to construct json message.");
            e2.printStackTrace();
            h(context);
        }
    }

    public static void enterPage(Context context, String str) {
        onEvent(context, "_PAGE_", str);
    }

    private void f(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("type", "flush");
            this.i.post(new l(this, context, jSONObject));
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    private static void f(Context context, JSONObject jSONObject) {
        try {
            FileOutputStream openFileOutput = context.openFileOutput(n(context), 0);
            openFileOutput.write(jSONObject.toString().getBytes());
            openFileOutput.close();
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
    }

    public static void flush(Context context) {
        if (context == null) {
            try {
                Log.e(n.b, "unexpected null context");
            } catch (Exception e2) {
                Log.e(n.b, "Exception occurred in Mobclick.flush(). ");
                e2.printStackTrace();
                return;
            }
        }
        a.e(context);
    }

    private static File g(Context context, JSONObject jSONObject) {
        String absolutePath;
        try {
            String string = jSONObject.has("path") ? jSONObject.getString("path") : "";
            String string2 = jSONObject.has("version") ? jSONObject.getString("version") : "";
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(string).openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setConnectTimeout(5000);
            httpURLConnection.connect();
            int contentLength = httpURLConnection.getContentLength();
            httpURLConnection.disconnect();
            if (Environment.getExternalStorageState().equals("mounted")) {
                File externalStorageDirectory = Environment.getExternalStorageDirectory();
                absolutePath = String.valueOf(externalStorageDirectory.getParent()) + "/" + externalStorageDirectory.getName() + "/download";
            } else {
                absolutePath = context.getFilesDir().getAbsolutePath();
            }
            File file = new File(absolutePath, a.a(context.getPackageName(), string2, contentLength));
            if (file.exists() && file.length() == ((long) contentLength)) {
                return file;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return null;
    }

    private static JSONObject g(Context context) {
        try {
            FileInputStream openFileInput = context.openFileInput(n(context));
            String str = "";
            byte[] bArr = new byte[16384];
            while (true) {
                int read = openFileInput.read(bArr);
                if (read == -1) {
                    break;
                }
                str = String.valueOf(str) + new String(bArr, 0, read);
            }
            if (str.length() == 0) {
                return null;
            }
            try {
                return new JSONObject(str);
            } catch (JSONException e2) {
                openFileInput.close();
                h(context);
                e2.printStackTrace();
                return null;
            }
        } catch (FileNotFoundException e3) {
            e3.printStackTrace();
            return null;
        } catch (IOException e4) {
            e4.printStackTrace();
            return null;
        }
    }

    public static JSONObject getUpdateInfo() {
        return p;
    }

    private static void h(Context context) {
        context.deleteFile(m(context));
        context.deleteFile(n(context));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException} */
    private static JSONObject i(Context context) {
        SharedPreferences j2 = j(context);
        JSONObject jSONObject = new JSONObject();
        try {
            long j3 = j2.getLong("req_time", 0);
            if (j3 != 0) {
                jSONObject.put("req_time", j3);
            }
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            String b2 = o.b(context);
            String a2 = o.a(b2);
            if (b2 == null || b2.equals("")) {
                Log.e(n.b, "No device id");
                return null;
            }
            jSONObject.put("idmd5", a2);
            jSONObject.put("device_model", Build.MODEL);
            String a3 = a(context);
            if (a3 == null) {
                Log.e(n.b, "No appkey");
                return null;
            }
            jSONObject.put("appkey", a3);
            jSONObject.put("channel", b(context));
            try {
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                String str = packageInfo.versionName;
                int i2 = packageInfo.versionCode;
                jSONObject.put("app_version", str);
                jSONObject.put("version_code", i2);
            } catch (PackageManager.NameNotFoundException e2) {
                e2.printStackTrace();
                jSONObject.put("app_version", "unknown");
                jSONObject.put("version_code", "unknown");
            }
            jSONObject.put("sdk_type", "Android");
            jSONObject.put("sdk_version", d);
            jSONObject.put("os", "Android");
            jSONObject.put("os_version", Build.VERSION.RELEASE);
            Configuration configuration = new Configuration();
            Settings.System.getConfiguration(context.getContentResolver(), configuration);
            if (configuration == null || configuration.locale == null) {
                jSONObject.put("country", "Unknown");
                jSONObject.put("language", "Unknown");
                jSONObject.put("timezone", 8);
            } else {
                jSONObject.put("country", configuration.locale.getCountry());
                jSONObject.put("language", configuration.locale.toString());
                Calendar instance = Calendar.getInstance(configuration.locale);
                if (instance != null) {
                    TimeZone timeZone = instance.getTimeZone();
                    if (timeZone != null) {
                        jSONObject.put("timezone", timeZone.getRawOffset() / 3600000);
                    } else {
                        jSONObject.put("timezone", 8);
                    }
                } else {
                    jSONObject.put("timezone", 8);
                }
            }
            try {
                DisplayMetrics displayMetrics = new DisplayMetrics();
                ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
                jSONObject.put("resolution", String.valueOf(String.valueOf(displayMetrics.heightPixels)) + "*" + String.valueOf(displayMetrics.widthPixels));
            } catch (Exception e3) {
                e3.printStackTrace();
                jSONObject.put("resolution", "Unknown");
            }
            try {
                String[] p2 = p(context);
                jSONObject.put("access", p2[0]);
                if (p2[0].equals("2G/3G")) {
                    jSONObject.put("access_subtype", p2[1]);
                }
            } catch (Exception e4) {
                e4.printStackTrace();
                jSONObject.put("access", "Unknown");
            }
            try {
                jSONObject.put("carrier", telephonyManager.getNetworkOperatorName());
            } catch (Exception e5) {
                e5.printStackTrace();
                jSONObject.put("carrier", "Unknown");
            }
            if (LOCATION_OPEN) {
                Location q = q(context);
                if (q != null) {
                    jSONObject.put("lat", String.valueOf(q.getLatitude()));
                    jSONObject.put("lng", String.valueOf(q.getLongitude()));
                } else {
                    jSONObject.put("lat", 0.0d);
                    jSONObject.put("lng", 0.0d);
                }
            }
            jSONObject.put("cpu", o.a());
            if (!j.equals("")) {
                jSONObject.put("gpu_vender", j);
            }
            if (!k.equals("")) {
                jSONObject.put("gpu_renderer", k);
            }
            SharedPreferences.Editor edit = j2.edit();
            edit.putString("header", jSONObject.toString());
            edit.commit();
            return jSONObject;
        } catch (JSONException e6) {
            e6.printStackTrace();
            return null;
        } catch (SecurityException e7) {
            Log.e(n.b, "Failed to get IMEI. Forget to add permission READ_PHONE_STATE? ", e7);
            e7.printStackTrace();
            return null;
        }
    }

    public static void initFeedback(Context context) {
        UmengFeedback.a(context);
        UmengFeedback.a(a);
    }

    private static SharedPreferences j(Context context) {
        return context.getSharedPreferences("mobclick_agent_header_" + context.getPackageName(), 0);
    }

    private static SharedPreferences k(Context context) {
        return context.getSharedPreferences("mobclick_agent_update_" + context.getPackageName(), 0);
    }

    private static SharedPreferences l(Context context) {
        return context.getSharedPreferences("mobclick_agent_state_" + context.getPackageName(), 0);
    }

    private static String m(Context context) {
        return "mobclick_agent_header_" + context.getPackageName();
    }

    private static String n(Context context) {
        return "mobclick_agent_cached_" + context.getPackageName();
    }

    private static boolean o(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            return false;
        }
        if (connectivityManager.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED) {
            return true;
        }
        return connectivityManager.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED;
    }

    public static void onError(Context context) {
        try {
            String a2 = a(context);
            if (a2 == null || a2.length() == 0) {
                Log.e(n.b, "unexpected empty appkey");
            } else if (context == null) {
                Log.e(n.b, "unexpected null context");
            } else {
                new k(context, a2, 2).start();
            }
        } catch (Exception e2) {
            Log.e(n.b, "Exception occurred in Mobclick.onError()");
            e2.printStackTrace();
        }
    }

    public static void onEvent(Context context, String str) {
        onEvent(context, str, 1);
    }

    public static void onEvent(Context context, String str, int i2) {
        onEvent(context, str, str, i2);
    }

    public static void onEvent(Context context, String str, String str2) {
        onEvent(context, str, str2, 1);
    }

    public static void onEvent(Context context, String str, String str2, int i2) {
        try {
            String a2 = a(context);
            if (a2 == null || a2.length() == 0) {
                Log.e(n.b, "unexpected empty appkey");
            } else if (context == null) {
                Log.e(n.b, "unexpected null context");
            } else if (str == null || str == "") {
                Log.e(n.b, "tag is null or empty");
            } else if (str2 == null || str2 == "") {
                Log.e(n.b, "label is null or empty");
            } else if (i2 <= 0) {
                Log.e(n.b, "Illegal value of acc");
            } else {
                new k(context, a2, str, str2, i2, 3).start();
            }
        } catch (Exception e2) {
            Log.e(n.b, "Exception occurred in Mobclick.onEvent(). ");
            e2.printStackTrace();
        }
    }

    public static void onPause(Context context) {
        if (context == null) {
            try {
                Log.e(n.b, "unexpected null context");
            } catch (Exception e2) {
                Log.e(n.b, "Exception occurred in Mobclick.onRause(). ");
                e2.printStackTrace();
            }
        } else {
            new k(context, 0).start();
        }
    }

    public static void onResume(Context context) {
        onResume(context, a(context), b(context));
    }

    public static void onResume(Context context, String str, String str2) {
        if (context == null) {
            try {
                Log.e(n.b, "unexpected null context");
            } catch (Exception e2) {
                Log.e(n.b, "Exception occurred in Mobclick.onResume(). ");
                e2.printStackTrace();
            }
        } else if (str == null || str.length() == 0) {
            Log.e(n.b, "unexpected empty appkey");
        } else {
            new k(context, str, str2, 1).start();
        }
    }

    public static void openActivityDurationTrack(boolean z) {
        l = z;
    }

    public static void openFeedbackActivity(Context context) {
        UmengFeedback.a(context);
        UmengFeedback.a(a);
        context.startActivity(new Intent(context, UmengFeedback.class));
    }

    private static String[] p(Context context) {
        String[] strArr = {"Unknown", "Unknown"};
        if (context.getPackageManager().checkPermission("android.permission.ACCESS_NETWORK_STATE", context.getPackageName()) != 0) {
            strArr[0] = "Unknown";
        } else {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager == null) {
                strArr[0] = "Unknown";
            } else if (connectivityManager.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {
                strArr[0] = "Wi-Fi";
            } else {
                NetworkInfo networkInfo = connectivityManager.getNetworkInfo(0);
                if (networkInfo.getState() == NetworkInfo.State.CONNECTED) {
                    strArr[0] = "2G/3G";
                    strArr[1] = networkInfo.getSubtypeName();
                }
            }
        }
        return strArr;
    }

    private static Location q(Context context) {
        return new f(context).a();
    }

    public static void setFeedbackListener(UmengFeedbackListener umengFeedbackListener) {
        n = umengFeedbackListener;
    }

    public static void setOpenGLContext(GL10 gl10) {
        if (gl10 != null) {
            String[] a2 = o.a(gl10);
            if (a2.length == 2) {
                j = a2[0];
                k = a2[1];
            }
        }
    }

    public static void setReportPolicy(int i2) {
        if (i2 < 0 || i2 > 5) {
            Log.e(n.b, "Illegal value of report policy");
        } else {
            g = i2;
        }
    }

    public static void setUpdateListener(UmengUpdateListener umengUpdateListener) {
        m = umengUpdateListener;
    }

    public static void setUpdateOnlyWifi(boolean z) {
        o = z;
    }

    public static void showUpdateDialog(Context context) {
        if (p != null) {
            File g2 = g(context, p);
            if (g2 == null || !enableCacheInUpdate) {
                d(context, p).show();
            } else {
                b(context, g2).show();
            }
        }
    }

    public static void update(Context context) {
        try {
            if (a.b()) {
                o.a(context);
            } else if (o && !p(context)[0].equals("Wi-Fi")) {
                m.onUpdateReturned(UpdateStatus.NoneWifi);
            } else if (context == null) {
                m.onUpdateReturned(UpdateStatus.No);
                Log.e(n.b, "unexpected null context");
            } else {
                a.a(context, a(context));
            }
        } catch (Exception e2) {
            Log.e(n.b, "Exception occurred in Mobclick.update(). " + e2.getMessage());
            e2.printStackTrace();
        }
    }

    public void onFeedbackReturned(JSONObject jSONObject) {
        try {
            if (this.h != null) {
                a.a(this.h, jSONObject);
            }
        } catch (Exception e2) {
            Log.e(n.b, "Exception occurred while sending feedback.");
            e2.printStackTrace();
        }
    }
}
