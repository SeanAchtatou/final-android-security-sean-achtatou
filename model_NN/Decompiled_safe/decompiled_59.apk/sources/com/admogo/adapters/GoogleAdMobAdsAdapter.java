package com.admogo.adapters;

import android.app.Activity;
import android.util.Log;
import com.admogo.AdMogoLayout;
import com.admogo.AdMogoTargeting;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import java.text.SimpleDateFormat;

public class GoogleAdMobAdsAdapter extends AdMogoAdapter implements AdListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$admogo$AdMogoTargeting$Gender;
    private Activity activity;
    private AdView adView;

    static /* synthetic */ int[] $SWITCH_TABLE$com$admogo$AdMogoTargeting$Gender() {
        int[] iArr = $SWITCH_TABLE$com$admogo$AdMogoTargeting$Gender;
        if (iArr == null) {
            iArr = new int[AdMogoTargeting.Gender.values().length];
            try {
                iArr[AdMogoTargeting.Gender.FEMALE.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[AdMogoTargeting.Gender.MALE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[AdMogoTargeting.Gender.UNKNOWN.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$admogo$AdMogoTargeting$Gender = iArr;
        }
        return iArr;
    }

    public GoogleAdMobAdsAdapter(AdMogoLayout adMogoLayout, Ration ration) {
        super(adMogoLayout, ration);
    }

    /* access modifiers changed from: protected */
    public String birthdayForAdMogoTargeting() {
        if (AdMogoTargeting.getBirthDate() != null) {
            return new SimpleDateFormat("yyyyMMdd").format(AdMogoTargeting.getBirthDate().getTime());
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public AdRequest.Gender genderForAdMogoTargeting() {
        switch ($SWITCH_TABLE$com$admogo$AdMogoTargeting$Gender()[AdMogoTargeting.getGender().ordinal()]) {
            case 2:
                return AdRequest.Gender.MALE;
            case 3:
                return AdRequest.Gender.FEMALE;
            default:
                return null;
        }
    }

    public void handle() {
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null) {
            this.activity = adMogoLayout.activityReference.get();
            if (this.activity != null) {
                try {
                    this.adView = new AdView(this.activity, AdSize.BANNER, this.ration.key);
                } catch (Exception e) {
                    Log.e(AdMogoUtil.ADMOGO, e.toString());
                    adMogoLayout.rollover();
                }
                this.adView.setAdListener(this);
                this.adView.loadAd(requestForAdMogoLayout(adMogoLayout));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void log(String message) {
        Log.d(AdMogoUtil.ADMOGO, "GoogleAdapter " + message);
    }

    /* access modifiers changed from: protected */
    public AdRequest requestForAdMogoLayout(AdMogoLayout layout) {
        AdRequest result = new AdRequest();
        result.setTesting(AdMogoTargeting.getTestMode());
        result.setGender(genderForAdMogoTargeting());
        result.setBirthday(birthdayForAdMogoTargeting());
        if (layout.extra.locationOn == 1) {
            result.setLocation(layout.adMogoManager.location);
        }
        result.setKeywords(AdMogoTargeting.getKeywordSet());
        return result;
    }

    public void onDismissScreen(Ad paramAd) {
    }

    public void onFailedToReceiveAd(Ad paramAd, AdRequest.ErrorCode arg1) {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "GoogleAdMob failure , code is" + arg1);
        ((AdView) paramAd).setAdListener((AdListener) null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.rollover();
        }
    }

    public void onLeaveApplication(Ad arg0) {
    }

    public void onPresentScreen(Ad arg0) {
    }

    public void onReceiveAd(Ad paramAd) {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "GoogleAdMob success");
        AdView adView2 = (AdView) paramAd;
        adView2.setAdListener((AdListener) null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            if (!(paramAd instanceof AdView)) {
                log("invalid AdView");
                return;
            }
            adMogoLayout.removeView(adView2);
            adMogoLayout.adMogoManager.resetRollover();
            adMogoLayout.handler.post(new AdMogoLayout.ViewAdRunnable(adMogoLayout, adView2, 1));
            adMogoLayout.rotateThreadedDelayed();
        }
    }

    public void finish() {
        Log.d(AdMogoUtil.ADMOGO, "AdMob Finished");
    }
}
