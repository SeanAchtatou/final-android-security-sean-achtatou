package com.admogo.adapters;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.util.Log;
import android.view.ViewGroup;
import com.admogo.AdMogoLayout;
import com.admogo.AdMogoTargeting;
import com.admogo.obj.Extra;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import net.youmi.android.AdListener;
import net.youmi.android.AdManager;
import net.youmi.android.AdView;
import org.json.JSONException;
import org.json.JSONObject;

public class YoumiAdapter extends AdMogoAdapter implements AdListener {
    private String AppID = null;
    private String AppSecret = null;
    private Activity activity;
    private AdView adView;

    public YoumiAdapter(AdMogoLayout adMogoLayout, Ration ration) throws JSONException {
        super(adMogoLayout, ration);
        JSONObject jsonObject = new JSONObject(this.ration.key);
        this.AppID = jsonObject.getString("AppID");
        this.AppSecret = jsonObject.getString("AppSecret");
    }

    public void handle() {
        double versionCode;
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null) {
            this.activity = adMogoLayout.activityReference.get();
            if (this.activity != null) {
                Extra extra = adMogoLayout.extra;
                int refreshTime = extra.cycleTime;
                if (refreshTime < 30) {
                    refreshTime = 30;
                } else if (refreshTime > 200) {
                    refreshTime = AdView.DEFAULT_BACKGROUND_TRANS;
                }
                try {
                    versionCode = (double) this.activity.getPackageManager().getPackageInfo(this.activity.getPackageName(), 0).versionCode;
                } catch (PackageManager.NameNotFoundException e) {
                    versionCode = 1.0d;
                }
                try {
                    AdManager.init(this.AppID, this.AppSecret, refreshTime, AdMogoTargeting.getTestMode(), versionCode);
                    this.adView = new AdView(this.activity, Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue), Color.rgb(extra.fgRed, extra.fgGreen, extra.fgBlue), 255);
                    this.adView.setAdListener(this);
                    adMogoLayout.addView(this.adView, new ViewGroup.LayoutParams(-2, -2));
                } catch (IllegalArgumentException e2) {
                    adMogoLayout.rollover();
                }
            }
        }
    }

    public void finish() {
        Log.d(AdMogoUtil.ADMOGO, "Youmi Finished");
    }

    public void onConnectFailed(AdView adView2) {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "youMi failure");
        adView2.setAdListener(null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.rollover();
        }
    }

    public void onReceiveAd(AdView adView2) {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "youMi success");
        adView2.setAdListener(null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.adMogoManager.resetRollover();
            adMogoLayout.handler.post(new AdMogoLayout.ViewAdRunnable(adMogoLayout, adView2, 24));
            adMogoLayout.rotateThreadedDelayed();
        }
    }
}
