package com.ctrl.qanimalmatch;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int bg = 2130837504;
        public static final int bg2 = 2130837505;
        public static final int cursor1 = 2130837506;
        public static final int cursor2 = 2130837507;
        public static final int icon = 2130837508;
        public static final int imgscore = 2130837509;
        public static final int num01 = 2130837510;
        public static final int star0 = 2130837511;
        public static final int star1 = 2130837512;
        public static final int star2 = 2130837513;
        public static final int star3 = 2130837514;
        public static final int star4 = 2130837515;
        public static final int star5 = 2130837516;
        public static final int star6 = 2130837517;
        public static final int star7 = 2130837518;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class raw {
        public static final int bgmusic = 2130968576;
        public static final int click = 2130968577;
        public static final int go = 2130968578;
    }

    public static final class string {
        public static final int about = 2131034116;
        public static final int app_name = 2131034113;
        public static final int hello = 2131034112;
        public static final int help = 2131034115;
        public static final int ok = 2131034118;
        public static final int start = 2131034114;
        public static final int version = 2131034117;
    }
}
