package com.ctrl.qanimalmatch;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import com.admogo.util.AdMogoUtil;
import com.mobclick.android.ReportPolicy;
import java.lang.reflect.Array;
import java.util.Random;

public class GameView extends View implements Runnable {
    public static int SCREEN_H = 455;
    public static int SCREEN_W = 320;
    static final int ST_PLAYING = 2;
    static int gameState = 2;
    final int BODY_H = 8;
    final int BODY_W = 8;
    int beginDrawX;
    int beginDrawY;
    private Bitmap bg;
    private Bitmap bg2;
    private Bitmap[] block = new Bitmap[8];
    final int blockCount = 8;
    private Bitmap bmFoot;
    private Bitmap bmHead;
    int[][] body = ((int[][]) Array.newInstance(Integer.TYPE, 8, 8));
    Bomb bomb;
    final int caseWidthX = 40;
    int caseWidthY = 40;
    int[][] clear = ((int[][]) Array.newInstance(Integer.TYPE, 8, 8));
    int clearFrame;
    final int clearFrameMax = 4;
    final int clearW = 30;
    Context context;
    int currentX = -1;
    int currentY = -1;
    private Bitmap cursor1;
    private Bitmap cursor2;
    final int cursorW = 36;
    private int delay = 50;
    int gameScore;
    int iDisScore;
    private Bitmap imgNum01;
    private Bitmap imgScore;
    boolean isClear;
    boolean isDown = true;
    boolean isExchange;
    boolean isReExchange;
    boolean isSelected;
    int moveFrame;
    final int moveFrameMax = 4;
    final int moveSpeed = 10;
    MediaPlayer mpBgMusic;
    MediaPlayer mpClick;
    MediaPlayer mpGo;
    Paint paint = new Paint();
    Random random = new Random();
    int scoreSpace;
    int selectedX;
    int selectedY;
    int[][] tempMove = ((int[][]) Array.newInstance(Integer.TYPE, 8, 8));
    float zfactor = 1.0f;

    public GameView(Context con, int w, int h) {
        super(con);
        this.context = con;
        setFocusable(true);
        this.paint.setColor(-7829368);
        this.paint.setTextSize(22.0f);
        initGlobalVar(w, h);
        this.beginDrawX = (SCREEN_W - 320) >> 1;
        this.beginDrawY = (SCREEN_H - (this.caseWidthY * 8)) >> 1;
        loadFP();
        this.bomb = new Bomb(this.block, SCREEN_W, SCREEN_H);
        this.mpBgMusic = MediaPlayer.create(con, (int) R.raw.bgmusic);
        this.mpClick = MediaPlayer.create(con, (int) R.raw.click);
        this.mpGo = MediaPlayer.create(con, (int) R.raw.click);
        if (this.zfactor < 1.0f) {
            this.beginDrawY = GetNewZ(this.beginDrawY);
            this.caseWidthY = GetNewZ(this.caseWidthY);
        }
        playSound_loop(this.mpBgMusic);
    }

    public void initGlobalVar(int w, int h) {
        Log.i("initGlobalVar", "w=" + w + ",h=" + h);
        this.zfactor = ((((float) h) * 1.0f) / ((float) w)) / 1.5f;
        Log.i("initGlobalVar", "zfactor=" + this.zfactor);
        if (this.zfactor > 1.0f) {
            this.zfactor = 1.0f;
        }
        Log.i("initGlobalVar", "zfactor=" + this.zfactor);
    }

    public int GetNewZ(int oldZ) {
        return (int) ((((float) oldZ) * this.zfactor) + 0.5f);
    }

    public void playSound(MediaPlayer mpSrc) {
        try {
            mpSrc.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void playSound_loop(MediaPlayer mpSrc) {
        try {
            mpSrc.setLooping(true);
            mpSrc.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void releaseSounds() {
        if (this.mpBgMusic != null) {
            this.mpBgMusic.release();
        }
        if (this.mpClick != null) {
            this.mpClick.release();
        }
        if (this.mpGo != null) {
            this.mpGo.release();
        }
    }

    public Bitmap createImage(Drawable tile, int w, int h) {
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        tile.setBounds(0, 0, w, h);
        tile.draw(canvas);
        return bitmap;
    }

    public void loadFP() {
        Resources r = getResources();
        this.bg = NewBmp(BitmapFactory.decodeResource(r, R.drawable.bg));
        this.bg2 = NewBmp(BitmapFactory.decodeResource(r, R.drawable.bg2));
        this.cursor1 = NewBmp(BitmapFactory.decodeResource(r, R.drawable.cursor1));
        this.cursor2 = NewBmp(BitmapFactory.decodeResource(r, R.drawable.cursor2));
        this.imgNum01 = NewBmp(BitmapFactory.decodeResource(r, R.drawable.num01));
        this.imgScore = NewBmp(BitmapFactory.decodeResource(r, R.drawable.imgscore));
        this.scoreSpace = this.imgScore.getWidth();
        this.block[0] = NewBmp(BitmapFactory.decodeResource(r, R.drawable.star0));
        this.block[1] = NewBmp(BitmapFactory.decodeResource(r, R.drawable.star1));
        this.block[2] = NewBmp(BitmapFactory.decodeResource(r, R.drawable.star2));
        this.block[3] = NewBmp(BitmapFactory.decodeResource(r, R.drawable.star3));
        this.block[4] = NewBmp(BitmapFactory.decodeResource(r, R.drawable.star4));
        this.block[5] = NewBmp(BitmapFactory.decodeResource(r, R.drawable.star5));
        this.block[6] = NewBmp(BitmapFactory.decodeResource(r, R.drawable.star6));
        this.block[7] = NewBmp(BitmapFactory.decodeResource(r, R.drawable.star7));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public Bitmap NewBmp(Bitmap oldBmp) {
        if (this.zfactor >= 1.0f) {
            return oldBmp;
        }
        Log.i("GameView", "oldBmpw=" + oldBmp.getWidth());
        Log.i("GameView", "oldBmph=" + oldBmp.getHeight());
        Matrix matrix = new Matrix();
        int width = oldBmp.getWidth();
        int height = oldBmp.getHeight();
        matrix.postScale(1.0f, this.zfactor);
        Bitmap NewBmp = Bitmap.createBitmap(oldBmp, 0, 0, width, height, matrix, true);
        Log.i("GameView", "NewBmpw=" + NewBmp.getWidth());
        Log.i("GameView", "NewBmph=" + NewBmp.getHeight());
        return NewBmp;
    }

    public void toState(int state) {
        gameState = state;
    }

    public void logic() {
        this.bomb.move();
        if (this.iDisScore < this.gameScore) {
            this.iDisScore += ((this.gameScore - this.iDisScore) / 2) + 1;
        }
        if (this.isClear) {
            this.clearFrame++;
            if (this.clearFrame >= 4) {
                this.clearFrame = 0;
                this.isClear = false;
                this.isDown = doDown();
            }
        }
        if (this.isDown || this.isExchange || this.isReExchange) {
            this.moveFrame++;
        }
        if (this.isDown && this.moveFrame >= 4) {
            this.moveFrame = 0;
            this.isDown = doDown();
            if (!this.isDown) {
                this.isClear = checkClear();
            }
        }
        if (this.isExchange && this.moveFrame >= 4) {
            this.moveFrame = 0;
            this.isExchange = false;
            this.tempMove = (int[][]) Array.newInstance(Integer.TYPE, 8, 8);
            this.isClear = checkClear();
            if (!this.isClear) {
                this.isReExchange = true;
                doExchange();
            }
        }
        if (this.isReExchange && this.moveFrame >= 4) {
            this.tempMove = (int[][]) Array.newInstance(Integer.TYPE, 8, 8);
            this.moveFrame = 0;
            this.isReExchange = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        switch (gameState) {
            case 2:
                paintPlaying(canvas);
                return;
            default:
                return;
        }
    }

    private void paintPlaying(Canvas canvas) {
        canvas.drawColor(-1);
        canvas.clipRect(0, 0, SCREEN_W, SCREEN_H);
        canvas.drawBitmap(this.bg, 0.0f, 0.0f, this.paint);
        this.paint.setAlpha(120);
        canvas.drawBitmap(this.bg2, (float) this.beginDrawX, (float) this.beginDrawY, this.paint);
        this.paint.setAlpha(255);
        for (int i = 0; i < this.tempMove.length; i++) {
            for (int j = 0; j < this.tempMove[i].length; j++) {
                switch (this.tempMove[i][j]) {
                    case 1:
                        paintBlock(canvas, this.body[i][j], ((i - 1) * 40) + (this.moveFrame * 10), ((j - 1) * this.caseWidthY) + (this.moveFrame * 10));
                        break;
                    case 2:
                        paintBlock(canvas, this.body[i][j], i * 40, ((j - 1) * this.caseWidthY) + (this.moveFrame * 10));
                        break;
                    case 3:
                        paintBlock(canvas, this.body[i][j], ((i + 1) * 40) - (this.moveFrame * 10), ((j - 1) * this.caseWidthY) + (this.moveFrame * 10));
                        break;
                    case 4:
                        paintBlock(canvas, this.body[i][j], ((i - 1) * 40) + (this.moveFrame * 10), this.caseWidthY * j);
                        break;
                    case 5:
                    default:
                        if (this.clear[i][j] != 0) {
                            break;
                        } else {
                            paintBlock(canvas, this.body[i][j], i * 40, this.caseWidthY * j);
                            break;
                        }
                    case 6:
                        paintBlock(canvas, this.body[i][j], ((i + 1) * 40) - (this.moveFrame * 10), this.caseWidthY * j);
                        break;
                    case 7:
                        paintBlock(canvas, this.body[i][j], ((i - 1) * 40) + (this.moveFrame * 10), ((j + 1) * this.caseWidthY) - (this.moveFrame * 10));
                        break;
                    case AdMogoUtil.NETWORK_TYPE_QUATTRO:
                        paintBlock(canvas, this.body[i][j], i * 40, ((j + 1) * this.caseWidthY) - (this.moveFrame * 10));
                        break;
                    case AdMogoUtil.NETWORK_TYPE_CUSTOM:
                        paintBlock(canvas, this.body[i][j], ((i + 1) * 40) - (this.moveFrame * 10), ((j + 1) * this.caseWidthY) - (this.moveFrame * 10));
                        break;
                }
            }
        }
        canvas.drawBitmap(this.imgScore, 0.0f, 0.0f, this.paint);
        paintNumber(canvas, this.iDisScore, this.scoreSpace, 0, 20, 35);
        canvas.drawBitmap(this.isSelected ? this.cursor1 : this.cursor2, (float) (this.beginDrawX + (this.currentX * 40)), (float) (this.beginDrawY + (this.currentY * this.caseWidthY)), this.paint);
        this.bomb.paint(canvas, this.paint);
    }

    private boolean doDown() {
        boolean isFull = false;
        for (int i = 0; i < this.body.length; i++) {
            int j = this.body[i].length - 1;
            while (true) {
                if (j < 0) {
                    break;
                }
                this.tempMove[i][j] = 0;
                if (this.body[i][j] == 0) {
                    isFull = true;
                    for (int k = j; k >= 0; k--) {
                        this.tempMove[i][k] = 2;
                        if (k == 0) {
                            this.body[i][k] = Math.abs(this.random.nextInt() % 8) + 1;
                        } else {
                            this.body[i][k] = this.body[i][k - 1];
                        }
                    }
                } else {
                    j--;
                }
            }
        }
        return isFull;
    }

    private void doExchange() {
        if (this.currentX - this.selectedX == -1) {
            if (this.currentY - this.selectedY == -1) {
                this.tempMove[this.currentX][this.currentY] = 9;
                this.tempMove[this.selectedX][this.selectedY] = 1;
            } else if (this.currentY - this.selectedY == 0) {
                this.tempMove[this.currentX][this.currentY] = 6;
                this.tempMove[this.selectedX][this.selectedY] = 4;
            } else if (this.currentY - this.selectedY == 1) {
                this.tempMove[this.currentX][this.currentY] = 3;
                this.tempMove[this.selectedX][this.selectedY] = 7;
            }
        } else if (this.currentX - this.selectedX == 0) {
            if (this.currentY - this.selectedY == -1) {
                this.tempMove[this.currentX][this.currentY] = 8;
                this.tempMove[this.selectedX][this.selectedY] = 2;
            } else if (this.currentY - this.selectedY == 1) {
                this.tempMove[this.currentX][this.currentY] = 2;
                this.tempMove[this.selectedX][this.selectedY] = 8;
            }
        } else if (this.currentX - this.selectedX == 1) {
            if (this.currentY - this.selectedY == -1) {
                this.tempMove[this.currentX][this.currentY] = 7;
                this.tempMove[this.selectedX][this.selectedY] = 3;
            } else if (this.currentY - this.selectedY == 0) {
                this.tempMove[this.currentX][this.currentY] = 4;
                this.tempMove[this.selectedX][this.selectedY] = 6;
            } else if (this.currentY - this.selectedY == 1) {
                this.tempMove[this.currentX][this.currentY] = 1;
                this.tempMove[this.selectedX][this.selectedY] = 9;
            }
        }
        int temp = this.body[this.selectedX][this.selectedY];
        this.body[this.selectedX][this.selectedY] = this.body[this.currentX][this.currentY];
        this.body[this.currentX][this.currentY] = temp;
    }

    private boolean checkClear() {
        for (int i = 0; i < this.body.length; i++) {
            for (int j = 0; j < this.body[i].length; j++) {
                if (j > 0 && j < this.body[i].length - 1 && this.body[i][j] == this.body[i][j - 1] && this.body[i][j] == this.body[i][j + 1]) {
                    int[] iArr = this.clear[i];
                    this.clear[i][j + 1] = 1;
                    this.clear[i][j - 1] = 1;
                    iArr[j] = 1;
                }
                if (i > 0 && i < this.body.length - 1 && this.body[i][j] == this.body[i - 1][j] && this.body[i][j] == this.body[i + 1][j]) {
                    int[] iArr2 = this.clear[i];
                    int[] iArr3 = this.clear[i - 1];
                    this.clear[i + 1][j] = 1;
                    iArr3[j] = 1;
                    iArr2[j] = 1;
                }
            }
        }
        boolean clearBlock = false;
        for (int i2 = 0; i2 < 8; i2++) {
            for (int j2 = 0; j2 < 8; j2++) {
                if (this.clear[i2][j2] == 1) {
                    clearBlock = true;
                    playSound(this.mpGo);
                    this.bomb.add(this.beginDrawX + (i2 * 40), this.beginDrawY + (this.caseWidthY * j2), this.body[i2][j2] - 1);
                    int[] iArr4 = this.clear[i2];
                    this.body[i2][j2] = 0;
                    iArr4[j2] = 0;
                    this.gameScore += 10;
                }
            }
        }
        playSound(this.mpClick);
        return clearBlock;
    }

    private void paintBlock(Canvas canvas, int index, int x, int y) {
        int index2 = index - 1;
        if (index2 >= 0 && index2 < this.block.length) {
            canvas.save();
            canvas.clipRect(this.beginDrawX, this.beginDrawY, this.beginDrawX + 320, this.beginDrawY + (this.caseWidthY * 8));
            canvas.drawBitmap(this.block[index2], (float) (this.beginDrawX + x), (float) (this.beginDrawY + y), this.paint);
            canvas.restore();
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case ReportPolicy.REALTIME /*0*/:
                if (!this.isDown && !this.isExchange && !this.isReExchange && this.moveFrame == 0) {
                    if (event.getX() > ((float) this.beginDrawX) && event.getX() < ((float) (this.beginDrawX + 320)) && event.getY() > ((float) this.beginDrawY) && event.getY() < ((float) (this.beginDrawY + (this.caseWidthY * 8)))) {
                        if (!this.isSelected) {
                            this.isSelected = true;
                            this.currentX = (int) ((event.getX() - ((float) this.beginDrawX)) / 40.0f);
                            this.currentY = (int) ((event.getY() - ((float) this.beginDrawY)) / ((float) this.caseWidthY));
                            this.selectedX = this.currentX;
                            this.selectedY = this.currentY;
                            break;
                        } else {
                            int tempX = (int) ((event.getX() - ((float) this.beginDrawX)) / 40.0f);
                            int tempY = (int) ((event.getY() - ((float) this.beginDrawY)) / ((float) this.caseWidthY));
                            if (tempX <= this.selectedX) {
                                if (tempX != this.selectedX) {
                                    if (tempX < this.selectedX) {
                                        if (tempY <= this.selectedY) {
                                            if (tempY != this.selectedY) {
                                                if (tempY < this.selectedY) {
                                                    moveLeftUp();
                                                    break;
                                                }
                                            } else {
                                                moveLeft();
                                                break;
                                            }
                                        } else {
                                            moveLeftDown();
                                            break;
                                        }
                                    }
                                } else if (tempY <= this.selectedY) {
                                    if (tempY != this.selectedY) {
                                        if (tempY < this.selectedY) {
                                            moveUp();
                                            break;
                                        }
                                    } else {
                                        this.isSelected = false;
                                        break;
                                    }
                                } else {
                                    moveDown();
                                    break;
                                }
                            } else if (tempY <= this.selectedY) {
                                if (tempY != this.selectedY) {
                                    if (tempY < this.selectedY) {
                                        moveRightUp();
                                        break;
                                    }
                                } else {
                                    moveRight();
                                    break;
                                }
                            } else {
                                moveRightDown();
                                break;
                            }
                        }
                    }
                } else {
                    return super.onTouchEvent(event);
                }
                break;
        }
        return super.onTouchEvent(event);
    }

    public boolean onKeyDown(int keyCode, KeyEvent msg) {
        if (this.isDown || this.isExchange || this.isReExchange || this.moveFrame != 0) {
            return super.onKeyDown(keyCode, msg);
        }
        if (keyCode == 23) {
            if (this.isDown || this.isExchange || this.isReExchange) {
                return super.onKeyDown(keyCode, msg);
            }
            this.isSelected = !this.isSelected;
            this.selectedX = this.currentX;
            this.selectedY = this.currentY;
        } else if (this.isExchange || this.isReExchange) {
            return super.onKeyDown(keyCode, msg);
        } else {
            if (keyCode == 19) {
                moveUp();
            } else if (keyCode == 20) {
                moveDown();
            } else if (keyCode == 21) {
                moveLeft();
            } else if (keyCode == 22) {
                moveRight();
            }
        }
        return super.onKeyDown(keyCode, msg);
    }

    public void setExchange() {
        if (this.isSelected) {
            this.isExchange = true;
            this.isSelected = false;
            doExchange();
        }
    }

    private void moveRightDown() {
        if (this.currentX == 7 || this.currentY == 7) {
            this.isSelected = false;
        }
        int i = this.currentX + 1;
        this.currentX = i;
        this.currentX = i % 8;
        int i2 = this.currentY + 1;
        this.currentY = i2;
        this.currentY = i2 % 8;
        setExchange();
    }

    private void moveLeftDown() {
        if (this.currentX == 0 || this.currentY == 7) {
            this.isSelected = false;
        }
        int i = this.currentX - 1;
        this.currentX = i;
        this.currentX = (i + 8) % 8;
        int i2 = this.currentY + 1;
        this.currentY = i2;
        this.currentY = i2 % 8;
        setExchange();
    }

    private void moveRightUp() {
        if (this.currentY == 0 || this.currentX == 7) {
            this.isSelected = false;
        }
        int i = this.currentX + 1;
        this.currentX = i;
        this.currentX = i % 8;
        int i2 = this.currentY - 1;
        this.currentY = i2;
        this.currentY = (i2 + 8) % 8;
        setExchange();
    }

    private void moveLeftUp() {
        if (this.currentX == 0 || this.currentY == 0) {
            this.isSelected = false;
        }
        int i = this.currentX - 1;
        this.currentX = i;
        this.currentX = (i + 8) % 8;
        int i2 = this.currentY - 1;
        this.currentY = i2;
        this.currentY = (i2 + 8) % 8;
        setExchange();
    }

    private void moveUp() {
        if (this.currentY == 0) {
            this.isSelected = false;
        }
        int i = this.currentY - 1;
        this.currentY = i;
        this.currentY = (i + 8) % 8;
        setExchange();
    }

    private void moveDown() {
        if (this.currentY == 7) {
            this.isSelected = false;
        }
        int i = this.currentY + 1;
        this.currentY = i;
        this.currentY = i % 8;
        setExchange();
    }

    private void moveLeft() {
        if (this.currentX == 0) {
            this.isSelected = false;
        }
        int i = this.currentX - 1;
        this.currentX = i;
        this.currentX = (i + 8) % 8;
        setExchange();
    }

    private void moveRight() {
        if (this.currentX == 7) {
            this.isSelected = false;
        }
        int i = this.currentX + 1;
        this.currentX = i;
        this.currentX = i % 8;
        setExchange();
    }

    private void paintNumber(Canvas canvas, int num, int x, int y, int w, int h) {
        int newH = GetNewZ(h);
        String tmpStr = new StringBuilder().append(Math.abs(num)).toString();
        for (int i = 0; i < tmpStr.length(); i++) {
            canvas.save();
            canvas.clipRect((i * w) + x, y, ((i + 1) * w) + x, y + newH);
            canvas.drawBitmap(this.imgNum01, (float) (((i - Integer.parseInt(new StringBuilder(String.valueOf(tmpStr.charAt(i))).toString())) * w) + x), (float) y, this.paint);
            canvas.restore();
        }
    }

    public void run() {
        logic();
        invalidate();
        postDelayed(this, (long) this.delay);
    }
}
