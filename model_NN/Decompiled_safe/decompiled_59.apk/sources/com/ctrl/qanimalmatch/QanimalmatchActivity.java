package com.ctrl.qanimalmatch;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.widget.FrameLayout;
import com.admogo.AdMogoLayout;
import com.admogo.AdMogoListener;
import com.admogo.AdMogoTargeting;
import com.mobclick.android.MobclickAgent;

public class QanimalmatchActivity extends Activity implements AdMogoListener {
    public static final String MYPREFS = "threesame";
    private GameView gameView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        Display display = getWindowManager().getDefaultDisplay();
        this.gameView = new GameView(this, display.getWidth(), display.getHeight());
        setContentView(this.gameView);
        new AlertDialog.Builder(this).setMessage("三个相同图片连成一横线或竖线时会消去并且得分").setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
        AdMogoTargeting.setTestMode(false);
        AdMogoLayout adMogoLayoutCode = new AdMogoLayout(this, "894f6a0587ee4e42b6824553342cc422");
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(-1, -2);
        params.bottomMargin = 0;
        adMogoLayoutCode.setAdMogoListener(this);
        params.gravity = 80;
        addContentView(adMogoLayoutCode, params);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        savePreferences();
        this.gameView.removeCallbacks(this.gameView);
        MobclickAgent.onPause(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        loadPreferences();
        this.gameView.run();
        MobclickAgent.onResume(this);
    }

    /* access modifiers changed from: protected */
    public void savePreferences() {
        SharedPreferences.Editor editor = getSharedPreferences(MYPREFS, 1).edit();
        editor.putInt("score", this.gameView.gameScore);
        editor.commit();
    }

    public void loadPreferences() {
        SharedPreferences gamePreferences = getSharedPreferences(MYPREFS, 1);
        this.gameView.gameScore = gamePreferences.getInt("score", 0);
    }

    public void onDestroy() {
        Log.i("onDestroy", "onDestroy.");
        super.onDestroy();
        this.gameView.releaseSounds();
    }

    public void onClickAd() {
        Log.v("onClickAd", "Click the buttom ad.");
    }

    public void onFailedReceiveAd() {
        Log.v("onFailedReceiveAd", "Failed to receive the buttom ad.");
    }

    public void onReceiveAd() {
        Log.v("onReceiveAd", "Receive the buttom ad.");
    }
}
