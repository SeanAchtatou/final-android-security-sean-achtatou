package com.adwo.adsdk;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

final class w extends WebViewClient {
    /* access modifiers changed from: private */
    public /* synthetic */ C0009j a;

    w(C0009j jVar) {
        this.a = jVar;
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        String str2;
        if (str != null) {
            String str3 = null;
            Activity activity = (Activity) webView.getContext();
            if (activity == null) {
                return false;
            }
            try {
                Uri parse = Uri.parse(str);
                if (parse.getScheme().equalsIgnoreCase("tel")) {
                    Intent intent = new Intent("android.intent.action.DIAL", parse);
                    if (activity != null) {
                        try {
                            activity.startActivity(intent);
                        } catch (ActivityNotFoundException e) {
                        }
                    }
                    return true;
                } else if (parse.getScheme().equalsIgnoreCase("sms")) {
                    Intent intent2 = new Intent("android.intent.action.VIEW", parse);
                    if (activity != null) {
                        try {
                            activity.startActivity(intent2);
                        } catch (ActivityNotFoundException e2) {
                        }
                    }
                    return true;
                } else if (parse.getScheme().equalsIgnoreCase("market") || str.toLowerCase().startsWith("http://market.android.com") || str.toLowerCase().startsWith("https://market.android.com")) {
                    Intent intent3 = new Intent("android.intent.action.VIEW", parse);
                    if (activity != null) {
                        try {
                            activity.startActivity(intent3);
                        } catch (ActivityNotFoundException e3) {
                        }
                    }
                    return true;
                } else if (parse.getScheme().equalsIgnoreCase("http") && str.toLowerCase().endsWith(".apk")) {
                    new Thread(new x(this, str, activity)).start();
                    return true;
                } else if ((parse.getScheme().equalsIgnoreCase("http") && str.toLowerCase().endsWith(".jpeg")) || str.toLowerCase().endsWith(".gif") || str.toLowerCase().endsWith(".png") || str.toLowerCase().endsWith(".jpg") || str.toLowerCase().endsWith(".bmp")) {
                    new Thread(new y(this, str, activity)).start();
                    return true;
                } else if (str.endsWith(".3gp") || str.endsWith(".mp3") || str.endsWith(".mp4") || str.endsWith(".mpeg") || str.endsWith(".wav")) {
                    Intent intent4 = new Intent("android.intent.action.VIEW");
                    intent4.addFlags(268435456);
                    intent4.setDataAndType(Uri.parse(str), O.a(str));
                    if (activity != null) {
                        try {
                            activity.startActivity(intent4);
                        } catch (ActivityNotFoundException e4) {
                        }
                    }
                    return true;
                } else {
                    URL url = new URL(str);
                    HttpURLConnection.setFollowRedirects(false);
                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("GET");
                    httpURLConnection.connect();
                    String headerField = httpURLConnection.getHeaderField("Location");
                    str3 = httpURLConnection.getHeaderField("Content-Type");
                    httpURLConnection.getResponseCode();
                    Log.d("Adwo AdSDK", "Response Code:" + httpURLConnection.getResponseCode() + " Response Message:" + httpURLConnection.getResponseMessage());
                    Log.i("Adwo AdSDK", "locurlString: " + headerField);
                    str2 = str3;
                    if (str == null) {
                        return false;
                    }
                    Uri parse2 = Uri.parse(str);
                    if (str2 == null) {
                        str2 = "";
                    }
                    if (!(parse2 == null || parse2.getScheme() == null)) {
                        if (parse2.getScheme().equalsIgnoreCase("market")) {
                            Log.i("Adwo AdSDK", "Android Market URL, launch the Market Application");
                            activity.startActivity(new Intent("android.intent.action.VIEW", parse2));
                        } else if (!parse2.getScheme().equalsIgnoreCase("rtsp") && (!parse2.getScheme().equalsIgnoreCase("http") || (!str2.equalsIgnoreCase("video/mp4") && !str2.equalsIgnoreCase("video/3gpp")))) {
                            if (parse2.getScheme().equalsIgnoreCase("tel")) {
                                Log.i("Adwo AdSDK", "Telephone Number, launch the phone");
                                activity.startActivity(new Intent("android.intent.action.DIAL", parse2));
                            } else if (parse2.getScheme().equalsIgnoreCase("sms")) {
                                activity.startActivity(new Intent("android.intent.action.VIEW", parse2));
                            } else if (!parse2.getScheme().equalsIgnoreCase("http") || parse2.getLastPathSegment() == null || (!parse2.getLastPathSegment().endsWith(".mp4") && !parse2.getLastPathSegment().endsWith(".3gp"))) {
                                webView.loadUrl(parse2.toString());
                                a();
                            }
                        }
                    }
                }
            } catch (MalformedURLException e5) {
                e5.printStackTrace();
                return false;
            } catch (IOException e6) {
                e6.printStackTrace();
                str2 = str3;
            }
        }
        return true;
    }

    public final void onPageFinished(WebView webView, String str) {
        if (this.a.a.canGoBack()) {
            if (this.a.f != null) {
                this.a.f.setVisibility(0);
            }
        } else if (this.a.f != null) {
            this.a.f.setVisibility(4);
        }
        if (this.a.a.canGoForward()) {
            if (this.a.e != null) {
                this.a.e.setVisibility(0);
            }
        } else if (this.a.e != null) {
            this.a.e.setVisibility(4);
        }
    }

    private void a() {
        if (this.a.h != null) {
            this.a.h.setVisibility(0);
            this.a.c(true);
            this.a.a(true);
            this.a.b(true);
        }
    }
}
