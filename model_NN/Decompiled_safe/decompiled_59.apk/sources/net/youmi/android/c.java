package net.youmi.android;

import android.view.animation.Animation;

class c implements Runnable {
    final /* synthetic */ bb a;

    c(bb bbVar) {
        this.a = bbVar;
    }

    public void run() {
        ac acVar = null;
        try {
            this.a.setVisibility(0);
            switch (this.a.d.b()) {
                case 1:
                    if (this.a.g.a(this.a.d)) {
                        acVar = this.a.g;
                        break;
                    }
                    break;
                case 2:
                    if (this.a.f.a(this.a.d)) {
                        acVar = this.a.f;
                        break;
                    }
                    break;
                case 3:
                    if (this.a.e.a(this.a.d)) {
                        acVar = this.a.e;
                        break;
                    }
                    break;
                case 4:
                    if (this.a.h.a(this.a.d)) {
                        acVar = this.a.h;
                        break;
                    }
                    break;
            }
            if (acVar != null) {
                this.a.l = this.a.d.b();
                if (this.a.d.b() == 4 || this.a.d.b() == 2) {
                    this.a.setBackgroundColor(0);
                } else if (this.a.i != null) {
                    this.a.setBackgroundDrawable(this.a.i);
                }
                this.a.postInvalidate();
                if (acVar != this.a.k) {
                    acVar.c();
                    if (this.a.k != null) {
                        if (this.a.d.b() == 4) {
                            this.a.k.a();
                        } else {
                            this.a.k.a(r.b(this.a.n));
                            this.a.k.a();
                        }
                    }
                    Animation a2 = r.a(this.a.n);
                    acVar.d();
                    acVar.a(a2);
                    this.a.k = acVar;
                    return;
                }
                this.a.k.e();
            }
        } catch (Exception e) {
        }
    }
}
