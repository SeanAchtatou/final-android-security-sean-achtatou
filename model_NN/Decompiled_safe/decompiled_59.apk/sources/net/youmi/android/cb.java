package net.youmi.android;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;

class cb extends WebViewClient {
    final /* synthetic */ cq a;

    cb(cq cqVar) {
        this.a = cqVar;
    }

    public void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        if (this.a.c != null) {
            this.a.c.a(webView.canGoBack());
            this.a.c.b(webView.canGoForward());
        }
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        try {
            if (bl.a(this.a.f, webView, str)) {
                return true;
            }
        } catch (Exception e) {
        }
        try {
            if (ad.f(this.a.f, str)) {
                return true;
            }
            if (ad.c(this.a.f, str)) {
                return true;
            }
            if (ad.d(this.a.f, str)) {
                return true;
            }
            try {
                if (str.toLowerCase().indexOf("market://") > -1) {
                    this.a.f.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
                    return true;
                }
                webView.loadUrl(str);
                return super.shouldOverrideUrlLoading(webView, str);
            } catch (Exception e2) {
            }
        } catch (Exception e3) {
        }
    }
}
