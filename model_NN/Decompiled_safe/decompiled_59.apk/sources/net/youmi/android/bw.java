package net.youmi.android;

import android.widget.RelativeLayout;

class bw extends RelativeLayout implements cp {
    cq a;
    AdActivity b;

    public bw(AdActivity adActivity, ak akVar) {
        super(adActivity);
        this.a = new cq(adActivity, akVar);
        this.b = adActivity;
        addView(this.a, ap.a());
    }

    public void a() {
        try {
            if (this.a.e()) {
                this.a.c();
            } else {
                this.b.finish();
            }
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.a.a(str);
    }

    /* access modifiers changed from: package-private */
    public void a(String str, String str2) {
        if (str2 != null) {
            this.a.a(str, str2, "text/html", "utf-8");
        }
    }
}
