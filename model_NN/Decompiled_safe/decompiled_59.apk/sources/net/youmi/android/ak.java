package net.youmi.android;

import android.util.DisplayMetrics;
import java.lang.reflect.Field;

class ak {
    private int a;
    private int b;
    private int c;
    private int d;
    private int e = 42;
    private int f = 4;
    private float g = 1.0f;
    private int h = 0;
    private int i = 160;
    private int j;
    private int k;
    private String l;

    ak(DisplayMetrics displayMetrics, int i2) {
        this.j = displayMetrics.widthPixels;
        this.k = displayMetrics.heightPixels;
        this.g = displayMetrics.density;
        this.i = 160;
        this.h = i2;
        try {
            Field field = displayMetrics.getClass().getField("densityDpi");
            if (field != null) {
                this.i = field.getInt(displayMetrics);
            }
        } catch (Exception e2) {
        }
        if (i2 < 4) {
            this.a = Math.round(((float) this.j) * this.g);
            this.b = Math.round(((float) this.k) * this.g);
            this.d = 48;
            this.c = 50;
            this.e = 42;
            this.f = 4;
            this.l = cr.h();
        } else {
            this.a = this.j;
            this.b = this.k;
            switch (this.i) {
                case 120:
                    this.d = 38;
                    this.l = cr.f();
                    break;
                case 160:
                    this.d = 48;
                    this.l = cr.h();
                    break;
                case 240:
                    this.d = 64;
                    this.l = cr.g();
                    break;
                case 320:
                    this.d = 64;
                    this.l = cr.i();
                    break;
                default:
                    this.d = 48;
                    this.l = cr.h();
                    break;
            }
            this.c = Math.round(50.0f * this.g);
            this.e = Math.round(42.0f * this.g);
            this.f = Math.round(4.0f * this.g);
        }
        if (this.a > this.b) {
            int i3 = this.a;
            this.a = this.b;
            this.b = i3;
        }
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public int a(int i2) {
        int i3 = i2 <= 0 ? 1 : i2;
        if (this.h < 4) {
            return i3;
        }
        int round = Math.round(((float) i3) * this.g);
        if (round <= 0) {
            return 1;
        }
        return round;
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public int c() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public int d() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public int e() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public int f() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public String g() {
        return this.l;
    }
}
