package net.youmi.android;

import android.app.Activity;
import android.view.animation.Animation;
import android.widget.RelativeLayout;

class k extends RelativeLayout implements ac {
    au a;
    ct b;
    final /* synthetic */ bb c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public k(bb bbVar, Activity activity, int i) {
        super(activity);
        this.c = bbVar;
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, i);
        layoutParams.addRule(13);
        this.b = new ct(activity);
        addView(this.b, layoutParams);
    }

    public void a() {
        this.b.a();
        setVisibility(8);
    }

    public void a(Animation animation) {
    }

    public boolean a(ax axVar) {
        if (axVar == null) {
            return false;
        }
        try {
            this.a = null;
            if (axVar.l() == null) {
                return false;
            }
            this.a = axVar.l();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.a = null;
    }

    public void c() {
        try {
            this.b.a(this.a);
        } catch (Exception e) {
        }
    }

    public void d() {
        setVisibility(0);
    }

    public void e() {
    }

    public void f() {
        removeAllViews();
        this.a = null;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        try {
            b();
        } catch (Exception e) {
        }
    }
}
