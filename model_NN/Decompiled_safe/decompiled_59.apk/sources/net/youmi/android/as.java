package net.youmi.android;

import com.mobclick.android.ReportPolicy;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.security.MessageDigest;

class as {
    private static final char a = ((char) Integer.parseInt("00000011", 2));
    private static final char b = ((char) Integer.parseInt("00001111", 2));
    private static final char c = ((char) Integer.parseInt("00111111", 2));
    private static final char d = ((char) Integer.parseInt("11111100", 2));
    private static final char e = ((char) Integer.parseInt("11110000", 2));
    private static final char f = ((char) Integer.parseInt("11000000", 2));
    private static final char[] g = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

    as() {
    }

    static String a(String str) {
        if (str == null || str.length() <= 0) {
            return null;
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes(), 0, str.length());
            return String.format("%032x", new BigInteger(1, instance.digest()));
        } catch (Exception e2) {
            return null;
        }
    }

    static String a(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer(((int) (((double) bArr.length) * 1.34d)) + 3);
        char c2 = 0;
        int i = 0;
        for (int i2 = 0; i2 < bArr.length; i2++) {
            i %= 8;
            while (i < 8) {
                switch (i) {
                    case ReportPolicy.REALTIME:
                        c2 = (char) (((char) (bArr[i2] & d)) >>> 2);
                        break;
                    case 2:
                        c2 = (char) (bArr[i2] & c);
                        break;
                    case 4:
                        c2 = (char) (((char) (bArr[i2] & b)) << 2);
                        if (i2 + 1 >= bArr.length) {
                            break;
                        } else {
                            c2 = (char) (c2 | ((bArr[i2 + 1] & f) >>> 6));
                            break;
                        }
                    case 6:
                        c2 = (char) (((char) (bArr[i2] & a)) << 4);
                        if (i2 + 1 >= bArr.length) {
                            break;
                        } else {
                            c2 = (char) (c2 | ((bArr[i2 + 1] & e) >>> 4));
                            break;
                        }
                }
                stringBuffer.append(g[c2]);
                i += 6;
            }
        }
        if (stringBuffer.length() % 4 != 0) {
            for (int length = 4 - (stringBuffer.length() % 4); length > 0; length--) {
                stringBuffer.append("=");
            }
        }
        return stringBuffer.toString();
    }

    static byte[] a(String str, String str2) {
        byte[] bArr = new byte[64];
        byte[] bArr2 = new byte[64];
        byte[] bArr3 = new byte[64];
        int length = str2.length();
        bz bzVar = new bz();
        if (str2.length() > 64) {
            byte[] a2 = bzVar.a(str2.getBytes());
            int length2 = a2.length;
            for (int i = 0; i < length2; i++) {
                bArr3[i] = a2[i];
            }
            length = length2;
        } else {
            byte[] bytes = str2.getBytes();
            for (int i2 = 0; i2 < bytes.length; i2++) {
                bArr3[i2] = bytes[i2];
            }
        }
        while (length < 64) {
            bArr3[length] = 0;
            length++;
        }
        for (int i3 = 0; i3 < 64; i3++) {
            bArr[i3] = (byte) (bArr3[i3] ^ 54);
            bArr2[i3] = (byte) (bArr3[i3] ^ 92);
        }
        return bzVar.a(a(bArr2, bzVar.a(a(bArr, str.getBytes()))));
    }

    private static byte[] a(byte[] bArr, byte[] bArr2) {
        byte[] bArr3 = new byte[(bArr.length + bArr2.length)];
        for (int i = 0; i < bArr.length; i++) {
            bArr3[i] = bArr[i];
        }
        for (int i2 = 0; i2 < bArr2.length; i2++) {
            bArr3[bArr.length + i2] = bArr2[i2];
        }
        return bArr3;
    }

    static String b(String str) {
        try {
            String encode = URLEncoder.encode(str, "UTF-8");
            return encode.indexOf("+") > -1 ? encode.replace("+", "%20") : encode;
        } catch (Exception e2) {
            return "";
        }
    }

    /* access modifiers changed from: private */
    public static String c(String str, String str2) {
        try {
            return a(a(str, str2));
        } catch (Exception e2) {
            return "";
        }
    }
}
