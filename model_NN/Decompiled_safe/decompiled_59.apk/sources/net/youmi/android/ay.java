package net.youmi.android;

import android.os.Environment;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

class ay {
    private String a = "";
    private String b = "";
    private long c = -1;
    private long d = -1;

    ay(String str, long j, long j2) {
        this.a = str;
        this.b = Environment.getExternalStorageDirectory() + "/" + str + "/";
        this.c = j;
        this.d = j2;
        try {
            File file = new File(this.b);
            if (!file.exists()) {
                file.mkdirs();
            }
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: package-private */
    public String a(String str) {
        String b2 = b(str);
        if (b2 != null) {
            return String.valueOf(this.b) + b2;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public boolean a(String str, byte[] bArr) {
        try {
            String a2 = a(str);
            if (str == null) {
                return false;
            }
            File file = new File(a2);
            if (file.exists()) {
                return true;
            }
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                fileOutputStream.write(bArr);
                try {
                    fileOutputStream.close();
                } catch (Exception e) {
                }
                return true;
            } catch (Exception e2) {
                return false;
            }
        } catch (Exception e3) {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public String b(String str) {
        if (str == null) {
            return null;
        }
        String trim = str.trim();
        if (trim.length() == 0) {
            return null;
        }
        return as.a(trim);
    }

    /* access modifiers changed from: package-private */
    public InputStream c(String str) {
        File file = new File(a(str));
        if (file.exists()) {
            try {
                return new FileInputStream(file);
            } catch (Exception e) {
            }
        }
        return null;
    }
}
