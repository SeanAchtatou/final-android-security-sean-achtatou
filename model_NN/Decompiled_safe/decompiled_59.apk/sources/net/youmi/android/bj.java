package net.youmi.android;

import android.view.MotionEvent;
import android.view.View;
import com.mobclick.android.ReportPolicy;

class bj implements View.OnTouchListener {
    final /* synthetic */ ci a;

    bj(ci ciVar) {
        this.a = ciVar;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case ReportPolicy.REALTIME:
            case 2:
                if (!this.a.t) {
                    return false;
                }
                try {
                    this.a.c.setImageBitmap(this.a.d());
                    return false;
                } catch (Exception e) {
                    return false;
                }
            case 1:
            default:
                try {
                    if (this.a.t) {
                        this.a.c.setImageBitmap(this.a.b());
                        return false;
                    }
                    this.a.c.setImageBitmap(this.a.c());
                    return false;
                } catch (Exception e2) {
                    return false;
                }
        }
    }
}
