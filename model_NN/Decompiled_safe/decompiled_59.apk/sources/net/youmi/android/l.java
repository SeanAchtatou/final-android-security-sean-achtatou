package net.youmi.android;

import android.app.Activity;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.util.DisplayMetrics;
import java.lang.reflect.Field;

class l {
    l() {
    }

    static ak a(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int i = -1;
        try {
            if (Integer.parseInt(Build.VERSION.SDK) >= 4) {
                ApplicationInfo applicationInfo = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0).applicationInfo;
                Field field = ApplicationInfo.class.getField("targetSdkVersion");
                if (!(applicationInfo == null || field == null)) {
                    i = field.getInt(applicationInfo);
                }
            }
        } catch (Exception e) {
        }
        return new ak(displayMetrics, i);
    }
}
