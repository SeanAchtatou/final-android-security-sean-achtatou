package net.youmi.android;

import android.graphics.Bitmap;

class ax {
    private int a = 1;
    private int b = 3;
    private String c;
    private int d;
    private boolean e = false;
    private boolean f = false;
    private String g;
    private String h;
    private Bitmap i;
    private au j;
    private z k;
    private Bitmap l;
    private String m;

    ax() {
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        if (i2 == 1 || i2 == 2 || i2 == 3) {
            this.a = i2;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Bitmap bitmap) {
        this.l = bitmap;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.m = str;
    }

    /* access modifiers changed from: package-private */
    public void a(z zVar) {
        this.k = zVar;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:44:0x0080=Splitter:B:44:0x0080, B:56:0x00a6=Splitter:B:56:0x00a6, B:69:0x00d6=Splitter:B:69:0x00d6} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(android.app.Activity r8, int r9, java.lang.String r10, java.lang.String r11, java.lang.String r12, java.lang.String r13, int r14, int r15) {
        /*
            r7 = this;
            r6 = 3
            r5 = 2
            r4 = 1
            r3 = 0
            if (r9 > 0) goto L_0x0008
            r0 = r3
        L_0x0007:
            return r0
        L_0x0008:
            if (r10 != 0) goto L_0x000c
            r0 = r3
            goto L_0x0007
        L_0x000c:
            java.lang.String r0 = r10.trim()     // Catch:{ Exception -> 0x00e0 }
            java.lang.String r1 = ""
            boolean r1 = r0.equals(r1)     // Catch:{ Exception -> 0x00e0 }
            if (r1 == 0) goto L_0x001a
            r0 = r3
            goto L_0x0007
        L_0x001a:
            r7.d = r9     // Catch:{ Exception -> 0x00e0 }
            r7.c = r0     // Catch:{ Exception -> 0x00e0 }
            if (r13 != 0) goto L_0x0036
            java.lang.String r0 = ""
        L_0x0022:
            if (r11 != 0) goto L_0x003b
            java.lang.String r1 = ""
        L_0x0026:
            r7.g = r1     // Catch:{ Exception -> 0x00e0 }
            if (r12 != 0) goto L_0x0040
            java.lang.String r2 = ""
        L_0x002c:
            r7.h = r2     // Catch:{ Exception -> 0x00e0 }
            if (r15 == r4) goto L_0x0045
            if (r15 == r6) goto L_0x0045
            if (r15 == r5) goto L_0x0045
            r0 = r3
            goto L_0x0007
        L_0x0036:
            java.lang.String r0 = r13.trim()     // Catch:{ Exception -> 0x00e0 }
            goto L_0x0022
        L_0x003b:
            java.lang.String r1 = r11.trim()     // Catch:{ Exception -> 0x00e0 }
            goto L_0x0026
        L_0x0040:
            java.lang.String r2 = r12.trim()     // Catch:{ Exception -> 0x00e0 }
            goto L_0x002c
        L_0x0045:
            r7.a = r15     // Catch:{ Exception -> 0x00e0 }
            r2 = 4
            if (r14 == r2) goto L_0x0052
            if (r14 == r6) goto L_0x0052
            if (r14 == r5) goto L_0x0052
            if (r14 == r4) goto L_0x0052
            r0 = r3
            goto L_0x0007
        L_0x0052:
            r7.b = r14     // Catch:{ Exception -> 0x00e0 }
            switch(r14) {
                case 1: goto L_0x0059;
                case 2: goto L_0x0063;
                case 3: goto L_0x0088;
                case 4: goto L_0x00b3;
                default: goto L_0x0057;
            }     // Catch:{ Exception -> 0x00e0 }
        L_0x0057:
            r0 = r3
            goto L_0x0007
        L_0x0059:
            java.lang.String r0 = ""
            boolean r0 = r1.equals(r0)     // Catch:{ Exception -> 0x00e0 }
            if (r0 == 0) goto L_0x00ad
            r0 = r3
            goto L_0x0007
        L_0x0063:
            java.lang.String r1 = ""
            boolean r1 = r0.equals(r1)     // Catch:{ Exception -> 0x00e0 }
            if (r1 == 0) goto L_0x006d
            r0 = r3
            goto L_0x0007
        L_0x006d:
            net.youmi.android.cx r1 = new net.youmi.android.cx     // Catch:{ Exception -> 0x00e8 }
            r1.<init>(r8)     // Catch:{ Exception -> 0x00e8 }
            net.youmi.android.ay r2 = net.youmi.android.af.b     // Catch:{ Exception -> 0x00e8 }
            boolean r0 = r1.a(r0, r2)     // Catch:{ Exception -> 0x00e8 }
            if (r0 == 0) goto L_0x0086
            android.graphics.Bitmap r0 = r1.b()     // Catch:{ Exception -> 0x00e8 }
            r7.i = r0     // Catch:{ Exception -> 0x00e8 }
        L_0x0080:
            android.graphics.Bitmap r0 = r7.i     // Catch:{ Exception -> 0x00e0 }
            if (r0 != 0) goto L_0x00ad
            r0 = r3
            goto L_0x0007
        L_0x0086:
            r0 = r3
            goto L_0x0007
        L_0x0088:
            java.lang.String r2 = ""
            boolean r1 = r1.equals(r2)     // Catch:{ Exception -> 0x00e0 }
            if (r1 == 0) goto L_0x0093
            r0 = r3
            goto L_0x0007
        L_0x0093:
            net.youmi.android.cx r1 = new net.youmi.android.cx     // Catch:{ Exception -> 0x00e6 }
            r1.<init>(r8)     // Catch:{ Exception -> 0x00e6 }
            net.youmi.android.ay r2 = net.youmi.android.af.a     // Catch:{ Exception -> 0x00e6 }
            boolean r0 = r1.a(r0, r2)     // Catch:{ Exception -> 0x00e6 }
            if (r0 == 0) goto L_0x00b0
            android.graphics.Bitmap r0 = r1.b()     // Catch:{ Exception -> 0x00e6 }
            r7.i = r0     // Catch:{ Exception -> 0x00e6 }
        L_0x00a6:
            android.graphics.Bitmap r0 = r7.i     // Catch:{ Exception -> 0x00e0 }
            if (r0 != 0) goto L_0x00ad
            r0 = 1
            r7.b = r0     // Catch:{ Exception -> 0x00e0 }
        L_0x00ad:
            r0 = r4
            goto L_0x0007
        L_0x00b0:
            r0 = r3
            goto L_0x0007
        L_0x00b3:
            java.lang.String r1 = ""
            boolean r1 = r0.equals(r1)     // Catch:{ Exception -> 0x00e0 }
            if (r1 == 0) goto L_0x00be
            r0 = r3
            goto L_0x0007
        L_0x00be:
            net.youmi.android.cx r1 = new net.youmi.android.cx     // Catch:{ Exception -> 0x00e4 }
            r1.<init>(r8)     // Catch:{ Exception -> 0x00e4 }
            net.youmi.android.ay r2 = net.youmi.android.af.d     // Catch:{ Exception -> 0x00e4 }
            boolean r0 = r1.a(r0, r2)     // Catch:{ Exception -> 0x00e4 }
            if (r0 == 0) goto L_0x00dd
            net.youmi.android.au r0 = new net.youmi.android.au     // Catch:{ Exception -> 0x00e4 }
            byte[] r1 = r1.c()     // Catch:{ Exception -> 0x00e4 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x00e4 }
            r7.j = r0     // Catch:{ Exception -> 0x00e4 }
        L_0x00d6:
            net.youmi.android.au r0 = r7.j     // Catch:{ Exception -> 0x00e0 }
            if (r0 != 0) goto L_0x00ad
            r0 = r3
            goto L_0x0007
        L_0x00dd:
            r0 = r3
            goto L_0x0007
        L_0x00e0:
            r0 = move-exception
            r0 = r3
            goto L_0x0007
        L_0x00e4:
            r0 = move-exception
            goto L_0x00d6
        L_0x00e6:
            r0 = move-exception
            goto L_0x00a6
        L_0x00e8:
            r0 = move-exception
            goto L_0x0080
        */
        throw new UnsupportedOperationException("Method not decompiled: net.youmi.android.ax.a(android.app.Activity, int, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, int):boolean");
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public Bitmap d() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public String e() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public int f() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public String g() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public z h() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public boolean i() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public void j() {
        this.e = true;
    }

    /* access modifiers changed from: package-private */
    public void k() {
        this.f = true;
    }

    /* access modifiers changed from: package-private */
    public au l() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public Bitmap m() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public String n() {
        return this.m;
    }
}
