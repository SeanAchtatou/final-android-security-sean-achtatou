package net.youmi.android;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.widget.FrameLayout;
import com.mobclick.android.ReportPolicy;

class bb extends FrameLayout {
    static final int a = Color.argb(150, 135, 206, 250);
    static final int b = Color.argb(150, 135, 206, 250);
    bp c = new f(this);
    /* access modifiers changed from: private */
    public ax d;
    /* access modifiers changed from: private */
    public q e;
    /* access modifiers changed from: private */
    public ce f;
    /* access modifiers changed from: private */
    public y g;
    /* access modifiers changed from: private */
    public k h;
    /* access modifiers changed from: private */
    public Drawable i;
    private boolean j = false;
    /* access modifiers changed from: private */
    public ac k;
    /* access modifiers changed from: private */
    public int l = 3;
    /* access modifiers changed from: private */
    public AdView m;
    /* access modifiers changed from: private */
    public ak n;
    /* access modifiers changed from: private */
    public Activity o;
    private br p;

    public bb(Activity activity, ak akVar, AdView adView, int i2, int i3, int i4) {
        super(activity);
        this.n = akVar;
        this.o = activity;
        this.m = adView;
        a(i2, i3, i4);
        f();
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        try {
            b();
            c();
            this.p.a(i2);
        } catch (Exception e2) {
        }
    }

    private void a(int i2, int i3, int i4) {
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, this.n.d());
        this.e = new q(this, this.o, this.n, i3);
        addView(this.e);
        this.e.setVisibility(8);
        this.f = new ce(this, this.o, this.n, false);
        addView(this.f, layoutParams);
        this.f.setVisibility(8);
        this.g = new y(this, this.o, this.n, i3);
        addView(this.g, layoutParams);
        this.g.setVisibility(8);
        this.h = new k(this, this.o, this.n.d());
        addView(this.h, layoutParams);
        Bitmap a2 = ah.a(1, this.n.d(), i2, i4);
        if (a2 == null) {
            setBackgroundColor(0);
            return;
        }
        this.i = new BitmapDrawable(a2);
        setBackgroundDrawable(this.i);
    }

    private void b() {
        if (this.p == null) {
            this.p = new br(this.o, this.n, this.m.getAdWidth());
            addView(this.p);
            this.p.bringToFront();
            this.p.setVisibility(8);
        }
    }

    private void c() {
        b();
        this.p.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void d() {
        b();
        this.p.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void e() {
        try {
            Intent intent = new Intent(this.o, AdActivity.class);
            AdActivity.a(intent);
            aj.a(this.d);
            this.o.startActivity(intent);
        } catch (Exception e2) {
        }
        this.m.d = false;
    }

    private void f() {
        setOnClickListener(new d(this));
    }

    /* access modifiers changed from: package-private */
    public void a() {
        try {
            this.o = null;
            this.p = null;
            this.i = null;
            this.d = null;
            this.g.b();
            this.e.b();
            this.h.f();
            this.f.f();
            removeAllViews();
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: package-private */
    public void a(ax axVar) {
        try {
            this.d = axVar;
            getHandler().post(new c(this));
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        try {
            getLayoutParams().width = this.m.getLayoutParams().width;
            getLayoutParams().height = this.n.d();
        } catch (Exception e2) {
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case ReportPolicy.REALTIME:
                this.j = true;
                break;
            case 1:
                this.j = false;
                break;
            case 2:
                this.j = true;
                break;
            default:
                this.j = false;
                break;
        }
        if (this.j) {
            if (this.l == 4 || this.l == 2) {
                setBackgroundColor(0);
            } else {
                setBackgroundColor(a);
            }
        } else if (this.l == 4 || this.l == 2) {
            setBackgroundColor(0);
        } else if (this.i != null) {
            setBackgroundDrawable(this.i);
        } else {
            setBackgroundColor(0);
        }
        return super.onTouchEvent(motionEvent);
    }
}
