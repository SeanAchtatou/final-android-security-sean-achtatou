package net.youmi.android;

import android.webkit.WebChromeClient;
import android.webkit.WebView;

class ca extends WebChromeClient {
    final /* synthetic */ cq a;

    ca(cq cqVar) {
        this.a = cqVar;
    }

    public void onProgressChanged(WebView webView, int i) {
        super.onProgressChanged(webView, i);
        try {
            if (this.a.f != null) {
                this.a.f.setProgressBarIndeterminateVisibility(true);
                this.a.f.setProgress(i * 100);
                if (i == 100) {
                    this.a.f.setProgressBarIndeterminateVisibility(false);
                }
            }
        } catch (Exception e) {
        }
    }

    public void onReceivedTitle(WebView webView, String str) {
        super.onReceivedTitle(webView, str);
        try {
            if (this.a.f != null) {
                this.a.f.setTitle(str);
            }
        } catch (Exception e) {
        }
    }
}
