package com.skyd.core.draw;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.skyd.core.vector.Vector2D;
import com.skyd.core.vector.Vector2DF;

public final class DrawHelper {
    public static Vector2D calculateScaleSize(double width, double height, double targetWidth, double targetHeight, boolean isRestrictMode) {
        if (isRestrictMode) {
            if (targetWidth / width < targetHeight / height) {
                return new Vector2D(targetWidth, (targetWidth / width) * height);
            }
            return new Vector2D((targetHeight / height) * width, targetHeight);
        } else if (targetWidth / width > targetHeight / height) {
            return new Vector2D(targetWidth, (targetWidth / width) * height);
        } else {
            return new Vector2D((targetHeight / height) * width, targetHeight);
        }
    }

    public static Vector2DF calculateScaleSize(float width, float height, float targetWidth, float targetHeight, boolean isRestrictMode) {
        if (isRestrictMode) {
            if (targetWidth / width < targetHeight / height) {
                return new Vector2DF(targetWidth, (targetWidth / width) * height);
            }
            return new Vector2DF((targetHeight / height) * width, targetHeight);
        } else if (targetWidth / width > targetHeight / height) {
            return new Vector2DF(targetWidth, (targetWidth / width) * height);
        } else {
            return new Vector2DF((targetHeight / height) * width, targetHeight);
        }
    }

    public static Bitmap loadBitmap(Context context, int resId, Bitmap.Config cfg) {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inPreferredConfig = cfg;
        opt.inPurgeable = true;
        opt.inInputShareable = true;
        return BitmapFactory.decodeStream(context.getResources().openRawResource(resId), null, opt);
    }

    public static Bitmap loadBitmap(Context context, int resId) {
        return loadBitmap(context, resId, Bitmap.Config.ARGB_8888);
    }
}
