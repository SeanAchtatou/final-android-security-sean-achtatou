package com.skyd.core.android.game;

import com.skyd.core.vector.VectorRect2DF;

public abstract class GamePixelImageSpirit extends GameSpirit implements IGameImageHolder {
    public boolean PixelSimpleHitTest(GamePixelImageSpirit targetObj) {
        VectorRect2DF s = new VectorRect2DF(getPosition().plusNew(getPositionOffset()), getSize().getClone());
        VectorRect2DF t = new VectorRect2DF(targetObj.getPosition().plusNew(targetObj.getPositionOffset()), targetObj.getSize().getClone());
        VectorRect2DF c = s.getIntersectRect(t);
        if (c == null) {
            return false;
        }
        float ssc = ((float) getDisplayImage().getHitTestMaskImage().getBitmap().getWidth()) / s.getWidth();
        int[] spx = getDisplayImage().getHitTestMaskImagePixels(new VectorRect2DF(c.getPosition().minusNew(s.getPosition()).scale(ssc), c.getSize().scaleNew(ssc)));
        float tsc = ((float) targetObj.getDisplayImage().getHitTestMaskImage().getBitmap().getWidth()) / t.getWidth();
        int[] tpx = targetObj.getDisplayImage().getHitTestMaskImagePixels(new VectorRect2DF(c.getPosition().minusNew(t.getPosition()).scale(tsc), c.getSize().scaleNew(tsc)));
        for (int i = 0; i < spx.length; i++) {
            if ((spx[i] >> 24) != 0 && (tpx[i] >> 24) != 0) {
                return true;
            }
        }
        return false;
    }
}
