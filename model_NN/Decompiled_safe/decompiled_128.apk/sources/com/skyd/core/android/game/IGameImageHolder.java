package com.skyd.core.android.game;

public interface IGameImageHolder {
    GameImage getDisplayImage();
}
