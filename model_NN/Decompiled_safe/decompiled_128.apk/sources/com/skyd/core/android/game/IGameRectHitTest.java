package com.skyd.core.android.game;

public interface IGameRectHitTest {
    float getRectHitTestBottom();

    float getRectHitTestLeft();

    float getRectHitTestRight();

    float getRectHitTestTop();
}
