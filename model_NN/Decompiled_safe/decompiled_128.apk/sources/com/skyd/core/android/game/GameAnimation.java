package com.skyd.core.android.game;

import java.util.Iterator;

public class GameAnimation extends GameNodeObject implements IGameImageHolder {
    private int _DisplayFrameIndex = -1;
    private int _FrameCounting = 0;
    private GameObjectList<GameAnimationFrame> _FrameList = new GameObjectList<>(this);
    private boolean _IsRunning = false;
    private int _LoopCounting = 0;
    private int _LoopTime = -1;

    public GameAnimation() {
    }

    public GameAnimation(GameAnimationFrame... frames) {
        for (GameAnimationFrame f : frames) {
            try {
                f.setParent(this);
            } catch (GameException e) {
                e.printStackTrace();
            }
            getFrameList().add(f);
        }
    }

    public GameObjectList<GameAnimationFrame> getFrameList() {
        return this._FrameList;
    }

    /* access modifiers changed from: protected */
    public void setFrameList(GameObjectList<GameAnimationFrame> value) {
        this._FrameList = value;
    }

    /* access modifiers changed from: protected */
    public void setFrameListToDefault() {
        setFrameList(new GameObjectList(this));
    }

    public int getDisplayFrameIndex() {
        return this._DisplayFrameIndex;
    }

    public void setDisplayFrameIndex(int value) {
        this._DisplayFrameIndex = value;
    }

    public void setDisplayFrameIndexToDefault() {
        setDisplayFrameIndex(-1);
    }

    public GameImage getDisplayImage() {
        return getDisplayFrame().getImage();
    }

    public GameAnimationFrame getDisplayFrame() {
        return this._FrameList.get(this._DisplayFrameIndex);
    }

    public int getFrameCounting() {
        return this._FrameCounting;
    }

    /* access modifiers changed from: protected */
    public void setFrameCounting(int value) {
        this._FrameCounting = value;
    }

    /* access modifiers changed from: protected */
    public void setFrameCountingToDefault() {
        setFrameCounting(0);
    }

    public int getLoopCounting() {
        return this._LoopCounting;
    }

    /* access modifiers changed from: protected */
    public void setLoopCounting(int value) {
        this._LoopCounting = value;
    }

    /* access modifiers changed from: protected */
    public void setLoopCountingToDefault() {
        setLoopCounting(0);
    }

    public void updateSelf() {
        super.updateSelf();
        if (this._IsRunning) {
            int i = this._FrameCounting + 1;
            this._FrameCounting = i;
            if (i >= getDisplayFrame().getDuration()) {
                nextFrame();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void updateChilds() {
        if (getIsRunning()) {
            super.updateChilds();
        }
    }

    public GameObject getDisplayContentChild() {
        return getDisplayFrame();
    }

    public int getLoopTime() {
        return this._LoopTime;
    }

    public void setLoopTime(int value) {
        this._LoopTime = value;
    }

    public void setLoopTimeToDefault() {
        setLoopTime(-1);
    }

    public void nextFrame() {
        this._FrameCounting = 0;
        int i = this._DisplayFrameIndex + 1;
        this._DisplayFrameIndex = i;
        if (i >= this._FrameList.size()) {
            if (this._LoopTime >= 0) {
                int i2 = this._LoopCounting;
                this._LoopCounting = i2 + 1;
                if (i2 >= this._LoopTime) {
                    stop();
                    return;
                }
            }
            this._DisplayFrameIndex = 0;
        }
    }

    public void restart() {
        this._FrameCounting = 0;
        this._DisplayFrameIndex = 0;
        this._LoopCounting = 0;
        start();
    }

    public void start() {
        if (this._DisplayFrameIndex == -1) {
            this._DisplayFrameIndex = 0;
        }
        this._IsRunning = true;
    }

    public void stop() {
        this._IsRunning = false;
    }

    public boolean getIsRunning() {
        return this._IsRunning;
    }

    /* access modifiers changed from: protected */
    public void setIsRunning(boolean value) {
        this._IsRunning = value;
    }

    /* access modifiers changed from: protected */
    public void setIsRunningToDefault() {
        setIsRunning(false);
    }

    public void setTotalDuration(int duration) {
        float t = (((float) getTotalDuration()) * 1.0f) / ((float) duration);
        Iterator<GameAnimationFrame> it = this._FrameList.iterator();
        while (it.hasNext()) {
            GameAnimationFrame f = it.next();
            f.setDuration(Math.round(((float) f.getDuration()) * t));
        }
    }

    public int getTotalDuration() {
        int t = 0;
        Iterator<GameAnimationFrame> it = this._FrameList.iterator();
        while (it.hasNext()) {
            t += it.next().getDuration();
        }
        return t;
    }
}
