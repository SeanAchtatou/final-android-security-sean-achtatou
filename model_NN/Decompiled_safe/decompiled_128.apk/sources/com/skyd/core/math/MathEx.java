package com.skyd.core.math;

public final class MathEx {
    public static double distance(double x1, double y1, double x2, double y2) {
        double dx = x2 - x1;
        double dy = y2 - y1;
        return Math.sqrt((dx * dx) + (dy * dy));
    }

    public static double anglesToRadians(double angle) {
        return 0.017453292519943295d * angle;
    }

    public static double radiansToAngles(double radian) {
        return 57.29577951308232d * radian;
    }

    public static double sinAngle(double angle) {
        return Math.sin(anglesToRadians(angle));
    }

    public static double cosAngle(double angle) {
        return Math.cos(anglesToRadians(angle));
    }

    public static double tanAngle(double angle) {
        return Math.tan(anglesToRadians(angle));
    }

    public static double atan2Angle(double y, double x) {
        return radiansToAngles(Math.atan2(y, x));
    }

    public static double angleOfLine(double x1, double y1, double x2, double y2) {
        return atan2Angle(y2 - y1, x2 - x1);
    }

    public static double acosAngle(double ratio) {
        return radiansToAngles(Math.acos(ratio));
    }

    public static double asinAngle(double ratio) {
        return radiansToAngles(Math.asin(ratio));
    }

    public static double fixAngle(double angle) {
        double a = angle % 360.0d;
        return a < 0.0d ? a + 360.0d : a;
    }

    public static float distance(float x1, float y1, float x2, float y2) {
        float dx = x2 - x1;
        float dy = y2 - y1;
        return (float) Math.sqrt((double) ((dx * dx) + (dy * dy)));
    }

    public static float anglesToRadians(float angle) {
        return (float) (((double) angle) * 0.017453292519943295d);
    }

    public static float radiansToAngles(float radian) {
        return (float) (((double) radian) * 57.29577951308232d);
    }

    public static float sinAngle(float angle) {
        return (float) Math.sin((double) anglesToRadians(angle));
    }

    public static float cosAngle(float angle) {
        return (float) Math.cos((double) anglesToRadians(angle));
    }

    public static float tanAngle(float angle) {
        return (float) Math.tan((double) anglesToRadians(angle));
    }

    public static float atan2Angle(float y, float x) {
        return (float) radiansToAngles(Math.atan2((double) y, (double) x));
    }

    public static float angleOfLine(float x1, float y1, float x2, float y2) {
        return atan2Angle(y2 - y1, x2 - x1);
    }

    public static float acosAngle(float ratio) {
        return (float) radiansToAngles(Math.acos((double) ratio));
    }

    public static float asinAngle(float ratio) {
        return (float) radiansToAngles(Math.asin((double) ratio));
    }

    public static float fixAngle(float angle) {
        float a = angle % 360.0f;
        return a < 0.0f ? a + 360.0f : a;
    }

    public static Long min(Long... values) {
        Long m = Long.MAX_VALUE;
        for (Long f : values) {
            m = Long.valueOf(Math.min(m.longValue(), f.longValue()));
        }
        return m;
    }

    public static Long max(Long... values) {
        Long m = Long.MIN_VALUE;
        for (Long f : values) {
            m = Long.valueOf(Math.max(m.longValue(), f.longValue()));
        }
        return m;
    }

    public static Integer min(Integer... values) {
        Integer m = Integer.MAX_VALUE;
        for (Integer f : values) {
            m = Integer.valueOf(Math.min(m.intValue(), f.intValue()));
        }
        return m;
    }

    public static Integer max(Integer... values) {
        Integer m = Integer.MIN_VALUE;
        for (Integer f : values) {
            m = Integer.valueOf(Math.max(m.intValue(), f.intValue()));
        }
        return m;
    }

    public static Float min(Float... values) {
        Float m = Float.valueOf(Float.MAX_VALUE);
        for (Float f : values) {
            m = Float.valueOf(Math.min(m.floatValue(), f.floatValue()));
        }
        return m;
    }

    public static Float max(Float... values) {
        Float m = Float.valueOf(Float.MIN_VALUE);
        for (Float f : values) {
            m = Float.valueOf(Math.max(m.floatValue(), f.floatValue()));
        }
        return m;
    }

    public static Double min(Double... values) {
        Double m = Double.valueOf(Double.MAX_VALUE);
        for (Double f : values) {
            m = Double.valueOf(Math.min(m.doubleValue(), f.doubleValue()));
        }
        return m;
    }

    public static Double max(Double... values) {
        Double m = Double.valueOf(Double.MIN_VALUE);
        for (Double f : values) {
            m = Double.valueOf(Math.max(m.doubleValue(), f.doubleValue()));
        }
        return m;
    }
}
