package com.skyd.bestpuzzle;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.widget.Toast;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.ui.ScoreloopManagerSingleton;
import com.skyd.bestpuzzle.n1664.R;
import com.skyd.core.android.game.GameImageSpirit;
import com.skyd.core.android.game.GameMaster;
import com.skyd.core.android.game.GameObject;
import com.skyd.core.android.game.GameScene;
import com.skyd.core.android.game.GameSpirit;
import com.skyd.core.draw.DrawHelper;
import com.skyd.core.vector.Vector2DF;
import com.skyd.core.vector.VectorRect2DF;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class PuzzleScene extends GameScene {
    public Controller Controller;
    public Desktop Desktop;
    boolean IsDrawingPuzzle;
    boolean IsDrawnSelectRect;
    public GameImageSpirit MoveButton;
    public UI MoveButtonSlot;
    public OriginalImage OriginalImage;
    public long PastTime;
    public long StartTime;
    public UI SubmitScoreButton;
    /* access modifiers changed from: private */
    public RectF SubmitScoreButtonRect;
    public GameSpirit Time;
    public UI ViewFullButton;
    /* access modifiers changed from: private */
    public RectF ViewFullButtonRect;
    public UI ZoomInButton;
    public UI ZoomOutButton;
    private boolean _IsFinished = false;
    private ArrayList<OnIsFinishedChangedListener> _IsFinishedChangedListenerList = null;
    long savetime = Long.MIN_VALUE;
    RectF selectRect;

    public interface OnIsFinishedChangedListener {
        void OnIsFinishedChangedEvent(Object obj, boolean z);
    }

    public PuzzleScene() {
        setMaxDrawCacheLayer(25.0f);
        this.StartTime = new Date().getTime();
        loadGameState();
    }

    public void loadGameState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        this.PastTime = c.getPastTime().longValue();
        this._IsFinished = c.getIsFinished().booleanValue();
    }

    public void saveGameState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        if (getIsFinished()) {
            c.setPastTime(Long.valueOf(this.PastTime));
        } else {
            c.setPastTime(Long.valueOf(this.PastTime + (new Date().getTime() - this.StartTime)));
        }
        c.setIsFinished(Boolean.valueOf(getIsFinished()));
    }

    public void setFinished() {
        if (!getIsFinished()) {
            this.SubmitScoreButton.show();
        }
        setIsFinished(true);
        this.PastTime += new Date().getTime() - this.StartTime;
    }

    public boolean getIsFinished() {
        return this._IsFinished;
    }

    private void setIsFinished(boolean value) {
        onIsFinishedChanged(value);
        this._IsFinished = value;
    }

    private void setIsFinishedToDefault() {
        setIsFinished(false);
    }

    public boolean addOnIsFinishedChangedListener(OnIsFinishedChangedListener listener) {
        if (this._IsFinishedChangedListenerList == null) {
            this._IsFinishedChangedListenerList = new ArrayList<>();
        } else if (this._IsFinishedChangedListenerList.contains(listener)) {
            return false;
        }
        this._IsFinishedChangedListenerList.add(listener);
        return true;
    }

    public boolean removeOnIsFinishedChangedListener(OnIsFinishedChangedListener listener) {
        if (this._IsFinishedChangedListenerList == null || !this._IsFinishedChangedListenerList.contains(listener)) {
            return false;
        }
        this._IsFinishedChangedListenerList.remove(listener);
        return true;
    }

    public void clearOnIsFinishedChangedListeners() {
        if (this._IsFinishedChangedListenerList != null) {
            this._IsFinishedChangedListenerList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public void onIsFinishedChanged(boolean newValue) {
        if (this._IsFinishedChangedListenerList != null) {
            Iterator<OnIsFinishedChangedListener> it = this._IsFinishedChangedListenerList.iterator();
            while (it.hasNext()) {
                it.next().OnIsFinishedChangedEvent(this, newValue);
            }
        }
    }

    public int getTargetCacheID() {
        return 1;
    }

    /* access modifiers changed from: protected */
    public void drawChilds(Canvas c, Rect drawArea) {
        this.IsDrawingPuzzle = false;
        this.IsDrawnSelectRect = false;
        super.drawChilds(c, drawArea);
    }

    /* access modifiers changed from: protected */
    public boolean onDrawingChild(GameObject child, Canvas c, Rect drawArea) {
        float lvl = ((GameSpirit) child).getLevel();
        if (!this.IsDrawingPuzzle && lvl <= 10.0f) {
            this.IsDrawingPuzzle = true;
            c.setMatrix(this.Desktop.getMatrix());
        } else if (lvl > 10.0f && this.IsDrawingPuzzle) {
            this.IsDrawingPuzzle = false;
            c.setMatrix(new Matrix());
        }
        if (lvl > 25.0f && this.selectRect != null && !this.IsDrawnSelectRect) {
            this.IsDrawnSelectRect = true;
            Paint p = new Paint();
            p.setARGB(100, 180, 225, 255);
            c.drawRect(this.selectRect, p);
        }
        return super.onDrawingChild(child, c, drawArea);
    }

    public void load(Context c) {
        loadOriginalImage(c);
        loadDesktop(c);
        loadInterface(c);
        loadPuzzle(c);
        loadPuzzleState();
        this.Desktop.updateCurrentObserveAreaRectF();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.skyd.core.draw.DrawHelper.calculateScaleSize(float, float, float, float, boolean):com.skyd.core.vector.Vector2DF
     arg types: [int, int, float, float, int]
     candidates:
      com.skyd.core.draw.DrawHelper.calculateScaleSize(double, double, double, double, boolean):com.skyd.core.vector.Vector2D
      com.skyd.core.draw.DrawHelper.calculateScaleSize(float, float, float, float, boolean):com.skyd.core.vector.Vector2DF */
    public void loadOriginalImage(Context c) {
        this.OriginalImage = new OriginalImage() {
            /* access modifiers changed from: protected */
            public boolean onDrawing(Canvas c, Rect drawArea) {
                c.drawARGB(180, 0, 0, 0);
                return super.onDrawing(c, drawArea);
            }
        };
        this.OriginalImage.setLevel(50.0f);
        this.OriginalImage.setName("OriginalImage");
        this.OriginalImage.getSize().resetWith(DrawHelper.calculateScaleSize(384.0f, 240.0f, (float) (GameMaster.getScreenWidth() - 40), (float) (GameMaster.getScreenHeight() - 40), true));
        this.OriginalImage.setTotalSeparateColumn(8);
        this.OriginalImage.setTotalSeparateRow(5);
        this.OriginalImage.getOriginalSize().reset(800.0f, 500.0f);
        this.OriginalImage.hide();
        this.OriginalImage.setIsUseAbsolutePosition(true);
        this.OriginalImage.setIsUseAbsoluteSize(true);
        this.OriginalImage.getImage().setIsUseAbsolutePosition(true);
        this.OriginalImage.getImage().setIsUseAbsoluteSize(true);
        this.OriginalImage.getPosition().reset(((float) (GameMaster.getScreenWidth() / 2)) - (this.OriginalImage.getSize().getX() / 2.0f), ((float) (GameMaster.getScreenHeight() / 2)) - (this.OriginalImage.getSize().getY() / 2.0f));
        getSpiritList().add(this.OriginalImage);
    }

    public void loadDesktop(Context c) {
        this.Desktop = new Desktop();
        this.Desktop.setLevel(-9999.0f);
        this.Desktop.setName("Desktop");
        this.Desktop.getSize().reset(1600.0f, 1000.0f);
        this.Desktop.getCurrentObservePosition().reset(800.0f, 500.0f);
        this.Desktop.setOriginalImage(this.OriginalImage);
        this.Desktop.setIsUseAbsolutePosition(true);
        this.Desktop.setIsUseAbsoluteSize(true);
        this.Desktop.ColorAPaint = new Paint();
        this.Desktop.ColorAPaint.setColor(Color.argb(255, 32, 30, 43));
        this.Desktop.ColorBPaint = new Paint();
        this.Desktop.ColorBPaint.setColor(Color.argb(255, 26, 24, 37));
        getSpiritList().add(this.Desktop);
    }

    public void loadPuzzle(Context c) {
        this.Controller = new Controller();
        this.Controller.setLevel(10.0f);
        this.Controller.setName("Controller");
        this.Controller.setDesktop(this.Desktop);
        this.Controller.setIsUseAbsolutePosition(true);
        this.Controller.setIsUseAbsoluteSize(true);
        getSpiritList().add(this.Controller);
        Puzzle.getRealitySize().reset(100.0f, 100.0f);
        Puzzle.getMaxRadius().reset(76.66666f, 76.66666f);
        c1613920405(c);
        c1431872831(c);
        c1404494026(c);
        c776188893(c);
        c1126791243(c);
        c1217677707(c);
        c969662768(c);
        c422595011(c);
        c362904324(c);
        c345594712(c);
        c2026754542(c);
        c1538767557(c);
        c2017758669(c);
        c1417286977(c);
        c1846395478(c);
        c144606166(c);
        c399365588(c);
        c1571673992(c);
        c11303032(c);
        c519585287(c);
        c518215134(c);
        c840606173(c);
        c170824034(c);
        c1483277057(c);
        c652541131(c);
        c369000380(c);
        c1668472540(c);
        c2108886431(c);
        c288563999(c);
        c1413505620(c);
        c1369089708(c);
        c2012198188(c);
        c1002936684(c);
        c2085467739(c);
        c575330746(c);
        c401861086(c);
        c211953248(c);
        c9616941(c);
        c1875148103(c);
        c294717271(c);
    }

    public void loadInterface(Context c) {
        this.MoveButton = new GameImageSpirit();
        this.MoveButton.setLevel(22.0f);
        this.MoveButton.getImage().loadImageFromResource(c, R.drawable.movebutton);
        this.MoveButton.getSize().reset(155.0f, 155.0f);
        this.MoveButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.MoveButton);
        final Vector2DF mvds = this.MoveButton.getDisplaySize();
        mvds.scale(0.5f);
        this.MoveButton.getPosition().reset(5.0f + mvds.getX(), ((float) (GameMaster.getScreenHeight() - 5)) - mvds.getY());
        this.MoveButton.getPositionOffset().resetWith(mvds.negateNew());
        this.MoveButtonSlot = new UI() {
            Vector2DF movevector;
            boolean needmove;

            public void executive(Vector2DF point) {
                this.movevector = point.minusNew(getPosition()).restrainLength(mvds.getX() * 0.4f);
                this.needmove = true;
            }

            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.needmove) {
                    PuzzleScene.this.MoveButton.getPosition().resetWith(getPosition().plusNew(this.movevector));
                    PuzzleScene.this.Desktop.getCurrentObservePosition().plus(this.movevector);
                    PuzzleScene.this.refreshDrawCacheBitmap();
                }
                super.updateSelf();
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < mvds.getX() + 5.0f;
            }

            public void reset() {
                this.needmove = false;
                PuzzleScene.this.MoveButton.getPosition().resetWith(getPosition());
                PuzzleScene.this.refreshDrawCacheBitmap();
            }
        };
        this.MoveButtonSlot.setLevel(21.0f);
        this.MoveButtonSlot.getImage().loadImageFromResource(c, R.drawable.movebuttonslot);
        this.MoveButtonSlot.getSize().reset(155.0f, 155.0f);
        this.MoveButtonSlot.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.MoveButtonSlot);
        this.MoveButtonSlot.getPosition().resetWith(this.MoveButton.getPosition());
        this.MoveButtonSlot.getPositionOffset().resetWith(this.MoveButton.getPositionOffset());
        this.ZoomInButton = new UI() {
            boolean zoom;

            public void executive(Vector2DF point) {
                this.zoom = true;
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < getDisplaySize().getX() / 2.0f;
            }

            public void reset() {
                this.zoom = false;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.lang.Math.min(float, float):float}
             arg types: [int, float]
             candidates:
              ClspMth{java.lang.Math.min(double, double):double}
              ClspMth{java.lang.Math.min(long, long):long}
              ClspMth{java.lang.Math.min(int, int):int}
              ClspMth{java.lang.Math.min(float, float):float} */
            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.zoom) {
                    PuzzleScene.this.Desktop.setZoom(Math.min(1.3f, PuzzleScene.this.Desktop.getZoom() * 1.05f));
                    PuzzleScene.this.refreshDrawCacheBitmap();
                }
                super.updateSelf();
            }
        };
        this.ZoomInButton.setLevel(21.0f);
        this.ZoomInButton.getImage().loadImageFromResource(c, R.drawable.zoombutton1);
        this.ZoomInButton.getSize().reset(77.0f, 77.0f);
        this.ZoomInButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.ZoomInButton);
        Vector2DF zibds = this.ZoomInButton.getDisplaySize().scale(0.5f);
        this.ZoomInButton.getPosition().reset(((float) (GameMaster.getScreenWidth() - 50)) - zibds.getX(), ((float) (GameMaster.getScreenHeight() - 10)) - zibds.getY());
        this.ZoomInButton.getPositionOffset().resetWith(zibds.negateNew());
        this.ZoomOutButton = new UI() {
            boolean zoom;

            public void executive(Vector2DF point) {
                this.zoom = true;
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < getDisplaySize().getX() / 2.0f;
            }

            public void reset() {
                this.zoom = false;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.lang.Math.max(float, float):float}
             arg types: [int, float]
             candidates:
              ClspMth{java.lang.Math.max(double, double):double}
              ClspMth{java.lang.Math.max(int, int):int}
              ClspMth{java.lang.Math.max(long, long):long}
              ClspMth{java.lang.Math.max(float, float):float} */
            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.zoom) {
                    PuzzleScene.this.Desktop.setZoom(Math.max(0.3f, PuzzleScene.this.Desktop.getZoom() * 0.95f));
                    PuzzleScene.this.refreshDrawCacheBitmap();
                    GameMaster.log(this, Float.valueOf(PuzzleScene.this.Desktop.getCurrentObserveAreaRectF().width()));
                }
                super.updateSelf();
            }
        };
        this.ZoomOutButton.setLevel(21.0f);
        this.ZoomOutButton.getImage().loadImageFromResource(c, R.drawable.zoombutton2);
        this.ZoomOutButton.getSize().reset(77.0f, 77.0f);
        this.ZoomOutButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.ZoomOutButton);
        Vector2DF zobds = this.ZoomOutButton.getDisplaySize().scale(0.5f);
        this.ZoomOutButton.getPosition().reset((((float) (GameMaster.getScreenWidth() - 100)) - zobds.getX()) - (zibds.getX() * 2.0f), ((float) (GameMaster.getScreenHeight() - 10)) - zobds.getY());
        this.ZoomOutButton.getPositionOffset().resetWith(zobds.negateNew());
        this.ViewFullButton = new UI() {
            public void executive(Vector2DF point) {
                if (PuzzleScene.this.OriginalImage.getImage().getImage() == null) {
                    PuzzleScene.this.OriginalImage.getImage().loadImageFromResource(GameMaster.getContext(), R.drawable.oim);
                }
                PuzzleScene.this.OriginalImage.show();
            }

            public boolean isInArea(Vector2DF point) {
                return point.isIn(PuzzleScene.this.ViewFullButtonRect);
            }

            public void reset() {
                PuzzleScene.this.OriginalImage.hide();
            }
        };
        this.ViewFullButton.getImage().loadImageFromResource(c, R.drawable.viewfull);
        this.ViewFullButton.setLevel(21.0f);
        this.ViewFullButton.setIsUseAbsolutePosition(true);
        this.ViewFullButton.getSize().reset(83.0f, 38.0f);
        getSpiritList().add(this.ViewFullButton);
        this.ViewFullButton.getPosition().reset(((float) (GameMaster.getScreenWidth() - 15)) - this.ViewFullButton.getDisplaySize().getX(), 15.0f);
        this.ViewFullButtonRect = new VectorRect2DF(this.ViewFullButton.getPosition(), this.ViewFullButton.getDisplaySize()).getRectF();
        this.SubmitScoreButton = new UI() {
            public void executive(Vector2DF point) {
                Score score = new Score(Double.valueOf(Math.ceil((double) (PuzzleScene.this.PastTime / 1000))), null);
                score.setMode(17);
                ScoreloopManagerSingleton.get().onGamePlayEnded(score);
                hide();
                Toast.makeText(GameMaster.getContext(), (int) R.string.Submitting, 1).show();
            }

            public boolean isInArea(Vector2DF point) {
                return getVisibleOriginalValue() && point.isIn(PuzzleScene.this.SubmitScoreButtonRect);
            }

            public void reset() {
            }
        };
        this.SubmitScoreButton.getImage().loadImageFromResource(c, R.drawable.submitscore);
        this.SubmitScoreButton.setLevel(21.0f);
        this.SubmitScoreButton.setIsUseAbsolutePosition(true);
        this.SubmitScoreButton.getSize().reset(114.0f, 40.0f);
        getSpiritList().add(this.SubmitScoreButton);
        this.SubmitScoreButton.getPosition().reset(15.0f, 60.0f);
        this.SubmitScoreButtonRect = new VectorRect2DF(this.SubmitScoreButton.getPosition(), this.SubmitScoreButton.getDisplaySize()).getRectF();
        this.SubmitScoreButton.hide();
        this.Time = new GameSpirit() {
            DecimalFormat FMT = new DecimalFormat("00");
            Paint p1;
            Paint p2;

            public GameObject getDisplayContentChild() {
                return null;
            }

            /* access modifiers changed from: protected */
            public void drawSelf(Canvas c, Rect drawArea) {
                if (this.p1 == null) {
                    this.p1 = new Paint();
                    this.p1.setARGB(160, 255, 255, 255);
                    this.p1.setAntiAlias(true);
                    this.p1.setTextSize(28.0f);
                    this.p1.setTypeface(Typeface.createFromAsset(GameMaster.getContext().getAssets(), "fonts/font.ttf"));
                }
                if (this.p2 == null) {
                    this.p2 = new Paint();
                    this.p2.setARGB(100, 0, 0, 0);
                    this.p2.setAntiAlias(true);
                    this.p2.setTextSize(28.0f);
                    this.p2.setTypeface(Typeface.createFromAsset(GameMaster.getContext().getAssets(), "fonts/font.ttf"));
                }
                long t = PuzzleScene.this.getIsFinished() ? PuzzleScene.this.PastTime : PuzzleScene.this.PastTime + (new Date().getTime() - PuzzleScene.this.StartTime);
                long h = t / 3600000;
                long m = (t - (((1000 * h) * 60) * 60)) / 60000;
                String str = String.valueOf(this.FMT.format(h)) + ":" + this.FMT.format(m) + ":" + this.FMT.format(((t - (((1000 * h) * 60) * 60)) - ((1000 * m) * 60)) / 1000) + (PuzzleScene.this.getIsFinished() ? " Finished" : "");
                c.drawText(str, 16.0f, 44.0f, this.p2);
                c.drawText(str, 15.0f, 43.0f, this.p1);
                super.drawSelf(c, drawArea);
            }
        };
        this.Time.setLevel(40.0f);
        getSpiritList().add(this.Time);
    }

    public float getMaxPuzzleLevel() {
        float maxlvl = 0.0f;
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            if (f.getLevel() <= 10.0f) {
                maxlvl = Math.max(f.getLevel(), maxlvl);
            }
        }
        return maxlvl;
    }

    public Puzzle getTouchPuzzle(Vector2DF v) {
        Vector2DF rv = this.Desktop.reMapPoint(v);
        Puzzle o = null;
        float olen = 99999.0f;
        for (int i = getSpiritList().size() - 1; i >= 0; i--) {
            if (getSpiritList().get(i) instanceof Puzzle) {
                Puzzle p = (Puzzle) getSpiritList().get(i);
                float len = p.getPositionInDesktop().minusNew(rv).getLength();
                if (len <= Puzzle.getRealitySize().getX() * 0.4f) {
                    return p;
                }
                if (len <= Puzzle.getRealitySize().getX() && olen > len) {
                    o = p;
                    olen = len;
                }
            }
        }
        return o;
    }

    public void startDrawSelectRect(Vector2DF startDragPoint, Vector2DF v) {
        this.selectRect = new VectorRect2DF(startDragPoint, v.minusNew(startDragPoint)).getFixedRectF();
    }

    public void stopDrawSelectRect() {
        this.selectRect = null;
    }

    public ArrayList<Puzzle> getSelectPuzzle(Vector2DF startDragPoint, Vector2DF v) {
        Vector2DF rs = this.Desktop.reMapPoint(startDragPoint);
        ArrayList<Puzzle> l = new ArrayList<>();
        RectF rect = new VectorRect2DF(rs, this.Desktop.reMapPoint(v).minusNew(rs)).getFixedRectF();
        float xo = Puzzle.getRealitySize().getX() / 3.0f;
        float yo = Puzzle.getRealitySize().getY() / 3.0f;
        RectF sr = new RectF(rect.left - xo, rect.top - yo, rect.right + xo, rect.bottom + yo);
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            if (f.getPositionInDesktop().isIn(sr)) {
                l.add(f);
            }
        }
        return l;
    }

    public void loadPuzzleState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        SharedPreferences s = c.getSharedPreferences();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            it.next().load(c, s);
        }
    }

    public void savePuzzleState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        SharedPreferences.Editor e = c.getSharedPreferences().edit();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            it.next().save(c, e);
        }
        e.commit();
    }

    public void reset() {
        this.Controller.reset();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            f.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
            this.Desktop.RandomlyPlaced(f);
        }
        refreshDrawCacheBitmap();
        setIsFinishedToDefault();
        this.StartTime = new Date().getTime();
        this.PastTime = 0;
        this.SubmitScoreButton.hide();
        saveGameState();
        savePuzzleState();
    }

    /* access modifiers changed from: package-private */
    public void c1613920405(Context c) {
        Puzzle p1613920405 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1613920405(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1613920405(editor, this);
            }
        };
        p1613920405.setID(1613920405);
        p1613920405.setName("1613920405");
        p1613920405.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1613920405);
        this.Desktop.RandomlyPlaced(p1613920405);
        p1613920405.setTopEdgeType(EdgeType.Flat);
        p1613920405.setBottomEdgeType(EdgeType.Concave);
        p1613920405.setLeftEdgeType(EdgeType.Flat);
        p1613920405.setRightEdgeType(EdgeType.Convex);
        p1613920405.setTopExactPuzzleID(-1);
        p1613920405.setBottomExactPuzzleID(1431872831);
        p1613920405.setLeftExactPuzzleID(-1);
        p1613920405.setRightExactPuzzleID(1217677707);
        p1613920405.getDisplayImage().loadImageFromResource(c, R.drawable.p1613920405h);
        p1613920405.setExactRow(0);
        p1613920405.setExactColumn(0);
        p1613920405.getSize().reset(126.6667f, 100.0f);
        p1613920405.getPositionOffset().reset(-50.0f, -50.0f);
        p1613920405.setIsUseAbsolutePosition(true);
        p1613920405.setIsUseAbsoluteSize(true);
        p1613920405.getImage().setIsUseAbsolutePosition(true);
        p1613920405.getImage().setIsUseAbsoluteSize(true);
        p1613920405.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1613920405.resetPosition();
        getSpiritList().add(p1613920405);
    }

    /* access modifiers changed from: package-private */
    public void c1431872831(Context c) {
        Puzzle p1431872831 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1431872831(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1431872831(editor, this);
            }
        };
        p1431872831.setID(1431872831);
        p1431872831.setName("1431872831");
        p1431872831.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1431872831);
        this.Desktop.RandomlyPlaced(p1431872831);
        p1431872831.setTopEdgeType(EdgeType.Convex);
        p1431872831.setBottomEdgeType(EdgeType.Concave);
        p1431872831.setLeftEdgeType(EdgeType.Flat);
        p1431872831.setRightEdgeType(EdgeType.Concave);
        p1431872831.setTopExactPuzzleID(1613920405);
        p1431872831.setBottomExactPuzzleID(1404494026);
        p1431872831.setLeftExactPuzzleID(-1);
        p1431872831.setRightExactPuzzleID(969662768);
        p1431872831.getDisplayImage().loadImageFromResource(c, R.drawable.p1431872831h);
        p1431872831.setExactRow(1);
        p1431872831.setExactColumn(0);
        p1431872831.getSize().reset(100.0f, 126.6667f);
        p1431872831.getPositionOffset().reset(-50.0f, -76.66666f);
        p1431872831.setIsUseAbsolutePosition(true);
        p1431872831.setIsUseAbsoluteSize(true);
        p1431872831.getImage().setIsUseAbsolutePosition(true);
        p1431872831.getImage().setIsUseAbsoluteSize(true);
        p1431872831.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1431872831.resetPosition();
        getSpiritList().add(p1431872831);
    }

    /* access modifiers changed from: package-private */
    public void c1404494026(Context c) {
        Puzzle p1404494026 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1404494026(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1404494026(editor, this);
            }
        };
        p1404494026.setID(1404494026);
        p1404494026.setName("1404494026");
        p1404494026.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1404494026);
        this.Desktop.RandomlyPlaced(p1404494026);
        p1404494026.setTopEdgeType(EdgeType.Convex);
        p1404494026.setBottomEdgeType(EdgeType.Convex);
        p1404494026.setLeftEdgeType(EdgeType.Flat);
        p1404494026.setRightEdgeType(EdgeType.Concave);
        p1404494026.setTopExactPuzzleID(1431872831);
        p1404494026.setBottomExactPuzzleID(776188893);
        p1404494026.setLeftExactPuzzleID(-1);
        p1404494026.setRightExactPuzzleID(422595011);
        p1404494026.getDisplayImage().loadImageFromResource(c, R.drawable.p1404494026h);
        p1404494026.setExactRow(2);
        p1404494026.setExactColumn(0);
        p1404494026.getSize().reset(100.0f, 153.3333f);
        p1404494026.getPositionOffset().reset(-50.0f, -76.66666f);
        p1404494026.setIsUseAbsolutePosition(true);
        p1404494026.setIsUseAbsoluteSize(true);
        p1404494026.getImage().setIsUseAbsolutePosition(true);
        p1404494026.getImage().setIsUseAbsoluteSize(true);
        p1404494026.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1404494026.resetPosition();
        getSpiritList().add(p1404494026);
    }

    /* access modifiers changed from: package-private */
    public void c776188893(Context c) {
        Puzzle p776188893 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load776188893(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save776188893(editor, this);
            }
        };
        p776188893.setID(776188893);
        p776188893.setName("776188893");
        p776188893.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p776188893);
        this.Desktop.RandomlyPlaced(p776188893);
        p776188893.setTopEdgeType(EdgeType.Concave);
        p776188893.setBottomEdgeType(EdgeType.Concave);
        p776188893.setLeftEdgeType(EdgeType.Flat);
        p776188893.setRightEdgeType(EdgeType.Convex);
        p776188893.setTopExactPuzzleID(1404494026);
        p776188893.setBottomExactPuzzleID(1126791243);
        p776188893.setLeftExactPuzzleID(-1);
        p776188893.setRightExactPuzzleID(362904324);
        p776188893.getDisplayImage().loadImageFromResource(c, R.drawable.p776188893h);
        p776188893.setExactRow(3);
        p776188893.setExactColumn(0);
        p776188893.getSize().reset(126.6667f, 100.0f);
        p776188893.getPositionOffset().reset(-50.0f, -50.0f);
        p776188893.setIsUseAbsolutePosition(true);
        p776188893.setIsUseAbsoluteSize(true);
        p776188893.getImage().setIsUseAbsolutePosition(true);
        p776188893.getImage().setIsUseAbsoluteSize(true);
        p776188893.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p776188893.resetPosition();
        getSpiritList().add(p776188893);
    }

    /* access modifiers changed from: package-private */
    public void c1126791243(Context c) {
        Puzzle p1126791243 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1126791243(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1126791243(editor, this);
            }
        };
        p1126791243.setID(1126791243);
        p1126791243.setName("1126791243");
        p1126791243.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1126791243);
        this.Desktop.RandomlyPlaced(p1126791243);
        p1126791243.setTopEdgeType(EdgeType.Convex);
        p1126791243.setBottomEdgeType(EdgeType.Flat);
        p1126791243.setLeftEdgeType(EdgeType.Flat);
        p1126791243.setRightEdgeType(EdgeType.Convex);
        p1126791243.setTopExactPuzzleID(776188893);
        p1126791243.setBottomExactPuzzleID(-1);
        p1126791243.setLeftExactPuzzleID(-1);
        p1126791243.setRightExactPuzzleID(345594712);
        p1126791243.getDisplayImage().loadImageFromResource(c, R.drawable.p1126791243h);
        p1126791243.setExactRow(4);
        p1126791243.setExactColumn(0);
        p1126791243.getSize().reset(126.6667f, 126.6667f);
        p1126791243.getPositionOffset().reset(-50.0f, -76.66666f);
        p1126791243.setIsUseAbsolutePosition(true);
        p1126791243.setIsUseAbsoluteSize(true);
        p1126791243.getImage().setIsUseAbsolutePosition(true);
        p1126791243.getImage().setIsUseAbsoluteSize(true);
        p1126791243.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1126791243.resetPosition();
        getSpiritList().add(p1126791243);
    }

    /* access modifiers changed from: package-private */
    public void c1217677707(Context c) {
        Puzzle p1217677707 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1217677707(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1217677707(editor, this);
            }
        };
        p1217677707.setID(1217677707);
        p1217677707.setName("1217677707");
        p1217677707.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1217677707);
        this.Desktop.RandomlyPlaced(p1217677707);
        p1217677707.setTopEdgeType(EdgeType.Flat);
        p1217677707.setBottomEdgeType(EdgeType.Convex);
        p1217677707.setLeftEdgeType(EdgeType.Concave);
        p1217677707.setRightEdgeType(EdgeType.Concave);
        p1217677707.setTopExactPuzzleID(-1);
        p1217677707.setBottomExactPuzzleID(969662768);
        p1217677707.setLeftExactPuzzleID(1613920405);
        p1217677707.setRightExactPuzzleID(2026754542);
        p1217677707.getDisplayImage().loadImageFromResource(c, R.drawable.p1217677707h);
        p1217677707.setExactRow(0);
        p1217677707.setExactColumn(1);
        p1217677707.getSize().reset(100.0f, 126.6667f);
        p1217677707.getPositionOffset().reset(-50.0f, -50.0f);
        p1217677707.setIsUseAbsolutePosition(true);
        p1217677707.setIsUseAbsoluteSize(true);
        p1217677707.getImage().setIsUseAbsolutePosition(true);
        p1217677707.getImage().setIsUseAbsoluteSize(true);
        p1217677707.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1217677707.resetPosition();
        getSpiritList().add(p1217677707);
    }

    /* access modifiers changed from: package-private */
    public void c969662768(Context c) {
        Puzzle p969662768 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load969662768(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save969662768(editor, this);
            }
        };
        p969662768.setID(969662768);
        p969662768.setName("969662768");
        p969662768.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p969662768);
        this.Desktop.RandomlyPlaced(p969662768);
        p969662768.setTopEdgeType(EdgeType.Concave);
        p969662768.setBottomEdgeType(EdgeType.Concave);
        p969662768.setLeftEdgeType(EdgeType.Convex);
        p969662768.setRightEdgeType(EdgeType.Concave);
        p969662768.setTopExactPuzzleID(1217677707);
        p969662768.setBottomExactPuzzleID(422595011);
        p969662768.setLeftExactPuzzleID(1431872831);
        p969662768.setRightExactPuzzleID(1538767557);
        p969662768.getDisplayImage().loadImageFromResource(c, R.drawable.p969662768h);
        p969662768.setExactRow(1);
        p969662768.setExactColumn(1);
        p969662768.getSize().reset(126.6667f, 100.0f);
        p969662768.getPositionOffset().reset(-76.66666f, -50.0f);
        p969662768.setIsUseAbsolutePosition(true);
        p969662768.setIsUseAbsoluteSize(true);
        p969662768.getImage().setIsUseAbsolutePosition(true);
        p969662768.getImage().setIsUseAbsoluteSize(true);
        p969662768.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p969662768.resetPosition();
        getSpiritList().add(p969662768);
    }

    /* access modifiers changed from: package-private */
    public void c422595011(Context c) {
        Puzzle p422595011 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load422595011(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save422595011(editor, this);
            }
        };
        p422595011.setID(422595011);
        p422595011.setName("422595011");
        p422595011.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p422595011);
        this.Desktop.RandomlyPlaced(p422595011);
        p422595011.setTopEdgeType(EdgeType.Convex);
        p422595011.setBottomEdgeType(EdgeType.Convex);
        p422595011.setLeftEdgeType(EdgeType.Convex);
        p422595011.setRightEdgeType(EdgeType.Concave);
        p422595011.setTopExactPuzzleID(969662768);
        p422595011.setBottomExactPuzzleID(362904324);
        p422595011.setLeftExactPuzzleID(1404494026);
        p422595011.setRightExactPuzzleID(2017758669);
        p422595011.getDisplayImage().loadImageFromResource(c, R.drawable.p422595011h);
        p422595011.setExactRow(2);
        p422595011.setExactColumn(1);
        p422595011.getSize().reset(126.6667f, 153.3333f);
        p422595011.getPositionOffset().reset(-76.66666f, -76.66666f);
        p422595011.setIsUseAbsolutePosition(true);
        p422595011.setIsUseAbsoluteSize(true);
        p422595011.getImage().setIsUseAbsolutePosition(true);
        p422595011.getImage().setIsUseAbsoluteSize(true);
        p422595011.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p422595011.resetPosition();
        getSpiritList().add(p422595011);
    }

    /* access modifiers changed from: package-private */
    public void c362904324(Context c) {
        Puzzle p362904324 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load362904324(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save362904324(editor, this);
            }
        };
        p362904324.setID(362904324);
        p362904324.setName("362904324");
        p362904324.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p362904324);
        this.Desktop.RandomlyPlaced(p362904324);
        p362904324.setTopEdgeType(EdgeType.Concave);
        p362904324.setBottomEdgeType(EdgeType.Concave);
        p362904324.setLeftEdgeType(EdgeType.Concave);
        p362904324.setRightEdgeType(EdgeType.Convex);
        p362904324.setTopExactPuzzleID(422595011);
        p362904324.setBottomExactPuzzleID(345594712);
        p362904324.setLeftExactPuzzleID(776188893);
        p362904324.setRightExactPuzzleID(1417286977);
        p362904324.getDisplayImage().loadImageFromResource(c, R.drawable.p362904324h);
        p362904324.setExactRow(3);
        p362904324.setExactColumn(1);
        p362904324.getSize().reset(126.6667f, 100.0f);
        p362904324.getPositionOffset().reset(-50.0f, -50.0f);
        p362904324.setIsUseAbsolutePosition(true);
        p362904324.setIsUseAbsoluteSize(true);
        p362904324.getImage().setIsUseAbsolutePosition(true);
        p362904324.getImage().setIsUseAbsoluteSize(true);
        p362904324.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p362904324.resetPosition();
        getSpiritList().add(p362904324);
    }

    /* access modifiers changed from: package-private */
    public void c345594712(Context c) {
        Puzzle p345594712 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load345594712(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save345594712(editor, this);
            }
        };
        p345594712.setID(345594712);
        p345594712.setName("345594712");
        p345594712.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p345594712);
        this.Desktop.RandomlyPlaced(p345594712);
        p345594712.setTopEdgeType(EdgeType.Convex);
        p345594712.setBottomEdgeType(EdgeType.Flat);
        p345594712.setLeftEdgeType(EdgeType.Concave);
        p345594712.setRightEdgeType(EdgeType.Convex);
        p345594712.setTopExactPuzzleID(362904324);
        p345594712.setBottomExactPuzzleID(-1);
        p345594712.setLeftExactPuzzleID(1126791243);
        p345594712.setRightExactPuzzleID(1846395478);
        p345594712.getDisplayImage().loadImageFromResource(c, R.drawable.p345594712h);
        p345594712.setExactRow(4);
        p345594712.setExactColumn(1);
        p345594712.getSize().reset(126.6667f, 126.6667f);
        p345594712.getPositionOffset().reset(-50.0f, -76.66666f);
        p345594712.setIsUseAbsolutePosition(true);
        p345594712.setIsUseAbsoluteSize(true);
        p345594712.getImage().setIsUseAbsolutePosition(true);
        p345594712.getImage().setIsUseAbsoluteSize(true);
        p345594712.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p345594712.resetPosition();
        getSpiritList().add(p345594712);
    }

    /* access modifiers changed from: package-private */
    public void c2026754542(Context c) {
        Puzzle p2026754542 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2026754542(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2026754542(editor, this);
            }
        };
        p2026754542.setID(2026754542);
        p2026754542.setName("2026754542");
        p2026754542.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2026754542);
        this.Desktop.RandomlyPlaced(p2026754542);
        p2026754542.setTopEdgeType(EdgeType.Flat);
        p2026754542.setBottomEdgeType(EdgeType.Concave);
        p2026754542.setLeftEdgeType(EdgeType.Convex);
        p2026754542.setRightEdgeType(EdgeType.Concave);
        p2026754542.setTopExactPuzzleID(-1);
        p2026754542.setBottomExactPuzzleID(1538767557);
        p2026754542.setLeftExactPuzzleID(1217677707);
        p2026754542.setRightExactPuzzleID(144606166);
        p2026754542.getDisplayImage().loadImageFromResource(c, R.drawable.p2026754542h);
        p2026754542.setExactRow(0);
        p2026754542.setExactColumn(2);
        p2026754542.getSize().reset(126.6667f, 100.0f);
        p2026754542.getPositionOffset().reset(-76.66666f, -50.0f);
        p2026754542.setIsUseAbsolutePosition(true);
        p2026754542.setIsUseAbsoluteSize(true);
        p2026754542.getImage().setIsUseAbsolutePosition(true);
        p2026754542.getImage().setIsUseAbsoluteSize(true);
        p2026754542.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2026754542.resetPosition();
        getSpiritList().add(p2026754542);
    }

    /* access modifiers changed from: package-private */
    public void c1538767557(Context c) {
        Puzzle p1538767557 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1538767557(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1538767557(editor, this);
            }
        };
        p1538767557.setID(1538767557);
        p1538767557.setName("1538767557");
        p1538767557.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1538767557);
        this.Desktop.RandomlyPlaced(p1538767557);
        p1538767557.setTopEdgeType(EdgeType.Convex);
        p1538767557.setBottomEdgeType(EdgeType.Concave);
        p1538767557.setLeftEdgeType(EdgeType.Convex);
        p1538767557.setRightEdgeType(EdgeType.Convex);
        p1538767557.setTopExactPuzzleID(2026754542);
        p1538767557.setBottomExactPuzzleID(2017758669);
        p1538767557.setLeftExactPuzzleID(969662768);
        p1538767557.setRightExactPuzzleID(399365588);
        p1538767557.getDisplayImage().loadImageFromResource(c, R.drawable.p1538767557h);
        p1538767557.setExactRow(1);
        p1538767557.setExactColumn(2);
        p1538767557.getSize().reset(153.3333f, 126.6667f);
        p1538767557.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1538767557.setIsUseAbsolutePosition(true);
        p1538767557.setIsUseAbsoluteSize(true);
        p1538767557.getImage().setIsUseAbsolutePosition(true);
        p1538767557.getImage().setIsUseAbsoluteSize(true);
        p1538767557.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1538767557.resetPosition();
        getSpiritList().add(p1538767557);
    }

    /* access modifiers changed from: package-private */
    public void c2017758669(Context c) {
        Puzzle p2017758669 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2017758669(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2017758669(editor, this);
            }
        };
        p2017758669.setID(2017758669);
        p2017758669.setName("2017758669");
        p2017758669.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2017758669);
        this.Desktop.RandomlyPlaced(p2017758669);
        p2017758669.setTopEdgeType(EdgeType.Convex);
        p2017758669.setBottomEdgeType(EdgeType.Convex);
        p2017758669.setLeftEdgeType(EdgeType.Convex);
        p2017758669.setRightEdgeType(EdgeType.Convex);
        p2017758669.setTopExactPuzzleID(1538767557);
        p2017758669.setBottomExactPuzzleID(1417286977);
        p2017758669.setLeftExactPuzzleID(422595011);
        p2017758669.setRightExactPuzzleID(1571673992);
        p2017758669.getDisplayImage().loadImageFromResource(c, R.drawable.p2017758669h);
        p2017758669.setExactRow(2);
        p2017758669.setExactColumn(2);
        p2017758669.getSize().reset(153.3333f, 153.3333f);
        p2017758669.getPositionOffset().reset(-76.66666f, -76.66666f);
        p2017758669.setIsUseAbsolutePosition(true);
        p2017758669.setIsUseAbsoluteSize(true);
        p2017758669.getImage().setIsUseAbsolutePosition(true);
        p2017758669.getImage().setIsUseAbsoluteSize(true);
        p2017758669.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2017758669.resetPosition();
        getSpiritList().add(p2017758669);
    }

    /* access modifiers changed from: package-private */
    public void c1417286977(Context c) {
        Puzzle p1417286977 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1417286977(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1417286977(editor, this);
            }
        };
        p1417286977.setID(1417286977);
        p1417286977.setName("1417286977");
        p1417286977.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1417286977);
        this.Desktop.RandomlyPlaced(p1417286977);
        p1417286977.setTopEdgeType(EdgeType.Concave);
        p1417286977.setBottomEdgeType(EdgeType.Concave);
        p1417286977.setLeftEdgeType(EdgeType.Concave);
        p1417286977.setRightEdgeType(EdgeType.Concave);
        p1417286977.setTopExactPuzzleID(2017758669);
        p1417286977.setBottomExactPuzzleID(1846395478);
        p1417286977.setLeftExactPuzzleID(362904324);
        p1417286977.setRightExactPuzzleID(11303032);
        p1417286977.getDisplayImage().loadImageFromResource(c, R.drawable.p1417286977h);
        p1417286977.setExactRow(3);
        p1417286977.setExactColumn(2);
        p1417286977.getSize().reset(100.0f, 100.0f);
        p1417286977.getPositionOffset().reset(-50.0f, -50.0f);
        p1417286977.setIsUseAbsolutePosition(true);
        p1417286977.setIsUseAbsoluteSize(true);
        p1417286977.getImage().setIsUseAbsolutePosition(true);
        p1417286977.getImage().setIsUseAbsoluteSize(true);
        p1417286977.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1417286977.resetPosition();
        getSpiritList().add(p1417286977);
    }

    /* access modifiers changed from: package-private */
    public void c1846395478(Context c) {
        Puzzle p1846395478 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1846395478(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1846395478(editor, this);
            }
        };
        p1846395478.setID(1846395478);
        p1846395478.setName("1846395478");
        p1846395478.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1846395478);
        this.Desktop.RandomlyPlaced(p1846395478);
        p1846395478.setTopEdgeType(EdgeType.Convex);
        p1846395478.setBottomEdgeType(EdgeType.Flat);
        p1846395478.setLeftEdgeType(EdgeType.Concave);
        p1846395478.setRightEdgeType(EdgeType.Convex);
        p1846395478.setTopExactPuzzleID(1417286977);
        p1846395478.setBottomExactPuzzleID(-1);
        p1846395478.setLeftExactPuzzleID(345594712);
        p1846395478.setRightExactPuzzleID(519585287);
        p1846395478.getDisplayImage().loadImageFromResource(c, R.drawable.p1846395478h);
        p1846395478.setExactRow(4);
        p1846395478.setExactColumn(2);
        p1846395478.getSize().reset(126.6667f, 126.6667f);
        p1846395478.getPositionOffset().reset(-50.0f, -76.66666f);
        p1846395478.setIsUseAbsolutePosition(true);
        p1846395478.setIsUseAbsoluteSize(true);
        p1846395478.getImage().setIsUseAbsolutePosition(true);
        p1846395478.getImage().setIsUseAbsoluteSize(true);
        p1846395478.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1846395478.resetPosition();
        getSpiritList().add(p1846395478);
    }

    /* access modifiers changed from: package-private */
    public void c144606166(Context c) {
        Puzzle p144606166 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load144606166(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save144606166(editor, this);
            }
        };
        p144606166.setID(144606166);
        p144606166.setName("144606166");
        p144606166.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p144606166);
        this.Desktop.RandomlyPlaced(p144606166);
        p144606166.setTopEdgeType(EdgeType.Flat);
        p144606166.setBottomEdgeType(EdgeType.Concave);
        p144606166.setLeftEdgeType(EdgeType.Convex);
        p144606166.setRightEdgeType(EdgeType.Convex);
        p144606166.setTopExactPuzzleID(-1);
        p144606166.setBottomExactPuzzleID(399365588);
        p144606166.setLeftExactPuzzleID(2026754542);
        p144606166.setRightExactPuzzleID(518215134);
        p144606166.getDisplayImage().loadImageFromResource(c, R.drawable.p144606166h);
        p144606166.setExactRow(0);
        p144606166.setExactColumn(3);
        p144606166.getSize().reset(153.3333f, 100.0f);
        p144606166.getPositionOffset().reset(-76.66666f, -50.0f);
        p144606166.setIsUseAbsolutePosition(true);
        p144606166.setIsUseAbsoluteSize(true);
        p144606166.getImage().setIsUseAbsolutePosition(true);
        p144606166.getImage().setIsUseAbsoluteSize(true);
        p144606166.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p144606166.resetPosition();
        getSpiritList().add(p144606166);
    }

    /* access modifiers changed from: package-private */
    public void c399365588(Context c) {
        Puzzle p399365588 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load399365588(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save399365588(editor, this);
            }
        };
        p399365588.setID(399365588);
        p399365588.setName("399365588");
        p399365588.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p399365588);
        this.Desktop.RandomlyPlaced(p399365588);
        p399365588.setTopEdgeType(EdgeType.Convex);
        p399365588.setBottomEdgeType(EdgeType.Concave);
        p399365588.setLeftEdgeType(EdgeType.Concave);
        p399365588.setRightEdgeType(EdgeType.Convex);
        p399365588.setTopExactPuzzleID(144606166);
        p399365588.setBottomExactPuzzleID(1571673992);
        p399365588.setLeftExactPuzzleID(1538767557);
        p399365588.setRightExactPuzzleID(840606173);
        p399365588.getDisplayImage().loadImageFromResource(c, R.drawable.p399365588h);
        p399365588.setExactRow(1);
        p399365588.setExactColumn(3);
        p399365588.getSize().reset(126.6667f, 126.6667f);
        p399365588.getPositionOffset().reset(-50.0f, -76.66666f);
        p399365588.setIsUseAbsolutePosition(true);
        p399365588.setIsUseAbsoluteSize(true);
        p399365588.getImage().setIsUseAbsolutePosition(true);
        p399365588.getImage().setIsUseAbsoluteSize(true);
        p399365588.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p399365588.resetPosition();
        getSpiritList().add(p399365588);
    }

    /* access modifiers changed from: package-private */
    public void c1571673992(Context c) {
        Puzzle p1571673992 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1571673992(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1571673992(editor, this);
            }
        };
        p1571673992.setID(1571673992);
        p1571673992.setName("1571673992");
        p1571673992.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1571673992);
        this.Desktop.RandomlyPlaced(p1571673992);
        p1571673992.setTopEdgeType(EdgeType.Convex);
        p1571673992.setBottomEdgeType(EdgeType.Concave);
        p1571673992.setLeftEdgeType(EdgeType.Concave);
        p1571673992.setRightEdgeType(EdgeType.Convex);
        p1571673992.setTopExactPuzzleID(399365588);
        p1571673992.setBottomExactPuzzleID(11303032);
        p1571673992.setLeftExactPuzzleID(2017758669);
        p1571673992.setRightExactPuzzleID(170824034);
        p1571673992.getDisplayImage().loadImageFromResource(c, R.drawable.p1571673992h);
        p1571673992.setExactRow(2);
        p1571673992.setExactColumn(3);
        p1571673992.getSize().reset(126.6667f, 126.6667f);
        p1571673992.getPositionOffset().reset(-50.0f, -76.66666f);
        p1571673992.setIsUseAbsolutePosition(true);
        p1571673992.setIsUseAbsoluteSize(true);
        p1571673992.getImage().setIsUseAbsolutePosition(true);
        p1571673992.getImage().setIsUseAbsoluteSize(true);
        p1571673992.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1571673992.resetPosition();
        getSpiritList().add(p1571673992);
    }

    /* access modifiers changed from: package-private */
    public void c11303032(Context c) {
        Puzzle p11303032 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load11303032(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save11303032(editor, this);
            }
        };
        p11303032.setID(11303032);
        p11303032.setName("11303032");
        p11303032.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p11303032);
        this.Desktop.RandomlyPlaced(p11303032);
        p11303032.setTopEdgeType(EdgeType.Convex);
        p11303032.setBottomEdgeType(EdgeType.Convex);
        p11303032.setLeftEdgeType(EdgeType.Convex);
        p11303032.setRightEdgeType(EdgeType.Convex);
        p11303032.setTopExactPuzzleID(1571673992);
        p11303032.setBottomExactPuzzleID(519585287);
        p11303032.setLeftExactPuzzleID(1417286977);
        p11303032.setRightExactPuzzleID(1483277057);
        p11303032.getDisplayImage().loadImageFromResource(c, R.drawable.p11303032h);
        p11303032.setExactRow(3);
        p11303032.setExactColumn(3);
        p11303032.getSize().reset(153.3333f, 153.3333f);
        p11303032.getPositionOffset().reset(-76.66666f, -76.66666f);
        p11303032.setIsUseAbsolutePosition(true);
        p11303032.setIsUseAbsoluteSize(true);
        p11303032.getImage().setIsUseAbsolutePosition(true);
        p11303032.getImage().setIsUseAbsoluteSize(true);
        p11303032.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p11303032.resetPosition();
        getSpiritList().add(p11303032);
    }

    /* access modifiers changed from: package-private */
    public void c519585287(Context c) {
        Puzzle p519585287 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load519585287(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save519585287(editor, this);
            }
        };
        p519585287.setID(519585287);
        p519585287.setName("519585287");
        p519585287.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p519585287);
        this.Desktop.RandomlyPlaced(p519585287);
        p519585287.setTopEdgeType(EdgeType.Concave);
        p519585287.setBottomEdgeType(EdgeType.Flat);
        p519585287.setLeftEdgeType(EdgeType.Concave);
        p519585287.setRightEdgeType(EdgeType.Convex);
        p519585287.setTopExactPuzzleID(11303032);
        p519585287.setBottomExactPuzzleID(-1);
        p519585287.setLeftExactPuzzleID(1846395478);
        p519585287.setRightExactPuzzleID(652541131);
        p519585287.getDisplayImage().loadImageFromResource(c, R.drawable.p519585287h);
        p519585287.setExactRow(4);
        p519585287.setExactColumn(3);
        p519585287.getSize().reset(126.6667f, 100.0f);
        p519585287.getPositionOffset().reset(-50.0f, -50.0f);
        p519585287.setIsUseAbsolutePosition(true);
        p519585287.setIsUseAbsoluteSize(true);
        p519585287.getImage().setIsUseAbsolutePosition(true);
        p519585287.getImage().setIsUseAbsoluteSize(true);
        p519585287.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p519585287.resetPosition();
        getSpiritList().add(p519585287);
    }

    /* access modifiers changed from: package-private */
    public void c518215134(Context c) {
        Puzzle p518215134 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load518215134(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save518215134(editor, this);
            }
        };
        p518215134.setID(518215134);
        p518215134.setName("518215134");
        p518215134.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p518215134);
        this.Desktop.RandomlyPlaced(p518215134);
        p518215134.setTopEdgeType(EdgeType.Flat);
        p518215134.setBottomEdgeType(EdgeType.Concave);
        p518215134.setLeftEdgeType(EdgeType.Concave);
        p518215134.setRightEdgeType(EdgeType.Concave);
        p518215134.setTopExactPuzzleID(-1);
        p518215134.setBottomExactPuzzleID(840606173);
        p518215134.setLeftExactPuzzleID(144606166);
        p518215134.setRightExactPuzzleID(369000380);
        p518215134.getDisplayImage().loadImageFromResource(c, R.drawable.p518215134h);
        p518215134.setExactRow(0);
        p518215134.setExactColumn(4);
        p518215134.getSize().reset(100.0f, 100.0f);
        p518215134.getPositionOffset().reset(-50.0f, -50.0f);
        p518215134.setIsUseAbsolutePosition(true);
        p518215134.setIsUseAbsoluteSize(true);
        p518215134.getImage().setIsUseAbsolutePosition(true);
        p518215134.getImage().setIsUseAbsoluteSize(true);
        p518215134.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p518215134.resetPosition();
        getSpiritList().add(p518215134);
    }

    /* access modifiers changed from: package-private */
    public void c840606173(Context c) {
        Puzzle p840606173 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load840606173(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save840606173(editor, this);
            }
        };
        p840606173.setID(840606173);
        p840606173.setName("840606173");
        p840606173.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p840606173);
        this.Desktop.RandomlyPlaced(p840606173);
        p840606173.setTopEdgeType(EdgeType.Convex);
        p840606173.setBottomEdgeType(EdgeType.Convex);
        p840606173.setLeftEdgeType(EdgeType.Concave);
        p840606173.setRightEdgeType(EdgeType.Concave);
        p840606173.setTopExactPuzzleID(518215134);
        p840606173.setBottomExactPuzzleID(170824034);
        p840606173.setLeftExactPuzzleID(399365588);
        p840606173.setRightExactPuzzleID(1668472540);
        p840606173.getDisplayImage().loadImageFromResource(c, R.drawable.p840606173h);
        p840606173.setExactRow(1);
        p840606173.setExactColumn(4);
        p840606173.getSize().reset(100.0f, 153.3333f);
        p840606173.getPositionOffset().reset(-50.0f, -76.66666f);
        p840606173.setIsUseAbsolutePosition(true);
        p840606173.setIsUseAbsoluteSize(true);
        p840606173.getImage().setIsUseAbsolutePosition(true);
        p840606173.getImage().setIsUseAbsoluteSize(true);
        p840606173.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p840606173.resetPosition();
        getSpiritList().add(p840606173);
    }

    /* access modifiers changed from: package-private */
    public void c170824034(Context c) {
        Puzzle p170824034 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load170824034(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save170824034(editor, this);
            }
        };
        p170824034.setID(170824034);
        p170824034.setName("170824034");
        p170824034.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p170824034);
        this.Desktop.RandomlyPlaced(p170824034);
        p170824034.setTopEdgeType(EdgeType.Concave);
        p170824034.setBottomEdgeType(EdgeType.Concave);
        p170824034.setLeftEdgeType(EdgeType.Concave);
        p170824034.setRightEdgeType(EdgeType.Concave);
        p170824034.setTopExactPuzzleID(840606173);
        p170824034.setBottomExactPuzzleID(1483277057);
        p170824034.setLeftExactPuzzleID(1571673992);
        p170824034.setRightExactPuzzleID(2108886431);
        p170824034.getDisplayImage().loadImageFromResource(c, R.drawable.p170824034h);
        p170824034.setExactRow(2);
        p170824034.setExactColumn(4);
        p170824034.getSize().reset(100.0f, 100.0f);
        p170824034.getPositionOffset().reset(-50.0f, -50.0f);
        p170824034.setIsUseAbsolutePosition(true);
        p170824034.setIsUseAbsoluteSize(true);
        p170824034.getImage().setIsUseAbsolutePosition(true);
        p170824034.getImage().setIsUseAbsoluteSize(true);
        p170824034.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p170824034.resetPosition();
        getSpiritList().add(p170824034);
    }

    /* access modifiers changed from: package-private */
    public void c1483277057(Context c) {
        Puzzle p1483277057 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1483277057(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1483277057(editor, this);
            }
        };
        p1483277057.setID(1483277057);
        p1483277057.setName("1483277057");
        p1483277057.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1483277057);
        this.Desktop.RandomlyPlaced(p1483277057);
        p1483277057.setTopEdgeType(EdgeType.Convex);
        p1483277057.setBottomEdgeType(EdgeType.Concave);
        p1483277057.setLeftEdgeType(EdgeType.Concave);
        p1483277057.setRightEdgeType(EdgeType.Concave);
        p1483277057.setTopExactPuzzleID(170824034);
        p1483277057.setBottomExactPuzzleID(652541131);
        p1483277057.setLeftExactPuzzleID(11303032);
        p1483277057.setRightExactPuzzleID(288563999);
        p1483277057.getDisplayImage().loadImageFromResource(c, R.drawable.p1483277057h);
        p1483277057.setExactRow(3);
        p1483277057.setExactColumn(4);
        p1483277057.getSize().reset(100.0f, 126.6667f);
        p1483277057.getPositionOffset().reset(-50.0f, -76.66666f);
        p1483277057.setIsUseAbsolutePosition(true);
        p1483277057.setIsUseAbsoluteSize(true);
        p1483277057.getImage().setIsUseAbsolutePosition(true);
        p1483277057.getImage().setIsUseAbsoluteSize(true);
        p1483277057.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1483277057.resetPosition();
        getSpiritList().add(p1483277057);
    }

    /* access modifiers changed from: package-private */
    public void c652541131(Context c) {
        Puzzle p652541131 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load652541131(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save652541131(editor, this);
            }
        };
        p652541131.setID(652541131);
        p652541131.setName("652541131");
        p652541131.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p652541131);
        this.Desktop.RandomlyPlaced(p652541131);
        p652541131.setTopEdgeType(EdgeType.Convex);
        p652541131.setBottomEdgeType(EdgeType.Flat);
        p652541131.setLeftEdgeType(EdgeType.Concave);
        p652541131.setRightEdgeType(EdgeType.Convex);
        p652541131.setTopExactPuzzleID(1483277057);
        p652541131.setBottomExactPuzzleID(-1);
        p652541131.setLeftExactPuzzleID(519585287);
        p652541131.setRightExactPuzzleID(1413505620);
        p652541131.getDisplayImage().loadImageFromResource(c, R.drawable.p652541131h);
        p652541131.setExactRow(4);
        p652541131.setExactColumn(4);
        p652541131.getSize().reset(126.6667f, 126.6667f);
        p652541131.getPositionOffset().reset(-50.0f, -76.66666f);
        p652541131.setIsUseAbsolutePosition(true);
        p652541131.setIsUseAbsoluteSize(true);
        p652541131.getImage().setIsUseAbsolutePosition(true);
        p652541131.getImage().setIsUseAbsoluteSize(true);
        p652541131.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p652541131.resetPosition();
        getSpiritList().add(p652541131);
    }

    /* access modifiers changed from: package-private */
    public void c369000380(Context c) {
        Puzzle p369000380 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load369000380(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save369000380(editor, this);
            }
        };
        p369000380.setID(369000380);
        p369000380.setName("369000380");
        p369000380.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p369000380);
        this.Desktop.RandomlyPlaced(p369000380);
        p369000380.setTopEdgeType(EdgeType.Flat);
        p369000380.setBottomEdgeType(EdgeType.Concave);
        p369000380.setLeftEdgeType(EdgeType.Convex);
        p369000380.setRightEdgeType(EdgeType.Convex);
        p369000380.setTopExactPuzzleID(-1);
        p369000380.setBottomExactPuzzleID(1668472540);
        p369000380.setLeftExactPuzzleID(518215134);
        p369000380.setRightExactPuzzleID(1369089708);
        p369000380.getDisplayImage().loadImageFromResource(c, R.drawable.p369000380h);
        p369000380.setExactRow(0);
        p369000380.setExactColumn(5);
        p369000380.getSize().reset(153.3333f, 100.0f);
        p369000380.getPositionOffset().reset(-76.66666f, -50.0f);
        p369000380.setIsUseAbsolutePosition(true);
        p369000380.setIsUseAbsoluteSize(true);
        p369000380.getImage().setIsUseAbsolutePosition(true);
        p369000380.getImage().setIsUseAbsoluteSize(true);
        p369000380.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p369000380.resetPosition();
        getSpiritList().add(p369000380);
    }

    /* access modifiers changed from: package-private */
    public void c1668472540(Context c) {
        Puzzle p1668472540 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1668472540(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1668472540(editor, this);
            }
        };
        p1668472540.setID(1668472540);
        p1668472540.setName("1668472540");
        p1668472540.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1668472540);
        this.Desktop.RandomlyPlaced(p1668472540);
        p1668472540.setTopEdgeType(EdgeType.Convex);
        p1668472540.setBottomEdgeType(EdgeType.Convex);
        p1668472540.setLeftEdgeType(EdgeType.Convex);
        p1668472540.setRightEdgeType(EdgeType.Concave);
        p1668472540.setTopExactPuzzleID(369000380);
        p1668472540.setBottomExactPuzzleID(2108886431);
        p1668472540.setLeftExactPuzzleID(840606173);
        p1668472540.setRightExactPuzzleID(2012198188);
        p1668472540.getDisplayImage().loadImageFromResource(c, R.drawable.p1668472540h);
        p1668472540.setExactRow(1);
        p1668472540.setExactColumn(5);
        p1668472540.getSize().reset(126.6667f, 153.3333f);
        p1668472540.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1668472540.setIsUseAbsolutePosition(true);
        p1668472540.setIsUseAbsoluteSize(true);
        p1668472540.getImage().setIsUseAbsolutePosition(true);
        p1668472540.getImage().setIsUseAbsoluteSize(true);
        p1668472540.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1668472540.resetPosition();
        getSpiritList().add(p1668472540);
    }

    /* access modifiers changed from: package-private */
    public void c2108886431(Context c) {
        Puzzle p2108886431 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2108886431(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2108886431(editor, this);
            }
        };
        p2108886431.setID(2108886431);
        p2108886431.setName("2108886431");
        p2108886431.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2108886431);
        this.Desktop.RandomlyPlaced(p2108886431);
        p2108886431.setTopEdgeType(EdgeType.Concave);
        p2108886431.setBottomEdgeType(EdgeType.Convex);
        p2108886431.setLeftEdgeType(EdgeType.Convex);
        p2108886431.setRightEdgeType(EdgeType.Concave);
        p2108886431.setTopExactPuzzleID(1668472540);
        p2108886431.setBottomExactPuzzleID(288563999);
        p2108886431.setLeftExactPuzzleID(170824034);
        p2108886431.setRightExactPuzzleID(1002936684);
        p2108886431.getDisplayImage().loadImageFromResource(c, R.drawable.p2108886431h);
        p2108886431.setExactRow(2);
        p2108886431.setExactColumn(5);
        p2108886431.getSize().reset(126.6667f, 126.6667f);
        p2108886431.getPositionOffset().reset(-76.66666f, -50.0f);
        p2108886431.setIsUseAbsolutePosition(true);
        p2108886431.setIsUseAbsoluteSize(true);
        p2108886431.getImage().setIsUseAbsolutePosition(true);
        p2108886431.getImage().setIsUseAbsoluteSize(true);
        p2108886431.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2108886431.resetPosition();
        getSpiritList().add(p2108886431);
    }

    /* access modifiers changed from: package-private */
    public void c288563999(Context c) {
        Puzzle p288563999 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load288563999(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save288563999(editor, this);
            }
        };
        p288563999.setID(288563999);
        p288563999.setName("288563999");
        p288563999.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p288563999);
        this.Desktop.RandomlyPlaced(p288563999);
        p288563999.setTopEdgeType(EdgeType.Concave);
        p288563999.setBottomEdgeType(EdgeType.Convex);
        p288563999.setLeftEdgeType(EdgeType.Convex);
        p288563999.setRightEdgeType(EdgeType.Concave);
        p288563999.setTopExactPuzzleID(2108886431);
        p288563999.setBottomExactPuzzleID(1413505620);
        p288563999.setLeftExactPuzzleID(1483277057);
        p288563999.setRightExactPuzzleID(2085467739);
        p288563999.getDisplayImage().loadImageFromResource(c, R.drawable.p288563999h);
        p288563999.setExactRow(3);
        p288563999.setExactColumn(5);
        p288563999.getSize().reset(126.6667f, 126.6667f);
        p288563999.getPositionOffset().reset(-76.66666f, -50.0f);
        p288563999.setIsUseAbsolutePosition(true);
        p288563999.setIsUseAbsoluteSize(true);
        p288563999.getImage().setIsUseAbsolutePosition(true);
        p288563999.getImage().setIsUseAbsoluteSize(true);
        p288563999.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p288563999.resetPosition();
        getSpiritList().add(p288563999);
    }

    /* access modifiers changed from: package-private */
    public void c1413505620(Context c) {
        Puzzle p1413505620 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1413505620(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1413505620(editor, this);
            }
        };
        p1413505620.setID(1413505620);
        p1413505620.setName("1413505620");
        p1413505620.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1413505620);
        this.Desktop.RandomlyPlaced(p1413505620);
        p1413505620.setTopEdgeType(EdgeType.Concave);
        p1413505620.setBottomEdgeType(EdgeType.Flat);
        p1413505620.setLeftEdgeType(EdgeType.Concave);
        p1413505620.setRightEdgeType(EdgeType.Concave);
        p1413505620.setTopExactPuzzleID(288563999);
        p1413505620.setBottomExactPuzzleID(-1);
        p1413505620.setLeftExactPuzzleID(652541131);
        p1413505620.setRightExactPuzzleID(575330746);
        p1413505620.getDisplayImage().loadImageFromResource(c, R.drawable.p1413505620h);
        p1413505620.setExactRow(4);
        p1413505620.setExactColumn(5);
        p1413505620.getSize().reset(100.0f, 100.0f);
        p1413505620.getPositionOffset().reset(-50.0f, -50.0f);
        p1413505620.setIsUseAbsolutePosition(true);
        p1413505620.setIsUseAbsoluteSize(true);
        p1413505620.getImage().setIsUseAbsolutePosition(true);
        p1413505620.getImage().setIsUseAbsoluteSize(true);
        p1413505620.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1413505620.resetPosition();
        getSpiritList().add(p1413505620);
    }

    /* access modifiers changed from: package-private */
    public void c1369089708(Context c) {
        Puzzle p1369089708 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1369089708(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1369089708(editor, this);
            }
        };
        p1369089708.setID(1369089708);
        p1369089708.setName("1369089708");
        p1369089708.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1369089708);
        this.Desktop.RandomlyPlaced(p1369089708);
        p1369089708.setTopEdgeType(EdgeType.Flat);
        p1369089708.setBottomEdgeType(EdgeType.Concave);
        p1369089708.setLeftEdgeType(EdgeType.Concave);
        p1369089708.setRightEdgeType(EdgeType.Convex);
        p1369089708.setTopExactPuzzleID(-1);
        p1369089708.setBottomExactPuzzleID(2012198188);
        p1369089708.setLeftExactPuzzleID(369000380);
        p1369089708.setRightExactPuzzleID(401861086);
        p1369089708.getDisplayImage().loadImageFromResource(c, R.drawable.p1369089708h);
        p1369089708.setExactRow(0);
        p1369089708.setExactColumn(6);
        p1369089708.getSize().reset(126.6667f, 100.0f);
        p1369089708.getPositionOffset().reset(-50.0f, -50.0f);
        p1369089708.setIsUseAbsolutePosition(true);
        p1369089708.setIsUseAbsoluteSize(true);
        p1369089708.getImage().setIsUseAbsolutePosition(true);
        p1369089708.getImage().setIsUseAbsoluteSize(true);
        p1369089708.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1369089708.resetPosition();
        getSpiritList().add(p1369089708);
    }

    /* access modifiers changed from: package-private */
    public void c2012198188(Context c) {
        Puzzle p2012198188 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2012198188(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2012198188(editor, this);
            }
        };
        p2012198188.setID(2012198188);
        p2012198188.setName("2012198188");
        p2012198188.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2012198188);
        this.Desktop.RandomlyPlaced(p2012198188);
        p2012198188.setTopEdgeType(EdgeType.Convex);
        p2012198188.setBottomEdgeType(EdgeType.Convex);
        p2012198188.setLeftEdgeType(EdgeType.Convex);
        p2012198188.setRightEdgeType(EdgeType.Concave);
        p2012198188.setTopExactPuzzleID(1369089708);
        p2012198188.setBottomExactPuzzleID(1002936684);
        p2012198188.setLeftExactPuzzleID(1668472540);
        p2012198188.setRightExactPuzzleID(211953248);
        p2012198188.getDisplayImage().loadImageFromResource(c, R.drawable.p2012198188h);
        p2012198188.setExactRow(1);
        p2012198188.setExactColumn(6);
        p2012198188.getSize().reset(126.6667f, 153.3333f);
        p2012198188.getPositionOffset().reset(-76.66666f, -76.66666f);
        p2012198188.setIsUseAbsolutePosition(true);
        p2012198188.setIsUseAbsoluteSize(true);
        p2012198188.getImage().setIsUseAbsolutePosition(true);
        p2012198188.getImage().setIsUseAbsoluteSize(true);
        p2012198188.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2012198188.resetPosition();
        getSpiritList().add(p2012198188);
    }

    /* access modifiers changed from: package-private */
    public void c1002936684(Context c) {
        Puzzle p1002936684 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1002936684(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1002936684(editor, this);
            }
        };
        p1002936684.setID(1002936684);
        p1002936684.setName("1002936684");
        p1002936684.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1002936684);
        this.Desktop.RandomlyPlaced(p1002936684);
        p1002936684.setTopEdgeType(EdgeType.Concave);
        p1002936684.setBottomEdgeType(EdgeType.Convex);
        p1002936684.setLeftEdgeType(EdgeType.Convex);
        p1002936684.setRightEdgeType(EdgeType.Concave);
        p1002936684.setTopExactPuzzleID(2012198188);
        p1002936684.setBottomExactPuzzleID(2085467739);
        p1002936684.setLeftExactPuzzleID(2108886431);
        p1002936684.setRightExactPuzzleID(9616941);
        p1002936684.getDisplayImage().loadImageFromResource(c, R.drawable.p1002936684h);
        p1002936684.setExactRow(2);
        p1002936684.setExactColumn(6);
        p1002936684.getSize().reset(126.6667f, 126.6667f);
        p1002936684.getPositionOffset().reset(-76.66666f, -50.0f);
        p1002936684.setIsUseAbsolutePosition(true);
        p1002936684.setIsUseAbsoluteSize(true);
        p1002936684.getImage().setIsUseAbsolutePosition(true);
        p1002936684.getImage().setIsUseAbsoluteSize(true);
        p1002936684.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1002936684.resetPosition();
        getSpiritList().add(p1002936684);
    }

    /* access modifiers changed from: package-private */
    public void c2085467739(Context c) {
        Puzzle p2085467739 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2085467739(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2085467739(editor, this);
            }
        };
        p2085467739.setID(2085467739);
        p2085467739.setName("2085467739");
        p2085467739.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2085467739);
        this.Desktop.RandomlyPlaced(p2085467739);
        p2085467739.setTopEdgeType(EdgeType.Concave);
        p2085467739.setBottomEdgeType(EdgeType.Convex);
        p2085467739.setLeftEdgeType(EdgeType.Convex);
        p2085467739.setRightEdgeType(EdgeType.Convex);
        p2085467739.setTopExactPuzzleID(1002936684);
        p2085467739.setBottomExactPuzzleID(575330746);
        p2085467739.setLeftExactPuzzleID(288563999);
        p2085467739.setRightExactPuzzleID(1875148103);
        p2085467739.getDisplayImage().loadImageFromResource(c, R.drawable.p2085467739h);
        p2085467739.setExactRow(3);
        p2085467739.setExactColumn(6);
        p2085467739.getSize().reset(153.3333f, 126.6667f);
        p2085467739.getPositionOffset().reset(-76.66666f, -50.0f);
        p2085467739.setIsUseAbsolutePosition(true);
        p2085467739.setIsUseAbsoluteSize(true);
        p2085467739.getImage().setIsUseAbsolutePosition(true);
        p2085467739.getImage().setIsUseAbsoluteSize(true);
        p2085467739.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2085467739.resetPosition();
        getSpiritList().add(p2085467739);
    }

    /* access modifiers changed from: package-private */
    public void c575330746(Context c) {
        Puzzle p575330746 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load575330746(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save575330746(editor, this);
            }
        };
        p575330746.setID(575330746);
        p575330746.setName("575330746");
        p575330746.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p575330746);
        this.Desktop.RandomlyPlaced(p575330746);
        p575330746.setTopEdgeType(EdgeType.Concave);
        p575330746.setBottomEdgeType(EdgeType.Flat);
        p575330746.setLeftEdgeType(EdgeType.Convex);
        p575330746.setRightEdgeType(EdgeType.Convex);
        p575330746.setTopExactPuzzleID(2085467739);
        p575330746.setBottomExactPuzzleID(-1);
        p575330746.setLeftExactPuzzleID(1413505620);
        p575330746.setRightExactPuzzleID(294717271);
        p575330746.getDisplayImage().loadImageFromResource(c, R.drawable.p575330746h);
        p575330746.setExactRow(4);
        p575330746.setExactColumn(6);
        p575330746.getSize().reset(153.3333f, 100.0f);
        p575330746.getPositionOffset().reset(-76.66666f, -50.0f);
        p575330746.setIsUseAbsolutePosition(true);
        p575330746.setIsUseAbsoluteSize(true);
        p575330746.getImage().setIsUseAbsolutePosition(true);
        p575330746.getImage().setIsUseAbsoluteSize(true);
        p575330746.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p575330746.resetPosition();
        getSpiritList().add(p575330746);
    }

    /* access modifiers changed from: package-private */
    public void c401861086(Context c) {
        Puzzle p401861086 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load401861086(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save401861086(editor, this);
            }
        };
        p401861086.setID(401861086);
        p401861086.setName("401861086");
        p401861086.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p401861086);
        this.Desktop.RandomlyPlaced(p401861086);
        p401861086.setTopEdgeType(EdgeType.Flat);
        p401861086.setBottomEdgeType(EdgeType.Concave);
        p401861086.setLeftEdgeType(EdgeType.Concave);
        p401861086.setRightEdgeType(EdgeType.Flat);
        p401861086.setTopExactPuzzleID(-1);
        p401861086.setBottomExactPuzzleID(211953248);
        p401861086.setLeftExactPuzzleID(1369089708);
        p401861086.setRightExactPuzzleID(-1);
        p401861086.getDisplayImage().loadImageFromResource(c, R.drawable.p401861086h);
        p401861086.setExactRow(0);
        p401861086.setExactColumn(7);
        p401861086.getSize().reset(100.0f, 100.0f);
        p401861086.getPositionOffset().reset(-50.0f, -50.0f);
        p401861086.setIsUseAbsolutePosition(true);
        p401861086.setIsUseAbsoluteSize(true);
        p401861086.getImage().setIsUseAbsolutePosition(true);
        p401861086.getImage().setIsUseAbsoluteSize(true);
        p401861086.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p401861086.resetPosition();
        getSpiritList().add(p401861086);
    }

    /* access modifiers changed from: package-private */
    public void c211953248(Context c) {
        Puzzle p211953248 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load211953248(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save211953248(editor, this);
            }
        };
        p211953248.setID(211953248);
        p211953248.setName("211953248");
        p211953248.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p211953248);
        this.Desktop.RandomlyPlaced(p211953248);
        p211953248.setTopEdgeType(EdgeType.Convex);
        p211953248.setBottomEdgeType(EdgeType.Convex);
        p211953248.setLeftEdgeType(EdgeType.Convex);
        p211953248.setRightEdgeType(EdgeType.Flat);
        p211953248.setTopExactPuzzleID(401861086);
        p211953248.setBottomExactPuzzleID(9616941);
        p211953248.setLeftExactPuzzleID(2012198188);
        p211953248.setRightExactPuzzleID(-1);
        p211953248.getDisplayImage().loadImageFromResource(c, R.drawable.p211953248h);
        p211953248.setExactRow(1);
        p211953248.setExactColumn(7);
        p211953248.getSize().reset(126.6667f, 153.3333f);
        p211953248.getPositionOffset().reset(-76.66666f, -76.66666f);
        p211953248.setIsUseAbsolutePosition(true);
        p211953248.setIsUseAbsoluteSize(true);
        p211953248.getImage().setIsUseAbsolutePosition(true);
        p211953248.getImage().setIsUseAbsoluteSize(true);
        p211953248.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p211953248.resetPosition();
        getSpiritList().add(p211953248);
    }

    /* access modifiers changed from: package-private */
    public void c9616941(Context c) {
        Puzzle p9616941 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load9616941(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save9616941(editor, this);
            }
        };
        p9616941.setID(9616941);
        p9616941.setName("9616941");
        p9616941.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p9616941);
        this.Desktop.RandomlyPlaced(p9616941);
        p9616941.setTopEdgeType(EdgeType.Concave);
        p9616941.setBottomEdgeType(EdgeType.Concave);
        p9616941.setLeftEdgeType(EdgeType.Convex);
        p9616941.setRightEdgeType(EdgeType.Flat);
        p9616941.setTopExactPuzzleID(211953248);
        p9616941.setBottomExactPuzzleID(1875148103);
        p9616941.setLeftExactPuzzleID(1002936684);
        p9616941.setRightExactPuzzleID(-1);
        p9616941.getDisplayImage().loadImageFromResource(c, R.drawable.p9616941h);
        p9616941.setExactRow(2);
        p9616941.setExactColumn(7);
        p9616941.getSize().reset(126.6667f, 100.0f);
        p9616941.getPositionOffset().reset(-76.66666f, -50.0f);
        p9616941.setIsUseAbsolutePosition(true);
        p9616941.setIsUseAbsoluteSize(true);
        p9616941.getImage().setIsUseAbsolutePosition(true);
        p9616941.getImage().setIsUseAbsoluteSize(true);
        p9616941.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p9616941.resetPosition();
        getSpiritList().add(p9616941);
    }

    /* access modifiers changed from: package-private */
    public void c1875148103(Context c) {
        Puzzle p1875148103 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1875148103(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1875148103(editor, this);
            }
        };
        p1875148103.setID(1875148103);
        p1875148103.setName("1875148103");
        p1875148103.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1875148103);
        this.Desktop.RandomlyPlaced(p1875148103);
        p1875148103.setTopEdgeType(EdgeType.Convex);
        p1875148103.setBottomEdgeType(EdgeType.Concave);
        p1875148103.setLeftEdgeType(EdgeType.Concave);
        p1875148103.setRightEdgeType(EdgeType.Flat);
        p1875148103.setTopExactPuzzleID(9616941);
        p1875148103.setBottomExactPuzzleID(294717271);
        p1875148103.setLeftExactPuzzleID(2085467739);
        p1875148103.setRightExactPuzzleID(-1);
        p1875148103.getDisplayImage().loadImageFromResource(c, R.drawable.p1875148103h);
        p1875148103.setExactRow(3);
        p1875148103.setExactColumn(7);
        p1875148103.getSize().reset(100.0f, 126.6667f);
        p1875148103.getPositionOffset().reset(-50.0f, -76.66666f);
        p1875148103.setIsUseAbsolutePosition(true);
        p1875148103.setIsUseAbsoluteSize(true);
        p1875148103.getImage().setIsUseAbsolutePosition(true);
        p1875148103.getImage().setIsUseAbsoluteSize(true);
        p1875148103.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1875148103.resetPosition();
        getSpiritList().add(p1875148103);
    }

    /* access modifiers changed from: package-private */
    public void c294717271(Context c) {
        Puzzle p294717271 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load294717271(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save294717271(editor, this);
            }
        };
        p294717271.setID(294717271);
        p294717271.setName("294717271");
        p294717271.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p294717271);
        this.Desktop.RandomlyPlaced(p294717271);
        p294717271.setTopEdgeType(EdgeType.Convex);
        p294717271.setBottomEdgeType(EdgeType.Flat);
        p294717271.setLeftEdgeType(EdgeType.Concave);
        p294717271.setRightEdgeType(EdgeType.Flat);
        p294717271.setTopExactPuzzleID(1875148103);
        p294717271.setBottomExactPuzzleID(-1);
        p294717271.setLeftExactPuzzleID(575330746);
        p294717271.setRightExactPuzzleID(-1);
        p294717271.getDisplayImage().loadImageFromResource(c, R.drawable.p294717271h);
        p294717271.setExactRow(4);
        p294717271.setExactColumn(7);
        p294717271.getSize().reset(100.0f, 126.6667f);
        p294717271.getPositionOffset().reset(-50.0f, -76.66666f);
        p294717271.setIsUseAbsolutePosition(true);
        p294717271.setIsUseAbsoluteSize(true);
        p294717271.getImage().setIsUseAbsolutePosition(true);
        p294717271.getImage().setIsUseAbsoluteSize(true);
        p294717271.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p294717271.resetPosition();
        getSpiritList().add(p294717271);
    }
}
