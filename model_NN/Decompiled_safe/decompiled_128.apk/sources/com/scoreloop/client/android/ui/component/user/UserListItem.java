package com.scoreloop.client.android.ui.component.user;

import android.graphics.drawable.Drawable;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.StandardListItem;
import com.skyd.bestpuzzle.n1664.R;

public class UserListItem extends StandardListItem<User> {
    private final boolean _playsSessionGame;

    public UserListItem(ComponentActivity context, Drawable drawable, User user, boolean playsSessionGame) {
        super(context, drawable, user.getDisplayName(), null, user);
        this._playsSessionGame = playsSessionGame;
    }

    /* access modifiers changed from: protected */
    public String getImageUrl() {
        return ((User) getTarget()).getImageUrl();
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.sl_list_item_icon_title;
    }

    public int getType() {
        return 23;
    }

    public Drawable getDrawable() {
        return getContext().getResources().getDrawable(R.drawable.sl_icon_user);
    }

    public boolean playsSessionGame() {
        return this._playsSessionGame;
    }
}
