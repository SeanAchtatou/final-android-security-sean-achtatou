package com.scoreloop.client.android.ui.framework;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.skyd.bestpuzzle.n1664.R;

public class TextButtonDialog extends BaseDialog {
    public TextButtonDialog(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public int getContentViewLayoutId() {
        return R.layout.sl_dialog_text_button;
    }

    public void onClick(View v) {
        if (this._listener != null && v.getId() == R.id.sl_button_ok) {
            this._listener.onAction(this, 0);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((Button) findViewById(R.id.sl_button_ok)).setOnClickListener(this);
    }
}
