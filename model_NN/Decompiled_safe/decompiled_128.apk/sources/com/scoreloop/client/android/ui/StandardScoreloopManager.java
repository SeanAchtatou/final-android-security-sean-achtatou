package com.scoreloop.client.android.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.util.Log;
import com.scoreloop.client.android.core.controller.ChallengeController;
import com.scoreloop.client.android.core.controller.ChallengeControllerObserver;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.controller.ScoreController;
import com.scoreloop.client.android.core.model.Achievement;
import com.scoreloop.client.android.core.model.AwardList;
import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.core.model.Client;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.SearchList;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.achievement.AchievementHeaderActivity;
import com.scoreloop.client.android.ui.component.achievement.AchievementListActivity;
import com.scoreloop.client.android.ui.component.achievement.AchievementsEngine;
import com.scoreloop.client.android.ui.component.agent.NewsAgent;
import com.scoreloop.client.android.ui.component.agent.NumberAchievementsAgent;
import com.scoreloop.client.android.ui.component.agent.UserAgent;
import com.scoreloop.client.android.ui.component.agent.UserBuddiesAgent;
import com.scoreloop.client.android.ui.component.agent.UserDetailsAgent;
import com.scoreloop.client.android.ui.component.base.Configuration;
import com.scoreloop.client.android.ui.component.base.Constant;
import com.scoreloop.client.android.ui.component.base.Factory;
import com.scoreloop.client.android.ui.component.base.Manager;
import com.scoreloop.client.android.ui.component.base.Tracker;
import com.scoreloop.client.android.ui.component.challenge.ChallengeAcceptListActivity;
import com.scoreloop.client.android.ui.component.challenge.ChallengeCreateListActivity;
import com.scoreloop.client.android.ui.component.challenge.ChallengeHeaderActivity;
import com.scoreloop.client.android.ui.component.challenge.ChallengeListActivity;
import com.scoreloop.client.android.ui.component.challenge.ChallengePaymentActivity;
import com.scoreloop.client.android.ui.component.entry.EntryListActivity;
import com.scoreloop.client.android.ui.component.game.GameDetailHeaderActivity;
import com.scoreloop.client.android.ui.component.game.GameDetailListActivity;
import com.scoreloop.client.android.ui.component.game.GameListActivity;
import com.scoreloop.client.android.ui.component.market.MarketHeaderActivity;
import com.scoreloop.client.android.ui.component.market.MarketListActivity;
import com.scoreloop.client.android.ui.component.news.NewsHeaderActivity;
import com.scoreloop.client.android.ui.component.news.NewsListActivity;
import com.scoreloop.client.android.ui.component.profile.ProfileSettingsListActivity;
import com.scoreloop.client.android.ui.component.profile.ProfileSettingsPictureListActivity;
import com.scoreloop.client.android.ui.component.score.ScoreHeaderActivity;
import com.scoreloop.client.android.ui.component.score.ScoreListActivity;
import com.scoreloop.client.android.ui.component.user.UserAddBuddyListActivity;
import com.scoreloop.client.android.ui.component.user.UserDetailListActivity;
import com.scoreloop.client.android.ui.component.user.UserHeaderActivity;
import com.scoreloop.client.android.ui.component.user.UserListActivity;
import com.scoreloop.client.android.ui.framework.BaseActivity;
import com.scoreloop.client.android.ui.framework.ScreenDescription;
import com.scoreloop.client.android.ui.framework.ScreenManager;
import com.scoreloop.client.android.ui.framework.ScreenManagerSingleton;
import com.scoreloop.client.android.ui.framework.StandardScreenManager;
import com.scoreloop.client.android.ui.framework.ValueStore;
import com.skyd.bestpuzzle.n1664.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class StandardScoreloopManager implements ScoreloopManager, Manager, Factory, ScreenDescription.ShortcutObserver, ScreenManager.Delegate, ValueStore.ValueSourceFactory, ChallengeControllerObserver, Runnable, Tracker {
    private static final String PREFERENCES_ENTRY_USER_IMAGE_URL = "userImageUrl";
    private static final String PREFERENCES_ENTRY_USER_NAME = "userName";
    private static final String PREFERENCES_NAME = "com.scoreloop.ui.login";
    private AchievementsEngine _achievementsEngine;
    private ValueStore _cachedUserValueStore;
    private ChallengeController _challengeController;
    private boolean _challengeOngoing;
    private final Client _client;
    /* access modifiers changed from: private */
    public final Configuration _configuration;
    private final Context _context;
    private final Handler _handler = new Handler();
    private Challenge _lastChallenge;
    private Score _lastScore;
    private int _lastStatus;
    private OnCanStartGamePlayObserver _onCanStartGamePlayObserver;
    private OnScoreSubmitObserver _onScoreSubmitObserver;
    private OnStartGamePlayRequestObserver _onStartGamePlayRequestObserver;
    private ScoreController _scoreController;
    private ValueStore _sessionGameValueStore;
    private ValueStore _sessionUserValueStore;

    class Checker {
        private static final String SCORELOOP_UI = "ScoreloopUI";
        private int _counter;
        private final Map<String, Object> _infos;
        private final String _kind;
        private final List<String> _missing = new ArrayList();
        private boolean _shouldBail;

        Checker(String kind, Map<String, Object> infos) {
            this._kind = kind;
            this._infos = infos;
        }

        /* access modifiers changed from: package-private */
        public void add(String name, Configuration.Feature feature) {
            if (StandardScoreloopManager.this._configuration.isFeatureEnabled(feature)) {
                add(name, new Object[0]);
            }
        }

        public void add(String name, Object... keyValuePairs) {
            Object actualValue;
            this._counter++;
            if (!this._infos.containsKey(name)) {
                this._missing.add(format(name, keyValuePairs));
            }
            for (int i = 0; i < keyValuePairs.length - 1; i += 2) {
                String key = (String) keyValuePairs[i];
                Object expectedValue = keyValuePairs[i + 1];
                Map<String, Object> details = (Map) this._infos.get(name);
                if (details == null || (actualValue = details.get(key)) == null || !actualValue.equals(expectedValue)) {
                    this._missing.add(format(name, keyValuePairs));
                    return;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void check() {
            if (this._shouldBail) {
                throw new VerifyException();
            }
        }

        private String format(String name, Object[] keyValuePairs) {
            StringBuilder buffer = new StringBuilder();
            buffer.append("android:name=\"");
            buffer.append(name);
            buffer.append("\"");
            for (int i = 0; i < keyValuePairs.length - 1; i += 2) {
                String key = (String) keyValuePairs[i];
                Object value = keyValuePairs[i + 1];
                buffer.append(" android:");
                buffer.append(key);
                buffer.append("=\"");
                if (key.equals("theme")) {
                    buffer.append(StandardScoreloopManager.this.getContext().getResources().getResourceName(((Integer) value).intValue()));
                } else {
                    buffer.append(value.toString());
                }
                buffer.append("\"");
            }
            return buffer.toString();
        }

        private void informDeveloper(String detail) {
            Log.e("ScoreloopUI", "=====================================================================================");
            Log.e("ScoreloopUI", "Manifest file verification error. Please resolve any issues first!");
            Log.e("ScoreloopUI", detail);
            for (String entry : this._missing) {
                Log.e("ScoreloopUI", "<" + this._kind + " " + entry + "/>");
            }
        }

        /* access modifiers changed from: package-private */
        public void reportOptional() {
            if (this._counter == this._missing.size()) {
                informDeveloper("At least one of following entries is mssing in your AndroidManifest.xml file:");
                this._shouldBail = true;
            }
        }

        /* access modifiers changed from: package-private */
        public void reportRequired() {
            if (this._missing.size() > 0) {
                informDeveloper("All the following entries are missing in your AndroidManifest.xml file:");
                this._shouldBail = true;
            }
        }
    }

    private static class VerifyException extends RuntimeException {
        private static final long serialVersionUID = 1;

        VerifyException() {
            super("Manifest Verification Error! See logcat output!");
        }
    }

    private static boolean containsKey(String[] keys, String key) {
        for (String string : keys) {
            if (string.equalsIgnoreCase(key)) {
                return true;
            }
        }
        return false;
    }

    static StandardScoreloopManager getFactory(ScoreloopManager scoreloopManager) {
        return (StandardScoreloopManager) scoreloopManager;
    }

    StandardScoreloopManager(Context context) {
        this._context = context;
        this._client = new Client(this._context, null);
        this._configuration = new Configuration(this._context, getSession());
        verifyManifest();
        ScreenManager screenManager = new StandardScreenManager();
        screenManager.setDelegate(this);
        ScreenManagerSingleton.init(screenManager);
        Constant.setup();
    }

    public void achieveAward(String awardId, boolean showToast, boolean submitNow) {
        Achievement achievement = getAchievement(awardId);
        if (!achievement.isAchieved()) {
            achievement.setAchieved();
            if (showToast) {
                showToastForAchievement(achievement);
            }
            if (submitNow) {
                submitAchievements(null);
            }
        }
    }

    public boolean canStartGamePlay() {
        if (this._onCanStartGamePlayObserver != null) {
            return this._onCanStartGamePlayObserver.onCanStartGamePlay();
        }
        throw new IllegalStateException("trying to check if gameplay can be started, but the callback is not set - did you call ScoreloopManagerSingleton.get().setOnCanStartGamePlayObserver(...)?");
    }

    public void challengeControllerDidFailOnInsufficientBalance(ChallengeController challengeController) {
        if (challengeController == this._challengeController) {
            this._lastStatus = 4;
            this._lastChallenge = null;
            this._challengeOngoing = false;
            if (this._onScoreSubmitObserver != null) {
                this._onScoreSubmitObserver.onScoreSubmit(this._lastStatus, null);
            }
        }
    }

    public void challengeControllerDidFailToAcceptChallenge(ChallengeController challengeController) {
        requestControllerDidFail(challengeController, new RuntimeException("challengeControllerDidFailToAcceptChallenge"));
    }

    public void challengeControllerDidFailToRejectChallenge(ChallengeController challengeController) {
        requestControllerDidFail(challengeController, new RuntimeException("challengeControllerDidFailToRejectChallenge"));
    }

    private void checkHasAwards() {
        if (!this._configuration.isFeatureEnabled(Configuration.Feature.ACHIEVEMENT)) {
            throw new IllegalStateException("you have to set 'ui.feature.achievement = true' in the scoreloop.properties first!");
        }
    }

    private void checkHasLoadedAchievements() {
        if (!hasLoadedAchievements()) {
            throw new IllegalStateException("you have to load the achievements first!");
        }
    }

    public ScreenDescription createAchievementScreenDescription(User user) {
        ScreenDescription description = createScreenDescription(user, null, true);
        description.setHeaderDescription(new Intent(getContext(), AchievementHeaderActivity.class));
        description.setBodyDescription(new Intent(getContext(), AchievementListActivity.class));
        return description;
    }

    public ScreenDescription createChallengeAcceptScreenDescription(Challenge challenge) {
        ScreenDescription description = createScreenDescription(null, null, true);
        description.setHeaderDescription(new Intent(getContext(), ChallengeHeaderActivity.class));
        description.setBodyDescription(new Intent(getContext(), ChallengeAcceptListActivity.class)).getArguments().putValue(Constant.CHALLENGE, challenge);
        return description;
    }

    public ScreenDescription createChallengeCreateScreenDescription(User user, Integer mode) {
        ScreenDescription description = createScreenDescription(null, null, true);
        description.setHeaderDescription(new Intent(getContext(), ChallengeHeaderActivity.class));
        description.setBodyDescription(new Intent(getContext(), ChallengeCreateListActivity.class)).getArguments().putValue(Constant.CONTESTANT, user);
        return description;
    }

    public ScreenDescription createChallengePaymentScreenDescription() {
        ScreenDescription description = createScreenDescription(null, null, true);
        description.setHeaderDescription(new Intent(getContext(), ChallengeHeaderActivity.class)).getArguments().putValue(Constant.CHALLENGE_HEADER_MODE, 1);
        description.setBodyDescription(new Intent(getContext(), ChallengePaymentActivity.class));
        return description;
    }

    public ScreenDescription createChallengeScreenDescription(User user) {
        ScreenDescription description = createScreenDescription(user, null, true);
        description.setHeaderDescription(new Intent(getContext(), ChallengeHeaderActivity.class));
        description.setBodyDescription(new Intent(getContext(), ChallengeListActivity.class));
        return description;
    }

    public ScreenDescription createEntryScreenDescription() {
        ScreenDescription description = createScreenDescription(null, null, true);
        description.setHeaderDescription(new Intent(getContext(), UserHeaderActivity.class)).getArguments().putValue("mode", UserHeaderActivity.ControlMode.PROFILE);
        description.setBodyDescription(new Intent(getContext(), EntryListActivity.class));
        description.setShortcutSelectionId(R.string.sl_home);
        return description;
    }

    public ScreenDescription createGameDetailScreenDescription(Game game) {
        ScreenDescription description = createScreenDescription(null, game, false);
        description.setHeaderDescription(new Intent(getContext(), GameDetailHeaderActivity.class));
        description.setBodyDescription(new Intent(getContext(), GameDetailListActivity.class));
        return description;
    }

    public ScreenDescription createGameScreenDescription(User user, int mode) {
        ScreenDescription description = createScreenDescription(user, null, true);
        if (mode == 0) {
            description.setHeaderDescription(new Intent(getContext(), UserHeaderActivity.class)).getArguments().putValue("mode", UserHeaderActivity.ControlMode.BUDDY);
        } else {
            description.setHeaderDescription(new Intent(getContext(), MarketHeaderActivity.class));
        }
        description.setBodyDescription(new Intent(getContext(), GameListActivity.class)).getArguments().putValue("mode", Integer.valueOf(mode));
        return description;
    }

    public ScreenDescription createMarketScreenDescription(User user) {
        ScreenDescription description = createScreenDescription(user, null, false);
        description.setHeaderDescription(new Intent(getContext(), MarketHeaderActivity.class));
        description.setBodyDescription(new Intent(getContext(), MarketListActivity.class));
        description.setShortcutSelectionId(R.string.sl_market);
        return description;
    }

    public ScreenDescription createNewsScreenDescription() {
        ScreenDescription description = createScreenDescription(null, null, true);
        description.setHeaderDescription(new Intent(getContext(), NewsHeaderActivity.class));
        description.setBodyDescription(new Intent(getContext(), NewsListActivity.class));
        return description;
    }

    public ScreenDescription createProfileSettingsPictureScreenDescription(User user) {
        ScreenDescription description = createScreenDescription(user, null, true);
        description.setHeaderDescription(new Intent(getContext(), UserHeaderActivity.class));
        description.setBodyDescription(new Intent(getContext(), ProfileSettingsPictureListActivity.class));
        return description;
    }

    public ScreenDescription createProfileSettingsScreenDescription(User user) {
        ScreenDescription description = createScreenDescription(user, null, true);
        description.setHeaderDescription(new Intent(getContext(), UserHeaderActivity.class));
        description.setBodyDescription(new Intent(getContext(), ProfileSettingsListActivity.class));
        return description;
    }

    public ScreenDescription createScoreScreenDescription(Game game, Integer mode, Integer leaderboard) {
        ScreenDescription description = createScreenDescription(null, game, true);
        description.getScreenValues().putValue("mode", mode != null ? mode : ensureGame(game).getMinMode());
        description.setHeaderDescription(new Intent(getContext(), ScoreHeaderActivity.class));
        description.addBodyDescription(R.string.sl_global, new Intent(getContext(), ScoreListActivity.class)).getArguments().putValue(Constant.SEARCH_LIST, SearchList.getGlobalScoreSearchList());
        description.addBodyDescription(R.string.sl_friends, new Intent(getContext(), ScoreListActivity.class)).getArguments().putValue(Constant.SEARCH_LIST, SearchList.getBuddiesScoreSearchList());
        description.addBodyDescription(R.string.sl_twentyfour, new Intent(getContext(), ScoreListActivity.class)).getArguments().putValue(Constant.SEARCH_LIST, SearchList.getTwentyFourHourScoreSearchList());
        if (leaderboard != null) {
            description.setSelectedBodyIndex(leaderboard.intValue());
        }
        return description;
    }

    private ScreenDescription createScreenDescription(User user, Game game, boolean useCached) {
        ScreenDescription description = new ScreenDescription();
        ValueStore screenValues = description.getScreenValues();
        screenValues.putValue(Constant.USER_VALUES, getUserValues(ensureUser(user)));
        screenValues.putValue(Constant.GAME_VALUES, getGameValues(ensureGame(game)));
        screenValues.putValue(Constant.SESSION_USER_VALUES, getSessionUserValues());
        screenValues.putValue(Constant.SESSION_GAME_VALUES, getSessionGameValues());
        screenValues.putValue(Constant.MANAGER, this);
        screenValues.putValue(Constant.FACTORY, this);
        screenValues.putValue(Constant.TRACKER, this);
        screenValues.putValue(Constant.CONFIGURATION, this._configuration);
        description.addShortcutObserver(this);
        description.addShortcutDescription(R.string.sl_home, R.drawable.sl_shortcut_home_default, R.drawable.sl_shortcut_home_active);
        description.addShortcutDescription(R.string.sl_friends, R.drawable.sl_shortcut_friends_default, R.drawable.sl_shortcut_friends_active);
        description.addShortcutDescription(R.string.sl_market, R.drawable.sl_shortcut_market_default, R.drawable.sl_shortcut_market_active);
        return description;
    }

    public ScreenDescription createUserAddBuddyScreenDescription() {
        ScreenDescription description = createScreenDescription(null, null, true);
        description.setHeaderDescription(new Intent(getContext(), UserHeaderActivity.class)).getArguments().putValue("mode", UserHeaderActivity.ControlMode.BLANK);
        description.setBodyDescription(new Intent(getContext(), UserAddBuddyListActivity.class));
        return description;
    }

    public ScreenDescription createUserDetailScreenDescription(User user, Boolean playsSessionGame) {
        ScreenDescription description = createScreenDescription(user, null, true);
        description.setHeaderDescription(new Intent(getContext(), UserHeaderActivity.class)).getArguments().putValue("mode", UserHeaderActivity.ControlMode.BUDDY);
        description.setBodyDescription(new Intent(getContext(), UserDetailListActivity.class)).getArguments().putValue(Constant.USER_PLAYS_SESSION_GAME, playsSessionGame);
        return description;
    }

    public ScreenDescription createUserScreenDescription(User user) {
        ScreenDescription description = createScreenDescription(user, null, true);
        description.setHeaderDescription(new Intent(getContext(), UserHeaderActivity.class)).getArguments().putValue("mode", getSession().isOwnedByUser(ensureUser(user)) ? UserHeaderActivity.ControlMode.BLANK : UserHeaderActivity.ControlMode.BUDDY);
        description.setBodyDescription(new Intent(getContext(), UserListActivity.class));
        if (getSession().isOwnedByUser(ensureUser(user))) {
            description.setShortcutSelectionId(R.string.sl_friends);
        }
        return description;
    }

    public void destroy() {
        ScreenManagerSingleton.destroy();
    }

    private void displayWithEmptyStack(ScreenDescription description) {
        ScreenManagerSingleton.get().displayWithEmptyStack(description);
    }

    private Game ensureGame(Game game) {
        return game != null ? game : getSession().getGame();
    }

    private User ensureUser(User user) {
        return user != null ? user : getSession().getUser();
    }

    private Map<String, Object> extractActivityInfo(PackageInfo packageInfo) {
        Map<String, Object> result = new HashMap<>();
        for (ActivityInfo info : packageInfo.activities) {
            Map<String, Object> details = null;
            if (info.theme != 0) {
                details = new HashMap<>();
                details.put("theme", Integer.valueOf(info.theme));
            }
            result.put(info.name, details);
        }
        return result;
    }

    private Map<String, Object> extractPermissionInfo(PackageInfo packageInfo) {
        Map<String, Object> result = new HashMap<>();
        String[] permissions = packageInfo.requestedPermissions;
        if (permissions != null) {
            for (String name : permissions) {
                result.put(name, null);
            }
        }
        return result;
    }

    public Achievement getAchievement(String awardId) {
        checkHasAwards();
        checkHasLoadedAchievements();
        return this._achievementsEngine.getAchievementsController().getAchievementForAwardIdentifier(awardId);
    }

    public List<Achievement> getAchievements() {
        checkHasAwards();
        checkHasLoadedAchievements();
        return this._achievementsEngine.getAchievementsController().getAchievements();
    }

    private AchievementsEngine getAchievementsEngine() {
        if (this._achievementsEngine == null) {
            this._achievementsEngine = new AchievementsEngine();
        }
        return this._achievementsEngine;
    }

    public AwardList getAwardList() {
        checkHasAwards();
        return getAchievementsEngine().getAchievementsController().getAwardList();
    }

    private ChallengeController getChallengeController() {
        if (this._challengeController == null) {
            this._challengeController = new ChallengeController(this);
        }
        return this._challengeController;
    }

    /* access modifiers changed from: package-private */
    public Configuration getConfiguration() {
        return this._configuration;
    }

    /* access modifiers changed from: private */
    public Context getContext() {
        return this._context;
    }

    private ValueStore getGameValues(Game game) {
        if (game.equals(getSession().getGame())) {
            return getSessionGameValues();
        }
        ValueStore valueStore = new ValueStore();
        valueStore.setValueSourceFactroy(this);
        valueStore.putValue(Constant.GAME, game);
        return valueStore;
    }

    public String getInfoString() {
        return this._client.getInfoString();
    }

    /* access modifiers changed from: package-private */
    public Challenge getLastChallenge() {
        return this._lastChallenge;
    }

    /* access modifiers changed from: package-private */
    public Score getLastScore() {
        return this._lastScore;
    }

    /* access modifiers changed from: package-private */
    public int getLastStatus() {
        return this._lastStatus;
    }

    private String getPersistedUserImageUrl() {
        return getContext().getSharedPreferences(PREFERENCES_NAME, 0).getString("userImageUrl", null);
    }

    private String getPersistedUserName() {
        return getContext().getSharedPreferences(PREFERENCES_NAME, 0).getString("userName", null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.core.controller.ScoreController.<init>(com.scoreloop.client.android.core.controller.RequestControllerObserver, boolean):void
     arg types: [com.scoreloop.client.android.ui.StandardScoreloopManager, int]
     candidates:
      com.scoreloop.client.android.core.controller.ScoreController.<init>(com.scoreloop.client.android.core.model.Session, com.scoreloop.client.android.core.controller.RequestControllerObserver):void
      com.scoreloop.client.android.core.controller.ScoreController.<init>(com.scoreloop.client.android.core.controller.RequestControllerObserver, boolean):void */
    private ScoreController getScoreController() {
        if (this._scoreController == null) {
            this._scoreController = new ScoreController((RequestControllerObserver) this, true);
        }
        return this._scoreController;
    }

    /* access modifiers changed from: protected */
    public Session getSession() {
        return this._client.getSession();
    }

    private ValueStore getSessionGameValues() {
        if (this._sessionGameValueStore == null) {
            this._sessionGameValueStore = new ValueStore();
            this._sessionGameValueStore.setValueSourceFactroy(this);
            this._sessionGameValueStore.putValue(Constant.GAME, getSession().getGame());
        }
        return this._sessionGameValueStore;
    }

    private ValueStore getSessionUserValues() {
        if (this._sessionUserValueStore == null) {
            this._sessionUserValueStore = new ValueStore();
            this._sessionUserValueStore.setValueSourceFactroy(this);
            this._sessionUserValueStore.putValue(Constant.USER, getSession().getUser());
        }
        return this._sessionUserValueStore;
    }

    private ValueStore getUserValues(User user) {
        if (getSession().isOwnedByUser(user)) {
            return getSessionUserValues();
        }
        if (this._cachedUserValueStore != null && this._cachedUserValueStore.getValue(Constant.USER).equals(user)) {
            return this._cachedUserValueStore;
        }
        ValueStore valueStore = new ValueStore();
        valueStore.setValueSourceFactroy(this);
        valueStore.putValue(Constant.USER, user);
        this._cachedUserValueStore = valueStore;
        return valueStore;
    }

    public ValueStore.ValueSource getValueSourceForKeyInStore(String key, ValueStore valueStore) {
        if (containsKey(UserAgent.SUPPORTED_KEYS, key)) {
            return new UserAgent();
        }
        if (containsKey(UserDetailsAgent.SUPPORTED_KEYS, key)) {
            return new UserDetailsAgent();
        }
        if (containsKey(NumberAchievementsAgent.SUPPORTED_KEYS, key)) {
            return new NumberAchievementsAgent();
        }
        if (containsKey(NewsAgent.SUPPORTED_KEYS, key)) {
            return new NewsAgent();
        }
        if (containsKey(UserBuddiesAgent.SUPPORTED_KEYS, key)) {
            return new UserBuddiesAgent();
        }
        return null;
    }

    public boolean hasLoadedAchievements() {
        checkHasAwards();
        return getAchievementsEngine().hasLoadedAchievements();
    }

    public boolean isAwardAchieved(String awardId) {
        return getAchievement(awardId).isAchieved();
    }

    public boolean isChallengeOngoing() {
        return this._challengeOngoing;
    }

    public void loadAchievements(boolean forceInitialSync, Runnable continuation) {
        checkHasAwards();
        getAchievementsEngine().loadAchievements(forceInitialSync, continuation);
    }

    public void onGamePlayEnded(Double scoreResult, Integer mode) {
        Score score = new Score(scoreResult, null);
        score.setMode(mode);
        onGamePlayEnded(score);
    }

    public void onGamePlayEnded(Score score) {
        Game game = getSession().getGame();
        Integer mode = score.getMode();
        if (game.hasModes()) {
            if (mode == null) {
                throw new IllegalArgumentException("the game has modes, but no mode was passed");
            }
            int minMode = game.getMinMode().intValue();
            int maxMode = game.getMaxMode().intValue();
            if (mode.intValue() < minMode || mode.intValue() >= maxMode) {
                throw new IllegalArgumentException("mode out of range [" + minMode + "," + maxMode + "[");
            }
        }
        if (game.hasModes() || mode == null) {
            this._lastStatus = 0;
            this._lastScore = score;
            if (!this._challengeOngoing) {
                this._lastChallenge = null;
                getScoreController().submitScore(this._lastScore);
            } else if (!this._configuration.isFeatureEnabled(Configuration.Feature.CHALLENGE)) {
                throw new IllegalStateException("we're in challenge mode, but the challenge feature is not enabled in the scoreloop.properties");
            } else {
                if (this._lastChallenge.isCreated()) {
                    this._lastChallenge.setContenderScore(this._lastScore);
                }
                if (this._lastChallenge.isAccepted()) {
                    this._lastChallenge.setContestantScore(this._lastScore);
                }
                getChallengeController().setChallenge(this._lastChallenge);
                this._challengeController.submitChallenge();
            }
        } else {
            throw new IllegalArgumentException("the game has no modes, but a mode was passed");
        }
    }

    public void onShortcut(int textId) {
        if (textId == R.string.sl_home) {
            displayWithEmptyStack(createEntryScreenDescription());
        } else if (textId == R.string.sl_friends) {
            displayWithEmptyStack(createUserScreenDescription(null));
        } else if (textId == R.string.sl_market) {
            displayWithEmptyStack(createMarketScreenDescription(null));
        }
    }

    public void persistSessionUserName() {
        User sessionUser = getSession().getUser();
        if (sessionUser.isAuthenticated()) {
            SharedPreferences.Editor preferences = getContext().getSharedPreferences(PREFERENCES_NAME, 0).edit();
            preferences.putString("userName", sessionUser.getDisplayName());
            preferences.putString("userImageUrl", sessionUser.getImageUrl());
            preferences.commit();
        }
    }

    public void requestControllerDidFail(RequestController aRequestController, Exception anException) {
        if (aRequestController == this._scoreController || aRequestController == this._challengeController) {
            this._lastScore = null;
            this._lastChallenge = null;
            this._lastStatus = 3;
            this._challengeOngoing = false;
            if (this._onScoreSubmitObserver != null) {
                this._onScoreSubmitObserver.onScoreSubmit(this._lastStatus, anException);
            }
        }
    }

    public void requestControllerDidReceiveResponse(RequestController aRequestController) {
        if (aRequestController == this._scoreController || aRequestController == this._challengeController) {
            if (this._challengeOngoing) {
                this._lastStatus = 2;
                this._challengeOngoing = false;
            } else {
                this._lastStatus = 1;
            }
            if (this._onScoreSubmitObserver != null) {
                this._onScoreSubmitObserver.onScoreSubmit(this._lastStatus, null);
            }
        }
    }

    public void run() {
        String userName = getPersistedUserName();
        if (userName != null) {
            BaseActivity.showToast(getContext(), String.format(getContext().getString(R.string.sl_format_welcome_back), userName), null, 1);
        }
    }

    public void screenManagerDidLeaveFramework(ScreenManager manager) {
        this._sessionUserValueStore = null;
        this._sessionGameValueStore = null;
        this._cachedUserValueStore = null;
        persistSessionUserName();
    }

    public boolean screenManagerWantsNewScreen(ScreenManager screenManager, ScreenDescription description, ScreenDescription referenceDescription) {
        ValueStore screenValues = description.getScreenValues();
        ValueStore referenceScreenValues = referenceDescription.getScreenValues();
        String userPath = ValueStore.concatenateKeys(Constant.USER_VALUES, Constant.USER);
        if (!((User) screenValues.getValue(userPath)).equals((User) referenceScreenValues.getValue(userPath))) {
            return true;
        }
        String gamePath = ValueStore.concatenateKeys(Constant.GAME_VALUES, Constant.GAME);
        return !((Game) screenValues.getValue(gamePath)).equals((Game) referenceScreenValues.getValue(gamePath));
    }

    public void screenManagerWillEnterFramework(ScreenManager manager) {
        String userName = getPersistedUserName();
        if (userName != null) {
            getSession().getUser().setLogin(userName);
        }
        String userImageUrl = getPersistedUserImageUrl();
        if (userImageUrl != null && getSessionUserValues().getValue("userImageUrl") == null) {
            getSessionUserValues().putValue("userImageUrl", userImageUrl);
        }
    }

    public void screenManagerWillShowOptionsMenu() {
    }

    public void screenManagerWillShowScreenDescription(ScreenDescription screenDescription, ScreenManager.Delegate.Direction direction) {
    }

    public void setOnCanStartGamePlayObserver(OnCanStartGamePlayObserver observer) {
        this._onCanStartGamePlayObserver = observer;
    }

    public void setOnScoreSubmitObserver(OnScoreSubmitObserver observer) {
        this._onScoreSubmitObserver = observer;
    }

    public void setOnStartGamePlayRequestObserver(OnStartGamePlayRequestObserver observer) {
        this._onStartGamePlayRequestObserver = observer;
    }

    private void showToastForAchievement(Achievement achievement) {
        BaseActivity.showToast(getContext(), String.format(getContext().getString(R.string.sl_format_unlocked), achievement.getAward().getLocalizedTitle()));
    }

    public void showWelcomeBackToast(long delay) {
        if (delay < 0) {
            throw new IllegalArgumentException("delay to showWelcomeBackToast must be zero or postive");
        }
        this._handler.postDelayed(this, delay);
    }

    public void startGamePlay(Integer mode, Challenge challenge) {
        if (this._onStartGamePlayRequestObserver == null) {
            throw new IllegalStateException("trying to start gameplay, but the callback is not set - did you call ScoreloopManagerSingleton.get().setOnStartGamePlayRequestObserver(...)?");
        }
        this._challengeOngoing = true;
        this._lastChallenge = challenge;
        this._onStartGamePlayRequestObserver.onStartGamePlayRequest(mode);
    }

    public void submitAchievements(Runnable continuation) {
        checkHasAwards();
        getAchievementsEngine().submitAchievements(continuation);
    }

    private void verifyManifest() {
        try {
            PackageInfo packageInfo = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 4097);
            Map<String, Object> info = extractActivityInfo(packageInfo);
            Checker required = new Checker("activity", info);
            required.add("com.scoreloop.client.android.ui.framework.ScreenActivity", new Object[0]);
            required.add("com.scoreloop.client.android.ui.framework.TabsActivity", new Object[0]);
            required.add("com.scoreloop.client.android.ui.component.market.MarketHeaderActivity", new Object[0]);
            required.add("com.scoreloop.client.android.ui.component.market.MarketListActivity", new Object[0]);
            required.add("com.scoreloop.client.android.ui.component.entry.EntryListActivity", new Object[0]);
            required.add("com.scoreloop.client.android.ui.component.post.PostOverlayActivity", "theme", Integer.valueOf((int) R.style.sl_dialog));
            required.add("com.scoreloop.client.android.ui.component.score.ScoreHeaderActivity", new Object[0]);
            required.add("com.scoreloop.client.android.ui.component.score.ScoreListActivity", new Object[0]);
            required.add("com.scoreloop.client.android.ui.component.user.UserAddBuddyListActivity", new Object[0]);
            required.add("com.scoreloop.client.android.ui.component.user.UserHeaderActivity", new Object[0]);
            required.add("com.scoreloop.client.android.ui.component.user.UserDetailListActivity", new Object[0]);
            required.add("com.scoreloop.client.android.ui.component.user.UserListActivity", new Object[0]);
            required.add("com.scoreloop.client.android.ui.component.game.GameDetailHeaderActivity", new Object[0]);
            required.add("com.scoreloop.client.android.ui.component.game.GameDetailListActivity", new Object[0]);
            required.add("com.scoreloop.client.android.ui.component.game.GameListActivity", new Object[0]);
            required.add("com.scoreloop.client.android.ui.component.achievement.AchievementHeaderActivity", Configuration.Feature.ACHIEVEMENT);
            required.add("com.scoreloop.client.android.ui.component.achievement.AchievementListActivity", Configuration.Feature.ACHIEVEMENT);
            required.add("com.scoreloop.client.android.ui.component.news.NewsHeaderActivity", Configuration.Feature.NEWS);
            required.add("com.scoreloop.client.android.ui.component.news.NewsListActivity", Configuration.Feature.NEWS);
            required.add("com.scoreloop.client.android.ui.component.challenge.ChallengeHeaderActivity", Configuration.Feature.CHALLENGE);
            required.add("com.scoreloop.client.android.ui.component.challenge.ChallengeListActivity", Configuration.Feature.CHALLENGE);
            required.add("com.scoreloop.client.android.ui.component.challenge.ChallengeAcceptListActivity", Configuration.Feature.CHALLENGE);
            required.add("com.scoreloop.client.android.ui.component.challenge.ChallengeCreateListActivity", Configuration.Feature.CHALLENGE);
            required.add("com.scoreloop.client.android.ui.component.challenge.ChallengePaymentActivity", Configuration.Feature.CHALLENGE);
            required.add("com.scoreloop.client.android.ui.component.profile.ProfileSettingsListActivity", new Object[0]);
            required.add("com.scoreloop.client.android.ui.component.profile.ProfileSettingsPictureListActivity", new Object[0]);
            required.reportRequired();
            Checker optional = new Checker("activity", info);
            optional.add("com.scoreloop.client.android.ui.EntryScreenActivity", new Object[0]);
            optional.add("com.scoreloop.client.android.ui.BuddiesScreenActivity", new Object[0]);
            optional.add("com.scoreloop.client.android.ui.LeaderboardsScreenActivity", new Object[0]);
            optional.add("com.scoreloop.client.android.ui.ChallengesScreenActivity", Configuration.Feature.CHALLENGE);
            optional.add("com.scoreloop.client.android.ui.AchievementsScreenActivity", Configuration.Feature.ACHIEVEMENT);
            optional.add("com.scoreloop.client.android.ui.SocialMarketScreenActivity", new Object[0]);
            optional.add("com.scoreloop.client.android.ui.ProfileScreenActivity", new Object[0]);
            optional.reportOptional();
            Checker permission = new Checker("uses-permission", extractPermissionInfo(packageInfo));
            permission.add("android.permission.INTERNET", new Object[0]);
            permission.add("android.permission.READ_PHONE_STATE", new Object[0]);
            permission.add("android.permission.READ_CONTACTS", Configuration.Feature.ADDRESS_BOOK);
            permission.reportRequired();
            required.check();
            optional.check();
            permission.check();
        } catch (PackageManager.NameNotFoundException e) {
            throw new VerifyException();
        }
    }

    public void trackPageView(String activityClassName, ValueStore arguments) {
    }

    public void trackEvent(String category, String action, String label, int value) {
    }
}
