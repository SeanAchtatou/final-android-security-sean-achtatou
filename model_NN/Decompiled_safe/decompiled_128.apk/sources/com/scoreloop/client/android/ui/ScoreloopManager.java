package com.scoreloop.client.android.ui;

import com.scoreloop.client.android.core.model.Achievement;
import com.scoreloop.client.android.core.model.AwardList;
import com.scoreloop.client.android.core.model.Score;
import java.util.List;

public interface ScoreloopManager {
    void achieveAward(String str, boolean z, boolean z2);

    void destroy();

    Achievement getAchievement(String str);

    List<Achievement> getAchievements();

    AwardList getAwardList();

    String getInfoString();

    boolean hasLoadedAchievements();

    boolean isAwardAchieved(String str);

    void loadAchievements(boolean z, Runnable runnable);

    void onGamePlayEnded(Score score);

    void onGamePlayEnded(Double d, Integer num);

    void setOnCanStartGamePlayObserver(OnCanStartGamePlayObserver onCanStartGamePlayObserver);

    void setOnScoreSubmitObserver(OnScoreSubmitObserver onScoreSubmitObserver);

    void setOnStartGamePlayRequestObserver(OnStartGamePlayRequestObserver onStartGamePlayRequestObserver);

    void showWelcomeBackToast(long j);

    void submitAchievements(Runnable runnable);
}
