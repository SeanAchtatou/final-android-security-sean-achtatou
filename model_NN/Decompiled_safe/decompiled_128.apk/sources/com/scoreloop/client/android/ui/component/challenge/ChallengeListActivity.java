package com.scoreloop.client.android.ui.component.challenge;

import android.os.Bundle;
import com.scoreloop.client.android.core.controller.ChallengesController;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.base.CaptionListItem;
import com.scoreloop.client.android.ui.component.base.ComponentListActivity;
import com.scoreloop.client.android.ui.component.base.Constant;
import com.scoreloop.client.android.ui.component.base.EmptyListItem;
import com.scoreloop.client.android.ui.component.base.ExpandableListItem;
import com.scoreloop.client.android.ui.component.base.Factory;
import com.scoreloop.client.android.ui.framework.BaseListAdapter;
import com.scoreloop.client.android.ui.framework.BaseListItem;
import com.scoreloop.client.android.ui.framework.ValueStore;
import com.skyd.bestpuzzle.n1664.R;
import java.util.Iterator;
import java.util.List;

public class ChallengeListActivity extends ComponentListActivity<BaseListItem> {
    private List<User> _buddies;
    private ExpandableListItem _expandableHistoryListItem;
    private ExpandableListItem _expandableOpenListItem;
    private List<Challenge> _history;
    private ChallengesController _historyController;
    private List<Challenge> _open;
    private ChallengesController _openController;
    private boolean _showPrize;
    private boolean _showSeeMoreHistory = true;
    private boolean _showSeeMoreOpen = true;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setListAdapter(new BaseListAdapter(this));
        this._openController = new ChallengesController(getRequestControllerObserver());
        this._historyController = new ChallengesController(getRequestControllerObserver());
        addObservedKeys(ValueStore.concatenateKeys(Constant.SESSION_USER_VALUES, Constant.USER_BUDDIES));
    }

    public void onListItemClick(BaseListItem item) {
        Factory factory = getFactory();
        if (item == this._expandableHistoryListItem) {
            this._showSeeMoreHistory = false;
            updateListIfReady();
        } else if (item == this._expandableOpenListItem) {
            this._showSeeMoreOpen = false;
            updateListIfReady();
        } else if (item.getType() == 6) {
            display(factory.createChallengeAcceptScreenDescription((Challenge) ((ChallengeOpenListItem) item).getTarget()));
        } else if (item.getType() == 5) {
            display(factory.createChallengeCreateScreenDescription((User) ((ChallengeCreateListItem) item).getTarget(), null));
        } else if (item.getType() == 4) {
            this._showPrize = !this._showPrize;
            BaseListAdapter<BaseListItem> adapter = getBaseListAdapter();
            for (int i = 0; i < adapter.getCount(); i++) {
                BaseListItem it = (BaseListItem) adapter.getItem(i);
                if (it.getType() == 4) {
                    ((ChallengeHistoryListItem) it).setShowPrize(this._showPrize);
                }
            }
            adapter.notifyDataSetChanged();
        }
    }

    public void onRefresh(int flags) {
        showSpinnerFor(this._openController);
        this._openController.loadOpenChallenges();
        showSpinnerFor(this._historyController);
        this._historyController.loadChallengeHistory();
    }

    public void onStart() {
        super.onStart();
        setNeedsRefresh();
    }

    public void onValueChanged(ValueStore valueStore, String key, Object oldValue, Object newValue) {
        if (newValue instanceof List) {
            this._buddies = (List) newValue;
            updateListIfReady();
        }
    }

    public void onValueSetDirty(ValueStore valueStore, String key) {
        getSessionUserValues().retrieveValue(Constant.USER_BUDDIES, ValueStore.RetrievalMode.NOT_DIRTY, null);
    }

    public void requestControllerDidReceiveResponseSafe(RequestController aRequestController) {
        if (aRequestController == this._historyController) {
            this._history = this._historyController.getChallenges();
        } else if (aRequestController == this._openController) {
            this._open = this._openController.getChallenges();
        }
        updateListIfReady();
    }

    private void updateListIfReady() {
        if (this._open != null && this._history != null && this._buddies != null) {
            BaseListAdapter<BaseListItem> adapter = getBaseListAdapter();
            adapter.clear();
            adapter.add(new CaptionListItem(this, null, getString(R.string.sl_open_challenges)));
            if (this._open.size() > 0) {
                int i = 0;
                Iterator<Challenge> it = this._open.iterator();
                while (true) {
                    if (it.hasNext()) {
                        Challenge challenge = it.next();
                        if (this._showSeeMoreOpen && (i = i + 1) > 2) {
                            this._expandableOpenListItem = new ExpandableListItem(this);
                            adapter.add(this._expandableOpenListItem);
                            break;
                        }
                        adapter.add(new ChallengeOpenListItem(this, challenge));
                    } else {
                        break;
                    }
                }
            } else {
                adapter.add(new EmptyListItem(this, getResources().getString(R.string.sl_no_open_challenges)));
            }
            adapter.add(new CaptionListItem(this, null, getString(R.string.sl_challenges_history)));
            if (this._history.size() > 0) {
                int i2 = 0;
                boolean addedItem = false;
                Iterator<Challenge> it2 = this._history.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    Challenge challenge2 = it2.next();
                    if (challenge2.isComplete() || challenge2.isOpen() || challenge2.isAssigned() || challenge2.isRejected() || challenge2.isAccepted()) {
                        addedItem = true;
                        if (this._showSeeMoreHistory && (i2 = i2 + 1) > 2) {
                            this._expandableHistoryListItem = new ExpandableListItem(this);
                            adapter.add(this._expandableHistoryListItem);
                            break;
                        }
                        adapter.add(new ChallengeHistoryListItem(this, challenge2, this._showPrize));
                    }
                }
                if (!addedItem) {
                    adapter.add(new EmptyListItem(this, getResources().getString(R.string.sl_no_history_challenges)));
                }
            } else {
                adapter.add(new EmptyListItem(this, getResources().getString(R.string.sl_no_history_challenges)));
            }
            adapter.add(new CaptionListItem(this, null, getString(R.string.sl_new_challenge)));
            adapter.add(new ChallengeCreateListItem(this, null));
            for (User user : this._buddies) {
                adapter.add(new ChallengeCreateListItem(this, user));
            }
        }
    }
}
