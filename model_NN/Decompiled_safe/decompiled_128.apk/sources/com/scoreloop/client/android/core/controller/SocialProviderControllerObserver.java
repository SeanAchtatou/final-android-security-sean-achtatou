package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.PublishedFor__1_0_0;

public interface SocialProviderControllerObserver {
    @PublishedFor__1_0_0
    void socialProviderControllerDidCancel(SocialProviderController socialProviderController);

    @PublishedFor__1_0_0
    void socialProviderControllerDidEnterInvalidCredentials(SocialProviderController socialProviderController);

    @PublishedFor__1_0_0
    void socialProviderControllerDidFail(SocialProviderController socialProviderController, Throwable th);

    @PublishedFor__1_0_0
    void socialProviderControllerDidSucceed(SocialProviderController socialProviderController);
}
