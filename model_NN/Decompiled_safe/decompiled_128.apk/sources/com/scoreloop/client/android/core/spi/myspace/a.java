package com.scoreloop.client.android.core.spi.myspace;

import android.app.Activity;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.spi.AuthViewController;
import com.scoreloop.client.android.core.spi.myspace.b;
import com.scoreloop.client.android.core.spi.myspace.c;
import com.scoreloop.client.android.core.util.HTTPUtils;
import com.scoreloop.client.android.core.util.Logger;
import com.scoreloop.client.android.core.util.OAuthBuilder;
import com.scoreloop.client.android.core.util.SetterIntent;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

class a extends AuthViewController {
    private Activity a;
    private String b;
    private String c;
    private String d;
    /* access modifiers changed from: private */
    public c e;
    private String f;
    private String g;
    private String h;

    a(Session session, AuthViewController.Observer observer) {
        super(session, observer);
    }

    private JSONObject a(String str, String str2) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("username", str);
        jSONObject.put("password", str2);
        return jSONObject;
    }

    private void a(Exception exc) {
        this.e.dismiss();
        a().a(exc);
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        try {
            a(this.c, this.d, b("https://roa.myspace.com/roa/09/messaging/login/" + this.b + "/" + str));
        } catch (MalformedURLException e2) {
            throw new IllegalStateException(e2);
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2, HttpPut httpPut) {
        try {
            this.c = str;
            this.d = str2;
            JSONObject jSONObject = new JSONObject(HTTPUtils.a(new DefaultHttpClient().execute(b(str, str2, httpPut))));
            Logger.a("myspace response parsed json:", jSONObject.toString(4));
            if (a(jSONObject)) {
                c(jSONObject);
            } else if (b(jSONObject)) {
                d(jSONObject);
            } else {
                f();
            }
        } catch (UnsupportedEncodingException e2) {
            a(e2);
        } catch (URISyntaxException e3) {
            a(e3);
        } catch (JSONException e4) {
            a(e4);
        } catch (ClientProtocolException e5) {
            a((Exception) e5);
        } catch (IOException e6) {
            a(e6);
        }
    }

    private boolean a(JSONObject jSONObject) throws JSONException {
        SetterIntent setterIntent = new SetterIntent();
        return setterIntent.h(jSONObject, "token", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE) && setterIntent.h(jSONObject, "tokenSecret", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE) && setterIntent.h(jSONObject, "userId", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE);
    }

    /* access modifiers changed from: private */
    public HttpPut b(String str) throws MalformedURLException {
        OAuthBuilder oAuthBuilder = new OAuthBuilder();
        oAuthBuilder.a(new URL(str));
        oAuthBuilder.a("73c6e22c33e14b56a329e7a19b40914c");
        return oAuthBuilder.b("814d340412384e27b0d480add625fb4391d5cefac08e431f9797c3c9fa94caaf", null);
    }

    private HttpPut b(String str, String str2, HttpPut httpPut) throws URISyntaxException, JSONException, UnsupportedEncodingException {
        httpPut.setHeader("Content-Type", "application/json");
        httpPut.setEntity(new StringEntity(a(str, str2).toString()));
        return httpPut;
    }

    private boolean b(JSONObject jSONObject) throws JSONException {
        SetterIntent setterIntent = new SetterIntent();
        if (!setterIntent.f(jSONObject, "captcha", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            return false;
        }
        JSONObject jSONObject2 = (JSONObject) setterIntent.a();
        return setterIntent.h(jSONObject2, "captchaImageUri", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE) && setterIntent.h(jSONObject2, "captchaGuid", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE);
    }

    private void c(JSONObject jSONObject) throws JSONException {
        Logger.a("myspace response parser", "MYSPACE: valid user credentials acquired");
        SetterIntent setterIntent = new SetterIntent();
        this.f = setterIntent.d(jSONObject, "token", SetterIntent.KeyMode.THROWS_WHEN_NO_KEY, SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE);
        this.g = setterIntent.d(jSONObject, "tokenSecret", SetterIntent.KeyMode.THROWS_WHEN_NO_KEY, SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE);
        this.h = setterIntent.d(jSONObject, "userId", SetterIntent.KeyMode.THROWS_WHEN_NO_KEY, SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE);
        this.e.dismiss();
        a().c();
    }

    private void d(JSONObject jSONObject) throws JSONException {
        SetterIntent setterIntent = new SetterIntent();
        JSONObject b2 = setterIntent.b(jSONObject, "captcha", SetterIntent.KeyMode.THROWS_WHEN_NO_KEY, SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE);
        Logger.a("myspace response parser", "MYSPACE: captcha request aquired");
        String d2 = setterIntent.d(b2, "captchaImageUri", SetterIntent.KeyMode.THROWS_WHEN_NO_KEY, SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE);
        this.b = setterIntent.d(b2, "captchaGuid", SetterIntent.KeyMode.THROWS_WHEN_NO_KEY, SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE);
        b bVar = new b(e(), new b.a() {
            public void a(String str) {
                a.this.a(str);
            }
        }, d2);
        this.e.dismiss();
        bVar.show();
    }

    private Activity e() {
        return this.a;
    }

    private void f() {
        this.e.dismiss();
        a().b();
    }

    public void a(Activity activity) {
        this.a = activity;
        this.e = new c(e(), new c.a() {
            public void a() {
                a.this.e.dismiss();
                a.this.a().b_();
            }

            public void a(String str, String str2) {
                try {
                    a.this.a(str, str2, a.this.b("https://roa.myspace.com/roa/09/messaging/login"));
                } catch (MalformedURLException e) {
                    throw new IllegalStateException(e);
                }
            }
        });
        this.e.show();
    }

    public String b() {
        return this.f;
    }

    public String c() {
        return this.g;
    }

    public String d() {
        return this.h;
    }
}
