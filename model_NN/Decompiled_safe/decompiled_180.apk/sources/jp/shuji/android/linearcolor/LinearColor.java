package jp.shuji.android.linearcolor;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

public class LinearColor extends Activity {
    ViewGroup.LayoutParams colorLayout = new ViewGroup.LayoutParams(120, -1);
    ConfigBean config;
    ViewGroup.LayoutParams normalLayout = new ViewGroup.LayoutParams(-1, -2);

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(1);
        setContentView((int) R.layout.main);
        FileUtil.init(this);
        initColor();
    }

    /* access modifiers changed from: private */
    public void initColor() {
        ConfigBean config2 = FileUtil.readColors();
        ((LinearLayout) findViewById(R.id.mainBack)).setBackgroundColor(config2.getBackColor());
        ((TextView) findViewById(R.id.mainTitle)).setTextColor(config2.getLineColor());
        Button startE = (Button) findViewById(R.id.mainStartEasy);
        startE.setTextColor(config2.getEnemyColor());
        startE.setBackgroundColor(config2.getBackColor());
        Button startH = (Button) findViewById(R.id.mainStartHard);
        startH.setTextColor(config2.getEnemyColor());
        startH.setBackgroundColor(config2.getBackColor());
        Button colors = (Button) findViewById(R.id.mainColors);
        colors.setTextColor(config2.getScoreColor());
        colors.setBackgroundColor(config2.getBackColor());
        Button ranking = (Button) findViewById(R.id.mainRanking);
        ranking.setTextColor(config2.getScoreColor());
        ranking.setBackgroundColor(config2.getBackColor());
    }

    public void gameStartEasy(View button) {
        Intent intent = new Intent(this, NyororiActivity.class);
        intent.putExtra("MODE", "EASY");
        startActivityForResult(intent, 0);
    }

    public void gameStartHard(View button) {
        Intent intent = new Intent(this, NyororiActivity.class);
        intent.putExtra("MODE", "HARD");
        startActivityForResult(intent, 0);
    }

    public void editConfig(View button) {
        this.config = FileUtil.readColors();
        AlertDialog.Builder configBuilder = new AlertDialog.Builder(this);
        configBuilder.setTitle("Config");
        LinearLayout baseLayout = new LinearLayout(this);
        baseLayout.setLayoutParams(this.normalLayout);
        baseLayout.setOrientation(1);
        baseLayout.addView(getColorLayout(getString(R.string.back), this.config.getBackColor()));
        baseLayout.addView(getColorLayout(getString(R.string.attack), this.config.getLineColor()));
        baseLayout.addView(getColorLayout(getString(R.string.enemy), this.config.getEnemyColor()));
        baseLayout.addView(getColorLayout(getString(R.string.score), this.config.getScoreColor()));
        configBuilder.setView(baseLayout);
        configBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                FileUtil.writeColors(LinearColor.this.config);
                LinearColor.this.initColor();
            }
        });
        configBuilder.setNeutralButton("INIT", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ConfigBean initConfig = new ConfigBean();
                initConfig.setBackColor(-256);
                initConfig.setLineColor(-65536);
                initConfig.setEnemyColor(-16777216);
                initConfig.setScoreColor(-16777216);
                FileUtil.writeColors(initConfig);
                LinearColor.this.initColor();
            }
        });
        configBuilder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        configBuilder.create().show();
    }

    public void ranking(View button) {
        startActivity(new Intent(this, RankingActivity.class));
    }

    private LinearLayout getColorLayout(String text, int color) {
        LinearLayout backLayout = new LinearLayout(this);
        backLayout.setOrientation(0);
        TextView backText = new TextView(this);
        backText.setText(text);
        backText.setTextSize(18.0f);
        backText.setLayoutParams(this.colorLayout);
        backText.setGravity(17);
        backLayout.addView(backText);
        backLayout.addView(getSpinner(text, color));
        return backLayout;
    }

    /* access modifiers changed from: private */
    public void setNewColor(String type, int color) {
        if (type.equals(getString(R.string.back))) {
            this.config.setBackColor(color);
        } else if (type.equals(getString(R.string.attack))) {
            this.config.setLineColor(color);
        } else if (type.equals(getString(R.string.enemy))) {
            this.config.setEnemyColor(color);
        } else if (type.equals(getString(R.string.score))) {
            this.config.setScoreColor(color);
        }
    }

    private Spinner getSpinner(String type, int color) {
        ArrayAdapter<String> sAdapter = new ArrayAdapter<>(this, 17367048);
        sAdapter.setDropDownViewResource(17367049);
        sAdapter.add("YELLOW");
        sAdapter.add("RED");
        sAdapter.add("BLACK");
        sAdapter.add("BLUE");
        sAdapter.add("WHITE");
        sAdapter.add("GREEN");
        sAdapter.add("GRAY");
        Spinner backSpinner = new Spinner(this);
        backSpinner.setLayoutParams(this.normalLayout);
        backSpinner.setTag(type);
        backSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Spinner spinner = (Spinner) parent;
                String text = (String) spinner.getSelectedItem();
                String type = (String) spinner.getTag();
                if (text.equals("YELLOW")) {
                    spinner.setBackgroundColor(-256);
                    LinearColor.this.setNewColor(type, -256);
                } else if (text.equals("RED")) {
                    spinner.setBackgroundColor(-65536);
                    LinearColor.this.setNewColor(type, -65536);
                } else if (text.equals("BLACK")) {
                    spinner.setBackgroundColor(-16777216);
                    LinearColor.this.setNewColor(type, -16777216);
                } else if (text.equals("BLUE")) {
                    spinner.setBackgroundColor(-16776961);
                    LinearColor.this.setNewColor(type, -16776961);
                } else if (text.equals("WHITE")) {
                    spinner.setBackgroundColor(-1);
                    LinearColor.this.setNewColor(type, -1);
                } else if (text.equals("GREEN")) {
                    spinner.setBackgroundColor(-16711936);
                    LinearColor.this.setNewColor(type, -16711936);
                } else if (text.equals("GRAY")) {
                    spinner.setBackgroundColor(-7829368);
                    LinearColor.this.setNewColor(type, -7829368);
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        backSpinner.setAdapter((SpinnerAdapter) sAdapter);
        if (color == -256) {
            backSpinner.setSelection(0);
        } else if (color == -65536) {
            backSpinner.setSelection(1);
        } else if (color == -16777216) {
            backSpinner.setSelection(2);
        } else if (color == -16776961) {
            backSpinner.setSelection(3);
        } else if (color == -1) {
            backSpinner.setSelection(4);
        } else if (color == -16711936) {
            backSpinner.setSelection(5);
        } else if (color == -7829368) {
            backSpinner.setSelection(6);
        }
        backSpinner.setBackgroundColor(color);
        return backSpinner;
    }
}
