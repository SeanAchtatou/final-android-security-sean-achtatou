package jp.shuji.android.linearcolor;

import android.graphics.Canvas;
import android.graphics.Paint;

public class Header {
    private Paint paint;

    public Header(int color) {
        this.paint = null;
        this.paint = new Paint();
        this.paint.setColor(color);
        this.paint.setAntiAlias(true);
        this.paint.setTextSize(24.0f);
    }

    public void draw(Canvas canvas, int score, int enemyLNum) {
        if (enemyLNum < 0) {
            enemyLNum = 0;
        }
        canvas.drawText("SCORE:" + Integer.toString(score), 0.0f, 30.0f, this.paint);
        canvas.drawText("ENEMY:" + Integer.toString(enemyLNum), (float) (NyororiSurface.X_MAX - ((int) this.paint.measureText("ENEMY:" + NyororiSurface.enemyMaxNum))), 30.0f, this.paint);
    }

    public void drawLast(Canvas canvas, int hitrate, int score, boolean isTop) {
        canvas.drawColor(-16777216);
        Paint paint2 = new Paint();
        paint2.setColor(-1);
        paint2.setAntiAlias(true);
        paint2.setTextSize(24.0f);
        canvas.drawText("HIT RATE:" + Integer.toString(hitrate) + "%", 0.0f, 60.0f, paint2);
        Paint paint3 = new Paint();
        paint3.setColor(-1);
        paint3.setAntiAlias(true);
        paint3.setTextSize(36.0f);
        canvas.drawText("TOTAL SCORE:", 0.0f, 120.0f, paint2);
        canvas.drawText(Integer.toString(score), 0.0f, 170.0f, paint3);
        Paint paint4 = new Paint();
        paint4.setColor(-65536);
        paint4.setAntiAlias(true);
        paint4.setTextSize(24.0f);
        int sabun = 0;
        if (isTop) {
            canvas.drawText("!NEW RECORD!", 0.0f, 220.0f, paint4);
            sabun = 40;
        }
        Paint paint5 = new Paint();
        paint5.setColor(-256);
        paint5.setAntiAlias(true);
        paint5.setTextSize(24.0f);
        canvas.drawText("(touch screen)", 0.0f, (float) (sabun + 220), paint5);
    }
}
