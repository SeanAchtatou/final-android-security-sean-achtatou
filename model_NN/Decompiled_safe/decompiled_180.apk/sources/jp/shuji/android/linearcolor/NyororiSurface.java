package jp.shuji.android.linearcolor;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.util.ArrayList;
import java.util.List;

public class NyororiSurface extends SurfaceView implements SurfaceHolder.Callback, Runnable {
    public static int X_MAX = 0;
    public static int X_MIN = 0;
    public static int Y_MAX = 0;
    public static int Y_MIN = 0;
    public static int enemyMaxNum = 50;
    private static int leastEnemyNum = enemyMaxNum;
    public static Integer resultPoint = null;
    private int attackNum = 0;
    private float attackX = 0.0f;
    private float attackY = 0.0f;
    private int backColor = 0;
    private int cLineNum = 0;
    private CLine[] clines = new CLine[this.maxLineNum];
    private ConfigBean configBean;
    private Context context;
    private float d = 5.0f;
    private double directAttackDistance2 = 400.0d;
    private int directScore = 10;
    private long endingStartTime = 0;
    private long endingWait = 3000;
    private Enemy[] enemies = new Enemy[this.enemyMax];
    private int enemyEvent = 0;
    private Flower[] enemyFlowers = new Flower[this.enemyMax];
    private int enemyMax = 5;
    private int enemyNum = 0;
    private int[] enemyNums = new int[enemyMaxNum];
    private float enemyRadius = 15.0f;
    private float[] enemyX = new float[enemyMaxNum];
    private float[] enemyY = new float[enemyMaxNum];
    double evc = 5.0E-7d;
    double evtc = 0.0d;
    double evx = 0.0d;
    double evy = 0.0d;
    private int flowerEnemyLineWidth = 15;
    private int flowerLineWidth = 15;
    private Flower[] flowers = new Flower[this.maxLineNum];
    private Hit hitScore = null;
    boolean isActivityEnd = false;
    private boolean isEndingStart = false;
    private boolean isRunnable = true;
    private SurfaceHolder mainHolder = null;
    Thread mainThread = null;
    private int maxLineNum = 3;
    private int maxTraceNum = 20;
    long nanoTime = 0;
    private int preScore = 0;
    private double reflectAttackDistance2 = 200.0d;
    private int reflectScore = 200;
    private int score = 0;
    private Header scoreBoard;
    private double throwAttackDistance2 = 900.0d;
    private int throwNum = 0;
    private int throwScore = 100;
    private int totalEnemyNum = 0;
    private int totscore;
    private float touchX = -1.0f;
    private float touchY = -1.0f;
    private final int traceScore = 1;

    public static synchronized void eraseEnemy() {
        synchronized (NyororiSurface.class) {
            leastEnemyNum--;
        }
    }

    private int getInitBackColor() {
        return this.configBean.getBackColor();
    }

    private int getInitLineColor() {
        return this.configBean.getLineColor();
    }

    private int getInitEnemyColor() {
        return this.configBean.getEnemyColor();
    }

    private int getInitScoreColor() {
        return this.configBean.getScoreColor();
    }

    public NyororiSurface(Context context2) {
        super(context2);
        this.context = context2;
        String mode = ((Activity) context2).getIntent().getStringExtra("MODE");
        leastEnemyNum = enemyMaxNum;
        if (mode != null) {
            if (mode.equals("EASY")) {
                this.directScore /= 2;
                this.throwScore /= 2;
                this.reflectScore /= 2;
                this.enemyRadius = (float) (((double) this.enemyRadius) * 1.5d);
                this.directAttackDistance2 = 900.0d;
                this.reflectAttackDistance2 = 400.0d;
                this.throwAttackDistance2 = 1600.0d;
                this.flowerEnemyLineWidth = (int) (((double) this.flowerEnemyLineWidth) * 1.5d);
            } else if (mode.equals("HARD")) {
                this.directScore = 10;
                this.throwScore = 100;
                this.reflectScore = 200;
                this.directAttackDistance2 = 400.0d;
                this.reflectAttackDistance2 = 200.0d;
                this.throwAttackDistance2 = 900.0d;
                this.flowerEnemyLineWidth = 15;
                this.enemyRadius = 15.0f;
            }
        }
        this.configBean = FileUtil.readColors();
        getHolder().addCallback(this);
        this.backColor = getInitBackColor();
        for (int i = 0; i < this.maxLineNum; i++) {
            this.clines[i] = new CLine(this.maxTraceNum, getInitLineColor());
            this.flowers[i] = new Flower(30, getInitLineColor(), this.flowerLineWidth);
        }
        this.scoreBoard = new Header(getInitScoreColor());
        this.hitScore = new Hit(getInitScoreColor());
    }

    private void createEnemyInfo() {
        for (int i = 0; i < this.enemyMax; i++) {
            this.enemies[i] = new Enemy(this.enemyRadius, getInitEnemyColor());
            this.enemyFlowers[i] = new Flower(30, getInitEnemyColor(), this.flowerEnemyLineWidth);
        }
        int leftEnemy = enemyMaxNum;
        for (int i2 = 0; i2 < enemyMaxNum; i2++) {
            int e = (int) ((Math.random() * ((double) this.enemyMax)) + 1.0d);
            if (leftEnemy <= this.enemyMax) {
                e = leftEnemy;
            }
            this.enemyNums[i2] = e;
            leftEnemy -= e;
            float xe = (float) (((int) (Math.random() * ((double) (X_MAX - 30)))) + 15);
            float ye = (float) (((int) (Math.random() * ((double) (Y_MAX - 30)))) + 15);
            this.enemyX[i2] = xe;
            this.enemyY[i2] = ye;
        }
    }

    private double distance2(float x0, float y0, float x1, float y1) {
        return Math.pow((double) (x0 - x1), 2.0d) + Math.pow((double) (y0 - y1), 2.0d);
    }

    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case 0:
                if (this.isActivityEnd) {
                    ((Activity) this.context).finish();
                }
                this.touchX = event.getX();
                this.touchY = event.getY();
                directAttack(this.touchX, this.touchY);
                this.nanoTime = System.nanoTime();
                break;
            case 1:
                this.clines[this.cLineNum].setMove(true);
                this.throwNum++;
                this.cLineNum++;
                if (this.cLineNum == this.maxLineNum) {
                    this.cLineNum = 0;
                    break;
                }
                break;
            case 2:
                if (distance2(event.getX(), this.touchX, event.getY(), this.touchY) > ((double) this.d)) {
                    this.clines[this.cLineNum].setMove(false);
                    this.clines[this.cLineNum].setLinePoints(this.touchX, this.touchY, event.getX(), event.getY(), System.nanoTime() - this.nanoTime);
                    this.touchX = event.getX();
                    this.touchY = event.getY();
                    directAttack(this.touchX, this.touchY);
                    this.nanoTime = System.nanoTime();
                    this.score++;
                    break;
                }
                break;
        }
        return true;
    }

    private void throwAttack(float x, float y, double vx, double vy, int idx) {
        for (int i = 0; i < this.enemyNum; i++) {
            if (!this.enemies[i].isEnd() && this.enemies[i].isValid() && distance2(this.enemies[i].getX0(), this.enemies[i].getY0(), x, y) < this.throwAttackDistance2) {
                this.enemies[i].setEnd(true);
                this.evtc = Math.sqrt(Math.pow(vx, 2.0d) + Math.pow(vy, 2.0d));
                if (this.enemies[i].getX0() - x >= 0.0f) {
                    this.evx = this.evtc / 3.0d;
                } else {
                    this.evx = (-this.evtc) / 3.0d;
                }
                if (this.enemies[i].getY0() - y >= 0.0f) {
                    this.evy = this.evtc / 3.0d;
                } else {
                    this.evy = (-this.evtc) / 3.0d;
                }
                this.flowers[idx].setNewFlower(this.evx, this.evy, x, y);
                this.clines[idx].setMove(false);
                this.enemyFlowers[i].setNewFlower(vx / 1.5d, vy / 1.5d, this.enemies[i].getX0(), this.enemies[i].getY0());
                this.attackNum++;
                this.score += this.throwScore;
                this.preScore = this.throwScore;
                eraseEnemy();
                this.attackX = this.enemies[i].getX0();
                this.attackY = this.enemies[i].getY0();
            }
        }
    }

    private void reflectAttack(float[] xp, float[] yp, double[] vxp, double[] vyp) {
        for (int i = 0; i < this.enemyNum; i++) {
            if (!this.enemies[i].isEnd() && this.enemies[i].isValid()) {
                for (int j = 0; j < 4; j++) {
                    if (distance2(this.enemies[i].getX0(), this.enemies[i].getY0(), xp[j], yp[j]) < this.reflectAttackDistance2) {
                        this.enemies[i].setEnd(true);
                        this.enemyFlowers[i].setNewFlower(vxp[j] / 1.1d, vyp[j] / 1.1d, this.enemies[i].getX0(), this.enemies[i].getY0());
                        this.attackNum++;
                        this.score += this.reflectScore;
                        this.preScore = this.reflectScore;
                        eraseEnemy();
                        this.attackX = this.enemies[i].getX0();
                        this.attackY = this.enemies[i].getY0();
                        vxp[j] = vxp[j] / 2.0d;
                        vyp[j] = vyp[j] / 2.0d;
                    }
                }
            }
        }
    }

    private void directAttack(float x, float y) {
        for (int i = 0; i < this.enemyNum; i++) {
            if (!this.enemies[i].isEnd() && this.enemies[i].isValid() && distance2(this.enemies[i].getX0(), this.enemies[i].getY0(), x, y) < this.directAttackDistance2) {
                this.enemies[i].setEnd(true);
                if (this.enemies[i].getX0() - x >= 0.0f) {
                    this.evx = this.evc;
                } else {
                    this.evx = -this.evc;
                }
                if (this.enemies[i].getY0() - y >= 0.0f) {
                    this.evy = this.evc;
                } else {
                    this.evy = -this.evc;
                }
                this.enemyFlowers[i].setNewFlower(this.evx, this.evy, this.enemies[i].getX0(), this.enemies[i].getY0());
                this.attackNum++;
                this.throwNum++;
                this.score += this.directScore;
                this.preScore = this.directScore;
                eraseEnemy();
                this.attackX = this.enemies[i].getX0();
                this.attackY = this.enemies[i].getY0();
            }
        }
    }

    public void run() {
        int acc;
        long pretime = 0;
        long dttime = 0;
        boolean isNewEnemy = true;
        boolean enemyBroken = false;
        while (this.isRunnable) {
            Canvas canvas = this.mainHolder.lockCanvas();
            if (!this.isEndingStart || System.currentTimeMillis() - this.endingStartTime <= this.endingWait) {
                if (pretime != 0) {
                    dttime = System.nanoTime() - pretime;
                }
                if (canvas != null) {
                    canvas.drawColor(this.backColor);
                    this.scoreBoard.draw(canvas, this.score, leastEnemyNum);
                    for (int i = 0; i < this.maxLineNum; i++) {
                        if (this.clines[i].isValid()) {
                            this.clines[i].draw(canvas);
                        }
                        if (this.flowers[i].isValid()) {
                            this.flowers[i].draw(canvas);
                        }
                    }
                    pretime = System.nanoTime();
                    int i2 = 0;
                    while (true) {
                        if (i2 < this.enemyNums[this.enemyEvent]) {
                            if (!this.enemies[i2].isEnd()) {
                                enemyBroken = false;
                                break;
                            } else {
                                enemyBroken = true;
                                i2++;
                            }
                        } else {
                            break;
                        }
                    }
                    if (enemyBroken) {
                        isNewEnemy = true;
                        enemyBroken = false;
                        this.enemyEvent = this.enemyEvent + 1;
                    }
                    if (isNewEnemy && this.enemyNums[this.enemyEvent] != 0) {
                        for (int i3 = 0; i3 < this.enemyNums[this.enemyEvent]; i3++) {
                            this.enemyNum = this.enemyNums[this.enemyEvent];
                            this.enemies[i3].setNewEnemy(this.enemyX[this.totalEnemyNum], this.enemyY[this.totalEnemyNum]);
                            this.totalEnemyNum = this.totalEnemyNum + 1;
                        }
                        isNewEnemy = false;
                    }
                    if (!this.isEndingStart && this.enemyNums[this.enemyEvent] == 0) {
                        this.isEndingStart = true;
                        this.endingStartTime = System.currentTimeMillis();
                    }
                    for (int i4 = 0; i4 < this.enemyNum; i4++) {
                        this.enemies[i4].draw(canvas);
                    }
                    for (int i5 = 0; i5 < this.enemyMax; i5++) {
                        if (this.enemyFlowers[i5].isValid()) {
                            this.enemyFlowers[i5].draw(canvas);
                        }
                        if (this.enemyFlowers[i5].isMove()) {
                            this.enemyFlowers[i5].setMovePoint(dttime);
                        }
                    }
                }
                for (int i6 = 0; i6 < this.maxLineNum; i6++) {
                    if (this.flowers[i6].isMove()) {
                        this.flowers[i6].setMovePoint(dttime);
                        reflectAttack(this.flowers[i6].getXp(), this.flowers[i6].getYp(), this.flowers[i6].getVx(), this.flowers[i6].getVy());
                    }
                    if (this.clines[i6].isMove()) {
                        this.clines[i6].setMovePoints(dttime);
                        throwAttack(this.clines[i6].getXn(), this.clines[i6].getYn(), this.clines[i6].getVx(), this.clines[i6].getVy(), i6);
                        if (this.clines[i6].getXn() < ((float) X_MIN)) {
                            if (this.clines[i6].getYn() < ((float) Y_MIN)) {
                                this.flowers[i6].setNewFlower(-this.clines[i6].getVx(), -this.clines[i6].getVy(), this.clines[i6].getXn(), this.clines[i6].getYn());
                                this.clines[i6].setMove(false);
                            } else if (this.clines[i6].getYn() > ((float) Y_MAX)) {
                                this.flowers[i6].setNewFlower(-this.clines[i6].getVx(), -this.clines[i6].getVy(), this.clines[i6].getXn(), this.clines[i6].getYn());
                                this.clines[i6].setMove(false);
                            } else {
                                this.flowers[i6].setNewFlower(-this.clines[i6].getVx(), this.clines[i6].getVy(), this.clines[i6].getXn(), this.clines[i6].getYn());
                                this.clines[i6].setMove(false);
                            }
                        } else if (this.clines[i6].getXn() > ((float) X_MAX)) {
                            if (this.clines[i6].getYn() < ((float) Y_MIN)) {
                                this.flowers[i6].setNewFlower(-this.clines[i6].getVx(), -this.clines[i6].getVy(), this.clines[i6].getXn(), this.clines[i6].getYn());
                                this.clines[i6].setMove(false);
                            } else if (this.clines[i6].getYn() > ((float) Y_MAX)) {
                                this.flowers[i6].setNewFlower(-this.clines[i6].getVx(), -this.clines[i6].getVy(), this.clines[i6].getXn(), this.clines[i6].getYn());
                                this.clines[i6].setMove(false);
                            } else {
                                this.flowers[i6].setNewFlower(-this.clines[i6].getVx(), this.clines[i6].getVy(), this.clines[i6].getXn(), this.clines[i6].getYn());
                                this.clines[i6].setMove(false);
                            }
                        } else if (this.clines[i6].getYn() < ((float) Y_MIN)) {
                            this.flowers[i6].setNewFlower(this.clines[i6].getVx(), -this.clines[i6].getVy(), this.clines[i6].getXn(), this.clines[i6].getYn());
                            this.clines[i6].setMove(false);
                        } else if (this.clines[i6].getYn() > ((float) Y_MAX)) {
                            this.flowers[i6].setNewFlower(this.clines[i6].getVx(), -this.clines[i6].getVy(), this.clines[i6].getXn(), this.clines[i6].getYn());
                            this.clines[i6].setMove(false);
                        }
                    }
                    this.hitScore.draw(canvas, this.preScore, this.attackX, this.attackY);
                }
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                }
                this.mainHolder.unlockCanvasAndPost(canvas);
            } else {
                if (this.throwNum != 0) {
                    acc = (this.attackNum * 100) / this.throwNum;
                } else {
                    acc = 0;
                }
                this.totscore = (int) (((double) (this.score * ((int) (100.0d - ((100.0d - ((double) acc)) / 3.0d))))) / 100.0d);
                resultPoint = Integer.valueOf(this.totscore);
                boolean isTop = true;
                List<Integer> list = FileUtil.readRank();
                if (list != null) {
                    for (Integer i7 : list) {
                        if (this.totscore < i7.intValue()) {
                            isTop = false;
                        }
                    }
                }
                ArrayList arrayList = new ArrayList();
                boolean isin = false;
                if (list != null) {
                    int i8 = 0;
                    while (true) {
                        if (i8 >= list.size()) {
                            break;
                        } else if (this.totscore >= list.get(i8).intValue()) {
                            isin = true;
                            list.add(i8, Integer.valueOf(this.totscore));
                            break;
                        } else {
                            i8++;
                        }
                    }
                    if (!isin) {
                        list.add(Integer.valueOf(this.totscore));
                    }
                    int i9 = 0;
                    while (i9 < list.size() && i9 != 10) {
                        arrayList.add(list.get(i9));
                        i9++;
                    }
                } else {
                    arrayList.add(Integer.valueOf(this.totscore));
                }
                FileUtil.writeRank(arrayList);
                this.scoreBoard.drawLast(canvas, acc, this.totscore, isTop);
                this.mainHolder.unlockCanvasAndPost(canvas);
                this.isRunnable = false;
            }
        }
        this.isActivityEnd = true;
    }

    public void surfaceCreated(SurfaceHolder holder) {
        this.mainHolder = holder;
        this.mainThread = new Thread(this);
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if (this.mainThread != null) {
            X_MAX = width;
            Y_MAX = height;
            createEnemyInfo();
            this.mainThread.start();
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        this.isRunnable = false;
        try {
            this.mainThread.join();
        } catch (InterruptedException e) {
        }
    }
}
