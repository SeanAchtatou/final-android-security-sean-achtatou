package jp.shuji.android.linearcolor;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import java.util.List;

public class RankingActivity extends Activity {
    public static final int RANKING_MAX = 10;
    String[] tit = {" 1:", " 2:", " 3:", " 4:", " 5:", " 6:", " 7:", " 8:", " 9:", "10:"};

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(1);
        setContentView((int) R.layout.ranking);
        List<Integer> list = FileUtil.readRank();
        ConfigBean config = FileUtil.readColors();
        ((LinearLayout) findViewById(R.id.rankBack)).setBackgroundColor(config.getBackColor());
        ((TextView) findViewById(R.id.rankText)).setTextColor(config.getLineColor());
        LinearLayout layout = (LinearLayout) findViewById(R.id.rankingLayout);
        if (list == null) {
            TextView text = new TextView(this);
            text.setTextColor(config.getScoreColor());
            text.setText("No data");
            text.setTextSize(16.0f);
            layout.addView(text);
            return;
        }
        int t = 0;
        for (Integer point : list) {
            TextView text2 = new TextView(this);
            text2.setTextColor(config.getScoreColor());
            if (t < 3) {
                text2.setTextSize(36.0f);
            } else {
                text2.setTextSize(24.0f);
            }
            text2.setLayoutParams(new TableLayout.LayoutParams(-2, -2));
            text2.setText(String.valueOf(this.tit[t]) + " " + Integer.toString(point.intValue()));
            layout.addView(text2);
            t++;
        }
    }

    public void confirm(View button) {
        finish();
    }
}
