package jp.shuji.android.linearcolor;

import android.graphics.Canvas;
import java.lang.reflect.Array;

public class Flower {
    public static final int flowerNum = 4;
    private int color;
    private double decayAlpha = 1.3d;
    private double decayWidth = 1.02d;
    private TraceLine[][] flowerLines;
    private int initAlpha = 255;
    private int initLineWidth = 15;
    private boolean isMove = false;
    private boolean isValid = false;
    private boolean[] isin = new boolean[4];
    private double jrangleaspect = 0.1d;
    private int lastCount = 0;
    private int lastCountMax = 200;
    private int[] lineNums = new int[4];
    private int maxLineNum;
    private double[] vx = new double[4];
    private double[] vy = new double[4];
    private int[] widths = new int[4];
    private float xn;
    private float[] xp = new float[4];
    private float yn;
    private float[] yp = new float[4];

    public boolean isMove() {
        return this.isMove;
    }

    public void setMove(boolean isMove2) {
        this.isMove = isMove2;
    }

    public double[] getVx() {
        return this.vx;
    }

    public void setVx(double[] vx2) {
        this.vx = vx2;
    }

    public double[] getVy() {
        return this.vy;
    }

    public void setVy(double[] vy2) {
        this.vy = vy2;
    }

    public float[] getXp() {
        return this.xp;
    }

    public void setXp(float[] xp2) {
        this.xp = xp2;
    }

    public float[] getYp() {
        return this.yp;
    }

    public void setYp(float[] yp2) {
        this.yp = yp2;
    }

    public boolean isValid() {
        return this.isValid;
    }

    public void setValid(boolean isValid2) {
        this.isValid = isValid2;
    }

    public void init() {
        this.flowerLines = (TraceLine[][]) Array.newInstance(TraceLine.class, 4, this.maxLineNum);
        for (int i = 0; i < 4; i++) {
            this.lineNums[i] = 0;
            this.widths[i] = this.initLineWidth;
            for (int j = 0; j < this.maxLineNum; j++) {
                this.flowerLines[i][j] = new TraceLine(this.color, this.initAlpha, this.initLineWidth, this.decayAlpha, this.decayWidth);
            }
            this.vx[i] = 0.0d;
            this.vy[i] = 0.0d;
            this.xp[i] = -50.0f;
            this.yp[i] = -50.0f;
        }
    }

    public Flower(int lineMax, int lineColor, int lineWidth) {
        this.initLineWidth = lineWidth;
        this.maxLineNum = lineMax;
        this.color = lineColor;
        this.flowerLines = (TraceLine[][]) Array.newInstance(TraceLine.class, 4, this.maxLineNum);
        for (int i = 0; i < 4; i++) {
            this.lineNums[i] = 0;
            this.widths[i] = this.initLineWidth;
            for (int j = 0; j < this.maxLineNum; j++) {
                this.flowerLines[i][j] = new TraceLine(this.color, this.initAlpha, this.initLineWidth, this.decayAlpha, this.decayWidth);
            }
        }
    }

    public void setNewFlower(double vx2, double vy2, float x0, float y0) {
        this.isValid = true;
        this.isMove = true;
        this.lastCount = 0;
        for (int i = 0; i < 4; i++) {
            this.lineNums[i] = 0;
            this.widths[i] = this.initLineWidth;
            this.isin[i] = false;
            for (int j = 0; j < this.maxLineNum; j++) {
                this.flowerLines[i][j] = new TraceLine(this.color, this.initAlpha, this.initLineWidth, this.decayAlpha, this.decayWidth);
            }
        }
        this.vx[0] = (0.6d * vx2) / 2.5d;
        this.vy[0] = (1.4d * vy2) / 2.5d;
        this.vx[1] = (0.8d * vx2) / 2.0d;
        this.vy[1] = (1.2d * vy2) / 2.0d;
        this.vx[2] = (1.2d * vx2) / 2.0d;
        this.vy[2] = (0.8d * vy2) / 2.0d;
        this.vx[3] = (1.4d * vx2) / 2.5d;
        this.vy[3] = (0.6d * vy2) / 2.5d;
        if (Math.abs(vx2 / vy2) < this.jrangleaspect) {
            this.vx[0] = vy2 / 2.0d;
            this.vx[1] = vy2 / 4.0d;
            this.vx[2] = (-vy2) / 4.0d;
            this.vx[3] = (-vy2) / 2.0d;
            this.vy[0] = vy2 / 4.0d;
            this.vy[1] = vy2 / 2.5d;
            this.vy[2] = vy2 / 2.5d;
            this.vy[3] = vy2 / 4.0d;
        }
        if (Math.abs(vy2 / vx2) < this.jrangleaspect) {
            this.vx[0] = vx2 / 4.0d;
            this.vx[1] = vx2 / 2.5d;
            this.vx[2] = vx2 / 2.5d;
            this.vx[3] = vx2 / 4.0d;
            this.vy[0] = vx2 / 2.0d;
            this.vy[1] = vx2 / 4.0d;
            this.vy[2] = (-vx2) / 4.0d;
            this.vy[3] = (-vx2) / 2.0d;
        }
        this.xp[0] = x0;
        this.xp[1] = x0;
        this.xp[2] = x0;
        this.xp[3] = x0;
        this.yp[0] = y0;
        this.yp[1] = y0;
        this.yp[2] = y0;
        this.yp[3] = y0;
    }

    public void setMovePoint(long nanoTime) {
        if (this.isMove) {
            for (int i = 0; i < 4; i++) {
                this.xn = this.xp[i] + ((float) (this.vx[i] * ((double) nanoTime)));
                this.yn = this.yp[i] + ((float) (this.vy[i] * ((double) nanoTime)));
                double[] dArr = this.vx;
                dArr[i] = dArr[i] * 0.9d;
                double[] dArr2 = this.vy;
                dArr2[i] = dArr2[i] * 0.9d;
                int[] iArr = this.widths;
                iArr[i] = iArr[i] / 2;
                TraceLine[] traceLineArr = this.flowerLines[i];
                int[] iArr2 = this.lineNums;
                int i2 = iArr2[i];
                iArr2[i] = i2 + 1;
                traceLineArr[i2].setPoints(this.xp[i], this.yp[i], this.xn, this.yn, this.widths[i]);
                this.xp[i] = this.xn;
                this.yp[i] = this.yn;
                if (this.xn > ((float) NyororiSurface.X_MIN) && this.xn < ((float) NyororiSurface.X_MAX) && this.yn > ((float) NyororiSurface.Y_MIN) && this.yn < ((float) NyororiSurface.Y_MAX)) {
                    this.isin[i] = true;
                }
                if (this.isin[i]) {
                    reflect((double) this.xn, (double) this.yn, i);
                }
                if (this.lineNums[i] == this.maxLineNum) {
                    this.lineNums[i] = 0;
                    this.isMove = false;
                    this.lastCount = 0;
                }
            }
        }
    }

    private void reflect(double x, double y, int idx) {
        if (x < ((double) NyororiSurface.X_MIN)) {
            if (y < ((double) NyororiSurface.Y_MIN)) {
                this.isin[idx] = false;
                double[] dArr = this.vx;
                dArr[idx] = dArr[idx] * -0.5d;
                double[] dArr2 = this.vy;
                dArr2[idx] = dArr2[idx] * -0.5d;
            } else if (y > ((double) NyororiSurface.Y_MAX)) {
                this.isin[idx] = false;
                double[] dArr3 = this.vx;
                dArr3[idx] = dArr3[idx] * -0.5d;
                double[] dArr4 = this.vy;
                dArr4[idx] = dArr4[idx] * -0.5d;
            } else {
                this.isin[idx] = false;
                double[] dArr5 = this.vx;
                dArr5[idx] = dArr5[idx] * -0.5d;
                double[] dArr6 = this.vy;
                dArr6[idx] = dArr6[idx] * 0.5d;
            }
        } else if (x > ((double) NyororiSurface.X_MAX)) {
            if (y < ((double) NyororiSurface.Y_MIN)) {
                this.isin[idx] = false;
                double[] dArr7 = this.vx;
                dArr7[idx] = dArr7[idx] * -0.5d;
                double[] dArr8 = this.vy;
                dArr8[idx] = dArr8[idx] * -0.5d;
            } else if (y > ((double) NyororiSurface.Y_MAX)) {
                this.isin[idx] = false;
                double[] dArr9 = this.vx;
                dArr9[idx] = dArr9[idx] * -0.5d;
                double[] dArr10 = this.vy;
                dArr10[idx] = dArr10[idx] * -0.5d;
            } else {
                this.isin[idx] = false;
                double[] dArr11 = this.vx;
                dArr11[idx] = dArr11[idx] * -0.5d;
                double[] dArr12 = this.vy;
                dArr12[idx] = dArr12[idx] * 0.5d;
            }
        } else if (y < ((double) NyororiSurface.Y_MIN)) {
            this.isin[idx] = false;
            double[] dArr13 = this.vx;
            dArr13[idx] = dArr13[idx] * 0.5d;
            double[] dArr14 = this.vy;
            dArr14[idx] = dArr14[idx] * -0.5d;
        } else if (y > ((double) NyororiSurface.Y_MAX)) {
            this.isin[idx] = false;
            double[] dArr15 = this.vx;
            dArr15[idx] = dArr15[idx] * 0.5d;
            double[] dArr16 = this.vy;
            dArr16[idx] = dArr16[idx] * -0.5d;
        }
    }

    public void draw(Canvas canvas) {
        if (this.isValid) {
            if (!this.isMove) {
                this.lastCount++;
                if (this.lastCount >= this.lastCountMax) {
                    this.isValid = false;
                }
            }
            for (int i = 0; i < 4; i++) {
                for (TraceLine line : this.flowerLines[i]) {
                    if (!line.isValid()) {
                        break;
                    }
                    line.draw(canvas);
                }
            }
        }
    }
}
