package jp.shuji.android.linearcolor;

public class ConfigBean {
    private int backColor;
    private int enemyColor;
    private int lineColor;
    private int scoreColor;

    public int getBackColor() {
        return this.backColor;
    }

    public void setBackColor(int backColor2) {
        this.backColor = backColor2;
    }

    public int getLineColor() {
        return this.lineColor;
    }

    public void setLineColor(int lineColor2) {
        this.lineColor = lineColor2;
    }

    public int getEnemyColor() {
        return this.enemyColor;
    }

    public void setEnemyColor(int enemyColor2) {
        this.enemyColor = enemyColor2;
    }

    public int getScoreColor() {
        return this.scoreColor;
    }

    public void setScoreColor(int scoreColor2) {
        this.scoreColor = scoreColor2;
    }
}
