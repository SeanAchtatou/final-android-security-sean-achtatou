package jp.shuji.android.linearcolor;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class FileUtil {
    private static String BASE_FOLDER = null;
    private static final String CONFIG_FILE = "config.dat";
    private static final String RANKING_FILE = "ranking.dat";

    public static void init(Context context) {
        BASE_FOLDER = String.valueOf(Environment.getDataDirectory().getPath()) + "/data/" + context.getPackageName() + "/data";
        File base = new File(BASE_FOLDER);
        if (!base.exists()) {
            base.mkdir();
        }
    }

    public static ConfigBean readColors() {
        ConfigBean config = new ConfigBean();
        int lineNum = 0;
        try {
            BufferedReader configReader = new BufferedReader(new InputStreamReader(new FileInputStream(String.valueOf(BASE_FOLDER) + "/" + CONFIG_FILE)));
            while (true) {
                String configData = configReader.readLine();
                if (configData == null) {
                    break;
                } else if (!configData.equals("")) {
                    if (lineNum == 0) {
                        config.setBackColor(Integer.parseInt(configData));
                    } else if (lineNum == 1) {
                        config.setLineColor(Integer.parseInt(configData));
                    } else if (lineNum == 2) {
                        config.setEnemyColor(Integer.parseInt(configData));
                    } else if (lineNum == 3) {
                        config.setScoreColor(Integer.parseInt(configData));
                    }
                    lineNum++;
                }
            }
        } catch (NumberFormatException e) {
            config.setBackColor(-256);
            config.setLineColor(-65536);
            config.setEnemyColor(-16777216);
            config.setScoreColor(-16777216);
        } catch (FileNotFoundException e2) {
            Log.w("nyorori", "file not found, but initialization is ok", e2);
            config.setBackColor(-256);
            config.setLineColor(-65536);
            config.setEnemyColor(-16777216);
            config.setScoreColor(-16777216);
            writeColors(config);
        } catch (IOException e3) {
            Log.e("nyorori", "file read or file close error", e3);
            config.setBackColor(-256);
            config.setLineColor(-65536);
            config.setEnemyColor(-16777216);
            config.setScoreColor(-16777216);
        }
        return config;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException} */
    public static void writeColors(ConfigBean config) {
        try {
            BufferedWriter configWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(String.valueOf(BASE_FOLDER) + "/" + CONFIG_FILE, false)));
            configWriter.write(Integer.toString(config.getBackColor()));
            configWriter.newLine();
            configWriter.write(Integer.toString(config.getLineColor()));
            configWriter.newLine();
            configWriter.write(Integer.toString(config.getEnemyColor()));
            configWriter.newLine();
            configWriter.write(Integer.toString(config.getScoreColor()));
            configWriter.newLine();
            configWriter.flush();
            configWriter.close();
        } catch (IOException e) {
            Log.e("nyorori", "file io error", e);
        }
    }

    public static List<Integer> readRank() {
        List<Integer> ranking = new ArrayList<>();
        try {
            BufferedReader rankReader = new BufferedReader(new InputStreamReader(new FileInputStream(String.valueOf(BASE_FOLDER) + "/" + RANKING_FILE)));
            while (true) {
                String rankingData = rankReader.readLine();
                if (rankingData == null) {
                    return ranking;
                }
                if (!rankingData.equals("")) {
                    ranking.add(Integer.valueOf(Integer.parseInt(rankingData)));
                }
            }
        } catch (FileNotFoundException e) {
            return null;
        } catch (IOException e2) {
            Log.e("nyorori", "file io error", e2);
            return ranking;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException} */
    public static void writeRank(List<Integer> ranking) {
        try {
            BufferedWriter rankWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(String.valueOf(BASE_FOLDER) + "/" + RANKING_FILE, false)));
            for (Integer ri : ranking) {
                rankWriter.write(Integer.toString(ri.intValue()));
                rankWriter.newLine();
            }
            rankWriter.flush();
            rankWriter.close();
        } catch (IOException e) {
            Log.e("nyorori", "file io error", e);
        }
    }
}
