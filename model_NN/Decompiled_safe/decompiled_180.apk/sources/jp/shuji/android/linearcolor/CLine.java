package jp.shuji.android.linearcolor;

import android.graphics.Canvas;

public class CLine {
    private int color;
    private double compensation = 1.0d;
    private double decayAlpha = 1.05d;
    private double decayWidth = 1.05d;
    private int initAlpha = 255;
    private int initLineWidth = 35;
    private boolean isMove = false;
    private boolean isValid = false;
    private int lineNum = 0;
    private TraceLine[] lines;
    private int maxLineNum;
    long time = 0;
    private double vmaxn = -3.0E-6d;
    private double vmaxp = 3.0E-6d;
    private double vx = 0.0d;
    private double vy = 0.0d;
    private float xn = 0.0f;
    private float xp = 0.0f;
    private float yn = 0.0f;
    private float yp = 0.0f;

    public double getVx() {
        return this.vx;
    }

    public double getVy() {
        return this.vy;
    }

    public boolean isValid() {
        return this.isValid;
    }

    public void setValid(boolean isValid2) {
        this.isValid = isValid2;
    }

    public boolean isMove() {
        return this.isMove;
    }

    public void setMove(boolean isMove2) {
        this.isMove = isMove2;
    }

    public CLine(int lineMax, int lineColor) {
        this.maxLineNum = lineMax;
        this.color = lineColor;
        this.lines = new TraceLine[this.maxLineNum];
        for (int i = 0; i < lineMax; i++) {
            this.lines[i] = new TraceLine(this.color, this.initAlpha, this.initLineWidth, this.decayAlpha, this.decayWidth);
        }
    }

    public void setLinePoints(float x0, float y0, float x1, float y1, long nanoTime) {
        this.isValid = true;
        TraceLine[] traceLineArr = this.lines;
        int i = this.lineNum;
        this.lineNum = i + 1;
        traceLineArr[i].setPoints(x0, y0, x1, y1, this.initLineWidth);
        this.xp = x1;
        this.yp = y1;
        this.vx = ((double) (x1 - x0)) / ((double) nanoTime);
        this.vy = ((double) (y1 - y0)) / ((double) nanoTime);
        if (this.vx > this.vmaxp) {
            this.vx = this.vmaxp;
        } else if (this.vx < this.vmaxn) {
            this.vx = this.vmaxn;
        }
        if (this.vy > this.vmaxp) {
            this.vy = this.vmaxp;
        } else if (this.vy < this.vmaxn) {
            this.vy = this.vmaxn;
        }
        if (this.lineNum == this.maxLineNum) {
            this.lineNum = 0;
        }
    }

    public void setMovePoints(long nanoTime) {
        this.xn = this.xp + ((float) (this.compensation * this.vx * ((double) nanoTime)));
        this.yn = this.yp + ((float) (this.compensation * this.vy * ((double) nanoTime)));
        TraceLine[] traceLineArr = this.lines;
        int i = this.lineNum;
        this.lineNum = i + 1;
        traceLineArr[i].setPoints(this.xp, this.yp, this.xn, this.yn, this.initLineWidth);
        this.xp = this.xn;
        this.yp = this.yn;
        if (this.lineNum == this.maxLineNum) {
            this.lineNum = 0;
        }
    }

    public float getXn() {
        return this.xn;
    }

    public float getYn() {
        return this.yn;
    }

    public void draw(Canvas canvas) {
        TraceLine[] traceLineArr = this.lines;
        int length = traceLineArr.length;
        int i = 0;
        while (i < length) {
            TraceLine line = traceLineArr[i];
            if (line.isValid()) {
                line.draw(canvas);
                i++;
            } else {
                return;
            }
        }
    }
}
