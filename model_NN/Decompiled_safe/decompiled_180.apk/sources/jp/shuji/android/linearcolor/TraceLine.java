package jp.shuji.android.linearcolor;

import android.graphics.Canvas;
import android.graphics.Paint;

public class TraceLine {
    private int alpha = this.initAlpha;
    private double decayAlpha = 1.05d;
    private double decayWidth = 1.05d;
    private int initAlpha = 255;
    private int initLineWidth = 35;
    private boolean isValid = false;
    private int lineWidth = this.initLineWidth;
    private Paint paint = null;
    private float x0;
    private float x1;
    private float y0;
    private float y1;

    public boolean isValid() {
        return this.isValid;
    }

    public void setValid(boolean isValid2) {
        this.isValid = isValid2;
    }

    public TraceLine(int color, int initAlpha2, int initLineWidth2, double decayAlpha2, double decayWidth2) {
        this.initAlpha = initAlpha2;
        this.initLineWidth = initLineWidth2;
        this.decayAlpha = decayAlpha2;
        this.decayWidth = decayWidth2;
        this.paint = new Paint();
        this.paint.setColor(color);
        this.paint.setStrokeWidth((float) initLineWidth2);
        this.paint.setAlpha(this.alpha);
        this.paint.setAntiAlias(true);
        this.paint.setStyle(Paint.Style.FILL_AND_STROKE);
        this.paint.setStrokeCap(Paint.Cap.ROUND);
        this.paint.setStrokeJoin(Paint.Join.ROUND);
        this.paint.setDither(true);
    }

    public void setPoints(float x02, float y02, float x12, float y12, int width) {
        this.x0 = x02;
        this.y0 = y02;
        this.x1 = x12;
        this.y1 = y12;
        this.alpha = this.initAlpha;
        this.lineWidth = this.initLineWidth;
        this.isValid = true;
    }

    public void decay() {
        this.alpha = (int) (((double) this.alpha) / this.decayAlpha);
        this.lineWidth = (int) (((double) this.lineWidth) / this.decayWidth);
        if (this.lineWidth <= 0) {
            this.lineWidth = 1;
        }
        this.paint.setStrokeWidth((float) this.lineWidth);
        this.paint.setAlpha(this.alpha);
    }

    public void draw(Canvas canvas) {
        canvas.drawLine(this.x0, this.y0, this.x1, this.y1, this.paint);
        decay();
    }

    public int getAlpha() {
        return this.alpha;
    }

    public void setAlpha(int alpha2) {
        this.alpha = alpha2;
    }
}
