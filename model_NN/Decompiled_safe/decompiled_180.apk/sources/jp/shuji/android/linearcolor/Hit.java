package jp.shuji.android.linearcolor;

import android.graphics.Canvas;
import android.graphics.Paint;

public class Hit {
    private int alpha;
    private String message;
    private Paint paint;
    private float x0;
    private float y0;

    public Hit(int color) {
        this.paint = null;
        this.message = null;
        this.alpha = 255;
        this.paint = new Paint();
        this.paint.setColor(color);
        this.paint.setAntiAlias(true);
        this.paint.setTextSize(36.0f);
        this.paint.setAlpha(this.alpha);
    }

    public void draw(Canvas canvas, int score, float x, float y) {
        this.alpha -= 3;
        if (this.alpha < 0) {
            this.alpha = 0;
        }
        if (!(this.x0 == x && this.y0 == y)) {
            this.alpha = 255;
        }
        this.paint.setAlpha(this.alpha);
        this.message = "+" + score;
        canvas.drawText(this.message, x - (this.paint.measureText(this.message) / 2.0f), y, this.paint);
        this.x0 = x;
        this.y0 = y;
    }
}
