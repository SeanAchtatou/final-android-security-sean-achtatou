package jp.shuji.android.linearcolor;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class NyororiActivity extends Activity {
    private static int points;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(1);
        setContentView(new NyororiSurface(this));
    }

    public static void ending(int point) {
        points = point;
    }

    public void onDestroy() {
        Intent intent = new Intent();
        intent.putExtra("points", Integer.toString(points));
        setResult(-1, intent);
        super.onDestroy();
    }
}
