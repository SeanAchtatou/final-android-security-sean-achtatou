package jp.shuji.android.linearcolor;

import android.graphics.Canvas;
import android.graphics.Paint;

public class Enemy {
    private int alpha;
    private boolean isEnd = false;
    private boolean isValid = false;
    private Paint paint = null;
    private float r = 15.0f;
    private int randomTime;
    private long validTime;
    private float x0;
    private float y0;

    public boolean isEnd() {
        return this.isEnd;
    }

    public boolean isValid() {
        return this.isValid;
    }

    public Enemy(float r2, int color) {
        this.r = r2;
        this.paint = new Paint();
        this.paint.setColor(color);
        this.paint.setAntiAlias(true);
        this.paint.setDither(true);
    }

    public void setNewEnemy(float x02, float y02) {
        this.x0 = x02;
        this.y0 = y02;
        this.alpha = 0;
        this.isValid = false;
        this.isEnd = false;
    }

    public void draw(Canvas canvas) {
        if (this.isEnd) {
            this.paint.setAlpha(0);
            canvas.drawCircle(this.x0, this.y0, this.r, this.paint);
            return;
        }
        if (!this.isValid) {
            this.alpha += 20;
            if (this.alpha > 255) {
                this.alpha = 255;
            }
            this.paint.setAlpha(this.alpha);
            if (this.alpha == 255) {
                this.isValid = true;
                this.validTime = System.currentTimeMillis();
                this.randomTime = ((int) (4000.0d * Math.random())) + 2000;
            }
        }
        if (this.isValid && System.currentTimeMillis() - this.validTime > ((long) this.randomTime)) {
            this.paint.setAlpha(this.alpha);
            this.alpha -= 10;
            if (this.alpha < 50) {
                this.isValid = false;
                if (!this.isEnd) {
                    NyororiSurface.eraseEnemy();
                }
                this.isEnd = true;
            }
        }
        canvas.drawCircle(this.x0, this.y0, this.r, this.paint);
    }

    public float getX0() {
        return this.x0;
    }

    public void setX0(float x02) {
        this.x0 = x02;
    }

    public float getY0() {
        return this.y0;
    }

    public void setY0(float y02) {
        this.y0 = y02;
    }

    public void setEnd(boolean isEnd2) {
        this.isEnd = isEnd2;
        this.isValid = !isEnd2;
    }
}
