package jp.shuji.android.linearcolor;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int icon = 2130837504;
    }

    public static final class id {
        public static final int mainBack = 2131034112;
        public static final int mainColors = 2131034116;
        public static final int mainRanking = 2131034117;
        public static final int mainStartEasy = 2131034114;
        public static final int mainStartHard = 2131034115;
        public static final int mainTitle = 2131034113;
        public static final int rankBack = 2131034118;
        public static final int rankText = 2131034119;
        public static final int rankingLayout = 2131034120;
    }

    public static final class layout {
        public static final int main = 2130903040;
        public static final int ranking = 2130903041;
    }

    public static final class string {
        public static final int app_name = 2130968576;
        public static final int attack = 2130968578;
        public static final int back = 2130968577;
        public static final int enemy = 2130968579;
        public static final int score = 2130968580;
    }
}
