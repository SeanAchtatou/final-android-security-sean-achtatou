package com.google.android.apps.analytics;

import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import com.google.android.apps.analytics.Dispatcher;
import com.google.android.apps.analytics.PipelinedRequester;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Locale;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;

class NetworkDispatcher implements Dispatcher {
    /* access modifiers changed from: private */
    public static final HttpHost GOOGLE_ANALYTICS_HOST = new HttpHost("www.google-analytics.com", 80);
    private static final int MAX_EVENTS_PER_PIPELINE = 30;
    private static final int MAX_SEQUENTIAL_REQUESTS = 5;
    private static final long MIN_RETRY_INTERVAL = 2;
    private static final String USER_AGENT_TEMPLATE = "%s/%s (Linux; U; Android %s; %s-%s; %s Build/%s)";
    private DispatcherThread dispatcherThread;
    private boolean dryRun;
    private final String userAgent;

    private static class DispatcherThread extends HandlerThread {
        /* access modifiers changed from: private */
        public final Dispatcher.Callbacks callbacks;
        /* access modifiers changed from: private */
        public AsyncDispatchTask currentTask;
        private Handler handlerExecuteOnDispatcherThread;
        /* access modifiers changed from: private */
        public int lastStatusCode;
        /* access modifiers changed from: private */
        public int maxEventsPerRequest;
        /* access modifiers changed from: private */
        public final NetworkDispatcher parent;
        /* access modifiers changed from: private */
        public final PipelinedRequester pipelinedRequester;
        /* access modifiers changed from: private */
        public final String referrer;
        /* access modifiers changed from: private */
        public final RequesterCallbacks requesterCallBacks;
        /* access modifiers changed from: private */
        public long retryInterval;
        /* access modifiers changed from: private */
        public final String userAgent;

        private class AsyncDispatchTask implements Runnable {
            private final LinkedList<Event> events = new LinkedList<>();

            public AsyncDispatchTask(Event[] eventArr) {
                Collections.addAll(this.events, eventArr);
            }

            /* JADX WARNING: Removed duplicated region for block: B:27:0x00c7  */
            /* JADX WARNING: Removed duplicated region for block: B:33:? A[RETURN, SYNTHETIC] */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            private void dispatchSomePendingEvents(boolean r5) throws java.io.IOException, org.apache.http.ParseException, org.apache.http.HttpException {
                /*
                    r4 = this;
                    com.google.android.apps.analytics.GoogleAnalyticsTracker r0 = com.google.android.apps.analytics.GoogleAnalyticsTracker.getInstance()
                    boolean r0 = r0.getDebug()
                    if (r0 == 0) goto L_0x0013
                    if (r5 == 0) goto L_0x0013
                    java.lang.String r0 = "GoogleAnalyticsTracker"
                    java.lang.String r1 = "dispatching events in dry run mode"
                    android.util.Log.v(r0, r1)
                L_0x0013:
                    r0 = 0
                    r1 = r0
                L_0x0015:
                    java.util.LinkedList<com.google.android.apps.analytics.Event> r0 = r4.events
                    int r0 = r0.size()
                    if (r1 >= r0) goto L_0x00c5
                    com.google.android.apps.analytics.NetworkDispatcher$DispatcherThread r0 = com.google.android.apps.analytics.NetworkDispatcher.DispatcherThread.this
                    int r0 = r0.maxEventsPerRequest
                    if (r1 >= r0) goto L_0x00c5
                    java.util.LinkedList<com.google.android.apps.analytics.Event> r0 = r4.events
                    java.lang.Object r0 = r0.get(r1)
                    com.google.android.apps.analytics.Event r0 = (com.google.android.apps.analytics.Event) r0
                    java.lang.String r2 = "__##GOOGLEPAGEVIEW##__"
                    java.lang.String r3 = r0.category
                    boolean r2 = r2.equals(r3)
                    if (r2 == 0) goto L_0x0086
                    com.google.android.apps.analytics.NetworkDispatcher$DispatcherThread r2 = com.google.android.apps.analytics.NetworkDispatcher.DispatcherThread.this
                    java.lang.String r2 = r2.referrer
                    java.lang.String r0 = com.google.android.apps.analytics.NetworkRequestUtil.constructPageviewRequestPath(r0, r2)
                L_0x0041:
                    org.apache.http.message.BasicHttpRequest r2 = new org.apache.http.message.BasicHttpRequest
                    java.lang.String r3 = "GET"
                    r2.<init>(r3, r0)
                    java.lang.String r0 = "Host"
                    org.apache.http.HttpHost r3 = com.google.android.apps.analytics.NetworkDispatcher.GOOGLE_ANALYTICS_HOST
                    java.lang.String r3 = r3.getHostName()
                    r2.addHeader(r0, r3)
                    java.lang.String r0 = "User-Agent"
                    com.google.android.apps.analytics.NetworkDispatcher$DispatcherThread r3 = com.google.android.apps.analytics.NetworkDispatcher.DispatcherThread.this
                    java.lang.String r3 = r3.userAgent
                    r2.addHeader(r0, r3)
                    com.google.android.apps.analytics.GoogleAnalyticsTracker r0 = com.google.android.apps.analytics.GoogleAnalyticsTracker.getInstance()
                    boolean r0 = r0.getDebug()
                    if (r0 == 0) goto L_0x0077
                    java.lang.String r0 = "GoogleAnalyticsTracker"
                    org.apache.http.RequestLine r3 = r2.getRequestLine()
                    java.lang.String r3 = r3.toString()
                    android.util.Log.i(r0, r3)
                L_0x0077:
                    if (r5 == 0) goto L_0x00bb
                    com.google.android.apps.analytics.NetworkDispatcher$DispatcherThread r0 = com.google.android.apps.analytics.NetworkDispatcher.DispatcherThread.this
                    com.google.android.apps.analytics.NetworkDispatcher$DispatcherThread$RequesterCallbacks r0 = r0.requesterCallBacks
                    r0.requestSent()
                L_0x0082:
                    int r0 = r1 + 1
                    r1 = r0
                    goto L_0x0015
                L_0x0086:
                    java.lang.String r2 = "__##GOOGLETRANSACTION##__"
                    java.lang.String r3 = r0.category
                    boolean r2 = r2.equals(r3)
                    if (r2 == 0) goto L_0x009b
                    com.google.android.apps.analytics.NetworkDispatcher$DispatcherThread r2 = com.google.android.apps.analytics.NetworkDispatcher.DispatcherThread.this
                    java.lang.String r2 = r2.referrer
                    java.lang.String r0 = com.google.android.apps.analytics.NetworkRequestUtil.constructTransactionRequestPath(r0, r2)
                    goto L_0x0041
                L_0x009b:
                    java.lang.String r2 = "__##GOOGLEITEM##__"
                    java.lang.String r3 = r0.category
                    boolean r2 = r2.equals(r3)
                    if (r2 == 0) goto L_0x00b0
                    com.google.android.apps.analytics.NetworkDispatcher$DispatcherThread r2 = com.google.android.apps.analytics.NetworkDispatcher.DispatcherThread.this
                    java.lang.String r2 = r2.referrer
                    java.lang.String r0 = com.google.android.apps.analytics.NetworkRequestUtil.constructItemRequestPath(r0, r2)
                    goto L_0x0041
                L_0x00b0:
                    com.google.android.apps.analytics.NetworkDispatcher$DispatcherThread r2 = com.google.android.apps.analytics.NetworkDispatcher.DispatcherThread.this
                    java.lang.String r2 = r2.referrer
                    java.lang.String r0 = com.google.android.apps.analytics.NetworkRequestUtil.constructEventRequestPath(r0, r2)
                    goto L_0x0041
                L_0x00bb:
                    com.google.android.apps.analytics.NetworkDispatcher$DispatcherThread r0 = com.google.android.apps.analytics.NetworkDispatcher.DispatcherThread.this
                    com.google.android.apps.analytics.PipelinedRequester r0 = r0.pipelinedRequester
                    r0.addRequest(r2)
                    goto L_0x0082
                L_0x00c5:
                    if (r5 != 0) goto L_0x00d0
                    com.google.android.apps.analytics.NetworkDispatcher$DispatcherThread r0 = com.google.android.apps.analytics.NetworkDispatcher.DispatcherThread.this
                    com.google.android.apps.analytics.PipelinedRequester r0 = r0.pipelinedRequester
                    r0.sendRequests()
                L_0x00d0:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.NetworkDispatcher.DispatcherThread.AsyncDispatchTask.dispatchSomePendingEvents(boolean):void");
            }

            public Event removeNextEvent() {
                return this.events.poll();
            }

            public void run() {
                AsyncDispatchTask unused = DispatcherThread.this.currentTask = this;
                int i = 0;
                while (i < 5 && this.events.size() > 0) {
                    long j = 0;
                    try {
                        if (DispatcherThread.this.lastStatusCode == 500 || DispatcherThread.this.lastStatusCode == 503) {
                            j = (long) (Math.random() * ((double) DispatcherThread.this.retryInterval));
                            if (DispatcherThread.this.retryInterval < 256) {
                                DispatcherThread.access$630(DispatcherThread.this, NetworkDispatcher.MIN_RETRY_INTERVAL);
                            }
                        } else {
                            long unused2 = DispatcherThread.this.retryInterval = NetworkDispatcher.MIN_RETRY_INTERVAL;
                        }
                        Thread.sleep(j * 1000);
                        dispatchSomePendingEvents(DispatcherThread.this.parent.isDryRun());
                        i++;
                    } catch (InterruptedException e) {
                        Log.w(GoogleAnalyticsTracker.LOG_TAG, "Couldn't sleep.", e);
                    } catch (IOException e2) {
                        Log.w(GoogleAnalyticsTracker.LOG_TAG, "Problem with socket or streams.", e2);
                    } catch (HttpException e3) {
                        Log.w(GoogleAnalyticsTracker.LOG_TAG, "Problem with http streams.", e3);
                    }
                }
                DispatcherThread.this.pipelinedRequester.finishedCurrentRequests();
                DispatcherThread.this.callbacks.dispatchFinished();
                AsyncDispatchTask unused3 = DispatcherThread.this.currentTask = null;
            }
        }

        private class RequesterCallbacks implements PipelinedRequester.Callbacks {
            private RequesterCallbacks() {
            }

            public void pipelineModeChanged(boolean z) {
                if (z) {
                    int unused = DispatcherThread.this.maxEventsPerRequest = NetworkDispatcher.MAX_EVENTS_PER_PIPELINE;
                } else {
                    int unused2 = DispatcherThread.this.maxEventsPerRequest = 1;
                }
            }

            public void requestSent() {
                Event removeNextEvent;
                if (DispatcherThread.this.currentTask != null && (removeNextEvent = DispatcherThread.this.currentTask.removeNextEvent()) != null) {
                    DispatcherThread.this.callbacks.eventDispatched(removeNextEvent.eventId);
                }
            }

            public void serverError(int i) {
                int unused = DispatcherThread.this.lastStatusCode = i;
            }
        }

        private DispatcherThread(Dispatcher.Callbacks callbacks2, PipelinedRequester pipelinedRequester2, String str, String str2, NetworkDispatcher networkDispatcher) {
            super("DispatcherThread");
            this.maxEventsPerRequest = NetworkDispatcher.MAX_EVENTS_PER_PIPELINE;
            this.currentTask = null;
            this.callbacks = callbacks2;
            this.referrer = str;
            this.userAgent = str2;
            this.pipelinedRequester = pipelinedRequester2;
            this.requesterCallBacks = new RequesterCallbacks();
            this.pipelinedRequester.installCallbacks(this.requesterCallBacks);
            this.parent = networkDispatcher;
        }

        private DispatcherThread(Dispatcher.Callbacks callbacks2, String str, String str2, NetworkDispatcher networkDispatcher) {
            this(callbacks2, new PipelinedRequester(NetworkDispatcher.GOOGLE_ANALYTICS_HOST), str, str2, networkDispatcher);
        }

        static /* synthetic */ long access$630(DispatcherThread dispatcherThread, long j) {
            long j2 = dispatcherThread.retryInterval * j;
            dispatcherThread.retryInterval = j2;
            return j2;
        }

        public void dispatchEvents(Event[] eventArr) {
            if (this.handlerExecuteOnDispatcherThread != null) {
                this.handlerExecuteOnDispatcherThread.post(new AsyncDispatchTask(eventArr));
            }
        }

        /* access modifiers changed from: protected */
        public void onLooperPrepared() {
            this.handlerExecuteOnDispatcherThread = new Handler();
        }
    }

    public NetworkDispatcher() {
        this(GoogleAnalyticsTracker.PRODUCT, GoogleAnalyticsTracker.VERSION);
    }

    public NetworkDispatcher(String str, String str2) {
        this.dryRun = false;
        Locale locale = Locale.getDefault();
        Object[] objArr = new Object[7];
        objArr[0] = str;
        objArr[1] = str2;
        objArr[2] = Build.VERSION.RELEASE;
        objArr[3] = locale.getLanguage() != null ? locale.getLanguage().toLowerCase() : "en";
        objArr[4] = locale.getCountry() != null ? locale.getCountry().toLowerCase() : "";
        objArr[5] = Build.MODEL;
        objArr[6] = Build.ID;
        this.userAgent = String.format(USER_AGENT_TEMPLATE, objArr);
    }

    public void dispatchEvents(Event[] eventArr) {
        if (this.dispatcherThread != null) {
            this.dispatcherThread.dispatchEvents(eventArr);
        }
    }

    /* access modifiers changed from: package-private */
    public String getUserAgent() {
        return this.userAgent;
    }

    public void init(Dispatcher.Callbacks callbacks, PipelinedRequester pipelinedRequester, String str) {
        stop();
        this.dispatcherThread = new DispatcherThread(callbacks, pipelinedRequester, str, this.userAgent, this);
        this.dispatcherThread.start();
    }

    public void init(Dispatcher.Callbacks callbacks, String str) {
        stop();
        this.dispatcherThread = new DispatcherThread(callbacks, str, this.userAgent, this);
        this.dispatcherThread.start();
    }

    public boolean isDryRun() {
        return this.dryRun;
    }

    public void setDryRun(boolean z) {
        this.dryRun = z;
    }

    public void stop() {
        if (this.dispatcherThread != null && this.dispatcherThread.getLooper() != null) {
            this.dispatcherThread.getLooper().quit();
            this.dispatcherThread = null;
        }
    }

    public void waitForThreadLooper() {
        this.dispatcherThread.getLooper();
    }
}
