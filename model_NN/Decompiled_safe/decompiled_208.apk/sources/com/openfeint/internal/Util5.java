package com.openfeint.internal;

import android.content.Context;

public class Util5 {
    public static String getAccountNameEclair(Context ctx) {
        try {
            Class<?> cAccountManager = Class.forName("android.accounts.AccountManager");
            Object accountManager = cAccountManager.getMethod("get", Context.class).invoke(cAccountManager, ctx);
            return (String) Class.forName("android.accounts.Account").getField("name").get(((Object[]) cAccountManager.getMethod("getAccountsByType", String.class).invoke(accountManager, "com.google"))[0]);
        } catch (Exception e) {
            return null;
        }
    }
}
