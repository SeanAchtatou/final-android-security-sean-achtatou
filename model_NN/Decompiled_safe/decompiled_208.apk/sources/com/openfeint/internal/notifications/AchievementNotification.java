package com.openfeint.internal.notifications;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.openfeint.api.Notification;
import com.openfeint.api.resource.Achievement;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.request.ExternalBitmapRequest;
import com.oziapp.coolLanding.R;
import java.util.HashMap;
import java.util.Map;

public class AchievementNotification extends NotificationBase {
    protected AchievementNotification(Achievement achievement, Map<String, Object> userData) {
        super(OpenFeintInternal.getRString(R.string.of_achievement_unlocked), null, Notification.Category.Achievement, Notification.Type.Success, userData);
    }

    public void loadedImage(Bitmap map) {
        this.displayView.invalidate();
    }

    /* Debug info: failed to restart local var, previous not found, register: 12 */
    /* access modifiers changed from: protected */
    public boolean createView() {
        String rString;
        String scoreText;
        final Achievement achievement = (Achievement) getUserData().get("achievement");
        this.displayView = ((LayoutInflater) OpenFeintInternal.getInstance().getContext().getSystemService("layout_inflater")).inflate((int) R.layout.of_achievement_notification, (ViewGroup) null);
        if (achievement.isUnlocked) {
            this.displayView.findViewById(R.id.of_achievement_progress_icon).setVisibility(4);
            if (achievement.gamerscore == 0) {
                this.displayView.findViewById(R.id.of_achievement_score_icon).setVisibility(4);
                this.displayView.findViewById(R.id.of_achievement_score).setVisibility(4);
            }
        } else {
            this.displayView.findViewById(R.id.of_achievement_score_icon).setVisibility(4);
        }
        TextView textView = (TextView) this.displayView.findViewById(R.id.of_achievement_text);
        if (achievement.title == null || achievement.title.length() <= 0) {
            rString = OpenFeintInternal.getRString(R.string.of_achievement_unlocked);
        } else {
            rString = achievement.title;
        }
        textView.setText(rString);
        if (achievement.isUnlocked) {
            scoreText = Integer.toString(achievement.gamerscore);
        } else {
            scoreText = String.format("%d%%", Integer.valueOf((int) achievement.percentComplete));
        }
        ((TextView) this.displayView.findViewById(R.id.of_achievement_score)).setText(scoreText);
        if (achievement.iconUrl != null) {
            Drawable iconImage = getResourceDrawable(achievement.iconUrl);
            if (iconImage == null) {
                new ExternalBitmapRequest(achievement.iconUrl) {
                    public void onSuccess(Bitmap responseBody) {
                        ((ImageView) AchievementNotification.this.displayView.findViewById(R.id.of_achievement_icon)).setImageDrawable(new BitmapDrawable(responseBody));
                        AchievementNotification.this.showToast();
                    }

                    public void onFailure(String exceptionMessage) {
                        OpenFeintInternal.log("NotificationImage", "Failed to load image " + achievement.iconUrl + ":" + exceptionMessage);
                        AchievementNotification.this.showToast();
                    }
                }.launch();
                return false;
            }
            ((ImageView) this.displayView.findViewById(R.id.of_achievement_icon)).setImageDrawable(iconImage);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void drawView(Canvas canvas) {
        this.displayView.draw(canvas);
    }

    public static void showStatus(Achievement achievement) {
        Map<String, Object> userData = new HashMap<>();
        userData.put("achievement", achievement);
        new AchievementNotification(achievement, userData).checkDelegateAndView();
    }
}
