package com.openfeint.internal.db;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public abstract class SQLiteOpenHelperX {
    private static final String TAG = SQLiteOpenHelperX.class.getSimpleName();
    private SQLiteDatabase mDatabase = null;
    private SQLiteOpenHelper mHelper;
    private String mName;
    private int mNewVersion;

    public abstract void onCreate(SQLiteDatabase sQLiteDatabase);

    public abstract void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2);

    public SQLiteOpenHelperX(String path, int version) {
        if (version < 1) {
            throw new IllegalArgumentException("Version must be >= 1, was " + version);
        }
        this.mName = path;
        this.mNewVersion = version;
    }

    public SQLiteOpenHelperX(SQLiteOpenHelper helper) {
        this.mHelper = helper;
    }

    public void setSQLiteOpenHelper(String path, int version) {
        if (version < 1) {
            throw new IllegalArgumentException("Version must be >= 1, was " + version);
        }
        this.mName = path;
        this.mNewVersion = version;
        close();
    }

    public void setSQLiteOpenHelper(SQLiteOpenHelper helper) {
        this.mHelper = helper;
        close();
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:33:0x0054=Splitter:B:33:0x0054, B:49:0x006f=Splitter:B:49:0x006f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized android.database.sqlite.SQLiteDatabase getWritableDatabase() {
        /*
            r5 = this;
            monitor-enter(r5)
            android.database.sqlite.SQLiteOpenHelper r3 = r5.mHelper     // Catch:{ all -> 0x0072 }
            if (r3 == 0) goto L_0x000d
            android.database.sqlite.SQLiteOpenHelper r3 = r5.mHelper     // Catch:{ all -> 0x0072 }
            android.database.sqlite.SQLiteDatabase r3 = r3.getWritableDatabase()     // Catch:{ all -> 0x0072 }
        L_0x000b:
            monitor-exit(r5)
            return r3
        L_0x000d:
            android.database.sqlite.SQLiteDatabase r3 = r5.mDatabase     // Catch:{ all -> 0x0072 }
            if (r3 == 0) goto L_0x0024
            android.database.sqlite.SQLiteDatabase r3 = r5.mDatabase     // Catch:{ all -> 0x0072 }
            boolean r3 = r3.isOpen()     // Catch:{ all -> 0x0072 }
            if (r3 == 0) goto L_0x0024
            android.database.sqlite.SQLiteDatabase r3 = r5.mDatabase     // Catch:{ all -> 0x0072 }
            boolean r3 = r3.isReadOnly()     // Catch:{ all -> 0x0072 }
            if (r3 != 0) goto L_0x0024
            android.database.sqlite.SQLiteDatabase r3 = r5.mDatabase     // Catch:{ all -> 0x0072 }
            goto L_0x000b
        L_0x0024:
            r1 = 0
            r0 = 0
            java.lang.String r3 = r5.mName     // Catch:{ all -> 0x0063 }
            r4 = 0
            android.database.sqlite.SQLiteDatabase r0 = android.database.sqlite.SQLiteDatabase.openOrCreateDatabase(r3, r4)     // Catch:{ all -> 0x0063 }
            int r2 = r0.getVersion()     // Catch:{ all -> 0x0063 }
            int r3 = r5.mNewVersion     // Catch:{ all -> 0x0063 }
            if (r2 == r3) goto L_0x0048
            r0.beginTransaction()     // Catch:{ all -> 0x0063 }
            if (r2 != 0) goto L_0x0058
            r5.onCreate(r0)     // Catch:{ all -> 0x005e }
        L_0x003d:
            int r3 = r5.mNewVersion     // Catch:{ all -> 0x005e }
            r0.setVersion(r3)     // Catch:{ all -> 0x005e }
            r0.setTransactionSuccessful()     // Catch:{ all -> 0x005e }
            r0.endTransaction()     // Catch:{ all -> 0x0063 }
        L_0x0048:
            r1 = 1
            if (r1 == 0) goto L_0x0075
            android.database.sqlite.SQLiteDatabase r3 = r5.mDatabase     // Catch:{ all -> 0x0072 }
            if (r3 == 0) goto L_0x0054
            android.database.sqlite.SQLiteDatabase r3 = r5.mDatabase     // Catch:{ Exception -> 0x0083 }
            r3.close()     // Catch:{ Exception -> 0x0083 }
        L_0x0054:
            r5.mDatabase = r0     // Catch:{ all -> 0x0072 }
        L_0x0056:
            r3 = r0
            goto L_0x000b
        L_0x0058:
            int r3 = r5.mNewVersion     // Catch:{ all -> 0x005e }
            r5.onUpgrade(r0, r2, r3)     // Catch:{ all -> 0x005e }
            goto L_0x003d
        L_0x005e:
            r3 = move-exception
            r0.endTransaction()     // Catch:{ all -> 0x0063 }
            throw r3     // Catch:{ all -> 0x0063 }
        L_0x0063:
            r3 = move-exception
            if (r1 == 0) goto L_0x007b
            android.database.sqlite.SQLiteDatabase r4 = r5.mDatabase     // Catch:{ all -> 0x0072 }
            if (r4 == 0) goto L_0x006f
            android.database.sqlite.SQLiteDatabase r4 = r5.mDatabase     // Catch:{ Exception -> 0x0081 }
            r4.close()     // Catch:{ Exception -> 0x0081 }
        L_0x006f:
            r5.mDatabase = r0     // Catch:{ all -> 0x0072 }
        L_0x0071:
            throw r3     // Catch:{ all -> 0x0072 }
        L_0x0072:
            r3 = move-exception
            monitor-exit(r5)
            throw r3
        L_0x0075:
            if (r0 == 0) goto L_0x0056
            r0.close()     // Catch:{ all -> 0x0072 }
            goto L_0x0056
        L_0x007b:
            if (r0 == 0) goto L_0x0071
            r0.close()     // Catch:{ all -> 0x0072 }
            goto L_0x0071
        L_0x0081:
            r4 = move-exception
            goto L_0x006f
        L_0x0083:
            r3 = move-exception
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.db.SQLiteOpenHelperX.getWritableDatabase():android.database.sqlite.SQLiteDatabase");
    }

    public synchronized SQLiteDatabase getReadableDatabase() {
        SQLiteDatabase db;
        SQLiteDatabase sQLiteDatabase;
        if (this.mHelper != null) {
            sQLiteDatabase = this.mHelper.getReadableDatabase();
        } else if (this.mDatabase == null || !this.mDatabase.isOpen()) {
            try {
                sQLiteDatabase = getWritableDatabase();
            } catch (SQLiteException e) {
                if (this.mName == null) {
                    throw e;
                }
                Log.e(TAG, "Couldn't open " + this.mName + " for writing (will try read-only):", e);
                db = null;
                db = SQLiteDatabase.openDatabase(this.mName, null, 1);
                if (db.getVersion() != this.mNewVersion) {
                    throw new SQLiteException("Can't upgrade read-only database from version " + db.getVersion() + " to " + this.mNewVersion + ": " + this.mName);
                }
                Log.w(TAG, "Opened " + this.mName + " in read-only mode");
                this.mDatabase = db;
                sQLiteDatabase = this.mDatabase;
                if (db != null) {
                    if (db != this.mDatabase) {
                        db.close();
                    }
                }
            } catch (Throwable th) {
                if (db != null) {
                    if (db != this.mDatabase) {
                        db.close();
                    }
                }
                throw th;
            }
        } else {
            sQLiteDatabase = this.mDatabase;
        }
        return sQLiteDatabase;
    }

    public synchronized void close() {
        if (this.mDatabase != null && this.mDatabase.isOpen()) {
            this.mDatabase.close();
            this.mDatabase = null;
        }
        if (this.mHelper != null) {
            this.mHelper.close();
        }
    }
}
