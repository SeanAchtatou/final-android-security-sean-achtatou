package com.openfeint.internal.resource;

import java.io.IOException;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;

public abstract class PrimitiveResourceProperty extends ResourceProperty {
    public abstract void copy(Resource resource, Resource resource2);

    public abstract void generate(Resource resource, JsonGenerator jsonGenerator, String str) throws JsonGenerationException, IOException;

    public abstract void parse(Resource resource, JsonParser jsonParser) throws JsonParseException, IOException;
}
