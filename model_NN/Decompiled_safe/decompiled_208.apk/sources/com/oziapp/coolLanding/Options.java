package com.oziapp.coolLanding;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class Options extends Activity {
    public static String bgsound_OnOff = "off";
    public static int checker;
    public static String clouds_OnOff = "off";
    public static String difficulty_Level = "easy";
    public static String drawLine_OnOff = "on";
    public static String emergencyPlane = "on";
    public static String militaryPlane = "on";
    public static String newModes = "landing";
    public static String playingModes = "classic";
    public static int screen_height;
    public static int screen_width;
    public static String showfps_OnOff = "on";
    public static String sound_OnOff = "on";
    public static String vipPlane = "on";
    CheckBox checkVip;
    CheckBox checkbox;
    CheckBox checkbox2;
    CheckBox checkbox3;
    CheckBox checkbox5;
    CheckBox emergencychkbox;
    CheckBox militarychkbox;
    /* access modifiers changed from: private */
    public TextView modetxt;
    View.OnClickListener myOptionOnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            if (Options.this.rEasy.isChecked()) {
                Options.difficulty_Level = "easy";
                Settings.difficulty_Level = Options.difficulty_Level;
            } else if (Options.this.rNormal.isChecked()) {
                Options.difficulty_Level = "normal";
                Settings.difficulty_Level = Options.difficulty_Level;
            } else if (Options.this.rHard.isChecked()) {
                Options.difficulty_Level = "hard";
                Settings.difficulty_Level = Options.difficulty_Level;
            }
            if (Options.this.rLanding.isChecked()) {
                Options.newModes = "landing";
                Options.this.rgSelect_Mode.setVisibility(0);
                Options.this.modetxt.setVisibility(0);
            } else if (Options.this.rTakeoff.isChecked()) {
                Toast.makeText(Options.this, "Available in Pro", 0).show();
                Options.this.rTakeoff.setChecked(false);
                Options.this.rLanding.setChecked(true);
            }
            if (Options.this.rTouch.isChecked()) {
                Options.playingModes = "classic";
                Settings.playingModes = Options.playingModes;
            } else if (Options.this.rClassic.isChecked()) {
                Options.playingModes = "kids";
                Settings.playingModes = Options.playingModes;
            }
        }
    };
    /* access modifiers changed from: private */
    public RadioButton rClassic;
    /* access modifiers changed from: private */
    public RadioButton rEasy;
    /* access modifiers changed from: private */
    public RadioButton rHard;
    /* access modifiers changed from: private */
    public RadioButton rLanding;
    /* access modifiers changed from: private */
    public RadioButton rNormal;
    /* access modifiers changed from: private */
    public RadioButton rTakeoff;
    /* access modifiers changed from: private */
    public RadioButton rTouch;
    /* access modifiers changed from: private */
    public RadioGroup rgSelect_Mode;

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        Intent intent = new Intent(this, Main.class);
        finish();
        startActivity(intent);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.options);
        this.checkbox = (CheckBox) findViewById(R.id.checkbox_sound);
        this.checkbox2 = (CheckBox) findViewById(R.id.checkbox_soundbg);
        this.checkbox3 = (CheckBox) findViewById(R.id.checkbox_clouds);
        this.checkbox5 = (CheckBox) findViewById(R.id.checkbox_showfps);
        this.emergencychkbox = (CheckBox) findViewById(R.id.emergencyplane);
        this.militarychkbox = (CheckBox) findViewById(R.id.mltryplane);
        this.checkVip = (CheckBox) findViewById(R.id.vipplane);
        this.rgSelect_Mode = (RadioGroup) findViewById(R.id.modes);
        this.rEasy = (RadioButton) findViewById(R.id.easy);
        this.rNormal = (RadioButton) findViewById(R.id.normal);
        this.rHard = (RadioButton) findViewById(R.id.hard);
        this.rClassic = (RadioButton) findViewById(R.id.classicMode);
        this.rTouch = (RadioButton) findViewById(R.id.touchMode);
        this.rLanding = (RadioButton) findViewById(R.id.landing);
        this.rTakeoff = (RadioButton) findViewById(R.id.takeoff);
        this.modetxt = (TextView) findViewById(R.id.selectmode);
        if (sound_OnOff == null) {
            sound_OnOff = Settings.sound_OnOff;
        }
        if (clouds_OnOff == null) {
            clouds_OnOff = Settings.clouds_OnOff;
        }
        if (bgsound_OnOff == null) {
            bgsound_OnOff = Settings.bgsound_OnOff;
        }
        if (sound_OnOff == "on") {
            this.checkbox.setChecked(true);
        } else {
            this.checkbox.setChecked(false);
        }
        if (bgsound_OnOff == "on") {
            this.checkbox2.setChecked(true);
        } else {
            this.checkbox2.setChecked(false);
        }
        if (clouds_OnOff == "on") {
            this.checkbox3.setChecked(true);
        } else {
            this.checkbox3.setChecked(false);
        }
        if (showfps_OnOff == "on") {
            this.checkbox5.setChecked(true);
        } else {
            this.checkbox5.setChecked(false);
        }
        this.emergencychkbox.setChecked(false);
        this.checkVip.setChecked(false);
        this.militarychkbox.setChecked(false);
        if (difficulty_Level == "easy") {
            this.rEasy.setChecked(true);
        } else if (difficulty_Level == "normal") {
            this.rNormal.setChecked(true);
        } else {
            this.rHard.setChecked(true);
        }
        if (newModes == "landing") {
            this.rLanding.setChecked(true);
            this.rgSelect_Mode.setVisibility(0);
            this.modetxt.setVisibility(0);
        } else {
            this.rTakeoff.setChecked(true);
            this.rgSelect_Mode.setVisibility(4);
            this.modetxt.setVisibility(4);
        }
        if (playingModes == "classic") {
            this.rTouch.setChecked(true);
        } else {
            this.rClassic.setChecked(true);
        }
        this.checkbox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    Options.sound_OnOff = "on";
                    Settings.sound_OnOff = Options.sound_OnOff;
                    return;
                }
                Options.sound_OnOff = "off";
                Settings.sound_OnOff = Options.sound_OnOff;
            }
        });
        this.checkbox2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    Options.bgsound_OnOff = "on";
                    Settings.bgsound_OnOff = Options.bgsound_OnOff;
                    return;
                }
                Options.bgsound_OnOff = "off";
                Settings.bgsound_OnOff = Options.bgsound_OnOff;
            }
        });
        this.checkbox3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    Options.clouds_OnOff = "on";
                    Settings.clouds_OnOff = Options.clouds_OnOff;
                    return;
                }
                Options.clouds_OnOff = "off";
                Settings.clouds_OnOff = Options.clouds_OnOff;
            }
        });
        this.checkbox5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    Options.showfps_OnOff = "on";
                    Settings.showfps_OnOff = Options.showfps_OnOff;
                    return;
                }
                Options.showfps_OnOff = "off";
                Settings.showfps_OnOff = Options.showfps_OnOff;
            }
        });
        this.emergencychkbox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(Options.this, "Option to have more Score, Available in Pro only", 0).show();
                Options.this.emergencychkbox.setChecked(false);
            }
        });
        this.checkVip.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(Options.this, "Option to have more Score, Available in Pro only", 0).show();
                Options.this.checkVip.setChecked(false);
            }
        });
        this.militarychkbox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(Options.this, "Option to have more Score, Available in Pro only", 0).show();
                Options.this.militarychkbox.setChecked(false);
            }
        });
        this.rEasy.setOnClickListener(this.myOptionOnClickListener);
        this.rNormal.setOnClickListener(this.myOptionOnClickListener);
        this.rHard.setOnClickListener(this.myOptionOnClickListener);
        this.rClassic.setOnClickListener(this.myOptionOnClickListener);
        this.rTouch.setOnClickListener(this.myOptionOnClickListener);
        this.rLanding.setOnClickListener(this.myOptionOnClickListener);
        this.rTakeoff.setOnClickListener(this.myOptionOnClickListener);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        System.gc();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }
}
