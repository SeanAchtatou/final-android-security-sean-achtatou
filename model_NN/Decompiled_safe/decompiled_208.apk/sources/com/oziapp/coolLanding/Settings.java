package com.oziapp.coolLanding;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;

public class Settings {
    public static String bgsound_OnOff;
    public static String clouds_OnOff;
    public static String difficulty_Level;
    public static String drawLine_OnOff;
    public static String playingModes;
    public static String showfps_OnOff;
    public static String sound_OnOff;
    public static GoogleAnalyticsTracker tracker;
}
