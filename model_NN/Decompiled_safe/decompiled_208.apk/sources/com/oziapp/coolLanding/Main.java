package com.oziapp.coolLanding;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Process;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import java.util.List;

public class Main extends Activity {
    /* access modifiers changed from: private */
    public Button Exit;
    private Button Help;
    /* access modifiers changed from: private */
    public Button MoreApps;
    int Rate_app_check2 = 0;
    /* access modifiers changed from: private */
    public Button SelectMap;
    /* access modifiers changed from: private */
    public Button Setting;
    private Button Start;
    public MediaPlayer clicksound;
    /* access modifiers changed from: private */
    public AlertDialog rateapp;
    int scnd_time_check2 = 0;

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        try {
            Settings.tracker.stop();
        } catch (Exception e) {
        }
        try {
            if (DragMain.minutes >= 18) {
                if (this.Rate_app_check2 == 0) {
                    this.rateapp.show();
                    this.scnd_time_check2++;
                    shared();
                }
                DragMain.minutes = 0;
            } else {
                finish();
            }
            return true;
        } catch (Exception e2) {
            Exception exc = e2;
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        try {
            getWindow().setFlags(1024, 1024);
            requestWindowFeature(1);
            this.Rate_app_check2 = getSharedPreferences("myPrefs", 1).getInt("Rate_app_check", this.scnd_time_check2);
            Log.e("2nd time check", new StringBuilder().append(this.scnd_time_check2).toString());
            Log.e("Rate_app_check", new StringBuilder().append(this.Rate_app_check2).toString());
            AlertDialog.Builder buypro_bld = new AlertDialog.Builder(this);
            buypro_bld.setMessage("").setCancelable(false).setPositiveButton("No Thanks", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Main.this.finish();
                }
            }).setNegativeButton("Continue", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Intent i = new Intent("android.intent.action.VIEW");
                    i.setData(Uri.parse("market://details?id=com.oziapp.coolLanding"));
                    Main.this.finish();
                    Main.this.startActivity(i);
                }
            });
            this.rateapp = buypro_bld.create();
            this.rateapp.setTitle("Give Your Comments");
            this.rateapp.setIcon((int) R.drawable.airflightfree);
            if (Options.sound_OnOff.equalsIgnoreCase("on")) {
                this.clicksound = new MediaPlayer();
                this.clicksound = MediaPlayer.create(getBaseContext(), (int) R.raw.click3);
            }
            super.onCreate(savedInstanceState);
            try {
                Settings.tracker = GoogleAnalyticsTracker.getInstance();
            } catch (NullPointerException e) {
            }
            try {
                Settings.tracker.start("UA-23917490-1", 30, getApplication());
            } catch (NullPointerException e2) {
            }
            try {
                Settings.tracker.setCustomVar(1, "Lite", "Start App", 2);
            } catch (NullPointerException e3) {
            }
            try {
                Settings.tracker.trackPageView("/" + getLocalClassName());
            } catch (NullPointerException e4) {
            }
            List<ActivityManager.RunningAppProcessInfo> activityes = ((ActivityManager) getSystemService("activity")).getRunningAppProcesses();
            for (int i = 0; i < activityes.size(); i++) {
                if (!activityes.get(i).processName.equals("com.oziapp.coolLanding")) {
                    Process.killProcess(activityes.get(i).pid);
                }
            }
            setContentView((int) R.layout.main);
            this.Start = (Button) findViewById(R.id.start);
            this.Help = (Button) findViewById(R.id.help);
            this.Setting = (Button) findViewById(R.id.settings);
            this.SelectMap = (Button) findViewById(R.id.selectmap);
            this.MoreApps = (Button) findViewById(R.id.moreapps);
            this.Exit = (Button) findViewById(R.id.exit);
            this.Start.setOnClickListener(new start());
            this.Help.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (Main.this.clicksound != null) {
                        try {
                            Main.this.clicksound.start();
                        } catch (Exception e) {
                        }
                    }
                    try {
                        Settings.tracker.trackEvent("Start App", "Clicked", " Start Button", 1);
                    } catch (Exception e2) {
                    }
                    Intent intent = new Intent(Main.this, Touch_Help.class);
                    Bundle b = new Bundle();
                    b.putBoolean("helpKey", true);
                    intent.putExtras(b);
                    Main.this.startActivity(intent);
                }
            });
            this.Setting.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (Main.this.clicksound != null) {
                        try {
                            Main.this.clicksound.start();
                        } catch (Exception e) {
                        }
                    }
                    try {
                        Settings.tracker.trackEvent("Start App", "Clicked", "Settings Button", 1);
                    } catch (Exception e2) {
                    }
                    Main.this.Setting.setBackgroundResource(R.drawable.settingson);
                    Intent intent = new Intent(Main.this, Options.class);
                    Main.this.finish();
                    Main.this.startActivity(intent);
                }
            });
            this.SelectMap.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (Main.this.clicksound != null) {
                        try {
                            Main.this.clicksound.start();
                        } catch (Exception e) {
                        }
                    }
                    try {
                        Settings.tracker.trackEvent("Start App", "Clicked", "Select Map Button", 1);
                    } catch (Exception e2) {
                    }
                    Main.this.SelectMap.setBackgroundResource(R.drawable.mapson);
                    Intent intent = new Intent(Main.this, SelectMap.class);
                    Main.this.finish();
                    Main.this.startActivity(intent);
                }
            });
            this.MoreApps.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (Main.this.clicksound != null) {
                        try {
                            Main.this.clicksound.start();
                        } catch (Exception e) {
                        }
                    }
                    try {
                        Settings.tracker.trackEvent("Start App", "Clicked", "Moreapps Button", 1);
                    } catch (Exception e2) {
                    }
                    Main.this.MoreApps.setBackgroundResource(R.drawable.moreon);
                    Intent intent = new Intent(Main.this, Moreapps.class);
                    Main.this.finish();
                    Main.this.startActivity(intent);
                }
            });
            this.Exit.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (Main.this.clicksound != null) {
                        try {
                            Main.this.clicksound.start();
                        } catch (Exception e) {
                        }
                    }
                    try {
                        Settings.tracker.trackEvent("Start App", "Clicked", "Exit Button", 1);
                    } catch (Exception e2) {
                    }
                    try {
                        Settings.tracker.stop();
                    } catch (Exception e3) {
                    }
                    Main.this.Exit.setBackgroundResource(R.drawable.exiton);
                    if (DragMain.minutes >= 18) {
                        if (Main.this.Rate_app_check2 == 0) {
                            Main.this.rateapp.show();
                            Main.this.scnd_time_check2++;
                            Main.this.shared();
                        }
                        DragMain.minutes = 0;
                        return;
                    }
                    Main.this.finish();
                }
            });
        } catch (Exception e5) {
        }
    }

    public void shared() {
        SharedPreferences.Editor prefsEditor = getSharedPreferences("myPrefs", 1).edit();
        prefsEditor.putInt("Rate_app_check2", this.scnd_time_check2);
        prefsEditor.commit();
    }

    public class start implements View.OnClickListener {
        public start() {
        }

        public void onClick(View view) {
            if (Main.this.clicksound != null) {
                try {
                    Main.this.clicksound.start();
                } catch (Exception e) {
                }
            }
            Intent intent = new Intent(Main.this, Touch_Help.class);
            Bundle b = new Bundle();
            b.putBoolean("helpKey", false);
            intent.putExtras(b);
            Main.this.startActivity(intent);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        try {
            if (this.Start != null) {
                this.Start.destroyDrawingCache();
                this.Start.setOnKeyListener(null);
                this.Start = null;
            }
            if (this.Help != null) {
                this.Help.destroyDrawingCache();
                this.Help.setOnKeyListener(null);
                this.Help = null;
            }
            if (this.Setting != null) {
                this.Setting.destroyDrawingCache();
                this.Setting.setOnKeyListener(null);
                this.Setting = null;
            }
            if (this.SelectMap != null) {
                this.SelectMap.destroyDrawingCache();
                this.SelectMap.setOnKeyListener(null);
                this.SelectMap = null;
            }
            if (this.MoreApps != null) {
                this.MoreApps.destroyDrawingCache();
                this.MoreApps.setOnKeyListener(null);
                this.MoreApps = null;
            }
            if (this.Exit != null) {
                this.Exit.destroyDrawingCache();
                this.Exit.setOnKeyListener(null);
                this.Exit = null;
            }
            if (this.clicksound != null) {
                this.clicksound.release();
                this.clicksound = null;
            }
            this.rateapp = null;
        } catch (Exception e) {
        }
        System.gc();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        System.gc();
    }
}
