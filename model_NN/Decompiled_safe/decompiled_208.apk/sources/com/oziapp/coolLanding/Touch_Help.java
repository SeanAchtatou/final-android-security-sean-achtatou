package com.oziapp.coolLanding;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.InflateException;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class Touch_Help extends Activity {
    Button BackButton;
    public MediaPlayer clicksound;
    int counter = 0;
    boolean flgStartGame = false;
    boolean goback = false;
    ImageView img;
    int[] imgs = {R.drawable.helpclassicmode1, R.drawable.helpclassicmode2, R.drawable.helpclassicmode3, R.drawable.helpclassicmode4, R.drawable.helpclassicmode5};
    int[] imgsAll = {R.drawable.helpclassicmode1, R.drawable.helpclassicmode2, R.drawable.helpclassicmode3, R.drawable.helpclassicmode4, R.drawable.helpclassicmode5, R.drawable.helpkidsmode1, R.drawable.helpkidsmode2, R.drawable.helpkidsmode3, R.drawable.helpkidsmode4, R.drawable.helpkidsmode5};
    int[] imgsKids = {R.drawable.helpkidsmode1, R.drawable.helpkidsmode2, R.drawable.helpkidsmode3, R.drawable.helpkidsmode4, R.drawable.helpkidsmode5};
    Button next;
    Button prev;
    boolean showAll = false;
    Button skip;
    int[] srcArray;
    int stages = 0;

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        finish();
        return true;
    }

    public void onCreate(Bundle savedInstanceState) {
        try {
            getWindow().setFlags(1024, 1024);
            requestWindowFeature(1);
            setContentView((int) R.layout.touchhelp);
            if (Options.sound_OnOff.equalsIgnoreCase("on")) {
                this.clicksound = new MediaPlayer();
                this.clicksound = MediaPlayer.create(getBaseContext(), (int) R.raw.click3);
            }
            super.onCreate(savedInstanceState);
            try {
                Settings.tracker.setCustomVar(1, "Lite", "Help", 2);
            } catch (Exception e) {
            }
            try {
                Settings.tracker.trackPageView("/" + getLocalClassName());
            } catch (Exception e2) {
            }
            this.img = (ImageView) findViewById(R.id.image);
            this.prev = (Button) findViewById(R.id.prev);
            this.next = (Button) findViewById(R.id.next);
            this.skip = (Button) findViewById(R.id.skip);
            this.BackButton = (Button) findViewById(R.id.exit);
            this.next.setVisibility(0);
            this.prev.setVisibility(0);
            this.showAll = getIntent().getExtras().getBoolean("helpKey");
            if (this.showAll) {
                this.skip.setVisibility(4);
            } else {
                this.skip.setVisibility(0);
            }
            if (this.showAll) {
                this.img.setBackgroundResource(this.imgsAll[0]);
                this.srcArray = this.imgsAll;
                this.counter = 9;
            } else if (Options.playingModes.equals("classic")) {
                this.img.setBackgroundResource(this.imgs[0]);
                this.srcArray = this.imgs;
                this.counter = 4;
            } else {
                this.img.setBackgroundResource(this.imgsKids[0]);
                this.srcArray = this.imgsKids;
                this.counter = 4;
            }
            this.next.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (Touch_Help.this.clicksound != null) {
                        Touch_Help.this.clicksound.start();
                    }
                    if (Touch_Help.this.flgStartGame) {
                        try {
                            Settings.tracker.trackEvent("Help", "Clicked", "Game Start", 1);
                        } catch (Exception e) {
                        }
                        Touch_Help.this.next.setBackgroundResource(R.drawable.starton);
                        if (Options.playingModes.equals("classic")) {
                            Intent intent = new Intent(Touch_Help.this, DragMain.class);
                            Touch_Help.this.finish();
                            Touch_Help.this.startActivity(intent);
                            return;
                        }
                        if (SelectMap.map_Select == R.drawable.kindamixed) {
                            SelectMap.map_Select = R.drawable.background1;
                        }
                        Intent intent2 = new Intent(Touch_Help.this, PlaneActivity.class);
                        Touch_Help.this.finish();
                        Touch_Help.this.startActivity(intent2);
                        return;
                    }
                    Touch_Help.this.stages++;
                    if (Touch_Help.this.stages > Touch_Help.this.counter) {
                        Touch_Help.this.stages--;
                    }
                    Touch_Help.this.img.setBackgroundResource(Touch_Help.this.srcArray[Touch_Help.this.stages]);
                    if (Touch_Help.this.stages != Touch_Help.this.counter) {
                        return;
                    }
                    if (!Touch_Help.this.showAll) {
                        Touch_Help.this.next.setBackgroundResource(R.drawable.startoff);
                        Touch_Help.this.flgStartGame = true;
                        return;
                    }
                    Touch_Help.this.next.setVisibility(4);
                }
            });
            this.prev.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    try {
                        Settings.tracker.trackEvent("Help", "Clicked", "Previous Button", 1);
                    } catch (Exception e) {
                    }
                    if (Touch_Help.this.clicksound != null) {
                        Touch_Help.this.clicksound.start();
                    }
                    Touch_Help.this.next.setVisibility(0);
                    if (Touch_Help.this.stages == 0) {
                        Touch_Help.this.finish();
                        return;
                    }
                    Touch_Help.this.stages--;
                    Touch_Help.this.img.setBackgroundResource(Touch_Help.this.srcArray[Touch_Help.this.stages]);
                    if (Touch_Help.this.flgStartGame) {
                        Touch_Help.this.next.setBackgroundResource(R.drawable.next);
                        Touch_Help.this.flgStartGame = false;
                    }
                }
            });
            this.skip.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    try {
                        Settings.tracker.trackEvent("Help", "Clicked", "Skip Button", 1);
                    } catch (Exception e) {
                    }
                    if (Touch_Help.this.clicksound != null) {
                        Touch_Help.this.clicksound.start();
                    }
                    Touch_Help.this.skip.setBackgroundResource(R.drawable.skipon);
                    if (Options.playingModes.equals("classic")) {
                        Intent intent = new Intent(Touch_Help.this, DragMain.class);
                        Touch_Help.this.finish();
                        Touch_Help.this.startActivity(intent);
                        return;
                    }
                    if (SelectMap.map_Select == R.drawable.kindamixed) {
                        SelectMap.map_Select = R.drawable.background1;
                    }
                    Intent intent2 = new Intent(Touch_Help.this, PlaneActivity.class);
                    Touch_Help.this.finish();
                    Touch_Help.this.startActivity(intent2);
                }
            });
            this.BackButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    try {
                        Settings.tracker.trackEvent("Help", "Clicked", "Back Button", 1);
                    } catch (Exception e) {
                    }
                    if (Touch_Help.this.clicksound != null) {
                        Touch_Help.this.clicksound.start();
                    }
                    Touch_Help.this.BackButton.setBackgroundResource(R.drawable.backon);
                    Touch_Help.this.finish();
                }
            });
        } catch (InflateException e3) {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        try {
            this.imgs = null;
            this.imgsAll = null;
            this.imgsKids = null;
            if (this.img != null) {
                this.img.destroyDrawingCache();
                this.img = null;
            }
            if (this.clicksound != null) {
                this.clicksound.release();
                this.clicksound = null;
            }
            if (this.prev != null) {
                this.prev.destroyDrawingCache();
                this.prev.setOnClickListener(null);
                this.prev = null;
            }
            if (this.next != null) {
                this.next.destroyDrawingCache();
                this.next.setOnClickListener(null);
                this.next = null;
            }
            if (this.skip != null) {
                this.skip.destroyDrawingCache();
                this.skip.setOnClickListener(null);
                this.skip = null;
            }
            if (this.BackButton != null) {
                this.BackButton.destroyDrawingCache();
                this.BackButton.setOnClickListener(null);
                this.BackButton = null;
            }
        } catch (NullPointerException e) {
        }
        System.gc();
    }
}
