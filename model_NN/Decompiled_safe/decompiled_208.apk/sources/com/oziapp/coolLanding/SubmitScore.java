package com.oziapp.coolLanding;

import android.app.Activity;
import android.os.Bundle;
import com.openfeint.api.OpenFeint;
import com.openfeint.api.OpenFeintDelegate;
import com.openfeint.api.OpenFeintSettings;
import com.openfeint.api.resource.Leaderboard;
import com.openfeint.api.resource.Score;
import com.openfeint.api.ui.Dashboard;

public class SubmitScore extends Activity {
    static final String gameID = "288312";
    static final String gameKey = "8XM5s18bmHyZ1ZcTlcSBg";
    static final String gameName = "Air Flight Control Free";
    static final String gameSecret = "vPIsgcZtMlbYDsAUaMDWP977AfSjJo3bYZw4uLFbEk";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            if (!OpenFeint.isUserLoggedIn()) {
                OpenFeint.initialize(this, new OpenFeintSettings(gameName, gameKey, gameSecret, gameID), new OpenFeintDelegate() {
                });
            }
            getWindow().setFlags(1024, 1024);
            requestWindowFeature(1);
            setContentView((int) R.layout.submitscore);
            int score = getIntent().getExtras().getInt("score");
            Dashboard.openLeaderboard("741137");
            new Score((long) score, null).submitTo(new Leaderboard("741137"), new Score.SubmitToCB() {
                public void onSuccess(boolean newHighScore) {
                    SubmitScore.this.setResult(-1);
                }

                public void onFailure(String exceptionMessage) {
                    SubmitScore.this.setResult(0);
                    SubmitScore.this.finish();
                }
            });
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }
}
