package com.oziapp.coolLanding;

import android.os.CountDownTimer;
import android.util.Log;

/* compiled from: MYCount */
class MyCount extends CountDownTimer {
    int i;
    Animation objMain;

    public MyCount(long millisInFuture, long countDownInterval, Animation mainobject) {
        super(millisInFuture, countDownInterval);
        this.objMain = mainobject;
        Log.d("MyCount", "Constructor...");
    }

    public void onFinish() {
        Log.d("MyCount", "onFinish");
        if (this.objMain != null) {
            Log.d("MyCount", "calling function");
            this.objMain.FinishTimer();
        }
    }

    public void onTick(long millisUntilFinished) {
    }
}
