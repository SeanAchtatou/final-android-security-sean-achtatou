package com.oziapp.coolLanding;

public class RouteNates {
    int posX;
    int posY;

    public RouteNates(int x, int y) {
        this.posX = x;
        this.posY = y;
    }

    public int getPosX() {
        return this.posX;
    }

    public void setPosX(int posX2) {
        this.posX = posX2;
    }

    public int getPosY() {
        return this.posY;
    }

    public void setPosY(int posY2) {
        this.posY = posY2;
    }
}
