package com.oziapp.coolLanding;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.admob.android.ads.AdListener;
import com.admob.android.ads.AdManager;
import com.admob.android.ads.AdView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class DragMain extends Activity implements AdListener {
    public static final String PREFS_PRIVATE = "PREFS_PRIVATE";
    static int minutes = 0;
    boolean GameSpeed = true;
    double Slope = 0.0d;
    public AdView _ad;
    CustomView _panelView;
    public ImageView _txt;
    double aggrX;
    double aggrY;
    AlertDialog alert;
    AlertDialog alert2;
    AlertDialog alert3;
    Animation anim;
    /* access modifiers changed from: private */
    public Bitmap arrivallarge;
    /* access modifiers changed from: private */
    public Bitmap arrivalsmall;
    public MediaPlayer backgroundSound;
    /* access modifiers changed from: private */
    public Bitmap boombitmap;
    Canvas c;
    /* access modifiers changed from: private */
    public Bitmap chances;
    int check;
    /* access modifiers changed from: private */
    public Bitmap clouds;
    float d1 = 0.0f;
    float d2 = 0.0f;
    double deltaX = 0.0d;
    double deltaY = 0.0d;
    int emergencySeconds = 0;
    long emergencyStart = System.currentTimeMillis();
    long emergencymilliseconds = 0;
    long endpause;
    public boolean flgPause = false;
    /* access modifiers changed from: private */
    public Bitmap frwd;
    int height;
    public Bitmap helig1;
    public Bitmap helig2;
    public Bitmap helig3;
    public Bitmap heliini;
    /* access modifiers changed from: private */
    public Bitmap heliland1;
    /* access modifiers changed from: private */
    public Bitmap heliland2;
    /* access modifiers changed from: private */
    public Bitmap heliland3;
    public Bitmap helind1;
    public Bitmap helind2;
    public Bitmap helind3;
    /* access modifiers changed from: private */
    public Bitmap helinormal1;
    /* access modifiers changed from: private */
    public Bitmap helinormal2;
    /* access modifiers changed from: private */
    public Bitmap helinormal3;
    private Bitmap heliwarning;
    public Bitmap heliwd1;
    public Bitmap heliwd2;
    public Bitmap heliwd3;
    /* access modifiers changed from: private */
    public Bitmap hi_score;
    /* access modifiers changed from: private */
    public Bitmap hi_score2;
    public int highscore = 0;
    AlertDialog life_lost;
    private Paint mDot;
    /* access modifiers changed from: private */
    public Handler m_Handler = new Handler() {
        public void handleMessage(Message msg) {
            int i = msg.what;
            for (int i2 = 0; i2 < DragMain.this._panelView.flying_Objects.size(); i2++) {
                if (DragMain.this._panelView.flying_Objects.get(i2).flgcrash) {
                    if (DragMain.this._panelView.flying_Objects.get(i2).warningObject != null) {
                        DragMain.this._panelView.RemoveWarning(DragMain.this._panelView.flying_Objects.get(i2).warningObject);
                        DragMain.this._panelView.flying_Objects.get(i2).warningObject = null;
                    }
                    DragMain.this._panelView.flying_Objects.remove(i2);
                }
            }
            DragMain.this.life_lost.show();
        }
    };
    public Bitmap mark;
    public Bitmap mark1;
    Matrix mat = null;
    int max = 2;
    long miliseconds = 0;
    /* access modifiers changed from: private */
    public Bitmap pause;
    long pauseTime = 0;
    int pause_1st_skip = 0;
    /* access modifiers changed from: private */
    public Bitmap play;
    /* access modifiers changed from: private */
    public Bitmap ply;
    int predir;
    /* access modifiers changed from: private */
    public SharedPreferences prefsPrivate;
    /* access modifiers changed from: private */
    public Bitmap redPlaneglow;
    /* access modifiers changed from: private */
    public Bitmap redPlaneshadow;
    /* access modifiers changed from: private */
    public Bitmap redlandwarning;
    /* access modifiers changed from: private */
    public Bitmap redwarning;
    public int score = 0;
    int seconds;
    boolean setInitials = false;
    long start = System.currentTimeMillis();
    long startpause;
    int width;
    /* access modifiers changed from: private */
    public Bitmap yellowPlaneglow;
    /* access modifiers changed from: private */
    public Bitmap yellowPlaneshadow;
    /* access modifiers changed from: private */
    public Bitmap yellowlandwarning;
    /* access modifiers changed from: private */
    public Bitmap yellowwarning;

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 3) {
            this.alert.show();
            this.emergencySeconds = 0;
            this.flgPause = true;
            Log.e("On pause home ", "");
            return true;
        } else if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        } else {
            this.alert3.show();
            this.emergencySeconds = 0;
            this.flgPause = true;
            return true;
        }
    }

    public void onPause() {
        this.pause_1st_skip = 1;
        Log.e("onPause", "onPause.1");
        super.onPause();
    }

    public void onResume() {
        if (this.pause_1st_skip == 1) {
            this.alert.show();
            this.emergencySeconds = 0;
            this.flgPause = true;
        }
        Log.e("onResume", "onResume.1");
        super.onResume();
    }

    public void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            getWindow().setFlags(1024, 1024);
            requestWindowFeature(1);
            try {
                Settings.tracker.setCustomVar(1, "Lite", "Classic Mode", 2);
            } catch (Exception e) {
            }
            try {
                Settings.tracker.trackPageView("/" + getLocalClassName());
            } catch (Exception e2) {
            }
            getWindow().setWindowAnimations(16973826);
            Display display = getWindowManager().getDefaultDisplay();
            this.width = display.getWidth();
            this.height = display.getHeight();
            Options.screen_height = this.height;
            Options.screen_width = this.width;
            this._panelView = new CustomView(this);
            AlertDialog.Builder alt_exit = new AlertDialog.Builder(this);
            alt_exit.setMessage("Current Game will be Lost").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    if (DragMain.this.highscore <= DragMain.this.score) {
                        DragMain.this.prefsPrivate = DragMain.this.getSharedPreferences("PREFS_PRIVATE", 0);
                        SharedPreferences.Editor prefsPrivateEditor1 = DragMain.this.prefsPrivate.edit();
                        if (SelectMap.map_Select == R.drawable.mountainjoy) {
                            DragMain.this.prefsPrivate = DragMain.this.getSharedPreferences("PREFS_PRIVATE", 0);
                            prefsPrivateEditor1.putInt("KEY_PRIVATE1", DragMain.this.score);
                            prefsPrivateEditor1.putInt("TOTAL_LANDED1", DragMain.this.prefsPrivate.getInt("TOTAL_LANDED1", 0) + DragMain.this.score);
                        } else if (SelectMap.map_Select == R.drawable.meadowolives) {
                            DragMain.this.prefsPrivate = DragMain.this.getSharedPreferences("PREFS_PRIVATE", 0);
                            prefsPrivateEditor1.putInt("KEY_PRIVATE2", DragMain.this.score);
                            prefsPrivateEditor1.putInt("TOTAL_LANDED2", DragMain.this.prefsPrivate.getInt("TOTAL_LANDED2", 0) + DragMain.this.score);
                        } else if (SelectMap.map_Select == R.drawable.maritimelands) {
                            DragMain.this.prefsPrivate = DragMain.this.getSharedPreferences("PREFS_PRIVATE", 0);
                            prefsPrivateEditor1.putInt("KEY_PRIVATE3", DragMain.this.score);
                            prefsPrivateEditor1.putInt("TOTAL_LANDED3", DragMain.this.prefsPrivate.getInt("TOTAL_LANDED3", 0) + DragMain.this.score);
                        } else if (SelectMap.map_Select == R.drawable.kindamixed) {
                            DragMain.this.prefsPrivate = DragMain.this.getSharedPreferences("PREFS_PRIVATE", 0);
                            prefsPrivateEditor1.putInt("KEY_PRIVATE4", DragMain.this.score);
                            prefsPrivateEditor1.putInt("TOTAL_LANDED4", DragMain.this.prefsPrivate.getInt("TOTAL_LANDED4", 0) + DragMain.this.score);
                        } else {
                            int i = SelectMap.map_Select;
                        }
                        if (DragMain.this.backgroundSound != null) {
                            DragMain.this.backgroundSound.stop();
                        }
                        prefsPrivateEditor1.commit();
                    }
                    DragMain.this.finish();
                }
            }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    DragMain.this.pauseTime += DragMain.this.endpause - DragMain.this.startpause;
                    DragMain.this.flgPause = false;
                    DragMain.this.emergencyStart = System.currentTimeMillis();
                }
            });
            this.alert3 = alt_exit.create();
            this.alert3.setTitle("Are You Sure?");
            this.alert3.setIcon((int) R.drawable.icon);
            AlertDialog.Builder alt_bld2 = new AlertDialog.Builder(this);
            alt_bld2.setMessage("Current Game will be Lost").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    if (DragMain.this.highscore <= DragMain.this.score) {
                        DragMain.this.prefsPrivate = DragMain.this.getSharedPreferences("PREFS_PRIVATE", 0);
                        SharedPreferences.Editor prefsPrivateEditor1 = DragMain.this.prefsPrivate.edit();
                        if (SelectMap.map_Select == R.drawable.mountainjoy) {
                            DragMain.this.prefsPrivate = DragMain.this.getSharedPreferences("PREFS_PRIVATE", 0);
                            prefsPrivateEditor1.putInt("KEY_PRIVATE1", DragMain.this.score);
                            prefsPrivateEditor1.putInt("TOTAL_LANDED1", DragMain.this.prefsPrivate.getInt("TOTAL_LANDED1", 0) + DragMain.this.score);
                        } else if (SelectMap.map_Select == R.drawable.meadowolives) {
                            DragMain.this.prefsPrivate = DragMain.this.getSharedPreferences("PREFS_PRIVATE", 0);
                            prefsPrivateEditor1.putInt("KEY_PRIVATE2", DragMain.this.score);
                            prefsPrivateEditor1.putInt("TOTAL_LANDED2", DragMain.this.prefsPrivate.getInt("TOTAL_LANDED2", 0) + DragMain.this.score);
                        } else if (SelectMap.map_Select == R.drawable.maritimelands) {
                            DragMain.this.prefsPrivate = DragMain.this.getSharedPreferences("PREFS_PRIVATE", 0);
                            prefsPrivateEditor1.putInt("KEY_PRIVATE3", DragMain.this.score);
                            prefsPrivateEditor1.putInt("TOTAL_LANDED3", DragMain.this.prefsPrivate.getInt("TOTAL_LANDED3", 0) + DragMain.this.score);
                        } else if (SelectMap.map_Select == R.drawable.kindamixed) {
                            DragMain.this.prefsPrivate = DragMain.this.getSharedPreferences("PREFS_PRIVATE", 0);
                            prefsPrivateEditor1.putInt("KEY_PRIVATE4", DragMain.this.score);
                            prefsPrivateEditor1.putInt("TOTAL_LANDED4", DragMain.this.prefsPrivate.getInt("TOTAL_LANDED4", 0) + DragMain.this.score);
                        } else {
                            int i = SelectMap.map_Select;
                        }
                        if (DragMain.this.backgroundSound != null) {
                            DragMain.this.backgroundSound.stop();
                        }
                        prefsPrivateEditor1.commit();
                    }
                    DragMain.this.finish();
                }
            }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    DragMain.this.alert.show();
                }
            });
            AlertDialog.Builder alt_bld = new AlertDialog.Builder(this);
            alt_bld.setMessage("").setCancelable(false).setPositiveButton("Resume", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    DragMain.this.pauseTime += DragMain.this.endpause - DragMain.this.startpause;
                    DragMain.this.flgPause = false;
                    DragMain.this.emergencyStart = System.currentTimeMillis();
                }
            }).setNegativeButton("Main Menu", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    DragMain.this.alert2.show();
                }
            });
            AlertDialog.Builder alt_life = new AlertDialog.Builder(this);
            alt_life.setMessage("Get a life on every 50 points scored.").setCancelable(false).setNeutralButton("Continue", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    DragMain.this.pauseTime += DragMain.this.endpause - DragMain.this.startpause;
                    DragMain.this.flgPause = false;
                    DragMain.this.emergencyStart = System.currentTimeMillis();
                    DragMain.this._panelView._thread.flgIsCrashed = false;
                }
            });
            this.alert = alt_bld.create();
            this.alert.setTitle("Game Paused");
            this.alert.setIcon((int) R.drawable.icon);
            this.alert2 = alt_bld2.create();
            this.alert2.setTitle("Are You Sure?");
            this.alert2.setIcon((int) R.drawable.icon);
            this.life_lost = alt_life.create();
            this.life_lost.setTitle("Chance Lost");
            this.life_lost.setIcon((int) R.drawable.icon);
            this.mDot = new Paint();
            this.mDot.setColor(-65536);
            this.mDot.setStrokeWidth(2.0f);
            StartVersusPanel(getIntent().getExtras());
        } catch (Exception e3) {
        }
    }

    public void onDestroy() {
        super.onDestroy();
        try {
            if (this._ad != null) {
                this._ad.removeAllViews();
                this._ad = null;
            }
            if (this.boombitmap != null) {
                this.boombitmap.recycle();
                this.boombitmap = null;
            }
            if (this.arrivalsmall != null) {
                this.arrivalsmall.recycle();
                this.arrivalsmall = null;
            }
            if (this.arrivallarge != null) {
                this.arrivallarge.recycle();
                this.arrivallarge = null;
            }
            if (this.clouds != null) {
                this.clouds.recycle();
                this.clouds = null;
            }
            if (this.yellowPlaneglow != null) {
                this.yellowPlaneglow.recycle();
                this.yellowPlaneglow = null;
            }
            if (this.yellowPlaneshadow != null) {
                this.yellowPlaneshadow.recycle();
                this.yellowPlaneshadow = null;
            }
            if (this.yellowlandwarning != null) {
                this.yellowlandwarning.recycle();
                this.yellowlandwarning = null;
            }
            if (this.yellowwarning != null) {
                this.yellowwarning.recycle();
                this.yellowwarning = null;
            }
            if (this.redPlaneglow != null) {
                this.redPlaneglow.recycle();
                this.redPlaneglow = null;
            }
            if (this.redPlaneshadow != null) {
                this.redPlaneshadow.recycle();
                this.redPlaneshadow = null;
            }
            if (this.redlandwarning != null) {
                this.redlandwarning.recycle();
                this.redlandwarning = null;
            }
            if (this.redwarning != null) {
                this.redwarning.recycle();
                this.redwarning = null;
            }
            if (this.helinormal1 != null) {
                this.helinormal1.recycle();
                this.helinormal1 = null;
            }
            if (this.play != null) {
                this.play.recycle();
            }
            if (this.pause != null) {
                this.pause.recycle();
            }
            if (this.pause != null) {
                this.ply.recycle();
            }
            if (this.frwd != null) {
                this.frwd.recycle();
            }
            if (this.mark != null) {
                this.mark.recycle();
            }
            if (this.mark1 != null) {
                this.mark1.recycle();
            }
            if (this.helind1 != null) {
                this.helind1.recycle();
            }
            if (this.helind2 != null) {
                this.helind2.recycle();
            }
            if (this.helind3 != null) {
                this.helind3.recycle();
            }
            if (this.heliwd1 != null) {
                this.heliwd1.recycle();
            }
            if (this.heliwd2 != null) {
                this.heliwd2.recycle();
            }
            if (this.heliwd3 != null) {
                this.heliwd3.recycle();
            }
            if (this.helig1 != null) {
                this.helig1.recycle();
            }
            if (this.helig2 != null) {
                this.helig2.recycle();
            }
            if (this.helig3 != null) {
                this.helig3.recycle();
            }
            if (this.heliini != null) {
                this.heliini.recycle();
            }
            if (this.helinormal2 != null) {
                this.helinormal2.recycle();
            }
            if (this.helinormal3 != null) {
                this.helinormal3.recycle();
            }
            if (this.heliland1 != null) {
                this.heliland1.recycle();
            }
            if (this.heliland2 != null) {
                this.heliland2.recycle();
            }
            if (this.heliland3 != null) {
                this.heliland3.recycle();
            }
            if (this.heliwarning != null) {
                this.heliwarning.recycle();
            }
        } catch (Exception e) {
        }
        System.gc();
    }

    public void onNewIntent(Intent newIntent) {
        super.onNewIntent(newIntent);
        setIntent(newIntent);
        StartVersusPanel(getIntent().getExtras());
    }

    public void StartVersusPanel(Bundle levelBundle) {
        this._ad = new AdView(this);
        this._txt = new ImageView(this);
        AdManager.setPublisherId("a14d5412760ca95");
        this._panelView.setLayoutParams(new ViewGroup.LayoutParams(-1, this.height - 70));
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(-1, -2);
        ViewGroup.LayoutParams params2 = new ViewGroup.LayoutParams(-1, -2);
        this._ad.setLayoutParams(params);
        this._txt.setLayoutParams(params2);
        this._txt.setBackgroundResource(R.drawable.advert);
        FrameLayout rl = new FrameLayout(this);
        rl.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        rl.addView(this._panelView);
        LinearLayout li = new LinearLayout(this);
        LinearLayout.LayoutParams bottomNavParams = new LinearLayout.LayoutParams(-1, -1);
        li.setOrientation(1);
        li.setGravity(80);
        li.setLayoutParams(bottomNavParams);
        li.addView(this._ad);
        LinearLayout li2 = new LinearLayout(this);
        LinearLayout.LayoutParams bottomNavParams2 = new LinearLayout.LayoutParams(-1, -1);
        li2.setOrientation(1);
        li2.setGravity(80);
        li2.setLayoutParams(bottomNavParams2);
        li2.addView(this._txt);
        rl.addView(li2);
        rl.addView(li);
        setContentView(rl);
    }

    public void onReceiveAd(AdView adView) {
    }

    public void onFailedToReceiveAd(AdView adView) {
    }

    public void onReceiveRefreshedAd(AdView adView) {
    }

    public void onFailedToReceiveRefreshedAd(AdView adView) {
    }

    public class CustomView extends SurfaceView implements SurfaceHolder.Callback {
        int Arrivalx = 0;
        int Arrivaly = 0;
        public int RedZoneX1 = ((int) (0.2801d * ((double) this.screen_width)));
        public int RedZoneX2 = ((int) (0.508d * ((double) this.screen_width)));
        public int RedZoneY1 = ((int) (0.5625d * ((double) this.screen_height)));
        public int RedZoneY2 = ((int) (0.6437d * ((double) this.screen_height)));
        float Xf;
        public int YellowZoneX1 = ((int) (0.2701d * ((double) this.screen_width)));
        public int YellowZoneX2 = ((int) (0.5964d * ((double) this.screen_width)));
        public int YellowZoneY1 = ((int) (0.32299999999999995d * ((double) this.screen_height)));
        public int YellowZoneY2 = ((int) (0.423d * ((double) this.screen_height)));
        float Yf;
        /* access modifiers changed from: private */
        public TutorialThread _thread;
        public MediaPlayer achievement;
        public Bitmap background;
        public Bitmap bitmap;
        private Bundle bundle;
        int cou = 0;
        int counter = 0;
        int counter1 = 0;
        int counter2 = 0;
        public MediaPlayer crash;
        int d = (Options.screen_width / 4);
        int d2 = (Options.screen_height / 2);
        DashPathEffect dashPath;
        public int direction = 0;
        private float down_mX;
        private float down_mY;
        private Paint ePaint;
        private int elsedirection = 0;
        private int endlandingX;
        private int endlandingY;
        public boolean flgClouds = false;
        public boolean flgLifelost = false;
        public boolean flgLine = true;
        public boolean flgModes = false;
        public boolean flgPlayAgain = true;
        public boolean flgdelta = true;
        public boolean flgdraged = true;
        public boolean flgemergencyPlane = true;
        public boolean flgfirst = true;
        public boolean flgfps = true;
        public boolean flgsecond = true;
        public ArrayList<DragGraphicObject> flying_Objects = null;
        private int fps;
        public Paint fpsPaint;
        boolean greenmark = false;
        private int helicheck = 0;
        public DragGraphicObject helicopter = null;
        public MediaPlayer helipad;
        public int helipadX1 = ((int) (0.5473d * ((double) this.screen_width)));
        public int helipadX2 = ((int) (0.6315d * ((double) this.screen_width)));
        public int helipadY1 = ((int) (0.7025d * ((double) this.screen_height)));
        public int helipadY2 = ((int) (0.825d * ((double) this.screen_height)));
        private int i = 0;
        public int index = 0;
        private int landingX1;
        private int landingY1;
        private MediaPlayer landsound;
        int life_count = 0;
        int life_increse_on = 50;
        public float lineX;
        public float lineY;
        Bitmap mBackgroundImage;
        public Paint mBitmapPaint;
        float mX;
        float mY;
        private String msg = "";
        private Paint msgPaint;
        private Paint msgPaint2;
        int msgcounter = 0;
        DragGraphicObject objTouched = null;
        private int planetype;
        private int pointcounter = 0;
        private int pscore1;
        Random rand = new Random();
        Random rand2 = new Random();
        private Paint rectPaint;
        public Bitmap redPlane;
        boolean redmark = false;
        public MediaPlayer runway1;
        public MediaPlayer runway2;
        int score_limit = 20;
        public int screen_height = (Options.screen_height - 70);
        public int screen_width = Options.screen_width;
        private float sleep;
        private final Paint tPaint = new Paint();
        public int tempZone1X1 = ((int) (0.23420000000000002d * ((double) this.screen_width)));
        public int tempZone1X2 = ((int) (0.2787d * ((double) this.screen_width)));
        public int tempZone1Y1 = ((int) (0.3583d * ((double) this.screen_height)));
        public int tempZone1Y2 = ((int) (0.4167d * ((double) this.screen_height)));
        public int tempZone2X1 = ((int) (0.23420000000000002d * ((double) this.screen_width)));
        public int tempZone2X2 = ((int) (0.2857d * ((double) this.screen_width)));
        public int tempZone2Y1 = ((int) (((double) this.screen_height) * 0.5832999999999999d));
        public int tempZone2Y2 = ((int) (0.6417d * ((double) this.screen_height)));
        private int tempheliX1;
        private int tempheliY1;
        public int temphelipadX1;
        public int temphelipadX2;
        public int temphelipadY1;
        public int temphelipadY2;
        private int templandingZone1X1;
        private int templandingZone1Y1;
        private int templandingZone2X1;
        private int templandingZone2X2;
        private int templandingZone2Y1;
        private int templandingZone2Y2;
        private float up_Xf;
        private float up_Yf;
        private MediaPlayer warningsound;
        public int x_index = 0;
        public int y_index = 0;
        public Bitmap yellowPlane;
        boolean yellowmark = false;
        public int zeplinX1 = ((int) (0.631d * ((double) this.screen_width)));
        public int zeplinX2 = ((int) (0.7341d * ((double) this.screen_width)));
        public int zeplinY1 = ((int) (((double) this.screen_height) * 0.5832999999999999d));
        public int zeplinY2 = ((int) (0.6667000000000001d * ((double) this.screen_height)));

        /* JADX WARN: Type inference failed for: r11v0, types: [android.view.SurfaceHolder$Callback, com.oziapp.coolLanding.DragMain$CustomView] */
        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public CustomView(android.content.Context r13) {
            /*
                r11 = this;
                r9 = 4603692129088805274(0x3fe399999999999a, double:0.6125)
                r7 = 4603429118870566836(0x3fe2aa64c2f837b4, double:0.5832999999999999)
                r6 = 0
                r5 = 1
                com.oziapp.coolLanding.DragMain.this = r12
                r11.<init>(r13)
                int r1 = com.oziapp.coolLanding.Options.screen_width
                r11.screen_width = r1
                int r1 = com.oziapp.coolLanding.Options.screen_height
                r2 = 70
                int r1 = r1 - r2
                r11.screen_height = r1
                int r1 = com.oziapp.coolLanding.Options.screen_width
                int r1 = r1 / 4
                r11.d = r1
                int r1 = com.oziapp.coolLanding.Options.screen_height
                int r1 = r1 / 2
                r11.d2 = r1
                r1 = 20
                r11.score_limit = r1
                r11.life_count = r6
                r1 = 50
                r11.life_increse_on = r1
                r1 = 4598537308955317004(0x3fd1495182a9930c, double:0.2701)
                int r3 = r11.screen_width
                double r3 = (double) r3
                double r1 = r1 * r3
                int r1 = (int) r1
                r11.YellowZoneX1 = r1
                r1 = 4599490270636468600(0x3fd4ac083126e978, double:0.32299999999999995)
                int r3 = r11.screen_height
                double r3 = (double) r3
                double r1 = r1 * r3
                int r1 = (int) r1
                r11.YellowZoneY1 = r1
                r1 = 4603547113180803944(0x3fe315b573eab368, double:0.5964)
                int r3 = r11.screen_width
                double r3 = (double) r3
                double r1 = r1 * r3
                int r1 = (int) r1
                r11.YellowZoneX2 = r1
                r1 = 4601291710487416799(0x3fdb126e978d4fdf, double:0.423)
                int r3 = r11.screen_height
                double r3 = (double) r3
                double r1 = r1 * r3
                int r1 = (int) r1
                r11.YellowZoneY2 = r1
                r1 = 4598717452940411824(0x3fd1ed288ce703b0, double:0.2801)
                int r3 = r11.screen_width
                double r3 = (double) r3
                double r1 = r1 * r3
                int r1 = (int) r1
                r11.RedZoneX1 = r1
                r1 = 4602750876766684840(0x3fe04189374bc6a8, double:0.508)
                int r3 = r11.screen_width
                double r3 = (double) r3
                double r1 = r1 * r3
                int r1 = (int) r1
                r11.RedZoneX2 = r1
                r1 = 4603241769126068224(0x3fe2000000000000, double:0.5625)
                int r3 = r11.screen_height
                double r3 = (double) r3
                double r1 = r1 * r3
                int r1 = (int) r1
                r11.RedZoneY1 = r1
                r1 = 4603973153705553193(0x3fe49930be0ded29, double:0.6437)
                int r3 = r11.screen_height
                double r3 = (double) r3
                double r1 = r1 * r3
                int r1 = (int) r1
                r11.RedZoneY2 = r1
                r1 = 4603104859697396161(0x3fe1837b4a2339c1, double:0.5473)
                int r3 = r11.screen_width
                double r3 = (double) r3
                double r1 = r1 * r3
                int r1 = (int) r1
                r11.helipadX1 = r1
                r1 = 4603863265874645352(0x3fe4353f7ced9168, double:0.6315)
                int r3 = r11.screen_width
                double r3 = (double) r3
                double r1 = r1 * r3
                int r1 = (int) r1
                r11.helipadX2 = r1
                r1 = 4604502777021731963(0x3fe67ae147ae147b, double:0.7025)
                int r3 = r11.screen_height
                double r3 = (double) r3
                double r1 = r1 * r3
                int r1 = (int) r1
                r11.helipadY1 = r1
                r1 = 4605606158930437734(0x3fea666666666666, double:0.825)
                int r3 = r11.screen_height
                double r3 = (double) r3
                double r1 = r1 * r3
                int r1 = (int) r1
                r11.helipadY2 = r1
                r1 = 4603858762275017982(0x3fe43126e978d4fe, double:0.631)
                int r3 = r11.screen_width
                double r3 = (double) r3
                double r1 = r1 * r3
                int r1 = (int) r1
                r11.zeplinX1 = r1
                r1 = 4604787404518181778(0x3fe77dbf487fcb92, double:0.7341)
                int r3 = r11.screen_width
                double r3 = (double) r3
                double r1 = r1 * r3
                int r1 = (int) r1
                r11.zeplinX2 = r1
                int r1 = r11.screen_height
                double r1 = (double) r1
                double r1 = r1 * r7
                int r1 = (int) r1
                r11.zeplinY1 = r1
                r1 = 4604180319288412236(0x3fe5559b3d07c84c, double:0.6667000000000001)
                int r3 = r11.screen_height
                double r3 = (double) r3
                double r1 = r1 * r3
                int r1 = (int) r1
                r11.zeplinY2 = r1
                r1 = 4597605964552376786(0x3fcdfa43fe5c91d2, double:0.23420000000000002)
                int r3 = r11.screen_width
                double r3 = (double) r3
                double r1 = r1 * r3
                int r1 = (int) r1
                r11.tempZone1X1 = r1
                r1 = 4600126178903853315(0x3fd6ee631f8a0903, double:0.3583)
                int r3 = r11.screen_height
                double r3 = (double) r3
                double r1 = r1 * r3
                int r1 = (int) r1
                r11.tempZone1Y1 = r1
                r1 = 4598692232782498549(0x3fd1d63886594af5, double:0.2787)
                int r3 = r11.screen_width
                double r3 = (double) r3
                double r1 = r1 * r3
                int r1 = (int) r1
                r11.tempZone1X2 = r1
                r1 = 4601178219776807063(0x3fdaab367a0f9097, double:0.4167)
                int r3 = r11.screen_height
                double r3 = (double) r3
                double r1 = r1 * r3
                int r1 = (int) r1
                r11.tempZone1Y2 = r1
                r1 = 4597605964552376786(0x3fcdfa43fe5c91d2, double:0.23420000000000002)
                int r3 = r11.screen_width
                double r3 = (double) r3
                double r1 = r1 * r3
                int r1 = (int) r1
                r11.tempZone2X1 = r1
                int r1 = r11.screen_height
                double r1 = (double) r1
                double r1 = r1 * r7
                int r1 = (int) r1
                r11.tempZone2Y1 = r1
                r1 = 4598818333572064923(0x3fd248e8a71de69b, double:0.2857)
                int r3 = r11.screen_width
                double r3 = (double) r3
                double r1 = r1 * r3
                int r1 = (int) r1
                r11.tempZone2X2 = r1
                r1 = 4603955139307043711(0x3fe488ce703afb7f, double:0.6417)
                int r3 = r11.screen_height
                double r3 = (double) r3
                double r1 = r1 * r3
                int r1 = (int) r1
                r11.tempZone2Y2 = r1
                r11.flgdraged = r5
                r1 = 0
                r11.flying_Objects = r1
                r1 = 0
                r11.helicopter = r1
                r11.x_index = r6
                r11.y_index = r6
                r11.index = r6
                r11.direction = r6
                r11.Arrivaly = r6
                r11.Arrivalx = r6
                java.util.Random r1 = new java.util.Random
                r1.<init>()
                r11.rand = r1
                java.util.Random r1 = new java.util.Random
                r1.<init>()
                r11.rand2 = r1
                r11.flgfirst = r5
                r11.flgsecond = r5
                r11.flgClouds = r6
                r11.flgLine = r5
                r11.flgModes = r6
                r11.flgfps = r5
                r11.flgemergencyPlane = r5
                r11.flgPlayAgain = r5
                r11.flgdelta = r5
                r11.i = r6
                android.graphics.Paint r1 = new android.graphics.Paint
                r1.<init>()
                r11.tPaint = r1
                r1 = 0
                r11.objTouched = r1
                r11.redmark = r6
                r11.yellowmark = r6
                r11.greenmark = r6
                r11.counter = r6
                r11.counter1 = r6
                r11.counter2 = r6
                r11.msgcounter = r6
                r11.helicheck = r6
                r11.elsedirection = r6
                java.lang.String r1 = ""
                r11.msg = r1
                r11.pointcounter = r6
                r11.flgLifelost = r6
                r11.cou = r6
                android.view.SurfaceHolder r1 = r11.getHolder()     // Catch:{ Exception -> 0x0cfa }
                r1.addCallback(r11)     // Catch:{ Exception -> 0x0cfa }
                r1 = 1
                r11.setFocusable(r1)     // Catch:{ Exception -> 0x0cfa }
                java.lang.String r1 = "PREFS_PRIVATE"
                r2 = 0
                android.content.SharedPreferences r1 = r12.getSharedPreferences(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.prefsPrivate = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.SharedPreferences r1 = r12.prefsPrivate     // Catch:{ Exception -> 0x0cfa }
                java.lang.String r2 = "KEY_PRIVATE"
                int r3 = r11.pscore1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1.getInt(r2, r3)     // Catch:{ Exception -> 0x0cfa }
                r12.highscore = r1     // Catch:{ Exception -> 0x0cfa }
                android.os.Bundle r1 = new android.os.Bundle     // Catch:{ Exception -> 0x0cfa }
                r1.<init>()     // Catch:{ Exception -> 0x0cfa }
                r11.bundle = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4598776900455493114(0x3fd22339c0ebedfa, double:0.2834)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.templandingZone1X1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4600690029577200101(0x3fd8ef34d6a161e5, double:0.3896)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.templandingZone1Y1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4599155202824192236(0x3fd37b4a2339c0ec, double:0.3044)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.templandingZone2X1 = r1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r1 = (double) r1     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r9
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.templandingZone2Y1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4603427317430715889(0x3fe2a8c154c985f1, double:0.5831000000000001)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempheliX1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4604930618986332160(0x3fe8000000000000, double:0.75)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempheliY1 = r1     // Catch:{ Exception -> 0x0cfa }
                java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ Exception -> 0x0cfa }
                r1.<init>()     // Catch:{ Exception -> 0x0cfa }
                r11.flying_Objects = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837610(0x7f02006a, float:1.7280179E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r11.redPlane = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837612(0x7f02006c, float:1.7280183E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.redPlaneshadow = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837622(0x7f020076, float:1.7280203E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.redPlaneglow = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837624(0x7f020078, float:1.7280207E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.redwarning = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837623(0x7f020077, float:1.7280205E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.redlandwarning = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837613(0x7f02006d, float:1.7280185E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r11.yellowPlane = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837615(0x7f02006f, float:1.728019E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.yellowPlaneshadow = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837640(0x7f020088, float:1.728024E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.yellowPlaneglow = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837642(0x7f02008a, float:1.7280244E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.yellowwarning = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837641(0x7f020089, float:1.7280242E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.yellowlandwarning = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837530(0x7f02001a, float:1.7280017E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.helinormal1 = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837531(0x7f02001b, float:1.7280019E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.helinormal2 = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837532(0x7f02001c, float:1.728002E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.helinormal3 = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837539(0x7f020023, float:1.7280035E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.heliland1 = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837540(0x7f020024, float:1.7280037E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.heliland2 = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837541(0x7f020025, float:1.728004E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.heliland3 = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837536(0x7f020020, float:1.7280029E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.helind1 = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837537(0x7f020021, float:1.728003E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.helind2 = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837538(0x7f020022, float:1.7280033E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.helind3 = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837542(0x7f020026, float:1.7280041E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.heliwd1 = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837543(0x7f020027, float:1.7280043E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.heliwd2 = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837544(0x7f020028, float:1.7280045E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.heliwd3 = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837533(0x7f02001d, float:1.7280023E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.helig1 = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837534(0x7f02001e, float:1.7280025E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.helig2 = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837535(0x7f02001f, float:1.7280027E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.helig3 = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837518(0x7f02000e, float:1.7279992E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.boombitmap = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837508(0x7f020004, float:1.7279972E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.arrivalsmall = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837509(0x7f020005, float:1.7279974E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.arrivallarge = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837520(0x7f020010, float:1.7279996E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.clouds = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837559(0x7f020037, float:1.7280075E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.hi_score = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837560(0x7f020038, float:1.7280078E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.hi_score2 = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837616(0x7f020070, float:1.7280191E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.play = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837608(0x7f020068, float:1.7280175E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.pause = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837609(0x7f020069, float:1.7280177E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.ply = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837526(0x7f020016, float:1.7280009E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.frwd = r1     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Bitmap r1 = r12.ply     // Catch:{ Exception -> 0x0cfa }
                r2 = 30
                r3 = 30
                r4 = 1
                android.graphics.Bitmap r1 = android.graphics.Bitmap.createScaledBitmap(r1, r2, r3, r4)     // Catch:{ Exception -> 0x0cfa }
                r12.ply = r1     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Bitmap r1 = r12.frwd     // Catch:{ Exception -> 0x0cfa }
                r2 = 30
                r3 = 30
                r4 = 1
                android.graphics.Bitmap r1 = android.graphics.Bitmap.createScaledBitmap(r1, r2, r3, r4)     // Catch:{ Exception -> 0x0cfa }
                r12.frwd = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837570(0x7f020042, float:1.7280098E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.mark = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837571(0x7f020043, float:1.72801E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.mark1 = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837519(0x7f02000f, float:1.7279994E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.chances = r1     // Catch:{ Exception -> 0x0cfa }
                com.oziapp.coolLanding.DragGraphicObject r0 = new com.oziapp.coolLanding.DragGraphicObject     // Catch:{ Exception -> 0x0cfa }
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837613(0x7f02006d, float:1.7280185E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r2 = -1035468800(0xffffffffc2480000, float:-50.0)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                r4 = 60
                int r3 = r3 - r4
                float r3 = (float) r3     // Catch:{ Exception -> 0x0cfa }
                r4 = 1139802112(0x43f00000, float:480.0)
                java.util.Random r5 = r11.rand2     // Catch:{ Exception -> 0x0cfa }
                int r6 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                int r5 = r5.nextInt(r6)     // Catch:{ Exception -> 0x0cfa }
                float r5 = (float) r5     // Catch:{ Exception -> 0x0cfa }
                r6 = 1
                r0.<init>(r1, r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x0cfa }
                r1 = 20
                r0.Arrivalx = r1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                r2 = 60
                int r1 = r1 - r2
                r0.Arrivaly = r1     // Catch:{ Exception -> 0x0cfa }
                java.util.ArrayList<com.oziapp.coolLanding.DragGraphicObject> r1 = r11.flying_Objects     // Catch:{ Exception -> 0x0cfa }
                r1.add(r0)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = new android.graphics.Paint     // Catch:{ Exception -> 0x0cfa }
                r1.<init>()     // Catch:{ Exception -> 0x0cfa }
                r11.mBitmapPaint = r1     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = new android.graphics.Paint     // Catch:{ Exception -> 0x0cfa }
                r1.<init>()     // Catch:{ Exception -> 0x0cfa }
                r11.fpsPaint = r1     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = new android.graphics.Paint     // Catch:{ Exception -> 0x0cfa }
                r1.<init>()     // Catch:{ Exception -> 0x0cfa }
                r11.rectPaint = r1     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = new android.graphics.Paint     // Catch:{ Exception -> 0x0cfa }
                r1.<init>()     // Catch:{ Exception -> 0x0cfa }
                r11.msgPaint = r1     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = new android.graphics.Paint     // Catch:{ Exception -> 0x0cfa }
                r1.<init>()     // Catch:{ Exception -> 0x0cfa }
                r11.msgPaint2 = r1     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = new android.graphics.Paint     // Catch:{ Exception -> 0x0cfa }
                r1.<init>()     // Catch:{ Exception -> 0x0cfa }
                r11.ePaint = r1     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.msgPaint     // Catch:{ Exception -> 0x0cfa }
                r2 = -1
                r1.setColor(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.msgPaint     // Catch:{ Exception -> 0x0cfa }
                r2 = 1101004800(0x41a00000, float:20.0)
                r1.setTextSize(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.msgPaint     // Catch:{ Exception -> 0x0cfa }
                r2 = 1
                r1.setFakeBoldText(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.msgPaint     // Catch:{ Exception -> 0x0cfa }
                r2 = 1
                r1.setAntiAlias(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.msgPaint     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint$Style r2 = android.graphics.Paint.Style.FILL     // Catch:{ Exception -> 0x0cfa }
                r1.setStyle(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.msgPaint     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Typeface r2 = android.graphics.Typeface.SANS_SERIF     // Catch:{ Exception -> 0x0cfa }
                r1.setTypeface(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.msgPaint2     // Catch:{ Exception -> 0x0cfa }
                r2 = -1
                r1.setColor(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.msgPaint2     // Catch:{ Exception -> 0x0cfa }
                r2 = 1103626240(0x41c80000, float:25.0)
                r1.setTextSize(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.msgPaint2     // Catch:{ Exception -> 0x0cfa }
                r2 = 1
                r1.setFakeBoldText(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.msgPaint2     // Catch:{ Exception -> 0x0cfa }
                r2 = 1
                r1.setAntiAlias(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.msgPaint2     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint$Style r2 = android.graphics.Paint.Style.FILL     // Catch:{ Exception -> 0x0cfa }
                r1.setStyle(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.msgPaint2     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Typeface r2 = android.graphics.Typeface.SANS_SERIF     // Catch:{ Exception -> 0x0cfa }
                r1.setTypeface(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.ePaint     // Catch:{ Exception -> 0x0cfa }
                r2 = 1101004800(0x41a00000, float:20.0)
                r1.setTextSize(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.ePaint     // Catch:{ Exception -> 0x0cfa }
                r2 = -65536(0xffffffffffff0000, float:NaN)
                r1.setColor(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.ePaint     // Catch:{ Exception -> 0x0cfa }
                r2 = 1
                r1.setFakeBoldText(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.ePaint     // Catch:{ Exception -> 0x0cfa }
                r2 = 1
                r1.setAntiAlias(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.ePaint     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint$Style r2 = android.graphics.Paint.Style.FILL     // Catch:{ Exception -> 0x0cfa }
                r1.setStyle(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.ePaint     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Typeface r2 = android.graphics.Typeface.SANS_SERIF     // Catch:{ Exception -> 0x0cfa }
                r1.setTypeface(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.rectPaint     // Catch:{ Exception -> 0x0cfa }
                r2 = -16777216(0xffffffffff000000, float:-1.7014118E38)
                r1.setColor(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.rectPaint     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint$Style r2 = android.graphics.Paint.Style.FILL     // Catch:{ Exception -> 0x0cfa }
                r1.setStyle(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.rectPaint     // Catch:{ Exception -> 0x0cfa }
                r2 = 1
                r1.setAntiAlias(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.rectPaint     // Catch:{ Exception -> 0x0cfa }
                android.graphics.BlurMaskFilter r2 = new android.graphics.BlurMaskFilter     // Catch:{ Exception -> 0x0cfa }
                r3 = 1097859072(0x41700000, float:15.0)
                android.graphics.BlurMaskFilter$Blur r4 = android.graphics.BlurMaskFilter.Blur.INNER     // Catch:{ Exception -> 0x0cfa }
                r2.<init>(r3, r4)     // Catch:{ Exception -> 0x0cfa }
                r1.setMaskFilter(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.tPaint     // Catch:{ Exception -> 0x0cfa }
                r2 = -1
                r1.setColor(r2)     // Catch:{ Exception -> 0x0cfa }
                int r1 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                r2 = 300(0x12c, float:4.2E-43)
                if (r1 <= r2) goto L_0x0cbd
                int r1 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                r2 = 360(0x168, float:5.04E-43)
                if (r1 >= r2) goto L_0x0cbd
                android.graphics.Paint r1 = r11.fpsPaint     // Catch:{ Exception -> 0x0cfa }
                r2 = 1092616192(0x41200000, float:10.0)
                r1.setTextSize(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.fpsPaint     // Catch:{ Exception -> 0x0cfa }
                r2 = 1
                r1.setFakeBoldText(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.fpsPaint     // Catch:{ Exception -> 0x0cfa }
                r2 = 1
                r1.setAntiAlias(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.fpsPaint     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint$Style r2 = android.graphics.Paint.Style.FILL     // Catch:{ Exception -> 0x0cfa }
                r1.setStyle(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.tPaint     // Catch:{ Exception -> 0x0cfa }
                r2 = 1092616192(0x41200000, float:10.0)
                r1.setTextSize(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.tPaint     // Catch:{ Exception -> 0x0cfa }
                r2 = 1
                r1.setFakeBoldText(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.tPaint     // Catch:{ Exception -> 0x0cfa }
                r2 = 1
                r1.setAntiAlias(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.tPaint     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint$Style r2 = android.graphics.Paint.Style.FILL     // Catch:{ Exception -> 0x0cfa }
                r1.setStyle(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.tPaint     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Typeface r2 = android.graphics.Typeface.SANS_SERIF     // Catch:{ Exception -> 0x0cfa }
                r1.setTypeface(r2)     // Catch:{ Exception -> 0x0cfa }
            L_0x057c:
                java.lang.String r1 = com.oziapp.coolLanding.Options.sound_OnOff     // Catch:{ Exception -> 0x0cfa }
                java.lang.String r2 = "on"
                boolean r1 = r1.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x0cfa }
                if (r1 == 0) goto L_0x05fe
                android.media.MediaPlayer r1 = new android.media.MediaPlayer     // Catch:{ Exception -> 0x0cfa }
                r1.<init>()     // Catch:{ Exception -> 0x0cfa }
                r11.landsound = r1     // Catch:{ Exception -> 0x0cfa }
                android.media.MediaPlayer r1 = new android.media.MediaPlayer     // Catch:{ Exception -> 0x0cfa }
                r1.<init>()     // Catch:{ Exception -> 0x0cfa }
                r11.crash = r1     // Catch:{ Exception -> 0x0cfa }
                android.media.MediaPlayer r1 = new android.media.MediaPlayer     // Catch:{ Exception -> 0x0cfa }
                r1.<init>()     // Catch:{ Exception -> 0x0cfa }
                r11.warningsound = r1     // Catch:{ Exception -> 0x0cfa }
                android.media.MediaPlayer r1 = new android.media.MediaPlayer     // Catch:{ Exception -> 0x0cfa }
                r1.<init>()     // Catch:{ Exception -> 0x0cfa }
                r11.runway1 = r1     // Catch:{ Exception -> 0x0cfa }
                android.media.MediaPlayer r1 = new android.media.MediaPlayer     // Catch:{ Exception -> 0x0cfa }
                r1.<init>()     // Catch:{ Exception -> 0x0cfa }
                r11.runway2 = r1     // Catch:{ Exception -> 0x0cfa }
                android.media.MediaPlayer r1 = new android.media.MediaPlayer     // Catch:{ Exception -> 0x0cfa }
                r1.<init>()     // Catch:{ Exception -> 0x0cfa }
                r11.helipad = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.Context r1 = r12.getBaseContext()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130968579(0x7f040003, float:1.7545816E38)
                android.media.MediaPlayer r1 = android.media.MediaPlayer.create(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r11.landsound = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.Context r1 = r12.getBaseContext()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130968578(0x7f040002, float:1.7545814E38)
                android.media.MediaPlayer r1 = android.media.MediaPlayer.create(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r11.crash = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.Context r1 = r12.getBaseContext()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130968583(0x7f040007, float:1.7545824E38)
                android.media.MediaPlayer r1 = android.media.MediaPlayer.create(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r11.warningsound = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.Context r1 = r12.getBaseContext()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130968582(0x7f040006, float:1.7545822E38)
                android.media.MediaPlayer r1 = android.media.MediaPlayer.create(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r11.runway1 = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.Context r1 = r12.getBaseContext()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130968582(0x7f040006, float:1.7545822E38)
                android.media.MediaPlayer r1 = android.media.MediaPlayer.create(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r11.runway2 = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.Context r1 = r12.getBaseContext()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130968582(0x7f040006, float:1.7545822E38)
                android.media.MediaPlayer r1 = android.media.MediaPlayer.create(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r11.helipad = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x05fe:
                java.lang.String r1 = com.oziapp.coolLanding.Options.sound_OnOff     // Catch:{ Exception -> 0x0cfa }
                java.lang.String r2 = "off"
                r1.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x0cfa }
                java.lang.String r1 = com.oziapp.coolLanding.Options.bgsound_OnOff     // Catch:{ Exception -> 0x0cfa }
                java.lang.String r2 = "on"
                boolean r1 = r1.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x0cfa }
                if (r1 == 0) goto L_0x0627
                android.content.Context r1 = r12.getBaseContext()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130968580(0x7f040004, float:1.7545818E38)
                android.media.MediaPlayer r1 = android.media.MediaPlayer.create(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r12.backgroundSound = r1     // Catch:{ Exception -> 0x0cfa }
                android.media.MediaPlayer r1 = r12.backgroundSound     // Catch:{ Exception -> 0x0cfa }
                r2 = 1
                r1.setLooping(r2)     // Catch:{ Exception -> 0x0cfa }
                android.media.MediaPlayer r1 = r12.backgroundSound     // Catch:{ Exception -> 0x0cfa }
                r1.start()     // Catch:{ Exception -> 0x0cfa }
            L_0x0627:
                java.lang.String r1 = com.oziapp.coolLanding.Options.bgsound_OnOff     // Catch:{ Exception -> 0x0cfa }
                java.lang.String r2 = "off"
                r1.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x0cfa }
                java.lang.String r1 = com.oziapp.coolLanding.Options.clouds_OnOff     // Catch:{ Exception -> 0x0cfa }
                java.lang.String r2 = "on"
                boolean r1 = r1.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x0cfa }
                if (r1 == 0) goto L_0x063b
                r1 = 1
                r11.flgClouds = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x063b:
                java.lang.String r1 = com.oziapp.coolLanding.Options.clouds_OnOff     // Catch:{ Exception -> 0x0cfa }
                java.lang.String r2 = "off"
                boolean r1 = r1.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x0cfa }
                if (r1 == 0) goto L_0x0648
                r1 = 0
                r11.flgClouds = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0648:
                java.lang.String r1 = com.oziapp.coolLanding.Options.showfps_OnOff     // Catch:{ Exception -> 0x0cfa }
                java.lang.String r2 = "off"
                boolean r1 = r1.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x0cfa }
                if (r1 == 0) goto L_0x0655
                r1 = 0
                r11.flgfps = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0655:
                int r1 = com.oziapp.coolLanding.SelectMap.map_Select     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837575(0x7f020047, float:1.7280108E38)
                if (r1 != r2) goto L_0x0841
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837575(0x7f020047, float:1.7280108E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r11.bitmap = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.SharedPreferences r1 = r12.prefsPrivate     // Catch:{ Exception -> 0x0cfa }
                java.lang.String r2 = "KEY_PRIVATE1"
                int r3 = r11.pscore1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1.getInt(r2, r3)     // Catch:{ Exception -> 0x0cfa }
                r12.highscore = r1     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.tPaint     // Catch:{ Exception -> 0x0cfa }
                r2 = -1
                r1.setColor(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.fpsPaint     // Catch:{ Exception -> 0x0cfa }
                r2 = -1
                r1.setColor(r2)     // Catch:{ Exception -> 0x0cfa }
                r1 = 4604068630017653448(0x3fe4f0068db8bac8, double:0.6543000000000001)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.YellowZoneX1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4601497074630424893(0x3fdbcd35a858793d, double:0.43439999999999995)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.YellowZoneX2 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4602874275396474792(0x3fe0b1c432ca57a8, double:0.5217)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.YellowZoneY1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4603511985103710454(0x3fe2f5c28f5c28f6, double:0.5925)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.YellowZoneY2 = r1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r11.YellowZoneX1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x06bf
                int r1 = r11.YellowZoneX1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.YellowZoneX1 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x06bf:
                int r1 = r11.YellowZoneX2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x06cb
                int r1 = r11.YellowZoneX2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.YellowZoneX2 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x06cb:
                int r1 = r11.YellowZoneY1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x06d7
                int r1 = r11.YellowZoneY1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.YellowZoneY1 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x06d7:
                int r1 = r11.YellowZoneY2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x06e3
                int r1 = r11.YellowZoneY2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.YellowZoneY2 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x06e3:
                r1 = 4597267293860398524(0x3fccc63f141205bc, double:0.2248)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.RedZoneX1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4602426617593514164(0x3fdf1a9fbe76c8b4, double:0.486)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.RedZoneX2 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4597753682620154537(0x3fce809d495182a9, double:0.23829999999999998)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.RedZoneY1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4599166011463297925(0x3fd3851eb851eb85, double:0.305)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.RedZoneY2 = r1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r11.RedZoneX1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x071f
                int r1 = r11.RedZoneX1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.RedZoneX1 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x071f:
                int r1 = r11.RedZoneX2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x072b
                int r1 = r11.RedZoneX2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.RedZoneX2 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x072b:
                int r1 = r11.RedZoneY1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x0737
                int r1 = r11.RedZoneY1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.RedZoneY1 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0737:
                int r1 = r11.RedZoneY2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x0743
                int r1 = r11.RedZoneY2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.RedZoneY2 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0743:
                r1 = 4605009882339773880(0x3fe84816f0068db8, double:0.7587999999999999)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.helipadX1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4605769189236948547(0x3feafaacd9e83e43, double:0.8431000000000001)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.helipadX2 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4604161404169977279(0x3fe54467381d7dbf, double:0.6646)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.helipadY1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4605230558721515035(0x3fe910cb295e9e1b, double:0.7833)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.helipadY2 = r1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r11.helipadX1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x077f
                int r1 = r11.helipadX1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.helipadX1 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x077f:
                int r1 = r11.helipadX2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x078b
                int r1 = r11.helipadX2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.helipadX2 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x078b:
                int r1 = r11.helipadY1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x0797
                int r1 = r11.helipadY1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.helipadY1 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0797:
                int r1 = r11.helipadY2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x07a3
                int r1 = r11.helipadY2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.helipadY2 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x07a3:
                r1 = 4604566728136440623(0x3fe6b50b0f27bb2f, double:0.7095999999999999)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempZone1X1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4603129179135383962(0x3fe199999999999a, double:0.55)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempZone1Y1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4604925214666779315(0x3fe7fb15b573eab3, double:0.7494)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempZone1X2 = r1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r1 = (double) r1     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r9
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempZone1Y2 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4595750481505900141(0x3fc762b6ae7d566d, double:0.1827)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempZone2X1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4598324739052905116(0x3fd087fcb923a29c, double:0.2583)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempZone2Y1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4597436629206387654(0x3fcd604189374bc6, double:0.22949999999999998)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempZone2X2 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4599526299433487565(0x3fd4cccccccccccd, double:0.325)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempZone2Y2 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4604482060463446058(0x3fe66809d495182a, double:0.7001999999999999)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.templandingZone1X1 = r1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r1 = (double) r1     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r7
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.templandingZone1Y1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4597688830785520402(0x3fce45a1cac08312, double:0.2365)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.templandingZone2X1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4599075939470750515(0x3fd3333333333333, double:0.3)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.templandingZone2Y1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4605305318475329385(0x3fe954c985f06f69, double:0.7916)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempheliX1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4604555018777409460(0x3fe6aa64c2f837b4, double:0.7082999999999999)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempheliY1 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0841:
                int r1 = com.oziapp.coolLanding.SelectMap.map_Select     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837572(0x7f020044, float:1.7280102E38)
                if (r1 != r2) goto L_0x0aa2
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837572(0x7f020044, float:1.7280102E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r11.bitmap = r1     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.tPaint     // Catch:{ Exception -> 0x0cfa }
                r2 = -1
                r1.setColor(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.fpsPaint     // Catch:{ Exception -> 0x0cfa }
                r2 = -1
                r1.setColor(r2)     // Catch:{ Exception -> 0x0cfa }
                android.content.SharedPreferences r1 = r12.prefsPrivate     // Catch:{ Exception -> 0x0cfa }
                java.lang.String r2 = "KEY_PRIVATE2"
                int r3 = r11.pscore1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1.getInt(r2, r3)     // Catch:{ Exception -> 0x0cfa }
                r12.highscore = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4600263088332525379(0x3fd76ae7d566cf43, double:0.36590000000000006)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.YellowZoneX1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4601160205378297581(0x3fda9ad42c3c9eed, double:0.4157)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.YellowZoneX2 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4599075939470750515(0x3fd3333333333333, double:0.3)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.YellowZoneY1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4602978758907829787(0x3fe110cb295e9e1b, double:0.5333)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.YellowZoneY2 = r1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r11.YellowZoneX1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x08ab
                int r1 = r11.YellowZoneX1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.YellowZoneX1 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x08ab:
                int r1 = r11.YellowZoneX2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x08b7
                int r1 = r11.YellowZoneX2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.YellowZoneX2 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x08b7:
                int r1 = r11.YellowZoneY1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x08c3
                int r1 = r11.YellowZoneY1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.YellowZoneY1 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x08c3:
                int r1 = r11.YellowZoneY2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x08cf
                int r1 = r11.YellowZoneY2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.YellowZoneY2 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x08cf:
                r1 = 4602707642210262083(0x3fe01a36e2eb1c43, double:0.5032)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.RedZoneX1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4603206641048974734(0x3fe1e00d1b71758e, double:0.5586)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.RedZoneX2 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4604757680760641133(0x3fe762b6ae7d566d, double:0.7308)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.RedZoneY1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4602941829390885349(0x3fe0ef34d6a161e5, double:0.5292)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.RedZoneY2 = r1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r11.RedZoneX1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x090b
                int r1 = r11.RedZoneX1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.RedZoneX1 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x090b:
                int r1 = r11.RedZoneX2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x0917
                int r1 = r11.RedZoneX2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.RedZoneX2 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0917:
                int r1 = r11.RedZoneY1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x0923
                int r1 = r11.RedZoneY1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.RedZoneY1 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0923:
                int r1 = r11.RedZoneY2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x092f
                int r1 = r11.RedZoneY2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.RedZoneY2 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x092f:
                r1 = 4600104561625641937(0x3fd6dab9f559b3d1, double:0.35710000000000003)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.helipadX1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4601644792698202646(0x3fdc538ef34d6a16, double:0.4426)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.helipadX2 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4603275996483236240(0x3fe21f212d773190, double:0.5663)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.helipadY1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4604367669032910848(0x3fe6000000000000, double:0.6875)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.helipadY2 = r1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r11.helipadX1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x0968
                int r1 = r11.helipadX1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.helipadX1 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0968:
                int r1 = r11.helipadX2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x0974
                int r1 = r11.helipadX2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.helipadX2 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0974:
                int r1 = r11.helipadY1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x0980
                int r1 = r11.helipadY1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.helipadY1 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0980:
                int r1 = r11.helipadY2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x098c
                int r1 = r11.helipadY2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.helipadY2 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x098c:
                r1 = 4603858762275017982(0x3fe43126e978d4fe, double:0.631)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.zeplinX1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4604787404518181778(0x3fe77dbf487fcb92, double:0.7341)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.zeplinX2 = r1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r1 = (double) r1     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r7
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.zeplinY1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4604180319288412236(0x3fe5559b3d07c84c, double:0.6667000000000001)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.zeplinY2 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4602922013552524919(0x3fe0dd2f1a9fbe77, double:0.527)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.landingX1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4605117968730830772(0x3fe8aa64c2f837b4, double:0.7707999999999999)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.landingY1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4601688027254625403(0x3fdc7ae147ae147b, double:0.445)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.endlandingX = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4604273093440736068(0x3fe5a9fbe76c8b44, double:0.677)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.endlandingY = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4600421615039408819(0x3fd7fb15b573eab3, double:0.3747)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempZone1X1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4598324739052905116(0x3fd087fcb923a29c, double:0.2583)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempZone1Y1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4601097154983514394(0x3fda617c1bda511a, double:0.4122)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempZone1X2 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4599526299433487565(0x3fd4cccccccccccd, double:0.325)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempZone1Y2 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4603192229530167149(0x3fe1d2f1a9fbe76d, double:0.557)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempZone2X2 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4602831941559977509(0x3fe08b4395810625, double:0.517)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempZone2X1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4605119770170681720(0x3fe8ac083126e978, double:0.7709999999999999)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempZone2Y1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4605344950152050245(0x3fe978d4fdf3b645, double:0.7959999999999999)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempZone2Y2 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4600877379321698714(0x3fd999999999999a, double:0.4)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.templandingZone1X1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4599827139888595914(0x3fd5de69ad42c3ca, double:0.3417)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.templandingZone1Y1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4600843151964530698(0x3fd97a786c22680a, double:0.3981)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempheliX1 = r1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r1 = (double) r1     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r9
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempheliY1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4603070632340228145(0x3fe1645a1cac0831, double:0.5435)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.templandingZone2X1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4602965248108947676(0x3fe104816f0068dc, double:0.5318)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.templandingZone2X2 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4605306219195254860(0x3fe9559b3d07c84c, double:0.7917000000000001)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.templandingZone2Y1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4605117968730830772(0x3fe8aa64c2f837b4, double:0.7707999999999999)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.templandingZone2Y2 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0aa2:
                int r1 = com.oziapp.coolLanding.SelectMap.map_Select     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837569(0x7f020041, float:1.7280096E38)
                if (r1 != r2) goto L_0x0cfc
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837569(0x7f020041, float:1.7280096E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r11.bitmap = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.SharedPreferences r1 = r12.prefsPrivate     // Catch:{ Exception -> 0x0cfa }
                java.lang.String r2 = "KEY_PRIVATE3"
                int r3 = r11.pscore1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1.getInt(r2, r3)     // Catch:{ Exception -> 0x0cfa }
                r12.highscore = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4604239766803493526(0x3fe58bac710cb296, double:0.6733)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.YellowZoneX1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4605473753101393042(0x3fe9edfa43fe5c92, double:0.8103)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.YellowZoneX2 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4603246272725695594(0x3fe204189374bc6a, double:0.563)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.YellowZoneY1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4603879478833303886(0x3fe443fe5c91d14e, double:0.6333)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.YellowZoneY2 = r1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r11.YellowZoneX1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x0b00
                int r1 = r11.YellowZoneX1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.YellowZoneX1 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0b00:
                int r1 = r11.YellowZoneX2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x0b0c
                int r1 = r11.YellowZoneX2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.YellowZoneX2 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0b0c:
                int r1 = r11.YellowZoneY1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x0b18
                int r1 = r11.YellowZoneY1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.YellowZoneY1 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0b18:
                int r1 = r11.YellowZoneY2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x0b24
                int r1 = r11.YellowZoneY2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.YellowZoneY2 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0b24:
                r1 = 4601518691908636273(0x3fdbe0ded288ce71, double:0.43560000000000004)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.RedZoneX1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4602383383037091407(0x3fdef34d6a161e4f, double:0.4836)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.RedZoneX2 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4597876180530019016(0x3fcef0068db8bac8, double:0.24170000000000003)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.RedZoneY1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4593311331947716280(0x3fbeb851eb851eb8, double:0.12)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.RedZoneY2 = r1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r11.RedZoneX1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x0b60
                int r1 = r11.RedZoneX1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.RedZoneX1 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0b60:
                int r1 = r11.RedZoneX2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x0b6c
                int r1 = r11.RedZoneX2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.RedZoneX2 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0b6c:
                int r1 = r11.RedZoneY1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x0b78
                int r1 = r11.RedZoneY1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.RedZoneY1 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0b78:
                int r1 = r11.RedZoneY2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x0b84
                int r1 = r11.RedZoneY2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.RedZoneY2 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0b84:
                r1 = 4601075537705303016(0x3fda4dd2f1a9fbe8, double:0.41100000000000003)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.helipadX1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4602953538749916512(0x3fe0f9db22d0e560, double:0.5305)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.helipadX2 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4604762184360268503(0x3fe766cf41f212d7, double:0.7313)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.helipadY1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4606056518893174784(0x3fec000000000000, double:0.875)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.helipadY2 = r1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r11.helipadX1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x0bbd
                int r1 = r11.helipadX1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.helipadX1 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0bbd:
                int r1 = r11.helipadX2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x0bc9
                int r1 = r11.helipadX2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.helipadX2 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0bc9:
                int r1 = r11.helipadY1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x0bd5
                int r1 = r11.helipadY1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.helipadY1 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0bd5:
                int r1 = r11.helipadY2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x0be1
                int r1 = r11.helipadY2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.helipadY2 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0be1:
                r1 = 4603807421239265958(0x3fe4027525460aa6, double:0.6253)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempZone1X1 = r1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r1 = (double) r1     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r7
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempZone1Y1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4604334342395668306(0x3fe5e1b089a02752, double:0.6838)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempZone1X2 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4603955139307043711(0x3fe488ce703afb7f, double:0.6417)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempZone1Y2 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4601560125025208081(0x3fdc068db8bac711, double:0.4379)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempZone2X1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4599601959907227389(0x3fd5119ce075f6fd, double:0.3292)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempZone2Y1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4602405000315302785(0x3fdf06f694467381, double:0.48479999999999995)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempZone2X2 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4600801718847958889(0x3fd954c985f06f69, double:0.3958)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempZone2Y2 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4604376676232165588(0x3fe6083126e978d4, double:0.6884999999999999)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.templandingZone1X1 = r1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r1 = (double) r1     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r9
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.templandingZone1Y1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4601979860510479011(0x3fdd844d013a92a3, double:0.4612)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.templandingZone2X1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4599225458978379215(0x3fd3bb2fec56d5cf, double:0.30829999999999996)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.templandingZone2Y1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4601940228833758150(0x3fdd604189374bc6, double:0.45899999999999996)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempheliX1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4605380978949069210(0x3fe999999999999a, double:0.8)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempheliY1 = r1     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.tPaint     // Catch:{ Exception -> 0x0cfa }
                r2 = -1
                r1.setColor(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.fpsPaint     // Catch:{ Exception -> 0x0cfa }
                r2 = -1
                r1.setColor(r2)     // Catch:{ Exception -> 0x0cfa }
            L_0x0c8b:
                int r1 = com.oziapp.coolLanding.SelectMap.map_Select     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837511(0x7f020007, float:1.7279978E38)
                if (r1 != r2) goto L_0x0cab
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837511(0x7f020007, float:1.7279978E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r11.bitmap = r1     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.tPaint     // Catch:{ Exception -> 0x0cfa }
                r2 = -1
                r1.setColor(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.fpsPaint     // Catch:{ Exception -> 0x0cfa }
                r2 = -1
                r1.setColor(r2)     // Catch:{ Exception -> 0x0cfa }
            L_0x0cab:
                android.graphics.Bitmap r1 = r11.bitmap     // Catch:{ Exception -> 0x0cfa }
                int r2 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                r4 = 1
                android.graphics.Bitmap r1 = android.graphics.Bitmap.createScaledBitmap(r1, r2, r3, r4)     // Catch:{ Exception -> 0x0cfa }
                r11.bitmap = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4607182418800017408(0x3ff0000000000000, double:1.0)
                com.oziapp.coolLanding.DragGraphicObject.gamespeed = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0cbc:
                return
            L_0x0cbd:
                android.graphics.Paint r1 = r11.fpsPaint     // Catch:{ Exception -> 0x0cfa }
                r2 = 1101004800(0x41a00000, float:20.0)
                r1.setTextSize(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.fpsPaint     // Catch:{ Exception -> 0x0cfa }
                r2 = 1
                r1.setFakeBoldText(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.fpsPaint     // Catch:{ Exception -> 0x0cfa }
                r2 = 1
                r1.setAntiAlias(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.fpsPaint     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint$Style r2 = android.graphics.Paint.Style.FILL     // Catch:{ Exception -> 0x0cfa }
                r1.setStyle(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.tPaint     // Catch:{ Exception -> 0x0cfa }
                r2 = 1101004800(0x41a00000, float:20.0)
                r1.setTextSize(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.tPaint     // Catch:{ Exception -> 0x0cfa }
                r2 = 1
                r1.setFakeBoldText(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.tPaint     // Catch:{ Exception -> 0x0cfa }
                r2 = 1
                r1.setAntiAlias(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.tPaint     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint$Style r2 = android.graphics.Paint.Style.FILL     // Catch:{ Exception -> 0x0cfa }
                r1.setStyle(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.tPaint     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Typeface r2 = android.graphics.Typeface.SANS_SERIF     // Catch:{ Exception -> 0x0cfa }
                r1.setTypeface(r2)     // Catch:{ Exception -> 0x0cfa }
                goto L_0x057c
            L_0x0cfa:
                r1 = move-exception
                goto L_0x0cbc
            L_0x0cfc:
                int r1 = com.oziapp.coolLanding.SelectMap.map_Select     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837565(0x7f02003d, float:1.7280088E38)
                if (r1 != r2) goto L_0x0c8b
                android.content.res.Resources r1 = r11.getResources()     // Catch:{ Exception -> 0x0cfa }
                r2 = 2130837565(0x7f02003d, float:1.7280088E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)     // Catch:{ Exception -> 0x0cfa }
                r11.bitmap = r1     // Catch:{ Exception -> 0x0cfa }
                android.content.SharedPreferences r1 = r12.prefsPrivate     // Catch:{ Exception -> 0x0cfa }
                java.lang.String r2 = "KEY_PRIVATE4"
                int r3 = r11.pscore1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1.getInt(r2, r3)     // Catch:{ Exception -> 0x0cfa }
                r12.highscore = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4603131881295160384(0x3fe19c0ebedfa440, double:0.5503)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.YellowZoneX1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4600063128509070127(0x3fd6b50b0f27bb2f, double:0.35479999999999995)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.YellowZoneX2 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4604930618986332160(0x3fe8000000000000, double:0.75)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.YellowZoneY1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4605775494276426865(0x3feb0068db8bac71, double:0.8438)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.YellowZoneY2 = r1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r11.YellowZoneX1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x0d57
                int r1 = r11.YellowZoneX1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.YellowZoneX1 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0d57:
                int r1 = r11.YellowZoneX2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x0d63
                int r1 = r11.YellowZoneX2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.YellowZoneX2 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0d63:
                int r1 = r11.YellowZoneY1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x0d6f
                int r1 = r11.YellowZoneY1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.YellowZoneY1 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0d6f:
                int r1 = r11.YellowZoneY2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x0d7b
                int r1 = r11.YellowZoneY2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.YellowZoneY2 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0d7b:
                r1 = 4599029102034625862(0x3fd3089a02752546, double:0.2974)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.helipadX1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4600317131528053824(0x3fd79c0ebedfa440, double:0.3689)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.helipadX2 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4603335443998317530(0x3fe25532617c1bda, double:0.5729)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.helipadY1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4604367669032910848(0x3fe6000000000000, double:0.6875)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.helipadY2 = r1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r11.helipadX1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x0db4
                int r1 = r11.helipadX1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.helipadX1 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0db4:
                int r1 = r11.helipadX2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x0dc0
                int r1 = r11.helipadX2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.helipadX2 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0dc0:
                int r1 = r11.helipadY1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x0dcc
                int r1 = r11.helipadY1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.helipadY1 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0dcc:
                int r1 = r11.helipadY2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x0dd8
                int r1 = r11.helipadY2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.helipadY2 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0dd8:
                r1 = 4596254884664165637(0x3fc92d77318fc505, double:0.1967)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.RedZoneX1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4598031104357200561(0x3fcf7ced916872b1, double:0.24600000000000002)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.RedZoneX2 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4599675818941116265(0x3fd554c985f06f69, double:0.3333)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.RedZoneY1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4603804719079489536(0x3fe4000000000000, double:0.625)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.RedZoneY2 = r1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r11.RedZoneX1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x0e11
                int r1 = r11.RedZoneX1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.RedZoneX1 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0e11:
                int r1 = r11.RedZoneX2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x0e1d
                int r1 = r11.RedZoneX2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.RedZoneX2 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0e1d:
                int r1 = r11.RedZoneY1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x0e29
                int r1 = r11.RedZoneY1     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.RedZoneY1 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0e29:
                int r1 = r11.RedZoneY2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 % 2
                if (r1 == 0) goto L_0x0e35
                int r1 = r11.RedZoneY2     // Catch:{ Exception -> 0x0cfa }
                int r1 = r1 + 1
                r11.RedZoneY2 = r1     // Catch:{ Exception -> 0x0cfa }
            L_0x0e35:
                r1 = 4603448934708927267(0x3fe2bc6a7ef9db23, double:0.5855)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempZone1X1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4605043208977016422(0x3fe8666666666666, double:0.7625)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempZone1Y1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4603768690282470572(0x3fe3df3b645a1cac, double:0.621)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempZone1X2 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4605775494276426865(0x3feb0068db8bac71, double:0.8438)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempZone1Y2 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4596298119220588393(0x3fc954c985f06f69, double:0.1979)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempZone2X1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4598542713274869848(0x3fd14e3bcd35a858, double:0.2704)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempZone2Y1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4598027501477498664(0x3fcf79a6b50b0f28, double:0.2459)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempZone2X2 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4599938829159354702(0x3fd643fe5c91d14e, double:0.3479)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempZone2Y2 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4603342649757721323(0x3fe25bc01a36e2eb, double:0.5737)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.templandingZone1X1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4605418809185939122(0x3fe9bc01a36e2eb2, double:0.8042)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.templandingZone1Y1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4597184427627254907(0x3fcc7ae147ae147b, double:0.2225)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.templandingZone2X1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4600427019358961664(0x3fd8000000000000, double:0.375)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.templandingZone2Y1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4599704641978731437(0x3fd56f0068db8bad, double:0.33490000000000003)
                int r3 = r11.screen_width     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempheliX1 = r1     // Catch:{ Exception -> 0x0cfa }
                r1 = 4603917309070173798(0x3fe4666666666666, double:0.6375)
                int r3 = r11.screen_height     // Catch:{ Exception -> 0x0cfa }
                double r3 = (double) r3     // Catch:{ Exception -> 0x0cfa }
                double r1 = r1 * r3
                int r1 = (int) r1     // Catch:{ Exception -> 0x0cfa }
                r11.tempheliY1 = r1     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.tPaint     // Catch:{ Exception -> 0x0cfa }
                r2 = -1
                r1.setColor(r2)     // Catch:{ Exception -> 0x0cfa }
                android.graphics.Paint r1 = r11.fpsPaint     // Catch:{ Exception -> 0x0cfa }
                r2 = -1
                r1.setColor(r2)     // Catch:{ Exception -> 0x0cfa }
                goto L_0x0c8b
            */
            throw new UnsupportedOperationException("Method not decompiled: com.oziapp.coolLanding.DragMain.CustomView.<init>(com.oziapp.coolLanding.DragMain, android.content.Context):void");
        }

        private void ShowAd() {
            DragMain.this._ad.setVisibility(0);
            if (0 == 0) {
                AlphaAnimation animation = new AlphaAnimation(0.0f, 1.0f);
                animation.setDuration(400);
                animation.setFillAfter(true);
                animation.setInterpolator(new AccelerateInterpolator());
                DragMain.this._ad.startAnimation(animation);
            }
        }

        public void onDraw(Canvas cnv) {
            try {
                cnv.drawBitmap(this.bitmap, 0.0f, 0.0f, (Paint) null);
                cnv.drawBitmap(DragMain.this.chances, (float) (this.screen_width / 4), (float) (this.screen_height - DragMain.this.chances.getHeight()), (Paint) null);
                cnv.drawText(" X " + this.life_count, (float) ((this.screen_width / 4) + DragMain.this.chances.getWidth()), (float) (this.screen_height - (DragMain.this.chances.getHeight() / 2)), this.tPaint);
                if (this.flgfps) {
                    cnv.drawRect(10.0f, 0.0f, 100.0f, 30.0f, this.rectPaint);
                    cnv.drawText("FPS : " + this.fps, 20.0f, 20.0f, this.fpsPaint);
                }
                if (!DragMain.this.flgPause) {
                    DragMain.this.miliseconds = (System.currentTimeMillis() - DragMain.this.start) - DragMain.this.pauseTime;
                    DragMain.this.seconds = (int) (((float) DragMain.this.miliseconds) / 1000.0f);
                    DragMain.minutes = DragMain.this.seconds / 60;
                    DragMain.this.seconds %= 60;
                    DragMain.this.startpause = System.currentTimeMillis();
                    DragMain.this.emergencymilliseconds = System.currentTimeMillis() - DragMain.this.emergencyStart;
                    DragMain.this.emergencySeconds = (int) (((float) DragMain.this.emergencymilliseconds) / 1000.0f);
                    DragMain.this.emergencySeconds %= 60;
                } else if (DragMain.this.flgPause) {
                    DragMain.this.endpause = System.currentTimeMillis();
                }
                cnv.drawRect((float) (this.screen_width - 150), 0.0f, (float) this.screen_width, 30.0f, this.rectPaint);
                cnv.drawText("Score : " + DragMain.this.score, (float) (this.screen_width - 140), 20.0f, this.tPaint);
                cnv.drawRect((float) ((this.screen_width / 2) - 20), 0.0f, (float) ((this.screen_width / 2) + 150), 30.0f, this.rectPaint);
                cnv.drawText("HighScore : " + DragMain.this.highscore, (float) (this.screen_width / 2), 20.0f, this.tPaint);
                if (DragMain.this.GameSpeed) {
                    cnv.drawBitmap(DragMain.this.ply, 20.0f, (float) (this.screen_height - 50), this.mBitmapPaint);
                } else {
                    cnv.drawBitmap(DragMain.this.frwd, 20.0f, (float) (this.screen_height - 50), this.mBitmapPaint);
                }
                if (DragMain.this.score > DragMain.this.highscore && DragMain.this.highscore > 0) {
                    if (this.cou <= 36 && this.cou >= 0) {
                        if (this.cou == 1) {
                            if (this.achievement != null) {
                                this.achievement.stop();
                                this.achievement.release();
                                this.achievement = null;
                            }
                            this.achievement = new MediaPlayer();
                            this.achievement = MediaPlayer.create(DragMain.this.getBaseContext(), (int) R.raw.achi);
                            try {
                                this.achievement.prepare();
                            } catch (Exception e) {
                            }
                            if (this.achievement != null && !this.achievement.isPlaying()) {
                                this.achievement.start();
                            }
                        }
                        if (this.cou <= 4) {
                            cnv.drawBitmap(DragMain.this.hi_score, (float) (this.screen_width - 700), (float) (this.screen_height - 170), this.mBitmapPaint);
                        }
                        if (this.cou <= 8 && this.cou > 4) {
                            cnv.drawBitmap(DragMain.this.hi_score2, (float) (this.screen_width - 700), (float) (this.screen_height - 170), this.mBitmapPaint);
                        }
                        if (this.cou <= 12 && this.cou > 8) {
                            cnv.drawBitmap(DragMain.this.hi_score, (float) (this.screen_width - 700), (float) (this.screen_height - 170), this.mBitmapPaint);
                        }
                        if (this.cou <= 16 && this.cou > 12) {
                            cnv.drawBitmap(DragMain.this.hi_score2, (float) (this.screen_width - 700), (float) (this.screen_height - 170), this.mBitmapPaint);
                        }
                        if (this.cou <= 20 && this.cou > 16) {
                            cnv.drawBitmap(DragMain.this.hi_score, (float) (this.screen_width - 700), (float) (this.screen_height - 170), this.mBitmapPaint);
                        }
                        if (this.cou <= 24 && this.cou > 20) {
                            cnv.drawBitmap(DragMain.this.hi_score2, (float) (this.screen_width - 700), (float) (this.screen_height - 170), this.mBitmapPaint);
                        }
                        if (this.cou <= 28 && this.cou > 24) {
                            cnv.drawBitmap(DragMain.this.hi_score, (float) (this.screen_width - 700), (float) (this.screen_height - 170), this.mBitmapPaint);
                        }
                        if (this.cou <= 32 && this.cou > 28) {
                            cnv.drawBitmap(DragMain.this.hi_score2, (float) (this.screen_width - 700), (float) (this.screen_height - 170), this.mBitmapPaint);
                        }
                        if (this.cou <= 36 && this.cou > 32) {
                            cnv.drawBitmap(DragMain.this.hi_score, (float) (this.screen_width - 700), (float) (this.screen_height - 170), this.mBitmapPaint);
                        }
                    }
                    this.cou++;
                }
                if (this.redmark) {
                    if (this.counter >= 0 && this.counter <= 3) {
                        cnv.drawBitmap(DragMain.this.mark, (float) this.RedZoneX1, (float) this.RedZoneY1, (Paint) null);
                        this.counter++;
                    } else if (this.counter >= 3 && this.counter <= 6) {
                        cnv.drawBitmap(DragMain.this.mark1, (float) this.RedZoneX1, (float) this.RedZoneY1, (Paint) null);
                        this.counter++;
                    } else if (this.counter > 6) {
                        this.counter = 0;
                    }
                } else if (this.yellowmark) {
                    if (this.counter1 >= 0 && this.counter1 <= 3) {
                        cnv.drawBitmap(DragMain.this.mark, (float) this.YellowZoneX1, (float) (this.YellowZoneY1 + 5), (Paint) null);
                        this.counter1++;
                    }
                    if (this.counter1 >= 3 && this.counter1 <= 6) {
                        cnv.drawBitmap(DragMain.this.mark1, (float) this.YellowZoneX1, (float) (this.YellowZoneY1 + 5), (Paint) null);
                        this.counter1++;
                    }
                    if (this.counter1 > 6) {
                        this.counter1 = 0;
                    }
                } else if (this.greenmark) {
                    if (this.counter2 >= 0 && this.counter2 <= 3) {
                        cnv.drawBitmap(DragMain.this.mark, (float) (this.helipadX1 + 10), (float) (this.helipadY1 + 5), (Paint) null);
                        this.counter2++;
                    }
                    if (this.counter2 >= 3 && this.counter2 <= 6) {
                        cnv.drawBitmap(DragMain.this.mark1, (float) (this.helipadX1 + 10), (float) (this.helipadY1 + 5), (Paint) null);
                        this.counter2++;
                    }
                    if (this.counter2 > 6) {
                        this.counter2 = 0;
                    }
                }
                if (!DragMain.this.flgPause) {
                    cnv.drawBitmap(DragMain.this.pause, (float) (this.screen_width - 50), (float) (this.screen_height - 70), this.mBitmapPaint);
                } else {
                    cnv.drawBitmap(DragMain.this.play, (float) (this.screen_width - 50), (float) (this.screen_height - 70), this.mBitmapPaint);
                }
                if (this.msgcounter >= 0 && this.msgcounter <= 20) {
                    cnv.drawText(this.msg, (float) (this.screen_width / 2), (float) (this.screen_height / 2), this.msgPaint);
                    this.msgcounter++;
                }
                if (this.msgcounter >= 20 && this.msgcounter <= 40) {
                    cnv.drawText(this.msg, (float) (this.screen_width / 2), (float) (this.screen_height / 2), this.msgPaint2);
                    this.msgcounter++;
                }
                for (int i2 = 0; i2 < this.flying_Objects.size(); i2++) {
                    if (!this.flying_Objects.get(i2).flgOnSurface) {
                        this.flying_Objects.get(i2).draw_Notification(cnv, DragMain.this.arrivalsmall, DragMain.this.arrivallarge);
                    }
                    if (this.flying_Objects.get(i2).PlaneType == 3) {
                        if (this.flying_Objects.get(i2).helicounter == 0) {
                            this.flying_Objects.get(i2).setBitmap(this.flying_Objects.get(i2).heli1);
                            this.flying_Objects.get(i2).helicounter++;
                        } else if (this.flying_Objects.get(i2).helicounter == 1) {
                            this.flying_Objects.get(i2).setBitmap(this.flying_Objects.get(i2).heli2);
                            this.flying_Objects.get(i2).helicounter++;
                        } else if (this.flying_Objects.get(i2).helicounter == 2) {
                            this.flying_Objects.get(i2).setBitmap(this.flying_Objects.get(i2).heli3);
                            this.flying_Objects.get(i2).helicounter = 0;
                        }
                    }
                    this.flying_Objects.get(i2).draw(cnv);
                }
                if (this.flgClouds) {
                    cnv.drawBitmap(DragMain.this.clouds, 0.0f, (float) (0.3125d * ((double) this.screen_height)), this.mBitmapPaint);
                    cnv.drawBitmap(DragMain.this.clouds, (float) (0.6579d * ((double) this.screen_width)), 0.0f, this.mBitmapPaint);
                    cnv.drawBitmap(DragMain.this.clouds, (float) (0.614d * ((double) this.screen_width)), (float) (0.625d * ((double) this.screen_height)), this.mBitmapPaint);
                }
            } catch (Exception e2) {
            }
        }

        public void updatePhysics(float sleepp, int fpss) {
            this.sleep = sleepp;
            this.fps = fpss;
            if (!DragMain.this.flgPause) {
                lifeIncrease();
                this.helicheck++;
                if (this.helicheck > 100) {
                    this.helicheck = 0;
                }
                for (int j = 0; j < this.flying_Objects.size() - 1; j++) {
                    if (!this.flying_Objects.get(j).flgLandingMode && this.flying_Objects.get(j).flgOnSurface) {
                        for (int in = j + 1; in < this.flying_Objects.size(); in++) {
                            if (!this.flying_Objects.get(in).flgLandingMode && this.flying_Objects.get(in).flgOnSurface) {
                                warningCheck(this.flying_Objects.get(j), this.flying_Objects.get(in));
                                crashCheck(this.flying_Objects.get(j), this.flying_Objects.get(in));
                            }
                        }
                    }
                }
                DragMain.this.check++;
                if (DragGraphicObject.gamespeed == 1.0d) {
                    if (Options.difficulty_Level == "hard") {
                        if (DragMain.this.check > 75) {
                            DragMain.this.check = 0;
                            addplanes();
                        }
                    } else if ((Options.difficulty_Level == "easy" || Options.difficulty_Level == "normal") && DragMain.this.check > 150) {
                        DragMain.this.check = 0;
                        addplanes();
                    }
                } else if (Options.difficulty_Level == "hard") {
                    if (DragMain.this.check > 35) {
                        DragMain.this.check = 0;
                        addplanes();
                    }
                } else if ((Options.difficulty_Level == "easy" || Options.difficulty_Level == "normal") && DragMain.this.check > 80) {
                    DragMain.this.check = 0;
                    addplanes();
                }
                if (Options.difficulty_Level == "easy" || Options.difficulty_Level == "normal") {
                    if (DragMain.minutes == 0 && DragMain.this.seconds == 30) {
                        DragMain.this.max = 3;
                    }
                    if (DragMain.minutes == 1 && DragMain.this.seconds == 0) {
                        DragMain.this.max = 5;
                    } else if (DragMain.minutes == 1 && DragMain.this.seconds == 30) {
                        DragMain.this.max = 6;
                    } else if (DragMain.minutes == 2 && DragMain.this.seconds == 0) {
                        DragMain.this.max = 7;
                    } else if (DragMain.minutes == 2 && DragMain.this.seconds == 30) {
                        DragMain.this.max = 8;
                    } else if (DragMain.minutes == 3 && DragMain.this.seconds == 0) {
                        DragMain.this.max = 9;
                    } else if (DragMain.minutes == 3 && DragMain.this.seconds == 30) {
                        if (Options.difficulty_Level == "easy") {
                            DragMain.this.max = 9;
                        } else if (Options.difficulty_Level == "normal") {
                            DragMain.this.max = 11;
                        }
                    }
                } else if (Options.difficulty_Level == "hard") {
                    if (DragMain.minutes == 0 && DragMain.this.seconds == 20) {
                        DragMain.this.max = 3;
                    }
                    if (DragMain.minutes == 0 && DragMain.this.seconds == 40) {
                        DragMain.this.max = 5;
                    } else if (DragMain.minutes == 1 && DragMain.this.seconds == 0) {
                        DragMain.this.max = 6;
                    } else if (DragMain.minutes == 1 && DragMain.this.seconds == 20) {
                        DragMain.this.max = 7;
                    } else if (DragMain.minutes == 1 && DragMain.this.seconds == 40) {
                        DragMain.this.max = 8;
                    } else if (DragMain.minutes == 2 && DragMain.this.seconds == 0) {
                        DragMain.this.max = 9;
                    } else if (DragMain.minutes == 2 && DragMain.this.seconds == 20) {
                        DragMain.this.max = 13;
                    }
                }
                for (int i2 = 0; i2 < this.flying_Objects.size(); i2++) {
                    if (!this.flying_Objects.get(i2).flgOnSurface && this.flying_Objects.get(i2).getX() - ((float) (this.flying_Objects.get(i2).getBitmap().getWidth() / 2)) > -20.0f && this.flying_Objects.get(i2).getX() + ((float) (this.flying_Objects.get(i2).getBitmap().getWidth() / 2)) < ((float) (this.screen_width + 20)) && this.flying_Objects.get(i2).getY() - ((float) (this.flying_Objects.get(i2).getBitmap().getHeight() / 2)) > -20.0f && this.flying_Objects.get(i2).getY() + ((float) (this.flying_Objects.get(i2).getBitmap().getHeight() / 2)) < ((float) (this.screen_height + 20))) {
                        this.flying_Objects.get(i2).flgOnSurface = true;
                    }
                    if (this.flying_Objects.get(i2).flgLandingMode && this.flying_Objects.get(i2).warningObject != null) {
                        RemoveWarning(this.flying_Objects.get(i2).warningObject);
                        this.flying_Objects.get(i2).warningObject = null;
                    }
                    if (this.flying_Objects.get(i2).PlaneType == 1) {
                        if (this.flying_Objects.get(i2).flgGoforLand) {
                            if (this.flying_Objects.get(i2).x_index == this.flying_Objects.get(i2).routes.size()) {
                                this.flying_Objects.get(i2).mPath.reset();
                                if (!this.flying_Objects.get(i2).flgLandingMode) {
                                    if (this.landsound != null) {
                                        this.landsound.start();
                                    }
                                    this.flying_Objects.get(i2).setBitmap(DragMain.this.yellowPlaneshadow);
                                    this.flying_Objects.get(i2).flgLandingMode = true;
                                    DragMain.this.score++;
                                    this.flying_Objects.remove(i2);
                                    this.flying_Objects.add(0, this.flying_Objects.get(i2));
                                }
                            }
                            if (SelectMap.map_Select == R.drawable.background1) {
                                if (this.flying_Objects.get(i2).getX() > ((float) (this.YellowZoneX2 - 40)) && this.flying_Objects.get(i2).flgLandingMode) {
                                    this.flying_Objects.remove(i2);
                                }
                            } else if (SelectMap.map_Select == R.drawable.mountainjoy || SelectMap.map_Select == R.drawable.kindamixed) {
                                if (this.flying_Objects.get(i2).getX() < ((float) (this.YellowZoneX2 + 40)) && this.flying_Objects.get(i2).flgLandingMode) {
                                    this.flying_Objects.remove(i2);
                                }
                            } else if (SelectMap.map_Select == R.drawable.meadowolives) {
                                if (this.flying_Objects.get(i2).getY() > ((float) this.YellowZoneY2) && this.flying_Objects.get(i2).flgLandingMode) {
                                    this.flying_Objects.remove(i2);
                                }
                            } else if (SelectMap.map_Select == R.drawable.maritimelands && this.flying_Objects.get(i2).getX() > ((float) (this.YellowZoneX2 - 20)) && this.flying_Objects.get(i2).flgLandingMode) {
                                this.flying_Objects.remove(i2);
                            }
                        }
                    } else if (this.flying_Objects.get(i2).PlaneType == 2) {
                        if (this.flying_Objects.get(i2).flgGoforLand) {
                            if (SelectMap.map_Select == R.drawable.background1 || SelectMap.map_Select == R.drawable.mountainjoy) {
                                if (this.flying_Objects.get(i2).x_index == this.flying_Objects.get(i2).routes.size()) {
                                    this.flying_Objects.get(i2).mPath.reset();
                                    if (!this.flying_Objects.get(i2).flgLandingMode) {
                                        if (this.landsound != null) {
                                            this.landsound.start();
                                        }
                                        this.flying_Objects.get(i2).setBitmap(DragMain.this.redPlaneshadow);
                                        this.flying_Objects.get(i2).flgLandingMode = true;
                                        DragMain.this.score++;
                                        this.flying_Objects.remove(i2);
                                        this.flying_Objects.add(0, this.flying_Objects.get(i2));
                                    }
                                }
                                if (this.flying_Objects.get(i2).getX() > ((float) (this.RedZoneX2 - 40)) && this.flying_Objects.get(i2).flgLandingMode) {
                                    this.flying_Objects.remove(i2);
                                }
                            } else if (SelectMap.map_Select == R.drawable.maritimelands) {
                                if (this.flying_Objects.get(i2).x_index == this.flying_Objects.get(i2).routes.size()) {
                                    this.flying_Objects.get(i2).mPath.reset();
                                    if (!this.flying_Objects.get(i2).flgLandingMode) {
                                        if (this.landsound != null) {
                                            this.landsound.start();
                                        }
                                        this.flying_Objects.get(i2).setBitmap(DragMain.this.redPlaneshadow);
                                        this.flying_Objects.get(i2).flgLandingMode = true;
                                        DragMain.this.score++;
                                        this.flying_Objects.remove(i2);
                                        this.flying_Objects.add(0, this.flying_Objects.get(i2));
                                    }
                                }
                                if (this.flying_Objects.get(i2).getY() < ((float) (this.RedZoneY1 - 30)) && this.flying_Objects.get(i2).flgLandingMode) {
                                    this.flying_Objects.remove(i2);
                                }
                            } else if (SelectMap.map_Select == R.drawable.meadowolives) {
                                if (this.flying_Objects.get(i2).x_index == this.flying_Objects.get(i2).routes.size()) {
                                    this.flying_Objects.get(i2).setBitmap(DragMain.this.redPlaneshadow);
                                    this.flying_Objects.get(i2).mPath.reset();
                                    if (!this.flying_Objects.get(i2).flgLandingMode) {
                                        if (this.landsound != null) {
                                            this.landsound.start();
                                        }
                                        this.flying_Objects.get(i2).flgLandingMode = true;
                                        this.flying_Objects.get(i2).mX = (float) this.landingX1;
                                        this.flying_Objects.get(i2).mY = (float) this.landingY1;
                                        this.flying_Objects.get(i2).Xf = (float) this.endlandingX;
                                        this.flying_Objects.get(i2).Yf = (float) this.endlandingY;
                                        DragMain.this.score++;
                                        this.flying_Objects.remove(i2);
                                        this.flying_Objects.add(0, this.flying_Objects.get(i2));
                                    }
                                }
                                if (this.flying_Objects.get(i2).getX() < ((float) (this.templandingZone2X1 - 60)) && this.flying_Objects.get(i2).flgLandingMode) {
                                    this.flying_Objects.remove(i2);
                                }
                            } else if (SelectMap.map_Select == R.drawable.kindamixed) {
                                if (this.flying_Objects.get(i2).x_index == this.flying_Objects.get(i2).routes.size()) {
                                    this.flying_Objects.get(i2).setBitmap(DragMain.this.redPlaneshadow);
                                    this.flying_Objects.get(i2).mPath.reset();
                                    if (!this.flying_Objects.get(i2).flgLandingMode) {
                                        if (this.landsound != null) {
                                            this.landsound.start();
                                        }
                                        this.flying_Objects.get(i2).flgLandingMode = true;
                                        DragMain.this.score++;
                                        this.flying_Objects.remove(i2);
                                        this.flying_Objects.add(0, this.flying_Objects.get(i2));
                                    }
                                }
                                if (this.flying_Objects.get(i2).getY() > ((float) this.RedZoneY2) && this.flying_Objects.get(i2).flgLandingMode) {
                                    this.flying_Objects.remove(i2);
                                }
                            }
                        }
                    } else if (this.flying_Objects.get(i2).PlaneType == 3 && this.flying_Objects.get(i2).flgGoforLand) {
                        if (this.flying_Objects.get(i2).x_index == this.flying_Objects.get(i2).routes.size()) {
                            this.flying_Objects.get(i2).mPath.reset();
                            this.flying_Objects.get(i2).setTouched(true);
                            if (!this.flying_Objects.get(i2).flgLandingMode) {
                                if (this.landsound != null) {
                                    this.landsound.start();
                                }
                                this.flying_Objects.get(i2).heli1 = DragMain.this.heliland1;
                                this.flying_Objects.get(i2).heli2 = DragMain.this.heliland2;
                                this.flying_Objects.get(i2).heli3 = DragMain.this.heliland3;
                                this.flying_Objects.get(i2).flgLandingMode = true;
                                DragMain.this.score++;
                                this.flying_Objects.remove(i2);
                                this.flying_Objects.add(0, this.flying_Objects.get(i2));
                            }
                        }
                        if (this.helicheck == 100 && this.flying_Objects.get(i2).flgLandingMode) {
                            this.flying_Objects.remove(i2);
                            this.helicheck = 0;
                        }
                    }
                }
                for (int i3 = 0; i3 < this.flying_Objects.size(); i3++) {
                    if (!this.flying_Objects.get(i3).isTouched()) {
                        this.flying_Objects.get(i3).update(this.screen_width, this.screen_height);
                    }
                }
            }
        }

        public void lifeIncrease() {
            if (DragMain.this.score >= this.life_increse_on) {
                this.life_increse_on += 50;
                this.life_count++;
            }
        }

        private void addplanes() {
            if (this.flying_Objects.size() < DragMain.this.max) {
                this.direction = this.rand2.nextInt(10);
                this.planetype = this.rand.nextInt(3);
                if (this.direction == 0 && DragMain.this.predir != 0) {
                    DragMain.this.predir = this.direction;
                    this.mX = (float) this.d;
                    this.mY = -50.0f;
                    this.Xf = (float) (this.d * 2);
                    this.Yf = 500.0f;
                    this.Arrivalx = this.d;
                    this.Arrivaly = 20;
                } else if (this.direction == 1 && DragMain.this.predir != 1) {
                    DragMain.this.predir = this.direction;
                    this.mX = (float) (this.d * 2);
                    this.mY = -50.0f;
                    this.Xf = (float) (this.d / 3);
                    this.Yf = 500.0f;
                    this.Arrivalx = this.d * 2;
                    this.Arrivaly = 20;
                } else if (this.direction == 2 && DragMain.this.predir != 2) {
                    DragMain.this.predir = this.direction;
                    this.mX = (float) (this.d * 3);
                    this.mY = -50.0f;
                    this.Xf = (float) (this.d / 2);
                    this.Yf = 500.0f;
                    this.Arrivalx = this.d * 3;
                    this.Arrivaly = 20;
                } else if (this.direction == 3 && DragMain.this.predir != 3) {
                    DragMain.this.predir = this.direction;
                    this.mX = (float) (this.d * 4);
                    this.mY = -50.0f;
                    this.Xf = (float) (this.d / 3);
                    this.Yf = 500.0f;
                    this.Arrivalx = ((int) this.mX) - 10;
                    this.Arrivaly = 20;
                } else if (this.direction == 4 && DragMain.this.predir != 4) {
                    DragMain.this.predir = this.direction;
                    this.mX = (float) this.d;
                    this.mY = (float) (this.screen_height + 50);
                    this.Xf = (float) (this.d / 3);
                    this.Yf = 0.0f;
                    this.Arrivalx = (int) this.mX;
                    this.Arrivaly = this.screen_height - 20;
                } else if (this.direction == 5 && DragMain.this.predir != 5) {
                    DragMain.this.predir = this.direction;
                    this.mX = (float) (this.d * 2);
                    this.mY = (float) (this.screen_height + 50);
                    this.Xf = (float) (this.d / 3);
                    this.Yf = 0.0f;
                    this.Arrivalx = (int) this.mX;
                    this.Arrivaly = this.screen_height - 20;
                } else if (this.direction == 6 && DragMain.this.predir != 6) {
                    DragMain.this.predir = this.direction;
                    this.mX = (float) (this.d * 3);
                    this.mY = (float) (this.screen_height + 50);
                    this.Xf = (float) (this.d / 2);
                    this.Yf = 0.0f;
                    this.Arrivalx = (int) this.mX;
                    this.Arrivaly = this.screen_height - 20;
                } else if (this.direction == 7 && DragMain.this.predir != 7) {
                    DragMain.this.predir = this.direction;
                    this.mX = (float) (this.d * 4);
                    this.mY = (float) (this.screen_height + 50);
                    this.Xf = (float) (this.d / 2);
                    this.Yf = 0.0f;
                    this.Arrivalx = ((int) this.mX) - 10;
                    this.Arrivaly = this.screen_height - 20;
                } else if (this.direction == 8 && DragMain.this.predir != 8) {
                    DragMain.this.predir = this.direction;
                    this.mX = -50.0f;
                    this.mY = (float) (this.d2 - 100);
                    this.Xf = (float) this.d;
                    this.Yf = (float) (this.d2 + 100);
                    this.Arrivalx = 20;
                    this.Arrivaly = (int) this.mY;
                } else if (this.direction == 9 && DragMain.this.predir != 9) {
                    DragMain.this.predir = this.direction;
                    this.mX = -50.0f;
                    this.mY = (float) ((this.d2 * 2) - 100);
                    this.Xf = (float) this.d;
                    this.Yf = (float) this.d2;
                    this.Arrivalx = 20;
                    this.Arrivaly = (int) this.mY;
                } else if (this.elsedirection == 0) {
                    this.mX = -50.0f;
                    this.mY = -50.0f;
                    this.Xf = (float) (this.screen_width - 10);
                    this.Yf = (float) (this.screen_height - 20);
                    this.Arrivalx = 10;
                    this.Arrivaly = 10;
                    this.elsedirection++;
                } else if (this.elsedirection == 1) {
                    this.mX = (float) (this.screen_width + 30);
                    this.mY = -30.0f;
                    this.Xf = 0.0f;
                    this.Yf = (float) this.screen_height;
                    this.Arrivalx = this.screen_width - 10;
                    this.Arrivaly = 10;
                    this.elsedirection--;
                }
                if (this.planetype == 0) {
                    DragGraphicObject objYellow = new DragGraphicObject(this.yellowPlane, this.mX, this.mY, this.Xf, this.Yf, 1);
                    objYellow.Arrivalx = this.Arrivalx;
                    objYellow.Arrivaly = this.Arrivaly;
                    this.flying_Objects.add(objYellow);
                } else if (this.planetype == 1) {
                    DragGraphicObject objRed = new DragGraphicObject(this.redPlane, this.mX, this.mY, this.Xf, this.Yf, 2);
                    objRed.Arrivalx = this.Arrivalx;
                    objRed.Arrivaly = this.Arrivaly;
                    this.flying_Objects.add(objRed);
                } else if (this.planetype == 2) {
                    DragGraphicObject objHeli = new DragGraphicObject(DragMain.this.helinormal1, this.mX, this.mY, this.Xf, this.Yf, 3);
                    objHeli.Arrivalx = this.Arrivalx;
                    objHeli.Arrivaly = this.Arrivaly;
                    objHeli.heli1 = DragMain.this.helinormal1;
                    objHeli.heli2 = DragMain.this.helinormal2;
                    objHeli.heli3 = DragMain.this.helinormal3;
                    this.flying_Objects.add(objHeli);
                }
            }
        }

        private DragGraphicObject GetTouchedObject(float eventX, float eventY) {
            for (int i2 = 0; i2 < this.flying_Objects.size(); i2++) {
                if (!this.flying_Objects.get(i2).flgLandingMode && this.flying_Objects.get(i2).flgOnSurface) {
                    float x = this.flying_Objects.get(i2).getX();
                    float y = this.flying_Objects.get(i2).getY();
                    Bitmap bitmap2 = this.flying_Objects.get(i2).getBitmap();
                    if (eventX >= (x - ((float) (bitmap2.getWidth() / 2))) - 15.0f && eventX <= ((float) (bitmap2.getWidth() / 2)) + x + 15.0f && eventY >= (y - ((float) (bitmap2.getHeight() / 2))) - 15.0f && eventY <= ((float) (bitmap2.getHeight() / 2)) + y + 15.0f) {
                        DragGraphicObject objTouched2 = this.flying_Objects.get(i2);
                        objTouched2.setTouched(true);
                        return objTouched2;
                    }
                }
            }
            return null;
        }

        public boolean onTouchEvent(MotionEvent evnt) {
            if (evnt.getAction() == 0) {
                this.down_mX = evnt.getX();
                this.down_mY = evnt.getY();
                if (this.down_mX > ((float) (this.screen_width - 55)) && this.down_mX < ((float) (this.screen_width - 20)) && this.down_mY > ((float) (this.screen_height - 60)) && this.down_mY < ((float) (this.screen_height - 20)) && !DragMain.this.flgPause) {
                    DragMain.this.alert.show();
                    DragMain.this.emergencySeconds = 0;
                    DragMain.this.flgPause = true;
                }
                if (!DragMain.this.flgPause) {
                    if (this.down_mX < 50.0f && this.down_mY > ((float) (this.screen_height - 50)) && DragMain.this.GameSpeed) {
                        DragMain.this.GameSpeed = false;
                        DragGraphicObject.gamespeed = 0.5d;
                    } else if (this.down_mX < 50.0f && this.down_mY > ((float) (this.screen_height - 50)) && !DragMain.this.GameSpeed) {
                        DragMain.this.GameSpeed = true;
                        DragGraphicObject.gamespeed = 1.0d;
                    }
                    this.lineX = this.down_mX;
                    this.lineY = this.down_mY;
                    this.objTouched = GetTouchedObject(this.down_mX, this.down_mY);
                }
                if (this.objTouched == null || this.objTouched.flgcrash) {
                    return true;
                }
                this.objTouched.x_index = 0;
                this.objTouched.n = 0;
                this.objTouched.flgGoforLand = false;
                this.objTouched.flglandmsg = false;
                this.objTouched.flgNotMOVE_ON_LINE = true;
                this.objTouched.flgDrawLine = false;
                this.objTouched.mPath = new Path();
                this.objTouched.mPaint.setStrokeWidth(5.0f);
                this.objTouched.mPaint.setPathEffect(this.objTouched.dashPath);
                this.objTouched.mPath.moveTo(evnt.getX(), evnt.getY());
                this.objTouched.routes = new ArrayList<>();
                if (this.objTouched.PlaneType == 1) {
                    this.objTouched.setBitmap(DragMain.this.yellowPlaneglow);
                    this.yellowmark = true;
                    this.redmark = false;
                    return true;
                } else if (this.objTouched.PlaneType == 2) {
                    this.objTouched.setBitmap(DragMain.this.redPlaneglow);
                    this.yellowmark = false;
                    this.redmark = true;
                    return true;
                } else if (this.objTouched.PlaneType != 3) {
                    return true;
                } else {
                    this.yellowmark = false;
                    this.redmark = false;
                    this.greenmark = true;
                    this.objTouched.heli1 = DragMain.this.helig1;
                    this.objTouched.heli2 = DragMain.this.helig2;
                    this.objTouched.heli3 = DragMain.this.helig3;
                    return true;
                }
            } else if (evnt.getAction() == 2) {
                float x = evnt.getX();
                float y = evnt.getY();
                this.x_index = 0;
                float dx = Math.abs(evnt.getX() - this.down_mX);
                float dy = Math.abs(evnt.getY() - this.down_mY);
                if ((dx < 5.0f && dy < 5.0f) || this.objTouched == null || this.objTouched.flgcrash) {
                    return true;
                }
                this.objTouched.routes.add(new RouteNates((int) evnt.getX(), (int) evnt.getY()));
                if (this.objTouched.isTouched()) {
                    this.objTouched.Xf = x;
                    this.objTouched.Yf = y;
                    this.objTouched.flgdelta = true;
                }
                this.objTouched.setTouched(false);
                if (this.objTouched.PlaneType == 1) {
                    if (!this.objTouched.routes.isEmpty() && !this.objTouched.flgGoforLand && x >= ((float) this.tempZone1X1) && x <= ((float) this.tempZone1X2) && y >= ((float) (this.tempZone1Y1 - 10)) && y <= ((float) (this.tempZone1Y2 + 10))) {
                        if (this.runway1 != null) {
                            this.runway1.start();
                        }
                        this.objTouched.routes = DouglasPeuckerReduction(this.objTouched.routes, Double.valueOf(2.0d));
                        if (SelectMap.map_Select == R.drawable.background1 || SelectMap.map_Select == R.drawable.maritimelands) {
                            this.objTouched.routes.add(new RouteNates(this.templandingZone1X1, this.templandingZone1Y1));
                            this.objTouched.routes.add(new RouteNates(this.templandingZone1X1 + 30, this.templandingZone1Y1));
                        }
                        if (SelectMap.map_Select == R.drawable.mountainjoy || SelectMap.map_Select == R.drawable.kindamixed) {
                            this.objTouched.routes.add(new RouteNates(this.templandingZone1X1, this.templandingZone1Y1));
                            this.objTouched.routes.add(new RouteNates(this.templandingZone1X1 - 30, this.templandingZone1Y1));
                        }
                        if (SelectMap.map_Select == R.drawable.meadowolives) {
                            this.objTouched.routes.add(new RouteNates(this.templandingZone1X1, this.templandingZone1Y1));
                            this.objTouched.routes.add(new RouteNates(this.templandingZone1X1, this.templandingZone1Y1 + 15));
                        }
                        this.objTouched.mPaint.setStrokeWidth(0.0f);
                        this.objTouched.mPaint.setPathEffect(null);
                        this.objTouched.flgGoforLand = true;
                        this.objTouched.setBitmap(DragMain.this.yellowPlaneshadow);
                        this.objTouched.flgdelta = true;
                        this.objTouched.flgNotMOVE_ON_LINE = false;
                        this.objTouched.flgDrawLine = true;
                        this.objTouched.setTouched(false);
                        this.msg = " Good Job!";
                        this.msgcounter = 0;
                        this.objTouched.flgpathmsg = true;
                        this.objTouched.flglandmsg = false;
                        this.objTouched = null;
                    }
                } else if (this.objTouched.PlaneType == 2) {
                    if (!this.objTouched.routes.isEmpty() && !this.objTouched.flgGoforLand && x >= ((float) (this.tempZone2X1 - 10)) && x <= ((float) this.tempZone2X2) && y >= ((float) (this.tempZone2Y1 - 10)) && y <= ((float) (this.tempZone2Y2 + 10))) {
                        if (this.runway1 != null) {
                            this.runway1.start();
                        }
                        this.objTouched.routes = DouglasPeuckerReduction(this.objTouched.routes, Double.valueOf(2.0d));
                        if (SelectMap.map_Select == R.drawable.background1 || SelectMap.map_Select == R.drawable.mountainjoy) {
                            this.objTouched.routes.add(new RouteNates(this.templandingZone2X1, this.templandingZone2Y1));
                            this.objTouched.routes.add(new RouteNates(this.templandingZone2X1 + 30, this.templandingZone2Y1));
                        }
                        if (SelectMap.map_Select == R.drawable.maritimelands) {
                            this.objTouched.routes.add(new RouteNates(this.templandingZone2X1, this.templandingZone2Y1));
                            this.objTouched.routes.add(new RouteNates(this.templandingZone2X1, this.templandingZone2Y1 - 15));
                        }
                        if (SelectMap.map_Select == R.drawable.meadowolives) {
                            this.objTouched.routes.add(new RouteNates(this.templandingZone2X1, this.templandingZone2Y1));
                            this.objTouched.routes.add(new RouteNates(this.templandingZone2X2, this.templandingZone2Y2));
                        }
                        if (SelectMap.map_Select == R.drawable.kindamixed) {
                            this.objTouched.routes.add(new RouteNates(this.templandingZone2X1, this.templandingZone2Y1));
                            this.objTouched.routes.add(new RouteNates(this.templandingZone2X1, this.templandingZone2Y1 + 15));
                        }
                        this.objTouched.mPaint.setStrokeWidth(0.0f);
                        this.objTouched.mPaint.setPathEffect(null);
                        this.objTouched.flgGoforLand = true;
                        this.objTouched.setBitmap(DragMain.this.redPlaneshadow);
                        this.objTouched.flgNotMOVE_ON_LINE = false;
                        this.objTouched.flgDrawLine = true;
                        this.msg = " Good Job!";
                        this.msgcounter = 0;
                        this.objTouched.flgpathmsg = true;
                        this.objTouched.flglandmsg = false;
                        this.objTouched.setTouched(false);
                        this.objTouched = null;
                    }
                } else if (this.objTouched.PlaneType == 3 && !this.objTouched.routes.isEmpty() && !this.objTouched.flgGoforLand && x >= ((float) (this.helipadX1 - 5)) && x <= ((float) this.helipadX2) && y >= ((float) (this.helipadY1 - 10)) && y <= ((float) this.helipadY2)) {
                    if (this.runway1 != null) {
                        this.runway1.start();
                    }
                    this.objTouched.routes = DouglasPeuckerReduction(this.objTouched.routes, Double.valueOf(2.0d));
                    this.objTouched.routes.add(new RouteNates(this.tempheliX1, this.tempheliY1 + 5));
                    this.objTouched.routes.add(new RouteNates(this.tempheliX1 + 5, this.tempheliY1 + 5));
                    this.objTouched.routes.add(new RouteNates(this.tempheliX1 + 10, this.tempheliY1 + 5));
                    this.objTouched.heli1 = DragMain.this.heliland1;
                    this.objTouched.heli2 = DragMain.this.heliland2;
                    this.objTouched.heli3 = DragMain.this.heliland3;
                    this.objTouched.mPaint.setStrokeWidth(0.0f);
                    this.objTouched.mPaint.setPathEffect(null);
                    this.objTouched.flgGoforLand = true;
                    this.objTouched.flgNotMOVE_ON_LINE = false;
                    this.objTouched.flgDrawLine = true;
                    this.msg = " Good Job!";
                    this.msgcounter = 0;
                    this.objTouched.flgpathmsg = true;
                    this.objTouched.flglandmsg = false;
                    this.objTouched.setTouched(false);
                    this.objTouched = null;
                }
                if (y <= ((float) this.screen_height)) {
                    return true;
                }
                this.objTouched.mPaint.setStrokeWidth(0.0f);
                this.objTouched.mPaint.setPathEffect(null);
                if (this.objTouched.PlaneType == 1) {
                    this.objTouched.setBitmap(this.yellowPlane);
                } else if (this.objTouched.PlaneType == 2) {
                    this.objTouched.setBitmap(this.redPlane);
                } else if (this.objTouched.PlaneType == 3) {
                    this.objTouched.heli1 = DragMain.this.helinormal1;
                    this.objTouched.heli2 = DragMain.this.helinormal2;
                    this.objTouched.heli3 = DragMain.this.helinormal3;
                }
                this.objTouched.flgdelta = true;
                this.objTouched.flgNotMOVE_ON_LINE = false;
                this.objTouched.flgDrawLine = true;
                this.objTouched.setTouched(false);
                this.objTouched = null;
                return true;
            } else if (evnt.getAction() != 1) {
                return true;
            } else {
                if (this.redmark) {
                    this.redmark = false;
                }
                if (this.yellowmark) {
                    this.yellowmark = false;
                }
                if (this.greenmark) {
                    this.greenmark = false;
                }
                this.x_index = 0;
                if (this.objTouched == null || this.objTouched.flgcrash) {
                    return true;
                }
                this.up_Xf = evnt.getX();
                this.up_Yf = evnt.getY();
                this.objTouched.routes = DouglasPeuckerReduction(this.objTouched.routes, Double.valueOf(2.0d));
                if (this.objTouched.PlaneType == 1) {
                    this.objTouched.setBitmap(this.yellowPlane);
                    this.objTouched.setTouched(false);
                } else if (this.objTouched.PlaneType == 2) {
                    this.objTouched.setBitmap(this.redPlane);
                    this.objTouched.setTouched(false);
                } else if (this.objTouched.PlaneType == 3) {
                    this.objTouched.heli1 = DragMain.this.helinormal1;
                    this.objTouched.heli2 = DragMain.this.helinormal2;
                    this.objTouched.heli3 = DragMain.this.helinormal3;
                    this.objTouched.setTouched(false);
                }
                this.objTouched.flgNotMOVE_ON_LINE = false;
                this.objTouched.flgDrawLine = true;
                this.objTouched.routes.isEmpty();
                this.objTouched.mPaint.setStrokeWidth(0.0f);
                this.msg = " Landing Zone Missed";
                this.msgcounter = 0;
                this.objTouched.flglandmsg = false;
                this.objTouched.mPaint.setPathEffect(null);
                this.objTouched = null;
                return true;
            }
        }

        /* Debug info: failed to restart local var, previous not found, register: 8 */
        public ArrayList<RouteNates> DouglasPeuckerReduction(ArrayList<RouteNates> Points, Double Tolerance) {
            if (Points == null || Points.size() < 3) {
                return Points;
            }
            int lastPoint = Points.size() - 1;
            List<Integer> pointIndexsToKeep = new ArrayList<>();
            pointIndexsToKeep.add(0);
            pointIndexsToKeep.add(Integer.valueOf(lastPoint));
            DouglasPeuckerReduction(Points, 0, lastPoint, Tolerance, pointIndexsToKeep);
            List<RouteNates> returnPoints = new ArrayList<>();
            Collections.sort(pointIndexsToKeep);
            for (Integer intValue : pointIndexsToKeep) {
                returnPoints.add(Points.get(intValue.intValue()));
            }
            return (ArrayList) returnPoints;
        }

        private void DouglasPeuckerReduction(List<RouteNates> points, int firstPoint, int lastPoint, Double tolerance, List<Integer> pointIndexsToKeep) {
            Double maxDistance = Double.valueOf(0.0d);
            int indexFarthest = 0;
            for (int index2 = firstPoint; index2 < lastPoint; index2++) {
                Double distance = PerpendicularDistance(points.get(firstPoint), points.get(lastPoint), points.get(index2));
                if (distance.doubleValue() > maxDistance.doubleValue()) {
                    maxDistance = distance;
                    indexFarthest = index2;
                }
            }
            if (maxDistance.doubleValue() > tolerance.doubleValue() && indexFarthest != 0) {
                pointIndexsToKeep.add(Integer.valueOf(indexFarthest));
                DouglasPeuckerReduction(points, firstPoint, indexFarthest, tolerance, pointIndexsToKeep);
                DouglasPeuckerReduction(points, indexFarthest, lastPoint, tolerance, pointIndexsToKeep);
            }
        }

        public Double PerpendicularDistance(RouteNates Point1, RouteNates Point2, RouteNates Point) {
            return Double.valueOf((Double.valueOf(Math.abs(0.5d * ((double) ((((((Point1.posX * Point2.posY) + (Point2.posX * Point.posY)) + (Point.posX * Point1.posY)) - (Point2.posX * Point1.posY)) - (Point.posX * Point2.posY)) - (Point1.posX * Point.posY))))).doubleValue() / Double.valueOf(Math.sqrt(Math.pow((double) (Point1.posX - Point2.posX), 2.0d) + Math.pow((double) (Point1.posY - Point2.posY), 2.0d))).doubleValue()) * 2.0d);
        }

        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        }

        public void surfaceCreated(SurfaceHolder holder) {
            this._thread = new TutorialThread(getHolder(), this, DragMain.this.m_Handler);
            this._thread.setRunning(true);
            this._thread.start();
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
            if (DragMain.this.backgroundSound != null) {
                DragMain.this.backgroundSound.setLooping(false);
                DragMain.this.backgroundSound.stop();
            }
            boolean retry = true;
            this._thread.setRunning(false);
            while (retry) {
                try {
                    this._thread.join();
                    retry = false;
                } catch (InterruptedException e) {
                }
            }
        }

        public void warningCheck(DragGraphicObject obj1, DragGraphicObject obj2) {
            if (((obj1.getX() + ((float) obj1.getBitmap().getWidth()) + 10.0f < obj2.getX() || obj1.getX() + ((float) obj1.getBitmap().getWidth()) + 10.0f > obj2.getX() + ((float) obj2.getBitmap().getWidth())) && (obj2.getX() + ((float) obj2.getBitmap().getWidth()) + 10.0f < obj1.getX() || obj2.getX() + ((float) obj2.getBitmap().getWidth()) + 10.0f > obj1.getX() + ((float) obj1.getBitmap().getWidth()))) || ((obj1.getY() + ((float) obj1.getBitmap().getHeight()) + 10.0f < obj2.getY() || obj1.getY() + ((float) obj1.getBitmap().getHeight()) + 10.0f > obj2.getY() + ((float) obj2.getBitmap().getHeight())) && (obj2.getY() + ((float) obj2.getBitmap().getHeight()) + 10.0f < obj1.getY() || obj2.getY() + ((float) obj2.getBitmap().getHeight()) + 10.0f > obj1.getY() + ((float) obj1.getBitmap().getHeight())))) {
                if (obj1.flgWarning && obj2.flgWarning) {
                    RemoveWarning(obj1);
                    RemoveWarning(obj2);
                    obj1.flgWarning = false;
                    obj2.flgWarning = false;
                }
            } else if (!obj1.flgLandingMode || !obj2.flgLandingMode) {
                if (this.warningsound != null) {
                    this.warningsound.start();
                }
                if (obj1.PlaneType == 2) {
                    if (!obj1.flgGoforLand) {
                        obj1.setBitmap(DragMain.this.redwarning);
                    } else {
                        obj1.setBitmap(DragMain.this.redlandwarning);
                    }
                } else if (obj1.PlaneType == 1) {
                    if (!obj1.flgGoforLand) {
                        obj1.setBitmap(DragMain.this.yellowwarning);
                    } else {
                        obj1.setBitmap(DragMain.this.yellowlandwarning);
                    }
                } else if (obj1.PlaneType == 3) {
                    if (obj1.flgGoforLand) {
                        obj1.heli1 = DragMain.this.heliwd1;
                        obj1.heli2 = DragMain.this.heliwd2;
                        obj1.heli2 = DragMain.this.heliwd3;
                    } else {
                        obj1.heli1 = DragMain.this.helind1;
                        obj1.heli2 = DragMain.this.helind2;
                        obj1.heli2 = DragMain.this.helind3;
                    }
                }
                if (obj2.PlaneType == 2) {
                    if (!obj2.flgGoforLand) {
                        obj2.setBitmap(DragMain.this.redwarning);
                    } else {
                        obj2.setBitmap(DragMain.this.redlandwarning);
                    }
                } else if (obj2.PlaneType == 1) {
                    if (!obj2.flgGoforLand) {
                        obj2.setBitmap(DragMain.this.yellowwarning);
                    } else {
                        obj2.setBitmap(DragMain.this.yellowlandwarning);
                    }
                } else if (obj2.PlaneType == 3) {
                    if (obj2.flgGoforLand) {
                        obj2.heli1 = DragMain.this.heliwd1;
                        obj2.heli2 = DragMain.this.heliwd2;
                        obj2.heli2 = DragMain.this.heliwd3;
                    } else {
                        obj2.heli1 = DragMain.this.helind1;
                        obj2.heli2 = DragMain.this.helind2;
                        obj2.heli2 = DragMain.this.helind3;
                    }
                }
                obj1.flgWarning = true;
                obj2.flgWarning = true;
                obj1.warningObject = obj2;
                obj2.warningObject = obj1;
            }
        }

        /* access modifiers changed from: private */
        public void RemoveWarning(DragGraphicObject obj) {
            if (obj.PlaneType == 2) {
                if (!obj.flgGoforLand) {
                    obj.setBitmap(this.redPlane);
                } else {
                    obj.setBitmap(DragMain.this.redPlaneshadow);
                }
            } else if (obj.PlaneType == 1) {
                if (!obj.flgGoforLand) {
                    obj.setBitmap(this.yellowPlane);
                } else {
                    obj.setBitmap(DragMain.this.yellowPlaneshadow);
                }
            } else if (obj.PlaneType != 3) {
            } else {
                if (!obj.flgGoforLand) {
                    obj.heli1 = DragMain.this.helinormal1;
                    obj.heli2 = DragMain.this.helinormal2;
                    obj.heli3 = DragMain.this.helinormal3;
                } else if (obj.flgGoforLand) {
                    obj.heli1 = DragMain.this.heliland1;
                    obj.heli2 = DragMain.this.heliland2;
                    obj.heli3 = DragMain.this.heliland3;
                }
            }
        }

        public void crashCheck(DragGraphicObject obj1, DragGraphicObject obj2) {
            if ((obj1.getX() + ((float) (obj1.getBitmap().getWidth() / 2)) >= obj2.getX() && obj1.getX() + ((float) (obj1.getBitmap().getWidth() / 2)) <= obj2.getX() + ((float) (obj2.getBitmap().getWidth() / 2))) || (obj2.getX() + ((float) (obj2.getBitmap().getWidth() / 2)) >= obj1.getX() && obj2.getX() + ((float) (obj2.getBitmap().getWidth() / 2)) <= obj1.getX() + ((float) (obj1.getBitmap().getWidth() / 2)))) {
                if ((obj1.getY() + ((float) (obj1.getBitmap().getHeight() / 2)) >= obj2.getY() && obj1.getY() + ((float) (obj1.getBitmap().getHeight() / 2)) <= obj2.getY() + ((float) (obj2.getBitmap().getHeight() / 2))) || (obj2.getY() + ((float) (obj2.getBitmap().getHeight() / 2)) >= obj1.getY() && obj2.getY() + ((float) (obj2.getBitmap().getHeight() / 2)) <= obj1.getY() + ((float) (obj1.getBitmap().getHeight() / 2)))) {
                    if (this.crash != null) {
                        this.crash.start();
                    }
                    obj1.flgcrash = true;
                    obj2.flgcrash = true;
                    obj1.mPath.reset();
                    obj2.mPath.reset();
                    obj1.setBitmap(DragMain.this.boombitmap);
                    obj2.setBitmap(DragMain.this.boombitmap);
                    if (obj1.PlaneType == 3) {
                        obj1.heli1 = DragMain.this.boombitmap;
                        obj1.heli2 = DragMain.this.boombitmap;
                        obj1.heli3 = DragMain.this.boombitmap;
                        obj2.setBitmap(DragMain.this.boombitmap);
                    }
                    if (obj2.PlaneType == 3) {
                        obj2.heli1 = DragMain.this.boombitmap;
                        obj2.heli2 = DragMain.this.boombitmap;
                        obj2.heli3 = DragMain.this.boombitmap;
                        obj1.setBitmap(DragMain.this.boombitmap);
                    }
                    this._thread.crashCounter = 0;
                    DragMain.this.flgPause = true;
                    this._thread.flgIsCrashed = true;
                }
            }
        }

        public void showmsg() {
        }

        private void Anim() {
        }

        public void showDialoge() {
        }
    }

    class TutorialThread extends Thread {
        private CustomView _panel;
        private boolean _run = false;
        private SurfaceHolder _surfaceHolder;
        Handler alerthandler = null;
        private Bundle bundle;
        int crashCounter = 0;
        boolean crashExit = false;
        public boolean flgIsCrashed = false;
        public boolean isHit = false;

        public TutorialThread(SurfaceHolder surfaceHolder, CustomView panel, Handler handler) {
            this._surfaceHolder = surfaceHolder;
            this._panel = panel;
            this.alerthandler = handler;
        }

        public void run() {
            int skip_ticks = 1000 / 25;
            long next_tick = System.currentTimeMillis();
            float interpolation = 0.0f;
            while (this._run) {
                Canvas c = null;
                try {
                    c = this._surfaceHolder.lockCanvas(null);
                    int loops = 0;
                    synchronized (this._surfaceHolder) {
                        while (System.currentTimeMillis() > next_tick && loops < 5) {
                            if (DragMain.this.seconds == 30 || DragMain.this.seconds == 59) {
                                Log.e("Refresh Add", new StringBuilder().append(DragMain.this.seconds).toString());
                                DragMain.this._ad.requestFreshAd();
                                DragMain.this._ad.setRequestInterval(13);
                            }
                            if (!this.flgIsCrashed) {
                                this._panel.updatePhysics(interpolation, 25);
                            } else {
                                this.crashCounter = this.crashCounter + 1;
                                if (this.crashCounter == 50 && this._panel.life_count < 1) {
                                    this.crashExit = true;
                                } else if (this.crashCounter >= 60 && this.crashCounter <= 79 && this._panel.life_count > 0) {
                                    for (int i = 0; i < this._panel.flying_Objects.size(); i++) {
                                        if (this._panel.flying_Objects.get(i).flgcrash) {
                                            if (this._panel.flying_Objects.get(i).warningObject != null) {
                                                this._panel.RemoveWarning(this._panel.flying_Objects.get(i).warningObject);
                                                this._panel.flying_Objects.get(i).warningObject = null;
                                            }
                                            this._panel.flying_Objects.remove(i);
                                        }
                                    }
                                } else if (this.crashCounter == 80 && this._panel.life_count > 0) {
                                    this._panel.life_count--;
                                    this.alerthandler.sendEmptyMessage(0);
                                }
                            }
                            next_tick += (long) skip_ticks;
                            loops++;
                        }
                        interpolation = (float) (((System.currentTimeMillis() + ((long) skip_ticks)) - next_tick) / ((long) skip_ticks));
                        DragGraphicObject.interpolation = interpolation;
                        this._panel.onDraw(c);
                        if (this.crashExit) {
                            this.bundle = new Bundle();
                            this.bundle.putInt("score", DragMain.this.score);
                            this.bundle.putBoolean("DragActivity", true);
                            this._run = false;
                            if (this._panel.flgPlayAgain) {
                                this._panel.flgPlayAgain = false;
                                Intent playagain = new Intent(DragMain.this, PlayAgain.class);
                                playagain.putExtras(this.bundle);
                                DragMain.this.finish();
                                DragMain.this.startActivity(playagain);
                            }
                        }
                    }
                } finally {
                    if (c != null) {
                        this._surfaceHolder.unlockCanvasAndPost(c);
                    }
                }
            }
        }

        public void setRunning(boolean run) {
            this._run = run;
        }
    }
}
