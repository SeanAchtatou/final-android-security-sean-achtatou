package com.oziapp.coolLanding;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import java.lang.reflect.Array;

public class GraphicObject {
    public static final int HELI = 3;
    public static final int RED_PLANE = 2;
    public static final int YELLOW_PLANE = 1;
    public static float interpolation;
    public int PlaneType;
    public float Xf;
    public float Yf;
    public double angle = 0.0d;
    private Bitmap bitmap;
    public Paint bitmapPaint;
    public double deltaX;
    public double deltaY;
    public boolean flgGoforLand;
    public boolean flgLandingMode;
    public boolean flgNotLanding = true;
    public boolean flgOnSurface;
    public boolean flgTouchEnable = true;
    public boolean flgWarning;
    public boolean flgYrun = false;
    public boolean gamespeed = false;
    public Bitmap heli1;
    public Bitmap heli2;
    public Bitmap heli3;
    public int helicounter = 0;
    public int mX;
    public int mY;
    Matrix mat = null;
    public double planespeed;
    public int[][] points;
    public int points_index;
    public float referenceMoveX;
    public float referenceMoveY;
    private Speed speed;
    private boolean touched;
    public float upX;
    public float upY;
    public GraphicObject warningObject;
    private int x;
    private int y;

    public GraphicObject(Bitmap bitmap2, int x2, int y2, float Xv, float Yv) {
        this.bitmap = bitmap2;
        this.x = x2;
        this.y = y2;
        this.speed = new Speed(Xv, Yv);
        this.flgLandingMode = false;
        this.points = (int[][]) Array.newInstance(Integer.TYPE, 50, 2);
        this.points_index = 0;
        this.mat = new Matrix();
        this.bitmapPaint = new Paint();
        this.flgOnSurface = false;
    }

    public GraphicObject(Bitmap bitmap2, int x2, int y2, float Xv, float Yv, int parmplaneType) {
        this.PlaneType = parmplaneType;
        this.bitmap = bitmap2;
        this.x = x2;
        this.y = y2;
        this.mX = (int) (0.527d * ((double) Options.screen_width));
        this.mY = (int) (0.7707999999999999d * ((double) Options.screen_height));
        this.Xf = (float) ((int) (0.445d * ((double) Options.screen_width)));
        this.Yf = (float) ((int) (0.677d * ((double) Options.screen_height)));
        this.speed = new Speed(Xv, Yv);
        this.flgLandingMode = false;
        this.points = (int[][]) Array.newInstance(Integer.TYPE, 50, 2);
        this.points_index = 0;
        this.mat = new Matrix();
        this.bitmapPaint = new Paint();
        this.flgOnSurface = false;
    }

    public Bitmap getBitmap() {
        return this.bitmap;
    }

    public void setBitmap(Bitmap bitmap2) {
        this.bitmap = bitmap2;
    }

    public int getX() {
        return this.x;
    }

    public void setX(int x2) {
        this.x = x2;
    }

    public int getY() {
        return this.y;
    }

    public void setY(int y2) {
        this.y = y2;
    }

    public float getAngle() {
        return (float) this.angle;
    }

    public void setAngle(float angle2) {
        this.angle = (double) angle2;
    }

    public boolean isTouched() {
        return this.touched;
    }

    public void setTouched(boolean touched2) {
        this.touched = touched2;
    }

    public Speed getSpeed() {
        return this.speed;
    }

    public void setSpeed(Speed speed2) {
        this.speed = speed2;
    }

    public void stop() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public void draw(Canvas canvas) {
        this.mat = new Matrix();
        this.mat.setRotate(getAngle());
        canvas.drawBitmap(Bitmap.createBitmap(this.bitmap, 0, 0, this.bitmap.getWidth(), this.bitmap.getHeight(), this.mat, true), (float) (this.x - (this.bitmap.getWidth() / 2)), (float) (this.y - (this.bitmap.getHeight() / 2)), this.bitmapPaint);
    }

    public void update() {
        if (this.gamespeed) {
            this.speed.setXv(2.0f);
            this.speed.setYv(2.0f);
        } else {
            this.speed.setXv(1.0f);
            this.speed.setYv(1.0f);
        }
        if (SelectMap.map_Select == R.drawable.meadowolives && this.PlaneType == 2 && this.flgLandingMode) {
            this.x -= 2;
            this.y--;
            return;
        }
        if (this.flgYrun) {
            this.y = (int) (((float) this.y) + (this.speed.getYv() * ((float) this.speed.getyDirection())));
        }
        if (!this.flgYrun) {
            this.x = (int) (((float) this.x) + (this.speed.getXv() * ((float) this.speed.getxDirection())));
        }
    }

    public void handleActionDown(int eventX, int eventY) {
        if (eventX < (this.x - (this.bitmap.getWidth() / 2)) - 10 || eventX > this.x + (this.bitmap.getWidth() / 2) + 10) {
            setTouched(false);
        } else if (eventY < (this.y - (this.bitmap.getHeight() / 2)) - 10 || eventY > this.y + (this.bitmap.getHeight() / 2) + 10) {
            setTouched(false);
        } else {
            setTouched(true);
        }
    }
}
