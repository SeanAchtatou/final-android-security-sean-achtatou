package com.oziapp.coolLanding;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class Moreapps extends Activity {
    ImageButton airflight;
    ImageButton airflightpro;
    Button backButton;
    ImageButton baseball;
    ImageButton baseballpro;
    ImageButton birdhunting;
    ImageButton birdhuntingpro;
    MediaPlayer clicksound;
    ImageButton gun;
    ImageButton gunpro;
    ImageButton talkingben;
    ImageButton talkingbenpro;

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        Intent intent = new Intent(this, Main.class);
        finish();
        startActivity(intent);
        return true;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            Settings.tracker.setCustomVar(1, "Lite", "MoreApps", 2);
        } catch (RuntimeException e) {
        }
        try {
            Settings.tracker.trackPageView("/" + getLocalClassName());
        } catch (RuntimeException e2) {
        }
        setContentView((int) R.layout.moreapps);
        getWindow().setFlags(1024, 1024);
        this.backButton = (Button) findViewById(R.id.back);
        if (Options.sound_OnOff.equalsIgnoreCase("on")) {
            this.clicksound = new MediaPlayer();
            this.clicksound = MediaPlayer.create(getBaseContext(), (int) R.raw.click3);
        }
        this.birdhunting = (ImageButton) findViewById(R.id.angry);
        this.birdhunting.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Moreapps.this.clicksound != null) {
                    Moreapps.this.clicksound.start();
                }
                Intent i = new Intent("android.intent.action.VIEW");
                i.setData(Uri.parse("market://details?id=com.oziapp.BirdHuntingLite"));
                Moreapps.this.startActivity(i);
            }
        });
        this.birdhuntingpro = (ImageButton) findViewById(R.id.angrypro);
        this.birdhuntingpro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Moreapps.this.clicksound != null) {
                    Moreapps.this.clicksound.start();
                }
                Intent i = new Intent("android.intent.action.VIEW");
                i.setData(Uri.parse("market://details?id=com.oziapp.BirdHuntingPro"));
                Moreapps.this.startActivity(i);
            }
        });
        this.airflight = (ImageButton) findViewById(R.id.airflight);
        this.airflight.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Moreapps.this.clicksound != null) {
                    Moreapps.this.clicksound.start();
                }
                Intent i = new Intent("android.intent.action.VIEW");
                i.setData(Uri.parse("market://details?id=com.oziapp.coolLanding"));
                Moreapps.this.startActivity(i);
            }
        });
        this.airflightpro = (ImageButton) findViewById(R.id.airflightpro);
        this.airflightpro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Moreapps.this.clicksound != null) {
                    Moreapps.this.clicksound.start();
                }
                Intent i = new Intent("android.intent.action.VIEW");
                i.setData(Uri.parse("market://details?id=com.oziapp.FlightControlPro"));
                Moreapps.this.startActivity(i);
            }
        });
        this.baseball = (ImageButton) findViewById(R.id.baseball);
        this.baseball.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Moreapps.this.clicksound != null) {
                    Moreapps.this.clicksound.start();
                }
                Intent i = new Intent("android.intent.action.VIEW");
                i.setData(Uri.parse("market://details?id=com.Oziapp.BassBall2011"));
                Moreapps.this.startActivity(i);
            }
        });
        this.baseballpro = (ImageButton) findViewById(R.id.baseballpro);
        this.baseballpro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Moreapps.this.clicksound != null) {
                    Moreapps.this.clicksound.start();
                }
                Intent i = new Intent("android.intent.action.VIEW");
                i.setData(Uri.parse("market://details?id=com.Oziapp.BassBall2011Pro"));
                Moreapps.this.startActivity(i);
            }
        });
        this.talkingben = (ImageButton) findViewById(R.id.dogsout);
        this.talkingben.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Moreapps.this.clicksound != null) {
                    Moreapps.this.clicksound.start();
                }
                Intent i = new Intent("android.intent.action.VIEW");
                i.setData(Uri.parse("market://details?id=com.oziapp.talkingpoochlite"));
                Moreapps.this.startActivity(i);
            }
        });
        this.talkingbenpro = (ImageButton) findViewById(R.id.dogsoutpro);
        this.talkingbenpro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Moreapps.this.clicksound != null) {
                    Moreapps.this.clicksound.start();
                }
                Intent i = new Intent("android.intent.action.VIEW");
                i.setData(Uri.parse("market://details?id=com.oziapp.talkingpoochpro"));
                Moreapps.this.startActivity(i);
            }
        });
        this.gun = (ImageButton) findViewById(R.id.gunnsmoke);
        this.gun.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Moreapps.this.clicksound != null) {
                    Moreapps.this.clicksound.start();
                }
                Intent i = new Intent("android.intent.action.VIEW");
                i.setData(Uri.parse("market://details?id=com.oziapp.gunsmokelite"));
                Moreapps.this.startActivity(i);
            }
        });
        this.gunpro = (ImageButton) findViewById(R.id.gunnsmokepro);
        this.gunpro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Moreapps.this.clicksound != null) {
                    Moreapps.this.clicksound.start();
                }
                Intent i = new Intent("android.intent.action.VIEW");
                i.setData(Uri.parse("market://details?id=com.oziapp.gunsmokepro"));
                Moreapps.this.startActivity(i);
            }
        });
        this.backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Moreapps.this.backButton.setBackgroundResource(R.drawable.backon);
                if (Moreapps.this.clicksound != null) {
                    Moreapps.this.clicksound.start();
                }
                Intent intent = new Intent(Moreapps.this, Main.class);
                Moreapps.this.finish();
                Moreapps.this.startActivity(intent);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        try {
            if (this.birdhunting != null) {
                this.birdhunting.setOnClickListener(null);
                this.birdhunting.destroyDrawingCache();
                this.birdhunting = null;
            }
            if (this.birdhuntingpro != null) {
                this.birdhuntingpro.setOnClickListener(null);
                this.birdhuntingpro.destroyDrawingCache();
                this.birdhuntingpro = null;
            }
            if (this.airflight != null) {
                this.airflight.setOnClickListener(null);
                this.airflight.destroyDrawingCache();
                this.airflight = null;
            }
            if (this.airflightpro != null) {
                this.airflightpro.setOnClickListener(null);
                this.airflightpro.destroyDrawingCache();
                this.airflightpro = null;
            }
            if (this.baseball != null) {
                this.baseball.setOnClickListener(null);
                this.baseball.destroyDrawingCache();
                this.baseball = null;
            }
            if (this.baseballpro != null) {
                this.baseballpro.setOnClickListener(null);
                this.baseballpro.destroyDrawingCache();
                this.baseballpro = null;
            }
            if (this.talkingben != null) {
                this.talkingben.setOnClickListener(null);
                this.talkingben.destroyDrawingCache();
                this.talkingben = null;
            }
            if (this.talkingbenpro != null) {
                this.talkingbenpro.setOnClickListener(null);
                this.talkingbenpro.destroyDrawingCache();
                this.talkingbenpro = null;
            }
            if (this.gun != null) {
                this.gun.setOnClickListener(null);
                this.gun.destroyDrawingCache();
                this.gun = null;
            }
            if (this.gunpro != null) {
                this.gunpro.setOnClickListener(null);
                this.gunpro.destroyDrawingCache();
                this.gunpro = null;
            }
            if (this.backButton != null) {
                this.backButton.setOnClickListener(null);
                this.backButton.destroyDrawingCache();
                this.backButton = null;
            }
            if (this.clicksound != null) {
                this.clicksound.release();
                this.clicksound = null;
            }
        } catch (NullPointerException e) {
        }
        System.gc();
    }
}
