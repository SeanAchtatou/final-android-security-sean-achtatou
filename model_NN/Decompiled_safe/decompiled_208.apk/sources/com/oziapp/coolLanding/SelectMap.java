package com.oziapp.coolLanding;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.ViewSwitcher;

public class SelectMap extends Activity implements AdapterView.OnItemSelectedListener, ViewSwitcher.ViewFactory {
    public static int map_Select = R.drawable.kindamixed;
    Button Select;
    public MediaPlayer clicksound;
    private Integer[] mImageIds = {Integer.valueOf((int) R.drawable.kindamixed), Integer.valueOf((int) R.drawable.background1), Integer.valueOf((int) R.drawable.mountainjoy), Integer.valueOf((int) R.drawable.meadowolives), Integer.valueOf((int) R.drawable.maritimelands), Integer.valueOf((int) R.drawable.greenmap), Integer.valueOf((int) R.drawable.paradisepoint)};
    private Integer[] mImageIdskids = {Integer.valueOf((int) R.drawable.background1), Integer.valueOf((int) R.drawable.mountainjoy), Integer.valueOf((int) R.drawable.meadowolives), Integer.valueOf((int) R.drawable.maritimelands)};
    private ImageSwitcher mSwitcher;
    /* access modifiers changed from: private */
    public Integer[] mThumbIds = {Integer.valueOf((int) R.drawable.kindamixed), Integer.valueOf((int) R.drawable.background1), Integer.valueOf((int) R.drawable.mountainjoy), Integer.valueOf((int) R.drawable.meadowolives), Integer.valueOf((int) R.drawable.maritimelands), Integer.valueOf((int) R.drawable.greenmap), Integer.valueOf((int) R.drawable.paradisepoint)};
    /* access modifiers changed from: private */
    public Integer[] mThumbIdskids = {Integer.valueOf((int) R.drawable.background1), Integer.valueOf((int) R.drawable.mountainjoy), Integer.valueOf((int) R.drawable.meadowolives), Integer.valueOf((int) R.drawable.maritimelands)};
    TextView tv1;
    TextView tvmapname;

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        Intent start = new Intent(this, Main.class);
        finish();
        startActivity(start);
        return true;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            Settings.tracker.setCustomVar(1, "Lite", "Select Map", 2);
            Settings.tracker.trackPageView("/" + getLocalClassName());
        } catch (Exception e) {
        }
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        if (Options.sound_OnOff.equalsIgnoreCase("on")) {
            this.clicksound = new MediaPlayer();
            this.clicksound = MediaPlayer.create(getBaseContext(), (int) R.raw.click3);
        }
        setContentView((int) R.layout.selectmap);
        this.Select = (Button) findViewById(R.id.select);
        this.tvmapname = (TextView) findViewById(R.id.tvmapname);
        this.tv1 = (TextView) findViewById(R.id.textViewProtxt);
        this.tv1.setVisibility(4);
        this.tvmapname.setText("Arid Terrain");
        this.mSwitcher = (ImageSwitcher) findViewById(R.id.switcher);
        this.mSwitcher.setFactory(this);
        this.mSwitcher.setInAnimation(AnimationUtils.loadAnimation(this, 17432576));
        this.mSwitcher.setOutAnimation(AnimationUtils.loadAnimation(this, 17432577));
        Gallery g = (Gallery) findViewById(R.id.gallery);
        g.setAdapter((SpinnerAdapter) new ImageAdapter(this));
        g.setOnItemSelectedListener(this);
        this.Select.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (SelectMap.this.clicksound != null) {
                    SelectMap.this.clicksound.start();
                }
                SelectMap.this.Select.setBackgroundResource(R.drawable.selecton);
                Intent start = new Intent(SelectMap.this, Main.class);
                SelectMap.this.finish();
                SelectMap.this.startActivity(start);
            }
        });
    }

    public void onItemSelected(AdapterView<?> adapterView, View v, int position, long id) {
        if (Options.playingModes.equalsIgnoreCase("kids")) {
            this.mSwitcher.setImageResource(this.mImageIdskids[position].intValue());
            if (position == 0) {
                map_Select = R.drawable.background1;
                this.tvmapname.setText("Arid Terrain");
                this.tv1.setVisibility(4);
                this.Select.setEnabled(true);
            } else if (position == 1) {
                this.tvmapname.setText("Mountain Joy");
                this.tv1.setVisibility(0);
                this.Select.setEnabled(false);
            } else if (position == 2) {
                this.tvmapname.setText("Meadow Olives");
                this.tv1.setVisibility(0);
                this.Select.setEnabled(false);
            } else if (position == 3) {
                this.tvmapname.setText("Maritime Lands");
                this.tv1.setVisibility(0);
                this.Select.setEnabled(false);
            }
        } else if (Options.playingModes.equalsIgnoreCase("classic")) {
            this.mSwitcher.setImageResource(this.mImageIds[position].intValue());
            if (position == 0) {
                map_Select = R.drawable.kindamixed;
                this.tvmapname.setText("Green Truf");
                this.tv1.setVisibility(4);
                this.Select.setEnabled(true);
            } else if (position == 1) {
                map_Select = R.drawable.background1;
                this.tvmapname.setText("Arid Terrain");
                this.tv1.setVisibility(4);
                this.Select.setEnabled(true);
            } else if (position == 2) {
                this.tvmapname.setText("Mountain Joy");
                this.tv1.setVisibility(0);
                this.Select.setEnabled(false);
            } else if (position == 3) {
                this.tvmapname.setText("Meadow Olives");
                this.tv1.setVisibility(0);
                this.Select.setEnabled(false);
            } else if (position == 4) {
                this.tvmapname.setText("Maritime Lands");
                this.tv1.setVisibility(0);
                this.Select.setEnabled(false);
            } else if (position == 5) {
                this.tvmapname.setText("Spring Fields");
                this.tv1.setVisibility(0);
                this.Select.setEnabled(false);
            } else if (position == 6) {
                this.tvmapname.setText("Paradise Point");
                this.tv1.setVisibility(0);
                this.Select.setEnabled(false);
            }
        }
    }

    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    public View makeView() {
        return new ImageView(this);
    }

    public class ImageAdapter extends BaseAdapter {
        private Context mContext;

        public ImageAdapter(Context c) {
            this.mContext = c;
        }

        public int getCount() {
            if (Options.playingModes.equalsIgnoreCase("kids")) {
                return SelectMap.this.mThumbIdskids.length;
            }
            return SelectMap.this.mThumbIds.length;
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView i = new ImageView(this.mContext);
            if (Options.playingModes.equalsIgnoreCase("kids")) {
                i.setImageResource(SelectMap.this.mThumbIdskids[position].intValue());
            } else {
                i.setImageResource(SelectMap.this.mThumbIds[position].intValue());
            }
            i.setAdjustViewBounds(true);
            return i;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.clicksound = null;
        this.Select = null;
        this.tvmapname = null;
        this.tv1 = null;
        this.mThumbIds = null;
        this.mImageIds = null;
        this.mThumbIdskids = null;
        this.mImageIdskids = null;
        System.gc();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }
}
