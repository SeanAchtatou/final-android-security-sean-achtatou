package com.oziapp.coolLanding;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.openfeint.api.OpenFeint;
import com.openfeint.api.OpenFeintDelegate;
import com.openfeint.api.OpenFeintSettings;
import com.openfeint.api.ui.Dashboard;

public class PlayAgain extends Activity {
    public static final int KEY_PRIVATE = 0;
    public static final int KEY_PRIVATE1 = 0;
    public static final int KEY_PRIVATE2 = 0;
    public static final int KEY_PRIVATE3 = 0;
    public static final int KEY_PRIVATE4 = 0;
    public static final String PREFS_PRIVATE = "PREFS_PRIVATE";
    public static final int TOTAL_LANDED = 0;
    public static final int TOTAL_LANDED1 = 0;
    public static final int TOTAL_LANDED2 = 0;
    public static final int TOTAL_LANDED3 = 0;
    public static final int TOTAL_LANDED4 = 0;
    static final String gameID = "288312";
    static final String gameKey = "8XM5s18bmHyZ1ZcTlcSBg";
    static final String gameName = "Air Flight Control Free";
    static final String gameSecret = "vPIsgcZtMlbYDsAUaMDWP977AfSjJo3bYZw4uLFbEk";
    private Button HighScore;
    /* access modifiers changed from: private */
    public Button Yes;
    public int _score;
    public int _score1;
    public int _score2;
    public int _score3;
    public int _score4;
    /* access modifiers changed from: private */
    public AlertDialog buypro;
    public MediaPlayer clicksound;
    private SharedPreferences prefsPrivate;
    public int pscore;
    public int pscore1;
    public int pscore2;
    public int pscore3;
    public int pscore4;
    public int score;
    private Button submitScore;
    private TextView topscore;
    public int total;
    public int total1;
    public int total2;
    public int total3;
    public int total4;
    private TextView totallanded;
    public int tscore;
    public int tscore1;
    public int tscore2;
    public int tscore3;
    public int tscore4;
    private TextView tvlevel;
    private TextView tvnxt;
    private TextView txvscore;

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            OpenFeint.initialize(getApplicationContext(), new OpenFeintSettings(gameName, gameKey, gameSecret, gameID), new OpenFeintDelegate() {
            });
        } catch (Exception e) {
        }
        try {
            Settings.tracker.setCustomVar(1, "Lite", "PlayagainPage", 2);
        } catch (RuntimeException e2) {
        }
        try {
            Settings.tracker.trackPageView("/" + getLocalClassName());
        } catch (RuntimeException e3) {
        }
        AlertDialog.Builder buypro_bld = new AlertDialog.Builder(this);
        buypro_bld.setMessage("Buy Full Version to Have More Fun ").setCancelable(false).setPositiveButton("Buy Full Version", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent i = new Intent("android.intent.action.VIEW");
                i.setData(Uri.parse("market://details?id=com.oziapp.FlightControlPro"));
                PlayAgain.this.startActivity(i);
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                PlayAgain.this.buypro.dismiss();
            }
        });
        this.buypro = buypro_bld.create();
        this.buypro.setTitle("Buy Full Version");
        this.buypro.setIcon((int) R.drawable.airflightpro);
        if (Options.sound_OnOff.equalsIgnoreCase("on")) {
            this.clicksound = new MediaPlayer();
            this.clicksound = MediaPlayer.create(getBaseContext(), (int) R.raw.click3);
        }
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        this.score = getIntent().getExtras().getInt("score");
        setContentView((int) R.layout.playagain);
        this.txvscore = (TextView) findViewById(R.id.score);
        this.tvlevel = (TextView) findViewById(R.id.tvlevel);
        this.tvnxt = (TextView) findViewById(R.id.tvnxt);
        this.topscore = (TextView) findViewById(R.id.topscore);
        this.totallanded = (TextView) findViewById(R.id.totallanded);
        this.Yes = (Button) findViewById(R.id.yes);
        this.HighScore = (Button) findViewById(R.id.highscore);
        this.submitScore = (Button) findViewById(R.id.submitscore);
        this.txvscore.setText(new StringBuilder().append(this.score).toString());
        if (SelectMap.map_Select == R.drawable.background1) {
            this.prefsPrivate = getSharedPreferences("PREFS_PRIVATE", 0);
            this.total = this.prefsPrivate.getInt("TOTAL_LANDED", this.tscore);
            this.tscore = this.total + this.score;
            this.prefsPrivate = getSharedPreferences("PREFS_PRIVATE", 0);
            SharedPreferences.Editor prefsPrivateEditor1 = this.prefsPrivate.edit();
            prefsPrivateEditor1.putInt("TOTAL_LANDED", this.tscore);
            prefsPrivateEditor1.commit();
            this.prefsPrivate = getSharedPreferences("PREFS_PRIVATE", 0);
            this._score = this.prefsPrivate.getInt("KEY_PRIVATE", this.pscore);
            if (this.score > this._score) {
                this.prefsPrivate = getSharedPreferences("PREFS_PRIVATE", 0);
                SharedPreferences.Editor prefsPrivateEditor = this.prefsPrivate.edit();
                prefsPrivateEditor.putInt("KEY_PRIVATE", this.score);
                prefsPrivateEditor.commit();
            }
            this.prefsPrivate = getSharedPreferences("PREFS_PRIVATE", 0);
            this.topscore.setText("Top Score on Arid Terrain:     " + this.prefsPrivate.getInt("KEY_PRIVATE", this.pscore));
            this.totallanded.setText("Total landed on Arid Terrain:     " + this.prefsPrivate.getInt("TOTAL_LANDED", this.tscore));
        } else if (SelectMap.map_Select == R.drawable.mountainjoy) {
            this.prefsPrivate = getSharedPreferences("PREFS_PRIVATE", 0);
            this.total1 = this.prefsPrivate.getInt("TOTAL_LANDED1", this.tscore1);
            this.tscore1 = this.total1 + this.score;
            this.prefsPrivate = getSharedPreferences("PREFS_PRIVATE", 0);
            SharedPreferences.Editor prefsPrivateEditor12 = this.prefsPrivate.edit();
            prefsPrivateEditor12.putInt("TOTAL_LANDED1", this.tscore1);
            prefsPrivateEditor12.commit();
            this.prefsPrivate = getSharedPreferences("PREFS_PRIVATE", 0);
            this._score1 = this.prefsPrivate.getInt("KEY_PRIVATE1", this.pscore1);
            if (this.score > this._score1) {
                this.prefsPrivate = getSharedPreferences("PREFS_PRIVATE", 0);
                SharedPreferences.Editor prefsPrivateEditor2 = this.prefsPrivate.edit();
                prefsPrivateEditor2.putInt("KEY_PRIVATE1", this.score);
                prefsPrivateEditor2.commit();
            }
            this.prefsPrivate = getSharedPreferences("PREFS_PRIVATE", 0);
            this.topscore.setText("Top Score on Arid Terrain:     " + this.prefsPrivate.getInt("KEY_PRIVATE1", this.pscore1));
            this.totallanded.setText("Total landed on Arid Terrain:     " + this.prefsPrivate.getInt("TOTAL_LANDED1", this.tscore1));
        } else if (SelectMap.map_Select == R.drawable.meadowolives) {
            this.prefsPrivate = getSharedPreferences("PREFS_PRIVATE", 0);
            this.total2 = this.prefsPrivate.getInt("TOTAL_LANDED2", this.tscore2);
            this.tscore2 = this.total2 + this.score;
            this.prefsPrivate = getSharedPreferences("PREFS_PRIVATE", 0);
            SharedPreferences.Editor prefsPrivateEditor13 = this.prefsPrivate.edit();
            prefsPrivateEditor13.putInt("TOTAL_LANDED2", this.tscore2);
            prefsPrivateEditor13.commit();
            this.prefsPrivate = getSharedPreferences("PREFS_PRIVATE", 0);
            this._score2 = this.prefsPrivate.getInt("KEY_PRIVATE2", this.pscore2);
            if (this.score > this._score2) {
                this.prefsPrivate = getSharedPreferences("PREFS_PRIVATE", 0);
                SharedPreferences.Editor prefsPrivateEditor3 = this.prefsPrivate.edit();
                prefsPrivateEditor3.putInt("KEY_PRIVATE2", this.score);
                prefsPrivateEditor3.commit();
            }
            this.prefsPrivate = getSharedPreferences("PREFS_PRIVATE", 0);
            this.topscore.setText("Top Score on Meadow Olives:     " + this.prefsPrivate.getInt("KEY_PRIVATE2", this.pscore2));
            this.totallanded.setText("Total landed on Meadow Olives:     " + this.prefsPrivate.getInt("TOTAL_LANDED2", this.tscore2));
        } else if (SelectMap.map_Select == R.drawable.maritimelands) {
            this.prefsPrivate = getSharedPreferences("PREFS_PRIVATE", 0);
            this.total3 = this.prefsPrivate.getInt("TOTAL_LANDED2", this.tscore3);
            this.tscore3 = this.total3 + this.score;
            this.prefsPrivate = getSharedPreferences("PREFS_PRIVATE", 0);
            SharedPreferences.Editor prefsPrivateEditor14 = this.prefsPrivate.edit();
            prefsPrivateEditor14.putInt("TOTAL_LANDED3", this.tscore3);
            prefsPrivateEditor14.commit();
            this.prefsPrivate = getSharedPreferences("PREFS_PRIVATE", 0);
            this._score3 = this.prefsPrivate.getInt("KEY_PRIVATE3", this.pscore3);
            if (this.score > this._score3) {
                this.prefsPrivate = getSharedPreferences("PREFS_PRIVATE", 0);
                SharedPreferences.Editor prefsPrivateEditor4 = this.prefsPrivate.edit();
                prefsPrivateEditor4.putInt("KEY_PRIVATE3", this.score);
                prefsPrivateEditor4.commit();
            }
            this.prefsPrivate = getSharedPreferences("PREFS_PRIVATE", 0);
            this.topscore.setText("Top Score on Maritime Lands:     " + this.prefsPrivate.getInt("KEY_PRIVATE3", this.pscore3));
            this.totallanded.setText("Total landed on Maritime Lands:     " + this.prefsPrivate.getInt("TOTAL_LANDED3", this.tscore3));
        } else if (SelectMap.map_Select == R.drawable.kindamixed) {
            this.prefsPrivate = getSharedPreferences("PREFS_PRIVATE", 0);
            this.total4 = this.prefsPrivate.getInt("TOTAL_LANDED4", this.tscore4);
            this.tscore4 = this.total4 + this.score;
            this.prefsPrivate = getSharedPreferences("PREFS_PRIVATE", 0);
            SharedPreferences.Editor prefsPrivateEditor15 = this.prefsPrivate.edit();
            prefsPrivateEditor15.putInt("TOTAL_LANDED4", this.tscore4);
            prefsPrivateEditor15.commit();
            this.prefsPrivate = getSharedPreferences("PREFS_PRIVATE", 0);
            this._score4 = this.prefsPrivate.getInt("KEY_PRIVATE4", this.pscore4);
            if (this.score > this._score4) {
                this.prefsPrivate = getSharedPreferences("PREFS_PRIVATE", 0);
                SharedPreferences.Editor prefsPrivateEditor5 = this.prefsPrivate.edit();
                prefsPrivateEditor5.putInt("KEY_PRIVATE4", this.score);
                prefsPrivateEditor5.commit();
            }
            this.prefsPrivate = getSharedPreferences("PREFS_PRIVATE", 0);
            this.topscore.setText("Top Score on Green Truf:     " + this.prefsPrivate.getInt("KEY_PRIVATE4", this.pscore4));
            this.totallanded.setText("Total landed on Green Truf:     " + this.prefsPrivate.getInt("TOTAL_LANDED4", this.tscore4));
        }
        if (this.score < 10) {
            this.tvlevel.setText("Amateur");
            this.tvnxt.setText("Land 10 aircrafts to reach next rank: Beginner");
        } else if (this.score < 20) {
            this.tvlevel.setText("Beginner");
            this.tvnxt.setText("Land 20 aircrafts to reach next rank: Airman");
        } else if (this.score < 30) {
            this.tvlevel.setText("Airman");
            this.tvnxt.setText("Land 30 aircrafts to reach next rank: Captain");
        } else if (this.score < 40) {
            this.tvlevel.setText("Captain");
            this.tvnxt.setText("Land 40 aircrafts to reach next rank: Co-Pilot");
        } else if (this.score < 56) {
            this.tvlevel.setText("Co-Pilot");
            this.tvnxt.setText("Land 56 aircrafts to reach next rank: Pilot");
        } else if (this.score < 70) {
            this.tvlevel.setText("Pilot");
            this.tvnxt.setText("Land 70 aircrafts to reach next rank: Master Pilot");
        } else if (this.score < 90) {
            this.tvlevel.setText("Master Pilot");
            this.tvnxt.setText("Land 90 aircrafts to reach next rank: Senior Captain");
        } else if (this.score < 100) {
            this.tvlevel.setText("Senior Captain");
            this.tvnxt.setText("Land 100 aircrafts to reach next rank: Chief Pilot");
        } else {
            this.tvlevel.setText("Chief Pilot");
            this.tvnxt.setText("Congratulations! You are now Chief Pilot.");
        }
        this.buypro.show();
        this.Yes.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    Settings.tracker.trackEvent("Playagain", "Clicked", "Playe Again Button", 1);
                } catch (Exception e) {
                }
                if (PlayAgain.this.clicksound != null) {
                    PlayAgain.this.clicksound.start();
                }
                PlayAgain.this.Yes.setBackgroundResource(R.drawable.playagainon);
                if (Options.playingModes == "classic") {
                    Intent intent = new Intent(PlayAgain.this, DragMain.class);
                    PlayAgain.this.finish();
                    PlayAgain.this.startActivity(intent);
                    return;
                }
                Intent intent2 = new Intent(PlayAgain.this, PlaneActivity.class);
                PlayAgain.this.finish();
                PlayAgain.this.startActivity(intent2);
            }
        });
        this.HighScore.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    Settings.tracker.trackEvent("Playagain", "Clicked", "High Score Button", 1);
                } catch (Exception e) {
                }
                if (PlayAgain.this.clicksound != null) {
                    PlayAgain.this.clicksound.start();
                }
                try {
                    Dashboard.openLeaderboard("741137");
                } catch (Exception e2) {
                }
            }
        });
        this.submitScore.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    Settings.tracker.trackEvent("Playagain", "Clicked", "Submit High Score", 1);
                } catch (Exception e) {
                }
                if (PlayAgain.this.clicksound != null) {
                    PlayAgain.this.clicksound.start();
                }
                Intent intent = new Intent(PlayAgain.this, SubmitScore.class);
                new Bundle().putInt("score", PlayAgain.this.score);
                intent.putExtra("score", PlayAgain.this.score);
                PlayAgain.this.startActivity(intent);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        try {
            if (this.Yes != null) {
                this.Yes.setOnClickListener(null);
                this.Yes.destroyDrawingCache();
                this.Yes = null;
            }
            if (this.HighScore != null) {
                this.HighScore.setOnClickListener(null);
                this.HighScore.destroyDrawingCache();
                this.HighScore = null;
            }
            if (this.submitScore != null) {
                this.submitScore.setOnClickListener(null);
                this.submitScore.destroyDrawingCache();
                this.submitScore = null;
            }
        } catch (NullPointerException e) {
        }
        System.gc();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }
}
