package com.oziapp.coolLanding;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Process;
import android.view.Display;
import android.view.KeyEvent;
import android.widget.MediaController;
import android.widget.VideoView;
import java.util.Timer;
import java.util.TimerTask;

public class Animation extends Activity {
    VideoView videoView;

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        if (this.videoView != null) {
            this.videoView.stopPlayback();
        }
        finish();
        Process.killProcess(Process.myPid());
        return true;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.anim);
        Display display = getWindowManager().getDefaultDisplay();
        int width = display.getWidth();
        Options.screen_height = display.getHeight();
        Options.screen_width = width;
        this.videoView = (VideoView) findViewById(R.id.VideoView);
        new MediaController(this).setAnchorView(this.videoView);
        this.videoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + ((int) R.raw.ozi)));
        this.videoView.start();
        new Timer(false).schedule(new MyAnimationRoutine2(), 8500);
    }

    public void FinishTimer() {
    }

    class MyAnimationRoutine2 extends TimerTask {
        MyAnimationRoutine2() {
        }

        public void run() {
            Intent intent = new Intent(Animation.this, Main.class);
            Animation.this.finish();
            Animation.this.startActivity(intent);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        try {
            if (this.videoView != null) {
                this.videoView = null;
            }
        } catch (Exception e) {
        }
        System.gc();
    }
}
