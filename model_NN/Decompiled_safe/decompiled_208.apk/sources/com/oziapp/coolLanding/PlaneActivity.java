package com.oziapp.coolLanding;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.lang.reflect.Array;
import java.util.Random;

public class PlaneActivity extends Activity {
    public static final String PREFS_PRIVATE = "PREFS_PRIVATE";
    boolean GameSpeed = true;
    AlertDialog alert;
    AlertDialog alert2;
    public boolean flgPause = false;
    /* access modifiers changed from: private */
    public SharedPreferences prefsPrivate;
    public int screen_height = Options.screen_height;
    public int screen_width = Options.screen_width;

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        try {
            Settings.tracker.setCustomVar(1, "Lite", "Kids Mode", 2);
            Settings.tracker.trackPageView("/" + getLocalClassName());
        } catch (Exception e) {
        }
        setContentView(new MyView(this));
        AlertDialog.Builder alt_bld2 = new AlertDialog.Builder(this);
        alt_bld2.setMessage("Current Game will be Lost").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                PlaneActivity.this.finish();
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                PlaneActivity.this.alert.show();
            }
        });
        AlertDialog.Builder alt_bld = new AlertDialog.Builder(this);
        alt_bld.setMessage("").setCancelable(false).setPositiveButton("Resume", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                PlaneActivity.this.flgPause = false;
            }
        }).setNegativeButton("Main Menu", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                PlaneActivity.this.alert2.show();
            }
        });
        this.alert = alt_bld.create();
        this.alert.setTitle("Game Paused");
        this.alert.setIcon((int) R.drawable.icon);
        this.alert2 = alt_bld2.create();
        this.alert2.setTitle("Are You Sure?");
        this.alert2.setIcon((int) R.drawable.icon);
    }

    public class MyView extends SurfaceView implements SurfaceHolder.Callback {
        private static final float TOUCH_TOLERANCE = 4.0f;
        private int Arrival2x;
        private int Arrival2y;
        private int ArrivalheliX;
        private int Arrivalx;
        private int Arrivaly;
        public int[] HelicopterX;
        public int[] HelicopterY;
        public int[] RandomPlane1X;
        public int[] RandomPlane1Y;
        public int[] RandomPlane2X;
        public int[] RandomPlane2Y;
        private float Xv;
        private float Yv;
        private TutorialThread _thread;
        private Bitmap arrivallarge;
        private Bitmap arrivalsmall;
        MediaPlayer backgroundSound;
        public Bitmap bitmap;
        private Bitmap boxbitmap;
        Bundle bundle;
        private Paint circlePaint;
        private Bitmap clouds;
        public MediaPlayer crash;
        boolean flgClouds;
        public boolean flgCrashControl;
        boolean flgLine;
        boolean flgModes;
        private boolean flgNotLanding;
        private boolean flgNotLanding2;
        private boolean flgNotLanding3;
        public boolean flgPlayAgain;
        private boolean flgRandomAllow;
        boolean flgfps;
        private boolean flghelipad;
        private boolean flgtoggle;
        private boolean flgtouched;
        private boolean flgtouched2;
        private int fps;
        private Paint fpsPaint;
        private Bitmap frwd;
        private int h;
        private GraphicObject helicopter;
        Bitmap heliland1;
        Bitmap heliland2;
        Bitmap heliland3;
        Bitmap helind1;
        Bitmap helind2;
        Bitmap helind3;
        Bitmap helinormal1;
        Bitmap helinormal2;
        Bitmap helinormal3;
        public MediaPlayer helipad;
        public int helipadX1;
        public int helipadX2;
        public int helipadY1;
        public int helipadY2;
        Bitmap heliwd1;
        Bitmap heliwd2;
        Bitmap heliwd3;
        private int highscore;
        private int i;
        private int j;
        private int k;
        public int landingZone1X1;
        public int landingZone1X2;
        public int landingZone1Y1;
        public int landingZone1Y2;
        public int landingZone2X1;
        public int landingZone2X2;
        public int landingZone2Y1;
        public int landingZone2Y2;
        public Paint linePaint;
        private Paint mBitmapPaint;
        private Path mPath;
        private Path mPath1;
        private Path mPath2;
        private Path mPath3;
        private float mX;
        private float mY;
        public Paint mdrawLinePaint;
        public int new1X;
        public int new1Y;
        public int new2X;
        public int new2Y;
        public int newheliX;
        private float nothelipadX1;
        private float nothelipadY1;
        private float notlandingZone1X1;
        private float notlandingZone1Y1;
        private float notlandingZone2X1;
        private float notlandingZone2Y1;
        private int p;
        private int p2;
        private Bitmap pause;
        private int plane1;
        private Bitmap plane1right;
        private Bitmap plane1rightlanding;
        private Bitmap plane1rightshadow;
        private int plane2;
        private Bitmap plane2down;
        private Bitmap plane2right;
        private Bitmap plane2rightlanding;
        private Bitmap plane2rightshadow;
        private Bitmap plane2up;
        private int plane_currentX;
        private int plane_currentX2;
        private int plane_currentX3;
        private int plane_currentY;
        private int plane_currentY2;
        private int plane_currentY3;
        private Bitmap play;
        private Bitmap ply;
        private int pscore1;
        private Random rand;
        private int rand_heli;
        private Paint rectPaint;
        public int redDirection;
        private GraphicObject redPlane;
        private Bitmap redlandwarning;
        private Bitmap redwarning;
        public MediaPlayer runway1;
        public MediaPlayer runway2;
        private int score;
        private float sleep;
        Speed speed;
        private final Paint tPaint = new Paint();
        public int tempZone1X1;
        public int tempZone1X2;
        public int tempZone1Y1;
        public int tempZone1Y2;
        public int tempZone2X1;
        public int tempZone2X2;
        public int tempZone2Y1;
        public int tempZone2Y2;
        private int tempheliX1;
        private int tempheliY1;
        public int temphelipadX1;
        public int temphelipadX2;
        public int temphelipadY1;
        public int temphelipadY2;
        private int templandingZone1X1;
        private int templandingZone1Y1;
        private int templandingZone2X1;
        private int templandingZone2X2;
        private int templandingZone2Y1;
        private int templandingZone2Y2;
        private float upX;
        private float upY;
        public float x;
        public float y;
        public int yellowDirection;
        private GraphicObject yellowPlane;
        private Bitmap yellowlandwarning;
        private Bitmap yellowwarning;

        public MyView(Context c) {
            super(c);
            this.landingZone1X1 = (int) (0.2701d * ((double) PlaneActivity.this.screen_width));
            this.landingZone1X2 = (int) (0.5964d * ((double) PlaneActivity.this.screen_width));
            this.landingZone1Y1 = (int) (0.32299999999999995d * ((double) PlaneActivity.this.screen_height));
            this.landingZone1Y2 = (int) (0.423d * ((double) PlaneActivity.this.screen_height));
            this.landingZone2X1 = (int) (0.2701d * ((double) PlaneActivity.this.screen_width));
            this.landingZone2X2 = (int) (0.508d * ((double) PlaneActivity.this.screen_width));
            this.landingZone2Y1 = (int) (0.5625d * ((double) PlaneActivity.this.screen_height));
            this.landingZone2Y2 = (int) (0.6437d * ((double) PlaneActivity.this.screen_height));
            this.helipadX1 = (int) (0.5473d * ((double) PlaneActivity.this.screen_width));
            this.helipadX2 = (int) (0.6315d * ((double) PlaneActivity.this.screen_width));
            this.helipadY1 = (int) (0.7025d * ((double) PlaneActivity.this.screen_height));
            this.helipadY2 = (int) (0.825d * ((double) PlaneActivity.this.screen_height));
            this.tempZone1X1 = (int) (0.23420000000000002d * ((double) PlaneActivity.this.screen_width));
            this.tempZone1Y1 = (int) (0.3583d * ((double) PlaneActivity.this.screen_height));
            this.tempZone1X2 = (int) (0.2787d * ((double) PlaneActivity.this.screen_width));
            this.tempZone1Y2 = (int) (0.4167d * ((double) PlaneActivity.this.screen_height));
            this.tempZone2X1 = (int) (0.23420000000000002d * ((double) PlaneActivity.this.screen_width));
            this.tempZone2Y1 = (int) (0.5832999999999999d * ((double) PlaneActivity.this.screen_height));
            this.tempZone2X2 = (int) (0.2857d * ((double) PlaneActivity.this.screen_width));
            this.tempZone2Y2 = (int) (0.6417d * ((double) PlaneActivity.this.screen_height));
            this.yellowDirection = 0;
            this.redDirection = 0;
            this.yellowPlane = null;
            this.redPlane = null;
            this.helicopter = null;
            this.flgtouched = false;
            this.flgtouched2 = false;
            this.flghelipad = false;
            this.flgCrashControl = false;
            this.RandomPlane1Y = new int[20];
            this.RandomPlane1X = new int[20];
            this.RandomPlane2Y = new int[20];
            this.RandomPlane2X = new int[20];
            this.HelicopterX = new int[20];
            this.HelicopterY = new int[20];
            this.Arrivaly = 30;
            this.Arrivalx = 570;
            this.Arrival2y = 140;
            this.Arrival2x = 10;
            this.ArrivalheliX = 160;
            this.new1Y = 0;
            this.new1X = 0;
            this.new2Y = 0;
            this.new2X = 0;
            this.newheliX = 0;
            this.plane1 = 2;
            this.plane2 = 2;
            this.h = 2;
            this.flgClouds = false;
            this.flgLine = false;
            this.flgModes = false;
            this.flgfps = false;
            this.flgNotLanding = false;
            this.flgNotLanding2 = false;
            this.i = 0;
            this.j = 0;
            this.k = 0;
            this.flgRandomAllow = true;
            this.flgPlayAgain = true;
            this.flgtoggle = false;
            getHolder().addCallback(this);
            PlaneActivity.this.prefsPrivate = PlaneActivity.this.getSharedPreferences("PREFS_PRIVATE", 0);
            this.highscore = PlaneActivity.this.prefsPrivate.getInt("KEY_PRIVATE", this.pscore1);
            if (PlaneActivity.this.screen_height % 2 != 0) {
                PlaneActivity.this.screen_height++;
            }
            if (PlaneActivity.this.screen_width % 2 != 0) {
                PlaneActivity.this.screen_width++;
            }
            if (this.landingZone1X1 % 2 != 0) {
                this.landingZone1X1++;
            }
            if (this.landingZone1X2 % 2 != 0) {
                this.landingZone1X2++;
            }
            if (this.landingZone1Y1 % 2 != 0) {
                this.landingZone1Y1++;
            }
            if (this.landingZone1Y2 % 2 != 0) {
                this.landingZone1Y2++;
            }
            if (this.landingZone2X1 % 2 != 0) {
                this.landingZone2X1++;
            }
            if (this.landingZone2X2 % 2 != 0) {
                this.landingZone2X2++;
            }
            if (this.landingZone2Y1 % 2 != 0) {
                this.landingZone2Y1++;
            }
            if (this.landingZone2Y2 % 2 != 0) {
                this.landingZone2Y2++;
            }
            if (this.helipadX1 % 2 != 0) {
                this.helipadX1++;
            }
            if (this.helipadX2 % 2 != 0) {
                this.helipadX2++;
            }
            if (this.helipadY1 % 2 != 0) {
                this.helipadY1++;
            }
            if (this.helipadY2 % 2 != 0) {
                this.helipadY2++;
            }
            int incrementY = PlaneActivity.this.screen_height / 20;
            int incrementX = PlaneActivity.this.screen_width / 20;
            this.RandomPlane1X[0] = incrementX;
            this.RandomPlane1Y[0] = incrementY;
            this.RandomPlane2X[0] = incrementX;
            this.RandomPlane2Y[0] = incrementY;
            this.HelicopterX[0] = incrementX;
            this.HelicopterY[0] = incrementY;
            for (int i2 = 1; i2 < 20; i2++) {
                if (i2 == 19 || i2 == 18) {
                    this.RandomPlane1X[i2] = incrementX;
                    this.RandomPlane1Y[i2] = incrementY;
                    this.RandomPlane2X[i2] = incrementX;
                    this.RandomPlane2Y[i2] = incrementY;
                    this.HelicopterX[i2] = incrementX;
                    this.HelicopterY[i2] = incrementY;
                } else {
                    this.RandomPlane1X[i2] = this.RandomPlane1X[i2 - 1] + incrementX;
                    this.RandomPlane1Y[i2] = this.RandomPlane1Y[i2 - 1] + incrementY;
                    this.RandomPlane2X[i2] = this.RandomPlane2X[i2 - 1] + incrementX;
                    this.RandomPlane2Y[i2] = this.RandomPlane2Y[i2 - 1] + incrementY;
                    this.HelicopterX[i2] = this.HelicopterX[i2 - 1] + incrementX;
                    this.HelicopterY[i2] = this.HelicopterY[i2 - 1] + incrementY;
                }
            }
            this.speed = new Speed();
            this.linePaint = new Paint();
            this.circlePaint = new Paint();
            this.fpsPaint = new Paint();
            this.mBitmapPaint = new Paint();
            this.rectPaint = new Paint();
            this.bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.background1);
            this.tPaint.setColor(-1);
            this.fpsPaint.setColor(-1);
            if (Options.difficulty_Level.equalsIgnoreCase("easy")) {
                this.Xv = 1.0f;
                this.Yv = 1.0f;
            }
            if (Options.difficulty_Level.equalsIgnoreCase("normal")) {
                this.Xv = 1.0f;
                this.Yv = 1.0f;
            }
            if (Options.difficulty_Level.equalsIgnoreCase("hard")) {
                this.Xv = 2.0f;
                this.Yv = 2.0f;
            }
            if (Options.sound_OnOff.equalsIgnoreCase("on")) {
                this.crash = new MediaPlayer();
                this.runway1 = new MediaPlayer();
                this.runway2 = new MediaPlayer();
                this.helipad = new MediaPlayer();
                this.crash = MediaPlayer.create(PlaneActivity.this.getBaseContext(), (int) R.raw.crash);
                this.runway1 = MediaPlayer.create(PlaneActivity.this.getBaseContext(), (int) R.raw.runway1);
                this.runway2 = MediaPlayer.create(PlaneActivity.this.getBaseContext(), (int) R.raw.runway1);
                this.helipad = MediaPlayer.create(PlaneActivity.this.getBaseContext(), (int) R.raw.runway1);
            }
            Options.sound_OnOff.equalsIgnoreCase("off");
            if (Options.bgsound_OnOff.equalsIgnoreCase("on")) {
                this.backgroundSound = MediaPlayer.create(PlaneActivity.this.getBaseContext(), (int) R.raw.music);
                this.backgroundSound.setLooping(true);
                this.backgroundSound.start();
            }
            Options.bgsound_OnOff.equalsIgnoreCase("off");
            if (Options.clouds_OnOff.equalsIgnoreCase("on")) {
                this.flgClouds = true;
            }
            if (Options.clouds_OnOff.equalsIgnoreCase("off")) {
                this.flgClouds = false;
            }
            if (Options.drawLine_OnOff.equalsIgnoreCase("on")) {
                this.flgLine = true;
            }
            if (Options.drawLine_OnOff.equalsIgnoreCase("off")) {
                this.flgLine = false;
            }
            if (Options.playingModes.equalsIgnoreCase("kids")) {
                this.flgModes = false;
            }
            if (Options.playingModes.equalsIgnoreCase("classic")) {
                this.flgModes = true;
            }
            if (Options.showfps_OnOff.equalsIgnoreCase("on")) {
                this.flgfps = true;
            }
            if (Options.showfps_OnOff.equalsIgnoreCase("off")) {
                this.flgfps = false;
            }
            if (SelectMap.map_Select == R.drawable.mountainjoy) {
                this.bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.mountainjoy);
                this.highscore = PlaneActivity.this.prefsPrivate.getInt("KEY_PRIVATE1", this.pscore1);
                this.tPaint.setColor(-1);
                this.fpsPaint.setColor(-1);
                this.landingZone1X1 = (int) (0.6543000000000001d * ((double) PlaneActivity.this.screen_width));
                this.landingZone1X2 = (int) (0.43439999999999995d * ((double) PlaneActivity.this.screen_width));
                this.landingZone1Y1 = (int) (0.5217d * ((double) PlaneActivity.this.screen_height));
                this.landingZone1Y2 = (int) (0.5925d * ((double) PlaneActivity.this.screen_height));
                if (this.landingZone1X1 % 2 != 0) {
                    this.landingZone1X1++;
                }
                if (this.landingZone1X2 % 2 != 0) {
                    this.landingZone1X2++;
                }
                if (this.landingZone1Y1 % 2 != 0) {
                    this.landingZone1Y1++;
                }
                if (this.landingZone1Y2 % 2 != 0) {
                    this.landingZone1Y2++;
                }
                this.landingZone2X1 = (int) (0.2248d * ((double) PlaneActivity.this.screen_width));
                this.landingZone2X2 = (int) (0.486d * ((double) PlaneActivity.this.screen_width));
                this.landingZone2Y1 = (int) (0.23829999999999998d * ((double) PlaneActivity.this.screen_height));
                this.landingZone2Y2 = (int) (0.305d * ((double) PlaneActivity.this.screen_height));
                if (this.landingZone2X1 % 2 != 0) {
                    this.landingZone2X1++;
                }
                if (this.landingZone2X2 % 2 != 0) {
                    this.landingZone2X2++;
                }
                if (this.landingZone2Y1 % 2 != 0) {
                    this.landingZone2Y1++;
                }
                if (this.landingZone2Y2 % 2 != 0) {
                    this.landingZone2Y2++;
                }
                this.helipadX1 = (int) (0.7587999999999999d * ((double) PlaneActivity.this.screen_width));
                this.helipadX2 = (int) (0.8431000000000001d * ((double) PlaneActivity.this.screen_width));
                this.helipadY1 = (int) (0.6646d * ((double) PlaneActivity.this.screen_height));
                this.helipadY2 = (int) (0.7833d * ((double) PlaneActivity.this.screen_height));
                if (this.helipadX1 % 2 != 0) {
                    this.helipadX1++;
                }
                if (this.helipadX2 % 2 != 0) {
                    this.helipadX2++;
                }
                if (this.helipadY1 % 2 != 0) {
                    this.helipadY1++;
                }
                if (this.helipadY2 % 2 != 0) {
                    this.helipadY2++;
                }
                this.tempZone1X1 = (int) (0.7095999999999999d * ((double) PlaneActivity.this.screen_width));
                this.tempZone1Y1 = (int) (0.55d * ((double) PlaneActivity.this.screen_height));
                this.tempZone1X2 = (int) (0.7494d * ((double) PlaneActivity.this.screen_width));
                this.tempZone1Y2 = (int) (0.6125d * ((double) PlaneActivity.this.screen_height));
                this.tempZone2X1 = (int) (0.1827d * ((double) PlaneActivity.this.screen_width));
                this.tempZone2Y1 = (int) (0.2583d * ((double) PlaneActivity.this.screen_height));
                this.tempZone2X2 = (int) (0.22949999999999998d * ((double) PlaneActivity.this.screen_width));
                this.tempZone2Y2 = (int) (0.325d * ((double) PlaneActivity.this.screen_height));
                this.templandingZone1X1 = (int) (0.7001999999999999d * ((double) PlaneActivity.this.screen_width));
                this.templandingZone1Y1 = (int) (0.5917d * ((double) PlaneActivity.this.screen_height));
                this.templandingZone2X1 = (int) (0.2365d * ((double) PlaneActivity.this.screen_width));
                this.templandingZone2Y1 = (int) (0.3042d * ((double) PlaneActivity.this.screen_height));
                this.tempheliX1 = (int) (0.7916d * ((double) PlaneActivity.this.screen_width));
                this.tempheliY1 = (int) (0.7082999999999999d * ((double) PlaneActivity.this.screen_height));
            } else if (SelectMap.map_Select == R.drawable.meadowolives) {
                this.bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.meadowolives);
                this.tPaint.setColor(-1);
                this.fpsPaint.setColor(-16777216);
                this.highscore = PlaneActivity.this.prefsPrivate.getInt("KEY_PRIVATE2", this.pscore1);
                this.landingZone1X1 = (int) (0.36590000000000006d * ((double) PlaneActivity.this.screen_width));
                this.landingZone1X2 = (int) (0.4157d * ((double) PlaneActivity.this.screen_width));
                this.landingZone1Y1 = (int) (0.3d * ((double) PlaneActivity.this.screen_height));
                this.landingZone1Y2 = (int) (0.5333d * ((double) PlaneActivity.this.screen_height));
                if (this.landingZone1X1 % 2 != 0) {
                    this.landingZone1X1++;
                }
                if (this.landingZone1X2 % 2 != 0) {
                    this.landingZone1X2++;
                }
                if (this.landingZone1Y1 % 2 != 0) {
                    this.landingZone1Y1++;
                }
                if (this.landingZone1Y2 % 2 != 0) {
                    this.landingZone1Y2++;
                }
                this.landingZone2X1 = (int) (0.5032d * ((double) PlaneActivity.this.screen_width));
                this.landingZone2X2 = (int) (0.5586d * ((double) PlaneActivity.this.screen_width));
                this.landingZone2Y1 = (int) (0.7308d * ((double) PlaneActivity.this.screen_height));
                this.landingZone2Y2 = (int) (0.5292d * ((double) PlaneActivity.this.screen_height));
                if (this.landingZone2X1 % 2 != 0) {
                    this.landingZone2X1++;
                }
                if (this.landingZone2X2 % 2 != 0) {
                    this.landingZone2X2++;
                }
                if (this.landingZone2Y1 % 2 != 0) {
                    this.landingZone2Y1++;
                }
                if (this.landingZone2Y2 % 2 != 0) {
                    this.landingZone2Y2++;
                }
                this.helipadX1 = (int) (0.35710000000000003d * ((double) PlaneActivity.this.screen_width));
                this.helipadX2 = (int) (0.4426d * ((double) PlaneActivity.this.screen_width));
                this.helipadY1 = (int) (0.5663d * ((double) PlaneActivity.this.screen_height));
                this.helipadY2 = (int) (0.6875d * ((double) PlaneActivity.this.screen_height));
                if (this.helipadX1 % 2 != 0) {
                    this.helipadX1++;
                }
                if (this.helipadX2 % 2 != 0) {
                    this.helipadX2++;
                }
                if (this.helipadY1 % 2 != 0) {
                    this.helipadY1++;
                }
                if (this.helipadY2 % 2 != 0) {
                    this.helipadY2++;
                }
                this.tempZone1X1 = (int) (0.3747d * ((double) PlaneActivity.this.screen_width));
                this.tempZone1X2 = (int) (0.4122d * ((double) PlaneActivity.this.screen_width));
                this.tempZone1Y1 = (int) (0.2583d * ((double) PlaneActivity.this.screen_height));
                this.tempZone1Y2 = (int) (0.325d * ((double) PlaneActivity.this.screen_height));
                this.tempZone2X2 = (int) (0.557d * ((double) PlaneActivity.this.screen_width));
                this.tempZone2X1 = (int) (0.517d * ((double) PlaneActivity.this.screen_width));
                this.tempZone2Y1 = (int) (0.7709999999999999d * ((double) PlaneActivity.this.screen_height));
                this.tempZone2Y2 = (int) (0.7959999999999999d * ((double) PlaneActivity.this.screen_height));
                this.templandingZone1X1 = (int) (0.4028d * ((double) PlaneActivity.this.screen_width));
                this.templandingZone1Y1 = (int) (0.3417d * ((double) PlaneActivity.this.screen_height));
                this.tempheliX1 = (int) (0.3981d * ((double) PlaneActivity.this.screen_width));
                this.tempheliY1 = (int) (0.6125d * ((double) PlaneActivity.this.screen_height));
                this.templandingZone2X1 = (int) (0.557d * ((double) PlaneActivity.this.screen_width));
                this.templandingZone2X2 = (int) (0.517d * ((double) PlaneActivity.this.screen_width));
                this.templandingZone2Y1 = (int) (0.7709999999999999d * ((double) PlaneActivity.this.screen_height));
                this.templandingZone2Y2 = (int) (0.7959999999999999d * ((double) PlaneActivity.this.screen_height));
            } else if (SelectMap.map_Select == R.drawable.maritimelands) {
                this.bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.maritimelands);
                this.highscore = PlaneActivity.this.prefsPrivate.getInt("KEY_PRIVATE3", this.pscore1);
                this.landingZone1X1 = (int) (0.6733d * ((double) PlaneActivity.this.screen_width));
                this.landingZone1X2 = (int) (0.8103d * ((double) PlaneActivity.this.screen_width));
                this.landingZone1Y1 = (int) (0.563d * ((double) PlaneActivity.this.screen_height));
                this.landingZone1Y2 = (int) (0.6333d * ((double) PlaneActivity.this.screen_height));
                if (this.landingZone1X1 % 2 != 0) {
                    this.landingZone1X1++;
                }
                if (this.landingZone1X2 % 2 != 0) {
                    this.landingZone1X2++;
                }
                if (this.landingZone1Y1 % 2 != 0) {
                    this.landingZone1Y1++;
                }
                if (this.landingZone1Y2 % 2 != 0) {
                    this.landingZone1Y2++;
                }
                this.helipadX1 = (int) (0.41100000000000003d * ((double) PlaneActivity.this.screen_width));
                this.helipadX2 = (int) (0.5305d * ((double) PlaneActivity.this.screen_width));
                this.helipadY1 = (int) (0.7313d * ((double) PlaneActivity.this.screen_height));
                this.helipadY2 = (int) (0.875d * ((double) PlaneActivity.this.screen_height));
                if (this.helipadX1 % 2 != 0) {
                    this.helipadX1++;
                }
                if (this.helipadX2 % 2 != 0) {
                    this.helipadX2++;
                }
                if (this.helipadY1 % 2 != 0) {
                    this.helipadY1++;
                }
                if (this.helipadY2 % 2 != 0) {
                    this.helipadY2++;
                }
                this.tempZone2X1 = (int) (0.4379d * ((double) PlaneActivity.this.screen_width));
                this.tempZone2X2 = (int) (0.48479999999999995d * ((double) PlaneActivity.this.screen_width));
                this.tempZone2Y1 = (int) (0.3292d * ((double) PlaneActivity.this.screen_height));
                this.tempZone2Y2 = (int) (0.3958d * ((double) PlaneActivity.this.screen_height));
                if (this.tempZone2X1 % 2 != 0) {
                    this.tempZone2X1++;
                }
                if (this.tempZone2X2 % 2 != 0) {
                    this.tempZone2X2++;
                }
                if (this.tempZone2Y1 % 2 != 0) {
                    this.tempZone2Y1++;
                }
                if (this.tempZone2Y2 % 2 != 0) {
                    this.tempZone2Y2++;
                }
                this.templandingZone1X1 = (int) (0.6884999999999999d * ((double) PlaneActivity.this.screen_width));
                this.templandingZone1Y1 = (int) (0.6292d * ((double) PlaneActivity.this.screen_height));
                this.templandingZone2X1 = (int) (0.4731d * ((double) PlaneActivity.this.screen_width));
                this.templandingZone2Y1 = (int) (0.30829999999999996d * ((double) PlaneActivity.this.screen_height));
                this.tempheliX1 = (int) (0.45899999999999996d * ((double) PlaneActivity.this.screen_width));
                this.tempheliY1 = (int) (0.8d * ((double) PlaneActivity.this.screen_height));
                this.tPaint.setColor(-1);
                this.fpsPaint.setColor(-16777216);
            }
            this.bitmap = Bitmap.createScaledBitmap(this.bitmap, PlaneActivity.this.screen_width, PlaneActivity.this.screen_height, true);
            this.bundle = new Bundle();
            this.plane1right = BitmapFactory.decodeResource(getResources(), R.drawable.plane1right);
            this.plane1rightshadow = BitmapFactory.decodeResource(getResources(), R.drawable.plane1rightshadow);
            this.plane1rightlanding = BitmapFactory.decodeResource(getResources(), R.drawable.plane1rightlanding);
            this.plane2right = BitmapFactory.decodeResource(getResources(), R.drawable.plane2right);
            this.plane2rightshadow = BitmapFactory.decodeResource(getResources(), R.drawable.plane2rightshadow);
            this.plane2rightlanding = BitmapFactory.decodeResource(getResources(), R.drawable.plane2rightlanding);
            this.yellowwarning = BitmapFactory.decodeResource(getResources(), R.drawable.yellowwarning);
            this.redwarning = BitmapFactory.decodeResource(getResources(), R.drawable.redwarning);
            this.yellowlandwarning = BitmapFactory.decodeResource(getResources(), R.drawable.yellowlandwng);
            this.redlandwarning = BitmapFactory.decodeResource(getResources(), R.drawable.redlandwng);
            this.arrivalsmall = BitmapFactory.decodeResource(getResources(), R.drawable.arrival);
            this.arrivallarge = BitmapFactory.decodeResource(getResources(), R.drawable.arrival_large);
            this.boxbitmap = BitmapFactory.decodeResource(getResources(), R.drawable.blast);
            this.clouds = BitmapFactory.decodeResource(getResources(), R.drawable.clouds1);
            this.play = BitmapFactory.decodeResource(getResources(), R.drawable.play1);
            this.pause = BitmapFactory.decodeResource(getResources(), R.drawable.pause1);
            this.ply = BitmapFactory.decodeResource(getResources(), R.drawable.pl);
            this.frwd = BitmapFactory.decodeResource(getResources(), R.drawable.frwd);
            this.ply = Bitmap.createScaledBitmap(this.ply, 30, 30, true);
            this.frwd = Bitmap.createScaledBitmap(this.frwd, 30, 30, true);
            this.rand = new Random();
            this.yellowPlane = new GraphicObject(BitmapFactory.decodeResource(getResources(), R.drawable.plane2right), PlaneActivity.this.screen_width + 100, 30, this.Xv, this.Yv, 1);
            if (PlaneActivity.this.GameSpeed) {
                this.yellowPlane.gamespeed = false;
            } else {
                this.yellowPlane.gamespeed = true;
            }
            this.redPlane = new GraphicObject(BitmapFactory.decodeResource(getResources(), R.drawable.plane1right), -60, 140, this.Xv, this.Yv, 2);
            if (PlaneActivity.this.GameSpeed) {
                this.redPlane.gamespeed = false;
            } else {
                this.redPlane.gamespeed = true;
            }
            this.helicopter = new GraphicObject(BitmapFactory.decodeResource(getResources(), R.drawable.heli1), 160, -200, 1.0f, 1.0f, 3);
            if (PlaneActivity.this.GameSpeed) {
                this.helicopter.gamespeed = false;
            } else {
                this.helicopter.gamespeed = true;
            }
            this.helinormal1 = BitmapFactory.decodeResource(getResources(), R.drawable.heli1);
            this.helinormal2 = BitmapFactory.decodeResource(getResources(), R.drawable.heli2);
            this.helinormal3 = BitmapFactory.decodeResource(getResources(), R.drawable.heli3);
            this.heliland1 = BitmapFactory.decodeResource(getResources(), R.drawable.heliw1);
            this.heliland2 = BitmapFactory.decodeResource(getResources(), R.drawable.heliw2);
            this.heliland3 = BitmapFactory.decodeResource(getResources(), R.drawable.heliw3);
            this.helind1 = BitmapFactory.decodeResource(getResources(), R.drawable.helind1);
            this.helind2 = BitmapFactory.decodeResource(getResources(), R.drawable.helind2);
            this.helind3 = BitmapFactory.decodeResource(getResources(), R.drawable.helind3);
            this.heliwd1 = BitmapFactory.decodeResource(getResources(), R.drawable.heliwd1);
            this.heliwd2 = BitmapFactory.decodeResource(getResources(), R.drawable.heliwd2);
            this.heliwd3 = BitmapFactory.decodeResource(getResources(), R.drawable.heliwd3);
            this.helicopter.flgYrun = true;
            this.helicopter.heli1 = this.helinormal1;
            this.helicopter.heli2 = this.helinormal2;
            this.helicopter.heli3 = this.helinormal3;
            this.helicopter.setAngle(90.0f);
            this.circlePaint.setColor(-1);
            this.circlePaint.setStyle(Paint.Style.STROKE);
            if (PlaneActivity.this.screen_width <= 300 || PlaneActivity.this.screen_width >= 360) {
                this.fpsPaint.setTextSize(15.0f);
                this.fpsPaint.setFakeBoldText(true);
                this.tPaint.setTextSize(20.0f);
                this.tPaint.setFakeBoldText(true);
                this.tPaint.setTypeface(Typeface.SANS_SERIF);
            } else {
                this.fpsPaint.setTextSize(10.0f);
                this.fpsPaint.setFakeBoldText(true);
                this.tPaint.setTextSize(10.0f);
                this.tPaint.setFakeBoldText(true);
                this.tPaint.setTypeface(Typeface.SANS_SERIF);
            }
            this.rectPaint.setColor(-16777216);
            this.rectPaint.setStyle(Paint.Style.FILL);
            this.rectPaint.setAntiAlias(true);
            this.rectPaint.setMaskFilter(new BlurMaskFilter(15.0f, BlurMaskFilter.Blur.INNER));
            this.linePaint.setAntiAlias(true);
            this.linePaint.setDither(true);
            this.linePaint.setColor(-1);
            this.linePaint.setStyle(Paint.Style.STROKE);
            this.linePaint.setStrokeJoin(Paint.Join.ROUND);
            this.linePaint.setStrokeCap(Paint.Cap.ROUND);
            this.linePaint.setStrokeWidth(1.0f);
            this.mdrawLinePaint = new Paint();
            this.mdrawLinePaint.setAntiAlias(true);
            this.mdrawLinePaint.setDither(true);
            this.mdrawLinePaint.setColor(-1);
            this.mdrawLinePaint.setStyle(Paint.Style.STROKE);
            this.mdrawLinePaint.setStrokeJoin(Paint.Join.ROUND);
            this.mdrawLinePaint.setStrokeCap(Paint.Cap.ROUND);
            this.mdrawLinePaint.setStrokeWidth(1.0f);
            this.mPath = new Path();
            this.mPath1 = new Path();
            this.mPath2 = new Path();
            this.mPath3 = new Path();
            this.Arrivaly = 30;
            this.Arrivalx = this.bitmap.getWidth() - 10;
            this.Arrival2y = 140;
            this.Arrival2x = 10;
            this.ArrivalheliX = 160;
        }

        /* access modifiers changed from: protected */
        public void onDraw(Canvas canvas) {
            canvas.drawBitmap(this.bitmap, 0.0f, 0.0f, this.mBitmapPaint);
            canvas.drawRect((float) (PlaneActivity.this.screen_width - 110), 0.0f, (float) PlaneActivity.this.screen_width, 30.0f, this.rectPaint);
            canvas.drawText("Score : " + this.score, (float) (PlaneActivity.this.screen_width - 100), 20.0f, this.tPaint);
            if (PlaneActivity.this.GameSpeed) {
                canvas.drawBitmap(this.frwd, 20.0f, (float) (PlaneActivity.this.screen_height - 50), this.mBitmapPaint);
            } else {
                canvas.drawBitmap(this.ply, 20.0f, (float) (PlaneActivity.this.screen_height - 50), this.mBitmapPaint);
            }
            if (this.flgfps) {
                canvas.drawRect(10.0f, 0.0f, 100.0f, 30.0f, this.rectPaint);
                canvas.drawText("FPS : " + this.fps, 20.0f, 20.0f, this.fpsPaint);
            }
            if (this.flgModes && (this.yellowPlane.isTouched() || this.redPlane.isTouched() || this.helicopter.isTouched())) {
                canvas.drawPath(this.mPath, this.mdrawLinePaint);
            }
            if (!PlaneActivity.this.flgPause) {
                canvas.drawBitmap(this.pause, (float) (PlaneActivity.this.screen_width - 50), (float) (PlaneActivity.this.screen_height - 70), this.mBitmapPaint);
            } else {
                canvas.drawBitmap(this.play, (float) (getWidth() - 50), (float) (getHeight() - 70), this.mBitmapPaint);
            }
            if (this.flgLine) {
                canvas.drawPath(this.mPath1, this.linePaint);
                canvas.drawPath(this.mPath2, this.linePaint);
                canvas.drawPath(this.mPath3, this.linePaint);
            }
            if (!this.yellowPlane.flgOnSurface) {
                if (this.i >= 0 && this.i <= 15) {
                    canvas.drawBitmap(this.arrivalsmall, (float) (this.Arrivalx - (this.arrivalsmall.getWidth() / 2)), (float) (this.Arrivaly - (this.arrivalsmall.getHeight() / 2)), (Paint) null);
                    this.i++;
                } else if (this.i < 15 || this.i > 30) {
                    this.i = 0;
                } else {
                    canvas.drawBitmap(this.arrivallarge, (float) (this.Arrivalx - (this.arrivallarge.getWidth() / 2)), (float) (this.Arrivaly - (this.arrivallarge.getHeight() / 2)), (Paint) null);
                    this.i++;
                }
            }
            if (!this.redPlane.flgOnSurface) {
                if (this.j >= 0 && this.j <= 15) {
                    canvas.drawBitmap(this.arrivalsmall, (float) (this.Arrival2x - (this.arrivalsmall.getWidth() / 2)), (float) (this.Arrival2y - (this.arrivalsmall.getHeight() / 2)), (Paint) null);
                    this.j++;
                } else if (this.j < 15 || this.j > 30) {
                    this.j = 0;
                } else {
                    canvas.drawBitmap(this.arrivallarge, (float) (this.Arrival2x - (this.arrivallarge.getWidth() / 2)), (float) (this.Arrival2y - (this.arrivallarge.getHeight() / 2)), (Paint) null);
                    this.j++;
                }
            }
            if (!this.helicopter.flgOnSurface) {
                if (this.k >= 0 && this.k <= 15) {
                    canvas.drawBitmap(this.arrivalsmall, (float) (this.ArrivalheliX - (this.arrivalsmall.getWidth() / 2)), (float) (10 - (this.arrivalsmall.getHeight() / 2)), (Paint) null);
                    this.k++;
                } else if (this.k < 15 || this.k > 30) {
                    this.k = 0;
                } else {
                    canvas.drawBitmap(this.arrivallarge, (float) (this.ArrivalheliX - (this.arrivallarge.getWidth() / 2)), (float) (10 - (this.arrivallarge.getHeight() / 2)), (Paint) null);
                    this.k++;
                }
            }
            this.yellowPlane.draw(canvas);
            this.redPlane.draw(canvas);
            if (this.helicopter.helicounter == 0) {
                this.helicopter.setBitmap(this.helicopter.heli1);
                this.helicopter.helicounter++;
            } else if (this.helicopter.helicounter == 1) {
                this.helicopter.setBitmap(this.helicopter.heli2);
                this.helicopter.helicounter++;
            } else if (this.helicopter.helicounter == 2) {
                this.helicopter.setBitmap(this.helicopter.heli3);
                this.helicopter.helicounter = 0;
            }
            this.helicopter.draw(canvas);
            if (this.flgClouds) {
                canvas.drawBitmap(this.clouds, 0.0f, (float) ((int) (0.3125d * ((double) PlaneActivity.this.screen_height))), this.mBitmapPaint);
                canvas.drawBitmap(this.clouds, (float) ((int) (0.6579d * ((double) PlaneActivity.this.screen_width))), 0.0f, this.mBitmapPaint);
                canvas.drawBitmap(this.clouds, (float) ((int) (0.614d * ((double) PlaneActivity.this.screen_width))), (float) ((int) (0.625d * ((double) PlaneActivity.this.screen_height))), this.mBitmapPaint);
            }
        }

        /* JADX WARN: Type inference failed for: r8v0, types: [com.oziapp.coolLanding.PlaneActivity$MyView] */
        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public void updatePhysics(float r9, int r10) {
            /*
                r8 = this;
                r8.sleep = r9
                r8.fps = r10
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                boolean r0 = r0.flgLandingMode
                if (r0 != 0) goto L_0x003d
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getX()
                com.oziapp.coolLanding.PlaneActivity r1 = com.oziapp.coolLanding.PlaneActivity.this
                int r1 = r1.screen_width
                r2 = 10
                int r1 = r1 - r2
                if (r0 >= r1) goto L_0x003d
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getX()
                if (r0 <= 0) goto L_0x003d
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getY()
                com.oziapp.coolLanding.PlaneActivity r1 = com.oziapp.coolLanding.PlaneActivity.this
                int r1 = r1.screen_height
                r2 = 10
                int r1 = r1 - r2
                if (r0 >= r1) goto L_0x003d
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getY()
                if (r0 <= 0) goto L_0x003d
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.flgOnSurface = r1
            L_0x003d:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                boolean r0 = r0.flgLandingMode
                if (r0 != 0) goto L_0x0076
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getX()
                com.oziapp.coolLanding.PlaneActivity r1 = com.oziapp.coolLanding.PlaneActivity.this
                int r1 = r1.screen_width
                r2 = 10
                int r1 = r1 - r2
                if (r0 >= r1) goto L_0x0076
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getX()
                if (r0 <= 0) goto L_0x0076
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getY()
                com.oziapp.coolLanding.PlaneActivity r1 = com.oziapp.coolLanding.PlaneActivity.this
                int r1 = r1.screen_height
                r2 = 10
                int r1 = r1 - r2
                if (r0 >= r1) goto L_0x0076
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getY()
                if (r0 <= 0) goto L_0x0076
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.flgOnSurface = r1
            L_0x0076:
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                boolean r0 = r0.flgLandingMode
                if (r0 != 0) goto L_0x00af
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                int r0 = r0.getX()
                com.oziapp.coolLanding.PlaneActivity r1 = com.oziapp.coolLanding.PlaneActivity.this
                int r1 = r1.screen_width
                r2 = 10
                int r1 = r1 - r2
                if (r0 >= r1) goto L_0x00af
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                int r0 = r0.getX()
                if (r0 <= 0) goto L_0x00af
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                int r0 = r0.getY()
                com.oziapp.coolLanding.PlaneActivity r1 = com.oziapp.coolLanding.PlaneActivity.this
                int r1 = r1.screen_height
                r2 = 10
                int r1 = r1 - r2
                if (r0 >= r1) goto L_0x00af
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                int r0 = r0.getY()
                if (r0 <= 0) goto L_0x00af
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                r1 = 1
                r0.flgOnSurface = r1
            L_0x00af:
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                boolean r0 = r0.flgPause
                if (r0 != 0) goto L_0x0b90
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                boolean r0 = r0.flgLandingMode
                if (r0 != 0) goto L_0x00cf
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                boolean r0 = r0.flgLandingMode
                if (r0 != 0) goto L_0x00cf
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.GraphicObject r1 = r8.yellowPlane
                r8.crashCheck(r0, r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.GraphicObject r1 = r8.yellowPlane
                r8.warningCheck(r0, r1)
            L_0x00cf:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                boolean r0 = r0.flgLandingMode
                if (r0 != 0) goto L_0x00e9
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                boolean r0 = r0.flgLandingMode
                if (r0 != 0) goto L_0x00e9
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.GraphicObject r1 = r8.helicopter
                r8.crashCheck(r0, r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.GraphicObject r1 = r8.helicopter
                r8.warningCheck(r0, r1)
            L_0x00e9:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                boolean r0 = r0.flgLandingMode
                if (r0 != 0) goto L_0x0103
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                boolean r0 = r0.flgLandingMode
                if (r0 != 0) goto L_0x0103
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.GraphicObject r1 = r8.helicopter
                r8.crashCheck(r0, r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.GraphicObject r1 = r8.helicopter
                r8.warningCheck(r0, r1)
            L_0x0103:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                boolean r0 = r0.flgLandingMode
                if (r0 == 0) goto L_0x011b
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.GraphicObject r0 = r0.warningObject
                if (r0 == 0) goto L_0x011b
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.GraphicObject r7 = r0.warningObject
                r8.RemoveWarning(r7)
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.warningObject = r1
            L_0x011b:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                boolean r0 = r0.flgLandingMode
                if (r0 == 0) goto L_0x0133
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.GraphicObject r0 = r0.warningObject
                if (r0 == 0) goto L_0x0133
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.GraphicObject r7 = r0.warningObject
                r8.RemoveWarning(r7)
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.warningObject = r1
            L_0x0133:
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                boolean r0 = r0.flgLandingMode
                if (r0 == 0) goto L_0x014b
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                com.oziapp.coolLanding.GraphicObject r0 = r0.warningObject
                if (r0 == 0) goto L_0x014b
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                com.oziapp.coolLanding.GraphicObject r7 = r0.warningObject
                r8.RemoveWarning(r7)
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                r1 = 0
                r0.warningObject = r1
            L_0x014b:
                boolean r0 = r8.flgtouched
                if (r0 == 0) goto L_0x0398
                int r0 = com.oziapp.coolLanding.SelectMap.map_Select
                r1 = 2130837575(0x7f020047, float:1.7280108E38)
                if (r0 != r1) goto L_0x0d51
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getX()
                r8.plane_currentX = r0
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getY()
                r8.plane_currentY = r0
                int r0 = r8.plane_currentX
                int r1 = r8.tempZone1X1
                if (r0 >= r1) goto L_0x018d
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getxDirection()
                r1 = -1
                if (r0 != r1) goto L_0x0188
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleXDirection()
            L_0x0188:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.flgYrun = r1
            L_0x018d:
                android.graphics.Path r0 = r8.mPath1
                r0.reset()
                android.graphics.Path r0 = r8.mPath1
                com.oziapp.coolLanding.GraphicObject r1 = r8.yellowPlane
                int r1 = r1.getX()
                float r1 = (float) r1
                com.oziapp.coolLanding.GraphicObject r2 = r8.yellowPlane
                int r2 = r2.getY()
                float r2 = (float) r2
                r0.moveTo(r1, r2)
                android.graphics.Path r0 = r8.mPath1
                int r1 = r8.tempZone1X1
                int r1 = r1 + 5
                float r1 = (float) r1
                com.oziapp.coolLanding.GraphicObject r2 = r8.yellowPlane
                int r2 = r2.getY()
                float r2 = (float) r2
                r0.lineTo(r1, r2)
                android.graphics.Path r0 = r8.mPath1
                int r1 = r8.tempZone1X1
                int r1 = r1 + 5
                float r1 = (float) r1
                int r2 = r8.landingZone1Y2
                int r3 = r8.landingZone1Y1
                int r2 = r2 + r3
                int r2 = r2 / 2
                int r2 = r2 + 5
                float r2 = (float) r2
                r0.lineTo(r1, r2)
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getX()
                int r1 = r8.tempZone1X1
                int r1 = r1 + 5
                if (r0 <= r1) goto L_0x0248
                android.graphics.Path r0 = r8.mPath1
                r0.reset()
                android.graphics.Path r0 = r8.mPath1
                com.oziapp.coolLanding.GraphicObject r1 = r8.yellowPlane
                int r1 = r1.getX()
                float r1 = (float) r1
                com.oziapp.coolLanding.GraphicObject r2 = r8.yellowPlane
                int r2 = r2.getY()
                float r2 = (float) r2
                r0.moveTo(r1, r2)
                android.graphics.Path r0 = r8.mPath1
                com.oziapp.coolLanding.GraphicObject r1 = r8.yellowPlane
                int r1 = r1.getX()
                float r1 = (float) r1
                int r2 = r8.landingZone1Y2
                int r3 = r8.landingZone1Y1
                int r2 = r2 + r3
                int r2 = r2 / 2
                float r2 = (float) r2
                r0.lineTo(r1, r2)
                android.graphics.Path r0 = r8.mPath1
                int r1 = r8.tempZone1X1
                int r1 = r1 + 5
                float r1 = (float) r1
                int r2 = r8.landingZone1Y2
                int r3 = r8.landingZone1Y1
                int r2 = r2 + r3
                int r2 = r2 / 2
                float r2 = (float) r2
                r0.lineTo(r1, r2)
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.flgYrun = r1
                int r0 = r8.plane_currentY
                int r1 = r8.landingZone1Y2
                int r2 = r8.landingZone1Y1
                int r1 = r1 + r2
                int r1 = r1 / 2
                int r1 = r1 + 5
                if (r0 <= r1) goto L_0x0b91
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = -1028390912(0xffffffffc2b40000, float:-90.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getyDirection()
                r1 = 1
                if (r0 != r1) goto L_0x0243
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleYDirection()
            L_0x0243:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.flgYrun = r1
            L_0x0248:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getY()
                int r1 = r8.landingZone1Y2
                int r2 = r8.landingZone1Y1
                int r1 = r1 + r2
                int r1 = r1 / 2
                int r1 = r1 + 5
                if (r0 == r1) goto L_0x026a
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getY()
                int r1 = r8.landingZone1Y2
                int r2 = r8.landingZone1Y1
                int r1 = r1 + r2
                int r1 = r1 / 2
                int r1 = r1 + 4
                if (r0 != r1) goto L_0x0398
            L_0x026a:
                boolean r0 = r8.flgtoggle
                if (r0 == 0) goto L_0x0398
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.flgTouchEnable = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1127481344(0x43340000, float:180.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getxDirection()
                r1 = 1
                if (r0 != r1) goto L_0x0290
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleXDirection()
            L_0x0290:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.flgYrun = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getX()
                int r1 = r8.tempZone1X1
                int r1 = r1 + 6
                if (r0 == r1) goto L_0x02ad
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getX()
                int r1 = r8.tempZone1X1
                int r1 = r1 + 7
                if (r0 != r1) goto L_0x02c0
            L_0x02ad:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.flgLandingMode = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.flgOnSurface = r1
                java.util.Random r0 = r8.rand
                r1 = 4
                int r0 = r0.nextInt(r1)
                r8.p = r0
            L_0x02c0:
                int r0 = r8.p
                r1 = 2
                if (r0 != r1) goto L_0x0bc5
                int r0 = r8.redDirection
                r1 = 2
                if (r0 == r1) goto L_0x0bc5
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                int r0 = r0.screen_width
                r1 = 10
                int r0 = r0 - r1
                r8.Arrivalx = r0
            L_0x02d3:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getX()
                int r1 = r8.tempZone1X1
                if (r0 >= r1) goto L_0x0328
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                boolean r0 = r0.flgLandingMode
                if (r0 == 0) goto L_0x0328
                int r0 = r8.plane2
                int r0 = r0 % 2
                if (r0 != 0) goto L_0x0315
                java.util.Random r0 = r8.rand
                r1 = 19
                int r0 = r0.nextInt(r1)
                r8.new1Y = r0
                java.util.Random r0 = r8.rand
                r1 = 19
                int r0 = r0.nextInt(r1)
                r8.new1X = r0
                int r0 = r8.p
                r1 = 1
                if (r0 != r1) goto L_0x0c03
                int r0 = r8.redDirection
                r1 = 1
                if (r0 == r1) goto L_0x0c03
                int[] r0 = r8.RandomPlane1X
                int r1 = r8.new1X
                r0 = r0[r1]
                r8.Arrivalx = r0
            L_0x030f:
                int r0 = r8.plane2
                int r0 = r0 + 1
                r8.plane2 = r0
            L_0x0315:
                android.graphics.Path r0 = r8.mPath1
                r0.reset()
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                android.graphics.Bitmap r1 = r8.plane2rightlanding
                r0.setBitmap(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1127481344(0x43340000, float:180.0)
                r0.setAngle(r1)
            L_0x0328:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getX()
                int r1 = r8.tempZone1X1
                r2 = 140(0x8c, float:1.96E-43)
                int r1 = r1 - r2
                if (r0 == r1) goto L_0x0342
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getX()
                int r1 = r8.tempZone1X1
                r2 = 139(0x8b, float:1.95E-43)
                int r1 = r1 - r2
                if (r0 != r1) goto L_0x0398
            L_0x0342:
                r0 = 0
                r8.flgtouched = r0
                int r0 = r8.score
                int r0 = r0 + 1
                r8.score = r0
                int r0 = r8.p
                r1 = 1
                if (r0 != r1) goto L_0x0c4e
                int r0 = r8.redDirection
                r1 = 1
                if (r0 == r1) goto L_0x0c4e
                com.oziapp.coolLanding.GraphicObject r0 = new com.oziapp.coolLanding.GraphicObject
                android.content.res.Resources r1 = r8.getResources()
                r2 = 2130837613(0x7f02006d, float:1.7280185E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)
                int[] r2 = r8.RandomPlane1X
                int r3 = r8.new1X
                r2 = r2[r3]
                com.oziapp.coolLanding.PlaneActivity r3 = com.oziapp.coolLanding.PlaneActivity.this
                int r3 = r3.screen_height
                float r4 = r8.Xv
                float r5 = r8.Yv
                r6 = 1
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r8.yellowPlane = r0
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                boolean r0 = r0.GameSpeed
                if (r0 == 0) goto L_0x0c47
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.gamespeed = r1
            L_0x0381:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.flgYrun = r1
                r0 = 0
                r8.flgtoggle = r0
            L_0x0389:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.flgTouchEnable = r1
                int r0 = r8.p
                r8.yellowDirection = r0
                int r0 = r8.plane2
                r1 = 1
                int r0 = r0 - r1
                r8.plane2 = r0
            L_0x0398:
                boolean r0 = r8.flgtouched2
                if (r0 == 0) goto L_0x05f3
                int r0 = com.oziapp.coolLanding.SelectMap.map_Select
                r1 = 2130837572(0x7f020044, float:1.7280102E38)
                if (r0 != r1) goto L_0x1702
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getX()
                r8.plane_currentX2 = r0
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getY()
                r8.plane_currentY2 = r0
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                boolean r0 = r0.flgLandingMode
                if (r0 != 0) goto L_0x0580
                int r0 = r8.plane_currentY2
                int r1 = r8.tempZone2Y2
                if (r0 <= r1) goto L_0x041e
                int r0 = r8.plane_currentX2
                int r1 = r8.tempZone2X1
                int r1 = r1 + 20
                if (r0 >= r1) goto L_0x155f
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getxDirection()
                r1 = -1
                if (r0 != r1) goto L_0x03e3
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleXDirection()
            L_0x03e3:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.flgYrun = r1
            L_0x03e8:
                android.graphics.Path r0 = r8.mPath2
                r0.reset()
                android.graphics.Path r0 = r8.mPath2
                com.oziapp.coolLanding.GraphicObject r1 = r8.redPlane
                int r1 = r1.getX()
                float r1 = (float) r1
                com.oziapp.coolLanding.GraphicObject r2 = r8.redPlane
                int r2 = r2.getY()
                float r2 = (float) r2
                r0.moveTo(r1, r2)
                android.graphics.Path r0 = r8.mPath2
                int r1 = r8.tempZone2X1
                int r1 = r1 + 25
                float r1 = (float) r1
                com.oziapp.coolLanding.GraphicObject r2 = r8.redPlane
                int r2 = r2.getY()
                float r2 = (float) r2
                r0.lineTo(r1, r2)
                android.graphics.Path r0 = r8.mPath2
                int r1 = r8.tempZone2X1
                int r1 = r1 + 25
                float r1 = (float) r1
                int r2 = r8.tempZone2Y2
                float r2 = (float) r2
                r0.lineTo(r1, r2)
            L_0x041e:
                int r0 = r8.plane_currentY2
                int r1 = r8.tempZone2Y2
                if (r0 >= r1) goto L_0x04ad
                android.graphics.Path r0 = r8.mPath2
                r0.reset()
                android.graphics.Path r0 = r8.mPath2
                com.oziapp.coolLanding.GraphicObject r1 = r8.redPlane
                int r1 = r1.getX()
                float r1 = (float) r1
                com.oziapp.coolLanding.GraphicObject r2 = r8.redPlane
                int r2 = r2.getY()
                float r2 = (float) r2
                r0.moveTo(r1, r2)
                android.graphics.Path r0 = r8.mPath2
                com.oziapp.coolLanding.GraphicObject r1 = r8.redPlane
                int r1 = r1.getX()
                float r1 = (float) r1
                int r2 = r8.tempZone2Y2
                float r2 = (float) r2
                r0.lineTo(r1, r2)
                android.graphics.Path r0 = r8.mPath2
                int r1 = r8.tempZone2X1
                int r1 = r1 + 25
                float r1 = (float) r1
                int r2 = r8.tempZone2Y2
                float r2 = (float) r2
                r0.lineTo(r1, r2)
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.flgYrun = r1
                int r0 = r8.plane_currentY2
                int r1 = r8.tempZone2Y2
                if (r0 >= r1) goto L_0x0485
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1119092736(0x42b40000, float:90.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getyDirection()
                r1 = -1
                if (r0 != r1) goto L_0x0480
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleYDirection()
            L_0x0480:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.flgYrun = r1
            L_0x0485:
                int r0 = r8.plane_currentY2
                int r1 = r8.tempZone2Y2
                if (r0 <= r1) goto L_0x04ad
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = -1028390912(0xffffffffc2b40000, float:-90.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getyDirection()
                r1 = 1
                if (r0 != r1) goto L_0x04a8
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleYDirection()
            L_0x04a8:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.flgYrun = r1
            L_0x04ad:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getX()
                int r1 = r8.tempZone2X1
                int r1 = r1 + 20
                if (r0 < r1) goto L_0x0580
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getX()
                int r1 = r8.tempZone2X1
                int r1 = r1 + 40
                if (r0 > r1) goto L_0x0580
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.flgYrun = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.flgTouchEnable = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = -1028390912(0xffffffffc2b40000, float:-90.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getyDirection()
                r1 = 1
                if (r0 != r1) goto L_0x04ec
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleYDirection()
            L_0x04ec:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getY()
                int r1 = r8.tempZone2Y2
                r2 = 30
                int r1 = r1 - r2
                if (r0 < r1) goto L_0x0580
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getY()
                int r1 = r8.tempZone2Y2
                if (r0 > r1) goto L_0x0580
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.GraphicObject r1 = r8.redPlane
                int r1 = r1.mX
                r0.setX(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.GraphicObject r1 = r8.redPlane
                int r1 = r1.mY
                r0.setY(r1)
                java.util.Random r0 = r8.rand
                r1 = 4
                int r0 = r0.nextInt(r1)
                r8.p2 = r0
                int r0 = r8.p2
                r1 = 2
                if (r0 != r1) goto L_0x1589
                int r0 = r8.yellowDirection
                r1 = 2
                if (r0 == r1) goto L_0x1589
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                int r0 = r0.screen_width
                r1 = 10
                int r0 = r0 - r1
                r8.Arrival2x = r0
            L_0x0531:
                int r0 = r8.plane1
                int r0 = r0 % 2
                if (r0 != 0) goto L_0x0563
                java.util.Random r0 = r8.rand
                r1 = 19
                int r0 = r0.nextInt(r1)
                r8.new2Y = r0
                java.util.Random r0 = r8.rand
                r1 = 19
                int r0 = r0.nextInt(r1)
                r8.new2X = r0
                int r0 = r8.p2
                r1 = 1
                if (r0 != r1) goto L_0x15c2
                int r0 = r8.yellowDirection
                r1 = 1
                if (r0 == r1) goto L_0x15c2
                int[] r0 = r8.RandomPlane2X
                int r1 = r8.new2X
                r0 = r0[r1]
                r8.Arrival2x = r0
            L_0x055d:
                int r0 = r8.plane1
                int r0 = r0 + 1
                r8.plane1 = r0
            L_0x0563:
                android.graphics.Path r0 = r8.mPath2
                r0.reset()
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.flgLandingMode = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.flgOnSurface = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                android.graphics.Bitmap r1 = r8.plane1rightlanding
                r0.setBitmap(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1129447424(0x43520000, float:210.0)
                r0.setAngle(r1)
            L_0x0580:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getY()
                int r1 = r8.tempZone2Y2
                r2 = 50
                int r1 = r1 - r2
                if (r0 < r1) goto L_0x05f3
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getY()
                int r1 = r8.tempZone2Y2
                r2 = 40
                int r1 = r1 - r2
                if (r0 > r1) goto L_0x05f3
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                boolean r0 = r0.flgLandingMode
                if (r0 == 0) goto L_0x05f3
                r0 = 0
                r8.flgtouched2 = r0
                int r0 = r8.score
                int r0 = r0 + 1
                r8.score = r0
                int r0 = r8.p2
                r1 = 1
                if (r0 != r1) goto L_0x160d
                int r0 = r8.yellowDirection
                r1 = 1
                if (r0 == r1) goto L_0x160d
                com.oziapp.coolLanding.GraphicObject r0 = new com.oziapp.coolLanding.GraphicObject
                android.content.res.Resources r1 = r8.getResources()
                r2 = 2130837610(0x7f02006a, float:1.7280179E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)
                int[] r2 = r8.RandomPlane2X
                int r3 = r8.new2X
                r2 = r2[r3]
                com.oziapp.coolLanding.PlaneActivity r3 = com.oziapp.coolLanding.PlaneActivity.this
                int r3 = r3.screen_height
                float r4 = r8.Xv
                float r5 = r8.Yv
                r6 = 2
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r8.redPlane = r0
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                boolean r0 = r0.GameSpeed
                if (r0 == 0) goto L_0x1606
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.gamespeed = r1
            L_0x05df:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.flgYrun = r1
            L_0x05e4:
                int r0 = r8.p2
                r8.redDirection = r0
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.flgTouchEnable = r1
                int r0 = r8.plane1
                r1 = 1
                int r0 = r0 - r1
                r8.plane1 = r0
            L_0x05f3:
                boolean r0 = r8.flghelipad
                if (r0 == 0) goto L_0x0830
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                int r0 = r0.getX()
                r8.plane_currentX3 = r0
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                int r0 = r0.getY()
                r8.plane_currentY3 = r0
                int r0 = r8.plane_currentY3
                int r1 = r8.helipadY1
                r2 = 5
                int r1 = r1 - r2
                if (r0 >= r1) goto L_0x066a
                android.graphics.Path r0 = r8.mPath3
                r0.reset()
                android.graphics.Path r0 = r8.mPath3
                com.oziapp.coolLanding.GraphicObject r1 = r8.helicopter
                int r1 = r1.getX()
                float r1 = (float) r1
                com.oziapp.coolLanding.GraphicObject r2 = r8.helicopter
                int r2 = r2.getY()
                float r2 = (float) r2
                r0.moveTo(r1, r2)
                android.graphics.Path r0 = r8.mPath3
                com.oziapp.coolLanding.GraphicObject r1 = r8.helicopter
                int r1 = r1.getX()
                float r1 = (float) r1
                int r2 = r8.helipadY1
                int r2 = r2 + 25
                float r2 = (float) r2
                r0.lineTo(r1, r2)
                int r0 = r8.plane_currentX3
                int r1 = r8.helipadX2
                if (r0 <= r1) goto L_0x1ee3
                android.graphics.Path r0 = r8.mPath3
                int r1 = r8.helipadX2
                int r1 = r1 + 5
                float r1 = (float) r1
                int r2 = r8.helipadY1
                int r2 = r2 + 25
                float r2 = (float) r2
                r0.lineTo(r1, r2)
            L_0x064d:
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                r1 = 1119092736(0x42b40000, float:90.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getyDirection()
                r1 = -1
                if (r0 != r1) goto L_0x066a
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleYDirection()
            L_0x066a:
                int r0 = r8.plane_currentY3
                int r1 = r8.helipadY2
                r2 = 5
                int r1 = r1 - r2
                if (r0 <= r1) goto L_0x06cd
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                r1 = -1028390912(0xffffffffc2b40000, float:-90.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getyDirection()
                r1 = 1
                if (r0 != r1) goto L_0x068f
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleYDirection()
            L_0x068f:
                android.graphics.Path r0 = r8.mPath3
                r0.reset()
                android.graphics.Path r0 = r8.mPath3
                com.oziapp.coolLanding.GraphicObject r1 = r8.helicopter
                int r1 = r1.getX()
                float r1 = (float) r1
                com.oziapp.coolLanding.GraphicObject r2 = r8.helicopter
                int r2 = r2.getY()
                float r2 = (float) r2
                r0.moveTo(r1, r2)
                android.graphics.Path r0 = r8.mPath3
                com.oziapp.coolLanding.GraphicObject r1 = r8.helicopter
                int r1 = r1.getX()
                float r1 = (float) r1
                int r2 = r8.helipadY1
                int r2 = r2 + 25
                float r2 = (float) r2
                r0.lineTo(r1, r2)
                int r0 = r8.plane_currentX3
                int r1 = r8.helipadX2
                if (r0 <= r1) goto L_0x1ef4
                android.graphics.Path r0 = r8.mPath3
                int r1 = r8.helipadX2
                int r1 = r1 + 5
                float r1 = (float) r1
                int r2 = r8.helipadY1
                int r2 = r2 + 25
                float r2 = (float) r2
                r0.lineTo(r1, r2)
            L_0x06cd:
                int r0 = r8.plane_currentY3
                int r1 = r8.helipadY1
                int r1 = r1 + 20
                if (r0 < r1) goto L_0x0830
                int r0 = r8.plane_currentY3
                int r1 = r8.helipadY1
                int r1 = r1 + 25
                if (r0 > r1) goto L_0x0830
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                r1 = 0
                r0.flgYrun = r1
                int r0 = r8.plane_currentX3
                int r1 = r8.helipadX2
                if (r0 <= r1) goto L_0x1f16
                android.graphics.Path r0 = r8.mPath3
                r0.reset()
                android.graphics.Path r0 = r8.mPath3
                com.oziapp.coolLanding.GraphicObject r1 = r8.helicopter
                int r1 = r1.getX()
                float r1 = (float) r1
                com.oziapp.coolLanding.GraphicObject r2 = r8.helicopter
                int r2 = r2.getY()
                float r2 = (float) r2
                r0.moveTo(r1, r2)
                int r0 = r8.plane_currentY3
                int r1 = r8.helipadY1
                int r1 = r1 + 10
                if (r0 <= r1) goto L_0x1f05
                android.graphics.Path r0 = r8.mPath3
                int r1 = r8.helipadX2
                int r1 = r1 + 5
                float r1 = (float) r1
                int r2 = r8.helipadY1
                int r2 = r2 + 25
                float r2 = (float) r2
                r0.lineTo(r1, r2)
            L_0x0717:
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getxDirection()
                r1 = 1
                if (r0 != r1) goto L_0x0732
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                r1 = 0
                r0.flgYrun = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleXDirection()
            L_0x0732:
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                r1 = 1127481344(0x43340000, float:180.0)
                r0.setAngle(r1)
            L_0x0739:
                int r0 = r8.plane_currentX3
                int r1 = r8.helipadX1
                if (r0 <= r1) goto L_0x07a3
                int r0 = r8.plane_currentX3
                int r1 = r8.helipadX2
                if (r0 >= r1) goto L_0x07a3
                int r0 = r8.plane_currentY3
                int r1 = r8.helipadY1
                if (r0 <= r1) goto L_0x07a3
                int r0 = r8.plane_currentY3
                int r1 = r8.helipadY2
                if (r0 >= r1) goto L_0x07a3
                boolean r0 = r8.flgRandomAllow
                if (r0 == 0) goto L_0x076d
                r0 = 0
                r8.flgRandomAllow = r0
                r0 = 0
                r8.rand_heli = r0
                java.util.Random r0 = r8.rand
                r1 = 19
                int r0 = r0.nextInt(r1)
                r8.newheliX = r0
                int[] r0 = r8.HelicopterX
                int r1 = r8.newheliX
                r0 = r0[r1]
                r8.ArrivalheliX = r0
            L_0x076d:
                int r0 = r8.rand_heli
                if (r0 != 0) goto L_0x0788
                int r0 = r8.p
                if (r0 != 0) goto L_0x0788
                int r0 = r8.Arrivalx
                int r0 = r0 + 48
                int r1 = r8.getWidth()
                r2 = 10
                int r1 = r1 - r2
                if (r0 >= r1) goto L_0x1f7a
                int r0 = r8.Arrivalx
                int r0 = r0 + 48
                r8.ArrivalheliX = r0
            L_0x0788:
                int r0 = r8.rand_heli
                if (r0 != 0) goto L_0x07a3
                int r0 = r8.p2
                if (r0 != 0) goto L_0x07a3
                int r0 = r8.Arrival2x
                int r0 = r0 + 48
                int r1 = r8.getWidth()
                r2 = 10
                int r1 = r1 - r2
                if (r0 >= r1) goto L_0x1f83
                int r0 = r8.Arrival2x
                int r0 = r0 + 48
                r8.ArrivalheliX = r0
            L_0x07a3:
                int r0 = r8.plane_currentX3
                int r1 = r8.helipadX1
                int r1 = r1 + 15
                if (r0 < r1) goto L_0x0830
                int r0 = r8.plane_currentX3
                int r1 = r8.helipadX2
                if (r0 > r1) goto L_0x0830
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                r1 = 0
                r0.flgTouchEnable = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                r1 = 1
                r0.flgLandingMode = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                r1 = 0
                r0.flgOnSurface = r1
                android.graphics.Path r0 = r8.mPath3
                r0.reset()
                int r0 = r8.plane_currentX3
                int r1 = r8.helipadX1
                int r1 = r1 + 30
                if (r0 == r1) goto L_0x07d5
                int r0 = r8.plane_currentX3
                int r1 = r8.helipadX1
                int r1 = r1 + 31
                if (r0 != r1) goto L_0x0830
            L_0x07d5:
                r0 = 0
                r8.flghelipad = r0
                int r0 = r8.score
                int r0 = r0 + 1
                r8.score = r0
                com.oziapp.coolLanding.GraphicObject r0 = new com.oziapp.coolLanding.GraphicObject
                android.content.res.Resources r1 = r8.getResources()
                r2 = 2130837530(0x7f02001a, float:1.7280017E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)
                int r2 = r8.ArrivalheliX
                r3 = 10
                r4 = 1065353216(0x3f800000, float:1.0)
                r5 = 1065353216(0x3f800000, float:1.0)
                r6 = 3
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r8.helicopter = r0
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                boolean r0 = r0.GameSpeed
                if (r0 == 0) goto L_0x1f8c
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                r1 = 0
                r0.gamespeed = r1
            L_0x0804:
                int r0 = r8.h
                r1 = 1
                int r0 = r0 - r1
                r8.h = r0
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                android.graphics.Bitmap r1 = r8.helinormal1
                r0.heli1 = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                android.graphics.Bitmap r1 = r8.helinormal2
                r0.heli2 = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                android.graphics.Bitmap r1 = r8.helinormal3
                r0.heli3 = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                r1 = 1119092736(0x42b40000, float:90.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                r1 = 1
                r0.flgYrun = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                r1 = 1
                r0.flgTouchEnable = r1
                r0 = 1
                r8.flgRandomAllow = r0
            L_0x0830:
                boolean r0 = r8.flgModes
                if (r0 != 0) goto L_0x0909
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                boolean r0 = r0.isTouched()
                if (r0 == 0) goto L_0x0877
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                android.graphics.Bitmap r1 = r8.plane2right
                r0.setBitmap(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.flgGoforLand = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getxDirection()
                r1 = -1
                if (r0 != r1) goto L_0x1f93
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.flgYrun = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleXDirection()
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.setTouched(r1)
                r0 = 0
                r8.flgtouched = r0
                android.graphics.Path r0 = r8.mPath1
                r0.reset()
            L_0x0877:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                boolean r0 = r0.isTouched()
                if (r0 == 0) goto L_0x08ba
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                android.graphics.Bitmap r1 = r8.plane1right
                r0.setBitmap(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.flgGoforLand = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getxDirection()
                r1 = -1
                if (r0 != r1) goto L_0x1fc5
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.flgYrun = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleXDirection()
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.setTouched(r1)
                r0 = 0
                r8.flgtouched2 = r0
                android.graphics.Path r0 = r8.mPath2
                r0.reset()
            L_0x08ba:
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                boolean r0 = r0.isTouched()
                if (r0 == 0) goto L_0x0909
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                r1 = 0
                r0.flgGoforLand = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getyDirection()
                r1 = 1
                if (r0 != r1) goto L_0x1ff7
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                r1 = 1
                r0.flgYrun = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                android.graphics.Bitmap r1 = r8.helinormal1
                r0.heli1 = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                android.graphics.Bitmap r1 = r8.helinormal2
                r0.heli2 = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                android.graphics.Bitmap r1 = r8.helinormal3
                r0.heli3 = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                r1 = -1028390912(0xffffffffc2b40000, float:-90.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleYDirection()
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                r1 = 0
                r0.setTouched(r1)
                r0 = 0
                r8.flghelipad = r0
                android.graphics.Path r0 = r8.mPath3
                r0.reset()
            L_0x0909:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getxDirection()
                r1 = 1
                if (r0 != r1) goto L_0x094a
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getX()
                com.oziapp.coolLanding.GraphicObject r1 = r8.yellowPlane
                android.graphics.Bitmap r1 = r1.getBitmap()
                int r1 = r1.getWidth()
                int r1 = r1 / 2
                int r0 = r0 + r1
                com.oziapp.coolLanding.PlaneActivity r1 = com.oziapp.coolLanding.PlaneActivity.this
                int r1 = r1.screen_width
                r2 = 10
                int r1 = r1 - r2
                if (r0 < r1) goto L_0x094a
                r0 = 0
                r8.flgNotLanding = r0
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleXDirection()
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1127481344(0x43340000, float:180.0)
                r0.setAngle(r1)
                android.graphics.Path r0 = r8.mPath1
                r0.reset()
            L_0x094a:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getxDirection()
                r1 = -1
                if (r0 != r1) goto L_0x0985
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getX()
                com.oziapp.coolLanding.GraphicObject r1 = r8.yellowPlane
                android.graphics.Bitmap r1 = r1.getBitmap()
                int r1 = r1.getWidth()
                int r1 = r1 / 2
                int r0 = r0 - r1
                r1 = -15
                if (r0 > r1) goto L_0x0985
                r0 = 0
                r8.flgNotLanding = r0
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleXDirection()
                android.graphics.Path r0 = r8.mPath1
                r0.reset()
            L_0x0985:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getyDirection()
                r1 = 1
                if (r0 != r1) goto L_0x09c3
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getY()
                com.oziapp.coolLanding.GraphicObject r1 = r8.yellowPlane
                android.graphics.Bitmap r1 = r1.getBitmap()
                int r1 = r1.getHeight()
                int r1 = r1 / 2
                int r0 = r0 + r1
                com.oziapp.coolLanding.PlaneActivity r1 = com.oziapp.coolLanding.PlaneActivity.this
                int r1 = r1.screen_height
                if (r0 < r1) goto L_0x09c3
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = -1028390912(0xffffffffc2b40000, float:-90.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleYDirection()
                r0 = 0
                r8.flgNotLanding = r0
                android.graphics.Path r0 = r8.mPath1
                r0.reset()
            L_0x09c3:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getyDirection()
                r1 = -1
                if (r0 != r1) goto L_0x09fd
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getY()
                com.oziapp.coolLanding.GraphicObject r1 = r8.yellowPlane
                android.graphics.Bitmap r1 = r1.getBitmap()
                int r1 = r1.getHeight()
                int r1 = r1 / 2
                int r0 = r0 - r1
                if (r0 > 0) goto L_0x09fd
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1119092736(0x42b40000, float:90.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleYDirection()
                r0 = 0
                r8.flgNotLanding = r0
                android.graphics.Path r0 = r8.mPath1
                r0.reset()
            L_0x09fd:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getxDirection()
                r1 = 1
                if (r0 != r1) goto L_0x0a3e
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getX()
                com.oziapp.coolLanding.GraphicObject r1 = r8.redPlane
                android.graphics.Bitmap r1 = r1.getBitmap()
                int r1 = r1.getWidth()
                int r1 = r1 / 2
                int r0 = r0 + r1
                com.oziapp.coolLanding.PlaneActivity r1 = com.oziapp.coolLanding.PlaneActivity.this
                int r1 = r1.screen_width
                r2 = 10
                int r1 = r1 - r2
                if (r0 < r1) goto L_0x0a3e
                r0 = 0
                r8.flgNotLanding2 = r0
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleXDirection()
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1127481344(0x43340000, float:180.0)
                r0.setAngle(r1)
                android.graphics.Path r0 = r8.mPath2
                r0.reset()
            L_0x0a3e:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getxDirection()
                r1 = -1
                if (r0 != r1) goto L_0x0a79
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getX()
                com.oziapp.coolLanding.GraphicObject r1 = r8.redPlane
                android.graphics.Bitmap r1 = r1.getBitmap()
                int r1 = r1.getWidth()
                int r1 = r1 / 2
                int r0 = r0 - r1
                r1 = -15
                if (r0 > r1) goto L_0x0a79
                r0 = 0
                r8.flgNotLanding2 = r0
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleXDirection()
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.setAngle(r1)
                android.graphics.Path r0 = r8.mPath2
                r0.reset()
            L_0x0a79:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getyDirection()
                r1 = 1
                if (r0 != r1) goto L_0x0ab7
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getY()
                com.oziapp.coolLanding.GraphicObject r1 = r8.redPlane
                android.graphics.Bitmap r1 = r1.getBitmap()
                int r1 = r1.getHeight()
                int r1 = r1 / 2
                int r0 = r0 + r1
                com.oziapp.coolLanding.PlaneActivity r1 = com.oziapp.coolLanding.PlaneActivity.this
                int r1 = r1.screen_height
                if (r0 < r1) goto L_0x0ab7
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = -1028390912(0xffffffffc2b40000, float:-90.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleYDirection()
                r0 = 0
                r8.flgNotLanding2 = r0
                android.graphics.Path r0 = r8.mPath2
                r0.reset()
            L_0x0ab7:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getyDirection()
                r1 = -1
                if (r0 != r1) goto L_0x0af1
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getY()
                com.oziapp.coolLanding.GraphicObject r1 = r8.redPlane
                android.graphics.Bitmap r1 = r1.getBitmap()
                int r1 = r1.getHeight()
                int r1 = r1 / 2
                int r0 = r0 - r1
                if (r0 > 0) goto L_0x0af1
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1119092736(0x42b40000, float:90.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleYDirection()
                r0 = 0
                r8.flgNotLanding2 = r0
                android.graphics.Path r0 = r8.mPath2
                r0.reset()
            L_0x0af1:
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getyDirection()
                r1 = 1
                if (r0 != r1) goto L_0x0b2f
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                int r0 = r0.getY()
                com.oziapp.coolLanding.GraphicObject r1 = r8.helicopter
                android.graphics.Bitmap r1 = r1.getBitmap()
                int r1 = r1.getHeight()
                int r1 = r1 / 2
                int r0 = r0 + r1
                com.oziapp.coolLanding.PlaneActivity r1 = com.oziapp.coolLanding.PlaneActivity.this
                int r1 = r1.screen_height
                if (r0 < r1) goto L_0x0b2f
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                r1 = -1028390912(0xffffffffc2b40000, float:-90.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleYDirection()
                android.graphics.Path r0 = r8.mPath3
                r0.reset()
                r0 = 0
                r8.flgNotLanding3 = r0
            L_0x0b2f:
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getyDirection()
                r1 = -1
                if (r0 != r1) goto L_0x0b69
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                int r0 = r0.getY()
                com.oziapp.coolLanding.GraphicObject r1 = r8.helicopter
                android.graphics.Bitmap r1 = r1.getBitmap()
                int r1 = r1.getHeight()
                int r1 = r1 / 2
                int r0 = r0 - r1
                if (r0 > 0) goto L_0x0b69
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                r1 = 1119092736(0x42b40000, float:90.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleYDirection()
                android.graphics.Path r0 = r8.mPath3
                r0.reset()
                r0 = 0
                r8.flgNotLanding3 = r0
            L_0x0b69:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                boolean r0 = r0.isTouched()
                if (r0 != 0) goto L_0x0b76
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r0.update()
            L_0x0b76:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                boolean r0 = r0.isTouched()
                if (r0 != 0) goto L_0x0b83
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r0.update()
            L_0x0b83:
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                boolean r0 = r0.isTouched()
                if (r0 != 0) goto L_0x0b90
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                r0.update()
            L_0x0b90:
                return
            L_0x0b91:
                int r0 = r8.plane_currentY
                int r1 = r8.landingZone1Y2
                int r2 = r8.landingZone1Y1
                int r1 = r1 + r2
                int r1 = r1 / 2
                int r1 = r1 + 5
                if (r0 >= r1) goto L_0x0248
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1119092736(0x42b40000, float:90.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getyDirection()
                r1 = -1
                if (r0 != r1) goto L_0x0bbb
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleYDirection()
            L_0x0bbb:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.flgYrun = r1
                r0 = 1
                r8.flgtoggle = r0
                goto L_0x0248
            L_0x0bc5:
                int r0 = r8.p
                r1 = 3
                if (r0 != r1) goto L_0x0bd5
                int r0 = r8.redDirection
                r1 = 3
                if (r0 == r1) goto L_0x0bd5
                r0 = 10
                r8.Arrivalx = r0
                goto L_0x02d3
            L_0x0bd5:
                int r0 = r8.p
                r1 = 1
                if (r0 != r1) goto L_0x0bea
                int r0 = r8.redDirection
                r1 = 1
                if (r0 == r1) goto L_0x0bea
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                int r0 = r0.screen_height
                r1 = 10
                int r0 = r0 - r1
                r8.Arrivaly = r0
                goto L_0x02d3
            L_0x0bea:
                int r0 = r8.p
                if (r0 != 0) goto L_0x0bf8
                int r0 = r8.redDirection
                if (r0 == 0) goto L_0x0bf8
                r0 = 10
                r8.Arrivaly = r0
                goto L_0x02d3
            L_0x0bf8:
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                int r0 = r0.screen_width
                r1 = 10
                int r0 = r0 - r1
                r8.Arrivalx = r0
                goto L_0x02d3
            L_0x0c03:
                int r0 = r8.p
                r1 = 2
                if (r0 != r1) goto L_0x0c17
                int r0 = r8.redDirection
                r1 = 2
                if (r0 == r1) goto L_0x0c17
                int[] r0 = r8.RandomPlane1Y
                int r1 = r8.new1Y
                r0 = r0[r1]
                r8.Arrivaly = r0
                goto L_0x030f
            L_0x0c17:
                int r0 = r8.p
                r1 = 3
                if (r0 != r1) goto L_0x0c2b
                int r0 = r8.redDirection
                r1 = 3
                if (r0 == r1) goto L_0x0c2b
                int[] r0 = r8.RandomPlane1Y
                int r1 = r8.new1Y
                r0 = r0[r1]
                r8.Arrivaly = r0
                goto L_0x030f
            L_0x0c2b:
                int r0 = r8.p
                if (r0 != 0) goto L_0x0c3d
                int r0 = r8.redDirection
                if (r0 == 0) goto L_0x0c3d
                int[] r0 = r8.RandomPlane1X
                int r1 = r8.new1X
                r0 = r0[r1]
                r8.Arrivalx = r0
                goto L_0x030f
            L_0x0c3d:
                int r0 = r8.getHeight()
                int r0 = r0 / 2
                r8.Arrivaly = r0
                goto L_0x030f
            L_0x0c47:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.gamespeed = r1
                goto L_0x0381
            L_0x0c4e:
                int r0 = r8.p
                r1 = 2
                if (r0 != r1) goto L_0x0c90
                int r0 = r8.redDirection
                r1 = 2
                if (r0 == r1) goto L_0x0c90
                com.oziapp.coolLanding.GraphicObject r0 = new com.oziapp.coolLanding.GraphicObject
                android.content.res.Resources r1 = r8.getResources()
                r2 = 2130837613(0x7f02006d, float:1.7280185E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)
                com.oziapp.coolLanding.PlaneActivity r2 = com.oziapp.coolLanding.PlaneActivity.this
                int r2 = r2.screen_width
                int[] r3 = r8.RandomPlane1Y
                int r4 = r8.new1Y
                r3 = r3[r4]
                float r4 = r8.Xv
                float r5 = r8.Yv
                r6 = 1
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r8.yellowPlane = r0
                r0 = 0
                r8.flgtoggle = r0
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                boolean r0 = r0.GameSpeed
                if (r0 == 0) goto L_0x0c89
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.gamespeed = r1
                goto L_0x0389
            L_0x0c89:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.gamespeed = r1
                goto L_0x0389
            L_0x0c90:
                int r0 = r8.p
                r1 = 3
                if (r0 != r1) goto L_0x0ccf
                int r0 = r8.redDirection
                r1 = 3
                if (r0 == r1) goto L_0x0ccf
                com.oziapp.coolLanding.GraphicObject r0 = new com.oziapp.coolLanding.GraphicObject
                android.content.res.Resources r1 = r8.getResources()
                r2 = 2130837613(0x7f02006d, float:1.7280185E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)
                r2 = 10
                int[] r3 = r8.RandomPlane1Y
                int r4 = r8.new1Y
                r3 = r3[r4]
                float r4 = r8.Xv
                float r5 = r8.Yv
                r6 = 1
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r8.yellowPlane = r0
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                boolean r0 = r0.GameSpeed
                if (r0 == 0) goto L_0x0cc9
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.gamespeed = r1
            L_0x0cc4:
                r0 = 0
                r8.flgtoggle = r0
                goto L_0x0389
            L_0x0cc9:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.gamespeed = r1
                goto L_0x0cc4
            L_0x0ccf:
                int r0 = r8.p
                if (r0 != 0) goto L_0x0d18
                int r0 = r8.redDirection
                if (r0 == 0) goto L_0x0d18
                com.oziapp.coolLanding.GraphicObject r0 = new com.oziapp.coolLanding.GraphicObject
                android.content.res.Resources r1 = r8.getResources()
                r2 = 2130837613(0x7f02006d, float:1.7280185E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)
                int[] r2 = r8.RandomPlane1X
                int r3 = r8.new1X
                r2 = r2[r3]
                r3 = 10
                float r4 = r8.Xv
                float r5 = r8.Yv
                r6 = 1
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r8.yellowPlane = r0
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1119092736(0x42b40000, float:90.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                boolean r0 = r0.GameSpeed
                if (r0 == 0) goto L_0x0d12
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.gamespeed = r1
            L_0x0d08:
                r0 = 0
                r8.flgtoggle = r0
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.flgYrun = r1
                goto L_0x0389
            L_0x0d12:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.gamespeed = r1
                goto L_0x0d08
            L_0x0d18:
                com.oziapp.coolLanding.GraphicObject r0 = new com.oziapp.coolLanding.GraphicObject
                android.content.res.Resources r1 = r8.getResources()
                r2 = 2130837613(0x7f02006d, float:1.7280185E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)
                com.oziapp.coolLanding.PlaneActivity r2 = com.oziapp.coolLanding.PlaneActivity.this
                int r2 = r2.screen_width
                com.oziapp.coolLanding.PlaneActivity r3 = com.oziapp.coolLanding.PlaneActivity.this
                int r3 = r3.screen_height
                int r3 = r3 / 2
                int r3 = r3 + 1
                float r4 = r8.Xv
                float r5 = r8.Yv
                r6 = 1
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r8.yellowPlane = r0
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                boolean r0 = r0.GameSpeed
                if (r0 == 0) goto L_0x0d4b
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.gamespeed = r1
            L_0x0d46:
                r0 = 0
                r8.flgtoggle = r0
                goto L_0x0389
            L_0x0d4b:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.gamespeed = r1
                goto L_0x0d46
            L_0x0d51:
                int r0 = com.oziapp.coolLanding.SelectMap.map_Select
                r1 = 2130837572(0x7f020044, float:1.7280102E38)
                if (r0 != r1) goto L_0x116e
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getY()
                int r1 = r8.tempZone1Y1
                int r1 = r1 + 40
                if (r0 >= r1) goto L_0x0dc9
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getX()
                int r1 = r8.tempZone1X1
                int r1 = r1 + 10
                if (r0 >= r1) goto L_0x0f84
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getxDirection()
                r1 = -1
                if (r0 != r1) goto L_0x0d8c
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleXDirection()
            L_0x0d8c:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.flgYrun = r1
            L_0x0d91:
                android.graphics.Path r0 = r8.mPath1
                r0.reset()
                android.graphics.Path r0 = r8.mPath1
                com.oziapp.coolLanding.GraphicObject r1 = r8.yellowPlane
                int r1 = r1.getX()
                float r1 = (float) r1
                com.oziapp.coolLanding.GraphicObject r2 = r8.yellowPlane
                int r2 = r2.getY()
                float r2 = (float) r2
                r0.moveTo(r1, r2)
                android.graphics.Path r0 = r8.mPath1
                int r1 = r8.tempZone1X1
                int r1 = r1 + 20
                float r1 = (float) r1
                com.oziapp.coolLanding.GraphicObject r2 = r8.yellowPlane
                int r2 = r2.getY()
                float r2 = (float) r2
                r0.lineTo(r1, r2)
                android.graphics.Path r0 = r8.mPath1
                int r1 = r8.tempZone1X1
                int r1 = r1 + 20
                float r1 = (float) r1
                int r2 = r8.tempZone1Y1
                int r2 = r2 + 40
                float r2 = (float) r2
                r0.lineTo(r1, r2)
            L_0x0dc9:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getY()
                int r1 = r8.tempZone1Y1
                int r1 = r1 + 40
                if (r0 <= r1) goto L_0x0e40
                android.graphics.Path r0 = r8.mPath1
                r0.reset()
                android.graphics.Path r0 = r8.mPath1
                com.oziapp.coolLanding.GraphicObject r1 = r8.yellowPlane
                int r1 = r1.getX()
                float r1 = (float) r1
                com.oziapp.coolLanding.GraphicObject r2 = r8.yellowPlane
                int r2 = r2.getY()
                float r2 = (float) r2
                r0.moveTo(r1, r2)
                android.graphics.Path r0 = r8.mPath1
                com.oziapp.coolLanding.GraphicObject r1 = r8.yellowPlane
                int r1 = r1.getX()
                float r1 = (float) r1
                int r2 = r8.tempZone1Y1
                int r2 = r2 + 40
                float r2 = (float) r2
                r0.lineTo(r1, r2)
                android.graphics.Path r0 = r8.mPath1
                int r1 = r8.tempZone1X1
                int r1 = r1 + 20
                float r1 = (float) r1
                int r2 = r8.tempZone1Y1
                int r2 = r2 + 40
                float r2 = (float) r2
                r0.lineTo(r1, r2)
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.flgYrun = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getY()
                int r1 = r8.tempZone1Y1
                int r1 = r1 + 40
                if (r0 <= r1) goto L_0x0fb2
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = -1028390912(0xffffffffc2b40000, float:-90.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getyDirection()
                r1 = 1
                if (r0 != r1) goto L_0x0e3b
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleYDirection()
            L_0x0e3b:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.flgYrun = r1
            L_0x0e40:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getX()
                int r1 = r8.tempZone1X1
                int r1 = r1 + 10
                if (r0 < r1) goto L_0x0398
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getX()
                int r1 = r8.tempZone1X1
                int r1 = r1 + 22
                if (r0 > r1) goto L_0x0398
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.flgTouchEnable = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1119092736(0x42b40000, float:90.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getyDirection()
                r1 = -1
                if (r0 != r1) goto L_0x0e7a
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleYDirection()
            L_0x0e7a:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.flgYrun = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getY()
                int r1 = r8.tempZone1Y1
                int r1 = r1 + 35
                if (r0 < r1) goto L_0x0eaa
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getY()
                int r1 = r8.tempZone1Y1
                int r1 = r1 + 45
                if (r0 > r1) goto L_0x0eaa
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.flgLandingMode = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.flgOnSurface = r1
                java.util.Random r0 = r8.rand
                r1 = 4
                int r0 = r0.nextInt(r1)
                r8.p = r0
            L_0x0eaa:
                int r0 = r8.p
                r1 = 2
                if (r0 != r1) goto L_0x0fe2
                int r0 = r8.redDirection
                r1 = 2
                if (r0 == r1) goto L_0x0fe2
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                int r0 = r0.screen_width
                r1 = 10
                int r0 = r0 - r1
                r8.Arrivalx = r0
            L_0x0ebd:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getY()
                int r1 = r8.tempZone1Y1
                int r1 = r1 + 40
                if (r0 <= r1) goto L_0x0f14
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                boolean r0 = r0.flgLandingMode
                if (r0 == 0) goto L_0x0f14
                int r0 = r8.plane2
                int r0 = r0 % 2
                if (r0 != 0) goto L_0x0f01
                java.util.Random r0 = r8.rand
                r1 = 19
                int r0 = r0.nextInt(r1)
                r8.new1Y = r0
                java.util.Random r0 = r8.rand
                r1 = 19
                int r0 = r0.nextInt(r1)
                r8.new1X = r0
                int r0 = r8.p
                r1 = 1
                if (r0 != r1) goto L_0x1020
                int r0 = r8.redDirection
                r1 = 1
                if (r0 == r1) goto L_0x1020
                int[] r0 = r8.RandomPlane1X
                int r1 = r8.new1X
                r0 = r0[r1]
                r8.Arrivalx = r0
            L_0x0efb:
                int r0 = r8.plane2
                int r0 = r0 + 1
                r8.plane2 = r0
            L_0x0f01:
                android.graphics.Path r0 = r8.mPath1
                r0.reset()
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                android.graphics.Bitmap r1 = r8.plane2rightlanding
                r0.setBitmap(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1119092736(0x42b40000, float:90.0)
                r0.setAngle(r1)
            L_0x0f14:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getY()
                int r1 = r8.tempZone1Y1
                int r1 = r1 + 120
                if (r0 == r1) goto L_0x0f2c
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getY()
                int r1 = r8.tempZone1Y1
                int r1 = r1 + 119
                if (r0 != r1) goto L_0x0398
            L_0x0f2c:
                r0 = 0
                r8.flgtouched = r0
                int r0 = r8.score
                int r0 = r0 + 1
                r8.score = r0
                int r0 = r8.p
                r1 = 1
                if (r0 != r1) goto L_0x106b
                int r0 = r8.redDirection
                r1 = 1
                if (r0 == r1) goto L_0x106b
                com.oziapp.coolLanding.GraphicObject r0 = new com.oziapp.coolLanding.GraphicObject
                android.content.res.Resources r1 = r8.getResources()
                r2 = 2130837613(0x7f02006d, float:1.7280185E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)
                int[] r2 = r8.RandomPlane1X
                int r3 = r8.new1X
                r2 = r2[r3]
                com.oziapp.coolLanding.PlaneActivity r3 = com.oziapp.coolLanding.PlaneActivity.this
                int r3 = r3.screen_height
                float r4 = r8.Xv
                float r5 = r8.Yv
                r6 = 1
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r8.yellowPlane = r0
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                boolean r0 = r0.GameSpeed
                if (r0 == 0) goto L_0x1064
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.gamespeed = r1
            L_0x0f6b:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.flgYrun = r1
                r0 = 0
                r8.flgtoggle = r0
            L_0x0f73:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.flgTouchEnable = r1
                int r0 = r8.p
                r8.yellowDirection = r0
                int r0 = r8.plane2
                r1 = 1
                int r0 = r0 - r1
                r8.plane2 = r0
                goto L_0x0398
            L_0x0f84:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getX()
                int r1 = r8.tempZone1X1
                if (r0 <= r1) goto L_0x0d91
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1127481344(0x43340000, float:180.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getxDirection()
                r1 = 1
                if (r0 != r1) goto L_0x0fab
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleXDirection()
            L_0x0fab:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.flgYrun = r1
                goto L_0x0d91
            L_0x0fb2:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getY()
                int r1 = r8.tempZone1Y1
                int r1 = r1 + 40
                if (r0 >= r1) goto L_0x0e40
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1119092736(0x42b40000, float:90.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getyDirection()
                r1 = -1
                if (r0 != r1) goto L_0x0fdb
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleYDirection()
            L_0x0fdb:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.flgYrun = r1
                goto L_0x0e40
            L_0x0fe2:
                int r0 = r8.p
                r1 = 3
                if (r0 != r1) goto L_0x0ff2
                int r0 = r8.redDirection
                r1 = 3
                if (r0 == r1) goto L_0x0ff2
                r0 = 10
                r8.Arrivalx = r0
                goto L_0x0ebd
            L_0x0ff2:
                int r0 = r8.p
                r1 = 1
                if (r0 != r1) goto L_0x1007
                int r0 = r8.redDirection
                r1 = 1
                if (r0 == r1) goto L_0x1007
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                int r0 = r0.screen_height
                r1 = 10
                int r0 = r0 - r1
                r8.Arrivaly = r0
                goto L_0x0ebd
            L_0x1007:
                int r0 = r8.p
                if (r0 != 0) goto L_0x1015
                int r0 = r8.redDirection
                if (r0 == 0) goto L_0x1015
                r0 = 10
                r8.Arrivaly = r0
                goto L_0x0ebd
            L_0x1015:
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                int r0 = r0.screen_width
                r1 = 10
                int r0 = r0 - r1
                r8.Arrivalx = r0
                goto L_0x0ebd
            L_0x1020:
                int r0 = r8.p
                r1 = 2
                if (r0 != r1) goto L_0x1034
                int r0 = r8.redDirection
                r1 = 2
                if (r0 == r1) goto L_0x1034
                int[] r0 = r8.RandomPlane1Y
                int r1 = r8.new1Y
                r0 = r0[r1]
                r8.Arrivaly = r0
                goto L_0x0efb
            L_0x1034:
                int r0 = r8.p
                r1 = 3
                if (r0 != r1) goto L_0x1048
                int r0 = r8.redDirection
                r1 = 3
                if (r0 == r1) goto L_0x1048
                int[] r0 = r8.RandomPlane1Y
                int r1 = r8.new1Y
                r0 = r0[r1]
                r8.Arrivaly = r0
                goto L_0x0efb
            L_0x1048:
                int r0 = r8.p
                if (r0 != 0) goto L_0x105a
                int r0 = r8.redDirection
                if (r0 == 0) goto L_0x105a
                int[] r0 = r8.RandomPlane1X
                int r1 = r8.new1X
                r0 = r0[r1]
                r8.Arrivalx = r0
                goto L_0x0efb
            L_0x105a:
                int r0 = r8.getHeight()
                int r0 = r0 / 2
                r8.Arrivaly = r0
                goto L_0x0efb
            L_0x1064:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.gamespeed = r1
                goto L_0x0f6b
            L_0x106b:
                int r0 = r8.p
                r1 = 2
                if (r0 != r1) goto L_0x10ad
                int r0 = r8.redDirection
                r1 = 2
                if (r0 == r1) goto L_0x10ad
                com.oziapp.coolLanding.GraphicObject r0 = new com.oziapp.coolLanding.GraphicObject
                android.content.res.Resources r1 = r8.getResources()
                r2 = 2130837613(0x7f02006d, float:1.7280185E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)
                com.oziapp.coolLanding.PlaneActivity r2 = com.oziapp.coolLanding.PlaneActivity.this
                int r2 = r2.screen_width
                int[] r3 = r8.RandomPlane1Y
                int r4 = r8.new1Y
                r3 = r3[r4]
                float r4 = r8.Xv
                float r5 = r8.Yv
                r6 = 1
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r8.yellowPlane = r0
                r0 = 0
                r8.flgtoggle = r0
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                boolean r0 = r0.GameSpeed
                if (r0 == 0) goto L_0x10a6
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.gamespeed = r1
                goto L_0x0f73
            L_0x10a6:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.gamespeed = r1
                goto L_0x0f73
            L_0x10ad:
                int r0 = r8.p
                r1 = 3
                if (r0 != r1) goto L_0x10ec
                int r0 = r8.redDirection
                r1 = 3
                if (r0 == r1) goto L_0x10ec
                com.oziapp.coolLanding.GraphicObject r0 = new com.oziapp.coolLanding.GraphicObject
                android.content.res.Resources r1 = r8.getResources()
                r2 = 2130837613(0x7f02006d, float:1.7280185E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)
                r2 = 10
                int[] r3 = r8.RandomPlane1Y
                int r4 = r8.new1Y
                r3 = r3[r4]
                float r4 = r8.Xv
                float r5 = r8.Yv
                r6 = 1
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r8.yellowPlane = r0
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                boolean r0 = r0.GameSpeed
                if (r0 == 0) goto L_0x10e6
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.gamespeed = r1
            L_0x10e1:
                r0 = 0
                r8.flgtoggle = r0
                goto L_0x0f73
            L_0x10e6:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.gamespeed = r1
                goto L_0x10e1
            L_0x10ec:
                int r0 = r8.p
                if (r0 != 0) goto L_0x1135
                int r0 = r8.redDirection
                if (r0 == 0) goto L_0x1135
                com.oziapp.coolLanding.GraphicObject r0 = new com.oziapp.coolLanding.GraphicObject
                android.content.res.Resources r1 = r8.getResources()
                r2 = 2130837613(0x7f02006d, float:1.7280185E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)
                int[] r2 = r8.RandomPlane1X
                int r3 = r8.new1X
                r2 = r2[r3]
                r3 = 10
                float r4 = r8.Xv
                float r5 = r8.Yv
                r6 = 1
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r8.yellowPlane = r0
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1119092736(0x42b40000, float:90.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                boolean r0 = r0.GameSpeed
                if (r0 == 0) goto L_0x112f
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.gamespeed = r1
            L_0x1125:
                r0 = 0
                r8.flgtoggle = r0
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.flgYrun = r1
                goto L_0x0f73
            L_0x112f:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.gamespeed = r1
                goto L_0x1125
            L_0x1135:
                com.oziapp.coolLanding.GraphicObject r0 = new com.oziapp.coolLanding.GraphicObject
                android.content.res.Resources r1 = r8.getResources()
                r2 = 2130837613(0x7f02006d, float:1.7280185E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)
                com.oziapp.coolLanding.PlaneActivity r2 = com.oziapp.coolLanding.PlaneActivity.this
                int r2 = r2.screen_width
                com.oziapp.coolLanding.PlaneActivity r3 = com.oziapp.coolLanding.PlaneActivity.this
                int r3 = r3.screen_height
                int r3 = r3 / 2
                int r3 = r3 + 1
                float r4 = r8.Xv
                float r5 = r8.Yv
                r6 = 1
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r8.yellowPlane = r0
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                boolean r0 = r0.GameSpeed
                if (r0 == 0) goto L_0x1168
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.gamespeed = r1
            L_0x1163:
                r0 = 0
                r8.flgtoggle = r0
                goto L_0x0f73
            L_0x1168:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.gamespeed = r1
                goto L_0x1163
            L_0x116e:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getxDirection()
                r1 = 1
                if (r0 != r1) goto L_0x1184
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleXDirection()
            L_0x1184:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getX()
                r8.plane_currentX = r0
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getY()
                r8.plane_currentY = r0
                int r0 = r8.plane_currentX
                int r1 = r8.landingZone1X1
                if (r0 <= r1) goto L_0x11a6
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1127481344(0x43340000, float:180.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.flgYrun = r1
            L_0x11a6:
                android.graphics.Path r0 = r8.mPath1
                r0.reset()
                android.graphics.Path r0 = r8.mPath1
                com.oziapp.coolLanding.GraphicObject r1 = r8.yellowPlane
                int r1 = r1.getX()
                float r1 = (float) r1
                com.oziapp.coolLanding.GraphicObject r2 = r8.yellowPlane
                int r2 = r2.getY()
                float r2 = (float) r2
                r0.moveTo(r1, r2)
                android.graphics.Path r0 = r8.mPath1
                int r1 = r8.landingZone1X1
                r2 = 5
                int r1 = r1 - r2
                float r1 = (float) r1
                com.oziapp.coolLanding.GraphicObject r2 = r8.yellowPlane
                int r2 = r2.getY()
                float r2 = (float) r2
                r0.lineTo(r1, r2)
                android.graphics.Path r0 = r8.mPath1
                int r1 = r8.landingZone1X1
                r2 = 5
                int r1 = r1 - r2
                float r1 = (float) r1
                int r2 = r8.landingZone1Y2
                int r3 = r8.landingZone1Y1
                int r2 = r2 + r3
                int r2 = r2 / 2
                float r2 = (float) r2
                r0.lineTo(r1, r2)
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getX()
                int r1 = r8.landingZone1X1
                r2 = 5
                int r1 = r1 - r2
                if (r0 >= r1) goto L_0x125d
                android.graphics.Path r0 = r8.mPath1
                r0.reset()
                android.graphics.Path r0 = r8.mPath1
                com.oziapp.coolLanding.GraphicObject r1 = r8.yellowPlane
                int r1 = r1.getX()
                float r1 = (float) r1
                com.oziapp.coolLanding.GraphicObject r2 = r8.yellowPlane
                int r2 = r2.getY()
                float r2 = (float) r2
                r0.moveTo(r1, r2)
                android.graphics.Path r0 = r8.mPath1
                com.oziapp.coolLanding.GraphicObject r1 = r8.yellowPlane
                int r1 = r1.getX()
                float r1 = (float) r1
                int r2 = r8.landingZone1Y2
                int r3 = r8.landingZone1Y1
                int r2 = r2 + r3
                int r2 = r2 / 2
                float r2 = (float) r2
                r0.lineTo(r1, r2)
                android.graphics.Path r0 = r8.mPath1
                int r1 = r8.landingZone1X1
                r2 = 5
                int r1 = r1 - r2
                float r1 = (float) r1
                int r2 = r8.landingZone1Y2
                int r3 = r8.landingZone1Y1
                int r2 = r2 + r3
                int r2 = r2 / 2
                float r2 = (float) r2
                r0.lineTo(r1, r2)
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.flgYrun = r1
                int r0 = r8.plane_currentY
                int r1 = r8.landingZone1Y2
                int r2 = r8.landingZone1Y1
                int r1 = r1 + r2
                int r1 = r1 / 2
                if (r0 <= r1) goto L_0x13ae
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = -1028390912(0xffffffffc2b40000, float:-90.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getyDirection()
                r1 = 1
                if (r0 != r1) goto L_0x1258
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleYDirection()
            L_0x1258:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.flgYrun = r1
            L_0x125d:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getY()
                int r1 = r8.landingZone1Y2
                int r2 = r8.landingZone1Y1
                int r1 = r1 + r2
                int r1 = r1 / 2
                if (r0 == r1) goto L_0x127d
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getY()
                int r1 = r8.landingZone1Y2
                int r2 = r8.landingZone1Y1
                int r1 = r1 + r2
                int r1 = r1 / 2
                r2 = 1
                int r1 = r1 - r2
                if (r0 != r1) goto L_0x0398
            L_0x127d:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.flgTouchEnable = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getxDirection()
                r1 = -1
                if (r0 != r1) goto L_0x129e
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleXDirection()
            L_0x129e:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.flgYrun = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getX()
                int r1 = r8.landingZone1X1
                int r1 = r1 + 6
                if (r0 == r1) goto L_0x12bb
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getX()
                int r1 = r8.landingZone1X1
                int r1 = r1 + 7
                if (r0 != r1) goto L_0x12ce
            L_0x12bb:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.flgLandingMode = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.flgOnSurface = r1
                java.util.Random r0 = r8.rand
                r1 = 4
                int r0 = r0.nextInt(r1)
                r8.p = r0
            L_0x12ce:
                int r0 = r8.p
                r1 = 2
                if (r0 != r1) goto L_0x13dd
                int r0 = r8.redDirection
                r1 = 2
                if (r0 == r1) goto L_0x13dd
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                int r0 = r0.screen_width
                r1 = 10
                int r0 = r0 - r1
                r8.Arrivalx = r0
            L_0x12e1:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getX()
                int r1 = r8.landingZone1X1
                int r1 = r1 + 5
                if (r0 <= r1) goto L_0x133f
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                boolean r0 = r0.flgLandingMode
                if (r0 == 0) goto L_0x133f
                int r0 = r8.plane2
                int r0 = r0 % 2
                if (r0 != 0) goto L_0x1325
                java.util.Random r0 = r8.rand
                r1 = 19
                int r0 = r0.nextInt(r1)
                r8.new1Y = r0
                java.util.Random r0 = r8.rand
                r1 = 19
                int r0 = r0.nextInt(r1)
                r8.new1X = r0
                int r0 = r8.p
                r1 = 1
                if (r0 != r1) goto L_0x141b
                int r0 = r8.redDirection
                r1 = 1
                if (r0 == r1) goto L_0x141b
                int[] r0 = r8.RandomPlane1X
                int r1 = r8.new1X
                r0 = r0[r1]
                r8.Arrivalx = r0
            L_0x131f:
                int r0 = r8.plane2
                int r0 = r0 + 1
                r8.plane2 = r0
            L_0x1325:
                android.graphics.Path r0 = r8.mPath1
                r0.reset()
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                android.graphics.Bitmap r1 = r8.plane2rightlanding
                r0.setBitmap(r1)
                int r0 = com.oziapp.coolLanding.SelectMap.map_Select
                r1 = 2130837575(0x7f020047, float:1.7280108E38)
                if (r0 != r1) goto L_0x133f
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1127481344(0x43340000, float:180.0)
                r0.setAngle(r1)
            L_0x133f:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getX()
                int r1 = r8.landingZone1X2
                r2 = 40
                int r1 = r1 - r2
                if (r0 == r1) goto L_0x1359
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                int r0 = r0.getX()
                int r1 = r8.landingZone1X2
                r2 = 39
                int r1 = r1 - r2
                if (r0 != r1) goto L_0x0398
            L_0x1359:
                r0 = 0
                r8.flgtouched = r0
                int r0 = r8.score
                int r0 = r0 + 1
                r8.score = r0
                int r0 = r8.p
                r1 = 1
                if (r0 != r1) goto L_0x1466
                int r0 = r8.redDirection
                r1 = 1
                if (r0 == r1) goto L_0x1466
                com.oziapp.coolLanding.GraphicObject r0 = new com.oziapp.coolLanding.GraphicObject
                android.content.res.Resources r1 = r8.getResources()
                r2 = 2130837613(0x7f02006d, float:1.7280185E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)
                int[] r2 = r8.RandomPlane1X
                int r3 = r8.new1X
                r2 = r2[r3]
                com.oziapp.coolLanding.PlaneActivity r3 = com.oziapp.coolLanding.PlaneActivity.this
                int r3 = r3.screen_height
                float r4 = r8.Xv
                float r5 = r8.Yv
                r6 = 1
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r8.yellowPlane = r0
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                boolean r0 = r0.GameSpeed
                if (r0 == 0) goto L_0x145f
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.gamespeed = r1
            L_0x1398:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.flgYrun = r1
            L_0x139d:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.flgTouchEnable = r1
                int r0 = r8.p
                r8.yellowDirection = r0
                int r0 = r8.plane2
                r1 = 1
                int r0 = r0 - r1
                r8.plane2 = r0
                goto L_0x0398
            L_0x13ae:
                int r0 = r8.plane_currentY
                int r1 = r8.landingZone1Y2
                int r2 = r8.landingZone1Y1
                int r1 = r1 + r2
                int r1 = r1 / 2
                if (r0 >= r1) goto L_0x125d
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1119092736(0x42b40000, float:90.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getyDirection()
                r1 = -1
                if (r0 != r1) goto L_0x13d6
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleYDirection()
            L_0x13d6:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.flgYrun = r1
                goto L_0x125d
            L_0x13dd:
                int r0 = r8.p
                r1 = 3
                if (r0 != r1) goto L_0x13ed
                int r0 = r8.redDirection
                r1 = 3
                if (r0 == r1) goto L_0x13ed
                r0 = 10
                r8.Arrivalx = r0
                goto L_0x12e1
            L_0x13ed:
                int r0 = r8.p
                r1 = 1
                if (r0 != r1) goto L_0x1402
                int r0 = r8.redDirection
                r1 = 1
                if (r0 == r1) goto L_0x1402
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                int r0 = r0.screen_height
                r1 = 10
                int r0 = r0 - r1
                r8.Arrivaly = r0
                goto L_0x12e1
            L_0x1402:
                int r0 = r8.p
                if (r0 != 0) goto L_0x1410
                int r0 = r8.redDirection
                if (r0 == 0) goto L_0x1410
                r0 = 10
                r8.Arrivaly = r0
                goto L_0x12e1
            L_0x1410:
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                int r0 = r0.screen_width
                r1 = 10
                int r0 = r0 - r1
                r8.Arrivalx = r0
                goto L_0x12e1
            L_0x141b:
                int r0 = r8.p
                r1 = 2
                if (r0 != r1) goto L_0x142f
                int r0 = r8.redDirection
                r1 = 2
                if (r0 == r1) goto L_0x142f
                int[] r0 = r8.RandomPlane1Y
                int r1 = r8.new1Y
                r0 = r0[r1]
                r8.Arrivaly = r0
                goto L_0x131f
            L_0x142f:
                int r0 = r8.p
                r1 = 3
                if (r0 != r1) goto L_0x1443
                int r0 = r8.redDirection
                r1 = 3
                if (r0 == r1) goto L_0x1443
                int[] r0 = r8.RandomPlane1Y
                int r1 = r8.new1Y
                r0 = r0[r1]
                r8.Arrivaly = r0
                goto L_0x131f
            L_0x1443:
                int r0 = r8.p
                if (r0 != 0) goto L_0x1455
                int r0 = r8.redDirection
                if (r0 == 0) goto L_0x1455
                int[] r0 = r8.RandomPlane1X
                int r1 = r8.new1X
                r0 = r0[r1]
                r8.Arrivalx = r0
                goto L_0x131f
            L_0x1455:
                int r0 = r8.getHeight()
                int r0 = r0 / 2
                r8.Arrivaly = r0
                goto L_0x131f
            L_0x145f:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.gamespeed = r1
                goto L_0x1398
            L_0x1466:
                int r0 = r8.p
                r1 = 2
                if (r0 != r1) goto L_0x14a5
                int r0 = r8.redDirection
                r1 = 2
                if (r0 == r1) goto L_0x14a5
                com.oziapp.coolLanding.GraphicObject r0 = new com.oziapp.coolLanding.GraphicObject
                android.content.res.Resources r1 = r8.getResources()
                r2 = 2130837613(0x7f02006d, float:1.7280185E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)
                com.oziapp.coolLanding.PlaneActivity r2 = com.oziapp.coolLanding.PlaneActivity.this
                int r2 = r2.screen_width
                int[] r3 = r8.RandomPlane1Y
                int r4 = r8.new1Y
                r3 = r3[r4]
                float r4 = r8.Xv
                float r5 = r8.Yv
                r6 = 1
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r8.yellowPlane = r0
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                boolean r0 = r0.GameSpeed
                if (r0 == 0) goto L_0x149e
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.gamespeed = r1
                goto L_0x139d
            L_0x149e:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.gamespeed = r1
                goto L_0x139d
            L_0x14a5:
                int r0 = r8.p
                r1 = 3
                if (r0 != r1) goto L_0x14e2
                int r0 = r8.redDirection
                r1 = 3
                if (r0 == r1) goto L_0x14e2
                com.oziapp.coolLanding.GraphicObject r0 = new com.oziapp.coolLanding.GraphicObject
                android.content.res.Resources r1 = r8.getResources()
                r2 = 2130837613(0x7f02006d, float:1.7280185E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)
                r2 = 10
                int[] r3 = r8.RandomPlane1Y
                int r4 = r8.new1Y
                r3 = r3[r4]
                float r4 = r8.Xv
                float r5 = r8.Yv
                r6 = 1
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r8.yellowPlane = r0
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                boolean r0 = r0.GameSpeed
                if (r0 == 0) goto L_0x14db
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.gamespeed = r1
                goto L_0x139d
            L_0x14db:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.gamespeed = r1
                goto L_0x139d
            L_0x14e2:
                int r0 = r8.p
                if (r0 != 0) goto L_0x1528
                int r0 = r8.redDirection
                if (r0 == 0) goto L_0x1528
                com.oziapp.coolLanding.GraphicObject r0 = new com.oziapp.coolLanding.GraphicObject
                android.content.res.Resources r1 = r8.getResources()
                r2 = 2130837613(0x7f02006d, float:1.7280185E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)
                int[] r2 = r8.RandomPlane1X
                int r3 = r8.new1X
                r2 = r2[r3]
                r3 = 10
                float r4 = r8.Xv
                float r5 = r8.Yv
                r6 = 1
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r8.yellowPlane = r0
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1119092736(0x42b40000, float:90.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                boolean r0 = r0.GameSpeed
                if (r0 == 0) goto L_0x1522
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.gamespeed = r1
            L_0x151b:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.flgYrun = r1
                goto L_0x139d
            L_0x1522:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.gamespeed = r1
                goto L_0x151b
            L_0x1528:
                com.oziapp.coolLanding.GraphicObject r0 = new com.oziapp.coolLanding.GraphicObject
                android.content.res.Resources r1 = r8.getResources()
                r2 = 2130837613(0x7f02006d, float:1.7280185E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)
                com.oziapp.coolLanding.PlaneActivity r2 = com.oziapp.coolLanding.PlaneActivity.this
                int r2 = r2.screen_width
                com.oziapp.coolLanding.PlaneActivity r3 = com.oziapp.coolLanding.PlaneActivity.this
                int r3 = r3.screen_height
                int r3 = r3 / 2
                int r3 = r3 + 1
                float r4 = r8.Xv
                float r5 = r8.Yv
                r6 = 1
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r8.yellowPlane = r0
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                boolean r0 = r0.GameSpeed
                if (r0 == 0) goto L_0x1558
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.gamespeed = r1
                goto L_0x139d
            L_0x1558:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1
                r0.gamespeed = r1
                goto L_0x139d
            L_0x155f:
                int r0 = r8.plane_currentX2
                int r1 = r8.tempZone2X1
                if (r0 <= r1) goto L_0x03e8
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1127481344(0x43340000, float:180.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getxDirection()
                r1 = 1
                if (r0 != r1) goto L_0x1582
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleXDirection()
            L_0x1582:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.flgYrun = r1
                goto L_0x03e8
            L_0x1589:
                int r0 = r8.p2
                r1 = 3
                if (r0 != r1) goto L_0x1599
                int r0 = r8.yellowDirection
                r1 = 3
                if (r0 == r1) goto L_0x1599
                r0 = 10
                r8.Arrival2x = r0
                goto L_0x0531
            L_0x1599:
                int r0 = r8.p2
                r1 = 1
                if (r0 != r1) goto L_0x15ae
                int r0 = r8.yellowDirection
                r1 = 1
                if (r0 == r1) goto L_0x15ae
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                int r0 = r0.screen_height
                r1 = 10
                int r0 = r0 - r1
                r8.Arrival2y = r0
                goto L_0x0531
            L_0x15ae:
                int r0 = r8.p2
                if (r0 != 0) goto L_0x15bc
                int r0 = r8.yellowDirection
                if (r0 == 0) goto L_0x15bc
                r0 = 10
                r8.Arrival2y = r0
                goto L_0x0531
            L_0x15bc:
                r0 = 10
                r8.Arrival2x = r0
                goto L_0x0531
            L_0x15c2:
                int r0 = r8.p2
                r1 = 2
                if (r0 != r1) goto L_0x15d6
                int r0 = r8.yellowDirection
                r1 = 2
                if (r0 == r1) goto L_0x15d6
                int[] r0 = r8.RandomPlane2Y
                int r1 = r8.new2Y
                r0 = r0[r1]
                r8.Arrival2y = r0
                goto L_0x055d
            L_0x15d6:
                int r0 = r8.p2
                r1 = 3
                if (r0 != r1) goto L_0x15ea
                int r0 = r8.yellowDirection
                r1 = 3
                if (r0 == r1) goto L_0x15ea
                int[] r0 = r8.RandomPlane2Y
                int r1 = r8.new2Y
                r0 = r0[r1]
                r8.Arrival2y = r0
                goto L_0x055d
            L_0x15ea:
                int r0 = r8.p2
                if (r0 != 0) goto L_0x15fc
                int r0 = r8.yellowDirection
                if (r0 == 0) goto L_0x15fc
                int[] r0 = r8.RandomPlane2X
                int r1 = r8.new2X
                r0 = r0[r1]
                r8.Arrival2x = r0
                goto L_0x055d
            L_0x15fc:
                int r0 = r8.getHeight()
                int r0 = r0 / 4
                r8.Arrival2y = r0
                goto L_0x055d
            L_0x1606:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.gamespeed = r1
                goto L_0x05df
            L_0x160d:
                int r0 = r8.p2
                r1 = 2
                if (r0 != r1) goto L_0x164c
                int r0 = r8.yellowDirection
                r1 = 2
                if (r0 == r1) goto L_0x164c
                com.oziapp.coolLanding.GraphicObject r0 = new com.oziapp.coolLanding.GraphicObject
                android.content.res.Resources r1 = r8.getResources()
                r2 = 2130837610(0x7f02006a, float:1.7280179E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)
                com.oziapp.coolLanding.PlaneActivity r2 = com.oziapp.coolLanding.PlaneActivity.this
                int r2 = r2.screen_width
                int[] r3 = r8.RandomPlane2Y
                int r4 = r8.new2Y
                r3 = r3[r4]
                float r4 = r8.Xv
                float r5 = r8.Yv
                r6 = 2
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r8.redPlane = r0
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                boolean r0 = r0.GameSpeed
                if (r0 == 0) goto L_0x1645
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.gamespeed = r1
                goto L_0x05e4
            L_0x1645:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.gamespeed = r1
                goto L_0x05e4
            L_0x164c:
                int r0 = r8.p2
                r1 = 3
                if (r0 != r1) goto L_0x1689
                int r0 = r8.yellowDirection
                r1 = 3
                if (r0 == r1) goto L_0x1689
                com.oziapp.coolLanding.GraphicObject r0 = new com.oziapp.coolLanding.GraphicObject
                android.content.res.Resources r1 = r8.getResources()
                r2 = 2130837610(0x7f02006a, float:1.7280179E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)
                r2 = 10
                int[] r3 = r8.RandomPlane2Y
                int r4 = r8.new2Y
                r3 = r3[r4]
                float r4 = r8.Xv
                float r5 = r8.Yv
                r6 = 2
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r8.redPlane = r0
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                boolean r0 = r0.GameSpeed
                if (r0 == 0) goto L_0x1682
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.gamespeed = r1
                goto L_0x05e4
            L_0x1682:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.gamespeed = r1
                goto L_0x05e4
            L_0x1689:
                int r0 = r8.p2
                if (r0 != 0) goto L_0x16cf
                int r0 = r8.yellowDirection
                if (r0 == 0) goto L_0x16cf
                com.oziapp.coolLanding.GraphicObject r0 = new com.oziapp.coolLanding.GraphicObject
                android.content.res.Resources r1 = r8.getResources()
                r2 = 2130837610(0x7f02006a, float:1.7280179E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)
                int[] r2 = r8.RandomPlane2X
                int r3 = r8.new2X
                r2 = r2[r3]
                r3 = 10
                float r4 = r8.Xv
                float r5 = r8.Yv
                r6 = 2
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r8.redPlane = r0
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1119092736(0x42b40000, float:90.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                boolean r0 = r0.GameSpeed
                if (r0 == 0) goto L_0x16c9
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.gamespeed = r1
            L_0x16c2:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.flgYrun = r1
                goto L_0x05e4
            L_0x16c9:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.gamespeed = r1
                goto L_0x16c2
            L_0x16cf:
                com.oziapp.coolLanding.GraphicObject r0 = new com.oziapp.coolLanding.GraphicObject
                android.content.res.Resources r1 = r8.getResources()
                r2 = 2130837610(0x7f02006a, float:1.7280179E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)
                r2 = 10
                com.oziapp.coolLanding.PlaneActivity r3 = com.oziapp.coolLanding.PlaneActivity.this
                int r3 = r3.screen_height
                int r3 = r3 / 4
                float r4 = r8.Xv
                float r5 = r8.Yv
                r6 = 2
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r8.redPlane = r0
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                boolean r0 = r0.GameSpeed
                if (r0 == 0) goto L_0x16fb
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.gamespeed = r1
                goto L_0x05e4
            L_0x16fb:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.gamespeed = r1
                goto L_0x05e4
            L_0x1702:
                int r0 = com.oziapp.coolLanding.SelectMap.map_Select
                r1 = 2130837569(0x7f020041, float:1.7280096E38)
                if (r0 != r1) goto L_0x1afd
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getX()
                r8.plane_currentX2 = r0
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getY()
                r8.plane_currentY2 = r0
                int r0 = r8.plane_currentY2
                int r1 = r8.tempZone2Y2
                if (r0 <= r1) goto L_0x177e
                int r0 = r8.plane_currentX2
                int r1 = r8.tempZone2X1
                if (r0 >= r1) goto L_0x195a
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getxDirection()
                r1 = -1
                if (r0 != r1) goto L_0x1741
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleXDirection()
            L_0x1741:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.flgYrun = r1
            L_0x1746:
                android.graphics.Path r0 = r8.mPath2
                r0.reset()
                android.graphics.Path r0 = r8.mPath2
                com.oziapp.coolLanding.GraphicObject r1 = r8.redPlane
                int r1 = r1.getX()
                float r1 = (float) r1
                com.oziapp.coolLanding.GraphicObject r2 = r8.redPlane
                int r2 = r2.getY()
                float r2 = (float) r2
                r0.moveTo(r1, r2)
                android.graphics.Path r0 = r8.mPath2
                int r1 = r8.tempZone2X1
                int r1 = r1 + 25
                float r1 = (float) r1
                com.oziapp.coolLanding.GraphicObject r2 = r8.redPlane
                int r2 = r2.getY()
                float r2 = (float) r2
                r0.lineTo(r1, r2)
                android.graphics.Path r0 = r8.mPath2
                int r1 = r8.tempZone2X1
                int r1 = r1 + 25
                float r1 = (float) r1
                int r2 = r8.tempZone2Y2
                int r2 = r2 + 5
                float r2 = (float) r2
                r0.lineTo(r1, r2)
            L_0x177e:
                int r0 = r8.plane_currentY2
                int r1 = r8.tempZone2Y2
                if (r0 >= r1) goto L_0x1811
                android.graphics.Path r0 = r8.mPath2
                r0.reset()
                android.graphics.Path r0 = r8.mPath2
                com.oziapp.coolLanding.GraphicObject r1 = r8.redPlane
                int r1 = r1.getX()
                float r1 = (float) r1
                com.oziapp.coolLanding.GraphicObject r2 = r8.redPlane
                int r2 = r2.getY()
                float r2 = (float) r2
                r0.moveTo(r1, r2)
                android.graphics.Path r0 = r8.mPath2
                com.oziapp.coolLanding.GraphicObject r1 = r8.redPlane
                int r1 = r1.getX()
                float r1 = (float) r1
                int r2 = r8.tempZone2Y2
                int r2 = r2 + 5
                float r2 = (float) r2
                r0.lineTo(r1, r2)
                android.graphics.Path r0 = r8.mPath2
                int r1 = r8.tempZone2X1
                int r1 = r1 + 25
                float r1 = (float) r1
                int r2 = r8.tempZone2Y2
                int r2 = r2 + 5
                float r2 = (float) r2
                r0.lineTo(r1, r2)
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.flgYrun = r1
                int r0 = r8.plane_currentY2
                int r1 = r8.tempZone2Y2
                if (r0 >= r1) goto L_0x17e9
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1119092736(0x42b40000, float:90.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getyDirection()
                r1 = -1
                if (r0 != r1) goto L_0x17e4
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleYDirection()
            L_0x17e4:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.flgYrun = r1
            L_0x17e9:
                int r0 = r8.plane_currentY2
                int r1 = r8.tempZone2Y2
                if (r0 <= r1) goto L_0x1811
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = -1028390912(0xffffffffc2b40000, float:-90.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getyDirection()
                r1 = 1
                if (r0 != r1) goto L_0x180c
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleYDirection()
            L_0x180c:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.flgYrun = r1
            L_0x1811:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getX()
                int r1 = r8.tempZone2X1
                int r1 = r1 + 20
                if (r0 < r1) goto L_0x05f3
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getX()
                int r1 = r8.tempZone2X1
                int r1 = r1 + 30
                if (r0 > r1) goto L_0x05f3
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.flgYrun = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.flgTouchEnable = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getyDirection()
                r1 = 1
                if (r0 != r1) goto L_0x1849
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleYDirection()
            L_0x1849:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = -1028390912(0xffffffffc2b40000, float:-90.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getY()
                int r1 = r8.tempZone2Y2
                if (r0 > r1) goto L_0x1879
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getY()
                int r1 = r8.tempZone2Y2
                r2 = 1
                int r1 = r1 - r2
                if (r0 > r1) goto L_0x1879
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.flgLandingMode = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.flgOnSurface = r1
                java.util.Random r0 = r8.rand
                r1 = 4
                int r0 = r0.nextInt(r1)
                r8.p2 = r0
            L_0x1879:
                int r0 = r8.p2
                r1 = 2
                if (r0 != r1) goto L_0x1984
                int r0 = r8.yellowDirection
                r1 = 2
                if (r0 == r1) goto L_0x1984
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                int r0 = r0.screen_width
                r1 = 10
                int r0 = r0 - r1
                r8.Arrival2x = r0
            L_0x188c:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getY()
                int r1 = r8.tempZone2Y2
                if (r0 >= r1) goto L_0x18eb
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                boolean r0 = r0.flgLandingMode
                if (r0 == 0) goto L_0x18eb
                int r0 = r8.plane1
                int r0 = r0 % 2
                if (r0 != 0) goto L_0x18ce
                java.util.Random r0 = r8.rand
                r1 = 19
                int r0 = r0.nextInt(r1)
                r8.new2Y = r0
                java.util.Random r0 = r8.rand
                r1 = 19
                int r0 = r0.nextInt(r1)
                r8.new2X = r0
                int r0 = r8.p2
                r1 = 1
                if (r0 != r1) goto L_0x19bd
                int r0 = r8.yellowDirection
                r1 = 1
                if (r0 == r1) goto L_0x19bd
                int[] r0 = r8.RandomPlane2X
                int r1 = r8.new2X
                r0 = r0[r1]
                r8.Arrival2x = r0
            L_0x18c8:
                int r0 = r8.plane1
                int r0 = r0 + 1
                r8.plane1 = r0
            L_0x18ce:
                android.graphics.Path r0 = r8.mPath2
                r0.reset()
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.flgLandingMode = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.flgOnSurface = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                android.graphics.Bitmap r1 = r8.plane1rightlanding
                r0.setBitmap(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = -1028390912(0xffffffffc2b40000, float:-90.0)
                r0.setAngle(r1)
            L_0x18eb:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getY()
                int r1 = r8.tempZone2Y2
                r2 = 100
                int r1 = r1 - r2
                if (r0 == r1) goto L_0x1905
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getY()
                int r1 = r8.tempZone2Y2
                r2 = 101(0x65, float:1.42E-43)
                int r1 = r1 - r2
                if (r0 != r1) goto L_0x05f3
            L_0x1905:
                r0 = 0
                r8.flgtouched2 = r0
                int r0 = r8.score
                int r0 = r0 + 1
                r8.score = r0
                int r0 = r8.p2
                r1 = 1
                if (r0 != r1) goto L_0x1a08
                int r0 = r8.yellowDirection
                r1 = 1
                if (r0 == r1) goto L_0x1a08
                com.oziapp.coolLanding.GraphicObject r0 = new com.oziapp.coolLanding.GraphicObject
                android.content.res.Resources r1 = r8.getResources()
                r2 = 2130837610(0x7f02006a, float:1.7280179E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)
                int[] r2 = r8.RandomPlane2X
                int r3 = r8.new2X
                r2 = r2[r3]
                com.oziapp.coolLanding.PlaneActivity r3 = com.oziapp.coolLanding.PlaneActivity.this
                int r3 = r3.screen_height
                float r4 = r8.Xv
                float r5 = r8.Yv
                r6 = 2
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r8.redPlane = r0
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                boolean r0 = r0.GameSpeed
                if (r0 == 0) goto L_0x1a01
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.gamespeed = r1
            L_0x1944:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.flgYrun = r1
            L_0x1949:
                int r0 = r8.p2
                r8.redDirection = r0
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.flgTouchEnable = r1
                int r0 = r8.plane1
                r1 = 1
                int r0 = r0 - r1
                r8.plane1 = r0
                goto L_0x05f3
            L_0x195a:
                int r0 = r8.plane_currentX2
                int r1 = r8.tempZone2X2
                if (r0 <= r1) goto L_0x1746
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1127481344(0x43340000, float:180.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getxDirection()
                r1 = 1
                if (r0 != r1) goto L_0x197d
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleXDirection()
            L_0x197d:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.flgYrun = r1
                goto L_0x1746
            L_0x1984:
                int r0 = r8.p2
                r1 = 3
                if (r0 != r1) goto L_0x1994
                int r0 = r8.yellowDirection
                r1 = 3
                if (r0 == r1) goto L_0x1994
                r0 = 10
                r8.Arrival2x = r0
                goto L_0x188c
            L_0x1994:
                int r0 = r8.p2
                r1 = 1
                if (r0 != r1) goto L_0x19a9
                int r0 = r8.yellowDirection
                r1 = 1
                if (r0 == r1) goto L_0x19a9
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                int r0 = r0.screen_height
                r1 = 10
                int r0 = r0 - r1
                r8.Arrival2y = r0
                goto L_0x188c
            L_0x19a9:
                int r0 = r8.p2
                if (r0 != 0) goto L_0x19b7
                int r0 = r8.yellowDirection
                if (r0 == 0) goto L_0x19b7
                r0 = 10
                r8.Arrival2y = r0
                goto L_0x188c
            L_0x19b7:
                r0 = 10
                r8.Arrival2x = r0
                goto L_0x188c
            L_0x19bd:
                int r0 = r8.p2
                r1 = 2
                if (r0 != r1) goto L_0x19d1
                int r0 = r8.yellowDirection
                r1 = 2
                if (r0 == r1) goto L_0x19d1
                int[] r0 = r8.RandomPlane2Y
                int r1 = r8.new2Y
                r0 = r0[r1]
                r8.Arrival2y = r0
                goto L_0x18c8
            L_0x19d1:
                int r0 = r8.p2
                r1 = 3
                if (r0 != r1) goto L_0x19e5
                int r0 = r8.yellowDirection
                r1 = 3
                if (r0 == r1) goto L_0x19e5
                int[] r0 = r8.RandomPlane2Y
                int r1 = r8.new2Y
                r0 = r0[r1]
                r8.Arrival2y = r0
                goto L_0x18c8
            L_0x19e5:
                int r0 = r8.p2
                if (r0 != 0) goto L_0x19f7
                int r0 = r8.yellowDirection
                if (r0 == 0) goto L_0x19f7
                int[] r0 = r8.RandomPlane2X
                int r1 = r8.new2X
                r0 = r0[r1]
                r8.Arrival2x = r0
                goto L_0x18c8
            L_0x19f7:
                int r0 = r8.getHeight()
                int r0 = r0 / 4
                r8.Arrival2y = r0
                goto L_0x18c8
            L_0x1a01:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.gamespeed = r1
                goto L_0x1944
            L_0x1a08:
                int r0 = r8.p2
                r1 = 2
                if (r0 != r1) goto L_0x1a47
                int r0 = r8.yellowDirection
                r1 = 2
                if (r0 == r1) goto L_0x1a47
                com.oziapp.coolLanding.GraphicObject r0 = new com.oziapp.coolLanding.GraphicObject
                android.content.res.Resources r1 = r8.getResources()
                r2 = 2130837610(0x7f02006a, float:1.7280179E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)
                com.oziapp.coolLanding.PlaneActivity r2 = com.oziapp.coolLanding.PlaneActivity.this
                int r2 = r2.screen_width
                int[] r3 = r8.RandomPlane2Y
                int r4 = r8.new2Y
                r3 = r3[r4]
                float r4 = r8.Xv
                float r5 = r8.Yv
                r6 = 2
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r8.redPlane = r0
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                boolean r0 = r0.GameSpeed
                if (r0 == 0) goto L_0x1a40
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.gamespeed = r1
                goto L_0x1949
            L_0x1a40:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.gamespeed = r1
                goto L_0x1949
            L_0x1a47:
                int r0 = r8.p2
                r1 = 3
                if (r0 != r1) goto L_0x1a84
                int r0 = r8.yellowDirection
                r1 = 3
                if (r0 == r1) goto L_0x1a84
                com.oziapp.coolLanding.GraphicObject r0 = new com.oziapp.coolLanding.GraphicObject
                android.content.res.Resources r1 = r8.getResources()
                r2 = 2130837610(0x7f02006a, float:1.7280179E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)
                r2 = 10
                int[] r3 = r8.RandomPlane2Y
                int r4 = r8.new2Y
                r3 = r3[r4]
                float r4 = r8.Xv
                float r5 = r8.Yv
                r6 = 2
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r8.redPlane = r0
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                boolean r0 = r0.GameSpeed
                if (r0 == 0) goto L_0x1a7d
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.gamespeed = r1
                goto L_0x1949
            L_0x1a7d:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.gamespeed = r1
                goto L_0x1949
            L_0x1a84:
                int r0 = r8.p2
                if (r0 != 0) goto L_0x1aca
                int r0 = r8.yellowDirection
                if (r0 == 0) goto L_0x1aca
                com.oziapp.coolLanding.GraphicObject r0 = new com.oziapp.coolLanding.GraphicObject
                android.content.res.Resources r1 = r8.getResources()
                r2 = 2130837610(0x7f02006a, float:1.7280179E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)
                int[] r2 = r8.RandomPlane2X
                int r3 = r8.new2X
                r2 = r2[r3]
                r3 = 10
                float r4 = r8.Xv
                float r5 = r8.Yv
                r6 = 2
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r8.redPlane = r0
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1119092736(0x42b40000, float:90.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                boolean r0 = r0.GameSpeed
                if (r0 == 0) goto L_0x1ac4
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.gamespeed = r1
            L_0x1abd:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.flgYrun = r1
                goto L_0x1949
            L_0x1ac4:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.gamespeed = r1
                goto L_0x1abd
            L_0x1aca:
                com.oziapp.coolLanding.GraphicObject r0 = new com.oziapp.coolLanding.GraphicObject
                android.content.res.Resources r1 = r8.getResources()
                r2 = 2130837610(0x7f02006a, float:1.7280179E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)
                r2 = 10
                com.oziapp.coolLanding.PlaneActivity r3 = com.oziapp.coolLanding.PlaneActivity.this
                int r3 = r3.screen_height
                int r3 = r3 / 4
                float r4 = r8.Xv
                float r5 = r8.Yv
                r6 = 2
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r8.redPlane = r0
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                boolean r0 = r0.GameSpeed
                if (r0 == 0) goto L_0x1af6
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.gamespeed = r1
                goto L_0x1949
            L_0x1af6:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.gamespeed = r1
                goto L_0x1949
            L_0x1afd:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getxDirection()
                r1 = 1
                if (r0 != r1) goto L_0x1b13
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleXDirection()
            L_0x1b13:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getX()
                r8.plane_currentX2 = r0
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getY()
                r8.plane_currentY2 = r0
                int r0 = r8.plane_currentX2
                int r1 = r8.landingZone2X1
                if (r0 <= r1) goto L_0x1b35
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1127481344(0x43340000, float:180.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.flgYrun = r1
            L_0x1b35:
                android.graphics.Path r0 = r8.mPath2
                r0.reset()
                android.graphics.Path r0 = r8.mPath2
                com.oziapp.coolLanding.GraphicObject r1 = r8.redPlane
                int r1 = r1.getX()
                float r1 = (float) r1
                com.oziapp.coolLanding.GraphicObject r2 = r8.redPlane
                int r2 = r2.getY()
                float r2 = (float) r2
                r0.moveTo(r1, r2)
                android.graphics.Path r0 = r8.mPath2
                int r1 = r8.landingZone2X1
                r2 = 5
                int r1 = r1 - r2
                float r1 = (float) r1
                com.oziapp.coolLanding.GraphicObject r2 = r8.redPlane
                int r2 = r2.getY()
                float r2 = (float) r2
                r0.lineTo(r1, r2)
                android.graphics.Path r0 = r8.mPath2
                int r1 = r8.landingZone2X1
                r2 = 5
                int r1 = r1 - r2
                float r1 = (float) r1
                int r2 = r8.landingZone2Y1
                int r3 = r8.landingZone2Y2
                int r2 = r2 + r3
                int r2 = r2 / 2
                r3 = 1
                int r2 = r2 - r3
                float r2 = (float) r2
                r0.lineTo(r1, r2)
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getX()
                int r1 = r8.landingZone2X1
                r2 = 5
                int r1 = r1 - r2
                if (r0 >= r1) goto L_0x1bec
                android.graphics.Path r0 = r8.mPath2
                r0.reset()
                android.graphics.Path r0 = r8.mPath2
                com.oziapp.coolLanding.GraphicObject r1 = r8.redPlane
                int r1 = r1.getX()
                float r1 = (float) r1
                com.oziapp.coolLanding.GraphicObject r2 = r8.redPlane
                int r2 = r2.getY()
                float r2 = (float) r2
                r0.moveTo(r1, r2)
                android.graphics.Path r0 = r8.mPath2
                com.oziapp.coolLanding.GraphicObject r1 = r8.redPlane
                int r1 = r1.getX()
                float r1 = (float) r1
                int r2 = r8.landingZone2Y2
                r3 = 10
                int r2 = r2 - r3
                float r2 = (float) r2
                r0.lineTo(r1, r2)
                android.graphics.Path r0 = r8.mPath2
                int r1 = r8.landingZone2X1
                r2 = 5
                int r1 = r1 - r2
                float r1 = (float) r1
                int r2 = r8.landingZone2Y2
                r3 = 10
                int r2 = r2 - r3
                float r2 = (float) r2
                r0.lineTo(r1, r2)
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.flgYrun = r1
                int r0 = r8.plane_currentY2
                int r1 = r8.landingZone2Y1
                int r2 = r8.landingZone2Y2
                int r1 = r1 + r2
                int r1 = r1 / 2
                int r1 = r1 + 1
                if (r0 <= r1) goto L_0x1d39
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = -1028390912(0xffffffffc2b40000, float:-90.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getyDirection()
                r1 = 1
                if (r0 != r1) goto L_0x1be7
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleYDirection()
            L_0x1be7:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.flgYrun = r1
            L_0x1bec:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getY()
                int r1 = r8.landingZone2Y1
                int r2 = r8.landingZone2Y2
                int r1 = r1 + r2
                int r1 = r1 / 2
                if (r0 == r1) goto L_0x1c0c
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getY()
                int r1 = r8.landingZone2Y1
                int r2 = r8.landingZone2Y2
                int r1 = r1 + r2
                int r1 = r1 / 2
                int r1 = r1 + 1
                if (r0 != r1) goto L_0x05f3
            L_0x1c0c:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.flgYrun = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.flgTouchEnable = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getxDirection()
                r1 = -1
                if (r0 != r1) goto L_0x1c2c
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleXDirection()
            L_0x1c2c:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getX()
                int r1 = r8.landingZone2X1
                int r1 = r1 + 6
                if (r0 == r1) goto L_0x1c4a
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getX()
                int r1 = r8.landingZone2X1
                int r1 = r1 + 7
                if (r0 != r1) goto L_0x1c5d
            L_0x1c4a:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.flgLandingMode = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.flgOnSurface = r1
                java.util.Random r0 = r8.rand
                r1 = 4
                int r0 = r0.nextInt(r1)
                r8.p2 = r0
            L_0x1c5d:
                int r0 = r8.p2
                r1 = 2
                if (r0 != r1) goto L_0x1d6a
                int r0 = r8.yellowDirection
                r1 = 2
                if (r0 == r1) goto L_0x1d6a
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                int r0 = r0.screen_width
                r1 = 10
                int r0 = r0 - r1
                r8.Arrival2x = r0
            L_0x1c70:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getX()
                int r1 = r8.landingZone2X1
                int r1 = r1 + 5
                if (r0 <= r1) goto L_0x1cca
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                boolean r0 = r0.flgLandingMode
                if (r0 == 0) goto L_0x1cca
                int r0 = r8.plane1
                int r0 = r0 % 2
                if (r0 != 0) goto L_0x1cb4
                java.util.Random r0 = r8.rand
                r1 = 19
                int r0 = r0.nextInt(r1)
                r8.new2Y = r0
                java.util.Random r0 = r8.rand
                r1 = 19
                int r0 = r0.nextInt(r1)
                r8.new2X = r0
                int r0 = r8.p2
                r1 = 1
                if (r0 != r1) goto L_0x1da3
                int r0 = r8.yellowDirection
                r1 = 1
                if (r0 == r1) goto L_0x1da3
                int[] r0 = r8.RandomPlane2X
                int r1 = r8.new2X
                r0 = r0[r1]
                r8.Arrival2x = r0
            L_0x1cae:
                int r0 = r8.plane1
                int r0 = r0 + 1
                r8.plane1 = r0
            L_0x1cb4:
                android.graphics.Path r0 = r8.mPath2
                r0.reset()
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.flgLandingMode = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.flgOnSurface = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                android.graphics.Bitmap r1 = r8.plane1rightlanding
                r0.setBitmap(r1)
            L_0x1cca:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getX()
                int r1 = r8.landingZone2X2
                r2 = 40
                int r1 = r1 - r2
                if (r0 == r1) goto L_0x1ce4
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                int r0 = r0.getX()
                int r1 = r8.landingZone2X2
                r2 = 39
                int r1 = r1 - r2
                if (r0 != r1) goto L_0x05f3
            L_0x1ce4:
                r0 = 0
                r8.flgtouched2 = r0
                int r0 = r8.score
                int r0 = r0 + 1
                r8.score = r0
                int r0 = r8.p2
                r1 = 1
                if (r0 != r1) goto L_0x1dee
                int r0 = r8.yellowDirection
                r1 = 1
                if (r0 == r1) goto L_0x1dee
                com.oziapp.coolLanding.GraphicObject r0 = new com.oziapp.coolLanding.GraphicObject
                android.content.res.Resources r1 = r8.getResources()
                r2 = 2130837610(0x7f02006a, float:1.7280179E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)
                int[] r2 = r8.RandomPlane2X
                int r3 = r8.new2X
                r2 = r2[r3]
                com.oziapp.coolLanding.PlaneActivity r3 = com.oziapp.coolLanding.PlaneActivity.this
                int r3 = r3.screen_height
                float r4 = r8.Xv
                float r5 = r8.Yv
                r6 = 2
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r8.redPlane = r0
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                boolean r0 = r0.GameSpeed
                if (r0 == 0) goto L_0x1de7
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.gamespeed = r1
            L_0x1d23:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.flgYrun = r1
            L_0x1d28:
                int r0 = r8.p2
                r8.redDirection = r0
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.flgTouchEnable = r1
                int r0 = r8.plane1
                r1 = 1
                int r0 = r0 - r1
                r8.plane1 = r0
                goto L_0x05f3
            L_0x1d39:
                int r0 = r8.plane_currentY2
                int r1 = r8.landingZone2Y1
                int r2 = r8.landingZone2Y2
                int r1 = r1 + r2
                int r1 = r1 / 2
                r2 = 1
                int r1 = r1 - r2
                if (r0 >= r1) goto L_0x1bec
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1119092736(0x42b40000, float:90.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getyDirection()
                r1 = -1
                if (r0 != r1) goto L_0x1d63
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleYDirection()
            L_0x1d63:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.flgYrun = r1
                goto L_0x1bec
            L_0x1d6a:
                int r0 = r8.p2
                r1 = 3
                if (r0 != r1) goto L_0x1d7a
                int r0 = r8.yellowDirection
                r1 = 3
                if (r0 == r1) goto L_0x1d7a
                r0 = 10
                r8.Arrival2x = r0
                goto L_0x1c70
            L_0x1d7a:
                int r0 = r8.p2
                r1 = 1
                if (r0 != r1) goto L_0x1d8f
                int r0 = r8.yellowDirection
                r1 = 1
                if (r0 == r1) goto L_0x1d8f
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                int r0 = r0.screen_height
                r1 = 10
                int r0 = r0 - r1
                r8.Arrival2y = r0
                goto L_0x1c70
            L_0x1d8f:
                int r0 = r8.p2
                if (r0 != 0) goto L_0x1d9d
                int r0 = r8.yellowDirection
                if (r0 == 0) goto L_0x1d9d
                r0 = 10
                r8.Arrival2y = r0
                goto L_0x1c70
            L_0x1d9d:
                r0 = 10
                r8.Arrival2x = r0
                goto L_0x1c70
            L_0x1da3:
                int r0 = r8.p2
                r1 = 2
                if (r0 != r1) goto L_0x1db7
                int r0 = r8.yellowDirection
                r1 = 2
                if (r0 == r1) goto L_0x1db7
                int[] r0 = r8.RandomPlane2Y
                int r1 = r8.new2Y
                r0 = r0[r1]
                r8.Arrival2y = r0
                goto L_0x1cae
            L_0x1db7:
                int r0 = r8.p2
                r1 = 3
                if (r0 != r1) goto L_0x1dcb
                int r0 = r8.yellowDirection
                r1 = 3
                if (r0 == r1) goto L_0x1dcb
                int[] r0 = r8.RandomPlane2Y
                int r1 = r8.new2Y
                r0 = r0[r1]
                r8.Arrival2y = r0
                goto L_0x1cae
            L_0x1dcb:
                int r0 = r8.p2
                if (r0 != 0) goto L_0x1ddd
                int r0 = r8.yellowDirection
                if (r0 == 0) goto L_0x1ddd
                int[] r0 = r8.RandomPlane2X
                int r1 = r8.new2X
                r0 = r0[r1]
                r8.Arrival2x = r0
                goto L_0x1cae
            L_0x1ddd:
                int r0 = r8.getHeight()
                int r0 = r0 / 4
                r8.Arrival2y = r0
                goto L_0x1cae
            L_0x1de7:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.gamespeed = r1
                goto L_0x1d23
            L_0x1dee:
                int r0 = r8.p2
                r1 = 2
                if (r0 != r1) goto L_0x1e2d
                int r0 = r8.yellowDirection
                r1 = 2
                if (r0 == r1) goto L_0x1e2d
                com.oziapp.coolLanding.GraphicObject r0 = new com.oziapp.coolLanding.GraphicObject
                android.content.res.Resources r1 = r8.getResources()
                r2 = 2130837610(0x7f02006a, float:1.7280179E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)
                com.oziapp.coolLanding.PlaneActivity r2 = com.oziapp.coolLanding.PlaneActivity.this
                int r2 = r2.screen_width
                int[] r3 = r8.RandomPlane2Y
                int r4 = r8.new2Y
                r3 = r3[r4]
                float r4 = r8.Xv
                float r5 = r8.Yv
                r6 = 2
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r8.redPlane = r0
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                boolean r0 = r0.GameSpeed
                if (r0 == 0) goto L_0x1e26
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.gamespeed = r1
                goto L_0x1d28
            L_0x1e26:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.gamespeed = r1
                goto L_0x1d28
            L_0x1e2d:
                int r0 = r8.p2
                r1 = 3
                if (r0 != r1) goto L_0x1e6a
                int r0 = r8.yellowDirection
                r1 = 3
                if (r0 == r1) goto L_0x1e6a
                com.oziapp.coolLanding.GraphicObject r0 = new com.oziapp.coolLanding.GraphicObject
                android.content.res.Resources r1 = r8.getResources()
                r2 = 2130837610(0x7f02006a, float:1.7280179E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)
                r2 = 10
                int[] r3 = r8.RandomPlane2Y
                int r4 = r8.new2Y
                r3 = r3[r4]
                float r4 = r8.Xv
                float r5 = r8.Yv
                r6 = 2
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r8.redPlane = r0
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                boolean r0 = r0.GameSpeed
                if (r0 == 0) goto L_0x1e63
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.gamespeed = r1
                goto L_0x1d28
            L_0x1e63:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.gamespeed = r1
                goto L_0x1d28
            L_0x1e6a:
                int r0 = r8.p2
                if (r0 != 0) goto L_0x1eb0
                int r0 = r8.yellowDirection
                if (r0 == 0) goto L_0x1eb0
                com.oziapp.coolLanding.GraphicObject r0 = new com.oziapp.coolLanding.GraphicObject
                android.content.res.Resources r1 = r8.getResources()
                r2 = 2130837610(0x7f02006a, float:1.7280179E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)
                int[] r2 = r8.RandomPlane2X
                int r3 = r8.new2X
                r2 = r2[r3]
                r3 = 10
                float r4 = r8.Xv
                float r5 = r8.Yv
                r6 = 2
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r8.redPlane = r0
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1119092736(0x42b40000, float:90.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                boolean r0 = r0.GameSpeed
                if (r0 == 0) goto L_0x1eaa
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.gamespeed = r1
            L_0x1ea3:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.flgYrun = r1
                goto L_0x1d28
            L_0x1eaa:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.gamespeed = r1
                goto L_0x1ea3
            L_0x1eb0:
                com.oziapp.coolLanding.GraphicObject r0 = new com.oziapp.coolLanding.GraphicObject
                android.content.res.Resources r1 = r8.getResources()
                r2 = 2130837610(0x7f02006a, float:1.7280179E38)
                android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)
                r2 = 10
                com.oziapp.coolLanding.PlaneActivity r3 = com.oziapp.coolLanding.PlaneActivity.this
                int r3 = r3.screen_height
                int r3 = r3 / 4
                float r4 = r8.Xv
                float r5 = r8.Yv
                r6 = 2
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r8.redPlane = r0
                com.oziapp.coolLanding.PlaneActivity r0 = com.oziapp.coolLanding.PlaneActivity.this
                boolean r0 = r0.GameSpeed
                if (r0 == 0) goto L_0x1edc
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.gamespeed = r1
                goto L_0x1d28
            L_0x1edc:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1
                r0.gamespeed = r1
                goto L_0x1d28
            L_0x1ee3:
                android.graphics.Path r0 = r8.mPath3
                int r1 = r8.helipadX1
                r2 = 5
                int r1 = r1 - r2
                float r1 = (float) r1
                int r2 = r8.helipadY1
                int r2 = r2 + 25
                float r2 = (float) r2
                r0.lineTo(r1, r2)
                goto L_0x064d
            L_0x1ef4:
                android.graphics.Path r0 = r8.mPath3
                int r1 = r8.helipadX1
                r2 = 5
                int r1 = r1 - r2
                float r1 = (float) r1
                int r2 = r8.helipadY1
                int r2 = r2 + 25
                float r2 = (float) r2
                r0.lineTo(r1, r2)
                goto L_0x06cd
            L_0x1f05:
                android.graphics.Path r0 = r8.mPath3
                int r1 = r8.helipadX2
                int r1 = r1 + 5
                float r1 = (float) r1
                int r2 = r8.helipadY1
                int r2 = r2 + 25
                float r2 = (float) r2
                r0.lineTo(r1, r2)
                goto L_0x0717
            L_0x1f16:
                int r0 = r8.plane_currentX3
                int r1 = r8.helipadX1
                if (r0 >= r1) goto L_0x0739
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                r1 = 0
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getxDirection()
                r1 = -1
                if (r0 != r1) goto L_0x1f38
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleXDirection()
            L_0x1f38:
                android.graphics.Path r0 = r8.mPath3
                r0.reset()
                android.graphics.Path r0 = r8.mPath3
                com.oziapp.coolLanding.GraphicObject r1 = r8.helicopter
                int r1 = r1.getX()
                float r1 = (float) r1
                com.oziapp.coolLanding.GraphicObject r2 = r8.helicopter
                int r2 = r2.getY()
                float r2 = (float) r2
                r0.moveTo(r1, r2)
                int r0 = r8.plane_currentY3
                int r1 = r8.helipadY1
                int r1 = r1 + 10
                if (r0 <= r1) goto L_0x1f69
                android.graphics.Path r0 = r8.mPath3
                int r1 = r8.helipadX1
                r2 = 5
                int r1 = r1 - r2
                float r1 = (float) r1
                int r2 = r8.helipadY1
                int r2 = r2 + 25
                float r2 = (float) r2
                r0.lineTo(r1, r2)
                goto L_0x0739
            L_0x1f69:
                android.graphics.Path r0 = r8.mPath3
                int r1 = r8.helipadX1
                r2 = 5
                int r1 = r1 - r2
                float r1 = (float) r1
                int r2 = r8.helipadY1
                int r2 = r2 + 25
                float r2 = (float) r2
                r0.lineTo(r1, r2)
                goto L_0x0739
            L_0x1f7a:
                int r0 = r8.Arrivalx
                r1 = 48
                int r0 = r0 - r1
                r8.ArrivalheliX = r0
                goto L_0x0788
            L_0x1f83:
                int r0 = r8.Arrival2x
                r1 = 48
                int r0 = r0 - r1
                r8.ArrivalheliX = r0
                goto L_0x07a3
            L_0x1f8c:
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                r1 = 1
                r0.gamespeed = r1
                goto L_0x0804
            L_0x1f93:
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getxDirection()
                r1 = 1
                if (r0 != r1) goto L_0x0877
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.flgYrun = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleXDirection()
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 1127481344(0x43340000, float:180.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.yellowPlane
                r1 = 0
                r0.setTouched(r1)
                r0 = 0
                r8.flgtouched = r0
                android.graphics.Path r0 = r8.mPath1
                r0.reset()
                goto L_0x0877
            L_0x1fc5:
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getxDirection()
                r1 = 1
                if (r0 != r1) goto L_0x08ba
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.flgYrun = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleXDirection()
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 1127481344(0x43340000, float:180.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.redPlane
                r1 = 0
                r0.setTouched(r1)
                r0 = 0
                r8.flgtouched2 = r0
                android.graphics.Path r0 = r8.mPath2
                r0.reset()
                goto L_0x08ba
            L_0x1ff7:
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                int r0 = r0.getyDirection()
                r1 = -1
                if (r0 != r1) goto L_0x0909
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                r1 = 1
                r0.flgYrun = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                android.graphics.Bitmap r1 = r8.helinormal1
                r0.heli1 = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                android.graphics.Bitmap r1 = r8.helinormal2
                r0.heli2 = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                android.graphics.Bitmap r1 = r8.helinormal3
                r0.heli3 = r1
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                r1 = 1119092736(0x42b40000, float:90.0)
                r0.setAngle(r1)
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                com.oziapp.coolLanding.Speed r0 = r0.getSpeed()
                r0.toggleYDirection()
                com.oziapp.coolLanding.GraphicObject r0 = r8.helicopter
                r1 = 0
                r0.setTouched(r1)
                r0 = 0
                r8.flghelipad = r0
                android.graphics.Path r0 = r8.mPath3
                r0.reset()
                goto L_0x0909
            */
            throw new UnsupportedOperationException("Method not decompiled: com.oziapp.coolLanding.PlaneActivity.MyView.updatePhysics(float, int):void");
        }

        private void warningCheck(GraphicObject obj1, GraphicObject obj2) {
            if (((obj1.getX() + obj1.getBitmap().getWidth() + 10 < obj2.getX() || obj1.getX() + obj1.getBitmap().getWidth() + 10 > obj2.getX() + obj2.getBitmap().getWidth()) && (obj2.getX() + obj2.getBitmap().getWidth() + 10 < obj1.getX() || obj2.getX() + obj2.getBitmap().getWidth() + 10 > obj1.getX() + obj1.getBitmap().getWidth())) || ((obj1.getY() + obj1.getBitmap().getHeight() + 10 < obj2.getY() || obj1.getY() + obj1.getBitmap().getHeight() + 10 > obj2.getY() + obj2.getBitmap().getHeight()) && (obj2.getY() + obj2.getBitmap().getHeight() + 10 < obj1.getY() || obj2.getY() + obj2.getBitmap().getHeight() + 10 > obj1.getY() + obj1.getBitmap().getHeight()))) {
                if (obj1.flgWarning && obj2.flgWarning) {
                    RemoveWarning(obj1);
                    RemoveWarning(obj2);
                    obj1.flgWarning = false;
                    obj2.flgWarning = false;
                }
            } else if (!obj1.flgLandingMode || !obj2.flgLandingMode) {
                if (obj1.PlaneType == 2) {
                    if (!obj1.flgGoforLand) {
                        obj1.setBitmap(this.redwarning);
                    } else {
                        obj1.setBitmap(this.redlandwarning);
                    }
                } else if (obj1.PlaneType == 1) {
                    if (!obj1.flgGoforLand) {
                        obj1.setBitmap(this.yellowwarning);
                    } else {
                        obj1.setBitmap(this.yellowlandwarning);
                    }
                } else if (obj1.PlaneType == 3) {
                    if (obj1.flgGoforLand) {
                        obj1.heli1 = this.heliwd1;
                        obj1.heli2 = this.heliwd2;
                        obj1.heli2 = this.heliwd3;
                    } else {
                        obj1.heli1 = this.helind1;
                        obj1.heli2 = this.helind2;
                        obj1.heli2 = this.helind3;
                    }
                }
                if (obj2.PlaneType == 2) {
                    if (!obj2.flgGoforLand) {
                        obj2.setBitmap(this.redwarning);
                    } else {
                        obj2.setBitmap(this.redlandwarning);
                    }
                } else if (obj2.PlaneType == 1) {
                    if (!obj2.flgGoforLand) {
                        obj2.setBitmap(this.yellowwarning);
                    } else {
                        obj2.setBitmap(this.yellowlandwarning);
                    }
                } else if (obj2.PlaneType == 3) {
                    if (obj2.flgGoforLand) {
                        obj2.heli1 = this.heliwd1;
                        obj2.heli2 = this.heliwd2;
                        obj2.heli2 = this.heliwd3;
                    } else {
                        obj2.heli1 = this.helind1;
                        obj2.heli2 = this.helind2;
                        obj2.heli2 = this.helind3;
                    }
                }
                obj1.flgWarning = true;
                obj2.flgWarning = true;
                obj1.warningObject = obj2;
                obj2.warningObject = obj1;
            }
        }

        private void RemoveWarning(GraphicObject obj) {
            if (obj.PlaneType == 2) {
                if (!obj.flgGoforLand) {
                    obj.setBitmap(this.plane1right);
                } else {
                    obj.setBitmap(this.plane1rightshadow);
                }
            } else if (obj.PlaneType == 1) {
                if (!obj.flgGoforLand) {
                    obj.setBitmap(this.plane2right);
                } else {
                    obj.setBitmap(this.plane2rightshadow);
                }
            } else if (obj.PlaneType != 3) {
            } else {
                if (!obj.flgGoforLand) {
                    obj.heli1 = this.helinormal1;
                    obj.heli2 = this.helinormal2;
                    obj.heli3 = this.helinormal3;
                } else if (obj.flgGoforLand) {
                    obj.heli1 = this.heliland1;
                    obj.heli2 = this.heliland2;
                    obj.heli3 = this.heliland3;
                }
            }
        }

        public boolean onTouchEvent(MotionEvent event) {
            synchronized (this._thread.getSurfaceHolder()) {
                switch (event.getAction()) {
                    case 0:
                        this.x = event.getX();
                        this.y = event.getY();
                        this.mX = this.x;
                        this.mY = this.y;
                        if (this.x > ((float) (PlaneActivity.this.screen_width - 55)) && this.x < ((float) (PlaneActivity.this.screen_width - 20)) && this.y > ((float) (PlaneActivity.this.screen_height - 60)) && this.y < ((float) (PlaneActivity.this.screen_height - 20))) {
                            if (!PlaneActivity.this.flgPause) {
                                PlaneActivity.this.alert.show();
                                PlaneActivity.this.flgPause = true;
                            } else {
                                PlaneActivity.this.flgPause = false;
                            }
                        }
                        if (!PlaneActivity.this.flgPause) {
                            if (this.x < 50.0f && this.y > 400.0f && PlaneActivity.this.GameSpeed) {
                                PlaneActivity.this.GameSpeed = false;
                                this.yellowPlane.gamespeed = true;
                                this.redPlane.gamespeed = true;
                                this.helicopter.gamespeed = true;
                            } else if (this.x < 50.0f && this.y > ((float) (PlaneActivity.this.screen_height - 100)) && !PlaneActivity.this.GameSpeed) {
                                PlaneActivity.this.GameSpeed = true;
                                this.yellowPlane.gamespeed = false;
                                this.redPlane.gamespeed = false;
                                this.helicopter.gamespeed = false;
                            }
                        }
                        if (!PlaneActivity.this.flgPause) {
                            if (this.yellowPlane.flgTouchEnable) {
                                this.yellowPlane.handleActionDown((int) event.getX(), (int) event.getY());
                            }
                            if (this.redPlane.flgTouchEnable) {
                                this.redPlane.handleActionDown((int) event.getX(), (int) event.getY());
                            }
                            if (this.helicopter.flgTouchEnable) {
                                this.helicopter.handleActionDown((int) event.getX(), (int) event.getY());
                            }
                            this.mPath.reset();
                            this.mPath.moveTo(this.x, this.y);
                            if (!this.flgModes) {
                                touch_start(this.x, this.y);
                            }
                            invalidate();
                            break;
                        }
                        break;
                    case 1:
                        this.upX = event.getX();
                        this.upY = event.getY();
                        break;
                    case 2:
                        this.x = event.getX();
                        this.y = event.getY();
                        invalidate();
                        break;
                }
            }
            return true;
        }

        private void touch_start(float x2, float y2) {
            if (this.yellowPlane.isTouched()) {
                this.yellowPlane.points_index = 0;
                this.yellowPlane.points = (int[][]) Array.newInstance(Integer.TYPE, 50, 2);
                this.yellowPlane.points[this.yellowPlane.points_index][0] = (int) x2;
                this.yellowPlane.points[this.yellowPlane.points_index][1] = (int) y2;
                this.yellowPlane.points_index++;
                this.mPath1.reset();
            }
            if (this.redPlane.isTouched()) {
                this.redPlane.points_index = 0;
                this.redPlane.points = (int[][]) Array.newInstance(Integer.TYPE, 50, 2);
                this.redPlane.points[this.redPlane.points_index][0] = (int) x2;
                this.redPlane.points[this.redPlane.points_index][1] = (int) y2;
                this.redPlane.points_index++;
                this.mPath2.reset();
            }
            if (this.helicopter.isTouched()) {
                this.helicopter.points_index = 0;
                this.helicopter.points = (int[][]) Array.newInstance(Integer.TYPE, 50, 2);
                this.helicopter.points[this.helicopter.points_index][0] = (int) x2;
                this.helicopter.points[this.helicopter.points_index][1] = (int) y2;
                this.helicopter.points_index++;
                this.mPath3.reset();
            }
            if (this.yellowPlane.getX() < PlaneActivity.this.screen_width - 10) {
                if (SelectMap.map_Select == R.drawable.mountainjoy) {
                    if (!this.flgtouched && x2 >= ((float) (this.tempZone1X1 - 100)) && x2 <= ((float) (this.tempZone1X1 + 10)) && y2 >= ((float) (this.landingZone1Y1 - 10)) && y2 <= ((float) (this.landingZone1Y2 + 10))) {
                        if (this.runway1 != null) {
                            this.runway1.start();
                        }
                        this.flgtouched = true;
                        this.yellowPlane.flgGoforLand = true;
                        this.yellowPlane.setBitmap(this.plane2rightshadow);
                        this.mPath1.moveTo((float) this.yellowPlane.getX(), (float) this.yellowPlane.getY());
                        if (this.yellowPlane.getX() > this.tempZone1X1) {
                            this.mPath1.lineTo((float) (this.tempZone1X1 + 10), (float) this.yellowPlane.getY());
                            this.mPath1.lineTo((float) (this.tempZone1X1 + 10), (float) (this.landingZone1Y2 - 10));
                        }
                        if (this.yellowPlane.getX() < this.tempZone1X1) {
                            this.mPath1.lineTo((float) this.yellowPlane.getX(), (float) (this.landingZone1Y2 - 10));
                            this.mPath1.lineTo((float) this.tempZone1X1, (float) (this.landingZone1Y2 - 10));
                        }
                    }
                } else if (SelectMap.map_Select == R.drawable.meadowolives) {
                    if (!this.flgtouched && x2 >= ((float) this.tempZone1X1) && x2 <= ((float) (this.tempZone1X1 + 40)) && y2 >= ((float) (this.tempZone1Y1 - 10)) && y2 <= ((float) (this.tempZone1Y1 + 100)) && (this.yellowPlane.getX() > this.tempZone1X1 + 50 || this.yellowPlane.getX() < this.tempZone1X1 - 20)) {
                        if (this.runway1 != null) {
                            this.runway1.start();
                        }
                        this.flgtouched = true;
                        this.yellowPlane.flgGoforLand = true;
                        this.yellowPlane.setBitmap(this.plane2rightshadow);
                        this.mPath1.moveTo((float) this.yellowPlane.getX(), (float) this.yellowPlane.getY());
                        if (this.yellowPlane.getY() > this.tempZone1Y1) {
                            this.mPath1.lineTo((float) this.yellowPlane.getX(), (float) (this.tempZone1Y1 + 40));
                            this.mPath1.lineTo((float) (this.tempZone1X1 + 20), (float) (this.tempZone1Y1 + 40));
                        }
                        if (this.yellowPlane.getY() < this.tempZone1Y1) {
                            this.mPath1.lineTo((float) (this.tempZone1X1 + 20), (float) this.yellowPlane.getY());
                            this.mPath1.lineTo((float) (this.tempZone1X1 + 20), (float) this.tempZone1Y1);
                        }
                    }
                } else if (!this.flgtouched && x2 >= ((float) (this.landingZone1X1 - 10)) && x2 <= ((float) (this.landingZone1X1 + 100)) && y2 >= ((float) (this.landingZone1Y1 - 10)) && y2 <= ((float) (this.landingZone1Y2 + 10))) {
                    if (this.runway1 != null) {
                        this.runway1.start();
                    }
                    this.flgtouched = true;
                    this.yellowPlane.flgGoforLand = true;
                    this.yellowPlane.setBitmap(this.plane2rightshadow);
                    this.mPath1.moveTo((float) this.yellowPlane.getX(), (float) this.yellowPlane.getY());
                    if (this.yellowPlane.getX() > this.landingZone1X1) {
                        this.mPath1.lineTo((float) (this.landingZone1X1 - 10), (float) this.yellowPlane.getY());
                        this.mPath1.lineTo((float) (this.landingZone1X1 - 10), (float) (this.landingZone1Y2 - 10));
                    }
                    if (this.yellowPlane.getX() < this.landingZone1X1) {
                        this.mPath1.lineTo((float) this.yellowPlane.getX(), (float) (this.landingZone1Y2 - 10));
                        this.mPath1.lineTo((float) this.landingZone1X1, (float) (this.landingZone1Y2 - 10));
                    }
                }
            }
            if (((float) this.redPlane.getX()) + (((float) this.redPlane.getBitmap().getWidth()) * 0.5f) > TOUCH_TOLERANCE) {
                if (SelectMap.map_Select == R.drawable.meadowolives) {
                    if (!this.flgtouched2 && x2 >= ((float) (this.tempZone2X1 - 10)) && x2 <= ((float) (this.tempZone2X1 + 50)) && y2 <= ((float) (this.tempZone2Y2 + 10)) && y2 >= ((float) (this.tempZone2Y2 - 100))) {
                        if (this.runway1 != null) {
                            this.runway2.start();
                        }
                        this.flgtouched2 = true;
                        this.redPlane.flgGoforLand = true;
                        this.redPlane.setBitmap(this.plane1rightshadow);
                        this.mPath2.moveTo((float) this.redPlane.getX(), (float) this.redPlane.getY());
                        if (this.redPlane.getX() > this.tempZone2X1) {
                            this.mPath2.lineTo((float) this.redPlane.getX(), (float) this.tempZone2Y2);
                            this.mPath2.lineTo((float) this.tempZone2X1, (float) this.tempZone2Y2);
                        }
                        if (this.redPlane.getX() < this.tempZone2X1) {
                            this.mPath2.lineTo((float) this.redPlane.getX(), (float) this.tempZone2Y2);
                            this.mPath2.lineTo((float) this.tempZone2X1, (float) this.tempZone2Y2);
                        }
                    }
                } else if (SelectMap.map_Select == R.drawable.maritimelands) {
                    if (!this.flgtouched2 && x2 >= ((float) (this.tempZone2X1 - 10)) && x2 <= ((float) (this.tempZone2X1 + 50)) && y2 <= ((float) (this.tempZone2Y2 + 10)) && y2 >= ((float) (this.tempZone2Y2 - 100))) {
                        if (this.runway1 != null) {
                            this.runway2.start();
                        }
                        this.flgtouched2 = true;
                        this.redPlane.flgGoforLand = true;
                        this.redPlane.setBitmap(this.plane1rightshadow);
                        this.mPath2.moveTo((float) this.redPlane.getX(), (float) this.redPlane.getY());
                        if (this.redPlane.getX() > this.tempZone2X1) {
                            this.mPath2.lineTo((float) this.redPlane.getX(), (float) this.tempZone2Y2);
                            this.mPath2.lineTo((float) this.tempZone2X1, (float) this.tempZone2Y2);
                        }
                        if (this.redPlane.getX() < this.tempZone2X1) {
                            this.mPath2.lineTo((float) this.redPlane.getX(), (float) this.tempZone2Y2);
                            this.mPath2.lineTo((float) this.tempZone2X1, (float) this.tempZone2Y2);
                        }
                    }
                } else if (!this.flgtouched2 && x2 >= ((float) (this.landingZone2X1 - 10)) && x2 <= ((float) (this.landingZone2X1 + 100)) && y2 >= ((float) (this.landingZone2Y1 - 10)) && y2 <= ((float) (this.landingZone2Y2 + 10))) {
                    if (this.runway1 != null) {
                        this.runway2.start();
                    }
                    this.flgtouched2 = true;
                    this.redPlane.flgGoforLand = true;
                    this.redPlane.setBitmap(this.plane1rightshadow);
                    this.mPath2.moveTo((float) this.redPlane.getX(), (float) this.redPlane.getY());
                    if (this.redPlane.getX() > this.landingZone2X1) {
                        this.mPath2.lineTo((float) (this.landingZone2X1 - 5), (float) this.redPlane.getY());
                        this.mPath2.lineTo((float) (this.landingZone2X1 - 5), (float) (this.landingZone2Y2 - 10));
                    }
                    if (this.redPlane.getX() < this.landingZone2X1) {
                        this.mPath2.lineTo((float) this.redPlane.getX(), (float) (this.landingZone2Y2 - 10));
                        this.mPath2.lineTo((float) (this.landingZone2X1 - 5), (float) (this.landingZone2Y2 - 10));
                    }
                }
            }
            if (this.helicopter.getY() > 6 && !this.flghelipad && x2 >= ((float) (this.helipadX1 - 5)) && x2 <= ((float) this.helipadX2) && y2 >= ((float) (this.helipadY1 - 5)) && y2 <= ((float) (this.helipadY2 + 5)) && this.helipad != null) {
                this.helipad.start();
                this.mPath3.moveTo((float) this.helicopter.getX(), (float) this.helicopter.getY());
                if (this.helicopter.getX() < this.helipadX1) {
                    this.mPath3.lineTo((float) this.helicopter.getX(), (float) (this.helipadY1 + 25));
                    this.mPath3.lineTo((float) (this.helipadX1 - 5), (float) (this.helipadY1 + 25));
                }
                if (this.helicopter.getX() > this.helipadX2) {
                    this.mPath3.lineTo((float) this.helicopter.getX(), (float) (this.helipadY1 + 25));
                    this.mPath3.lineTo((float) (this.helipadX2 + 5), (float) (this.helipadY1 + 25));
                }
                this.flghelipad = true;
                this.helicopter.flgGoforLand = true;
                this.helicopter.heli1 = this.heliland1;
                this.helicopter.heli2 = this.heliland2;
                this.helicopter.heli3 = this.heliland3;
            }
        }

        public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
        }

        public void surfaceCreated(SurfaceHolder holder) {
            this._thread = new TutorialThread(getHolder(), this);
            this._thread.setRunning(true);
            this._thread.start();
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
            if (this.backgroundSound != null) {
                this.backgroundSound.setLooping(false);
                this.backgroundSound.stop();
            }
            boolean retry = true;
            this._thread.setRunning(false);
            while (retry) {
                try {
                    this._thread.join();
                    retry = false;
                } catch (InterruptedException e) {
                }
            }
        }

        public void crashCheck(GraphicObject obj1, GraphicObject obj2) {
            if ((obj1.getX() + (obj1.getBitmap().getWidth() / 2) >= obj2.getX() && obj1.getX() + (obj1.getBitmap().getWidth() / 2) <= obj2.getX() + (obj2.getBitmap().getWidth() / 2)) || (obj2.getX() + (obj2.getBitmap().getWidth() / 2) >= obj1.getX() && obj2.getX() + (obj2.getBitmap().getWidth() / 2) <= obj1.getX() + (obj1.getBitmap().getWidth() / 2))) {
                if ((obj1.getY() + (obj1.getBitmap().getHeight() / 2) >= obj2.getY() && obj1.getY() + (obj1.getBitmap().getHeight() / 2) <= obj2.getY() + (obj2.getBitmap().getHeight() / 2)) || (obj2.getY() + (obj2.getBitmap().getHeight() / 2) >= obj1.getY() && obj2.getY() + (obj2.getBitmap().getHeight() / 2) <= obj1.getY() + (obj1.getBitmap().getHeight() / 2))) {
                    if (this.crash != null) {
                        this.crash.start();
                    }
                    this.flgtouched = false;
                    this.flgtouched2 = false;
                    obj1.setBitmap(this.boxbitmap);
                    obj2.setBitmap(this.boxbitmap);
                    this._thread.setRunning(false);
                    if (this.flgPlayAgain) {
                        this.flgPlayAgain = false;
                        Intent playagain = new Intent(PlaneActivity.this, PlayAgain.class);
                        this.bundle = new Bundle();
                        this.bundle.putInt("score", this.score);
                        playagain.putExtras(this.bundle);
                        PlaneActivity.this.finish();
                        PlaneActivity.this.startActivity(playagain);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }
}
