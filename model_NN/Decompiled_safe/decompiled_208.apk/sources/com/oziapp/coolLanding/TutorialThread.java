package com.oziapp.coolLanding;

import android.graphics.Canvas;
import android.view.SurfaceHolder;
import com.oziapp.coolLanding.PlaneActivity;

class TutorialThread extends Thread {
    private PlaneActivity.MyView _panel;
    private boolean _run = false;
    private SurfaceHolder _surfaceHolder;

    public TutorialThread(SurfaceHolder surfaceHolder, PlaneActivity.MyView myView) {
        this._surfaceHolder = surfaceHolder;
        this._panel = myView;
    }

    public void setRunning(boolean run) {
        this._run = run;
    }

    public SurfaceHolder getSurfaceHolder() {
        return this._surfaceHolder;
    }

    public void run() {
        int skip_ticks = 1000 / 25;
        long next_tick = System.currentTimeMillis();
        float interpolation = 0.0f;
        while (this._run) {
            Canvas c = null;
            try {
                c = this._surfaceHolder.lockCanvas(null);
                int loops = 0;
                synchronized (this._surfaceHolder) {
                    while (System.currentTimeMillis() > next_tick && loops < 5) {
                        this._panel.updatePhysics(interpolation, 25);
                        next_tick += (long) skip_ticks;
                        loops++;
                    }
                    interpolation = (float) (((System.currentTimeMillis() + ((long) skip_ticks)) - next_tick) / ((long) skip_ticks));
                    GraphicObject.interpolation = interpolation;
                    this._panel.onDraw(c);
                }
            } finally {
                if (c != null) {
                    this._surfaceHolder.unlockCanvasAndPost(c);
                }
            }
        }
    }
}
