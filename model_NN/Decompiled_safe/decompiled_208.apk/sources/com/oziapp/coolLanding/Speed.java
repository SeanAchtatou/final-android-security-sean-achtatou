package com.oziapp.coolLanding;

public class Speed {
    public static final int DIRECTION_DOWN = 1;
    public static final int DIRECTION_LEFT = -1;
    public static final int DIRECTION_RIGHT = 1;
    public static final int DIRECTION_UP = -1;
    private int xDirection;
    private float xv;
    private int yDirection;
    private float yv;

    public Speed() {
        this.xv = 1.0f;
        this.yv = 1.0f;
        this.xDirection = 1;
        this.yDirection = 1;
        this.xv = 1.0f;
        this.yv = 1.0f;
    }

    public Speed(float xv2, float yv2) {
        this.xv = 1.0f;
        this.yv = 1.0f;
        this.xDirection = 1;
        this.yDirection = 1;
        this.xv = xv2;
        this.yv = yv2;
    }

    public float getXv() {
        return this.xv;
    }

    public void setXv(float xv2) {
        this.xv = xv2;
    }

    public float getYv() {
        return this.yv;
    }

    public void setYv(float yv2) {
        this.yv = yv2;
    }

    public int getxDirection() {
        return this.xDirection;
    }

    public void setxDirection(int xDirection2) {
        this.xDirection = xDirection2;
    }

    public int getyDirection() {
        return this.yDirection;
    }

    public void setyDirection(int yDirection2) {
        this.yDirection = yDirection2;
    }

    public void toggleXDirection() {
        this.xDirection *= -1;
    }

    public void toggleYDirection() {
        this.yDirection *= -1;
    }
}
