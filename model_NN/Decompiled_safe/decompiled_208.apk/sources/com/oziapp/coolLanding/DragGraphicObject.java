package com.oziapp.coolLanding;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import java.util.ArrayList;
import java.util.Random;

public class DragGraphicObject {
    public static final int HELI = 3;
    public static final int RED_PLANE = 2;
    public static final int YELLOW_PLANE = 1;
    public static double gamespeed = 0.5d;
    public static float interpolation;
    int Arrivalx = 0;
    int Arrivaly = 0;
    public int PlaneType;
    public float Xf;
    public float Yf;
    public double angle = 0.0d;
    private Bitmap bitmap;
    public Paint bitmapPaint;
    private int current_lineX = 0;
    private int current_lineY = 0;
    DashPathEffect dashPath;
    private double degree;
    public double deltaX;
    public double deltaY;
    public boolean flgDrawLine;
    public boolean flgGoforLand;
    public boolean flgLandingMode;
    public boolean flgNotLanding;
    public boolean flgNotMOVE_ON_LINE;
    public boolean flgOnSurface;
    public boolean flgTouchEnable = true;
    public boolean flgWarning;
    public boolean flgYrun = false;
    public boolean flgcrash;
    public boolean flgdelta;
    public boolean flgdraged;
    public boolean flglandmsg = false;
    public boolean flgmissedmsg = false;
    public boolean flgpathmsg = false;
    public Bitmap heli1;
    public Bitmap heli2;
    public Bitmap heli3;
    public int helicounter = 0;
    private float i = 0.0f;
    Paint mPaint;
    Path mPath;
    public float mX;
    public float mY;
    Matrix mat = null;
    public String msg = " ";
    public int msgcounter = 0;
    public int n = 0;
    public String name;
    public double planespeed;
    public int points_index;
    public double prevangle = 0.0d;
    Random rand;
    Random rand2;
    public float referenceMoveX;
    public float referenceMoveY;
    public ArrayList<RouteNates> routes;
    public int scale_counter;
    private Speed speed;
    public Paint tPaint;
    private boolean touched;
    public float upX;
    public float upY;
    public DragGraphicObject warningObject;
    private float x;
    public int x_index;
    private float y;

    public DragGraphicObject(Bitmap bitmap2, float mX2, float mY2, float Xf2, float Yf2, int parmplaneType) {
        this.bitmap = bitmap2;
        this.speed = new Speed();
        this.x = mX2;
        this.y = mY2;
        this.mX = mX2;
        this.mY = mY2;
        this.Xf = Xf2;
        this.Yf = Yf2;
        if (parmplaneType == 3) {
            this.planespeed = 0.5d;
        } else {
            this.planespeed = 1.0d;
        }
        this.PlaneType = parmplaneType;
        this.x_index = 0;
        this.i = 0.0f;
        this.scale_counter = 0;
        this.flgLandingMode = false;
        this.flgGoforLand = false;
        this.flgdraged = true;
        this.flgdelta = true;
        this.flgNotLanding = true;
        this.flgOnSurface = false;
        this.flgNotMOVE_ON_LINE = true;
        this.flgWarning = false;
        this.flgcrash = false;
        this.mat = new Matrix();
        this.mPath = new Path();
        this.mPaint = new Paint();
        this.mPaint.setAntiAlias(true);
        this.mPaint.setDither(true);
        this.mPaint.setColor(-1);
        this.mPaint.setStyle(Paint.Style.STROKE);
        this.mPaint.setStrokeCap(Paint.Cap.BUTT);
        this.mPaint.setStrokeJoin(Paint.Join.MITER);
        this.dashPath = new DashPathEffect(new float[]{20.0f, 5.0f}, 1.0f);
        this.mPaint.setPathEffect(this.dashPath);
        this.bitmapPaint = new Paint();
        this.tPaint = new Paint();
        this.tPaint.setColor(-65536);
        this.tPaint.setFakeBoldText(true);
        this.tPaint.setAntiAlias(true);
        this.tPaint.setTextSize(15.0f);
        this.routes = new ArrayList<>();
        this.rand = new Random();
        this.rand2 = new Random();
        this.msg = " ";
    }

    public Bitmap getBitmap() {
        return this.bitmap;
    }

    public void setBitmap(Bitmap bitmap2) {
        this.bitmap = bitmap2;
    }

    public float getX() {
        return this.x;
    }

    public void setX(float x2) {
        this.x = x2;
    }

    public float getY() {
        return this.y;
    }

    public void setY(float y2) {
        this.y = y2;
    }

    public double getAngle() {
        return this.angle;
    }

    public void setAngle(double angle2) {
        this.angle = angle2;
    }

    public boolean isTouched() {
        return this.touched;
    }

    public void setTouched(boolean touched2) {
        this.touched = touched2;
    }

    public Speed getSpeed() {
        return this.speed;
    }

    public void setSpeed(Speed speed2) {
        this.speed = speed2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public void draw(Canvas canvas) {
        try {
            if (!this.flgLandingMode) {
                this.mat = new Matrix();
                if (this.flgDrawLine) {
                    this.mPaint.setStrokeWidth(0.0f);
                }
                canvas.drawPath(this.mPath, this.mPaint);
                this.mat.postRotate((float) getAngle(), this.mX, this.mY);
                Bitmap bitmap2 = Bitmap.createBitmap(this.bitmap, 0, 0, this.bitmap.getWidth(), this.bitmap.getHeight(), this.mat, true);
                canvas.drawBitmap(bitmap2, this.x - ((float) (bitmap2.getWidth() / 2)), this.y - ((float) (bitmap2.getHeight() / 2)), this.bitmapPaint);
                return;
            }
            this.mat = new Matrix();
            if (SelectMap.map_Select == R.drawable.background1) {
                this.mat.setRotate(0.0f);
                if (this.PlaneType == 1 || this.PlaneType == 2) {
                    if (this.scale_counter >= 0 && this.scale_counter < 15) {
                        this.mat.postScale(0.7f, 0.7f, this.x + ((float) this.bitmap.getWidth()), this.y + ((float) this.bitmap.getHeight()));
                    } else if (this.scale_counter >= 15 && this.scale_counter < 30) {
                        this.mat.postScale(0.6f, 0.6f, this.x + ((float) this.bitmap.getWidth()), this.y + ((float) this.bitmap.getHeight()));
                    } else if (this.scale_counter >= 30 && this.scale_counter < 45) {
                        this.mat.postScale(0.5f, 0.5f, this.x + ((float) this.bitmap.getWidth()), this.y + ((float) this.bitmap.getHeight()));
                    } else if (this.scale_counter >= 45) {
                        this.mat.postScale(0.4f, 0.4f, this.x + ((float) this.bitmap.getWidth()), this.y + ((float) this.bitmap.getHeight()));
                    }
                    this.scale_counter++;
                }
            } else if (SelectMap.map_Select == R.drawable.mountainjoy) {
                if (this.PlaneType == 1) {
                    this.mat.setRotate(180.0f);
                } else if (this.PlaneType == 2) {
                    this.mat.setRotate(0.0f);
                }
                if (this.PlaneType == 1 || this.PlaneType == 2) {
                    if (this.scale_counter >= 0 && this.scale_counter < 15) {
                        this.mat.postScale(0.7f, 0.7f, (this.x + ((float) this.bitmap.getWidth())) / 2.0f, (this.y + ((float) this.bitmap.getHeight())) / 2.0f);
                    } else if (this.scale_counter >= 15 && this.scale_counter < 30) {
                        this.mat.postScale(0.6f, 0.6f, (this.x + ((float) this.bitmap.getWidth())) / 2.0f, (this.y + ((float) this.bitmap.getHeight())) / 2.0f);
                    } else if (this.scale_counter >= 30 && this.scale_counter < 45) {
                        this.mat.postScale(0.5f, 0.5f, (this.x + ((float) this.bitmap.getWidth())) / 2.0f, (this.y + ((float) this.bitmap.getHeight())) / 2.0f);
                    } else if (this.scale_counter >= 45) {
                        this.mat.postScale(0.4f, 0.4f, (this.x + ((float) this.bitmap.getWidth())) / 2.0f, (this.y + ((float) this.bitmap.getHeight())) / 2.0f);
                    }
                    this.scale_counter++;
                }
            } else if (SelectMap.map_Select == R.drawable.meadowolives) {
                if (this.PlaneType == 1) {
                    this.mat.setRotate(90.0f);
                } else if (this.PlaneType == 2) {
                    this.mat.setRotate(210.0f);
                }
                if (this.PlaneType == 1 || this.PlaneType == 2) {
                    if (this.scale_counter >= 0 && this.scale_counter < 15) {
                        this.mat.postScale(0.7f, 0.7f, this.x + ((float) this.bitmap.getWidth()), this.y + ((float) this.bitmap.getHeight()));
                    } else if (this.scale_counter >= 15 && this.scale_counter < 30) {
                        this.mat.postScale(0.6f, 0.6f, this.x + ((float) this.bitmap.getWidth()), this.y + ((float) this.bitmap.getHeight()));
                    } else if (this.scale_counter >= 30 && this.scale_counter < 45) {
                        this.mat.postScale(0.5f, 0.5f, this.x + ((float) this.bitmap.getWidth()), this.y + ((float) this.bitmap.getHeight()));
                    } else if (this.scale_counter >= 45) {
                        this.mat.postScale(0.4f, 0.4f, this.x + ((float) this.bitmap.getWidth()), this.y + ((float) this.bitmap.getHeight()));
                    }
                    this.scale_counter++;
                }
            } else if (SelectMap.map_Select == R.drawable.maritimelands) {
                if (this.PlaneType == 1) {
                    this.mat.setRotate(0.0f);
                } else if (this.PlaneType == 2) {
                    this.mat.setRotate(-90.0f);
                }
                if (this.PlaneType == 1 || this.PlaneType == 2) {
                    if (this.scale_counter >= 0 && this.scale_counter < 15) {
                        this.mat.postScale(0.7f, 0.7f, this.x + ((float) this.bitmap.getWidth()), this.y + ((float) this.bitmap.getHeight()));
                    } else if (this.scale_counter >= 15 && this.scale_counter < 30) {
                        this.mat.postScale(0.6f, 0.6f, this.x + ((float) this.bitmap.getWidth()), this.y + ((float) this.bitmap.getHeight()));
                    } else if (this.scale_counter >= 30 && this.scale_counter < 45) {
                        this.mat.postScale(0.5f, 0.5f, this.x + ((float) this.bitmap.getWidth()), this.y + ((float) this.bitmap.getHeight()));
                    } else if (this.scale_counter >= 45) {
                        this.mat.postScale(0.4f, 0.4f, this.x + ((float) this.bitmap.getWidth()), this.y + ((float) this.bitmap.getHeight()));
                    }
                    this.scale_counter++;
                }
            } else if (SelectMap.map_Select == R.drawable.kindamixed) {
                if (this.PlaneType == 1) {
                    this.mat.setRotate(180.0f);
                }
                if (this.PlaneType == 2) {
                    this.mat.setRotate(90.0f);
                }
                if (this.PlaneType == 1 || this.PlaneType == 2) {
                    if (this.scale_counter >= 0 && this.scale_counter < 15) {
                        this.mat.postScale(0.7f, 0.7f, this.x + ((float) this.bitmap.getWidth()), this.y + ((float) this.bitmap.getHeight()));
                    } else if (this.scale_counter >= 15 && this.scale_counter < 30) {
                        this.mat.postScale(0.6f, 0.6f, this.x + ((float) this.bitmap.getWidth()), this.y + ((float) this.bitmap.getHeight()));
                    } else if (this.scale_counter >= 30 && this.scale_counter < 45) {
                        this.mat.postScale(0.5f, 0.5f, this.x + ((float) this.bitmap.getWidth()), this.y + ((float) this.bitmap.getHeight()));
                    } else if (this.scale_counter >= 45) {
                        this.mat.postScale(0.4f, 0.4f, this.x + ((float) this.bitmap.getWidth()), this.y + ((float) this.bitmap.getHeight()));
                    }
                    this.scale_counter++;
                }
            }
            if (this.PlaneType == 3) {
                if (this.scale_counter >= 0 && this.scale_counter < 20) {
                    this.mat.postScale(0.7f, 0.7f, this.x, this.y);
                } else if (this.scale_counter >= 20) {
                    this.mat.postScale(0.5f, 0.5f, this.mX + 5.0f, this.mY);
                }
                this.scale_counter++;
            }
            Bitmap bitmap3 = Bitmap.createBitmap(this.bitmap, 0, 0, this.bitmap.getWidth(), this.bitmap.getHeight(), this.mat, true);
            canvas.drawBitmap(bitmap3, this.x - ((float) (bitmap3.getWidth() / 2)), this.y - ((float) (bitmap3.getHeight() / 2)), this.bitmapPaint);
        } catch (Exception e) {
        }
    }

    public void update(int screen_width, int screen_height) {
        float Yi;
        float Yi2;
        this.planespeed = gamespeed;
        if (this.PlaneType == 3) {
            this.planespeed = gamespeed / 2.0d;
        }
        if (!this.flgLandingMode) {
            if (!this.routes.isEmpty()) {
                this.mPath.reset();
                this.mPath.moveTo(this.mX, this.mY);
                float linex = this.mX;
                float liney = this.mY;
                for (int i2 = this.n; i2 < this.routes.size(); i2++) {
                    try {
                        RouteNates points = this.routes.get(i2);
                        this.current_lineX = points.posX;
                        this.current_lineY = points.posY;
                        this.mPath.quadTo(linex, liney, (((float) this.current_lineX) + linex) / 2.0f, (((float) this.current_lineY) + liney) / 2.0f);
                        linex = (float) this.current_lineX;
                        liney = (float) this.current_lineY;
                    } catch (IndexOutOfBoundsException e) {
                    }
                }
            }
            if (this.mX >= this.Xf - 1.0f && this.mX <= this.Xf + 1.0f && !this.flgLandingMode) {
                if (this.x_index < this.routes.size()) {
                    RouteNates epoint = this.routes.get(this.x_index);
                    this.Xf = (float) epoint.posX;
                    this.Yf = (float) epoint.posY;
                    this.flgdelta = true;
                    if (this.x_index > 0) {
                        this.n++;
                    }
                    this.x_index++;
                } else {
                    this.mPath.reset();
                    this.routes.clear();
                    this.flgNotMOVE_ON_LINE = true;
                    setTouched(false);
                    this.n = 0;
                }
            }
            if (this.flgdelta) {
                this.deltaX = (double) (this.Xf - this.mX);
                this.deltaY = (double) (this.Yf - this.mY);
                this.degree = Math.atan2(this.deltaY, this.deltaX);
                this.angle = (this.degree * 180.0d) / 3.14d;
                this.flgdelta = false;
            }
            if (this.deltaX > 0.0d) {
                if (this.deltaY > 0.0d) {
                    float d2 = (float) (((double) ((float) Math.sin(this.degree))) * this.planespeed);
                    Yi2 = this.mY;
                    this.mY += d2;
                    setY(this.mY);
                } else {
                    float d22 = (float) (((double) ((float) (Math.sin(this.degree) * -1.0d))) * this.planespeed);
                    Yi2 = this.mY;
                    this.mY -= d22;
                    setY(this.mY);
                }
                float d1 = (float) (((double) ((float) Math.cos(this.degree))) * this.planespeed);
                float Xi = this.mX;
                this.mX += d1;
                setX(this.mX);
                this.degree = Math.atan2((double) (this.mY - Yi2), (double) (this.mX - Xi));
                this.angle = (this.degree * 180.0d) / 3.14d;
            } else if (this.deltaX < 0.0d) {
                if (this.deltaY > 0.0d) {
                    float d23 = (float) (((double) ((float) Math.sin(this.degree))) * this.planespeed);
                    Yi = this.mY;
                    this.mY += d23;
                    setY(this.mY);
                } else {
                    float d24 = (float) (((double) ((float) (Math.sin(this.degree) * -1.0d))) * this.planespeed);
                    Yi = this.mY;
                    this.mY -= d24;
                    setY(this.mY);
                }
                float d12 = (float) (((double) ((float) Math.cos(this.degree))) * this.planespeed);
                float Xi2 = this.mX;
                this.mX -= -1.0f * d12;
                setX(this.mX);
                this.degree = Math.atan2((double) (this.mY - Yi), (double) (this.mX - Xi2));
                this.angle = (this.degree * 180.0d) / 3.14d;
            }
            if (this.flgOnSurface) {
                if (this.x + ((float) (this.bitmap.getWidth() / 2)) >= ((float) (screen_width + 25))) {
                    this.Xf = 0.0f;
                    this.Yf = (float) this.rand.nextInt(320);
                    this.flgdelta = true;
                }
                if (this.x - ((float) (this.bitmap.getWidth() / 2)) <= -25.0f) {
                    this.Xf = 320.0f;
                    this.Yf = (float) this.rand2.nextInt(screen_height);
                    this.flgdelta = true;
                }
                if (this.y + ((float) (this.bitmap.getHeight() / 2)) >= ((float) (screen_height + 25))) {
                    if (this.mPath != null) {
                        this.mPath.reset();
                    }
                    if (this.routes != null) {
                        this.routes.clear();
                    }
                    this.Xf = (float) this.rand2.nextInt(screen_width);
                    this.Yf = 10.0f;
                    this.flgdelta = true;
                }
                if (this.y - ((float) (this.bitmap.getHeight() / 2)) <= -25.0f) {
                    this.Xf = (float) this.rand.nextInt(500);
                    this.Yf = 320.0f;
                    this.flgdelta = true;
                }
            }
        }
        if (!this.flgLandingMode) {
            return;
        }
        if (SelectMap.map_Select == R.drawable.background1) {
            this.x = getX() + (this.speed.getXv() * ((float) this.speed.getxDirection()));
        } else if (SelectMap.map_Select == R.drawable.mountainjoy) {
            if (this.PlaneType == 1) {
                this.x = getX() - (this.speed.getXv() * ((float) this.speed.getxDirection()));
            } else if (this.PlaneType == 2) {
                this.x = getX() + (this.speed.getXv() * ((float) this.speed.getxDirection()));
            }
        } else if (SelectMap.map_Select == R.drawable.meadowolives) {
            if (this.PlaneType == 1) {
                this.y = getY() + (this.speed.getYv() * ((float) this.speed.getyDirection()));
            } else if (this.PlaneType == 2) {
                this.deltaX = (double) (this.Xf - this.mX);
                this.deltaY = (double) (this.Yf - this.mY);
                this.degree = Math.atan2(this.deltaY, this.deltaX);
                this.angle = (this.degree * 180.0d) / 3.14d;
                if (this.deltaX > 0.0d) {
                    if (this.deltaY > 0.0d) {
                        this.mY += (float) (((double) ((float) Math.sin(this.degree))) * this.planespeed);
                        setY(this.mY);
                    } else {
                        this.mY -= (float) (((double) ((float) (Math.sin(this.degree) * -1.0d))) * this.planespeed);
                        setY(this.mY);
                    }
                    this.mX += (float) (((double) ((float) Math.cos(this.degree))) * this.planespeed);
                    setX(this.mX);
                } else if (this.deltaX < 0.0d) {
                    if (this.deltaY > 0.0d) {
                        this.mY += (float) (((double) ((float) Math.sin(this.degree))) * this.planespeed);
                        setY(this.mY);
                    } else {
                        this.mY -= (float) (((double) ((float) (Math.sin(this.degree) * -1.0d))) * this.planespeed);
                        setY(this.mY);
                    }
                    this.mX -= -1.0f * ((float) (((double) ((float) Math.cos(this.degree))) * this.planespeed));
                    setX(this.mX);
                }
            }
        } else if (SelectMap.map_Select == R.drawable.maritimelands) {
            if (this.PlaneType == 1) {
                this.x = getX() + (this.speed.getXv() * ((float) this.speed.getxDirection()));
            } else if (this.PlaneType == 2) {
                this.y = getY() - (this.speed.getYv() * ((float) this.speed.getyDirection()));
            }
        } else if (SelectMap.map_Select != R.drawable.kindamixed) {
        } else {
            if (this.PlaneType == 2) {
                this.y = getY() + (this.speed.getYv() * ((float) this.speed.getyDirection()));
            } else if (this.PlaneType == 1) {
                this.x = getX() - (this.speed.getXv() * ((float) this.speed.getxDirection()));
            }
        }
    }

    public void draw_Notification(Canvas cnv, Bitmap arrivalsmall, Bitmap arrivallarge) {
        if (this.i >= 0.0f && this.i <= 15.0f) {
            cnv.drawBitmap(arrivalsmall, (float) (this.Arrivalx - (arrivalsmall.getWidth() / 2)), (float) (this.Arrivaly - (arrivalsmall.getHeight() / 2)), (Paint) null);
            this.i += 1.0f;
        } else if (this.i < 15.0f || this.i > 30.0f) {
            this.i = 0.0f;
        } else {
            cnv.drawBitmap(arrivallarge, (float) (this.Arrivalx - (arrivallarge.getWidth() / 2)), (float) (this.Arrivaly - (arrivallarge.getHeight() / 2)), (Paint) null);
            this.i += 1.0f;
        }
    }

    public void handleActionDown(int eventX, int eventY) {
        if (((float) eventX) < (this.x - ((float) (this.bitmap.getWidth() / 2))) - 10.0f || ((float) eventX) > this.x + ((float) (this.bitmap.getWidth() / 2)) + 10.0f) {
            setTouched(false);
        } else if (((float) eventY) < (this.y - ((float) (this.bitmap.getHeight() / 2))) - 10.0f || ((float) eventY) > this.y + ((float) (this.bitmap.getHeight() / 2)) + 10.0f) {
            setTouched(false);
        } else {
            setTouched(true);
        }
    }
}
