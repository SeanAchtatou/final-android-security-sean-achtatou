package org.codehaus.jackson.io;

public final class NumberInput {
    static final long L_BILLION = 1000000000;
    static final String MAX_LONG_STR = String.valueOf(Long.MAX_VALUE);
    static final String MIN_LONG_STR_NO_SIGN = String.valueOf(Long.MIN_VALUE).substring(1);

    public static final int parseInt(char[] digitChars, int offset, int len) {
        int num = digitChars[offset] - '0';
        int len2 = len + offset;
        int offset2 = offset + 1;
        if (offset2 >= len2) {
            return num;
        }
        int num2 = (num * 10) + (digitChars[offset2] - '0');
        int offset3 = offset2 + 1;
        if (offset3 >= len2) {
            return num2;
        }
        int num3 = (num2 * 10) + (digitChars[offset3] - '0');
        int offset4 = offset3 + 1;
        if (offset4 >= len2) {
            return num3;
        }
        int num4 = (num3 * 10) + (digitChars[offset4] - '0');
        int offset5 = offset4 + 1;
        if (offset5 >= len2) {
            return num4;
        }
        int num5 = (num4 * 10) + (digitChars[offset5] - '0');
        int offset6 = offset5 + 1;
        if (offset6 >= len2) {
            return num5;
        }
        int num6 = (num5 * 10) + (digitChars[offset6] - '0');
        int offset7 = offset6 + 1;
        if (offset7 >= len2) {
            return num6;
        }
        int num7 = (num6 * 10) + (digitChars[offset7] - '0');
        int offset8 = offset7 + 1;
        if (offset8 >= len2) {
            return num7;
        }
        int num8 = (num7 * 10) + (digitChars[offset8] - '0');
        int offset9 = offset8 + 1;
        if (offset9 < len2) {
            return (num8 * 10) + (digitChars[offset9] - '0');
        }
        return num8;
    }

    public static final long parseLong(char[] digitChars, int offset, int len) {
        int len1 = len - 9;
        return ((long) parseInt(digitChars, offset + len1, 9)) + (((long) parseInt(digitChars, offset, len1)) * L_BILLION);
    }

    public static final boolean inLongRange(char[] digitChars, int offset, int len, boolean negative) {
        String cmpStr = negative ? MIN_LONG_STR_NO_SIGN : MAX_LONG_STR;
        int cmpLen = cmpStr.length();
        if (len < cmpLen) {
            return true;
        }
        if (len > cmpLen) {
            return false;
        }
        for (int i = 0; i < cmpLen; i++) {
            if (digitChars[offset + i] > cmpStr.charAt(i)) {
                return false;
            }
        }
        return true;
    }

    public static final boolean inLongRange(String numberStr, boolean negative) {
        String cmpStr = negative ? MIN_LONG_STR_NO_SIGN : MAX_LONG_STR;
        int cmpLen = cmpStr.length();
        int actualLen = numberStr.length();
        if (actualLen < cmpLen) {
            return true;
        }
        if (actualLen > cmpLen) {
            return false;
        }
        for (int i = 0; i < cmpLen; i++) {
            if (numberStr.charAt(i) > cmpStr.charAt(i)) {
                return false;
            }
        }
        return true;
    }
}
