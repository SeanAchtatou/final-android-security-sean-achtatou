package com.clandawson.zcrazymath;

import java.util.Random;

public class Brainz {
    private int[] enabled_numbers = new int[10];
    private int[] enabled_operators = new int[4];
    final Random myRandom = new Random();
    private int number_of_enabled_numbers = 10;
    private int number_of_enabled_operators = 1;

    enum eOperator {
        SUB,
        ADD,
        MUL,
        DIV
    }

    public Brainz() {
        for (int i = 0; i < 10; i++) {
            this.enabled_numbers[i] = 1;
        }
        this.enabled_operators[eOperator.SUB.ordinal()] = 0;
        this.enabled_operators[eOperator.ADD.ordinal()] = 1;
        this.enabled_operators[eOperator.MUL.ordinal()] = 0;
        this.enabled_operators[eOperator.DIV.ordinal()] = 0;
    }

    public void RandomGame() {
        this.number_of_enabled_numbers = 0;
        while (this.number_of_enabled_numbers < 2) {
            this.number_of_enabled_numbers = 0;
            for (int i = 0; i < 10; i++) {
                this.enabled_numbers[i] = this.myRandom.nextInt(2);
                if (this.enabled_numbers[i] == 1) {
                    this.number_of_enabled_numbers++;
                }
            }
        }
        this.number_of_enabled_operators = 0;
        while (this.number_of_enabled_operators == 0) {
            for (int i2 = 0; i2 < 4; i2++) {
                this.enabled_operators[i2] = this.myRandom.nextInt(2);
                if (this.enabled_operators[i2] == 1) {
                    this.number_of_enabled_operators++;
                }
            }
        }
    }

    public ZCrazyEquation NextEquation() {
        ZCrazyEquation equation = new ZCrazyEquation();
        int argument1 = 0;
        int argument2 = 0;
        int operator = eOperator.ADD.ordinal();
        int answer = 0;
        int index = this.myRandom.nextInt(this.number_of_enabled_operators) + 1;
        int count = 0;
        for (int i = 0; i < 4; i++) {
            if (this.enabled_operators[i] == 1 && (count = count + 1) == index) {
                operator = i;
            }
        }
        while (true) {
            int index2 = this.myRandom.nextInt(this.number_of_enabled_numbers) + 1;
            int count2 = 0;
            for (int i2 = 0; i2 < 10; i2++) {
                if (this.enabled_numbers[i2] == 1 && (count2 = count2 + 1) == index2) {
                    argument1 = i2;
                }
            }
            int index3 = this.myRandom.nextInt(this.number_of_enabled_numbers) + 1;
            int count3 = 0;
            for (int i3 = 0; i3 < 10; i3++) {
                if (this.enabled_numbers[i3] == 1 && (count3 = count3 + 1) == index3) {
                    argument2 = i3;
                }
            }
            if (operator == eOperator.DIV.ordinal() && this.number_of_enabled_numbers == 1 && argument1 == 0) {
                argument2 = 1;
            }
            if ((operator != eOperator.SUB.ordinal() || argument1 - argument2 >= 0) && !(operator == eOperator.DIV.ordinal() && argument2 == 0)) {
            }
        }
        if (operator == eOperator.SUB.ordinal()) {
            answer = argument1 - argument2;
            equation.operator = "-";
        }
        if (operator == eOperator.ADD.ordinal()) {
            answer = argument1 + argument2;
            equation.operator = "+";
        }
        if (operator == eOperator.MUL.ordinal()) {
            answer = argument1 * argument2;
            equation.operator = "x";
        }
        if (operator == eOperator.DIV.ordinal()) {
            answer = argument1;
            argument1 = answer * argument2;
            equation.operator = "/";
        }
        equation.argument1 = Integer.toString(argument1);
        equation.argument2 = Integer.toString(argument2);
        equation.answer = Integer.toString(answer);
        return equation;
    }

    public int ToggleEnabledNumber(int znumber) {
        if (znumber < 0 || znumber > 9) {
            return 0;
        }
        if (this.enabled_numbers[znumber] == 1) {
            if (this.number_of_enabled_numbers > 1) {
                this.enabled_numbers[znumber] = 0;
                this.number_of_enabled_numbers--;
            }
        } else if (this.enabled_numbers[znumber] == 0) {
            this.enabled_numbers[znumber] = 1;
            this.number_of_enabled_numbers++;
        }
        return this.enabled_numbers[znumber];
    }

    public int ToggleEnabledOperator(int zoperator) {
        if (zoperator < 0 || zoperator > 3) {
            return 0;
        }
        if (this.enabled_operators[zoperator] == 1) {
            if (this.number_of_enabled_operators > 1) {
                this.enabled_operators[zoperator] = 0;
                this.number_of_enabled_operators--;
            }
        } else if (this.enabled_operators[zoperator] == 0) {
            this.enabled_operators[zoperator] = 1;
            this.number_of_enabled_operators++;
        }
        return this.enabled_operators[zoperator];
    }

    public int IsNumberEnabled(int znumber) {
        if (znumber < 0 || znumber > 9) {
            return 0;
        }
        return this.enabled_numbers[znumber];
    }

    public int IsOperatorEnabled(int zop) {
        if (zop < 0 || zop > 3) {
            return 0;
        }
        return this.enabled_operators[zop];
    }
}
