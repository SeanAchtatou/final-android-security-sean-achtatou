package com.clandawson.zcrazymath;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import java.util.ArrayList;

public class zcrazyMath_choosePlayer extends Activity {
    private View.OnClickListener HandleButtonAddPlayer = new View.OnClickListener() {
        public void onClick(View v) {
            String UserName = zcrazyMath_choosePlayer.this.et_NewUser.getText().toString();
            zcrazyMath_choosePlayer.this.et_NewUser.setText("");
            zcrazyMath_choosePlayer.this.tv_debug.setText("Adding user name " + UserName);
            zcrazyMath_choosePlayer.this.rb_players.add(new RadioButton(v.getContext()));
            ((RadioButton) zcrazyMath_choosePlayer.this.rb_players.get(zcrazyMath_choosePlayer.this.rb_players.size() - 1)).setText(UserName);
            zcrazyMath_choosePlayer.this.rg_ChoosePlayer.addView((View) zcrazyMath_choosePlayer.this.rb_players.get(zcrazyMath_choosePlayer.this.rb_players.size() - 1));
            ((RadioButton) zcrazyMath_choosePlayer.this.rb_players.get(zcrazyMath_choosePlayer.this.rb_players.size() - 1)).setChecked(true);
        }
    };
    private View.OnClickListener HandleButtonOk = new View.OnClickListener() {
        public void onClick(View v) {
            String UserName = ((RadioButton) zcrazyMath_choosePlayer.this.findViewById(zcrazyMath_choosePlayer.this.rg_ChoosePlayer.getCheckedRadioButtonId())).getText().toString();
            zcrazyMath_choosePlayer.this.tv_debug.setText("Going with user name: " + UserName);
            Intent resultIntent = new Intent();
            resultIntent.putExtra("PLAYER_NAME", UserName);
            zcrazyMath_choosePlayer.this.setResult(-1, resultIntent);
            zcrazyMath_choosePlayer.this.finish();
        }
    };
    private View.OnClickListener HandleButtonRemovePlayer = new View.OnClickListener() {
        public void onClick(View v) {
            RadioButton rb_wip = (RadioButton) zcrazyMath_choosePlayer.this.findViewById(zcrazyMath_choosePlayer.this.rg_ChoosePlayer.getCheckedRadioButtonId());
            String UserName = rb_wip.getText().toString();
            zcrazyMath_choosePlayer.this.rg_ChoosePlayer.removeView(rb_wip);
            int count = 0;
            while (true) {
                if (count >= zcrazyMath_choosePlayer.this.rb_players.size()) {
                    break;
                } else if (((RadioButton) zcrazyMath_choosePlayer.this.rb_players.get(count)).getText().toString() == UserName) {
                    zcrazyMath_choosePlayer.this.rb_players.remove(count);
                    break;
                } else {
                    count++;
                }
            }
            if (zcrazyMath_choosePlayer.this.rb_players.size() == 0) {
                zcrazyMath_choosePlayer.this.rb_players.add(new RadioButton(v.getContext()));
                ((RadioButton) zcrazyMath_choosePlayer.this.rb_players.get(zcrazyMath_choosePlayer.this.rb_players.size() - 1)).setText("Anonymous");
                zcrazyMath_choosePlayer.this.rg_ChoosePlayer.addView((RadioButton) zcrazyMath_choosePlayer.this.rb_players.get(0));
            }
            ((RadioButton) zcrazyMath_choosePlayer.this.rb_players.get(zcrazyMath_choosePlayer.this.rb_players.size() - 1)).setChecked(true);
            zcrazyMath_choosePlayer.this.tv_debug.setText("Removing user name " + UserName);
        }
    };
    private Button btn_AddPlayer;
    private Button btn_Ok;
    private Button btn_RemovePlayer;
    /* access modifiers changed from: private */
    public EditText et_NewUser;
    /* access modifiers changed from: private */
    public ArrayList<RadioButton> rb_players;
    /* access modifiers changed from: private */
    public RadioGroup rg_ChoosePlayer;
    /* access modifiers changed from: private */
    public TextView tv_debug;

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.chooseplayer);
        this.btn_Ok = (Button) findViewById(R.id.btn_ok);
        this.btn_AddPlayer = (Button) findViewById(R.id.btn_add_player);
        this.btn_RemovePlayer = (Button) findViewById(R.id.btn_remove_player);
        this.btn_Ok.setOnClickListener(this.HandleButtonOk);
        this.btn_AddPlayer.setOnClickListener(this.HandleButtonAddPlayer);
        this.btn_RemovePlayer.setOnClickListener(this.HandleButtonRemovePlayer);
        this.tv_debug = (TextView) findViewById(R.id.tv_debug);
        this.rb_players = new ArrayList<>();
        this.rg_ChoosePlayer = (RadioGroup) findViewById(R.id.rg_choose_player);
        this.et_NewUser = (EditText) findViewById(R.id.et_new_user);
        this.rb_players.add(new RadioButton(this));
        this.rb_players.get(this.rb_players.size() - 1).setText("Anonymous");
        this.rb_players.add(new RadioButton(this));
        this.rb_players.get(this.rb_players.size() - 1).setText("Nicole");
        this.rb_players.add(new RadioButton(this));
        this.rb_players.get(this.rb_players.size() - 1).setText("Allison");
        for (int count = 0; count < this.rb_players.size(); count++) {
            this.rg_ChoosePlayer.addView(this.rb_players.get(count));
        }
        this.rb_players.get(this.rb_players.size() - 1).setChecked(true);
    }
}
