package com.clandawson.zcrazymath;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class zcrazyMath extends Activity {
    private static final int INTENT_CHOOSE_PLAYER = 0;
    private static final int INTENT_PLAY_GAME = 1;
    private static final int INTENT_SHOW_SCORES = 2;
    private View.OnClickListener HandleButtonChoosePlayer = new View.OnClickListener() {
        public void onClick(View v) {
            zcrazyMath.this.startActivityForResult(new Intent(v.getContext(), zcrazyMath_choosePlayer.class), zcrazyMath.INTENT_CHOOSE_PLAYER);
        }
    };
    private View.OnClickListener HandleButtonPlayGame = new View.OnClickListener() {
        public void onClick(View v) {
            Intent myIntent = new Intent(v.getContext(), zcrazyMath_play.class);
            myIntent.putExtra("PLAYER_NAME", zcrazyMath.this.player_name);
            zcrazyMath.this.startActivityForResult(myIntent, 1);
        }
    };
    private View.OnClickListener HandleButtonShowScores = new View.OnClickListener() {
        public void onClick(View v) {
        }
    };
    private Button btn_ChoosePlayer;
    private Button btn_Play;
    private Button btn_ShowScores;
    String player_name = "Player1";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.btn_Play = (Button) findViewById(R.id.btn_play);
        this.btn_ChoosePlayer = (Button) findViewById(R.id.btn_choose_player);
        this.btn_ShowScores = (Button) findViewById(R.id.btn_show_hi_score);
        this.btn_Play.setOnClickListener(this.HandleButtonPlayGame);
        this.btn_ChoosePlayer.setOnClickListener(this.HandleButtonChoosePlayer);
        this.btn_ShowScores.setOnClickListener(this.HandleButtonShowScores);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case INTENT_CHOOSE_PLAYER /*0*/:
                if (resultCode == -1) {
                    this.player_name = data.getStringExtra("PLAYER_NAME");
                    return;
                }
                return;
            default:
                return;
        }
    }
}
