package com.clandawson.zcrazymath;

import android.app.Activity;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.clandawson.zcrazymath.Brainz;

public class zcrazyMath_play extends Activity {
    private static int NUMBER_OF_ROUNDS = 25;
    /* access modifiers changed from: private */
    public static int TIME_LIMIT = 60;
    /* access modifiers changed from: private */
    public Button btn_PlayAgain;
    private Button btn_pntr;
    private mCountDownTimer cd_tmr = new mCountDownTimer((long) (TIME_LIMIT * 1000), 1000);
    private int cumulative_answer;
    private int current_equation;
    private ZCrazyEquation[] equations = new ZCrazyEquation[NUMBER_OF_ROUNDS];
    /* access modifiers changed from: private */
    public eGameState game_state;
    private Brainz mBrainz;
    private String player_name = "";
    /* access modifiers changed from: private */
    public TextView tv_game_status;
    private TextView tv_hello;
    private TextView tv_next;
    private TextView tv_prev;
    private TextView tv_question;
    private TextView tv_this;
    /* access modifiers changed from: private */
    public TextView tv_time;

    private enum eGameState {
        RUNNING,
        PAUSED,
        TIMED_OUT,
        FINISHED
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.player_name = extras.getString("PLAYER_NAME");
            if (this.player_name != "") {
                this.player_name = String.valueOf(this.player_name) + ": ";
            }
        }
        setContentView((int) R.layout.playgame);
        this.btn_PlayAgain = (Button) findViewById(R.id.btn_play_again);
        this.tv_hello = (TextView) findViewById(R.id.tv_hello);
        this.tv_prev = (TextView) findViewById(R.id.tv_prev);
        this.tv_next = (TextView) findViewById(R.id.tv_next);
        this.tv_this = (TextView) findViewById(R.id.tv_this);
        this.tv_question = (TextView) findViewById(R.id.tv_question);
        this.tv_game_status = (TextView) findViewById(R.id.tv_game_status);
        this.tv_time = (TextView) findViewById(R.id.tv_time);
        this.tv_hello.setText(String.valueOf(this.player_name) + ((Object) this.tv_hello.getText()));
        this.tv_prev.setText("");
        this.tv_next.setText("");
        this.tv_this.setText("");
        this.game_state = eGameState.PAUSED;
        this.mBrainz = new Brainz();
        this.mBrainz.RandomGame();
        StartNewGame();
    }

    public void StartNewGame() {
        this.current_equation = 0;
        this.cumulative_answer = -1;
        this.cd_tmr.cancel();
        this.game_state = eGameState.PAUSED;
        this.tv_game_status.setText("Answer below.  Answer to start!!!");
        this.btn_PlayAgain.setEnabled(false);
        for (int i = 0; i < NUMBER_OF_ROUNDS; i++) {
            this.equations[i] = this.mBrainz.NextEquation();
        }
        UpdateStrings();
        this.btn_pntr = (Button) findViewById(R.id.btn_enable_0);
        if (this.mBrainz.IsNumberEnabled(0) == 0) {
            this.btn_pntr.setTextColor(-1);
        } else {
            this.btn_pntr.setTextColor(-16777216);
        }
        this.btn_pntr = (Button) findViewById(R.id.btn_enable_1);
        if (this.mBrainz.IsNumberEnabled(1) == 0) {
            this.btn_pntr.setTextColor(-1);
        } else {
            this.btn_pntr.setTextColor(-16777216);
        }
        this.btn_pntr = (Button) findViewById(R.id.btn_enable_2);
        if (this.mBrainz.IsNumberEnabled(2) == 0) {
            this.btn_pntr.setTextColor(-1);
        } else {
            this.btn_pntr.setTextColor(-16777216);
        }
        this.btn_pntr = (Button) findViewById(R.id.btn_enable_3);
        if (this.mBrainz.IsNumberEnabled(3) == 0) {
            this.btn_pntr.setTextColor(-1);
        } else {
            this.btn_pntr.setTextColor(-16777216);
        }
        this.btn_pntr = (Button) findViewById(R.id.btn_enable_4);
        if (this.mBrainz.IsNumberEnabled(4) == 0) {
            this.btn_pntr.setTextColor(-1);
        } else {
            this.btn_pntr.setTextColor(-16777216);
        }
        this.btn_pntr = (Button) findViewById(R.id.btn_enable_5);
        if (this.mBrainz.IsNumberEnabled(5) == 0) {
            this.btn_pntr.setTextColor(-1);
        } else {
            this.btn_pntr.setTextColor(-16777216);
        }
        this.btn_pntr = (Button) findViewById(R.id.btn_enable_6);
        if (this.mBrainz.IsNumberEnabled(6) == 0) {
            this.btn_pntr.setTextColor(-1);
        } else {
            this.btn_pntr.setTextColor(-16777216);
        }
        this.btn_pntr = (Button) findViewById(R.id.btn_enable_7);
        if (this.mBrainz.IsNumberEnabled(7) == 0) {
            this.btn_pntr.setTextColor(-1);
        } else {
            this.btn_pntr.setTextColor(-16777216);
        }
        this.btn_pntr = (Button) findViewById(R.id.btn_enable_8);
        if (this.mBrainz.IsNumberEnabled(8) == 0) {
            this.btn_pntr.setTextColor(-1);
        } else {
            this.btn_pntr.setTextColor(-16777216);
        }
        this.btn_pntr = (Button) findViewById(R.id.btn_enable_9);
        if (this.mBrainz.IsNumberEnabled(9) == 0) {
            this.btn_pntr.setTextColor(-1);
        } else {
            this.btn_pntr.setTextColor(-16777216);
        }
        this.btn_pntr = (Button) findViewById(R.id.btn_enable_sub);
        if (this.mBrainz.IsOperatorEnabled(Brainz.eOperator.SUB.ordinal()) == 0) {
            this.btn_pntr.setTextColor(-1);
        } else {
            this.btn_pntr.setTextColor(-16777216);
        }
        this.btn_pntr = (Button) findViewById(R.id.btn_enable_add);
        if (this.mBrainz.IsOperatorEnabled(Brainz.eOperator.ADD.ordinal()) == 0) {
            this.btn_pntr.setTextColor(-1);
        } else {
            this.btn_pntr.setTextColor(-16777216);
        }
        this.btn_pntr = (Button) findViewById(R.id.btn_enable_mul);
        if (this.mBrainz.IsOperatorEnabled(Brainz.eOperator.MUL.ordinal()) == 0) {
            this.btn_pntr.setTextColor(-1);
        } else {
            this.btn_pntr.setTextColor(-16777216);
        }
        this.btn_pntr = (Button) findViewById(R.id.btn_enable_div);
        if (this.mBrainz.IsOperatorEnabled(Brainz.eOperator.DIV.ordinal()) == 0) {
            this.btn_pntr.setTextColor(-1);
        } else {
            this.btn_pntr.setTextColor(-16777216);
        }
    }

    public void UpdateStrings() {
        String cumulative_answer_string = "";
        if (this.game_state == eGameState.PAUSED) {
            this.tv_time.setText("Time: ");
            this.tv_time.setTextColor(-1);
        }
        if (this.current_equation != NUMBER_OF_ROUNDS) {
            this.tv_question.setText("Question " + (this.current_equation + 1) + "/" + NUMBER_OF_ROUNDS);
        }
        if (this.cumulative_answer != -1) {
            cumulative_answer_string = String.valueOf(this.cumulative_answer);
        }
        if (this.current_equation == 0) {
            this.tv_prev.setText("");
        } else {
            this.tv_prev.setText(" " + this.equations[this.current_equation - 1].argument1 + "  " + this.equations[this.current_equation - 1].operator + " " + this.equations[this.current_equation - 1].argument2 + " = " + this.equations[this.current_equation - 1].answer);
        }
        if (this.current_equation < NUMBER_OF_ROUNDS) {
            this.tv_this.setText("  " + this.equations[this.current_equation].argument1 + " " + this.equations[this.current_equation].operator + " " + this.equations[this.current_equation].argument2 + " = " + cumulative_answer_string);
        } else {
            this.tv_this.setText("  " + this.equations[this.current_equation - 1].argument1 + " " + this.equations[this.current_equation - 1].operator + " " + this.equations[this.current_equation - 1].argument2 + " = " + this.equations[this.current_equation - 1].answer);
        }
        if (this.current_equation >= NUMBER_OF_ROUNDS) {
            this.cd_tmr.cancel();
            this.game_state = eGameState.FINISHED;
            this.btn_PlayAgain.setEnabled(true);
            this.tv_game_status.setText("Press \"Play Again\" to Restart");
            this.tv_next.setText("Well Done!!!");
        } else if (this.current_equation >= NUMBER_OF_ROUNDS - 1) {
            this.tv_next.setText("");
        } else {
            this.tv_next.setText("  " + this.equations[this.current_equation + 1].argument1 + " " + this.equations[this.current_equation + 1].operator + " " + this.equations[this.current_equation + 1].argument2 + " = ");
        }
    }

    public void CheckAnswer(int button_number) {
        if (this.cumulative_answer <= 1000 && this.game_state != eGameState.FINISHED && this.game_state != eGameState.TIMED_OUT) {
            if (this.game_state == eGameState.PAUSED) {
                this.cd_tmr.start();
                this.game_state = eGameState.RUNNING;
                this.btn_PlayAgain.setEnabled(false);
            }
            int current_answer = Integer.parseInt(this.equations[this.current_equation].answer);
            if (this.cumulative_answer == -1) {
                this.cumulative_answer = 0;
            }
            this.cumulative_answer *= 10;
            this.cumulative_answer += button_number;
            if (this.cumulative_answer == current_answer) {
                this.cumulative_answer = -1;
                if (this.current_equation != NUMBER_OF_ROUNDS) {
                    this.current_equation++;
                }
            }
            UpdateStrings();
        }
    }

    public void HandleButtonClear(View v) {
        this.cumulative_answer = -1;
        UpdateStrings();
    }

    public void HandleButtonPlayAgain(View v) {
        if (this.game_state == eGameState.FINISHED || this.game_state == eGameState.TIMED_OUT) {
            StartNewGame();
        }
    }

    public void HandleButtonEnableNum(View v) {
        this.btn_pntr = (Button) v;
        if (this.mBrainz.ToggleEnabledNumber(Integer.parseInt(this.btn_pntr.getText().toString())) == 1) {
            this.btn_pntr.setTextColor(-16777216);
        } else {
            this.btn_pntr.setTextColor(-1);
        }
        StartNewGame();
    }

    public void HandleButtonEnableSub(View v) {
        int button_number = Brainz.eOperator.SUB.ordinal();
        this.btn_pntr = (Button) findViewById(R.id.btn_enable_sub);
        if (this.mBrainz.ToggleEnabledOperator(button_number) == 1) {
            this.btn_pntr.setTextColor(-16777216);
        } else {
            this.btn_pntr.setTextColor(-1);
        }
        StartNewGame();
    }

    public void HandleButtonEnableAdd(View v) {
        int button_number = Brainz.eOperator.ADD.ordinal();
        this.btn_pntr = (Button) findViewById(R.id.btn_enable_add);
        if (this.mBrainz.ToggleEnabledOperator(button_number) == 1) {
            this.btn_pntr.setTextColor(-16777216);
        } else {
            this.btn_pntr.setTextColor(-1);
        }
        StartNewGame();
    }

    public void HandleButtonEnableMul(View v) {
        int button_number = Brainz.eOperator.MUL.ordinal();
        this.btn_pntr = (Button) findViewById(R.id.btn_enable_mul);
        if (this.mBrainz.ToggleEnabledOperator(button_number) == 1) {
            this.btn_pntr.setTextColor(-16777216);
        } else {
            this.btn_pntr.setTextColor(-1);
        }
        StartNewGame();
    }

    public void HandleButtonEnableDiv(View v) {
        int button_number = Brainz.eOperator.DIV.ordinal();
        this.btn_pntr = (Button) findViewById(R.id.btn_enable_div);
        if (this.mBrainz.ToggleEnabledOperator(button_number) == 1) {
            this.btn_pntr.setTextColor(-16777216);
        } else {
            this.btn_pntr.setTextColor(-1);
        }
        StartNewGame();
    }

    public void HandleButtonPlay(View v) {
        this.btn_pntr = (Button) v;
        int button_number = Integer.parseInt(this.btn_pntr.getText().toString());
        this.tv_game_status.setText("Answer below!!!");
        CheckAnswer(button_number);
    }

    class mCountDownTimer extends CountDownTimer {
        public mCountDownTimer(long startTime, long interval) {
            super(startTime, interval);
        }

        public void onFinish() {
            zcrazyMath_play.this.game_state = eGameState.TIMED_OUT;
            zcrazyMath_play.this.btn_PlayAgain.setEnabled(true);
            zcrazyMath_play.this.tv_game_status.setText("Press \"Play Again\" to Restart");
            zcrazyMath_play.this.tv_time.setText("Time's up!");
            zcrazyMath_play.this.tv_time.setTextColor(-65536);
        }

        public void onTick(long millisUntilFinished) {
            zcrazyMath_play.this.tv_time.setText("Time: " + String.valueOf((int) (((((long) zcrazyMath_play.TIME_LIMIT) * 1000) - millisUntilFinished) / 1000)));
        }
    }
}
