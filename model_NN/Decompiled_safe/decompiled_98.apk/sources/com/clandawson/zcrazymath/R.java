package com.clandawson.zcrazymath;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int icon = 2130837504;
        public static final int launcher = 2130837505;
    }

    public static final class id {
        public static final int btn_add_player = 2131034116;
        public static final int btn_answer_0 = 2131034144;
        public static final int btn_answer_1 = 2131034145;
        public static final int btn_answer_2 = 2131034146;
        public static final int btn_answer_3 = 2131034147;
        public static final int btn_answer_4 = 2131034148;
        public static final int btn_answer_5 = 2131034149;
        public static final int btn_answer_6 = 2131034150;
        public static final int btn_answer_7 = 2131034151;
        public static final int btn_answer_8 = 2131034152;
        public static final int btn_answer_9 = 2131034153;
        public static final int btn_choose_player = 2131034120;
        public static final int btn_clear = 2131034112;
        public static final int btn_enable_0 = 2131034133;
        public static final int btn_enable_1 = 2131034124;
        public static final int btn_enable_2 = 2131034125;
        public static final int btn_enable_3 = 2131034126;
        public static final int btn_enable_4 = 2131034127;
        public static final int btn_enable_5 = 2131034128;
        public static final int btn_enable_6 = 2131034129;
        public static final int btn_enable_7 = 2131034130;
        public static final int btn_enable_8 = 2131034131;
        public static final int btn_enable_9 = 2131034132;
        public static final int btn_enable_add = 2131034135;
        public static final int btn_enable_div = 2131034137;
        public static final int btn_enable_mul = 2131034136;
        public static final int btn_enable_sub = 2131034134;
        public static final int btn_ok = 2131034118;
        public static final int btn_play = 2131034122;
        public static final int btn_play_again = 2131034113;
        public static final int btn_remove_player = 2131034117;
        public static final int btn_show_hi_score = 2131034121;
        public static final int et_new_user = 2131034115;
        public static final int rg_choose_player = 2131034114;
        public static final int tv_debug = 2131034119;
        public static final int tv_game_status = 2131034143;
        public static final int tv_hello = 2131034123;
        public static final int tv_next = 2131034142;
        public static final int tv_prev = 2131034140;
        public static final int tv_question = 2131034138;
        public static final int tv_this = 2131034141;
        public static final int tv_time = 2131034139;
    }

    public static final class layout {
        public static final int areyousure = 2130903040;
        public static final int chooseplayer = 2130903041;
        public static final int main = 2130903042;
        public static final int playgame = 2130903043;
        public static final int playgame_landscape = 2130903044;
    }

    public static final class string {
        public static final int app_name = 2130968577;
        public static final int hello = 2130968576;
    }
}
