package com.clandawson.zcrazymath;

import android.net.Uri;
import android.provider.BaseColumns;

public class ZCrazyMathProviderMetaData {
    public static final String AUTHORITY = "com.clandawson.providor.ZCrazyMath";
    public static final String DATABASE_NAME = "zcrazymath.db";
    public static final int DATABASE_VERSION = 1;
    public static final String ZCRAZYMATH_TABLE_NAME = "zcrazymath";

    public static final class ZCrazyMathTableMetaData implements BaseColumns {
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.clandawson.zcrazymath";
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.clandawson.zcrazymath";
        public static final Uri CONTENT_URI = Uri.parse("content://com.clandawson.providor.ZCrazyMath/zcrazymath");
        public static final String CURRENT_LEVEL = "current_level";
        public static final String DEFAULT_SORT_ORDER = "name time";
        public static final String RECORD_TIME = "record_time";
        public static final String TABLE_NAME = "zcrazymath";
        public static final String USER_NAME = "name";
    }

    private ZCrazyMathProviderMetaData() {
    }
}
