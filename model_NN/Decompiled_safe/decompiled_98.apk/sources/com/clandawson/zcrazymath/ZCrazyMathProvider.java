package com.clandawson.zcrazymath;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import com.clandawson.zcrazymath.ZCrazyMathProviderMetaData;
import java.util.HashMap;

public class ZCrazyMathProvider extends ContentProvider {
    private static final int INCOMING_ZCRAZYMATH_COLLECTION_URI_INDICATOR = 1;
    private static final int INCOMING_ZCRAZYMATH_SINGLE_URI_INDICATOR = 2;
    private static final String TAG = "ZCrazyMathProvider";
    private static final UriMatcher sUriMatcher = new UriMatcher(-1);
    private static HashMap<String, String> sZCrazyMathProjectionMap = new HashMap<>();
    private DatabaseHelper mOpenHelper;

    static {
        sZCrazyMathProjectionMap.put("_id", "_id");
        sZCrazyMathProjectionMap.put(ZCrazyMathProviderMetaData.ZCrazyMathTableMetaData.USER_NAME, ZCrazyMathProviderMetaData.ZCrazyMathTableMetaData.USER_NAME);
        sZCrazyMathProjectionMap.put(ZCrazyMathProviderMetaData.ZCrazyMathTableMetaData.CURRENT_LEVEL, ZCrazyMathProviderMetaData.ZCrazyMathTableMetaData.CURRENT_LEVEL);
        sZCrazyMathProjectionMap.put(ZCrazyMathProviderMetaData.ZCrazyMathTableMetaData.RECORD_TIME, ZCrazyMathProviderMetaData.ZCrazyMathTableMetaData.RECORD_TIME);
        sUriMatcher.addURI(ZCrazyMathProviderMetaData.AUTHORITY, "zcrazymath", 1);
        sUriMatcher.addURI(ZCrazyMathProviderMetaData.AUTHORITY, "zcrazymath/#", INCOMING_ZCRAZYMATH_SINGLE_URI_INDICATOR);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, ZCrazyMathProviderMetaData.DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 1);
        }

        public void onCreate(SQLiteDatabase db) {
            Log.d(ZCrazyMathProvider.TAG, "Inner oncreate called");
            db.execSQL("CREATE TABLE zcrazymath (_id INTEGER PRIMARY KEY,name TEXT,current_level INTEGER,record_time INTEGER,);");
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.d(ZCrazyMathProvider.TAG, "Inner on upgrade called");
            Log.w(ZCrazyMathProvider.TAG, "Upgrading DB from version " + oldVersion + " to " + newVersion + " which will destroy old data!!!");
            db.execSQL("DROP TABLE IF EXISTS zcrazymath");
            onCreate(db);
        }
    }

    public boolean onCreate() {
        Log.d(TAG, "main onCreate called");
        this.mOpenHelper = new DatabaseHelper(getContext());
        return true;
    }

    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String orderBy;
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        switch (sUriMatcher.match(uri)) {
            case 1:
                qb.setTables("zcrazymath");
                qb.setProjectionMap(sZCrazyMathProjectionMap);
                break;
            case INCOMING_ZCRAZYMATH_SINGLE_URI_INDICATOR /*2*/:
                qb.setTables("zcrazymath");
                qb.setProjectionMap(sZCrazyMathProjectionMap);
                qb.appendWhere("_id=" + uri.getPathSegments().get(1));
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        if (TextUtils.isEmpty(sortOrder)) {
            orderBy = ZCrazyMathProviderMetaData.ZCrazyMathTableMetaData.DEFAULT_SORT_ORDER;
        } else {
            orderBy = sortOrder;
        }
        Cursor c = qb.query(this.mOpenHelper.getReadableDatabase(), projection, selection, selectionArgs, null, null, orderBy);
        Log.d(TAG, "Query found the following number of records: " + c.getCount());
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    public String getType(Uri uri) {
        switch (sUriMatcher.match(uri)) {
            case 1:
                return ZCrazyMathProviderMetaData.ZCrazyMathTableMetaData.CONTENT_TYPE;
            case INCOMING_ZCRAZYMATH_SINGLE_URI_INDICATOR /*2*/:
                return ZCrazyMathProviderMetaData.ZCrazyMathTableMetaData.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public Uri insert(Uri uri, ContentValues initialValues) {
        ContentValues values;
        if (sUriMatcher.match(uri) != 1) {
            throw new IllegalArgumentException("Unknown URI " + uri);
        }
        if (initialValues != null) {
            values = new ContentValues(initialValues);
        } else {
            values = new ContentValues();
        }
        if (!values.containsKey(ZCrazyMathProviderMetaData.ZCrazyMathTableMetaData.USER_NAME)) {
            throw new SQLException("Failed to insert row because a user name is required " + uri);
        }
        if (!values.containsKey(ZCrazyMathProviderMetaData.ZCrazyMathTableMetaData.CURRENT_LEVEL)) {
            values.put(ZCrazyMathProviderMetaData.ZCrazyMathTableMetaData.CURRENT_LEVEL, (Integer) 0);
        }
        if (!values.containsKey(ZCrazyMathProviderMetaData.ZCrazyMathTableMetaData.RECORD_TIME)) {
            values.put(ZCrazyMathProviderMetaData.ZCrazyMathTableMetaData.RECORD_TIME, (Integer) 0);
        }
        long rowId = this.mOpenHelper.getWritableDatabase().insert("zcrazymath", ZCrazyMathProviderMetaData.ZCrazyMathTableMetaData.USER_NAME, values);
        if (rowId > 0) {
            Uri insertedUri = ContentUris.withAppendedId(ZCrazyMathProviderMetaData.ZCrazyMathTableMetaData.CONTENT_URI, rowId);
            getContext().getContentResolver().notifyChange(insertedUri, null);
            return insertedUri;
        }
        throw new SQLException("Failed to insert row into " + uri);
    }

    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int count;
        SQLiteDatabase db = this.mOpenHelper.getWritableDatabase();
        switch (sUriMatcher.match(uri)) {
            case 1:
                Log.d(TAG, " Deleting entire DB!!!");
                count = db.delete("zcrazymath", selection, selectionArgs);
                break;
            case INCOMING_ZCRAZYMATH_SINGLE_URI_INDICATOR /*2*/:
                Log.d(TAG, " Deleting Single Entry!!!");
                count = db.delete("zcrazymath", "_id=" + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""), selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    public int update(Uri uri, ContentValues values, String where, String[] whereArgs) {
        SQLiteDatabase db = this.mOpenHelper.getWritableDatabase();
        switch (sUriMatcher.match(uri)) {
            case 1:
                Log.d(TAG, " Deleting entire DB!!!");
                return db.update("zcrazymath", values, where, whereArgs);
            case INCOMING_ZCRAZYMATH_SINGLE_URI_INDICATOR /*2*/:
                Log.d(TAG, " Deleting Single Entry!!!");
                return db.update("zcrazymath", values, "_id=" + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""), whereArgs);
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
    }
}
