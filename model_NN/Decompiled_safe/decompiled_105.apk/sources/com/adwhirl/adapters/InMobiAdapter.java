package com.adwhirl.adapters;

import android.app.Activity;
import android.location.Location;
import android.util.Log;
import android.view.ViewGroup;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.AdWhirlTargeting;
import com.adwhirl.obj.Extra;
import com.adwhirl.obj.Ration;
import com.adwhirl.util.AdWhirlUtil;
import com.inmobi.androidsdk.EducationType;
import com.inmobi.androidsdk.EthnicityType;
import com.inmobi.androidsdk.GenderType;
import com.inmobi.androidsdk.InMobiAdDelegate;
import com.inmobi.androidsdk.InMobiAdRenderer;
import com.inmobi.androidsdk.InMobiRuntime;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public final class InMobiAdapter extends AdWhirlAdapter implements InMobiAdDelegate {
    private static final int THREAD_COUNT = 1;
    /* access modifiers changed from: private */
    public InMobiAdRenderer adRenderer;
    private ExecutorService adRequestExecutor;
    private Extra extra;
    private Activity parentActivity;

    public InMobiAdapter(AdWhirlLayout adWhirlLayout, Ration ration) {
        super(adWhirlLayout, ration);
        this.extra = adWhirlLayout.extra;
    }

    public void handle() {
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null) {
            this.parentActivity = adWhirlLayout.activityReference.get();
            if (this.parentActivity != null) {
                if (this.adRenderer == null) {
                    this.adRenderer = InMobiRuntime.initializeAdRenderer(this, this.parentActivity);
                    this.adRequestExecutor = Executors.newFixedThreadPool(1);
                }
                this.adRequestExecutor.submit(new Runnable() {
                    public void run() {
                        try {
                            InMobiAdapter.this.loadNewInMobiAd();
                        } catch (InterruptedException e) {
                            Log.e(AdWhirlUtil.ADWHIRL, "Interrupted while waiting for ad retrieval from InMobi to complete", e);
                            Thread.currentThread().interrupt();
                        }
                    }
                });
            }
        }
    }

    /* access modifiers changed from: private */
    public void loadNewInMobiAd() throws InterruptedException {
        Future<Boolean> adLoadResult = this.adRenderer.loadNewAd();
        for (int i = 0; i < 60 && !adLoadResult.isDone(); i++) {
            Thread.sleep(500);
        }
        boolean isRequestSuccessful = retrieveAdLoadResult(adLoadResult);
        final AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (isRequestSuccessful) {
            this.parentActivity.runOnUiThread(new Thread() {
                public void run() {
                    if (adWhirlLayout != null) {
                        adWhirlLayout.adWhirlManager.resetRollover();
                        adWhirlLayout.handler.post(new AdWhirlLayout.ViewAdRunnable(adWhirlLayout, (ViewGroup) InMobiAdapter.this.adRenderer.getViewArea()));
                        adWhirlLayout.rotateThreadedDelayed();
                    }
                }
            });
        } else {
            this.parentActivity.runOnUiThread(new Thread() {
                public void run() {
                    adWhirlLayout.rollover();
                }
            });
        }
    }

    private static boolean retrieveAdLoadResult(Future<Boolean> adLoadResult) throws InterruptedException {
        try {
            if (adLoadResult.isDone()) {
                return adLoadResult.get().booleanValue();
            }
            adLoadResult.cancel(true);
            return false;
        } catch (ExecutionException e) {
            Log.e(AdWhirlUtil.ADWHIRL, "Error obtaining ad load result", e);
            return false;
        }
    }

    public int age() {
        return AdWhirlTargeting.getAge();
    }

    public String areaCode() {
        return null;
    }

    public Location currentLocation() {
        return null;
    }

    public Date dateOfBirth() {
        return null;
    }

    public EducationType education() {
        return null;
    }

    public EthnicityType ethnicity() {
        return null;
    }

    public GenderType gender() {
        AdWhirlTargeting.Gender gender = AdWhirlTargeting.getGender();
        if (AdWhirlTargeting.Gender.MALE == gender) {
            return GenderType.G_M;
        }
        if (AdWhirlTargeting.Gender.FEMALE == gender) {
            return GenderType.G_F;
        }
        return GenderType.G_None;
    }

    public int income() {
        return 0;
    }

    public String interests() {
        return null;
    }

    public boolean isLocationInquiryAllowed() {
        if (this.extra.locationOn == 1) {
            return true;
        }
        return false;
    }

    public boolean isPublisherProvidingLocation() {
        return false;
    }

    public String keywords() {
        return AdWhirlTargeting.getKeywords();
    }

    public String postalCode() {
        return AdWhirlTargeting.getPostalCode();
    }

    public String searchString() {
        return null;
    }

    public String siteId() {
        return this.ration.key;
    }

    public boolean testMode() {
        return AdWhirlTargeting.getTestMode();
    }
}
