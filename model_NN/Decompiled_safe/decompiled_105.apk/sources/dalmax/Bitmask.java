package dalmax;

public abstract class Bitmask {
    static int[] m_vMultiplyDeBruijnBitPosition2;
    static byte[] s_nNumBitInByte = new byte[256];

    static {
        int[] iArr = new int[32];
        iArr[1] = 1;
        iArr[2] = 28;
        iArr[3] = 2;
        iArr[4] = 29;
        iArr[5] = 14;
        iArr[6] = 24;
        iArr[7] = 3;
        iArr[8] = 30;
        iArr[9] = 22;
        iArr[10] = 20;
        iArr[11] = 15;
        iArr[12] = 25;
        iArr[13] = 17;
        iArr[14] = 4;
        iArr[15] = 8;
        iArr[16] = 31;
        iArr[17] = 27;
        iArr[18] = 13;
        iArr[19] = 23;
        iArr[20] = 21;
        iArr[21] = 19;
        iArr[22] = 16;
        iArr[23] = 7;
        iArr[24] = 26;
        iArr[25] = 12;
        iArr[26] = 18;
        iArr[27] = 6;
        iArr[28] = 11;
        iArr[29] = 5;
        iArr[30] = 10;
        iArr[31] = 9;
        m_vMultiplyDeBruijnBitPosition2 = iArr;
        s_nNumBitInByte[0] = 0;
        for (int i = 0; i < 256; i++) {
            s_nNumBitInByte[i] = (byte) ((i & 1) + s_nNumBitInByte[i / 2]);
        }
    }

    public static final byte GetSingleBitPosition(int nMaskWith1Bit) {
        return (byte) m_vMultiplyDeBruijnBitPosition2[((125613361 * nMaskWith1Bit) >> 27) & 31];
    }

    public static final byte GetNumBitInMask(byte nMask) {
        return s_nNumBitInByte[nMask];
    }

    public static final byte GetNumBitInMask(short nMask) {
        return (byte) (s_nNumBitInByte[nMask & 255] + s_nNumBitInByte[(nMask >> 8) & 255]);
    }

    public static final byte GetNumBitInMask(int nMask) {
        return (byte) (s_nNumBitInByte[nMask & 255] + s_nNumBitInByte[(nMask >> 8) & 255] + s_nNumBitInByte[(nMask >> 16) & 255] + s_nNumBitInByte[(nMask << 24) & 255]);
    }
}
