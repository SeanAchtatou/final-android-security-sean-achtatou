package dalmax;

public class Util {
    static long s_random_status = 1;

    public static final long RandomLong() {
        s_random_status ^= s_random_status << 21;
        s_random_status ^= s_random_status >>> 35;
        s_random_status ^= s_random_status << 4;
        return s_random_status & Long.MAX_VALUE;
    }

    public static final long RandomLong(long minIncluded, long maxIncluded) {
        return (RandomLong() % ((maxIncluded - minIncluded) + 1)) + minIncluded;
    }

    public static final int Min(int a, int b) {
        return a < b ? a : b;
    }

    public static final int Max(int a, int b) {
        return a > b ? a : b;
    }

    public static final int Square(int a) {
        return a * a;
    }
}
