package dalmax.GAE;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;

public class GAEConnector {
    static GAEConnector s_GAEConnector = null;
    private String m_accountName;
    private Cookie m_authCookie;
    private String m_gaeAppBaseUrl;
    private String m_gaeAppLoginUrl = (String.valueOf(this.m_gaeAppBaseUrl) + "/_ah/login");
    private String m_lastContent;
    private int m_lastHttpCode;
    private boolean m_usehttps;

    public static GAEConnector GetGAEConnector(String accountName, String gaeAppBaseUrl, boolean bForceRecreate) {
        if (bForceRecreate || s_GAEConnector == null) {
            s_GAEConnector = new GAEConnector(accountName, gaeAppBaseUrl);
        }
        return s_GAEConnector;
    }

    protected GAEConnector(String accountName, String gaeAppBaseUrl) {
        this.m_accountName = accountName;
        this.m_gaeAppBaseUrl = gaeAppBaseUrl;
        this.m_usehttps = gaeAppBaseUrl.startsWith("https");
    }

    public String getLastContent() {
        return this.m_lastContent;
    }

    public String getAccountName() {
        return this.m_accountName;
    }

    public int getLastHttpCode() {
        return this.m_lastHttpCode;
    }

    public boolean isAuthenticated() {
        return this.m_authCookie != null;
    }

    public boolean Authenticate(Context context) {
        boolean z;
        int i = 0;
        while (i < 2) {
            if (i > 0) {
                z = true;
            } else {
                z = false;
            }
            try {
                this.m_authCookie = retrieveAuthCookie(buildAuthToken(context, z));
                if (this.m_authCookie != null) {
                    return true;
                }
                i++;
            } catch (Exception e) {
                Log.e("Authenticate", "ERROR " + e.toString());
                return false;
            }
        }
        return false;
    }

    private String buildAuthToken(Context context, boolean invalidateCurrent) {
        Account account;
        Log.v("GAEConnector", "buildAuthToken - invalidateCurrent=" + invalidateCurrent);
        try {
            AccountManager mgr = AccountManager.get(context);
            if (this.m_accountName == null) {
                account = mgr.getAccountsByType("com.google")[0];
            } else {
                account = new Account(this.m_accountName, "com.google");
            }
            AccountManagerFuture<Bundle> accountManagerFuture = mgr.getAuthToken(account, "ah", (Bundle) null, (Activity) context, (AccountManagerCallback<Bundle>) null, (Handler) null);
            this.m_accountName = account.name;
            String authToken = accountManagerFuture.getResult().get("authtoken").toString();
            Log.v("authToken", authToken);
            if (!invalidateCurrent) {
                return authToken;
            }
            mgr.invalidateAuthToken(account.type, authToken);
            return buildAuthToken(context, false);
        } catch (Exception e) {
            Exception ex = e;
            ex.printStackTrace();
            Log.v("buildAuthToken", "EXCEPTION: " + ex.toString());
            return null;
        }
    }

    private Cookie retrieveAuthCookie(String authToken) throws ClientProtocolException, IOException {
        String url = String.valueOf(this.m_gaeAppLoginUrl) + "?continue=" + URLEncoder.encode(this.m_gaeAppBaseUrl, "UTF-8") + "&auth=" + URLEncoder.encode(authToken, "UTF-8");
        Log.d("retrieveAuthCookie", "cookieUrl = " + url);
        DefaultHttpClient httpClient = new DefaultHttpClient();
        BasicHttpParams params = new BasicHttpParams();
        HttpClientParams.setRedirecting(params, false);
        httpClient.setParams(params);
        if (httpClient.execute(new HttpGet(url)).getStatusLine().getStatusCode() != 302) {
            return null;
        }
        Cookie theCookie = null;
        String cookieName = this.m_usehttps ? "SACSID" : "ACSID";
        for (Cookie c : httpClient.getCookieStore().getCookies()) {
            if (c.getName().equals(cookieName)) {
                theCookie = c;
                Log.v("retrieveAuthCookie", "TheCookie: " + theCookie.getName() + " = " + theCookie.getValue());
            }
        }
        return theCookie;
    }

    public int GETContent(String relativeUrl, boolean authenticated, boolean disableRedirect) throws Exception {
        DefaultHttpClient httpClient = CreateHttpClient(disableRedirect);
        HttpGet httpget = new HttpGet(String.valueOf(this.m_gaeAppBaseUrl) + relativeUrl);
        SetHttpRequestAuthCookie(httpget, authenticated);
        HttpResponse httpResp = httpClient.execute(httpget);
        this.m_lastHttpCode = httpResp.getStatusLine().getStatusCode();
        Log.d("GetContent", "httpStatusCode: " + this.m_lastHttpCode);
        this.m_lastContent = ReadTextFromHttpResponse(httpResp);
        Log.v("GetContent", "content=" + this.m_lastContent);
        return this.m_lastHttpCode;
    }

    public int POSTContent(String relativeUrl, List<NameValuePair> params, boolean authenticated, boolean disableRedirect) throws Exception {
        DefaultHttpClient httpClient = CreateHttpClient(disableRedirect);
        HttpPost httpPost = new HttpPost(String.valueOf(this.m_gaeAppBaseUrl) + relativeUrl);
        if (params != null) {
            httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
        }
        SetHttpRequestAuthCookie(httpPost, authenticated);
        HttpResponse httpResp = httpClient.execute(httpPost);
        this.m_lastHttpCode = httpResp.getStatusLine().getStatusCode();
        Log.v("GetContent", "httpStatusCode: " + this.m_lastHttpCode);
        this.m_lastContent = ReadTextFromHttpResponse(httpResp);
        Log.v("GetContent", "content=" + this.m_lastContent);
        return this.m_lastHttpCode;
    }

    public int POSTContentBody(String relativeUrl, String body, boolean authenticated, boolean disableRedirect) throws Exception {
        DefaultHttpClient httpClient = CreateHttpClient(disableRedirect);
        HttpPost httpPost = new HttpPost(String.valueOf(this.m_gaeAppBaseUrl) + relativeUrl);
        SetHttpRequestAuthCookie(httpPost, authenticated);
        httpPost.setEntity(new ByteArrayEntity(body.getBytes("UTF-8")));
        HttpResponse httpResp = httpClient.execute(httpPost);
        this.m_lastHttpCode = httpResp.getStatusLine().getStatusCode();
        Log.v("GetContent", "httpStatusCode: " + this.m_lastHttpCode);
        this.m_lastContent = ReadTextFromHttpResponse(httpResp);
        Log.v("GetContent", "content=" + this.m_lastContent);
        return this.m_lastHttpCode;
    }

    private String ReadTextFromHttpResponse(HttpResponse httpResp) throws IllegalStateException, IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(httpResp.getEntity().getContent()));
        StringBuffer sb = new StringBuffer();
        while (true) {
            String line = reader.readLine();
            if (line == null) {
                reader.close();
                return sb.toString();
            }
            sb.append(line);
        }
    }

    private void SetHttpRequestAuthCookie(HttpRequestBase httpReq, boolean authenticated) throws Exception {
        if (!authenticated) {
            return;
        }
        if (this.m_authCookie != null) {
            httpReq.setHeader("Cookie", String.valueOf(this.m_authCookie.getName()) + "=" + this.m_authCookie.getValue());
            return;
        }
        throw new Exception("The connector is not currently authenticated");
    }

    private DefaultHttpClient CreateHttpClient(boolean disableRedirect) {
        DefaultHttpClient httpClient = new DefaultHttpClient();
        if (disableRedirect) {
            BasicHttpParams params = new BasicHttpParams();
            HttpClientParams.setRedirecting(params, false);
            httpClient.setParams(params);
        }
        return httpClient;
    }
}
