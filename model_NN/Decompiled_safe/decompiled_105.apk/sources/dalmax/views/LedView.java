package dalmax.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.ArcShape;
import android.view.View;

public class LedView extends View {
    boolean m_bState;
    int m_nSize;

    public void SetState(boolean bState) {
        this.m_bState = bState;
        invalidate();
    }

    public boolean GetState() {
        return this.m_bState;
    }

    public LedView(Context oContext) {
        super(oContext);
        this.m_bState = false;
        this.m_nSize = 16;
        this.m_bState = false;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int parentWidth = View.MeasureSpec.getSize(widthMeasureSpec);
        int parentHeight = View.MeasureSpec.getSize(heightMeasureSpec);
        if (parentWidth < this.m_nSize) {
            this.m_nSize = parentWidth;
        }
        if (parentHeight < this.m_nSize) {
            this.m_nSize = parentHeight;
        }
        setMeasuredDimension(this.m_nSize, this.m_nSize);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas oCanvas) {
        ShapeDrawable currShape = new ShapeDrawable(new ArcShape(0.0f, 360.0f));
        currShape.getPaint().setColor(-65281);
        currShape.setBounds(1, 1, this.m_nSize, this.m_nSize);
        currShape.draw(oCanvas);
        currShape.getPaint().setColor(-3355444);
        currShape.setBounds(0, 0, this.m_nSize - 1, this.m_nSize - 1);
        currShape.draw(oCanvas);
        if (this.m_bState) {
            currShape.getPaint().setColor(-65536);
        } else {
            currShape.getPaint().setColor(-7829368);
        }
        currShape.setBounds(1, 1, this.m_nSize - 1, this.m_nSize - 1);
        currShape.draw(oCanvas);
    }
}
