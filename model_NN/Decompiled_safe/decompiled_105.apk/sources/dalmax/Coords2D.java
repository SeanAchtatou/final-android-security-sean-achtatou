package dalmax;

public class Coords2D {
    public int x;
    public int y;

    public Coords2D(int x2, int y2) {
        Set(x2, y2);
    }

    public Coords2D() {
        Set(0, 0);
    }

    public Coords2D(Coords2D oOther) {
        Set(oOther);
    }

    public void Set(Coords2D oOther) {
        if (oOther != null) {
            Set(oOther.x, oOther.y);
        } else {
            Set(0, 0);
        }
    }

    public void Set(int x2, int y2) {
        this.x = x2;
        this.y = y2;
    }

    public int Max() {
        return this.x > this.y ? this.x : this.y;
    }

    public int Min() {
        return this.x < this.y ? this.x : this.y;
    }

    public void Set(int v) {
        Set(v, v);
    }

    public boolean equals(Coords2D oOther) {
        if (oOther == null) {
            return false;
        }
        if (this.x == oOther.x && this.y == oOther.y) {
            return true;
        }
        return false;
    }

    public Coords2D clone() {
        return new Coords2D(this.x, this.y);
    }

    public int GetDistSquare(Coords2D oOther) {
        if (oOther != null) {
            return ((this.x - oOther.x) * (this.x - oOther.x)) + ((this.y - oOther.y) * (this.y - oOther.y));
        }
        return 0;
    }
}
