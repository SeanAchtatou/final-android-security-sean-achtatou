package dalmax.games.turnBasedGames.online;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
    private static final long serialVersionUID = 1;
    private Date m_CreationDate;
    int m_nScore;
    private Long m_nid;
    private String m_sComment;
    private String m_sNickName;
    private String m_sUniqName;

    public User() {
        this("");
    }

    public User(String sUniqName) {
        this(sUniqName, "", "", 0);
    }

    public User(String sUniqName, String sNickName, String sComment, int nScore) {
        this.m_nid = null;
        this.m_sUniqName = sUniqName;
        this.m_sNickName = sNickName;
        this.m_sComment = sComment;
        this.m_nScore = nScore;
    }

    public User(String sUniqName, String sNickName, String sComment, int nScore, Date dtCreationDate) {
        this.m_nid = null;
        this.m_sUniqName = sUniqName;
        this.m_sNickName = sNickName;
        this.m_sComment = sComment;
        this.m_nScore = nScore;
    }

    public User(User oUser) {
        this.m_nid = null;
        this.m_sUniqName = oUser.m_sUniqName;
        this.m_sNickName = oUser.m_sNickName;
        this.m_nScore = oUser.m_nScore;
        this.m_sComment = oUser.m_sComment;
    }

    public void setId(Long id) {
        this.m_nid = id;
    }

    public String getUniqName() {
        return this.m_sUniqName;
    }

    public void setUniqName(String m_sUniqName2) {
        this.m_sUniqName = m_sUniqName2;
    }

    public String getNickName() {
        return this.m_sNickName;
    }

    public void setNickName(String m_sNickName2) {
        this.m_sNickName = m_sNickName2;
    }

    public String getComment() {
        return this.m_sComment;
    }

    public void setComment(String m_sComment2) {
        this.m_sComment = m_sComment2;
    }

    public Date getCreationDate() {
        return this.m_CreationDate;
    }

    public void setCreationDate(Date m_CreationDate2) {
        this.m_CreationDate = m_CreationDate2;
    }

    public int getScore() {
        return this.m_nScore;
    }

    public void setScore(int m_nScore2) {
        this.m_nScore = m_nScore2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        User other = (User) obj;
        if (this.m_CreationDate == null) {
            if (other.m_CreationDate != null) {
                return false;
            }
        } else if (!this.m_CreationDate.equals(other.m_CreationDate)) {
            return false;
        }
        if (this.m_sUniqName == null) {
            if (other.m_sUniqName != null) {
                return false;
            }
        } else if (!this.m_sUniqName.equals(other.m_sUniqName)) {
            return false;
        }
        return true;
    }

    public Long getId() {
        return this.m_nid;
    }

    public String toString() {
        return this.m_sUniqName;
    }
}
