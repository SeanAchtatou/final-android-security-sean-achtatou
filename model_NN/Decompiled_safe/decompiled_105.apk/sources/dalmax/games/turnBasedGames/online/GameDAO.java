package dalmax.games.turnBasedGames.online;

import java.io.Serializable;
import java.util.ArrayList;

public interface GameDAO extends Serializable {
    Long add(Game game);

    ArrayList<Game> getAll(String str);

    void join(Game game);

    void remove(Game game);
}
