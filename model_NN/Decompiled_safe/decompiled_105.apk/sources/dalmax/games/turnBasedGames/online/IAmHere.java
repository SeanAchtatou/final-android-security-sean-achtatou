package dalmax.games.turnBasedGames.online;

import java.io.Serializable;

public class IAmHere implements Serializable {
    private static final long serialVersionUID = 1;
    private Long m_nidGameType = 0L;
    private Long m_nidUser = 0L;
    private String m_sGameTypeName;
    private String m_sUserName;

    public IAmHere(String sUserName, String sGameTypeName) {
        this.m_sUserName = sUserName;
        this.m_sGameTypeName = sGameTypeName;
    }

    public IAmHere(IAmHere oSrc) {
        this.m_sUserName = oSrc.m_sUserName;
        this.m_sGameTypeName = oSrc.m_sGameTypeName;
    }

    public void setUserId(Long id) {
        this.m_nidUser = id;
    }

    public void setGameTypeId(Long id) {
        this.m_nidGameType = id;
    }

    public String getUserName() {
        return this.m_sUserName;
    }

    public void setUserName(String sUserName) {
        this.m_sUserName = sUserName;
    }

    public String getGameTypeName() {
        return this.m_sGameTypeName;
    }

    public void setGameTypeName(String sGameTypeName) {
        this.m_sGameTypeName = sGameTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        IAmHere other = (IAmHere) obj;
        if (this.m_sUserName == null) {
            if (other.m_sUserName != null) {
                return false;
            }
        } else if (!this.m_sUserName.equals(other.m_sUserName)) {
            return false;
        }
        if (this.m_sGameTypeName == null) {
            if (other.m_sGameTypeName != null) {
                return false;
            }
        } else if (!this.m_sGameTypeName.equals(other.m_sGameTypeName)) {
            return false;
        }
        return true;
    }

    public Long getUserId() {
        return this.m_nidUser;
    }

    public Long getGameTypeId() {
        return this.m_nidGameType;
    }

    public String toString() {
        return String.valueOf(this.m_sUserName) + ":" + this.m_sGameTypeName;
    }
}
