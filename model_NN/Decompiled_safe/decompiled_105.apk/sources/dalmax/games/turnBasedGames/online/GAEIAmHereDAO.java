package dalmax.games.turnBasedGames.online;

import android.content.Context;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

public class GAEIAmHereDAO implements IAmHereDAO {
    private static final long serialVersionUID = 1;
    private final String BASE_URL = "http://dalmax79.appspot.com/";
    private final String IAMHERE = "iamhere";
    private HttpClient client = new DefaultHttpClient();
    Context m_context = null;

    public GAEIAmHereDAO(Context context) {
        this.m_context = context;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v9, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: java.lang.Long} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Long Live(dalmax.games.turnBasedGames.online.IAmHere r11) {
        /*
            r10 = this;
            r8 = 0
            java.lang.Long r3 = java.lang.Long.valueOf(r8)
            org.apache.http.client.methods.HttpPost r5 = new org.apache.http.client.methods.HttpPost
            java.lang.String r8 = "http://dalmax79.appspot.com/iamhere"
            r5.<init>(r8)
            org.apache.http.entity.SerializableEntity r8 = new org.apache.http.entity.SerializableEntity     // Catch:{ IOException -> 0x0046, ClassNotFoundException -> 0x004c }
            r9 = 1
            r8.<init>(r11, r9)     // Catch:{ IOException -> 0x0046, ClassNotFoundException -> 0x004c }
            r5.setEntity(r8)     // Catch:{ IOException -> 0x0046, ClassNotFoundException -> 0x004c }
            org.apache.http.client.HttpClient r8 = r10.client     // Catch:{ IOException -> 0x0046, ClassNotFoundException -> 0x004c }
            org.apache.http.HttpResponse r7 = r8.execute(r5)     // Catch:{ IOException -> 0x0046, ClassNotFoundException -> 0x004c }
            org.apache.http.HttpEntity r8 = r7.getEntity()     // Catch:{ IOException -> 0x0046, ClassNotFoundException -> 0x004c }
            java.io.InputStream r2 = r8.getContent()     // Catch:{ IOException -> 0x0046, ClassNotFoundException -> 0x004c }
            java.io.ObjectInputStream r6 = new java.io.ObjectInputStream     // Catch:{ IOException -> 0x0046, ClassNotFoundException -> 0x004c }
            r6.<init>(r2)     // Catch:{ IOException -> 0x0046, ClassNotFoundException -> 0x004c }
            java.lang.Object r4 = r6.readObject()     // Catch:{ IOException -> 0x0046, ClassNotFoundException -> 0x004c }
            java.util.ArrayList r4 = (java.util.ArrayList) r4     // Catch:{ IOException -> 0x0046, ClassNotFoundException -> 0x004c }
            r8 = 0
            java.lang.Object r10 = r4.get(r8)     // Catch:{ IOException -> 0x0046, ClassNotFoundException -> 0x004c }
            java.lang.Long r10 = (java.lang.Long) r10     // Catch:{ IOException -> 0x0046, ClassNotFoundException -> 0x004c }
            r11.setGameTypeId(r10)     // Catch:{ IOException -> 0x0046, ClassNotFoundException -> 0x004c }
            r8 = 1
            java.lang.Object r8 = r4.get(r8)     // Catch:{ IOException -> 0x0046, ClassNotFoundException -> 0x004c }
            r0 = r8
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ IOException -> 0x0046, ClassNotFoundException -> 0x004c }
            r3 = r0
            r11.setUserId(r3)     // Catch:{ IOException -> 0x0046, ClassNotFoundException -> 0x004c }
        L_0x0045:
            return r3
        L_0x0046:
            r8 = move-exception
            r1 = r8
            r1.printStackTrace()
            goto L_0x0045
        L_0x004c:
            r8 = move-exception
            r1 = r8
            r1.printStackTrace()
            goto L_0x0045
        */
        throw new UnsupportedOperationException("Method not decompiled: dalmax.games.turnBasedGames.online.GAEIAmHereDAO.Live(dalmax.games.turnBasedGames.online.IAmHere):java.lang.Long");
    }
}
