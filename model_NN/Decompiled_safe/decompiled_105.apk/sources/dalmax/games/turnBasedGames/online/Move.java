package dalmax.games.turnBasedGames.online;

import java.io.Serializable;

public class Move implements Serializable {
    private static final long serialVersionUID = 1;
    private short m_nPlayerIdx;
    private int m_nPly;
    private Long m_nid;
    private Long m_nidGame;
    private String m_sMove;

    public String toString() {
        return String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf("") + " id:") + String.valueOf(this.m_nid)) + " idGame:") + String.valueOf(this.m_nidGame)) + " nPly:") + String.valueOf(this.m_nPly)) + " playerIdx:") + String.valueOf((int) this.m_nPlayerIdx)) + " sMove:") + String.valueOf(this.m_sMove);
    }

    public Move(String sMove, Long nidGame) {
        this.m_nid = 0L;
        this.m_nidGame = 0L;
        this.m_nPly = 0;
        this.m_nPlayerIdx = 0;
        this.m_sMove = "";
        this.m_sMove = sMove;
        this.m_nidGame = nidGame;
    }

    public Move(Long nid, Long nidGame, int nPly, short nPlayerIdx, String sMove) {
        this(sMove, nidGame);
        this.m_nid = nid;
        this.m_nPly = nPly;
        this.m_nPlayerIdx = nPlayerIdx;
    }

    public Move(Move oMove) {
        this.m_nid = 0L;
        this.m_nidGame = 0L;
        this.m_nPly = 0;
        this.m_nPlayerIdx = 0;
        this.m_sMove = "";
        this.m_nid = oMove.m_nid;
        this.m_nidGame = oMove.m_nidGame;
        this.m_nPlayerIdx = oMove.m_nPlayerIdx;
        this.m_nPly = oMove.m_nPly;
        this.m_sMove = oMove.m_sMove;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Move oOther = (Move) obj;
        if (this.m_sMove == null) {
            if (oOther.m_sMove != null) {
                return false;
            }
        } else if (!this.m_sMove.equals(oOther.m_sMove)) {
            return false;
        }
        if (this.m_nidGame != oOther.m_nidGame) {
            return false;
        }
        if (this.m_nPly != oOther.m_nPly) {
            return false;
        }
        return true;
    }

    public Long GetId() {
        return this.m_nid;
    }

    public Long GetGameID() {
        return this.m_nidGame;
    }

    public int GetPly() {
        return this.m_nPly;
    }

    public short GetPlayerIdx() {
        return this.m_nPlayerIdx;
    }

    public String GetMove() {
        return this.m_sMove;
    }

    public void SetId(Long nid) {
        this.m_nid = nid;
    }
}
