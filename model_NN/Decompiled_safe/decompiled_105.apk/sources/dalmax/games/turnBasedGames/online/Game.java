package dalmax.games.turnBasedGames.online;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class Game implements Serializable {
    private static final long serialVersionUID = 1;
    private Date m_CreationDate;
    private Long m_nid;
    private Long m_nidGameType;
    private Long m_nidPlayer1;
    private Long m_nidPlayer2;
    private String m_sComment;

    public Game() {
        this("");
    }

    public Game(String sGameName) {
        this(0, 0L, 0L, "");
    }

    public Game(long nidGameType, Long nidPlayer1, Long nidPlayer2, String sComment) {
        this.m_nidGameType = Long.valueOf(nidGameType);
        this.m_nidPlayer1 = nidPlayer1;
        this.m_nidPlayer2 = nidPlayer2;
        this.m_sComment = sComment;
        this.m_CreationDate = Calendar.getInstance().getTime();
    }

    public Game(Game oGame) {
        this.m_nidGameType = oGame.m_nidGameType;
        this.m_nidPlayer1 = oGame.m_nidPlayer1;
        this.m_nidPlayer2 = oGame.m_nidPlayer2;
        this.m_sComment = oGame.m_sComment;
        this.m_CreationDate = (Date) oGame.m_CreationDate.clone();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Game other = (Game) obj;
        if (this.m_CreationDate == null) {
            if (other.m_CreationDate != null) {
                return false;
            }
        } else if (!this.m_CreationDate.equals(other.m_CreationDate)) {
            return false;
        }
        if (this.m_nidGameType == null) {
            if (other.m_nidGameType != null) {
                return false;
            }
        } else if (this.m_nidGameType != other.m_nidGameType) {
            return false;
        }
        if (this.m_nidPlayer1 == null) {
            if (other.m_nidPlayer1 != null) {
                return false;
            }
        } else if (this.m_nidPlayer1 != other.m_nidPlayer1) {
            return false;
        }
        if (this.m_nidPlayer2 == null) {
            if (other.m_nidPlayer2 != null) {
                return false;
            }
        } else if (this.m_nidPlayer2 != other.m_nidPlayer2) {
            return false;
        }
        if (this.m_sComment == null) {
            if (other.m_sComment != null) {
                return false;
            }
        } else if (!this.m_sComment.equals(other.m_sComment)) {
            return false;
        }
        return true;
    }

    public Long getId() {
        return this.m_nid;
    }

    public Date getCreationDate() {
        return this.m_CreationDate;
    }

    public long getGameType() {
        return this.m_nidGameType.longValue();
    }

    public Long getPlayer1() {
        return this.m_nidPlayer1;
    }

    public Long getPlayer2() {
        return this.m_nidPlayer2;
    }

    public String getNamePlayer1() {
        return "A";
    }

    public String getNamePlayer2() {
        return "B";
    }

    public String getComment() {
        return this.m_sComment;
    }

    public void setId(Long id) {
        this.m_nid = id;
    }

    public void setCreationDate(Date date) {
        this.m_CreationDate = date;
    }

    public void setGameName(long nidGameType) {
        this.m_nidGameType = Long.valueOf(nidGameType);
    }

    public void setPlayer1(Long nidPlayer1) {
        this.m_nidPlayer1 = nidPlayer1;
    }

    public void setPlayer2(Long nidPlayer2) {
        this.m_nidPlayer2 = nidPlayer2;
    }

    public void setComment(String sComment) {
        this.m_sComment = sComment;
    }

    public String toString() {
        String s;
        if (getPlayer1() == null || getPlayer1().equals("") || getPlayer2() == null || getPlayer2().equals("")) {
            s = String.valueOf("") + "Available board\n";
        } else {
            s = String.valueOf("") + "Full board\n";
        }
        return String.valueOf(s) + getPlayer1() + " vs " + getPlayer2() + "\n";
    }
}
