package dalmax.games.turnBasedGames.online;

import java.io.Serializable;
import java.util.ArrayList;

public interface MoveDAO extends Serializable {
    Long add(Move move);

    ArrayList<Move> getAll(Long l);
}
