package dalmax.games.turnBasedGames.online;

import java.io.Serializable;

public class GameType implements Serializable {
    private static final long serialVersionUID = 1;
    private Long m_nid;
    private String m_sGameName;

    public GameType(String sGameName) {
        this.m_sGameName = sGameName;
    }

    public GameType(GameType oGame) {
        this.m_sGameName = oGame.m_sGameName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        GameType other = (GameType) obj;
        if (this.m_sGameName == null) {
            if (other.m_sGameName != null) {
                return false;
            }
        } else if (!this.m_sGameName.equals(other.m_sGameName)) {
            return false;
        }
        return true;
    }

    public Long getId() {
        return this.m_nid;
    }

    public String getGameName() {
        return this.m_sGameName;
    }

    public void setId(Long id) {
        this.m_nid = id;
    }

    public String toString() {
        return this.m_sGameName;
    }
}
