package dalmax.games.turnBasedGames.online;

import android.content.Context;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class GAEMoveDAO implements MoveDAO {
    private static final long serialVersionUID = 1;
    private final String BASE_URL = "http://dalmax79.appspot.com/";
    private final String GET = "getmoves";
    private final String SAVE = "savemove";
    private HttpClient client = new DefaultHttpClient();
    Context m_context = null;

    public GAEMoveDAO(Context context) {
        this.m_context = context;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v7, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: java.lang.Long} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Long add(dalmax.games.turnBasedGames.online.Move r10) {
        /*
            r9 = this;
            r7 = 0
            java.lang.Long r3 = java.lang.Long.valueOf(r7)
            org.apache.http.client.methods.HttpPost r4 = new org.apache.http.client.methods.HttpPost
            java.lang.String r7 = "http://dalmax79.appspot.com/savemove"
            r4.<init>(r7)
            org.apache.http.entity.SerializableEntity r7 = new org.apache.http.entity.SerializableEntity     // Catch:{ IOException -> 0x0035, ClassNotFoundException -> 0x003b }
            r8 = 1
            r7.<init>(r10, r8)     // Catch:{ IOException -> 0x0035, ClassNotFoundException -> 0x003b }
            r4.setEntity(r7)     // Catch:{ IOException -> 0x0035, ClassNotFoundException -> 0x003b }
            org.apache.http.client.HttpClient r7 = r9.client     // Catch:{ IOException -> 0x0035, ClassNotFoundException -> 0x003b }
            org.apache.http.HttpResponse r6 = r7.execute(r4)     // Catch:{ IOException -> 0x0035, ClassNotFoundException -> 0x003b }
            org.apache.http.HttpEntity r7 = r6.getEntity()     // Catch:{ IOException -> 0x0035, ClassNotFoundException -> 0x003b }
            java.io.InputStream r2 = r7.getContent()     // Catch:{ IOException -> 0x0035, ClassNotFoundException -> 0x003b }
            java.io.ObjectInputStream r5 = new java.io.ObjectInputStream     // Catch:{ IOException -> 0x0035, ClassNotFoundException -> 0x003b }
            r5.<init>(r2)     // Catch:{ IOException -> 0x0035, ClassNotFoundException -> 0x003b }
            java.lang.Object r7 = r5.readObject()     // Catch:{ IOException -> 0x0035, ClassNotFoundException -> 0x003b }
            r0 = r7
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ IOException -> 0x0035, ClassNotFoundException -> 0x003b }
            r3 = r0
            r10.SetId(r3)     // Catch:{ IOException -> 0x0035, ClassNotFoundException -> 0x003b }
        L_0x0034:
            return r3
        L_0x0035:
            r7 = move-exception
            r1 = r7
            r1.printStackTrace()
            goto L_0x0034
        L_0x003b:
            r7 = move-exception
            r1 = r7
            r1.printStackTrace()
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: dalmax.games.turnBasedGames.online.GAEMoveDAO.add(dalmax.games.turnBasedGames.online.Move):java.lang.Long");
    }

    public ArrayList<Move> getAll(Long nidGame) {
        try {
            return (ArrayList) new ObjectInputStream(this.client.execute(new HttpGet("http://dalmax79.appspot.com/getmoves" + ("?nidGame=" + nidGame.toString()))).getEntity().getContent()).readObject();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        } catch (ClassNotFoundException e3) {
            e3.printStackTrace();
            return null;
        }
    }
}
