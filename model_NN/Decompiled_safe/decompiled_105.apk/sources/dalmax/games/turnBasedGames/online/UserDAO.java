package dalmax.games.turnBasedGames.online;

import java.io.Serializable;
import java.util.ArrayList;

public interface UserDAO extends Serializable {
    Long add(User user);

    User get(User user);

    ArrayList<User> getAll();

    Boolean setNickname(User user);
}
