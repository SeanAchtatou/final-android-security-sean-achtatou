package dalmax.games.turnBasedGames.online;

import android.content.Context;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

public class GAEGameTypeDAO implements GameTypeDAO {
    private static final long serialVersionUID = 1;
    private final String ADD = "addgametype";
    private final String BASE_URL = "http://dalmax79.appspot.com/";
    private HttpClient client = new DefaultHttpClient();
    Context m_context = null;

    public GAEGameTypeDAO(Context context) {
        this.m_context = context;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v7, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: java.lang.Long} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Long add(java.lang.String r11) {
        /*
            r10 = this;
            r8 = 0
            java.lang.Long r3 = java.lang.Long.valueOf(r8)
            org.apache.http.client.methods.HttpPost r5 = new org.apache.http.client.methods.HttpPost
            java.lang.String r8 = "http://dalmax79.appspot.com/addgametype"
            r5.<init>(r8)
            dalmax.games.turnBasedGames.online.GameType r4 = new dalmax.games.turnBasedGames.online.GameType     // Catch:{ IOException -> 0x003a, ClassNotFoundException -> 0x0040 }
            r4.<init>(r11)     // Catch:{ IOException -> 0x003a, ClassNotFoundException -> 0x0040 }
            org.apache.http.entity.SerializableEntity r8 = new org.apache.http.entity.SerializableEntity     // Catch:{ IOException -> 0x003a, ClassNotFoundException -> 0x0040 }
            r9 = 1
            r8.<init>(r4, r9)     // Catch:{ IOException -> 0x003a, ClassNotFoundException -> 0x0040 }
            r5.setEntity(r8)     // Catch:{ IOException -> 0x003a, ClassNotFoundException -> 0x0040 }
            org.apache.http.client.HttpClient r8 = r10.client     // Catch:{ IOException -> 0x003a, ClassNotFoundException -> 0x0040 }
            org.apache.http.HttpResponse r7 = r8.execute(r5)     // Catch:{ IOException -> 0x003a, ClassNotFoundException -> 0x0040 }
            org.apache.http.HttpEntity r8 = r7.getEntity()     // Catch:{ IOException -> 0x003a, ClassNotFoundException -> 0x0040 }
            java.io.InputStream r2 = r8.getContent()     // Catch:{ IOException -> 0x003a, ClassNotFoundException -> 0x0040 }
            java.io.ObjectInputStream r6 = new java.io.ObjectInputStream     // Catch:{ IOException -> 0x003a, ClassNotFoundException -> 0x0040 }
            r6.<init>(r2)     // Catch:{ IOException -> 0x003a, ClassNotFoundException -> 0x0040 }
            java.lang.Object r8 = r6.readObject()     // Catch:{ IOException -> 0x003a, ClassNotFoundException -> 0x0040 }
            r0 = r8
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ IOException -> 0x003a, ClassNotFoundException -> 0x0040 }
            r3 = r0
            r4.setId(r3)     // Catch:{ IOException -> 0x003a, ClassNotFoundException -> 0x0040 }
        L_0x0039:
            return r3
        L_0x003a:
            r8 = move-exception
            r1 = r8
            r1.printStackTrace()
            goto L_0x0039
        L_0x0040:
            r8 = move-exception
            r1 = r8
            r1.printStackTrace()
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: dalmax.games.turnBasedGames.online.GAEGameTypeDAO.add(java.lang.String):java.lang.Long");
    }
}
