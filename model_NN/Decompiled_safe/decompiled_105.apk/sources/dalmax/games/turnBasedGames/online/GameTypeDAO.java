package dalmax.games.turnBasedGames.online;

import java.io.Serializable;

public interface GameTypeDAO extends Serializable {
    Long add(String str);
}
