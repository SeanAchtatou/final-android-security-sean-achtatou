package dalmax.games.turnBasedGames.connect4;

import dalmax.games.turnBasedGames.ViewMove;
import java.lang.reflect.Array;

public class Connect4ViewMove extends ViewMove {
    static long[][] s_coord2Mask = ((long[][]) Array.newInstance(Long.TYPE, s_nNRows, s_nNCols));
    static byte s_nNCols = 7;
    static byte s_nNRows = 6;
    int m_c;
    int m_r;

    static {
        for (byte r = 0; r < s_nNRows; r = (byte) (r + 1)) {
            for (byte c = 0; c < s_nNCols; c = (byte) (c + 1)) {
                s_coord2Mask[r][c] = 1 << ((s_nNCols * r) + c);
            }
        }
    }

    public Connect4ViewMove() {
        this.m_r = -1;
        this.m_c = -1;
    }

    public Connect4ViewMove(int xc, int yr) {
        this.m_c = xc;
        this.m_r = yr;
    }

    public boolean IsValid() {
        if (this.m_c < 0 || this.m_r < 0 || this.m_c >= s_nNCols || this.m_r >= s_nNRows) {
            return false;
        }
        return true;
    }

    public boolean SetFromString(String sStringedMove) {
        if (!sStringedMove.startsWith("C4:")) {
            return false;
        }
        String[] sCoords = sStringedMove.split(":")[1].split("-");
        this.m_c = Integer.parseInt(sCoords[0]);
        this.m_r = Integer.parseInt(sCoords[1]);
        return true;
    }

    public String toString() {
        return String.valueOf(String.valueOf(String.valueOf("C4:") + String.valueOf(this.m_c)) + "-") + String.valueOf(this.m_r);
    }
}
