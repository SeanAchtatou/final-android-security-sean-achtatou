package dalmax.games.turnBasedGames.connect4;

import dalmax.games.turnBasedGames.Engine;
import dalmax.games.turnBasedGames.EngineMove;
import dalmax.games.turnBasedGames.ViewMove;

public class Connect4EngineMove extends EngineMove {
    byte m_c;
    byte m_r;

    public Connect4EngineMove(byte xc, byte yr) {
        this.m_c = xc;
        this.m_r = yr;
    }

    public void Clear() {
        this.m_r = -1;
        this.m_c = -1;
    }

    public ViewMove GetViewMove(Engine engine) {
        return new Connect4ViewMove(this.m_c, this.m_r);
    }
}
