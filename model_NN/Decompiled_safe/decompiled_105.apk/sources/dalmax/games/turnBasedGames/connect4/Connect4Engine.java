package dalmax.games.turnBasedGames.connect4;

import dalmax.Coords2D;
import dalmax.Util;
import dalmax.games.turnBasedGames.Engine;
import dalmax.games.turnBasedGames.EngineMove;
import dalmax.games.turnBasedGames.EnginePosition;
import dalmax.games.turnBasedGames.GameStatusDescription;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class Connect4Engine extends Engine {
    static byte[] s_nIdxFirstColBit = new byte[128];
    static byte[] s_nIdxFirstRowBit = new byte[128];
    static byte[] s_nNMaxAdiacentBits = new byte[128];
    byte m_LastGetMove = 0;
    Coords2D m_LastMenPushed = null;
    byte[][] m_Matrix = null;
    byte m_nNCol = 7;
    byte m_nNRow = 6;
    long[][] m_vBitmasks = ((long[][]) Array.newInstance(Long.TYPE, 2, 4));

    static {
        for (short i = 0; i < 128; i = (short) (i + 1)) {
            s_nNMaxAdiacentBits[i] = 0;
            s_nIdxFirstColBit[i] = 6;
            s_nIdxFirstRowBit[i] = 7;
            int mask = i;
            byte idx = 0;
            while (mask != 0) {
                if ((mask & 1) != 0) {
                    byte nNBit = 0;
                    while ((mask & 1) != 0) {
                        nNBit = (byte) (nNBit + 1);
                        mask >>= 1;
                    }
                    if (nNBit > s_nNMaxAdiacentBits[i]) {
                        s_nNMaxAdiacentBits[i] = nNBit;
                    }
                }
                mask >>= 1;
                idx = (byte) (idx + 1);
            }
            int mask2 = i;
            byte idx2 = 0;
            while (true) {
                if (mask2 == 0) {
                    break;
                } else if ((mask2 & 1) != 0) {
                    s_nIdxFirstRowBit[i] = idx2;
                    s_nIdxFirstColBit[i] = idx2;
                    break;
                } else {
                    mask2 >>= 1;
                    idx2 = (byte) (idx2 + 1);
                }
            }
        }
    }

    public Connect4Engine(int nLevel, byte nNRow, byte nNCol) {
        super(nLevel, 5000);
        this.m_nNRow = nNRow;
        this.m_nNCol = nNCol;
        this.m_Matrix = (byte[][]) Array.newInstance(Byte.TYPE, this.m_nNRow, this.m_nNCol);
    }

    private void SetBitmasks(byte r, byte c, byte nPlayer) {
        for (int i = 0; i < 2; i++) {
            long nMaskBitForRows = 1 << ((this.m_nNCol * r) + c);
            long[] jArr = this.m_vBitmasks[i];
            jArr[0] = jArr[0] & (-1 ^ nMaskBitForRows);
            long nMaskBitForCols = 1 << ((this.m_nNRow * c) + r);
            long[] jArr2 = this.m_vBitmasks[i];
            jArr2[1] = jArr2[1] & (-1 ^ nMaskBitForCols);
        }
        if (nPlayer != 0) {
            byte nPlayerIdx = (byte) (nPlayer > 0 ? 0 : 1);
            long nMaskBitForRows2 = 1 << ((this.m_nNCol * r) + c);
            long[] jArr3 = this.m_vBitmasks[nPlayerIdx];
            jArr3[0] = jArr3[0] | nMaskBitForRows2;
            long nMaskBitForCols2 = 1 << ((this.m_nNRow * c) + r);
            long[] jArr4 = this.m_vBitmasks[nPlayerIdx];
            jArr4[1] = jArr4[1] | nMaskBitForCols2;
        }
    }

    private byte GetColMask(byte c) {
        return (byte) ((int) (((this.m_vBitmasks[0][1] >> (this.m_nNRow * c)) & 63) | ((this.m_vBitmasks[1][1] >> (this.m_nNRow * c)) & 63)));
    }

    /* access modifiers changed from: protected */
    public Object clone() throws CloneNotSupportedException {
        Connect4Engine oClone = (Connect4Engine) super.clone();
        oClone.m_nNRow = this.m_nNRow;
        oClone.m_nNCol = this.m_nNCol;
        oClone.m_Matrix = (byte[][]) Array.newInstance(Byte.TYPE, this.m_nNRow, this.m_nNCol);
        for (int r = 0; r < this.m_nNRow; r++) {
            for (int c = 0; c < this.m_nNCol; c++) {
                oClone.m_Matrix[r][c] = this.m_Matrix[r][c];
            }
        }
        oClone.m_vBitmasks = (long[][]) Array.newInstance(Long.TYPE, 2, 4);
        for (int p = 0; p < 2; p++) {
            for (int m = 0; m < 4; m++) {
                oClone.m_vBitmasks[p][m] = this.m_vBitmasks[p][m];
            }
        }
        return oClone;
    }

    /* access modifiers changed from: protected */
    public void CopyFrom(Connect4Engine oCopyFrom) throws CloneNotSupportedException {
        super.CopyFrom((Engine) oCopyFrom);
        if (!(this.m_nNRow == oCopyFrom.m_nNRow && this.m_nNCol == oCopyFrom.m_nNCol)) {
            this.m_nNRow = oCopyFrom.m_nNRow;
            this.m_nNCol = oCopyFrom.m_nNCol;
            this.m_Matrix = (byte[][]) Array.newInstance(Byte.TYPE, this.m_nNRow, this.m_nNCol);
        }
        for (int r = 0; r < this.m_nNRow; r++) {
            for (int c = 0; c < this.m_nNCol; c++) {
                this.m_Matrix[r][c] = oCopyFrom.m_Matrix[r][c];
            }
        }
        for (int p = 0; p < 2; p++) {
            for (int m = 0; m < 4; m++) {
                this.m_vBitmasks[p][m] = oCopyFrom.m_vBitmasks[p][m];
            }
        }
    }

    /* access modifiers changed from: protected */
    public EnginePosition GetPosition() {
        return new Connect4EnginePosition(this.m_nNRow, this.m_nNCol, this.m_vBitmasks[0][0], this.m_vBitmasks[1][0], (byte) this.m_nPlayerToMove);
    }

    /* access modifiers changed from: protected */
    public void CopyInfoFromBoardGameDescription(GameStatusDescription oBoardInfo) {
        for (int p = 0; p < 2; p++) {
            for (int m = 0; m < 4; m++) {
                this.m_vBitmasks[p][m] = 0;
            }
        }
        for (byte r = 0; r < this.m_nNRow; r = (byte) (r + 1)) {
            for (byte c = 0; c < this.m_nNCol; c = (byte) (c + 1)) {
                byte val = ((Connect4BoardDescription) oBoardInfo).GetBoard()[r][c];
                this.m_Matrix[r][c] = val;
                SetBitmasks(r, c, val);
            }
        }
        this.m_nPlayerToMove = oBoardInfo.GetPlayer();
    }

    /* access modifiers changed from: protected */
    public int InitMoves(boolean bComputeAll) {
        this.m_LastGetMove = -1;
        if (!bComputeAll) {
            return 0;
        }
        int nMoves = 0;
        for (byte c = 0; c < this.m_nNCol; c = (byte) (c + 1)) {
            if (s_nIdxFirstColBit[GetColMask(c)] > 0) {
                nMoves++;
            }
        }
        return nMoves;
    }

    /* access modifiers changed from: protected */
    public EngineMove GetNextMove() {
        this.m_LastGetMove = (byte) (this.m_LastGetMove + 1);
        while (this.m_LastGetMove < this.m_nNCol) {
            byte nColMask = GetColMask(this.m_LastGetMove);
            if (s_nIdxFirstColBit[nColMask] > 0) {
                return new Connect4EngineMove(this.m_LastGetMove, (byte) (s_nIdxFirstColBit[nColMask] - 1));
            }
            this.m_LastGetMove = (byte) (this.m_LastGetMove + 1);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public Engine DoMove(EngineMove oMoveToCast, Engine oDestinationBoard) throws CloneNotSupportedException {
        Connect4Engine oDestBoard = (Connect4Engine) oDestinationBoard;
        Connect4EngineMove oMove = (Connect4EngineMove) oMoveToCast;
        if (oDestBoard == null) {
            oDestBoard = (Connect4Engine) clone();
        } else {
            oDestBoard.CopyFrom(this);
        }
        oDestBoard.m_Matrix[oMove.m_r][oMove.m_c] = (byte) this.m_nPlayerToMove;
        oDestBoard.SetBitmasks(oMove.m_r, oMove.m_c, (byte) this.m_nPlayerToMove);
        oDestBoard.m_LastMenPushed = new Coords2D(oMove.m_c, oMove.m_r);
        oDestBoard.ChangePlayerToMove();
        return oDestBoard;
    }

    /* JADX WARN: Type inference failed for: r0v17, types: [int] */
    /* JADX WARN: Type inference failed for: r0v33, types: [int] */
    /* JADX WARN: Type inference failed for: r0v35, types: [int] */
    /* JADX WARN: Type inference failed for: r0v39, types: [int] */
    /* JADX WARN: Type inference failed for: r0v51, types: [int] */
    /* JADX WARN: Type inference failed for: r0v62, types: [int] */
    /* JADX WARN: Type inference failed for: r0v64, types: [int] */
    /* JADX WARN: Type inference failed for: r0v68, types: [int] */
    /* JADX WARN: Type inference failed for: r0v80, types: [int] */
    /* JADX WARN: Type inference failed for: r0v91, types: [int] */
    /* JADX WARN: Type inference failed for: r0v93, types: [int] */
    /* JADX WARN: Type inference failed for: r0v97, types: [int] */
    /* JADX WARN: Type inference failed for: r0v103, types: [int] */
    /* JADX WARN: Type inference failed for: r0v105, types: [int] */
    /* JADX WARN: Type inference failed for: r0v109, types: [int] */
    /* JADX WARN: Type inference failed for: r0v121, types: [int] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int GetStaticBoardScore() {
        /*
            r24 = this;
            r2 = 2147483637(0x7ffffff5, float:NaN)
            r18 = 1
            r0 = r24
            r1 = r18
            dalmax.games.turnBasedGames.Engine$EWinPairLose r8 = r0.CheckWinPairLose(r1)
            dalmax.games.turnBasedGames.Engine$EWinPairLose r18 = dalmax.games.turnBasedGames.Engine.EWinPairLose.Win
            r0 = r8
            r1 = r18
            if (r0 != r1) goto L_0x0017
            r18 = r2
        L_0x0016:
            return r18
        L_0x0017:
            dalmax.games.turnBasedGames.Engine$EWinPairLose r18 = dalmax.games.turnBasedGames.Engine.EWinPairLose.Lose
            r0 = r8
            r1 = r18
            if (r0 != r1) goto L_0x0023
            r0 = r2
            int r0 = -r0
            r18 = r0
            goto L_0x0016
        L_0x0023:
            r4 = 0
            r5 = 0
            r9 = 0
            r7 = 0
            r3 = 0
        L_0x0028:
            r0 = r24
            byte r0 = r0.m_nNCol
            r18 = r0
            r0 = r3
            r1 = r18
            if (r0 < r1) goto L_0x004e
            r0 = r9
            long r0 = (long) r0
            r18 = r0
            r20 = -3
            r22 = 3
            long r20 = dalmax.Util.RandomLong(r20, r22)
            long r18 = r18 + r20
            r0 = r18
            int r0 = (int) r0
            r9 = r0
            r0 = r24
            int r0 = r0.m_nPlayerToMove
            r18 = r0
            int r18 = r18 * r9
            goto L_0x0016
        L_0x004e:
            r0 = r24
            r1 = r3
            byte r16 = r0.GetColMask(r1)
            byte[] r18 = dalmax.games.turnBasedGames.connect4.Connect4Engine.s_nIdxFirstColBit
            byte r17 = r18[r16]
            if (r17 != 0) goto L_0x0062
        L_0x005b:
            int r18 = r3 + 1
            r0 = r18
            byte r0 = (byte) r0
            r3 = r0
            goto L_0x0028
        L_0x0062:
            r18 = 1
            int r18 = r17 - r18
            r0 = r18
            byte r0 = (byte) r0
            r17 = r0
            r7 = -1
        L_0x006c:
            r18 = 1
            r0 = r7
            r1 = r18
            if (r0 > r1) goto L_0x005b
            r6 = 4
            r0 = r24
            int r0 = r0.m_nPlayerToMove
            r18 = r0
            r0 = r7
            r1 = r18
            if (r0 != r1) goto L_0x0080
            r6 = 3
        L_0x0080:
            r18 = 3
            int r18 = r3 - r18
            r19 = 0
            int r14 = dalmax.Util.Max(r18, r19)
            int r18 = r3 + 3
            r0 = r24
            byte r0 = r0.m_nNCol
            r19 = r0
            r20 = 1
            int r19 = r19 - r20
            int r12 = dalmax.Util.Min(r18, r19)
            r4 = 0
            r5 = 1
            r18 = 1
            int r10 = r3 - r18
        L_0x00a0:
            if (r10 < r14) goto L_0x00b6
            r0 = r24
            byte[][] r0 = r0.m_Matrix
            r18 = r0
            r18 = r18[r17]
            byte r18 = r18[r10]
            r0 = r7
            int r0 = -r0
            r19 = r0
            r0 = r18
            r1 = r19
            if (r0 != r1) goto L_0x00ef
        L_0x00b6:
            int r10 = r3 + 1
        L_0x00b8:
            if (r10 > r12) goto L_0x00ce
            r0 = r24
            byte[][] r0 = r0.m_Matrix
            r18 = r0
            r18 = r18[r17]
            byte r18 = r18[r10]
            r0 = r7
            int r0 = -r0
            r19 = r0
            r0 = r18
            r1 = r19
            if (r0 != r1) goto L_0x010e
        L_0x00ce:
            int r18 = r4 + r5
            r19 = 4
            r0 = r18
            r1 = r19
            if (r0 < r1) goto L_0x0141
            if (r4 <= 0) goto L_0x0141
            if (r4 < r6) goto L_0x013b
            r18 = 4
            r0 = r4
            r1 = r18
            if (r0 != r1) goto L_0x012d
            r0 = r24
            int r0 = r0.m_nPlayerToMove
            r18 = r0
            int r18 = r18 * r7
            int r18 = r18 * r2
            goto L_0x0016
        L_0x00ef:
            r0 = r24
            byte[][] r0 = r0.m_Matrix
            r18 = r0
            r18 = r18[r17]
            byte r18 = r18[r10]
            r0 = r18
            r1 = r7
            if (r0 != r1) goto L_0x0107
            int r18 = r4 + 1
            r0 = r18
            byte r0 = (byte) r0
            r4 = r0
        L_0x0104:
            int r10 = r10 + -1
            goto L_0x00a0
        L_0x0107:
            int r18 = r5 + 1
            r0 = r18
            byte r0 = (byte) r0
            r5 = r0
            goto L_0x0104
        L_0x010e:
            r0 = r24
            byte[][] r0 = r0.m_Matrix
            r18 = r0
            r18 = r18[r17]
            byte r18 = r18[r10]
            r0 = r18
            r1 = r7
            if (r0 != r1) goto L_0x0126
            int r18 = r4 + 1
            r0 = r18
            byte r0 = (byte) r0
            r4 = r0
        L_0x0123:
            int r10 = r10 + 1
            goto L_0x00b8
        L_0x0126:
            int r18 = r5 + 1
            r0 = r18
            byte r0 = (byte) r0
            r5 = r0
            goto L_0x0123
        L_0x012d:
            r0 = r24
            int r0 = r0.m_nPlayerToMove
            r18 = r0
            int r18 = r18 * r7
            int r19 = r2 >> 1
            int r18 = r18 * r19
            goto L_0x0016
        L_0x013b:
            int r18 = r4 << r4
            int r18 = r18 * r7
            int r9 = r9 + r18
        L_0x0141:
            r18 = 3
            int r18 = r17 - r18
            r19 = 0
            int r15 = dalmax.Util.Max(r18, r19)
            int r18 = r17 + 3
            r0 = r24
            byte r0 = r0.m_nNRow
            r19 = r0
            r20 = 1
            int r19 = r19 - r20
            int r13 = dalmax.Util.Min(r18, r19)
            r4 = 0
            int r18 = r17 + 1
            r0 = r18
            byte r0 = (byte) r0
            r5 = r0
            int r11 = r17 + 1
        L_0x0164:
            if (r11 > r13) goto L_0x017a
            r0 = r24
            byte[][] r0 = r0.m_Matrix
            r18 = r0
            r18 = r18[r11]
            byte r18 = r18[r3]
            r0 = r7
            int r0 = -r0
            r19 = r0
            r0 = r18
            r1 = r19
            if (r0 != r1) goto L_0x019b
        L_0x017a:
            int r18 = r4 + r5
            r19 = 4
            r0 = r18
            r1 = r19
            if (r0 < r1) goto L_0x01b8
            if (r4 <= 0) goto L_0x01b8
            if (r4 < r6) goto L_0x01b2
            r18 = 4
            r0 = r4
            r1 = r18
            if (r0 != r1) goto L_0x01a4
            r0 = r24
            int r0 = r0.m_nPlayerToMove
            r18 = r0
            int r18 = r18 * r7
            int r18 = r18 * r2
            goto L_0x0016
        L_0x019b:
            int r18 = r4 + 1
            r0 = r18
            byte r0 = (byte) r0
            r4 = r0
            int r11 = r11 + 1
            goto L_0x0164
        L_0x01a4:
            r0 = r24
            int r0 = r0.m_nPlayerToMove
            r18 = r0
            int r18 = r18 * r7
            int r19 = r2 >> 1
            int r18 = r18 * r19
            goto L_0x0016
        L_0x01b2:
            int r18 = r4 << r4
            int r18 = r18 * r7
            int r9 = r9 + r18
        L_0x01b8:
            r4 = 0
            r5 = 1
            r18 = 1
            int r11 = r17 - r18
            r18 = 1
            int r10 = r3 - r18
        L_0x01c2:
            if (r11 < r15) goto L_0x01da
            if (r10 < r14) goto L_0x01da
            r0 = r24
            byte[][] r0 = r0.m_Matrix
            r18 = r0
            r18 = r18[r11]
            byte r18 = r18[r10]
            r0 = r7
            int r0 = -r0
            r19 = r0
            r0 = r18
            r1 = r19
            if (r0 != r1) goto L_0x0217
        L_0x01da:
            int r11 = r17 + 1
            int r10 = r3 + 1
        L_0x01de:
            if (r11 > r13) goto L_0x01f6
            if (r10 > r12) goto L_0x01f6
            r0 = r24
            byte[][] r0 = r0.m_Matrix
            r18 = r0
            r18 = r18[r11]
            byte r18 = r18[r10]
            r0 = r7
            int r0 = -r0
            r19 = r0
            r0 = r18
            r1 = r19
            if (r0 != r1) goto L_0x0238
        L_0x01f6:
            int r18 = r4 + r5
            r19 = 4
            r0 = r18
            r1 = r19
            if (r0 < r1) goto L_0x026d
            if (r4 <= 0) goto L_0x026d
            if (r4 < r6) goto L_0x0267
            r18 = 4
            r0 = r4
            r1 = r18
            if (r0 != r1) goto L_0x0259
            r0 = r24
            int r0 = r0.m_nPlayerToMove
            r18 = r0
            int r18 = r18 * r7
            int r18 = r18 * r2
            goto L_0x0016
        L_0x0217:
            r0 = r24
            byte[][] r0 = r0.m_Matrix
            r18 = r0
            r18 = r18[r11]
            byte r18 = r18[r10]
            r0 = r18
            r1 = r7
            if (r0 != r1) goto L_0x0231
            int r18 = r4 + 1
            r0 = r18
            byte r0 = (byte) r0
            r4 = r0
        L_0x022c:
            int r11 = r11 + -1
            int r10 = r10 + -1
            goto L_0x01c2
        L_0x0231:
            int r18 = r5 + 1
            r0 = r18
            byte r0 = (byte) r0
            r5 = r0
            goto L_0x022c
        L_0x0238:
            r0 = r24
            byte[][] r0 = r0.m_Matrix
            r18 = r0
            r18 = r18[r11]
            byte r18 = r18[r10]
            r0 = r18
            r1 = r7
            if (r0 != r1) goto L_0x0252
            int r18 = r4 + 1
            r0 = r18
            byte r0 = (byte) r0
            r4 = r0
        L_0x024d:
            int r11 = r11 + 1
            int r10 = r10 + 1
            goto L_0x01de
        L_0x0252:
            int r18 = r5 + 1
            r0 = r18
            byte r0 = (byte) r0
            r5 = r0
            goto L_0x024d
        L_0x0259:
            r0 = r24
            int r0 = r0.m_nPlayerToMove
            r18 = r0
            int r18 = r18 * r7
            int r19 = r2 >> 1
            int r18 = r18 * r19
            goto L_0x0016
        L_0x0267:
            int r18 = r4 << r4
            int r18 = r18 * r7
            int r9 = r9 + r18
        L_0x026d:
            r4 = 0
            r5 = 1
            r18 = 1
            int r11 = r17 - r18
            int r10 = r3 + 1
        L_0x0275:
            if (r11 < r15) goto L_0x028d
            if (r10 > r12) goto L_0x028d
            r0 = r24
            byte[][] r0 = r0.m_Matrix
            r18 = r0
            r18 = r18[r11]
            byte r18 = r18[r10]
            r0 = r7
            int r0 = -r0
            r19 = r0
            r0 = r18
            r1 = r19
            if (r0 != r1) goto L_0x02cc
        L_0x028d:
            int r11 = r17 + 1
            r18 = 1
            int r10 = r3 - r18
        L_0x0293:
            if (r11 > r13) goto L_0x02ab
            if (r10 < r14) goto L_0x02ab
            r0 = r24
            byte[][] r0 = r0.m_Matrix
            r18 = r0
            r18 = r18[r11]
            byte r18 = r18[r10]
            r0 = r7
            int r0 = -r0
            r19 = r0
            r0 = r18
            r1 = r19
            if (r0 != r1) goto L_0x02ed
        L_0x02ab:
            int r18 = r4 + r5
            r19 = 4
            r0 = r18
            r1 = r19
            if (r0 < r1) goto L_0x0322
            if (r4 <= 0) goto L_0x0322
            if (r4 < r6) goto L_0x031c
            r18 = 4
            r0 = r4
            r1 = r18
            if (r0 != r1) goto L_0x030e
            r0 = r24
            int r0 = r0.m_nPlayerToMove
            r18 = r0
            int r18 = r18 * r7
            int r18 = r18 * r2
            goto L_0x0016
        L_0x02cc:
            r0 = r24
            byte[][] r0 = r0.m_Matrix
            r18 = r0
            r18 = r18[r11]
            byte r18 = r18[r10]
            r0 = r18
            r1 = r7
            if (r0 != r1) goto L_0x02e6
            int r18 = r4 + 1
            r0 = r18
            byte r0 = (byte) r0
            r4 = r0
        L_0x02e1:
            int r11 = r11 + -1
            int r10 = r10 + 1
            goto L_0x0275
        L_0x02e6:
            int r18 = r5 + 1
            r0 = r18
            byte r0 = (byte) r0
            r5 = r0
            goto L_0x02e1
        L_0x02ed:
            r0 = r24
            byte[][] r0 = r0.m_Matrix
            r18 = r0
            r18 = r18[r11]
            byte r18 = r18[r10]
            r0 = r18
            r1 = r7
            if (r0 != r1) goto L_0x0307
            int r18 = r4 + 1
            r0 = r18
            byte r0 = (byte) r0
            r4 = r0
        L_0x0302:
            int r11 = r11 + 1
            int r10 = r10 + -1
            goto L_0x0293
        L_0x0307:
            int r18 = r5 + 1
            r0 = r18
            byte r0 = (byte) r0
            r5 = r0
            goto L_0x0302
        L_0x030e:
            r0 = r24
            int r0 = r0.m_nPlayerToMove
            r18 = r0
            int r18 = r18 * r7
            int r19 = r2 >> 1
            int r18 = r18 * r19
            goto L_0x0016
        L_0x031c:
            int r18 = r4 << r4
            int r18 = r18 * r7
            int r9 = r9 + r18
        L_0x0322:
            int r18 = r7 + 2
            r0 = r18
            byte r0 = (byte) r0
            r7 = r0
            goto L_0x006c
        */
        throw new UnsupportedOperationException("Method not decompiled: dalmax.games.turnBasedGames.connect4.Connect4Engine.GetStaticBoardScore():int");
    }

    public final int GetNextPlayerToMove(int nPlayerToMove) {
        return -nPlayerToMove;
    }

    /* access modifiers changed from: protected */
    public int ComputeEngineMoveList(ArrayList<EngineMove> lMoveList) {
        int nNMoves = 0;
        for (byte c = 0; c < this.m_nNCol; c = (byte) (c + 1)) {
            byte r = (byte) (this.m_nNRow - 1);
            while (true) {
                if (r < 0) {
                    break;
                } else if (this.m_Matrix[r][c] == 0) {
                    lMoveList.add(new Connect4EngineMove(c, r));
                    nNMoves++;
                    break;
                } else {
                    r = (byte) (r - 1);
                }
            }
        }
        return nNMoves;
    }

    public Engine.EWinPairLose CheckWinPairLose(boolean bThereAreMoves) {
        if (this.m_LastMenPushed != null && WinByMove((byte) this.m_LastMenPushed.y, (byte) this.m_LastMenPushed.x)) {
            return Engine.EWinPairLose.Lose;
        }
        byte c = 0;
        while (c < this.m_nNCol) {
            byte r = s_nIdxFirstColBit[GetColMask(c)];
            if (r >= this.m_nNRow || !WinByMove(r, c)) {
                c = (byte) (c + 1);
            } else if (this.m_Matrix[r][c] * this.m_nPlayerToMove > 0) {
                return Engine.EWinPairLose.Win;
            } else {
                return Engine.EWinPairLose.Lose;
            }
        }
        if (!bThereAreMoves) {
            return Engine.EWinPairLose.Pair;
        }
        return Engine.EWinPairLose.None;
    }

    /* access modifiers changed from: protected */
    public boolean WinByMove(byte r, byte c) {
        int minC = Util.Max(c - 3, 0);
        int maxC = Util.Min(c + 3, this.m_nNCol - 1);
        byte curr = this.m_Matrix[r][c];
        int cnt = 1;
        int ic = c - 1;
        while (ic >= minC && this.m_Matrix[r][ic] == curr) {
            cnt++;
            ic--;
        }
        int ic2 = c + 1;
        while (ic2 <= maxC && this.m_Matrix[r][ic2] == curr) {
            cnt++;
            ic2++;
        }
        if (cnt >= 4) {
            return true;
        }
        int minR = Util.Max(r - 4, 0);
        int maxR = Util.Min(r + 4, this.m_nNRow - 1);
        int cnt2 = 1;
        int ir = r - 1;
        while (ir >= minR && this.m_Matrix[ir][c] == curr) {
            cnt2++;
            ir--;
        }
        int ir2 = r + 1;
        while (ir2 <= maxR && this.m_Matrix[ir2][c] == curr) {
            cnt2++;
            ir2++;
        }
        if (cnt2 >= 4) {
            return true;
        }
        int cnt3 = 1;
        int ir3 = r - 1;
        int ic3 = c - 1;
        while (ir3 >= minR && ic3 >= minC && this.m_Matrix[ir3][ic3] == curr) {
            cnt3++;
            ir3--;
            ic3--;
        }
        int ir4 = r + 1;
        int ic4 = c + 1;
        while (ir4 <= maxR && ic4 <= maxC && this.m_Matrix[ir4][ic4] == curr) {
            cnt3++;
            ir4++;
            ic4++;
        }
        if (cnt3 >= 4) {
            return true;
        }
        int cnt4 = 1;
        int ir5 = r - 1;
        int ic5 = c + 1;
        while (ir5 >= minR && ic5 <= maxC && this.m_Matrix[ir5][ic5] == curr) {
            cnt4++;
            ir5--;
            ic5++;
        }
        int ir6 = r + 1;
        int ic6 = c - 1;
        while (ir6 <= maxR && ic6 >= minC && this.m_Matrix[ir6][ic6] == curr) {
            cnt4++;
            ir6++;
            ic6--;
        }
        if (cnt4 >= 4) {
            return true;
        }
        return false;
    }
}
