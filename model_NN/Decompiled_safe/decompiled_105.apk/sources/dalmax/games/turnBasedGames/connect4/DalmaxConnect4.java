package dalmax.games.turnBasedGames.connect4;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Debug;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.AdWhirlManager;
import com.adwhirl.AdWhirlTargeting;
import dalmax.games.turnBasedGames.views.TwoPlayerView;
import dalmax.views.LedView;
import java.util.Random;

public class DalmaxConnect4 extends Activity implements SeekBar.OnSeekBarChangeListener, AdWhirlLayout.AdWhirlInterface {
    static boolean s_bTraceLog = false;
    LedView m_AndroidLedView = null;
    LedView m_HumanLedView = null;
    ProgressBar m_androidThinking;
    TwoPlayerView.EGameMode m_eGameMode = TwoPlayerView.EGameMode.singlePlayer;
    short m_nLevel = 100;
    Connect4View m_oConnect4BoardView = null;
    Connect4Engine m_oConnect4Engine = null;
    SeekBar m_oSeekBarLevel = null;
    RadioButton m_radio_android;
    RadioButton m_radio_human;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (s_bTraceLog) {
            Debug.startMethodTracing("DalmaxFourInRow");
        }
        this.m_nLevel = (short) getPreferences(0).getInt("level", 100);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        StartMenu(null);
    }

    private void AdWhirl() {
        AdWhirlManager.setConfigExpireTimeout(300000);
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) findViewById(R.id.adwhirl_layout);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        AdWhirlTargeting.setTestMode(false);
        float density = getResources().getDisplayMetrics().density;
        adWhirlLayout.setAdWhirlInterface(this);
        adWhirlLayout.setMaxWidth((int) (((float) 320) * density));
        adWhirlLayout.setMaxHeight((int) (((float) 52) * density));
        layoutParams.addRule(14);
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.AdsRelativeLayout);
        layout.setGravity(1);
        layout.invalidate();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        SharedPreferences.Editor editor = getPreferences(0).edit();
        editor.putInt("level", this.m_nLevel);
        editor.commit();
        if (s_bTraceLog) {
            Debug.stopMethodTracing();
        }
    }

    public void StartMenu(View view) {
        setContentView((int) R.layout.main);
        AdWhirl();
        if (this.m_oConnect4Engine != null) {
            this.m_oConnect4Engine.Exits();
            this.m_oConnect4Engine = null;
        }
        this.m_oConnect4Engine = null;
    }

    public void StartMenu_1PlayerMode(View view) {
        this.m_eGameMode = TwoPlayerView.EGameMode.singlePlayer;
        setContentView((int) R.layout.menu1p);
        AdWhirl();
        if (this.m_oConnect4Engine != null) {
            this.m_oConnect4Engine.Exits();
            this.m_oConnect4Engine = null;
        }
        this.m_oConnect4BoardView = null;
        ((TextView) findViewById(R.id.label_level)).setText(((Object) getResources().getText(R.string.diff_level)) + ": " + String.valueOf((int) this.m_nLevel) + " %");
        this.m_oSeekBarLevel = (SeekBar) findViewById(R.id.seekbar_level);
        this.m_oSeekBarLevel.setMax(100);
        this.m_oSeekBarLevel.setProgress(this.m_nLevel);
        this.m_oSeekBarLevel.setOnSeekBarChangeListener(this);
        this.m_radio_human = (RadioButton) findViewById(R.id.radio_start_human);
        this.m_radio_android = (RadioButton) findViewById(R.id.radio_start_android);
    }

    public void StartMenu_2PlayersMode(View view) {
        this.m_eGameMode = TwoPlayerView.EGameMode.twoPlayers;
        setContentView((int) R.layout.menu2p);
        AdWhirl();
        if (this.m_oConnect4Engine != null) {
            this.m_oConnect4Engine.Exits();
            this.m_oConnect4Engine = null;
        }
        this.m_oConnect4BoardView = null;
        this.m_radio_human = (RadioButton) findViewById(R.id.radio_start_human);
        this.m_radio_android = (RadioButton) findViewById(R.id.radio_start_android);
    }

    public void SetHumanTurn(boolean bHumanTurn) {
        this.m_HumanLedView.SetState(bHumanTurn);
        this.m_AndroidLedView.SetState(!bHumanTurn);
        if (bHumanTurn || this.m_eGameMode == TwoPlayerView.EGameMode.twoPlayers) {
            this.m_androidThinking.setVisibility(4);
            return;
        }
        this.m_androidThinking.setIndeterminate(true);
        this.m_androidThinking.setVisibility(0);
    }

    public void SetEndedGame() {
        this.m_androidThinking.setVisibility(4);
    }

    public void StartNewGame(View view) {
        setContentView((int) R.layout.game_view);
        AdWhirl();
        this.m_androidThinking = (ProgressBar) findViewById(R.id.androidThinking);
        this.m_AndroidLedView = new LedView(this);
        ((FrameLayout) findViewById(R.id.topPlayerTurnLed)).addView(this.m_AndroidLedView);
        if (this.m_eGameMode == TwoPlayerView.EGameMode.twoPlayers) {
            ((TextView) findViewById(R.id.topPlayerTurn)).setText((int) R.string.PlayerHuman2);
            ((TextView) findViewById(R.id.bottomPlayerTurn)).setText((int) R.string.PlayerHuman1);
        } else {
            ((TextView) findViewById(R.id.topPlayerTurn)).setText((int) R.string.PlayerAndroid);
            ((TextView) findViewById(R.id.bottomPlayerTurn)).setText((int) R.string.PlayerUniqueHuman);
        }
        this.m_HumanLedView = new LedView(this);
        ((FrameLayout) findViewById(R.id.bottomPlayerTurnLed)).addView(this.m_HumanLedView);
        this.m_oConnect4Engine = new Connect4Engine(this.m_nLevel, (byte) 6, (byte) 7);
        this.m_oConnect4BoardView = new Connect4View(this, this.m_oConnect4Engine, GetHumanBegin(), this.m_eGameMode);
        new Thread(this.m_oConnect4Engine).start();
        ((FrameLayout) findViewById(R.id.board_holder)).addView(this.m_oConnect4BoardView);
    }

    /* access modifiers changed from: protected */
    public boolean GetHumanBegin() {
        if (this.m_eGameMode == TwoPlayerView.EGameMode.twoPlayers) {
            return true;
        }
        if (this.m_radio_human.isChecked()) {
            return true;
        }
        if (this.m_radio_android.isChecked()) {
            return false;
        }
        return new Random().nextBoolean();
    }

    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (this.m_oSeekBarLevel == seekBar) {
            this.m_nLevel = (short) progress;
            ((TextView) findViewById(R.id.label_level)).setText(((Object) getResources().getText(R.string.diff_level)) + ": " + String.valueOf((int) this.m_nLevel) + " %");
        }
    }

    public void QuitGame(View view) {
        new AlertDialog.Builder(this).setMessage((int) R.string.confirm_quit).setCancelable(false).setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                DalmaxConnect4.this.StartMenu(null);
            }
        }).setNegativeButton((int) R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        }).create().show();
    }

    public void Undo(View view) {
        if (this.m_oConnect4BoardView != null) {
            this.m_oConnect4BoardView.UndoMove();
        }
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    public void adWhirlGeneric() {
    }
}
