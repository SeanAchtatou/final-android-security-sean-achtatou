package dalmax.games.turnBasedGames.connect4;

import dalmax.games.turnBasedGames.GameStatusDescription;
import java.lang.reflect.Array;

public class Connect4BoardDescription extends GameStatusDescription {
    byte[][] m_Matrix;
    byte m_nNCols;
    byte m_nNRows;

    public Connect4BoardDescription() {
    }

    public Connect4BoardDescription(byte nNRows, byte nNCols) {
        this.m_nNRows = nNRows;
        this.m_nNCols = nNCols;
        this.m_Matrix = (byte[][]) Array.newInstance(Byte.TYPE, nNRows, nNCols);
    }

    public byte[][] GetBoard() {
        return this.m_Matrix;
    }

    public Object clone() throws CloneNotSupportedException {
        Connect4BoardDescription o = (Connect4BoardDescription) super.clone();
        o.m_Matrix = (byte[][]) Array.newInstance(Byte.TYPE, this.m_nNRows, this.m_nNCols);
        for (byte r = 0; r < this.m_nNRows; r = (byte) (r + 1)) {
            for (byte c = 0; c < this.m_nNCols; c = (byte) (c + 1)) {
                o.m_Matrix[r][c] = this.m_Matrix[r][c];
            }
        }
        return o;
    }
}
