package dalmax.games.turnBasedGames.connect4;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
    }

    public static final class drawable {
        public static final int icon = 2130837504;
        public static final int nuvole = 2130837505;
        public static final int title = 2130837506;
    }

    public static final class id {
        public static final int AdsRelativeLayout = 2131099648;
        public static final int adwhirl_layout = 2131099649;
        public static final int androidThinking = 2131099657;
        public static final int board_holder = 2131099651;
        public static final int bottomPlayerTurn = 2131099654;
        public static final int bottomPlayerTurnLed = 2131099653;
        public static final int button_1Player = 2131099661;
        public static final int button_2Players = 2131099662;
        public static final int button_backMenu = 2131099672;
        public static final int button_quit = 2131099659;
        public static final int button_start = 2131099663;
        public static final int button_undo = 2131099660;
        public static final int buttons = 2131099658;
        public static final int game = 2131099650;
        public static final int label_0 = 2131099665;
        public static final int label_100 = 2131099666;
        public static final int label_level = 2131099664;
        public static final int label_start = 2131099668;
        public static final int radio_start_android = 2131099671;
        public static final int radio_start_human = 2131099670;
        public static final int radio_start_random = 2131099669;
        public static final int seekbar_level = 2131099667;
        public static final int topPlayerTurn = 2131099655;
        public static final int topPlayerTurnLed = 2131099656;
        public static final int turns = 2131099652;
    }

    public static final class layout {
        public static final int game_view = 2130903040;
        public static final int main = 2130903041;
        public static final int menu1p = 2130903042;
        public static final int menu2p = 2130903043;
    }

    public static final class string {
        public static final int BackToMainMenu = 2130968607;
        public static final int PlayerAndroid = 2130968601;
        public static final int PlayerHuman1 = 2130968603;
        public static final int PlayerHuman2 = 2130968604;
        public static final int PlayerUniqueHuman = 2130968602;
        public static final int android = 2130968599;
        public static final int app_name = 2130968576;
        public static final int cento_perc = 2130968596;
        public static final int confirm_quit = 2130968587;
        public static final int diff_level = 2130968594;
        public static final int exit = 2130968593;
        public static final int goBackToGame = 2130968592;
        public static final int human = 2130968598;
        public static final int new_game = 2130968579;
        public static final int no = 2130968589;
        public static final int ok = 2130968590;
        public static final int pair = 2130968584;
        public static final int playerLoseWithComputer = 2130968581;
        public static final int playerWinIn2PlayerMode = 2130968583;
        public static final int playerWinWithComputer = 2130968582;
        public static final int quit_game = 2130968586;
        public static final int random = 2130968600;
        public static final int restart = 2130968591;
        public static final int rulesTitle = 2130968585;
        public static final int settings = 2130968580;
        public static final int showRules = 2130968606;
        public static final int start1Player = 2130968577;
        public static final int start2Players = 2130968578;
        public static final int undo = 2130968605;
        public static final int whoStart = 2130968597;
        public static final int yes = 2130968588;
        public static final int zero_perc = 2130968595;
    }

    public static final class style {
        public static final int CodeFont = 2131034112;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
    }
}
