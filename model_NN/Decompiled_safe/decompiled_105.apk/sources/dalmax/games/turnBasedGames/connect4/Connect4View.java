package dalmax.games.turnBasedGames.connect4;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import dalmax.Coords2D;
import dalmax.Util;
import dalmax.games.turnBasedGames.GameStatusDescription;
import dalmax.games.turnBasedGames.ViewMove;
import dalmax.games.turnBasedGames.views.TwoPlayerView;
import java.util.Iterator;

public class Connect4View extends TwoPlayerView {
    static int s_nMaxTimeForMove = 3000;
    Bitmap m_BoardBitmap;
    final Paint m_EraserPaint = new Paint();
    Bitmap m_SelectedBitmap;
    Coords2D[] m_WinnerCoords = null;
    boolean m_bToDraw = true;
    Bitmap.Config m_bmpconfig = Bitmap.Config.ARGB_8888;
    int m_nBoardHeightSize;
    int m_nBoardWidthSize;
    byte m_nNCols = 7;
    byte m_nNRows = 6;
    int m_nSelectedCol = -1;
    DalmaxConnect4 m_oConnect4Context;
    Bitmap m_oRManBitmap;
    Bitmap m_oWallPaper;
    Bitmap m_oYManBitmap;

    public Connect4View(Context oContext, Connect4Engine oFourInRowEngine, boolean bHumanBegin, TwoPlayerView.EGameMode eGameMode) {
        super(oContext, bHumanBegin, eGameMode, oFourInRowEngine, s_nMaxTimeForMove, 0L);
        this.m_oConnect4Context = (DalmaxConnect4) oContext;
        this.m_oMoveCurrent = new Connect4ViewMove();
        this.m_EraserPaint.setAlpha(0);
        this.m_EraserPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        this.m_EraserPaint.setAntiAlias(true);
        this.m_oBoardDescription = new Connect4BoardDescription(this.m_nNRows, this.m_nNCols);
        this.m_oBoardDescription.SetPlayer((byte) (IsFirstHumanTurn() ? -1 : 1));
        this.m_oEngine.SetInitialBoard(this.m_oBoardDescription);
        if (!IsFirstHumanTurn() && eGameMode == TwoPlayerView.EGameMode.singlePlayer) {
            MakeComputerMove(s_nMaxTimeForMove);
        }
    }

    /* access modifiers changed from: protected */
    public void SetFirstHumanTurn(boolean bFirstHumanTurn) {
        this.m_bFirstHumanTurn = bFirstHumanTurn;
        ((DalmaxConnect4) super.getContext()).SetHumanTurn(bFirstHumanTurn);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int nWidth = View.MeasureSpec.getSize(widthMeasureSpec);
        int nHeight = View.MeasureSpec.getSize(heightMeasureSpec);
        if (nWidth / this.m_nNCols < nHeight / (this.m_nNRows + 1)) {
            this.m_nBoardWidthSize = nWidth;
            this.m_nBoardHeightSize = ((this.m_nNRows + 1) * nWidth) / this.m_nNCols;
        } else {
            this.m_nBoardHeightSize = nHeight;
            this.m_nBoardWidthSize = (this.m_nNCols * nHeight) / (this.m_nNRows + 1);
        }
        this.m_ScreenBoardCoords.Set(0, (this.m_nBoardWidthSize * (this.m_nNCols - this.m_nNRows)) / this.m_nNCols);
        setMeasuredDimension(this.m_nBoardWidthSize, this.m_nBoardHeightSize);
        this.m_bToDraw = true;
    }

    /* access modifiers changed from: package-private */
    public void PrepareOffLineDraws() {
        boolean bError = false;
        try {
            this.m_bmpconfig = Bitmap.Config.ARGB_8888;
            PrepareOfflineDrawInner();
        } catch (Exception e) {
            bError = true;
        }
        if (bError) {
            Runtime.getRuntime().gc();
            this.m_bmpconfig = Bitmap.Config.ARGB_4444;
            PrepareOfflineDrawInner();
        }
        this.m_bToDraw = false;
    }

    /* access modifiers changed from: package-private */
    public void PrepareOfflineDrawInner() {
        Canvas oWallpaperCanvas = new Canvas();
        this.m_oWallPaper = Bitmap.createBitmap(this.m_nBoardWidthSize, this.m_nBoardHeightSize, this.m_bmpconfig);
        oWallpaperCanvas.setBitmap(this.m_oWallPaper);
        oWallpaperCanvas.drawColor(-15654145);
        try {
            oWallpaperCanvas.drawBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.nuvole), (Rect) null, new Rect(0, 0, this.m_nBoardWidthSize, this.m_nBoardHeightSize), (Paint) null);
        } catch (Exception e) {
        }
        int nCaseSize = this.m_nBoardWidthSize / this.m_nNCols;
        int nHoleSize = (int) (((double) nCaseSize) * 0.75d);
        this.m_BoardBitmap = Bitmap.createBitmap(this.m_nBoardWidthSize, this.m_nBoardHeightSize, this.m_bmpconfig);
        Canvas bitmapCanvas = new Canvas();
        bitmapCanvas.setBitmap(this.m_BoardBitmap);
        Paint pnt = new Paint();
        pnt.setARGB(255, 34, 34, 255);
        bitmapCanvas.drawRect(0.0f, 0.0f, (float) (this.m_nBoardWidthSize - 2), (float) (this.m_nBoardHeightSize - 2), pnt);
        pnt.setARGB(255, 5, 5, 85);
        bitmapCanvas.drawRect(2.0f, 2.0f, (float) this.m_nBoardWidthSize, (float) this.m_nBoardHeightSize, pnt);
        pnt.setARGB(255, 17, 17, 170);
        bitmapCanvas.drawRect(2.0f, 2.0f, (float) (this.m_nBoardWidthSize - 2), (float) (this.m_nBoardHeightSize - 2), pnt);
        for (int r = 0; r < this.m_nNRows; r++) {
            for (int c = 0; c < this.m_nNCols; c++) {
                pnt.setARGB(255, 34, 34, 255);
                bitmapCanvas.drawCircle((float) (((nCaseSize * c) + (nCaseSize >> 1)) - 2), (float) (((nCaseSize * r) + (nCaseSize >> 1)) - 2), (float) ((nHoleSize >> 1) + 2), pnt);
                pnt.setARGB(255, 5, 5, 102);
                bitmapCanvas.drawCircle((float) ((nCaseSize * c) + (nCaseSize >> 1) + 2), (float) ((nCaseSize * r) + (nCaseSize >> 1) + 2), (float) ((nHoleSize >> 1) + 2), pnt);
                pnt.setARGB(255, 5, 5, 102);
                bitmapCanvas.drawCircle((float) (((nCaseSize * c) + (nCaseSize >> 1)) - 2), (float) (((nCaseSize * r) + (nCaseSize >> 1)) - 2), (float) (nHoleSize >> 1), pnt);
                pnt.setARGB(255, 34, 34, 255);
                bitmapCanvas.drawCircle((float) ((nCaseSize * c) + (nCaseSize >> 1) + 2), (float) ((nCaseSize * r) + (nCaseSize >> 1) + 2), (float) (nHoleSize >> 1), pnt);
                bitmapCanvas.drawCircle((float) ((nCaseSize * c) + (nCaseSize >> 1)), (float) ((nCaseSize * r) + (nCaseSize >> 1)), (float) (nHoleSize >> 1), this.m_EraserPaint);
            }
        }
        this.m_SelectedBitmap = Bitmap.createBitmap(nCaseSize, this.m_nBoardHeightSize, this.m_bmpconfig);
        Canvas selectedBitmapCanvas = new Canvas();
        selectedBitmapCanvas.setBitmap(this.m_SelectedBitmap);
        selectedBitmapCanvas.drawColor(-65536);
        Paint myPaint = new Paint();
        myPaint.setAlpha(64);
        myPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        myPaint.setAntiAlias(true);
        selectedBitmapCanvas.drawRect(0.0f, 0.0f, (float) nCaseSize, (float) this.m_nBoardHeightSize, myPaint);
        Rect oCellBound = new Rect();
        this.m_oRManBitmap = Bitmap.createBitmap(nCaseSize, nCaseSize, this.m_bmpconfig);
        Canvas canvas = new Canvas(this.m_oRManBitmap);
        this.m_oYManBitmap = Bitmap.createBitmap(nCaseSize, nCaseSize, this.m_bmpconfig);
        Canvas canvas2 = new Canvas(this.m_oYManBitmap);
        int nMargin = nCaseSize >> 4;
        int nBordo = nCaseSize / 6;
        if (nMargin < 1) {
            nMargin = 1;
        }
        if (nBordo < 1) {
            nBordo = 2;
        }
        oCellBound.set(nMargin, nMargin, nCaseSize, nCaseSize);
        DrawMan(-11206656, canvas, oCellBound);
        DrawMan(-11184896, canvas2, oCellBound);
        oCellBound.set(0, 0, nCaseSize - nMargin, nCaseSize - nMargin);
        DrawMan(-65536, canvas, oCellBound);
        DrawMan(-256, canvas2, oCellBound);
        oCellBound.set(nMargin, nMargin, nCaseSize - nMargin, nCaseSize - nMargin);
        DrawMan(-5636096, canvas, oCellBound);
        DrawMan(-5592576, canvas2, oCellBound);
        oCellBound.set(nBordo, nBordo, nCaseSize - (nBordo + nMargin), nCaseSize - (nBordo + nMargin));
        DrawMan(-11206656, canvas, oCellBound);
        DrawMan(-11184896, canvas2, oCellBound);
        oCellBound.set(nBordo + nMargin, nBordo + nMargin, nCaseSize - nBordo, nCaseSize - nBordo);
        DrawMan(-65536, canvas, oCellBound);
        DrawMan(-256, canvas2, oCellBound);
        oCellBound.set(nBordo + nMargin, nBordo + nMargin, nCaseSize - (nBordo + nMargin), nCaseSize - (nBordo + nMargin));
        DrawMan(-5636096, canvas, oCellBound);
        DrawMan(-5592576, canvas2, oCellBound);
    }

    /* access modifiers changed from: package-private */
    public void DrawMan(int nColor, Canvas oManCanvas, Rect oCellBound) {
        ShapeDrawable currShape = new ShapeDrawable(new OvalShape());
        currShape.setBounds(oCellBound);
        currShape.getPaint().setColor(nColor);
        currShape.draw(oManCanvas);
    }

    /* access modifiers changed from: protected */
    public void DrawStaticPositionToCanvas(Canvas canvas, GameStatusDescription oBoardDescription) {
        Bitmap oManBitmap;
        if (this.m_bToDraw) {
            PrepareOffLineDraws();
        }
        canvas.drawBitmap(this.m_oWallPaper, 0.0f, 0.0f, (Paint) null);
        Connect4BoardDescription oBoardDescr = (Connect4BoardDescription) oBoardDescription;
        int nCaseSize = this.m_nBoardWidthSize / this.m_nNCols;
        for (int r = 0; r < this.m_nNRows; r++) {
            for (int c = 0; c < this.m_nNCols; c++) {
                if (oBoardDescr.GetBoard()[r][c] != 0) {
                    if (oBoardDescr.GetBoard()[r][c] > 0) {
                        canvas.drawBitmap(this.m_oRManBitmap, (float) ((nCaseSize * c) + this.m_ScreenBoardCoords.x), (float) ((nCaseSize * r) + this.m_ScreenBoardCoords.y), (Paint) null);
                    } else {
                        canvas.drawBitmap(this.m_oYManBitmap, (float) ((nCaseSize * c) + this.m_ScreenBoardCoords.x), (float) ((nCaseSize * r) + this.m_ScreenBoardCoords.y), (Paint) null);
                    }
                }
            }
        }
        canvas.drawBitmap(this.m_BoardBitmap, (float) this.m_ScreenBoardCoords.x, (float) this.m_ScreenBoardCoords.y, new Paint());
        if (this.m_nSelectedCol >= 0) {
            if (this.m_bFirstHumanTurn) {
                oManBitmap = this.m_oYManBitmap;
            } else {
                oManBitmap = this.m_oRManBitmap;
            }
            canvas.drawBitmap(oManBitmap, (float) ((this.m_nSelectedCol * nCaseSize) + this.m_ScreenBoardCoords.x), 0.0f, (Paint) null);
            canvas.drawBitmap(this.m_SelectedBitmap, (float) ((this.m_nSelectedCol * nCaseSize) + this.m_ScreenBoardCoords.x), 0.0f, new Paint());
        }
        if (!this.m_bEndedGame || this.m_WinnerCoords == null) {
            this.m_WinnerCoords = null;
            return;
        }
        Paint paint = new Paint();
        paint.setColor(-16711936);
        int nLineWidth = nCaseSize >> 3;
        if (nLineWidth < 2) {
            nLineWidth = 2;
        }
        paint.setStrokeWidth((float) nLineWidth);
        Canvas canvas2 = canvas;
        canvas2.drawLine((float) ((this.m_WinnerCoords[0].x * nCaseSize) + (nCaseSize / 2) + this.m_ScreenBoardCoords.x), (float) ((this.m_WinnerCoords[0].y * nCaseSize) + (nCaseSize / 2) + this.m_ScreenBoardCoords.y), (float) ((this.m_WinnerCoords[1].x * nCaseSize) + (nCaseSize / 2) + this.m_ScreenBoardCoords.x), (float) ((this.m_WinnerCoords[1].y * nCaseSize) + (nCaseSize / 2) + this.m_ScreenBoardCoords.y), paint);
    }

    public void WinnerDialog(int nWinnerPlayer) {
        String strMessage;
        this.m_WinnerCoords = WinSequence();
        DrawPosition();
        try {
            this.m_oConnect4Context.SetEndedGame();
            if (nWinnerPlayer == 0) {
                strMessage = this.m_oConnect4Context.getString(R.string.pair);
            } else if (this.m_eGameMode == TwoPlayerView.EGameMode.twoPlayers) {
                String strMessage2 = this.m_oConnect4Context.getString(R.string.playerWinIn2PlayerMode);
                if (nWinnerPlayer > 0) {
                    strMessage = String.valueOf(strMessage2) + " " + this.m_oConnect4Context.getString(R.string.PlayerHuman2);
                } else {
                    strMessage = String.valueOf(strMessage2) + " " + this.m_oConnect4Context.getString(R.string.PlayerHuman1);
                }
            } else if (nWinnerPlayer > 0) {
                strMessage = this.m_oConnect4Context.getString(R.string.playerLoseWithComputer);
            } else {
                strMessage = this.m_oConnect4Context.getString(R.string.playerWinWithComputer);
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setMessage(strMessage);
            final DalmaxConnect4 oActivity = (DalmaxConnect4) getContext();
            builder.setPositiveButton(this.m_oConnect4Context.getString(R.string.restart), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                    oActivity.StartNewGame(null);
                }
            });
            builder.setNeutralButton(this.m_oConnect4Context.getString(R.string.goBackToGame), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            builder.setNegativeButton(this.m_oConnect4Context.getString(R.string.exit), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                    oActivity.QuitGame(null);
                }
            });
            builder.create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 6 */
    /* access modifiers changed from: protected */
    public void ApplyMoveToBoardDescription(GameStatusDescription oBoardDescription) {
        Connect4BoardDescription oBoardDescr = (Connect4BoardDescription) oBoardDescription;
        for (int r = this.m_nNRows - 1; r >= 0; r--) {
            if (oBoardDescr.GetBoard()[r][((Connect4ViewMove) this.m_oMoveCurrent).m_c] == 0) {
                oBoardDescr.GetBoard()[r][((Connect4ViewMove) this.m_oMoveCurrent).m_c] = (byte) oBoardDescr.GetPlayer();
                ((Connect4ViewMove) this.m_oMoveCurrent).m_r = r;
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void DrawMove(dalmax.games.turnBasedGames.GameStatusDescription r31) {
        /*
            r30 = this;
            r0 = r30
            dalmax.games.turnBasedGames.ViewMove r0 = r0.m_oMoveCurrent     // Catch:{ Exception -> 0x019e }
            r5 = r0
            dalmax.games.turnBasedGames.connect4.Connect4ViewMove r5 = (dalmax.games.turnBasedGames.connect4.Connect4ViewMove) r5     // Catch:{ Exception -> 0x019e }
            r0 = r5
            int r0 = r0.m_c     // Catch:{ Exception -> 0x019e }
            r26 = r0
            if (r26 >= 0) goto L_0x000f
        L_0x000e:
            return
        L_0x000f:
            r16 = 100
            r0 = r30
            int r0 = r0.m_nBoardWidthSize     // Catch:{ Exception -> 0x019e }
            r26 = r0
            r0 = r30
            int r0 = r0.m_nBoardHeightSize     // Catch:{ Exception -> 0x019e }
            r27 = r0
            r0 = r30
            android.graphics.Bitmap$Config r0 = r0.m_bmpconfig     // Catch:{ Exception -> 0x019e }
            r28 = r0
            android.graphics.Bitmap r20 = android.graphics.Bitmap.createBitmap(r26, r27, r28)     // Catch:{ Exception -> 0x019e }
            android.graphics.Canvas r21 = new android.graphics.Canvas     // Catch:{ Exception -> 0x019e }
            r0 = r21
            r1 = r20
            r0.<init>(r1)     // Catch:{ Exception -> 0x019e }
            r0 = r30
            r1 = r21
            r2 = r31
            r0.DrawStaticPositionToCanvas(r1, r2)     // Catch:{ Exception -> 0x019e }
            r0 = r30
            int r0 = r0.m_nBoardWidthSize     // Catch:{ Exception -> 0x019e }
            r26 = r0
            r0 = r30
            byte r0 = r0.m_nNCols     // Catch:{ Exception -> 0x019e }
            r27 = r0
            int r9 = r26 / r27
            r0 = r30
            boolean r0 = r0.m_bFirstHumanTurn     // Catch:{ Exception -> 0x019e }
            r26 = r0
            if (r26 == 0) goto L_0x00b5
            r0 = r30
            android.graphics.Bitmap r0 = r0.m_oYManBitmap     // Catch:{ Exception -> 0x019e }
            r22 = r0
        L_0x0055:
            long r18 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x019e }
            r0 = r30
            dalmax.games.turnBasedGames.ViewMove r0 = r0.m_oMoveCurrent     // Catch:{ Exception -> 0x019e }
            r31 = r0
            dalmax.games.turnBasedGames.connect4.Connect4ViewMove r31 = (dalmax.games.turnBasedGames.connect4.Connect4ViewMove) r31     // Catch:{ Exception -> 0x019e }
            r0 = r31
            int r0 = r0.m_c     // Catch:{ Exception -> 0x019e }
            r5 = r0
            r0 = r30
            dalmax.games.turnBasedGames.ViewMove r0 = r0.m_oMoveCurrent     // Catch:{ Exception -> 0x019e }
            r31 = r0
            dalmax.games.turnBasedGames.connect4.Connect4ViewMove r31 = (dalmax.games.turnBasedGames.connect4.Connect4ViewMove) r31     // Catch:{ Exception -> 0x019e }
            r0 = r31
            int r0 = r0.m_r     // Catch:{ Exception -> 0x019e }
            r24 = r0
            int r13 = -r9
            int r26 = r24 * r9
            int r27 = r9 >> 1
            int r12 = r26 + r27
            r0 = r12
            long r0 = (long) r0     // Catch:{ Exception -> 0x019e }
            r26 = r0
            long r26 = r26 * r16
            r0 = r9
            long r0 = (long) r0     // Catch:{ Exception -> 0x019e }
            r28 = r0
            long r14 = r26 / r28
            r10 = r18
        L_0x0089:
            long r26 = r18 + r14
            int r26 = (r10 > r26 ? 1 : (r10 == r26 ? 0 : -1))
            if (r26 < 0) goto L_0x00bc
            r23 = 0
            r0 = r30
            dalmax.games.turnBasedGames.connect4.DalmaxConnect4 r0 = r0.m_oConnect4Context     // Catch:{ Exception -> 0x00ae }
            r26 = r0
            java.lang.String r27 = "vibrator"
            java.lang.Object r24 = r26.getSystemService(r27)     // Catch:{ Exception -> 0x00ae }
            r0 = r24
            android.os.Vibrator r0 = (android.os.Vibrator) r0     // Catch:{ Exception -> 0x00ae }
            r23 = r0
            r26 = 50
            r0 = r23
            r1 = r26
            r0.vibrate(r1)     // Catch:{ Exception -> 0x00ae }
            goto L_0x000e
        L_0x00ae:
            r26 = move-exception
            r7 = r26
            r23 = 0
            goto L_0x000e
        L_0x00b5:
            r0 = r30
            android.graphics.Bitmap r0 = r0.m_oRManBitmap     // Catch:{ Exception -> 0x019e }
            r22 = r0
            goto L_0x0055
        L_0x00bc:
            long r26 = r10 - r18
            r0 = r26
            float r0 = (float) r0
            r26 = r0
            r0 = r14
            float r0 = (float) r0
            r27 = r0
            float r25 = r26 / r27
            r26 = 1065353216(0x3f800000, float:1.0)
            float r26 = r26 - r25
            int r27 = r9 >> 1
            r0 = r27
            int r0 = -r0
            r27 = r0
            r0 = r27
            float r0 = (float) r0
            r27 = r0
            float r26 = r26 * r27
            int r27 = r24 * r9
            r0 = r27
            float r0 = (float) r0
            r27 = r0
            float r27 = r27 * r25
            float r26 = r26 + r27
            r0 = r26
            int r0 = (int) r0
            r8 = r0
            if (r8 == r13) goto L_0x01a1
            r6 = 0
            r0 = r30
            android.view.SurfaceHolder r0 = r0.m_oSurfaceHolder     // Catch:{ all -> 0x018e }
            r26 = r0
            android.graphics.Canvas r6 = r26.lockCanvas()     // Catch:{ all -> 0x018e }
            if (r6 == 0) goto L_0x017a
            r26 = 0
            r27 = 0
            r28 = 0
            r0 = r6
            r1 = r20
            r2 = r26
            r3 = r27
            r4 = r28
            r0.drawBitmap(r1, r2, r3, r4)     // Catch:{ all -> 0x018e }
            r0 = r30
            dalmax.Coords2D r0 = r0.m_ScreenBoardCoords     // Catch:{ all -> 0x018e }
            r26 = r0
            r0 = r26
            int r0 = r0.x     // Catch:{ all -> 0x018e }
            r26 = r0
            int r27 = r9 * r5
            int r26 = r26 + r27
            r0 = r26
            float r0 = (float) r0     // Catch:{ all -> 0x018e }
            r26 = r0
            r0 = r30
            dalmax.Coords2D r0 = r0.m_ScreenBoardCoords     // Catch:{ all -> 0x018e }
            r27 = r0
            r0 = r27
            int r0 = r0.y     // Catch:{ all -> 0x018e }
            r27 = r0
            int r27 = r27 + r8
            r0 = r27
            float r0 = (float) r0     // Catch:{ all -> 0x018e }
            r27 = r0
            r28 = 0
            r0 = r6
            r1 = r22
            r2 = r26
            r3 = r27
            r4 = r28
            r0.drawBitmap(r1, r2, r3, r4)     // Catch:{ all -> 0x018e }
            r0 = r30
            android.graphics.Bitmap r0 = r0.m_BoardBitmap     // Catch:{ all -> 0x018e }
            r26 = r0
            r0 = r30
            dalmax.Coords2D r0 = r0.m_ScreenBoardCoords     // Catch:{ all -> 0x018e }
            r27 = r0
            r0 = r27
            int r0 = r0.x     // Catch:{ all -> 0x018e }
            r27 = r0
            r0 = r27
            float r0 = (float) r0     // Catch:{ all -> 0x018e }
            r27 = r0
            r0 = r30
            dalmax.Coords2D r0 = r0.m_ScreenBoardCoords     // Catch:{ all -> 0x018e }
            r28 = r0
            r0 = r28
            int r0 = r0.y     // Catch:{ all -> 0x018e }
            r28 = r0
            r0 = r28
            float r0 = (float) r0     // Catch:{ all -> 0x018e }
            r28 = r0
            android.graphics.Paint r29 = new android.graphics.Paint     // Catch:{ all -> 0x018e }
            r29.<init>()     // Catch:{ all -> 0x018e }
            r0 = r6
            r1 = r26
            r2 = r27
            r3 = r28
            r4 = r29
            r0.drawBitmap(r1, r2, r3, r4)     // Catch:{ all -> 0x018e }
        L_0x017a:
            if (r6 == 0) goto L_0x0188
            r0 = r30
            android.view.SurfaceHolder r0 = r0.m_oSurfaceHolder     // Catch:{ Exception -> 0x019e }
            r26 = r0
            r0 = r26
            r1 = r6
            r0.unlockCanvasAndPost(r1)     // Catch:{ Exception -> 0x019e }
        L_0x0188:
            long r10 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x019e }
            goto L_0x0089
        L_0x018e:
            r26 = move-exception
            if (r6 == 0) goto L_0x019d
            r0 = r30
            android.view.SurfaceHolder r0 = r0.m_oSurfaceHolder     // Catch:{ Exception -> 0x019e }
            r27 = r0
            r0 = r27
            r1 = r6
            r0.unlockCanvasAndPost(r1)     // Catch:{ Exception -> 0x019e }
        L_0x019d:
            throw r26     // Catch:{ Exception -> 0x019e }
        L_0x019e:
            r26 = move-exception
            goto L_0x000e
        L_0x01a1:
            r26 = 1
            java.lang.Thread.sleep(r26)     // Catch:{ InterruptedException -> 0x01a7 }
            goto L_0x0188
        L_0x01a7:
            r26 = move-exception
            goto L_0x0188
        */
        throw new UnsupportedOperationException("Method not decompiled: dalmax.games.turnBasedGames.connect4.Connect4View.DrawMove(dalmax.games.turnBasedGames.GameStatusDescription):void");
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int nWidth, int nHeight) {
        if (nWidth / this.m_nNCols < nHeight / (this.m_nNRows + 1)) {
            this.m_nBoardWidthSize = nWidth;
            this.m_nBoardHeightSize = ((this.m_nNRows + 1) * nWidth) / this.m_nNCols;
        } else {
            this.m_nBoardHeightSize = nHeight;
            this.m_nBoardWidthSize = (this.m_nNCols * nHeight) / (this.m_nNRows + 1);
        }
        this.m_ScreenBoardCoords.Set(0, (this.m_nBoardWidthSize * (this.m_nNCols - this.m_nNRows)) / this.m_nNCols);
        this.m_bToDraw = true;
        DrawPosition();
    }

    public boolean onTouch(View v, MotionEvent event) {
        if ((IsFirstHumanTurn() || this.m_eGameMode == TwoPlayerView.EGameMode.twoPlayers) && !this.m_bEndedGame) {
            Coords2D oBoardCoords = GetBoardCoords(new Coords2D((int) event.getX(), (int) event.getY()));
            this.m_nSelectedCol = -1;
            if (oBoardCoords != null) {
                if ((event.getAction() == 0 || event.getAction() == 2) && this.m_nSelectedCol != oBoardCoords.x) {
                    this.m_oEngine.ComputeMoveList(this.m_lMoveList);
                    Iterator it = this.m_lMoveList.iterator();
                    while (true) {
                        if (it.hasNext()) {
                            if (((Connect4ViewMove) ((ViewMove) it.next())).m_c == oBoardCoords.x) {
                                this.m_nSelectedCol = oBoardCoords.x;
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                } else if (event.getAction() == 1) {
                    Iterator it2 = this.m_lMoveList.iterator();
                    while (it2.hasNext()) {
                        ViewMove oMove = (ViewMove) it2.next();
                        if (((Connect4ViewMove) oMove).m_c == oBoardCoords.x) {
                            ((Connect4ViewMove) this.m_oMoveCurrent).m_r = ((Connect4ViewMove) oMove).m_r;
                            ((Connect4ViewMove) this.m_oMoveCurrent).m_c = ((Connect4ViewMove) oMove).m_c;
                            try {
                                ApplyMove();
                            } catch (CloneNotSupportedException e) {
                            }
                            return true;
                        }
                    }
                }
            }
            DrawPosition();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public Coords2D GetBoardCoords(Coords2D oScreenCoords) {
        Coords2D oBoardCoords = new Coords2D(((oScreenCoords.x - this.m_ScreenBoardCoords.x) * this.m_nNCols) / this.m_nBoardWidthSize, ((oScreenCoords.y - this.m_ScreenBoardCoords.y) * this.m_nNRows) / this.m_nBoardHeightSize);
        if (oBoardCoords.x < 0 || oBoardCoords.x >= this.m_nNCols || oBoardCoords.y < 0 || oBoardCoords.y >= this.m_nNRows) {
            return null;
        }
        return oBoardCoords;
    }

    /* access modifiers changed from: protected */
    public Coords2D[] WinSequence() {
        Connect4BoardDescription oBoardDescr = (Connect4BoardDescription) this.m_oBoardDescription;
        int r = 0;
        while (r < this.m_nNRows) {
            int c = 0;
            while (c < this.m_nNCols) {
                byte curr = oBoardDescr.GetBoard()[r][c];
                if (curr != 0) {
                    int minC = Util.Max(c - 3, 0);
                    int maxC = Util.Min(c + 3, this.m_nNCols - 1);
                    this.m_WinnerCoords = new Coords2D[2];
                    this.m_WinnerCoords[0] = new Coords2D();
                    this.m_WinnerCoords[1] = new Coords2D();
                    Coords2D coords2D = this.m_WinnerCoords[0];
                    this.m_WinnerCoords[1].y = r;
                    coords2D.y = r;
                    Coords2D coords2D2 = this.m_WinnerCoords[0];
                    this.m_WinnerCoords[1].x = c;
                    coords2D2.x = c;
                    int cnt = 1;
                    int ic = c - 1;
                    while (ic >= minC && oBoardDescr.GetBoard()[r][ic] == curr) {
                        cnt++;
                        this.m_WinnerCoords[0].x = ic;
                        ic--;
                    }
                    int ic2 = c + 1;
                    while (ic2 <= maxC && oBoardDescr.GetBoard()[r][ic2] == curr) {
                        cnt++;
                        this.m_WinnerCoords[1].x = ic2;
                        ic2++;
                    }
                    if (cnt >= 4) {
                        return this.m_WinnerCoords;
                    }
                    int minR = Util.Max(r - 4, 0);
                    int maxR = Util.Min(r + 4, this.m_nNRows - 1);
                    int cnt2 = 1;
                    Coords2D coords2D3 = this.m_WinnerCoords[0];
                    this.m_WinnerCoords[1].y = r;
                    coords2D3.y = r;
                    Coords2D coords2D4 = this.m_WinnerCoords[0];
                    this.m_WinnerCoords[1].x = c;
                    coords2D4.x = c;
                    int ir = r - 1;
                    while (ir >= minR && oBoardDescr.GetBoard()[ir][c] == curr) {
                        cnt2++;
                        this.m_WinnerCoords[0].y = ir;
                        ir--;
                    }
                    int ir2 = r + 1;
                    while (ir2 <= maxR && oBoardDescr.GetBoard()[ir2][c] == curr) {
                        cnt2++;
                        this.m_WinnerCoords[1].y = ir2;
                        ir2++;
                    }
                    if (cnt2 >= 4) {
                        return this.m_WinnerCoords;
                    }
                    Coords2D coords2D5 = this.m_WinnerCoords[0];
                    this.m_WinnerCoords[1].y = r;
                    coords2D5.y = r;
                    Coords2D coords2D6 = this.m_WinnerCoords[0];
                    this.m_WinnerCoords[1].x = c;
                    coords2D6.x = c;
                    int cnt3 = 1;
                    int ir3 = r - 1;
                    int ic3 = c - 1;
                    while (ir3 >= minR && ic3 >= minC && oBoardDescr.GetBoard()[ir3][ic3] == curr) {
                        cnt3++;
                        this.m_WinnerCoords[0].y = ir3;
                        this.m_WinnerCoords[0].x = ic3;
                        ir3--;
                        ic3--;
                    }
                    int ir4 = r + 1;
                    int ic4 = c + 1;
                    while (ir4 <= maxR && ic4 <= maxC && oBoardDescr.GetBoard()[ir4][ic4] == curr) {
                        cnt3++;
                        this.m_WinnerCoords[1].y = ir4;
                        this.m_WinnerCoords[1].x = ic4;
                        ir4++;
                        ic4++;
                    }
                    if (cnt3 >= 4) {
                        return this.m_WinnerCoords;
                    }
                    Coords2D coords2D7 = this.m_WinnerCoords[0];
                    this.m_WinnerCoords[1].y = r;
                    coords2D7.y = r;
                    Coords2D coords2D8 = this.m_WinnerCoords[0];
                    this.m_WinnerCoords[1].x = c;
                    coords2D8.x = c;
                    int cnt4 = 1;
                    int ir5 = r - 1;
                    int ic5 = c + 1;
                    while (ir5 >= minR && ic5 <= maxC && oBoardDescr.GetBoard()[ir5][ic5] == curr) {
                        cnt4++;
                        this.m_WinnerCoords[0].y = ir5;
                        this.m_WinnerCoords[0].x = ic5;
                        ir5--;
                        ic5++;
                    }
                    int ir6 = r + 1;
                    int ic6 = c - 1;
                    while (ir6 <= maxR && ic6 >= minC && oBoardDescr.GetBoard()[ir6][ic6] == curr) {
                        cnt4++;
                        this.m_WinnerCoords[1].y = ir6;
                        this.m_WinnerCoords[1].x = ic6;
                        ir6++;
                        ic6--;
                    }
                    if (cnt4 >= 4) {
                        return this.m_WinnerCoords;
                    }
                }
                c++;
            }
            r++;
        }
        this.m_WinnerCoords = null;
        return null;
    }

    /* access modifiers changed from: protected */
    public void PassMove() {
    }
}
