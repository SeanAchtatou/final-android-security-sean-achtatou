package dalmax.games.turnBasedGames.connect4;

import dalmax.games.turnBasedGames.EnginePosition;

public class Connect4EnginePosition extends EnginePosition {
    public long[] m_nMasks = new long[2];
    public byte m_nPlayerToMove;

    public Connect4EnginePosition(byte nNRows, byte nNCols, long nMask0, long nMask1, byte nPlayerToMove) {
        this.m_nPlayerToMove = nPlayerToMove;
        this.m_nMasks[0] = nMask0;
        this.m_nMasks[1] = nMask1;
    }

    public Object clone() throws CloneNotSupportedException {
        Connect4EnginePosition oClone = (Connect4EnginePosition) super.clone();
        oClone.m_nPlayerToMove = this.m_nPlayerToMove;
        oClone.m_nMasks[0] = this.m_nMasks[0];
        oClone.m_nMasks[1] = this.m_nMasks[1];
        return oClone;
    }

    public int hashCode() {
        return (int) (((this.m_nMasks[0] ^ this.m_nMasks[1]) ^ (this.m_nMasks[0] >> 32)) ^ (this.m_nMasks[1] >> 32));
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (getClass() != o.getClass()) {
            return false;
        }
        Connect4EnginePosition oPosition = (Connect4EnginePosition) o;
        if (oPosition.m_nPlayerToMove == this.m_nPlayerToMove && oPosition.m_nMasks[0] == this.m_nMasks[0] && oPosition.m_nMasks[1] == this.m_nMasks[1]) {
            return true;
        }
        return false;
    }
}
