package dalmax.games.turnBasedGames;

import dalmax.games.turnBasedGames.Engine;
import java.util.ArrayList;

public interface IEngine extends Runnable {
    void ChangePlayerToMove();

    Engine.EWinPairLose CheckWinPairLose(boolean z);

    int ComputeMoveList(ArrayList<ViewMove> arrayList);

    void Exits();

    ViewMove GetBestMoveIfPresent();

    int GetLevel();

    ViewMove GetMove();

    void SetBoardAfterMoveDone(GameStatusDescription gameStatusDescription);

    void SetBoardAfterMoveUndone(GameStatusDescription gameStatusDescription);

    void SetComputing(boolean z, boolean z2);

    void SetInitialBoard(GameStatusDescription gameStatusDescription);

    void SetPauseDT(boolean z);
}
