package dalmax.games.turnBasedGames;

public abstract class EngineMove extends BoardGameMove {
    private boolean m_bStationaryMove = true;

    public abstract void Clear();

    public abstract ViewMove GetViewMove(Engine engine);

    public final void SetStationaryMove(boolean bStationary) {
        this.m_bStationaryMove = bStationary;
    }

    public final boolean IsStationaryMove() {
        return this.m_bStationaryMove;
    }
}
