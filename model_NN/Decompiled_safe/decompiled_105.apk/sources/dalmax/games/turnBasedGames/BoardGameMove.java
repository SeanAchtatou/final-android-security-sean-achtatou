package dalmax.games.turnBasedGames;

public abstract class BoardGameMove implements Cloneable {
    public abstract void Clear();

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
