package dalmax.games.turnBasedGames;

import android.content.Context;
import dalmax.Util;
import dalmax.games.turnBasedGames.BoardGameHashTable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

public abstract class Engine implements IEngine, Runnable, Cloneable {
    static boolean m_bCompute;
    boolean m_bBestMoveFinded;
    boolean m_bComputing;
    boolean m_bContinue;
    private boolean m_bDeepThinkingMode;
    boolean m_bNoMoveFinded;
    protected boolean m_bPauseOnDT;
    ArrayList<EnginePosition> m_lPositionsHistory;
    int m_nDeep;
    int m_nHashTableSize;
    protected int m_nLevel;
    protected int m_nNPlayers;
    protected int m_nPlayerToMove;
    EngineMove m_oBestEngineMove;
    protected Context m_oContext;
    BoardGameHashTable m_oHashTable;
    final ReentrantLock m_oLockComputing;
    final ReentrantLock m_oLockToAccessBestMove;
    final ReentrantLock m_oLockToGetMoveFromOutSide;
    Random m_oRandom;

    public enum EWinPairLose {
        None,
        Win,
        Pair,
        Lose
    }

    public abstract EWinPairLose CheckWinPairLose(boolean z);

    /* access modifiers changed from: protected */
    public abstract int ComputeEngineMoveList(ArrayList<EngineMove> arrayList);

    /* access modifiers changed from: protected */
    public abstract void CopyInfoFromBoardGameDescription(GameStatusDescription gameStatusDescription);

    /* access modifiers changed from: protected */
    public abstract Engine DoMove(EngineMove engineMove, Engine engine) throws CloneNotSupportedException;

    /* access modifiers changed from: protected */
    public abstract EngineMove GetNextMove();

    public abstract int GetNextPlayerToMove(int i);

    /* access modifiers changed from: protected */
    public abstract EnginePosition GetPosition();

    /* access modifiers changed from: protected */
    public abstract int GetStaticBoardScore();

    /* access modifiers changed from: protected */
    public abstract int InitMoves(boolean z);

    public Engine(int nLevel, int nHashTableSize) {
        this.m_bContinue = true;
        this.m_oBestEngineMove = null;
        this.m_oLockToAccessBestMove = new ReentrantLock();
        this.m_oLockToGetMoveFromOutSide = new ReentrantLock();
        this.m_oLockComputing = new ReentrantLock();
        this.m_bBestMoveFinded = false;
        this.m_nLevel = 100;
        this.m_bNoMoveFinded = false;
        this.m_oHashTable = null;
        this.m_nHashTableSize = 5000;
        this.m_lPositionsHistory = null;
        this.m_oContext = null;
        this.m_bDeepThinkingMode = false;
        this.m_bPauseOnDT = false;
        this.m_bContinue = true;
        this.m_nPlayerToMove = 1;
        this.m_nNPlayers = 2;
        this.m_nDeep = 0;
        this.m_oBestEngineMove = null;
        m_bCompute = false;
        this.m_bComputing = false;
        this.m_nHashTableSize = nHashTableSize;
        if (nLevel > 100) {
            this.m_nLevel = 100;
        } else if (nLevel < 1) {
            this.m_nLevel = 1;
        } else {
            this.m_nLevel = nLevel;
        }
    }

    public void RunEngine() {
        run();
    }

    public int GetLevel() {
        return this.m_nLevel;
    }

    public ViewMove GetBestMoveIfPresent() {
        if (this.m_bBestMoveFinded) {
            return GetMove();
        }
        return null;
    }

    public ViewMove GetMove() {
        ViewMove oCurrBestMove = null;
        this.m_oLockToGetMoveFromOutSide.lock();
        try {
            boolean bCompute = m_bCompute;
            boolean bDTMode = this.m_bDeepThinkingMode;
            while (this.m_oBestEngineMove == null && !this.m_bNoMoveFinded) {
                m_bCompute = true;
                this.m_bDeepThinkingMode = false;
                try {
                    Thread.sleep(50);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            m_bCompute = bCompute;
            this.m_bDeepThinkingMode = bDTMode;
            if (!this.m_bNoMoveFinded) {
                this.m_oLockToAccessBestMove.lock();
                try {
                    oCurrBestMove = this.m_oBestEngineMove.GetViewMove(this);
                    this.m_oLockToAccessBestMove.unlock();
                } catch (Exception e2) {
                    e2.printStackTrace();
                    this.m_oLockToAccessBestMove.unlock();
                } catch (Throwable th) {
                    this.m_oLockToAccessBestMove.unlock();
                    throw th;
                }
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        } finally {
            this.m_oLockToGetMoveFromOutSide.unlock();
        }
        return oCurrBestMove;
    }

    /* access modifiers changed from: protected */
    public Object clone() throws CloneNotSupportedException {
        Engine oClone = (Engine) super.clone();
        oClone.m_nPlayerToMove = this.m_nPlayerToMove;
        return oClone;
    }

    public final void SetBoardAfterMoveDone(GameStatusDescription oBoardInfo) {
        boolean bComputeLastState = m_bCompute;
        m_bCompute = false;
        this.m_oLockComputing.lock();
        while (this.m_bComputing) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
            }
        }
        try {
            if (this.m_lPositionsHistory != null) {
                this.m_lPositionsHistory.add((EnginePosition) GetPosition().clone());
            }
        } catch (CloneNotSupportedException e2) {
            e2.printStackTrace();
        } catch (Throwable th) {
            this.m_oLockComputing.unlock();
            throw th;
        }
        CopyInfoFromBoardGameDescription(oBoardInfo);
        this.m_oLockToGetMoveFromOutSide.lock();
        this.m_oBestEngineMove = null;
        this.m_oLockToGetMoveFromOutSide.unlock();
        this.m_nDeep = 0;
        this.m_bNoMoveFinded = false;
        m_bCompute = bComputeLastState;
        this.m_oLockComputing.unlock();
    }

    public final void SetBoardAfterMoveUndone(GameStatusDescription oBoardInfo) {
        boolean bComputeLastState = m_bCompute;
        m_bCompute = false;
        this.m_oLockComputing.lock();
        while (this.m_bComputing) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
            }
        }
        try {
            if (this.m_lPositionsHistory != null && this.m_lPositionsHistory.size() > 0) {
                this.m_lPositionsHistory.remove(this.m_lPositionsHistory.size() - 1);
            }
            CopyInfoFromBoardGameDescription(oBoardInfo);
            this.m_oLockToGetMoveFromOutSide.lock();
            this.m_oBestEngineMove = null;
            this.m_oLockToGetMoveFromOutSide.unlock();
            this.m_nDeep = 0;
            this.m_bNoMoveFinded = false;
            m_bCompute = bComputeLastState;
        } finally {
            this.m_oLockComputing.unlock();
        }
    }

    public void SetInitialBoard(GameStatusDescription oBoardInfo) {
        boolean bComputeLastState = m_bCompute;
        m_bCompute = false;
        this.m_oLockComputing.lock();
        while (this.m_bComputing) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
            }
        }
        try {
            if (this.m_lPositionsHistory != null) {
                this.m_lPositionsHistory.clear();
            }
            CopyInfoFromBoardGameDescription(oBoardInfo);
            this.m_oLockToGetMoveFromOutSide.lock();
            this.m_oBestEngineMove = null;
            this.m_oLockToGetMoveFromOutSide.unlock();
            this.m_nDeep = 0;
            this.m_bNoMoveFinded = false;
            m_bCompute = bComputeLastState;
        } finally {
            this.m_oLockComputing.unlock();
        }
    }

    public void SetComputing(boolean bCompute, boolean bDeepThinkingMode) {
        m_bCompute = bCompute;
        this.m_bBestMoveFinded = false;
        this.m_bDeepThinkingMode = bDeepThinkingMode;
    }

    public void Exits() {
        this.m_bContinue = false;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r3 = this;
            r1 = 0
            r3.m_bNoMoveFinded = r1
            r1 = 0
            r3.m_oBestEngineMove = r1
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r3.m_lPositionsHistory = r1
            r1 = 0
            r3.m_nDeep = r1     // Catch:{ CloneNotSupportedException -> 0x003c }
        L_0x0010:
            boolean r1 = r3.m_bContinue     // Catch:{ CloneNotSupportedException -> 0x003c }
            if (r1 != 0) goto L_0x0015
        L_0x0014:
            return
        L_0x0015:
            r1 = 0
            r3.m_bBestMoveFinded = r1     // Catch:{ CloneNotSupportedException -> 0x003c }
            java.util.concurrent.locks.ReentrantLock r1 = r3.m_oLockComputing     // Catch:{ CloneNotSupportedException -> 0x003c }
            r1.lock()     // Catch:{ CloneNotSupportedException -> 0x003c }
            boolean r1 = dalmax.games.turnBasedGames.Engine.m_bCompute     // Catch:{ all -> 0x0042 }
            if (r1 == 0) goto L_0x002c
            r1 = 1
            r3.m_bComputing = r1     // Catch:{ all -> 0x0042 }
            int r1 = r3.m_nDeep     // Catch:{ all -> 0x0042 }
            r3.ComputeNegaScoutBestMove(r1)     // Catch:{ all -> 0x0042 }
            r1 = 0
            r3.m_bComputing = r1     // Catch:{ all -> 0x0042 }
        L_0x002c:
            java.util.concurrent.locks.ReentrantLock r1 = r3.m_oLockComputing     // Catch:{ CloneNotSupportedException -> 0x003c }
            r1.unlock()     // Catch:{ CloneNotSupportedException -> 0x003c }
        L_0x0031:
            boolean r1 = r3.m_bContinue     // Catch:{ InterruptedException -> 0x0055 }
            if (r1 != 0) goto L_0x0049
        L_0x0035:
            int r1 = r3.m_nDeep     // Catch:{ CloneNotSupportedException -> 0x003c }
            int r1 = r1 + 1
            r3.m_nDeep = r1     // Catch:{ CloneNotSupportedException -> 0x003c }
            goto L_0x0010
        L_0x003c:
            r1 = move-exception
            r0 = r1
            r0.printStackTrace()
            goto L_0x0014
        L_0x0042:
            r1 = move-exception
            java.util.concurrent.locks.ReentrantLock r2 = r3.m_oLockComputing     // Catch:{ CloneNotSupportedException -> 0x003c }
            r2.unlock()     // Catch:{ CloneNotSupportedException -> 0x003c }
            throw r1     // Catch:{ CloneNotSupportedException -> 0x003c }
        L_0x0049:
            r1 = 10
            java.lang.Thread.sleep(r1)     // Catch:{ InterruptedException -> 0x0055 }
            boolean r1 = r3.ContinueComputingCriteria()     // Catch:{ InterruptedException -> 0x0055 }
            if (r1 == 0) goto L_0x0031
            goto L_0x0035
        L_0x0055:
            r1 = move-exception
            r0 = r1
            r0.printStackTrace()     // Catch:{ CloneNotSupportedException -> 0x003c }
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: dalmax.games.turnBasedGames.Engine.run():void");
    }

    private boolean ContinueComputingCriteria() {
        if (this.m_bNoMoveFinded) {
            return false;
        }
        if (!m_bCompute) {
            return false;
        }
        if (!this.m_bPauseOnDT || !this.m_bDeepThinkingMode) {
            return this.m_nDeep == 0 || this.m_nLevel > ((this.m_nDeep + 1) * 100) / 20;
        }
        return false;
    }

    public final void ChangePlayerToMove() {
        this.m_nPlayerToMove = GetNextPlayerToMove(this.m_nPlayerToMove);
    }

    public void CopyFrom(Engine oCopyFrom) {
        this.m_nNPlayers = oCopyFrom.m_nNPlayers;
        this.m_oBestEngineMove = null;
        this.m_nPlayerToMove = oCopyFrom.m_nPlayerToMove;
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: protected */
    public void ComputeNegaScoutBestMove(int nDeep) throws CloneNotSupportedException {
        int nScore;
        int nAlfa = -2147483637;
        if (nDeep <= 0) {
            nDeep = 1;
        }
        EngineMove oMove = null;
        EngineMove oHistoryMove = null;
        if (this.m_oHashTable == null) {
            this.m_oHashTable = new BoardGameHashTable(this.m_nHashTableSize);
        }
        BoardGameHashTable.BoardHistoryElement oHistoryElement = this.m_oHashTable.GetMove(GetPosition());
        boolean bUniqHistoryMove = false;
        if (oHistoryElement != null) {
            if (oHistoryElement.m_nDeep < nDeep || oHistoryElement.m_nAlfa > -2147483637 || oHistoryElement.m_nBeta < 2147483637) {
                oHistoryMove = oHistoryElement.m_oEngineMove;
                oMove = oHistoryMove;
                if (oHistoryElement.m_bUniqMove) {
                    bUniqHistoryMove = true;
                } else if (this.m_oBestEngineMove != null) {
                    oHistoryMove = this.m_oBestEngineMove;
                    oMove = oHistoryMove;
                }
            } else {
                this.m_oLockToAccessBestMove.lock();
                try {
                    this.m_oBestEngineMove = oHistoryElement.m_oEngineMove;
                    return;
                } finally {
                    this.m_oLockToAccessBestMove.unlock();
                }
            }
        }
        int nMoves = 1;
        if (!bUniqHistoryMove) {
            nMoves = InitMoves(true);
        }
        if (oHistoryElement == null) {
            if (this.m_oBestEngineMove != null) {
                oHistoryMove = this.m_oBestEngineMove;
                oMove = oHistoryMove;
            } else {
                oMove = GetNextMove();
            }
        }
        this.m_oLockToAccessBestMove.lock();
        try {
            this.m_oBestEngineMove = oMove;
            if (oMove == null) {
                this.m_bNoMoveFinded = true;
                return;
            }
            if (nMoves == 1) {
                this.m_bBestMoveFinded = true;
            }
            int nB = 2147483637;
            int nBestScore = -2147483647;
            int nMoveIdx = 0;
            do {
                if (nMoveIdx <= 0 || !oMove.equals(oHistoryMove)) {
                    Engine boardMoved = DoMove(oMove, null);
                    int nDeepToCompute = nDeep - 1;
                    if (!oMove.IsStationaryMove()) {
                        nDeepToCompute++;
                    }
                    if (this.m_bDeepThinkingMode) {
                        nScore = -boardMoved.GetNegaScoutBoardScore(nDeepToCompute, -2147483637, --2147483637, this.m_oHashTable);
                    } else {
                        nScore = -boardMoved.GetNegaScoutBoardScore(nDeepToCompute, -nB, -nAlfa, this.m_oHashTable);
                        if (m_bCompute && nAlfa < nScore && nScore < 2147483637 && nMoveIdx > 0) {
                            nScore = -boardMoved.GetNegaScoutBoardScore(nDeep - 1, -2147483637, -nAlfa, this.m_oHashTable);
                        }
                    }
                    boolean bMoveCorrectly = true;
                    if (this.m_nLevel < 100) {
                        bMoveCorrectly = this.m_nLevel + ((100 - this.m_nLevel) >> 1) > ((byte) ((int) Util.RandomLong(0, 99)));
                    }
                    if (m_bCompute) {
                        if (bMoveCorrectly == (nBestScore < nScore)) {
                            int nDiv = 0;
                            Iterator<EnginePosition> it = this.m_lPositionsHistory.iterator();
                            while (it.hasNext()) {
                                if (it.next().equals(boardMoved.GetPosition())) {
                                    nDiv++;
                                }
                            }
                            int nScore2 = nScore >> nDiv;
                            if (bMoveCorrectly == (nBestScore < nScore2)) {
                                nBestScore = nScore2;
                                nAlfa = nBestScore;
                                this.m_oLockToAccessBestMove.lock();
                                try {
                                    this.m_oBestEngineMove = oMove;
                                    this.m_oLockToAccessBestMove.unlock();
                                    if (nBestScore > 2147482647) {
                                        this.m_bBestMoveFinded = true;
                                    }
                                } catch (Throwable th) {
                                    this.m_oLockToAccessBestMove.unlock();
                                    throw th;
                                }
                            }
                        }
                    }
                    nB = nAlfa + 1;
                    nMoveIdx++;
                }
                if (m_bCompute && nAlfa < 2147483637 && !bUniqHistoryMove) {
                    oMove = GetNextMove();
                }
            } while (oMove != null);
            if (m_bCompute && this.m_oBestEngineMove != null && this.m_oHashTable != null) {
                this.m_oHashTable.Put(GetPosition(), this.m_oBestEngineMove, nDeep, -2147483637, 2147483637, nAlfa, nMoveIdx <= 1 && GetNextMove() == null);
            }
        } finally {
            this.m_oLockToAccessBestMove.unlock();
        }
    }

    /* access modifiers changed from: protected */
    public int GetNegaScoutBoardScore(int nDeep, int nAlfa, int nBeta, BoardGameHashTable oHistory) throws CloneNotSupportedException {
        EngineMove oMove;
        Engine boardMoved;
        if (nDeep <= 0) {
            return GetStaticBoardScore();
        }
        if (!m_bCompute) {
            return -2147483647;
        }
        EngineMove oHistoryMove = null;
        EngineMove oBestMove = null;
        BoardGameHashTable.BoardHistoryElement oHistoryElement = oHistory.GetMove(GetPosition());
        boolean bUniqHistoryMove = false;
        if (oHistoryElement == null) {
            InitMoves(false);
            oMove = GetNextMove();
        } else if (oHistoryElement.m_nDeep >= nDeep && oHistoryElement.m_nAlfa <= nAlfa && oHistoryElement.m_nBeta >= nBeta) {
            return oHistoryElement.m_nScore;
        } else {
            oHistoryMove = oHistoryElement.m_oEngineMove;
            oBestMove = oHistoryMove;
            oMove = oHistoryMove;
            if (oHistoryElement.m_bUniqMove) {
                bUniqHistoryMove = true;
            } else {
                InitMoves(false);
            }
        }
        EWinPairLose eWinPairLose = CheckWinPairLose(oMove != null);
        if (eWinPairLose == EWinPairLose.Lose) {
            return -2147483646;
        }
        if (eWinPairLose == EWinPairLose.Win) {
            return 2147483646;
        }
        if (eWinPairLose == EWinPairLose.Pair) {
            return 0;
        }
        int nB = nBeta;
        int nOrigAlfa = nAlfa;
        int nOrigBeta = nBeta;
        int nMoveIdx = 0;
        do {
            if (nMoveIdx <= 0 || oMove == null || !oMove.equals(oHistoryMove)) {
                int nDeepToCompute = nDeep - 1;
                if (oMove != null && !oMove.IsStationaryMove()) {
                    nDeepToCompute++;
                }
                if (oMove != null) {
                    boardMoved = DoMove(oMove, null);
                } else {
                    boardMoved = (Engine) clone();
                    boardMoved.m_nPlayerToMove = -this.m_nPlayerToMove;
                }
                int nScore = -boardMoved.GetNegaScoutBoardScore(nDeepToCompute, -nB, -nAlfa, oHistory);
                if (nAlfa < nScore && nScore < nBeta && nMoveIdx > 0) {
                    nScore = -boardMoved.GetNegaScoutBoardScore(nDeepToCompute, -nBeta, -nAlfa, oHistory);
                }
                if (nAlfa < nScore) {
                    nAlfa = nScore;
                    oBestMove = oMove;
                }
                nB = nAlfa + 1;
                nMoveIdx++;
            }
            if (m_bCompute && nAlfa < nBeta && !bUniqHistoryMove) {
                oMove = GetNextMove();
            }
        } while (oMove != null);
        if (m_bCompute) {
            if (nAlfa > 2147482647) {
                nAlfa--;
            } else if (nAlfa < -2147482647) {
                nAlfa++;
            }
            if (!(oBestMove == null || oHistory == null)) {
                oHistory.Put(GetPosition(), oBestMove, nDeep, nOrigAlfa, nOrigBeta, nAlfa, nMoveIdx <= 1 && GetNextMove() == null);
            }
        }
        return nAlfa;
    }

    public int ComputeMoveList(ArrayList<ViewMove> lMoveList) {
        ArrayList<EngineMove> lEngineMoveList = new ArrayList<>();
        int nMoves = ComputeEngineMoveList(lEngineMoveList);
        lMoveList.clear();
        Iterator it = lEngineMoveList.iterator();
        while (it.hasNext()) {
            lMoveList.add(((EngineMove) it.next()).GetViewMove(this));
        }
        return nMoves;
    }

    public void SetPauseDT(boolean bPauseOnDT) {
        this.m_bPauseOnDT = bPauseOnDT;
    }
}
