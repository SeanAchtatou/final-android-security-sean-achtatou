package dalmax.games.turnBasedGames;

public class BoardGameHashTable {
    private static boolean m_bEnabled = true;
    int m_nSize = 0;
    BoardHistoryElement[] m_vPositions = null;

    protected class BoardHistoryElement {
        public boolean m_bUniqMove;
        public int m_nAlfa;
        public int m_nBeta;
        public int m_nDeep = -1;
        public int m_nScore;
        public EngineMove m_oEngineMove = null;
        public EnginePosition m_oPosition = null;

        protected BoardHistoryElement() {
        }
    }

    public BoardGameHashTable(int nSize) {
        this.m_nSize = nSize;
        if (!m_bEnabled) {
            this.m_nSize = 0;
        }
        this.m_vPositions = new BoardHistoryElement[this.m_nSize];
    }

    public void Put(EnginePosition oPosition, EngineMove oMove, int nDeep, int nAlfa, int nBeta, int nScore, boolean bUniqMove) {
        int nIdx = GetIdx(oPosition);
        if (nIdx >= 0) {
            if (this.m_vPositions[nIdx] == null || nDeep >= this.m_vPositions[nIdx].m_nDeep || nAlfa < this.m_vPositions[nIdx].m_nAlfa || nBeta > this.m_vPositions[nIdx].m_nBeta || !this.m_vPositions[nIdx].m_oPosition.equals(oPosition)) {
                if (this.m_vPositions[nIdx] == null) {
                    this.m_vPositions[nIdx] = new BoardHistoryElement();
                }
                this.m_vPositions[nIdx].m_nDeep = nDeep;
                this.m_vPositions[nIdx].m_nAlfa = nAlfa;
                this.m_vPositions[nIdx].m_nBeta = nBeta;
                this.m_vPositions[nIdx].m_nScore = nScore;
                this.m_vPositions[nIdx].m_bUniqMove = bUniqMove;
                try {
                    this.m_vPositions[nIdx].m_oPosition = (EnginePosition) oPosition.clone();
                    this.m_vPositions[nIdx].m_oEngineMove = (EngineMove) oMove.clone();
                } catch (CloneNotSupportedException e) {
                    this.m_vPositions[nIdx] = null;
                }
            }
        }
    }

    public BoardHistoryElement GetMove(EnginePosition oPosition) {
        int nIdx = GetIdx(oPosition);
        if (nIdx < 0 || this.m_vPositions[nIdx] == null || this.m_vPositions[nIdx].m_nDeep < 0) {
            return null;
        }
        if (oPosition.equals(this.m_vPositions[nIdx].m_oPosition)) {
            return this.m_vPositions[nIdx];
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public int GetIdx(EnginePosition oPosition) {
        if (oPosition == null || this.m_nSize <= 0) {
            return -1;
        }
        int nIdx = (int) (((long) oPosition.hashCode()) % ((long) this.m_nSize));
        if (nIdx < 0) {
            nIdx = -nIdx;
        }
        return nIdx;
    }
}
