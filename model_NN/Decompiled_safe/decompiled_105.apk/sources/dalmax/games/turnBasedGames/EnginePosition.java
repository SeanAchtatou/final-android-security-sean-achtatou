package dalmax.games.turnBasedGames;

public abstract class EnginePosition implements Cloneable {
    public Object clone() throws CloneNotSupportedException {
        return (EnginePosition) super.clone();
    }

    public int hashCode() {
        return 0;
    }
}
