package dalmax.games.turnBasedGames;

public abstract class ViewMove extends BoardGameMove {
    public abstract boolean IsValid();

    public abstract boolean SetFromString(String str);

    public void Clear() {
    }
}
