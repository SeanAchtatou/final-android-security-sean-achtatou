package dalmax.games.turnBasedGames;

public class GameStatusDescription implements Cloneable {
    protected byte m_nPlayerToMove = -1;

    public int GetPlayer() {
        return this.m_nPlayerToMove;
    }

    public void SetPlayer(byte nPlayerToMove) {
        this.m_nPlayerToMove = nPlayerToMove;
    }

    public int GetPlayerToMove() {
        return this.m_nPlayerToMove;
    }

    public void SetPlayerToMove(byte nPlayerToMove) {
        this.m_nPlayerToMove = nPlayerToMove;
    }

    public Object clone() throws CloneNotSupportedException {
        GameStatusDescription o = (GameStatusDescription) super.clone();
        o.m_nPlayerToMove = this.m_nPlayerToMove;
        return o;
    }
}
