package dalmax.games.turnBasedGames;

public class BoardGameHistory {
    private static boolean m_bEnabled = false;
    int m_nSize = 0;
    BoardHistoryElement[] m_vPositions = null;

    protected class BoardHistoryElement {
        public int m_nAlfa;
        public int m_nBeta;
        public int m_nDeep = -1;
        public int m_nScore;
        public Engine m_oEngine = null;
        public EngineMove m_oEngineMove = null;

        protected BoardHistoryElement() {
        }
    }

    public BoardGameHistory(int nSize) {
        this.m_vPositions = new BoardHistoryElement[nSize];
        this.m_nSize = nSize;
    }

    public void Put(Engine oBoardGame, EngineMove oMove, int nDeep, int nAlfa, int nBeta, int nScore) {
        int nIdx;
        if (!m_bEnabled || (nIdx = GetIdx(oBoardGame)) < 0) {
            return;
        }
        if (this.m_vPositions[nIdx] == null || nDeep >= this.m_vPositions[nIdx].m_nDeep || !this.m_vPositions[nIdx].m_oEngine.equals(oBoardGame)) {
            if (this.m_vPositions[nIdx] == null) {
                this.m_vPositions[nIdx] = new BoardHistoryElement();
            }
            try {
                this.m_vPositions[nIdx].m_oEngine = (Engine) oBoardGame.clone();
                this.m_vPositions[nIdx].m_oEngineMove = (EngineMove) oMove.clone();
            } catch (CloneNotSupportedException e) {
            }
            this.m_vPositions[nIdx].m_nDeep = nDeep;
        }
    }

    public BoardHistoryElement GetMove(Engine oBoardGame) {
        if (!m_bEnabled) {
            return null;
        }
        int nIdx = GetIdx(oBoardGame);
        if (nIdx < 0 || this.m_vPositions[nIdx] == null || this.m_vPositions[nIdx].m_nDeep < 0) {
            return null;
        }
        if (oBoardGame.equals(this.m_vPositions[nIdx].m_oEngine)) {
            return this.m_vPositions[nIdx];
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public int GetIdx(Engine oBoardGame) {
        if (this.m_nSize < 0) {
            return -1;
        }
        int nIdx = (int) (((long) oBoardGame.hashCode()) % ((long) this.m_nSize));
        if (nIdx < 0) {
            nIdx = -nIdx;
        }
        return nIdx;
    }
}
