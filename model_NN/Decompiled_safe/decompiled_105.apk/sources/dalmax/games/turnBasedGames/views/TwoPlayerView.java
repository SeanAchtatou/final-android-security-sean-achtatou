package dalmax.games.turnBasedGames.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.os.CountDownTimer;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import dalmax.Coords2D;
import dalmax.GAE.GAEConnector;
import dalmax.Util;
import dalmax.games.turnBasedGames.Engine;
import dalmax.games.turnBasedGames.GameStatusDescription;
import dalmax.games.turnBasedGames.IEngine;
import dalmax.games.turnBasedGames.ViewMove;
import dalmax.games.turnBasedGames.online.GAEMoveDAO;
import dalmax.games.turnBasedGames.online.Move;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import java.util.concurrent.locks.ReentrantLock;

public abstract class TwoPlayerView extends SurfaceView implements View.OnTouchListener, SurfaceHolder.Callback {
    private static /* synthetic */ int[] $SWITCH_TABLE$dalmax$games$turnBasedGames$views$TwoPlayerView$EGameMode;
    protected Coords2D m_ScreenBoardCoords = new Coords2D(0, 0);
    protected boolean m_bDeepThinking = false;
    protected boolean m_bEndedGame = false;
    protected boolean m_bFirstHumanBegin = true;
    protected boolean m_bFirstHumanTurn = true;
    protected boolean m_bPassMove = false;
    protected boolean m_bShowAvailableMoves = true;
    protected boolean m_bShowLastMove = true;
    protected boolean m_bVibration = true;
    protected EGameMode m_eGameMode = EGameMode.singlePlayer;
    protected ArrayList<ViewMove> m_lMoveList = new ArrayList<>();
    protected int m_nMaxTimePerMove = 5000;
    int m_nPlies = 0;
    protected Long m_nidOnlineGame = 0L;
    protected GameStatusDescription m_oBoardDescription;
    protected IEngine m_oEngine;
    protected LinkedList<GameStatusDescription> m_oGameHistory = new LinkedList<>();
    protected ViewMove m_oMoveCurrent = null;
    GAEMoveDAO m_oMoveDAO = null;
    protected SurfaceHolder m_oSurfaceHolder = getHolder();
    CountDownTimer m_oTimerForAndroidThink = null;
    public Rect m_oViewRect = new Rect();
    protected Vector<ViewMove> m_vMoveHistory = new Vector<>();

    public enum EGameMode {
        singlePlayer,
        twoPlayers,
        online
    }

    /* access modifiers changed from: protected */
    public abstract void ApplyMoveToBoardDescription(GameStatusDescription gameStatusDescription);

    /* access modifiers changed from: protected */
    public abstract void DrawMove(GameStatusDescription gameStatusDescription);

    /* access modifiers changed from: protected */
    public abstract void DrawStaticPositionToCanvas(Canvas canvas, GameStatusDescription gameStatusDescription);

    /* access modifiers changed from: protected */
    public abstract void PassMove();

    public abstract void WinnerDialog(int i);

    public abstract boolean onTouch(View view, MotionEvent motionEvent);

    public abstract void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3);

    static /* synthetic */ int[] $SWITCH_TABLE$dalmax$games$turnBasedGames$views$TwoPlayerView$EGameMode() {
        int[] iArr = $SWITCH_TABLE$dalmax$games$turnBasedGames$views$TwoPlayerView$EGameMode;
        if (iArr == null) {
            iArr = new int[EGameMode.values().length];
            try {
                iArr[EGameMode.online.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[EGameMode.singlePlayer.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[EGameMode.twoPlayers.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$dalmax$games$turnBasedGames$views$TwoPlayerView$EGameMode = iArr;
        }
        return iArr;
    }

    /* access modifiers changed from: protected */
    public void DrawPosition() {
        Canvas canvas = null;
        try {
            canvas = this.m_oSurfaceHolder.lockCanvas();
            if (canvas != null) {
                DrawStaticPositionToCanvas(canvas, this.m_oBoardDescription);
                if (canvas != null) {
                    this.m_oSurfaceHolder.unlockCanvasAndPost(canvas);
                }
            }
        } finally {
            if (canvas != null) {
                this.m_oSurfaceHolder.unlockCanvasAndPost(canvas);
            }
        }
    }

    public TwoPlayerView(Context context, boolean bHumanBegin, EGameMode eGameMode, IEngine oEngine, int nMaxTimeForMove, Long nidOnlineGame) {
        super(context);
        this.m_oSurfaceHolder.addCallback(this);
        this.m_oEngine = oEngine;
        this.m_nidOnlineGame = nidOnlineGame;
        setOnTouchListener(this);
        this.m_bFirstHumanBegin = bHumanBegin;
        SetFirstHumanTurn(bHumanBegin);
        this.m_eGameMode = eGameMode;
        this.m_bDeepThinking = false;
        if (this.m_eGameMode == EGameMode.singlePlayer && this.m_oEngine.GetLevel() > 90) {
            this.m_bDeepThinking = true;
        }
        float t = (float) Util.Max(nMaxTimeForMove - 100, 400);
        float l = (float) this.m_oEngine.GetLevel();
        if (l <= 50.0f) {
            this.m_nMaxTimePerMove = (int) (((((double) l) / 100.0d) * 0.4d * ((double) t)) + 100.0d);
        } else {
            this.m_nMaxTimePerMove = (int) (((((((((double) l) / 100.0d) * 0.8d) * ((double) t)) * 2.0d) + ((0.2d * ((double) t)) * 2.0d)) - ((double) t)) + 100.0d);
        }
        if (this.m_eGameMode == EGameMode.online) {
            this.m_oMoveDAO = new GAEMoveDAO(context);
        }
    }

    public void Exits() {
        try {
            if (this.m_oTimerForAndroidThink != null) {
                this.m_oTimerForAndroidThink.cancel();
            }
            this.m_oTimerForAndroidThink = null;
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void CallMeAtEndOfConstructor() {
        int i;
        ArrayList<Move> lMoves = GetOnLineMoves();
        if (lMoves != null && lMoves.size() > 0) {
            Iterator<Move> it = lMoves.iterator();
            while (it.hasNext()) {
                this.m_nPlies++;
                this.m_oMoveCurrent.SetFromString(it.next().GetMove());
                ApplyMoveToBoardDescription(this.m_oBoardDescription);
                SetFirstHumanTurn(!IsFirstHumanTurn());
                GameStatusDescription gameStatusDescription = this.m_oBoardDescription;
                if (IsFirstHumanTurn()) {
                    i = -1;
                } else {
                    i = 1;
                }
                gameStatusDescription.SetPlayerToMove((byte) i);
                try {
                    this.m_vMoveHistory.add((ViewMove) this.m_oMoveCurrent.clone());
                } catch (CloneNotSupportedException e) {
                }
                this.m_oMoveCurrent.Clear();
            }
        }
        this.m_oEngine.SetInitialBoard(this.m_oBoardDescription);
        this.m_oEngine.ComputeMoveList(this.m_lMoveList);
        DrawPosition();
        if (!IsFirstHumanTurn() && this.m_eGameMode == EGameMode.singlePlayer) {
            MakeComputerMove(2000);
        } else if (!IsFirstHumanTurn() && this.m_eGameMode == EGameMode.online) {
            MakeNetworkMove();
        }
    }

    /* access modifiers changed from: package-private */
    public ArrayList<Move> GetOnLineMoves() {
        if (this.m_eGameMode != EGameMode.online) {
            return null;
        }
        GAEConnector GetGAEConnector = GAEConnector.GetGAEConnector("", "", false);
        return this.m_oMoveDAO.getAll(this.m_nidOnlineGame);
    }

    public void ShowLastMove(boolean bShow) {
        this.m_bShowLastMove = bShow;
        DrawPosition();
    }

    public void ShowAvailableMoves(boolean bShow) {
        this.m_bShowAvailableMoves = bShow;
        DrawPosition();
    }

    public void SetVibration(boolean bVibration) {
        this.m_bVibration = bVibration;
    }

    /* access modifiers changed from: protected */
    public void SetFirstHumanTurn(boolean bFirstHumanTurn) {
        this.m_bFirstHumanTurn = bFirstHumanTurn;
    }

    /* access modifiers changed from: protected */
    public boolean IsFirstHumanTurn() {
        return this.m_bFirstHumanTurn;
    }

    /* access modifiers changed from: protected */
    public void PublishMoveToNetwork() {
        if (this.m_eGameMode == EGameMode.online) {
            String sMove = this.m_oMoveCurrent.toString();
            GAEConnector GetGAEConnector = GAEConnector.GetGAEConnector("", "", false);
            this.m_oMoveDAO.add(new Move(0L, this.m_nidOnlineGame, this.m_nPlies, (short) (IsFirstHumanTurn() ? 0 : 1), sMove));
        }
    }

    /* access modifiers changed from: protected */
    public void MakeNetworkMove() {
        new Timer().scheduleAtFixedRate(new TimerTask() {
            public void run() {
                ArrayList<Move> lMoves = TwoPlayerView.this.GetOnLineMoves();
                if (lMoves != null && lMoves.size() > 0) {
                    Iterator<Move> it = lMoves.iterator();
                    while (it.hasNext()) {
                        Move move = it.next();
                        if (move.GetPly() == TwoPlayerView.this.m_nPlies + 1) {
                            TwoPlayerView.this.m_oMoveCurrent.SetFromString(move.GetMove());
                            cancel();
                            try {
                                TwoPlayerView.this.ApplyMove();
                            } catch (CloneNotSupportedException e) {
                            }
                        }
                    }
                }
            }
        }, (long) 3000, (long) 1500);
    }

    /* access modifiers changed from: protected */
    public void MakeComputerMove(int nTime) {
        final int nNMoves = this.m_oEngine.ComputeMoveList(this.m_lMoveList);
        long nTimeForComputing = (long) this.m_nMaxTimePerMove;
        if (nTime > 0) {
            nTimeForComputing = (long) nTime;
        }
        if (nNMoves <= 1) {
            if (nNMoves == 1) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                }
                try {
                    this.m_oMoveCurrent = (ViewMove) this.m_oEngine.GetMove().clone();
                } catch (CloneNotSupportedException e2) {
                    e2.printStackTrace();
                    return;
                }
            } else {
                this.m_oMoveCurrent.Clear();
            }
            ApplyMove();
            return;
        }
        this.m_oEngine.SetComputing(true, false);
        this.m_oTimerForAndroidThink = new CountDownTimer(nTimeForComputing, 500) {
            private boolean bMoveDone = false;
            final ReentrantLock m_oLockToMove = new ReentrantLock();

            private void DoMoveIfFound(ViewMove oViewMoveFromEngine, boolean bAcceptNull) throws CloneNotSupportedException {
                this.m_oLockToMove.lock();
                try {
                    if (!this.bMoveDone && (bAcceptNull || oViewMoveFromEngine != null)) {
                        this.bMoveDone = true;
                        TwoPlayerView.this.m_oEngine.SetComputing(TwoPlayerView.this.m_bDeepThinking, true);
                        if (oViewMoveFromEngine != null) {
                            TwoPlayerView.this.m_oMoveCurrent = (ViewMove) oViewMoveFromEngine.clone();
                        } else {
                            TwoPlayerView.this.m_oMoveCurrent.Clear();
                        }
                        cancel();
                        TwoPlayerView.this.ApplyMove();
                    }
                } catch (CloneNotSupportedException e) {
                    e.printStackTrace();
                } catch (Throwable th) {
                    this.m_oLockToMove.unlock();
                    throw th;
                }
                this.m_oLockToMove.unlock();
            }

            public void onFinish() {
                try {
                    DoMoveIfFound(TwoPlayerView.this.m_oEngine.GetMove(), true);
                } catch (CloneNotSupportedException e) {
                    e.printStackTrace();
                }
            }

            public void onTick(long millisUntilFinished) {
                try {
                    DoMoveIfFound(TwoPlayerView.this.m_oEngine.GetBestMoveIfPresent(), nNMoves <= 1);
                } catch (CloneNotSupportedException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public void UndoMove() {
        byte nNMoveToBeUndone;
        boolean z;
        if (this.m_oTimerForAndroidThink != null) {
            this.m_oTimerForAndroidThink.cancel();
        }
        this.m_oEngine.SetComputing(false, false);
        if (this.m_eGameMode != EGameMode.singlePlayer || !IsFirstHumanTurn()) {
            nNMoveToBeUndone = 1;
        } else {
            nNMoveToBeUndone = 2;
        }
        for (byte i = 0; i < nNMoveToBeUndone; i = (byte) (i + 1)) {
            if (this.m_oGameHistory.size() > 0) {
                this.m_bEndedGame = false;
                GameStatusDescription oLastGameStatus = this.m_oGameHistory.removeLast();
                if (oLastGameStatus != null) {
                    this.m_nPlies--;
                    this.m_oBoardDescription = oLastGameStatus;
                    if (IsFirstHumanTurn()) {
                        z = false;
                    } else {
                        z = true;
                    }
                    SetFirstHumanTurn(z);
                    this.m_oMoveCurrent.Clear();
                    this.m_oEngine.SetBoardAfterMoveUndone(this.m_oBoardDescription);
                    this.m_oEngine.ComputeMoveList(this.m_lMoveList);
                    DrawPosition();
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                    }
                }
            }
        }
        DrawPosition();
        switch ($SWITCH_TABLE$dalmax$games$turnBasedGames$views$TwoPlayerView$EGameMode()[this.m_eGameMode.ordinal()]) {
            case 1:
                if (IsFirstHumanTurn()) {
                    this.m_oEngine.SetComputing(this.m_bDeepThinking, true);
                    return;
                } else {
                    MakeComputerMove(-1);
                    return;
                }
            case 2:
            default:
                return;
            case 3:
                if (!IsFirstHumanTurn()) {
                    MakeNetworkMove();
                    return;
                }
                return;
        }
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: protected */
    public void ApplyMove() throws CloneNotSupportedException {
        boolean z;
        int i;
        boolean z2;
        this.m_oEngine.SetComputing(false, false);
        DrawMove(this.m_oBoardDescription);
        try {
            this.m_oGameHistory.add((GameStatusDescription) this.m_oBoardDescription.clone());
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        this.m_nPlies++;
        ApplyMoveToBoardDescription(this.m_oBoardDescription);
        if (IsFirstHumanTurn()) {
            z = false;
        } else {
            z = true;
        }
        SetFirstHumanTurn(z);
        GameStatusDescription gameStatusDescription = this.m_oBoardDescription;
        if (IsFirstHumanTurn()) {
            i = -1;
        } else {
            i = 1;
        }
        gameStatusDescription.SetPlayerToMove((byte) i);
        this.m_vMoveHistory.add((ViewMove) this.m_oMoveCurrent.clone());
        PublishMoveToNetwork();
        this.m_oMoveCurrent.Clear();
        this.m_oEngine.SetBoardAfterMoveDone(this.m_oBoardDescription);
        int nNMoves = this.m_oEngine.ComputeMoveList(this.m_lMoveList);
        DrawPosition();
        IEngine iEngine = this.m_oEngine;
        if (nNMoves != 0) {
            z2 = true;
        } else {
            z2 = false;
        }
        Engine.EWinPairLose eWinPairLose = iEngine.CheckWinPairLose(z2);
        if (eWinPairLose != Engine.EWinPairLose.None) {
            byte nWinnerPlayer = 0;
            if (eWinPairLose == Engine.EWinPairLose.Win) {
                nWinnerPlayer = (byte) this.m_oBoardDescription.GetPlayer();
            } else if (eWinPairLose == Engine.EWinPairLose.Lose) {
                nWinnerPlayer = (byte) (-this.m_oBoardDescription.GetPlayer());
            }
            this.m_bEndedGame = true;
            DrawPosition();
            WinnerDialog(nWinnerPlayer);
        } else if (nNMoves == 0) {
            this.m_bPassMove = true;
            try {
                PassMove();
                this.m_bPassMove = false;
                DrawPosition();
                ApplyMove();
            } catch (Throwable th) {
                this.m_bPassMove = false;
                throw th;
            }
        } else {
            switch ($SWITCH_TABLE$dalmax$games$turnBasedGames$views$TwoPlayerView$EGameMode()[this.m_eGameMode.ordinal()]) {
                case 1:
                    if (IsFirstHumanTurn()) {
                        this.m_oEngine.SetComputing(this.m_bDeepThinking, true);
                        return;
                    } else {
                        MakeComputerMove(-1);
                        return;
                    }
                case 2:
                default:
                    return;
                case 3:
                    if (!IsFirstHumanTurn()) {
                        MakeNetworkMove();
                        return;
                    }
                    return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        Matrix matrix = new Matrix();
        matrix.postScale(((float) newWidth) / ((float) width), ((float) newHeight) / ((float) height));
        return Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        DrawStaticPositionToCanvas(canvas, this.m_oBoardDescription);
    }

    public void surfaceCreated(SurfaceHolder holder) {
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
    }
}
