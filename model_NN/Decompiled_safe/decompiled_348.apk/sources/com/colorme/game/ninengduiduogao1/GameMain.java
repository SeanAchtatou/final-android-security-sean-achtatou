package com.colorme.game.ninengduiduogao1;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.view.KeyEvent;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.adwhirl.AdWhirlLayout;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class GameMain extends Activity {
    /* access modifiers changed from: private */
    public AdWhirlLayout adWhirlLayout;
    private boolean isStrat;
    private String key = "a1943990b1cf4685938759412f4f5f7f";
    private TimerTask mTask = null;
    /* access modifiers changed from: private */
    public int mTime = 0;
    private Timer mTimer = null;
    private WebView mWebView;
    public Handler m_Handle = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    if (GameMain.this.adWhirlLayout.getVisibility() == 0) {
                        if (GameMain.this.mTime == 10) {
                            GameMain.this.adWhirlLayout.setVisibility(4);
                        }
                    } else if (4 == GameMain.this.adWhirlLayout.getVisibility() && GameMain.this.mTime == 15) {
                        GameMain.this.adWhirlLayout.setVisibility(0);
                        int unused = GameMain.this.mTime = 0;
                    }
                    GameMain.access$104(GameMain.this);
                    return;
                default:
                    return;
            }
        }
    };

    static /* synthetic */ int access$104(GameMain gameMain) {
        int i = gameMain.mTime + 1;
        gameMain.mTime = i;
        return i;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        CheckFlash();
        setContentView((int) R.layout.main);
        this.mWebView = (WebView) findViewById(R.id.webView);
        WebSettings settings = this.mWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setPluginsEnabled(true);
        settings.setPluginState(WebSettings.PluginState.ON);
        this.mWebView.setWebChromeClient(new WebChromeClient());
        this.mWebView.setScrollBarStyle(33554432);
        this.mWebView.clearCache(true);
        this.mWebView.loadUrl("file:///android_asset/main.swf");
        this.mTimer = new Timer(true);
        this.mTask = new TimerTask() {
            public void run() {
                Message message = new Message();
                message.what = 1;
                GameMain.this.m_Handle.sendMessage(message);
            }
        };
        this.mTimer.schedule(this.mTask, 1000, 1000);
        this.adWhirlLayout = new AdWhirlLayout(this, this.key);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(14);
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.layout_main);
        linearLayout.setGravity(1);
        linearLayout.addView(this.adWhirlLayout, layoutParams);
        linearLayout.invalidate();
    }

    /* access modifiers changed from: protected */
    public void CheckFlash() {
        int i = 0;
        List<PackageInfo> installedPackages = getPackageManager().getInstalledPackages(0);
        while (i < installedPackages.size()) {
            PackageInfo packageInfo = installedPackages.get(i);
            if (packageInfo.versionName == null || !packageInfo.packageName.equals("com.adobe.flashplayer")) {
                i++;
            } else if (packageInfo.versionCode < 102000000) {
                FTAADlg.DisplayDlg(this, R.string.msgTipVersion);
                return;
            } else {
                return;
            }
        }
        FTAADlg.DisplayDlg(this, R.string.msgTip);
    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.mWebView.freeMemory();
        Process.killProcess(Process.myPid());
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    public void gameResume() {
        if (!this.isStrat) {
            this.isStrat = true;
            this.mWebView.loadUrl("javascript:callJsPlay()");
        }
    }

    public void gamePause() {
        this.isStrat = false;
        this.mWebView.loadUrl("javascript:callJsStop()");
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        FTAADlg.ExitDlg(this);
        return true;
    }
}
