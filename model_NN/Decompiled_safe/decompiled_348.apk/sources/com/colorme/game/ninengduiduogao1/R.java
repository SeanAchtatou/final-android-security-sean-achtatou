package com.colorme.game.ninengduiduogao1;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int icon = 2130837504;
    }

    public static final class id {
        public static final int layout_main = 2131034112;
        public static final int webView = 2131034113;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class string {
        public static final int app_name = 2130968576;
        public static final int btn_No = 2130968578;
        public static final int btn_Yes = 2130968577;
        public static final int exitTip = 2130968582;
        public static final int msgTip = 2130968580;
        public static final int msgTipVersion = 2130968581;
        public static final int tipTitle = 2130968579;
    }
}
