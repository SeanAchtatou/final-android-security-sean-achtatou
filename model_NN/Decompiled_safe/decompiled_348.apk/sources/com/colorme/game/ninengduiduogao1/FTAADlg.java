package com.colorme.game.ninengduiduogao1;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;

public class FTAADlg {
    public static void DisplayDlg(final Context context, int i) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle((int) R.string.tipTitle);
        builder.setMessage(i);
        builder.setPositiveButton((int) R.string.btn_Yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setData(Uri.parse("market://details?id=com.adobe.flashplayer"));
                if (context.getPackageManager().resolveActivity(intent, 65536) != null) {
                    context.startActivity(intent);
                }
            }
        });
        builder.setNegativeButton((int) R.string.btn_No, (DialogInterface.OnClickListener) null);
        builder.show();
    }

    public static void ExitDlg(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle((int) R.string.tipTitle);
        builder.setMessage((int) R.string.exitTip);
        builder.setPositiveButton((int) R.string.btn_Yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (context instanceof GameMain) {
                    ((GameMain) context).finish();
                }
            }
        });
        builder.setNegativeButton((int) R.string.btn_No, (DialogInterface.OnClickListener) null);
        builder.show();
    }
}
