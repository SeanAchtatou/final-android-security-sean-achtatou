package softkos.turnmeon;

import android.graphics.drawable.Drawable;

public class ImageLoader {
    static ImageLoader instance = null;
    String[] image_names = {"240x50off", "240x50on", "gamename", "howplay1", "howplay2", "twitter", "solved", "stage", "trash_off", "trash_on", "zoom_in", "zoom_out", "scroll_enabled", "cancel", "square_0", "square_1", "square_2", "arrow_left", "arrow_right", "arrow_down", "arrow_up", "rotateme_ad", "frogs_jump_ad", "untangle_ad", "cubix_ad", "boxit_ad", "ume_ad", "tripeaks_ad"};
    Drawable[] images;
    int[] img_id = {R.drawable.button_off, R.drawable.button_on, R.drawable.gamename, R.drawable.howplay, R.drawable.howplay2, R.drawable.twitter, R.drawable.solved, R.drawable.stage, R.drawable.trash_off, R.drawable.trash_on, R.drawable.zoom_in, R.drawable.zoom_out, R.drawable.scroll_enabled, R.drawable.cancel, R.drawable.square_0, R.drawable.square_1, R.drawable.square_2, R.drawable.arrow_left, R.drawable.arrow_right, R.drawable.arrow_down, R.drawable.arrow_up, R.drawable.rotateme_ad, R.drawable.frogs_jump_ad, R.drawable.untangle_ad, R.drawable.cubix_ad, R.drawable.boxit_ad, R.drawable.ume_ad, R.drawable.tripeaks_ad};

    public static ImageLoader getInstance() {
        if (instance == null) {
            instance = new ImageLoader();
        }
        return instance;
    }

    public void LoadImages() {
        int len = this.img_id.length;
        this.images = new Drawable[this.image_names.length];
        for (int i = 0; i < len; i++) {
            this.images[i] = MainActivity.getInstance().getResources().getDrawable(this.img_id[i]);
        }
    }

    public Drawable getImage(String img) {
        for (int i = 0; i < this.image_names.length; i++) {
            if (img.equals(this.image_names[i])) {
                return this.images[i];
            }
        }
        return null;
    }
}
