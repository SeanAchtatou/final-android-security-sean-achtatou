package softkos.turnmeon;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import softkos.turnmeon.Vars;

public class MainActivity extends Activity {
    static MainActivity instance;
    Button closeSettingsDlgButton = null;
    GameCanvas gameCanvas = null;
    RelativeLayout gameLayout = null;
    Button homepageBtn = null;
    HowPlayCanvas howPlayCanvas = null;
    MenuCanvas menuCanvas = null;
    Button resetAllLevelsBtn = null;
    Dialog settingsDlg = null;
    Button soundSettingBtn = null;
    Button vibrationSettingDlg = null;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        instance = this;
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        Vars.getInstance().screenSize(dm.widthPixels, dm.heightPixels);
        ImageLoader.getInstance().LoadImages();
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        showMenu();
    }

    public void showMenu() {
        Vars.getInstance().gameState = Vars.GameState.Menu;
        if (this.menuCanvas == null) {
            this.menuCanvas = new MenuCanvas(this);
        }
        if (this.gameCanvas != null) {
            this.gameCanvas.showUI(false);
        }
        OtherApp.getInstance().random();
        this.menuCanvas.updateButtonText();
        this.menuCanvas.showUI(true);
        this.menuCanvas.layoutUI();
        setContentView(this.menuCanvas);
    }

    public void showHowPlay() {
        Vars.getInstance().gameState = Vars.GameState.HowPlay;
        if (this.howPlayCanvas == null) {
            this.howPlayCanvas = new HowPlayCanvas(this);
        }
        if (this.menuCanvas != null) {
            this.menuCanvas.showUI(false);
        }
        setContentView(this.howPlayCanvas);
    }

    public void showHighscores() {
    }

    public void showGame() {
        if (this.menuCanvas != null) {
            this.menuCanvas.showUI(false);
        }
        if (this.gameCanvas == null) {
            this.gameLayout = new RelativeLayout(this);
            this.gameLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
            this.gameCanvas = new GameCanvas(this);
            AdView adView = new AdView(this, AdSize.BANNER, "a14d696b8005011");
            RelativeLayout.LayoutParams lparams = new RelativeLayout.LayoutParams(-1, -2);
            adView.setLayoutParams(lparams);
            lparams.addRule(12);
            this.gameLayout.addView(adView);
            this.gameLayout.addView(this.gameCanvas);
            adView.loadAd(new AdRequest());
            adView.bringToFront();
        }
        Vars.getInstance().gameState = Vars.GameState.Game;
        this.gameCanvas.showUI(true);
        Vars.getInstance().newGame();
        setContentView(this.gameLayout);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4 && (Vars.getInstance().gameState == Vars.GameState.Game || Vars.getInstance().gameState == Vars.GameState.Highscores || Vars.getInstance().gameState == Vars.GameState.HowPlay)) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode != 4 || (Vars.getInstance().gameState != Vars.GameState.Game && Vars.getInstance().gameState != Vars.GameState.Highscores && Vars.getInstance().gameState != Vars.GameState.HowPlay)) {
            return super.onKeyUp(keyCode, event);
        }
        if (Vars.getInstance().gameState != Vars.GameState.Game || GameCanvas.getInstance() == null || GameCanvas.getInstance().getLevelSelectionDialog() == null) {
            showMenu();
            return true;
        }
        GameCanvas.getInstance().dismissDialogs();
        return true;
    }

    public static MainActivity getInstance() {
        return instance;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 2001, 0, "Prev level").setIcon((int) R.drawable.arrow_left);
        menu.add(0, 2002, 0, "Next level").setIcon((int) R.drawable.arrow_right);
        MenuItem add = menu.add(0, 2000, 0, "Reset level");
        MenuItem add2 = menu.add(0, 2004, 0, "Select level");
        MenuItem add3 = menu.add(0, 2006, 0, "Settings");
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 2000:
                Vars.getInstance().resetLevel();
                FileWR.getInstance().saveGame();
                return true;
            case 2001:
                Vars.getInstance().prevLevel();
                return true;
            case 2002:
                Vars.getInstance().nextLevel();
                return true;
            case 2003:
                resetAllLevels();
                FileWR.getInstance().saveGame();
                return true;
            case 2004:
                GameCanvas.getInstance().selectLevelDialog();
                return true;
            case 2005:
            default:
                return false;
            case 2006:
                showSettingsDialog();
                return true;
        }
    }

    public void resetAllLevels() {
        AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
        alertbox.setTitle("Reset all levels");
        alertbox.setMessage("Do you want reset all solved levels?");
        alertbox.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                Levels.getInstance().resetLevels();
                FileWR.getInstance().saveGame();
            }
        });
        alertbox.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
            }
        });
        alertbox.show();
    }

    public void vibrate(int ms) {
        if (Settings.getInstnace().getIntSettings(Settings.VIBRATION_DISABLED) == 0) {
            try {
                ((Vibrator) getSystemService("vibrator")).vibrate((long) ms);
            } catch (Exception e) {
            }
        }
    }

    public Dialog getSettingsDlg() {
        return this.settingsDlg;
    }

    public void showSettingsDialog() {
        String s;
        this.settingsDlg = new Dialog(this);
        this.settingsDlg.requestWindowFeature(1);
        this.settingsDlg.setContentView((int) R.layout.settings);
        this.settingsDlg.show();
        this.closeSettingsDlgButton = (Button) this.settingsDlg.findViewById(R.id.close_settings_dlg);
        this.closeSettingsDlgButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Settings.getInstnace().saveSettings();
                MainActivity.getInstance().getSettingsDlg().dismiss();
            }
        });
        this.vibrationSettingDlg = (Button) this.settingsDlg.findViewById(R.id.vibration_setting);
        if (Settings.getInstnace().getIntSettings(Settings.VIBRATION_DISABLED) != 0) {
            s = String.valueOf("Vibration: ") + "OFF";
        } else {
            s = String.valueOf("Vibration: ") + "ON";
        }
        this.vibrationSettingDlg.setText(s);
        this.vibrationSettingDlg.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                String s;
                Settings.getInstnace().toggleSettings(Settings.VIBRATION_DISABLED);
                if (Settings.getInstnace().getIntSettings(Settings.VIBRATION_DISABLED) != 0) {
                    s = String.valueOf("Vibration: ") + "OFF";
                } else {
                    s = String.valueOf("Vibration: ") + "ON";
                }
                MainActivity.this.vibrationSettingDlg.setText(s);
            }
        });
        this.homepageBtn = (Button) this.settingsDlg.findViewById(R.id.home_page);
        this.homepageBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MainActivity.getInstance().startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.mobilsoft.pl")));
            }
        });
        this.resetAllLevelsBtn = (Button) this.settingsDlg.findViewById(R.id.reset_all_levels);
        this.resetAllLevelsBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MainActivity.this.resetAllLevels();
            }
        });
    }
}
