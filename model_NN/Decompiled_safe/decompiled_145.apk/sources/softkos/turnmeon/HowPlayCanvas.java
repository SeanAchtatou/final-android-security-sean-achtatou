package softkos.turnmeon;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;

public class HowPlayCanvas extends View {
    static HowPlayCanvas instance = null;
    int page = 1;
    Paint paint = new Paint();
    PaintManager paintMgr;
    Vars vars;

    public HowPlayCanvas(Context c) {
        super(c);
        instance = this;
        this.vars = Vars.getInstance();
        this.paintMgr = PaintManager.getInstance();
        initUI();
    }

    public void showUI(boolean show) {
        for (int i = 0; i < this.vars.getButtonListSize(); i++) {
            this.vars.getButton(i).hide();
        }
    }

    public void initUI() {
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        layoutUI();
    }

    public void layoutUI() {
    }

    public void mouseDown(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDown(x, y);
            if (pressed != 0) {
                invalidate();
                return;
            }
        }
    }

    public void mouseUp(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseUp(x, y);
            if (pressed != 0) {
                handleCommand(Vars.getInstance().getButton(i).getId());
                invalidate();
                return;
            }
        }
        if (((double) y) <= ((double) getHeight()) - (((double) getHeight()) * 0.2d)) {
            return;
        }
        if (this.page == 1) {
            this.page = 2;
        } else {
            this.page = 1;
        }
    }

    public void mouseDrag(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDrag(x, y);
            if (pressed != 0) {
                invalidate();
                return;
            }
        }
    }

    public void handleCommand(int id) {
    }

    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();
        if (event.getAction() == 0) {
            mouseDown(x, y);
        } else if (2 == event.getAction()) {
            mouseDrag(x, y);
        } else if (1 == event.getAction()) {
            mouseUp(x, y);
        }
        invalidate();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.paint.setAntiAlias(true);
        int c1 = Color.rgb(33, 49, 74);
        int c2 = Color.rgb(43, 59, 84);
        int size = getWidth() / 5;
        int curr_col = c1;
        for (int y = 0; y < getHeight(); y += size) {
            for (int x = 0; x < getWidth(); x += size) {
                if (curr_col == c1) {
                    curr_col = c2;
                } else {
                    curr_col = c1;
                }
                this.paintMgr.setColor(curr_col);
                this.paintMgr.fillRectangle(canvas, x, y, size, size);
            }
        }
        this.paintMgr.drawImage(canvas, "howplay1", 0, 0, getWidth(), getHeight());
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            Vars.getInstance().getButton(i).draw(canvas, this.paint);
        }
    }

    public static HowPlayCanvas getInstance() {
        return instance;
    }
}
