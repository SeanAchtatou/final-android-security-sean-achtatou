package softkos.turnmeon;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FileWR {
    static FileWR instance = null;
    String FILENAME = "connectus";
    int file_pos = 0;

    public static final byte[] intToByteArray(int value) {
        return new byte[]{(byte) (value >>> 24), (byte) (value >>> 16), (byte) (value >>> 8), (byte) value};
    }

    public static final int byteArrayToInt(byte[] b) {
        return (b[0] << 24) + ((b[1] & 255) << 16) + ((b[2] & 255) << 8) + (b[3] & 255);
    }

    public void writeInt(FileOutputStream fos, int val) {
        try {
            fos.write(intToByteArray(val));
        } catch (Exception e) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, ?[OBJECT, ARRAY], java.io.IOException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, ?[OBJECT, ARRAY], java.io.FileNotFoundException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public void saveGame() {
        try {
            FileOutputStream fos = MainActivity.getInstance().getApplicationContext().openFileOutput(this.FILENAME, 0);
            for (int i = 0; i < 800; i++) {
                writeInt(fos, Levels.getInstance().isSolved(i));
            }
            try {
                fos.close();
            } catch (IOException e) {
                Logger.getLogger(FileWR.class.getName()).log(Level.SEVERE, (String) null, (Throwable) e);
            }
        } catch (FileNotFoundException e2) {
            Logger.getLogger(FileWR.class.getName()).log(Level.SEVERE, (String) null, (Throwable) e2);
        }
    }

    public int getInt(byte[] buffer) {
        if (this.file_pos + 3 >= buffer.length) {
            return 0;
        }
        byte[] int_buf = {buffer[this.file_pos], buffer[this.file_pos + 1], buffer[this.file_pos + 2], buffer[this.file_pos + 3]};
        this.file_pos += 4;
        return byteArrayToInt(int_buf);
    }

    public String getString(byte[] buffer, int len) {
        byte[] tmp = new byte[len];
        for (int i = 0; i < len; i++) {
            tmp[i] = buffer[this.file_pos + i];
        }
        this.file_pos += len;
        return new String(tmp);
    }

    public void loadGame() {
        try {
            FileInputStream fis = MainActivity.getInstance().getApplicationContext().openFileInput(this.FILENAME);
            try {
                byte[] buffer = new byte[fis.available()];
                fis.read(buffer);
                if (buffer.length == 3200) {
                    for (int i = 0; i < 800; i++) {
                        if (getInt(buffer) == 1) {
                            Levels.getInstance().setSolved(i);
                        }
                    }
                }
                fis.close();
            } catch (IOException e) {
            }
        } catch (FileNotFoundException e2) {
            FileNotFoundException fileNotFoundException = e2;
        }
    }

    public static FileWR getInstance() {
        if (instance == null) {
            instance = new FileWR();
        }
        return instance;
    }
}
