package softkos.turnmeon;

public class Line {
    public int end;
    public int start;
    public boolean visible = true;
    public int xoff1 = (Vars.getInstance().nextInt(20) - 10);
    public int xoff2 = (Vars.getInstance().nextInt(20) - 10);
    public int yoff1 = (Vars.getInstance().nextInt(20) - 10);
    public int yoff2 = (Vars.getInstance().nextInt(20) - 10);

    public void hide() {
        this.visible = false;
    }
}
