package softkos.turnmeon;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int arrow_down = 2130837504;
        public static final int arrow_left = 2130837505;
        public static final int arrow_right = 2130837506;
        public static final int arrow_up = 2130837507;
        public static final int boxit_ad = 2130837508;
        public static final int button_off = 2130837509;
        public static final int button_on = 2130837510;
        public static final int cancel = 2130837511;
        public static final int cubix_ad = 2130837512;
        public static final int frogs_jump_ad = 2130837513;
        public static final int gamename = 2130837514;
        public static final int howplay = 2130837515;
        public static final int howplay2 = 2130837516;
        public static final int icon = 2130837517;
        public static final int rotateme_ad = 2130837518;
        public static final int scroll_enabled = 2130837519;
        public static final int solved = 2130837520;
        public static final int square_0 = 2130837521;
        public static final int square_1 = 2130837522;
        public static final int square_2 = 2130837523;
        public static final int square_3 = 2130837524;
        public static final int stage = 2130837525;
        public static final int trash_off = 2130837526;
        public static final int trash_on = 2130837527;
        public static final int tripeaks_ad = 2130837528;
        public static final int twitter = 2130837529;
        public static final int ume_ad = 2130837530;
        public static final int untangle_ad = 2130837531;
        public static final int zoom_in = 2130837532;
        public static final int zoom_out = 2130837533;
    }

    public static final class id {
        public static final int Settings_empty1 = 2131034120;
        public static final int Settings_title = 2131034119;
        public static final int cancel = 2131034118;
        public static final int close_settings_dlg = 2131034125;
        public static final int empty_5 = 2131034124;
        public static final int home_page = 2131034123;
        public static final int layout_root = 2131034112;
        public static final int layout_root3 = 2131034116;
        public static final int next_level = 2131034115;
        public static final int ok = 2131034117;
        public static final int prev_level = 2131034113;
        public static final int reset_all_levels = 2131034122;
        public static final int selected_level = 2131034114;
        public static final int vibration_setting = 2131034121;
    }

    public static final class layout {
        public static final int level_selection = 2130903040;
        public static final int main = 2130903041;
        public static final int settings = 2130903042;
    }

    public static final class string {
        public static final int app_name = 2130968577;
        public static final int hello = 2130968576;
    }
}
