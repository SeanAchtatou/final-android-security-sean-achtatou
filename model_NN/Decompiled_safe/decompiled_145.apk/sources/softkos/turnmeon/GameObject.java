package softkos.turnmeon;

public class GameObject {
    int dest_px;
    int dest_py;
    int height;
    boolean is_hiding = false;
    int origin_x;
    int origin_y;
    int owner_id = -1;
    int px;
    int py;
    boolean visible = true;
    int width;

    public void setOriginPos(int x, int y) {
        this.origin_x = x;
        this.origin_y = y;
    }

    public boolean checkSelection(int x, int y) {
        if (!this.visible) {
            return false;
        }
        return x > getPx() && x < getPx() + getWidth() && y > getPy() && y < getPy() + getHeight();
    }

    public boolean isHiding() {
        return this.is_hiding;
    }

    public int getOriginX() {
        return this.origin_x;
    }

    public int getOriginY() {
        return this.origin_y;
    }

    public boolean isVisible() {
        return this.visible;
    }

    public void hide() {
        this.is_hiding = true;
    }

    public void show() {
        this.is_hiding = false;
        this.visible = true;
    }

    public void setVisible(boolean v) {
        this.visible = v;
    }

    public void setOwnerID(int id) {
        this.owner_id = id;
    }

    public int getOwnerID() {
        return this.owner_id;
    }

    public int getPx() {
        return this.px;
    }

    public int getPy() {
        return this.py;
    }

    public void SetPos(int x, int y) {
        this.px = x;
        this.py = y;
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public void setSize(int w, int h) {
        this.width = w;
        this.height = h;
    }

    public int getDestPx() {
        return this.dest_px;
    }

    public int getDestPy() {
        return this.dest_py;
    }

    public void setDestPos(int x, int y) {
        this.dest_px = x;
        this.dest_py = y;
    }

    public void update() {
        int speed_x = Math.abs(this.px - this.dest_px) / 5;
        int speed_y = Math.abs(this.py - this.dest_py) / 5;
        if (speed_x < 2) {
            speed_x = 2;
        }
        if (speed_y < 2) {
            speed_y = 2;
        }
        if (this.px != this.dest_px) {
            if (Math.abs(this.px - this.dest_px) <= speed_x) {
                this.px = this.dest_px;
            } else if (this.px > this.dest_px) {
                this.px -= speed_x;
            } else if (this.px < this.dest_px) {
                this.px += speed_x;
            }
        }
        if (this.py == this.dest_py) {
            return;
        }
        if (Math.abs(this.py - this.dest_py) <= speed_y) {
            this.py = this.dest_py;
        } else if (this.py > this.dest_py) {
            this.py -= speed_y;
        } else if (this.py < this.dest_py) {
            this.py += speed_y;
        }
    }
}
