package softkos.turnmeon;

import java.util.Random;

public class OtherApp {
    static OtherApp instance = null;
    public String[] button_text = {"   Play Tripeaks!", "   Untangle me", "   Try Frogs Jump", "    Get Cubix", "    Try BoxIt"};
    public int current;
    public String[] images = {"tripeaks_ad", "ume_ad", "frogs_jump_ad", "cubix_ad", "boxit_ad"};
    Random r = new Random();
    public String[] urls = {"market://details?id=softkos.tripeaks", "market://details?id=softkos.untanglemeextreme", "market://details?id=sofkos.frogsjump", "market://details?id=sofkos.cubix", "market://details?id=softkos.boxit"};

    public OtherApp() {
        random();
    }

    public void random() {
        this.current = this.r.nextInt(this.button_text.length);
    }

    public static OtherApp getInstance() {
        if (instance == null) {
            instance = new OtherApp();
        }
        return instance;
    }
}
