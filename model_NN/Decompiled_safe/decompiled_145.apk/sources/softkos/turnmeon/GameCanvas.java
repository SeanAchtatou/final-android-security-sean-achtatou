package softkos.turnmeon;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;

public class GameCanvas extends View {
    static GameCanvas instance = null;
    gButton disableScrollBtn = null;
    int key_size;
    AlertDialog lvlSelectDlg = null;
    int mouseDownX = 0;
    int mouseDownY = 0;
    int mouseX = -1;
    int mouseY = -1;
    Paint paint;
    PaintManager paintMgr;
    gButton playNextBtn = null;
    boolean scrollEnabled = true;
    int tmp_line_ex = -1;
    int tmp_line_ey = -1;
    int tmp_line_sx = -1;
    int tmp_line_sy = -1;
    Vars vars;
    int vpx;
    int vpy;
    gButton zoomInBtn = null;
    gButton zoomOutBtn = null;

    public GameCanvas(Context context) {
        super(context);
        instance = this;
        this.paint = new Paint();
        this.paintMgr = PaintManager.getInstance();
        this.vars = Vars.getInstance();
        initUI();
    }

    public void initUI() {
        int btn_w = 240;
        int btn_h = 50;
        if (this.vars.getScreenWidth() > 400) {
            btn_w = 360;
            btn_h = 80;
        }
        this.playNextBtn = new gButton();
        this.playNextBtn.setSize(btn_w, btn_h);
        this.playNextBtn.setId(Vars.getInstance().NEXT_LEVEL);
        this.playNextBtn.hide();
        this.playNextBtn.setText("Next level");
        this.playNextBtn.setBackImages("240x50off", "240x50on");
        Vars.getInstance().addButton(this.playNextBtn);
        this.disableScrollBtn = new gButton();
        this.disableScrollBtn.setSize(btn_h, btn_h);
        this.disableScrollBtn.setId(Vars.getInstance().TOGGLE_SCREEN_SCROLL);
        this.disableScrollBtn.hide();
        this.disableScrollBtn.setImage("scroll_enabled");
        this.disableScrollBtn.setBackImages(null, null);
        Vars.getInstance().addButton(this.disableScrollBtn);
        this.zoomInBtn = new gButton();
        this.zoomInBtn.setSize(btn_h, btn_h);
        this.zoomInBtn.setId(Vars.getInstance().ZOOM_IN);
        this.zoomInBtn.hide();
        this.zoomInBtn.setImage("zoom_in");
        this.zoomInBtn.setBackImages(null, null);
        Vars.getInstance().addButton(this.zoomInBtn);
        this.zoomOutBtn = new gButton();
        this.zoomOutBtn.setSize(btn_h, btn_h);
        this.zoomOutBtn.setId(Vars.getInstance().ZOOM_OUT);
        this.zoomOutBtn.hide();
        this.zoomOutBtn.setImage("zoom_out");
        this.zoomOutBtn.setBackImages(null, null);
        Vars.getInstance().addButton(this.zoomOutBtn);
    }

    public void showUI(boolean show) {
        this.vars.hideAllButtons();
        if (show) {
            this.disableScrollBtn.show();
            this.zoomInBtn.show();
            this.zoomOutBtn.show();
        }
    }

    public void layoutUI() {
        if (this.playNextBtn != null) {
            this.playNextBtn.setPosition((getWidth() / 2) - (this.playNextBtn.getWidth() / 2), ((getHeight() / 2) + (getWidth() / 2)) - ((int) (((double) this.playNextBtn.getHeight()) * 1.5d)));
            this.zoomInBtn.setPosition(this.vars.getScreenWidth() - this.disableScrollBtn.getWidth(), 0);
            this.disableScrollBtn.setPosition(this.vars.getScreenWidth() - this.disableScrollBtn.getWidth(), this.disableScrollBtn.getHeight() + 2);
            this.zoomOutBtn.setPosition(0, 0);
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        layoutUI();
    }

    public void drawBack(Canvas canvas) {
        int c1 = Color.rgb(33, 49, 74);
        int c2 = Color.rgb(43, 59, 84);
        int size = getWidth() / 3;
        int curr_col = c1;
        int vpx2 = this.vars.getVpx();
        int bx = vpx2 % (size * 2);
        int by = this.vars.getVpy() % (size * 2);
        for (int y = -2; y < 12; y++) {
            for (int x = -2; x < 5; x++) {
                if (curr_col == c1) {
                    curr_col = c2;
                } else {
                    curr_col = c1;
                }
                this.paintMgr.setColor(curr_col);
                this.paintMgr.fillRectangle(canvas, (x * size) - bx, (y * size) - by, size, size);
            }
        }
    }

    public void drawGame(Canvas canvas) {
        drawBack(canvas);
        int vpx2 = this.vars.getVpx();
        int vpy2 = this.vars.getVpy();
        this.paintMgr.setColor(-1);
        if (this.tmp_line_sx >= 0 && this.tmp_line_ex >= 0) {
            this.paintMgr.drawLine(canvas, this.tmp_line_sx, this.tmp_line_sy, this.tmp_line_ex, this.tmp_line_ey);
        }
        double zl = this.vars.getZoomLevel();
        this.paintMgr.setColor(-1);
        this.paintMgr.setStrokeWidth(3);
        for (int i = 0; i < this.vars.getLineList().size(); i++) {
            if (this.vars.getLine(i).visible) {
                int si = this.vars.getLine(i).start;
                int ei = this.vars.getLine(i).end;
                this.paintMgr.drawLine(canvas, ((int) (((double) (this.vars.getSquare(si).getPx() + (this.vars.getSquare(si).getWidth() / 2))) * zl)) - vpx2, ((int) (((double) (this.vars.getSquare(si).getPy() + (this.vars.getSquare(si).getHeight() / 2))) * zl)) - vpy2, ((int) (((double) (this.vars.getSquare(ei).getPx() + (this.vars.getSquare(ei).getWidth() / 2))) * zl)) - vpx2, ((int) (((double) (this.vars.getSquare(ei).getPy() + (this.vars.getSquare(ei).getHeight() / 2))) * zl)) - vpy2);
            }
        }
        this.paintMgr.setColor(-16777216);
        String image = "";
        for (int i2 = 0; i2 < this.vars.getSquareList().size(); i2++) {
            int val = this.vars.getSquare(i2).getValue();
            int curr_val = this.vars.getSquare(i2).getCurrVal();
            if (curr_val < val) {
                image = "square_0";
            }
            if (curr_val == val) {
                image = "square_1";
            }
            if (curr_val > val) {
                image = "square_2";
            }
            this.paintMgr.drawImage(canvas, image, ((int) (((double) this.vars.getSquare(i2).getPx()) * zl)) - vpx2, ((int) (((double) this.vars.getSquare(i2).getPy()) * zl)) - vpy2, (int) (((double) this.vars.getSquare(i2).getWidth()) * zl), (int) (((double) this.vars.getSquare(i2).getHeight()) * zl));
            this.paintMgr.setTextSize(25);
            this.paintMgr.drawString(canvas, new StringBuilder().append(this.vars.getSquare(i2).getValue()).toString(), ((int) (((double) this.vars.getSquare(i2).getPx()) * zl)) - vpx2, ((int) (((double) this.vars.getSquare(i2).getPy()) * zl)) - vpy2, (int) (((double) this.vars.getSquare(i2).getWidth()) * zl), (int) (((double) this.vars.getSquare(i2).getHeight()) * zl), PaintManager.STR_CENTER);
        }
        this.paintMgr.setTextSize(26);
        this.paintMgr.setColor(-1);
        this.paintMgr.drawString(canvas, "Level: " + (this.vars.getLevel() + 1), 0, 8, getWidth(), this.paintMgr.getTextSize(), PaintManager.STR_CENTER);
        this.playNextBtn.hide();
        if (this.vars.isEndLevel()) {
            this.paintMgr.drawImage(canvas, "solved", 0, (getHeight() / 2) - (getWidth() / 2), getWidth(), getWidth());
            this.playNextBtn.show();
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.paint == null) {
            this.paint = new Paint();
        }
        if (canvas != null) {
            drawGame(canvas);
            for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
                if (Vars.getInstance().getButton(i).getId() < Vars.getInstance().TRASH_0) {
                    Vars.getInstance().getButton(i).draw(canvas, this.paint);
                } else {
                    int bw = Vars.getInstance().getButton(i).getWidth();
                    int bh = Vars.getInstance().getButton(i).getHeight();
                    Vars.getInstance().getButton(i).setSize((int) (((double) bw) * this.vars.getZoomLevel()), (int) (((double) bh) * this.vars.getZoomLevel()));
                    Vars.getInstance().getButton(i).draw(canvas, this.paint);
                    Vars.getInstance().getButton(i).setSize(bw, bh);
                }
            }
            if (!this.scrollEnabled) {
                this.paintMgr.drawImage(canvas, "cancel", this.disableScrollBtn.getPx(), this.disableScrollBtn.getPy(), this.disableScrollBtn.getWidth(), this.disableScrollBtn.getHeight());
            }
        }
    }

    public void mouseDown(int x, int y) {
        this.mouseDownX = x;
        this.mouseDownY = y;
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDown(x, y);
            if (pressed != 0) {
                invalidate();
                return;
            }
        }
        if (this.scrollEnabled) {
            this.vars.startScroll(x, y);
        }
        if (!this.vars.isEndLevel() && this.vars.getSquareAt(x, y) >= 0) {
            this.tmp_line_sx = x;
            this.tmp_line_sy = y;
            MainActivity.getInstance().vibrate(50);
        }
    }

    public void mouseDrag(int x, int y) {
        this.mouseX = x;
        this.mouseY = y;
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDrag(x, y);
            if (pressed != 0) {
                invalidate();
                return;
            }
        }
        if (this.scrollEnabled) {
            this.vars.scroll(x, y);
        }
        if (!this.vars.isEndLevel()) {
            this.tmp_line_ex = x;
            this.tmp_line_ey = y;
        }
    }

    public void mouseUp(int x, int y) {
        this.vars.stopScroll(x, y);
        this.mouseX = -1;
        this.mouseY = -1;
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseUp(x, y);
            if (pressed != 0) {
                handleCommand(Vars.getInstance().getButton(i).getId());
                invalidate();
                return;
            }
        }
        if (this.vars.getSquareAt(x, y) >= 0) {
            MainActivity.getInstance().vibrate(50);
        }
        if (!this.vars.isEndLevel()) {
            this.vars.addLine(this.tmp_line_sx, this.tmp_line_sy, this.tmp_line_ex, this.tmp_line_ey);
        }
        this.tmp_line_ex = -1;
        this.tmp_line_ey = -1;
        this.tmp_line_sx = -1;
        this.tmp_line_sy = -1;
        invalidate();
    }

    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();
        if (event.getAction() == 0) {
            mouseDown(x, y);
        } else if (2 == event.getAction()) {
            mouseDrag(x, y);
        } else if (1 == event.getAction()) {
            mouseUp(x, y);
        }
        invalidate();
        return true;
    }

    public void handleCommand(int id) {
        if (id >= Vars.getInstance().TRASH_0) {
            this.vars.removeLine(id - Vars.getInstance().TRASH_0);
        }
        if (id == Vars.getInstance().NEXT_LEVEL) {
            this.vars.nextLevel();
        }
        if (id == Vars.getInstance().TOGGLE_SCREEN_SCROLL) {
            if (this.scrollEnabled) {
                this.scrollEnabled = false;
            } else {
                this.scrollEnabled = true;
            }
        }
        if (id == Vars.getInstance().ZOOM_IN) {
            this.vars.zoomIn();
        }
        if (id == Vars.getInstance().ZOOM_OUT) {
            this.vars.zoomOut();
        }
    }

    public static GameCanvas getInstance() {
        return instance;
    }

    public AlertDialog getLevelSelectionDialog() {
        return this.lvlSelectDlg;
    }

    public void dismissDialogs() {
        this.lvlSelectDlg = null;
    }

    /* access modifiers changed from: package-private */
    public void selectLevelDialog() {
        String[] items = new String[Levels.getInstance().getLevelCount()];
        for (int i = 0; i < Levels.getInstance().getLevelCount(); i++) {
            items[i] = "Level " + (i + 1);
            if (Levels.getInstance().isSolved(i) != 0) {
                items[i] = String.valueOf(items[i]) + ": solved";
            }
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.getInstance());
        builder.setTitle("Select level");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                Vars.getInstance().setLevel(item);
                Vars.getInstance().nextLevel();
                Vars.getInstance().prevLevel();
                GameCanvas.this.lvlSelectDlg.dismiss();
                GameCanvas.this.lvlSelectDlg = null;
            }
        });
        this.lvlSelectDlg = builder.create();
        this.lvlSelectDlg.show();
    }
}
