package softkos.turnmeon;

public class Levels {
    static Levels instance = null;
    int[] solved_levels = new int[1000];

    public int getLevelCount() {
        return 92;
    }

    public Square[] getLevel(int l) {
        if (l >= 0 && l < 4) {
            Vars.getInstance().setBoardSize(6, 9);
        } else if (l >= 4 && l < 42) {
            Vars.getInstance().setBoardSize(7, 10);
        } else if (l >= 42) {
            Vars.getInstance().setBoardSize(8, 12);
        }
        if (l == 0) {
            return new Square[]{new Square(1, 4, 1), new Square(4, 4, 1)};
        } else if (l == 1) {
            return new Square[]{new Square(1, 5, 1), new Square(4, 4, 1), new Square(2, 2, 2)};
        } else if (l == 2) {
            return new Square[]{new Square(3, 2, 2), new Square(1, 6, 2), new Square(4, 6, 2)};
        } else if (l == 3) {
            return new Square[]{new Square(1, 3, 3), new Square(4, 3, 3), new Square(1, 5, 2), new Square(4, 5, 2)};
        } else if (l == 4) {
            return new Square[]{new Square(1, 2, 2), new Square(6, 2, 3), new Square(3, 4, 2), new Square(3, 6, 3)};
        } else if (l == 5) {
            return new Square[]{new Square(3, 2, 3), new Square(1, 5, 3), new Square(5, 5, 3), new Square(3, 7, 3)};
        } else if (l == 6) {
            return new Square[]{new Square(1, 1, 2), new Square(5, 1, 3), new Square(3, 4, 2), new Square(1, 6, 3), new Square(5, 6, 4)};
        } else if (l == 7) {
            return new Square[]{new Square(2, 1, 3), new Square(1, 3, 3), new Square(5, 3, 3), new Square(2, 6, 3), new Square(4, 6, 2)};
        } else if (l == 8) {
            return new Square[]{new Square(3, 1, 4), new Square(1, 3, 4), new Square(5, 3, 3), new Square(2, 6, 2), new Square(4, 6, 3)};
        } else if (l == 9) {
            return new Square[]{new Square(2, 1, 3), new Square(4, 1, 3), new Square(3, 4, 3), new Square(1, 7, 4), new Square(5, 7, 3)};
        } else if (l == 10) {
            return new Square[]{new Square(1, 1, 2), new Square(5, 1, 2), new Square(1, 3, 3), new Square(5, 3, 4), new Square(1, 5, 3), new Square(5, 5, 4)};
        } else if (l == 11) {
            return new Square[]{new Square(2, 1, 3), new Square(4, 1, 3), new Square(1, 3, 2), new Square(5, 3, 4), new Square(2, 5, 2), new Square(4, 5, 4)};
        } else if (l == 12) {
            return new Square[]{new Square(1, 1, 3), new Square(5, 1, 2), new Square(2, 3, 4), new Square(4, 3, 2), new Square(1, 5, 3), new Square(5, 5, 2)};
        } else if (l == 13) {
            return new Square[]{new Square(3, 1, 3), new Square(1, 4, 2), new Square(3, 4, 3), new Square(5, 4, 4), new Square(2, 7, 3), new Square(4, 7, 3)};
        } else if (l == 14) {
            return new Square[]{new Square(1, 2, 2), new Square(5, 2, 2), new Square(3, 4, 4), new Square(1, 6, 4), new Square(5, 6, 4), new Square(3, 7, 2)};
        } else if (l == 15) {
            return new Square[]{new Square(3, 1, 3), new Square(2, 3, 3), new Square(4, 3, 4), new Square(1, 5, 3), new Square(5, 5, 3), new Square(3, 7, 4)};
        } else if (l == 16) {
            return new Square[]{new Square(1, 1, 2), new Square(5, 1, 3), new Square(3, 3, 4), new Square(3, 5, 4), new Square(3, 7, 3), new Square(1, 7, 3), new Square(5, 7, 3)};
        } else if (l == 17) {
            return new Square[]{new Square(1, 1, 2), new Square(5, 1, 2), new Square(2, 3, 3), new Square(4, 3, 3), new Square(3, 5, 2), new Square(1, 6, 3), new Square(5, 6, 3)};
        } else if (l == 18) {
            return new Square[]{new Square(1, 1, 3), new Square(5, 1, 4), new Square(1, 3, 3), new Square(3, 3, 2), new Square(5, 3, 4), new Square(1, 6, 2), new Square(5, 6, 2)};
        } else if (l == 19) {
            return new Square[]{new Square(3, 1, 3), new Square(2, 3, 5), new Square(4, 3, 4), new Square(1, 5, 3), new Square(3, 5, 5), new Square(5, 5, 2), new Square(3, 7, 2)};
        } else if (l == 20) {
            return new Square[]{new Square(3, 1, 2), new Square(2, 3, 2), new Square(4, 3, 4), new Square(1, 5, 2), new Square(5, 5, 4), new Square(2, 7, 4), new Square(4, 7, 2)};
        } else if (l == 21) {
            return new Square[]{new Square(1, 1, 2), new Square(3, 1, 4), new Square(5, 1, 2), new Square(2, 4, 3), new Square(4, 4, 5), new Square(1, 7, 4), new Square(5, 7, 2)};
        } else if (l == 22) {
            return new Square[]{new Square(1, 1, 2), new Square(3, 1, 5), new Square(5, 1, 2), new Square(1, 3, 3), new Square(5, 3, 2), new Square(1, 6, 4), new Square(3, 6, 1), new Square(5, 6, 3)};
        } else if (l == 23) {
            return new Square[]{new Square(3, 1, 3), new Square(2, 3, 1), new Square(4, 3, 4), new Square(1, 5, 4), new Square(5, 5, 3), new Square(1, 7, 3), new Square(5, 7, 3), new Square(3, 8, 3)};
        } else if (l == 24) {
            return new Square[]{new Square(1, 1, 2), new Square(3, 1, 4), new Square(5, 1, 2), new Square(1, 3, 4), new Square(3, 3, 3), new Square(5, 3, 3), new Square(1, 6, 3), new Square(5, 6, 3)};
        } else if (l == 25) {
            return new Square[]{new Square(3, 1, 2), new Square(1, 2, 3), new Square(5, 2, 2), new Square(2, 4, 3), new Square(4, 4, 5), new Square(1, 6, 2), new Square(5, 6, 2), new Square(3, 7, 3)};
        } else if (l == 26) {
            return new Square[]{new Square(2, 1, 3), new Square(4, 1, 5), new Square(1, 3, 5), new Square(5, 3, 3), new Square(1, 5, 2), new Square(5, 5, 1), new Square(2, 7, 4), new Square(4, 7, 3)};
        } else if (l == 27) {
            return new Square[]{new Square(2, 1, 5), new Square(4, 1, 2), new Square(1, 3, 3), new Square(5, 3, 3), new Square(2, 5, 3), new Square(4, 5, 5), new Square(1, 7, 4), new Square(5, 7, 3)};
        } else if (l == 28) {
            return new Square[]{new Square(3, 1, 5), new Square(2, 3, 2), new Square(4, 3, 3), new Square(1, 5, 2), new Square(3, 5, 3), new Square(5, 5, 3), new Square(2, 7, 4), new Square(4, 7, 4)};
        } else if (l == 29) {
            return new Square[]{new Square(1, 1, 2), new Square(3, 1, 2), new Square(5, 1, 3), new Square(1, 3, 3), new Square(3, 3, 4), new Square(5, 3, 4), new Square(1, 6, 4), new Square(3, 6, 4), new Square(5, 6, 2)};
        } else if (l == 30) {
            return new Square[]{new Square(2, 1, 2), new Square(4, 1, 5), new Square(1, 3, 4), new Square(3, 3, 2), new Square(5, 3, 5), new Square(2, 5, 4), new Square(4, 5, 4), new Square(1, 7, 3), new Square(5, 7, 3)};
        } else if (l == 31) {
            return new Square[]{new Square(2, 1, 4), new Square(4, 1, 3), new Square(1, 3, 3), new Square(5, 3, 2), new Square(1, 5, 3), new Square(3, 5, 4), new Square(5, 5, 4), new Square(2, 7, 3), new Square(4, 7, 2)};
        } else if (l == 32) {
            return new Square[]{new Square(2, 1, 3), new Square(4, 1, 3), new Square(1, 3, 3), new Square(3, 3, 6), new Square(5, 3, 4), new Square(2, 5, 3), new Square(4, 5, 4), new Square(1, 7, 3), new Square(5, 7, 3)};
        } else if (l == 33) {
            return new Square[]{new Square(1, 1, 2), new Square(3, 1, 3), new Square(5, 1, 2), new Square(2, 3, 4), new Square(4, 3, 6), new Square(1, 5, 4), new Square(3, 5, 5), new Square(5, 5, 3), new Square(3, 7, 3)};
        } else if (l == 34) {
            return new Square[]{new Square(2, 1, 3), new Square(4, 1, 2), new Square(1, 3, 2), new Square(3, 4, 5), new Square(5, 3, 3), new Square(1, 5, 2), new Square(5, 5, 4), new Square(2, 7, 2), new Square(4, 7, 3)};
        } else if (l == 35) {
            return new Square[]{new Square(2, 1, 3), new Square(4, 1, 4), new Square(1, 3, 3), new Square(3, 3, 4), new Square(5, 3, 3), new Square(1, 5, 3), new Square(3, 5, 3), new Square(5, 5, 2), new Square(2, 7, 5), new Square(4, 7, 2)};
        } else if (l == 36) {
            return new Square[]{new Square(1, 1, 3), new Square(3, 1, 3), new Square(5, 1, 3), new Square(2, 3, 4), new Square(4, 3, 3), new Square(1, 5, 4), new Square(3, 5, 6), new Square(5, 5, 2), new Square(2, 7, 3), new Square(4, 7, 3)};
        } else if (l == 37) {
            return new Square[]{new Square(1, 1, 2), new Square(3, 1, 4), new Square(5, 1, 2), new Square(1, 3, 4), new Square(5, 3, 4), new Square(1, 5, 4), new Square(5, 5, 3), new Square(1, 7, 3), new Square(3, 7, 3), new Square(5, 7, 3)};
        } else if (l == 38) {
            return new Square[]{new Square(2, 1, 3), new Square(4, 1, 3), new Square(1, 3, 4), new Square(5, 3, 2), new Square(3, 4, 4), new Square(1, 5, 2), new Square(5, 5, 4), new Square(3, 6, 5), new Square(1, 7, 2), new Square(5, 7, 3)};
        } else if (l == 39) {
            return new Square[]{new Square(3, 1, 4), new Square(5, 1, 2), new Square(1, 3, 3), new Square(3, 3, 6), new Square(5, 3, 3), new Square(1, 5, 4), new Square(3, 5, 5), new Square(5, 5, 4), new Square(1, 7, 3), new Square(3, 7, 4)};
        } else if (l == 40) {
            return new Square[]{new Square(1, 1, 2), new Square(3, 1, 3), new Square(1, 3, 4), new Square(3, 3, 6), new Square(5, 3, 3), new Square(1, 5, 4), new Square(3, 5, 4), new Square(5, 5, 3), new Square(3, 7, 4), new Square(5, 7, 3)};
        } else if (l == 41) {
            return new Square[]{new Square(1, 1, 3), new Square(3, 1, 4), new Square(5, 1, 2), new Square(1, 3, 4), new Square(3, 3, 4), new Square(5, 3, 2), new Square(1, 5, 3), new Square(3, 5, 4), new Square(5, 5, 5), new Square(2, 7, 3), new Square(4, 7, 2)};
        } else if (l == 42) {
            return new Square[]{new Square(1, 1, 2), new Square(3, 1, 3), new Square(6, 1, 3), new Square(1, 4, 5), new Square(4, 4, 5), new Square(6, 4, 4), new Square(1, 7, 4), new Square(3, 7, 3), new Square(6, 7, 2), new Square(2, 9, 2), new Square(5, 9, 3)};
        } else if (l == 43) {
            return new Square[]{new Square(1, 1, 2), new Square(6, 1, 3), new Square(3, 2, 6), new Square(1, 3, 2), new Square(6, 3, 3), new Square(4, 4, 4), new Square(1, 5, 6), new Square(6, 6, 5), new Square(1, 9, 4), new Square(3, 8, 4), new Square(6, 9, 3)};
        } else if (l == 44) {
            return new Square[]{new Square(1, 1, 3), new Square(3, 1, 5), new Square(6, 1, 2), new Square(1, 4, 7), new Square(4, 4, 6), new Square(6, 4, 2), new Square(1, 7, 3), new Square(3, 7, 6), new Square(6, 7, 5), new Square(2, 9, 4), new Square(5, 9, 3)};
        } else if (l == 45) {
            return new Square[]{new Square(1, 1, 3), new Square(6, 1, 3), new Square(3, 2, 3), new Square(1, 3, 3), new Square(6, 3, 3), new Square(4, 4, 8), new Square(1, 5, 4), new Square(6, 6, 4), new Square(1, 9, 3), new Square(3, 8, 4), new Square(6, 9, 2)};
        } else if (l == 46) {
            return new Square[]{new Square(2, 1, 2), new Square(5, 1, 4), new Square(1, 3, 3), new Square(3, 3, 7), new Square(5, 3, 5), new Square(2, 6, 4), new Square(4, 6, 5), new Square(6, 6, 5), new Square(1, 9, 3), new Square(3, 9, 2), new Square(5, 9, 2)};
        } else if (l == 47) {
            return new Square[]{new Square(1, 1, 2), new Square(3, 1, 5), new Square(6, 1, 4), new Square(1, 4, 5), new Square(4, 4, 6), new Square(6, 4, 4), new Square(1, 7, 3), new Square(3, 7, 6), new Square(6, 7, 3), new Square(2, 9, 3), new Square(5, 9, 3)};
        } else if (l == 48) {
            return new Square[]{new Square(2, 1, 3), new Square(5, 1, 3), new Square(1, 3, 4), new Square(3, 3, 3), new Square(5, 3, 6), new Square(2, 6, 5), new Square(4, 6, 5), new Square(6, 6, 5), new Square(1, 9, 2), new Square(3, 9, 4), new Square(5, 9, 2)};
        } else if (l == 49) {
            return new Square[]{new Square(1, 1, 2), new Square(6, 1, 4), new Square(3, 2, 5), new Square(1, 3, 3), new Square(6, 3, 4), new Square(4, 4, 6), new Square(1, 5, 4), new Square(6, 6, 5), new Square(1, 9, 3), new Square(3, 8, 5), new Square(6, 9, 3)};
        } else if (l == 50) {
            return new Square[]{new Square(2, 1, 5), new Square(5, 1, 3), new Square(1, 3, 4), new Square(3, 3, 4), new Square(5, 3, 4), new Square(2, 6, 5), new Square(4, 6, 5), new Square(6, 6, 5), new Square(1, 9, 3), new Square(3, 9, 3), new Square(5, 9, 3)};
        } else if (l == 51) {
            return new Square[]{new Square(1, 1, 3), new Square(7, 1, 3), new Square(4, 2, 5), new Square(1, 4, 4), new Square(4, 5, 4), new Square(7, 7, 4), new Square(7, 4, 7), new Square(1, 7, 3), new Square(4, 8, 8), new Square(2, 9, 3), new Square(7, 9, 2), new Square(4, 11, 4)};
        } else if (l == 52) {
            return new Square[]{new Square(1, 1, 2), new Square(3, 1, 2), new Square(5, 1, 5), new Square(7, 1, 2), new Square(2, 4, 4), new Square(4, 4, 5), new Square(6, 4, 5), new Square(1, 8, 3), new Square(3, 8, 6), new Square(5, 8, 4), new Square(7, 8, 3), new Square(4, 11, 3)};
        } else if (l == 53) {
            return new Square[]{new Square(1, 1, 2), new Square(7, 1, 4), new Square(4, 2, 4), new Square(1, 4, 5), new Square(4, 5, 4), new Square(7, 7, 5), new Square(7, 4, 4), new Square(1, 7, 4), new Square(4, 8, 5), new Square(2, 9, 4), new Square(7, 9, 3), new Square(4, 11, 4)};
        } else if (l == 54) {
            return new Square[]{new Square(1, 1, 3), new Square(3, 1, 3), new Square(5, 1, 4), new Square(7, 1, 3), new Square(2, 4, 5), new Square(4, 4, 6), new Square(6, 4, 6), new Square(1, 8, 5), new Square(3, 8, 4), new Square(5, 8, 4), new Square(7, 8, 4), new Square(4, 11, 5)};
        } else if (l == 55) {
            return new Square[]{new Square(1, 1, 3), new Square(4, 1, 4), new Square(7, 2, 4), new Square(1, 4, 4), new Square(3, 4, 9), new Square(7, 4, 4), new Square(1, 7, 4), new Square(4, 7, 8), new Square(7, 7, 4), new Square(1, 10, 3), new Square(4, 10, 3), new Square(7, 10, 2)};
        } else if (l == 56) {
            return new Square[]{new Square(1, 1, 2), new Square(7, 1, 2), new Square(4, 2, 4), new Square(1, 4, 4), new Square(4, 5, 7), new Square(7, 7, 3), new Square(7, 4, 3), new Square(1, 7, 2), new Square(4, 8, 7), new Square(2, 9, 3), new Square(7, 9, 3), new Square(4, 11, 4)};
        } else if (l == 57) {
            return new Square[]{new Square(1, 1, 2), new Square(4, 1, 4), new Square(7, 2, 4), new Square(1, 4, 4), new Square(3, 4, 5), new Square(7, 4, 2), new Square(1, 7, 2), new Square(4, 7, 6), new Square(7, 7, 6), new Square(1, 10, 3), new Square(4, 10, 4), new Square(7, 10, 2)};
        } else if (l == 58) {
            return new Square[]{new Square(1, 1, 3), new Square(3, 1, 3), new Square(5, 1, 2), new Square(7, 1, 3), new Square(2, 4, 5), new Square(4, 4, 6), new Square(6, 4, 6), new Square(1, 8, 3), new Square(3, 8, 5), new Square(5, 8, 3), new Square(7, 8, 3), new Square(4, 11, 4)};
        } else if (l == 59) {
            return new Square[]{new Square(1, 1, 2), new Square(4, 1, 5), new Square(7, 2, 3), new Square(1, 4, 4), new Square(3, 4, 4), new Square(7, 4, 3), new Square(1, 7, 4), new Square(4, 7, 5), new Square(7, 7, 7), new Square(1, 10, 3), new Square(4, 10, 3), new Square(7, 10, 3)};
        } else if (l == 60) {
            return new Square[]{new Square(1, 1, 3), new Square(4, 1, 4), new Square(7, 2, 3), new Square(1, 4, 4), new Square(3, 3, 4), new Square(5, 6, 7), new Square(8, 4, 3), new Square(1, 7, 5), new Square(4, 8, 6), new Square(7, 7, 5), new Square(1, 10, 3), new Square(4, 11, 3), new Square(7, 10, 2)};
        } else if (l == 61) {
            return new Square[]{new Square(1, 1, 2), new Square(4, 1, 4), new Square(7, 2, 3), new Square(1, 4, 3), new Square(4, 3, 7), new Square(5, 6, 5), new Square(8, 4, 5), new Square(1, 7, 4), new Square(4, 8, 5), new Square(7, 7, 5), new Square(1, 11, 4), new Square(4, 11, 5), new Square(8, 11, 4)};
        } else if (l == 62) {
            return new Square[]{new Square(1, 1, 2), new Square(4, 1, 4), new Square(7, 2, 4), new Square(1, 4, 3), new Square(3, 3, 6), new Square(5, 6, 6), new Square(8, 4, 4), new Square(1, 7, 4), new Square(4, 8, 6), new Square(7, 7, 5), new Square(1, 10, 3), new Square(4, 11, 4), new Square(7, 10, 3)};
        } else if (l == 63) {
            return new Square[]{new Square(1, 1, 3), new Square(4, 1, 2), new Square(7, 2, 3), new Square(1, 4, 4), new Square(4, 3, 6), new Square(5, 6, 5), new Square(8, 4, 7), new Square(1, 7, 6), new Square(4, 8, 7), new Square(7, 7, 5), new Square(1, 11, 4), new Square(4, 11, 3), new Square(8, 11, 3)};
        } else if (l == 64) {
            return new Square[]{new Square(1, 1, 3), new Square(4, 1, 4), new Square(7, 2, 4), new Square(1, 4, 3), new Square(3, 3, 5), new Square(5, 6, 3), new Square(8, 4, 3), new Square(1, 7, 3), new Square(4, 8, 7), new Square(7, 7, 6), new Square(1, 10, 3), new Square(4, 11, 5), new Square(7, 10, 3)};
        } else if (l == 65) {
            return new Square[]{new Square(1, 1, 2), new Square(4, 1, 4), new Square(7, 2, 3), new Square(1, 4, 6), new Square(4, 3, 8), new Square(5, 6, 3), new Square(8, 4, 5), new Square(1, 7, 4), new Square(4, 8, 4), new Square(7, 7, 6), new Square(1, 11, 2), new Square(4, 11, 5), new Square(8, 11, 4)};
        } else if (l == 66) {
            return new Square[]{new Square(1, 1, 3), new Square(5, 1, 3), new Square(9, 1, 2), new Square(1, 4, 3), new Square(4, 4, 5), new Square(6, 6, 2), new Square(8, 4, 5), new Square(1, 7, 4), new Square(4, 9, 5), new Square(9, 7, 4), new Square(1, 11, 3), new Square(3, 13, 3), new Square(6, 13, 3), new Square(9, 11, 3)};
        } else if (l == 67) {
            return new Square[]{new Square(1, 1, 3), new Square(5, 1, 2), new Square(9, 1, 2), new Square(2, 4, 3), new Square(5, 4, 6), new Square(9, 6, 3), new Square(8, 4, 3), new Square(1, 7, 6), new Square(4, 9, 6), new Square(9, 9, 3), new Square(1, 13, 2), new Square(3, 12, 3), new Square(6, 13, 3), new Square(9, 13, 3)};
        } else if (l == 68) {
            return new Square[]{new Square(1, 1, 2), new Square(5, 1, 4), new Square(9, 1, 4), new Square(1, 4, 3), new Square(4, 4, 6), new Square(6, 6, 4), new Square(8, 4, 4), new Square(1, 7, 2), new Square(4, 9, 5), new Square(9, 7, 5), new Square(1, 11, 3), new Square(3, 13, 2), new Square(6, 13, 3), new Square(9, 11, 3)};
        } else if (l == 69) {
            return new Square[]{new Square(0, 1, 2), new Square(5, 1, 3), new Square(9, 1, 3), new Square(2, 3, 3), new Square(5, 4, 10), new Square(9, 7, 3), new Square(8, 4, 2), new Square(1, 7, 3), new Square(5, 8, 4), new Square(8, 10, 5), new Square(1, 13, 4), new Square(3, 11, 5), new Square(6, 13, 3), new Square(9, 13, 2)};
        } else if (l == 70) {
            return new Square[]{new Square(1, 1, 3), new Square(5, 1, 4), new Square(9, 1, 2), new Square(1, 4, 4), new Square(4, 4, 6), new Square(6, 6, 5), new Square(8, 4, 3), new Square(1, 7, 4), new Square(4, 9, 7), new Square(9, 7, 7), new Square(1, 11, 3), new Square(3, 13, 4), new Square(6, 13, 1), new Square(9, 11, 3)};
        } else if (l == 71) {
            return new Square[]{new Square(1, 1, 5), new Square(5, 1, 2), new Square(9, 1, 5), new Square(2, 4, 3), new Square(5, 4, 2), new Square(9, 6, 4), new Square(8, 4, 1), new Square(1, 7, 4), new Square(4, 9, 4), new Square(9, 9, 5), new Square(1, 13, 2), new Square(3, 12, 7), new Square(6, 13, 3), new Square(9, 13, 3)};
        } else if (l == 72) {
            return new Square[]{new Square(0, 1, 3), new Square(5, 1, 4), new Square(9, 1, 4), new Square(2, 3, 3), new Square(5, 4, 6), new Square(9, 7, 5), new Square(8, 4, 4), new Square(1, 7, 5), new Square(5, 8, 6), new Square(8, 10, 4), new Square(1, 13, 3), new Square(3, 11, 5), new Square(6, 13, 4), new Square(9, 13, 2)};
        } else if (l == 73) {
            return new Square[]{new Square(1, 1, 2), new Square(5, 1, 2), new Square(9, 1, 3), new Square(1, 4, 2), new Square(4, 4, 5), new Square(6, 6, 4), new Square(8, 4, 3), new Square(1, 7, 4), new Square(4, 9, 4), new Square(9, 7, 7), new Square(1, 11, 4), new Square(3, 13, 2), new Square(6, 13, 6), new Square(9, 11, 2)};
        } else if (l == 74) {
            return new Square[]{new Square(1, 1, 3), new Square(5, 1, 3), new Square(9, 1, 3), new Square(2, 4, 1), new Square(5, 4, 8), new Square(9, 6, 5), new Square(8, 4, 1), new Square(1, 7, 5), new Square(4, 9, 8), new Square(9, 9, 3), new Square(1, 13, 4), new Square(3, 12, 3), new Square(6, 13, 4), new Square(9, 13, 3)};
        } else if (l == 75) {
            return new Square[]{new Square(0, 1, 3), new Square(5, 1, 4), new Square(9, 1, 4), new Square(2, 3, 4), new Square(5, 4, 6), new Square(9, 7, 5), new Square(8, 4, 3), new Square(1, 7, 6), new Square(5, 8, 6), new Square(8, 10, 5), new Square(1, 13, 3), new Square(3, 11, 5), new Square(6, 13, 4), new Square(9, 13, 2)};
        } else if (l == 76) {
            return new Square[]{new Square(1, 1, 4), new Square(5, 1, 4), new Square(9, 1, 3), new Square(2, 4, 3), new Square(5, 4, 6), new Square(9, 6, 3), new Square(8, 4, 6), new Square(1, 7, 6), new Square(4, 9, 6), new Square(9, 9, 4), new Square(1, 13, 4), new Square(3, 12, 3), new Square(6, 13, 6), new Square(9, 13, 2)};
        } else if (l == 77) {
            return new Square[]{new Square(0, 1, 2), new Square(5, 1, 5), new Square(9, 1, 3), new Square(2, 3, 4), new Square(5, 4, 5), new Square(9, 7, 4), new Square(8, 4, 4), new Square(1, 7, 5), new Square(5, 8, 5), new Square(8, 10, 6), new Square(1, 13, 3), new Square(3, 11, 4), new Square(6, 13, 5), new Square(9, 13, 3)};
        } else if (l == 78) {
            return new Square[]{new Square(1, 1, 2), new Square(4, 1, 2), new Square(7, 2, 9), new Square(1, 4, 4), new Square(4, 3, 1), new Square(6, 6, 3), new Square(9, 4, 3), new Square(1, 7, 3), new Square(4, 8, 3), new Square(9, 8, 6), new Square(1, 10, 3), new Square(6, 11, 2), new Square(9, 11, 2), new Square(4, 13, 2), new Square(1, 13, 5)};
        } else if (l == 79) {
            return new Square[]{new Square(1, 1, 3), new Square(4, 1, 4), new Square(9, 2, 3), new Square(2, 4, 4), new Square(5, 3, 7), new Square(6, 6, 7), new Square(9, 4, 4), new Square(1, 7, 6), new Square(5, 9, 3), new Square(9, 8, 4), new Square(2, 10, 6), new Square(5, 11, 6), new Square(9, 11, 3), new Square(7, 13, 4), new Square(1, 13, 4)};
        } else if (l == 80) {
            return new Square[]{new Square(1, 1, 2), new Square(4, 1, 3), new Square(7, 2, 4), new Square(1, 4, 5), new Square(4, 3, 4), new Square(6, 6, 4), new Square(9, 4, 4), new Square(1, 7, 4), new Square(4, 8, 11), new Square(9, 8, 5), new Square(1, 10, 4), new Square(6, 11, 5), new Square(9, 11, 3), new Square(4, 13, 3), new Square(1, 13, 3)};
        } else if (l == 81) {
            return new Square[]{new Square(1, 1, 3), new Square(4, 1, 3), new Square(9, 2, 3), new Square(2, 4, 3), new Square(5, 3, 6), new Square(6, 6, 6), new Square(9, 4, 5), new Square(1, 7, 5), new Square(5, 9, 2), new Square(9, 8, 4), new Square(2, 10, 3), new Square(5, 11, 4), new Square(9, 11, 3), new Square(7, 13, 3), new Square(1, 13, 3)};
        } else if (l == 82) {
            return new Square[]{new Square(0, 1, 3), new Square(5, 1, 5), new Square(9, 1, 3), new Square(1, 3, 3), new Square(6, 4, 4), new Square(10, 7, 4), new Square(8, 4, 4), new Square(1, 7, 4), new Square(5, 8, 8), new Square(8, 10, 3), new Square(1, 13, 2), new Square(3, 12, 3), new Square(6, 13, 3), new Square(9, 13, 3)};
        } else if (l == 83) {
            return new Square[]{new Square(0, 1, 3), new Square(4, 0, 5), new Square(7, 1, 3), new Square(9, 0, 3), new Square(-1, 3, 2), new Square(6, 5, 5), new Square(10, 7, 4), new Square(8, 4, 5), new Square(0, 7, 4), new Square(5, 8, 4), new Square(8, 10, 5), new Square(1, 13, 3), new Square(3, 12, 2), new Square(6, 13, 4), new Square(9, 13, 2)};
        } else if (l == 84) {
            return new Square[]{new Square(0, -1, 2), new Square(4, 0, 3), new Square(7, 1, 3), new Square(9, 0, 3), new Square(1, 4, 4), new Square(6, 4, 5), new Square(11, 7, 5), new Square(8, 4, 3), new Square(0, 7, 6), new Square(5, 8, 3), new Square(8, 9, 5), new Square(0, 14, 3), new Square(3, 12, 6), new Square(6, 14, 3), new Square(9, 13, 4)};
        } else if (l == 85) {
            return new Square[]{new Square(-1, 1, 3), new Square(3, 0, 5), new Square(6, -1, 4), new Square(9, 1, 4), new Square(2, 4, 7), new Square(6, 3, 5), new Square(11, 4, 3), new Square(8, 5, 5), new Square(-1, 7, 4), new Square(5, 8, 3), new Square(8, 9, 4), new Square(0, 14, 4), new Square(4, 12, 2), new Square(7, 14, 4), new Square(10, 11, 3)};
        } else if (l == 86) {
            return new Square[]{new Square(1, 1, 1), new Square(2, -1, 5), new Square(4, 0, 3), new Square(7, 1, 5), new Square(0, 5, 2), new Square(7, 3, 2), new Square(5, 4, 5), new Square(10, 5, 4), new Square(1, 7, 5), new Square(5, 9, 8), new Square(9, 8, 4), new Square(0, 14, 5), new Square(4, 13, 3), new Square(7, 15, 2), new Square(9, 14, 4)};
        } else if (l == 87) {
            return new Square[]{new Square(-1, 1, 2), new Square(3, 0, 4), new Square(6, -1, 3), new Square(9, 1, 6), new Square(2, 4, 7), new Square(6, 3, 4), new Square(11, 4, 4), new Square(8, 5, 5), new Square(-1, 7, 3), new Square(5, 8, 5), new Square(8, 9, 5), new Square(0, 14, 3), new Square(4, 12, 5), new Square(7, 14, 3), new Square(10, 11, 5)};
        } else if (l == 88) {
            return new Square[]{new Square(1, 1, 5), new Square(2, -1, 3), new Square(4, 0, 3), new Square(7, 1, 4), new Square(0, 5, 4), new Square(7, 3, 4), new Square(5, 4, 3), new Square(10, 5, 6), new Square(1, 7, 4), new Square(5, 9, 1), new Square(9, 8, 3), new Square(0, 14, 5), new Square(4, 13, 4), new Square(7, 15, 4), new Square(9, 14, 5)};
        } else if (l == 89) {
            return new Square[]{new Square(-1, -1, 4), new Square(2, 0, 2), new Square(4, 1, 5), new Square(9, -1, 5), new Square(0, 4, 4), new Square(7, 4, 3), new Square(5, 5, 7), new Square(9, 4, 4), new Square(1, 8, 5), new Square(5, 9, 4), new Square(9, 9, 6), new Square(12, 8, 3), new Square(0, 11, 4), new Square(4, 12, 4), new Square(7, 13, 3), new Square(9, 12, 3)};
        } else if (l == 90) {
            return new Square[]{new Square(1, 1, 3), new Square(2, -1, 2), new Square(4, 0, 2), new Square(7, 1, 2), new Square(0, 5, 2), new Square(7, 3, 4), new Square(5, 4, 4), new Square(10, 5, 4), new Square(1, 7, 4), new Square(5, 9, 6), new Square(9, 8, 3), new Square(13, 9, 4), new Square(0, 14, 2), new Square(4, 13, 1), new Square(7, 15, 3), new Square(9, 14, 2)};
        } else if (l == 91) {
            return new Square[]{new Square(-1, -1, 2), new Square(2, 0, 5), new Square(4, 1, 4), new Square(9, -1, 4), new Square(0, 4, 4), new Square(7, 4, 3), new Square(5, 5, 3), new Square(9, 4, 5), new Square(1, 8, 2), new Square(5, 9, 4), new Square(9, 9, 1), new Square(12, 8, 6), new Square(0, 11, 4), new Square(4, 12, 3), new Square(7, 13, 4), new Square(9, 12, 2)};
        } else if (l != 92) {
            return null;
        } else {
            return new Square[]{new Square(1, 1, 3), new Square(2, -1, 3), new Square(4, 0, 2), new Square(7, 1, 6), new Square(0, 5, 6), new Square(7, 3, 5), new Square(5, 4, 5), new Square(10, 5, 3), new Square(1, 7, 4), new Square(5, 9, 8), new Square(9, 8, 4), new Square(13, 9, 4), new Square(0, 14, 4), new Square(4, 13, 5), new Square(7, 15, 3), new Square(9, 14, 3)};
        }
    }

    public Levels() {
        for (int i = 0; i < this.solved_levels.length; i++) {
            this.solved_levels[i] = 0;
        }
    }

    public void resetLevel(int l) {
        this.solved_levels[l] = 0;
    }

    public void setSolved(int i) {
        this.solved_levels[i] = 1;
    }

    public int isSolved(int i) {
        return this.solved_levels[i];
    }

    public void resetLevels() {
        for (int i = 0; i < this.solved_levels.length; i++) {
            this.solved_levels[i] = 0;
        }
    }

    public static Levels getInstance() {
        if (instance == null) {
            instance = new Levels();
        }
        return instance;
    }
}
