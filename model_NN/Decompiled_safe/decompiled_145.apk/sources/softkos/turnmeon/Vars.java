package softkos.turnmeon;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;

public class Vars {
    static Vars instance = null;
    public int GO_TO_MOBILSOFT;
    public int HOWTOPLAY;
    public int NEXT_LEVEL;
    public int OTHER_APP;
    public int START_GAME;
    public int TOGGLE_SCREEN_SCROLL;
    public int TRASH_0;
    public int TWITTER;
    public int ZOOM_IN;
    public int ZOOM_OUT;
    int[][] board;
    int board_h;
    int board_w;
    ArrayList<gButton> buttonList;
    public GameState gameState;
    int level;
    ArrayList<Line> lineList;
    public int painting;
    Random random;
    int scaleFactor;
    int screenHeight;
    int screenWidth;
    int scrollStartX;
    int scrollStartY;
    ArrayList<Square> squareList;
    Timer updateTimer;
    int vpx;
    int vpy;
    double zoomLevel;

    public enum GameState {
        Menu,
        Game,
        Highscores,
        HowPlay
    }

    public int getScaleFactor() {
        return this.scaleFactor;
    }

    public void resetVp() {
        this.vpx = 0;
        this.vpy = 0;
    }

    public int getBoardW() {
        return this.board_w;
    }

    public int getBoardH() {
        return this.board_h;
    }

    public void setBoardSize(int w, int h) {
        this.board_w = w;
        this.board_h = h;
        this.board = (int[][]) Array.newInstance(Integer.TYPE, w, h);
    }

    public Vars() {
        this.scaleFactor = 2;
        this.painting = 0;
        this.level = 0;
        this.board_w = 7;
        this.board_h = 10;
        this.buttonList = null;
        this.scrollStartX = -1;
        this.scrollStartY = -1;
        this.squareList = null;
        this.lineList = null;
        this.zoomLevel = 100.0d;
        this.START_GAME = 100;
        this.GO_TO_MOBILSOFT = 101;
        this.HOWTOPLAY = 102;
        this.TWITTER = 103;
        this.OTHER_APP = 104;
        this.NEXT_LEVEL = 200;
        this.TOGGLE_SCREEN_SCROLL = 201;
        this.ZOOM_IN = 202;
        this.ZOOM_OUT = 203;
        this.TRASH_0 = 1000;
        this.buttonList = new ArrayList<>();
        this.buttonList.clear();
        this.squareList = new ArrayList<>();
        this.squareList.clear();
        this.lineList = new ArrayList<>();
        this.lineList.clear();
        this.random = new Random();
        this.updateTimer = new Timer();
        this.updateTimer.schedule(new UpdateTimer(), 10, 40);
        this.level = 0;
        this.zoomLevel = 100.0d;
    }

    public void advanceFrame() {
        GameState gameState2 = GameState.Game;
        if (this.gameState == GameState.Menu) {
            for (int i = 0; i < this.buttonList.size(); i++) {
                if (getButton(i).canMove()) {
                    getButton(i).update();
                }
            }
        }
    }

    public int getSquareAt(int x, int y) {
        for (int i = 0; i < this.squareList.size(); i++) {
            Square s = getSquare(i);
            int w = s.getWidth();
            int h = s.getHeight();
            if (x >= ((int) (((double) (s.getPx() - (w / 4))) * getZoomLevel())) - this.vpx && x <= ((int) (((double) ((s.getPx() + w) + (w / 2))) * getZoomLevel())) - this.vpx && y >= ((int) (((double) (s.getPy() - (h / 4))) * getZoomLevel())) - this.vpy && y <= ((int) (((double) ((s.getPy() + h) + (h / 2))) * getZoomLevel())) - this.vpy) {
                return i;
            }
        }
        return -1;
    }

    public void hideAllButtons() {
        for (int i = 0; i < this.buttonList.size(); i++) {
            getButton(i).hide();
        }
    }

    public void updateSquares() {
        for (int i = 0; i < this.squareList.size(); i++) {
            getSquare(i).setCurrVal(0);
        }
        for (int i2 = 0; i2 < this.lineList.size(); i2++) {
            if (getLine(i2).visible) {
                int i1 = getLine(i2).start;
                int i22 = getLine(i2).end;
                getSquare(i1).setCurrVal(getSquare(i1).getCurrVal() + 1);
                getSquare(i22).setCurrVal(getSquare(i22).getCurrVal() + 1);
            }
        }
    }

    public boolean isEndLevel() {
        int solved = Levels.getInstance().isSolved(getLevel());
        if (solved != 0) {
            return true;
        }
        for (int i = 0; i < this.squareList.size(); i++) {
            if (getSquare(i).getValue() != getSquare(i).getCurrVal()) {
                return false;
            }
        }
        if (solved == 0) {
            removeTrashButtons();
            Levels.getInstance().setSolved(getLevel());
            FileWR.getInstance().saveGame();
        }
        return true;
    }

    public void addLine(int sx, int sy, int ex, int ey) {
        int si = getSquareAt(sx, sy);
        int ei = getSquareAt(ex, ey);
        if (si >= 0 && ei >= 0 && si != ei) {
            for (int i = 0; i < this.lineList.size(); i++) {
                if (getLine(i).visible) {
                    if (getLine(i).start != si || getLine(i).end != ei) {
                        if (getLine(i).end == si && getLine(i).start == ei) {
                            return;
                        }
                    } else {
                        return;
                    }
                }
            }
            Line l = new Line();
            l.start = si;
            l.end = ei;
            this.lineList.add(l);
            gButton trashBtn = new gButton();
            int btn_size = this.screenWidth / 10;
            trashBtn.setSize(btn_size, btn_size);
            int w = getSquare(si).getWidth();
            int h = getSquare(si).getHeight();
            int x = (((((getSquare(si).getPx() + l.xoff1) + getSquare(ei).getPx()) + l.xoff2) + w) / 2) - (btn_size / 2);
            int y = (((((getSquare(si).getPy() + l.yoff1) + getSquare(ei).getPy()) + l.yoff2) + h) / 2) - (btn_size / 2);
            trashBtn.setPosition(x, y);
            trashBtn.setOriginPos(x, y);
            trashBtn.show();
            trashBtn.setId((getInstance().TRASH_0 + this.lineList.size()) - 1);
            trashBtn.setBackImages("trash_off", "trash_on");
            this.buttonList.add(trashBtn);
            updateSquares();
            updateTrashButtons();
            String s = "Square s[] = {";
            for (int i2 = 0; i2 < this.squareList.size(); i2++) {
                s = String.valueOf(s) + "new Square(" + getSquare(i2).getBoardX() + "," + getSquare(i2).getBoardY() + "," + getSquare(i2).getCurrVal() + "), ";
            }
            System.out.println("Level = " + (String.valueOf(s) + "};"));
        }
    }

    public void removeLine(int l) {
        int id;
        int i = 0;
        while (true) {
            if (i >= this.buttonList.size()) {
                break;
            } else if (getButton(i).isVisible() && (id = getButton(i).getId()) >= this.TRASH_0 && id - this.TRASH_0 == l) {
                getButton(i).hide();
                break;
            } else {
                i++;
            }
        }
        getLine(l).hide();
        updateSquares();
    }

    public void initLevel() {
        resetVp();
        this.squareList.clear();
        this.lineList.clear();
        Square[] s = Levels.getInstance().getLevel(this.level);
        int cell_w = this.screenWidth / this.board_w;
        int cell_h = this.screenHeight / this.board_h;
        int square_w = cell_w;
        int square_h = cell_w;
        for (int i = 0; i < s.length; i++) {
            s[i].SetPos(s[i].getBoardX() * cell_w, s[i].getBoardY() * cell_h);
            s[i].setSize(square_w, square_h);
            this.squareList.add(s[i]);
        }
        removeTrashButtons();
    }

    public int getVpx() {
        return this.vpx;
    }

    public int getVpy() {
        return this.vpy;
    }

    public void startScroll(int x, int y) {
        if (getSquareAt(x, y) < 0) {
            this.scrollStartX = x;
            this.scrollStartY = y;
        }
    }

    public void scroll(int x, int y) {
        if (this.scrollStartX >= 0) {
            this.vpx -= x - this.scrollStartX;
            this.vpy -= y - this.scrollStartY;
            this.scrollStartX = x;
            this.scrollStartY = y;
            updateTrashButtons();
        }
    }

    public void stopScroll(int x, int y) {
        this.scrollStartX = -1;
        this.scrollStartY = -1;
    }

    public void removeTrashButtons() {
        int i = 0;
        while (i < this.buttonList.size()) {
            if (getButton(i).getId() >= this.TRASH_0) {
                this.buttonList.remove(i);
                i = 0;
            }
            i++;
        }
    }

    public void updateTrashButtons() {
        for (int i = 0; i < this.buttonList.size(); i++) {
            if (getButton(i).getId() >= this.TRASH_0) {
                this.buttonList.get(i).SetPos(((int) (((double) this.buttonList.get(i).getOriginX()) * getZoomLevel())) - this.vpx, ((int) (((double) this.buttonList.get(i).getOriginY()) * getZoomLevel())) - this.vpy);
            }
        }
    }

    public double dist(int x1, int y1, int x2, int y2) {
        return Math.sqrt((double) (((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2))));
    }

    public void setLevel(int l) {
        this.level = l;
    }

    public void nextLevel() {
        this.level++;
        if (this.level >= Levels.getInstance().getLevelCount()) {
            this.level = Levels.getInstance().getLevelCount() - 1;
        }
        initLevel();
    }

    public void prevLevel() {
        this.level--;
        if (this.level < 0) {
            this.level = 0;
        }
        initLevel();
    }

    public void resetLevel() {
        initLevel();
        Levels.getInstance().resetLevel(getLevel());
    }

    public int getLevel() {
        return this.level;
    }

    public void newGame() {
        this.level = 0;
        FileWR.getInstance().loadGame();
        int i = 0;
        while (true) {
            if (i >= Levels.getInstance().getLevelCount()) {
                break;
            } else if (Levels.getInstance().isSolved(i) == 0) {
                setLevel(i);
                break;
            } else {
                i++;
            }
        }
        initLevel();
    }

    public int getButtonListSize() {
        return this.buttonList.size();
    }

    public void addButton(gButton b) {
        this.buttonList.add(b);
    }

    public ArrayList<gButton> getButtonList() {
        return this.buttonList;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public gButton getButton(int b) {
        if (b < 0 || b >= this.buttonList.size()) {
            return null;
        }
        return this.buttonList.get(b);
    }

    public int getScreenWidth() {
        return this.screenWidth;
    }

    public int getScreenHeight() {
        return this.screenHeight;
    }

    public void init() {
        this.random = new Random();
    }

    public int GetNextInt(int max) {
        if (max == 0) {
            max = 1;
        }
        return Math.abs(this.random.nextInt() % max);
    }

    public void screenSize(int w, int h) {
        this.screenWidth = w;
        this.screenHeight = h;
        if (w > 350) {
            this.scaleFactor = 3;
        } else if (w < 260) {
            this.scaleFactor = 1;
        } else {
            this.scaleFactor = 2;
        }
    }

    public static Vars getInstance() {
        if (instance == null) {
            instance = new Vars();
            instance.init();
        }
        return instance;
    }

    public int nextInt(int max) {
        if (max < 1) {
            return 0;
        }
        return this.random.nextInt(max);
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public Square getSquare(int i) {
        if (i < 0 || i >= this.squareList.size()) {
            return null;
        }
        return this.squareList.get(i);
    }

    public ArrayList<Square> getSquareList() {
        return this.squareList;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public Line getLine(int i) {
        if (i < 0 || i >= this.lineList.size()) {
            return null;
        }
        return this.lineList.get(i);
    }

    public void zoomIn() {
        this.zoomLevel += 10.0d;
        if (this.zoomLevel > 120.0d) {
            this.zoomLevel = 120.0d;
        }
        updateTrashButtons();
    }

    public void zoomOut() {
        this.zoomLevel -= 10.0d;
        if (this.zoomLevel < 40.0d) {
            this.zoomLevel = 40.0d;
        }
        updateTrashButtons();
    }

    public double getZoomLevel() {
        return this.zoomLevel / 100.0d;
    }

    public ArrayList<Line> getLineList() {
        return this.lineList;
    }
}
