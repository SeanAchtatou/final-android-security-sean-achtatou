package softkos.turnmeon;

import java.util.TimerTask;
import softkos.turnmeon.Vars;

public class UpdateTimer extends TimerTask {
    public void run() {
        Vars vars = Vars.getInstance();
        if (vars.gameState == Vars.GameState.Game) {
            Vars.getInstance().advanceFrame();
            if (GameCanvas.getInstance() != null) {
                GameCanvas.getInstance().postInvalidate();
            }
        }
        if (vars.gameState == Vars.GameState.Menu) {
            vars.advanceFrame();
            if (MenuCanvas.getInstance() != null) {
                MenuCanvas.getInstance().postInvalidate();
            }
        }
    }
}
