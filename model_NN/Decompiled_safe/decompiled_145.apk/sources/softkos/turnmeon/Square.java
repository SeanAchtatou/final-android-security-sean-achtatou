package softkos.turnmeon;

public class Square extends GameObject {
    int board_x;
    int board_y;
    int curr_value;
    int value;

    public int getValue() {
        return this.value;
    }

    public int getCurrVal() {
        return this.curr_value;
    }

    public void setCurrVal(int v) {
        this.curr_value = v;
    }

    public int getBoardX() {
        return this.board_x;
    }

    public int getBoardY() {
        return this.board_y;
    }

    public Square(int x, int y, int v) {
        this.board_x = x;
        this.board_y = y;
        this.value = v;
    }
}
