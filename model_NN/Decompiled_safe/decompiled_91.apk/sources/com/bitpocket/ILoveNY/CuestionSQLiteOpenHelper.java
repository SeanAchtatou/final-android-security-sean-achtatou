package com.bitpocket.ILoveNY;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.bitpocket.ILoveNY.PregResp.Pregunta;
import com.bitpocket.ILoveNY.PregResp.Respuesta;

public class CuestionSQLiteOpenHelper extends SQLiteOpenHelper {
    private static final String CLASSTAG = CuestionSQLiteOpenHelper.class.getSimpleName();
    public static final String DB_NAME = "cuestions_db.sqlite";
    public static final String POINTS = "points";
    public static final String POINTS_TABLE = "points";
    public static final String PREGUNTAS_TABLE = "preguntas";
    public static final String PREGUNTA_CATEGORIA = "categoria";
    public static final String PREGUNTA_CREATOR = "creator";
    public static final String PREGUNTA_ID = "idPregunta";
    public static final String PREGUNTA_IMAGEN = "img";
    public static final String PREGUNTA_NIVEL = "nivel";
    public static final String PREGUNTA_SHOWN = "isShown";
    public static final String PREGUNTA_TEXTO = "txt";
    public static final String RESPUESTAS_TABLE = "respuestas";
    public static final String RESPUESTA_ID = "idRespuesta";
    public static final String RESPUESTA_ID_PREGUNTA = "idPregunta";
    public static final String RESPUESTA_PUNTUACION = "puntuacion";
    public static final String RESPUESTA_TEXTO = "txt";
    public static final int VERSION = 13;
    private static Context ctxt;

    public CuestionSQLiteOpenHelper(Context context) {
        super(context, DB_NAME, (SQLiteDatabase.CursorFactory) null, 13);
        ctxt = context;
    }

    public void onCreate(SQLiteDatabase db) {
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " onCreate");
        clearDatabase(db);
        createTables(db);
        initPoints(db, 0);
        addQuestionsToDB(db);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " onUpgrade old:" + oldVersion + " - new:" + newVersion);
        onCreate(db);
    }

    private void clearDatabase(SQLiteDatabase db) {
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " clearDatabase");
        db.execSQL("DROP TABLE IF EXISTS preguntas");
        db.execSQL("DROP TABLE IF EXISTS respuestas");
        db.execSQL("DROP TABLE IF EXISTS points");
    }

    private void createTables(SQLiteDatabase db) {
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " createTables");
        db.execSQL("create table preguntas (idPregunta integer primary key not null,nivel integer,img text,txt text,categoria text,creator text,isShown integer default 0);");
        db.execSQL("create table respuestas (idRespuesta integer primary key,idPregunta integer not null,txt text,puntuacion integer);");
        db.execSQL("create table points (points integer );");
    }

    public void initPoints(SQLiteDatabase db, int value) {
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " init points");
        db.delete("points", null, null);
        ContentValues values = new ContentValues();
        values.put("points", Integer.valueOf(value));
        db.insert("points", null, values);
    }

    private String getStringResourceByName(String aString) {
        return ctxt.getResources().getString(ctxt.getResources().getIdentifier(aString, "string", ctxt.getPackageName()));
    }

    private void addQuestionsToDB(SQLiteDatabase db) {
        addQuestionsToDB(db, 0);
    }

    private void addQuestionsToDB(SQLiteDatabase db, int start) {
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " addQuestionsToDB " + start);
        for (long i = (long) start; i <= 52; i++) {
            addQuestion(db, new Pregunta(i, i, getStringResourceByName(PREGUNTA_CATEGORIA + (1 + i)), getStringResourceByName("imagen" + (1 + i)), getStringResourceByName("preg" + (1 + i)), getStringResourceByName(PREGUNTA_CREATOR + (1 + i)), 0));
            Log.v(Constants.LOGTAG, " " + CLASSTAG + " addedQuestionsToDB " + i);
            for (Integer j = 1; j.intValue() <= 4; j = Integer.valueOf(j.intValue() + 1)) {
                addRespuesta(db, new Respuesta(i, getStringResourceByName("resp" + (1 + i) + j), getStringResourceByName(new StringBuilder("correcta").append(1 + i).toString()).equals(j.toString()) ? 5 : 0));
            }
        }
    }

    private void addQuestion(SQLiteDatabase db, Pregunta p) {
        ContentValues values = new ContentValues();
        values.put("idPregunta", Long.valueOf(p.getId()));
        values.put(PREGUNTA_CATEGORIA, p.getCategoria());
        values.put(PREGUNTA_IMAGEN, p.getImg_ref());
        values.put("txt", p.getTexto());
        values.put(PREGUNTA_SHOWN, Integer.valueOf(p.getIsShown()));
        values.put(PREGUNTA_CREATOR, p.getCreator());
        db.insert(PREGUNTAS_TABLE, null, values);
    }

    private void addRespuesta(SQLiteDatabase db, Respuesta r) {
        ContentValues values = new ContentValues();
        values.putNull(RESPUESTA_ID);
        values.put("idPregunta", Long.valueOf(r.getIdPregunta()));
        values.put(RESPUESTA_PUNTUACION, Integer.valueOf(r.getPuntuacion()));
        values.put("txt", r.getTexto());
        db.insert(RESPUESTAS_TABLE, null, values);
    }
}
