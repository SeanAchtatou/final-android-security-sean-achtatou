package com.bitpocket.ILoveNY.Activities;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.agi.fbsdk.AsyncFacebookRunner;
import com.agi.fbsdk.Facebook;
import com.agi.fbsdk.FacebookError;
import com.bitpocket.ILoveNY.Constants;
import com.bitpocket.ILoveNY.CuestionSQLiteOpenHelper;
import com.bitpocket.ILoveNY.Facebook.BaseDialogListener;
import com.bitpocket.ILoveNY.Facebook.BaseRequestListener;
import com.bitpocket.ILoveNY.Facebook.LoginButton;
import com.bitpocket.ILoveNY.Facebook.SessionEvents;
import com.bitpocket.ILoveNY.Facebook.SessionStore;
import com.bitpocket.ILoveNY.ILoveApplication;
import com.bitpocket.ILoveNY.PregResp.Pregunta;
import com.bitpocket.ILoveNY.R;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mobclix.android.sdk.MobclixAdView;
import com.mobclix.android.sdk.MobclixAdViewListener;
import com.mobclix.android.sdk.MobclixMMABannerXLAdView;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

public class end extends ListActivity implements MobclixAdViewListener {
    /* access modifiers changed from: private */
    public static final String CLASSTAG = end.class.getSimpleName();
    private static final int MORE_APPS = 3;
    private static final int PLAY_DELETE = 1;
    private static final int QUIT = 4;
    private static final int SEND = 2;
    private static final int SENT_EMAIL = 1;
    private String[] answers;
    /* access modifiers changed from: private */
    public ILoveApplication app;
    private int[] arrayRespuestas = new int[53];
    private ImageButton btnPlay;
    private int count = 0;
    private String[] finalAnswers;
    /* access modifiers changed from: private */
    public AsyncFacebookRunner mAsyncRunner;
    /* access modifiers changed from: private */
    public Facebook mFacebook;
    private LoginButton mLoginButton;
    /* access modifiers changed from: private */
    public Button mPostButton;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog;
    /* access modifiers changed from: private */
    public int puntuacion = 0;
    /* access modifiers changed from: private */
    public int puntuacionGame = 0;
    /* access modifiers changed from: private */
    public String[] questions;
    /* access modifiers changed from: private */
    public GoogleAnalyticsTracker tracker;
    private TextView tvPlay;
    private TextView tvRespuestas;
    private TextView tvTitre;

    public void onCreate(Bundle savedInstanceState) {
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " onCreate");
        super.onCreate(savedInstanceState);
        if (getWindow().getWindowManager().getDefaultDisplay().getHeight() <= 320) {
            requestWindowFeature(1);
            setContentView((int) R.layout.end);
        } else {
            requestWindowFeature(4);
            setContentView((int) R.layout.end);
            setFeatureDrawableResource(4, R.drawable.icon);
        }
        this.tracker = GoogleAnalyticsTracker.getInstance();
        this.tracker.start(getString(R.string.analytics), this);
        this.tracker.trackPageView("/end");
        ((MobclixMMABannerXLAdView) findViewById(R.id.advertising_banner_view)).addMobclixAdViewListener(this);
        Bundle extras = getIntent().getExtras();
        this.arrayRespuestas = (int[]) extras.get(CuestionSQLiteOpenHelper.RESPUESTAS_TABLE);
        this.count = extras.getInt("count", 5);
        setUpViews();
        this.puntuacion = this.app.loadPoints();
        if (this.count > 0) {
            checkAnswers();
        }
        if (this.puntuacion + this.puntuacionGame == 265) {
            new AlertDialog.Builder(this).setTitle(getString(R.string.version)).setIcon(17301659).setMessage(getResources().getString(R.string.end)).setPositiveButton((int) R.string.why_not, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    end.this.startActivityForResult(new Intent(end.this, send.class), 1);
                }
            }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            }).create().show();
        }
        int porcentaje = ((this.puntuacion + this.puntuacionGame) * 100) / Constants.MAX_POINTS;
        Log.d(Constants.LOGTAG, " " + CLASSTAG + " Porcentaje: " + porcentaje);
        if (porcentaje <= 20) {
            this.tvTitre.setText(getText(R.string.tourist).toString());
        } else if (porcentaje <= 65) {
            this.tvTitre.setText(getText(R.string.citoyen).toString());
        } else if (porcentaje <= 95) {
            this.tvTitre.setText(getText(R.string.taxidriver).toString());
        } else {
            this.tvTitre.setText(getText(R.string.mayor).toString());
        }
        this.tvRespuestas.setText(String.valueOf(getText(R.string.mypoints).toString()) + " " + (this.puntuacion + this.puntuacionGame) + "/" + ((int) Constants.MAX_POINTS));
    }

    private void checkAnswers() {
        int j = 0;
        int top = this.count;
        this.answers = new String[top];
        this.questions = new String[top];
        new ArrayList();
        List<Pregunta> currentQuestions = this.app.getCurrentQuestions();
        for (int i = 0; i < top; i++) {
            int id = (int) currentQuestions.get(i).getId();
            if (currentQuestions.get(id).getIsShown() > 0) {
                top++;
                if (i == 52) {
                    break;
                }
            } else {
                int points = currentQuestions.get(id).getRespuestas().get(this.arrayRespuestas[id]).getPuntuacion();
                this.answers[j] = String.valueOf(getResources().getString(R.string.question)) + " " + (j + 1) + ": " + points + " " + getResources().getString(R.string.questionpoints);
                if (points > 0) {
                    this.questions[j] = String.valueOf(currentQuestions.get(id).getTexto()) + ": " + currentQuestions.get(id).getRespuestas().get(this.arrayRespuestas[id]).getTexto();
                    this.app.showPregunta((int) currentQuestions.get(id).getId_db(), true);
                    this.puntuacionGame += points;
                } else {
                    this.questions[j] = currentQuestions.get(id).getTexto();
                }
                j++;
            }
        }
        this.finalAnswers = new String[j];
        for (int i2 = 0; i2 < j; i2++) {
            this.finalAnswers[i2] = this.answers[i2];
        }
        this.answers = null;
        setListAdapter(new ArrayAdapter(this, (int) R.layout.list_item, this.finalAnswers));
        ListView lv = getListView();
        if (getWindow().getWindowManager().getDefaultDisplay().getHeight() <= 450) {
            lv.setVisibility(8);
        }
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Toast.makeText(end.this.getApplicationContext(), end.this.questions[position], 0).show();
            }
        });
        if (this.puntuacionGame > 0) {
            this.app.savePoints(this.puntuacion + this.puntuacionGame);
        }
    }

    private void setUpViews() {
        int i;
        this.app = (ILoveApplication) getApplication();
        this.tvTitre = (TextView) findViewById(R.id.tvTitre);
        this.tvRespuestas = (TextView) findViewById(R.id.tvRespuestas);
        this.btnPlay = (ImageButton) findViewById(R.id.btnPlay);
        this.tvPlay = (TextView) findViewById(R.id.tvPlay);
        this.mLoginButton = (LoginButton) findViewById(R.id.login);
        this.mPostButton = (Button) findViewById(R.id.postButton);
        this.btnPlay.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                end.this.startActivity(new Intent(end.this, home.class));
                end.this.finish();
            }
        });
        if (this.count == 0) {
            this.btnPlay.setVisibility(8);
            this.tvPlay.setVisibility(8);
        } else {
            this.btnPlay.setVisibility(0);
            this.tvPlay.setVisibility(0);
        }
        this.mFacebook = new Facebook(Constants.ID);
        SessionStore.restore(this.mFacebook, this);
        SessionEvents.addAuthListener(new SampleAuthListener());
        SessionEvents.addLogoutListener(new SampleLogoutListener());
        this.mLoginButton.init(this, this.mFacebook, Constants.PERMISSIONS);
        this.mAsyncRunner = new AsyncFacebookRunner(this.mFacebook);
        this.mPostButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String message;
                end.this.progressDialog = ProgressDialog.show(end.this, "Connecting to Facebook", "Please wait");
                Bundle params = new Bundle();
                int porcentaje = ((end.this.puntuacion + end.this.puntuacionGame) * 100) / Constants.MAX_POINTS;
                Log.d(Constants.LOGTAG, " " + end.CLASSTAG + " Porcentaje: " + porcentaje);
                if (porcentaje <= 20) {
                    message = String.valueOf(end.this.getText(R.string.tourist_long).toString()) + " " + end.this.getText(R.string.mypoints).toString() + (end.this.puntuacion + end.this.puntuacionGame) + "/" + ((int) Constants.MAX_POINTS);
                } else if (porcentaje <= 65) {
                    message = String.valueOf(end.this.getText(R.string.citoyen_long).toString()) + " " + end.this.getText(R.string.mypoints).toString() + (end.this.puntuacion + end.this.puntuacionGame) + "/" + ((int) Constants.MAX_POINTS);
                } else if (porcentaje <= 95) {
                    message = String.valueOf(end.this.getText(R.string.taxidriver_long).toString()) + " " + end.this.getText(R.string.mypoints).toString() + (end.this.puntuacion + end.this.puntuacionGame) + "/" + ((int) Constants.MAX_POINTS);
                } else {
                    message = String.valueOf(end.this.getText(R.string.mayor_long).toString()) + " " + end.this.getText(R.string.mypoints).toString() + (end.this.puntuacion + end.this.puntuacionGame) + "/" + ((int) Constants.MAX_POINTS);
                }
                params.putString("message", message);
                params.putString("name", "I love NY for Android");
                try {
                    params.putString("picture", "http://1.bp.blogspot.com/_-JrJdHsCm8U/TBQADN7qsYI/AAAAAAAAAAM/-EARBsf15wc/s200/ilnytest8.png");
                    params.putString("link", "http://ilvandroid.blogspot.com/");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                end.this.mAsyncRunner.request("me/feed", params, new SampleRequestListener(end.this, null));
            }
        });
        Button button = this.mPostButton;
        if (this.mFacebook.isSessionValid()) {
            i = 0;
        } else {
            i = 4;
        }
        button.setVisibility(i);
    }

    private class SampleRequestListener extends BaseRequestListener {
        private SampleRequestListener() {
        }

        /* synthetic */ SampleRequestListener(end end, SampleRequestListener sampleRequestListener) {
            this();
        }

        public void onFacebookError(FacebookError e, Object state) {
            super.onFacebookError(e);
            end.this.runOnUiThread(new Runnable() {
                public void run() {
                    end.this.progressDialog.dismiss();
                    Toast.makeText(end.this, "Error. Message has not been published.", 1).show();
                    try {
                        end.this.mFacebook.logout(end.this);
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e2) {
                        e2.printStackTrace();
                    }
                }
            });
        }

        public void onComplete(String response, Object state) {
            end.this.runOnUiThread(new Runnable() {
                public void run() {
                    end.this.progressDialog.dismiss();
                    end.this.tracker.trackPageView("/fb_post");
                    Toast.makeText(end.this, "Message has been published on your wall.", 1).show();
                }
            });
        }

        public void onFileNotFoundException(FileNotFoundException e, Object state) {
        }

        public void onIOException(IOException e, Object state) {
        }

        public void onMalformedURLException(MalformedURLException e, Object state) {
        }
    }

    public class SampleAuthListener implements SessionEvents.AuthListener {
        public SampleAuthListener() {
        }

        public void onAuthSucceed() {
            end.this.tracker.trackPageView("/fb_login");
            end.this.mPostButton.setVisibility(0);
        }

        public void onAuthFail(String error) {
        }
    }

    public class SampleLogoutListener implements SessionEvents.LogoutListener {
        public SampleLogoutListener() {
        }

        public void onLogoutBegin() {
            end.this.progressDialog = ProgressDialog.show(end.this, "Logging out", "Please wait");
        }

        public void onLogoutFinish() {
            end.this.mPostButton.setVisibility(4);
            end.this.progressDialog.dismiss();
        }

        public void onLogoutError(String error) {
            end.this.progressDialog.dismiss();
        }
    }

    public class SampleDialogListener extends BaseDialogListener {
        public SampleDialogListener() {
        }

        public void onComplete(Bundle values) {
            String postId = values.getString("post_id");
            if (postId != null) {
                Log.d("Facebook-Example", "Dialog Success! post_id=" + postId);
            } else {
                Log.d("Facebook-Example", "No wall post made");
            }
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        if (this.count > 0) {
            menu.add(0, 1, 0, (int) R.string.playagain).setIcon(17301540);
        } else {
            menu.add(0, 1, 0, (int) R.string.delete).setIcon(17301564);
        }
        menu.add(0, 2, 0, (int) R.string.send).setIcon(17301584);
        menu.add(0, 3, 0, (int) R.string.more_apps).setIcon(17301555);
        menu.add(0, 4, 0, (int) R.string.quit).setIcon(17301560);
        return true;
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        super.onMenuItemSelected(featureId, item);
        switch (item.getItemId()) {
            case 1:
                if (this.count > 0) {
                    startActivity(new Intent(this, home.class));
                    finish();
                    return true;
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(getResources().getString(R.string.confirm_delete)).setCancelable(false).setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Log.v(Constants.LOGTAG, " " + end.CLASSTAG + " Deleting data");
                        end.this.app.reset();
                        Toast.makeText(end.this, end.this.getResources().getString(R.string.data_deleted), 1).show();
                        end.this.startActivity(new Intent(end.this, home.class));
                        end.this.finish();
                    }
                }).setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Log.v(Constants.LOGTAG, " " + end.CLASSTAG + " Not deleting data");
                        dialog.cancel();
                    }
                });
                builder.show();
                return true;
            case 2:
                startActivity(new Intent(this, send.class));
                return true;
            case 3:
                this.tracker.trackPageView("/more_apps");
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:bitpocket")));
                return true;
            case 4:
                finish();
                return true;
            default:
                return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (1 == requestCode && -1 == resultCode) {
            Toast.makeText(this, "Email sent... Thanks!!", 0);
        } else {
            Toast.makeText(this, "Error sending email", 0);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.tracker.dispatch();
        this.tracker.stop();
    }

    public void onSuccessfulLoad(MobclixAdView view) {
        Log.v(Constants.LOGTAG, "The ad request was successful!");
        view.setVisibility(0);
    }

    public void onFailedLoad(MobclixAdView view, int errorCode) {
        Log.v(Constants.LOGTAG, "The ad request failed with error code: " + errorCode);
        view.setVisibility(8);
    }

    public void onAdClick(MobclixAdView adView) {
        Log.v(Constants.LOGTAG, "Ad clicked!");
    }

    public void onCustomAdTouchThrough(MobclixAdView adView, String string) {
        Log.v(Constants.LOGTAG, "The custom ad responded with '" + string + "' when touched!");
    }

    public boolean onOpenAllocationLoad(MobclixAdView adView, int openAllocationCode) {
        Log.v(Constants.LOGTAG, "The ad request returned open allocation code: " + openAllocationCode);
        return false;
    }

    public String keywords() {
        return "quiz,trivia,ny,new york,york,nueva york,manhattan,eeuu,america,questions,mobclix";
    }

    public String query() {
        return "query";
    }
}
