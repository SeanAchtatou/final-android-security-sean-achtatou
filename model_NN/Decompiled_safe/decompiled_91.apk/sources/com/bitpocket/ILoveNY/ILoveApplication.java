package com.bitpocket.ILoveNY;

import android.app.Application;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.bitpocket.ILoveNY.PregResp.Pregunta;
import com.bitpocket.ILoveNY.PregResp.Respuesta;
import java.util.ArrayList;
import java.util.List;

public class ILoveApplication extends Application {
    private static final String CLASSTAG = ILoveApplication.class.getSimpleName();
    private int[] arrayPreguntas = new int[53];
    private List<Pregunta> currentQuestions = new ArrayList();
    private SQLiteDatabase database;

    public void onCreate() {
        super.onCreate();
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " onCreate");
        this.database = new CuestionSQLiteOpenHelper(this).getWritableDatabase();
    }

    public void mixQuestions() {
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " mixQuestions");
        initRandomIds();
        loadQuestions();
    }

    public List<Pregunta> getCurrentQuestions() {
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " getCurrentQuestions");
        return this.currentQuestions;
    }

    public int loadPoints() {
        int points = 0;
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " loadPoints");
        Cursor pointsCursor = this.database.query("points", new String[]{"points"}, null, null, null, null, null);
        pointsCursor.moveToFirst();
        if (!pointsCursor.isAfterLast()) {
            do {
                points = pointsCursor.getInt(0);
            } while (pointsCursor.moveToNext());
        }
        pointsCursor.close();
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " points: " + points);
        return points;
    }

    public boolean isFinished() {
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " loadPoints");
        boolean finished = true;
        Cursor finishCursor = this.database.query(CuestionSQLiteOpenHelper.PREGUNTAS_TABLE, new String[]{"idPregunta"}, "isShown = 0", null, null, null, null);
        finishCursor.moveToFirst();
        if (!finishCursor.isAfterLast()) {
            finished = false;
        }
        finishCursor.close();
        return finished;
    }

    private void initRandomIds() {
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " initRandomIds");
        for (int i = 0; i < this.arrayPreguntas.length; i++) {
            this.arrayPreguntas[i] = i;
        }
        for (int j = this.arrayPreguntas.length - 1; j >= 0; j--) {
            int random = (int) (Math.random() * 53.0d);
            if (random != j) {
                int tmp = this.arrayPreguntas[j];
                this.arrayPreguntas[j] = this.arrayPreguntas[random];
                this.arrayPreguntas[random] = tmp;
            }
        }
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " initIds: " + this.arrayPreguntas[0] + "-" + this.arrayPreguntas[1] + "-" + this.arrayPreguntas[2] + "-" + this.arrayPreguntas[3] + "-" + this.arrayPreguntas[4] + "-" + this.arrayPreguntas[5] + "-" + this.arrayPreguntas[6] + "-" + this.arrayPreguntas[7] + "-" + this.arrayPreguntas[8] + "-" + this.arrayPreguntas[9]);
    }

    private void loadQuestions() {
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " loadQuestions");
        this.currentQuestions = new ArrayList();
        Cursor cuestionCursor = this.database.query(CuestionSQLiteOpenHelper.PREGUNTAS_TABLE, new String[]{"idPregunta", "txt", CuestionSQLiteOpenHelper.PREGUNTA_NIVEL, CuestionSQLiteOpenHelper.PREGUNTA_CATEGORIA, CuestionSQLiteOpenHelper.PREGUNTA_IMAGEN, CuestionSQLiteOpenHelper.PREGUNTA_CREATOR, CuestionSQLiteOpenHelper.PREGUNTA_SHOWN}, null, null, null, null, null);
        cuestionCursor.moveToFirst();
        if (!cuestionCursor.isAfterLast()) {
            do {
                long id = (long) this.arrayPreguntas[(int) cuestionCursor.getLong(0)];
                long id_db = cuestionCursor.getLong(0);
                String texto = cuestionCursor.getString(1);
                String categoria = cuestionCursor.getString(3);
                String img = cuestionCursor.getString(4);
                String creator = cuestionCursor.getString(5);
                int isShown = cuestionCursor.getInt(6);
                Pregunta p = new Pregunta(id, id_db, categoria, img, texto, creator, isShown);
                p.setRespuestas(loadRespuestas((long) ((int) cuestionCursor.getLong(0))));
                this.currentQuestions.add(p);
                Log.v(Constants.LOGTAG, " " + CLASSTAG + " loading Question " + id + " shown: " + isShown);
            } while (cuestionCursor.moveToNext());
        }
        cuestionCursor.close();
    }

    private ArrayList<Respuesta> loadRespuestas(long id) {
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " loadRespuestas: " + id);
        ArrayList<Respuesta> respuestas = new ArrayList<>();
        Cursor respuestaCursor = this.database.query(CuestionSQLiteOpenHelper.RESPUESTAS_TABLE, new String[]{CuestionSQLiteOpenHelper.RESPUESTA_ID, "idPregunta", CuestionSQLiteOpenHelper.RESPUESTA_PUNTUACION, "txt"}, "idPregunta = " + id, null, null, null, null);
        respuestaCursor.moveToFirst();
        if (!respuestaCursor.isAfterLast()) {
            do {
                respuestas.add(new Respuesta(respuestaCursor.getLong(0), respuestaCursor.getLong(1), respuestaCursor.getString(3), respuestaCursor.getInt(2)));
            } while (respuestaCursor.moveToNext());
        }
        respuestaCursor.close();
        return respuestas;
    }

    public void savePoints(int points) {
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " saving points: " + points);
        this.database.delete("points", null, null);
        ContentValues values = new ContentValues();
        values.put("points", Integer.valueOf(points));
        this.database.insert("points", null, values);
    }

    public void showPregunta(int idPregunta, boolean value) {
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " showing Pregunta: " + idPregunta);
        ContentValues values = new ContentValues();
        values.put(CuestionSQLiteOpenHelper.PREGUNTA_SHOWN, Boolean.valueOf(value));
        this.database.update(CuestionSQLiteOpenHelper.PREGUNTAS_TABLE, values, "idPregunta=" + idPregunta, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    public void reset() {
        savePoints(0);
        ContentValues values = new ContentValues();
        values.put(CuestionSQLiteOpenHelper.PREGUNTA_SHOWN, (Boolean) false);
        this.database.update(CuestionSQLiteOpenHelper.PREGUNTAS_TABLE, values, null, null);
    }
}
