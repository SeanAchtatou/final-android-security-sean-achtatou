package com.bitpocket.ILoveNY.Prefs;

import android.content.Context;
import android.content.SharedPreferences;

public class Prefs {
    private long _date_firstLaunch = 0;
    private boolean _dontshowagain = false;
    private SharedPreferences.Editor _editor = null;
    private boolean _first_time = true;
    private long _launch_count = 0;
    private SharedPreferences _prefs = null;

    public Prefs(Context context) {
        this._prefs = context.getSharedPreferences("PREFS_PRIVATE", 0);
        this._editor = this._prefs.edit();
    }

    public String getValue(String key, String defaultvalue) {
        if (this._prefs == null) {
            return "Unknown";
        }
        return this._prefs.getString(key, defaultvalue);
    }

    public void setValue(String key, String value) {
        if (this._editor != null) {
            this._editor.putString(key, value);
        }
    }

    public boolean getFirstTime() {
        if (this._prefs == null) {
            return false;
        }
        this._first_time = this._prefs.getBoolean("first_time", true);
        return this._first_time;
    }

    public void setFirstTime(boolean newValue) {
        if (this._editor != null) {
            this._editor.putBoolean("first_time", newValue);
        }
    }

    public boolean getDontShowAgain() {
        if (this._prefs == null) {
            return false;
        }
        this._dontshowagain = this._prefs.getBoolean("dontshowagain", false);
        return this._dontshowagain;
    }

    public void setDontShowAgain(boolean newValue) {
        if (this._editor != null) {
            this._editor.putBoolean("dontshowagain", newValue);
        }
    }

    public long getLaunchCount() {
        if (this._prefs == null) {
            return 0;
        }
        this._launch_count = this._prefs.getLong("launch_count", 0);
        return this._launch_count;
    }

    public void setLaunchCount(long newValue) {
        if (this._editor != null) {
            this._editor.putLong("launch_count", newValue);
        }
    }

    public long getDateFirstLaunchCount() {
        if (this._prefs == null) {
            return 0;
        }
        this._date_firstLaunch = this._prefs.getLong("date_firstLaunch", 0);
        return this._date_firstLaunch;
    }

    public void setDateFirstLaunchCount(long newValue) {
        if (this._editor != null) {
            this._editor.putLong("date_firstLaunch", newValue);
        }
    }

    public void save() {
        if (this._editor != null) {
            this._editor.commit();
        }
    }
}
