package com.bitpocket.ILoveNY.Facebook;

import android.util.Log;
import com.bitpocket.ILoveNY.Constants;
import java.util.Iterator;
import java.util.LinkedList;

public class SessionEvents {
    private static final String CLASSTAG = SessionEvents.class.getSimpleName();
    private static LinkedList<AuthListener> mAuthListeners = new LinkedList<>();
    private static LinkedList<DataListener> mDataListeners = new LinkedList<>();
    private static LinkedList<LogoutListener> mLogoutListeners = new LinkedList<>();

    public interface AuthListener {
        void onAuthFail(String str);

        void onAuthSucceed();
    }

    public interface DataListener {
        void onDataFail(String str);

        void onDataSucceed();
    }

    public interface LogoutListener {
        void onLogoutBegin();

        void onLogoutError(String str);

        void onLogoutFinish();
    }

    public static void addAuthListener(AuthListener listener) {
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " addAuthListener");
        mAuthListeners.add(listener);
    }

    public static void addDataListener(DataListener listener) {
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " addDataListener");
        mDataListeners.add(listener);
    }

    public static void removeAuthListener(AuthListener listener) {
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " removeAuthListener");
        mAuthListeners.remove(listener);
    }

    public static void removeDataListener(DataListener listener) {
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " removeDataListener");
        mDataListeners.remove(listener);
    }

    public static void addLogoutListener(LogoutListener listener) {
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " addLogoutListener");
        mLogoutListeners.add(listener);
    }

    public static void removeLogoutListener(LogoutListener listener) {
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " removeLogoutListener");
        mLogoutListeners.remove(listener);
    }

    public static void onLoginSuccess() {
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " onLoginSuccess");
        Iterator<AuthListener> it = mAuthListeners.iterator();
        while (it.hasNext()) {
            it.next().onAuthSucceed();
        }
    }

    public static void onDataSuccess() {
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " onDataSuccess");
        Iterator<DataListener> it = mDataListeners.iterator();
        while (it.hasNext()) {
            it.next().onDataSucceed();
        }
    }

    public static void onLoginError(String error) {
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " onLoginError");
        Iterator<AuthListener> it = mAuthListeners.iterator();
        while (it.hasNext()) {
            it.next().onAuthFail(error);
        }
    }

    public static void onDataError(String error) {
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " onDataError");
        Iterator<DataListener> it = mDataListeners.iterator();
        while (it.hasNext()) {
            it.next().onDataFail(error);
        }
    }

    public static void onLogoutBegin() {
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " onLogoutBegin");
        Iterator<LogoutListener> it = mLogoutListeners.iterator();
        while (it.hasNext()) {
            it.next().onLogoutBegin();
        }
    }

    public static void onLogoutFinish() {
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " onLogoutFinish");
        Iterator<LogoutListener> it = mLogoutListeners.iterator();
        while (it.hasNext()) {
            it.next().onLogoutFinish();
        }
    }

    public static void onLogoutError(String error) {
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " onLogoutError");
        Iterator<LogoutListener> it = mLogoutListeners.iterator();
        while (it.hasNext()) {
            it.next().onLogoutError(error);
        }
    }
}
