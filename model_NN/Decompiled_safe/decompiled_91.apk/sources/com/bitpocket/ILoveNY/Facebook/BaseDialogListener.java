package com.bitpocket.ILoveNY.Facebook;

import com.agi.fbsdk.DialogError;
import com.agi.fbsdk.Facebook;
import com.agi.fbsdk.FacebookError;

public abstract class BaseDialogListener implements Facebook.DialogListener {
    public void onFacebookError(FacebookError e) {
        e.printStackTrace();
    }

    public void onError(DialogError e) {
        e.printStackTrace();
    }

    public void onCancel() {
    }
}
