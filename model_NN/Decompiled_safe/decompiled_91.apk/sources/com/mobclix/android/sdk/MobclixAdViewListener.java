package com.mobclix.android.sdk;

public interface MobclixAdViewListener {
    public static final int ADSIZE_DISABLED = -999999;
    public static final int SUBALLOCATION_ADMOB = -750;
    public static final int SUBALLOCATION_GOOGLE = -10100;
    public static final int SUBALLOCATION_MILLENNIAL = -111111;
    public static final int SUBALLOCATION_OTHER = -1006;
    public static final int UNAVAILABLE = -503;
    public static final int UNKNOWN_ERROR = 0;

    String keywords();

    void onAdClick(MobclixAdView mobclixAdView);

    void onCustomAdTouchThrough(MobclixAdView mobclixAdView, String str);

    void onFailedLoad(MobclixAdView mobclixAdView, int i);

    boolean onOpenAllocationLoad(MobclixAdView mobclixAdView, int i);

    void onSuccessfulLoad(MobclixAdView mobclixAdView);

    String query();
}
