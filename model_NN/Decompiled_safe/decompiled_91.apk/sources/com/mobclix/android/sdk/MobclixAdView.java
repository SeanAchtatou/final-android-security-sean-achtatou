package com.mobclix.android.sdk;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.widget.ViewFlipper;
import com.mobclix.android.sdk.Mobclix;
import com.mobclix.android.sdk.MobclixCreative;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Timer;

public abstract class MobclixAdView extends ViewFlipper {
    static final String ADSIZE_300x250 = "300x250";
    static final String ADSIZE_320x50 = "320x50";
    private static final long MINIMUM_FORCED_REFRESH_TIME = 5000;
    private static final long MINIMUM_REFRESH_TIME = 15000;
    static final String OPEN_ALLOCATION_MILLENNIAL = "openmillennial";
    static int debugOrdinal = 0;
    static HashMap<String, Long> lastAutoplayTime = new HashMap<>();
    private String TAG = "MobclixAdView";
    MobclixCreative ad = null;
    String adCode = "";
    /* access modifiers changed from: private */
    public String adSpace = "";
    private Thread adThread;
    int allowAutoplay = -1;
    int backgroundColor = 0;
    String creativeId = "";
    MobclixCreativeManager creativeManager = null;
    boolean detached = false;
    int getNextAdAttempts = 0;
    final AdResponseHandler handler = new AdResponseHandler(this, null);
    float height;
    /* access modifiers changed from: private */
    public MobclixInstrumentation instrumentation;
    String instrumentationGroup = null;
    private boolean isManuallyPaused = false;
    long lastAdLoad = 0;
    HashSet<MobclixAdViewListener> listeners = new HashSet<>();
    Object lock = new Object();
    MobclixCreative.HTMLPagePool mHTMLPagePool;
    int ordinal = 0;
    MobclixCreative prevAd = null;
    String rawResponse = null;
    final RemoteConfigReadyHandler rcHandler = new RemoteConfigReadyHandler(this, null);
    /* access modifiers changed from: private */
    public long refreshRate = 0;
    boolean remoteConfigSet = false;
    String requestedAdUrlForAdView = null;
    int requireUserInteraction = -1;
    boolean restored = false;
    boolean rotate = true;
    float scale = 1.0f;
    String size;
    boolean testMode = false;
    private Timer timer = null;
    MobclixUtilityView utilityView = null;
    float width;

    MobclixAdView(Context a, String s) {
        super(a);
        this.size = s;
        try {
            initialize((Activity) a);
        } catch (Mobclix.MobclixPermissionException e) {
            throw e;
        } catch (Exception e2) {
        }
    }

    MobclixAdView(Context a, String s, AttributeSet attrs) {
        super(a, attrs);
        this.size = s;
        String colorString = attrs.getAttributeValue("http://schemas.android.com/apk/res/android", "background");
        if (colorString != null) {
            this.backgroundColor = Color.parseColor(colorString);
        }
        try {
            initialize((Activity) a);
        } catch (Mobclix.MobclixPermissionException e) {
            throw e;
        } catch (Exception e2) {
        }
    }

    public void setRequestedAdUrlForAdView(String url) {
        this.requestedAdUrlForAdView = url;
    }

    public String getRequestedAdUrlForAdView() {
        return this.requestedAdUrlForAdView;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        try {
            if (Integer.parseInt(Build.VERSION.SDK) >= 7) {
                try {
                    super.onDetachedFromWindow();
                    super.stopFlipping();
                } catch (IllegalArgumentException e) {
                    Log.w(this.TAG, "Android project  issue 6191  workaround.");
                    super.stopFlipping();
                } catch (Throwable th) {
                    super.stopFlipping();
                    throw th;
                }
            } else {
                super.onDetachedFromWindow();
            }
        } catch (Exception e2) {
            super.onDetachedFromWindow();
        }
        int i = 0;
        while (getChildCount() > i) {
            try {
                if (getChildAt(i).getClass() == MobclixCreative.class) {
                    int j = 0;
                    while (j < getChildCount()) {
                        try {
                            try {
                                if (((MobclixCreative) getChildAt(i)).getChildAt(j).getClass() == MobclixCreative.OpenAllocationPage.class) {
                                    ((MobclixCreative.OpenAllocationPage) ((MobclixCreative) getChildAt(i)).getChildAt(j)).dealloc();
                                }
                            } catch (Exception e3) {
                            }
                            j++;
                        } catch (Exception e4) {
                        }
                    }
                    i++;
                } else if (getChildAt(i).getClass() == Class.forName("com.google.ads.AdView")) {
                    try {
                        Class.forName("com.google.ads.AdView").getMethod("destroy", new Class[0]).invoke(getChildAt(i), new Object[0]);
                        getChildAt(i).setOnClickListener(null);
                        removeViewAt(i);
                    } catch (Throwable th2) {
                    }
                } else if (getChildAt(i).getClass() == Class.forName("com.millennialmedia.android.MMAdView")) {
                    try {
                        Class.forName("com.millennialmedia.android.MMAdView").getMethod("halt", new Class[0]).invoke(getChildAt(i), new Object[0]);
                        getChildAt(i).setOnClickListener(null);
                        removeViewAt(i);
                    } catch (Exception e5) {
                    }
                } else {
                    try {
                        removeViewAt(i);
                    } catch (Exception e6) {
                    }
                }
            } catch (Exception e7) {
            }
        }
        this.detached = true;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        this.detached = false;
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        stopFetchAdRequestTimer();
        if (this.ad != null) {
            this.ad.onStop();
        }
        super.finalize();
    }

    public boolean addMobclixAdViewListener(MobclixAdViewListener l) {
        if (l == null) {
            return false;
        }
        return this.listeners.add(l);
    }

    public boolean removeMobclixAdViewListener(MobclixAdViewListener l) {
        return this.listeners.remove(l);
    }

    public void setLayoutParams(ViewGroup.LayoutParams params) {
        int formatWidth = Integer.parseInt(this.size.split("x")[0]);
        int formatHeight = Integer.parseInt(this.size.split("x")[1]);
        this.width = TypedValue.applyDimension(1, (float) formatWidth, getResources().getDisplayMetrics());
        this.height = TypedValue.applyDimension(1, (float) formatHeight, getResources().getDisplayMetrics());
        this.scale = getResources().getDisplayMetrics().density;
        params.width = (int) this.width;
        params.height = (int) this.height;
        super.setLayoutParams(params);
    }

    public void setRefreshTime(long rate) {
        stopFetchAdRequestTimer();
        this.refreshRate = rate;
        if (this.refreshRate >= 0) {
            if (this.refreshRate < MINIMUM_REFRESH_TIME) {
                this.refreshRate = MINIMUM_REFRESH_TIME;
            }
            try {
                this.timer = new Timer();
                this.timer.scheduleAtFixedRate(new FetchAdResponseThread(this.handler), this.refreshRate, this.refreshRate);
            } catch (Exception e) {
            }
        }
    }

    public void setAllowAutoplay(boolean auto) {
        this.allowAutoplay = auto ? 1 : 0;
    }

    public void setRichMediaRequiresUserInteraction(boolean user) {
        this.requireUserInteraction = user ? 1 : 0;
    }

    public void setShouldRotate(boolean shouldRotate) {
        this.rotate = shouldRotate;
    }

    public void setTestMode(boolean t) {
        this.testMode = t;
    }

    public void setAdSpace(String a) {
        this.adSpace = a;
    }

    public void setCreativeId(String c) {
        this.creativeId = c;
    }

    public String getCreativeId() {
        return this.creativeId;
    }

    public boolean allowAutoplay() {
        return this.allowAutoplay == 1;
    }

    public boolean richMediaRequiresUserInteraction() {
        return this.requireUserInteraction == 1;
    }

    public boolean shouldRotate() {
        return this.rotate;
    }

    public void onWindowFocusChanged(boolean hasWindowFocus) {
        if (!hasWindowFocus) {
            try {
                stopFetchAdRequestTimer();
            } catch (Exception e) {
            }
            try {
                Mobclix.getInstance().location.stopLocation();
            } catch (Exception e2) {
            }
            try {
                if (this.ad != null) {
                    this.ad.onPause();
                }
            } catch (Exception e3) {
            }
        } else if (this.ad != null) {
            Mobclix.getInstance().sessionEvent();
            if (!this.isManuallyPaused) {
                resume();
            }
            if (this.ad != null) {
                this.ad.onResume();
            }
        }
    }

    private void stopFetchAdRequestTimer() {
        synchronized (this) {
            if (this.timer != null) {
                this.timer.cancel();
                this.timer.purge();
                this.timer = null;
            }
        }
    }

    public void pause() {
        this.isManuallyPaused = true;
        stopFetchAdRequestTimer();
    }

    public void resume() {
        if (Mobclix.getInstance().isEnabled(this.size)) {
            if (this.refreshRate != 0) {
                setRefreshTime(this.refreshRate);
            } else {
                setRefreshTime(Mobclix.getInstance().getRefreshTime(this.size));
            }
        }
        this.isManuallyPaused = false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x013c, code lost:
        r13 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        com.mobclix.android.sdk.Mobclix.getInstance().setUserAgent("Mozilla/5.0 (Linux; U; Android 1.1; en-us; dream) AppleWebKit/525.10+ (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0149, code lost:
        r13 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x014b, code lost:
        throw r13;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0149 A[ExcHandler: MobclixPermissionException (r13v3 'e' com.mobclix.android.sdk.Mobclix$MobclixPermissionException A[CUSTOM_DECLARE]), Splitter:B:7:0x004e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void initialize(android.app.Activity r20) {
        /*
            r19 = this;
            boolean r13 = r19.isInEditMode()
            if (r13 == 0) goto L_0x0007
        L_0x0006:
            return
        L_0x0007:
            r0 = r19
            java.lang.Object r0 = r0.lock
            r13 = r0
            monitor-enter(r13)
            com.mobclix.android.sdk.MobclixInstrumentation r14 = com.mobclix.android.sdk.MobclixInstrumentation.getInstance()     // Catch:{ all -> 0x0139 }
            r0 = r14
            r1 = r19
            r1.instrumentation = r0     // Catch:{ all -> 0x0139 }
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ all -> 0x0139 }
            java.lang.String r15 = com.mobclix.android.sdk.MobclixInstrumentation.ADVIEW     // Catch:{ all -> 0x0139 }
            java.lang.String r15 = java.lang.String.valueOf(r15)     // Catch:{ all -> 0x0139 }
            r14.<init>(r15)     // Catch:{ all -> 0x0139 }
            java.lang.String r15 = "_"
            java.lang.StringBuilder r14 = r14.append(r15)     // Catch:{ all -> 0x0139 }
            r0 = r19
            java.lang.String r0 = r0.size     // Catch:{ all -> 0x0139 }
            r15 = r0
            java.lang.StringBuilder r14 = r14.append(r15)     // Catch:{ all -> 0x0139 }
            java.lang.String r15 = "_"
            java.lang.StringBuilder r14 = r14.append(r15)     // Catch:{ all -> 0x0139 }
            int r15 = com.mobclix.android.sdk.MobclixAdView.debugOrdinal     // Catch:{ all -> 0x0139 }
            int r15 = r15 + 1
            java.lang.StringBuilder r14 = r14.append(r15)     // Catch:{ all -> 0x0139 }
            java.lang.String r14 = r14.toString()     // Catch:{ all -> 0x0139 }
            r0 = r14
            r1 = r19
            r1.instrumentationGroup = r0     // Catch:{ all -> 0x0139 }
            int r14 = com.mobclix.android.sdk.MobclixAdView.debugOrdinal     // Catch:{ all -> 0x0139 }
            int r14 = r14 + 1
            com.mobclix.android.sdk.MobclixAdView.debugOrdinal = r14     // Catch:{ all -> 0x0139 }
            monitor-exit(r13)     // Catch:{ all -> 0x0139 }
            com.mobclix.android.sdk.Mobclix.onCreate(r20)     // Catch:{ MobclixPermissionException -> 0x0149, Exception -> 0x0172 }
            com.mobclix.android.sdk.Mobclix r13 = com.mobclix.android.sdk.Mobclix.getInstance()     // Catch:{ MobclixPermissionException -> 0x0149, Exception -> 0x0172 }
            java.lang.String r13 = r13.getUserAgent()     // Catch:{ MobclixPermissionException -> 0x0149, Exception -> 0x0172 }
            java.lang.String r14 = "null"
            boolean r13 = r13.equals(r14)     // Catch:{ MobclixPermissionException -> 0x0149, Exception -> 0x0172 }
            if (r13 == 0) goto L_0x008e
            android.webkit.WebView r12 = new android.webkit.WebView     // Catch:{ MobclixPermissionException -> 0x0149, Exception -> 0x0172 }
            android.content.Context r13 = r19.getContext()     // Catch:{ MobclixPermissionException -> 0x0149, Exception -> 0x0172 }
            r12.<init>(r13)     // Catch:{ MobclixPermissionException -> 0x0149, Exception -> 0x0172 }
            android.webkit.WebSettings r10 = r12.getSettings()     // Catch:{ MobclixPermissionException -> 0x0149, Exception -> 0x0172 }
            java.lang.Class r13 = r10.getClass()     // Catch:{ Exception -> 0x013c, MobclixPermissionException -> 0x0149 }
            java.lang.String r14 = "getUserAgentString"
            r15 = 0
            java.lang.Class[] r15 = new java.lang.Class[r15]     // Catch:{ Exception -> 0x013c, MobclixPermissionException -> 0x0149 }
            java.lang.reflect.Method r6 = r13.getDeclaredMethod(r14, r15)     // Catch:{ Exception -> 0x013c, MobclixPermissionException -> 0x0149 }
            com.mobclix.android.sdk.Mobclix r13 = com.mobclix.android.sdk.Mobclix.getInstance()     // Catch:{ Exception -> 0x013c, MobclixPermissionException -> 0x0149 }
            r14 = 0
            java.lang.Object[] r14 = new java.lang.Object[r14]     // Catch:{ Exception -> 0x013c, MobclixPermissionException -> 0x0149 }
            java.lang.Object r20 = r6.invoke(r10, r14)     // Catch:{ Exception -> 0x013c, MobclixPermissionException -> 0x0149 }
            java.lang.String r20 = (java.lang.String) r20     // Catch:{ Exception -> 0x013c, MobclixPermissionException -> 0x0149 }
            r0 = r13
            r1 = r20
            r0.setUserAgent(r1)     // Catch:{ Exception -> 0x013c, MobclixPermissionException -> 0x0149 }
        L_0x008e:
            com.mobclix.android.sdk.MobclixCreative$HTMLPagePool r13 = new com.mobclix.android.sdk.MobclixCreative$HTMLPagePool     // Catch:{ MobclixPermissionException -> 0x0149, Exception -> 0x0172 }
            r13.<init>()     // Catch:{ MobclixPermissionException -> 0x0149, Exception -> 0x0172 }
            r0 = r13
            r1 = r19
            r1.mHTMLPagePool = r0     // Catch:{ MobclixPermissionException -> 0x0149, Exception -> 0x0172 }
            com.mobclix.android.sdk.MobclixAdView$MobclixUtilityView r13 = new com.mobclix.android.sdk.MobclixAdView$MobclixUtilityView     // Catch:{ MobclixPermissionException -> 0x0149, Exception -> 0x0172 }
            android.content.Context r14 = r19.getContext()     // Catch:{ MobclixPermissionException -> 0x0149, Exception -> 0x0172 }
            r0 = r13
            r1 = r19
            r2 = r14
            r0.<init>(r2)     // Catch:{ MobclixPermissionException -> 0x0149, Exception -> 0x0172 }
            r0 = r13
            r1 = r19
            r1.utilityView = r0     // Catch:{ MobclixPermissionException -> 0x0149, Exception -> 0x0172 }
            r0 = r19
            com.mobclix.android.sdk.MobclixAdView$MobclixUtilityView r0 = r0.utilityView     // Catch:{ MobclixPermissionException -> 0x0149, Exception -> 0x0172 }
            r13 = r0
            android.widget.FrameLayout$LayoutParams r14 = new android.widget.FrameLayout$LayoutParams     // Catch:{ MobclixPermissionException -> 0x0149, Exception -> 0x0172 }
            r15 = -1
            r16 = -1
            r14.<init>(r15, r16)     // Catch:{ MobclixPermissionException -> 0x0149, Exception -> 0x0172 }
            r13.setLayoutParams(r14)     // Catch:{ MobclixPermissionException -> 0x0149, Exception -> 0x0172 }
            r0 = r19
            com.mobclix.android.sdk.MobclixAdView$MobclixUtilityView r0 = r0.utilityView     // Catch:{ MobclixPermissionException -> 0x0149, Exception -> 0x0172 }
            r13 = r0
            r14 = 0
            r13.setBackgroundColor(r14)     // Catch:{ MobclixPermissionException -> 0x0149, Exception -> 0x0172 }
        L_0x00c3:
            java.lang.String[] r13 = com.mobclix.android.sdk.Mobclix.MC_AD_SIZES
            int r14 = r13.length
            r15 = 0
        L_0x00c7:
            if (r15 < r14) goto L_0x014c
            r13 = 1
            r0 = r19
            r1 = r13
            r0.requestDisallowInterceptTouchEvent(r1)
            r0 = r19
            int r0 = r0.backgroundColor
            r13 = r0
            r0 = r19
            r1 = r13
            r0.setBackgroundColor(r1)
            java.lang.Object r13 = r19.getTag()     // Catch:{ Exception -> 0x016f }
            java.lang.String r13 = r13.toString()     // Catch:{ Exception -> 0x016f }
            r0 = r13
            r1 = r19
            r1.adSpace = r0     // Catch:{ Exception -> 0x016f }
        L_0x00e8:
            java.lang.String r3 = "com.admob.android.ads.AdManager"
            java.lang.Class r4 = java.lang.Class.forName(r3)     // Catch:{ Exception -> 0x016d }
            java.lang.String r13 = "setTestDevices"
            r14 = 1
            java.lang.Class[] r14 = new java.lang.Class[r14]     // Catch:{ Exception -> 0x016d }
            r15 = 0
            java.lang.Class<java.lang.String[]> r16 = java.lang.String[].class
            r14[r15] = r16     // Catch:{ Exception -> 0x016d }
            java.lang.reflect.Method r7 = r4.getMethod(r13, r14)     // Catch:{ Exception -> 0x016d }
            java.lang.String r13 = "TEST_EMULATOR"
            java.lang.reflect.Field r13 = r4.getField(r13)     // Catch:{ Exception -> 0x016d }
            r14 = 0
            java.lang.Object r11 = r13.get(r14)     // Catch:{ Exception -> 0x016d }
            java.lang.String r11 = (java.lang.String) r11     // Catch:{ Exception -> 0x016d }
            r13 = 0
            r14 = 1
            java.lang.Object[] r14 = new java.lang.Object[r14]     // Catch:{ Exception -> 0x016d }
            r15 = 0
            r16 = 1
            r0 = r16
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x016d }
            r16 = r0
            r17 = 0
            r16[r17] = r11     // Catch:{ Exception -> 0x016d }
            r14[r15] = r16     // Catch:{ Exception -> 0x016d }
            r7.invoke(r13, r14)     // Catch:{ Exception -> 0x016d }
        L_0x011f:
            com.mobclix.android.sdk.Mobclix r13 = com.mobclix.android.sdk.Mobclix.getInstance()
            if (r13 == 0) goto L_0x0006
            java.lang.Thread r8 = new java.lang.Thread
            com.mobclix.android.sdk.MobclixAdView$WaitForRemoteConfigThread r13 = new com.mobclix.android.sdk.MobclixAdView$WaitForRemoteConfigThread
            r14 = 0
            r0 = r13
            r1 = r19
            r2 = r14
            r0.<init>(r1, r2)
            r8.<init>(r13)
            r8.start()
            goto L_0x0006
        L_0x0139:
            r14 = move-exception
            monitor-exit(r13)     // Catch:{ all -> 0x0139 }
            throw r14
        L_0x013c:
            r13 = move-exception
            r5 = r13
            com.mobclix.android.sdk.Mobclix r13 = com.mobclix.android.sdk.Mobclix.getInstance()     // Catch:{ MobclixPermissionException -> 0x0149, Exception -> 0x0172 }
            java.lang.String r14 = "Mozilla/5.0 (Linux; U; Android 1.1; en-us; dream) AppleWebKit/525.10+ (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2"
            r13.setUserAgent(r14)     // Catch:{ MobclixPermissionException -> 0x0149, Exception -> 0x0172 }
            goto L_0x008e
        L_0x0149:
            r13 = move-exception
            r5 = r13
            throw r5
        L_0x014c:
            r9 = r13[r15]
            java.util.HashMap<java.lang.String, java.lang.Long> r16 = com.mobclix.android.sdk.MobclixAdView.lastAutoplayTime
            r0 = r16
            r1 = r9
            boolean r16 = r0.containsKey(r1)
            if (r16 != 0) goto L_0x0169
            java.util.HashMap<java.lang.String, java.lang.Long> r16 = com.mobclix.android.sdk.MobclixAdView.lastAutoplayTime
            r17 = 0
            java.lang.Long r17 = java.lang.Long.valueOf(r17)
            r0 = r16
            r1 = r9
            r2 = r17
            r0.put(r1, r2)
        L_0x0169:
            int r15 = r15 + 1
            goto L_0x00c7
        L_0x016d:
            r13 = move-exception
            goto L_0x011f
        L_0x016f:
            r13 = move-exception
            goto L_0x00e8
        L_0x0172:
            r13 = move-exception
            goto L_0x00c3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.MobclixAdView.initialize(android.app.Activity):void");
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        super.onSaveInstanceState();
        stopFetchAdRequestTimer();
        if (this.ad == null) {
            return null;
        }
        this.ad.onStop();
        Bundle savedInstanceState = new Bundle();
        savedInstanceState.putString("response", this.rawResponse);
        savedInstanceState.putInt("nCreative", this.creativeManager.getIndex());
        return savedInstanceState;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable state) {
        super.onRestoreInstanceState(View.BaseSavedState.EMPTY_STATE);
        try {
            this.rawResponse = ((Bundle) state).getString("response");
            if (!this.rawResponse.equals("")) {
                try {
                    this.creativeManager = new MobclixCreativeManager(this.rawResponse, ((Bundle) state).getInt("nCreative"));
                } catch (Exception e) {
                }
                restoreAd();
                this.restored = true;
            }
        } catch (Exception e2) {
        }
    }

    public void getNextAd(String params) {
        this.ad = null;
        if (this.getNextAdAttempts > 3) {
            this.getNextAdAttempts = 0;
            return;
        }
        this.getNextAdAttempts++;
        this.lastAdLoad = 0;
        if (this.creativeManager == null || this.creativeManager.creatives == null) {
            getAd(params);
        } else if (!this.creativeManager.nextCreative()) {
            getAd(params);
        } else {
            try {
                this.ad = new MobclixCreative(this, this.creativeManager.getCreative(), false);
                if (!this.ad.isInitialized()) {
                    getNextAd("");
                }
            } catch (Exception e) {
                getNextAd("");
            }
        }
    }

    public void getAd() {
        getAd(null);
    }

    /* access modifiers changed from: package-private */
    public void getAd(String params) {
        if (this.remoteConfigSet && !this.detached && System.currentTimeMillis() >= this.lastAdLoad + MINIMUM_FORCED_REFRESH_TIME) {
            this.lastAdLoad = System.currentTimeMillis();
            if (this.adThread != null) {
                this.adThread.interrupt();
                this.adThread = null;
            }
            String instrPath = this.instrumentation.benchmarkStart(this.instrumentation.startGroup(this.instrumentationGroup, MobclixInstrumentation.ADVIEW), "start_request");
            FetchAdResponseThread fetchAdResponseThread = new FetchAdResponseThread(this.handler);
            if (params != null) {
                fetchAdResponseThread.setNextRequestParams(params);
            }
            this.adThread = new Thread(fetchAdResponseThread);
            this.adThread.start();
            String instrPath2 = this.instrumentation.benchmarkFinishPath(instrPath);
        }
    }

    private void restoreAd() {
        try {
            Mobclix.onCreate((Activity) getContext());
        } catch (Exception e) {
        }
        try {
            if (this.adThread != null) {
                this.adThread.interrupt();
            }
            if (Mobclix.getInstance().isEnabled(this.size)) {
                this.remoteConfigSet = true;
                this.ad = new MobclixCreative(this, this.creativeManager.getCreative(), true);
                setRefreshTime(Mobclix.getInstance().getRefreshTime(this.size));
                return;
            }
            Iterator<MobclixAdViewListener> it = this.listeners.iterator();
            while (it.hasNext()) {
                MobclixAdViewListener listener = it.next();
                if (listener != null) {
                    listener.onFailedLoad(this, -999999);
                }
            }
        } catch (Exception e2) {
        }
    }

    public void cancelAd() {
        synchronized (this) {
            if (this.adThread != null) {
                this.adThread.interrupt();
            }
        }
        stopFetchAdRequestTimer();
    }

    public void destroy() {
        while (getChildCount() > 0) {
            try {
                if (getChildAt(0).getClass() == MobclixCreative.class) {
                    int j = 0;
                    while (j < getChildCount()) {
                        try {
                            try {
                                ((MobclixCreative.Page) ((MobclixCreative) getChildAt(0)).getChildAt(j)).dealloc();
                            } catch (Exception e) {
                            }
                            j++;
                        } catch (Exception e2) {
                        }
                    }
                    removeViewAt(0);
                } else if (getChildAt(0).getClass() == Class.forName("com.google.ads.AdView")) {
                    try {
                        Class.forName("com.google.ads.AdView").getMethod("destroy", new Class[0]).invoke(getChildAt(0), new Object[0]);
                        getChildAt(0).setOnClickListener(null);
                        removeViewAt(0);
                    } catch (Throwable th) {
                    }
                } else if (getChildAt(0).getClass() == Class.forName("com.millennialmedia.android.MMAdView")) {
                    try {
                        Class.forName("com.millennialmedia.android.MMAdView").getMethod("halt", new Class[0]).invoke(getChildAt(0), new Object[0]);
                        getChildAt(0).setOnClickListener(null);
                        removeViewAt(0);
                    } catch (Throwable th2) {
                    }
                } else {
                    try {
                        removeViewAt(0);
                    } catch (Exception e3) {
                    }
                }
            } catch (Exception e4) {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onVisibilityChanged(View view, int visibility) {
        if (visibility == 0 && this.ad != null && !this.ad.trackingPixelsFired) {
            this.ad.fireOnShowTrackingPixels();
        }
    }

    private class WaitForRemoteConfigThread implements Runnable {
        private WaitForRemoteConfigThread() {
        }

        /* synthetic */ WaitForRemoteConfigThread(MobclixAdView mobclixAdView, WaitForRemoteConfigThread waitForRemoteConfigThread) {
            this();
        }

        public void run() {
            Long t = Long.valueOf(System.currentTimeMillis());
            do {
                try {
                    if (Mobclix.getInstance().isRemoteConfigSet() == 1) {
                        break;
                    }
                } catch (Exception e) {
                    return;
                }
            } while (System.currentTimeMillis() - t.longValue() < 10000);
            CookieManager.getInstance().setAcceptCookie(true);
            MobclixAdView.this.rcHandler.sendEmptyMessage(0);
        }
    }

    private class RemoteConfigReadyHandler extends Handler {
        private RemoteConfigReadyHandler() {
        }

        /* synthetic */ RemoteConfigReadyHandler(MobclixAdView mobclixAdView, RemoteConfigReadyHandler remoteConfigReadyHandler) {
            this();
        }

        public void handleMessage(Message msg) {
            if (!MobclixAdView.this.restored) {
                if (Mobclix.getInstance().isEnabled(MobclixAdView.this.size)) {
                    MobclixAdView.this.remoteConfigSet = true;
                    if (MobclixAdView.this.allowAutoplay == -1) {
                        MobclixAdView.this.setAllowAutoplay(Mobclix.getInstance().shouldAutoplay(MobclixAdView.this.size));
                    }
                    if (MobclixAdView.this.requireUserInteraction == -1) {
                        MobclixAdView.this.setRichMediaRequiresUserInteraction(Mobclix.getInstance().rmRequireUserInteraction(MobclixAdView.this.size));
                    }
                    if (MobclixAdView.this.refreshRate == 0 && Mobclix.getInstance().getRefreshTime(MobclixAdView.this.size) >= MobclixAdView.MINIMUM_REFRESH_TIME) {
                        MobclixAdView.this.setRefreshTime(Mobclix.getInstance().getRefreshTime(MobclixAdView.this.size));
                    }
                    MobclixAdView.this.getAd();
                    return;
                }
                Iterator<MobclixAdViewListener> it = MobclixAdView.this.listeners.iterator();
                while (it.hasNext()) {
                    MobclixAdViewListener listener = it.next();
                    if (listener != null) {
                        listener.onFailedLoad(MobclixAdView.this, -999999);
                    }
                }
            }
        }
    }

    private class FetchAdResponseThread extends Mobclix.FetchResponseThread {
        String nextRequestParams = "";

        FetchAdResponseThread(Handler h) {
            super("", h);
        }

        public void run() {
            try {
                if (MobclixAdView.this.utilityView == null || !MobclixAdView.this.utilityView.hasLoadedAd() || !MobclixAdView.this.utilityView.hasUndisplayedAd()) {
                    MobclixAdView.this.utilityView.reset();
                    setUrl(getAdUrl());
                    super.run();
                    return;
                }
                MobclixAdView.this.utilityView.postInvalidate();
            } catch (Exception e) {
            }
        }

        /* access modifiers changed from: package-private */
        public void setNextRequestParams(String p) {
            if (p == null) {
                p = "";
            }
            this.nextRequestParams = p;
        }

        private String getAdUrl() {
            String instrPath = MobclixAdView.this.instrumentation.benchmarkStart(MobclixAdView.this.instrumentation.startGroup(MobclixAdView.this.instrumentationGroup, MobclixInstrumentation.ADVIEW), "build_request");
            StringBuffer data = new StringBuffer();
            StringBuffer keywordsBuffer = new StringBuffer();
            String keywords = "";
            StringBuffer queryBuffer = new StringBuffer();
            try {
                Mobclix controller = Mobclix.getInstance();
                instrPath = MobclixAdView.this.instrumentation.benchmarkStart(instrPath, "ad_feed_id_params");
                data.append(controller.getAdServer());
                data.append("?p=android");
                if (MobclixAdView.this.creativeId == null || MobclixAdView.this.creativeId.equals("")) {
                    if (MobclixAdView.this.adCode.equals("")) {
                        data.append("&i=").append(URLEncoder.encode(controller.getApplicationId(), "UTF-8"));
                        data.append("&s=").append(URLEncoder.encode(MobclixAdView.this.size, "UTF-8"));
                    } else {
                        data.append("&a=").append(URLEncoder.encode(MobclixAdView.this.adCode, "UTF-8"));
                    }
                    if (MobclixAdView.this.requestedAdUrlForAdView != null && !MobclixAdView.this.requestedAdUrlForAdView.equals("")) {
                        data.append("&adurl=").append(URLEncoder.encode(MobclixAdView.this.requestedAdUrlForAdView, "UTF-8"));
                    }
                } else {
                    data.append("&cr=").append(URLEncoder.encode(MobclixAdView.this.creativeId, "UTF-8"));
                }
                data.append("&rt=").append(URLEncoder.encode(controller.getRuntimePlatform(), "UTF-8"));
                data.append("&rtv=").append(URLEncoder.encode(controller.getRuntimePlatformVersion(), "UTF-8"));
                String instrPath2 = MobclixAdView.this.instrumentation.benchmarkStart(MobclixAdView.this.instrumentation.benchmarkFinishPath(instrPath), "software_env");
                data.append("&av=").append(URLEncoder.encode(controller.getApplicationVersion(), "UTF-8"));
                data.append("&u=").append(URLEncoder.encode(controller.getDeviceId(), "UTF-8"));
                data.append("&andid=").append(URLEncoder.encode(controller.getAndroidId(), "UTF-8"));
                data.append("&v=").append(URLEncoder.encode(controller.getMobclixVersion()));
                data.append("&ct=").append(URLEncoder.encode(controller.getConnectionType()));
                String instrPath3 = MobclixAdView.this.instrumentation.benchmarkStart(MobclixAdView.this.instrumentation.benchmarkFinishPath(instrPath2), "hardware_env");
                data.append("&dm=").append(URLEncoder.encode(controller.getDeviceModel(), "UTF-8"));
                data.append("&hwdm=").append(URLEncoder.encode(controller.getDeviceHardwareModel(), "UTF-8"));
                data.append("&sv=").append(URLEncoder.encode(controller.getAndroidVersion(), "UTF-8"));
                data.append("&ua=").append(URLEncoder.encode(controller.getUserAgent(), "UTF-8"));
                if (controller.isRootedSet()) {
                    if (controller.isDeviceRooted()) {
                        data.append("&jb=1");
                    } else {
                        data.append("&jb=0");
                    }
                }
                String instrPath4 = MobclixAdView.this.instrumentation.benchmarkStart(MobclixAdView.this.instrumentation.benchmarkFinishPath(instrPath3), "ad_view_state_id_params");
                data.append("&o=").append(MobclixAdView.this.ordinal);
                MobclixAdView.this.ordinal++;
                if (MobclixAdView.this.allowAutoplay != 1 || !Mobclix.getInstance().hasBeenIntervalSinceLastAutoplay(MobclixAdView.this.size)) {
                    data.append("&ap=0");
                } else {
                    data.append("&ap=1");
                }
                if (MobclixAdView.this.adSpace != null && !MobclixAdView.this.adSpace.equals("")) {
                    data.append("&as=").append(URLEncoder.encode(MobclixAdView.this.adSpace));
                }
                if (MobclixAdView.this.testMode) {
                    data.append("&t=1");
                }
                String instrPath5 = MobclixAdView.this.instrumentation.benchmarkStart(MobclixAdView.this.instrumentation.benchmarkFinishPath(instrPath4), "geo_lo");
                if (!controller.getGPS().equals("null")) {
                    data.append("&ll=").append(URLEncoder.encode(controller.getGPS(), "UTF-8"));
                }
                data.append("&l=").append(URLEncoder.encode(controller.getLocale(), "UTF-8"));
                String instrPath6 = MobclixAdView.this.instrumentation.benchmarkFinishPath(instrPath5);
                if (controller.getMcc() != null && !controller.getMcc().equals("null")) {
                    data.append("&mcc=").append(URLEncoder.encode(controller.getMcc(), "UTF-8"));
                }
                if (controller.getMnc() != null && !controller.getMnc().equals("null")) {
                    data.append("&mnc=").append(URLEncoder.encode(controller.getMnc(), "UTF-8"));
                }
                String instrPath7 = MobclixAdView.this.instrumentation.benchmarkStart(instrPath6, "keywords");
                Iterator<MobclixAdViewListener> it = MobclixAdView.this.listeners.iterator();
                while (it.hasNext()) {
                    MobclixAdViewListener listener = it.next();
                    if (listener != null) {
                        keywords = listener.keywords();
                    }
                    if (keywords == null) {
                        keywords = "";
                    }
                    if (!keywords.equals("")) {
                        if (keywordsBuffer.length() == 0) {
                            keywordsBuffer.append("&k=").append(URLEncoder.encode(keywords, "UTF-8"));
                        } else {
                            keywordsBuffer.append("%2C").append(URLEncoder.encode(keywords, "UTF-8"));
                        }
                    }
                    String query = listener.query();
                    if (query == null) {
                        query = "";
                    }
                    if (!query.equals("")) {
                        if (queryBuffer.length() == 0) {
                            queryBuffer.append("&q=").append(URLEncoder.encode(query, "UTF-8"));
                        } else {
                            queryBuffer.append("%2B").append(URLEncoder.encode(query, "UTF-8"));
                        }
                    }
                }
                if (keywordsBuffer.length() > 0) {
                    data.append(keywordsBuffer);
                }
                String instrPath8 = MobclixAdView.this.instrumentation.benchmarkStart(MobclixAdView.this.instrumentation.benchmarkFinishPath(instrPath7), "query");
                if (queryBuffer.length() > 0) {
                    data.append(queryBuffer);
                }
                String instrPath9 = MobclixAdView.this.instrumentation.benchmarkStart(MobclixAdView.this.instrumentation.benchmarkFinishPath(instrPath8), "additional_params");
                if (!this.nextRequestParams.equals("")) {
                    data.append(this.nextRequestParams);
                }
                this.nextRequestParams = "";
                if (MobclixDemographics.demo != null) {
                    if (MobclixDemographics.demo.containsKey(MobclixDemographics.Birthdate)) {
                        data.append("&d=").append(URLEncoder.encode(MobclixDemographics.demo.get(MobclixDemographics.Birthdate), "UTF-8"));
                    }
                    if (MobclixDemographics.demo.containsKey(MobclixDemographics.Education)) {
                        data.append("&e=").append(URLEncoder.encode(MobclixDemographics.demo.get(MobclixDemographics.Education), "UTF-8"));
                    }
                    if (MobclixDemographics.demo.containsKey(MobclixDemographics.Ethnicity)) {
                        data.append("&r=").append(URLEncoder.encode(MobclixDemographics.demo.get(MobclixDemographics.Ethnicity), "UTF-8"));
                    }
                    if (MobclixDemographics.demo.containsKey(MobclixDemographics.Gender)) {
                        data.append("&g=").append(URLEncoder.encode(MobclixDemographics.demo.get(MobclixDemographics.Gender), "UTF-8"));
                    }
                    if (MobclixDemographics.demo.containsKey(MobclixDemographics.DatingGender)) {
                        data.append("&dg=").append(URLEncoder.encode(MobclixDemographics.demo.get(MobclixDemographics.DatingGender), "UTF-8"));
                    }
                    if (MobclixDemographics.demo.containsKey(MobclixDemographics.Income)) {
                        data.append("&m=").append(URLEncoder.encode(MobclixDemographics.demo.get(MobclixDemographics.Income), "UTF-8"));
                    }
                    if (MobclixDemographics.demo.containsKey(MobclixDemographics.MaritalStatus)) {
                        data.append("&x=").append(URLEncoder.encode(MobclixDemographics.demo.get(MobclixDemographics.MaritalStatus), "UTF-8"));
                    }
                    if (MobclixDemographics.demo.containsKey(MobclixDemographics.Religion)) {
                        data.append("&j=").append(URLEncoder.encode(MobclixDemographics.demo.get(MobclixDemographics.Religion), "UTF-8"));
                    }
                    if (MobclixDemographics.demo.containsKey(MobclixDemographics.AreaCode)) {
                        data.append("&c=").append(URLEncoder.encode(MobclixDemographics.demo.get(MobclixDemographics.AreaCode), "UTF-8"));
                    }
                    if (MobclixDemographics.demo.containsKey(MobclixDemographics.City)) {
                        data.append("&ci=").append(URLEncoder.encode(MobclixDemographics.demo.get(MobclixDemographics.City), "UTF-8"));
                    }
                    if (MobclixDemographics.demo.containsKey(MobclixDemographics.Country)) {
                        data.append("&co=").append(URLEncoder.encode(MobclixDemographics.demo.get(MobclixDemographics.Country), "UTF-8"));
                    }
                    if (MobclixDemographics.demo.containsKey(MobclixDemographics.DMACode)) {
                        data.append("&dc=").append(URLEncoder.encode(MobclixDemographics.demo.get(MobclixDemographics.DMACode), "UTF-8"));
                    }
                    if (MobclixDemographics.demo.containsKey(MobclixDemographics.PostalCode)) {
                        data.append("&z=").append(URLEncoder.encode(MobclixDemographics.demo.get(MobclixDemographics.PostalCode), "UTF-8"));
                    }
                    if (MobclixDemographics.demo.containsKey(MobclixDemographics.Region)) {
                        data.append("&re=").append(URLEncoder.encode(MobclixDemographics.demo.get(MobclixDemographics.Region), "UTF-8"));
                    }
                }
                String instrPath10 = MobclixAdView.this.instrumentation.benchmarkFinishPath(MobclixAdView.this.instrumentation.benchmarkFinishPath(instrPath9));
                MobclixAdView.this.instrumentation.addInfo(data.toString(), "request_url", MobclixAdView.this.instrumentationGroup);
                return data.toString();
            } catch (Exception e) {
                String instrPath11 = MobclixAdView.this.instrumentation.benchmarkFinishPath(MobclixAdView.this.instrumentation.benchmarkFinishPath(instrPath));
                MobclixAdView.this.instrumentation.finishGroup(MobclixAdView.this.instrumentationGroup);
                return "";
            }
        }
    }

    private class AdResponseHandler extends Handler {
        private AdResponseHandler() {
        }

        /* synthetic */ AdResponseHandler(MobclixAdView mobclixAdView, AdResponseHandler adResponseHandler) {
            this();
        }

        public void handleMessage(Message msg) {
            if (!MobclixAdView.this.detached) {
                String type = msg.getData().getString("type");
                if (type.equals("success")) {
                    String instrPath = MobclixAdView.this.instrumentation.benchmarkStart(MobclixAdView.this.instrumentation.startGroup(MobclixAdView.this.instrumentationGroup, MobclixInstrumentation.ADVIEW), "handle_response");
                    if (MobclixAdView.this.ad != null) {
                        MobclixAdView.this.prevAd = MobclixAdView.this.ad;
                    }
                    try {
                        String instrPath2 = MobclixAdView.this.instrumentation.benchmarkStart(instrPath, "a_decode_json");
                        MobclixAdView.this.rawResponse = msg.getData().getString("response");
                        MobclixAdView.this.instrumentation.addInfo(MobclixAdView.this.rawResponse, "raw_json", MobclixAdView.this.instrumentationGroup);
                        MobclixAdView.this.creativeManager = new MobclixCreativeManager(MobclixAdView.this.rawResponse, 0);
                        if (MobclixAdView.this.creativeManager.length() >= 1) {
                            MobclixAdView.this.instrumentation.addInfo(MobclixAdView.this.creativeManager.getCreative(), "decoded_json", MobclixAdView.this.instrumentationGroup);
                            MobclixAdView.this.ad = new MobclixCreative(MobclixAdView.this, MobclixAdView.this.creativeManager.getCreative(), false);
                            if (!MobclixAdView.this.ad.isInitialized()) {
                                MobclixAdView.this.getNextAd("");
                            }
                        }
                        String instrPath3 = MobclixAdView.this.instrumentation.benchmarkFinishPath(instrPath2);
                    } catch (Exception e) {
                        String instrPath4 = MobclixAdView.this.instrumentation.benchmarkFinishPath(MobclixAdView.this.instrumentation.benchmarkFinishPath(instrPath));
                        MobclixAdView.this.instrumentation.finishGroup(MobclixAdView.this.instrumentationGroup);
                        MobclixAdView.this.getNextAd("");
                    }
                } else if (type.equals("failure")) {
                    int errorCode = msg.getData().getInt("errorCode");
                    Iterator<MobclixAdViewListener> it = MobclixAdView.this.listeners.iterator();
                    while (it.hasNext()) {
                        MobclixAdViewListener listener = it.next();
                        if (listener != null) {
                            listener.onFailedLoad(MobclixAdView.this, errorCode);
                        }
                    }
                    MobclixAdView.this.lastAdLoad = 0;
                }
            }
        }
    }

    class MobclixUtilityView extends View {
        boolean loaded = false;
        boolean undisplayed = false;

        /* access modifiers changed from: package-private */
        public void reset() {
            this.loaded = false;
            this.undisplayed = false;
        }

        /* access modifiers changed from: package-private */
        public boolean hasLoadedAd() {
            return this.loaded;
        }

        /* access modifiers changed from: package-private */
        public boolean hasUndisplayedAd() {
            return this.undisplayed;
        }

        public MobclixUtilityView(Context context) {
            super(context);
        }

        public void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            if (this.undisplayed) {
                Mobclix.getInstance().sessionEvent();
                this.loaded = false;
                this.undisplayed = false;
            }
        }
    }
}
