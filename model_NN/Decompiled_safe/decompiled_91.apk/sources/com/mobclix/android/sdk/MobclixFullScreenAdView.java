package com.mobclix.android.sdk;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.mobclix.android.sdk.Mobclix;
import com.mobclix.android.sdk.MobclixCreative;
import java.lang.ref.SoftReference;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

public class MobclixFullScreenAdView {
    static int debugOrdinal = 0;
    private String TAG = "MobclixFullScreenAdView";
    Activity activity = null;
    MobclixWebView ad = null;
    String adCode = "";
    /* access modifiers changed from: private */
    public Thread adThread;
    int backgroundColor = 0;
    Mobclix controller = null;
    /* access modifiers changed from: private */
    public String creativeId = "";
    JSONArray creatives = null;
    final AdResponseHandler handler = new AdResponseHandler(this, null);
    boolean hasAd = false;
    /* access modifiers changed from: private */
    public MobclixInstrumentation instrumentation = MobclixInstrumentation.getInstance();
    String instrumentationGroup = null;
    HashSet<MobclixFullScreenAdViewListener> listeners = new HashSet<>();
    Object lock = new Object();
    MobclixCreative.HTMLPagePool mHTMLPagePool;
    int nCreative = 0;
    /* access modifiers changed from: private */
    public ArrayList<String> onLoadUrls;
    /* access modifiers changed from: private */
    public ArrayList<String> onTouchUrls;
    String rawResponse = null;
    final RemoteConfigReadyHandler rcHandler = new RemoteConfigReadyHandler(this, null);
    boolean requestAndDisplayAd = false;
    String requestedAdUrlForAdView = null;
    float scale = 1.0f;
    int screenHeight;
    int screenWidth;

    public boolean hasAd() {
        return this.hasAd;
    }

    /* access modifiers changed from: package-private */
    public Activity getActivity() {
        return this.activity;
    }

    public MobclixFullScreenAdView(Activity a) {
        initialize(a);
    }

    public void requestAd() {
        if (!this.hasAd && this.adThread == null) {
            new Thread(new WaitForRemoteConfigThread(this, null)).start();
        }
    }

    public boolean displayRequestedAd() {
        if (!this.hasAd || this.ad == null) {
            Log.e(this.TAG, "FullScreen Ad did not display, ad not yet loaded.");
            return false;
        }
        if (this.ad.failedVideoAttempt) {
            this.ad.loadAd();
        }
        Mobclix.getInstance().webview = new SoftReference<>(this.ad);
        Intent mIntent = new Intent();
        String packageName = this.activity.getPackageName();
        mIntent.setClassName(packageName, MobclixBrowserActivity.class.getName()).putExtra(String.valueOf(packageName) + ".type", "fullscreen");
        this.activity.startActivity(mIntent);
        Iterator<MobclixFullScreenAdViewListener> it = this.listeners.iterator();
        while (it.hasNext()) {
            MobclixFullScreenAdViewListener listener = it.next();
            if (listener != null) {
                listener.onPresentAd(this);
            }
        }
        Iterator<String> it2 = this.onLoadUrls.iterator();
        while (it2.hasNext()) {
            new Thread(new Mobclix.FetchImageThread(it2.next(), new Mobclix.BitmapHandler())).start();
        }
        this.ad = null;
        this.hasAd = false;
        this.requestAndDisplayAd = false;
        this.onLoadUrls = null;
        this.onTouchUrls = null;
        return true;
    }

    public void requestAndDisplayAd() {
        if (this.hasAd) {
            displayRequestedAd();
        } else if (this.adThread == null) {
            this.requestAndDisplayAd = true;
            new Thread(new WaitForRemoteConfigThread(this, null)).start();
        }
    }

    public void setCreativeId(String c) {
        this.creativeId = c;
    }

    public String getCreativeId() {
        return this.creativeId;
    }

    public void setRequestedAdUrlForAdView(String url) {
        this.requestedAdUrlForAdView = url;
    }

    public String getRequestedAdUrlForAdView() {
        return this.requestedAdUrlForAdView;
    }

    public boolean addMobclixAdViewListener(MobclixFullScreenAdViewListener l) {
        if (l == null) {
            return false;
        }
        return this.listeners.add(l);
    }

    public boolean removeMobclixAdViewListener(MobclixFullScreenAdViewListener l) {
        return this.listeners.remove(l);
    }

    private void initialize(Activity a) {
        synchronized (this.lock) {
            this.instrumentationGroup = String.valueOf(MobclixInstrumentation.ADVIEW) + "_fullscreen_" + (debugOrdinal + 1);
            debugOrdinal++;
        }
        this.activity = a;
        Mobclix.onCreate(a);
        try {
            this.controller = Mobclix.getInstance();
            if (this.controller.getUserAgent().equals("null")) {
                WebSettings settings = new WebView(a.getApplicationContext()).getSettings();
                try {
                    this.controller.setUserAgent((String) settings.getClass().getDeclaredMethod("getUserAgentString", new Class[0]).invoke(settings, new Object[0]));
                } catch (Exception e) {
                    this.controller.setUserAgent("Mozilla/5.0 (Linux; U; Android 1.1; en-us; dream) AppleWebKit/525.10+ (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2");
                }
            }
            this.mHTMLPagePool = new MobclixCreative.HTMLPagePool();
        } catch (Exception e2) {
        }
        DisplayMetrics dm = new DisplayMetrics();
        a.getWindowManager().getDefaultDisplay().getMetrics(dm);
        this.screenWidth = dm.widthPixels;
        this.screenHeight = dm.heightPixels;
        this.scale = a.getResources().getDisplayMetrics().density;
        CookieManager.getInstance().setAcceptCookie(true);
    }

    private class WaitForRemoteConfigThread implements Runnable {
        private WaitForRemoteConfigThread() {
        }

        /* synthetic */ WaitForRemoteConfigThread(MobclixFullScreenAdView mobclixFullScreenAdView, WaitForRemoteConfigThread waitForRemoteConfigThread) {
            this();
        }

        public void run() {
            Long t = Long.valueOf(System.currentTimeMillis());
            do {
                try {
                    if (MobclixFullScreenAdView.this.controller.isRemoteConfigSet() == 1) {
                        break;
                    }
                } catch (Exception e) {
                    return;
                }
            } while (System.currentTimeMillis() - t.longValue() < 10000);
            MobclixFullScreenAdView.this.rcHandler.sendEmptyMessage(0);
        }
    }

    private class RemoteConfigReadyHandler extends Handler {
        private RemoteConfigReadyHandler() {
        }

        /* synthetic */ RemoteConfigReadyHandler(MobclixFullScreenAdView mobclixFullScreenAdView, RemoteConfigReadyHandler remoteConfigReadyHandler) {
            this();
        }

        public void handleMessage(Message msg) {
            MobclixFullScreenAdView.this.onLoadUrls = new ArrayList();
            MobclixFullScreenAdView.this.onTouchUrls = new ArrayList();
            MobclixFullScreenAdView.this.getAd();
        }
    }

    /* access modifiers changed from: package-private */
    public void getAd() {
        if (this.adThread != null) {
            this.adThread.interrupt();
            this.adThread = null;
        }
        String instrPath = this.instrumentation.benchmarkStart(this.instrumentation.startGroup(this.instrumentationGroup, MobclixInstrumentation.ADVIEW), "start_request");
        this.adThread = new Thread(new FetchAdResponseThread(this.handler));
        this.adThread.start();
        String instrPath2 = this.instrumentation.benchmarkFinishPath(instrPath);
    }

    /* access modifiers changed from: package-private */
    public void onPageFinished(MobclixWebView webview) {
        this.hasAd = true;
        this.ad = webview;
        Iterator<MobclixFullScreenAdViewListener> it = this.listeners.iterator();
        while (it.hasNext()) {
            MobclixFullScreenAdViewListener listener = it.next();
            if (listener != null) {
                listener.onFinishLoad(this);
            }
        }
        if (this.requestAndDisplayAd) {
            displayRequestedAd();
        }
    }

    private class FetchAdResponseThread extends Mobclix.FetchResponseThread {
        String nextRequestParams = "";

        FetchAdResponseThread(Handler h) {
            super("", h);
        }

        public void run() {
            setUrl(getAdUrl());
            super.run();
        }

        private String getAdUrl() {
            String instrPath = MobclixFullScreenAdView.this.instrumentation.benchmarkStart(MobclixFullScreenAdView.this.instrumentation.startGroup(MobclixFullScreenAdView.this.instrumentationGroup, MobclixInstrumentation.ADVIEW), "build_request");
            StringBuffer data = new StringBuffer();
            StringBuffer keywordsBuffer = new StringBuffer();
            String keywords = "";
            StringBuffer queryBuffer = new StringBuffer();
            try {
                instrPath = MobclixFullScreenAdView.this.instrumentation.benchmarkStart(instrPath, "ad_feed_id_params");
                data.append(MobclixFullScreenAdView.this.controller.getAdServer());
                data.append("?p=android");
                if (MobclixFullScreenAdView.this.creativeId == null || MobclixFullScreenAdView.this.creativeId.equals("")) {
                    if (MobclixFullScreenAdView.this.adCode.equals("")) {
                        data.append("&i=").append(URLEncoder.encode(MobclixFullScreenAdView.this.controller.getApplicationId(), "UTF-8"));
                        data.append("&s=").append(URLEncoder.encode("fullscreen", "UTF-8"));
                    } else {
                        data.append("&a=").append(URLEncoder.encode(MobclixFullScreenAdView.this.adCode, "UTF-8"));
                    }
                    if (MobclixFullScreenAdView.this.requestedAdUrlForAdView != null && !MobclixFullScreenAdView.this.requestedAdUrlForAdView.equals("")) {
                        data.append("&adurl=").append(URLEncoder.encode(MobclixFullScreenAdView.this.requestedAdUrlForAdView, "UTF-8"));
                    }
                } else {
                    data.append("&cr=").append(URLEncoder.encode(MobclixFullScreenAdView.this.creativeId, "UTF-8"));
                }
                data.append("&rt=").append(URLEncoder.encode(MobclixFullScreenAdView.this.controller.getRuntimePlatform(), "UTF-8"));
                data.append("&rtv=").append(URLEncoder.encode(MobclixFullScreenAdView.this.controller.getRuntimePlatformVersion(), "UTF-8"));
                String instrPath2 = MobclixFullScreenAdView.this.instrumentation.benchmarkStart(MobclixFullScreenAdView.this.instrumentation.benchmarkFinishPath(instrPath), "software_env");
                data.append("&av=").append(URLEncoder.encode(MobclixFullScreenAdView.this.controller.getApplicationVersion(), "UTF-8"));
                data.append("&u=").append(URLEncoder.encode(MobclixFullScreenAdView.this.controller.getDeviceId(), "UTF-8"));
                data.append("&andid=").append(URLEncoder.encode(MobclixFullScreenAdView.this.controller.getAndroidId(), "UTF-8"));
                data.append("&v=").append(URLEncoder.encode(MobclixFullScreenAdView.this.controller.getMobclixVersion(), "UTF-8"));
                data.append("&ct=").append(URLEncoder.encode(MobclixFullScreenAdView.this.controller.getConnectionType()));
                String instrPath3 = MobclixFullScreenAdView.this.instrumentation.benchmarkStart(MobclixFullScreenAdView.this.instrumentation.benchmarkFinishPath(instrPath2), "hardware_env");
                data.append("&dm=").append(URLEncoder.encode(MobclixFullScreenAdView.this.controller.getDeviceModel(), "UTF-8"));
                data.append("&hwdm=").append(URLEncoder.encode(MobclixFullScreenAdView.this.controller.getDeviceHardwareModel(), "UTF-8"));
                data.append("&sv=").append(URLEncoder.encode(MobclixFullScreenAdView.this.controller.getAndroidVersion(), "UTF-8"));
                data.append("&ua=").append(URLEncoder.encode(MobclixFullScreenAdView.this.controller.getUserAgent(), "UTF-8"));
                if (MobclixFullScreenAdView.this.controller.isRootedSet()) {
                    if (MobclixFullScreenAdView.this.controller.isDeviceRooted()) {
                        data.append("&jb=1");
                    } else {
                        data.append("&jb=0");
                    }
                }
                String instrPath4 = MobclixFullScreenAdView.this.instrumentation.benchmarkStart(MobclixFullScreenAdView.this.instrumentation.benchmarkFinishPath(MobclixFullScreenAdView.this.instrumentation.benchmarkStart(MobclixFullScreenAdView.this.instrumentation.benchmarkFinishPath(instrPath3), "ad_view_state_id_params")), "geo_lo");
                if (!MobclixFullScreenAdView.this.controller.getGPS().equals("null")) {
                    data.append("&ll=").append(URLEncoder.encode(MobclixFullScreenAdView.this.controller.getGPS(), "UTF-8"));
                }
                data.append("&l=").append(URLEncoder.encode(MobclixFullScreenAdView.this.controller.getLocale(), "UTF-8"));
                String instrPath5 = MobclixFullScreenAdView.this.instrumentation.benchmarkStart(MobclixFullScreenAdView.this.instrumentation.benchmarkFinishPath(instrPath4), "keywords");
                Iterator<MobclixFullScreenAdViewListener> it = MobclixFullScreenAdView.this.listeners.iterator();
                while (it.hasNext()) {
                    MobclixFullScreenAdViewListener listener = it.next();
                    if (listener != null) {
                        keywords = listener.keywords();
                    }
                    if (keywords == null) {
                        keywords = "";
                    }
                    if (!keywords.equals("")) {
                        if (keywordsBuffer.length() == 0) {
                            keywordsBuffer.append("&k=").append(URLEncoder.encode(keywords, "UTF-8"));
                        } else {
                            keywordsBuffer.append("%2C").append(URLEncoder.encode(keywords, "UTF-8"));
                        }
                    }
                    String query = listener.query();
                    if (query == null) {
                        query = "";
                    }
                    if (!query.equals("")) {
                        if (queryBuffer.length() == 0) {
                            queryBuffer.append("&q=").append(URLEncoder.encode(query, "UTF-8"));
                        } else {
                            queryBuffer.append("%2B").append(URLEncoder.encode(query, "UTF-8"));
                        }
                    }
                }
                if (keywordsBuffer.length() > 0) {
                    data.append(keywordsBuffer);
                }
                String instrPath6 = MobclixFullScreenAdView.this.instrumentation.benchmarkStart(MobclixFullScreenAdView.this.instrumentation.benchmarkFinishPath(instrPath5), "query");
                if (queryBuffer.length() > 0) {
                    data.append(queryBuffer);
                }
                String instrPath7 = MobclixFullScreenAdView.this.instrumentation.benchmarkStart(MobclixFullScreenAdView.this.instrumentation.benchmarkFinishPath(instrPath6), "query");
                if (!this.nextRequestParams.equals("")) {
                    data.append(this.nextRequestParams);
                }
                this.nextRequestParams = "";
                String instrPath8 = MobclixFullScreenAdView.this.instrumentation.benchmarkFinishPath(MobclixFullScreenAdView.this.instrumentation.benchmarkFinishPath(instrPath7));
                MobclixFullScreenAdView.this.instrumentation.addInfo(data.toString(), "request_url", MobclixFullScreenAdView.this.instrumentationGroup);
                return data.toString();
            } catch (Exception e) {
                String instrPath9 = MobclixFullScreenAdView.this.instrumentation.benchmarkFinishPath(MobclixFullScreenAdView.this.instrumentation.benchmarkFinishPath(instrPath));
                MobclixFullScreenAdView.this.instrumentation.finishGroup(MobclixFullScreenAdView.this.instrumentationGroup);
                return "";
            }
        }
    }

    private class AdResponseHandler extends Handler {
        private AdResponseHandler() {
        }

        /* synthetic */ AdResponseHandler(MobclixFullScreenAdView mobclixFullScreenAdView, AdResponseHandler adResponseHandler) {
            this();
        }

        public void handleMessage(Message msg) {
            MobclixFullScreenAdView.this.adThread = null;
            String type = msg.getData().getString("type");
            if (type.equals("success")) {
                String instrPath = MobclixFullScreenAdView.this.instrumentation.benchmarkStart(MobclixFullScreenAdView.this.instrumentation.startGroup(MobclixFullScreenAdView.this.instrumentationGroup, MobclixInstrumentation.ADVIEW), "handle_response");
                try {
                    String instrPath2 = MobclixFullScreenAdView.this.instrumentation.benchmarkStart(instrPath, "a_decode_json");
                    MobclixFullScreenAdView.this.rawResponse = msg.getData().getString("response");
                    MobclixFullScreenAdView.this.instrumentation.addInfo(MobclixFullScreenAdView.this.rawResponse, "raw_json", MobclixFullScreenAdView.this.instrumentationGroup);
                    MobclixFullScreenAdView.this.creatives = new JSONObject(MobclixFullScreenAdView.this.rawResponse).getJSONArray("creatives");
                    if (MobclixFullScreenAdView.this.creatives.length() >= 1) {
                        MobclixFullScreenAdView.this.nCreative = 0;
                        MobclixFullScreenAdView.this.instrumentation.addInfo(MobclixFullScreenAdView.this.creatives.getJSONObject(MobclixFullScreenAdView.this.nCreative), "decoded_json", MobclixFullScreenAdView.this.instrumentationGroup);
                        JSONObject creative = MobclixFullScreenAdView.this.creatives.getJSONObject(MobclixFullScreenAdView.this.nCreative);
                        String instrPath3 = MobclixFullScreenAdView.this.instrumentation.benchmarkStart(instrPath2, "b_build_models");
                        try {
                            JSONObject eventUrls = creative.getJSONObject("eventUrls");
                            try {
                                JSONArray t = eventUrls.getJSONArray("onShow");
                                for (int i = 0; i < t.length(); i++) {
                                    MobclixFullScreenAdView.this.onLoadUrls.add(t.getString(i));
                                }
                            } catch (Exception e) {
                            }
                            JSONArray t2 = eventUrls.getJSONArray("onTouch");
                            for (int i2 = 0; i2 < t2.length(); i2++) {
                                MobclixFullScreenAdView.this.onTouchUrls.add(t2.getString(i2));
                            }
                        } catch (Exception e2) {
                        }
                        String instrPath4 = MobclixFullScreenAdView.this.instrumentation.benchmarkStart(MobclixFullScreenAdView.this.instrumentation.benchmarkStart(MobclixFullScreenAdView.this.instrumentation.benchmarkFinishPath(instrPath3), "c_build_creative"), "a_determine_type");
                        JSONObject properties = creative.getJSONObject("props");
                        String type2 = creative.getString("type");
                        instrPath = MobclixFullScreenAdView.this.instrumentation.benchmarkStart(MobclixFullScreenAdView.this.instrumentation.benchmarkFinishPath(instrPath4), "b_get_view");
                        if (type2.equals("html")) {
                            MobclixFullScreenAdView.this.mHTMLPagePool.getHTMLPage(MobclixFullScreenAdView.this).loadAd(properties.getString("html"));
                            instrPath2 = MobclixFullScreenAdView.this.instrumentation.benchmarkFinishPath(MobclixFullScreenAdView.this.instrumentation.benchmarkFinishPath(instrPath));
                        } else {
                            throw new Exception("Unsupported ad type");
                        }
                    }
                    String instrPath5 = MobclixFullScreenAdView.this.instrumentation.benchmarkFinishPath(instrPath2);
                } catch (Exception e3) {
                    String instrPath6 = MobclixFullScreenAdView.this.instrumentation.benchmarkFinishPath(MobclixFullScreenAdView.this.instrumentation.benchmarkFinishPath(MobclixFullScreenAdView.this.instrumentation.benchmarkFinishPath(instrPath)));
                    MobclixFullScreenAdView.this.instrumentation.finishGroup(MobclixFullScreenAdView.this.instrumentationGroup);
                    Iterator<MobclixFullScreenAdViewListener> it = MobclixFullScreenAdView.this.listeners.iterator();
                    while (it.hasNext()) {
                        MobclixFullScreenAdViewListener listener = it.next();
                        if (listener != null) {
                            listener.onFailedLoad(MobclixFullScreenAdView.this, 0);
                        }
                    }
                }
            } else if (type.equals("failure")) {
                int errorCode = msg.getData().getInt("errorCode");
                Iterator<MobclixFullScreenAdViewListener> it2 = MobclixFullScreenAdView.this.listeners.iterator();
                while (it2.hasNext()) {
                    MobclixFullScreenAdViewListener listener2 = it2.next();
                    if (listener2 != null) {
                        listener2.onFailedLoad(MobclixFullScreenAdView.this, errorCode);
                    }
                }
            }
        }
    }
}
