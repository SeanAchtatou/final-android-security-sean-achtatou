package com.mobclix.android.sdk;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.VideoView;
import java.lang.ref.SoftReference;

class MobclixWebView extends WebView implements View.OnTouchListener {
    private static final String TAG = "MobclixWebView";
    boolean displayed = false;
    boolean failedVideoAttempt = false;
    MobclixFullScreenAdView fullScreenAdView = null;
    private String html = null;
    MobclixJavascriptInterface jsInterface = null;
    boolean loaded = false;
    Object mCustomViewCallback = null;
    MobclixCreative parentCreative = null;
    boolean touched = false;
    VideoView video = null;

    public MobclixWebView(MobclixCreative creative) {
        super(creative.parentAdView.getContext());
        this.parentCreative = creative;
        setOnTouchListener(this);
    }

    public MobclixWebView(MobclixFullScreenAdView adview) {
        super(adview.getActivity());
        this.fullScreenAdView = adview;
    }

    public Context getWorkingContext() {
        if (this.parentCreative != null) {
            return this.parentCreative.parentAdView.getContext();
        }
        if (this.fullScreenAdView != null) {
            return this.fullScreenAdView.getActivity();
        }
        return getContext();
    }

    public Context getTopContext() {
        Context currentActivity = getWorkingContext();
        if (this.jsInterface == null || this.jsInterface.expanderActivity == null) {
            return currentActivity;
        }
        return this.jsInterface.expanderActivity.getContext();
    }

    public void setJavascriptInterface(MobclixJavascriptInterface jsInt) {
        this.jsInterface = jsInt;
    }

    public MobclixJavascriptInterface getJavascriptInterface() {
        return this.jsInterface;
    }

    public void setAdHtml(String h) {
        this.html = h;
    }

    public void loadAd() {
        try {
            loadDataWithBaseURL(null, this.html, "text/html", "utf-8", null);
        } catch (Exception e) {
        }
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.displayed) {
            this.displayed = true;
            try {
                this.parentCreative.fireOnShowTrackingPixels();
            } catch (Exception e) {
            }
            if (this.jsInterface != null) {
                this.jsInterface.adDidDisplay();
            }
        }
    }

    public boolean onTouch(View v, MotionEvent e) {
        try {
            if (!this.touched) {
                this.parentCreative.fireOnTouchTrackingPixels();
            }
        } catch (Exception e2) {
        }
        this.touched = true;
        return false;
    }

    /* access modifiers changed from: package-private */
    public void showCustomViewVideo(View view, Object callback) {
        Mobclix.getInstance().cameraWebview = new SoftReference<>(this);
        Mobclix.getInstance().secondaryView = new SoftReference<>(view);
        Intent mIntent = new Intent();
        String packageName = getContext().getPackageName();
        mIntent.setClassName(packageName, MobclixBrowserActivity.class.getName()).putExtra(String.valueOf(packageName) + ".type", "html5Video");
        getWorkingContext().startActivity(mIntent);
        this.mCustomViewCallback = callback;
    }

    /* access modifiers changed from: package-private */
    public void killCustomViewVideo(View view, Object callback) {
        this.video = (VideoView) ((FrameLayout) view).getFocusedChild();
        if (Integer.parseInt(Build.VERSION.SDK) < 9) {
            this.video.stopPlayback();
            try {
                this.mCustomViewCallback.getClass().getMethod("onCustomViewHidden", new Class[0]).invoke(this.mCustomViewCallback, new Object[0]);
                this.mCustomViewCallback = null;
            } catch (Exception e) {
            }
        } else {
            ((FrameLayout) view).removeView(this.video);
            this.video.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                public boolean onError(MediaPlayer mp, int i, int j) {
                    try {
                        MobclixWebView.this.video.stopPlayback();
                    } catch (Exception e) {
                    }
                    try {
                        MobclixWebView.this.mCustomViewCallback.getClass().getMethod("onCustomViewHidden", new Class[0]).invoke(MobclixWebView.this.mCustomViewCallback, new Object[0]);
                        MobclixWebView.this.mCustomViewCallback = null;
                    } catch (Exception e2) {
                        Log.v(MobclixWebView.TAG, e2.toString());
                    }
                    try {
                        ((ViewGroup) MobclixWebView.this.getParent()).removeView(MobclixWebView.this.video);
                        MobclixWebView.this.video = null;
                        return true;
                    } catch (Exception e3) {
                        return true;
                    }
                }
            });
            this.video.setVideoURI(Uri.parse("http://a.mobclix.com/fail"));
            this.video.setLayoutParams(new ViewGroup.LayoutParams(0, 0));
            ((WindowManager) getWorkingContext().getSystemService("window")).addView(this.video, new WindowManager.LayoutParams(1, 1));
            this.video.setMediaController(null);
            this.video.start();
            this.video.stopPlayback();
            this.failedVideoAttempt = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void reset() {
        scrollTo(0, 0);
        clearHistory();
        clearCache(false);
        this.jsInterface.reset();
        this.loaded = false;
        this.displayed = false;
        this.touched = false;
    }
}
