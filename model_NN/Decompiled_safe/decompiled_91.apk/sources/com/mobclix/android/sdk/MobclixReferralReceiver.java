package com.mobclix.android.sdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class MobclixReferralReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String referrerString = intent.getExtras().getString("referrer");
        if (referrerString != null && !referrerString.equals("")) {
            SharedPreferences.Editor preferencesEditor = context.getSharedPreferences(String.valueOf(context.getPackageName()) + ".MCConfig", 0).edit();
            preferencesEditor.putString("MCReferralData", referrerString);
            preferencesEditor.commit();
        }
        try {
            Class<?> InstallReceiver = Class.forName("com.admob.android.ads.analytics.InstallReceiver");
            Object adMobReceiver = InstallReceiver.getConstructor(new Class[0]).newInstance(new Object[0]);
            InstallReceiver.getMethod("onReceive", Context.class, Intent.class).invoke(adMobReceiver, context, intent);
        } catch (Exception e) {
            try {
                Class<?> AnalyticsReceiver = Class.forName("com.google.android.apps.analytics.AnalyticsReceiver");
                Object googleReceiver = AnalyticsReceiver.getConstructor(new Class[0]).newInstance(new Object[0]);
                AnalyticsReceiver.getMethod("onReceive", Context.class, Intent.class).invoke(googleReceiver, context, intent);
            } catch (Exception e2) {
            }
        }
    }
}
