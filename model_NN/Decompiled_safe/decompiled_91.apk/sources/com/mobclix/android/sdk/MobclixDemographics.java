package com.mobclix.android.sdk;

import android.app.Activity;
import java.util.HashMap;
import java.util.Map;

public class MobclixDemographics {
    public static final String AreaCode = "dac";
    public static final String Birthdate = "dbd";
    public static final String City = "dci";
    public static final String Country = "dco";
    public static final String DMACode = "ddc";
    public static final String DatingGender = "ddg";
    public static final String Education = "dec";
    public static final int EducationBachelorsDegree = 4;
    public static final int EducationDoctoralDegree = 6;
    public static final int EducationHighSchool = 1;
    public static final int EducationInCollege = 3;
    public static final int EducationMastersDegree = 5;
    public static final int EducationSomeCollege = 2;
    public static final int EducationUnknown = 0;
    public static final String Ethnicity = "den";
    public static final int EthnicityAsian = 2;
    public static final int EthnicityBlack = 3;
    public static final int EthnicityHispanic = 4;
    public static final int EthnicityMixed = 1;
    public static final int EthnicityNativeAmerican = 5;
    public static final int EthnicityUnknown = 0;
    public static final int EthnicityWhite = 6;
    public static final String Gender = "dg";
    public static final int GenderBoth = 3;
    public static final int GenderFemale = 2;
    public static final int GenderMale = 1;
    public static final int GenderUnknown = 0;
    public static final String Income = "dic";
    public static final int MaritalMarried = 3;
    public static final int MaritalSingleAvailable = 1;
    public static final int MaritalSingleUnavailable = 2;
    public static final String MaritalStatus = "dms";
    public static final int MaritalUnknown = 0;
    public static final String MetroCode = "dmc";
    public static final String PostalCode = "dpo";
    public static final String Region = "drg";
    public static final String Religion = "drl";
    public static final int ReligionBuddhism = 1;
    public static final int ReligionChristianity = 2;
    public static final int ReligionHinduism = 3;
    public static final int ReligionIslam = 4;
    public static final int ReligionJudaism = 5;
    public static final int ReligionOther = 7;
    public static final int ReligionUnaffiliated = 6;
    public static final int ReligionUnknown = 0;
    private static String TAG = "mobclixDemographics";
    static Mobclix controller = Mobclix.getInstance();
    static HashMap<String, String> demo = null;

    public static void sendDemographics(Activity a, Map<String, Object> d) {
        sendDemographics(a, d, null);
    }

    /* JADX INFO: Multiple debug info for r1v22 java.lang.String: [D('isValid' boolean), D('temp' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r1v28 java.lang.String: [D('isValid' boolean), D('temp' java.lang.String)] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x01ad  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x00b1 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void sendDemographics(android.app.Activity r10, java.util.Map<java.lang.String, java.lang.Object> r11, com.mobclix.android.sdk.MobclixFeedback.Listener r12) {
        /*
            if (r11 != 0) goto L_0x0003
        L_0x0002:
            return
        L_0x0003:
            com.mobclix.android.sdk.Mobclix.onCreate(r10)
            java.util.HashMap r3 = new java.util.HashMap
            r3.<init>()
            java.lang.String r1 = "dbd"
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r3.put(r1, r2)
            java.lang.String r1 = "dec"
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r3.put(r1, r2)
            java.lang.String r1 = "den"
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r3.put(r1, r2)
            java.lang.String r1 = "dg"
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r3.put(r1, r2)
            java.lang.String r1 = "ddg"
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r3.put(r1, r2)
            java.lang.String r1 = "dic"
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r3.put(r1, r2)
            java.lang.String r1 = "dms"
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r3.put(r1, r2)
            java.lang.String r1 = "drl"
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r3.put(r1, r2)
            java.lang.String r1 = "dac"
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r3.put(r1, r2)
            java.lang.String r1 = "dci"
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r3.put(r1, r2)
            java.lang.String r1 = "dco"
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r3.put(r1, r2)
            java.lang.String r1 = "ddc"
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r3.put(r1, r2)
            java.lang.String r1 = "dmc"
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r3.put(r1, r2)
            java.lang.String r1 = "dpo"
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r3.put(r1, r2)
            java.lang.String r1 = "drg"
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r3.put(r1, r2)
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
            com.mobclix.android.sdk.MobclixDemographics.demo = r1
            r1 = 0
            java.util.Set r2 = r11.keySet()
            java.util.Iterator r8 = r2.iterator()
        L_0x00b1:
            boolean r1 = r8.hasNext()
            if (r1 != 0) goto L_0x0194
            com.mobclix.android.sdk.Mobclix r11 = com.mobclix.android.sdk.MobclixDemographics.controller
            java.lang.String r2 = r11.getFeedbackServer()
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
            java.lang.String r11 = "p=android&t=demo"
            r1.append(r11)     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.lang.String r11 = "&a="
            java.lang.StringBuffer r11 = r1.append(r11)     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            com.mobclix.android.sdk.Mobclix r3 = com.mobclix.android.sdk.MobclixDemographics.controller     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.lang.String r3 = r3.getApplicationId()     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.lang.String r4 = "UTF-8"
            java.lang.String r3 = java.net.URLEncoder.encode(r3, r4)     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            r11.append(r3)     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.lang.String r11 = "&v="
            java.lang.StringBuffer r11 = r1.append(r11)     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            com.mobclix.android.sdk.Mobclix r3 = com.mobclix.android.sdk.MobclixDemographics.controller     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.lang.String r3 = r3.getApplicationVersion()     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.lang.String r4 = "UTF-8"
            java.lang.String r3 = java.net.URLEncoder.encode(r3, r4)     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            r11.append(r3)     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.lang.String r11 = "&m="
            java.lang.StringBuffer r11 = r1.append(r11)     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            com.mobclix.android.sdk.Mobclix r3 = com.mobclix.android.sdk.MobclixDemographics.controller     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.lang.String r3 = r3.getMobclixVersion()     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.lang.String r4 = "UTF-8"
            java.lang.String r3 = java.net.URLEncoder.encode(r3, r4)     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            r11.append(r3)     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.lang.String r11 = "&d="
            java.lang.StringBuffer r11 = r1.append(r11)     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            com.mobclix.android.sdk.Mobclix r3 = com.mobclix.android.sdk.MobclixDemographics.controller     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.lang.String r3 = r3.getDeviceId()     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.lang.String r4 = "UTF-8"
            java.lang.String r3 = java.net.URLEncoder.encode(r3, r4)     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            r11.append(r3)     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.lang.String r11 = "&dt="
            java.lang.StringBuffer r11 = r1.append(r11)     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            com.mobclix.android.sdk.Mobclix r3 = com.mobclix.android.sdk.MobclixDemographics.controller     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.lang.String r3 = r3.getDeviceModel()     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.lang.String r4 = "UTF-8"
            java.lang.String r3 = java.net.URLEncoder.encode(r3, r4)     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            r11.append(r3)     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.lang.String r11 = "&os="
            java.lang.StringBuffer r11 = r1.append(r11)     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            com.mobclix.android.sdk.Mobclix r3 = com.mobclix.android.sdk.MobclixDemographics.controller     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.lang.String r3 = r3.getAndroidVersion()     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.lang.String r4 = "UTF-8"
            java.lang.String r3 = java.net.URLEncoder.encode(r3, r4)     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            r11.append(r3)     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            com.mobclix.android.sdk.Mobclix r11 = com.mobclix.android.sdk.MobclixDemographics.controller     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.lang.String r11 = r11.getGPS()     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.lang.String r3 = "null"
            boolean r3 = r11.equals(r3)     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            if (r3 != 0) goto L_0x0171
            java.lang.String r3 = "UTF-8"
            java.lang.String r11 = java.net.URLEncoder.encode(r11, r3)     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.lang.String r3 = "&gps="
            java.lang.StringBuffer r3 = r1.append(r3)     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            r4 = 0
            r5 = 24
            int r6 = r11.length()     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            int r5 = java.lang.Math.min(r5, r6)     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.lang.String r4 = r11.substring(r4, r5)     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            r3.append(r4)     // Catch:{ UnsupportedEncodingException -> 0x02ef }
        L_0x0171:
            java.util.HashMap<java.lang.String, java.lang.String> r11 = com.mobclix.android.sdk.MobclixDemographics.demo     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.util.Set r11 = r11.keySet()     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.util.Iterator r3 = r11.iterator()     // Catch:{ UnsupportedEncodingException -> 0x02ef }
        L_0x017b:
            boolean r11 = r3.hasNext()     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            if (r11 != 0) goto L_0x02bd
            java.lang.Thread r11 = new java.lang.Thread
            com.mobclix.android.sdk.MobclixUtility$POSTThread r3 = new com.mobclix.android.sdk.MobclixUtility$POSTThread
            java.lang.String r1 = r1.toString()
            r3.<init>(r2, r1, r10, r12)
            r11.<init>(r3)
            r11.run()
            goto L_0x0002
        L_0x0194:
            java.lang.Object r2 = r8.next()
            java.lang.String r2 = (java.lang.String) r2
            java.lang.String r5 = ""
            r1 = 0
            java.lang.Object r6 = r11.get(r2)
            if (r6 == 0) goto L_0x02f2
            java.lang.Object r4 = r3.get(r2)
            if (r4 != 0) goto L_0x01b4
            r1 = 0
            r4 = r5
        L_0x01ab:
            if (r1 == 0) goto L_0x00b1
            java.util.HashMap<java.lang.String, java.lang.String> r5 = com.mobclix.android.sdk.MobclixDemographics.demo
            r5.put(r2, r4)
            goto L_0x00b1
        L_0x01b4:
            java.lang.Class r4 = r6.getClass()
            java.lang.Class<java.util.Date> r7 = java.util.Date.class
            if (r4 != r7) goto L_0x01d3
            java.lang.String r4 = "dbd"
            boolean r4 = r2.equals(r4)
            if (r4 == 0) goto L_0x02f2
            java.text.SimpleDateFormat r1 = new java.text.SimpleDateFormat
            java.lang.String r4 = "yyyyMMdd"
            r1.<init>(r4)
            java.util.Date r6 = (java.util.Date) r6
            java.lang.String r4 = r1.format(r6)
            r1 = 1
            goto L_0x01ab
        L_0x01d3:
            java.lang.Class r4 = r6.getClass()
            java.lang.Class<java.lang.String> r7 = java.lang.String.class
            if (r4 != r7) goto L_0x0231
            java.lang.String r4 = "dci"
            boolean r4 = r2.equals(r4)
            if (r4 != 0) goto L_0x021b
            java.lang.String r4 = "dco"
            boolean r4 = r2.equals(r4)
            if (r4 != 0) goto L_0x021b
            java.lang.String r4 = "dpo"
            boolean r4 = r2.equals(r4)
            if (r4 != 0) goto L_0x021b
            java.lang.String r4 = "drg"
            boolean r4 = r2.equals(r4)
            if (r4 != 0) goto L_0x021b
            java.lang.String r4 = "dac"
            boolean r4 = r2.equals(r4)
            if (r4 != 0) goto L_0x021b
            java.lang.String r4 = "ddc"
            boolean r4 = r2.equals(r4)
            if (r4 != 0) goto L_0x021b
            java.lang.String r4 = "dic"
            boolean r4 = r2.equals(r4)
            if (r4 != 0) goto L_0x021b
            java.lang.String r4 = "dmc"
            boolean r4 = r2.equals(r4)
            if (r4 == 0) goto L_0x02f2
        L_0x021b:
            r0 = r6
            java.lang.String r0 = (java.lang.String) r0
            r1 = r0
            java.lang.String r4 = r1.trim()
            java.lang.String r1 = ""
            boolean r1 = r4.equals(r1)
            if (r1 == 0) goto L_0x022e
            r1 = 0
            goto L_0x01ab
        L_0x022e:
            r1 = 1
            goto L_0x01ab
        L_0x0231:
            java.lang.Class r4 = r6.getClass()
            java.lang.Class<java.lang.Integer> r7 = java.lang.Integer.class
            if (r4 != r7) goto L_0x02f2
            java.lang.String r4 = "dac"
            boolean r4 = r2.equals(r4)
            if (r4 != 0) goto L_0x0259
            java.lang.String r4 = "ddc"
            boolean r4 = r2.equals(r4)
            if (r4 != 0) goto L_0x0259
            java.lang.String r4 = "dic"
            boolean r4 = r2.equals(r4)
            if (r4 != 0) goto L_0x0259
            java.lang.String r4 = "dmc"
            boolean r4 = r2.equals(r4)
            if (r4 == 0) goto L_0x0271
        L_0x0259:
            java.lang.Integer r6 = (java.lang.Integer) r6
            java.lang.String r1 = r6.toString()
            java.lang.String r4 = r1.trim()
            java.lang.String r1 = ""
            boolean r1 = r4.equals(r1)
            if (r1 == 0) goto L_0x026e
            r1 = 0
            goto L_0x01ab
        L_0x026e:
            r1 = 1
            goto L_0x01ab
        L_0x0271:
            r4 = 0
            java.lang.String r7 = "dg"
            boolean r7 = r2.equals(r7)
            if (r7 == 0) goto L_0x028f
            r4 = 2
        L_0x027b:
            r0 = r6
            java.lang.Integer r0 = (java.lang.Integer) r0
            r7 = r0
            int r7 = r7.intValue()
            r9 = 1
            if (r9 > r7) goto L_0x02f2
            if (r7 > r4) goto L_0x02f2
            java.lang.String r4 = r6.toString()
            r1 = 1
            goto L_0x01ab
        L_0x028f:
            java.lang.String r7 = "ddg"
            boolean r7 = r2.equals(r7)
            if (r7 != 0) goto L_0x029f
            java.lang.String r7 = "dms"
            boolean r7 = r2.equals(r7)
            if (r7 == 0) goto L_0x02a1
        L_0x029f:
            r4 = 3
            goto L_0x027b
        L_0x02a1:
            java.lang.String r7 = "dec"
            boolean r7 = r2.equals(r7)
            if (r7 != 0) goto L_0x02b1
            java.lang.String r7 = "den"
            boolean r7 = r2.equals(r7)
            if (r7 == 0) goto L_0x02b3
        L_0x02b1:
            r4 = 6
            goto L_0x027b
        L_0x02b3:
            java.lang.String r7 = "drl"
            boolean r7 = r2.equals(r7)
            if (r7 == 0) goto L_0x027b
            r4 = 7
            goto L_0x027b
        L_0x02bd:
            java.lang.Object r11 = r3.next()     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.lang.String r11 = (java.lang.String) r11     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.lang.String r5 = "&"
            r4.<init>(r5)     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.lang.StringBuilder r4 = r4.append(r11)     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.lang.String r5 = "="
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.lang.String r4 = r4.toString()     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.lang.StringBuffer r4 = r1.append(r4)     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.util.HashMap<java.lang.String, java.lang.String> r5 = com.mobclix.android.sdk.MobclixDemographics.demo     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.lang.Object r11 = r5.get(r11)     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.lang.String r11 = (java.lang.String) r11     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            java.lang.String r5 = "UTF-8"
            java.lang.String r11 = java.net.URLEncoder.encode(r11, r5)     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            r4.append(r11)     // Catch:{ UnsupportedEncodingException -> 0x02ef }
            goto L_0x017b
        L_0x02ef:
            r10 = move-exception
            goto L_0x0002
        L_0x02f2:
            r4 = r5
            goto L_0x01ab
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.MobclixDemographics.sendDemographics(android.app.Activity, java.util.Map, com.mobclix.android.sdk.MobclixFeedback$Listener):void");
    }
}
