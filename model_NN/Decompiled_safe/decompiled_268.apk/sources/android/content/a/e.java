package android.content.a;

import java.io.IOException;

/* compiled from: ProGuard */
public class e {

    /* renamed from: a  reason: collision with root package name */
    private int[] f21a;
    private int[] b;
    private int[] c;
    private int[] d;

    public static e a(d dVar) {
        int i;
        c.a(dVar, 1835009);
        int b2 = dVar.b();
        int b3 = dVar.b();
        int b4 = dVar.b();
        dVar.b();
        int b5 = dVar.b();
        int b6 = dVar.b();
        e eVar = new e();
        eVar.f21a = dVar.b(b3);
        if (b4 != 0) {
            eVar.c = dVar.b(b4);
        }
        if (b6 == 0) {
            i = b2;
        } else {
            i = b6;
        }
        int i2 = i - b5;
        if (i2 % 4 != 0) {
            throw new IOException("String data size is not multiple of 4 (" + i2 + ").");
        }
        eVar.b = dVar.b(i2 / 4);
        if (b6 != 0) {
            int i3 = b2 - b6;
            if (i3 % 4 != 0) {
                throw new IOException("Style data size is not multiple of 4 (" + i3 + ").");
            }
            eVar.d = dVar.b(i3 / 4);
        }
        return eVar;
    }

    public String a(int i) {
        if (i < 0 || this.f21a == null || i >= this.f21a.length) {
            return null;
        }
        int i2 = this.f21a[i];
        int a2 = a(this.b, i2);
        StringBuilder sb = new StringBuilder(a2);
        while (a2 != 0) {
            i2 += 2;
            sb.append((char) a(this.b, i2));
            a2--;
        }
        return sb.toString();
    }

    public int a(String str) {
        if (str == null) {
            return -1;
        }
        for (int i = 0; i != this.f21a.length; i++) {
            int i2 = this.f21a[i];
            int a2 = a(this.b, i2);
            if (a2 == str.length()) {
                int i3 = i2;
                int i4 = 0;
                while (i4 != a2) {
                    i3 += 2;
                    if (str.charAt(i4) != a(this.b, i3)) {
                        break;
                    }
                    i4++;
                }
                if (i4 == a2) {
                    return i;
                }
            }
        }
        return -1;
    }

    private e() {
    }

    private static final int a(int[] iArr, int i) {
        int i2 = iArr[i / 4];
        if ((i % 4) / 2 == 0) {
            return i2 & 65535;
        }
        return i2 >>> 16;
    }
}
