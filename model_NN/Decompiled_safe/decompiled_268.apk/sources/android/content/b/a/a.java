package android.content.b.a;

import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class a {
    public static int r = 0;

    /* renamed from: a  reason: collision with root package name */
    public final short f22a;
    public final short b;
    public final char[] c;
    public final char[] d;
    public final byte e;
    public final byte f;
    public final short g;
    public final byte h;
    public final byte i;
    public final byte j;
    public final short k;
    public final short l;
    public final short m;
    public final byte n;
    public final byte o;
    public final boolean p;
    public final String q;

    public a() {
        this.f22a = 0;
        this.b = 0;
        this.c = new char[]{0, 0};
        this.d = new char[]{0, 0};
        this.e = 0;
        this.f = 0;
        this.g = 0;
        this.h = 0;
        this.i = 0;
        this.j = 0;
        this.k = 0;
        this.l = 0;
        this.m = 0;
        this.n = 0;
        this.o = 0;
        this.p = false;
        this.q = Constants.STR_EMPTY;
    }

    public a(short s, short s2, char[] cArr, char[] cArr2, byte b2, byte b3, short s3, byte b4, byte b5, byte b6, short s4, short s5, short s6, byte b7, byte b8, boolean z) {
        boolean z2;
        if (b2 < 0 || b2 > 3) {
            b2 = 0;
            z2 = true;
        } else {
            z2 = z;
        }
        if (b3 < 0 || b3 > 3) {
            b3 = 0;
            z2 = true;
        }
        if (s3 < -1) {
            s3 = 0;
            z2 = true;
        }
        if (b4 < 0 || b4 > 3) {
            b4 = 0;
            z2 = true;
        }
        if (b5 < 0 || b5 > 4) {
            b5 = 0;
            z2 = true;
        }
        this.f22a = s;
        this.b = s2;
        this.c = cArr;
        this.d = cArr2;
        this.e = b2;
        this.f = b3;
        this.g = s3;
        this.h = b4;
        this.i = b5;
        this.j = b6;
        this.k = s4;
        this.l = s5;
        this.m = s6;
        this.n = b7;
        this.o = b8;
        this.p = z2;
        this.q = Constants.STR_EMPTY;
    }
}
