package android.support.v4.content;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: ProGuard */
final class c implements ThreadFactory {

    /* renamed from: a  reason: collision with root package name */
    private final AtomicInteger f72a = new AtomicInteger(1);

    c() {
    }

    public Thread newThread(Runnable runnable) {
        return new Thread(runnable, "ModernAsyncTask #" + this.f72a.getAndIncrement());
    }
}
