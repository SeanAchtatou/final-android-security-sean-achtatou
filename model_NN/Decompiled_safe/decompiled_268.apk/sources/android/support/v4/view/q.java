package android.support.v4.view;

import android.os.Parcel;
import android.support.v4.a.c;
import android.support.v4.view.DirectionalViewPager;

/* compiled from: ProGuard */
final class q implements c<DirectionalViewPager.SavedState> {
    q() {
    }

    /* renamed from: b */
    public DirectionalViewPager.SavedState a(Parcel parcel, ClassLoader classLoader) {
        return new DirectionalViewPager.SavedState(parcel, classLoader);
    }

    /* renamed from: b */
    public DirectionalViewPager.SavedState[] a(int i) {
        return new DirectionalViewPager.SavedState[i];
    }
}
