package android.support.v4.view;

import android.database.DataSetObserver;

/* compiled from: ProGuard */
class bx extends DataSetObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ViewPager f89a;

    private bx(ViewPager viewPager) {
        this.f89a = viewPager;
    }

    /* synthetic */ bx(ViewPager viewPager, bq bqVar) {
        this(viewPager);
    }

    public void onChanged() {
        this.f89a.dataSetChanged();
    }

    public void onInvalidated() {
        this.f89a.dataSetChanged();
    }
}
