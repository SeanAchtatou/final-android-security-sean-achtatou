package android.support.v4.app;

import android.view.animation.Animation;

/* compiled from: ProGuard */
class p implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Fragment f63a;
    final /* synthetic */ n b;

    p(n nVar, Fragment fragment) {
        this.b = nVar;
        this.f63a = fragment;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.n.a(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.n.a(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.n.a(android.support.v4.app.Fragment, int, int, int, boolean):void */
    public void onAnimationEnd(Animation animation) {
        if (this.f63a.b != null) {
            this.f63a.b = null;
            this.b.a(this.f63a, this.f63a.c, 0, 0, false);
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }
}
