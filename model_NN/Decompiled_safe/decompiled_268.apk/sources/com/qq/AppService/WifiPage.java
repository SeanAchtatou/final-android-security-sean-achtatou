package com.qq.AppService;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import java.net.InetAddress;

/* compiled from: ProGuard */
public final class WifiPage extends Activity {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f211a = false;
    public static WifiPage b = null;
    public static Handler c = null;
    private static ax d = ax.a();
    private volatile int e = 0;

    public static void a() {
        if (d != null) {
            d.c();
            d = null;
        }
        if (b != null) {
            b.finish();
            b = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        b = null;
        c = null;
        if (this.e != 0) {
            s.a(-1);
            this.e = 0;
        }
        s.a(this);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    public void startActivity(Intent intent) {
        super.startActivity(intent);
    }

    public static int a(Context context) {
        int a2 = ae.a(context);
        if (a2 != -1) {
            return a2;
        }
        InetAddress b2 = ae.b(context);
        if (b2 != null) {
            Log.e("com.qq.connect", b2.getHostAddress());
        }
        return ae.a(b2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void b(Context context) {
        if (d == null || !ax.f230a) {
            Log.d("com.qq.connect", "startIPCServer start wifiAdminServer");
            if (d == null) {
                d = ax.a();
            }
            d.a(context, 13091);
        }
        if (!AppService.c) {
            Log.d("com.qq.connect", "startIPCServer startService");
            Intent intent = new Intent();
            intent.setClass(context.getApplicationContext(), AppService.class);
            intent.putExtra("isWifi", true);
            context.getApplicationContext().startService(intent);
            return;
        }
        Log.d("com.qq.connect", "switch to wifi");
    }

    public void b() {
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        b();
        super.onResume();
    }
}
