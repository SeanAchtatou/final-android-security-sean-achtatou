package com.qq.AppService;

import android.content.Context;
import android.content.Intent;
import com.tencent.connector.ConnectEventReceiver;

/* compiled from: ProGuard */
public class ac {
    public static Intent a(Context context) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.tencent.connector.ConnectEventReceiver");
        intent.setAction("disconnect");
        return intent;
    }

    public static Intent b(Context context) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.tencent.connector.ConnectEventReceiver");
        intent.setAction("refreshOnNetChange");
        return intent;
    }

    public static Intent c(Context context) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.tencent.connector.ConnectEventReceiver");
        intent.setAction("connect");
        return intent;
    }

    public static Intent d(Context context) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.tencent.connector.ConnectEventReceiver");
        intent.setAction("refresh");
        return intent;
    }

    public static Intent e(Context context) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.tencent.connector.ConnectEventReceiver");
        intent.setAction("notification_connection");
        return intent;
    }

    public static Intent f(Context context) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.tencent.connector.ConnectEventReceiver");
        intent.setAction("notification_no_connection");
        return intent;
    }

    public static Intent g(Context context) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.tencent.connector.ConnectEventReceiver");
        intent.setAction("destory");
        return intent;
    }

    public static Intent a(Context context, boolean z) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.tencent.connector.ConnectEventReceiver");
        intent.setAction("usb");
        intent.putExtra("value", z);
        return intent;
    }

    public static Intent a(Context context, String str, String str2, String str3, String str4, String str5) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.tencent.connector.ConnectEventReceiver");
        intent.setAction("login");
        intent.putExtra("name", str);
        intent.putExtra("ip", str2);
        intent.putExtra("key", str3);
        intent.putExtra("uin", str4);
        intent.putExtra("nickname", str5);
        return intent;
    }

    public static Intent h(Context context) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.tencent.connector.ConnectEventReceiver");
        intent.setAction("cancelLogin");
        return intent;
    }

    public static Intent a(Context context, String str, String str2, String str3, boolean z) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.tencent.connector.ConnectEventReceiver");
        intent.setAction("pc_ping");
        intent.putExtra("name", str);
        intent.putExtra("ip", str2);
        intent.putExtra("pc_guid", str3);
        intent.putExtra("author", z);
        return intent;
    }

    public static Intent a(Context context, int i, String str, String str2) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.tencent.connector.ConnectEventReceiver");
        intent.setAction("low_tip");
        intent.putExtra("push_type", i);
        intent.putExtra("channelid", str);
        intent.putExtra("versionCode", str2);
        return intent;
    }

    public static void a(Context context, Intent intent) {
        new ConnectEventReceiver().onReceive(context, intent);
    }
}
