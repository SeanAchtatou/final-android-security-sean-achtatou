package com.qq.AppService;

import android.os.Process;
import com.tencent.assistant.utils.XLog;
import com.tencent.connector.ipc.a;
import com.tencent.pangu.download.DownloadingService;

/* compiled from: ProGuard */
class m implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AstApp f246a;

    m(AstApp astApp) {
        this.f246a = astApp;
    }

    public void run() {
        XLog.i("KillMe", "kill process in main called.last create time:" + AstApp.w + ",startKill time:" + AstApp.v);
        if (AstApp.w > AstApp.v) {
            DownloadingService.b();
            return;
        }
        a.a().b();
        Process.killProcess(Process.myPid());
    }
}
