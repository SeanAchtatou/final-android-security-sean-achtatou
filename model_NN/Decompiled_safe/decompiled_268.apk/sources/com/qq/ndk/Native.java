package com.qq.ndk;

import com.qq.AppService.AppService;
import com.tencent.jni.YYBNDK;

/* compiled from: ProGuard */
public final class Native extends YYBNDK {
    public static final int CONTAIN_WITH = 1;
    public static final int ENDWITH = 2;
    public static final int ENQUALS = 3;
    public static final int ROTATE_LINE_WRAP = 2;
    public static final int ROTATE_NO = 0;
    public static final int ROTATE_PIX_WRAP = 1;
    public static final int START_WITH = 0;

    public native int backupToFd(int i, String str, String str2, String str3);

    public native int captureScreenToFd(int i, String str, String str2, int i2, int i3);

    public native void close(int i);

    public native int createProcess(String str);

    public native byte[] encrypt(byte[] bArr, int i, int i2);

    public native String[] findAllRootFiles(String str);

    public native void findFileCallback(FileNative fileNative, String str, String str2, String str3, String str4, int i);

    public native NativeFileObject[] findFileList(String str, String str2, int i);

    public native String[] findFiles(String str, String str2, int i);

    public native String[] findMutiFiles(String str, String[] strArr, int i);

    public native long getInode(String str);

    public native byte[] getScreenShot(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9);

    public native byte[] getScreenShotByAurora(String str, String str2, int i, int i2);

    public native int getScreenShotToFd(int i, String str, String str2, int i2, int i3);

    public native int kill(int i);

    public native String[] listFileDic(String str);

    public native byte[] rc(byte[] bArr, int i);

    public native int readFile(String str, byte[] bArr, int i, int i2, int i3);

    public native int restoreFromFd(int i, String str, String str2);

    public native void setDefaultKey();

    public native void sync();

    public native int system(String str);

    public native byte[] unEncrypt(byte[] bArr, int i, int i2);

    public native void uncryptFile(String str);

    public native int writeAuth(String str);

    static {
        try {
            System.loadLibrary("qqndkfile_ex");
        } catch (Throwable th) {
            th.printStackTrace();
            AppService.c();
        }
    }

    public int getFilePermission(String str) {
        return super.getFilePermission(str);
    }

    public boolean setFilePermission(String str, int i) {
        return super.setFilePermission(str, i);
    }

    public boolean createFile(String str, int i) {
        return super.createDir(str, i);
    }

    public boolean createDir(String str, int i) {
        return super.createDir(str, i);
    }

    public int getLastAccessTime(String str) {
        return super.getLastAccessTime(str);
    }

    public int getUid(String str) {
        return super.getUid(str);
    }

    public long getFileSize(String str) {
        return super.getFileSize(str);
    }

    public long getDirSize(String str) {
        return super.getFileSize(str);
    }
}
