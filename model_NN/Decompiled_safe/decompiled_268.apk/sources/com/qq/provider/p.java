package com.qq.provider;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.pm.e;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import com.qq.AppService.AppInstaller;
import com.qq.AppService.AstApp;
import com.qq.AppService.r;
import com.qq.b.a;
import com.qq.c.d;
import com.qq.c.f;
import com.qq.c.h;
import com.qq.g.c;
import com.qq.ndk.Native;
import com.qq.provider.cache2.CacheProvider;
import com.qq.util.aa;
import com.qq.util.m;
import com.tencent.open.SocialConstants;
import java.io.File;
import java.security.cert.CertificateEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.security.cert.CertificateException;
import javax.security.cert.X509Certificate;

/* compiled from: ProGuard */
public class p {

    /* renamed from: a  reason: collision with root package name */
    private static p f347a;

    public static byte[] a(Context context, String str) {
        Cursor query;
        byte[] bArr = null;
        if (!(str == null || context == null || (query = context.getContentResolver().query(CacheProvider.f334a, new String[]{"icon"}, "pkg = '" + str + "'", null, null)) == null)) {
            if (query.moveToFirst()) {
                try {
                    bArr = query.getBlob(0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            query.close();
        }
        return bArr;
    }

    public static p a() {
        if (f347a == null) {
            return new p();
        }
        return f347a;
    }

    public static boolean b(Context context, String str) {
        try {
            context.getPackageManager().getPackageInfo(str, 0);
            return true;
        } catch (Throwable th) {
            return false;
        }
    }

    public static File a(Context context) {
        if (!Environment.getExternalStorageState().equals("mounted")) {
            return null;
        }
        File file = new File(Environment.getExternalStorageDirectory() + File.separator + "application");
        if (file.exists()) {
            return file;
        }
        file.mkdir();
        return file;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void a(Context context, File file) {
        Intent intent = new Intent();
        intent.addFlags(134217728);
        intent.addFlags(268435456);
        intent.setClass(context, AppInstaller.class);
        intent.putExtra(SocialConstants.PARAM_ACT, "install");
        intent.putExtra("path", file.getAbsolutePath());
        intent.putExtra("delete", true);
        context.startActivity(intent);
    }

    private void b(Context context, File file) {
        Intent intent = new Intent();
        intent.addFlags(134217728);
        intent.addFlags(268435456);
        intent.setClass(context, AppInstaller.class);
        intent.putExtra(SocialConstants.PARAM_ACT, "install");
        intent.putExtra("path", file.getAbsolutePath());
        context.startActivity(intent);
    }

    private void c(Context context, String str) {
        Intent intent = new Intent();
        intent.addFlags(134217728);
        intent.addFlags(268435456);
        intent.setClass(context, AppInstaller.class);
        intent.putExtra(SocialConstants.PARAM_ACT, "uninstall");
        intent.putExtra("pkg", str);
        context.startActivity(intent);
    }

    public void a(Context context, c cVar) {
        ArrayList arrayList = new ArrayList();
        List<PackageInfo> installedPackages = context.getPackageManager().getInstalledPackages(8192);
        arrayList.add(r.a(installedPackages.size()));
        int i = 0;
        for (int i2 = 0; i2 < installedPackages.size(); i2++) {
            PackageInfo packageInfo = installedPackages.get(i2);
            if (!(packageInfo.applicationInfo == null || packageInfo.applicationInfo.sourceDir == null || (!packageInfo.applicationInfo.sourceDir.startsWith("/mnt") && !new File(packageInfo.applicationInfo.sourceDir).exists()))) {
                arrayList.add(r.a(packageInfo.packageName));
                i++;
            }
        }
        installedPackages.clear();
        arrayList.set(0, r.a(i));
        cVar.a(0);
        cVar.a(arrayList);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00aa, code lost:
        if (r3 >= (r2 + r8)) goto L_0x00ac;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00d0, code lost:
        if (r5 < (r2 + r8)) goto L_0x00d2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0145, code lost:
        if (r5 >= (r2 + r8)) goto L_0x00ac;
     */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0130  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x017e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(android.content.Context r17, com.qq.g.c r18) {
        /*
            r16 = this;
            int r1 = r18.e()
            r2 = 3
            if (r1 >= r2) goto L_0x000e
            r1 = 1
            r0 = r18
            r0.a(r1)
        L_0x000d:
            return
        L_0x000e:
            int r1 = r18.h()
            if (r1 >= 0) goto L_0x01aa
            r1 = 0
            r2 = r1
        L_0x0016:
            int r8 = r18.h()
            int r1 = r18.h()
            if (r1 < 0) goto L_0x0023
            r3 = 2
            if (r1 <= r3) goto L_0x01a7
        L_0x0023:
            r1 = 0
            r7 = r1
        L_0x0025:
            if (r8 > 0) goto L_0x002e
            r1 = 1
            r0 = r18
            r0.a(r1)
            goto L_0x000d
        L_0x002e:
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>()
            android.content.pm.PackageManager r10 = r17.getPackageManager()
            r1 = 8192(0x2000, float:1.14794E-41)
            java.util.List r11 = r10.getInstalledPackages(r1)
            int r1 = r11.size()
            if (r2 < r1) goto L_0x0057
            r1 = 0
            r0 = r18
            r0.a(r1)
            r1 = 0
            byte[] r1 = com.qq.AppService.r.a(r1)
            r9.add(r1)
            r0 = r18
            r0.a(r9)
            goto L_0x000d
        L_0x0057:
            byte[] r1 = com.qq.AppService.r.a(r8)
            r9.add(r1)
            r4 = 0
            r3 = 0
            java.io.ByteArrayOutputStream r12 = new java.io.ByteArrayOutputStream
            r12.<init>()
            r1 = 0
            r5 = r4
            r4 = r3
            r3 = r1
        L_0x0069:
            int r1 = r11.size()
            if (r3 >= r1) goto L_0x00ac
            java.lang.Object r1 = r11.get(r3)
            android.content.pm.PackageInfo r1 = (android.content.pm.PackageInfo) r1
            android.content.pm.ApplicationInfo r6 = r1.applicationInfo
            if (r6 == 0) goto L_0x007f
            android.content.pm.ApplicationInfo r6 = r1.applicationInfo
            java.lang.String r6 = r6.sourceDir
            if (r6 != 0) goto L_0x0083
        L_0x007f:
            int r1 = r3 + 1
            r3 = r1
            goto L_0x0069
        L_0x0083:
            android.content.pm.ApplicationInfo r6 = r1.applicationInfo
            java.lang.String r6 = r6.sourceDir
            java.lang.String r13 = "/mnt"
            boolean r6 = r6.startsWith(r13)
            if (r6 != 0) goto L_0x009e
            java.io.File r6 = new java.io.File
            android.content.pm.ApplicationInfo r13 = r1.applicationInfo
            java.lang.String r13 = r13.sourceDir
            r6.<init>(r13)
            boolean r6 = r6.exists()
            if (r6 == 0) goto L_0x007f
        L_0x009e:
            android.content.pm.ApplicationInfo r6 = r1.applicationInfo
            int r6 = r6.flags
            int r6 = r6 % 2
            if (r7 != 0) goto L_0x00c4
            if (r3 < r2) goto L_0x007f
            int r13 = r2 + r8
            if (r3 < r13) goto L_0x00d2
        L_0x00ac:
            r12.close()     // Catch:{ IOException -> 0x01a1 }
        L_0x00af:
            r1 = 0
            byte[] r2 = com.qq.AppService.r.a(r4)
            r9.set(r1, r2)
            r0 = r18
            r0.a(r9)
            r1 = 0
            r0 = r18
            r0.a(r1)
            goto L_0x000d
        L_0x00c4:
            r13 = 1
            if (r7 != r13) goto L_0x0137
            if (r6 == 0) goto L_0x007f
            if (r5 >= r2) goto L_0x00ce
            int r5 = r5 + 1
            goto L_0x007f
        L_0x00ce:
            int r13 = r2 + r8
            if (r5 >= r13) goto L_0x00ac
        L_0x00d2:
            android.content.pm.ApplicationInfo r13 = r1.applicationInfo
            java.lang.CharSequence r13 = r13.loadLabel(r10)
            java.lang.String r13 = r13.toString()
            byte[] r13 = com.qq.AppService.r.a(r13)
            r9.add(r13)
            java.lang.String r13 = r1.packageName
            byte[] r13 = com.qq.AppService.r.a(r13)
            r9.add(r13)
            java.lang.String r13 = r1.versionName
            byte[] r13 = com.qq.AppService.r.a(r13)
            r9.add(r13)
            byte[] r6 = com.qq.AppService.r.a(r6)
            r9.add(r6)
            java.io.File r6 = new java.io.File
            android.content.pm.ApplicationInfo r13 = r1.applicationInfo
            java.lang.String r13 = r13.sourceDir
            r6.<init>(r13)
            long r13 = r6.length()
            int r6 = (int) r13
            byte[] r6 = com.qq.AppService.r.a(r6)
            r9.add(r6)
            java.lang.String r6 = r1.packageName
            r0 = r17
            byte[] r6 = a(r0, r6)
            if (r6 != 0) goto L_0x019d
            r6 = 0
            android.content.pm.ApplicationInfo r1 = r1.applicationInfo
            android.graphics.drawable.Drawable r1 = r1.loadIcon(r10)
            if (r1 == 0) goto L_0x017c
            boolean r13 = r1 instanceof android.graphics.drawable.BitmapDrawable
            if (r13 == 0) goto L_0x0151
            android.graphics.drawable.BitmapDrawable r1 = (android.graphics.drawable.BitmapDrawable) r1
            android.graphics.Bitmap r1 = r1.getBitmap()
        L_0x012e:
            if (r1 != 0) goto L_0x017e
            byte[] r1 = com.qq.AppService.r.hr
            r9.add(r1)
            goto L_0x007f
        L_0x0137:
            r13 = 2
            if (r7 != r13) goto L_0x0149
            r13 = 1
            if (r6 == r13) goto L_0x007f
            if (r5 >= r2) goto L_0x0143
            int r5 = r5 + 1
            goto L_0x007f
        L_0x0143:
            int r13 = r2 + r8
            if (r5 < r13) goto L_0x00d2
            goto L_0x00ac
        L_0x0149:
            r1 = 1
            r0 = r18
            r0.a(r1)
            goto L_0x000d
        L_0x0151:
            boolean r13 = r1 instanceof android.graphics.drawable.StateListDrawable
            if (r13 == 0) goto L_0x0160
            android.graphics.drawable.Drawable r1 = r1.getCurrent()
            android.graphics.drawable.BitmapDrawable r1 = (android.graphics.drawable.BitmapDrawable) r1
            android.graphics.Bitmap r1 = r1.getBitmap()
            goto L_0x012e
        L_0x0160:
            java.lang.String r13 = "com.qq.connect"
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            java.lang.String r15 = "unkown drawable!::"
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r1 = r1.toString()
            java.lang.StringBuilder r1 = r14.append(r1)
            java.lang.String r1 = r1.toString()
            android.util.Log.d(r13, r1)
        L_0x017c:
            r1 = r6
            goto L_0x012e
        L_0x017e:
            r12.reset()
            android.graphics.Bitmap$CompressFormat r6 = android.graphics.Bitmap.CompressFormat.PNG
            r13 = 100
            r1.compress(r6, r13, r12)
            r12.flush()     // Catch:{ IOException -> 0x0198 }
        L_0x018b:
            byte[] r1 = r12.toByteArray()
            r9.add(r1)
        L_0x0192:
            int r4 = r4 + 1
            if (r4 < r8) goto L_0x007f
            goto L_0x00ac
        L_0x0198:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x018b
        L_0x019d:
            r9.add(r6)
            goto L_0x0192
        L_0x01a1:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00af
        L_0x01a7:
            r7 = r1
            goto L_0x0025
        L_0x01aa:
            r2 = r1
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.provider.p.b(android.content.Context, com.qq.g.c):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:138:0x0340  */
    /* JADX WARNING: Removed duplicated region for block: B:144:0x0378  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c(android.content.Context r23, com.qq.g.c r24) {
        /*
            r22 = this;
            int r2 = r24.e()
            r3 = 4
            if (r2 >= r3) goto L_0x000e
            r2 = 1
            r0 = r24
            r0.a(r2)
        L_0x000d:
            return
        L_0x000e:
            int r2 = r24.h()
            if (r2 >= 0) goto L_0x03b3
            r2 = 0
            r8 = r2
        L_0x0016:
            int r14 = r24.h()
            int r2 = r24.h()
            if (r2 < 0) goto L_0x0023
            r3 = 2
            if (r2 <= r3) goto L_0x03b0
        L_0x0023:
            r2 = 0
            r9 = r2
        L_0x0025:
            int r15 = r24.h()
            int r16 = r24.h()
            if (r14 > 0) goto L_0x0036
            r2 = 1
            r0 = r24
            r0.a(r2)
            goto L_0x000d
        L_0x0036:
            java.util.ArrayList r17 = new java.util.ArrayList
            r17.<init>()
            r12 = 0
            r11 = 0
            r2 = 0
            int r3 = com.qq.util.j.f369a
            if (r3 < 0) goto L_0x03ac
            if (r16 != 0) goto L_0x03ac
            boolean r3 = com.qq.AppService.q.f249a
            if (r3 == 0) goto L_0x03ac
            r10 = 1
            r13 = 0
            android.content.ContentResolver r2 = r23.getContentResolver()     // Catch:{ Throwable -> 0x008b }
            android.net.Uri r3 = com.qq.provider.cache2.CacheProvider.f334a     // Catch:{ Throwable -> 0x008b }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r2 = r2.query(r3, r4, r5, r6, r7)     // Catch:{ Throwable -> 0x008b }
            r7 = r2
        L_0x0059:
            if (r7 != 0) goto L_0x0091
            r2 = 0
            r4 = r11
            r5 = r12
        L_0x005e:
            if (r2 != 0) goto L_0x0250
            android.content.pm.PackageManager r10 = r23.getPackageManager()
            if (r16 <= 0) goto L_0x01ee
            r2 = 12288(0x3000, float:1.7219E-41)
            java.util.List r2 = r10.getInstalledPackages(r2)
            r3 = r2
        L_0x006d:
            int r2 = r3.size()
            if (r8 < r2) goto L_0x01f7
            r2 = 0
            r0 = r24
            r0.a(r2)
            r2 = 0
            byte[] r2 = com.qq.AppService.r.a(r2)
            r0 = r17
            r0.add(r2)
            r0 = r24
            r1 = r17
            r0.a(r1)
            goto L_0x000d
        L_0x008b:
            r2 = move-exception
            r2.printStackTrace()
            r7 = r13
            goto L_0x0059
        L_0x0091:
            boolean r4 = r7.moveToFirst()
            if (r4 != 0) goto L_0x03a9
            r2 = 0
        L_0x0098:
            int r3 = r7.getCount()
            if (r8 < r3) goto L_0x00ba
            r7.close()
            r2 = 0
            r0 = r24
            r0.a(r2)
            r2 = 0
            byte[] r2 = com.qq.AppService.r.a(r2)
            r0 = r17
            r0.add(r2)
            r0 = r24
            r1 = r17
            r0.a(r1)
            goto L_0x000d
        L_0x00ba:
            byte[] r3 = com.qq.AppService.r.a(r14)
            r0 = r17
            r0.add(r3)
            r3 = 0
            r6 = r3
            r5 = r12
            r3 = r4
            r4 = r11
        L_0x00c8:
            if (r3 == 0) goto L_0x010b
            java.lang.String r3 = "flag"
            int r3 = r7.getColumnIndex(r3)
            int r3 = r7.getInt(r3)
            java.lang.String r10 = "pkg"
            int r10 = r7.getColumnIndex(r10)
            java.lang.String r10 = r7.getString(r10)
            java.lang.String r11 = "path"
            int r11 = r7.getColumnIndex(r11)
            java.lang.String r11 = r7.getString(r11)
            if (r11 == 0) goto L_0x00f5
            java.io.File r12 = new java.io.File
            r12.<init>(r11)
            boolean r11 = r12.exists()
            if (r11 != 0) goto L_0x00fc
        L_0x00f5:
            boolean r3 = r7.moveToNext()
        L_0x00f9:
            int r6 = r6 + 1
            goto L_0x00c8
        L_0x00fc:
            int r3 = r3 % 2
            if (r9 != 0) goto L_0x0110
            if (r6 >= r8) goto L_0x0107
            boolean r3 = r7.moveToNext()
            goto L_0x00f9
        L_0x0107:
            int r11 = r8 + r14
            if (r6 < r11) goto L_0x0127
        L_0x010b:
            r7.close()
            goto L_0x005e
        L_0x0110:
            r11 = 1
            if (r9 != r11) goto L_0x01ca
            if (r3 != 0) goto L_0x011a
            boolean r3 = r7.moveToNext()
            goto L_0x00f9
        L_0x011a:
            if (r5 >= r8) goto L_0x0123
            boolean r3 = r7.moveToNext()
            int r5 = r5 + 1
            goto L_0x00f9
        L_0x0123:
            int r11 = r8 + r14
            if (r5 >= r11) goto L_0x010b
        L_0x0127:
            java.lang.String r11 = "app_name"
            int r11 = r7.getColumnIndex(r11)
            java.lang.String r11 = r7.getString(r11)
            java.lang.String r12 = "version"
            int r12 = r7.getColumnIndex(r12)
            java.lang.String r12 = r7.getString(r12)
            java.lang.String r13 = "version_code"
            int r13 = r7.getColumnIndex(r13)
            int r13 = r7.getInt(r13)
            java.lang.String r18 = "date"
            r0 = r18
            int r18 = r7.getColumnIndex(r0)
            r0 = r18
            long r18 = r7.getLong(r0)
            java.lang.String r20 = "size"
            r0 = r20
            int r20 = r7.getColumnIndex(r0)
            r0 = r20
            long r20 = r7.getLong(r0)
            byte[] r11 = com.qq.AppService.r.a(r11)
            r0 = r17
            r0.add(r11)
            byte[] r10 = com.qq.AppService.r.a(r10)
            r0 = r17
            r0.add(r10)
            byte[] r10 = com.qq.AppService.r.a(r12)
            r0 = r17
            r0.add(r10)
            byte[] r10 = com.qq.AppService.r.a(r13)
            r0 = r17
            r0.add(r10)
            byte[] r10 = com.qq.AppService.r.a(r18)
            r0 = r17
            r0.add(r10)
            byte[] r3 = com.qq.AppService.r.a(r3)
            r0 = r17
            r0.add(r3)
            r0 = r20
            int r3 = (int) r0
            byte[] r3 = com.qq.AppService.r.a(r3)
            r0 = r17
            r0.add(r3)
            if (r16 <= 0) goto L_0x01af
            r3 = 0
            byte[] r3 = com.qq.AppService.r.a(r3)
            r0 = r17
            r0.add(r3)
        L_0x01af:
            if (r15 <= 0) goto L_0x01c0
            java.lang.String r3 = "icon"
            int r3 = r7.getColumnIndex(r3)
            byte[] r3 = r7.getBlob(r3)
            r0 = r17
            r0.add(r3)
        L_0x01c0:
            boolean r3 = r7.moveToNext()
            int r4 = r4 + 1
            if (r4 < r14) goto L_0x00f9
            goto L_0x010b
        L_0x01ca:
            r11 = 2
            if (r9 != r11) goto L_0x01e6
            r11 = 1
            if (r3 != r11) goto L_0x01d6
            boolean r3 = r7.moveToNext()
            goto L_0x00f9
        L_0x01d6:
            if (r5 >= r8) goto L_0x01e0
            boolean r3 = r7.moveToNext()
            int r5 = r5 + 1
            goto L_0x00f9
        L_0x01e0:
            int r11 = r8 + r14
            if (r5 < r11) goto L_0x0127
            goto L_0x010b
        L_0x01e6:
            r2 = 1
            r0 = r24
            r0.a(r2)
            goto L_0x000d
        L_0x01ee:
            r2 = 8192(0x2000, float:1.14794E-41)
            java.util.List r2 = r10.getInstalledPackages(r2)
            r3 = r2
            goto L_0x006d
        L_0x01f7:
            byte[] r2 = com.qq.AppService.r.a(r14)
            r0 = r17
            r0.add(r2)
            java.io.ByteArrayOutputStream r11 = new java.io.ByteArrayOutputStream
            r11.<init>()
            r2 = 0
            r6 = r5
            r5 = r4
            r4 = r2
        L_0x0209:
            int r2 = r3.size()
            if (r4 >= r2) goto L_0x03a6
            java.lang.Object r2 = r3.get(r4)
            android.content.pm.PackageInfo r2 = (android.content.pm.PackageInfo) r2
            android.content.pm.ApplicationInfo r7 = r2.applicationInfo
            if (r7 == 0) goto L_0x021f
            android.content.pm.ApplicationInfo r7 = r2.applicationInfo
            java.lang.String r7 = r7.sourceDir
            if (r7 != 0) goto L_0x0223
        L_0x021f:
            int r2 = r4 + 1
            r4 = r2
            goto L_0x0209
        L_0x0223:
            android.content.pm.ApplicationInfo r7 = r2.applicationInfo
            java.lang.String r7 = r7.sourceDir
            java.lang.String r12 = "/mnt"
            boolean r7 = r7.startsWith(r12)
            if (r7 != 0) goto L_0x023e
            java.io.File r7 = new java.io.File
            android.content.pm.ApplicationInfo r12 = r2.applicationInfo
            java.lang.String r12 = r12.sourceDir
            r7.<init>(r12)
            boolean r7 = r7.exists()
            if (r7 == 0) goto L_0x021f
        L_0x023e:
            android.content.pm.ApplicationInfo r7 = r2.applicationInfo
            int r7 = r7.flags
            int r7 = r7 % 2
            if (r9 != 0) goto L_0x0269
            if (r4 < r8) goto L_0x021f
            int r12 = r8 + r14
            if (r4 < r12) goto L_0x0292
            r4 = r5
        L_0x024d:
            r11.close()     // Catch:{ IOException -> 0x03a0 }
        L_0x0250:
            r2 = 0
            byte[] r3 = com.qq.AppService.r.a(r4)
            r0 = r17
            r0.set(r2, r3)
            r0 = r24
            r1 = r17
            r0.a(r1)
            r2 = 0
            r0 = r24
            r0.a(r2)
            goto L_0x000d
        L_0x0269:
            r12 = 1
            if (r9 != r12) goto L_0x0279
            if (r7 == 0) goto L_0x021f
            if (r6 >= r8) goto L_0x0273
            int r6 = r6 + 1
            goto L_0x021f
        L_0x0273:
            int r12 = r8 + r14
            if (r6 < r12) goto L_0x0292
            r4 = r5
            goto L_0x024d
        L_0x0279:
            r12 = 2
            if (r9 != r12) goto L_0x028a
            r12 = 1
            if (r7 == r12) goto L_0x021f
            if (r6 >= r8) goto L_0x0284
            int r6 = r6 + 1
            goto L_0x021f
        L_0x0284:
            int r12 = r8 + r14
            if (r6 < r12) goto L_0x0292
            r4 = r5
            goto L_0x024d
        L_0x028a:
            r2 = 1
            r0 = r24
            r0.a(r2)
            goto L_0x000d
        L_0x0292:
            android.content.pm.ApplicationInfo r12 = r2.applicationInfo
            java.lang.CharSequence r12 = r12.loadLabel(r10)
            java.lang.String r12 = r12.toString()
            byte[] r12 = com.qq.AppService.r.a(r12)
            r0 = r17
            r0.add(r12)
            java.lang.String r12 = r2.packageName
            byte[] r12 = com.qq.AppService.r.a(r12)
            r0 = r17
            r0.add(r12)
            java.lang.String r12 = r2.versionName
            byte[] r12 = com.qq.AppService.r.a(r12)
            r0 = r17
            r0.add(r12)
            int r12 = r2.versionCode
            byte[] r12 = com.qq.AppService.r.a(r12)
            r0 = r17
            r0.add(r12)
            java.io.File r12 = new java.io.File
            android.content.pm.ApplicationInfo r13 = r2.applicationInfo
            java.lang.String r13 = r13.sourceDir
            r12.<init>(r13)
            long r18 = r12.lastModified()
            byte[] r13 = com.qq.AppService.r.a(r18)
            r0 = r17
            r0.add(r13)
            long r12 = r12.length()
            int r12 = (int) r12
            byte[] r7 = com.qq.AppService.r.a(r7)
            r0 = r17
            r0.add(r7)
            byte[] r7 = com.qq.AppService.r.a(r12)
            r0 = r17
            r0.add(r7)
            if (r16 <= 0) goto L_0x031f
            java.lang.String[] r12 = r2.requestedPermissions
            if (r12 == 0) goto L_0x0315
            int r7 = r12.length
            byte[] r7 = com.qq.AppService.r.a(r7)
            r0 = r17
            r0.add(r7)
            r7 = 0
        L_0x0304:
            int r13 = r12.length
            if (r7 >= r13) goto L_0x031f
            r13 = r12[r7]
            byte[] r13 = com.qq.AppService.r.a(r13)
            r0 = r17
            r0.add(r13)
            int r7 = r7 + 1
            goto L_0x0304
        L_0x0315:
            r7 = 0
            byte[] r7 = com.qq.AppService.r.a(r7)
            r0 = r17
            r0.add(r7)
        L_0x031f:
            if (r15 <= 0) goto L_0x038e
            java.lang.String r7 = r2.packageName
            r0 = r23
            byte[] r7 = a(r0, r7)
            if (r7 != 0) goto L_0x039a
            r7 = 0
            android.content.pm.ApplicationInfo r2 = r2.applicationInfo
            android.graphics.drawable.Drawable r2 = r2.loadIcon(r10)
            if (r2 == 0) goto L_0x0376
            boolean r12 = r2 instanceof android.graphics.drawable.BitmapDrawable
            if (r12 == 0) goto L_0x0349
            android.graphics.drawable.BitmapDrawable r2 = (android.graphics.drawable.BitmapDrawable) r2
            android.graphics.Bitmap r2 = r2.getBitmap()
        L_0x033e:
            if (r2 != 0) goto L_0x0378
            byte[] r2 = com.qq.AppService.r.hr
            r0 = r17
            r0.add(r2)
            goto L_0x021f
        L_0x0349:
            boolean r12 = r2 instanceof android.graphics.drawable.StateListDrawable
            if (r12 == 0) goto L_0x0358
            android.graphics.drawable.Drawable r2 = r2.getCurrent()
            android.graphics.drawable.BitmapDrawable r2 = (android.graphics.drawable.BitmapDrawable) r2
            android.graphics.Bitmap r2 = r2.getBitmap()
            goto L_0x033e
        L_0x0358:
            java.lang.String r12 = "com.qq.connect"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r18 = "unkown drawable!::"
            r0 = r18
            java.lang.StringBuilder r13 = r13.append(r0)
            java.lang.String r2 = r2.toString()
            java.lang.StringBuilder r2 = r13.append(r2)
            java.lang.String r2 = r2.toString()
            android.util.Log.d(r12, r2)
        L_0x0376:
            r2 = r7
            goto L_0x033e
        L_0x0378:
            r11.reset()
            android.graphics.Bitmap$CompressFormat r7 = android.graphics.Bitmap.CompressFormat.PNG
            r12 = 100
            r2.compress(r7, r12, r11)
            r11.flush()     // Catch:{ IOException -> 0x0395 }
        L_0x0385:
            byte[] r2 = r11.toByteArray()
            r0 = r17
            r0.add(r2)
        L_0x038e:
            int r5 = r5 + 1
            if (r5 < r14) goto L_0x021f
            r4 = r5
            goto L_0x024d
        L_0x0395:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0385
        L_0x039a:
            r0 = r17
            r0.add(r7)
            goto L_0x038e
        L_0x03a0:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0250
        L_0x03a6:
            r4 = r5
            goto L_0x024d
        L_0x03a9:
            r2 = r10
            goto L_0x0098
        L_0x03ac:
            r4 = r11
            r5 = r12
            goto L_0x005e
        L_0x03b0:
            r9 = r2
            goto L_0x0025
        L_0x03b3:
            r8 = r2
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.provider.p.c(android.content.Context, com.qq.g.c):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:113:0x0292  */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x02c8  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x019f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void d(android.content.Context r22, com.qq.g.c r23) {
        /*
            r21 = this;
            int r1 = r23.e()
            r2 = 4
            if (r1 >= r2) goto L_0x000e
            r1 = 1
            r0 = r23
            r0.a(r1)
        L_0x000d:
            return
        L_0x000e:
            int r1 = r23.h()
            if (r1 >= 0) goto L_0x0307
            r1 = 0
            r2 = r1
        L_0x0016:
            int r10 = r23.h()
            int r1 = r23.h()
            if (r1 < 0) goto L_0x0023
            r3 = 2
            if (r1 <= r3) goto L_0x0304
        L_0x0023:
            r1 = 0
            r3 = r1
        L_0x0025:
            int r11 = r23.h()
            int r12 = r23.h()
            if (r10 > 0) goto L_0x0036
            r1 = 1
            r0 = r23
            r0.a(r1)
            goto L_0x000d
        L_0x0036:
            r13 = 0
            java.util.ArrayList r14 = new java.util.ArrayList
            r14.<init>()
            android.content.pm.PackageManager r15 = r22.getPackageManager()
            r5 = 0
            r4 = 0
            android.content.pm.PackageManager r16 = r22.getPackageManager()
            r6 = 0
            if (r12 <= 0) goto L_0x0068
            r1 = 4096(0x1000, float:5.74E-42)
            r0 = r16
            java.util.List r1 = r0.getInstalledPackages(r1)     // Catch:{ Throwable -> 0x0070 }
        L_0x0051:
            r9 = r1
        L_0x0052:
            if (r9 != 0) goto L_0x0076
            r1 = 0
            r0 = r23
            r0.a(r1)
            r1 = 0
            byte[] r1 = com.qq.AppService.r.a(r1)
            r14.add(r1)
            r0 = r23
            r0.a(r14)
            goto L_0x000d
        L_0x0068:
            r1 = 0
            r0 = r16
            java.util.List r1 = r0.getInstalledPackages(r1)     // Catch:{ Throwable -> 0x0070 }
            goto L_0x0051
        L_0x0070:
            r1 = move-exception
            r1.printStackTrace()
            r9 = r6
            goto L_0x0052
        L_0x0076:
            int r1 = r9.size()
            if (r2 < r1) goto L_0x0091
            r1 = 0
            r0 = r23
            r0.a(r1)
            r1 = 0
            byte[] r1 = com.qq.AppService.r.a(r1)
            r14.add(r1)
            r0 = r23
            r0.a(r14)
            goto L_0x000d
        L_0x0091:
            byte[] r1 = com.qq.AppService.r.a(r10)
            r14.add(r1)
            java.io.ByteArrayOutputStream r17 = new java.io.ByteArrayOutputStream
            r17.<init>()
            r1 = 0
            r6 = r5
            r5 = r4
            r4 = r1
        L_0x00a1:
            int r1 = r9.size()
            if (r4 >= r1) goto L_0x0301
            java.lang.Object r1 = r9.get(r4)
            android.content.pm.PackageInfo r1 = (android.content.pm.PackageInfo) r1
            android.content.pm.ApplicationInfo r7 = r1.applicationInfo
            if (r7 == 0) goto L_0x00b7
            android.content.pm.ApplicationInfo r7 = r1.applicationInfo
            java.lang.String r7 = r7.sourceDir
            if (r7 != 0) goto L_0x00bb
        L_0x00b7:
            int r1 = r4 + 1
            r4 = r1
            goto L_0x00a1
        L_0x00bb:
            android.content.pm.ApplicationInfo r7 = r1.applicationInfo
            java.lang.String r7 = r7.sourceDir
            java.lang.String r8 = "/mnt"
            boolean r7 = r7.startsWith(r8)
            if (r7 != 0) goto L_0x00d6
            java.io.File r7 = new java.io.File
            android.content.pm.ApplicationInfo r8 = r1.applicationInfo
            java.lang.String r8 = r8.sourceDir
            r7.<init>(r8)
            boolean r7 = r7.exists()
            if (r7 == 0) goto L_0x00b7
        L_0x00d6:
            android.content.pm.ApplicationInfo r7 = r1.applicationInfo
            int r7 = r7.flags
            int r18 = r7 % 2
            if (r3 != 0) goto L_0x014d
            if (r4 < r2) goto L_0x00b7
            int r7 = r2 + r10
            if (r4 < r7) goto L_0x017b
            r4 = r5
        L_0x00e5:
            r17.close()     // Catch:{ IOException -> 0x02ee }
        L_0x00e8:
            java.lang.String r1 = "com.qq.connect"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r5 = " GetAPKBatch3!!!!!!!!"
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.StringBuilder r2 = r2.append(r10)
            java.lang.String r5 = " value"
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = " photo:"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r11)
            java.lang.String r3 = " permission :"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r12)
            java.lang.String r3 = " count:"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r3 = " loadcache:"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r13)
            java.lang.String r2 = r2.toString()
            android.util.Log.d(r1, r2)
            int r1 = r14.size()
            if (r1 <= 0) goto L_0x02f4
            r1 = 0
            byte[] r2 = com.qq.AppService.r.a(r4)
            r14.set(r1, r2)
        L_0x0140:
            r0 = r23
            r0.a(r14)
            r1 = 0
            r0 = r23
            r0.a(r1)
            goto L_0x000d
        L_0x014d:
            r7 = 1
            if (r3 != r7) goto L_0x015e
            if (r18 == 0) goto L_0x00b7
            if (r6 >= r2) goto L_0x0158
            int r6 = r6 + 1
            goto L_0x00b7
        L_0x0158:
            int r7 = r2 + r10
            if (r6 < r7) goto L_0x017b
            r4 = r5
            goto L_0x00e5
        L_0x015e:
            r7 = 2
            if (r3 != r7) goto L_0x0173
            r7 = 1
            r0 = r18
            if (r0 == r7) goto L_0x00b7
            if (r6 >= r2) goto L_0x016c
            int r6 = r6 + 1
            goto L_0x00b7
        L_0x016c:
            int r7 = r2 + r10
            if (r6 < r7) goto L_0x017b
            r4 = r5
            goto L_0x00e5
        L_0x0173:
            r1 = 1
            r0 = r23
            r0.a(r1)
            goto L_0x000d
        L_0x017b:
            r8 = 0
            android.content.pm.ApplicationInfo r7 = r1.applicationInfo
            if (r7 == 0) goto L_0x02fe
            android.content.pm.ApplicationInfo r7 = r1.applicationInfo
            int r7 = r7.labelRes
            if (r7 == 0) goto L_0x0233
            android.content.pm.ApplicationInfo r7 = r1.applicationInfo     // Catch:{ Throwable -> 0x022f }
            android.content.res.Resources r7 = r15.getResourcesForApplication(r7)     // Catch:{ Throwable -> 0x022f }
            android.content.pm.ApplicationInfo r0 = r1.applicationInfo     // Catch:{ Throwable -> 0x022f }
            r19 = r0
            r0 = r19
            int r0 = r0.labelRes     // Catch:{ Throwable -> 0x022f }
            r19 = r0
            r0 = r19
            java.lang.String r8 = r7.getString(r0)     // Catch:{ Throwable -> 0x022f }
            r7 = r8
        L_0x019d:
            if (r7 != 0) goto L_0x01a7
            android.content.pm.ApplicationInfo r7 = r1.applicationInfo
            r0 = r16
            java.lang.CharSequence r7 = r7.loadLabel(r0)
        L_0x01a7:
            if (r7 == 0) goto L_0x0236
            java.lang.String r7 = r7.toString()
            byte[] r7 = com.qq.AppService.r.a(r7)
            r14.add(r7)
        L_0x01b4:
            java.lang.String r7 = r1.packageName
            byte[] r7 = com.qq.AppService.r.a(r7)
            r14.add(r7)
            java.lang.String r7 = r1.versionName
            byte[] r7 = com.qq.AppService.r.a(r7)
            r14.add(r7)
            int r7 = r1.versionCode
            byte[] r7 = com.qq.AppService.r.a(r7)
            r14.add(r7)
            android.content.pm.ApplicationInfo r7 = r1.applicationInfo
            if (r7 == 0) goto L_0x023d
            android.content.pm.ApplicationInfo r7 = r1.applicationInfo
            java.lang.String r7 = r7.sourceDir
            if (r7 == 0) goto L_0x023d
            java.io.File r7 = new java.io.File
            android.content.pm.ApplicationInfo r8 = r1.applicationInfo
            java.lang.String r8 = r8.sourceDir
            r7.<init>(r8)
            long r19 = r7.lastModified()
            byte[] r8 = com.qq.AppService.r.a(r19)
            r14.add(r8)
            java.lang.String r8 = r7.getAbsolutePath()
            byte[] r8 = com.qq.AppService.r.a(r8)
            r14.add(r8)
            long r7 = r7.length()
            int r7 = (int) r7
            byte[] r8 = com.qq.AppService.r.a(r18)
            r14.add(r8)
            byte[] r7 = com.qq.AppService.r.a(r7)
            r14.add(r7)
        L_0x020b:
            if (r12 <= 0) goto L_0x0266
            java.lang.String[] r8 = r1.requestedPermissions
            if (r8 == 0) goto L_0x025e
            int r7 = r8.length
            byte[] r7 = com.qq.AppService.r.a(r7)
            r14.add(r7)
            r7 = 0
        L_0x021a:
            int r0 = r8.length
            r18 = r0
            r0 = r18
            if (r7 >= r0) goto L_0x0266
            r18 = r8[r7]
            byte[] r18 = com.qq.AppService.r.a(r18)
            r0 = r18
            r14.add(r0)
            int r7 = r7 + 1
            goto L_0x021a
        L_0x022f:
            r7 = move-exception
            r7.printStackTrace()
        L_0x0233:
            r7 = r8
            goto L_0x019d
        L_0x0236:
            byte[] r7 = com.qq.AppService.r.hr
            r14.add(r7)
            goto L_0x01b4
        L_0x023d:
            r7 = 0
            byte[] r7 = com.qq.AppService.r.a(r7)
            r14.add(r7)
            r7 = 0
            byte[] r7 = com.qq.AppService.r.a(r7)
            r14.add(r7)
            byte[] r7 = com.qq.AppService.r.a(r18)
            r14.add(r7)
            r7 = 0
            byte[] r7 = com.qq.AppService.r.a(r7)
            r14.add(r7)
            goto L_0x020b
        L_0x025e:
            r7 = 0
            byte[] r7 = com.qq.AppService.r.a(r7)
            r14.add(r7)
        L_0x0266:
            if (r11 <= 0) goto L_0x02de
            java.lang.String r7 = r1.packageName
            r0 = r22
            byte[] r7 = a(r0, r7)
            if (r7 != 0) goto L_0x02ea
            r7 = 0
            android.content.pm.ApplicationInfo r8 = r1.applicationInfo
            if (r8 != 0) goto L_0x027e
            byte[] r1 = com.qq.AppService.r.hr
            r14.add(r1)
            goto L_0x00b7
        L_0x027e:
            android.content.pm.ApplicationInfo r1 = r1.applicationInfo
            android.graphics.drawable.Drawable r1 = r1.loadIcon(r15)
            if (r1 == 0) goto L_0x02c6
            boolean r8 = r1 instanceof android.graphics.drawable.BitmapDrawable
            if (r8 == 0) goto L_0x0299
            android.graphics.drawable.BitmapDrawable r1 = (android.graphics.drawable.BitmapDrawable) r1
            android.graphics.Bitmap r1 = r1.getBitmap()
        L_0x0290:
            if (r1 != 0) goto L_0x02c8
            byte[] r1 = com.qq.AppService.r.hr
            r14.add(r1)
            goto L_0x00b7
        L_0x0299:
            boolean r8 = r1 instanceof android.graphics.drawable.StateListDrawable
            if (r8 == 0) goto L_0x02a8
            android.graphics.drawable.Drawable r1 = r1.getCurrent()
            android.graphics.drawable.BitmapDrawable r1 = (android.graphics.drawable.BitmapDrawable) r1
            android.graphics.Bitmap r1 = r1.getBitmap()
            goto L_0x0290
        L_0x02a8:
            java.lang.String r8 = "com.qq.connect"
            java.lang.StringBuilder r18 = new java.lang.StringBuilder
            r18.<init>()
            java.lang.String r19 = "unkown drawable!::"
            java.lang.StringBuilder r18 = r18.append(r19)
            java.lang.String r1 = r1.toString()
            r0 = r18
            java.lang.StringBuilder r1 = r0.append(r1)
            java.lang.String r1 = r1.toString()
            android.util.Log.d(r8, r1)
        L_0x02c6:
            r1 = r7
            goto L_0x0290
        L_0x02c8:
            r17.reset()
            android.graphics.Bitmap$CompressFormat r7 = android.graphics.Bitmap.CompressFormat.PNG
            r8 = 100
            r0 = r17
            r1.compress(r7, r8, r0)
            r17.flush()     // Catch:{ IOException -> 0x02e5 }
        L_0x02d7:
            byte[] r1 = r17.toByteArray()
            r14.add(r1)
        L_0x02de:
            int r5 = r5 + 1
            if (r5 < r10) goto L_0x00b7
            r4 = r5
            goto L_0x00e5
        L_0x02e5:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x02d7
        L_0x02ea:
            r14.add(r7)
            goto L_0x02de
        L_0x02ee:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00e8
        L_0x02f4:
            r1 = 0
            byte[] r1 = com.qq.AppService.r.a(r1)
            r14.add(r1)
            goto L_0x0140
        L_0x02fe:
            r7 = r8
            goto L_0x01a7
        L_0x0301:
            r4 = r5
            goto L_0x00e5
        L_0x0304:
            r3 = r1
            goto L_0x0025
        L_0x0307:
            r2 = r1
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.provider.p.d(android.content.Context, com.qq.g.c):void");
    }

    public void e(Context context, c cVar) {
        boolean z;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (r.b(j)) {
            cVar.a(1);
            return;
        }
        try {
            context.getPackageManager().getPackageInfo(j, 0);
            z = true;
        } catch (Exception e) {
            e.printStackTrace();
            z = false;
        }
        ArrayList arrayList = new ArrayList();
        if (z) {
            arrayList.add(r.a(1));
        } else {
            arrayList.add(r.a(0));
        }
        cVar.a(0);
        cVar.a(arrayList);
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x00aa  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0100  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0104  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void f(android.content.Context r8, com.qq.g.c r9) {
        /*
            r7 = this;
            r1 = 0
            r6 = 0
            r3 = 1
            int r0 = r9.e()
            if (r0 >= r3) goto L_0x000d
            r9.a(r3)
        L_0x000c:
            return
        L_0x000d:
            java.lang.String r0 = "com.qq.connect"
            java.lang.String r2 = "getInstallAPPInfo"
            android.util.Log.d(r0, r2)
            java.lang.String r0 = r9.j()
            boolean r2 = com.qq.AppService.r.b(r0)
            if (r2 == 0) goto L_0x0022
            r9.a(r3)
            goto L_0x000c
        L_0x0022:
            android.content.pm.PackageManager r2 = r8.getPackageManager()     // Catch:{ Exception -> 0x003d }
            r3 = 8192(0x2000, float:1.14794E-41)
            android.content.pm.PackageInfo r0 = r2.getPackageInfo(r0, r3)     // Catch:{ Exception -> 0x003d }
            if (r0 == 0) goto L_0x0038
            android.content.pm.ApplicationInfo r2 = r0.applicationInfo
            if (r2 == 0) goto L_0x0038
            android.content.pm.ApplicationInfo r2 = r0.applicationInfo
            java.lang.String r2 = r2.sourceDir
            if (r2 != 0) goto L_0x0046
        L_0x0038:
            r0 = 7
            r9.a(r0)
            goto L_0x000c
        L_0x003d:
            r0 = move-exception
            r0.printStackTrace()
            r0 = 3
            r9.a(r0)
            goto L_0x000c
        L_0x0046:
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            android.content.pm.ApplicationInfo r3 = r0.applicationInfo
            android.content.pm.PackageManager r4 = r8.getPackageManager()
            java.lang.CharSequence r3 = r3.loadLabel(r4)
            java.lang.String r3 = r3.toString()
            byte[] r3 = com.qq.AppService.r.a(r3)
            r2.add(r3)
            java.lang.String r3 = r0.packageName
            byte[] r3 = com.qq.AppService.r.a(r3)
            r2.add(r3)
            java.lang.String r3 = r0.versionName
            byte[] r3 = com.qq.AppService.r.a(r3)
            r2.add(r3)
            android.content.pm.ApplicationInfo r3 = r0.applicationInfo
            int r3 = r3.flags
            byte[] r3 = com.qq.AppService.r.a(r3)
            r2.add(r3)
            java.io.File r3 = new java.io.File
            android.content.pm.ApplicationInfo r4 = r0.applicationInfo
            java.lang.String r4 = r4.sourceDir
            r3.<init>(r4)
            long r3 = r3.length()
            int r3 = (int) r3
            byte[] r3 = com.qq.AppService.r.a(r3)
            r2.add(r3)
            android.content.pm.ApplicationInfo r0 = r0.applicationInfo
            android.content.pm.PackageManager r3 = r8.getPackageManager()
            android.graphics.drawable.Drawable r0 = r0.loadIcon(r3)
            if (r0 == 0) goto L_0x00f9
            boolean r3 = r0 instanceof android.graphics.drawable.BitmapDrawable
            if (r3 == 0) goto L_0x00ce
            android.graphics.drawable.BitmapDrawable r0 = (android.graphics.drawable.BitmapDrawable) r0
            android.graphics.Bitmap r0 = r0.getBitmap()
        L_0x00a8:
            if (r0 == 0) goto L_0x0104
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream
            r1.<init>()
            android.graphics.Bitmap$CompressFormat r3 = android.graphics.Bitmap.CompressFormat.PNG
            r4 = 100
            r0.compress(r3, r4, r1)
            byte[] r0 = r1.toByteArray()
            r1.close()     // Catch:{ IOException -> 0x00fb }
        L_0x00bd:
            if (r0 != 0) goto L_0x0100
            byte[] r0 = com.qq.AppService.r.a(r6)
            r2.add(r0)
        L_0x00c6:
            r9.a(r2)
            r9.a(r6)
            goto L_0x000c
        L_0x00ce:
            boolean r3 = r0 instanceof android.graphics.drawable.StateListDrawable
            if (r3 == 0) goto L_0x00dd
            android.graphics.drawable.Drawable r0 = r0.getCurrent()
            android.graphics.drawable.BitmapDrawable r0 = (android.graphics.drawable.BitmapDrawable) r0
            android.graphics.Bitmap r0 = r0.getBitmap()
            goto L_0x00a8
        L_0x00dd:
            java.lang.String r3 = "com.qq.connect"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "unkown drawable!::"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r0 = r4.append(r0)
            java.lang.String r0 = r0.toString()
            android.util.Log.d(r3, r0)
        L_0x00f9:
            r0 = r1
            goto L_0x00a8
        L_0x00fb:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00bd
        L_0x0100:
            r2.add(r0)
            goto L_0x00c6
        L_0x0104:
            r0 = r1
            goto L_0x00bd
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.provider.p.f(android.content.Context, com.qq.g.c):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x00fb  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0155  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void g(android.content.Context r9, com.qq.g.c r10) {
        /*
            r8 = this;
            r5 = 1
            r2 = 0
            int r0 = r10.e()
            r1 = 2
            if (r0 >= r1) goto L_0x000d
            r10.a(r5)
        L_0x000c:
            return
        L_0x000d:
            java.lang.String r0 = "com.qq.connect"
            java.lang.String r1 = "getInstallAPPInfo2"
            android.util.Log.d(r0, r1)
            java.lang.String r0 = r10.j()
            int r3 = r10.h()
            int r1 = r10.h()
            boolean r4 = com.qq.AppService.r.b(r0)
            if (r4 == 0) goto L_0x002a
            r10.a(r5)
            goto L_0x000c
        L_0x002a:
            if (r1 <= 0) goto L_0x0047
            android.content.pm.PackageManager r4 = r9.getPackageManager()     // Catch:{ Exception -> 0x0052 }
            r5 = 12288(0x3000, float:1.7219E-41)
            android.content.pm.PackageInfo r0 = r4.getPackageInfo(r0, r5)     // Catch:{ Exception -> 0x0052 }
        L_0x0036:
            if (r0 == 0) goto L_0x0042
            android.content.pm.ApplicationInfo r4 = r0.applicationInfo
            if (r4 == 0) goto L_0x0042
            android.content.pm.ApplicationInfo r4 = r0.applicationInfo
            java.lang.String r4 = r4.sourceDir
            if (r4 != 0) goto L_0x005b
        L_0x0042:
            r0 = 7
            r10.a(r0)
            goto L_0x000c
        L_0x0047:
            android.content.pm.PackageManager r4 = r9.getPackageManager()     // Catch:{ Exception -> 0x0052 }
            r5 = 8192(0x2000, float:1.14794E-41)
            android.content.pm.PackageInfo r0 = r4.getPackageInfo(r0, r5)     // Catch:{ Exception -> 0x0052 }
            goto L_0x0036
        L_0x0052:
            r0 = move-exception
            r0.printStackTrace()
            r0 = 3
            r10.a(r0)
            goto L_0x000c
        L_0x005b:
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            android.content.pm.ApplicationInfo r5 = r0.applicationInfo
            android.content.pm.PackageManager r6 = r9.getPackageManager()
            java.lang.CharSequence r5 = r5.loadLabel(r6)
            java.lang.String r5 = r5.toString()
            byte[] r5 = com.qq.AppService.r.a(r5)
            r4.add(r5)
            java.lang.String r5 = r0.packageName
            byte[] r5 = com.qq.AppService.r.a(r5)
            r4.add(r5)
            java.lang.String r5 = r0.versionName
            byte[] r5 = com.qq.AppService.r.a(r5)
            r4.add(r5)
            int r5 = r0.versionCode
            byte[] r5 = com.qq.AppService.r.a(r5)
            r4.add(r5)
            java.io.File r5 = new java.io.File
            android.content.pm.ApplicationInfo r6 = r0.applicationInfo
            java.lang.String r6 = r6.sourceDir
            r5.<init>(r6)
            long r6 = r5.lastModified()
            byte[] r6 = com.qq.AppService.r.a(r6)
            r4.add(r6)
            android.content.pm.ApplicationInfo r6 = r0.applicationInfo
            int r6 = r6.flags
            byte[] r6 = com.qq.AppService.r.a(r6)
            r4.add(r6)
            long r5 = r5.length()
            int r5 = (int) r5
            byte[] r5 = com.qq.AppService.r.a(r5)
            r4.add(r5)
            if (r1 <= 0) goto L_0x00e0
            java.lang.String[] r5 = r0.requestedPermissions
            if (r5 == 0) goto L_0x00d9
            int r1 = r5.length
            byte[] r1 = com.qq.AppService.r.a(r1)
            r4.add(r1)
            r1 = r2
        L_0x00ca:
            int r6 = r5.length
            if (r1 >= r6) goto L_0x00e0
            r6 = r5[r1]
            byte[] r6 = com.qq.AppService.r.a(r6)
            r4.add(r6)
            int r1 = r1 + 1
            goto L_0x00ca
        L_0x00d9:
            byte[] r1 = com.qq.AppService.r.a(r2)
            r4.add(r1)
        L_0x00e0:
            if (r3 <= 0) goto L_0x0117
            r1 = 0
            android.content.pm.ApplicationInfo r0 = r0.applicationInfo
            android.content.pm.PackageManager r3 = r9.getPackageManager()
            android.graphics.drawable.Drawable r0 = r0.loadIcon(r3)
            if (r0 == 0) goto L_0x014a
            boolean r3 = r0 instanceof android.graphics.drawable.BitmapDrawable
            if (r3 == 0) goto L_0x011f
            android.graphics.drawable.BitmapDrawable r0 = (android.graphics.drawable.BitmapDrawable) r0
            android.graphics.Bitmap r0 = r0.getBitmap()
        L_0x00f9:
            if (r0 == 0) goto L_0x0155
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream
            r1.<init>()
            android.graphics.Bitmap$CompressFormat r3 = android.graphics.Bitmap.CompressFormat.PNG
            r5 = 100
            r0.compress(r3, r5, r1)
            byte[] r0 = r1.toByteArray()
            r1.close()     // Catch:{ IOException -> 0x014c }
        L_0x010e:
            if (r0 != 0) goto L_0x0151
            byte[] r0 = com.qq.AppService.r.a(r2)
            r4.add(r0)
        L_0x0117:
            r10.a(r4)
            r10.a(r2)
            goto L_0x000c
        L_0x011f:
            boolean r3 = r0 instanceof android.graphics.drawable.StateListDrawable
            if (r3 == 0) goto L_0x012e
            android.graphics.drawable.Drawable r0 = r0.getCurrent()
            android.graphics.drawable.BitmapDrawable r0 = (android.graphics.drawable.BitmapDrawable) r0
            android.graphics.Bitmap r0 = r0.getBitmap()
            goto L_0x00f9
        L_0x012e:
            java.lang.String r3 = "com.qq.connect"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "unkown drawable!::"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r0 = r5.append(r0)
            java.lang.String r0 = r0.toString()
            android.util.Log.d(r3, r0)
        L_0x014a:
            r0 = r1
            goto L_0x00f9
        L_0x014c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x010e
        L_0x0151:
            r4.add(r0)
            goto L_0x0117
        L_0x0155:
            byte[] r0 = com.qq.AppService.r.hr
            r4.add(r0)
            goto L_0x0117
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.provider.p.g(android.content.Context, com.qq.g.c):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:38:0x013d  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0197  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void h(android.content.Context r9, com.qq.g.c r10) {
        /*
            r8 = this;
            r5 = 1
            r2 = 0
            int r0 = r10.e()
            r1 = 2
            if (r0 >= r1) goto L_0x000d
            r10.a(r5)
        L_0x000c:
            return
        L_0x000d:
            java.lang.String r0 = r10.j()
            java.lang.String r1 = "com.qq.connect"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "getInstallAPPInfo3"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r3 = r3.toString()
            android.util.Log.d(r1, r3)
            int r3 = r10.h()
            int r1 = r10.h()
            boolean r4 = com.qq.AppService.r.b(r0)
            if (r4 == 0) goto L_0x003b
            r10.a(r5)
            goto L_0x000c
        L_0x003b:
            if (r1 <= 0) goto L_0x0058
            android.content.pm.PackageManager r4 = r9.getPackageManager()     // Catch:{ Exception -> 0x0062 }
            r5 = 4096(0x1000, float:5.74E-42)
            android.content.pm.PackageInfo r0 = r4.getPackageInfo(r0, r5)     // Catch:{ Exception -> 0x0062 }
        L_0x0047:
            if (r0 == 0) goto L_0x0053
            android.content.pm.ApplicationInfo r4 = r0.applicationInfo
            if (r4 == 0) goto L_0x0053
            android.content.pm.ApplicationInfo r4 = r0.applicationInfo
            java.lang.String r4 = r4.sourceDir
            if (r4 != 0) goto L_0x006b
        L_0x0053:
            r0 = 7
            r10.a(r0)
            goto L_0x000c
        L_0x0058:
            android.content.pm.PackageManager r4 = r9.getPackageManager()     // Catch:{ Exception -> 0x0062 }
            r5 = 0
            android.content.pm.PackageInfo r0 = r4.getPackageInfo(r0, r5)     // Catch:{ Exception -> 0x0062 }
            goto L_0x0047
        L_0x0062:
            r0 = move-exception
            r0.printStackTrace()
            r0 = 3
            r10.a(r0)
            goto L_0x000c
        L_0x006b:
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            android.content.pm.ApplicationInfo r5 = r0.applicationInfo
            android.content.pm.PackageManager r6 = r9.getPackageManager()
            java.lang.CharSequence r5 = r5.loadLabel(r6)
            java.lang.String r5 = r5.toString()
            byte[] r5 = com.qq.AppService.r.a(r5)
            r4.add(r5)
            java.lang.String r5 = r0.packageName
            byte[] r5 = com.qq.AppService.r.a(r5)
            r4.add(r5)
            java.lang.String r5 = r0.versionName
            byte[] r5 = com.qq.AppService.r.a(r5)
            r4.add(r5)
            int r5 = r0.versionCode
            byte[] r5 = com.qq.AppService.r.a(r5)
            r4.add(r5)
            android.content.pm.ApplicationInfo r5 = r0.applicationInfo
            java.lang.String r5 = r5.sourceDir
            if (r5 == 0) goto L_0x00fa
            java.io.File r5 = new java.io.File
            android.content.pm.ApplicationInfo r6 = r0.applicationInfo
            java.lang.String r6 = r6.sourceDir
            r5.<init>(r6)
            long r6 = r5.lastModified()
            byte[] r6 = com.qq.AppService.r.a(r6)
            r4.add(r6)
            java.lang.String r6 = r5.getAbsolutePath()
            byte[] r6 = com.qq.AppService.r.a(r6)
            r4.add(r6)
            android.content.pm.ApplicationInfo r6 = r0.applicationInfo
            int r6 = r6.flags
            byte[] r6 = com.qq.AppService.r.a(r6)
            r4.add(r6)
            long r5 = r5.length()
            int r5 = (int) r5
            byte[] r5 = com.qq.AppService.r.a(r5)
            r4.add(r5)
        L_0x00dc:
            if (r1 <= 0) goto L_0x0122
            java.lang.String[] r5 = r0.requestedPermissions
            if (r5 == 0) goto L_0x011b
            int r1 = r5.length
            byte[] r1 = com.qq.AppService.r.a(r1)
            r4.add(r1)
            r1 = r2
        L_0x00eb:
            int r6 = r5.length
            if (r1 >= r6) goto L_0x0122
            r6 = r5[r1]
            byte[] r6 = com.qq.AppService.r.a(r6)
            r4.add(r6)
            int r1 = r1 + 1
            goto L_0x00eb
        L_0x00fa:
            r5 = 0
            byte[] r5 = com.qq.AppService.r.a(r5)
            r4.add(r5)
            byte[] r5 = com.qq.AppService.r.hr
            r4.add(r5)
            android.content.pm.ApplicationInfo r5 = r0.applicationInfo
            int r5 = r5.flags
            byte[] r5 = com.qq.AppService.r.a(r5)
            r4.add(r5)
            byte[] r5 = com.qq.AppService.r.a(r2)
            r4.add(r5)
            goto L_0x00dc
        L_0x011b:
            byte[] r1 = com.qq.AppService.r.a(r2)
            r4.add(r1)
        L_0x0122:
            if (r3 <= 0) goto L_0x0159
            r1 = 0
            android.content.pm.ApplicationInfo r0 = r0.applicationInfo
            android.content.pm.PackageManager r3 = r9.getPackageManager()
            android.graphics.drawable.Drawable r0 = r0.loadIcon(r3)
            if (r0 == 0) goto L_0x018c
            boolean r3 = r0 instanceof android.graphics.drawable.BitmapDrawable
            if (r3 == 0) goto L_0x0161
            android.graphics.drawable.BitmapDrawable r0 = (android.graphics.drawable.BitmapDrawable) r0
            android.graphics.Bitmap r0 = r0.getBitmap()
        L_0x013b:
            if (r0 == 0) goto L_0x0197
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream
            r1.<init>()
            android.graphics.Bitmap$CompressFormat r3 = android.graphics.Bitmap.CompressFormat.PNG
            r5 = 100
            r0.compress(r3, r5, r1)
            byte[] r0 = r1.toByteArray()
            r1.close()     // Catch:{ IOException -> 0x018e }
        L_0x0150:
            if (r0 != 0) goto L_0x0193
            byte[] r0 = com.qq.AppService.r.a(r2)
            r4.add(r0)
        L_0x0159:
            r10.a(r4)
            r10.a(r2)
            goto L_0x000c
        L_0x0161:
            boolean r3 = r0 instanceof android.graphics.drawable.StateListDrawable
            if (r3 == 0) goto L_0x0170
            android.graphics.drawable.Drawable r0 = r0.getCurrent()
            android.graphics.drawable.BitmapDrawable r0 = (android.graphics.drawable.BitmapDrawable) r0
            android.graphics.Bitmap r0 = r0.getBitmap()
            goto L_0x013b
        L_0x0170:
            java.lang.String r3 = "com.qq.connect"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "unkown drawable!::"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r0 = r5.append(r0)
            java.lang.String r0 = r0.toString()
            android.util.Log.d(r3, r0)
        L_0x018c:
            r0 = r1
            goto L_0x013b
        L_0x018e:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0150
        L_0x0193:
            r4.add(r0)
            goto L_0x0159
        L_0x0197:
            byte[] r0 = com.qq.AppService.r.hr
            r4.add(r0)
            goto L_0x0159
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.provider.p.h(android.content.Context, com.qq.g.c):void");
    }

    public void i(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (h < 0 || h > 2) {
            cVar.a(1);
            return;
        }
        List<PackageInfo> installedPackages = context.getPackageManager().getInstalledPackages(8192);
        int i = 0;
        for (int i2 = 0; i2 < installedPackages.size(); i2++) {
            PackageInfo packageInfo = installedPackages.get(i2);
            if (!(packageInfo.applicationInfo == null || packageInfo.applicationInfo.sourceDir == null || (!packageInfo.applicationInfo.sourceDir.startsWith("/mnt") && !new File(packageInfo.applicationInfo.sourceDir).exists()))) {
                int i3 = packageInfo.applicationInfo.flags % 2;
                if (h == 0) {
                    i++;
                } else if (h == 1 && i3 == 1) {
                    i++;
                } else if (h == 2 && i3 == 0) {
                    i++;
                }
            }
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(i));
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void j(Context context, c cVar) {
        PackageInfo packageInfo;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (cVar.e() < h + 1) {
            cVar.a(1);
            return;
        }
        PackageManager packageManager = context.getPackageManager();
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(h));
        for (int i = 0; i < h; i++) {
            String j = cVar.j();
            if (r.b(j)) {
                packageInfo = null;
            } else {
                try {
                    packageInfo = packageManager.getPackageInfo(j, 0);
                } catch (Exception e) {
                    packageInfo = null;
                }
            }
            if (packageInfo != null) {
                arrayList.add(r.a(packageInfo.packageName));
                arrayList.add(r.a(packageInfo.versionName));
                arrayList.add(r.a(packageInfo.applicationInfo.sourceDir));
            } else {
                arrayList.add(r.hr);
                arrayList.add(r.hr);
                arrayList.add(r.hr);
            }
        }
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void k(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        List<PackageInfo> installedPackages = context.getPackageManager().getInstalledPackages(0);
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(installedPackages.size()));
        int i = 0;
        for (int i2 = 0; i2 < installedPackages.size(); i2++) {
            PackageInfo packageInfo = installedPackages.get(i2);
            if (!(packageInfo.applicationInfo == null || packageInfo.applicationInfo.sourceDir == null || !new File(packageInfo.applicationInfo.sourceDir).exists())) {
                if (h == 0) {
                    arrayList.add(r.a(packageInfo.packageName));
                    arrayList.add(r.a(packageInfo.versionName));
                    arrayList.add(r.a(packageInfo.applicationInfo.sourceDir));
                    i++;
                } else if (h == 1 && packageInfo.applicationInfo.flags % 2 == 1) {
                    arrayList.add(r.a(packageInfo.packageName));
                    arrayList.add(r.a(packageInfo.versionName));
                    arrayList.add(r.a(packageInfo.applicationInfo.sourceDir));
                    i++;
                } else if (h == 2 && packageInfo.applicationInfo.flags % 2 != 1) {
                    arrayList.add(r.a(packageInfo.packageName));
                    arrayList.add(r.a(packageInfo.versionName));
                    arrayList.add(r.a(packageInfo.applicationInfo.sourceDir));
                    i++;
                }
            }
        }
        arrayList.set(0, r.a(i));
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void l(Context context, c cVar) {
        int i;
        boolean z;
        com.qq.b.c c;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (r.b(j)) {
            cVar.a(1);
            return;
        }
        if (cVar.e() >= 2) {
            i = cVar.h();
        } else {
            i = 0;
        }
        if (!a.f260a) {
            a.a(context);
        }
        ArrayList arrayList = new ArrayList();
        Native nativeR = new Native();
        nativeR.getFilePermission(j);
        if (j.startsWith(context.getApplicationInfo().dataDir) && nativeR.getFilePermission(j) % 10 < 6) {
            nativeR.setFilePermission(j, 511);
        }
        String str = null;
        if (j.contains(" ") || j.contains("\n") || j.contains("\r") || j.contains("'") || j.contains("\"")) {
            File file = new File(j);
            str = file.getParent() + "/" + System.currentTimeMillis() + "-" + new Random().nextInt(100000) + ".apk";
            file.renameTo(new File(str));
        }
        if (a.c || a.b) {
            a aVar = new a();
            if (str == null) {
                c = aVar.c(a.a(j, i), 180000);
            } else {
                c = aVar.c(a.a(str, i), 180000);
            }
            aVar.a();
            if (c == null || c.c == null) {
                z = false;
            } else {
                arrayList.add(r.a(1));
                arrayList.add(c.c);
                z = true;
            }
            if (str != null) {
                new File(str).renameTo(new File(j));
            }
        } else {
            z = false;
        }
        if (!z) {
            if (str != null) {
                b(context, new File(str));
            } else {
                b(context, new File(j));
            }
            arrayList.add(r.a(0));
            arrayList.add(r.hr);
        }
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void m(Context context, c cVar) {
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        int h = cVar.h();
        if (r.b(j)) {
            cVar.a(1);
            return;
        }
        Uri parse = Uri.parse("tmast://download?pname=" + j + "&versioncode=" + h + "&oplist=\"1;2\"");
        Intent intent = new Intent();
        intent.setData(parse);
        intent.addFlags(268435456);
        intent.setAction("android.intent.action.VIEW");
        context.startActivity(intent);
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0064  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void n(android.content.Context r8, com.qq.g.c r9) {
        /*
            r7 = this;
            r1 = 0
            r0 = 1
            int r2 = r9.e()
            if (r2 >= r0) goto L_0x000c
            r9.a(r0)
        L_0x000b:
            return
        L_0x000c:
            java.lang.String r2 = r9.j()
            boolean r3 = com.qq.AppService.r.b(r2)
            if (r3 == 0) goto L_0x001a
            r9.a(r0)
            goto L_0x000b
        L_0x001a:
            boolean r3 = com.qq.b.a.f260a
            if (r3 != 0) goto L_0x0021
            com.qq.b.a.a(r8)
        L_0x0021:
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            boolean r4 = com.qq.b.a.c
            if (r4 != 0) goto L_0x002e
            boolean r4 = com.qq.b.a.b
            if (r4 == 0) goto L_0x007a
        L_0x002e:
            com.qq.b.a r4 = new com.qq.b.a
            r4.<init>()
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "/system/bin/pm uninstall "
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.StringBuilder r5 = r5.append(r2)
            java.lang.String r5 = r5.toString()
            r6 = 80000(0x13880, float:1.12104E-40)
            com.qq.b.c r5 = r4.d(r5, r6)
            r4.a()
            if (r5 == 0) goto L_0x007a
            byte[] r4 = r5.c
            if (r4 == 0) goto L_0x007a
            byte[] r4 = com.qq.AppService.r.a(r0)
            r3.add(r4)
            byte[] r4 = r5.c
            r3.add(r4)
        L_0x0062:
            if (r0 != 0) goto L_0x0073
            r7.c(r8, r2)
            byte[] r0 = com.qq.AppService.r.a(r1)
            r3.add(r0)
            byte[] r0 = com.qq.AppService.r.hr
            r3.add(r0)
        L_0x0073:
            r9.a(r3)
            r9.a(r1)
            goto L_0x000b
        L_0x007a:
            r0 = r1
            goto L_0x0062
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.provider.p.n(android.content.Context, com.qq.g.c):void");
    }

    public void o(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (r.b(j)) {
            cVar.a(1);
            return;
        }
        PackageInfo packageInfo = null;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(j, 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (packageInfo == null) {
            cVar.a(1);
            return;
        }
        String str = packageInfo.applicationInfo.dataDir;
        if (d.f266a == null) {
            d.b(context);
        }
        if (h.c(str, 775)) {
            ArrayList arrayList = new ArrayList();
            arrayList.add(r.a(str));
            cVar.a(arrayList);
            return;
        }
        cVar.a(8);
    }

    public void p(Context context, c cVar) {
        PackageInfo packageInfo;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (r.b(j)) {
            cVar.a(1);
            return;
        }
        try {
            packageInfo = context.getPackageManager().getPackageInfo(j, 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            packageInfo = null;
        }
        if (packageInfo == null) {
            cVar.a(1);
            return;
        }
        h.c("/data/local", 777);
        File file = new File("/data/local/tmp/");
        if (!file.exists()) {
            file.mkdirs();
        }
        h.c(file.getAbsolutePath(), 777);
        if (!file.exists() || !file.canWrite()) {
            if (!file.exists()) {
                file = new File("/data/data/" + AstApp.i().getPackageName());
            }
            if (!file.exists()) {
                cVar.a(4);
                return;
            }
            String str = packageInfo.applicationInfo.dataDir;
            h.c(str, 777);
            ArrayList arrayList = new ArrayList();
            arrayList.add(r.a(str));
            arrayList.add(r.a(file.getAbsolutePath()));
            cVar.a(arrayList);
            cVar.a(0);
            return;
        }
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(r.a("/data/data/"));
        arrayList2.add(r.a(file.getAbsolutePath()));
        cVar.a(arrayList2);
        cVar.a(0);
    }

    public void q(Context context, c cVar) {
        PackageInfo packageInfo;
        X509Certificate x509Certificate;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (r.b(j)) {
            cVar.a(1);
            return;
        }
        ArrayList arrayList = new ArrayList();
        PackageManager packageManager = context.getPackageManager();
        try {
            packageInfo = packageManager.getPackageInfo(j, 64);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            packageInfo = null;
        }
        if (packageInfo == null || packageInfo.applicationInfo == null || packageInfo.applicationInfo.sourceDir == null) {
            cVar.a(3);
            return;
        }
        arrayList.add(r.a(packageInfo.packageName));
        arrayList.add(r.a(packageInfo.applicationInfo.loadLabel(packageManager).toString()));
        arrayList.add(r.a(packageInfo.versionCode));
        arrayList.add(r.a(packageInfo.versionName));
        arrayList.add(r.a(packageInfo.applicationInfo.sourceDir));
        arrayList.add(r.a(packageInfo.applicationInfo.flags % 2));
        Signature[] signatureArr = packageInfo.signatures;
        if (signatureArr == null || signatureArr.length <= 0 || signatureArr[0] == null) {
            arrayList.add(r.hr);
        } else {
            try {
                x509Certificate = X509Certificate.getInstance(signatureArr[0].toByteArray());
            } catch (CertificateException e2) {
                e2.printStackTrace();
                x509Certificate = null;
            }
            if (x509Certificate != null) {
                arrayList.add(r.a(x509Certificate.getSerialNumber().toString(16)));
            } else {
                arrayList.add(r.hr);
            }
        }
        arrayList.add(r.a((String) null));
        cVar.a(arrayList);
    }

    public void r(Context context, c cVar) {
        int i;
        X509Certificate x509Certificate;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        ArrayList arrayList = new ArrayList();
        PackageManager packageManager = context.getPackageManager();
        List<PackageInfo> installedPackages = packageManager.getInstalledPackages(64);
        int h = cVar.h();
        int size = installedPackages.size();
        arrayList.add(r.a(size));
        int i2 = 0;
        int i3 = 0;
        while (i2 < size) {
            PackageInfo packageInfo = installedPackages.get(i2);
            int i4 = packageInfo.applicationInfo.flags % 2;
            if (h <= 0 || i4 != 1) {
                arrayList.add(r.a(packageInfo.packageName));
                arrayList.add(r.a(packageInfo.applicationInfo.loadLabel(packageManager).toString()));
                arrayList.add(r.a(packageInfo.versionCode));
                arrayList.add(r.a(packageInfo.versionName));
                arrayList.add(r.a(packageInfo.applicationInfo.sourceDir));
                arrayList.add(r.a(i4));
                Signature[] signatureArr = packageInfo.signatures;
                if (signatureArr == null || signatureArr.length <= 0 || signatureArr[0] == null) {
                    arrayList.add(r.hr);
                } else {
                    try {
                        x509Certificate = X509Certificate.getInstance(signatureArr[0].toByteArray());
                    } catch (CertificateException e) {
                        e.printStackTrace();
                        x509Certificate = null;
                    }
                    if (x509Certificate != null) {
                        arrayList.add(r.a(x509Certificate.getSerialNumber().toString(16)));
                    } else {
                        arrayList.add(r.hr);
                    }
                }
                arrayList.add(r.a((String) null));
                i = i3 + 1;
            } else {
                i = i3;
            }
            i2++;
            i3 = i;
        }
        arrayList.set(0, r.a(i3));
        cVar.a(arrayList);
    }

    public void s(Context context, c cVar) {
        int i;
        byte[] bArr;
        String str;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        ArrayList arrayList = new ArrayList();
        PackageManager packageManager = context.getPackageManager();
        List<PackageInfo> installedPackages = packageManager.getInstalledPackages(64);
        int h = cVar.h();
        int size = installedPackages.size();
        arrayList.add(r.a(size));
        int i2 = 0;
        int i3 = 0;
        while (i2 < size) {
            PackageInfo packageInfo = installedPackages.get(i2);
            int i4 = packageInfo.applicationInfo.flags % 2;
            if (h <= 0 || i4 != 1) {
                arrayList.add(r.a(packageInfo.packageName));
                arrayList.add(r.a(packageInfo.applicationInfo.loadLabel(packageManager).toString()));
                arrayList.add(r.a(packageInfo.versionCode));
                arrayList.add(r.a(packageInfo.versionName));
                arrayList.add(r.a(packageInfo.applicationInfo.sourceDir));
                arrayList.add(r.a(i4));
                File file = new File(packageInfo.applicationInfo.sourceDir);
                if (file.exists()) {
                    arrayList.add(r.a((int) file.length()));
                } else {
                    arrayList.add(r.a(0));
                }
                Signature[] signatureArr = packageInfo.signatures;
                if (signatureArr == null || signatureArr.length <= 0 || signatureArr[0] == null) {
                    arrayList.add(r.hr);
                } else {
                    java.security.cert.X509Certificate a2 = aa.a(signatureArr[0]);
                    if (a2 != null) {
                        try {
                            bArr = a2.getEncoded();
                        } catch (CertificateEncodingException e) {
                            e.printStackTrace();
                            bArr = null;
                        }
                        if (bArr != null) {
                            str = m.b(bArr);
                        } else {
                            str = null;
                        }
                        arrayList.add(r.a(str));
                    } else {
                        arrayList.add(r.hr);
                    }
                }
                i = i3 + 1;
            } else {
                i = i3;
            }
            i2++;
            i3 = i;
        }
        arrayList.set(0, r.a(i3));
        cVar.a(arrayList);
    }

    public void t(Context context, c cVar) {
        PackageInfo packageInfo;
        byte[] bArr;
        String str = null;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (r.b(j)) {
            cVar.a(1);
            return;
        }
        ArrayList arrayList = new ArrayList();
        PackageManager packageManager = context.getPackageManager();
        try {
            packageInfo = packageManager.getPackageInfo(j, 64);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            packageInfo = null;
        }
        if (packageInfo == null || packageInfo.applicationInfo == null || packageInfo.applicationInfo.sourceDir == null) {
            cVar.a(3);
            return;
        }
        arrayList.add(r.a(packageInfo.packageName));
        arrayList.add(r.a(packageInfo.applicationInfo.loadLabel(packageManager).toString()));
        arrayList.add(r.a(packageInfo.versionCode));
        arrayList.add(r.a(packageInfo.versionName));
        arrayList.add(r.a(packageInfo.applicationInfo.sourceDir));
        arrayList.add(r.a(packageInfo.applicationInfo.flags % 2));
        File file = new File(packageInfo.applicationInfo.sourceDir);
        if (file.exists()) {
            arrayList.add(r.a((int) file.length()));
        } else {
            arrayList.add(r.a(0));
        }
        Signature[] signatureArr = packageInfo.signatures;
        if (signatureArr == null || signatureArr.length <= 0 || signatureArr[0] == null) {
            arrayList.add(r.hr);
        } else {
            java.security.cert.X509Certificate a2 = aa.a(signatureArr[0]);
            if (a2 != null) {
                try {
                    bArr = a2.getEncoded();
                } catch (CertificateEncodingException e2) {
                    e2.printStackTrace();
                    bArr = null;
                }
                if (bArr != null) {
                    str = m.b(bArr);
                }
                arrayList.add(r.a(str));
            } else {
                arrayList.add(r.hr);
            }
        }
        cVar.a(arrayList);
    }

    public void u(Context context, c cVar) {
        byte[] bArr;
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        long currentTimeMillis = System.currentTimeMillis();
        int h = cVar.h();
        int h2 = cVar.h();
        if (h < 0) {
            h = 0;
        }
        if (cVar.e() < h + 2) {
            cVar.a(1);
            return;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(h));
        for (int i = 0; i < h; i++) {
            String j = cVar.j();
            File file = new File(j);
            if (!file.exists()) {
                cVar.a(6);
                return;
            }
            e eVar = new e();
            PackageInfo packageInfo = null;
            boolean z = false;
            if (h2 % 4 >= 2) {
                z = true;
            }
            try {
                packageInfo = eVar.a(j, z ? 4096 : 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
            arrayList.add(r.a(j));
            arrayList.add(r.a(eVar.f31a));
            if (packageInfo == null || packageInfo.applicationInfo == null) {
                arrayList.add(r.hr);
                arrayList.add(r.hr);
                arrayList.add(r.hr);
                arrayList.add(r.a(0));
            } else {
                String str = null;
                try {
                    str = eVar.a(context, j, packageInfo.applicationInfo.labelRes);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                arrayList.add(r.a(str));
                arrayList.add(r.a(packageInfo.applicationInfo.packageName));
                arrayList.add(r.a(packageInfo.versionName));
                arrayList.add(r.a(packageInfo.versionCode));
            }
            arrayList.add(r.a(r.b(file.lastModified())));
            arrayList.add(r.a((int) file.length()));
            if (packageInfo == null || packageInfo.requestedPermissions == null) {
                arrayList.add(r.a(0));
            } else {
                arrayList.add(r.a(r3));
                for (String a2 : packageInfo.requestedPermissions) {
                    arrayList.add(r.a(a2));
                }
            }
            if (h2 % 2 >= 1) {
                try {
                    bArr = eVar.c(context, j, packageInfo.applicationInfo.icon);
                } catch (Exception e3) {
                    e3.printStackTrace();
                    bArr = null;
                }
                if (bArr == null) {
                    bArr = r.hr;
                }
                arrayList.add(bArr);
            }
            eVar.a();
        }
        cVar.a(arrayList);
        cVar.a(0);
        Log.d("com.qq.connect", "time spent" + (System.currentTimeMillis() - currentTimeMillis));
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x00b2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void v(android.content.Context r11, com.qq.g.c r12) {
        /*
            r10 = this;
            int r0 = r12.e()
            r1 = 2
            if (r0 >= r1) goto L_0x000c
            r0 = 1
            r12.a(r0)
        L_0x000b:
            return
        L_0x000c:
            long r3 = java.lang.System.currentTimeMillis()
            java.lang.String r5 = r12.j()
            int r6 = r12.h()
            boolean r0 = com.qq.AppService.r.b(r5)
            if (r0 == 0) goto L_0x0023
            r0 = 1
            r12.a(r0)
            goto L_0x000b
        L_0x0023:
            java.io.File r7 = new java.io.File
            r7.<init>(r5)
            boolean r0 = r7.exists()
            if (r0 != 0) goto L_0x0033
            r0 = 6
            r12.a(r0)
            goto L_0x000b
        L_0x0033:
            android.content.pm.e r8 = new android.content.pm.e
            r8.<init>()
            r0 = 0
            r1 = 0
            int r2 = r6 % 4
            r9 = 2
            if (r2 < r9) goto L_0x0040
            r1 = 1
        L_0x0040:
            if (r1 == 0) goto L_0x00e0
            r1 = 4096(0x1000, float:5.74E-42)
        L_0x0044:
            android.content.pm.PackageInfo r0 = r8.a(r5, r1)     // Catch:{ Exception -> 0x00e3 }
        L_0x0048:
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>()
            int r1 = r8.f31a
            byte[] r1 = com.qq.AppService.r.a(r1)
            r9.add(r1)
            if (r0 == 0) goto L_0x005c
            android.content.pm.ApplicationInfo r1 = r0.applicationInfo
            if (r1 != 0) goto L_0x00e9
        L_0x005c:
            byte[] r1 = com.qq.AppService.r.hr
            r9.add(r1)
            byte[] r1 = com.qq.AppService.r.hr
            r9.add(r1)
            byte[] r1 = com.qq.AppService.r.hr
            r9.add(r1)
            r1 = 0
            byte[] r1 = com.qq.AppService.r.a(r1)
            r9.add(r1)
        L_0x0073:
            long r1 = r7.lastModified()
            java.lang.String r1 = com.qq.AppService.r.b(r1)
            byte[] r1 = com.qq.AppService.r.a(r1)
            r9.add(r1)
            long r1 = r7.length()
            int r1 = (int) r1
            byte[] r1 = com.qq.AppService.r.a(r1)
            r9.add(r1)
            if (r0 == 0) goto L_0x0094
            java.lang.String[] r1 = r0.requestedPermissions
            if (r1 != 0) goto L_0x011d
        L_0x0094:
            r1 = 0
            byte[] r1 = com.qq.AppService.r.a(r1)
            r9.add(r1)
        L_0x009c:
            int r1 = r6 % 2
            r2 = 1
            if (r1 < r2) goto L_0x00b7
            r1 = 0
            if (r0 == 0) goto L_0x013c
            android.content.pm.ApplicationInfo r2 = r0.applicationInfo
            if (r2 == 0) goto L_0x013c
            android.content.pm.ApplicationInfo r0 = r0.applicationInfo     // Catch:{ Exception -> 0x0138 }
            int r0 = r0.icon     // Catch:{ Exception -> 0x0138 }
            byte[] r0 = r8.c(r11, r5, r0)     // Catch:{ Exception -> 0x0138 }
        L_0x00b0:
            if (r0 != 0) goto L_0x00b4
            byte[] r0 = com.qq.AppService.r.hr
        L_0x00b4:
            r9.add(r0)
        L_0x00b7:
            r8.a()
            r12.a(r9)
            r0 = 0
            r12.a(r0)
            long r0 = java.lang.System.currentTimeMillis()
            java.lang.String r2 = "com.qq.connect"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "time spent"
            java.lang.StringBuilder r5 = r5.append(r6)
            long r0 = r0 - r3
            java.lang.StringBuilder r0 = r5.append(r0)
            java.lang.String r0 = r0.toString()
            android.util.Log.d(r2, r0)
            goto L_0x000b
        L_0x00e0:
            r1 = 0
            goto L_0x0044
        L_0x00e3:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0048
        L_0x00e9:
            r1 = 0
            android.content.pm.ApplicationInfo r2 = r0.applicationInfo     // Catch:{ Exception -> 0x0118 }
            int r2 = r2.labelRes     // Catch:{ Exception -> 0x0118 }
            java.lang.String r1 = r8.a(r11, r5, r2)     // Catch:{ Exception -> 0x0118 }
        L_0x00f2:
            byte[] r1 = com.qq.AppService.r.a(r1)
            r9.add(r1)
            android.content.pm.ApplicationInfo r1 = r0.applicationInfo
            java.lang.String r1 = r1.packageName
            byte[] r1 = com.qq.AppService.r.a(r1)
            r9.add(r1)
            java.lang.String r1 = r0.versionName
            byte[] r1 = com.qq.AppService.r.a(r1)
            r9.add(r1)
            int r1 = r0.versionCode
            byte[] r1 = com.qq.AppService.r.a(r1)
            r9.add(r1)
            goto L_0x0073
        L_0x0118:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00f2
        L_0x011d:
            java.lang.String[] r1 = r0.requestedPermissions
            int r2 = r1.length
            byte[] r1 = com.qq.AppService.r.a(r2)
            r9.add(r1)
            r1 = 0
        L_0x0128:
            if (r1 >= r2) goto L_0x009c
            java.lang.String[] r7 = r0.requestedPermissions
            r7 = r7[r1]
            byte[] r7 = com.qq.AppService.r.a(r7)
            r9.add(r7)
            int r1 = r1 + 1
            goto L_0x0128
        L_0x0138:
            r0 = move-exception
            r0.printStackTrace()
        L_0x013c:
            r0 = r1
            goto L_0x00b0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.provider.p.v(android.content.Context, com.qq.g.c):void");
    }

    public void w(Context context, c cVar) {
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        String j2 = cVar.j();
        File file = new File(j);
        File file2 = new File(j2);
        if (!file.exists() || !file2.exists()) {
            cVar.a(6);
            return;
        }
        Log.d("com.qq.connect", j + " --- " + j2);
        if (!new File("/data/data").canWrite()) {
            h.d("/data/data", 775);
        }
        String str = "/data/data/" + file2.getName();
        h.c(str, 777);
        int uid = new Native().getUid(str);
        if (uid <= 0) {
            PackageInfo packageInfo = null;
            try {
                packageInfo = context.getPackageManager().getPackageInfo(file2.getName(), 0);
            } catch (PackageManager.NameNotFoundException e) {
            }
            if (packageInfo != null) {
                uid = packageInfo.applicationInfo.uid;
            }
        }
        Log.d("com.qq.connect", "uid:" + uid);
        File[] listFiles = file2.listFiles();
        if (listFiles != null) {
            for (int i = 0; i < listFiles.length; i++) {
                if (!listFiles[i].getName().equals("lib")) {
                    File file3 = new File(str + File.separator + listFiles[i].getName());
                    if (file3.exists()) {
                        try {
                            n.b(file3);
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                    }
                    Log.d("com.qq.connect", "copy " + listFiles[i] + " -->" + file3);
                    try {
                        n.d(listFiles[i], file3);
                    } catch (Exception e3) {
                        e3.printStackTrace();
                    }
                    if (uid > 0) {
                        h.a(file3.getAbsolutePath(), uid);
                    }
                }
            }
        }
        h.c(str, 777);
        try {
            n.b(file2);
        } catch (Exception e4) {
            e4.printStackTrace();
        }
    }

    public void x(Context context, c cVar) {
        boolean z;
        int i;
        String str;
        String str2;
        String str3;
        boolean z2;
        boolean z3;
        if (cVar.e() < 3) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (cVar.h() > 0) {
            z = true;
        } else {
            z = false;
        }
        int h = cVar.h();
        if (h < 0) {
            h = 0;
        }
        if (Integer.parseInt(Build.VERSION.SDK) < 8) {
            i = 0;
        } else {
            i = h;
        }
        if (j == null) {
            cVar.a(1);
            return;
        }
        if (!a.f260a) {
            a.a(context);
        }
        if (j.startsWith(context.getApplicationInfo().dataDir)) {
            Native nativeR = new Native();
            if (nativeR.getFilePermission(j) % 10 < 6) {
                nativeR.setFilePermission(j, 511);
            }
        }
        Log.d("com.qq.connect", " in power_install ...........");
        if (j.contains(" ") || j.contains("\n") || j.contains("\r") || j.contains("'") || j.contains("\"")) {
            File file = new File(j);
            str = file.getParent() + "/" + System.currentTimeMillis() + "-" + new Random().nextInt(100000) + ".apk";
            file.renameTo(new File(str));
        } else {
            str = null;
        }
        if (a.c || a.b) {
            if (z) {
                str2 = "/system/bin/pm install " + " -r ";
            } else {
                str2 = "/system/bin/pm install ";
            }
            if (i == 1) {
                str2 = str2 + " -f ";
            } else if (i == 2) {
                str2 = str2 + " -s ";
            }
            if (str != null) {
                str3 = str2 + str;
            } else {
                str3 = str2 + j;
            }
            a aVar = new a();
            com.qq.b.c d = aVar.d(str3, 180000);
            aVar.a();
            if (d == null || d.c == null || !new String(d.c).contains("Success")) {
                z2 = false;
            } else {
                z2 = true;
            }
            ArrayList arrayList = new ArrayList();
            if (z2) {
                arrayList.add(r.a(0));
                arrayList.add(r.hr);
            } else {
                arrayList.add(r.a(1));
                if (d == null || d.c == null) {
                    arrayList.add(r.hr);
                } else {
                    arrayList.add(d.c);
                }
            }
            cVar.a(arrayList);
            if (str != null) {
                new File(str).renameTo(new File(j));
                return;
            }
            return;
        }
        if (str != null) {
            new File(str).renameTo(new File(j));
        }
        d.b(context);
        if (r.b(j) || !j.endsWith(".apk")) {
            cVar.a(1);
            return;
        }
        File file2 = new File(j);
        if (!file2.exists()) {
            cVar.a(6);
            return;
        }
        File file3 = new File("/data/local");
        if (!file3.exists() || !file3.canRead() || !file3.canWrite()) {
            com.qq.c.e.b("/data/local");
        }
        File file4 = new File("/data/local/" + System.currentTimeMillis());
        try {
            z3 = n.d(file2, file4);
        } catch (Exception e) {
            e.printStackTrace();
            z3 = false;
        }
        if (!z3) {
            file4.delete();
            cVar.a(11);
        } else if (!com.qq.c.e.b(file4.getAbsolutePath())) {
            file4.delete();
            cVar.a(4);
        } else {
            cVar.b = null;
            boolean a2 = f.a(file4.getAbsolutePath(), z, i, cVar);
            if (a2) {
                file4.delete();
            }
            ArrayList arrayList2 = new ArrayList();
            if (a2) {
                arrayList2.add(r.a(0));
                arrayList2.add(r.hr);
            } else {
                arrayList2.add(r.a(1));
                if (cVar.b != null) {
                    arrayList2.add(cVar.b);
                } else {
                    arrayList2.add(r.hr);
                }
            }
            cVar.a(arrayList2);
            cVar.a(0);
        }
    }

    public void y(Context context, c cVar) {
        String str;
        boolean z;
        boolean z2 = true;
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        int h = cVar.h();
        if (r.b(j)) {
            cVar.a(1);
            return;
        }
        PackageInfo packageInfo = null;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(j, 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (packageInfo == null) {
            cVar.a(7);
        } else if (a.c || a.b) {
            a aVar = new a();
            if (h > 0) {
                str = "/system/bin/pm  uninstall -k " + j;
            } else {
                str = "/system/bin/pm uninstall " + j;
            }
            com.qq.b.c d = aVar.d(str, 80000);
            aVar.a();
            if (d == null || d.c == null || !new String(d.c).contains("Success")) {
                z2 = false;
            }
            if (z2) {
                cVar.a(0);
            } else {
                cVar.a(8);
            }
        } else {
            d.b(context);
            if (h > 0) {
                z = true;
            } else {
                z = false;
            }
            if (f.a(j, z)) {
                cVar.a(0);
            } else {
                cVar.a(8);
            }
        }
    }

    public void z(Context context, c cVar) {
        ApplicationInfo applicationInfo;
        Intent intent = null;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        PackageManager packageManager = context.getPackageManager();
        try {
            applicationInfo = packageManager.getApplicationInfo(j, 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            applicationInfo = null;
        }
        if (applicationInfo == null) {
            cVar.a(1);
            return;
        }
        Intent intent2 = new Intent("com.android.launcher.action.UNINSTALL_SHORTCUT");
        intent2.putExtra("android.intent.extra.shortcut.NAME", applicationInfo.loadLabel(packageManager));
        try {
            intent = packageManager.getLaunchIntentForPackage(j);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        if (intent == null) {
            cVar.a(7);
            return;
        }
        intent.setAction("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.LAUNCHER");
        intent.setFlags(270532608);
        intent2.putExtra("android.intent.extra.shortcut.INTENT", intent);
        context.sendBroadcast(intent2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void A(Context context, c cVar) {
        ApplicationInfo applicationInfo;
        Intent intent;
        boolean z = true;
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        int h = cVar.h();
        PackageManager packageManager = context.getPackageManager();
        try {
            applicationInfo = packageManager.getApplicationInfo(j, 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            applicationInfo = null;
        }
        if (applicationInfo == null) {
            cVar.a(1);
            return;
        }
        Intent intent2 = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
        if (h > 0) {
            intent2.putExtra("duplicate", true);
        } else {
            intent2.putExtra("duplicate", false);
        }
        intent2.putExtra("android.intent.extra.shortcut.NAME", applicationInfo.loadLabel(packageManager));
        Intent.ShortcutIconResource shortcutIconResource = new Intent.ShortcutIconResource();
        shortcutIconResource.packageName = j;
        try {
            shortcutIconResource.resourceName = packageManager.getResourcesForApplication(applicationInfo).getResourceName(applicationInfo.icon);
        } catch (Exception e2) {
            e2.printStackTrace();
            z = false;
        }
        if (!z) {
            cVar.a(8);
            return;
        }
        intent2.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", shortcutIconResource);
        try {
            intent = packageManager.getLaunchIntentForPackage(j);
        } catch (Exception e3) {
            e3.printStackTrace();
            intent = null;
        }
        if (intent == null) {
            cVar.a(7);
            return;
        }
        intent.setAction("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.LAUNCHER");
        intent.setFlags(270532608);
        intent2.putExtra("android.intent.extra.shortcut.INTENT", intent);
        context.sendBroadcast(intent2);
    }

    public void B(Context context, c cVar) {
        PackageInfo packageInfo;
        byte[] bArr;
        String str;
        String str2 = null;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (r.b(j)) {
            cVar.a(1);
            return;
        }
        try {
            packageInfo = context.getPackageManager().getPackageInfo(j, 64);
        } catch (PackageManager.NameNotFoundException e) {
            packageInfo = null;
        }
        if (packageInfo == null) {
            cVar.a(1);
            return;
        }
        Signature[] signatureArr = packageInfo.signatures;
        if (signatureArr == null || signatureArr[0] == null) {
            cVar.a(7);
            return;
        }
        java.security.cert.X509Certificate a2 = aa.a(signatureArr[0]);
        if (a2 == null) {
            cVar.a(7);
            return;
        }
        try {
            bArr = a2.getEncoded();
        } catch (CertificateEncodingException e2) {
            bArr = null;
        }
        if (bArr != null) {
            str = m.b(bArr);
            str2 = m.b(new String(a(bArr)).getBytes());
        } else {
            str = null;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(j));
        arrayList.add(r.a(str));
        arrayList.add(r.a(str2));
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void C(Context context, c cVar) {
        int i;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(0));
        List<PackageInfo> installedPackages = context.getPackageManager().getInstalledPackages(64);
        if (installedPackages == null) {
            cVar.a(7);
            return;
        }
        int size = installedPackages.size();
        int i2 = 0;
        int i3 = 0;
        while (i2 < size) {
            PackageInfo packageInfo = installedPackages.get(i2);
            if (packageInfo != null) {
                if (packageInfo.applicationInfo == null) {
                    i = i3;
                } else if (h != 0 || packageInfo.applicationInfo.flags % 2 <= 0) {
                    Signature[] signatureArr = packageInfo.signatures;
                    if (signatureArr != null) {
                        if (signatureArr[0] == null) {
                            i = i3;
                        } else {
                            java.security.cert.X509Certificate a2 = aa.a(signatureArr[0]);
                            if (a2 == null) {
                                i = i3;
                            } else {
                                byte[] bArr = null;
                                try {
                                    bArr = a2.getEncoded();
                                } catch (CertificateEncodingException e) {
                                }
                                if (bArr != null) {
                                    String b = m.b(bArr);
                                    String b2 = m.b(new String(a(bArr)).getBytes());
                                    arrayList.add(r.a(packageInfo.packageName));
                                    arrayList.add(r.a(b));
                                    arrayList.add(r.a(b2));
                                    i = i3 + 1;
                                }
                            }
                        }
                    }
                } else {
                    i = i3;
                }
                i2++;
                i3 = i;
            }
            i = i3;
            i2++;
            i3 = i;
        }
        arrayList.set(0, r.a(i3));
        cVar.a(arrayList);
        cVar.a(0);
    }

    private static char[] a(byte[] bArr) {
        int i;
        int length = bArr.length;
        char[] cArr = new char[(length * 2)];
        for (int i2 = 0; i2 < length; i2++) {
            byte b = bArr[i2];
            int i3 = (b >> 4) & 15;
            cArr[i2 * 2] = (char) (i3 >= 10 ? (i3 + 97) - 10 : i3 + 48);
            byte b2 = b & 15;
            int i4 = (i2 * 2) + 1;
            if (b2 >= 10) {
                i = (b2 + 97) - 10;
            } else {
                i = b2 + 48;
            }
            cArr[i4] = (char) i;
        }
        return cArr;
    }

    public void D(Context context, c cVar) {
        if (cVar.e() < 3) {
            cVar.a(8);
            return;
        }
        String j = cVar.j();
        int h = cVar.h();
        int h2 = cVar.h();
        if (r.b(j)) {
            j = "/";
        }
        if (h < 0) {
            h = 0;
        }
        if (h2 < 1 || h2 > 3) {
            h2 = 3;
        }
        long currentTimeMillis = System.currentTimeMillis() + ((long) (j.hashCode() << 32));
        new com.qq.m.h(j, h, h2, currentTimeMillis).start();
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(currentTimeMillis));
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void E(Context context, c cVar) {
        Throwable th;
        com.qq.m.h hVar;
        if (cVar.e() < 2) {
            cVar.a(8);
            return;
        }
        String j = cVar.j();
        int h = cVar.h();
        if (r.b(j)) {
            j = Environment.getExternalStorageDirectory().getAbsolutePath();
        }
        if (h < 0) {
            h = 0;
        }
        Log.w("com.qq.connect", "scanPkgArchiveSync:" + j + h);
        try {
            hVar = new com.qq.m.h(j, h, 1, 0);
            try {
                hVar.run();
            } catch (Throwable th2) {
                th = th2;
                th.printStackTrace();
                Log.w("com.qq.connect", th.toString());
                Log.w("com.qq.connect", "scanPkgArchiveSync over");
                cVar.a(hVar.a());
                cVar.a(0);
            }
        } catch (Throwable th3) {
            th = th3;
            hVar = null;
            th.printStackTrace();
            Log.w("com.qq.connect", th.toString());
            Log.w("com.qq.connect", "scanPkgArchiveSync over");
            cVar.a(hVar.a());
            cVar.a(0);
        }
        Log.w("com.qq.connect", "scanPkgArchiveSync over");
        cVar.a(hVar.a());
        cVar.a(0);
    }
}
