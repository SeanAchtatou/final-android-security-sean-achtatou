package com.qq.provider;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.qq.AppService.r;
import com.qq.g.c;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class w {

    /* renamed from: a  reason: collision with root package name */
    public static final Uri f354a = Uri.parse("content://icc/adn");
    public static final Uri b = Uri.parse("content://simcontacts/simPeople");
    private static w c = null;

    private w() {
    }

    public static w a() {
        if (c == null) {
            c = new w();
        }
        return c;
    }

    public void a(Context context, c cVar) {
        int simState = ((TelephonyManager) context.getSystemService("phone")).getSimState();
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(simState));
        cVar.a(0);
        cVar.a(arrayList);
    }

    public void b(Context context, c cVar) {
        Cursor query = context.getContentResolver().query(f354a, null, null, null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        boolean moveToFirst = query.moveToFirst();
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.hs);
        boolean z = moveToFirst;
        int i = 0;
        while (z) {
            String string = query.getString(query.getColumnIndex("name"));
            String string2 = query.getString(query.getColumnIndex("number"));
            arrayList.add(r.a(query.getInt(query.getColumnIndex("_id"))));
            arrayList.add(r.a(string));
            arrayList.add(r.a(string2));
            z = query.moveToNext();
            i++;
        }
        query.close();
        arrayList.set(0, r.a(i));
        cVar.a(0);
        cVar.a(arrayList);
        Log.d("com.qq.connect", "queryADN:" + i);
    }

    public void c(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(0);
            return;
        }
        String j = cVar.j();
        String j2 = cVar.j();
        if (r.b(j)) {
            j = Constants.STR_EMPTY;
        }
        if (r.b(j2)) {
            j2 = Constants.STR_EMPTY;
        }
        if (context.getContentResolver().delete(f354a, "tag= '" + j + "' AND number= '" + j2 + "'", null) > 0) {
            cVar.a(0);
        } else {
            cVar.a(8);
        }
    }

    public void d(Context context, c cVar) {
        if (cVar.e() < 3) {
            cVar.a(0);
            return;
        }
        String j = cVar.j();
        String j2 = cVar.j();
        String j3 = cVar.j();
        String j4 = cVar.j();
        ContentValues contentValues = new ContentValues();
        contentValues.put("tag", j);
        contentValues.put("number", j2);
        contentValues.put("newTag", j3);
        contentValues.put("newNumber", j4);
        if (context.getContentResolver().update(f354a, contentValues, null, null) > 0) {
            cVar.a(0);
        } else {
            cVar.a(8);
        }
    }

    public void e(Context context, c cVar) {
        int i;
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        String j2 = cVar.j();
        ContentValues contentValues = new ContentValues();
        if (r.b(j)) {
            contentValues.putNull("tag");
        } else {
            contentValues.put("tag", j);
        }
        if (r.b(j2)) {
            contentValues.putNull("number");
        } else {
            contentValues.put("number", j2);
        }
        Uri insert = context.getContentResolver().insert(f354a, contentValues);
        if (insert == null) {
            cVar.a(8);
            return;
        }
        Cursor query = context.getContentResolver().query(b, null, null, null, null);
        if (query != null) {
            contentValues.clear();
            if (r.b(j)) {
                contentValues.putNull("name");
            } else {
                contentValues.put("name", j);
            }
            if (r.b(j2)) {
                contentValues.putNull("number");
            } else {
                contentValues.put("number", j2);
            }
            context.getContentResolver().insert(b, contentValues);
            query.close();
        }
        ArrayList arrayList = new ArrayList();
        try {
            i = Integer.parseInt(insert.getLastPathSegment());
        } catch (Exception e) {
            e.printStackTrace();
            i = 0;
        }
        arrayList.add(r.a(i));
        cVar.a(0);
        cVar.a(arrayList);
    }
}
