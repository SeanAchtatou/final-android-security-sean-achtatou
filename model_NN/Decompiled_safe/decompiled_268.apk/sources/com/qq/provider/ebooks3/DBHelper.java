package com.qq.provider.ebooks3;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.tencent.open.SocialConstants;

/* compiled from: ProGuard */
public class DBHelper extends SQLiteOpenHelper {
    public static final String CONTACTS_TABLE = "ebooks";
    private static final String DATABASE_CREATE = "CREATE TABLE ebooks (_id integer primary key autoincrement,name text,type text,size long,data text,kind integer,label text,date_add long,date_modify long,date_last_read long,read_times text);";
    public static final String DATABASE_NAME = "ebooks.db";
    public static final int DATABASE_VERSION = 6;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 6);
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        Log.d("com.qq.connect", "onCreate db!");
        sQLiteDatabase.execSQL(DATABASE_CREATE);
        sQLiteDatabase.execSQL(createLogin());
        sQLiteDatabase.execSQL(createQXInfo());
        sQLiteDatabase.execSQL(createQXFriends());
        sQLiteDatabase.execSQL(createFriendsData());
    }

    public void onOpen(SQLiteDatabase sQLiteDatabase) {
        super.onOpen(sQLiteDatabase);
        int version = sQLiteDatabase.getVersion();
        if (version != 0) {
            if (version < 6) {
                onUpgrade(sQLiteDatabase, version, 6);
            } else if (version > 6) {
                onDowngrade(sQLiteDatabase, version, 6);
            }
        }
    }

    private String createFriendsData() {
        return "CREATE TABLE qx_cache (" + "_id  integer primary key autoincrement," + "f_id integer ," + "type integer," + "data text);";
    }

    private String createQXFriends() {
        return "CREATE TABLE qx_friends_table (" + "_id integer primary key autoincrement," + "de integer , " + "number text);";
    }

    private String createQXInfo() {
        return "CREATE TABLE qx_user (" + "_id integer primary key autoincrement," + "uid long ," + "key text," + "number text," + "lc_scan_time long," + "lc_scan_count integer," + "lc_scan_lid integer," + "status integer);";
    }

    private String createLogin() {
        return "CREATE TABLE login (" + "_id integer primary key autoincrement," + "name text," + "key text," + "type integer," + "date long);";
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        Log.d("com.qq.connect", "onUpgrade " + i + ":" + i2);
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS ebooks");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS login");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS qx_user");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS qx_friends_table");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS qx_cache");
        onCreate(sQLiteDatabase);
        sQLiteDatabase.setVersion(i2);
    }

    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        Log.d("com.qq.connect", "onDowngrade " + i + ":" + i2);
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS ebooks");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS login");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS qx_user");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS qx_friends_table");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS qx_cache");
        onCreate(sQLiteDatabase);
        sQLiteDatabase.setVersion(i2);
    }

    public void savePcInfo(String str, String str2) {
        SQLiteDatabase sQLiteDatabase;
        try {
            sQLiteDatabase = getWritableDatabase();
        } catch (SQLException e) {
            e.printStackTrace();
            sQLiteDatabase = null;
        }
        if (sQLiteDatabase != null) {
            Cursor query = sQLiteDatabase.query("login", new String[]{"key", "_id"}, "key = '" + str2 + "'", null, null, null, "_id");
            if (query != null) {
                if (query.moveToFirst()) {
                    query.close();
                    sQLiteDatabase.close();
                    return;
                }
                query.close();
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put("key", str2);
            contentValues.put(SocialConstants.PARAM_TYPE, "0");
            if (str != null) {
                contentValues.put("name", str);
            } else {
                contentValues.putNull("name");
            }
            contentValues.put("date", Long.valueOf(System.currentTimeMillis()));
            try {
                sQLiteDatabase.insertOrThrow("login", null, contentValues);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            sQLiteDatabase.close();
        }
    }
}
