package com.qq.provider.cache2;

import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import android.util.Log;
import com.qq.AppService.r;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public int f335a = 0;
    public String b = null;
    public String c = null;
    public String d = null;
    public int e = 0;
    public int f = 0;
    public String g = null;
    public long h = 0;
    public long i = 0;
    public byte[] j = null;
    public long k = 0;

    public ContentValues a() {
        if (this.b == null) {
            return null;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("pkg", this.b);
        if (this.c != null) {
            contentValues.put("app_name", this.c);
        }
        contentValues.put("version_code", Integer.valueOf(this.e));
        contentValues.put("flag", Integer.valueOf(this.f % 2));
        if (this.g != null) {
            contentValues.put("path", this.g);
        }
        contentValues.put("size", Long.valueOf(this.h));
        contentValues.put("date", Long.valueOf(this.i));
        if (this.j != null) {
            contentValues.put("icon", this.j);
        }
        if (this.d != null) {
            contentValues.put("version", this.d);
        }
        contentValues.put("time", Long.valueOf(this.k));
        return contentValues;
    }

    public int a(Context context) {
        if (this.b == null) {
            return -1;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("pkg", this.b);
        if (this.c != null) {
            contentValues.put("app_name", this.c);
        }
        contentValues.put("version_code", Integer.valueOf(this.e));
        contentValues.put("flag", Integer.valueOf(this.f % 2));
        if (this.g != null) {
            contentValues.put("path", this.g);
        }
        contentValues.put("size", Long.valueOf(this.h));
        contentValues.put("date", Long.valueOf(this.i));
        if (this.j != null) {
            contentValues.put("icon", this.j);
        }
        if (this.d != null) {
            contentValues.put("version", this.d);
        }
        contentValues.put("time", Long.valueOf(this.k));
        Uri insert = context.getContentResolver().insert(CacheProvider.f334a, contentValues);
        if (insert != null) {
            return Integer.parseInt(insert.getLastPathSegment());
        }
        return -1;
    }

    public static void a(Context context, String str) {
        PackageInfo packageInfo;
        a aVar;
        if (str != null && context != null) {
            PackageManager packageManager = context.getPackageManager();
            try {
                packageInfo = packageManager.getPackageInfo(str, 0);
            } catch (PackageManager.NameNotFoundException e2) {
                packageInfo = null;
            }
            if (packageInfo == null || packageInfo.applicationInfo == null) {
                context.getContentResolver().delete(CacheProvider.f334a, "pkg = '" + str + "'", null);
                return;
            }
            Cursor query = context.getContentResolver().query(CacheProvider.f334a, new String[]{"_id", "pkg", "version_code", "date", "time", "version"}, "pkg = '" + str + "'", null, null);
            if (query != null) {
                if (query.moveToFirst()) {
                    aVar = new a();
                    aVar.f335a = query.getInt(0);
                    aVar.b = query.getString(1);
                    aVar.e = query.getInt(2);
                    aVar.d = query.getString(5);
                    aVar.i = query.getLong(3);
                    aVar.k = query.getLong(4);
                } else {
                    aVar = null;
                }
                query.close();
                if (aVar == null) {
                    b(context, packageInfo, packageManager);
                } else if (aVar.e != packageInfo.versionCode) {
                    context.getContentResolver().delete(CacheProvider.f334a, "pkg = '" + packageInfo.packageName + "'", null);
                    b(context, packageInfo, packageManager);
                } else if (packageInfo.applicationInfo.sourceDir == null && aVar.g != null) {
                    context.getContentResolver().delete(CacheProvider.f334a, "pkg = '" + packageInfo.packageName + "'", null);
                    b(context, packageInfo, packageManager);
                } else if (packageInfo.applicationInfo.sourceDir != null && aVar.g == null) {
                    context.getContentResolver().delete(CacheProvider.f334a, "pkg = '" + packageInfo.packageName + "'", null);
                    b(context, packageInfo, packageManager);
                } else if (packageInfo.applicationInfo.sourceDir != null && aVar.g != null && !aVar.g.equals(packageInfo.applicationInfo.sourceDir)) {
                    context.getContentResolver().delete(CacheProvider.f334a, "pkg = '" + packageInfo.packageName + "'", null);
                    b(context, packageInfo, packageManager);
                }
            }
        }
    }

    public static void b(Context context) {
        a[] aVarArr = null;
        PackageManager packageManager = context.getPackageManager();
        Cursor query = context.getContentResolver().query(CacheProvider.f334a, new String[]{"_id", "pkg", "version_code", "date", "time", "version"}, null, null, null);
        if (query != null) {
            int count = query.getCount();
            if (count > 0) {
                aVarArr = new a[count];
            }
            int i2 = 0;
            for (boolean moveToFirst = query.moveToFirst(); moveToFirst && aVarArr != null; moveToFirst = query.moveToNext()) {
                aVarArr[i2] = new a();
                aVarArr[i2].f335a = query.getInt(0);
                aVarArr[i2].b = query.getString(1);
                aVarArr[i2].e = query.getInt(2);
                aVarArr[i2].d = query.getString(5);
                aVarArr[i2].i = query.getLong(3);
                aVarArr[i2].k = query.getLong(4);
                i2++;
            }
            query.close();
            Log.d("com.qq.connect", "updateAllIconCache over ,cache count: " + count);
            a(context, packageManager, aVarArr);
        }
    }

    private static void a(Context context, PackageManager packageManager, a[] aVarArr) {
        ContentValues a2;
        ContentValues a3;
        a a4;
        List<PackageInfo> installedPackages = packageManager.getInstalledPackages(8192);
        if (installedPackages != null && installedPackages.size() > 0) {
            int size = installedPackages.size();
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            for (int i2 = 0; i2 < size; i2++) {
                PackageInfo packageInfo = installedPackages.get(i2);
                if (packageInfo != null && !r.b(packageInfo.packageName)) {
                    int a5 = a(packageInfo.packageName, aVarArr);
                    if (a5 == -1) {
                        a a6 = a(context, packageInfo, packageManager);
                        if (a6 != null) {
                            arrayList.add(a6);
                        }
                    } else if (!(aVarArr[a5].i == new File(packageInfo.applicationInfo.sourceDir).lastModified() || (a4 = a(context, packageInfo, packageManager)) == null)) {
                        arrayList2.add(a4);
                    }
                }
            }
            ArrayList arrayList3 = new ArrayList();
            if (arrayList.size() > 0) {
                int size2 = arrayList.size();
                for (int i3 = 0; i3 < size2; i3++) {
                    a aVar = (a) arrayList.get(i3);
                    if (!(aVar == null || (a3 = aVar.a()) == null)) {
                        arrayList3.add(ContentProviderOperation.newInsert(CacheProvider.f334a).withValues(a3).build());
                    }
                }
            }
            if (arrayList2.size() > 0) {
                int size3 = arrayList2.size();
                for (int i4 = 0; i4 < size3; i4++) {
                    a aVar2 = (a) arrayList2.get(i4);
                    if (!(aVar2 == null || (a2 = aVar2.a()) == null)) {
                        arrayList3.add(ContentProviderOperation.newUpdate(CacheProvider.f334a).withValues(a2).withSelection("pkg = '" + aVar2.b + "'", null).build());
                    }
                }
            }
            if (arrayList3.size() != 0) {
                try {
                    context.getContentResolver().applyBatch("com.qq.provider.cache2", arrayList3);
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                } catch (OperationApplicationException e3) {
                    e3.printStackTrace();
                }
            }
        }
    }

    public static int a(String str, a[] aVarArr) {
        if (aVarArr == null || str == null) {
            return -1;
        }
        int length = aVarArr.length;
        for (int i2 = 0; i2 < length; i2++) {
            if (aVarArr[i2] != null && aVarArr[i2].b != null && aVarArr[i2].b.equals(str)) {
                return i2;
            }
        }
        return -1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:47:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.qq.provider.cache2.a a(android.content.Context r6, android.content.pm.PackageInfo r7, android.content.pm.PackageManager r8) {
        /*
            r1 = 0
            if (r7 == 0) goto L_0x0007
            android.content.pm.ApplicationInfo r0 = r7.applicationInfo
            if (r0 != 0) goto L_0x0008
        L_0x0007:
            return r1
        L_0x0008:
            com.qq.provider.cache2.a r2 = new com.qq.provider.cache2.a
            r2.<init>()
            java.lang.String r0 = r7.packageName
            r2.b = r0
            android.content.pm.ApplicationInfo r0 = r7.applicationInfo
            java.lang.CharSequence r0 = r0.loadLabel(r8)
            java.lang.String r0 = r0.toString()
            r2.c = r0
            java.lang.String r0 = r2.c
            java.lang.String r3 = r2.b
            boolean r0 = r0.equals(r3)
            if (r0 == 0) goto L_0x003d
            android.content.pm.ApplicationInfo r0 = r7.applicationInfo
            int r0 = r0.labelRes
            if (r0 == 0) goto L_0x003d
            android.content.pm.ApplicationInfo r0 = r7.applicationInfo     // Catch:{ Throwable -> 0x00a3 }
            android.content.res.Resources r0 = r8.getResourcesForApplication(r0)     // Catch:{ Throwable -> 0x00a3 }
            android.content.pm.ApplicationInfo r3 = r7.applicationInfo     // Catch:{ Throwable -> 0x00a3 }
            int r3 = r3.labelRes     // Catch:{ Throwable -> 0x00a3 }
            java.lang.String r0 = r0.getString(r3)     // Catch:{ Throwable -> 0x00a3 }
            r2.c = r0     // Catch:{ Throwable -> 0x00a3 }
        L_0x003d:
            java.lang.String r0 = r7.versionName
            r2.d = r0
            int r0 = r7.versionCode
            r2.e = r0
            android.content.pm.ApplicationInfo r0 = r7.applicationInfo
            int r0 = r0.flags
            r2.f = r0
            android.content.pm.ApplicationInfo r0 = r7.applicationInfo
            java.lang.String r0 = r0.sourceDir
            r2.g = r0
            java.lang.String r0 = r2.g
            boolean r0 = com.qq.AppService.r.b(r0)
            if (r0 != 0) goto L_0x00e4
            java.io.File r0 = new java.io.File
            java.lang.String r3 = r2.g
            r0.<init>(r3)
        L_0x0060:
            if (r0 == 0) goto L_0x006e
            long r3 = r0.length()
            r2.h = r3
            long r3 = r0.lastModified()
            r2.i = r3
        L_0x006e:
            long r3 = java.lang.System.currentTimeMillis()
            r2.k = r3
            android.content.pm.ApplicationInfo r0 = r7.applicationInfo     // Catch:{ Exception -> 0x00d5 }
            android.graphics.drawable.Drawable r0 = r8.getApplicationIcon(r0)     // Catch:{ Exception -> 0x00d5 }
            if (r0 == 0) goto L_0x00d3
            boolean r3 = r0 instanceof android.graphics.drawable.BitmapDrawable     // Catch:{ Exception -> 0x00d5 }
            if (r3 == 0) goto L_0x00a8
            android.graphics.drawable.BitmapDrawable r0 = (android.graphics.drawable.BitmapDrawable) r0     // Catch:{ Exception -> 0x00d5 }
            android.graphics.Bitmap r0 = r0.getBitmap()     // Catch:{ Exception -> 0x00d5 }
        L_0x0086:
            if (r0 == 0) goto L_0x0007
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream
            r1.<init>()
            android.graphics.Bitmap$CompressFormat r3 = android.graphics.Bitmap.CompressFormat.PNG
            r4 = 100
            r0.compress(r3, r4, r1)
            r1.flush()     // Catch:{ IOException -> 0x00da }
        L_0x0097:
            byte[] r0 = r1.toByteArray()
            r2.j = r0
            r1.close()     // Catch:{ IOException -> 0x00df }
        L_0x00a0:
            r1 = r2
            goto L_0x0007
        L_0x00a3:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x003d
        L_0x00a8:
            boolean r3 = r0 instanceof android.graphics.drawable.StateListDrawable     // Catch:{ Exception -> 0x00d5 }
            if (r3 == 0) goto L_0x00b7
            android.graphics.drawable.Drawable r0 = r0.getCurrent()     // Catch:{ Exception -> 0x00d5 }
            android.graphics.drawable.BitmapDrawable r0 = (android.graphics.drawable.BitmapDrawable) r0     // Catch:{ Exception -> 0x00d5 }
            android.graphics.Bitmap r0 = r0.getBitmap()     // Catch:{ Exception -> 0x00d5 }
            goto L_0x0086
        L_0x00b7:
            java.lang.String r3 = "com.qq.connect"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00d5 }
            r4.<init>()     // Catch:{ Exception -> 0x00d5 }
            java.lang.String r5 = "unkown drawable!::"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x00d5 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00d5 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ Exception -> 0x00d5 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00d5 }
            android.util.Log.d(r3, r0)     // Catch:{ Exception -> 0x00d5 }
        L_0x00d3:
            r0 = r1
            goto L_0x0086
        L_0x00d5:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00d3
        L_0x00da:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0097
        L_0x00df:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00a0
        L_0x00e4:
            r0 = r1
            goto L_0x0060
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.provider.cache2.a.a(android.content.Context, android.content.pm.PackageInfo, android.content.pm.PackageManager):com.qq.provider.cache2.a");
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0068 A[Catch:{ Exception -> 0x0097 }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x009c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int b(android.content.Context r7, android.content.pm.PackageInfo r8, android.content.pm.PackageManager r9) {
        /*
            r1 = 0
            r2 = -1
            if (r8 == 0) goto L_0x0008
            android.content.pm.ApplicationInfo r0 = r8.applicationInfo
            if (r0 != 0) goto L_0x000a
        L_0x0008:
            r0 = r2
        L_0x0009:
            return r0
        L_0x000a:
            com.qq.provider.cache2.a r3 = new com.qq.provider.cache2.a
            r3.<init>()
            java.lang.String r0 = r8.packageName
            r3.b = r0
            android.content.pm.ApplicationInfo r0 = r8.applicationInfo
            java.lang.CharSequence r0 = r0.loadLabel(r9)
            java.lang.String r0 = (java.lang.String) r0
            r3.c = r0
            java.lang.String r0 = r8.versionName
            r3.d = r0
            int r0 = r8.versionCode
            r3.e = r0
            android.content.pm.ApplicationInfo r0 = r8.applicationInfo
            int r0 = r0.flags
            r3.f = r0
            android.content.pm.ApplicationInfo r0 = r8.applicationInfo
            java.lang.String r0 = r0.sourceDir
            r3.g = r0
            java.lang.String r0 = r3.g
            boolean r0 = com.qq.AppService.r.b(r0)
            if (r0 != 0) goto L_0x00c4
            java.io.File r0 = new java.io.File
            java.lang.String r4 = r3.g
            r0.<init>(r4)
        L_0x0040:
            if (r0 == 0) goto L_0x004e
            long r4 = r0.length()
            r3.h = r4
            long r4 = r0.lastModified()
            r3.i = r4
        L_0x004e:
            long r4 = java.lang.System.currentTimeMillis()
            r3.k = r4
            android.content.pm.ApplicationInfo r0 = r8.applicationInfo     // Catch:{ Exception -> 0x0097 }
            android.graphics.drawable.Drawable r0 = r0.loadIcon(r9)     // Catch:{ Exception -> 0x0097 }
            if (r0 == 0) goto L_0x0095
            boolean r4 = r0 instanceof android.graphics.drawable.BitmapDrawable     // Catch:{ Exception -> 0x0097 }
            if (r4 == 0) goto L_0x006a
            android.graphics.drawable.BitmapDrawable r0 = (android.graphics.drawable.BitmapDrawable) r0     // Catch:{ Exception -> 0x0097 }
            android.graphics.Bitmap r0 = r0.getBitmap()     // Catch:{ Exception -> 0x0097 }
        L_0x0066:
            if (r0 != 0) goto L_0x009c
            r0 = r2
            goto L_0x0009
        L_0x006a:
            boolean r4 = r0 instanceof android.graphics.drawable.StateListDrawable     // Catch:{ Exception -> 0x0097 }
            if (r4 == 0) goto L_0x0079
            android.graphics.drawable.Drawable r0 = r0.getCurrent()     // Catch:{ Exception -> 0x0097 }
            android.graphics.drawable.BitmapDrawable r0 = (android.graphics.drawable.BitmapDrawable) r0     // Catch:{ Exception -> 0x0097 }
            android.graphics.Bitmap r0 = r0.getBitmap()     // Catch:{ Exception -> 0x0097 }
            goto L_0x0066
        L_0x0079:
            java.lang.String r4 = "com.qq.connect"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0097 }
            r5.<init>()     // Catch:{ Exception -> 0x0097 }
            java.lang.String r6 = "unkown drawable!::"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0097 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0097 }
            java.lang.StringBuilder r0 = r5.append(r0)     // Catch:{ Exception -> 0x0097 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0097 }
            android.util.Log.d(r4, r0)     // Catch:{ Exception -> 0x0097 }
        L_0x0095:
            r0 = r1
            goto L_0x0066
        L_0x0097:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0095
        L_0x009c:
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream
            r1.<init>()
            android.graphics.Bitmap$CompressFormat r2 = android.graphics.Bitmap.CompressFormat.PNG
            r4 = 100
            r0.compress(r2, r4, r1)
            r1.flush()     // Catch:{ IOException -> 0x00ba }
        L_0x00ab:
            byte[] r0 = r1.toByteArray()
            r3.j = r0
            r1.close()     // Catch:{ IOException -> 0x00bf }
        L_0x00b4:
            int r0 = r3.a(r7)
            goto L_0x0009
        L_0x00ba:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00ab
        L_0x00bf:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00b4
        L_0x00c4:
            r0 = r1
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.provider.cache2.a.b(android.content.Context, android.content.pm.PackageInfo, android.content.pm.PackageManager):int");
    }
}
