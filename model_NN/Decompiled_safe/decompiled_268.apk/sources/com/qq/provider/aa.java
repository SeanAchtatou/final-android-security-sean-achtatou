package com.qq.provider;

import android.content.Context;
import com.qq.g.c;

/* compiled from: ProGuard */
public class aa {

    /* renamed from: a  reason: collision with root package name */
    private static aa f318a = null;
    private final int b = 1;
    private final int c = 2;
    private final int d = 3;
    private final int e = 4;

    private aa() {
    }

    public static aa a() {
        if (f318a == null) {
            f318a = new aa();
        }
        return f318a;
    }

    public void a(Context context, c cVar) {
        boolean z;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        getClass();
        if (h == 1) {
            z = a(context);
        } else {
            getClass();
            if (h == 2) {
                z = b(context);
            } else {
                getClass();
                if (h == 3) {
                    z = c(context);
                } else {
                    getClass();
                    if (h == 4) {
                        z = d(context);
                    } else {
                        z = false;
                    }
                }
            }
        }
        if (z) {
            cVar.a(0);
        } else {
            cVar.a(8);
        }
    }

    public boolean a(Context context) {
        return true;
    }

    public boolean b(Context context) {
        return true;
    }

    public boolean c(Context context) {
        return true;
    }

    public boolean d(Context context) {
        return true;
    }
}
