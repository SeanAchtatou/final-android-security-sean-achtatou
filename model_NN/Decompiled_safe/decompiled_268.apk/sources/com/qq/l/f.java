package com.qq.l;

import android.content.Context;
import com.qq.util.a;
import com.qq.util.k;
import com.qq.util.m;
import com.tencent.assistant.db.table.i;
import com.tencent.assistant.utils.t;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

/* compiled from: ProGuard */
public class f {
    private static volatile f e;

    /* renamed from: a  reason: collision with root package name */
    private String f305a;
    /* access modifiers changed from: private */
    public Context b;
    private volatile boolean c = false;
    private Thread d;

    public static synchronized f a() {
        f fVar;
        synchronized (f.class) {
            if (e == null) {
                e = new f();
            }
            fVar = e;
        }
        return fVar;
    }

    private f() {
    }

    private String b(Context context) {
        if (this.f305a == null) {
            this.f305a = t.g();
            if (this.f305a == "000000000000000" || this.f305a == null) {
                this.f305a = "null";
            }
        }
        return this.f305a;
    }

    public void a(Context context) {
        this.b = context;
        this.f305a = b(this.b);
        if (this.d == null || !this.d.isAlive()) {
            this.d = new g(this);
            this.d.start();
        }
        this.c = false;
    }

    public void b() {
        this.c = true;
        synchronized (this) {
            notify();
        }
        c();
    }

    private void c() {
        e.a().b();
    }

    /* access modifiers changed from: private */
    public ConcurrentLinkedQueue<d> d() {
        ConcurrentLinkedQueue<d> concurrentLinkedQueue = new ConcurrentLinkedQueue<>();
        List<d> a2 = i.a().a(50);
        if (a2 != null && !a2.isEmpty()) {
            int size = a2.size();
            for (int i = 0; i < size; i++) {
                concurrentLinkedQueue.offer(a2.get(i));
            }
        }
        return concurrentLinkedQueue;
    }

    /* access modifiers changed from: private */
    public void a(Context context, ConcurrentLinkedQueue<d> concurrentLinkedQueue) {
        long j;
        if (concurrentLinkedQueue != null && c(context)) {
            ArrayList arrayList = new ArrayList();
            Iterator<d> it = concurrentLinkedQueue.iterator();
            long j2 = 1000;
            while (it.hasNext()) {
                d next = it.next();
                if (a(context, next.b(), next.c()) != 200) {
                    if (j2 >= 64000) {
                        break;
                    }
                    synchronized (this) {
                        try {
                            wait(j2);
                        } catch (InterruptedException e2) {
                        }
                    }
                    j = j2 * 2;
                } else {
                    it.remove();
                    arrayList.add(Long.valueOf(next.a()));
                    j = 1000;
                }
                if (this.c) {
                    break;
                }
                j2 = j;
            }
            i.a().b(arrayList);
            concurrentLinkedQueue.clear();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x00b2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int a(android.content.Context r11, java.lang.String r12, java.lang.String r13) {
        /*
            r10 = this;
            r7 = 0
            r6 = -1
            java.lang.String r0 = r10.a(r11, r12)
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 == 0) goto L_0x000e
            r0 = r6
        L_0x000d:
            return r0
        L_0x000e:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r2 = "&"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = "log"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = "="
            r0.append(r2)
            java.lang.String r0 = "UTF-8"
            java.lang.String r0 = java.net.URLEncoder.encode(r13, r0)     // Catch:{ UnsupportedEncodingException -> 0x009b }
            r1.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x009b }
            java.lang.String r0 = r1.toString()     // Catch:{ UnsupportedEncodingException -> 0x009b }
            java.lang.String r1 = "UTF-8"
            byte[] r0 = r0.getBytes(r1)     // Catch:{ UnsupportedEncodingException -> 0x009b }
            java.lang.String r1 = "1B61$CABC!A241%3A53^3D4B"
            java.lang.String r0 = r10.a(r0, r1)     // Catch:{ UnsupportedEncodingException -> 0x009b }
            boolean r1 = android.text.TextUtils.isEmpty(r0)     // Catch:{ UnsupportedEncodingException -> 0x009b }
            if (r1 == 0) goto L_0x0049
            r0 = r6
            goto L_0x000d
        L_0x0049:
            java.lang.String r1 = "UTF-8"
            java.lang.String r0 = java.net.URLEncoder.encode(r0, r1)     // Catch:{ UnsupportedEncodingException -> 0x009b }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r1 = "post"
            java.lang.StringBuilder r1 = r8.append(r1)
            java.lang.String r2 = "="
            java.lang.StringBuilder r1 = r1.append(r2)
            r1.append(r0)
            com.qq.i.a r0 = new com.qq.i.a     // Catch:{ Throwable -> 0x00a2, all -> 0x00af }
            java.lang.String r1 = "http://agent.sj.qq.com/query.do"
            r2 = 1
            r3 = 0
            r4 = 0
            r5 = 0
            r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ Throwable -> 0x00a2, all -> 0x00af }
            java.lang.String r1 = "application/x-www-form-urlencoded"
            r0.a(r1)     // Catch:{ Throwable -> 0x00bd, all -> 0x00b6 }
            r1 = 35000(0x88b8, float:4.9045E-41)
            r0.a(r1)     // Catch:{ Throwable -> 0x00bd, all -> 0x00b6 }
            r1 = 15000(0x3a98, float:2.102E-41)
            r0.b(r1)     // Catch:{ Throwable -> 0x00bd, all -> 0x00b6 }
            r0.b()     // Catch:{ Throwable -> 0x00bd, all -> 0x00b6 }
            java.lang.String r1 = r8.toString()     // Catch:{ Throwable -> 0x00bd, all -> 0x00b6 }
            byte[] r1 = r1.getBytes()     // Catch:{ Throwable -> 0x00bd, all -> 0x00b6 }
            r0.a(r1)     // Catch:{ Throwable -> 0x00bd, all -> 0x00b6 }
            r0.c()     // Catch:{ Throwable -> 0x00bd, all -> 0x00b6 }
            int r1 = r0.d()     // Catch:{ Throwable -> 0x00bd, all -> 0x00b6 }
            if (r0 == 0) goto L_0x0098
            r0.f()
        L_0x0098:
            r0 = r1
            goto L_0x000d
        L_0x009b:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r6
            goto L_0x000d
        L_0x00a2:
            r0 = move-exception
            r1 = r7
        L_0x00a4:
            r0.printStackTrace()     // Catch:{ all -> 0x00ba }
            if (r1 == 0) goto L_0x00ac
            r1.f()
        L_0x00ac:
            r0 = r6
            goto L_0x000d
        L_0x00af:
            r0 = move-exception
        L_0x00b0:
            if (r7 == 0) goto L_0x00b5
            r7.f()
        L_0x00b5:
            throw r0
        L_0x00b6:
            r1 = move-exception
            r7 = r0
            r0 = r1
            goto L_0x00b0
        L_0x00ba:
            r0 = move-exception
            r7 = r1
            goto L_0x00b0
        L_0x00bd:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x00a4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.l.f.a(android.content.Context, java.lang.String, java.lang.String):int");
    }

    private String a(Context context, String str) {
        StringBuilder sb = new StringBuilder();
        try {
            String encode = URLEncoder.encode(b(context), "UTF-8");
            sb.append("key").append("=").append(encode).append("&").append("aid").append("=").append(str);
            String sb2 = sb.toString();
            StringBuilder sb3 = new StringBuilder();
            sb3.append("d41d8cd98f00b204e9800998ecf8427e").append("key").append(encode).append("aid").append(str);
            String b2 = m.b(sb3.toString().getBytes());
            sb = new StringBuilder();
            sb.append(sb2).append("&").append("api_sig").append("=").append(b2);
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        return sb.toString();
    }

    private String a(byte[] bArr, String str) {
        try {
            return new String(a.a(k.a(str.getBytes("UTF-8"), bArr)));
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        return null;
    }

    private boolean c(Context context) {
        return com.tencent.connector.ipc.a.e(context) == 1;
    }
}
