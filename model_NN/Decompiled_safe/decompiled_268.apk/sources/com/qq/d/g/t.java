package com.qq.d.g;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import com.tencent.assistant.st.STConst;
import com.tencent.connect.common.Constants;
import java.lang.reflect.Method;
import java.net.NetworkInterface;
import java.util.Enumeration;

/* compiled from: ProGuard */
public class t {
    private static String a(byte b) {
        String str = STConst.ST_STATUS_DEFAULT + Integer.toHexString(b).toUpperCase();
        int length = str.length();
        return str.substring(length - 2, length);
    }

    public static String a(Context context) {
        String macAddress;
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_WIFI_STATE") == 0) {
            try {
                WifiInfo connectionInfo = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo();
                if (!(connectionInfo == null || (macAddress = connectionInfo.getMacAddress()) == null || macAddress.length() <= 0)) {
                    return macAddress;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        String a2 = a();
        return (a2 == null || a2.length() <= 0) ? "00-00-00-00-00-00" : a2;
    }

    public static String a() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            if (networkInterfaces != null) {
                Method method = Class.forName("java.net.NetworkInterface").getMethod("getHardwareAddress", new Class[0]);
                while (networkInterfaces.hasMoreElements()) {
                    Object invoke = method.invoke(networkInterfaces.nextElement(), new Object[0]);
                    if (invoke instanceof byte[]) {
                        byte[] bArr = (byte[]) invoke;
                        StringBuffer stringBuffer = new StringBuffer();
                        for (int i = 0; i < bArr.length - 1; i++) {
                            stringBuffer.append(a(bArr[i]));
                            stringBuffer.append("-");
                        }
                        stringBuffer.append(a(bArr[bArr.length - 1]));
                        return stringBuffer.toString();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Constants.STR_EMPTY;
    }
}
