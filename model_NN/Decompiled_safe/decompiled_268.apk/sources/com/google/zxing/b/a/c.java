package com.google.zxing.b.a;

import com.google.zxing.j;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private final int f136a;
    private final int[] b;
    private final j[] c;

    public c(int i, int[] iArr, int i2, int i3, int i4) {
        this.f136a = i;
        this.b = iArr;
        this.c = new j[]{new j((float) i2, (float) i4), new j((float) i3, (float) i4)};
    }

    public int a() {
        return this.f136a;
    }

    public int[] b() {
        return this.b;
    }

    public j[] c() {
        return this.c;
    }
}
