package com.google.zxing.b.a.a.a;

import com.google.zxing.common.a;
import com.tencent.connect.common.Constants;

public abstract class j {

    /* renamed from: a  reason: collision with root package name */
    protected final a f128a;
    protected final s b;

    j(a aVar) {
        this.f128a = aVar;
        this.b = new s(aVar);
    }

    public static j a(a aVar) {
        if (aVar.a(1)) {
            return new g(aVar);
        }
        if (!aVar.a(2)) {
            return new k(aVar);
        }
        switch (s.a(aVar, 1, 4)) {
            case 4:
                return new a(aVar);
            case 5:
                return new b(aVar);
            default:
                switch (s.a(aVar, 1, 5)) {
                    case 12:
                        return new c(aVar);
                    case 13:
                        return new d(aVar);
                    default:
                        switch (s.a(aVar, 1, 7)) {
                            case 56:
                                return new e(aVar, "310", Constants.VIA_REPORT_TYPE_SHARE_TO_QZONE);
                            case 57:
                                return new e(aVar, "320", Constants.VIA_REPORT_TYPE_SHARE_TO_QZONE);
                            case 58:
                                return new e(aVar, "310", Constants.VIA_REPORT_TYPE_JOININ_GROUP);
                            case 59:
                                return new e(aVar, "320", Constants.VIA_REPORT_TYPE_JOININ_GROUP);
                            case 60:
                                return new e(aVar, "310", Constants.VIA_REPORT_TYPE_WPA_STATE);
                            case 61:
                                return new e(aVar, "320", Constants.VIA_REPORT_TYPE_WPA_STATE);
                            case 62:
                                return new e(aVar, "310", "17");
                            case 63:
                                return new e(aVar, "320", "17");
                            default:
                                throw new IllegalStateException(new StringBuffer().append("unknown decoder: ").append(aVar).toString());
                        }
                }
        }
    }

    public abstract String a();
}
