package com.google.zxing.b;

import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.common.a;
import com.google.zxing.h;
import com.google.zxing.j;
import com.tencent.assistant.component.TouchAnalizer;
import java.util.Hashtable;

public final class c extends k {

    /* renamed from: a  reason: collision with root package name */
    static final int[] f139a = {52, 289, 97, 352, 49, 304, 112, 37, 292, 100, 265, 73, 328, 25, 280, 88, 13, 268, 76, 28, 259, 67, 322, 19, 274, 82, 7, 262, 70, 22, 385, 193, 448, 145, TouchAnalizer.CLICK_AREA, 208, 133, 388, 196, 148, 168, 162, 138, 42};
    private static final char[] b = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. *$/+%".toCharArray();
    private static final int c = f139a[39];
    private final boolean d;
    private final boolean e;

    public c() {
        this.d = false;
        this.e = false;
    }

    public c(boolean z) {
        this.d = z;
        this.e = false;
    }

    private static char a(int i) {
        for (int i2 = 0; i2 < f139a.length; i2++) {
            if (f139a[i2] == i) {
                return b[i2];
            }
        }
        throw NotFoundException.a();
    }

    private static int a(int[] iArr) {
        int i = 0;
        while (true) {
            int i2 = Integer.MAX_VALUE;
            for (int i3 : iArr) {
                if (i3 < i2 && i3 > i) {
                    i2 = i3;
                }
            }
            int i4 = 0;
            int i5 = 0;
            int i6 = 0;
            for (int i7 = 0; i7 < r7; i7++) {
                int i8 = iArr[i7];
                if (iArr[i7] > i2) {
                    i4 |= 1 << ((r7 - 1) - i7);
                    i6++;
                    i5 += i8;
                }
            }
            if (i6 == 3) {
                int i9 = i6;
                for (int i10 = 0; i10 < r7 && i9 > 0; i10++) {
                    int i11 = iArr[i10];
                    if (iArr[i10] > i2) {
                        i9--;
                        if ((i11 << 1) >= i5) {
                            return -1;
                        }
                    }
                }
                return i4;
            } else if (i6 <= 3) {
                return -1;
            } else {
                i = i2;
            }
        }
    }

    private static String a(StringBuffer stringBuffer) {
        int i;
        char c2;
        int length = stringBuffer.length();
        StringBuffer stringBuffer2 = new StringBuffer(length);
        int i2 = 0;
        while (i2 < length) {
            char charAt = stringBuffer.charAt(i2);
            if (charAt == '+' || charAt == '$' || charAt == '%' || charAt == '/') {
                char charAt2 = stringBuffer.charAt(i2 + 1);
                switch (charAt) {
                    case '$':
                        if (charAt2 >= 'A' && charAt2 <= 'Z') {
                            c2 = (char) (charAt2 - '@');
                            break;
                        } else {
                            throw FormatException.a();
                        }
                        break;
                    case '%':
                        if (charAt2 < 'A' || charAt2 > 'E') {
                            if (charAt2 >= 'F' && charAt2 <= 'W') {
                                c2 = (char) (charAt2 - 11);
                                break;
                            } else {
                                throw FormatException.a();
                            }
                        } else {
                            c2 = (char) (charAt2 - '&');
                            break;
                        }
                        break;
                    case '+':
                        if (charAt2 >= 'A' && charAt2 <= 'Z') {
                            c2 = (char) (charAt2 + ' ');
                            break;
                        } else {
                            throw FormatException.a();
                        }
                        break;
                    case '/':
                        if (charAt2 >= 'A' && charAt2 <= 'O') {
                            c2 = (char) (charAt2 - ' ');
                            break;
                        } else if (charAt2 == 'Z') {
                            c2 = ':';
                            break;
                        } else {
                            throw FormatException.a();
                        }
                    default:
                        c2 = 0;
                        break;
                }
                stringBuffer2.append(c2);
                i = i2 + 1;
            } else {
                stringBuffer2.append(charAt);
                i = i2;
            }
            i2 = i + 1;
        }
        return stringBuffer2.toString();
    }

    private static int[] a(a aVar) {
        int a2 = aVar.a();
        int i = 0;
        while (i < a2 && !aVar.a(i)) {
            i++;
        }
        int[] iArr = new int[9];
        int length = iArr.length;
        boolean z = false;
        int i2 = 0;
        for (int i3 = i; i3 < a2; i3++) {
            if (aVar.a(i3) ^ z) {
                iArr[i2] = iArr[i2] + 1;
            } else {
                if (i2 != length - 1) {
                    i2++;
                } else if (a(iArr) != c || !aVar.a(Math.max(0, i - ((i3 - i) / 2)), i, false)) {
                    i += iArr[0] + iArr[1];
                    for (int i4 = 2; i4 < length; i4++) {
                        iArr[i4 - 2] = iArr[i4];
                    }
                    iArr[length - 2] = 0;
                    iArr[length - 1] = 0;
                    i2--;
                } else {
                    return new int[]{i, i3};
                }
                iArr[i2] = 1;
                z = !z;
            }
        }
        throw NotFoundException.a();
    }

    public h a(int i, a aVar, Hashtable hashtable) {
        int[] a2 = a(aVar);
        int i2 = a2[1];
        int a3 = aVar.a();
        while (i2 < a3 && !aVar.a(i2)) {
            i2++;
        }
        StringBuffer stringBuffer = new StringBuffer(20);
        int[] iArr = new int[9];
        while (true) {
            a(aVar, i2, iArr);
            int a4 = a(iArr);
            if (a4 < 0) {
                throw NotFoundException.a();
            }
            char a5 = a(a4);
            stringBuffer.append(a5);
            int i3 = i2;
            for (int i4 : iArr) {
                i3 += i4;
            }
            int i5 = i3;
            while (i5 < a3 && !aVar.a(i5)) {
                i5++;
            }
            if (a5 == '*') {
                stringBuffer.deleteCharAt(stringBuffer.length() - 1);
                int i6 = 0;
                for (int i7 : iArr) {
                    i6 += i7;
                }
                int i8 = (i5 - i2) - i6;
                if (i5 == a3 || i8 / 2 >= i6) {
                    if (this.d) {
                        int length = stringBuffer.length() - 1;
                        int i9 = 0;
                        for (int i10 = 0; i10 < length; i10++) {
                            i9 += "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. *$/+%".indexOf(stringBuffer.charAt(i10));
                        }
                        if (stringBuffer.charAt(length) != b[i9 % 43]) {
                            throw ChecksumException.a();
                        }
                        stringBuffer.deleteCharAt(length);
                    }
                    if (stringBuffer.length() == 0) {
                        throw NotFoundException.a();
                    }
                    return new h(this.e ? a(stringBuffer) : stringBuffer.toString(), null, new j[]{new j(((float) (a2[1] + a2[0])) / 2.0f, (float) i), new j(((float) (i2 + i5)) / 2.0f, (float) i)}, com.google.zxing.a.i);
                }
                throw NotFoundException.a();
            }
            i2 = i5;
        }
    }
}
