package com.google.zxing.common.reedsolomon;

final class b {

    /* renamed from: a  reason: collision with root package name */
    private final a f170a;
    private final int[] b;

    b(a aVar, int[] iArr) {
        int i = 1;
        if (iArr == null || iArr.length == 0) {
            throw new IllegalArgumentException();
        }
        this.f170a = aVar;
        int length = iArr.length;
        if (length <= 1 || iArr[0] != 0) {
            this.b = iArr;
            return;
        }
        while (i < length && iArr[i] == 0) {
            i++;
        }
        if (i == length) {
            this.b = aVar.a().b;
            return;
        }
        this.b = new int[(length - i)];
        System.arraycopy(iArr, i, this.b, 0, this.b.length);
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.b.length - 1;
    }

    /* access modifiers changed from: package-private */
    public int a(int i) {
        return this.b[(this.b.length - 1) - i];
    }

    /* access modifiers changed from: package-private */
    public b a(int i, int i2) {
        if (i < 0) {
            throw new IllegalArgumentException();
        } else if (i2 == 0) {
            return this.f170a.a();
        } else {
            int length = this.b.length;
            int[] iArr = new int[(length + i)];
            for (int i3 = 0; i3 < length; i3++) {
                iArr[i3] = this.f170a.c(this.b[i3], i2);
            }
            return new b(this.f170a, iArr);
        }
    }

    /* access modifiers changed from: package-private */
    public b a(b bVar) {
        if (!this.f170a.equals(bVar.f170a)) {
            throw new IllegalArgumentException("GF256Polys do not have same GF256 field");
        } else if (b()) {
            return bVar;
        } else {
            if (bVar.b()) {
                return this;
            }
            int[] iArr = this.b;
            int[] iArr2 = bVar.b;
            if (iArr.length <= iArr2.length) {
                int[] iArr3 = iArr2;
                iArr2 = iArr;
                iArr = iArr3;
            }
            int[] iArr4 = new int[iArr.length];
            int length = iArr.length - iArr2.length;
            System.arraycopy(iArr, 0, iArr4, 0, length);
            for (int i = length; i < iArr.length; i++) {
                iArr4[i] = a.b(iArr2[i - length], iArr[i]);
            }
            return new b(this.f170a, iArr4);
        }
    }

    /* access modifiers changed from: package-private */
    public int b(int i) {
        int i2 = 0;
        if (i == 0) {
            return a(0);
        }
        int length = this.b.length;
        if (i == 1) {
            int i3 = 0;
            while (i2 < length) {
                int b2 = a.b(i3, this.b[i2]);
                i2++;
                i3 = b2;
            }
            return i3;
        }
        int i4 = this.b[0];
        int i5 = 1;
        while (i5 < length) {
            int b3 = a.b(this.f170a.c(i, i4), this.b[i5]);
            i5++;
            i4 = b3;
        }
        return i4;
    }

    /* access modifiers changed from: package-private */
    public b b(b bVar) {
        if (!this.f170a.equals(bVar.f170a)) {
            throw new IllegalArgumentException("GF256Polys do not have same GF256 field");
        } else if (b() || bVar.b()) {
            return this.f170a.a();
        } else {
            int[] iArr = this.b;
            int length = iArr.length;
            int[] iArr2 = bVar.b;
            int length2 = iArr2.length;
            int[] iArr3 = new int[((length + length2) - 1)];
            for (int i = 0; i < length; i++) {
                int i2 = iArr[i];
                for (int i3 = 0; i3 < length2; i3++) {
                    iArr3[i + i3] = a.b(iArr3[i + i3], this.f170a.c(i2, iArr2[i3]));
                }
            }
            return new b(this.f170a, iArr3);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return this.b[0] == 0;
    }

    /* access modifiers changed from: package-private */
    public b c(int i) {
        if (i == 0) {
            return this.f170a.a();
        }
        if (i == 1) {
            return this;
        }
        int length = this.b.length;
        int[] iArr = new int[length];
        for (int i2 = 0; i2 < length; i2++) {
            iArr[i2] = this.f170a.c(this.b[i2], i);
        }
        return new b(this.f170a, iArr);
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer(a() * 8);
        for (int a2 = a(); a2 >= 0; a2--) {
            int a3 = a(a2);
            if (a3 != 0) {
                if (a3 < 0) {
                    stringBuffer.append(" - ");
                    a3 = -a3;
                } else if (stringBuffer.length() > 0) {
                    stringBuffer.append(" + ");
                }
                if (a2 == 0 || a3 != 1) {
                    int b2 = this.f170a.b(a3);
                    if (b2 == 0) {
                        stringBuffer.append('1');
                    } else if (b2 == 1) {
                        stringBuffer.append('a');
                    } else {
                        stringBuffer.append("a^");
                        stringBuffer.append(b2);
                    }
                }
                if (a2 != 0) {
                    if (a2 == 1) {
                        stringBuffer.append('x');
                    } else {
                        stringBuffer.append("x^");
                        stringBuffer.append(a2);
                    }
                }
            }
        }
        return stringBuffer.toString();
    }
}
