package com.google.zxing.d.a;

import com.google.zxing.FormatException;
import com.google.zxing.common.c;
import com.google.zxing.common.d;
import com.google.zxing.common.g;
import com.google.zxing.common.o;
import java.io.UnsupportedEncodingException;
import java.util.Hashtable;
import java.util.Vector;

final class m {

    /* renamed from: a  reason: collision with root package name */
    private static final char[] f177a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', ' ', '$', '%', '*', '+', '-', '.', '/', ':'};

    private m() {
    }

    private static char a(int i) {
        if (i < f177a.length) {
            return f177a[i];
        }
        throw FormatException.a();
    }

    private static int a(c cVar) {
        int a2 = cVar.a(8);
        if ((a2 & 128) == 0) {
            return a2 & 127;
        }
        if ((a2 & 192) == 128) {
            return ((a2 & 63) << 8) | cVar.a(8);
        } else if ((a2 & 224) == 192) {
            return ((a2 & 31) << 16) | cVar.a(16);
        } else {
            throw new IllegalArgumentException(new StringBuffer().append("Bad ECI bits starting with byte ").append(a2).toString());
        }
    }

    static g a(byte[] bArr, r rVar, o oVar, Hashtable hashtable) {
        q a2;
        boolean z;
        Vector vector = null;
        c cVar = new c(bArr);
        StringBuffer stringBuffer = new StringBuffer(50);
        boolean z2 = false;
        Vector vector2 = new Vector(1);
        d dVar = null;
        while (true) {
            if (cVar.a() < 4) {
                a2 = q.f181a;
            } else {
                try {
                    a2 = q.a(cVar.a(4));
                } catch (IllegalArgumentException e) {
                    throw FormatException.a();
                }
            }
            if (a2.equals(q.f181a)) {
                z = z2;
            } else if (a2.equals(q.h) || a2.equals(q.i)) {
                z = true;
            } else if (a2.equals(q.d)) {
                cVar.a(16);
                z = z2;
            } else if (a2.equals(q.f)) {
                dVar = d.a(a(cVar));
                if (dVar == null) {
                    throw FormatException.a();
                }
                z = z2;
            } else {
                int a3 = cVar.a(a2.a(rVar));
                if (a2.equals(q.b)) {
                    b(cVar, stringBuffer, a3);
                    z = z2;
                } else if (a2.equals(q.c)) {
                    a(cVar, stringBuffer, a3, z2);
                    z = z2;
                } else if (a2.equals(q.e)) {
                    a(cVar, stringBuffer, a3, dVar, vector2, hashtable);
                    z = z2;
                } else if (a2.equals(q.g)) {
                    a(cVar, stringBuffer, a3);
                    z = z2;
                } else {
                    throw FormatException.a();
                }
            }
            if (a2.equals(q.f181a)) {
                String stringBuffer2 = stringBuffer.toString();
                if (!vector2.isEmpty()) {
                    vector = vector2;
                }
                return new g(bArr, stringBuffer2, vector, oVar);
            }
            z2 = z;
        }
    }

    private static void a(c cVar, StringBuffer stringBuffer, int i) {
        byte[] bArr = new byte[(i * 2)];
        int i2 = 0;
        while (i > 0) {
            int a2 = cVar.a(13);
            int i3 = (a2 % 192) | ((a2 / 192) << 8);
            int i4 = i3 + (i3 < 7936 ? 33088 : 49472);
            bArr[i2] = (byte) (i4 >> 8);
            bArr[i2 + 1] = (byte) i4;
            i--;
            i2 += 2;
        }
        try {
            stringBuffer.append(new String(bArr, "SJIS"));
        } catch (UnsupportedEncodingException e) {
            throw FormatException.a();
        }
    }

    private static void a(c cVar, StringBuffer stringBuffer, int i, d dVar, Vector vector, Hashtable hashtable) {
        byte[] bArr = new byte[i];
        if ((i << 3) > cVar.a()) {
            throw FormatException.a();
        }
        for (int i2 = 0; i2 < i; i2++) {
            bArr[i2] = (byte) cVar.a(8);
        }
        try {
            stringBuffer.append(new String(bArr, dVar == null ? o.a(bArr, hashtable) : dVar.a()));
            vector.addElement(bArr);
        } catch (UnsupportedEncodingException e) {
            throw FormatException.a();
        }
    }

    private static void a(c cVar, StringBuffer stringBuffer, int i, boolean z) {
        while (i > 1) {
            int a2 = cVar.a(11);
            stringBuffer.append(a(a2 / 45));
            stringBuffer.append(a(a2 % 45));
            i -= 2;
        }
        if (i == 1) {
            stringBuffer.append(a(cVar.a(6)));
        }
        if (z) {
            for (int length = stringBuffer.length(); length < stringBuffer.length(); length++) {
                if (stringBuffer.charAt(length) == '%') {
                    if (length >= stringBuffer.length() - 1 || stringBuffer.charAt(length + 1) != '%') {
                        stringBuffer.setCharAt(length, 29);
                    } else {
                        stringBuffer.deleteCharAt(length + 1);
                    }
                }
            }
        }
    }

    private static void b(c cVar, StringBuffer stringBuffer, int i) {
        while (i >= 3) {
            int a2 = cVar.a(10);
            if (a2 >= 1000) {
                throw FormatException.a();
            }
            stringBuffer.append(a(a2 / 100));
            stringBuffer.append(a((a2 / 10) % 10));
            stringBuffer.append(a(a2 % 10));
            i -= 3;
        }
        if (i == 2) {
            int a3 = cVar.a(7);
            if (a3 >= 100) {
                throw FormatException.a();
            }
            stringBuffer.append(a(a3 / 10));
            stringBuffer.append(a(a3 % 10));
        } else if (i == 1) {
            int a4 = cVar.a(4);
            if (a4 >= 10) {
                throw FormatException.a();
            }
            stringBuffer.append(a(a4));
        }
    }
}
