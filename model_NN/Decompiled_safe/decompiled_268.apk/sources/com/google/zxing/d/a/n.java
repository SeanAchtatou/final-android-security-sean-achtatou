package com.google.zxing.d.a;

import com.google.zxing.ChecksumException;
import com.google.zxing.common.b;
import com.google.zxing.common.g;
import com.google.zxing.common.reedsolomon.ReedSolomonException;
import com.google.zxing.common.reedsolomon.a;
import com.google.zxing.common.reedsolomon.c;
import java.util.Hashtable;

public final class n {

    /* renamed from: a  reason: collision with root package name */
    private final c f178a = new c(a.f169a);

    private void a(byte[] bArr, int i) {
        int length = bArr.length;
        int[] iArr = new int[length];
        for (int i2 = 0; i2 < length; i2++) {
            iArr[i2] = bArr[i2] & 255;
        }
        try {
            this.f178a.a(iArr, bArr.length - i);
            for (int i3 = 0; i3 < i; i3++) {
                bArr[i3] = (byte) iArr[i3];
            }
        } catch (ReedSolomonException e) {
            throw ChecksumException.a();
        }
    }

    public g a(b bVar, Hashtable hashtable) {
        a aVar = new a(bVar);
        r b = aVar.b();
        o a2 = aVar.a().a();
        b[] a3 = b.a(aVar.c(), b, a2);
        int i = 0;
        for (b a4 : a3) {
            i += a4.a();
        }
        byte[] bArr = new byte[i];
        int i2 = 0;
        for (b bVar2 : a3) {
            byte[] b2 = bVar2.b();
            int a5 = bVar2.a();
            a(b2, a5);
            int i3 = 0;
            while (i3 < a5) {
                bArr[i2] = b2[i3];
                i3++;
                i2++;
            }
        }
        return m.a(bArr, b, a2, hashtable);
    }
}
