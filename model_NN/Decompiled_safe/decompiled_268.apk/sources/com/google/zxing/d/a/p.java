package com.google.zxing.d.a;

final class p {

    /* renamed from: a  reason: collision with root package name */
    private static final int[][] f180a = {new int[]{21522, 0}, new int[]{20773, 1}, new int[]{24188, 2}, new int[]{23371, 3}, new int[]{17913, 4}, new int[]{16590, 5}, new int[]{20375, 6}, new int[]{19104, 7}, new int[]{30660, 8}, new int[]{29427, 9}, new int[]{32170, 10}, new int[]{30877, 11}, new int[]{26159, 12}, new int[]{25368, 13}, new int[]{27713, 14}, new int[]{26998, 15}, new int[]{5769, 16}, new int[]{5054, 17}, new int[]{7399, 18}, new int[]{6608, 19}, new int[]{1890, 20}, new int[]{597, 21}, new int[]{3340, 22}, new int[]{2107, 23}, new int[]{13663, 24}, new int[]{12392, 25}, new int[]{16177, 26}, new int[]{14854, 27}, new int[]{9396, 28}, new int[]{8579, 29}, new int[]{11994, 30}, new int[]{11245, 31}};
    private static final int[] b = {0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4};
    private final o c;
    private final byte d;

    private p(int i) {
        this.c = o.a((i >> 3) & 3);
        this.d = (byte) (i & 7);
    }

    static int a(int i, int i2) {
        int i3 = i ^ i2;
        return b[(i3 >>> 28) & 15] + b[i3 & 15] + b[(i3 >>> 4) & 15] + b[(i3 >>> 8) & 15] + b[(i3 >>> 12) & 15] + b[(i3 >>> 16) & 15] + b[(i3 >>> 20) & 15] + b[(i3 >>> 24) & 15];
    }

    static p b(int i, int i2) {
        p c2 = c(i, i2);
        return c2 != null ? c2 : c(i ^ 21522, i2 ^ 21522);
    }

    private static p c(int i, int i2) {
        int i3 = 0;
        int i4 = Integer.MAX_VALUE;
        for (int[] iArr : f180a) {
            int i5 = iArr[0];
            if (i5 == i || i5 == i2) {
                return new p(iArr[1]);
            }
            int a2 = a(i, i5);
            if (a2 < i4) {
                i3 = iArr[1];
            } else {
                a2 = i4;
            }
            if (i == i2 || (i4 = a(i2, i5)) >= a2) {
                i4 = a2;
            } else {
                i3 = iArr[1];
            }
        }
        if (i4 <= 3) {
            return new p(i3);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public o a() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public byte b() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof p)) {
            return false;
        }
        p pVar = (p) obj;
        return this.c == pVar.c && this.d == pVar.d;
    }

    public int hashCode() {
        return (this.c.a() << 3) | this.d;
    }
}
