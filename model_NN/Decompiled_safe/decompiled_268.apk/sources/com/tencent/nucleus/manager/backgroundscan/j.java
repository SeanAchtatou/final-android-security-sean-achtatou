package com.tencent.nucleus.manager.backgroundscan;

import com.tencent.assistant.db.table.f;
import com.tencent.assistant.localres.callback.b;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.XLog;
import com.tencent.nucleus.manager.backgroundscan.BackgroundScanManager;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
class j implements b {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BackgroundScanManager f2831a;

    j(BackgroundScanManager backgroundScanManager) {
        this.f2831a = backgroundScanManager;
    }

    public void a(List<LocalApkInfo> list) {
    }

    public void a(List<LocalApkInfo> list, boolean z, boolean z2, int i) {
        if (i != 0 || list == null || list.isEmpty()) {
            XLog.i("BackgroundScan", "<scan> pkg scan finish , none suggested pkgs !");
            this.f2831a.i.put((byte) 3, BackgroundScanManager.SStatus.finish);
            return;
        }
        a((long) b(list));
        XLog.i("BackgroundScan", "<scan> pkg scan finish ,suggested size = " + b(list));
    }

    public void a(int i, int i2, long j) {
    }

    public void b(List<LocalApkInfo> list, int i, long j) {
    }

    public void a(List<LocalApkInfo> list, LocalApkInfo localApkInfo, boolean z, boolean z2) {
    }

    public void a(List<LocalApkInfo> list, int i, long j) {
    }

    private void a(long j) {
        BackgroundScan a2 = this.f2831a.a((byte) 3);
        a2.e = j;
        f.a().b(a2);
        this.f2831a.i.put((byte) 3, BackgroundScanManager.SStatus.finish);
        this.f2831a.f.b(this.f2831a.j);
    }

    private int b(List<LocalApkInfo> list) {
        int i = 0;
        Iterator<LocalApkInfo> it = list.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            if (it.next().mIsSmartSlect) {
                i = i2 + 1;
            } else {
                i = i2;
            }
        }
    }
}
