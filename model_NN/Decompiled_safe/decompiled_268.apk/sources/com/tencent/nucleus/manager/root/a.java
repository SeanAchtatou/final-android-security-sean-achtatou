package com.tencent.nucleus.manager.root;

import android.os.Handler;
import android.os.Message;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.module.k;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;

/* compiled from: ProGuard */
class a extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RootUtilInstallActivity f2969a;

    a(RootUtilInstallActivity rootUtilInstallActivity) {
        this.f2969a = rootUtilInstallActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.root.RootUtilInstallActivity.a(com.tencent.nucleus.manager.root.RootUtilInstallActivity, boolean):void
     arg types: [com.tencent.nucleus.manager.root.RootUtilInstallActivity, int]
     candidates:
      com.tencent.nucleus.manager.root.RootUtilInstallActivity.a(com.tencent.nucleus.manager.root.RootUtilInstallActivity, com.tencent.pangu.download.DownloadInfo):java.lang.CharSequence
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.nucleus.manager.root.RootUtilInstallActivity.a(com.tencent.nucleus.manager.root.RootUtilInstallActivity, boolean):void */
    public void handleMessage(Message message) {
        super.handleMessage(message);
        switch (message.what) {
            case EventDispatcherEnum.PLUGIN_EVENT_LOGIN_START:
                this.f2969a.B.a(this.f2969a.A);
                this.f2969a.B.register(this.f2969a);
                return;
            case 10002:
                this.f2969a.x.a("打开");
                this.f2969a.b(false);
                this.f2969a.x.a(this.f2969a.A);
                this.f2969a.x.setOnClickListener(new b(this));
                return;
            case 10003:
                XLog.d("miles", "get KingRoot [com.kingroot.RushRoot] from server success...");
                this.f2969a.x.setVisibility(0);
                this.f2969a.y.setVisibility(8);
                this.f2969a.b(false);
                this.f2969a.x.a(this.f2969a.A);
                StatInfo buildDownloadSTInfo = STInfoBuilder.buildDownloadSTInfo(this.f2969a.n, this.f2969a.A);
                DownloadInfo a2 = DownloadProxy.a().a(this.f2969a.A);
                if (a2 != null && a2.needReCreateInfo(this.f2969a.A)) {
                    DownloadProxy.a().b(a2.downloadTicket);
                    a2 = null;
                }
                if (a2 == null) {
                    a2 = DownloadInfo.createDownloadInfo(this.f2969a.A, buildDownloadSTInfo, this.f2969a.x);
                    this.f2969a.x.a(this.f2969a.A);
                } else {
                    a2.updateDownloadInfoStatInfo(buildDownloadSTInfo);
                }
                AppConst.AppState d = k.d(this.f2969a.A);
                if (d == AppConst.AppState.DOWNLOADED) {
                    this.f2969a.x.a(this.f2969a.getResources().getString(R.string.appbutton_install));
                } else if (d == AppConst.AppState.DOWNLOAD || d == AppConst.AppState.UPDATE || d == AppConst.AppState.ILLEGAL) {
                    this.f2969a.x.a(this.f2969a.a(a2));
                    if (d == AppConst.AppState.ILLEGAL) {
                        this.f2969a.x.a(AppConst.AppState.DOWNLOAD);
                    }
                }
                this.f2969a.x.setOnClickListener(new c(this));
                return;
            case 10004:
                this.f2969a.x.setVisibility(8);
                this.f2969a.y.setVisibility(0);
                this.f2969a.b(false);
                this.f2969a.x.a(this.f2969a.getResources().getString(R.string.disconnected));
                XLog.e("miles", "get KingRoot [com.kingroot.RushRoot] from server fail...");
                return;
            default:
                return;
        }
    }
}
