package com.tencent.nucleus.manager.about;

import android.content.Context;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
public class FeedbackInputView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private Context f2731a;
    /* access modifiers changed from: private */
    public EditText b;
    private TextView c;
    private TextView d;
    private View.OnClickListener e = new k(this);

    public FeedbackInputView(Context context) {
        super(context);
        this.f2731a = context;
        d();
    }

    public FeedbackInputView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f2731a = context;
        d();
    }

    private void d() {
        View inflate = LayoutInflater.from(this.f2731a).inflate((int) R.layout.feedback_input, this);
        this.b = (EditText) inflate.findViewById(R.id.feedback_input);
        this.b.setOnClickListener(this.e);
        this.c = (TextView) inflate.findViewById(R.id.overflow_text);
        this.d = (TextView) inflate.findViewById(R.id.send_button);
    }

    public EditText a() {
        return this.b;
    }

    public void b() {
        this.d.setEnabled(false);
        this.c.setVisibility(0);
    }

    public void c() {
        this.d.setEnabled(true);
        this.c.setVisibility(8);
    }

    public void a(View.OnClickListener onClickListener) {
        if (this.d != null) {
            this.d.setOnClickListener(onClickListener);
        }
    }

    public void setOnKeyListener(View.OnKeyListener onKeyListener) {
        if (this.b != null) {
            this.b.setOnKeyListener(onKeyListener);
        }
    }

    public void a(TextWatcher textWatcher) {
        if (this.b != null) {
            this.b.addTextChangedListener(textWatcher);
        }
    }
}
