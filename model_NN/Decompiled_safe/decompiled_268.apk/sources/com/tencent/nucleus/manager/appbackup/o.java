package com.tencent.nucleus.manager.appbackup;

import android.content.SharedPreferences;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.nucleus.socialcontact.login.j;
import java.util.Map;
import java.util.Set;

/* compiled from: ProGuard */
public class o {
    public static boolean a() {
        if (!j.a().j()) {
            return false;
        }
        String k = k();
        SharedPreferences j = j();
        if (j == null || TextUtils.isEmpty(k)) {
            return false;
        }
        return j.getBoolean(k, false);
    }

    public static long b() {
        SharedPreferences j = j();
        String k = k();
        if (TextUtils.isEmpty(k)) {
            return 0;
        }
        String str = k + "last_switchdevice_push_time";
        if (j != null) {
            return j.getLong(str, 0);
        }
        return 0;
    }

    public static long c() {
        SharedPreferences j = j();
        if (j != null) {
            return j.getLong("last_refreshdevice_push_time", 0);
        }
        return 0;
    }

    public static long d() {
        SharedPreferences j = j();
        if (j != null) {
            return j.getLong("last_push_time", 0);
        }
        return 0;
    }

    public static void a(boolean z) {
        String k = k();
        SharedPreferences j = j();
        if (j != null && !TextUtils.isEmpty(k)) {
            j.edit().putBoolean(k, z).commit();
        }
    }

    public static void e() {
        boolean z = false;
        String k = k();
        SharedPreferences j = j();
        if (j != null && !TextUtils.isEmpty(k)) {
            if (!j.getBoolean(k, false)) {
                z = true;
            }
            j.edit().putBoolean(k, z).commit();
        }
    }

    private static SharedPreferences j() {
        try {
            return AstApp.i().getSharedPreferences("sp_backup", 0);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String k() {
        if (!j.a().j()) {
            return null;
        }
        if (j.a().k()) {
            return String.valueOf(j.a().p());
        }
        return j.a().s();
    }

    public static void f() {
        SharedPreferences j = j();
        String k = k();
        if (!TextUtils.isEmpty(k)) {
            String str = k + "backuped_flag";
            if (j != null) {
                j.edit().putBoolean(str, true).commit();
            }
            a(System.currentTimeMillis());
        }
    }

    public static boolean g() {
        SharedPreferences j = j();
        String k = k();
        if (TextUtils.isEmpty(k)) {
            return false;
        }
        String str = k + "backuped_flag";
        if (j != null) {
            return j.getBoolean(str, false);
        }
        return false;
    }

    public static boolean h() {
        Map<String, ?> all = j().getAll();
        if (all == null) {
            return false;
        }
        Set<String> keySet = all.keySet();
        if (keySet == null) {
            return false;
        }
        for (String next : keySet) {
            if (next != null && next.endsWith("backuped_flag")) {
                return true;
            }
        }
        return false;
    }

    public static void a(long j) {
        String k = k();
        if (!TextUtils.isEmpty(k)) {
            String str = k + "_time";
            SharedPreferences j2 = j();
            if (j2 != null) {
                j2.edit().putLong(str, j).commit();
            }
        }
    }

    public static long i() {
        String k = k();
        if (TextUtils.isEmpty(k)) {
            return 0;
        }
        String str = k + "_time";
        SharedPreferences j = j();
        if (j != null) {
            return j.getLong(str, 0);
        }
        return 0;
    }
}
