package com.tencent.nucleus.manager.about;

import com.tencent.assistant.Global;
import com.tencent.assistant.protocol.jce.PostFeedbackRequest;

/* compiled from: ProGuard */
class ag implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f2738a;
    final /* synthetic */ af b;

    ag(af afVar, String str) {
        this.b = afVar;
        this.f2738a = str;
    }

    public void run() {
        synchronized (this.b.b) {
            if (this.b.f2737a > 0) {
                boolean unused = this.b.cancel(this.b.f2737a);
            }
            PostFeedbackRequest postFeedbackRequest = new PostFeedbackRequest();
            postFeedbackRequest.f1438a = this.f2738a;
            postFeedbackRequest.b = Global.getNotNullTerminal();
            int unused2 = this.b.f2737a = this.b.send(postFeedbackRequest);
        }
    }
}
