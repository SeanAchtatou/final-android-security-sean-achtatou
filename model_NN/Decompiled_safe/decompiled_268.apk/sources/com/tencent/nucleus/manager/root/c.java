package com.tencent.nucleus.manager.root;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.module.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class c extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f2971a;

    c(a aVar) {
        this.f2971a = aVar;
    }

    public void onTMAClick(View view) {
        this.f2971a.f2969a.v();
    }

    public STInfoV2 getStInfo() {
        return STInfoBuilder.buildSTInfo(this.f2971a.f2969a, this.f2971a.f2969a.A, "03_001", a.a(k.d(this.f2971a.f2969a.A)), a.a(k.d(this.f2971a.f2969a.A), this.f2971a.f2969a.A));
    }
}
