package com.tencent.nucleus.manager.setting;

import android.view.View;
import com.tencent.assistant.adapter.af;
import com.tencent.assistantv2.model.ItemElement;

/* compiled from: ProGuard */
class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ItemElement f2999a;
    final /* synthetic */ View b;
    final /* synthetic */ int c;
    final /* synthetic */ af d;
    final /* synthetic */ SettingActivity e;

    e(SettingActivity settingActivity, ItemElement itemElement, View view, int i, af afVar) {
        this.e = settingActivity;
        this.f2999a = itemElement;
        this.b = view;
        this.c = i;
        this.d = afVar;
    }

    public void run() {
        this.e.w.a(this.f2999a.d, this.b, this.c, this.d.g.b());
    }
}
