package com.tencent.nucleus.manager.component;

import android.view.animation.Animation;
import android.view.animation.Transformation;

/* compiled from: ProGuard */
public class p extends Animation {

    /* renamed from: a  reason: collision with root package name */
    private q f2872a;
    private float b;
    private float c;

    public p(float f, float f2) {
        this.b = f;
        this.c = f2;
    }

    public void a(q qVar) {
        this.f2872a = qVar;
    }

    /* access modifiers changed from: protected */
    public void applyTransformation(float f, Transformation transformation) {
        if (this.f2872a != null) {
            this.f2872a.a(f, transformation, this.b, this.c);
        }
        super.applyTransformation(f, transformation);
    }
}
