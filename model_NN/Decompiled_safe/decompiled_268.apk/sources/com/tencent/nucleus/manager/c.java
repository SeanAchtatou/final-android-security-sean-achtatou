package com.tencent.nucleus.manager;

import android.os.IBinder;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.at;
import com.tencent.tmsecurelite.commom.DataEntity;
import com.tencent.tmsecurelite.optimize.f;

/* compiled from: ProGuard */
class c extends f {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DockRubbishRelateService f2838a;

    c(DockRubbishRelateService dockRubbishRelateService) {
        this.f2838a = dockRubbishRelateService;
    }

    public IBinder asBinder() {
        return null;
    }

    public void c() {
        XLog.d("DockRubbish", "DockRubbishRelateService onScanStarted.");
    }

    public void a(int i) {
    }

    public void b() {
        XLog.d("DockRubbish", "DockRubbishRelateService onScanFinished.");
        XLog.d("DockRubbish", "垃圾总大小：" + at.c(this.f2838a.c));
        XLog.d("DockRubbish", "软件缓存：" + at.c(this.f2838a.d));
        XLog.d("DockRubbish", "多余安装包：" + at.c(this.f2838a.e));
        XLog.d("DockRubbish", "卸载残留：" + at.c(this.f2838a.f));
        XLog.d("DockRubbish", "垃圾文件：" + at.c(this.f2838a.g));
        this.f2838a.b();
        StringBuilder sb = new StringBuilder();
        sb.append("[status]\nresultcode=").append(0).append("\n").append("[rubbishsize]\ntotalsize=").append(this.f2838a.c).append("\n").append("1=").append(this.f2838a.d).append("\n").append("2=").append(this.f2838a.e).append("\n").append("3=").append(this.f2838a.f).append("\n").append("4=").append(this.f2838a.g);
        this.f2838a.a(sb.toString());
        this.f2838a.a(0, true);
        this.f2838a.stopSelf();
    }

    public void a() {
        XLog.d("DockRubbish", "DockRubbishRelateService onScanCanceled");
    }

    public void a(int i, DataEntity dataEntity) {
        try {
            if (dataEntity.getBoolean("rubbish.suggest")) {
                long j = dataEntity.getLong("rubbish.size");
                switch (i) {
                    case 1:
                        DockRubbishRelateService.f(this.f2838a, j);
                        break;
                    case 2:
                        DockRubbishRelateService.g(this.f2838a, j);
                        break;
                    case 3:
                        DockRubbishRelateService.h(this.f2838a, j);
                        break;
                    case 4:
                        DockRubbishRelateService.i(this.f2838a, j);
                        break;
                }
                DockRubbishRelateService.j(this.f2838a, j);
            }
        } catch (Exception e) {
            XLog.d("DockRubbish", "DockRubbishRelateService. onRubbishFound Exception.");
            e.printStackTrace();
        }
    }
}
