package com.tencent.nucleus.manager.appbackup;

import android.view.View;
import android.widget.TextView;

/* compiled from: ProGuard */
class j implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BackupApplistDialog f2809a;

    j(BackupApplistDialog backupApplistDialog) {
        this.f2809a = backupApplistDialog;
    }

    public void onClick(View view) {
        boolean z = true;
        boolean unused = this.f2809a.j = true;
        TextView c = this.f2809a.f;
        if (this.f2809a.f.isSelected()) {
            z = false;
        }
        c.setSelected(z);
        this.f2809a.d.a(this.f2809a.f.isSelected());
        this.f2809a.b();
    }
}
