package com.tencent.nucleus.manager.floatingwindow;

import android.view.animation.Animation;

/* compiled from: ProGuard */
class z implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RocketLauncher f2922a;

    z(RocketLauncher rocketLauncher) {
        this.f2922a = rocketLauncher;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        this.f2922a.i.setVisibility(8);
        this.f2922a.h.clearAnimation();
        this.f2922a.j.clearAnimation();
        this.f2922a.h.startAnimation(this.f2922a.b());
        this.f2922a.a(this.f2922a.m, 0.0f, 0.0f, (float) this.f2922a.e, (float) (-this.f2922a.y), 500, 400, false, this.f2922a.A);
        this.f2922a.j.startAnimation(this.f2922a.c());
        this.f2922a.a(this.f2922a.k, 0.0f, 0.7f, 500);
    }
}
