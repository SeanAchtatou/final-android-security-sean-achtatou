package com.tencent.nucleus.manager.component;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.Global;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.component.ProgressResultView;
import com.tencent.assistantv2.component.at;
import com.tencent.beacon.event.a;
import com.tencent.smtt.sdk.WebView;
import java.util.HashMap;

/* compiled from: ProGuard */
public class ScaningProgressView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    public Context f2843a;
    public LayoutInflater b;
    public View c;
    public Handler d;
    /* access modifiers changed from: private */
    public RollTextView e;
    private TextView f;
    private TextView g;
    /* access modifiers changed from: private */
    public ProgressBar h;
    /* access modifiers changed from: private */
    public ProgressResultView i;
    private boolean j;
    private float k;

    public ScaningProgressView(Context context) {
        this(context, null);
    }

    public ScaningProgressView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = true;
        this.k = 1.0f;
        this.f2843a = context;
        this.b = (LayoutInflater) this.f2843a.getSystemService("layout_inflater");
        this.d = new Handler();
        a();
        setOnClickListener(new u(this));
    }

    /* access modifiers changed from: private */
    public void e() {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", Global.getQUAForBeacon());
        hashMap.put("B3", t.g());
        a.a("ScaningProgressViewClick", true, -1, -1, hashMap, false);
        XLog.d("beacon", "beacon report >> event: ScaningProgressViewClick, totalTime : 0, params : " + hashMap.toString());
    }

    public void a() {
        try {
            this.c = this.b.inflate((int) R.layout.scan_progress_layout, this);
        } catch (Throwable th) {
            com.tencent.assistant.manager.t.a().b();
            this.c = this.b.inflate((int) R.layout.scan_progress_layout, this);
        }
        this.h = (ProgressBar) this.c.findViewById(R.id.pb_scanning);
        this.i = (ProgressResultView) this.c.findViewById(R.id.scanning_result);
        this.e = (RollTextView) this.c.findViewById(R.id.header_size);
        this.f = (TextView) this.c.findViewById(R.id.header_size_unit);
        this.g = (TextView) this.c.findViewById(R.id.header_size_tip);
        this.e.a(this.f);
        d();
        if (t.v().startsWith("Xiaomi")) {
            this.e.setTextSize(2, 28.0f);
        }
    }

    public void b() {
        this.h.setVisibility(0);
        this.i.setVisibility(4);
    }

    public void c() {
        this.h.setVisibility(4);
        this.i.setVisibility(0);
    }

    public void a(float f2, at atVar) {
        this.i.setVisibility(0);
        this.h.setVisibility(4);
        this.i.a(f2, atVar);
        this.e.setTextColor((int) WebView.NIGHT_MODE_COLOR);
    }

    public void a(long j2) {
        this.d.post(new v(this, j2));
    }

    public void b(long j2) {
        a(j2, 2);
    }

    public void a(long j2, int i2) {
        this.d.post(new w(this, i2, j2));
    }

    public void d() {
        this.e.a(0.0d);
    }
}
