package com.tencent.nucleus.manager.spaceclean;

import android.app.ActivityManager;
import android.content.pm.PackageManager;
import android.os.Message;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.utils.XLog;
import java.util.List;

/* compiled from: ProGuard */
class j implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BigFileCleanActivity f3033a;

    j(BigFileCleanActivity bigFileCleanActivity) {
        this.f3033a = bigFileCleanActivity;
    }

    public void run() {
        boolean z = false;
        PackageManager packageManager = this.f3033a.getPackageManager();
        XLog.d("miles", "loadData---run");
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) this.f3033a.getSystemService("activity")).getRunningAppProcesses();
        if (runningAppProcesses != null) {
            int i = 0;
            while (true) {
                if (i >= runningAppProcesses.size()) {
                    break;
                }
                String str = runningAppProcesses.get(i).pkgList[0];
                if (!AppConst.TENCENT_MOBILE_MANAGER_PKGNAME.equals(str) && !this.f3033a.n.getPackageName().equals(str) && this.f3033a.a(packageManager, str)) {
                    z = true;
                    break;
                }
                i++;
            }
        }
        boolean unused = this.f3033a.G = z;
        boolean unused2 = this.f3033a.H = true;
        Message.obtain(this.f3033a.P, 29).sendToTarget();
    }
}
