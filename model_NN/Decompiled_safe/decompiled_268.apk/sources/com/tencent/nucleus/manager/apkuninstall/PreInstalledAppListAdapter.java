package com.tencent.nucleus.manager.apkuninstall;

import android.content.Context;
import android.os.Handler;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.LocalPkgSizeTextView;
import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.bm;
import com.tencent.assistant.utils.bo;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.component.MovingProgressBar;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class PreInstalledAppListAdapter extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private Context f2781a;
    private List<LocalApkInfo> b = new ArrayList();
    private LayoutInflater c;
    private IViewInvalidater d;
    private boolean e = true;
    private int f = 0;
    private Handler g;
    private boolean h;
    /* access modifiers changed from: private */
    public boolean i = true;

    public PreInstalledAppListAdapter(Context context) {
        this.f2781a = context;
        this.c = LayoutInflater.from(context);
    }

    public void a(List<LocalApkInfo> list, int i2) {
        this.b.clear();
        this.b.addAll(list);
        this.f = i2;
        notifyDataSetChanged();
    }

    public int getCount() {
        if (this.b != null) {
            return this.b.size();
        }
        return 0;
    }

    public Object getItem(int i2) {
        if (this.b != null) {
            return this.b.get(i2);
        }
        return null;
    }

    public long getItemId(int i2) {
        return 0;
    }

    public View getView(int i2, View view, ViewGroup viewGroup) {
        i iVar;
        if (view == null || view.getTag() == null) {
            view = this.c.inflate((int) R.layout.installed_app_list_item, (ViewGroup) null);
            iVar = new i(null);
            iVar.f2791a = view.findViewById(R.id.container_layout);
            iVar.c = (TextView) view.findViewById(R.id.soft_name_txt);
            iVar.b = (TXImageView) view.findViewById(R.id.soft_icon_img);
            iVar.d = (LocalPkgSizeTextView) view.findViewById(R.id.soft_size_txt);
            iVar.e = (TextView) view.findViewById(R.id.tv_check);
            iVar.f = (TextView) view.findViewById(R.id.last_used_time_txt);
            iVar.g = (TextView) view.findViewById(R.id.popbar_title);
            iVar.h = (TextView) view.findViewById(R.id.tv_risk_tips);
            iVar.k = view.findViewById(R.id.rl_appdetail);
            iVar.i = view.findViewById(R.id.uninstall_progress);
            iVar.j = (MovingProgressBar) view.findViewById(R.id.app_uninstall_progress_bar);
            iVar.j.a(AstApp.i().getResources().getDimensionPixelSize(R.dimen.app_detail_pic_gap));
            iVar.j.b(AstApp.i().getResources().getDimensionPixelSize(R.dimen.install_progress_bar_width));
            iVar.l = view.findViewById(R.id.divide_line);
            iVar.m = view.findViewById(R.id.top_margin);
            iVar.n = view.findViewById(R.id.bottom_margin);
            iVar.o = view.findViewById(R.id.last_line);
            view.setTag(iVar);
        } else {
            iVar = (i) view.getTag();
        }
        a(this.b.get(i2), iVar, i2, a(this.f, i2));
        return view;
    }

    private void a(LocalApkInfo localApkInfo, i iVar, int i2, STInfoV2 sTInfoV2) {
        iVar.c.setText(localApkInfo.mAppName);
        iVar.b.setInvalidater(this.d);
        iVar.b.updateImageView(localApkInfo.mPackageName, R.drawable.pic_defaule, TXImageView.TXImageViewType.INSTALL_APK_ICON);
        if (this.h || !a()) {
            iVar.e.setEnabled(false);
            iVar.f2791a.setEnabled(false);
            if (localApkInfo.mIsSelect) {
                iVar.i.setVisibility(0);
                iVar.k.setVisibility(8);
                iVar.j.a();
            } else {
                iVar.i.setVisibility(8);
                iVar.k.setVisibility(0);
                iVar.j.b();
            }
        } else {
            iVar.e.setEnabled(true);
            iVar.f2791a.setEnabled(true);
            iVar.i.setVisibility(8);
            iVar.k.setVisibility(0);
            iVar.j.b();
        }
        iVar.e.setSelected(localApkInfo.mIsSelect);
        if (this.f == 1) {
            iVar.d.setTextColor(this.f2781a.getResources().getColor(R.color.appadmin_highlight_text));
        } else {
            iVar.d.setTextColor(this.f2781a.getResources().getColor(R.color.appadmin_card_size_text));
        }
        iVar.d.updateTextView(localApkInfo.mPackageName, localApkInfo.occupySize);
        iVar.d.setInvalidater(this.d);
        if (i2 == 0) {
            iVar.g.setText(this.f2781a.getString(R.string.popbar_title_installed_count, Integer.valueOf(getCount())));
            iVar.g.setVisibility(0);
        } else {
            iVar.g.setVisibility(8);
        }
        if (this.f == 3) {
            a(iVar.f, localApkInfo.mInstallDate);
        } else {
            a(iVar.f, localApkInfo.mLastLaunchTime, localApkInfo.mFakeLastLaunchTime);
        }
        if (this.e) {
            ((RelativeLayout.LayoutParams) iVar.e.getLayoutParams()).rightMargin = by.a(this.f2781a, 16.0f);
            ((LinearLayout.LayoutParams) iVar.o.getLayoutParams()).rightMargin = by.a(this.f2781a, 16.0f);
        } else {
            ((RelativeLayout.LayoutParams) iVar.e.getLayoutParams()).rightMargin = by.a(this.f2781a, 46.0f);
            ((LinearLayout.LayoutParams) iVar.o.getLayoutParams()).rightMargin = by.a(this.f2781a, 30.0f);
        }
        iVar.e.setOnClickListener(new f(this, iVar, localApkInfo, sTInfoV2));
        iVar.f2791a.setOnClickListener(new g(this, iVar, localApkInfo, sTInfoV2));
        a(iVar.f2791a, iVar.h, iVar.l, iVar.o, iVar.m, iVar.n, i2, this.b.size());
    }

    /* access modifiers changed from: private */
    public void a(i iVar, LocalApkInfo localApkInfo) {
        h hVar = new h(this, iVar, localApkInfo);
        hVar.blockCaller = true;
        hVar.titleRes = this.f2781a.getString(R.string.app_admin_uninstall_dialog_tips_title);
        hVar.lBtnTxtRes = this.f2781a.getString(R.string.cancel);
        hVar.rBtnTxtRes = this.f2781a.getString(R.string.app_admin_uninstall_dialog_tips_confirm);
        hVar.contentRes = this.f2781a.getString(R.string.app_admin_uninstall_dialog_tips_content);
        DialogUtils.show2BtnDialog(hVar);
    }

    /* access modifiers changed from: private */
    public void b(i iVar, LocalApkInfo localApkInfo) {
        iVar.e.setSelected(!iVar.e.isSelected());
        localApkInfo.mIsSelect = iVar.e.isSelected();
        if (this.g != null) {
            this.g.sendMessage(this.g.obtainMessage(10705, localApkInfo));
        }
    }

    private STInfoV2 a(int i2, int i3) {
        String b2 = b(i2, i3);
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f2781a, 100);
        buildSTInfo.slotId = b2;
        if (a()) {
            buildSTInfo.scene = STConst.ST_PAGE_PRE_APP_UNINSTALL_ROOT;
            b.getInstance().exposure(buildSTInfo);
        } else {
            buildSTInfo.scene = STConst.ST_PAGE_PRE_APP_UNINSTALL_NO_ROOT;
        }
        return buildSTInfo;
    }

    private String b(int i2, int i3) {
        if (i2 == 0) {
            return "03_" + bm.a(i3 + 1);
        }
        if (i2 == 1) {
            return "04_" + bm.a(i3 + 1);
        }
        if (i2 == 2) {
            return "05_" + bm.a(i3 + 1);
        }
        return "06_" + bm.a(i3 + 1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    private void a(TextView textView, long j, long j2) {
        int f2;
        long a2 = m.a().a("key_first_load_installed_app_time", 0L);
        boolean z = a2 != 0 && (System.currentTimeMillis() - a2) - 259200000 >= 0;
        if (j > 0 || z) {
            if (j > 0) {
                f2 = bo.f(j);
            } else {
                f2 = bo.f(j2);
            }
            if (f2 == 0) {
                textView.setVisibility(0);
                textView.setText(this.f2781a.getResources().getString(R.string.today));
                return;
            }
            String format = String.format(this.f2781a.getResources().getString(R.string.last_used_time), Integer.valueOf(f2));
            if (this.f == 0) {
                SpannableString spannableString = new SpannableString(format);
                spannableString.setSpan(new ForegroundColorSpan(this.f2781a.getResources().getColor(R.color.appadmin_highlight_text)), 0, format.indexOf("天") + 1, 33);
                textView.setText(spannableString);
            } else {
                textView.setText(format);
            }
            textView.setVisibility(0);
            return;
        }
        textView.setVisibility(8);
    }

    private void a(TextView textView, long j) {
        if (j <= 0) {
            textView.setVisibility(8);
            return;
        }
        int f2 = bo.f(j);
        if (f2 == 0) {
            textView.setVisibility(0);
            textView.setText(this.f2781a.getResources().getString(R.string.today_install));
            return;
        }
        textView.setText(String.format(this.f2781a.getResources().getString(R.string.install_time), Integer.valueOf(f2)));
        textView.setVisibility(0);
    }

    public void a(View view, TextView textView, View view2, View view3, View view4, View view5, int i2, int i3) {
        if (i3 == 1) {
            view3.setVisibility(8);
            view5.setVisibility(0);
            view4.setVisibility(0);
            if (a()) {
                textView.setVisibility(0);
                view2.setVisibility(0);
            } else {
                textView.setVisibility(8);
                view2.setVisibility(8);
            }
        } else if (i2 == 0) {
            view3.setVisibility(0);
            view5.setVisibility(8);
            if (a()) {
                view4.setVisibility(8);
                textView.setVisibility(0);
                view2.setVisibility(0);
            } else {
                view4.setVisibility(0);
                textView.setVisibility(8);
                view2.setVisibility(8);
            }
        } else if (i2 == i3 - 1) {
            view3.setVisibility(8);
            view5.setVisibility(0);
            view4.setVisibility(8);
            textView.setVisibility(8);
            view2.setVisibility(8);
        } else {
            view3.setVisibility(0);
            view5.setVisibility(8);
            view4.setVisibility(8);
            textView.setVisibility(8);
            view2.setVisibility(8);
        }
        view.setBackgroundResource(R.drawable.helper_cardbg_all_selector_new);
    }

    public int a(int i2) {
        for (int i3 = 0; i3 < getCount(); i3++) {
            String str = this.b.get(i3).mSortKey;
            if (!TextUtils.isEmpty(str)) {
                char charAt = str.toUpperCase().charAt(0);
                if (charAt == i2) {
                    return i3;
                }
                if (i2 == 35 && charAt >= '0' && charAt <= '9') {
                    return i3;
                }
            }
        }
        return -1;
    }

    private boolean a() {
        return PreInstallAppListView.f2780a;
    }

    public void a(IViewInvalidater iViewInvalidater) {
        this.d = iViewInvalidater;
    }

    public void a(boolean z) {
        this.e = z;
    }

    public void a(Handler handler) {
        this.g = handler;
    }

    public void a(boolean z, boolean z2) {
        this.h = z;
        if (z2) {
            notifyDataSetChanged();
        }
    }
}
