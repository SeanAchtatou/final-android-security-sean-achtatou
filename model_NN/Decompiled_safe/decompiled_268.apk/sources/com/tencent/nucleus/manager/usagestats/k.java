package com.tencent.nucleus.manager.usagestats;

import java.util.Collection;
import java.util.Iterator;

/* compiled from: ProGuard */
final class k implements Collection<V> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ f f3069a;

    k(f fVar) {
        this.f3069a = fVar;
    }

    public boolean add(V v) {
        throw new UnsupportedOperationException();
    }

    public boolean addAll(Collection<? extends V> collection) {
        throw new UnsupportedOperationException();
    }

    public void clear() {
        this.f3069a.c();
    }

    public boolean contains(Object obj) {
        return this.f3069a.b(obj) >= 0;
    }

    public boolean containsAll(Collection<?> collection) {
        for (Object contains : collection) {
            if (!contains(contains)) {
                return false;
            }
        }
        return true;
    }

    public boolean isEmpty() {
        return this.f3069a.a() == 0;
    }

    public Iterator<V> iterator() {
        return new g(this.f3069a, 1);
    }

    public boolean remove(Object obj) {
        int b = this.f3069a.b(obj);
        if (b < 0) {
            return false;
        }
        this.f3069a.a(b);
        return true;
    }

    public boolean removeAll(Collection<?> collection) {
        int i = 0;
        int a2 = this.f3069a.a();
        boolean z = false;
        while (i < a2) {
            if (collection.contains(this.f3069a.a(i, 1))) {
                this.f3069a.a(i);
                i--;
                a2--;
                z = true;
            }
            i++;
        }
        return z;
    }

    public boolean retainAll(Collection<?> collection) {
        int i = 0;
        int a2 = this.f3069a.a();
        boolean z = false;
        while (i < a2) {
            if (!collection.contains(this.f3069a.a(i, 1))) {
                this.f3069a.a(i);
                i--;
                a2--;
                z = true;
            }
            i++;
        }
        return z;
    }

    public int size() {
        return this.f3069a.a();
    }

    public Object[] toArray() {
        return this.f3069a.b(1);
    }

    public <T> T[] toArray(T[] tArr) {
        return this.f3069a.a(tArr, 1);
    }
}
