package com.tencent.nucleus.manager.floatingwindow;

import android.graphics.drawable.AnimationDrawable;
import android.view.animation.Animation;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class aa implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RocketLauncher f2896a;

    aa(RocketLauncher rocketLauncher) {
        this.f2896a = rocketLauncher;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.floatingwindow.RocketLauncher.a(com.tencent.nucleus.manager.floatingwindow.RocketLauncher, boolean):boolean
     arg types: [com.tencent.nucleus.manager.floatingwindow.RocketLauncher, int]
     candidates:
      com.tencent.nucleus.manager.floatingwindow.RocketLauncher.a(com.tencent.nucleus.manager.floatingwindow.RocketLauncher, android.graphics.drawable.AnimationDrawable):android.graphics.drawable.AnimationDrawable
      com.tencent.nucleus.manager.floatingwindow.RocketLauncher.a(int, int):void
      com.tencent.nucleus.manager.floatingwindow.RocketLauncher.a(boolean, boolean):void
      com.tencent.nucleus.manager.floatingwindow.RocketLauncher.a(com.tencent.nucleus.manager.floatingwindow.RocketLauncher, boolean):boolean */
    public void onAnimationEnd(Animation animation) {
        this.f2896a.m.setVisibility(0);
        try {
            this.f2896a.m.setBackgroundResource(R.drawable.rocket_filcker_anim);
            AnimationDrawable unused = this.f2896a.n = (AnimationDrawable) this.f2896a.m.getBackground();
            this.f2896a.n.start();
        } catch (Throwable th) {
            XLog.e("floatingwindow", "RocketLauncher >> <mRocketPrepareAnimListener.onAnimationEnd> throws exception");
        }
        boolean unused2 = this.f2896a.v = false;
        if (this.f2896a.w) {
            this.f2896a.a(this.f2896a.x);
            boolean unused3 = this.f2896a.w = false;
        }
    }
}
