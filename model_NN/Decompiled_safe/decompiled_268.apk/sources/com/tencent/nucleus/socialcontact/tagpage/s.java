package com.tencent.nucleus.socialcontact.tagpage;

import java.io.File;
import java.util.Comparator;

/* compiled from: ProGuard */
class s implements Comparator<File> {
    private s() {
    }

    /* synthetic */ s(m mVar) {
        this();
    }

    /* renamed from: a */
    public int compare(File file, File file2) {
        if (file.lastModified() <= file2.lastModified()) {
            return -1;
        }
        return 1;
    }
}
