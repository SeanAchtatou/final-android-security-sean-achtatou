package com.tencent.nucleus.socialcontact.comment;

import android.view.View;
import android.widget.ImageView;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class s implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CommentHeaderTagView f3135a;

    s(CommentHeaderTagView commentHeaderTagView) {
        this.f3135a = commentHeaderTagView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.socialcontact.comment.CommentHeaderTagView.a(com.tencent.nucleus.socialcontact.comment.CommentHeaderTagView, boolean):boolean
     arg types: [com.tencent.nucleus.socialcontact.comment.CommentHeaderTagView, int]
     candidates:
      com.tencent.nucleus.socialcontact.comment.CommentHeaderTagView.a(com.tencent.assistant.protocol.jce.CommentTagInfo, java.util.List<com.tencent.assistant.protocol.jce.CommentTagInfo>):void
      com.tencent.nucleus.socialcontact.comment.CommentHeaderTagView.a(com.tencent.nucleus.socialcontact.comment.CommentHeaderTagView, boolean):boolean */
    public void onClick(View view) {
        ImageView imageView = (ImageView) view;
        if (this.f3135a.h) {
            boolean unused = this.f3135a.h = false;
            imageView.setImageResource(R.drawable.icon_open);
            this.f3135a.e.a(false);
            this.f3135a.e.requestLayout();
            return;
        }
        boolean unused2 = this.f3135a.h = true;
        imageView.setImageResource(R.drawable.icon_close);
        this.f3135a.e.a(true);
        this.f3135a.e.requestLayout();
    }
}
