package com.tencent.nucleus.socialcontact.comment;

import android.widget.RatingBar;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class bf implements RatingBar.OnRatingBarChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PopViewDialog f3112a;

    bf(PopViewDialog popViewDialog) {
        this.f3112a = popViewDialog;
    }

    public void onRatingChanged(RatingBar ratingBar, float f, boolean z) {
        this.f3112a.f.setTextColor(this.f3112a.b.getResources().getColor(R.color.appadmin_risk_tips));
        if (f > 4.0f) {
            this.f3112a.f.setText((int) R.string.comment_five_star);
        } else if (f > 3.0f) {
            this.f3112a.f.setText((int) R.string.comment_four_star);
        } else if (f > 2.0f) {
            this.f3112a.f.setText((int) R.string.comment_three_star);
        } else if (f > 1.0f) {
            this.f3112a.f.setText((int) R.string.comment_two_star);
        } else if (f > 0.0f) {
            this.f3112a.f.setText((int) R.string.comment_one_star);
        } else {
            this.f3112a.f.setText((int) R.string.comment_pop_star_text);
        }
        STInfoV2 c = this.f3112a.i();
        if (c != null) {
            c.slotId = a.a(this.f3112a.h(), "001");
            c.actionId = 200;
            l.a(c);
        }
    }
}
