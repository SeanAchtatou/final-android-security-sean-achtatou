package com.tencent.nucleus.socialcontact.comment;

import com.tencent.assistant.protocol.jce.ReplyDetail;
import java.util.ArrayList;

/* compiled from: ProGuard */
class av implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ar f3103a;
    private int b = 0;
    private byte[] c;
    /* access modifiers changed from: private */
    public boolean d = true;
    private ArrayList<ReplyDetail> e;
    private byte[] f;
    private int g = -1;
    /* access modifiers changed from: private */
    public long h;

    public av(ar arVar) {
        this.f3103a = arVar;
    }

    public void a(byte[] bArr) {
        if (bArr != null && bArr.length != 0) {
            b();
            this.b = 1;
            this.c = bArr;
            this.g = this.f3103a.a(this.c);
        }
    }

    public int a() {
        return this.g;
    }

    public void b() {
        this.g = -1;
        this.c = null;
        this.d = true;
        this.f = null;
        this.e = null;
        this.b = 0;
        this.h = 0;
    }

    public boolean c() {
        return this.b == 1;
    }

    public byte[] d() {
        return this.c;
    }

    public ArrayList<ReplyDetail> e() {
        return this.e;
    }

    public boolean f() {
        return this.d;
    }

    public byte[] g() {
        return this.f;
    }

    public void a(ArrayList<ReplyDetail> arrayList, boolean z, byte[] bArr, long j) {
        this.e = arrayList;
        this.d = z;
        this.f = bArr;
        this.h = j;
        this.b = 2;
    }

    public void h() {
        this.b = 2;
    }

    /* renamed from: i */
    public av clone() {
        try {
            av avVar = (av) super.clone();
            if (this.e == null) {
                return avVar;
            }
            avVar.e = (ArrayList) this.e.clone();
            return avVar;
        } catch (CloneNotSupportedException e2) {
            return this;
        }
    }
}
