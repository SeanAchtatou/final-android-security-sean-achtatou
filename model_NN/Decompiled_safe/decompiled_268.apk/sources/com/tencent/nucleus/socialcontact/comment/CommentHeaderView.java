package com.tencent.nucleus.socialcontact.comment;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.RatingView;

/* compiled from: ProGuard */
public class CommentHeaderView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private Context f3077a;
    private TextView b;
    private RatingView c;
    private TextView d;
    private CommentProgressBar e;
    private LayoutInflater f;

    private void a() {
        this.f = LayoutInflater.from(this.f3077a);
        View inflate = this.f.inflate((int) R.layout.comment_detail_header_layout, this);
        this.b = (TextView) inflate.findViewById(R.id.comment_average_numbers);
        this.c = (RatingView) inflate.findViewById(R.id.comment_score);
        this.d = (TextView) inflate.findViewById(R.id.comment_average_text_totol_custom);
        this.e = (CommentProgressBar) inflate.findViewById(R.id.commentBar);
    }

    public CommentHeaderView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3077a = context;
        a();
    }

    public CommentHeaderView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f3077a = context;
        a();
    }

    public CommentHeaderView(Context context) {
        super(context);
        this.f3077a = context;
        a();
    }
}
