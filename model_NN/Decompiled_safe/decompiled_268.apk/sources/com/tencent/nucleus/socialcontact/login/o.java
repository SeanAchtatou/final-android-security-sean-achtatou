package com.tencent.nucleus.socialcontact.login;

import android.view.View;
import com.tencent.assistant.plugin.PluginHelper;

/* compiled from: ProGuard */
class o implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PluginLoadingDialog f3166a;

    o(PluginLoadingDialog pluginLoadingDialog) {
        this.f3166a = pluginLoadingDialog;
    }

    public void onClick(View view) {
        int requireInstall = PluginHelper.requireInstall(h.b);
        if (requireInstall != 1) {
            if (requireInstall == -1) {
                this.f3166a.b();
            } else if (requireInstall == 0) {
                this.f3166a.b();
            }
        }
    }
}
