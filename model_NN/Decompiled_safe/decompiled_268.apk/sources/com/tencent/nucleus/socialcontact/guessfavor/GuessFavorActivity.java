package com.tencent.nucleus.socialcontact.guessfavor;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.DownloadProgressButton;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorPage;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.net.c;
import com.tencent.assistant.protocol.jce.RecommendAppInfoEx;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.page.STPageInfo;
import com.tencent.nucleus.socialcontact.login.j;
import java.util.List;

/* compiled from: ProGuard */
public class GuessFavorActivity extends BaseActivity implements UIEventListener, i {
    /* access modifiers changed from: private */
    public LoadingView A = null;
    private TextView B;
    private LinearLayout C;
    private Button D;
    private String E = DownloadProgressButton.TMA_ST_NAVBAR_DOWNLOAD_TAG;
    private String F = "05_001";
    private final String G = "Jie";
    private boolean H = false;
    private ApkResCallback.Stub I = new a(this);
    private OnTMAParamExClickListener J = new d(this);
    /* access modifiers changed from: private */
    public Context n;
    private SecondNavigationTitleViewV5 u;
    private ListView v;
    /* access modifiers changed from: private */
    public GuessFavorAdapter w;
    /* access modifiers changed from: private */
    public j x;
    private ViewStub y = null;
    /* access modifiers changed from: private */
    public NormalErrorPage z = null;

    public int f() {
        return STConst.ST_PAGE_GUESS_FAVOR_LOGIN;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_guess_favor);
        this.n = this;
        z();
        t();
    }

    private void t() {
        if (this.x == null) {
            this.x = new j();
            this.x.register(this);
        }
        this.x.a();
        w();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.w.c();
        this.u.l();
        v();
        XLog.i("Jie", "adapter onResume");
        y();
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        ApkResourceManager.getInstance().registerApkResCallback(this.I);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.w != null) {
            this.w.b();
        }
        this.u.m();
        u();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.w != null) {
            this.w.d();
        }
        if (this.x != null) {
            this.x.b();
            this.x.unregister(this);
        }
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        ApkResourceManager.getInstance().unRegisterApkResCallback(this.I);
    }

    private void u() {
        if (this.A != null && this.A.getVisibility() == 0) {
            this.H = true;
            this.A.setVisibility(8);
        }
    }

    private void v() {
        if (this.A != null && this.H) {
            this.A.setVisibility(0);
        }
    }

    /* access modifiers changed from: private */
    public void w() {
        if (this.A != null) {
            this.A.setVisibility(0);
        }
        if (this.v != null) {
            this.v.setVisibility(8);
        }
        if (this.D != null) {
            this.D.setEnabled(false);
            this.D.setVisibility(8);
        }
    }

    private void x() {
        this.H = false;
        if (this.A != null) {
            this.A.setVisibility(8);
        }
        if (this.v != null) {
            this.v.setVisibility(0);
        }
        if (this.D != null) {
            this.D.setEnabled(true);
        }
    }

    private void y() {
        this.u.b(getResources().getString(R.string.p_my_fav));
        if (this.C == null) {
            return;
        }
        if (j.a().j()) {
            this.C.setVisibility(8);
        } else {
            this.C.setVisibility(0);
        }
    }

    private void z() {
        this.y = (ViewStub) findViewById(R.id.error_stub);
        this.u = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.u.a(this);
        this.u.i();
        this.A = (LoadingView) findViewById(R.id.loading);
        this.v = (ListView) findViewById(R.id.list_view);
        this.w = new GuessFavorAdapter(this.n, this.v);
        this.v.setDivider(null);
        this.v.setAdapter((ListAdapter) this.w);
        this.v.setOnItemClickListener(new c(this));
        if (this.A == null || this.A.getVisibility() != 0) {
            this.v.setVisibility(0);
        } else {
            this.v.setVisibility(8);
        }
        this.B = (TextView) findViewById(R.id.tv_login);
        this.B.setTag(R.id.tma_st_slot_tag, this.E);
        this.D = (Button) findViewById(R.id.btn_change);
        this.D.setTag(R.id.tma_st_slot_tag, this.F);
        this.C = (LinearLayout) findViewById(R.id.layout_login);
        this.B.setOnClickListener(this.J);
        this.D.setOnClickListener(this.J);
        this.D.setVisibility(8);
    }

    private void b(int i) {
        if (this.z == null) {
            this.y.inflate();
            this.z = (NormalErrorPage) findViewById(R.id.error);
            this.z.setButtonClickListener(new e(this));
        }
        this.z.setErrorType(i);
        if (i == 4) {
            this.z.setErrorText(getResources().getString(R.string.collection_empty_tip), null);
        }
        if (this.v != null) {
            this.v.setVisibility(8);
        }
        this.z.setVisibility(0);
        if (this.D != null) {
            this.D.setVisibility(8);
        }
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS:
                XLog.i("Jie", "UI_EVENT_LOGIN_SUCCESS");
                t();
                if (this.C != null) {
                    this.C.setVisibility(8);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_LOGIN_FAIL:
            case EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL:
            default:
                return;
        }
    }

    public void a(int i, int i2, List<RecommendAppInfoEx> list, List<SimpleAppModel> list2) {
        x();
        if (this.v != null) {
            if (i2 == 0) {
                if (!(list == null || this.w == null)) {
                    this.w.a(list, list2);
                }
                if (list == null || list.isEmpty()) {
                    b(4);
                } else {
                    if (this.z != null) {
                        this.z.setVisibility(8);
                    }
                    this.v.setVisibility(0);
                    if (i == -1) {
                        Toast.makeText(this, getString(R.string.disconnected), 0).show();
                    }
                }
                this.D.setVisibility(0);
            } else if (i2 == -800) {
                b(3);
            } else {
                b(2);
            }
        }
    }

    public void a(int i, int i2) {
        x();
        if (!c.a()) {
            b(3);
        } else {
            b(2);
        }
    }

    public STPageInfo n() {
        this.p.f2060a = STConst.ST_PAGE_GUESS_FAVOR_LOGIN;
        return this.p;
    }
}
