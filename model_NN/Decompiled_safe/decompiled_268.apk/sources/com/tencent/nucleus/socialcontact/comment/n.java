package com.tencent.nucleus.socialcontact.comment;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.protocol.jce.GetCommentListResponse;
import java.util.List;

/* compiled from: ProGuard */
class n implements CallbackHelper.Caller<j> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f3130a;
    final /* synthetic */ boolean b;
    final /* synthetic */ GetCommentListResponse c;
    final /* synthetic */ List d;
    final /* synthetic */ k e;

    n(k kVar, int i, boolean z, GetCommentListResponse getCommentListResponse, List list) {
        this.e = kVar;
        this.f3130a = i;
        this.b = z;
        this.c = getCommentListResponse;
        this.d = list;
    }

    /* renamed from: a */
    public void call(j jVar) {
        jVar.a(this.f3130a, 0, this.b, this.e.h, this.c.e, this.c.h, this.d, this.e.e, this.e.f, this.c.i);
    }
}
