package com.tencent.nucleus.socialcontact.login;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.module.callback.i;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.protocol.jce.GetUserInfoRequest;
import com.tencent.assistant.protocol.jce.GetUserInfoResponse;

/* compiled from: ProGuard */
public class a extends BaseEngine<i> implements NetworkMonitor.ConnectivityChangeListener {
    private static a f;

    /* renamed from: a  reason: collision with root package name */
    i f3155a = new d(this);
    private int b = -1;
    private int c = -1;
    private int d;
    private Object e = new Object();

    public static synchronized a a() {
        a aVar;
        synchronized (a.class) {
            if (f == null) {
                f = new a();
            }
            aVar = f;
        }
        return aVar;
    }

    private a() {
        register(this.f3155a);
        t.a().a(this);
    }

    public int b() {
        d();
        int c2 = c();
        this.c = c2;
        return c2;
    }

    private int c() {
        GetUserInfoRequest getUserInfoRequest = new GetUserInfoRequest();
        if (this.b > 0) {
            cancel(this.b);
        }
        this.b = send(getUserInfoRequest);
        return this.b;
    }

    private void d() {
        synchronized (this.e) {
            this.d = 0;
        }
    }

    private void e() {
        synchronized (this.e) {
            this.d++;
        }
    }

    private boolean f() {
        boolean z;
        synchronized (this.e) {
            if (this.d < 2) {
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        d();
        notifyDataChangedInMainThread(new b(this, this.c, jceStruct, (GetUserInfoResponse) jceStruct2));
        this.b = -1;
        this.c = -1;
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        boolean z = true;
        if (l.b(i2) && f() && i2 != -801 && i2 != -800) {
            c();
            e();
            z = false;
        }
        if (z) {
            d();
            notifyDataChangedInMainThread(new c(this, this.c, i2, jceStruct, jceStruct2));
            this.b = -1;
            this.c = -1;
        }
    }

    public void onConnected(APN apn) {
        if (j.a().j() && !l.g()) {
            b();
        }
    }

    public void onDisconnected(APN apn) {
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
    }
}
