package com.tencent.nucleus.socialcontact.login;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
public class n extends e {
    private static n d;

    public static synchronized n b() {
        n nVar;
        synchronized (n.class) {
            if (d == null) {
                d = new n();
            }
            nVar = d;
        }
        return nVar;
    }

    private n() {
        super(AppConst.IdentityType.NONE);
    }

    /* access modifiers changed from: protected */
    public JceStruct a() {
        return null;
    }

    /* access modifiers changed from: protected */
    public byte[] getKey() {
        return "ji*9^&43U0X-~./(".getBytes();
    }
}
