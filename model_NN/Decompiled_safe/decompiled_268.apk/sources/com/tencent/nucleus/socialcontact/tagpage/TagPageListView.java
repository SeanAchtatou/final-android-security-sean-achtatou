package com.tencent.nucleus.socialcontact.tagpage;

import android.content.Context;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.AbsListView;
import com.tencent.assistant.component.txscrollview.TXGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;

/* compiled from: ProGuard */
public class TagPageListView extends TXGetMoreListView {
    private static final String u = TagPageListView.class.getSimpleName();
    private AbsListView.OnScrollListener v = null;

    public TagPageListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public TagPageListView(Context context) {
        super(context);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0 && motionEvent.getEdgeFlags() != 0) {
            return false;
        }
        switch (motionEvent.getAction()) {
            case 0:
                if (!p()) {
                    return false;
                }
                PointF pointF = this.r;
                PointF pointF2 = this.q;
                float x = motionEvent.getX();
                pointF2.x = x;
                pointF.x = x;
                PointF pointF3 = this.r;
                PointF pointF4 = this.q;
                float y = motionEvent.getY();
                pointF4.y = y;
                pointF3.y = y;
                return true;
            case 1:
            case 3:
                return j();
            case 2:
                if (!this.m) {
                    return false;
                }
                if (this.r.y < motionEvent.getY() && TXScrollViewBase.ScrollMode.PULL_FROM_END == this.o) {
                    return true;
                }
                this.r.x = motionEvent.getX();
                this.r.y = motionEvent.getY();
                if (this.o != TXScrollViewBase.ScrollMode.NOSCROLL) {
                    i();
                }
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public int i() {
        return super.i();
    }

    public void b(boolean z) {
        if (this.e != null) {
            this.e.setVisibility(z ? 0 : 8);
        }
    }

    public void setOnScrollListener(AbsListView.OnScrollListener onScrollListener) {
        this.v = onScrollListener;
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (this.v != null) {
            this.v.onScroll(absListView, i, i2, i3);
        }
        super.onScroll(absListView, i, i2, i3);
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        if (this.v != null) {
            this.v.onScrollStateChanged(absListView, i);
        }
        super.onScrollStateChanged(absListView, i);
    }

    public void g() {
        this.f691a = TXRefreshScrollViewBase.RefreshState.REFRESH_LOAD_FINISH;
        a(true);
    }

    /* access modifiers changed from: protected */
    public void a(boolean z) {
        super.a(z);
    }
}
