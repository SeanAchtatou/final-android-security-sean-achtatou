package com.tencent.assistant.localres;

import android.content.Context;
import com.qq.AppService.AstApp;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class LocalMediaManager {
    private static LocalMediaManager b = null;

    /* renamed from: a  reason: collision with root package name */
    private Context f805a;
    private ArrayList<LocalMediaLoader<?>> c;

    protected LocalMediaManager() {
        this.f805a = null;
        this.f805a = AstApp.i().getBaseContext();
        this.c = new ArrayList<>();
    }

    public static LocalMediaManager getInstance() {
        if (b == null) {
            b = new LocalMediaManager();
        }
        return b;
    }

    public LocalMediaLoader<?> getLoader(int i) {
        switch (i) {
            case 1:
                return new LocalImageLoader(this.f805a);
            case 2:
                return new LocalVideoLoader(this.f805a);
            case 3:
                return new LocalAudioLoader(this.f805a);
            case 4:
            default:
                return null;
            case 5:
                return new ao(this.f805a);
            case 6:
                return new LocalAVLoader(this.f805a);
            case 7:
                return new WiFiReceiveLoader(this.f805a);
        }
    }
}
