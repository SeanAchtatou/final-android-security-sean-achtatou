package com.tencent.assistant.localres.model;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.qq.AppService.AstApp;

/* compiled from: ProGuard */
public class LocalApkInfo implements Parcelable {
    public static final int APK_STATE_INSTALL = 2;
    public static final int APK_STATE_NORMAL = 1;
    public static final int APK_STATE_SILENT_INSTALL = 3;
    public static final byte APP_INSTALL_TYPE_NO_SYSTEM = 1;
    public static final byte APP_INSTALL_TYPE_SYSTEM = 2;
    public static final Parcelable.Creator<LocalApkInfo> CREATOR = new b();
    public static final int ILLEGAL_LAUNCH_TIME = -1;
    public static final int INSTALL_LOCATION_PHONE = 1;
    public static final int INSTALL_LOCATION_SDCARD = 2;
    public int flags;
    public int launchCount;
    public int mApkState = 1;
    public int mAppIconRes;
    public String mAppName;
    public long mAppid;
    public long mBatteryUsage = 0;
    public long mDataUsage = 0;
    public long mFakeLastLaunchTime = -1;
    public int mGrayVersionCode = 0;
    public boolean mInstall = false;
    public long mInstallDate;
    public byte mInstalleLocation = 1;
    public boolean mIsEmpty = false;
    public boolean mIsEnabled = true;
    public boolean mIsHistory = false;
    public boolean mIsInternalDownload = true;
    public boolean mIsSelect = false;
    public boolean mIsSmartSlect = false;
    public boolean mIsUpdateApk = false;
    public long mLastLaunchTime = -1;
    public long mLastModified = 0;
    public String mLocalFilePath;
    public String mPackageName;
    public String mSortKey;
    public int mUid;
    public int mVersionCode;
    public String mVersionName;
    public String manifestMd5;
    public long occupySize = 0;
    public String signature;

    public LocalApkInfo() {
    }

    public LocalApkInfo(String str, String str2, String str3, String str4, int i, int i2, int i3, int i4, Long l, Long l2, String str5, String str6, String str7, long j, long j2) {
        this.mPackageName = str;
        this.mVersionName = str2;
        this.mAppName = str3;
        this.mSortKey = str4;
        this.mAppIconRes = i;
        this.mVersionCode = i2;
        this.launchCount = i3;
        this.flags = i4;
        this.occupySize = l.longValue();
        this.mInstallDate = l2.longValue();
        this.signature = str5;
        this.manifestMd5 = str6;
        this.mLocalFilePath = str7;
        this.mDataUsage = j;
        this.mBatteryUsage = j2;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int i2;
        int i3;
        int i4 = 1;
        parcel.writeString(this.mPackageName);
        parcel.writeString(this.mVersionName);
        parcel.writeString(this.mAppName);
        parcel.writeString(this.mSortKey);
        parcel.writeInt(this.mAppIconRes);
        parcel.writeInt(this.mVersionCode);
        parcel.writeInt(this.launchCount);
        parcel.writeInt(i);
        parcel.writeLong(this.occupySize);
        parcel.writeLong(this.mInstallDate);
        parcel.writeString(this.signature);
        parcel.writeString(this.manifestMd5);
        parcel.writeString(this.mLocalFilePath);
        parcel.writeLong(this.mLastLaunchTime);
        parcel.writeLong(this.mFakeLastLaunchTime);
        parcel.writeLong(this.mDataUsage);
        parcel.writeLong(this.mBatteryUsage);
        parcel.writeByte(this.mInstalleLocation);
        parcel.writeInt(this.mUid);
        if (this.mIsSelect) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        parcel.writeByte((byte) i2);
        if (this.mIsSmartSlect) {
            i3 = 1;
        } else {
            i3 = 0;
        }
        parcel.writeByte((byte) i3);
        if (!this.mIsEnabled) {
            i4 = 0;
        }
        parcel.writeByte((byte) i4);
    }

    public int hashCode() {
        String localApkInfoKey = getLocalApkInfoKey();
        if (localApkInfoKey == null) {
            return 0;
        }
        return localApkInfoKey.hashCode();
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof LocalApkInfo)) {
            return false;
        }
        return ((LocalApkInfo) obj).getLocalApkInfoKey().equals(getLocalApkInfoKey());
    }

    public String getLocalApkInfoKey() {
        if (TextUtils.isEmpty(this.mPackageName)) {
            return this.mPackageName;
        }
        return this.mPackageName + this.mVersionCode + this.mGrayVersionCode;
    }

    public Intent getStartIntent() {
        return AstApp.i().getPackageManager().getLaunchIntentForPackage(this.mPackageName);
    }

    public byte getAppType() {
        return (this.flags & 1) != 0 ? (byte) 2 : 1;
    }
}
