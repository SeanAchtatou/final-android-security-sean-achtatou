package com.tencent.assistant.localres.localapk.loadapkservice;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.tencent.assistant.utils.XLog;
import java.lang.ref.WeakReference;

/* compiled from: ProGuard */
class b extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private WeakReference<GetApkInfoService> f843a;

    b(Looper looper, GetApkInfoService getApkInfoService) {
        super(looper);
        this.f843a = new WeakReference<>(getApkInfoService);
    }

    public void handleMessage(Message message) {
        XLog.d(GetApkInfoService.f841a, "[GetApkInfoService] handleMessage msg.what: " + message.what);
        if (this.f843a.get() != null) {
            switch (message.what) {
                case 1:
                    Bundle bundle = new Bundle();
                    bundle.putString("s", "Hello!");
                    this.f843a.get().a(message.replyTo, 1, message.arg1, bundle);
                    return;
                case 2:
                    this.f843a.get().a(message);
                    return;
                case 3:
                    this.f843a.get().b(message);
                    return;
                default:
                    super.handleMessage(message);
                    return;
            }
        }
    }
}
