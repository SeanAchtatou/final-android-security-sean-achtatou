package com.tencent.assistant.localres;

import com.qq.AppService.AstApp;
import com.tencent.assistant.db.table.p;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.e;
import java.util.List;

/* compiled from: ProGuard */
class k implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ long f830a;
    final /* synthetic */ boolean b;
    final /* synthetic */ ApkResourceManager c;

    k(ApkResourceManager apkResourceManager, long j, boolean z) {
        this.c = apkResourceManager;
        this.f830a = j;
        this.b = z;
    }

    public void run() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        List<String> b2 = e.b(AstApp.i());
        for (String str : b2) {
            LocalApkInfo localApkInfo = (LocalApkInfo) this.c.h.get(str);
            if (localApkInfo != null) {
                localApkInfo.mLastLaunchTime = this.f830a;
                localApkInfo.mFakeLastLaunchTime = this.f830a;
            }
        }
        this.c.j();
        if (this.c.c == null) {
            p unused = this.c.c = new p(AstApp.i());
        }
        this.c.c.a(b2, this.f830a);
        if (this.b) {
            this.c.h();
        }
    }
}
