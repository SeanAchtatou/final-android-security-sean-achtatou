package com.tencent.assistant.localres;

import com.tencent.assistant.localres.LocalMediaLoader;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
class am implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ LocalMediaLoader f819a;
    final /* synthetic */ ArrayList b;
    final /* synthetic */ boolean c;
    final /* synthetic */ LocalMediaLoader d;

    am(LocalMediaLoader localMediaLoader, LocalMediaLoader localMediaLoader2, ArrayList arrayList, boolean z) {
        this.d = localMediaLoader;
        this.f819a = localMediaLoader2;
        this.b = arrayList;
        this.c = z;
    }

    public void run() {
        synchronized (this.d.e) {
            Iterator<WeakReference<LocalMediaLoader.ILocalMediaLoaderListener<T>>> it = this.d.e.iterator();
            while (it.hasNext()) {
                LocalMediaLoader.ILocalMediaLoaderListener iLocalMediaLoaderListener = (LocalMediaLoader.ILocalMediaLoaderListener) it.next().get();
                if (iLocalMediaLoaderListener != null) {
                    iLocalMediaLoaderListener.onLocalMediaLoaderFinish(this.f819a, this.b, this.c);
                }
            }
        }
    }
}
