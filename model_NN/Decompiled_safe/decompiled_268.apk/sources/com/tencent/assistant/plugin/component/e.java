package com.tencent.assistant.plugin.component;

import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
/* synthetic */ class e {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f1092a = new int[AppConst.AppState.values().length];

    static {
        try {
            f1092a[AppConst.AppState.DOWNLOADING.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f1092a[AppConst.AppState.PAUSED.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f1092a[AppConst.AppState.QUEUING.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f1092a[AppConst.AppState.FAIL.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f1092a[AppConst.AppState.DOWNLOAD.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
        try {
            f1092a[AppConst.AppState.UPDATE.ordinal()] = 6;
        } catch (NoSuchFieldError e6) {
        }
        try {
            f1092a[AppConst.AppState.INSTALLED.ordinal()] = 7;
        } catch (NoSuchFieldError e7) {
        }
        try {
            f1092a[AppConst.AppState.DOWNLOADED.ordinal()] = 8;
        } catch (NoSuchFieldError e8) {
        }
        try {
            f1092a[AppConst.AppState.INSTALLING.ordinal()] = 9;
        } catch (NoSuchFieldError e9) {
        }
        try {
            f1092a[AppConst.AppState.UNINSTALLING.ordinal()] = 10;
        } catch (NoSuchFieldError e10) {
        }
        try {
            f1092a[AppConst.AppState.ILLEGAL.ordinal()] = 11;
        } catch (NoSuchFieldError e11) {
        }
    }
}
