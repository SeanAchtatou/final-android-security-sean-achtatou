package com.tencent.assistant.plugin;

import java.util.Arrays;

/* compiled from: ProGuard */
public class UserLoginInfo {
    public static final int STATE_LOGIN_CANCELLED = 0;
    public static final int STATE_LOGIN_FAILED = 1;
    public static final int STATE_LOGOUT = 4;
    public static final int STATE_NORMAL_LOGIN_SUCC = 2;
    public static final int STATE_QUICK_LOGIN_SUCC = 3;

    /* renamed from: a  reason: collision with root package name */
    private int f1075a;
    private String b;
    private String c;
    private byte[] d;
    private long e;

    public long getUin() {
        return this.e;
    }

    public void setUin(long j) {
        this.e = j;
    }

    public String getNickName() {
        return this.c;
    }

    public void setNickName(String str) {
        this.c = str;
    }

    public int getState() {
        return this.f1075a;
    }

    public void setState(int i) {
        this.f1075a = i;
    }

    public String getPic() {
        return this.b;
    }

    public void setPic(String str) {
        this.b = str;
    }

    public byte[] getA2() {
        return this.d;
    }

    public void setA2(byte[] bArr) {
        this.d = bArr;
    }

    public String toString() {
        return "UserLoginInfo [state=" + this.f1075a + ", pic=" + this.b + ", nickName=" + this.c + ", A2=" + Arrays.toString(this.d) + ", uin=" + this.e + "]";
    }
}
