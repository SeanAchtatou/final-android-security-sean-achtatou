package com.tencent.assistant.plugin.component;

import android.app.Activity;
import android.content.Context;
import android.os.Message;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.module.k;
import com.tencent.assistant.plugin.PluginDownloadInfo;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.PluginStartEntry;
import com.tencent.assistant.plugin.mgr.c;
import com.tencent.assistant.plugin.mgr.i;
import com.tencent.assistant.plugin.proxy.PluginProxyActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.at;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.component.appdetail.ExchangeColorTextView;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;

/* compiled from: ProGuard */
public class PluginDownStateButton extends RelativeLayout implements UIEventListener {
    private static final String TMA_ST_PLUGIN_SLOT = "03_001";
    private Context context;
    private View.OnClickListener defaultListener;
    private TextView fileSizeView;
    private Activity host;
    private boolean installFail;
    /* access modifiers changed from: private */
    public boolean isPreDownload;
    /* access modifiers changed from: private */
    public boolean isUpdate;
    private AstApp mApp;
    private Button mButton;
    private LayoutInflater mInflater;
    private ProgressBar mProgressBar;
    private TextView mTextView;
    private ExchangeColorTextView mTextViewExt;
    private boolean openFail;
    private PluginDownloadInfo pluginDownloadInfo;
    /* access modifiers changed from: private */
    public PluginStartEntry pluginStartEntry;

    /* access modifiers changed from: private */
    public void pluginSTReport(int i) {
        int i2;
        int i3 = 2000;
        if (this.context instanceof BaseActivity) {
            i2 = ((BaseActivity) this.context).f();
            i3 = ((BaseActivity) this.context).m();
        } else {
            i2 = 2000;
        }
        String str = this.pluginStartEntry != null ? this.pluginStartEntry.getPluginId() + "|" + this.pluginStartEntry.getPackageName() + "|" + this.pluginStartEntry.getStartActivity() + "|" + 2 : Constants.STR_EMPTY;
        STInfoV2 sTInfoV2 = new STInfoV2(i2, "03_001", i3, STConst.ST_DEFAULT_SLOT, i);
        sTInfoV2.extraData = str;
        l.a(sTInfoV2);
    }

    public PluginDownStateButton(Context context2) {
        this(context2, null);
    }

    public PluginDownStateButton(Context context2, AttributeSet attributeSet) {
        super(context2, attributeSet);
        this.installFail = false;
        this.openFail = false;
        this.isUpdate = false;
        this.isPreDownload = false;
        this.defaultListener = new a(this);
        this.context = context2;
        this.mInflater = LayoutInflater.from(context2);
        initButtonView();
        this.mApp = AstApp.i();
        setOnClickListener(this.defaultListener);
    }

    public void setHost(Activity activity) {
        this.host = activity;
    }

    public void onResume() {
        this.mApp.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_SUCC, this);
        this.mApp.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_DOWNLOADING, this);
        this.mApp.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_QUEUING, this);
        this.mApp.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_PAUSE, this);
        this.mApp.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_FAIL, this);
        this.mApp.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_DELETE, this);
        this.mApp.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FAIL, this);
        this.mApp.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_SUCC, this);
        this.mApp.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_PRELOAD_FAIL, this);
        this.mApp.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_PRELOAD_SUCC, this);
        this.mApp.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FINAL_FAIL, this);
        if (!this.isPreDownload) {
            PluginInfo a2 = i.b().a(this.pluginStartEntry.getPackageName());
            if (a2 == null || a2.getVersion() < this.pluginStartEntry.getVersionCode()) {
                updateStateBtn();
                return;
            }
            PluginProxyActivity.a(this.context, a2.getPackageName(), a2.getVersion(), this.pluginStartEntry.getStartActivity(), a2.getInProcess(), null, a2.getLaunchApplication());
            this.host.finish();
        }
    }

    public void onPause() {
        this.mApp.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_SUCC, this);
        this.mApp.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_DOWNLOADING, this);
        this.mApp.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_QUEUING, this);
        this.mApp.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_PAUSE, this);
        this.mApp.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_FAIL, this);
        this.mApp.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_DELETE, this);
        this.mApp.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_SUCC, this);
        this.mApp.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FAIL, this);
        this.mApp.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_PRELOAD_FAIL, this);
        this.mApp.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_PRELOAD_SUCC, this);
        this.mApp.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FINAL_FAIL, this);
    }

    private void initButtonView() {
        this.mInflater.inflate((int) R.layout.component_plugin_down_button, this);
        this.mButton = (Button) findViewById(R.id.state_btn);
        this.mProgressBar = (ProgressBar) findViewById(R.id.appdownload_progress_bar);
        this.mTextView = (TextView) findViewById(R.id.app_floating_txt);
        this.mTextViewExt = (ExchangeColorTextView) findViewById(R.id.app_floating_txt_b);
        this.fileSizeView = (TextView) findViewById(R.id.file_size_txt);
    }

    public void setDownloadInfo(PluginDownloadInfo pluginDownloadInfo2, PluginStartEntry pluginStartEntry2) {
        this.pluginDownloadInfo = pluginDownloadInfo2;
        this.pluginStartEntry = pluginStartEntry2;
        updateStateBtn();
        DownloadInfo b = c.a().b(pluginDownloadInfo2);
        if (b != null) {
            this.isPreDownload = b.uiType == SimpleDownloadInfo.UIType.PLUGIN_PREDOWNLOAD;
        }
    }

    public void setUpdateState(boolean z) {
        this.isUpdate = z;
    }

    private void updateStateBtn() {
        AppConst.AppState a2;
        DownloadInfo downloadInfo = null;
        PluginInfo a3 = i.b().a(this.pluginStartEntry.getPackageName());
        if (a3 == null || a3.getVersion() < this.pluginDownloadInfo.version) {
            downloadInfo = c.a().b(this.pluginDownloadInfo);
            if (downloadInfo == null) {
                downloadInfo = c.a().a(this.pluginDownloadInfo, SimpleDownloadInfo.UIType.PLUGIN_PREDOWNLOAD);
            }
            a2 = k.a(downloadInfo, this.pluginDownloadInfo, a3);
            if (this.isPreDownload) {
                a2 = AppConst.AppState.DOWNLOAD;
            }
        } else {
            a2 = AppConst.AppState.INSTALLED;
        }
        updateBtnTextColor(a2);
        updateBtnText(downloadInfo, a2);
        updateBtnBg(a2);
        updateBtnVisible(a2);
    }

    private void updateBtnVisible(AppConst.AppState appState) {
        switch (e.f1092a[appState.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
                setDownloadBarVisibility(0);
                setDownloadButtonVisibility(8);
                break;
            default:
                setDownloadBarVisibility(8);
                setDownloadButtonVisibility(0);
                break;
        }
        if (appState == AppConst.AppState.DOWNLOAD || appState == AppConst.AppState.UPDATE) {
            this.fileSizeView.setVisibility(0);
        } else {
            this.fileSizeView.setVisibility(8);
        }
    }

    private void setDownloadBarVisibility(int i) {
        if (this.mProgressBar != null) {
            this.mProgressBar.setVisibility(i);
        }
    }

    private void setDownloadButtonVisibility(int i) {
        if (this.mButton != null) {
            this.mButton.setVisibility(i);
        }
    }

    private void updateBtnBg(AppConst.AppState appState) {
        if (appState != null) {
            switch (e.f1092a[appState.ordinal()]) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 8:
                case 9:
                    this.mButton.setBackgroundResource(R.drawable.btn_green_selector);
                    return;
                case 5:
                case 6:
                    this.mButton.setBackgroundResource(R.drawable.btn_green_selector);
                    return;
                case 7:
                    this.mButton.setBackgroundResource(R.drawable.appdetail_bar_btn_installed_selector_v5);
                    return;
                case 10:
                    return;
                default:
                    this.mButton.setBackgroundResource(R.drawable.btn_green_selector);
                    return;
            }
        }
    }

    private void updateBtnText(DownloadInfo downloadInfo, AppConst.AppState appState) {
        int i;
        if (appState != null) {
            if (downloadInfo == null || downloadInfo.response == null) {
                i = 0;
            } else {
                boolean isProgressShowFake = SimpleDownloadInfo.isProgressShowFake(downloadInfo, appState);
                XLog.d("miles", "PluginDownStateButton >> showFake = " + isProgressShowFake);
                if (isProgressShowFake) {
                    i = downloadInfo.response.f;
                } else {
                    i = SimpleDownloadInfo.getPercent(downloadInfo);
                }
                XLog.d("miles", "PluginDownStateButton >> fakePercent = " + downloadInfo.response.f);
                XLog.d("miles", "PluginDownStateButton >> final downloadPercent = " + i);
                setProgressAndDownloading(i, 0);
            }
            switch (e.f1092a[appState.ordinal()]) {
                case 1:
                    updateBtnText(i, downloadInfo != null ? i + "%" : this.context.getResources().getString(R.string.downloading_display_pause));
                    return;
                case 2:
                case 4:
                    updateBtnText(i, this.context.getResources().getString(R.string.appbutton_continuing));
                    return;
                case 3:
                    updateBtnText(i, this.context.getResources().getString(R.string.queuing));
                    return;
                case 5:
                    updateBtnText(0, this.context.getResources().getString(R.string.plugin_download));
                    this.fileSizeView.setText("(" + at.a(this.pluginDownloadInfo.fileSize) + ")");
                    return;
                case 6:
                    updateBtnText(0, this.context.getResources().getString(R.string.plugin_update));
                    this.fileSizeView.setText("(" + at.a(this.pluginDownloadInfo.fileSize) + ")");
                    return;
                case 7:
                case 8:
                case 9:
                    if (this.openFail || this.installFail) {
                        updateBtnText(i, this.context.getResources().getString(R.string.plugin_preload_fail_txt));
                        return;
                    } else {
                        updateBtnText(i, this.context.getResources().getString(R.string.plugin_installing));
                        return;
                    }
                case 10:
                default:
                    updateBtnText(i, this.context.getResources().getString(R.string.appbutton_unknown));
                    return;
                case 11:
                    return;
            }
        }
    }

    private void updateBtnText(int i, String str) {
        if (i <= 0 || i >= 100) {
            this.mTextView.setVisibility(0);
            this.mTextViewExt.setVisibility(8);
            this.mTextView.setText(str);
            return;
        }
        this.mTextView.setVisibility(8);
        this.mTextViewExt.setVisibility(0);
        this.mTextViewExt.setText(str);
        this.mTextViewExt.invalidate();
    }

    private void setProgressAndDownloading(int i, int i2) {
        if (this.mProgressBar != null) {
            this.mProgressBar.setProgress(i);
            this.mProgressBar.setSecondaryProgress(i2);
            invalidate();
            if (i > 0 && i < 100) {
                this.mTextViewExt.setTextWhiteLenth(((float) i) / 100.0f);
                this.mTextViewExt.invalidate();
            }
        }
    }

    private void updateBtnTextColor(AppConst.AppState appState) {
        if (appState == AppConst.AppState.DOWNLOAD || appState == AppConst.AppState.UPDATE) {
            this.mTextView.setTextColor(this.context.getResources().getColor(17170443));
        } else {
            this.mTextView.setTextColor(this.context.getResources().getColor(R.color.appdetail_tag_text_color_blue_n));
        }
    }

    public void handleUIEvent(Message message) {
        if (!this.isPreDownload) {
            String str = Constants.STR_EMPTY;
            if (message.obj instanceof String) {
                str = (String) message.obj;
            }
            if (str.equals(this.pluginDownloadInfo.getDownloadTicket())) {
                switch (message.what) {
                    case EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_SUCC:
                        PluginInfo a2 = i.b().a(str);
                        if (a2 != null && a2.getVersion() > this.pluginStartEntry.getVersionCode()) {
                            this.pluginStartEntry.setVersionCode(a2.getVersion());
                        }
                        TemporaryThreadManager.get().start(new d(this));
                        this.installFail = false;
                        break;
                    case EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FAIL:
                    case EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FINAL_FAIL:
                        Toast.makeText(this.context, (int) R.string.plugin_install_fail, 0).show();
                        this.installFail = true;
                        break;
                    case EventDispatcherEnum.UI_EVENT_PLUGIN_PRELOAD_SUCC:
                        PluginInfo a3 = i.b().a(str);
                        if (a3 != null && a3.getVersion() > this.pluginStartEntry.getVersionCode()) {
                            this.pluginStartEntry.setVersionCode(a3.getVersion());
                        }
                        PluginInfo a4 = i.b().a(this.pluginStartEntry.getPackageName(), this.pluginStartEntry.getVersionCode());
                        if (a4 == null) {
                            Toast.makeText(this.context, (int) R.string.plugin_entry_not_exist, 0).show();
                            this.openFail = true;
                            break;
                        } else {
                            PluginProxyActivity.a(this.context, a4.getPackageName(), a4.getVersion(), this.pluginStartEntry.getStartActivity(), a4.getInProcess(), null, a4.getLaunchApplication());
                            this.host.finish();
                            this.openFail = false;
                            break;
                        }
                    case EventDispatcherEnum.UI_EVENT_PLUGIN_PRELOAD_FAIL:
                        Toast.makeText(this.context, (int) R.string.plugin_preload_fail_toast, 0).show();
                        this.openFail = true;
                        break;
                }
                updateStateBtn();
            }
        }
    }
}
