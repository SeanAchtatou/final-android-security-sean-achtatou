package com.tencent.assistant.plugin.mgr;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.Global;
import com.tencent.assistant.db.table.t;
import com.tencent.assistant.event.EventDispatcher;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.XLog;
import com.tencent.beacon.event.a;
import com.tencent.connect.common.Constants;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public class i {
    private static EventDispatcher d = AstApp.i().j();
    private static boolean e = false;
    private static i f;

    /* renamed from: a  reason: collision with root package name */
    private t f1103a;
    private Map<String, PluginInfo> b;
    private Map<String, PluginInfo> c;

    private i() {
        a();
    }

    public synchronized void a() {
        PluginInfo pluginInfo;
        this.f1103a = new t();
        List<PluginInfo> a2 = this.f1103a.a();
        if (a2 != null) {
            ConcurrentHashMap concurrentHashMap = new ConcurrentHashMap(a2.size());
            ConcurrentHashMap concurrentHashMap2 = new ConcurrentHashMap(a2.size());
            for (PluginInfo next : a2) {
                if (a.a(next) && ((pluginInfo = (PluginInfo) concurrentHashMap.get(next.getPackageName())) == null || pluginInfo.getVersion() < next.getVersion())) {
                    concurrentHashMap2.put(next.getPluginUniqueKey(), next);
                    concurrentHashMap.put(next.getPackageName(), next);
                }
            }
            this.b = concurrentHashMap2;
            this.c = concurrentHashMap;
        }
    }

    public static synchronized i b() {
        i iVar;
        synchronized (i.class) {
            if (f == null) {
                f = new i();
            }
            iVar = f;
        }
        return iVar;
    }

    public List<PluginInfo> a(int i) {
        ArrayList arrayList = new ArrayList();
        for (String next : this.c.keySet()) {
            PluginInfo pluginInfo = this.c.get(next);
            if (pluginInfo != null && (i == -1 || i == pluginInfo.getInProcess())) {
                arrayList.add(this.c.get(next));
            }
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.plugin.mgr.i.a(android.content.Context, java.lang.String, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, java.lang.String, int]
     candidates:
      com.tencent.assistant.plugin.mgr.i.a(java.lang.String, android.content.pm.ApplicationInfo, android.content.pm.ActivityInfo[], java.lang.String):java.lang.String
      com.tencent.assistant.plugin.mgr.i.a(android.content.Context, java.lang.String, java.lang.String, boolean):boolean */
    public synchronized boolean a(Context context, String str, String str2) {
        return a(context, str, str2, false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x019c A[Catch:{ Exception -> 0x05a7, Exception -> 0x0115 }] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x035e A[Catch:{ Exception -> 0x05a7, Exception -> 0x0115 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean a(android.content.Context r30, java.lang.String r31, java.lang.String r32, boolean r33) {
        /*
            r29 = this;
            monitor-enter(r29)
            java.io.File r3 = new java.io.File     // Catch:{ all -> 0x0057 }
            r0 = r31
            r3.<init>(r0)     // Catch:{ all -> 0x0057 }
            boolean r1 = r3.exists()     // Catch:{ all -> 0x0057 }
            if (r1 != 0) goto L_0x005a
            com.tencent.assistant.event.EventDispatcher r1 = com.tencent.assistant.plugin.mgr.i.d     // Catch:{ all -> 0x0057 }
            com.tencent.assistant.event.EventDispatcher r2 = com.tencent.assistant.plugin.mgr.i.d     // Catch:{ all -> 0x0057 }
            r3 = 1116(0x45c, float:1.564E-42)
            r0 = r32
            android.os.Message r2 = r2.obtainMessage(r3, r0)     // Catch:{ all -> 0x0057 }
            r1.sendMessage(r2)     // Catch:{ all -> 0x0057 }
            r3 = 0
            r4 = 1
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0057 }
            r1.<init>()     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = "当前插件文件不存在:"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0057 }
            r0 = r31
            java.lang.StringBuilder r1 = r1.append(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r5 = r1.toString()     // Catch:{ all -> 0x0057 }
            r6 = -1
            r1 = r29
            r2 = r32
            r1.a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0057 }
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0057 }
            r2.<init>()     // Catch:{ all -> 0x0057 }
            java.lang.String r3 = "当前插件文件不存在:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0057 }
            r0 = r31
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0057 }
            r1.<init>(r2)     // Catch:{ all -> 0x0057 }
            throw r1     // Catch:{ all -> 0x0057 }
        L_0x0057:
            r1 = move-exception
            monitor-exit(r29)
            throw r1
        L_0x005a:
            android.content.pm.PackageManager r1 = r30.getPackageManager()     // Catch:{ Exception -> 0x0115 }
            r2 = 129(0x81, float:1.81E-43)
            r0 = r31
            android.content.pm.PackageInfo r7 = com.tencent.assistant.utils.e.a(r1, r0, r2)     // Catch:{ Exception -> 0x0115 }
            com.tencent.assistant.plugin.PluginInfo r8 = new com.tencent.assistant.plugin.PluginInfo     // Catch:{ Exception -> 0x0115 }
            r8.<init>()     // Catch:{ Exception -> 0x0115 }
            if (r7 == 0) goto L_0x0089
            java.lang.String r1 = r7.packageName     // Catch:{ Exception -> 0x0115 }
            r8.setPackageName(r1)     // Catch:{ Exception -> 0x0115 }
            int r1 = r7.versionCode     // Catch:{ Exception -> 0x0115 }
            r8.setVersion(r1)     // Catch:{ Exception -> 0x0115 }
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x0115 }
            r0 = r31
            r1.<init>(r0)     // Catch:{ Exception -> 0x0115 }
            java.lang.String r1 = com.tencent.assistant.utils.aq.a(r1)     // Catch:{ Exception -> 0x0115 }
            java.lang.String r1 = r1.toLowerCase()     // Catch:{ Exception -> 0x0115 }
            r8.setFileMd5(r1)     // Catch:{ Exception -> 0x0115 }
        L_0x0089:
            if (r32 == 0) goto L_0x0177
            if (r7 == 0) goto L_0x0177
            java.lang.String r1 = r7.packageName     // Catch:{ all -> 0x0057 }
            if (r1 == 0) goto L_0x0177
            java.lang.String r1 = r7.packageName     // Catch:{ all -> 0x0057 }
            r0 = r32
            boolean r1 = r0.equals(r1)     // Catch:{ all -> 0x0057 }
            if (r1 != 0) goto L_0x0177
            com.tencent.assistant.event.EventDispatcher r1 = com.tencent.assistant.plugin.mgr.i.d     // Catch:{ all -> 0x0057 }
            com.tencent.assistant.event.EventDispatcher r2 = com.tencent.assistant.plugin.mgr.i.d     // Catch:{ all -> 0x0057 }
            r3 = 1106(0x452, float:1.55E-42)
            r0 = r32
            android.os.Message r2 = r2.obtainMessage(r3, r0)     // Catch:{ all -> 0x0057 }
            r1.sendMessage(r2)     // Catch:{ all -> 0x0057 }
            r3 = 0
            r4 = 3
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0057 }
            r1.<init>()     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = "当前插件文件的包名不正确:"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0057 }
            r0 = r31
            java.lang.StringBuilder r1 = r1.append(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = ",expect packagename:"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0057 }
            r0 = r32
            java.lang.StringBuilder r1 = r1.append(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = ",acctual:"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = r7.packageName     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0057 }
            java.lang.String r5 = r1.toString()     // Catch:{ all -> 0x0057 }
            int r6 = r7.versionCode     // Catch:{ all -> 0x0057 }
            r1 = r29
            r2 = r32
            r1.a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0057 }
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0057 }
            r2.<init>()     // Catch:{ all -> 0x0057 }
            java.lang.String r3 = "当前插件文件的包名不正确:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0057 }
            r0 = r31
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r3 = ",expect packagename:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0057 }
            r0 = r32
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r3 = ",acctual:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0057 }
            java.lang.String r3 = r7.packageName     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0057 }
            r1.<init>(r2)     // Catch:{ all -> 0x0057 }
            throw r1     // Catch:{ all -> 0x0057 }
        L_0x0115:
            r1 = move-exception
            r7 = r1
            r7.printStackTrace()     // Catch:{ all -> 0x0057 }
            com.tencent.assistant.event.EventDispatcher r1 = com.tencent.assistant.plugin.mgr.i.d     // Catch:{ all -> 0x0057 }
            com.tencent.assistant.event.EventDispatcher r2 = com.tencent.assistant.plugin.mgr.i.d     // Catch:{ all -> 0x0057 }
            r3 = 1116(0x45c, float:1.564E-42)
            r0 = r32
            android.os.Message r2 = r2.obtainMessage(r3, r0)     // Catch:{ all -> 0x0057 }
            r1.sendMessage(r2)     // Catch:{ all -> 0x0057 }
            r3 = 0
            r4 = 2
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0057 }
            r1.<init>()     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = "当前插件文件异常:"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0057 }
            r0 = r31
            java.lang.StringBuilder r1 = r1.append(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = ",ex:"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ all -> 0x0057 }
            java.lang.String r5 = r1.toString()     // Catch:{ all -> 0x0057 }
            r6 = -1
            r1 = r29
            r2 = r32
            r1.a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0057 }
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0057 }
            r2.<init>()     // Catch:{ all -> 0x0057 }
            java.lang.String r3 = "当前插件文件异常:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0057 }
            r0 = r31
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r3 = ",ex:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0057 }
            r1.<init>(r2)     // Catch:{ all -> 0x0057 }
            throw r1     // Catch:{ all -> 0x0057 }
        L_0x0177:
            if (r7 == 0) goto L_0x03c0
            android.content.pm.ApplicationInfo r1 = r7.applicationInfo     // Catch:{ all -> 0x0057 }
            if (r1 == 0) goto L_0x03c0
            android.content.pm.ApplicationInfo r1 = r7.applicationInfo     // Catch:{ all -> 0x0057 }
            r0 = r31
            r1.sourceDir = r0     // Catch:{ all -> 0x0057 }
            android.content.pm.ApplicationInfo r1 = r7.applicationInfo     // Catch:{ all -> 0x0057 }
            r0 = r31
            r1.publicSourceDir = r0     // Catch:{ all -> 0x0057 }
            android.content.pm.ApplicationInfo r1 = r7.applicationInfo     // Catch:{ all -> 0x0057 }
            android.os.Bundle r1 = r1.metaData     // Catch:{ all -> 0x0057 }
            if (r1 != 0) goto L_0x06e8
            r2 = 0
            r4 = 2
            android.content.pm.a r2 = android.content.pm.b.a(r3, r4)     // Catch:{ Throwable -> 0x0358 }
        L_0x0195:
            if (r2 == 0) goto L_0x06e8
            android.os.Bundle r1 = r2.j     // Catch:{ all -> 0x0057 }
            r5 = r1
        L_0x019a:
            if (r5 == 0) goto L_0x035e
            java.lang.String r1 = "min_f_level"
            r2 = -1
            int r1 = r5.getInt(r1, r2)     // Catch:{ all -> 0x0057 }
            r2 = -1
            if (r1 != r2) goto L_0x06e5
            java.lang.String r1 = "min_f_level"
            java.lang.String r1 = r5.getString(r1)     // Catch:{ all -> 0x0057 }
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ all -> 0x0057 }
            r4 = r1
        L_0x01b1:
            java.lang.String r1 = "min_bao_version"
            r2 = -1
            int r1 = r5.getInt(r1, r2)     // Catch:{ all -> 0x0057 }
            r2 = -1
            if (r1 != r2) goto L_0x06e2
            java.lang.String r1 = "min_bao_version"
            java.lang.String r1 = r5.getString(r1)     // Catch:{ all -> 0x0057 }
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ all -> 0x0057 }
            r3 = r1
        L_0x01c6:
            java.lang.String r1 = "min_api_level"
            r2 = -1
            int r1 = r5.getInt(r1, r2)     // Catch:{ all -> 0x0057 }
            r2 = -1
            if (r1 != r2) goto L_0x06df
            java.lang.String r1 = "min_api_level"
            java.lang.String r1 = r5.getString(r1)     // Catch:{ all -> 0x0057 }
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ all -> 0x0057 }
            r2 = r1
        L_0x01db:
            java.lang.String r1 = "inprocess"
            r6 = -1
            int r1 = r5.getInt(r1, r6)     // Catch:{ all -> 0x0057 }
            r6 = -1
            if (r1 != r6) goto L_0x01ef
            java.lang.String r1 = "inprocess"
            java.lang.String r1 = r5.getString(r1)     // Catch:{ all -> 0x0057 }
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ all -> 0x0057 }
        L_0x01ef:
            java.lang.String r6 = "sms_receiver"
            java.lang.String r6 = r5.getString(r6)     // Catch:{ all -> 0x0057 }
            java.lang.String r9 = "main_receiver"
            java.lang.String r9 = r5.getString(r9)     // Catch:{ all -> 0x0057 }
            java.lang.String r10 = "apk_receiver"
            java.lang.String r10 = r5.getString(r10)     // Catch:{ all -> 0x0057 }
            java.lang.String r11 = "sms_sent_receiver"
            java.lang.String r11 = r5.getString(r11)     // Catch:{ all -> 0x0057 }
            java.lang.String r12 = "author_receiver"
            java.lang.String r12 = r5.getString(r12)     // Catch:{ all -> 0x0057 }
            java.lang.String r13 = "heart_beat_receiver"
            java.lang.String r13 = r5.getString(r13)     // Catch:{ all -> 0x0057 }
            java.lang.String r14 = "app_service"
            java.lang.String r14 = r5.getString(r14)     // Catch:{ all -> 0x0057 }
            java.lang.String r15 = "ipc_service"
            java.lang.String r15 = r5.getString(r15)     // Catch:{ all -> 0x0057 }
            java.lang.String r16 = "dock_receiver"
            r0 = r16
            java.lang.String r16 = r5.getString(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r17 = "acc_service"
            r0 = r17
            java.lang.String r17 = r5.getString(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r18 = "deep_acc_service"
            r0 = r18
            java.lang.String r18 = r5.getString(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r19 = "dock_sec_service"
            r0 = r19
            java.lang.String r19 = r5.getString(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r20 = "command_handle_service"
            r0 = r20
            java.lang.String r20 = r5.getString(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r21 = "freewifi_receiver"
            r0 = r21
            java.lang.String r21 = r5.getString(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r22 = "freewifi_service"
            r0 = r22
            java.lang.String r22 = r5.getString(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r23 = "switch_phone_service"
            r0 = r23
            java.lang.String r23 = r5.getString(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r24 = "switch_phone_restore_service"
            r0 = r24
            java.lang.String r24 = r5.getString(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r25 = "start_activity"
            r0 = r25
            java.lang.String r25 = r5.getString(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r26 = "launch_application"
            r0 = r26
            java.lang.String r26 = r5.getString(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r27 = "qreader_receiver"
            r0 = r27
            java.lang.String r5 = r5.getString(r0)     // Catch:{ all -> 0x0057 }
            r8.setMinFLevel(r4)     // Catch:{ all -> 0x0057 }
            r8.setMinBaoVersion(r3)     // Catch:{ all -> 0x0057 }
            r8.setMinApiLevel(r2)     // Catch:{ all -> 0x0057 }
            r8.setInProcess(r1)     // Catch:{ all -> 0x0057 }
            r8.setSmsReceiverImpl(r6)     // Catch:{ all -> 0x0057 }
            r8.setMainReceiverImpl(r9)     // Catch:{ all -> 0x0057 }
            r8.setApkRecieverImpl(r10)     // Catch:{ all -> 0x0057 }
            r8.setSmsSentReceiverImpl(r11)     // Catch:{ all -> 0x0057 }
            r8.setAuthorReceiverImpl(r12)     // Catch:{ all -> 0x0057 }
            r8.setHeartBeatReceiverImpl(r13)     // Catch:{ all -> 0x0057 }
            r8.setAppServiceImpl(r14)     // Catch:{ all -> 0x0057 }
            r8.setIpcServiceImpl(r15)     // Catch:{ all -> 0x0057 }
            r0 = r26
            r8.setLaunchApplication(r0)     // Catch:{ all -> 0x0057 }
            r0 = r16
            r8.setDockReceiverImpl(r0)     // Catch:{ all -> 0x0057 }
            r0 = r17
            r8.setAccelerationServiceImpl(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r1 = "dock_sec_service"
            r0 = r19
            r8.addExtendServiceImpl(r1, r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r1 = "deep_acc_service"
            r0 = r18
            r8.addExtendServiceImpl(r1, r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r1 = "command_handle_service"
            r0 = r20
            r8.addExtendServiceImpl(r1, r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r1 = "freewifi_service"
            r0 = r22
            r8.addExtendServiceImpl(r1, r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r1 = "qreader_receiver"
            r8.addExtendReceiverImpl(r1, r5)     // Catch:{ all -> 0x0057 }
            java.lang.String r1 = "freewifi_receiver"
            r0 = r21
            r8.addExtendReceiverImpl(r1, r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r1 = "switch_phone_service"
            r0 = r23
            r8.addExtendServiceImpl(r1, r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r1 = "switch_phone_restore_service"
            r0 = r24
            r8.addExtendServiceImpl(r1, r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r1 = r8.getPackageName()     // Catch:{ all -> 0x0057 }
            int r2 = r8.getVersion()     // Catch:{ all -> 0x0057 }
            boolean r1 = com.tencent.assistant.plugin.mgr.a.a(r1, r2)     // Catch:{ all -> 0x0057 }
            if (r1 != 0) goto L_0x0422
            com.tencent.assistant.event.EventDispatcher r1 = com.tencent.assistant.plugin.mgr.i.d     // Catch:{ all -> 0x0057 }
            com.tencent.assistant.event.EventDispatcher r2 = com.tencent.assistant.plugin.mgr.i.d     // Catch:{ all -> 0x0057 }
            r3 = 1106(0x452, float:1.55E-42)
            java.lang.String r4 = r8.getPackageName()     // Catch:{ all -> 0x0057 }
            android.os.Message r2 = r2.obtainMessage(r3, r4)     // Catch:{ all -> 0x0057 }
            r1.sendMessage(r2)     // Catch:{ all -> 0x0057 }
            r3 = 0
            r4 = 6
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0057 }
            r1.<init>()     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = "当前安装的插件文件不兼容当前版本应用宝:"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0057 }
            r0 = r31
            java.lang.StringBuilder r1 = r1.append(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = ",plugininfo:"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r1 = r1.append(r8)     // Catch:{ all -> 0x0057 }
            java.lang.String r5 = r1.toString()     // Catch:{ all -> 0x0057 }
            int r6 = r8.getVersion()     // Catch:{ all -> 0x0057 }
            r1 = r29
            r2 = r32
            r1.a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0057 }
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0057 }
            r2.<init>()     // Catch:{ all -> 0x0057 }
            java.lang.String r3 = "当前安装的插件文件不兼容当前版本应用宝:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0057 }
            r0 = r31
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r3 = ",plugininfo:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r2 = r2.append(r8)     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0057 }
            r1.<init>(r2)     // Catch:{ all -> 0x0057 }
            throw r1     // Catch:{ all -> 0x0057 }
        L_0x0358:
            r3 = move-exception
            r3.printStackTrace()     // Catch:{ all -> 0x0057 }
            goto L_0x0195
        L_0x035e:
            com.tencent.assistant.event.EventDispatcher r1 = com.tencent.assistant.plugin.mgr.i.d     // Catch:{ all -> 0x0057 }
            com.tencent.assistant.event.EventDispatcher r2 = com.tencent.assistant.plugin.mgr.i.d     // Catch:{ all -> 0x0057 }
            r3 = 1106(0x452, float:1.55E-42)
            java.lang.String r4 = r8.getPackageName()     // Catch:{ all -> 0x0057 }
            android.os.Message r2 = r2.obtainMessage(r3, r4)     // Catch:{ all -> 0x0057 }
            r1.sendMessage(r2)     // Catch:{ all -> 0x0057 }
            r3 = 0
            r4 = 4
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0057 }
            r1.<init>()     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = "当前安装的非合法插件文件:"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0057 }
            r0 = r31
            java.lang.StringBuilder r1 = r1.append(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = ",plugininfo:"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r1 = r1.append(r8)     // Catch:{ all -> 0x0057 }
            java.lang.String r5 = r1.toString()     // Catch:{ all -> 0x0057 }
            int r6 = r8.getVersion()     // Catch:{ all -> 0x0057 }
            r1 = r29
            r2 = r32
            r1.a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0057 }
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0057 }
            r2.<init>()     // Catch:{ all -> 0x0057 }
            java.lang.String r3 = "当前安装的非合法插件文件:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0057 }
            r0 = r31
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r3 = ",plugininfo:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r2 = r2.append(r8)     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0057 }
            r1.<init>(r2)     // Catch:{ all -> 0x0057 }
            throw r1     // Catch:{ all -> 0x0057 }
        L_0x03c0:
            com.tencent.assistant.event.EventDispatcher r1 = com.tencent.assistant.plugin.mgr.i.d     // Catch:{ all -> 0x0057 }
            com.tencent.assistant.event.EventDispatcher r2 = com.tencent.assistant.plugin.mgr.i.d     // Catch:{ all -> 0x0057 }
            r3 = 1106(0x452, float:1.55E-42)
            java.lang.String r4 = r8.getPackageName()     // Catch:{ all -> 0x0057 }
            android.os.Message r2 = r2.obtainMessage(r3, r4)     // Catch:{ all -> 0x0057 }
            r1.sendMessage(r2)     // Catch:{ all -> 0x0057 }
            r3 = 0
            r4 = 5
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0057 }
            r1.<init>()     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = "当前安装的非合法插件文件1:"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0057 }
            r0 = r31
            java.lang.StringBuilder r1 = r1.append(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = ",plugininfo:"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r1 = r1.append(r8)     // Catch:{ all -> 0x0057 }
            java.lang.String r5 = r1.toString()     // Catch:{ all -> 0x0057 }
            int r6 = r8.getVersion()     // Catch:{ all -> 0x0057 }
            r1 = r29
            r2 = r32
            r1.a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0057 }
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0057 }
            r2.<init>()     // Catch:{ all -> 0x0057 }
            java.lang.String r3 = "当前安装的非合法插件文件1:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0057 }
            r0 = r31
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r3 = ",plugininfo:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r2 = r2.append(r8)     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0057 }
            r1.<init>(r2)     // Catch:{ all -> 0x0057 }
            throw r1     // Catch:{ all -> 0x0057 }
        L_0x0422:
            int r1 = r8.getMinFLevel()     // Catch:{ all -> 0x0057 }
            int r2 = com.tencent.assistant.plugin.mgr.j.b()     // Catch:{ all -> 0x0057 }
            if (r1 > r2) goto L_0x043e
            int r1 = r8.getMinBaoVersion()     // Catch:{ all -> 0x0057 }
            int r2 = com.tencent.assistant.Global.getAppVersionCode()     // Catch:{ all -> 0x0057 }
            if (r1 > r2) goto L_0x043e
            int r1 = r8.getMinApiLevel()     // Catch:{ all -> 0x0057 }
            int r2 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x0057 }
            if (r1 <= r2) goto L_0x04b8
        L_0x043e:
            com.tencent.assistant.event.EventDispatcher r1 = com.tencent.assistant.plugin.mgr.i.d     // Catch:{ all -> 0x0057 }
            com.tencent.assistant.event.EventDispatcher r2 = com.tencent.assistant.plugin.mgr.i.d     // Catch:{ all -> 0x0057 }
            r3 = 1106(0x452, float:1.55E-42)
            java.lang.String r4 = r8.getPackageName()     // Catch:{ all -> 0x0057 }
            android.os.Message r2 = r2.obtainMessage(r3, r4)     // Catch:{ all -> 0x0057 }
            r1.sendMessage(r2)     // Catch:{ all -> 0x0057 }
            r3 = 0
            r4 = 7
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0057 }
            r1.<init>()     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = "当前安装的插件文件不符合版本要求:"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0057 }
            r0 = r31
            java.lang.StringBuilder r1 = r1.append(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = ",plugininfo:"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r1 = r1.append(r8)     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = ",current api level:"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0057 }
            int r2 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0057 }
            java.lang.String r5 = r1.toString()     // Catch:{ all -> 0x0057 }
            int r6 = r8.getVersion()     // Catch:{ all -> 0x0057 }
            r1 = r29
            r2 = r32
            r1.a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0057 }
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0057 }
            r2.<init>()     // Catch:{ all -> 0x0057 }
            java.lang.String r3 = "当前安装的插件文件不符合版本要求:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0057 }
            r0 = r31
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r3 = ",plugininfo:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r2 = r2.append(r8)     // Catch:{ all -> 0x0057 }
            java.lang.String r3 = ",current api level:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0057 }
            int r3 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0057 }
            r1.<init>(r2)     // Catch:{ all -> 0x0057 }
            throw r1     // Catch:{ all -> 0x0057 }
        L_0x04b8:
            java.lang.String r1 = r8.getPackageName()     // Catch:{ all -> 0x0057 }
            r0 = r29
            com.tencent.assistant.plugin.PluginInfo r9 = r0.a(r1)     // Catch:{ all -> 0x0057 }
            if (r9 == 0) goto L_0x054a
            int r1 = r9.getVersion()     // Catch:{ all -> 0x0057 }
            int r2 = r8.getVersion()     // Catch:{ all -> 0x0057 }
            if (r1 <= r2) goto L_0x052a
            r3 = 0
            r4 = 10
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0057 }
            r1.<init>()     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = "当前安装的插件文件:"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0057 }
            r0 = r31
            java.lang.StringBuilder r1 = r1.append(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = ",plugininfo:"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r1 = r1.append(r8)     // Catch:{ all -> 0x0057 }
            java.lang.String r5 = r1.toString()     // Catch:{ all -> 0x0057 }
            int r6 = r8.getVersion()     // Catch:{ all -> 0x0057 }
            r1 = r29
            r2 = r32
            r1.a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0057 }
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0057 }
            r2.<init>()     // Catch:{ all -> 0x0057 }
            java.lang.String r3 = "当前安装的插件文件版本过旧，已安装版本:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r2 = r2.append(r9)     // Catch:{ all -> 0x0057 }
            java.lang.String r3 = ",需要安装的路径:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0057 }
            r0 = r31
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r3 = ",plugininfo:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r2 = r2.append(r8)     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0057 }
            r1.<init>(r2)     // Catch:{ all -> 0x0057 }
            throw r1     // Catch:{ all -> 0x0057 }
        L_0x052a:
            if (r33 != 0) goto L_0x054a
            int r1 = r9.getVersion()     // Catch:{ all -> 0x0057 }
            int r2 = r8.getVersion()     // Catch:{ all -> 0x0057 }
            if (r1 != r2) goto L_0x054a
            com.tencent.assistant.event.EventDispatcher r1 = com.tencent.assistant.plugin.mgr.i.d     // Catch:{ all -> 0x0057 }
            com.tencent.assistant.event.EventDispatcher r2 = com.tencent.assistant.plugin.mgr.i.d     // Catch:{ all -> 0x0057 }
            r3 = 1105(0x451, float:1.548E-42)
            java.lang.String r4 = r8.getPackageName()     // Catch:{ all -> 0x0057 }
            android.os.Message r2 = r2.obtainMessage(r3, r4)     // Catch:{ all -> 0x0057 }
            r1.sendMessage(r2)     // Catch:{ all -> 0x0057 }
            r1 = 1
        L_0x0548:
            monitor-exit(r29)
            return r1
        L_0x054a:
            int r1 = r8.getInProcess()     // Catch:{ all -> 0x0057 }
            r2 = 1
            if (r1 != r2) goto L_0x0554
            r29.b(r30)     // Catch:{ all -> 0x0057 }
        L_0x0554:
            r0 = r29
            com.tencent.assistant.db.table.t r1 = r0.f1103a     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = r8.getPackageName()     // Catch:{ all -> 0x0057 }
            r1.a(r2)     // Catch:{ all -> 0x0057 }
            r0 = r29
            r0.c(r8)     // Catch:{ Exception -> 0x05a7 }
            if (r7 == 0) goto L_0x0573
            java.lang.String r1 = r7.packageName     // Catch:{ Exception -> 0x05a7 }
            int r2 = r7.versionCode     // Catch:{ Exception -> 0x05a7 }
            java.lang.String r1 = com.tencent.assistant.plugin.mgr.j.a(r1, r2)     // Catch:{ Exception -> 0x05a7 }
            r0 = r31
            com.tencent.assistant.utils.FileUtil.copy(r0, r1)     // Catch:{ Exception -> 0x05a7 }
        L_0x0573:
            if (r7 == 0) goto L_0x0623
            boolean r1 = android.text.TextUtils.isEmpty(r25)     // Catch:{ all -> 0x0057 }
            if (r1 != 0) goto L_0x0623
            java.lang.String r1 = "\\|"
            r0 = r25
            java.lang.String[] r2 = r0.split(r1)     // Catch:{ all -> 0x0057 }
            int r3 = r2.length     // Catch:{ all -> 0x0057 }
            r1 = 0
        L_0x0585:
            if (r1 >= r3) goto L_0x0623
            r4 = r2[r1]     // Catch:{ all -> 0x0057 }
            boolean r5 = android.text.TextUtils.isEmpty(r4)     // Catch:{ all -> 0x0057 }
            if (r5 != 0) goto L_0x05a4
            java.lang.String r5 = r7.packageName     // Catch:{ all -> 0x0057 }
            android.content.pm.ApplicationInfo r6 = r7.applicationInfo     // Catch:{ all -> 0x0057 }
            android.content.pm.ActivityInfo[] r9 = r7.activities     // Catch:{ all -> 0x0057 }
            r0 = r29
            java.lang.String r5 = r0.a(r5, r6, r9, r4)     // Catch:{ all -> 0x0057 }
            boolean r6 = android.text.TextUtils.isEmpty(r5)     // Catch:{ all -> 0x0057 }
            if (r6 != 0) goto L_0x05a4
            r8.addEntry(r5, r4)     // Catch:{ all -> 0x0057 }
        L_0x05a4:
            int r1 = r1 + 1
            goto L_0x0585
        L_0x05a7:
            r1 = move-exception
            r7 = r1
            r7.printStackTrace()     // Catch:{ all -> 0x0057 }
            com.tencent.assistant.event.EventDispatcher r1 = com.tencent.assistant.plugin.mgr.i.d     // Catch:{ all -> 0x0057 }
            com.tencent.assistant.event.EventDispatcher r2 = com.tencent.assistant.plugin.mgr.i.d     // Catch:{ all -> 0x0057 }
            r3 = 1106(0x452, float:1.55E-42)
            java.lang.String r4 = r8.getPackageName()     // Catch:{ all -> 0x0057 }
            android.os.Message r2 = r2.obtainMessage(r3, r4)     // Catch:{ all -> 0x0057 }
            r1.sendMessage(r2)     // Catch:{ all -> 0x0057 }
            r3 = 0
            r4 = 8
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0057 }
            r1.<init>()     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = "插件文件拷贝失败:"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0057 }
            r0 = r31
            java.lang.StringBuilder r1 = r1.append(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = "ex:"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = ",plugininfo:"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r1 = r1.append(r8)     // Catch:{ all -> 0x0057 }
            java.lang.String r5 = r1.toString()     // Catch:{ all -> 0x0057 }
            int r6 = r8.getVersion()     // Catch:{ all -> 0x0057 }
            r1 = r29
            r2 = r32
            r1.a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0057 }
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0057 }
            r2.<init>()     // Catch:{ all -> 0x0057 }
            java.lang.String r3 = "插件文件拷贝失败:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0057 }
            r0 = r31
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r3 = "ex:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ all -> 0x0057 }
            java.lang.String r3 = ",plugininfo:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r2 = r2.append(r8)     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0057 }
            r1.<init>(r2)     // Catch:{ all -> 0x0057 }
            throw r1     // Catch:{ all -> 0x0057 }
        L_0x0623:
            r2 = 0
            r0 = r29
            com.tencent.assistant.db.table.t r1 = r0.f1103a     // Catch:{ RuntimeException -> 0x0696 }
            long r3 = r1.a(r8)     // Catch:{ RuntimeException -> 0x0696 }
            java.lang.String r1 = "kevin"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0696 }
            r5.<init>()     // Catch:{ RuntimeException -> 0x0696 }
            java.lang.String r6 = "instalPlugin--> "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ RuntimeException -> 0x0696 }
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ RuntimeException -> 0x0696 }
            java.lang.String r3 = r3.toString()     // Catch:{ RuntimeException -> 0x0696 }
            com.tencent.assistant.utils.XLog.d(r1, r3)     // Catch:{ RuntimeException -> 0x0696 }
            java.lang.String r7 = r8.getPluginUniqueKey()     // Catch:{ RuntimeException -> 0x0696 }
            r0 = r29
            java.util.Map<java.lang.String, com.tencent.assistant.plugin.PluginInfo> r1 = r0.b     // Catch:{ RuntimeException -> 0x06d8 }
            r1.put(r7, r8)     // Catch:{ RuntimeException -> 0x06d8 }
            r0 = r29
            java.util.Map<java.lang.String, com.tencent.assistant.plugin.PluginInfo> r1 = r0.c     // Catch:{ RuntimeException -> 0x06d8 }
            java.lang.String r2 = r8.getPackageName()     // Catch:{ RuntimeException -> 0x06d8 }
            r1.put(r2, r8)     // Catch:{ RuntimeException -> 0x06d8 }
            r0 = r29
            r0.b(r8)     // Catch:{ RuntimeException -> 0x06d8 }
            r0 = r30
            com.tencent.assistant.plugin.mgr.h.b(r0, r8)     // Catch:{ RuntimeException -> 0x06d8 }
            r0 = r29
            r0.a(r8)     // Catch:{ RuntimeException -> 0x06d8 }
            com.tencent.assistant.plugin.mgr.j.a(r30)     // Catch:{ RuntimeException -> 0x06d8 }
            com.tencent.assistant.event.EventDispatcher r1 = com.tencent.assistant.plugin.mgr.i.d     // Catch:{ RuntimeException -> 0x06d8 }
            com.tencent.assistant.event.EventDispatcher r2 = com.tencent.assistant.plugin.mgr.i.d     // Catch:{ RuntimeException -> 0x06d8 }
            r3 = 1105(0x451, float:1.548E-42)
            java.lang.String r4 = r8.getPackageName()     // Catch:{ RuntimeException -> 0x06d8 }
            android.os.Message r2 = r2.obtainMessage(r3, r4)     // Catch:{ RuntimeException -> 0x06d8 }
            r1.sendMessage(r2)     // Catch:{ RuntimeException -> 0x06d8 }
            if (r32 != 0) goto L_0x0693
            java.lang.String r2 = r8.getPackageName()     // Catch:{ RuntimeException -> 0x06d8 }
        L_0x0683:
            r3 = 1
            r4 = 0
            java.lang.String r5 = "正常安装"
            int r6 = r8.getVersion()     // Catch:{ RuntimeException -> 0x06d8 }
            r1 = r29
            r1.a(r2, r3, r4, r5, r6)     // Catch:{ RuntimeException -> 0x06d8 }
            r1 = 1
            goto L_0x0548
        L_0x0693:
            r2 = r32
            goto L_0x0683
        L_0x0696:
            r1 = move-exception
            r7 = r1
            r1 = r2
        L_0x0699:
            com.tencent.assistant.event.EventDispatcher r2 = com.tencent.assistant.plugin.mgr.i.d     // Catch:{ all -> 0x0057 }
            com.tencent.assistant.event.EventDispatcher r3 = com.tencent.assistant.plugin.mgr.i.d     // Catch:{ all -> 0x0057 }
            r4 = 1106(0x452, float:1.55E-42)
            java.lang.String r5 = r8.getPackageName()     // Catch:{ all -> 0x0057 }
            android.os.Message r3 = r3.obtainMessage(r4, r5)     // Catch:{ all -> 0x0057 }
            r2.sendMessage(r3)     // Catch:{ all -> 0x0057 }
            r0 = r29
            com.tencent.assistant.db.table.t r2 = r0.f1103a     // Catch:{ all -> 0x0057 }
            java.lang.String r3 = r8.getPackageName()     // Catch:{ all -> 0x0057 }
            r2.a(r3)     // Catch:{ all -> 0x0057 }
            r0 = r29
            java.util.Map<java.lang.String, com.tencent.assistant.plugin.PluginInfo> r2 = r0.b     // Catch:{ all -> 0x0057 }
            r2.remove(r1)     // Catch:{ all -> 0x0057 }
            r0 = r29
            java.util.Map<java.lang.String, com.tencent.assistant.plugin.PluginInfo> r1 = r0.c     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = r8.getPackageName()     // Catch:{ all -> 0x0057 }
            r1.remove(r2)     // Catch:{ all -> 0x0057 }
            r3 = 0
            r4 = 9
            java.lang.String r5 = "插件安装过程抛出异常"
            int r6 = r8.getVersion()     // Catch:{ all -> 0x0057 }
            r1 = r29
            r2 = r32
            r1.a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0057 }
            throw r7     // Catch:{ all -> 0x0057 }
        L_0x06d8:
            r1 = move-exception
            r28 = r1
            r1 = r7
            r7 = r28
            goto L_0x0699
        L_0x06df:
            r2 = r1
            goto L_0x01db
        L_0x06e2:
            r3 = r1
            goto L_0x01c6
        L_0x06e5:
            r4 = r1
            goto L_0x01b1
        L_0x06e8:
            r5 = r1
            goto L_0x019a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.plugin.mgr.i.a(android.content.Context, java.lang.String, java.lang.String, boolean):boolean");
    }

    private void a(String str, boolean z, int i, String str2, int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", Global.getQUAForBeacon());
        hashMap.put("B3", com.tencent.assistant.utils.t.g());
        hashMap.put("B4", String.valueOf(i));
        hashMap.put("B5", str2);
        hashMap.put("B6", i2 + Constants.STR_EMPTY);
        hashMap.put("B7", String.valueOf(z));
        a.a("plugin_install_" + str, z, -1, -1, hashMap, true);
    }

    private void a(PluginInfo pluginInfo) {
        if (pluginInfo != null && !TextUtils.isEmpty(pluginInfo.getPackageName()) && pluginInfo.getPackageName().equals("com.assistant.accelerate")) {
            boolean writeToAppData = FileUtil.writeToAppData("plugin.ini", pluginInfo.getPackageName() + ":" + String.valueOf(pluginInfo.getVersion()), 1);
            HashMap hashMap = new HashMap();
            hashMap.put("B1", Global.getPhoneGuidAndGen());
            hashMap.put("B2", Global.getQUAForBeacon());
            hashMap.put("B3", com.tencent.assistant.utils.t.g());
            XLog.d("beacon", "beacon report >> WritePluginIniResult. " + hashMap.toString());
            a.a("WritePluginIniResult", writeToAppData, -1, -1, hashMap, true);
        }
    }

    private void b(Context context) {
        context.sendBroadcast(new Intent("com.tencent.android.qqdownloader.action.CONNECT_DOWN"));
    }

    private void b(PluginInfo pluginInfo) {
        FileUtil.extractSoToFilePath(j.a(pluginInfo.getPackageName(), pluginInfo.getVersion()), j.a(pluginInfo.getPackageName()));
    }

    private void c(PluginInfo pluginInfo) {
        File file;
        File[] listFiles;
        File file2;
        File[] listFiles2;
        File file3;
        File[] listFiles3;
        String c2 = j.c(pluginInfo.getPackageName());
        String pluginDir = FileUtil.getPluginDir();
        if (!(pluginDir == null || (file3 = new File(pluginDir)) == null || (listFiles3 = file3.listFiles()) == null || listFiles3.length <= 0)) {
            for (File file4 : listFiles3) {
                if (file4.getName().startsWith(c2)) {
                    file4.delete();
                }
            }
        }
        String a2 = j.a(pluginInfo.getPackageName());
        if (!(a2 == null || (file2 = new File(a2)) == null || (listFiles2 = file2.listFiles()) == null || listFiles2.length <= 0)) {
            for (File delete : listFiles2) {
                delete.delete();
            }
        }
        String b2 = j.b(pluginInfo.getPackageName());
        if (b2 != null && (file = new File(b2)) != null && (listFiles = file.listFiles()) != null && listFiles.length > 0) {
            for (File file5 : listFiles) {
                if (file5.getName().startsWith(c2)) {
                    file5.delete();
                }
            }
        }
    }

    private String a(String str, ApplicationInfo applicationInfo, ActivityInfo[] activityInfoArr, String str2) {
        CharSequence text;
        PackageManager packageManager = AstApp.i().getPackageManager();
        for (ActivityInfo activityInfo : activityInfoArr) {
            if (activityInfo.name != null && activityInfo.name.equals(str2) && packageManager != null && (text = packageManager.getText(str, activityInfo.labelRes, applicationInfo)) != null) {
                return text.toString();
            }
        }
        return null;
    }

    public PluginInfo a(String str, int i) {
        String d2 = j.d(str, i);
        if (this.b.get(d2) == null && AstApp.i() != null && !AstApp.i().s()) {
            a();
        }
        return this.b.get(d2);
    }

    public synchronized void a(Context context, String str, int i) {
        try {
            PluginInfo a2 = a(str, i);
            if (!(a2 == null || h.a(context, a2) == null)) {
                AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_PLUGIN_PRELOAD_SUCC, str));
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_PLUGIN_PRELOAD_FAIL, str));
    }

    public PluginInfo a(String str) {
        if (this.c.get(str) == null && AstApp.i() != null && !AstApp.i().s()) {
            a();
        }
        return this.c.get(str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.plugin.mgr.i.a(android.content.Context, java.lang.String, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.assistant.plugin.mgr.i.a(java.lang.String, android.content.pm.ApplicationInfo, android.content.pm.ActivityInfo[], java.lang.String):java.lang.String
      com.tencent.assistant.plugin.mgr.i.a(android.content.Context, java.lang.String, java.lang.String, boolean):boolean */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0110, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0111, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00c0 A[SYNTHETIC, Splitter:B:37:0x00c0] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00c5 A[SYNTHETIC, Splitter:B:40:0x00c5] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a(android.content.Context r13) {
        /*
            r12 = this;
            r0 = 0
            r2 = 0
            monitor-enter(r12)
            java.lang.String r4 = "plugin"
            android.content.res.Resources r1 = r13.getResources()     // Catch:{ all -> 0x00cc }
            android.content.res.AssetManager r1 = r1.getAssets()     // Catch:{ all -> 0x00cc }
            java.lang.String[] r5 = r1.list(r4)     // Catch:{ all -> 0x00cc }
            if (r5 == 0) goto L_0x0016
            int r1 = r5.length     // Catch:{ all -> 0x00cc }
            if (r1 != 0) goto L_0x0018
        L_0x0016:
            monitor-exit(r12)
            return
        L_0x0018:
            java.lang.String r6 = com.tencent.assistant.utils.FileUtil.getTmpRootDir()     // Catch:{ all -> 0x00cc }
            r1 = 1024(0x400, float:1.435E-42)
            byte[] r7 = new byte[r1]     // Catch:{ all -> 0x00cc }
            int r8 = r5.length     // Catch:{ all -> 0x00cc }
        L_0x0021:
            if (r0 >= r8) goto L_0x00f1
            r1 = r5[r0]     // Catch:{ all -> 0x00cc }
            boolean r3 = android.text.TextUtils.isEmpty(r1)     // Catch:{ all -> 0x00cc }
            if (r3 == 0) goto L_0x002e
        L_0x002b:
            int r0 = r0 + 1
            goto L_0x0021
        L_0x002e:
            r3 = 47
            int r3 = r1.lastIndexOf(r3)     // Catch:{ all -> 0x00cc }
            int r3 = r3 + 1
            java.lang.String r3 = r1.substring(r3)     // Catch:{ all -> 0x00cc }
            boolean r9 = android.text.TextUtils.isEmpty(r3)     // Catch:{ all -> 0x00cc }
            if (r9 != 0) goto L_0x002b
            java.lang.String r9 = ".plg"
            boolean r9 = r3.endsWith(r9)     // Catch:{ all -> 0x00cc }
            if (r9 == 0) goto L_0x002b
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x00cc }
            r9.<init>()     // Catch:{ all -> 0x00cc }
            java.lang.StringBuilder r9 = r9.append(r6)     // Catch:{ all -> 0x00cc }
            java.lang.String r10 = java.io.File.separator     // Catch:{ all -> 0x00cc }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ all -> 0x00cc }
            java.lang.StringBuilder r3 = r9.append(r3)     // Catch:{ all -> 0x00cc }
            java.lang.String r9 = r3.toString()     // Catch:{ all -> 0x00cc }
            android.content.res.Resources r3 = r13.getResources()     // Catch:{ Exception -> 0x0129, all -> 0x011f }
            android.content.res.AssetManager r3 = r3.getAssets()     // Catch:{ Exception -> 0x0129, all -> 0x011f }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0129, all -> 0x011f }
            r10.<init>()     // Catch:{ Exception -> 0x0129, all -> 0x011f }
            java.lang.StringBuilder r10 = r10.append(r4)     // Catch:{ Exception -> 0x0129, all -> 0x011f }
            java.lang.String r11 = java.io.File.separator     // Catch:{ Exception -> 0x0129, all -> 0x011f }
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x0129, all -> 0x011f }
            java.lang.StringBuilder r1 = r10.append(r1)     // Catch:{ Exception -> 0x0129, all -> 0x011f }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0129, all -> 0x011f }
            java.io.InputStream r3 = r3.open(r1)     // Catch:{ Exception -> 0x0129, all -> 0x011f }
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x012d, all -> 0x0122 }
            r1.<init>(r9)     // Catch:{ Exception -> 0x012d, all -> 0x0122 }
        L_0x0087:
            int r10 = r3.read(r7)     // Catch:{ Exception -> 0x0093, all -> 0x0126 }
            r11 = -1
            if (r10 == r11) goto L_0x00cf
            r11 = 0
            r1.write(r7, r11, r10)     // Catch:{ Exception -> 0x0093, all -> 0x0126 }
            goto L_0x0087
        L_0x0093:
            r0 = move-exception
            r2 = r3
        L_0x0095:
            java.lang.String r3 = "PluginInstalledManager"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00bd }
            r4.<init>()     // Catch:{ all -> 0x00bd }
            java.lang.String r5 = "install plugin fail."
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x00bd }
            java.lang.StringBuilder r4 = r4.append(r9)     // Catch:{ all -> 0x00bd }
            java.lang.String r5 = ",ex:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x00bd }
            java.lang.StringBuilder r4 = r4.append(r0)     // Catch:{ all -> 0x00bd }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x00bd }
            com.tencent.assistant.utils.XLog.w(r3, r4)     // Catch:{ all -> 0x00bd }
            java.lang.RuntimeException r3 = new java.lang.RuntimeException     // Catch:{ all -> 0x00bd }
            r3.<init>(r0)     // Catch:{ all -> 0x00bd }
            throw r3     // Catch:{ all -> 0x00bd }
        L_0x00bd:
            r0 = move-exception
        L_0x00be:
            if (r2 == 0) goto L_0x00c3
            r2.close()     // Catch:{ IOException -> 0x0106 }
        L_0x00c3:
            if (r1 == 0) goto L_0x00c8
            r1.close()     // Catch:{ IOException -> 0x010b }
        L_0x00c8:
            com.tencent.assistant.utils.FileUtil.deleteFile(r9)     // Catch:{ Exception -> 0x0110 }
        L_0x00cb:
            throw r0     // Catch:{ all -> 0x00cc }
        L_0x00cc:
            r0 = move-exception
            monitor-exit(r12)
            throw r0
        L_0x00cf:
            r3.close()     // Catch:{ Exception -> 0x0093, all -> 0x0126 }
            r1.close()     // Catch:{ Exception -> 0x0093, all -> 0x0126 }
            r1 = 0
            r3 = 0
            r10 = 0
            r11 = 1
            r12.a(r13, r9, r10, r11)     // Catch:{ Exception -> 0x0129, all -> 0x011f }
            if (r2 == 0) goto L_0x00e1
            r1.close()     // Catch:{ IOException -> 0x0115 }
        L_0x00e1:
            if (r2 == 0) goto L_0x00e6
            r3.close()     // Catch:{ IOException -> 0x011a }
        L_0x00e6:
            com.tencent.assistant.utils.FileUtil.deleteFile(r9)     // Catch:{ Exception -> 0x00eb }
            goto L_0x002b
        L_0x00eb:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x00cc }
            goto L_0x002b
        L_0x00f1:
            r12.a()     // Catch:{ all -> 0x00cc }
            com.tencent.assistant.event.EventDispatcher r0 = com.tencent.assistant.plugin.mgr.i.d     // Catch:{ all -> 0x00cc }
            com.tencent.assistant.event.EventDispatcher r1 = com.tencent.assistant.plugin.mgr.i.d     // Catch:{ all -> 0x00cc }
            r2 = 1112(0x458, float:1.558E-42)
            android.os.Message r1 = r1.obtainMessage(r2)     // Catch:{ all -> 0x00cc }
            r0.sendMessage(r1)     // Catch:{ all -> 0x00cc }
            r0 = 1
            com.tencent.assistant.plugin.mgr.i.e = r0     // Catch:{ all -> 0x00cc }
            goto L_0x0016
        L_0x0106:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ all -> 0x00cc }
            goto L_0x00c3
        L_0x010b:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x00cc }
            goto L_0x00c8
        L_0x0110:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x00cc }
            goto L_0x00cb
        L_0x0115:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x00cc }
            goto L_0x00e1
        L_0x011a:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x00cc }
            goto L_0x00e6
        L_0x011f:
            r0 = move-exception
            r1 = r2
            goto L_0x00be
        L_0x0122:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x00be
        L_0x0126:
            r0 = move-exception
            r2 = r3
            goto L_0x00be
        L_0x0129:
            r0 = move-exception
            r1 = r2
            goto L_0x0095
        L_0x012d:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x0095
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.plugin.mgr.i.a(android.content.Context):void");
    }

    public static void a(boolean z) {
        e = z;
    }
}
