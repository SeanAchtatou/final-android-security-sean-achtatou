package com.tencent.assistant.plugin.a;

import com.tencent.assistant.login.model.MoblieQIdentityInfo;
import com.tencent.assistant.plugin.SimpleLoginInfo;
import com.tencent.nucleus.socialcontact.login.j;
import com.tencent.nucleus.socialcontact.login.l;
import com.tencent.nucleus.socialcontact.login.m;

/* compiled from: ProGuard */
public abstract class a {
    public static SimpleLoginInfo a() {
        j a2 = j.a();
        if (a2.k()) {
            MoblieQIdentityInfo moblieQIdentityInfo = (MoblieQIdentityInfo) a2.c();
            m f = l.f();
            if (moblieQIdentityInfo != null) {
                SimpleLoginInfo simpleLoginInfo = new SimpleLoginInfo();
                simpleLoginInfo.nickName = f.b;
                simpleLoginInfo.skey = moblieQIdentityInfo.getSKey();
                simpleLoginInfo.uin = moblieQIdentityInfo.getUin();
                simpleLoginInfo.sid = moblieQIdentityInfo.getSid();
                simpleLoginInfo.vkey = moblieQIdentityInfo.getVkey();
                return simpleLoginInfo;
            }
        }
        return null;
    }
}
