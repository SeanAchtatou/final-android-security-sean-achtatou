package com.tencent.assistant.plugin;

import android.os.Build;
import android.util.Pair;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.Global;
import com.tencent.assistant.db.table.s;
import com.tencent.assistant.m;
import com.tencent.assistant.model.QuickEntranceNotify;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.module.ak;
import com.tencent.assistant.plugin.mgr.a;
import com.tencent.assistant.plugin.mgr.j;
import com.tencent.assistant.protocol.jce.GetPluginListRequest;
import com.tencent.assistant.protocol.jce.GetPluginListResponse;
import com.tencent.assistant.protocol.jce.MAPlugin;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* compiled from: ProGuard */
public class GetPluginListEngine extends BaseEngine<GetPluginListCallback> implements ak {

    /* renamed from: a  reason: collision with root package name */
    private static GetPluginListEngine f1062a;
    private long b = m.a().v();
    private int c;
    private s d;
    /* access modifiers changed from: private */
    public List<PluginDownloadInfo> e = null;
    private Set<String> f = null;

    public static synchronized GetPluginListEngine getInstance() {
        GetPluginListEngine getPluginListEngine;
        synchronized (GetPluginListEngine.class) {
            if (f1062a == null) {
                f1062a = new GetPluginListEngine();
            }
            getPluginListEngine = f1062a;
        }
        return getPluginListEngine;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x006a, code lost:
        if (com.qq.AppService.AstApp.d() != false) goto L_0x006c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private GetPluginListEngine() {
        /*
            r4 = this;
            r0 = 0
            r4.<init>()
            r4.e = r0
            r4.f = r0
            com.tencent.assistant.m r0 = com.tencent.assistant.m.a()
            long r0 = r0.v()
            r4.b = r0
            com.tencent.assistant.module.ag r0 = com.tencent.assistant.module.ag.b()
            r0.a(r4)
            com.tencent.assistant.db.table.s r0 = new com.tencent.assistant.db.table.s
            r0.<init>()
            r4.d = r0
            com.tencent.assistant.db.table.s r0 = r4.d
            java.util.List r0 = r0.a()
            if (r0 == 0) goto L_0x003e
            int r1 = r0.size()
            if (r1 <= 0) goto L_0x003e
            android.util.Pair r1 = r4.a(r0)
            java.lang.Object r0 = r1.first
            java.util.List r0 = (java.util.List) r0
            r4.e = r0
            java.lang.Object r0 = r1.second
            java.util.Set r0 = (java.util.Set) r0
            r4.f = r0
        L_0x003e:
            com.qq.AppService.AstApp r0 = com.qq.AppService.AstApp.i()
            boolean r0 = r0.s()
            if (r0 != 0) goto L_0x0049
        L_0x0048:
            return
        L_0x0049:
            java.util.List<com.tencent.assistant.plugin.PluginDownloadInfo> r0 = r4.e
            if (r0 == 0) goto L_0x006c
            java.util.List<com.tencent.assistant.plugin.PluginDownloadInfo> r0 = r4.e
            int r0 = r0.size()
            if (r0 == 0) goto L_0x006c
            long r0 = r4.b
            r2 = -1
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 == 0) goto L_0x006c
            com.qq.AppService.AstApp r0 = com.qq.AppService.AstApp.i()
            if (r0 == 0) goto L_0x0072
            com.qq.AppService.AstApp.i()
            boolean r0 = com.qq.AppService.AstApp.d()
            if (r0 == 0) goto L_0x0072
        L_0x006c:
            r0 = 0
            r4.refreshData(r0)
            goto L_0x0048
        L_0x0072:
            long r0 = r4.b
            r4.refreshData(r0)
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.plugin.GetPluginListEngine.<init>():void");
    }

    private Pair<List<PluginDownloadInfo>, Set<String>> a(List<PluginDownloadInfo> list) {
        ArrayList arrayList = new ArrayList(list.size());
        HashSet hashSet = new HashSet(0);
        for (PluginDownloadInfo next : list) {
            if (a.a(next)) {
                arrayList.add(next);
            } else {
                hashSet.add(next.pluginPackageName);
            }
        }
        return Pair.create(arrayList, hashSet);
    }

    public int refreshData(long j) {
        this.b = j;
        return sendRequest(j);
    }

    public int sendRequest(long j) {
        notifyDataChangedInMainThread(new a(this));
        GetPluginListRequest getPluginListRequest = new GetPluginListRequest();
        getPluginListRequest.d((int) j);
        getPluginListRequest.b(Global.getAppVersionCode());
        getPluginListRequest.c(Build.VERSION.SDK_INT);
        getPluginListRequest.a(j.b());
        this.c = send(getPluginListRequest);
        return this.c;
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        String str;
        if (i == this.c) {
            GetPluginListResponse getPluginListResponse = (GetPluginListResponse) jceStruct2;
            ArrayList<MAPlugin> a2 = getPluginListResponse.a();
            long b2 = getPluginListResponse.b();
            if (a2 != null && a2.size() != 0 && (b2 != this.b || a2.size() != 0)) {
                if (a2 != null && a2.size() > 0) {
                    List<PluginDownloadInfo> list = this.e;
                    HashSet hashSet = new HashSet(a2.size());
                    ArrayList arrayList = new ArrayList(a2.size());
                    HashSet hashSet2 = new HashSet(0);
                    for (MAPlugin next : a2) {
                        PluginDownloadInfo pluginDownloadInfo = new PluginDownloadInfo();
                        pluginDownloadInfo.pluginId = next.f1408a;
                        pluginDownloadInfo.iconUrl = next.b;
                        pluginDownloadInfo.name = next.c;
                        pluginDownloadInfo.desc = next.d;
                        pluginDownloadInfo.fileSize = next.f;
                        pluginDownloadInfo.version = next.g;
                        pluginDownloadInfo.minPluginVersion = next.h;
                        pluginDownloadInfo.downUrl = next.i;
                        pluginDownloadInfo.type = next.j;
                        if (next.u == null) {
                            str = Constants.STR_EMPTY;
                        } else {
                            str = next.u.f1125a;
                        }
                        pluginDownloadInfo.actionUrl = str;
                        pluginDownloadInfo.startActivity = next.l;
                        pluginDownloadInfo.pluginPackageName = next.m;
                        pluginDownloadInfo.displayOrder = next.n;
                        pluginDownloadInfo.minApiLevel = next.o;
                        pluginDownloadInfo.minBaoVersion = next.p;
                        pluginDownloadInfo.needPreDownload = next.q;
                        pluginDownloadInfo.imgUrl = next.r;
                        pluginDownloadInfo.downloadTicket = pluginDownloadInfo.getDownloadTicket();
                        pluginDownloadInfo.pluginForBaoType = next.t;
                        this.d.a(pluginDownloadInfo);
                        hashSet.add(Integer.valueOf(next.f1408a));
                        if (a.a(pluginDownloadInfo)) {
                            arrayList.add(pluginDownloadInfo);
                        } else {
                            hashSet2.add(pluginDownloadInfo.pluginPackageName);
                        }
                    }
                    try {
                        Collections.sort(arrayList);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    if (list != null && list.size() > 0) {
                        for (PluginDownloadInfo next2 : list) {
                            if (!hashSet.contains(Integer.valueOf(next2.pluginId))) {
                                this.d.a(next2.pluginId);
                            }
                        }
                    }
                    this.e = arrayList;
                    this.f = hashSet2;
                }
                this.b = b2;
                m.a().a(this.b);
                notifyDataChangedInMainThread(new c(this));
            } else if (this.e != null && this.e.size() > 0) {
                notifyDataChangedInMainThread(new b(this));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChangedInMainThread(new d(this, i2));
    }

    public void onLocalDataHasUpdate() {
        long a2 = m.a().a((byte) 6);
        if (a2 != -11 && this.b != -1 && this.b != a2) {
            sendRequest(this.b);
        }
    }

    public void onPromptHasNewNotify(ArrayList<QuickEntranceNotify> arrayList) {
    }

    public List<PluginDownloadInfo> getList() {
        return this.e;
    }

    public PluginDownloadInfo getPlugin(int i) {
        if (this.e != null && this.e.size() > 0) {
            for (PluginDownloadInfo next : this.e) {
                if (next.pluginId == i) {
                    return next;
                }
            }
        }
        return null;
    }

    public PluginDownloadInfo getPlugin(String str) {
        if (this.e != null && this.e.size() > 0) {
            for (PluginDownloadInfo next : this.e) {
                if (next.getDownloadTicket() != null && next.getDownloadTicket().equals(str)) {
                    return next;
                }
            }
        }
        return null;
    }

    public PluginDownloadInfo getPluginByPackageName(String str) {
        if (this.e != null && this.e.size() > 0) {
            for (PluginDownloadInfo next : this.e) {
                if (next.pluginPackageName != null && next.pluginPackageName.equals(str)) {
                    return next;
                }
            }
        }
        return null;
    }

    public boolean isCompactForBaoType(String str) {
        return this.f == null || !this.f.contains(str);
    }
}
