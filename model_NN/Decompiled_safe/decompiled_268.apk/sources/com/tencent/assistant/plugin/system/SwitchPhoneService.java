package com.tencent.assistant.plugin.system;

import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
public class SwitchPhoneService extends BaseAppService {
    public int a() {
        return 0;
    }

    public String a(PluginInfo pluginInfo) {
        XLog.d("SwitchPhoneService-frame", "SwitchPhoneService getServiceClassPath... ");
        if (pluginInfo != null) {
            return pluginInfo.getExtendServiceImpl(PluginInfo.META_DATA_SWITCH_PHONE_SERVICE);
        }
        return null;
    }
}
