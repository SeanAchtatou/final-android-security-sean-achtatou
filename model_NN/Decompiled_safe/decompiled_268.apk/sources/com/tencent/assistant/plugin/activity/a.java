package com.tencent.assistant.plugin.activity;

import com.qq.AppService.AstApp;
import com.tencent.assistant.plugin.PluginDownloadInfo;
import com.tencent.assistant.plugin.mgr.i;
import com.tencent.pangu.download.DownloadInfo;

/* compiled from: ProGuard */
class a implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfo f1081a;
    final /* synthetic */ PluginDownloadInfo b;
    final /* synthetic */ PluginDetailActivity c;

    a(PluginDetailActivity pluginDetailActivity, DownloadInfo downloadInfo, PluginDownloadInfo pluginDownloadInfo) {
        this.c = pluginDetailActivity;
        this.f1081a = downloadInfo;
        this.b = pluginDownloadInfo;
    }

    public void run() {
        try {
            i.b().a(AstApp.i(), this.f1081a.getDownloadingPath(), this.b.pluginPackageName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
