package com.tencent.assistant.module;

import com.tencent.pangu.download.SimpleDownloadInfo;

/* compiled from: ProGuard */
/* synthetic */ class l {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f998a = new int[SimpleDownloadInfo.DownloadState.values().length];

    static {
        try {
            f998a[SimpleDownloadInfo.DownloadState.QUEUING.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f998a[SimpleDownloadInfo.DownloadState.DOWNLOADING.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f998a[SimpleDownloadInfo.DownloadState.PAUSED.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f998a[SimpleDownloadInfo.DownloadState.WAITTING_FOR_WIFI.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f998a[SimpleDownloadInfo.DownloadState.FAIL.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
        try {
            f998a[SimpleDownloadInfo.DownloadState.COMPLETE.ordinal()] = 6;
        } catch (NoSuchFieldError e6) {
        }
        try {
            f998a[SimpleDownloadInfo.DownloadState.INIT.ordinal()] = 7;
        } catch (NoSuchFieldError e7) {
        }
        try {
            f998a[SimpleDownloadInfo.DownloadState.SUCC.ordinal()] = 8;
        } catch (NoSuchFieldError e8) {
        }
        try {
            f998a[SimpleDownloadInfo.DownloadState.INSTALLED.ordinal()] = 9;
        } catch (NoSuchFieldError e9) {
        }
    }
}
