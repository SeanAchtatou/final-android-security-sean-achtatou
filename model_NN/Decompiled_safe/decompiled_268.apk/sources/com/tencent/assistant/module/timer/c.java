package com.tencent.assistant.module.timer;

import com.tencent.nucleus.manager.backgroundscan.BackgroundScanTimerJob;
import java.util.ArrayList;
import java.util.LinkedList;

/* compiled from: ProGuard */
public class c extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private LinkedList<String> f1014a = new LinkedList<>();
    private Object b = new Object();

    public c() {
        setName("Thread_TimerJobQueue");
        setDaemon(true);
        start();
    }

    public void run() {
        ArrayList<String> arrayList;
        while (true) {
            synchronized (this.b) {
                while (this.f1014a.size() == 0) {
                    try {
                        this.b.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                arrayList = new ArrayList<>(this.f1014a);
                this.f1014a.clear();
            }
            if (arrayList != null && !arrayList.isEmpty()) {
                for (String b2 : arrayList) {
                    b(b2);
                }
            } else {
                return;
            }
        }
    }

    public void a(String str) {
        synchronized (this.b) {
            this.f1014a.add(str);
            this.b.notify();
        }
    }

    private void b(String str) {
        TimerJob timerJob;
        try {
            Class<?> cls = Class.forName(str);
            if (cls.equals(BackgroundScanTimerJob.class)) {
                BackgroundScanTimerJob h = BackgroundScanTimerJob.h();
                if (h != null && h.a()) {
                    h.c();
                }
            } else if ((cls.newInstance() instanceof TimerJob) && (timerJob = (TimerJob) cls.newInstance()) != null && timerJob.a()) {
                timerJob.c();
            }
        } catch (ClassNotFoundException | Exception | IllegalAccessException | InstantiationException e) {
        }
    }
}
