package com.tencent.assistant.module.nac;

import com.tencent.assistant.Global;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    public HashMap<String, c> f1003a = new HashMap<>();

    public b() {
        String serverAddress = Global.getServerAddress();
        this.f1003a.put(serverAddress, new c(this, 1, serverAddress, 80, null, new ArrayList(), new ArrayList()));
    }

    public short a(String str) {
        try {
            c cVar = this.f1003a.get(new URL(str).getHost());
            if (cVar != null) {
                return cVar.f1004a;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return -1;
    }
}
