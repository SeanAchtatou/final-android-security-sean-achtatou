package com.tencent.assistant.module;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.g;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/* compiled from: ProGuard */
class ab implements CallbackHelper.Caller<g> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f956a;
    final /* synthetic */ int b;
    final /* synthetic */ boolean c;
    final /* synthetic */ y d;

    ab(y yVar, int i, int i2, boolean z) {
        this.d = yVar;
        this.f956a = i;
        this.b = i2;
        this.c = z;
    }

    /* renamed from: a */
    public void call(g gVar) {
        g gVar2 = gVar;
        gVar2.a(this.f956a, this.b, this.c, new LinkedHashMap(), new ArrayList(), true);
    }
}
