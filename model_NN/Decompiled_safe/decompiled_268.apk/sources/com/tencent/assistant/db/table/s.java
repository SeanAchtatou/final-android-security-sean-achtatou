package com.tencent.assistant.db.table;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.helper.DownloadDbHelper;
import com.tencent.assistant.db.helper.SQLiteDatabaseWrapper;
import com.tencent.assistant.db.helper.SqliteHelper;
import com.tencent.assistant.plugin.PluginDownloadInfo;
import com.tencent.open.SocialConstants;

/* compiled from: ProGuard */
public class s implements IBaseTable {
    public void a(PluginDownloadInfo pluginDownloadInfo) {
        if (pluginDownloadInfo != null) {
            try {
                SQLiteDatabaseWrapper writableDatabaseWrapper = getHelper().getWritableDatabaseWrapper();
                if (a(pluginDownloadInfo, writableDatabaseWrapper) <= 0) {
                    ContentValues contentValues = new ContentValues();
                    a(contentValues, pluginDownloadInfo);
                    writableDatabaseWrapper.insert("plugin_downloadinfos", null, contentValues);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private int a(PluginDownloadInfo pluginDownloadInfo, SQLiteDatabaseWrapper sQLiteDatabaseWrapper) {
        if (pluginDownloadInfo == null) {
            return -1;
        }
        try {
            ContentValues contentValues = new ContentValues();
            a(contentValues, pluginDownloadInfo);
            int update = sQLiteDatabaseWrapper.update("plugin_downloadinfos", contentValues, "plugin_id = ?", new String[]{String.valueOf(pluginDownloadInfo.pluginId)});
            if (update <= 0) {
                return 0;
            }
            return update;
        } catch (Exception e) {
            e.printStackTrace();
            return -2;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002f, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0034, code lost:
        if (r1 == null) goto L_0x002f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x002a, code lost:
        if (r1 == null) goto L_0x002f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002c, code lost:
        r1.close();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<com.tencent.assistant.plugin.PluginDownloadInfo> a() {
        /*
            r5 = this;
            r1 = 0
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            com.tencent.assistant.db.helper.SqliteHelper r0 = r5.getHelper()
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r0 = r0.getReadableDatabaseWrapper()
            java.lang.String r3 = "select * from plugin_downloadinfos order by plugin_displayDrder asc"
            r4 = 0
            android.database.Cursor r1 = r0.rawQuery(r3, r4)     // Catch:{ Exception -> 0x0030 }
            if (r1 == 0) goto L_0x002a
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x0030 }
            if (r0 == 0) goto L_0x002a
        L_0x001d:
            com.tencent.assistant.plugin.PluginDownloadInfo r0 = r5.a(r1)     // Catch:{ Exception -> 0x0030 }
            r2.add(r0)     // Catch:{ Exception -> 0x0030 }
            boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x0030 }
            if (r0 != 0) goto L_0x001d
        L_0x002a:
            if (r1 == 0) goto L_0x002f
        L_0x002c:
            r1.close()
        L_0x002f:
            return r2
        L_0x0030:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0037 }
            if (r1 == 0) goto L_0x002f
            goto L_0x002c
        L_0x0037:
            r0 = move-exception
            if (r1 == 0) goto L_0x003d
            r1.close()
        L_0x003d:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.s.a():java.util.List");
    }

    public void a(int i) {
        getHelper().getWritableDatabaseWrapper().delete("plugin_downloadinfos", "plugin_id = ?", new String[]{String.valueOf(i)});
    }

    public PluginDownloadInfo a(Cursor cursor) {
        PluginDownloadInfo pluginDownloadInfo = new PluginDownloadInfo();
        if (cursor != null) {
            pluginDownloadInfo.pluginId = cursor.getInt(cursor.getColumnIndexOrThrow("plugin_id"));
            pluginDownloadInfo.downloadTicket = cursor.getString(cursor.getColumnIndexOrThrow("plugin_downloadTicket"));
            pluginDownloadInfo.pluginPackageName = cursor.getString(cursor.getColumnIndexOrThrow("plugin_packagename"));
            pluginDownloadInfo.version = cursor.getInt(cursor.getColumnIndexOrThrow("plugin_version"));
            pluginDownloadInfo.name = cursor.getString(cursor.getColumnIndexOrThrow("plugin_name"));
            pluginDownloadInfo.displayOrder = cursor.getInt(cursor.getColumnIndexOrThrow("plugin_displayDrder"));
            pluginDownloadInfo.desc = cursor.getString(cursor.getColumnIndexOrThrow("plugin_desc"));
            pluginDownloadInfo.iconUrl = cursor.getString(cursor.getColumnIndexOrThrow("plugin_iconUrl"));
            pluginDownloadInfo.imgUrl = cursor.getString(cursor.getColumnIndexOrThrow("plugin_imgUrl"));
            pluginDownloadInfo.fileSize = cursor.getLong(cursor.getColumnIndexOrThrow("plugin_fileSize"));
            pluginDownloadInfo.minApiLevel = cursor.getInt(cursor.getColumnIndexOrThrow("plugin_minApiLevel"));
            pluginDownloadInfo.minPluginVersion = cursor.getInt(cursor.getColumnIndexOrThrow("plugin_minPluginVersion"));
            pluginDownloadInfo.minBaoVersion = cursor.getInt(cursor.getColumnIndexOrThrow("plugin_minBaoVersion"));
            pluginDownloadInfo.needPreDownload = cursor.getInt(cursor.getColumnIndexOrThrow("plugin_needPreDownload"));
            pluginDownloadInfo.downUrl = cursor.getString(cursor.getColumnIndexOrThrow("plugin_downUrl"));
            pluginDownloadInfo.startActivity = cursor.getString(cursor.getColumnIndexOrThrow("plugin_startActivity"));
            pluginDownloadInfo.pluginForBaoType = cursor.getInt(cursor.getColumnIndexOrThrow("plugin_for_bao_type"));
            pluginDownloadInfo.type = cursor.getInt(cursor.getColumnIndexOrThrow(SocialConstants.PARAM_TYPE));
            pluginDownloadInfo.actionUrl = cursor.getString(cursor.getColumnIndexOrThrow("actionUrl"));
        }
        return pluginDownloadInfo;
    }

    public void a(ContentValues contentValues, PluginDownloadInfo pluginDownloadInfo) {
        if (pluginDownloadInfo != null) {
            contentValues.put("plugin_id", Integer.valueOf(pluginDownloadInfo.pluginId));
            contentValues.put("plugin_downloadTicket", pluginDownloadInfo.downloadTicket);
            contentValues.put("plugin_packagename", pluginDownloadInfo.pluginPackageName);
            contentValues.put("plugin_version", Integer.valueOf(pluginDownloadInfo.version));
            contentValues.put("plugin_name", pluginDownloadInfo.name);
            contentValues.put("plugin_displayDrder", Integer.valueOf(pluginDownloadInfo.displayOrder));
            contentValues.put("plugin_desc", pluginDownloadInfo.desc);
            contentValues.put("plugin_iconUrl", pluginDownloadInfo.iconUrl);
            contentValues.put("plugin_imgUrl", pluginDownloadInfo.imgUrl);
            contentValues.put("plugin_fileSize", Long.valueOf(pluginDownloadInfo.fileSize));
            contentValues.put("plugin_minApiLevel", Integer.valueOf(pluginDownloadInfo.minApiLevel));
            contentValues.put("plugin_minPluginVersion", Integer.valueOf(pluginDownloadInfo.minPluginVersion));
            contentValues.put("plugin_minBaoVersion", Integer.valueOf(pluginDownloadInfo.minBaoVersion));
            contentValues.put("plugin_needPreDownload", Integer.valueOf(pluginDownloadInfo.needPreDownload));
            contentValues.put("plugin_downUrl", pluginDownloadInfo.downUrl);
            contentValues.put("plugin_startActivity", pluginDownloadInfo.startActivity);
            contentValues.put("plugin_for_bao_type", Integer.valueOf(pluginDownloadInfo.pluginForBaoType));
            contentValues.put(SocialConstants.PARAM_TYPE, Integer.valueOf(pluginDownloadInfo.type));
            contentValues.put("actionUrl", pluginDownloadInfo.actionUrl);
        }
    }

    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return "plugin_downloadinfos";
    }

    public String createTableSQL() {
        return "CREATE TABLE if not exists plugin_downloadinfos (row_id INTEGER PRIMARY KEY AUTOINCREMENT,plugin_id INTEGER UNIQUE,plugin_downloadTicket TEXT UINQUE,plugin_packagename TEXT,plugin_version INTEGER,plugin_displayDrder INTEGER,plugin_name TEXT,plugin_desc TEXT,plugin_iconUrl TEXT,plugin_imgUrl TEXT,plugin_fileSize INTEGER,plugin_minApiLevel INTEGER,plugin_minPluginVersion INTEGER,plugin_minBaoVersion INTEGER,plugin_needPreDownload INTEGER,plugin_for_bao_type INTEGER,plugin_downUrl TEXT,plugin_startActivity TEXT,type INTEGER,actionUrl TEXT);";
    }

    public String[] getAlterSQL(int i, int i2) {
        if (i2 == 4) {
            return new String[]{"CREATE TABLE if not exists plugin_downloadinfos (row_id INTEGER PRIMARY KEY AUTOINCREMENT,plugin_id INTEGER UNIQUE,plugin_downloadTicket TEXT UINQUE,plugin_packagename TEXT,plugin_version INTEGER,plugin_displayDrder INTEGER,plugin_name TEXT,plugin_desc TEXT,plugin_iconUrl TEXT,plugin_imgUrl TEXT,plugin_fileSize INTEGER,plugin_minApiLevel INTEGER,plugin_minPluginVersion INTEGER,plugin_minBaoVersion INTEGER,plugin_needPreDownload INTEGER,plugin_for_bao_type INTEGER,plugin_downUrl TEXT,plugin_startActivity TEXT,type INTEGER,actionUrl TEXT);"};
        } else if (i != 10 || i2 != 11) {
            return null;
        } else {
            return new String[]{"alter table plugin_downloadinfos add column type INTEGER;", "alter table plugin_downloadinfos add column actionUrl TEXT;"};
        }
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public SqliteHelper getHelper() {
        return DownloadDbHelper.get(AstApp.i());
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }
}
