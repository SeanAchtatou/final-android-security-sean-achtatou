package com.tencent.assistant.db.table;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.helper.AstDbHelper;
import com.tencent.assistant.db.helper.SQLiteDatabaseWrapper;
import com.tencent.assistant.db.helper.SqliteHelper;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class d implements IBaseTable {
    public d() {
    }

    public d(Context context) {
    }

    public boolean a(String str, String str2, String str3) {
        return a(str, str2, str3, null);
    }

    public boolean a(String str, String str2, String str3, byte[] bArr) {
        SQLiteDatabaseWrapper writableDatabaseWrapper = getHelper().getWritableDatabaseWrapper();
        ContentValues contentValues = new ContentValues();
        contentValues.put("value", str3);
        if (bArr != null) {
            contentValues.put("data", bArr);
        }
        if (writableDatabaseWrapper.update("setting", contentValues, "uin=? and key=?", new String[]{str, str2}) > 0) {
            return true;
        }
        return false;
    }

    public boolean a(String str, String str2) {
        return a(Constants.STR_EMPTY, str, str2);
    }

    public boolean b(String str, String str2, String str3) {
        return b(str, str2, str3, null);
    }

    public boolean b(String str, String str2, String str3, byte[] bArr) {
        SQLiteDatabaseWrapper writableDatabaseWrapper = getHelper().getWritableDatabaseWrapper();
        ContentValues contentValues = new ContentValues();
        contentValues.put("uin", str);
        contentValues.put("key", str2);
        contentValues.put("value", str3);
        if (bArr != null) {
            contentValues.put("data", bArr);
        }
        return writableDatabaseWrapper.insert("setting", null, contentValues) > 0;
    }

    public boolean b(String str, String str2) {
        return b(Constants.STR_EMPTY, str, str2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String c(java.lang.String r10, java.lang.String r11) {
        /*
            r9 = this;
            r8 = 0
            com.tencent.assistant.db.helper.SqliteHelper r0 = r9.getHelper()
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r0 = r0.getReadableDatabaseWrapper()
            java.lang.String r1 = "setting"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x003f, all -> 0x0038 }
            r3 = 0
            java.lang.String r4 = "value"
            r2[r3] = r4     // Catch:{ Exception -> 0x003f, all -> 0x0038 }
            java.lang.String r3 = "uin=? and key=?"
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x003f, all -> 0x0038 }
            r5 = 0
            r4[r5] = r10     // Catch:{ Exception -> 0x003f, all -> 0x0038 }
            r5 = 1
            r4[r5] = r11     // Catch:{ Exception -> 0x003f, all -> 0x0038 }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x003f, all -> 0x0038 }
            if (r1 == 0) goto L_0x0049
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x0052, all -> 0x004f }
            if (r0 == 0) goto L_0x0049
            r0 = 0
            java.lang.String r0 = r1.getString(r0)     // Catch:{ Exception -> 0x0052, all -> 0x004f }
            if (r1 == 0) goto L_0x0037
            r1.close()
        L_0x0037:
            return r0
        L_0x0038:
            r0 = move-exception
        L_0x0039:
            if (r8 == 0) goto L_0x003e
            r8.close()
        L_0x003e:
            throw r0
        L_0x003f:
            r0 = move-exception
            r0 = r8
        L_0x0041:
            if (r0 == 0) goto L_0x0046
            r0.close()
        L_0x0046:
            java.lang.String r0 = ""
            goto L_0x0037
        L_0x0049:
            if (r1 == 0) goto L_0x0046
            r1.close()
            goto L_0x0046
        L_0x004f:
            r0 = move-exception
            r8 = r1
            goto L_0x0039
        L_0x0052:
            r0 = move-exception
            r0 = r1
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.d.c(java.lang.String, java.lang.String):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte[] d(java.lang.String r10, java.lang.String r11) {
        /*
            r9 = this;
            r8 = 0
            com.tencent.assistant.db.helper.SqliteHelper r0 = r9.getHelper()
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r0 = r0.getReadableDatabaseWrapper()
            java.lang.String r1 = "setting"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x003f, all -> 0x0038 }
            r3 = 0
            java.lang.String r4 = "data"
            r2[r3] = r4     // Catch:{ Exception -> 0x003f, all -> 0x0038 }
            java.lang.String r3 = "uin=? and key=?"
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x003f, all -> 0x0038 }
            r5 = 0
            r4[r5] = r10     // Catch:{ Exception -> 0x003f, all -> 0x0038 }
            r5 = 1
            r4[r5] = r11     // Catch:{ Exception -> 0x003f, all -> 0x0038 }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x003f, all -> 0x0038 }
            if (r1 == 0) goto L_0x0048
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x0051, all -> 0x004e }
            if (r0 == 0) goto L_0x0048
            r0 = 0
            byte[] r0 = r1.getBlob(r0)     // Catch:{ Exception -> 0x0051, all -> 0x004e }
            if (r1 == 0) goto L_0x0037
            r1.close()
        L_0x0037:
            return r0
        L_0x0038:
            r0 = move-exception
        L_0x0039:
            if (r8 == 0) goto L_0x003e
            r8.close()
        L_0x003e:
            throw r0
        L_0x003f:
            r0 = move-exception
            r0 = r8
        L_0x0041:
            if (r0 == 0) goto L_0x0046
            r0.close()
        L_0x0046:
            r0 = r8
            goto L_0x0037
        L_0x0048:
            if (r1 == 0) goto L_0x0046
            r1.close()
            goto L_0x0046
        L_0x004e:
            r0 = move-exception
            r8 = r1
            goto L_0x0039
        L_0x0051:
            r0 = move-exception
            r0 = r1
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.d.d(java.lang.String, java.lang.String):byte[]");
    }

    public String a(String str) {
        return c(Constants.STR_EMPTY, str);
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0067  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.util.Map<java.lang.String, java.lang.String> r10) {
        /*
            r9 = this;
            r8 = 0
            com.tencent.assistant.db.helper.SqliteHelper r0 = r9.getHelper()
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r0 = r0.getReadableDatabaseWrapper()
            java.lang.String r1 = "setting"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x005b, all -> 0x0063 }
            r3 = 0
            java.lang.String r4 = "uin, key, value"
            r2[r3] = r4     // Catch:{ Exception -> 0x005b, all -> 0x0063 }
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x005b, all -> 0x0063 }
            if (r1 == 0) goto L_0x0055
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x006d }
            if (r0 == 0) goto L_0x0055
        L_0x0024:
            r0 = 0
            java.lang.String r0 = r1.getString(r0)     // Catch:{ Exception -> 0x006d }
            r2 = 1
            java.lang.String r2 = r1.getString(r2)     // Catch:{ Exception -> 0x006d }
            r3 = 2
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x006d }
            if (r3 == 0) goto L_0x004f
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x006d }
            r4.<init>()     // Catch:{ Exception -> 0x006d }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ Exception -> 0x006d }
            java.lang.String r4 = "_"
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ Exception -> 0x006d }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x006d }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x006d }
            r10.put(r0, r3)     // Catch:{ Exception -> 0x006d }
        L_0x004f:
            boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x006d }
            if (r0 != 0) goto L_0x0024
        L_0x0055:
            if (r1 == 0) goto L_0x005a
        L_0x0057:
            r1.close()
        L_0x005a:
            return
        L_0x005b:
            r0 = move-exception
            r1 = r8
        L_0x005d:
            r0.printStackTrace()     // Catch:{ all -> 0x006b }
            if (r1 == 0) goto L_0x005a
            goto L_0x0057
        L_0x0063:
            r0 = move-exception
            r1 = r8
        L_0x0065:
            if (r1 == 0) goto L_0x006a
            r1.close()
        L_0x006a:
            throw r0
        L_0x006b:
            r0 = move-exception
            goto L_0x0065
        L_0x006d:
            r0 = move-exception
            goto L_0x005d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.d.a(java.util.Map):void");
    }

    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return "setting";
    }

    public String createTableSQL() {
        return "CREATE TABLE if not exists setting (_id INTEGER PRIMARY KEY AUTOINCREMENT,uin TEXT,key TEXT,value TEXT,data BLOB);";
    }

    public String[] getAlterSQL(int i, int i2) {
        if (i != 6 || i2 != 7) {
            return null;
        }
        return new String[]{"alter table setting add column data BLOB;"};
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public SqliteHelper getHelper() {
        return AstDbHelper.get(AstApp.i());
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }
}
