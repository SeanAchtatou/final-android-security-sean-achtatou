package com.tencent.assistant.manager;

import android.content.Context;
import android.os.Message;
import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.a.a;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.model.l;
import com.tencent.nucleus.socialcontact.login.j;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class u extends c implements UIEventListener {
    public static final String e = ("tmast://appdetails?appid=10910&pname=com.tencent.mm&appname=微信&oplist=0&versioncode=0&" + a.D + "=1");
    public static ArrayList<String> f = new ArrayList<>();
    private static u g;
    private Message h;
    private final int i = 350;
    private List<Long> j = new ArrayList();
    private int k = -1;

    static {
        f.add("HTC One X");
        f.add("HTC One");
        f.add("HTC X720d");
        f.add("HTC 802t");
        f.add("HTC S720e");
    }

    public static synchronized u b() {
        u uVar;
        synchronized (u.class) {
            if (g == null) {
                g = new u();
            }
            uVar = g;
        }
        return uVar;
    }

    private u() {
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_FAIL, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL, this);
    }

    public int a(Context context) {
        j.a().b(AppConst.IdentityType.WX);
        this.k = a();
        return this.k;
    }

    /* access modifiers changed from: protected */
    public void a(int i2, int i3, int i4) {
        l lVar = (l) this.c.remove(Integer.valueOf(i2));
        if (lVar != null) {
            Message obtainMessage = AstApp.i().j().obtainMessage();
            obtainMessage.what = EventDispatcherEnum.UI_EVENT_WX_WRITE_TOKEN_SUCCESS;
            obtainMessage.arg1 = i4;
            obtainMessage.arg2 = lVar.c;
            obtainMessage.obj = lVar;
            AstApp.i().j().sendMessage(obtainMessage);
        }
    }

    /* access modifiers changed from: protected */
    public void a(int i2, int i3) {
        l lVar = (l) this.c.remove(Integer.valueOf(i2));
        if (lVar != null) {
            if (lVar.b > 3 || lVar.f948a == null) {
                j.a().f();
                Message obtainMessage = AstApp.i().j().obtainMessage();
                obtainMessage.what = EventDispatcherEnum.UI_EVENT_WX_WRITE_TOKEN_FAIL;
                obtainMessage.obj = lVar;
                obtainMessage.arg2 = lVar.c;
                AstApp.i().j().sendMessage(obtainMessage);
                return;
            }
            int a2 = this.b.a(lVar.f948a);
            lVar.b++;
            this.c.put(Integer.valueOf(a2), lVar);
        }
    }

    public Message c() {
        return this.h;
    }

    public void a(Message message) {
        this.h = message;
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS:
                if (message.arg1 == AppConst.LoginEgnineType.ENGINE_WX.ordinal() && this.k != -1) {
                    Message obtainMessage = AstApp.i().j().obtainMessage();
                    obtainMessage.what = EventDispatcherEnum.UI_EVENT_WX_AUTH_SUCCESS;
                    obtainMessage.arg2 = this.k;
                    this.k = -1;
                    this.h = obtainMessage;
                    AstApp.i().j().sendMessage(obtainMessage);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_LOGIN_FAIL:
                if (message.arg1 == AppConst.LoginEgnineType.ENGINE_WX.ordinal() && this.k != -1) {
                    Message obtainMessage2 = AstApp.i().j().obtainMessage();
                    obtainMessage2.what = EventDispatcherEnum.UI_EVENT_WX_AUTH_FAIL;
                    obtainMessage2.arg2 = this.k;
                    this.k = -1;
                    this.h = obtainMessage2;
                    AstApp.i().j().sendMessage(obtainMessage2);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL:
                if (message.arg1 == AppConst.LoginEgnineType.ENGINE_WX.ordinal() && this.k != -1) {
                    Message obtainMessage3 = AstApp.i().j().obtainMessage();
                    obtainMessage3.what = EventDispatcherEnum.UI_EVENT_WX_AUTH_CANCEL;
                    obtainMessage3.arg2 = this.k;
                    this.k = -1;
                    this.h = obtainMessage3;
                    AstApp.i().j().sendMessage(obtainMessage3);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
