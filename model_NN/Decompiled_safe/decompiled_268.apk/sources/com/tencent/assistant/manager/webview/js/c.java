package com.tencent.assistant.manager.webview.js;

import com.tencent.assistant.b.b;
import com.tencent.assistant.protocol.jce.LbsCell;
import com.tencent.assistant.protocol.jce.LbsData;
import com.tencent.assistant.protocol.jce.LbsLocation;
import com.tencent.assistant.protocol.jce.LbsWifiMac;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: ProGuard */
class c implements b {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ JsBridge f924a;

    c(JsBridge jsBridge) {
        this.f924a = jsBridge;
    }

    public void a(int i, LbsData lbsData) {
        if (this.f924a.mLBSDataBundle != null) {
            int i2 = this.f924a.mLBSDataBundle.getInt("seqid");
            String string = this.f924a.mLBSDataBundle.getString("method");
            String string2 = this.f924a.mLBSDataBundle.getString("function");
            if (lbsData == null) {
                this.f924a.responseFail(string2, i2, string, -1);
                return;
            }
            LbsLocation a2 = lbsData.a();
            JSONObject jSONObject = new JSONObject();
            JSONObject jSONObject2 = new JSONObject();
            if (a2 != null) {
                try {
                    jSONObject2.put("accuracy", a2.d);
                    jSONObject2.put("altitude", a2.c);
                    jSONObject2.put("bearing", a2.e);
                    jSONObject2.put("latitude", a2.f1401a);
                    jSONObject2.put("longitude", a2.b);
                    jSONObject2.put("speed", a2.f);
                    jSONObject2.put("time", a2.g);
                } catch (Exception e) {
                    this.f924a.responseFail(string2, i2, string, -3);
                    return;
                }
            }
            jSONObject.put("location", jSONObject2);
            if (lbsData.b() != null) {
                JSONArray jSONArray = new JSONArray();
                JSONObject jSONObject3 = new JSONObject();
                Iterator<LbsCell> it = lbsData.b().iterator();
                while (it.hasNext()) {
                    LbsCell next = it.next();
                    jSONObject3.put("cellid", next.d());
                    jSONObject3.put("mcc", next.a());
                    jSONObject3.put("mnc", next.b());
                    jSONObject3.put("lac", next.c());
                    jSONObject3.put("rssi", next.e());
                    jSONArray.put(jSONObject3);
                }
                jSONObject.put("cells", jSONArray);
            }
            if (lbsData.c() != null) {
                JSONArray jSONArray2 = new JSONArray();
                JSONObject jSONObject4 = new JSONObject();
                Iterator<LbsWifiMac> it2 = lbsData.c().iterator();
                while (it2.hasNext()) {
                    LbsWifiMac next2 = it2.next();
                    jSONObject4.put("mac", next2.a());
                    jSONObject4.put("rssi", next2.b());
                    jSONArray2.put(jSONObject4);
                }
                jSONObject.put("wifis", jSONArray2);
            }
            this.f924a.response(string2, i2, string, jSONObject.toString());
        }
    }
}
