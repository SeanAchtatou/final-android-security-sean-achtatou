package com.tencent.assistant.manager.webview.js;

import com.tencent.assistant.utils.bm;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;

/* compiled from: ProGuard */
class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ f f928a;

    g(f fVar) {
        this.f928a = fVar;
    }

    public void run() {
        try {
            bm.c(this.f928a.f927a.getQueryParameter("appid"));
            long c = bm.c(this.f928a.f927a.getQueryParameter("apkid"));
            String queryParameter = this.f928a.f927a.getQueryParameter("packagename");
            int d = bm.d(this.f928a.f927a.getQueryParameter("versioncode"));
            int d2 = bm.d(this.f928a.f927a.getQueryParameter("grayversioncode"));
            DownloadInfo downloadInfo = null;
            if (c > 0) {
                downloadInfo = DownloadProxy.a().d(String.valueOf(c));
            }
            if (downloadInfo == null) {
                downloadInfo = DownloadProxy.a().a(queryParameter, d, d2);
            }
            if (downloadInfo != null) {
                DownloadProxy.a().b(downloadInfo.downloadTicket);
                this.f928a.e.response(this.f928a.b, this.f928a.c, this.f928a.d, String.valueOf(0));
                return;
            }
            this.f928a.e.responseFail(this.f928a.b, this.f928a.c, this.f928a.d, -2);
        } catch (Exception e) {
            this.f928a.e.responseFail(this.f928a.b, this.f928a.c, this.f928a.d, -3);
        }
    }
}
