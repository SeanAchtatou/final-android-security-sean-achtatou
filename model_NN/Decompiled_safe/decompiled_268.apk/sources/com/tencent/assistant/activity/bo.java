package com.tencent.assistant.activity;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class bo implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PanelManagerActivity f426a;

    bo(PanelManagerActivity panelManagerActivity) {
        this.f426a = panelManagerActivity;
    }

    public void onClick(View view) {
        l.a(new STInfoV2(this.f426a.f(), this.f426a.n, this.f426a.m(), STConst.ST_DEFAULT_SLOT, 200));
        this.f426a.startActivity(new Intent("com.tencent.qlauncher.action.ThemeMarket"));
    }
}
