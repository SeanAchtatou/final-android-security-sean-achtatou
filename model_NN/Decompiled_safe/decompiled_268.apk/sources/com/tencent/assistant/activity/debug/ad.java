package com.tencent.assistant.activity.debug;

import android.view.View;
import com.tencent.assistant.db.table.t;
import com.tencent.assistant.plugin.PluginInfo;
import java.util.List;

/* compiled from: ProGuard */
class ad implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DActivity f477a;

    ad(DActivity dActivity) {
        this.f477a = dActivity;
    }

    public void onClick(View view) {
        t tVar = new t();
        List<PluginInfo> a2 = tVar.a();
        if (a2 != null && a2.size() > 0) {
            for (PluginInfo packageName : a2) {
                tVar.b(packageName.getPackageName());
            }
        }
    }
}
