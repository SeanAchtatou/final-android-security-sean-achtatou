package com.tencent.assistant.activity;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.BackupApp;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.pangu.download.a;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
class aj implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ArrayList f395a;
    final /* synthetic */ ai b;

    aj(ai aiVar, ArrayList arrayList) {
        this.b = aiVar;
        this.f395a = arrayList;
    }

    public void run() {
        ArrayList<SimpleAppModel> c = k.c((ArrayList<BackupApp>) this.f395a);
        if (c != null && c.size() > 0) {
            Iterator<SimpleAppModel> it = c.iterator();
            while (it.hasNext()) {
                SimpleAppModel next = it.next();
                AppConst.AppState d = k.d(next);
                if (d == null || !(d == AppConst.AppState.INSTALLED || d == AppConst.AppState.INSTALLING)) {
                    a.a().a(next, STInfoBuilder.buildDownloadSTInfo(this.b.f394a.af, next));
                }
            }
        }
    }
}
