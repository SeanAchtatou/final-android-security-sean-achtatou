package com.tencent.assistant.sdk;

import android.content.Context;
import android.text.TextUtils;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.sdk.param.jce.BatchDownloadActionRequest;
import com.tencent.assistant.sdk.param.jce.BatchDownloadActionResponse;
import com.tencent.assistant.sdk.param.jce.IPCBaseParam;
import com.tencent.assistant.sdk.param.jce.IPCDownloadParam;
import com.tencent.assistant.sdk.param.jce.IPCRequest;
import com.tencent.pangu.download.DownloadInfo;

/* compiled from: ProGuard */
public class r extends p {
    protected int k;
    protected BatchDownloadActionRequest l;
    protected BatchDownloadActionResponse m;

    public r(Context context, IPCRequest iPCRequest) {
        super(context, iPCRequest);
        this.e = 1;
    }

    public r(Context context, IPCRequest iPCRequest, String str) {
        super(context, iPCRequest, str);
        this.e = 1;
    }

    /* access modifiers changed from: protected */
    public void a(JceStruct jceStruct) {
        this.k = -1;
        if (jceStruct instanceof BatchDownloadActionRequest) {
            this.l = (BatchDownloadActionRequest) jceStruct;
            if (this.l != null) {
                this.k = this.l.f1658a;
                this.f = this.l.b;
                this.h = this.l.c;
                this.i = this.l.d;
                this.j = this.l.e;
            }
        }
    }

    /* access modifiers changed from: protected */
    public JceStruct a() {
        return null;
    }

    /* access modifiers changed from: protected */
    public IPCBaseParam b() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public boolean a(DownloadInfo downloadInfo) {
        IPCBaseParam iPCBaseParam;
        if (this.k == 1 || this.k == 2 || this.k == 3) {
            return true;
        }
        if (this.k != 4) {
            return false;
        }
        int i = 0;
        while (true) {
            if (i >= (this.f == null ? 0 : this.f.size())) {
                return false;
            }
            IPCDownloadParam iPCDownloadParam = (IPCDownloadParam) this.f.get(i);
            if (iPCDownloadParam != null && (iPCBaseParam = iPCDownloadParam.f1664a) != null && !TextUtils.isEmpty(iPCBaseParam.d) && iPCBaseParam.d.equals(downloadInfo.packageName)) {
                return true;
            }
            i++;
        }
    }
}
