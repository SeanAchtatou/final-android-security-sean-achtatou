package com.tencent.assistant.smartcard.d;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.SmartCardPicDownloadNode;
import com.tencent.assistant.protocol.jce.SmartCardPicNode;
import com.tencent.assistant.protocol.jce.SmartCardPicTemplate;
import com.tencent.assistant.protocol.jce.SmartCardTitle;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class g extends a {
    public String e = Constants.STR_EMPTY;
    public int f;
    public int g;
    private List<SimpleAppModel> h;
    private List<SmartCardPicNode> i;

    public boolean a(byte b, JceStruct jceStruct) {
        if (!(jceStruct instanceof SmartCardPicTemplate)) {
            return false;
        }
        SmartCardPicTemplate smartCardPicTemplate = (SmartCardPicTemplate) jceStruct;
        SmartCardTitle smartCardTitle = smartCardPicTemplate.b;
        this.j = b;
        if (smartCardTitle != null) {
            this.l = smartCardTitle.b;
            this.p = smartCardTitle.c;
            this.o = smartCardTitle.d;
        }
        this.n = smartCardPicTemplate.c;
        this.k = smartCardPicTemplate.f1514a;
        this.i = smartCardPicTemplate.d;
        if (this.h == null) {
            this.h = new ArrayList();
        } else {
            this.h.clear();
        }
        Iterator<SmartCardPicDownloadNode> it = smartCardPicTemplate.e.iterator();
        while (it.hasNext()) {
            new SimpleAppModel();
            this.h.add(k.a(it.next().f1512a));
        }
        this.k = smartCardPicTemplate.f1514a;
        this.f = smartCardPicTemplate.i;
        this.g = smartCardPicTemplate.j;
        this.e = smartCardPicTemplate.f;
        return true;
    }

    public x c() {
        if (this.f <= 0 || this.g <= 0) {
            return null;
        }
        x xVar = new x();
        xVar.f = this.k;
        xVar.e = this.j;
        xVar.f1767a = this.f;
        xVar.b = this.g;
        return xVar;
    }

    public List<SimpleAppModel> a() {
        return this.h;
    }

    public List<SimpleAppModel> g() {
        return this.h;
    }
}
