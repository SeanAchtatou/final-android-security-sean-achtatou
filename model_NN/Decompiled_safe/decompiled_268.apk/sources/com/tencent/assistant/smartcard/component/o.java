package com.tencent.assistant.smartcard.component;

import android.os.Bundle;
import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.st.page.STPageInfo;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class o implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ NormalSmartcardBaseItem f1732a;

    o(NormalSmartcardBaseItem normalSmartcardBaseItem) {
        this.f1732a = normalSmartcardBaseItem;
    }

    public void onClick(View view) {
        int i;
        STPageInfo n;
        Bundle bundle = new Bundle();
        if (!(this.f1732a.f1693a instanceof BaseActivity) || (n = ((BaseActivity) this.f1732a.f1693a).n()) == null) {
            i = 0;
        } else {
            i = n.f2060a;
        }
        bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.f1732a.c(i));
        b.b(this.f1732a.f1693a, this.f1732a.d.o, bundle);
        this.f1732a.a("03_001", 200, this.f1732a.d.s, -1);
    }
}
