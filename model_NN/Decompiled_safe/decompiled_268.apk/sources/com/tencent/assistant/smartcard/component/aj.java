package com.tencent.assistant.smartcard.component;

import android.content.Context;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.smartcard.d.ad;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class aj extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchSmartCardYuyiTagItem f1711a;
    private Context b;
    private ad c;

    public aj(SearchSmartCardYuyiTagItem searchSmartCardYuyiTagItem, Context context, ad adVar) {
        this.f1711a = searchSmartCardYuyiTagItem;
        this.b = context;
        this.c = adVar;
    }

    public void onTMAClick(View view) {
        if (this.c != null) {
            b.a(this.f1711a.getContext(), this.c.o);
        }
    }

    public STInfoV2 getStInfo(View view) {
        return null;
    }
}
