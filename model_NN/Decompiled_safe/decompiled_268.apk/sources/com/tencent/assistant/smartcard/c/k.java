package com.tencent.assistant.smartcard.c;

import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.d.p;
import com.tencent.assistant.smartcard.d.q;
import com.tencent.assistant.smartcard.d.w;
import com.tencent.assistant.smartcard.d.x;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class k extends z {
    public boolean a(n nVar, List<Long> list) {
        if (nVar == null || nVar.j != 5) {
            return false;
        }
        return a((p) nVar, (w) this.f1692a.get(Integer.valueOf(nVar.j)), (x) this.b.get(Integer.valueOf(nVar.j)), list);
    }

    private boolean a(p pVar, w wVar, x xVar, List<Long> list) {
        if (xVar == null) {
            return false;
        }
        pVar.a(list);
        if (wVar == null) {
            wVar = new w();
            wVar.f = pVar.k;
            wVar.e = pVar.j;
            this.f1692a.put(Integer.valueOf(wVar.e), wVar);
        }
        if (wVar.b >= xVar.b) {
            a(pVar.t, pVar.k + "||" + pVar.j + "|" + 1, pVar.j);
            return false;
        } else if (wVar.f1766a >= xVar.f1767a) {
            a(pVar.t, pVar.k + "||" + pVar.j + "|" + 2, pVar.j);
            return false;
        } else {
            a(pVar);
            if (pVar.c != null && pVar.c.size() >= xVar.g) {
                return true;
            }
            a(pVar.t, pVar.k + "||" + pVar.j + "|" + 3, pVar.j);
            return false;
        }
    }

    private void a(p pVar) {
        if (pVar != null && pVar.c != null && pVar.c.size() != 0) {
            ArrayList arrayList = new ArrayList();
            for (q next : pVar.c) {
                if (next.f1760a == null || ApkResourceManager.getInstance().getInstalledApkInfo(next.f1760a.c) != null) {
                    arrayList.add(next);
                }
            }
            pVar.c.removeAll(arrayList);
        }
    }

    public void a(n nVar) {
        if (nVar != null && nVar.j == 5) {
            p pVar = (p) nVar;
            x xVar = (x) this.b.get(Integer.valueOf(pVar.j));
            if (xVar != null) {
                pVar.q = xVar.d;
                pVar.d = xVar.g;
                pVar.e = pVar.c.size();
            }
        }
    }
}
