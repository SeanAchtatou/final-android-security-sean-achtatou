package com.tencent.assistant.smartcard.component;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.d.z;
import com.tencent.assistant.st.i;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.st.b.e;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public class NormalSmartCardAppVerticalItem extends NormalSmartcardBaseItem {
    private TextView i;
    private LinearLayout l;

    public NormalSmartCardAppVerticalItem(Context context) {
        super(context);
    }

    public NormalSmartCardAppVerticalItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NormalSmartCardAppVerticalItem(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        super(context, nVar, asVar, iViewInvalidater);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.f) {
            this.f = true;
            d();
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        int i2;
        this.c = this.b.inflate((int) R.layout.smartcard_applist, this);
        this.i = (TextView) this.c.findViewById(R.id.card_title);
        this.l = (LinearLayout) this.c.findViewById(R.id.card_list);
        z zVar = (z) this.d;
        if (zVar == null || zVar.a() == null) {
            i2 = 0;
        } else {
            i2 = zVar.a().size();
        }
        if (i2 > 3) {
            i2 = 3;
        }
        for (int i3 = 0; i3 < i2; i3++) {
            f();
        }
        setClickable(false);
        g();
    }

    private void f() {
        NormalSmartCardAppNode normalSmartCardAppNode = new NormalSmartCardAppNode(this.f1693a);
        normalSmartCardAppNode.setMinimumHeight(by.a(this.f1693a, 87.0f));
        this.l.addView(normalSmartCardAppNode);
    }

    private void g() {
        int i2;
        z zVar = (z) this.d;
        if (zVar == null || zVar.a() == null || zVar.a().size() == 0) {
            a(8);
            setBackgroundResource(17170445);
            setPadding(0, 0, 0, 0);
            return;
        }
        a(0);
        this.i.setText(zVar.k());
        if (zVar.b()) {
            try {
                this.i.setBackgroundResource(R.drawable.common_index_tag);
            } catch (Throwable th) {
                t.a().b();
            }
            this.i.setTextColor(getResources().getColor(R.color.smart_card_applist_title_font_color));
        } else {
            this.i.setBackgroundResource(0);
            this.i.setTextColor(getResources().getColor(R.color.apk_name_v5));
        }
        int size = zVar.a().size();
        if (size > 3) {
            i2 = 3;
        } else {
            i2 = size;
        }
        int childCount = this.l.getChildCount();
        if (childCount < i2) {
            for (int i3 = 0; i3 < i2 - childCount; i3++) {
                f();
            }
        }
        int childCount2 = this.l.getChildCount();
        for (int i4 = 0; i4 < childCount2; i4++) {
            NormalSmartCardAppNode normalSmartCardAppNode = (NormalSmartCardAppNode) this.l.getChildAt(i4);
            if (normalSmartCardAppNode != null) {
                if (i4 < i2) {
                    a(zVar, i4);
                    normalSmartCardAppNode.setVisibility(0);
                } else {
                    normalSmartCardAppNode.setVisibility(8);
                }
            }
        }
        int i5 = 0;
        while (i5 < i2) {
            ((NormalSmartCardAppNode) this.l.getChildAt(i5)).a(zVar.a().get(i5).f1768a, zVar.a().get(i5).a(), a(zVar, i5), c(a(this.f1693a)), i5 < i2 + -1, ListItemInfoView.InfoType.CATEGORY_SIZE);
            i5++;
        }
    }

    private STInfoV2 a(z zVar, int i2) {
        STInfoV2 a2 = a(i.a(zVar, i2 + 1), 100);
        if (!(a2 == null || zVar == null || zVar.a() == null || zVar.a().size() <= i2)) {
            a2.updateWithSimpleAppModel(zVar.a().get(i2).f1768a);
        }
        if (this.h == null) {
            this.h = new e();
        }
        this.h.a(a2);
        return a2;
    }

    private void a(int i2) {
        this.c.setVisibility(i2);
        this.i.setVisibility(i2);
        this.l.setVisibility(i2);
    }

    /* access modifiers changed from: protected */
    public void b() {
        g();
    }
}
