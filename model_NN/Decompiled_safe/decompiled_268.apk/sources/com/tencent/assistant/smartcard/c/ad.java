package com.tencent.assistant.smartcard.c;

import com.tencent.assistant.smartcard.d.aa;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.d.v;
import com.tencent.assistant.smartcard.d.w;
import com.tencent.assistant.smartcard.d.x;
import java.util.List;

/* compiled from: ProGuard */
public class ad extends z {
    public boolean a(n nVar, List<Long> list) {
        if (nVar == null || !(nVar instanceof aa)) {
            return false;
        }
        aa aaVar = (aa) nVar;
        return a(aaVar, (w) this.f1692a.get(Integer.valueOf(aaVar.l())), (x) this.b.get(Integer.valueOf(aaVar.l())));
    }

    /* access modifiers changed from: protected */
    public boolean a(aa aaVar, w wVar, x xVar) {
        if (xVar == null) {
            return false;
        }
        if (wVar == null) {
            wVar = new w();
            wVar.f = aaVar.k;
            wVar.e = aaVar.j;
            this.f1692a.put(Integer.valueOf(wVar.a()), wVar);
        }
        if (System.currentTimeMillis() / 1000 > xVar.h) {
            return false;
        }
        if (wVar.c >= xVar.c) {
            a(aaVar.t, aaVar.k + "||" + aaVar.j + "|" + 1, aaVar.j);
            return false;
        } else if (wVar.f1766a < xVar.f1767a) {
            return true;
        } else {
            a(aaVar.t, aaVar.k + "||" + aaVar.j + "|" + 2, aaVar.j);
            return false;
        }
    }

    public void a(v vVar) {
        w wVar;
        int i;
        if (vVar != null) {
            w wVar2 = (w) this.f1692a.get(Integer.valueOf(vVar.a()));
            if (wVar2 == null) {
                w wVar3 = new w();
                wVar3.e = vVar.f1765a;
                wVar3.f = vVar.b;
                wVar = wVar3;
            } else {
                wVar = wVar2;
            }
            if (vVar.d) {
                wVar.d = true;
            }
            if (vVar.c) {
                if (a(vVar.e * 1000)) {
                    wVar.f1766a++;
                }
                x xVar = (x) this.b.get(Integer.valueOf(vVar.a()));
                if (xVar != null) {
                    i = xVar.i;
                } else {
                    i = 7;
                }
                if (a(vVar.e * 1000, (long) i)) {
                    wVar.b++;
                }
                wVar.c++;
            }
            this.f1692a.put(Integer.valueOf(wVar.a()), wVar);
        }
    }

    public void a(x xVar) {
        this.b.put(Integer.valueOf(xVar.a()), xVar);
    }

    public void a(n nVar) {
        if (nVar != null && nVar.j == 0) {
            aa aaVar = (aa) nVar;
            x xVar = (x) this.b.get(Integer.valueOf(aaVar.l()));
            if (xVar != null) {
                aaVar.q = xVar.d;
            }
        }
    }
}
