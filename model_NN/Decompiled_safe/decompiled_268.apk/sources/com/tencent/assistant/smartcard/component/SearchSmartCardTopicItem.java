package com.tencent.assistant.smartcard.component;

import android.content.Context;
import android.graphics.Canvas;
import android.text.Html;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.smartcard.d.aa;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistantv2.adapter.smartlist.z;
import com.tencent.assistantv2.st.page.a;
import com.tencent.assistantv2.st.page.d;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class SearchSmartCardTopicItem extends SearchSmartCardBaseItem {
    private ImageView l;
    private TXImageView m;
    private TextView n;
    private z o;
    private final String p = Constants.VIA_REPORT_TYPE_START_WAP;

    public SearchSmartCardTopicItem(Context context) {
        super(context);
    }

    public SearchSmartCardTopicItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public SearchSmartCardTopicItem(Context context, n nVar, as asVar, z zVar) {
        super(context, nVar, asVar);
        this.o = zVar;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.f) {
            this.f = true;
            h();
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.c = this.b.inflate((int) R.layout.smartcard_topic, this);
        this.m = (TXImageView) this.c.findViewById(R.id.topic_pic);
        setOnClickListener(this.k);
        this.n = (TextView) this.c.findViewById(R.id.topic_title);
        this.l = (ImageView) this.c.findViewById(R.id.close);
        this.l.setVisibility(8);
        i();
    }

    /* access modifiers changed from: protected */
    public void b() {
        i();
    }

    private void i() {
        aa aaVar = null;
        if (this.d instanceof aa) {
            aaVar = (aa) this.d;
        }
        if (aaVar != null) {
            this.m.updateImageView(aaVar.f1745a, -1, TXImageView.TXImageViewType.NETWORK_IMAGE_MIDDLE);
            this.n.setText(Html.fromHtml(aaVar.p));
        }
    }

    /* access modifiers changed from: protected */
    public String c() {
        return a.a(Constants.VIA_REPORT_TYPE_START_WAP, this.o == null ? 0 : this.o.a());
    }

    /* access modifiers changed from: protected */
    public int d() {
        return d.f2062a;
    }

    /* access modifiers changed from: protected */
    public String e() {
        if (this.o == null) {
            return null;
        }
        return this.o.d();
    }

    /* access modifiers changed from: protected */
    public long f() {
        if (this.o == null) {
            return 0;
        }
        return this.o.c();
    }

    /* access modifiers changed from: protected */
    public int g() {
        if (this.o == null) {
            return 2000;
        }
        return this.o.b();
    }
}
