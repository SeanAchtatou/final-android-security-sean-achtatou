package com.tencent.assistant.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.r;
import com.tencent.nucleus.manager.uninstallwatch.a;
import com.tencent.nucleus.manager.uninstallwatch.e;

/* compiled from: ProGuard */
public class PackageChangedReceiver extends BroadcastReceiver {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.uninstallwatch.e.a(android.content.Context, boolean):void
     arg types: [com.qq.AppService.AstApp, int]
     candidates:
      com.tencent.nucleus.manager.uninstallwatch.e.a(com.tencent.nucleus.manager.uninstallwatch.e, android.content.Context):void
      com.tencent.nucleus.manager.uninstallwatch.e.a(com.tencent.nucleus.manager.uninstallwatch.e, boolean):boolean
      com.tencent.nucleus.manager.uninstallwatch.e.a(android.content.Context, boolean):void */
    public void onReceive(Context context, Intent intent) {
        String dataString = intent.getDataString();
        if (!TextUtils.isEmpty(dataString)) {
            String substring = dataString.substring(dataString.lastIndexOf(58) + 1);
            boolean booleanExtra = intent.getBooleanExtra("android.intent.extra.REPLACING", false);
            if (b(intent)) {
                ApkResourceManager.getInstance().onPackageAdded(substring, booleanExtra);
                TemporaryThreadManager.get().start(new b(this, substring));
            } else if (a(intent)) {
                ApkResourceManager.getInstance().onPackageRemoved(substring, booleanExtra);
            } else if (c(intent)) {
                ApkResourceManager.getInstance().onPackageReplaced(substring, booleanExtra);
            } else if (d(intent)) {
                ApkResourceManager.getInstance().onPackageStateChange(substring, intent.getBooleanExtra("enabled", true));
            }
            if (a(intent) && !TextUtils.isEmpty(substring) && substring.equals(a.a())) {
                e.a().a((Context) AstApp.i(), false);
            }
            r.c("pkgChange");
        }
    }

    private boolean a(Intent intent) {
        return intent.getAction().equals("android.intent.action.PACKAGE_REMOVED");
    }

    private boolean b(Intent intent) {
        return intent.getAction().equals("android.intent.action.PACKAGE_ADDED");
    }

    private boolean c(Intent intent) {
        return intent.getAction().equals("android.intent.action.PACKAGE_REPLACED");
    }

    private boolean d(Intent intent) {
        return intent.getAction().equals("com.tencent.android.qqdownloader.PACKAGE_STATE_CHANGE");
    }
}
