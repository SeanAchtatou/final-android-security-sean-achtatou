package com.tencent.assistant.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.tencent.assistant.protocol.jce.ActionUrl;

/* compiled from: ProGuard */
final class a implements Parcelable.Creator<AppGroupInfo> {
    a() {
    }

    /* renamed from: a */
    public AppGroupInfo createFromParcel(Parcel parcel) {
        AppGroupInfo appGroupInfo = new AppGroupInfo();
        appGroupInfo.f935a = parcel.readInt();
        appGroupInfo.b = parcel.readString();
        appGroupInfo.c = parcel.readString();
        appGroupInfo.d = parcel.readString();
        appGroupInfo.e = parcel.readByte();
        appGroupInfo.f = parcel.readInt();
        appGroupInfo.g = new ActionUrl();
        appGroupInfo.g.f1125a = parcel.readString();
        appGroupInfo.g.b = parcel.readInt();
        return appGroupInfo;
    }

    /* renamed from: a */
    public AppGroupInfo[] newArray(int i) {
        return null;
    }
}
