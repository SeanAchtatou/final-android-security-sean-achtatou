package com.tencent.assistant.adapter;

import android.view.View;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.nucleus.manager.component.SwitchButton;

/* compiled from: ProGuard */
class aa extends ae {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f580a;
    final /* synthetic */ int b;
    final /* synthetic */ int c;
    final /* synthetic */ y d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    aa(y yVar, int i, int i2, int i3) {
        super(yVar, null);
        this.d = yVar;
        this.f580a = i;
        this.b = i2;
        this.c = i3;
    }

    public void onSwitchButtonClick(View view, boolean z) {
        this.d.a(this.f580a, view, this.b, z);
    }

    public STInfoV2 getStInfo(View view) {
        if (!(view instanceof SwitchButton)) {
            return null;
        }
        return this.d.a(this.d.d(this.c, this.b), this.d.b(!((SwitchButton) view).b()), 200);
    }
}
