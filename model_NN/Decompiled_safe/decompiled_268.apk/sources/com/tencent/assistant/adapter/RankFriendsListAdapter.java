package com.tencent.assistant.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.b;
import com.tencent.assistantv2.adapter.smartlist.SmartItemType;
import com.tencent.assistantv2.adapter.smartlist.aa;
import com.tencent.assistantv2.adapter.smartlist.ab;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.nucleus.socialcontact.login.j;
import com.tencent.pangu.adapter.RankNormalListAdapter;

/* compiled from: ProGuard */
public class RankFriendsListAdapter extends RankNormalListAdapter {
    private static int r = (b + 1);

    public RankFriendsListAdapter(Context context, View view, b bVar) {
        super(context, view, bVar);
    }

    public int getItemViewType(int i) {
        SimpleAppModel a2 = this.h.a(i);
        if (!e()) {
            if (TextUtils.isEmpty(a2.X)) {
                return r;
            }
            return f3404a;
        } else if (SimpleAppModel.CARD_TYPE.NORMAL == a2.U) {
            if (TextUtils.isEmpty(a2.X)) {
                return r;
            }
            return f3404a;
        } else if (SimpleAppModel.CARD_TYPE.QUALITY == a2.U) {
            return b;
        } else {
            return f3404a;
        }
    }

    public int getCount() {
        if (this.h == null) {
            return 0;
        }
        return this.h.a();
    }

    public int getViewTypeCount() {
        return r + 1;
    }

    /* renamed from: a */
    public SimpleAppModel getItem(int i) {
        if (this.h == null) {
            return null;
        }
        if (!j.a().k()) {
            return this.h.a(i - 1);
        }
        return this.h.a(i);
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        SimpleAppModel simpleAppModel;
        if (this.h == null || i >= this.h.a()) {
            simpleAppModel = null;
        } else {
            simpleAppModel = this.h.a(i);
        }
        aa aaVar = new aa();
        aaVar.a(ListItemInfoView.InfoType.CATEGORY_SIZE);
        aaVar.d(false);
        aaVar.e(true);
        aaVar.b(this.h.a(simpleAppModel));
        SmartItemType smartItemType = SmartItemType.NORMAL;
        if (f3404a == getItemViewType(i)) {
            if (!a(simpleAppModel)) {
                smartItemType = SmartItemType.NORMAL_NO_REASON;
            } else {
                smartItemType = SmartItemType.NORMAL;
            }
        } else if (b == getItemViewType(i)) {
            smartItemType = SmartItemType.COMPETITIVE;
        }
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f, simpleAppModel, a.a(this.p, i), 100, null);
        if (this.q != null) {
            this.q.exposure(buildSTInfo);
        }
        aaVar.a(buildSTInfo);
        com.tencent.pangu.model.b bVar = new com.tencent.pangu.model.b();
        bVar.b = 1;
        bVar.c = simpleAppModel;
        View a2 = ab.a(this.f, aaVar, view, smartItemType, i, bVar, null);
        if (a2 != null) {
            if (i == 0) {
                a2.setBackgroundResource(R.drawable.bg_card_selector_padding_notop);
            } else {
                a2.setBackgroundResource(R.drawable.bg_card_selector_padding);
            }
        }
        return a2;
    }

    public boolean a(SimpleAppModel simpleAppModel) {
        if (simpleAppModel == null || (TextUtils.isEmpty(simpleAppModel.X) && (simpleAppModel.av == null || simpleAppModel.av.a() == 0))) {
            return false;
        }
        return true;
    }
}
