package com.tencent.assistant.utils;

import android.content.SharedPreferences;
import android.text.format.Time;
import android.util.Log;
import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class ar {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1824a = ar.class.getSimpleName();

    public static int a() {
        int i = i();
        if (i != 0) {
            return 100 - i;
        }
        float j = ((float) j()) * 0.3f;
        float k = ((float) k()) * 0.3f;
        XLog.i(f1824a, "subMemoryScore=" + j + " subvolScore=" + k);
        return (int) Math.ceil((double) (((100.0f - j) - k) - Math.max(((float) m()) * 1.0f, Math.max(((float) b()) * 1.0f, ((float) l()) * 1.0f))));
    }

    private static int j() {
        float d = d();
        float f = o().getFloat("k_l_m_r", 0.15f);
        if (!a(0)) {
            XLog.i(f1824a, "last memory is not the same day");
            f = 0.15f;
        }
        float f2 = 1.0f - ((1.0f - d) / (1.0f - f));
        XLog.i(f1824a, "memory result=" + f2 + " ratio=" + d);
        if (((double) f2) > 0.6d) {
            return 100;
        }
        if (((double) f2) > 0.4d && ((double) f2) <= 0.6d) {
            return 70;
        }
        if (((double) f2) > 0.3d && ((double) f2) <= 0.4d) {
            return 50;
        }
        if (((double) f2) > 0.2d && ((double) f2) <= 0.3d) {
            return 30;
        }
        if (f2 <= 0.0f || ((double) f2) > 0.2d) {
            return 0;
        }
        return 20;
    }

    private static int k() {
        float n = n();
        float f = o().getFloat("k_l_v_r", 0.2f);
        if (!a(1)) {
            XLog.i(f1824a, "last vol is not the same day");
            f = 0.2f;
        }
        float f2 = 1.0f - ((1.0f - n) / (1.0f - f));
        XLog.i(f1824a, "vol result=" + f2 + " volratio=" + n + " lastVolRatio=" + f);
        if (((double) f2) > 0.6d) {
            return 100;
        }
        if (((double) f2) > 0.4d && ((double) f2) <= 0.6d) {
            return 80;
        }
        if (((double) f2) > 0.3d && ((double) f2) <= 0.4d) {
            return 60;
        }
        if (((double) f2) > 0.2d && ((double) f2) <= 0.3d) {
            return 40;
        }
        if (f2 <= 0.0f || ((double) f2) > 0.2d) {
            return 0;
        }
        return 20;
    }

    public static int b() {
        String[] split = o().getString("k_l_v_l", Constants.STR_EMPTY).split("|");
        if (split == null || split.length <= 0) {
            return 0;
        }
        for (String b : split) {
            if (e.b(b, 0)) {
                return 10;
            }
        }
        return 0;
    }

    private static int l() {
        return c() ? 0 : 10;
    }

    public static boolean c() {
        return e.b(AppConst.TENCENT_MOBILE_MANAGER_PKGNAME, 0);
    }

    private static int m() {
        if (System.currentTimeMillis() - o().getLong("k_l_d_a_t", 0) > 259200000) {
            return 10;
        }
        return 0;
    }

    public static float d() {
        long q = t.q();
        long r = t.r();
        if (q <= 0 || r <= 0) {
            return 0.15f;
        }
        return (((float) (q - r)) * 1.0f) / ((float) q);
    }

    private static float n() {
        long f = t.f();
        long e = t.e();
        Log.i(f1824a, "totalVol=" + f + " freeVol=" + e);
        if (f <= 0 || e <= 0) {
            return 0.2f;
        }
        return 1.0f - ((((float) (f - e)) * 1.0f) / ((float) f));
    }

    public static void e() {
        SharedPreferences.Editor edit = o().edit();
        edit.putFloat("k_l_m_r", d());
        edit.putLong("manager_0", System.currentTimeMillis());
        edit.commit();
    }

    public static void f() {
        SharedPreferences.Editor edit = o().edit();
        edit.putFloat("k_l_v_r", n());
        edit.putLong("manager_1", System.currentTimeMillis());
        edit.commit();
    }

    public static void a(String str) {
        XLog.i(f1824a, ">>pkgs=" + str);
        SharedPreferences.Editor edit = o().edit();
        edit.putString("k_l_v_l", str);
        edit.commit();
    }

    public static void b(String str) {
        XLog.i(f1824a, ">>pkgName=" + str);
        a(o().getString("k_l_v_l", Constants.STR_EMPTY).replace(str, Constants.STR_EMPTY).replace("||", "|"));
    }

    public static void g() {
        SharedPreferences.Editor edit = o().edit();
        edit.putLong("k_l_c_t", System.currentTimeMillis());
        edit.commit();
    }

    public static void h() {
        SharedPreferences.Editor edit = o().edit();
        edit.putLong("k_l_d_a_t", System.currentTimeMillis());
        edit.commit();
    }

    private static SharedPreferences o() {
        return AstApp.i().getSharedPreferences("manager", 0);
    }

    public static void a(int i, Class<?> cls) {
        String string = o().getString("k_l_s_s", Constants.STR_EMPTY);
        String name = cls == null ? Constants.STR_EMPTY : cls.getName();
        SharedPreferences.Editor edit = o().edit();
        edit.putInt(string, 0);
        edit.putString("k_l_s_s", name);
        edit.putLong("manager_5", System.currentTimeMillis());
        edit.putInt(name, i);
        edit.commit();
    }

    public static void b(int i, Class<?> cls) {
        String name = cls == null ? Constants.STR_EMPTY : cls.getName();
        SharedPreferences.Editor edit = o().edit();
        edit.putInt(name, i);
        edit.commit();
    }

    public static int i() {
        if (!a(5)) {
            XLog.i(f1824a, "last op is not the same day");
            return 0;
        }
        SharedPreferences o = o();
        return o.getInt(o.getString("k_l_s_s", Constants.STR_EMPTY), 0);
    }

    private static boolean a(int i) {
        long j = o().getLong("manager_" + i, 0);
        long currentTimeMillis = System.currentTimeMillis();
        Time time = new Time();
        time.set(j);
        String str = time.year + "_" + time.month + "_" + time.monthDay;
        time.set(currentTimeMillis);
        return str.equals(time.year + "_" + time.month + "_" + time.monthDay);
    }
}
