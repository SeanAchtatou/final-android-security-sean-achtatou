package com.tencent.assistant.utils;

import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.protocol.jce.AppUpdateInfo;
import com.tencent.assistant.protocol.jce.AutoDownloadInfo;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.manager.SelfUpdateManager;

/* compiled from: ProGuard */
public class g {
    public static boolean a(String str, int i, int i2, String str2, int i3, boolean z) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        if (i <= i3) {
            return false;
        }
        LocalApkInfo installedApkInfo = ApkResourceManager.getInstance().getInstalledApkInfo(str);
        if (z) {
            if (installedApkInfo == null) {
                return false;
            }
            if (i3 != installedApkInfo.mVersionCode || TextUtils.isEmpty(str2) || !str2.equals(installedApkInfo.manifestMd5)) {
                return false;
            }
            return true;
        } else if (installedApkInfo == null || i > installedApkInfo.mVersionCode) {
            return true;
        } else {
            if (i < installedApkInfo.mVersionCode) {
                return false;
            }
            if (TextUtils.equals(AstApp.i().getPackageName(), str) && SelfUpdateManager.a().l() && i2 == installedApkInfo.mGrayVersionCode) {
                return true;
            }
            if (i2 != 0 && i2 == installedApkInfo.mGrayVersionCode) {
                return false;
            }
            if (i2 == 0 || installedApkInfo.mGrayVersionCode == 0 || i2 > installedApkInfo.mGrayVersionCode) {
                return true;
            }
            return false;
        }
    }

    public static boolean a(String str, int i, int i2, String str2, int i3) {
        return a(str, i, i2, str2, i3, true);
    }

    public static boolean b(String str, int i, int i2, String str2, int i3, boolean z) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        if (i <= i3) {
            return false;
        }
        LocalApkInfo installedApkInfo = ApkResourceManager.getInstance().getInstalledApkInfo(str);
        if (z) {
            if (installedApkInfo == null) {
                return false;
            }
            if (i3 != installedApkInfo.mVersionCode || TextUtils.isEmpty(str2) || !str2.equals(installedApkInfo.manifestMd5)) {
                return false;
            }
            return true;
        } else if (installedApkInfo == null || i > installedApkInfo.mVersionCode) {
            return true;
        } else {
            if (i < installedApkInfo.mVersionCode) {
                return false;
            }
            if (TextUtils.equals(AstApp.i().getPackageName(), str) && SelfUpdateManager.a().l() && i2 == installedApkInfo.mGrayVersionCode) {
                return true;
            }
            if (i2 == installedApkInfo.mGrayVersionCode) {
                return false;
            }
            if (i2 == 0 || installedApkInfo.mGrayVersionCode == 0 || i2 > installedApkInfo.mGrayVersionCode) {
                return true;
            }
            return false;
        }
    }

    public static boolean b(String str, int i, int i2, String str2, int i3) {
        return b(str, i, i2, str2, i3, false);
    }

    public static boolean a(AppUpdateInfo appUpdateInfo) {
        if (appUpdateInfo == null || ApkResourceManager.getInstance().getInstalledApkInfo(appUpdateInfo.f1162a) == null) {
            return false;
        }
        return b(appUpdateInfo.f1162a, appUpdateInfo.d, appUpdateInfo.w, appUpdateInfo.E, appUpdateInfo.C);
    }

    public static boolean a(DownloadInfo downloadInfo) {
        if (downloadInfo == null) {
            return false;
        }
        return a(downloadInfo.packageName, downloadInfo.versionCode, downloadInfo.grayVersionCode, downloadInfo.sllLocalManifestMd5, downloadInfo.sllLocalVersionCode, downloadInfo.isSslUpdate());
    }

    public static boolean b(DownloadInfo downloadInfo) {
        if (downloadInfo == null) {
            return false;
        }
        return a(downloadInfo.packageName, downloadInfo.versionCode, downloadInfo.grayVersionCode, downloadInfo.sllLocalManifestMd5, downloadInfo.sllLocalVersionCode);
    }

    public static boolean a(AutoDownloadInfo autoDownloadInfo) {
        if (autoDownloadInfo == null) {
            return false;
        }
        return b(autoDownloadInfo.f1166a, autoDownloadInfo.d, 0, autoDownloadInfo.u, autoDownloadInfo.t);
    }
}
