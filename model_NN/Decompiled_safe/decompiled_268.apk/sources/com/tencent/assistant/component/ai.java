package com.tencent.assistant.component;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.utils.be;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class ai extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SecondNavigationTitleView f630a;

    ai(SecondNavigationTitleView secondNavigationTitleView) {
        this.f630a = secondNavigationTitleView;
    }

    public void onTMAClick(View view) {
        this.f630a.dialog.b(!be.b());
        be.a(this.f630a.dialog.a());
        if (this.f630a.iv_red_dot != null) {
            this.f630a.iv_red_dot.setVisibility(8);
        }
        if (this.f630a.firstShowFloatFlag) {
            boolean unused = this.f630a.firstShowFloatFlag = false;
        }
        if (this.f630a.hostActivity != null && !this.f630a.hostActivity.isFinishing()) {
            this.f630a.dialog.show();
        }
        if (!(this.f630a.appModel == null || this.f630a.simpleAppModel == null)) {
            this.f630a.dialog.a(this.f630a.appModel.f941a.f, this.f630a.simpleAppModel.f938a);
        }
        Window window = this.f630a.dialog.getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        attributes.gravity = 49;
        attributes.y = this.f630a.showMore.getTop() + this.f630a.showMore.getHeight();
        attributes.width = (int) (((double) this.f630a.dialog.getWindow().getWindowManager().getDefaultDisplay().getWidth()) * 0.95d);
        window.setAttributes(attributes);
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f630a.context, 200);
        if (buildSTInfo != null) {
            buildSTInfo.slotId = "03_001";
            buildSTInfo.updateWithSimpleAppModel(this.f630a.simpleAppModel);
        }
        return buildSTInfo;
    }
}
