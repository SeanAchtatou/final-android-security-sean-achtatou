package com.tencent.assistant.component.txscrollview;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.utils.by;

/* compiled from: ProGuard */
public class RefreshListLoading extends TXLoadingLayoutBase {
    private View c;
    private ProgressBar d;
    private ImageView e;
    private ImageView f;
    private TextView g;
    private TextView h;
    private RelativeLayout i;
    private RotateAnimation j;
    private RotateAnimation k;
    private CharSequence l;
    private CharSequence m;
    private CharSequence n;
    private CharSequence o;
    private CharSequence p;
    private CharSequence q;
    private String r;
    private boolean s = false;

    public RefreshListLoading(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, TXScrollViewBase.ScrollMode.PULL_FROM_END);
        a();
    }

    public RefreshListLoading(Context context, TXScrollViewBase.ScrollDirection scrollDirection, TXScrollViewBase.ScrollMode scrollMode) {
        super(context, scrollDirection, scrollMode);
        a(context, this.b);
        a();
    }

    public void setRefreshTimeKey(String str) {
        this.r = str;
    }

    private void a() {
        this.j = new RotateAnimation(0.0f, -180.0f, 1, 0.5f, 1, 0.5f);
        this.j.setInterpolator(new LinearInterpolator());
        this.j.setDuration(100);
        this.j.setFillAfter(true);
        this.k = new RotateAnimation(-180.0f, 0.0f, 1, 0.5f, 1, 0.5f);
        this.k.setInterpolator(new LinearInterpolator());
        this.k.setDuration(100);
        this.k.setFillAfter(true);
    }

    public void reset() {
        if (this.b == TXScrollViewBase.ScrollMode.PULL_FROM_START) {
            this.e.setVisibility(0);
            this.c.setVisibility(0);
        } else {
            this.e.setVisibility(8);
            this.c.setVisibility(8);
        }
        this.e.clearAnimation();
        this.g.setVisibility(0);
        this.g.setText(this.l);
        this.d.setVisibility(8);
        this.f.setVisibility(8);
    }

    public void pullToRefresh() {
        this.e.setVisibility(0);
        this.e.clearAnimation();
        this.g.setVisibility(0);
        this.g.setText(this.l);
        this.h.setVisibility(8);
        this.d.setVisibility(8);
        this.f.setVisibility(8);
    }

    public void releaseToRefresh() {
        this.e.clearAnimation();
        this.e.startAnimation(this.j);
        this.g.setVisibility(0);
        this.g.setText(this.m);
        this.h.setVisibility(8);
    }

    public void refreshing() {
        this.e.clearAnimation();
        this.e.setVisibility(8);
        this.f.setVisibility(8);
        this.g.setText(this.n);
        this.h.setVisibility(8);
        this.c.setVisibility(0);
        this.d.setVisibility(0);
    }

    public void loadFinish(String str) {
        this.e.setVisibility(8);
        this.f.setVisibility(0);
        try {
            this.f.setImageResource(R.drawable.loaded_all);
        } catch (Throwable th) {
            t.a().b();
        }
        this.g.setText(str);
        this.h.setVisibility(8);
        this.d.setVisibility(8);
        if (!TextUtils.isEmpty(str)) {
            this.c.setVisibility(0);
            this.g.setVisibility(0);
            return;
        }
        this.c.setVisibility(8);
        this.g.setVisibility(8);
    }

    public void setWidth(int i2) {
        getLayoutParams().width = i2;
        requestLayout();
    }

    public void setHeight(int i2) {
        getLayoutParams().height = i2;
        requestLayout();
    }

    public void onPull(int i2) {
    }

    public void hideAllSubViews() {
        if (this.e.getVisibility() == 0) {
            this.e.setVisibility(4);
        }
        if (this.f.getVisibility() == 0) {
            this.f.setVisibility(4);
        }
        if (this.g.getVisibility() == 0) {
            this.g.setVisibility(4);
        }
        if (this.h.getVisibility() == 0) {
            this.h.setVisibility(4);
        }
        if (this.d.getVisibility() == 0) {
            this.d.setVisibility(4);
        }
    }

    public void showAllSubViews() {
        if (this.e.getVisibility() == 4) {
            this.e.setVisibility(0);
        }
        if (this.f.getVisibility() == 4) {
            this.f.setVisibility(0);
        }
        if (this.g.getVisibility() == 4) {
            this.g.setVisibility(0);
        }
        if (this.h.getVisibility() == 4) {
        }
        if (this.d.getVisibility() == 4) {
            this.d.setVisibility(0);
        }
    }

    public int getContentSize() {
        return this.i.getHeight();
    }

    public int getTriggerSize() {
        return getResources().getDimensionPixelSize(R.dimen.tx_pull_to_refresh_trigger_size);
    }

    private void a(Context context, TXScrollViewBase.ScrollMode scrollMode) {
        if (scrollMode == TXScrollViewBase.ScrollMode.PULL_FROM_START) {
            this.l = context.getString(R.string.refresh_list_loading_pull_from_start);
            this.n = context.getString(R.string.refresh_list_loading_refreshing_from_start);
        } else {
            this.l = context.getString(R.string.refresh_list_loading_pullToRefresh);
            this.n = context.getString(R.string.refresh_list_loading_refreshing_from_end);
        }
        this.m = context.getString(R.string.refresh_list_loading_releaseToRefresh);
        this.q = context.getString(R.string.refresh_loading_fail);
        this.o = context.getString(R.string.refresh_list_loading_loadfinish);
        this.p = context.getString(R.string.refresh_suc);
        LayoutInflater.from(context).inflate((int) R.layout.refresh_list_loading, this);
        this.i = (RelativeLayout) findViewById(R.id.refresh_list_loading_content);
        this.c = findViewById(R.id.left_ly);
        this.d = (ProgressBar) findViewById(R.id.refresh_list_loading_progress);
        this.e = (ImageView) findViewById(R.id.refresh_image);
        this.f = (ImageView) findViewById(R.id.result_img);
        this.g = (TextView) findViewById(R.id.refresh_text);
        this.h = (TextView) findViewById(R.id.small_txt);
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.i.getLayoutParams();
        if (scrollMode == TXScrollViewBase.ScrollMode.PULL_FROM_START) {
            layoutParams.height = by.a(getContext(), 60.0f);
            layoutParams.gravity = 80;
            this.e.setVisibility(0);
        } else {
            layoutParams.height = by.a(getContext(), 63.0f);
            layoutParams.gravity = 48;
            this.e.setVisibility(8);
        }
        reset();
    }

    public void refreshSuc() {
        this.e.setVisibility(8);
        this.d.setVisibility(8);
        if (this.s) {
            this.f.setImageResource(R.drawable.right_white);
        } else {
            this.f.setImageResource(R.drawable.right);
        }
        this.f.setVisibility(0);
        this.g.setVisibility(0);
        this.g.setText(this.p);
        this.h.setVisibility(8);
    }

    public void refreshFail(String str) {
        this.e.setVisibility(8);
        this.d.setVisibility(8);
        if (this.s) {
            this.f.setImageResource(R.drawable.shibai_white);
        } else {
            this.f.setImageResource(R.drawable.shibai);
        }
        this.f.setVisibility(0);
        this.g.setVisibility(0);
        this.g.setText(str);
        this.h.setVisibility(8);
    }

    public void loadSuc() {
        reset();
    }

    public void loadFail() {
        this.e.setVisibility(8);
        this.d.setVisibility(8);
        this.f.setImageResource(R.drawable.shibai);
        this.f.setVisibility(0);
        this.g.setVisibility(0);
        this.g.setText(this.q);
        this.h.setVisibility(8);
    }

    public void refreshLoadingResource() {
        this.s = true;
        this.e.setImageResource(R.drawable.refresh_arrow_white);
        Drawable drawable = getContext().getResources().getDrawable(R.anim.rotate_loading_view_withtopbanner);
        drawable.setBounds(0, by.a(getContext(), 16.0f), 0, by.a(getContext(), 16.0f));
        this.d.setIndeterminateDrawable(drawable);
        this.g.setTextColor(getContext().getResources().getColor(R.color.white));
        this.h.setTextColor(getContext().getResources().getColor(R.color.white));
    }

    public void resetLoadingResource() {
        this.s = false;
        this.e.setImageResource(R.drawable.refresh_arrow);
        Drawable drawable = getContext().getResources().getDrawable(R.anim.rotate_loading_view);
        drawable.setBounds(0, by.a(getContext(), 16.0f), 0, by.a(getContext(), 16.0f));
        this.d.setIndeterminateDrawable(drawable);
        this.g.setTextColor(getContext().getResources().getColor(R.color.pull_refresh_txt_color));
        this.h.setTextColor(getContext().getResources().getColor(R.color.pull_refresh_txt_small_color));
    }
}
