package com.tencent.assistant.component;

import android.view.View;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class g implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ColorCardView f667a;

    g(ColorCardView colorCardView) {
        this.f667a = colorCardView;
    }

    public void onClick(View view) {
        int i;
        try {
            i = ((Integer) view.getTag(R.id.category_pos)).intValue();
        } catch (Exception e) {
            e.printStackTrace();
            i = 0;
        }
        this.f667a.itemActionReport(i, 200);
        if (this.f667a.mOutListener != null) {
            this.f667a.mOutListener.onClick(view);
        }
    }
}
