package com.tencent.assistant.component.dialog;

import android.content.Context;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
public class OneButtonDialogView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private LayoutInflater f646a;
    private TextView b;
    private TextView c;
    private Button d;

    public OneButtonDialogView(Context context) {
        this(context, null);
    }

    public OneButtonDialogView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f646a = LayoutInflater.from(context);
        a();
    }

    private void a() {
        this.f646a.inflate((int) R.layout.dialog_1button, this);
        this.b = (TextView) findViewById(R.id.title);
        this.c = (TextView) findViewById(R.id.msg);
        this.c.setMovementMethod(ScrollingMovementMethod.getInstance());
        this.d = (Button) findViewById(R.id.positive_btn);
    }

    public void setTitleAndMsg(boolean z, String str, String str2) {
        if (z) {
            this.b.setVisibility(0);
            if (!TextUtils.isEmpty(str)) {
                this.b.setText(str);
            }
        } else {
            this.b.setVisibility(8);
        }
        if (!TextUtils.isEmpty(str2)) {
            this.c.setText(str2);
        }
    }

    public void setButton(String str, View.OnClickListener onClickListener) {
        if (!TextUtils.isEmpty(str)) {
            this.d.setText(str);
        }
        if (onClickListener != null) {
            this.d.setOnClickListener(onClickListener);
        }
    }
}
