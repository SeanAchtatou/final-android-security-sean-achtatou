package com.tencent.assistant.component;

import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.smartcard.d.b;
import java.util.ArrayList;

/* compiled from: ProGuard */
class z implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f728a;
    final /* synthetic */ y b;

    z(y yVar, int i) {
        this.b = yVar;
        this.f728a = i;
    }

    public void run() {
        boolean z = false;
        ArrayList<SimpleAppModel> arrayList = this.b.f727a.getNoNetworkInstallApps().c;
        if (arrayList != null && arrayList.size() < 4) {
            z = true;
        }
        if (this.f728a == 0 && this.b.f727a.currentState == 30 && z) {
            b unused = this.b.f727a.smartCardAppModelGlobal = null;
            this.b.f727a.setErrorType(30);
        }
    }
}
