package com.tencent.assistant.component;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.activity.AppDetailActivityV5;

/* compiled from: ProGuard */
class b extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppIconView f640a;

    b(AppIconView appIconView) {
        this.f640a = appIconView;
    }

    public void onTMAClick(View view) {
        if (this.f640a.iconClickListener != null) {
            this.f640a.iconClickListener.onClick();
            return;
        }
        Intent intent = new Intent(this.f640a.mContext, AppDetailActivityV5.class);
        if (this.f640a.mContext instanceof BaseActivity) {
            intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, ((BaseActivity) this.f640a.mContext).f());
        }
        intent.putExtra("simpleModeInfo", this.f640a.mAppModel);
        intent.putExtra("statInfo", this.f640a.statInfo);
        this.f640a.mContext.startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        if (this.f640a.statInfo == null || !(this.f640a.statInfo instanceof STInfoV2)) {
            return null;
        }
        this.f640a.statInfo.updateStatus(this.f640a.mAppModel);
        return (STInfoV2) this.f640a.statInfo;
    }
}
