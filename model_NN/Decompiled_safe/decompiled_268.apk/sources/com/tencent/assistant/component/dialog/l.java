package com.tencent.assistant.component.dialog;

import android.app.Dialog;
import android.view.View;
import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
final class l implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.TwoBtnDialogInfo f658a;
    final /* synthetic */ Dialog b;

    l(AppConst.TwoBtnDialogInfo twoBtnDialogInfo, Dialog dialog) {
        this.f658a = twoBtnDialogInfo;
        this.b = dialog;
    }

    public void onClick(View view) {
        if (this.f658a != null) {
            try {
                this.f658a.onLeftBtnClick();
                this.b.dismiss();
            } catch (Throwable th) {
            }
        }
    }
}
