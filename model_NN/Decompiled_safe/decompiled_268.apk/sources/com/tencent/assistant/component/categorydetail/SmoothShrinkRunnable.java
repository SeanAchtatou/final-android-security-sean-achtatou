package com.tencent.assistant.component.categorydetail;

import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.AbsListView;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
public class SmoothShrinkRunnable implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private View f643a;
    private int b;
    private int c;
    private long d;
    private Interpolator e = new AccelerateDecelerateInterpolator();
    private long f = -1;
    private SmoothShrinkListener g;
    private boolean h = false;

    public SmoothShrinkRunnable(View view, int i, int i2, long j) {
        this.f643a = view;
        this.b = i;
        this.c = i2;
        this.d = j;
    }

    public void setListener(SmoothShrinkListener smoothShrinkListener) {
        this.g = smoothShrinkListener;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public void run() {
        if (this.d <= 0) {
            b();
        } else if (this.f <= 0) {
            this.f = System.currentTimeMillis();
            a();
            if (this.f643a != null) {
                this.f643a.postDelayed(this, 10);
            }
        } else if (System.currentTimeMillis() - this.f >= this.d) {
            b();
        } else {
            int round = this.b - Math.round(this.e.getInterpolation(((float) Math.max(Math.min(((System.currentTimeMillis() - this.f) * 1000) / this.d, 1000L), 0L)) / 1000.0f) * ((float) (this.b - this.c)));
            XLog.d("yanhui-fth", "height:" + round + " newHeight:" + this.c);
            if (round <= this.c) {
                b();
                return;
            }
            b(round);
            a(round);
            if (this.f643a != null) {
                this.f643a.postDelayed(this, 10);
            }
        }
    }

    private void a(int i) {
        if (this.f643a != null) {
            this.f643a.setLayoutParams(new AbsListView.LayoutParams(-1, i));
            this.f643a.postInvalidate();
        }
    }

    private void a() {
        this.h = true;
        if (this.g != null) {
            this.g.onStart();
        }
    }

    private void b(int i) {
        this.h = true;
        if (this.g != null) {
            this.g.onShrink(i);
        }
    }

    private void b() {
        XLog.d("yanhui-fth", "stop:" + this.c);
        this.h = false;
        a(this.c);
        if (this.g != null) {
            this.g.onStop();
        }
    }

    public boolean isShinking() {
        return this.h;
    }
}
