package com.tencent.assistant.component.txscrollview;

import com.tencent.assistant.component.txscrollview.TXScrollViewBase;

/* compiled from: ProGuard */
class q implements TXScrollViewBase.ISmoothScrollRunnableListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TXRefreshScrollViewBase f719a;

    q(TXRefreshScrollViewBase tXRefreshScrollViewBase) {
        this.f719a = tXRefreshScrollViewBase;
    }

    public void onSmoothScrollFinished() {
        if (this.f719a.k != null) {
            this.f719a.k.onTXRefreshListViewRefresh(this.f719a.n);
        }
    }
}
