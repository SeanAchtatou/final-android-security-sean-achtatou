package com.tencent.assistant.component;

import android.view.View;

/* compiled from: ProGuard */
class aq implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TotalTabLayout f638a;

    private aq(TotalTabLayout totalTabLayout) {
        this.f638a = totalTabLayout;
    }

    /* synthetic */ aq(TotalTabLayout totalTabLayout, an anVar) {
        this(totalTabLayout);
    }

    public void onClick(View view) {
        int id = view.getId();
        for (int i = 0; i < this.f638a.mTabStrArray.length; i++) {
            if (this.f638a.getTabView(i) != null && id == this.f638a.getTabView(i).getId()) {
                this.f638a.selectTab(i, true);
                if (this.f638a.viewClickListener != null) {
                    this.f638a.viewClickListener.onClick(view);
                }
            }
        }
    }
}
