package com.tencent.tmsecurelite.a;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* compiled from: ProGuard */
public abstract class h extends Binder implements p {
    public h() {
        attachInterface(this, "com.tencent.tmsecurelite.IEncryptListener");
    }

    public static p a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.tencent.tmsecurelite.IEncryptListener");
        if (queryLocalInterface == null || !(queryLocalInterface instanceof p)) {
            return new g(iBinder);
        }
        return (p) queryLocalInterface;
    }

    /* access modifiers changed from: protected */
    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        switch (i) {
            case 1:
                a(parcel.readInt());
                parcel2.writeNoException();
                return true;
            case 2:
                b(parcel.readInt());
                parcel2.writeNoException();
                return true;
            case 3:
                c(parcel.readInt());
                parcel2.writeNoException();
                return true;
            case 4:
                a(parcel.readInt(), parcel.readString());
                parcel2.writeNoException();
                return true;
            case 5:
                a(parcel.readInt(), parcel.readInt(), parcel.readString());
                parcel2.writeNoException();
                return true;
            case 6:
                a(parcel.readInt(), parcel.readInt(), parcel.readInt());
                parcel2.writeNoException();
                return true;
            case 7:
                a(parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt());
                parcel2.writeNoException();
                return true;
            default:
                return true;
        }
    }
}
