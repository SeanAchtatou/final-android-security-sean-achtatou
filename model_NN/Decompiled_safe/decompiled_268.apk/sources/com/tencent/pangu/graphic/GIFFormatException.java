package com.tencent.pangu.graphic;

/* compiled from: ProGuard */
public class GIFFormatException extends Exception {
    public GIFFormatException() {
        super("The image is not a GIF.");
    }
}
