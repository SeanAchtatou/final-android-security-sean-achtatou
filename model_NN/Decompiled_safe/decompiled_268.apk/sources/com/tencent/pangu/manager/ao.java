package com.tencent.pangu.manager;

import android.util.Log;
import com.tencent.pangu.download.DownloadInfo;
import java.util.concurrent.CountDownLatch;

/* compiled from: ProGuard */
class ao implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfo f3797a;
    final /* synthetic */ ak b;

    ao(ak akVar, DownloadInfo downloadInfo) {
        this.b = akVar;
        this.f3797a = downloadInfo;
    }

    public void run() {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        try {
            this.b.a(countDownLatch);
            countDownLatch.await();
            if (this.b.e) {
                Log.i("QubeManager", "<Qube> downloadLastestQubeWithTheme download theme");
                this.f3797a.autoInstall = false;
                DownloadProxy.a().d(this.f3797a);
                DownloadProxy.a().c(this.f3797a);
                return;
            }
            this.b.c(this.f3797a);
        } catch (Exception e) {
            Log.e("QubeManager", "downloadLastestQubeWithTheme", e);
        }
    }
}
