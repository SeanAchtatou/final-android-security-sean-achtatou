package com.tencent.pangu.manager;

import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.protocol.jce.Banner;
import com.tencent.assistant.protocol.jce.TopBanner;
import com.tencent.assistant.smartcard.b.c;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistantv2.st.business.LaunchSpeedSTManager;
import com.tencent.pangu.module.a.f;
import com.tencent.pangu.module.ar;
import java.util.List;

/* compiled from: ProGuard */
class aa implements f {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ x f3784a;

    private aa(x xVar) {
        this.f3784a = xVar;
    }

    /* synthetic */ aa(x xVar, y yVar) {
        this(xVar);
    }

    public void a(int i, int i2, boolean z, ar arVar, boolean z2, List<Banner> list, TopBanner topBanner, long j, List<SimpleAppModel> list2, c cVar) {
        if (LaunchSpeedSTManager.d().b(i)) {
            LaunchSpeedSTManager.d().a(LaunchSpeedSTManager.TYPE_TIME_POINT.HomeManager_onPageLoad_Begin);
        }
        if (z2) {
            int unused = this.f3784a.e = 0;
        }
        long unused2 = this.f3784a.d = System.currentTimeMillis();
        System.currentTimeMillis();
        List a2 = this.f3784a.a(cVar, list2, this.f3784a.e, arVar != null ? arVar.b : this.f3784a.e + this.f3784a.b.a());
        List a3 = this.f3784a.a(list);
        this.f3784a.a(topBanner, j);
        this.f3784a.a(i, i2, z, arVar, z2, a3, this.f3784a.g, a2);
        if (LaunchSpeedSTManager.d().b(i)) {
            LaunchSpeedSTManager.d().a(LaunchSpeedSTManager.TYPE_TIME_POINT.HomeManager_CardMergeProcess_End);
        }
        TemporaryThreadManager.get().startDelayed(new ab(this), 1000);
    }
}
