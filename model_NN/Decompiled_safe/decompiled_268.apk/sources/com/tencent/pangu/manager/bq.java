package com.tencent.pangu.manager;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.m;
import com.tencent.assistant.protocol.jce.AppSimpleDetail;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.XLog;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.link.b;
import com.tencent.pangu.module.a.p;
import com.tencent.pangu.module.bh;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class bq implements p {

    /* renamed from: a  reason: collision with root package name */
    public static bq f3825a = null;
    public static String[] b = {Environment.getExternalStorageDirectory().getAbsolutePath() + "/Download", Environment.getExternalStorageDirectory().getAbsolutePath() + "/UCDownloads/", Environment.getExternalStorageDirectory().getAbsolutePath() + "/baidu/AppSearch/downloads", Environment.getExternalStorageDirectory().getAbsolutePath() + "/mm/download", Environment.getExternalStorageDirectory().getAbsolutePath() + "/SogouDownload", Environment.getExternalStorageDirectory().getAbsolutePath() + "/MxBrowser/Downloads", Environment.getExternalStorageDirectory().getAbsolutePath() + "/Android/data", Environment.getExternalStorageDirectory().getAbsolutePath() + "/tencent/QQfile_recv", Environment.getExternalStorageDirectory().getAbsolutePath() + "/hao123/down/apk", Environment.getExternalStorageDirectory().getAbsolutePath() + "/QQBrowser/安装包", Environment.getExternalStorageDirectory().getAbsolutePath() + "/ydBrowser/download", Environment.getExternalStorageDirectory().getAbsolutePath() + "/360Browser"};
    private bh c = new bh();

    public static synchronized bq a() {
        bq bqVar;
        synchronized (bq.class) {
            if (f3825a == null) {
                f3825a = new bq();
            }
            bqVar = f3825a;
        }
        return bqVar;
    }

    private bq() {
        if (this.c != null) {
            this.c.register(this);
        }
    }

    public void a(String str) {
        if (this.c != null) {
            this.c.a(b(str), null, (byte) 1);
        }
    }

    public void a(int i, int i2, byte b2, byte b3, AppSimpleDetail appSimpleDetail) {
    }

    public void a(int i, int i2, byte b2, byte b3, String str) {
        if (!m.a().Z() && !TextUtils.isEmpty(str)) {
            Context m = AstApp.m();
            if (m == null) {
                m = AstApp.i().getApplicationContext();
            }
            b.c(m, str);
            m.a().u(true);
        }
    }

    public void a(int i, int i2) {
    }

    private String b(String str) {
        if (TextUtils.isEmpty(str)) {
            return Constants.STR_EMPTY;
        }
        String substring = str.substring(str.lastIndexOf("/") + 1);
        XLog.d("SourceCheckManager", " filename:" + substring);
        return substring;
    }

    public static void b() {
        try {
            if (!m.a().Z()) {
                XLog.d("SourceCheckManager", "开始检查。。。。。");
                long currentTimeMillis = System.currentTimeMillis();
                ArrayList arrayList = new ArrayList();
                for (String scanFile : b) {
                    List<String> scanFile2 = FileUtil.scanFile(scanFile, Arrays.asList("apk", "APK"));
                    if (scanFile2 != null) {
                        XLog.i("SourceCheckManager", "ALL PATH:" + scanFile2);
                        Iterator<String> it = scanFile2.iterator();
                        while (true) {
                            if (it.hasNext()) {
                                String next = it.next();
                                if (next != null && next.contains("YYB.998886")) {
                                    arrayList.add(next);
                                    XLog.i("SourceCheckManager", "找到啦:" + next);
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                    }
                }
                if (System.currentTimeMillis() - currentTimeMillis <= 60000 && arrayList != null && arrayList.size() > 0) {
                    XLog.i("SourceCheckManager", "filePath:" + ((String) arrayList.get(0)));
                    a().a((String) arrayList.get(0));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
