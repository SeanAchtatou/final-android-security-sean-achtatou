package com.tencent.pangu.download;

import android.os.Handler;
import com.tencent.assistant.utils.ah;

/* compiled from: ProGuard */
public abstract class k {

    /* renamed from: a  reason: collision with root package name */
    private static Object f3764a = new Object();
    private static Handler b = null;

    public static synchronized Handler a() {
        Handler handler;
        synchronized (k.class) {
            if (b == null) {
                b = ah.a("DownLogicHandler");
            }
            handler = b;
        }
        return handler;
    }
}
