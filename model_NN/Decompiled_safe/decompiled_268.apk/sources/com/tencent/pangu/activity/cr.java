package com.tencent.pangu.activity;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.net.c;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class cr extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ StartPopWindowActivity f3369a;

    cr(StartPopWindowActivity startPopWindowActivity) {
        this.f3369a = startPopWindowActivity;
    }

    public void onTMAClick(View view) {
        this.f3369a.C();
        this.f3369a.t();
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3369a.K, 200);
        c.k();
        if (c.e()) {
            buildSTInfo.slotId = a.a("04", "001");
        } else {
            buildSTInfo.slotId = a.a("04", "002");
        }
        if (this.f3369a.J != null && this.f3369a.F.a() == this.f3369a.J.size()) {
            buildSTInfo.actionId = 200;
            buildSTInfo.status = "02";
        }
        return buildSTInfo;
    }
}
