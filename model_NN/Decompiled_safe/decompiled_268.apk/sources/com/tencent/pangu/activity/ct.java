package com.tencent.pangu.activity;

import android.view.View;
import android.widget.TextView;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import java.util.ArrayList;

/* compiled from: ProGuard */
class ct extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ StartPopWindowActivity f3371a;

    ct(StartPopWindowActivity startPopWindowActivity) {
        this.f3371a = startPopWindowActivity;
    }

    public void onTMAClick(View view) {
        boolean z = false;
        if (this.f3371a.F != null && this.f3371a.F.getCount() != 0) {
            ArrayList<Boolean> c = this.f3371a.F.c();
            if (this.f3371a.C.isSelected()) {
                for (int i = 0; i < c.size(); i++) {
                    if (c.get(i).booleanValue()) {
                        this.f3371a.F.a(i);
                    }
                }
            } else {
                for (int i2 = 0; i2 < c.size(); i2++) {
                    if (!c.get(i2).booleanValue()) {
                        this.f3371a.F.a(i2);
                    }
                }
            }
            TextView h = this.f3371a.C;
            if (!this.f3371a.C.isSelected()) {
                z = true;
            }
            h.setSelected(z);
            this.f3371a.u();
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3371a.K, 200);
        buildSTInfo.slotId = a.a("05_", "001");
        return buildSTInfo;
    }
}
