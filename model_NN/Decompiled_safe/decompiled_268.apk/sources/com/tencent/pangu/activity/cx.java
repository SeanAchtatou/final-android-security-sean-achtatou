package com.tencent.pangu.activity;

import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class cx implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    public boolean f3375a = false;
    long b = 0;
    int c = this.g.getResources().getDimensionPixelSize(R.dimen.banner_img_size);
    int d;
    int e = 20;
    int f = 800;
    final /* synthetic */ StartPopWindowActivity g;
    private long h = -1;
    private int i = 0;
    private Interpolator j = new DecelerateInterpolator();

    public cx(StartPopWindowActivity startPopWindowActivity) {
        this.g = startPopWindowActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public void run() {
        if (this.h == -1) {
            this.f3375a = true;
            this.h = System.currentTimeMillis();
        } else {
            this.b = ((System.currentTimeMillis() - this.h) * 1000) / ((long) this.f);
            this.b = Math.max(Math.min(this.b, 1000L), 0L);
            if (this.b > 500) {
                this.d = Math.round(((float) this.c) * this.j.getInterpolation(((float) (1000 - this.b)) / 1000.0f));
            } else {
                this.d = Math.round(((float) this.c) * this.j.getInterpolation(((float) this.b) / 1000.0f));
            }
            if (this.g.z != null) {
                this.g.z.scrollTo(0, this.i + this.d);
            }
        }
        if (this.b >= 1000) {
            this.f3375a = false;
        } else if (this.f3375a && this.g.z != null) {
            this.g.z.postDelayed(this, (long) this.e);
        }
    }
}
