package com.tencent.pangu.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.socialcontact.comment.CommentReplyListActivity;
import com.tencent.pangu.component.l;

/* compiled from: ProGuard */
class c implements l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppDetailActivityV5 f3351a;

    c(AppDetailActivityV5 appDetailActivityV5) {
        this.f3351a = appDetailActivityV5;
    }

    public void a(boolean z, long j) {
        if (!z) {
            this.f3351a.X.a(true);
        } else {
            com.tencent.assistantv2.st.l.a(new STInfoV2(STConst.ST_PAGE_APP_DETAIL_COMMENT, this.f3351a.bh, 0, STConst.ST_DEFAULT_SLOT, 200));
        }
        this.f3351a.X.a(z, AppConst.AppState.INSTALLED, j, this.f3351a.al, this.f3351a.am, this.f3351a.an, false);
    }

    public void a(long j, String str, int i, int i2) {
        XLog.d("commentdetail", j + "; con" + (str != null ? str : Constants.STR_EMPTY));
        if (j == -1) {
            this.f3351a.X.a(false, AppConst.AppState.INSTALLED, j, str, i, i2, false);
            if (this.f3351a.af != null && this.f3351a.af.f3081a) {
                this.f3351a.af.f3081a = false;
                this.f3351a.af.show();
            }
            this.f3351a.X.a(true);
        } else if (j == -2) {
            if (this.f3351a.af != null) {
                if (this.f3351a.af.f3081a) {
                    this.f3351a.af.f3081a = false;
                }
                if (this.f3351a.af.isShowing()) {
                    this.f3351a.af.dismiss();
                }
            }
            this.f3351a.X.a(true);
        } else {
            this.f3351a.X.a(true, AppConst.AppState.INSTALLED, j, str, i, i2, false);
            if (this.f3351a.af != null && this.f3351a.af.f3081a) {
                this.f3351a.af.f3081a = false;
                d dVar = new d(this);
                Context baseContext = AstApp.i().getBaseContext();
                dVar.titleRes = baseContext.getString(R.string.comment_hascomment_title);
                dVar.contentRes = baseContext.getString(R.string.comment_hascomment_content);
                dVar.btnTxtRes = baseContext.getString(R.string.comment_hascomment_button);
                dVar.blockCaller = true;
                DialogUtils.show1BtnDialog(dVar);
            }
            this.f3351a.X.a(false);
        }
    }

    public void a(Bundle bundle) {
        Intent intent = new Intent(this.f3351a, CommentReplyListActivity.class);
        intent.putExtras(bundle);
        if (!(this.f3351a instanceof Activity)) {
            intent.addFlags(268435456);
        }
        this.f3351a.startActivityForResult(intent, 100);
    }
}
