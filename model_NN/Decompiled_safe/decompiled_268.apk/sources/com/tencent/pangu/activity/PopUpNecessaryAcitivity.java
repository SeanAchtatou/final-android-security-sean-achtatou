package com.tencent.pangu.activity;

import android.os.Bundle;
import android.support.v4.view.ExViewPager;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.StrinptTipsView;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.m;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.GetPopupNecessaryResponse;
import com.tencent.assistant.protocol.jce.PopupScene;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.at;
import com.tencent.assistant.utils.bm;
import com.tencent.assistantv2.activity.MainActivity;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.component.PopUpContentView;
import com.tencent.pangu.component.w;
import com.tencent.pangu.component.x;
import com.tencent.pangu.component.y;
import com.tencent.pangu.module.timer.RecoverAppListReceiver;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
public class PopUpNecessaryAcitivity extends BaseActivity implements Animation.AnimationListener, x {
    /* access modifiers changed from: private */
    public int A = 0;
    private long B = 0;
    private int C = 0;
    private GetPopupNecessaryResponse D = new GetPopupNecessaryResponse();
    /* access modifiers changed from: private */
    public int E = 0;
    private boolean F = false;
    private ViewPager.OnPageChangeListener G = new bn(this);
    private OnTMAParamExClickListener H = new bo(this);
    private View n;
    private ExViewPager u;
    /* access modifiers changed from: private */
    public StrinptTipsView v;
    private Button w;
    private RelativeLayout x;
    /* access modifiers changed from: private */
    public y y;
    private ArrayList<PopupScene> z = new ArrayList<>();

    /* access modifiers changed from: protected */
    public boolean g() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.start_pop_new_window_layout);
        getWindow().setLayout(-1, -1);
        u();
        w();
        x();
        this.y.a(0);
        Animation loadAnimation = AnimationUtils.loadAnimation(this, R.anim.pop_up_necessary_up);
        loadAnimation.setAnimationListener(this);
        this.n.startAnimation(loadAnimation);
        this.F = true;
    }

    public void finish() {
        super.finish();
    }

    public void t() {
        Animation loadAnimation = AnimationUtils.loadAnimation(this, R.anim.pop_up_necessary_down);
        loadAnimation.setAnimationListener(this);
        this.n.startAnimation(loadAnimation);
        this.F = false;
    }

    /* access modifiers changed from: protected */
    public void u() {
        this.D = (GetPopupNecessaryResponse) getIntent().getSerializableExtra("param_key_response_data");
        if (this.D != null && this.D.d.size() > 0) {
            this.z = this.D.d;
        }
        this.A = this.z.size();
        m.a().b(Constants.STR_EMPTY, "key_re_app_list_state", Integer.valueOf(RecoverAppListReceiver.RecoverAppListState.NOMAL.ordinal()));
        MainActivity.z = RecoverAppListReceiver.RecoverAppListState.NOMAL.ordinal();
    }

    private void w() {
        this.n = findViewById(R.id.page);
        this.v = (StrinptTipsView) findViewById(R.id.point);
        this.v.setSelect(this.A, 0, 1);
        this.x = (RelativeLayout) findViewById(R.id.bottom_view);
        this.x.setOnTouchListener(new bm(this));
        this.u = (ExViewPager) findViewById(R.id.content_view);
        this.y = new y();
        this.u.setAdapter(this.y);
        this.u.setOnPageChangeListener(this.G);
        this.w = (Button) findViewById(R.id.download_btn);
        this.w.setEnabled(true);
        this.w.setOnClickListener(this.H);
    }

    private void x() {
        int i;
        boolean z2;
        ArrayList arrayList = new ArrayList();
        Iterator<PopupScene> it = this.z.iterator();
        int i2 = 0;
        while (it.hasNext()) {
            PopupScene next = it.next();
            if (next != null) {
                PopUpContentView popUpContentView = new PopUpContentView(this, this, i2);
                ArrayList<SimpleAppModel> b = k.b(next.a());
                if (i2 == 0) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                popUpContentView.a(next.c, next.f1437a, z2, b);
                arrayList.add(popUpContentView);
                i = i2 + 1;
            } else {
                i = i2;
            }
            this.y.a(arrayList);
            this.B = this.y.a();
            this.C = this.y.b();
            this.w.setText(getString(R.string.pop_up_nessary_download_btn_total_size, new Object[]{Integer.valueOf(this.C), at.a(this.B)}));
            i2 = i;
        }
    }

    public void a(long j, boolean z2) {
        b(j, z2);
    }

    private void b(long j, boolean z2) {
        if (z2) {
            this.C++;
            this.B += j;
        } else {
            this.C--;
            this.B -= j;
        }
        if (this.C == 0) {
            this.w.setEnabled(false);
            this.w.setTextColor(getResources().getColor(R.color.state_disable));
        } else {
            this.w.setEnabled(true);
            this.w.setTextColor(getResources().getColor(R.color.white));
        }
        this.w.setText(getString(R.string.pop_up_nessary_download_btn_total_size, new Object[]{Integer.valueOf(this.C), at.a(this.B)}));
    }

    /* access modifiers changed from: private */
    public void y() {
        if (this.n != null) {
            this.n.postDelayed(new bp(this), 500);
        }
        t();
    }

    public void overridePendingTransition(int i, int i2) {
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        z();
        t();
        return false;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        z();
        t();
        return super.onTouchEvent(motionEvent);
    }

    public void v() {
        t();
    }

    public boolean k() {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean i() {
        return false;
    }

    public int f() {
        return STConst.ST_PAGE_POP_UP_NECESSRAY_CONTENT_VEIW + this.E;
    }

    /* access modifiers changed from: private */
    public void a(w wVar) {
        STInfoV2 sTInfoV2;
        if (wVar != null && (sTInfoV2 = new STInfoV2(wVar.c, STConst.ST_DEFAULT_SLOT, 2000, STConst.ST_DEFAULT_SLOT, 900)) != null) {
            sTInfoV2.extraData = wVar.f3744a != null ? wVar.f3744a.c + "|" + wVar.f3744a.g : Constants.STR_EMPTY;
            sTInfoV2.slotId = b(wVar.b);
            sTInfoV2.updateWithSimpleAppModel(wVar.f3744a);
            sTInfoV2.isImmediately = false;
            l.a(sTInfoV2);
        }
    }

    /* access modifiers changed from: private */
    public String b(int i) {
        return "03_" + bm.a(i + 1);
    }

    private void z() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this, 200);
        buildSTInfo.slotId = a.a("06", "001");
        l.a(buildSTInfo);
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        if (!this.F) {
            this.n.setVisibility(8);
            finish();
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }
}
