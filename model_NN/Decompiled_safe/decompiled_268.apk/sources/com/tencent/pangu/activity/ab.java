package com.tencent.pangu.activity;

import android.content.DialogInterface;
import android.view.KeyEvent;

/* compiled from: ProGuard */
class ab implements DialogInterface.OnKeyListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppTreasureBoxActivity f3303a;

    ab(AppTreasureBoxActivity appTreasureBoxActivity) {
        this.f3303a = appTreasureBoxActivity;
    }

    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        if (i != 4) {
            return false;
        }
        if (keyEvent.getAction() != 1) {
            return true;
        }
        if (this.f3303a.B != null) {
            this.f3303a.B.dismiss();
        }
        if (this.f3303a.y == null) {
            return true;
        }
        this.f3303a.y.setVisibility(0);
        return true;
    }
}
