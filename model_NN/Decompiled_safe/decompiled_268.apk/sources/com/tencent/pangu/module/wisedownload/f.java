package com.tencent.pangu.module.wisedownload;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.m;
import com.tencent.pangu.module.wisedownload.condition.b;
import com.tencent.pangu.module.wisedownload.condition.i;

/* compiled from: ProGuard */
public class f extends b {
    public f() {
        a();
    }

    public void a() {
        this.d = new i(this);
    }

    /* access modifiers changed from: protected */
    public void c() {
        this.b = new b(this);
    }

    public boolean b() {
        return m.a().f(AppConst.WISE_DOWNLOAD_SWITCH_TYPE.NEW_DOWNLOAD);
    }

    public void a(p pVar) {
        int i;
        int i2;
        int i3;
        boolean z;
        boolean z2;
        int i4 = 0;
        if (pVar != null) {
            boolean e = e();
            if (this.d == null || !(this.d instanceof i)) {
                i = 0;
                i2 = 0;
                i3 = 0;
                z = false;
                z2 = false;
            } else {
                i iVar = (i) this.d;
                z2 = iVar.i();
                z = iVar.j();
                i3 = iVar.m();
                i2 = iVar.n();
                i = iVar.k();
                i4 = iVar.l();
            }
            pVar.b(e, z2, z, i3, i2, i, i4);
        }
    }
}
