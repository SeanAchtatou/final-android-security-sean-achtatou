package com.tencent.pangu.module.wisedownload;

import com.tencent.pangu.module.wisedownload.condition.ThresholdCondition;

/* compiled from: ProGuard */
/* synthetic */ class c {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f3957a = new int[ThresholdCondition.CONDITION_TRIGGER_ACTION.values().length];
    static final /* synthetic */ int[] b = new int[ThresholdCondition.CONDITION_TYPE.values().length];

    static {
        try {
            b[ThresholdCondition.CONDITION_TYPE.CONDITION_SWITCH.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            b[ThresholdCondition.CONDITION_TYPE.CONDITION_TIME.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            b[ThresholdCondition.CONDITION_TYPE.CONDITION_PRIMARY.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            b[ThresholdCondition.CONDITION_TYPE.CONDITION_OTHER.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f3957a[ThresholdCondition.CONDITION_TRIGGER_ACTION.SCREEN_ON.ordinal()] = 1;
        } catch (NoSuchFieldError e5) {
        }
        try {
            f3957a[ThresholdCondition.CONDITION_TRIGGER_ACTION.USER_PRESENT.ordinal()] = 2;
        } catch (NoSuchFieldError e6) {
        }
        try {
            f3957a[ThresholdCondition.CONDITION_TRIGGER_ACTION.CONNECT.ordinal()] = 3;
        } catch (NoSuchFieldError e7) {
        }
        try {
            f3957a[ThresholdCondition.CONDITION_TRIGGER_ACTION.CONNECT_CHANGED.ordinal()] = 4;
        } catch (NoSuchFieldError e8) {
        }
        try {
            f3957a[ThresholdCondition.CONDITION_TRIGGER_ACTION.DISCONNECT.ordinal()] = 5;
        } catch (NoSuchFieldError e9) {
        }
        try {
            f3957a[ThresholdCondition.CONDITION_TRIGGER_ACTION.BATTERY_CHANGED.ordinal()] = 6;
        } catch (NoSuchFieldError e10) {
        }
    }
}
