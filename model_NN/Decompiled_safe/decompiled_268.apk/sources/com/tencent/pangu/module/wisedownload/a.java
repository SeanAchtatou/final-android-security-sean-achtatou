package com.tencent.pangu.module.wisedownload;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.m;
import com.tencent.pangu.module.wisedownload.condition.e;
import com.tencent.pangu.module.wisedownload.condition.o;

/* compiled from: ProGuard */
public class a extends b {
    public a() {
        a();
    }

    public void a() {
        this.d = new o(this);
    }

    /* access modifiers changed from: protected */
    public void c() {
        this.b = new e(this);
    }

    public boolean b() {
        return m.a().f(AppConst.WISE_DOWNLOAD_SWITCH_TYPE.UPDATE);
    }

    public void a(p pVar) {
        int i;
        int i2;
        int i3;
        boolean z;
        boolean z2;
        int i4 = 0;
        if (pVar != null) {
            boolean e = e();
            if (this.d == null || !(this.d instanceof o)) {
                i = 0;
                i2 = 0;
                i3 = 0;
                z = false;
                z2 = false;
            } else {
                o oVar = (o) this.d;
                z2 = oVar.b();
                z = oVar.h();
                i3 = oVar.i();
                i2 = oVar.j();
                i = oVar.k();
                i4 = oVar.l();
            }
            pVar.a(e, z2, z, i3, i2, i, i4);
        }
    }
}
