package com.tencent.pangu.mediadownload;

import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.downloadsdk.DownloadManager;
import com.tencent.pangu.model.AbstractDownloadInfo;

/* compiled from: ProGuard */
class aa implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ r f3853a;

    aa(r rVar) {
        this.f3853a = rVar;
    }

    public void run() {
        for (q next : this.f3853a.a()) {
            DownloadManager.a().a(8, next.m);
            if (next.s == AbstractDownloadInfo.DownState.DOWNLOADING || next.s == AbstractDownloadInfo.DownState.QUEUING) {
                next.s = AbstractDownloadInfo.DownState.FAIL;
                this.f3853a.c.sendMessage(this.f3853a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_FAIL, next));
                this.f3853a.b.a(next);
            }
        }
    }
}
