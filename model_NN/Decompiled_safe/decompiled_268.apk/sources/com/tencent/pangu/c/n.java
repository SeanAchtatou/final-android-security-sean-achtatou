package com.tencent.pangu.c;

import android.os.Message;
import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.utils.XLog;
import com.tencent.nucleus.socialcontact.login.l;

/* compiled from: ProGuard */
class n implements d {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ e f3483a;

    n(e eVar) {
        this.f3483a = eVar;
    }

    public void a(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        Message obtainMessage = AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_SHARE_SUCCESS);
        obtainMessage.obj = 2;
        AstApp.i().j().sendMessage(obtainMessage);
        XLog.i("xjp", "*** share succeeded ***");
    }

    public void a(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        XLog.i("Jie", "share fail:errcode=" + i2);
        l.c(i2);
        Message obtainMessage = AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_SHARE_FAIL);
        obtainMessage.obj = 2;
        AstApp.i().j().sendMessage(obtainMessage);
    }
}
