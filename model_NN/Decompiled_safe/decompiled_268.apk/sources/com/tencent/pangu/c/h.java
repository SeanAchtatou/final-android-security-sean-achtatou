package com.tencent.pangu.c;

import android.app.Dialog;
import java.lang.ref.WeakReference;

/* compiled from: ProGuard */
class h implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ e f3477a;

    h(e eVar) {
        this.f3477a = eVar;
    }

    public void run() {
        for (WeakReference weakReference : e.g) {
            Dialog dialog = (Dialog) weakReference.get();
            if (!(dialog == null || this.f3477a.e == null || this.f3477a.e.get() == null || this.f3477a.e.get().isFinishing())) {
                try {
                    dialog.dismiss();
                } catch (Throwable th) {
                }
            }
        }
    }
}
