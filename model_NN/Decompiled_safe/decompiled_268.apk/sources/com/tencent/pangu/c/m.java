package com.tencent.pangu.c;

import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.component.bg;

/* compiled from: ProGuard */
class m implements CallbackHelper.Caller<d> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f3482a;
    final /* synthetic */ int b;
    final /* synthetic */ JceStruct c;
    final /* synthetic */ JceStruct d;
    final /* synthetic */ e e;

    m(e eVar, int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        this.e = eVar;
        this.f3482a = i;
        this.b = i2;
        this.c = jceStruct;
        this.d = jceStruct2;
    }

    /* renamed from: a */
    public void call(d dVar) {
        XLog.i("ShareEngine", "share fail");
        dVar.a(this.f3482a, this.b, this.c, this.d);
        if (this.e.n) {
            bg.a(AstApp.i().getApplicationContext(), (int) R.string.share_fail, 0);
        }
    }
}
