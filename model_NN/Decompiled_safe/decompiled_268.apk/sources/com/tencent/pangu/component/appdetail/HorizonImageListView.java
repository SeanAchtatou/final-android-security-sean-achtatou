package com.tencent.pangu.component.appdetail;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.utils.bm;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class HorizonImageListView extends HorizontalScrollView implements TXImageView.ITXImageViewListener {

    /* renamed from: a  reason: collision with root package name */
    boolean f3553a = true;
    private LinearLayout b;
    /* access modifiers changed from: private */
    public ArrayList<String> c;
    /* access modifiers changed from: private */
    public aw d;
    private View.OnClickListener e = null;
    private int f = 0;
    private int g;
    private int h;
    private int i;
    private Context j;
    private int k = 4;
    private ba l;

    public HorizonImageListView(Context context) {
        super(context);
        setHorizontalScrollBarEnabled(false);
    }

    public HorizonImageListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setHorizontalScrollBarEnabled(false);
        this.j = context;
        this.b = new LinearLayout(context);
        addView(this.b, new ViewGroup.LayoutParams(-1, -1));
        this.b.setOrientation(0);
        this.b.setGravity(17);
        this.c = new ArrayList<>();
        this.e = new av(this, context);
    }

    public void a(int i2, ArrayList<String> arrayList, int i3, int i4) {
        boolean z;
        this.c.clear();
        this.b.removeAllViews();
        this.i = i2;
        this.g = i3;
        this.i = i2;
        this.h = i4;
        int i5 = 0;
        while (i5 < arrayList.size()) {
            if (i5 < arrayList.size() - 1) {
                String str = arrayList.get(i5);
                if (i5 < i2) {
                    z = true;
                } else {
                    z = false;
                }
                a(str, i3, i4, z);
            } else {
                a(arrayList.get(i5), 0, i4, i5 < i2);
            }
            i5++;
        }
        this.b.requestLayout();
    }

    public void a(String str, int i2, int i3, boolean z) {
        ViewGroup.MarginLayoutParams marginLayoutParams;
        int i4;
        if (str != null) {
            this.c.add(str);
            TXImageView tXImageView = new TXImageView(getContext());
            tXImageView.setId(this.k);
            this.k++;
            if (!z) {
                this.b.addView(tXImageView, new LinearLayout.LayoutParams(-2, 300));
            } else {
                FrameLayout frameLayout = new FrameLayout(getContext());
                tXImageView.setId(-1);
                this.k++;
                ImageView imageView = new ImageView(getContext());
                imageView.setImageResource(R.drawable.common_icon_gameappdetail_playvideo_big);
                frameLayout.addView(tXImageView, new FrameLayout.LayoutParams(-2, 300, 17));
                frameLayout.addView(imageView, new FrameLayout.LayoutParams(-2, -2, 17));
                tXImageView.setTag(R.drawable.common_icon_gameappdetail_playvideo_big, frameLayout);
                this.b.addView(frameLayout, new LinearLayout.LayoutParams(-2, 300));
            }
            tXImageView.setTag(str);
            tXImageView.setTag(R.id.tma_st_slot_tag, "05_" + bm.a(this.c.size()));
            tXImageView.setOnClickListener(this.e);
            tXImageView.setListener(this);
            tXImageView.updateImageView(str, i3, TXImageView.TXImageViewType.NETWORK_IMAGE_MIDDLE);
            if (!z) {
                marginLayoutParams = (ViewGroup.MarginLayoutParams) tXImageView.getLayoutParams();
                marginLayoutParams.rightMargin = i2;
            } else {
                marginLayoutParams = (ViewGroup.MarginLayoutParams) ((FrameLayout) tXImageView.getTag(R.drawable.common_icon_gameappdetail_playvideo_big)).getLayoutParams();
                marginLayoutParams.rightMargin = i2;
            }
            int i5 = this.f;
            Drawable drawable = tXImageView.getDrawable();
            if (drawable != null) {
                i4 = (int) (((((float) drawable.getMinimumWidth()) * 1.0f) / ((float) drawable.getMinimumHeight())) * 1.0f * ((float) i5) * 1.0f);
            } else {
                i4 = 0;
            }
            marginLayoutParams.width = i4;
            marginLayoutParams.height = i5;
            tXImageView.invalidate();
            if (z) {
                ViewGroup.MarginLayoutParams marginLayoutParams2 = (ViewGroup.MarginLayoutParams) ((View) tXImageView.getTag(R.drawable.common_icon_gameappdetail_playvideo_big)).getLayoutParams();
                marginLayoutParams2.width = i4;
                marginLayoutParams2.height = i5;
                ((View) tXImageView.getTag(R.drawable.common_icon_gameappdetail_playvideo_big)).setLayoutParams(marginLayoutParams2);
                ViewGroup.MarginLayoutParams marginLayoutParams3 = (ViewGroup.MarginLayoutParams) tXImageView.getLayoutParams();
                marginLayoutParams3.width = i4;
                marginLayoutParams3.height = i5;
                tXImageView.setLayoutParams(marginLayoutParams3);
            }
        }
    }

    public void a(int i2) {
        this.f = i2;
    }

    public void a(aw awVar) {
        this.d = awVar;
    }

    public void onTXImageViewLoadImageFinish(TXImageView tXImageView, Bitmap bitmap) {
        if (bitmap != null && !bitmap.isRecycled()) {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) tXImageView.getLayoutParams();
            int i2 = this.f;
            marginLayoutParams.width = (int) (((((float) bitmap.getWidth()) * 1.0f) / ((float) bitmap.getHeight())) * 1.0f * ((float) i2) * 1.0f);
            marginLayoutParams.height = i2;
            if (tXImageView.getLayoutParams() instanceof FrameLayout.LayoutParams) {
                marginLayoutParams.width = (int) (((((float) bitmap.getWidth()) * 1.0f) / ((float) bitmap.getHeight())) * 1.0f * ((float) i2) * 1.0f);
                marginLayoutParams.height = i2;
                ((FrameLayout) tXImageView.getTag(R.drawable.common_icon_gameappdetail_playvideo_big)).setLayoutParams((LinearLayout.LayoutParams) ((FrameLayout) tXImageView.getTag(R.drawable.common_icon_gameappdetail_playvideo_big)).getLayoutParams());
            }
            tXImageView.invalidate();
            if (this.b != null) {
                this.b.requestLayout();
            }
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return super.onInterceptTouchEvent(motionEvent);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return super.onTouchEvent(motionEvent);
    }

    public void a(ba baVar) {
        this.l = baVar;
    }

    public boolean a() {
        return this.f3553a;
    }

    /* access modifiers changed from: protected */
    public void onScrollChanged(int i2, int i3, int i4, int i5) {
        if (getChildAt(getChildCount() - 1).getRight() - (getWidth() + getScrollX()) == 0) {
            this.f3553a = false;
            return;
        }
        this.f3553a = true;
        super.onScrollChanged(i2, i3, i4, i5);
    }

    public boolean b() {
        View childAt = getChildAt(0);
        if (childAt == null || getWidth() >= childAt.getWidth()) {
            return false;
        }
        return true;
    }

    public void c() {
        boolean z;
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(this.c);
        this.c.clear();
        int i2 = 0;
        while (i2 < arrayList.size()) {
            if (i2 < arrayList.size() - 1) {
                String str = (String) arrayList.get(i2);
                int i3 = this.g;
                int i4 = this.h;
                if (i2 < this.i) {
                    z = true;
                } else {
                    z = false;
                }
                a(str, i3, i4, z);
            } else {
                a((String) arrayList.get(i2), 0, this.h, i2 < this.i);
            }
            i2++;
        }
        this.b.requestLayout();
    }

    public void d() {
        this.b.removeAllViews();
    }
}
