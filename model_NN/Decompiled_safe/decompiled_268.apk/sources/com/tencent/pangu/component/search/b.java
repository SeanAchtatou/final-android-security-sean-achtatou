package com.tencent.pangu.component.search;

import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.pangu.module.a.k;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
class b extends k {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ NativeSearchResultPage f3700a;

    private b(NativeSearchResultPage nativeSearchResultPage) {
        this.f3700a = nativeSearchResultPage;
    }

    /* synthetic */ b(NativeSearchResultPage nativeSearchResultPage, a aVar) {
        this(nativeSearchResultPage);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.component.search.NativeSearchResultPage.a(com.tencent.pangu.component.search.NativeSearchResultPage, boolean):boolean
     arg types: [com.tencent.pangu.component.search.NativeSearchResultPage, int]
     candidates:
      com.tencent.pangu.component.search.NativeSearchResultPage.a(long, java.lang.String):void
      com.tencent.pangu.component.search.NativeSearchResultPage.a(java.lang.String, int):void
      com.tencent.pangu.component.search.ISearchResultPage.a(java.lang.String, int):void
      com.tencent.pangu.component.search.NativeSearchResultPage.a(com.tencent.pangu.component.search.NativeSearchResultPage, boolean):boolean */
    public void a(int i, int i2, boolean z, int i3, ArrayList<String> arrayList, boolean z2, List<com.tencent.pangu.model.b> list, long j, String str, int i4) {
        this.f3700a.a(false);
        if (i2 == 0) {
            this.f3700a.f = j;
            if (this.f3700a.r == null || !(this.f3700a.r.b().e() == null || this.f3700a.r.b().e().size() == 0)) {
                boolean unused = this.f3700a.q = false;
                this.f3700a.a(z, i3, arrayList, z2, list, j, str, i4);
            } else {
                boolean unused2 = this.f3700a.q = true;
                this.f3700a.o();
            }
        } else {
            boolean unused3 = this.f3700a.q = false;
            this.f3700a.a(i2, z, z2);
        }
        if (z) {
            this.f3700a.a(j, this.f3700a.f3690a);
        }
    }

    public void a(ArrayList<SimpleAppModel> arrayList, ArrayList<String> arrayList2, int i) {
    }
}
