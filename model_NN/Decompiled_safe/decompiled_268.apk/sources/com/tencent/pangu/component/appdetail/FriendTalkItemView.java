package com.tencent.pangu.component.appdetail;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.protocol.jce.CommentDetail;
import com.tencent.assistant.utils.bm;

/* compiled from: ProGuard */
public class FriendTalkItemView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private Context f3551a;
    private TXAppIconView b;
    private TextView c;
    private TextView d;
    private TextView e;
    private TextView f;
    private ImageView g;

    public FriendTalkItemView(Context context) {
        super(context);
        a(context);
    }

    public FriendTalkItemView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public FriendTalkItemView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        a(context);
    }

    private void a(Context context) {
        this.f3551a = context;
        LayoutInflater.from(this.f3551a).inflate((int) R.layout.appdetail_friend_talk_item_view, this);
        this.b = (TXAppIconView) findViewById(R.id.picture);
        this.c = (TextView) findViewById(R.id.myorfriends);
        this.d = (TextView) findViewById(R.id.nick_name);
        this.e = (TextView) findViewById(R.id.likecount);
        this.f = (TextView) findViewById(R.id.content);
        this.g = (ImageView) findViewById(R.id.divider);
    }

    public FriendTalkItemView a(CommentDetail commentDetail) {
        this.b.updateImageView(commentDetail.i, R.drawable.common_icon_useravarta_default, TXImageView.TXImageViewType.ROUND_IMAGE);
        if (commentDetail.l) {
            this.d.setText((int) R.string.comment_nick_my_comment);
            this.d.setTextColor(Color.parseColor("#ff7a0c"));
            this.c.setVisibility(8);
        } else if (TextUtils.isEmpty(commentDetail.c)) {
            this.d.setText((int) R.string.comment_default_nick_name);
            this.c.setVisibility(8);
        } else if (!commentDetail.c.startsWith("(好友)")) {
            this.d.setText(commentDetail.c);
            this.c.setVisibility(8);
        } else {
            String substring = commentDetail.c.substring(4);
            if (substring.length() > 8) {
                substring = substring.substring(0, 8) + "...";
            }
            if (TextUtils.isEmpty(substring)) {
                this.d.setText((int) R.string.comment_default_nick_name);
                this.c.setVisibility(8);
            } else {
                this.d.setText(substring);
                this.d.setTextColor(Color.parseColor("#ff7a0c"));
                this.c.setVisibility(0);
            }
        }
        this.e.setText(bm.a(commentDetail.b(), 2) + "已赞");
        this.f.setText(commentDetail.g);
        return this;
    }
}
