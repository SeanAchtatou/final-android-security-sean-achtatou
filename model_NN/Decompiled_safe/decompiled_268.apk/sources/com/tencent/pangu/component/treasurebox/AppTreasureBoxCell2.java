package com.tencent.pangu.component.treasurebox;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import com.tencent.android.qqdownloader.R;
import com.tencent.pangu.model.h;

/* compiled from: ProGuard */
public class AppTreasureBoxCell2 extends AppTreasureBoxCell {
    private Button d;

    public AppTreasureBoxCell2(Context context) {
        super(context);
        a(context);
    }

    public AppTreasureBoxCell2(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    private void a(Context context) {
        LayoutInflater.from(context).inflate((int) R.layout.app_treasure_box_item_error, this);
        this.d = (Button) findViewById(R.id.btn);
        this.d.setOnClickListener(this);
    }

    public void onClick(View view) {
        if (view == this.d && this.c != null) {
            this.c.t();
        }
    }

    public void a() {
        this.f3730a = false;
    }

    public void b() {
        this.f3730a = true;
    }

    public void a(h hVar) {
    }
}
