package com.tencent.pangu.component.appdetail;

import android.os.Bundle;
import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.protocol.jce.AppExCfg;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.activity.AppDetailActivityV5;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class u extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppExCfg f3621a;
    final /* synthetic */ int b;
    final /* synthetic */ AppdetailGiftView c;

    u(AppdetailGiftView appdetailGiftView, AppExCfg appExCfg, int i) {
        this.c = appdetailGiftView;
        this.f3621a = appExCfg;
        this.b = i;
    }

    public void onTMAClick(View view) {
        if (this.f3621a.d != null) {
            Bundle bundle = new Bundle();
            bundle.putSerializable("com.tencent.assistant.ACTION_URL", this.f3621a.d);
            if (this.c.b instanceof BaseActivity) {
                bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, ((BaseActivity) this.c.b).f());
            }
            b.b(this.c.b, this.f3621a.d.f1125a, bundle);
        }
    }

    public STInfoV2 getStInfo() {
        if (!(this.c.b instanceof AppDetailActivityV5)) {
            return null;
        }
        STInfoV2 t = ((AppDetailActivityV5) this.c.b).t();
        t.slotId = a.a(this.c.d == 1 ? Constants.VIA_ACT_TYPE_NINETEEN : "18", this.b + 1);
        t.actionId = 200;
        return t;
    }
}
