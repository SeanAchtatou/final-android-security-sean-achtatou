package com.tencent.pangu.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.HeaderViewListAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.TXExpandableListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.m;
import com.tencent.assistant.module.callback.g;
import com.tencent.assistant.module.y;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistantv2.activity.a;
import com.tencent.assistantv2.st.business.CostTimeSTManager;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.game.activity.n;
import com.tencent.nucleus.manager.component.SwitchButton;
import com.tencent.pangu.adapter.al;
import com.tencent.pangu.component.appdetail.ad;

/* compiled from: ProGuard */
public class RankCustomizeListView extends TXExpandableListView implements ITXRefreshListViewListener {
    /* access modifiers changed from: private */
    public static String[] G;
    /* access modifiers changed from: private */
    public ai A;
    private int B = 1;
    /* access modifiers changed from: private */
    public ListViewScrollListener C;
    private int D;
    private int E;
    private long F = 0;
    private final int H = 1;
    private final int I = 2;
    private final String J = "isFirstPage";
    private final String K = "hasNext";
    private final String L = "key_Appid";
    private final String M = "key_data";
    /* access modifiers changed from: private */
    public ViewInvalidateMessageHandler N = new ad(this);
    LinearLayout f;
    View g;
    SwitchButton h;
    ad i;
    public boolean j = false;
    TextView k;
    boolean[] l = new boolean[10];
    int u = 0;
    public boolean v = false;
    protected a w;
    g x = new aa(this);
    /* access modifiers changed from: private */
    public y y = null;
    /* access modifiers changed from: private */
    public al z = null;

    public void a(ListViewScrollListener listViewScrollListener) {
        this.C = listViewScrollListener;
        setOnScrollListener(this.C);
    }

    public void a(ai aiVar) {
        this.A = aiVar;
    }

    public void a(y yVar, int i2, int i3) {
        this.y = yVar;
        this.y.register(this.x);
        this.D = i2;
        this.E = i3;
        setRefreshListViewListener(this);
    }

    public RankCustomizeListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setGroupIndicator(null);
        setDivider(null);
        setChildDivider(null);
        setSelector(R.drawable.transparent_selector);
        setRefreshListViewListener(this);
        setOnGroupClickListener(new ab(this));
        r();
    }

    public ExpandableListView c() {
        return (ExpandableListView) this.s;
    }

    private void r() {
        ExpandableListAdapter expandableListAdapter = ((ExpandableListView) this.s).getExpandableListAdapter();
        if (expandableListAdapter instanceof HeaderViewListAdapter) {
            this.z = (al) ((HeaderViewListAdapter) expandableListAdapter).getWrappedAdapter();
        } else {
            this.z = (al) ((ExpandableListView) this.s).getExpandableListAdapter();
        }
        if (this.z != null && G == null) {
            G = AstApp.i().getResources().getStringArray(R.array.logo_tips3);
        }
    }

    public void c(int i2, int i3) {
        if (this.z == null) {
            r();
        }
        this.D = i2;
        this.E = i3;
        if (this.z.getGroupCount() <= 0 || n()) {
            this.A.a();
            this.y.b();
            d(this.D, this.E);
            return;
        }
        this.C.sendMessage(new ViewInvalidateMessage(2, null, this.N));
        if (this.E > 0) {
            c().setSelection(0);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.component.RankCustomizeListView.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.assistant.component.txscrollview.TXScrollViewBase.a(android.content.Context, android.view.View):void
      com.tencent.assistant.component.txscrollview.TXScrollViewBase.a(int, int):void
      com.tencent.pangu.component.RankCustomizeListView.a(boolean, boolean):void */
    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        if (scrollState == TXScrollViewBase.ScrollState.ScrollState_FromEnd) {
            TemporaryThreadManager.get().start(new ac(this));
        } else if (scrollState == TXScrollViewBase.ScrollState.ScrollState_FromStart && this.g != null) {
            a(true, true);
        }
    }

    public void d() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.component.RankCustomizeListView.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.assistant.component.txscrollview.TXScrollViewBase.a(android.content.Context, android.view.View):void
      com.tencent.assistant.component.txscrollview.TXScrollViewBase.a(int, int):void
      com.tencent.pangu.component.RankCustomizeListView.a(boolean, boolean):void */
    public void g() {
        if (this.h != null) {
            this.h.a(com.tencent.assistantv2.manager.a.a().b().c());
        }
        if (this.z != null && this.z.getGroupCount() > 0) {
            this.z.a();
        }
        if (this.h != null && this.z != null && this.z.getGroupCount() > 0) {
            if (!this.v) {
                if (m.a().al()) {
                    a(true, false);
                    m.a().z(false);
                }
            } else if (m.a().am()) {
                a(true, false);
                m.a().A(false);
            }
        }
    }

    public void recycleData() {
        super.recycleData();
        this.y.unregister(this.x);
    }

    public void k() {
        if (this.f == null) {
            this.f = (LinearLayout) LayoutInflater.from(getContext()).inflate((int) R.layout.v5_hide_installed_apps_area, (ViewGroup) null);
            ((ExpandableListView) this.s).addHeaderView(this.f);
            this.g = this.f.findViewById(R.id.header_container);
            this.h = (SwitchButton) this.f.findViewById(R.id.hide_btn);
            this.h.setClickable(false);
            this.k = (TextView) this.f.findViewById(R.id.tips);
            this.h.a(com.tencent.assistantv2.manager.a.a().b().c());
            this.g.setOnClickListener(new af(this));
        }
    }

    public void a(a aVar) {
        this.w = aVar;
    }

    /* access modifiers changed from: package-private */
    public void l() {
        STInfoV2 sTInfoV2 = new STInfoV2(this.w.G(), "08", this.w.G(), "08", 100);
        if (!this.v || !(this.w instanceof n)) {
            sTInfoV2.slotId = com.tencent.assistantv2.st.page.a.a("08", 0);
        } else {
            sTInfoV2.slotId = com.tencent.assistantv2.st.page.a.a(((n) this.w).M(), 0);
        }
        if (!com.tencent.assistantv2.manager.a.a().b().c()) {
            sTInfoV2.status = "01";
        } else {
            sTInfoV2.status = "02";
        }
        l.a(sTInfoV2);
    }

    /* access modifiers changed from: private */
    public void a(int i2, int i3, boolean z2, boolean z3) {
        if (i3 == 0) {
            this.A.b();
            if (this.z == null) {
                r();
            }
            if (this.z.getGroupCount() == 0) {
                this.A.a(10);
                return;
            }
            this.z.notifyDataSetChanged();
            this.F = System.currentTimeMillis();
            if (z2) {
                c().setSelection(0);
            }
            onRefreshComplete(z3, true);
        } else if (z2) {
            l.a((int) STConst.ST_PAGE_RANK_RECOMMEND, CostTimeSTManager.TIMETYPE.CANCEL, System.currentTimeMillis());
            if (-800 == i3) {
                this.A.a(30);
            } else if (this.B <= 0) {
                this.A.a(30);
            } else {
                this.B--;
                d(this.D, this.E);
            }
        } else {
            onRefreshComplete(z3, false);
            this.A.c();
        }
    }

    public void d(int i2, int i3) {
        if (this.y != null) {
            TemporaryThreadManager.get().start(new ah(this, i2, i3));
        }
    }

    public al m() {
        return this.z;
    }

    public boolean n() {
        return this.F == 0 || System.currentTimeMillis() - this.F > m.a().ai();
    }

    public void a(boolean z2, boolean z3) {
        if (this.g == null) {
            return;
        }
        if (!z2) {
            if (z3) {
                this.g.setClickable(false);
                this.j = false;
                if (this.i == null) {
                    this.i = new ad(this.g);
                    this.i.i = c();
                }
                this.i.f3563a = true;
                this.g.postDelayed(this.i, 5);
                return;
            }
            this.g.setVisibility(8);
            this.j = false;
            this.k.setVisibility(8);
            if (this.i != null) {
                this.i.f3563a = false;
            }
        } else if (!this.j) {
            if (this.i != null) {
                this.i.f3563a = false;
            }
            this.g.setVisibility(0);
            this.g.setBackgroundResource(R.drawable.v2_button_background_selector);
            this.g.setPressed(false);
            c().setSelection(1);
            this.g.setClickable(true);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.g.getLayoutParams();
            if (layoutParams.height != getResources().getDimensionPixelSize(R.dimen.hide_installed_apps_area_height)) {
                layoutParams.height = getResources().getDimensionPixelSize(R.dimen.hide_installed_apps_area_height);
                this.g.setLayoutParams(layoutParams);
            }
            this.j = true;
        }
    }
}
