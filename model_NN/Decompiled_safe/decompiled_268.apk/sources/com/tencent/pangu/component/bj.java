package com.tencent.pangu.component;

import android.view.View;
import com.tencent.assistant.component.SecondNavigationTitleView;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.activity.SelfUpdateActivity;
import com.tencent.pangu.manager.SelfUpdateManager;

/* compiled from: ProGuard */
class bj extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SelfNormalUpdateView f3655a;

    bj(SelfNormalUpdateView selfNormalUpdateView) {
        this.f3655a = selfNormalUpdateView;
    }

    public void onTMAClick(View view) {
        this.f3655a.g();
        SelfUpdateManager.a().o().b(false);
    }

    public STInfoV2 getStInfo() {
        if (!(this.f3655a.getContext() instanceof SelfUpdateActivity)) {
            return null;
        }
        STInfoV2 t = ((SelfUpdateActivity) this.f3655a.getContext()).t();
        t.slotId = SecondNavigationTitleView.TMA_ST_NAVBAR_SEARCH_TAG;
        if (!SelfUpdateManager.a().a(this.f3655a.b.e)) {
            t.actionId = 900;
            return t;
        }
        t.actionId = 305;
        return t;
    }
}
