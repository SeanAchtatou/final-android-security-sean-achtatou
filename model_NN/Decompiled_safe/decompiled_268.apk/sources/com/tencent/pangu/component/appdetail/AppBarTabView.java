package com.tencent.pangu.component.appdetail;

import android.annotation.SuppressLint;
import android.os.Build;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.AppBarWebView;
import com.tencent.assistant.component.NormalErrorPage;
import com.tencent.assistant.manager.webview.js.JsBridge;
import com.tencent.assistant.manager.webview.js.l;
import com.tencent.assistant.utils.FileUtil;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* compiled from: ProGuard */
public class AppBarTabView extends LinearLayout implements bc, be, bf {

    /* renamed from: a  reason: collision with root package name */
    protected FrameLayout f3525a;
    protected AppBarWebView b;
    protected BaseActivity c;
    private InnerScrollView d;
    private RelativeLayout e;
    /* access modifiers changed from: private */
    public ProgressBar f;
    /* access modifiers changed from: private */
    public NormalErrorPage g;
    /* access modifiers changed from: private */
    public JsBridge h;
    private int i;
    private WebChromeClient j = new c(this);

    public AppBarTabView(BaseActivity baseActivity) {
        super(baseActivity);
        this.c = baseActivity;
        a();
        e();
        f();
    }

    public boolean a(String str) {
        if (this.b == null) {
            return false;
        }
        l.a(this.c, str, "ALL");
        if (!TextUtils.isEmpty(this.b.getUrl())) {
            return false;
        }
        this.b.setScrollEnable(false);
        this.b.loadUrl(str);
        return true;
    }

    /* access modifiers changed from: protected */
    public void a() {
        View inflate = LayoutInflater.from(this.c).inflate((int) R.layout.appdetail_appbar_tab, this);
        this.d = (InnerScrollView) inflate.findViewById(R.id.inner_scrollview);
        this.d.setFillViewport(true);
        this.e = (RelativeLayout) inflate.findViewById(R.id.browser_content_view);
        this.b = new AppBarWebView(this.c);
        this.f3525a = (FrameLayout) inflate.findViewById(R.id.webview_container);
        this.f3525a.addView(this.b);
        this.f = (ProgressBar) findViewById(R.id.loading_view);
        this.h = new JsBridge(this.c, this.b);
        this.d.a((be) this);
        this.d.a((bf) this);
    }

    private void e() {
        this.g = (NormalErrorPage) findViewById(R.id.error_page_view);
        this.g.setButtonClickListener(new a(this));
    }

    private void b(boolean z) {
        if (z) {
            this.g.setVisibility(4);
            this.b.setVisibility(0);
            return;
        }
        this.g.setVisibility(0);
        this.b.setVisibility(4);
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    private void f() {
        this.b.setScrollbarFadingEnabled(true);
        this.b.requestFocus();
        this.b.setFocusable(true);
        this.b.setFocusableInTouchMode(true);
        this.b.setScrollBarStyle(0);
        WebSettings settings = this.b.getSettings();
        settings.setBuiltInZoomControls(false);
        settings.setUserAgentString(settings.getUserAgentString() + "/" + "qqdownloader/" + this.h.getVersion() + "/appdetail" + "/apiLevel/" + Build.VERSION.SDK_INT);
        settings.setCacheMode(2);
        settings.setJavaScriptEnabled(true);
        Class<?> cls = settings.getClass();
        try {
            Method method = cls.getMethod("setPluginsEnabled", Boolean.TYPE);
            if (method != null) {
                method.invoke(settings, true);
            }
        } catch (NoSuchMethodException e2) {
            e2.printStackTrace();
        } catch (Throwable th) {
            th.printStackTrace();
        }
        try {
            Method method2 = settings.getClass().getMethod("setDomStorageEnabled", Boolean.TYPE);
            if (method2 != null) {
                method2.invoke(settings, true);
            }
        } catch (SecurityException e3) {
            e3.printStackTrace();
        } catch (IllegalAccessException | IllegalArgumentException | NoSuchMethodException | InvocationTargetException e4) {
        }
        try {
            Method method3 = this.b.getClass().getMethod("removeJavascriptInterface", String.class);
            if (method3 != null) {
                method3.invoke(this.b, "searchBoxJavaBridge_");
            }
        } catch (Exception e5) {
            e5.printStackTrace();
        }
        settings.setAppCachePath(FileUtil.getWebViewCacheDir());
        settings.setDatabasePath(FileUtil.getWebViewCacheDir());
        settings.setDatabaseEnabled(true);
        settings.setAppCacheEnabled(true);
        this.b.setWebChromeClient(this.j);
        this.b.setWebViewClient(new d(this, null));
        this.b.setDownloadListener(new b(this));
    }

    /* access modifiers changed from: private */
    public void g() {
        ViewGroup.LayoutParams layoutParams = this.e.getLayoutParams();
        layoutParams.height = this.i;
        this.e.setLayoutParams(layoutParams);
        ViewGroup.LayoutParams layoutParams2 = this.g.getLayoutParams();
        layoutParams2.height = this.i;
        this.g.setLayoutParams(layoutParams2);
    }

    /* access modifiers changed from: private */
    public void b(int i2) {
        b(false);
        this.g.setErrorType(i2);
    }

    public void a(int i2) {
        this.i = i2;
    }

    public void b() {
        if (this.b != null && this.b.getHeight() != this.i) {
            ViewGroup.LayoutParams layoutParams = this.b.getLayoutParams();
            layoutParams.height = this.i;
            this.f3525a.removeAllViews();
            this.f3525a.addView(this.b, layoutParams);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        if (this.b != null) {
            this.f3525a.removeAllViews();
            try {
                this.b.stopLoading();
                this.b.setVisibility(8);
                this.b.destroy();
            } catch (Exception e2) {
            }
            this.b = null;
        }
        super.onDetachedFromWindow();
    }

    public bd c() {
        return this.d;
    }

    public void d() {
        if (this.b != null && this.d != null && this.b != null) {
            this.b.setScrollEnable(true);
        }
    }

    public void a(boolean z) {
        if (this.b != null && this.d != null && this.b != null) {
            this.b.setScrollEnable(z);
            b();
        }
    }
}
