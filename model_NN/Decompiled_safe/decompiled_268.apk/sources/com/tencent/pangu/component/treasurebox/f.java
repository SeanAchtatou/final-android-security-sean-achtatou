package com.tencent.pangu.component.treasurebox;

import android.view.View;
import android.view.animation.Animation;

/* compiled from: ProGuard */
class f implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    View f3738a;
    int b;
    int c;
    int d;
    int e;
    boolean f;
    final /* synthetic */ e g;

    public f(e eVar, View view, int i, int i2, int i3, int i4, boolean z) {
        this.g = eVar;
        this.f3738a = view;
        this.b = i;
        this.c = i2;
        this.d = i3;
        this.e = i4;
        this.f = z;
    }

    public void onAnimationEnd(Animation animation) {
        if (this.f3738a != null) {
            this.f3738a.clearAnimation();
            if (this.f) {
                this.f3738a.layout(this.b, this.c, this.d, this.e);
            } else {
                this.f3738a.setVisibility(8);
            }
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }
}
