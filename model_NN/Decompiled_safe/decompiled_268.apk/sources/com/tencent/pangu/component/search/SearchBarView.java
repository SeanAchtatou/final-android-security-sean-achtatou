package com.tencent.pangu.component.search;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
public class SearchBarView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private EditText f3692a;
    private View b;
    private View c;
    private View d;
    private View e;
    private Paint f;

    public SearchBarView(Context context) {
        this(context, null);
    }

    public SearchBarView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public SearchBarView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        a(context, attributeSet);
    }

    private void a(Context context, AttributeSet attributeSet) {
        setOrientation(0);
        inflate(context, R.layout.search_navigation_layout, this);
        this.f3692a = (EditText) findViewById(R.id.input_box);
        this.b = findViewById(R.id.search_btn);
        this.c = findViewById(R.id.cancel_btn);
        this.d = findViewById(R.id.delete_txt);
        this.e = findViewById(R.id.divider);
        this.f = new Paint();
        this.f.setAntiAlias(true);
        this.f.setStyle(Paint.Style.FILL);
        this.f.setColor(-1447447);
        this.f.setStrokeWidth(1.0f);
        setWillNotDraw(false);
    }

    public EditText a() {
        return this.f3692a;
    }

    public void setOnKeyListener(View.OnKeyListener onKeyListener) {
        this.f3692a.setOnKeyListener(onKeyListener);
    }

    public void a(View.OnClickListener onClickListener) {
        if (this.b != null) {
            this.b.setOnClickListener(onClickListener);
        }
    }

    public void b(View.OnClickListener onClickListener) {
        if (this.c != null) {
            this.c.setOnClickListener(onClickListener);
        }
    }

    public void a(TextWatcher textWatcher) {
        if (this.f3692a != null) {
            this.f3692a.addTextChangedListener(textWatcher);
        }
    }

    public void c(View.OnClickListener onClickListener) {
        if (this.d != null) {
            this.d.setOnClickListener(onClickListener);
        }
    }

    public void a(int i) {
        this.d.setVisibility(i);
        this.e.setVisibility(i);
    }

    public void a(String str) {
        if (this.f3692a != null) {
            this.f3692a.setHint(str);
        }
    }

    public void b(String str) {
        if (this.f3692a != null) {
            this.f3692a.setText(str);
        }
    }

    public String b() {
        if (this.f3692a == null || this.f3692a.getText() == null) {
            return null;
        }
        return this.f3692a.getText().toString();
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawLine(0.0f, (float) (getHeight() - 1), (float) getWidth(), (float) (getHeight() - 1), this.f);
        setWillNotDraw(true);
    }
}
