package com.tencent.pangu.component.banner;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.manager.i;
import com.tencent.assistant.protocol.jce.Banner;
import com.tencent.assistant.protocol.jce.GftGetGameGiftFlagResponse;

/* compiled from: ProGuard */
public class a extends f {
    public a(Banner banner) {
        super(banner);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View a(Context context, ViewGroup viewGroup, int i, long j, int i2) {
        GftGetGameGiftFlagResponse r;
        boolean z = true;
        try {
            View inflate = LayoutInflater.from(context).inflate((int) R.layout.banner_apptab_node_view, viewGroup, false);
            inflate.setBackgroundColor(c());
            TXImageView tXImageView = (TXImageView) inflate.findViewById(R.id.title_img);
            TextView textView = (TextView) inflate.findViewById(R.id.title);
            ImageView imageView = (ImageView) inflate.findViewById(R.id.new_gift);
            if (this.f3641a.getTag() == null || imageView == null || (r = i.y().r()) == null || r.f1364a != 0 || r.b != 1) {
                z = false;
            } else {
                imageView.setVisibility(0);
            }
            tXImageView.updateImageView(this.f3641a.c, 0, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            textView.setText(this.f3641a.b);
            inflate.setOnClickListener(new b(this, context, i2, z, imageView));
            return inflate;
        } catch (Throwable th) {
            th.printStackTrace();
            return new View(AstApp.i());
        }
    }

    public int a() {
        return 1;
    }
}
