package com.tencent.pangu.component;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.plugin.mgr.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class a extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BookReadButton f3509a;

    a(BookReadButton bookReadButton) {
        this.f3509a = bookReadButton;
    }

    public void onTMAClick(View view) {
        if (this.f3509a.b != null) {
            k.a().a(this.f3509a.getContext(), this.f3509a.b.c, this.f3509a.b.f3855a, -1, -1, -1, this.f3509a.b.e);
        }
    }

    public STInfoV2 getStInfo() {
        if (this.f3509a.c != null) {
            this.f3509a.c.actionId = 200;
            this.f3509a.c.status = Constants.VIA_REPORT_TYPE_SET_AVATAR;
        }
        return this.f3509a.c;
    }
}
