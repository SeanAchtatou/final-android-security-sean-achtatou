package com.tencent.pangu.component;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.tencent.assistant.adapter.RankFriendsListAdapter;

/* compiled from: ProGuard */
public class RankFriendsListView extends RankNormalListView {
    public RankFriendsListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.L = new aj(this);
    }

    /* access modifiers changed from: protected */
    public void q() {
        ListAdapter adapter = ((ListView) this.s).getAdapter();
        if (adapter instanceof HeaderViewListAdapter) {
            this.u = (RankFriendsListAdapter) ((HeaderViewListAdapter) adapter).getWrappedAdapter();
        } else {
            this.u = (RankFriendsListAdapter) ((ListView) this.s).getAdapter();
        }
        if (this.u == null) {
        }
    }
}
