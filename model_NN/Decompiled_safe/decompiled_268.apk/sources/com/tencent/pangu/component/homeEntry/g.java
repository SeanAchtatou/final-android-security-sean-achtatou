package com.tencent.pangu.component.homeEntry;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Bitmap f3679a;
    final /* synthetic */ HomeEntryTemplateView b;

    g(HomeEntryTemplateView homeEntryTemplateView, Bitmap bitmap) {
        this.b = homeEntryTemplateView;
        this.f3679a = bitmap;
    }

    public void run() {
        XLog.v("home_entry", "template---requestImg--template name:" + getClass().getSimpleName() + "--bitmap refresh1111");
        this.b.setBackgroundResource(0);
        this.b.c.setBackgroundDrawable(new BitmapDrawable(this.f3679a));
    }
}
