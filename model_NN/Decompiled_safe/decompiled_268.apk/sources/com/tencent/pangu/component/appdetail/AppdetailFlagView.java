package com.tencent.pangu.component.appdetail;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.utils.by;

/* compiled from: ProGuard */
public class AppdetailFlagView extends LinearLayout {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public View f3531a;
    /* access modifiers changed from: private */
    public ImageView b;
    private TextView[] c = new TextView[3];
    private TextView[] d = new TextView[3];
    private SimpleAppModel e;
    /* access modifiers changed from: private */
    public String f = "06";
    /* access modifiers changed from: private */
    public String g = "001";
    /* access modifiers changed from: private */
    public String h = "002";
    private boolean i = false;

    public AppdetailFlagView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public AppdetailFlagView(Context context) {
        super(context);
        a(context);
    }

    private void a(Context context) {
        View inflate = LayoutInflater.from(context).inflate((int) R.layout.appdetail_flag_view, this);
        this.f3531a = inflate.findViewById(R.id.appdetail_tag_explain_view);
        this.b = (ImageView) findViewById(R.id.appdetail_tag_expand_more_img);
        this.c[0] = (TextView) inflate.findViewById(R.id.product_flag_novirus_txt);
        this.c[1] = (TextView) inflate.findViewById(R.id.product_flag_noad_txt);
        this.c[2] = (TextView) inflate.findViewById(R.id.product_flag_usercare_txt);
        this.d[0] = (TextView) inflate.findViewById(R.id.text_2);
        this.d[1] = (TextView) inflate.findViewById(R.id.text_3);
        this.d[2] = (TextView) inflate.findViewById(R.id.text_4);
        setOnClickListener(new q(this, context));
        this.d[2].setClickable(true);
        this.d[2].setOnClickListener(new r(this, context));
    }

    public void a(SimpleAppModel simpleAppModel) {
        if (simpleAppModel != null) {
            this.e = simpleAppModel;
            a();
        }
    }

    public static boolean a(SimpleAppModel simpleAppModel, int i2) {
        if (simpleAppModel != null && (((int) (simpleAppModel.B >> (i2 * 2))) & 3) == 1) {
            return true;
        }
        return false;
    }

    private void a() {
        int i2;
        int i3;
        int i4;
        String str;
        this.d[0].setText("已通过腾讯手机管家、金山手机毒霸检查无病毒");
        try {
            Drawable drawable = getResources().getDrawable(R.drawable.common_icon_appdetail_past);
            int a2 = by.a(getContext(), 14.0f);
            drawable.setBounds(0, 0, a2, a2);
            this.d[0].setCompoundDrawables(drawable, null, null, null);
            if (!a(this.e, 0)) {
                i2 = R.drawable.icon_3_2;
                i3 = R.string.appdetail_noad_txt_y;
                i4 = R.drawable.common_icon_appdetail_didnotpast;
                str = "已通过腾讯手机管家、金山手机毒霸检查有广告";
            } else {
                i2 = R.drawable.icon_3_1;
                i3 = R.string.appdetail_noad_txt_n;
                i4 = R.drawable.common_icon_appdetail_past;
                str = "已通过腾讯手机管家、金山手机毒霸检查无广告";
            }
            if (i2 != -1) {
                this.c[1].setCompoundDrawablesWithIntrinsicBounds(i2, 0, 0, 0);
            }
            if (i3 != -1) {
                this.c[1].setText(i3);
            }
            if (i4 != -1) {
                Drawable drawable2 = getResources().getDrawable(i4);
                drawable2.setBounds(0, 0, a2, a2);
                this.d[1].setCompoundDrawables(drawable2, null, null, null);
            }
            this.d[1].setText(str);
            if (!this.e.g()) {
                this.c[2].setVisibility(8);
                if (this.d[2].getVisibility() != 8) {
                    this.d[2].setVisibility(8);
                }
            } else if (this.d[2].getVisibility() != 0) {
                this.d[2].setVisibility(0);
            }
            if (this.c[1].getVisibility() == 8 && this.c[0].getVisibility() == 8 && this.c[2].getVisibility() == 8) {
                setVisibility(8);
            }
        } catch (OutOfMemoryError e2) {
            t.a().b();
        } catch (Throwable th) {
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        if (!this.i) {
            this.i = true;
            String string = getContext().getResources().getString(R.string.text_user_care_left);
            String string2 = getContext().getResources().getString(R.string.text_user_care_right);
            int length = string.length();
            StringBuilder sb = new StringBuilder();
            sb.append(string);
            sb.append(string2);
            SpannableString spannableString = new SpannableString(sb.toString());
            ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(getContext().getResources().getColor(R.color.detail_user_care_txt_left_part_color));
            ForegroundColorSpan foregroundColorSpan2 = new ForegroundColorSpan(getContext().getResources().getColor(R.color.detail_user_care_txt_right_part_color));
            spannableString.setSpan(foregroundColorSpan, 0, length, 33);
            spannableString.setSpan(foregroundColorSpan2, length, sb.toString().length(), 33);
            this.d[2].setText(spannableString);
            Drawable drawable = getContext().getResources().getDrawable(R.drawable.common_icon_appdetail_past);
            int a2 = by.a(getContext(), 14.0f);
            drawable.setBounds(0, 0, a2, a2);
            this.d[2].setCompoundDrawables(drawable, null, null, null);
        }
    }
}
