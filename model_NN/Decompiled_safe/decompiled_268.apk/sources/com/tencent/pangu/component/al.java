package com.tencent.pangu.component;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.nucleus.socialcontact.login.j;
import com.tencent.pangu.activity.AppDetailActivityV5;

/* compiled from: ProGuard */
class al extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RankFriendsLoginErrorView f3520a;

    al(RankFriendsLoginErrorView rankFriendsLoginErrorView) {
        this.f3520a = rankFriendsLoginErrorView;
    }

    public void onTMAClick(View view) {
        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo("com.tencent.mm");
        if (localApkInfo == null || localApkInfo.mVersionCode < 350) {
            Toast.makeText(this.f3520a.f3497a, localApkInfo == null ? R.string.toast_friend_wxnotinstall : R.string.toast_friend_wxnotupdate, 0).show();
            Intent intent = new Intent(AstApp.i(), AppDetailActivityV5.class);
            intent.setFlags(268435456);
            intent.putExtra("com.tencent.assistant.PACKAGE_NAME", "com.tencent.mm");
            this.f3520a.f3497a.startActivity(intent);
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putInt(AppConst.KEY_LOGIN_TYPE, 5);
        bundle.putInt(AppConst.KEY_FROM_TYPE, 13);
        j.a().a(AppConst.IdentityType.WX, bundle);
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3520a.getContext(), 200);
        buildSTInfo.slotId = a.a("08", "002");
        return buildSTInfo;
    }
}
