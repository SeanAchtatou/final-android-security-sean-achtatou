package com.tencent.pangu.utils.installuninstall;

import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistant.utils.FunctionUtils;

/* compiled from: ProGuard */
class g extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ InstallUninstallDialogManager f4002a;

    g(InstallUninstallDialogManager installUninstallDialogManager) {
        this.f4002a = installUninstallDialogManager;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean
     arg types: [com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, int]
     candidates:
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT):com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_TYPE, com.tencent.pangu.utils.installuninstall.InstallUninstallTaskBean):boolean
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean */
    public void onCancell() {
        boolean unused = this.f4002a.c = false;
        this.f4002a.c();
        this.f4002a.a(this.pageId, AstApp.m() != null ? AstApp.m().f() : 2000, "00_002", STConstAction.ACTION_HIT_SEARCH_CANCEL);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean
     arg types: [com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, int]
     candidates:
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT):com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_TYPE, com.tencent.pangu.utils.installuninstall.InstallUninstallTaskBean):boolean
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean */
    public void onLeftBtnClick() {
        boolean unused = this.f4002a.c = false;
        this.f4002a.c();
        this.f4002a.a(this.pageId, AstApp.m() != null ? AstApp.m().f() : 2000, "00_002", STConstAction.ACTION_HIT_SEARCH_CANCEL);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean
     arg types: [com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, int]
     candidates:
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT):com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_TYPE, com.tencent.pangu.utils.installuninstall.InstallUninstallTaskBean):boolean
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean */
    public void onRightBtnClick() {
        boolean unused = this.f4002a.c = false;
        this.f4002a.c();
        FunctionUtils.a(this.pageId, this.f4002a.e.fileSize);
        this.f4002a.a(this.pageId, AstApp.m() != null ? AstApp.m().f() : 2000, "00_001", 200);
    }
}
