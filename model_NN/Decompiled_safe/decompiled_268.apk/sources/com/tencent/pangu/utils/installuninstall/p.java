package com.tencent.pangu.utils.installuninstall;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.SparseIntArray;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.Global;
import com.tencent.assistant.event.EventDispatcher;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.notification.a;
import com.tencent.assistant.manager.notification.z;
import com.tencent.assistant.module.update.j;
import com.tencent.assistant.protocol.jce.AppUpdateInfo;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.h;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.st.l;
import com.tencent.nucleus.manager.root.e;
import com.tencent.pangu.activity.SelfUpdateActivity;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import com.tencent.pangu.manager.ak;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
public class p {
    private static p b;
    private static Handler d = new u(AstApp.i().getMainLooper());
    private static Handler e;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public EventDispatcher f4009a = AstApp.i().j();
    private SparseIntArray c = new SparseIntArray();

    private p() {
    }

    public static synchronized p a() {
        p pVar;
        synchronized (p.class) {
            if (b == null) {
                b = new p();
            }
            pVar = b;
        }
        return pVar;
    }

    public synchronized void a(DownloadInfo downloadInfo, boolean z) {
        TemporaryThreadManager.get().start(new q(this, downloadInfo, z));
    }

    public void a(ArrayList<DownloadInfo> arrayList, boolean z) {
        TemporaryThreadManager.get().start(new r(this, arrayList, z));
    }

    public void b(ArrayList<DownloadInfo> arrayList, boolean z) {
        TemporaryThreadManager.get().start(new s(this, arrayList, z));
    }

    public void a(String str, String str2, String str3, String str4, int i, String str5, String str6, long j, boolean z, DownloadInfo downloadInfo) {
        DownloadInfo downloadInfo2;
        boolean z2;
        AppUpdateInfo a2;
        int Y;
        if (TextUtils.isEmpty(str4)) {
            this.f4009a.sendMessage(this.f4009a.obtainMessage(EventDispatcherEnum.UI_EVENT_CANCEL_APP_INSTALL_TASK, new InstallUninstallTaskBean(-2, str2, i, str3)));
            return;
        }
        if (z) {
            if (ak.a().b(str2)) {
                ak.a();
                if (ak.b()) {
                    ak.a();
                    ak.a(false);
                    downloadInfo2 = null;
                    z2 = false;
                }
            }
            DownloadInfo d2 = DownloadProxy.a().d(str);
            if (d2 != null && (d2.isUiTypeWiseAppUpdateDownload() || d2.isUiTypeWiseBookingDownload())) {
                downloadInfo2 = d2;
                z2 = true;
            } else if (!m.a().k()) {
                this.f4009a.sendMessage(this.f4009a.obtainMessage(EventDispatcherEnum.UI_EVENT_CANCEL_APP_INSTALL_TASK, new InstallUninstallTaskBean(-2, str2, i, str3)));
                return;
            } else {
                downloadInfo2 = d2;
                z2 = false;
            }
        } else {
            downloadInfo2 = null;
            z2 = false;
        }
        if (z2) {
            if (t.x()) {
                Y = m.a().X();
            } else {
                Y = m.a().Y();
            }
            if (t.a(Y, str2)) {
                DownloadInfo d3 = DownloadProxy.a().d(str);
                if (d3 != null) {
                    if (d3.isUiTypeWiseBookingDownload()) {
                        a.a().a(str);
                    } else if (d3.isUiTypeWiseSubscribtionDownloadAutoInstall()) {
                        z.a().a(str);
                    }
                }
                this.f4009a.sendMessage(this.f4009a.obtainMessage(EventDispatcherEnum.UI_EVENT_CANCEL_APP_INSTALL_TASK, new InstallUninstallTaskBean(-2, str2, i, str3)));
                return;
            }
        }
        int a3 = a(1);
        if (!z2 || b()) {
            l.a(downloadInfo, (byte) a3, z);
            InstallUninstallTaskBean installUninstallTaskBean = new InstallUninstallTaskBean(a3, 1, str2, str3);
            installUninstallTaskBean.isAutoInstall = z;
            this.f4009a.sendMessage(this.f4009a.obtainMessage(EventDispatcherEnum.UI_EVENT_ADD_APP_INSTALL_TASK, installUninstallTaskBean));
            boolean z3 = false;
            if (z2 && (a2 = j.b().a(downloadInfo2.packageName)) != null) {
                z3 = h.b(a2.q);
            }
            if (a3 == 0) {
                ac.a().a(str4, str2, i, str3, str5, str6, j, z);
            } else if (b(a3)) {
                ac.a().a(a3, str, str4, str2, i, str3, str5, str6, j, !z2, !z2, z3, z, downloadInfo.applinkInfo);
            } else if (a3 != -2) {
            } else {
                if (AstApp.i().l()) {
                    InstallUninstallTaskBean installUninstallTaskBean2 = new InstallUninstallTaskBean(0, 1, str4, str2, i, str3, str5, str6, j);
                    installUninstallTaskBean2.isAutoInstall = z;
                    installUninstallTaskBean2.applinkInfo = downloadInfo.applinkInfo;
                    a(installUninstallTaskBean2);
                    return;
                }
                ac.a().a(str4, str2, i, str3, str5, str6, j, z);
            }
        } else {
            this.f4009a.sendMessage(this.f4009a.obtainMessage(EventDispatcherEnum.UI_EVENT_CANCEL_APP_INSTALL_TASK, new InstallUninstallTaskBean(-2, str2, i, str3)));
        }
    }

    public boolean b() {
        return b(a(1));
    }

    private boolean b(int i) {
        return i == 2 || i == 1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:33:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int a(int r10) {
        /*
            r9 = this;
            r1 = 2
            r2 = 1
            r0 = 0
            com.tencent.assistant.m r3 = com.tencent.assistant.m.a()
            boolean r3 = r3.j()
            com.tencent.nucleus.manager.root.e r4 = com.tencent.nucleus.manager.root.e.a()
            boolean r4 = r4.c()
            com.tencent.assistant.m r5 = com.tencent.assistant.m.a()
            com.tencent.assistant.AppConst$ROOT_STATUS r5 = r5.n()
            com.tencent.assistant.AppConst$ROOT_STATUS r6 = com.tencent.assistant.AppConst.ROOT_STATUS.ROOTED
            if (r5 == r6) goto L_0x0023
            if (r4 != 0) goto L_0x0023
            if (r3 == 0) goto L_0x00b2
        L_0x0023:
            if (r3 != 0) goto L_0x00b9
            com.tencent.assistant.m r3 = com.tencent.assistant.m.a()
            int r3 = r3.f()
            r5 = 5
            if (r3 >= r5) goto L_0x00d3
            com.tencent.assistant.m r3 = com.tencent.assistant.m.a()
            long r5 = r3.g()
            long r7 = java.lang.System.currentTimeMillis()
            long r5 = r7 - r5
            r7 = 604800000(0x240c8400, double:2.988109026E-315)
            int r3 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r3 <= 0) goto L_0x00d3
            r3 = r2
        L_0x0046:
            if (r3 == 0) goto L_0x00b2
            if (r4 == 0) goto L_0x00b7
            com.tencent.assistant.m r0 = com.tencent.assistant.m.a()
            r0.b(r2)
            com.tencent.assistant.m r0 = com.tencent.assistant.m.a()
            com.tencent.assistant.m r3 = com.tencent.assistant.m.a()
            int r3 = r3.f()
            int r3 = r3 + 1
            r0.c(r3)
            com.qq.AppService.AstApp r0 = com.qq.AppService.AstApp.i()
            boolean r0 = r0.l()
            if (r0 == 0) goto L_0x0089
            if (r10 != r2) goto L_0x00b3
            r0 = 2131362137(0x7f0a0159, float:1.8344046E38)
        L_0x0071:
            com.qq.AppService.AstApp r2 = com.qq.AppService.AstApp.i()
            android.content.res.Resources r2 = r2.getResources()
            java.lang.String r0 = r2.getString(r0)
            android.os.Handler r2 = com.tencent.assistant.utils.ah.a()
            com.tencent.pangu.utils.installuninstall.t r3 = new com.tencent.pangu.utils.installuninstall.t
            r3.<init>(r9, r0)
            r2.post(r3)
        L_0x0089:
            com.tencent.assistant.m r0 = com.tencent.assistant.m.a()
            com.tencent.assistant.AppConst$ROOT_STATUS r0 = r0.n()
            com.tencent.assistant.AppConst$ROOT_STATUS r2 = com.tencent.assistant.AppConst.ROOT_STATUS.ROOTED
            if (r0 != r2) goto L_0x00b1
            boolean r0 = com.tencent.pangu.utils.installuninstall.InstallUninstallUtil.a()
            java.lang.String r2 = "InstallUninstallHelper"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "handle root switch, permRootReqResult="
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r0 = r0.toString()
            com.tencent.assistant.utils.XLog.d(r2, r0)
        L_0x00b1:
            r0 = r1
        L_0x00b2:
            return r0
        L_0x00b3:
            r0 = 2131362138(0x7f0a015a, float:1.8344048E38)
            goto L_0x0071
        L_0x00b7:
            r0 = -2
            goto L_0x00b2
        L_0x00b9:
            if (r4 == 0) goto L_0x00bd
            r0 = r1
            goto L_0x00b2
        L_0x00bd:
            com.tencent.assistant.m r1 = com.tencent.assistant.m.a()
            boolean r1 = r1.i()
            if (r1 != 0) goto L_0x00d1
            com.tencent.assistant.m r1 = com.tencent.assistant.m.a()
            boolean r1 = r1.h()
            if (r1 != 0) goto L_0x00b2
        L_0x00d1:
            r0 = r2
            goto L_0x00b2
        L_0x00d3:
            r3 = r0
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.pangu.utils.installuninstall.p.a(int):int");
    }

    private void a(InstallUninstallTaskBean installUninstallTaskBean) {
        Message message = new Message();
        message.obj = installUninstallTaskBean;
        message.what = 1;
        if (AstApp.m() instanceof SelfUpdateActivity) {
            d.sendMessageDelayed(message, 200);
        } else {
            d.sendMessage(message);
        }
    }

    /* access modifiers changed from: private */
    public void a(List<InstallUninstallTaskBean> list) {
        Message message = new Message();
        message.obj = list;
        message.what = 2;
        if (AstApp.m() instanceof SelfUpdateActivity) {
            d.sendMessageDelayed(message, 200);
        } else {
            d.sendMessage(message);
        }
    }

    /* access modifiers changed from: private */
    public static synchronized Handler e() {
        Handler handler;
        synchronized (p.class) {
            if (e == null) {
                e = ah.a("InstallUninstallHandler");
            }
            handler = e;
        }
        return handler;
    }

    /* access modifiers changed from: private */
    public AppConst.TwoBtnDialogInfo b(InstallUninstallTaskBean installUninstallTaskBean) {
        v vVar = new v(this, installUninstallTaskBean);
        Context baseContext = AstApp.i().getBaseContext();
        vVar.titleRes = baseContext.getString(installUninstallTaskBean.action == 1 ? R.string.request_root_authority_install_title : R.string.request_root_authority_uninstall_title);
        vVar.contentRes = baseContext.getString(installUninstallTaskBean.action == 1 ? R.string.request_root_authority_install_msg : R.string.request_root_authority_uninstall_msg);
        vVar.lBtnTxtRes = baseContext.getString(R.string.request_root_authority_negative_btn);
        vVar.rBtnTxtRes = baseContext.getString(R.string.request_root_authority_positive_btn);
        vVar.blockCaller = true;
        return vVar;
    }

    /* access modifiers changed from: private */
    public AppConst.TwoBtnDialogInfo b(List<InstallUninstallTaskBean> list) {
        y yVar = new y(this, list);
        Context baseContext = AstApp.i().getBaseContext();
        yVar.titleRes = baseContext.getString(list.get(0).action == 1 ? R.string.request_root_authority_install_title : R.string.request_root_authority_uninstall_title);
        yVar.contentRes = baseContext.getString(list.get(0).action == 1 ? R.string.request_root_authority_install_msg : R.string.request_root_authority_uninstall_msg);
        yVar.lBtnTxtRes = baseContext.getString(R.string.request_root_authority_negative_btn);
        yVar.rBtnTxtRes = baseContext.getString(R.string.request_root_authority_positive_btn);
        yVar.blockCaller = true;
        return yVar;
    }

    /* access modifiers changed from: private */
    public static void b(int i, int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuid());
        hashMap.put("B2", String.valueOf(i));
        hashMap.put("B3", String.valueOf(i));
        com.tencent.beacon.event.a.a("tempRootPopWin ", true, 0, 0, hashMap, true);
    }

    public boolean c() {
        return ac.a().b();
    }

    public void a(Context context, String str, String str2, long j, String str3) {
        a(context, str, str2, j, str3, null, true, true);
    }

    public void a(Context context, String str, String str2, long j, String str3, String str4, boolean z, boolean z2) {
        if (!TextUtils.isEmpty(str2) && !a(str2)) {
            this.f4009a.sendMessage(this.f4009a.obtainMessage(EventDispatcherEnum.UI_EVENT_ADD_APP_UNINSTALL_TASK));
            int i = -2;
            if (z) {
                i = a(-1);
            } else if (m.a().n() == AppConst.ROOT_STATUS.ROOTED) {
                i = 1;
            } else if (e.a().c()) {
                i = 2;
            }
            if (i == 0 && z2) {
                this.f4009a.sendMessage(this.f4009a.obtainMessage(1024, str2));
                InstallUninstallUtil.a(str2);
            } else if (b(i)) {
                TemporaryThreadManager.get().start(new ab(this, i, str2, str3, z2, str4));
            } else if (i == -2) {
                InstallUninstallTaskBean installUninstallTaskBean = new InstallUninstallTaskBean(0, -1, str2, str3);
                if (!z2) {
                    installUninstallTaskBean.trySystemAfterSilentFail = false;
                    installUninstallTaskBean.isSystemApp = true;
                    installUninstallTaskBean.filePath = str4;
                }
                if (AstApp.i().l()) {
                    a(installUninstallTaskBean);
                } else if (z2) {
                    this.f4009a.sendMessage(this.f4009a.obtainMessage(1024, str2));
                    InstallUninstallUtil.a(str2);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void c(InstallUninstallTaskBean installUninstallTaskBean) {
        boolean a2;
        this.c.put(installUninstallTaskBean.packageName.hashCode(), Integer.MIN_VALUE);
        this.f4009a.sendMessage(this.f4009a.obtainMessage(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_START, installUninstallTaskBean));
        if (installUninstallTaskBean.isSystemApp) {
            a2 = InstallUninstallUtil.a(installUninstallTaskBean.packageName, installUninstallTaskBean.filePath);
        } else {
            a2 = InstallUninstallUtil.a(installUninstallTaskBean.style, installUninstallTaskBean.packageName);
        }
        this.c.delete(installUninstallTaskBean.packageName.hashCode());
        this.f4009a.sendMessage(this.f4009a.obtainMessage(a2 ? EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_SUCC : EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_FAIL, installUninstallTaskBean));
        if (a2 || !installUninstallTaskBean.isSystemApp) {
            ac.a().a(-1, a2, null, installUninstallTaskBean);
        }
        if (!a2 && installUninstallTaskBean.trySystemAfterSilentFail) {
            this.f4009a.sendMessage(this.f4009a.obtainMessage(1024, installUninstallTaskBean.packageName));
            InstallUninstallUtil.a(installUninstallTaskBean.packageName);
        }
    }

    public boolean a(String str) {
        if (!TextUtils.isEmpty(str) && this.c != null && this.c.indexOfKey(str.hashCode()) >= 0) {
            return true;
        }
        return false;
    }
}
