package com.tencent.pangu.adapter;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;

/* compiled from: ProGuard */
class l extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfoMultiAdapter f3453a;

    l(DownloadInfoMultiAdapter downloadInfoMultiAdapter) {
        this.f3453a = downloadInfoMultiAdapter;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.adapter.DownloadInfoMultiAdapter, boolean):boolean
     arg types: [com.tencent.pangu.adapter.DownloadInfoMultiAdapter, int]
     candidates:
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(int, int):java.lang.String
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.mediadownload.o, com.tencent.pangu.model.d):java.lang.String
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.adapter.DownloadInfoMultiAdapter, java.util.List):java.util.ArrayList
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(java.util.List<com.tencent.pangu.download.DownloadInfoWrapper>, int):java.util.ArrayList<com.tencent.assistant.protocol.jce.InstalledAppItem>
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.adapter.DownloadInfoMultiAdapter, com.tencent.assistant.component.invalidater.ViewInvalidateMessage):void
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.adapter.DownloadInfoMultiAdapter, com.tencent.pangu.download.DownloadInfo):void
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.adapter.DownloadInfoMultiAdapter, com.tencent.pangu.download.DownloadInfoWrapper):void
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.adapter.ad, com.tencent.pangu.adapter.DownloadInfoMultiAdapter$CreatingTaskStatusEnum):void
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.adapter.af, com.tencent.pangu.model.d):void
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.adapter.ai, com.tencent.pangu.mediadownload.q):void
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.adapter.aj, boolean):void
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.download.DownloadInfoWrapper, android.view.View):void
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(java.util.List<com.tencent.pangu.download.DownloadInfoWrapper>, long):boolean
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(java.util.List<com.tencent.pangu.download.DownloadInfoWrapper>, com.tencent.pangu.download.DownloadInfoWrapper):boolean
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(java.lang.String, com.tencent.assistant.protocol.jce.InstalledAppItem):void
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.adapter.DownloadInfoMultiAdapter, boolean):boolean */
    public void onTMAClick(View view) {
        if (this.f3453a.f.size() > 0) {
            ArrayList arrayList = new ArrayList(this.f3453a.f);
            synchronized (this.f3453a.f) {
                this.f3453a.f.clear();
                this.f3453a.g.clear();
                boolean unused = this.f3453a.s = true;
                this.f3453a.notifyDataSetChanged();
            }
            TemporaryThreadManager.get().start(new m(this, arrayList));
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3453a.m, 200);
        buildSTInfo.status = Constants.VIA_REPORT_TYPE_MAKE_FRIEND;
        return buildSTInfo;
    }
}
