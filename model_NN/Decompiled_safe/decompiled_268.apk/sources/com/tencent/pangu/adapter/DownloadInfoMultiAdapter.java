package com.tencent.pangu.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.ApkMetaInfoLoader;
import com.tencent.assistant.component.SecondNavigationTitleView;
import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.assistant.component.invalidater.CommonViewInvalidater;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import com.tencent.assistant.component.txscrollview.TXExpandableListView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.module.ay;
import com.tencent.assistant.module.k;
import com.tencent.assistant.module.update.j;
import com.tencent.assistant.protocol.jce.AppUpdateInfo;
import com.tencent.assistant.protocol.jce.InstalledAppItem;
import com.tencent.assistant.protocol.jce.RecommendAppInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.at;
import com.tencent.assistant.utils.bm;
import com.tencent.assistant.utils.bo;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.MovingProgressBar;
import com.tencent.assistantv2.component.fps.FPSProgressBar;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.component.BookReadButton;
import com.tencent.pangu.component.DownloadListFooterView;
import com.tencent.pangu.component.DownloadNumView;
import com.tencent.pangu.component.FileDownloadButton;
import com.tencent.pangu.component.VideoDownloadButton;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.DownloadInfoWrapper;
import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import com.tencent.pangu.manager.ak;
import com.tencent.pangu.mediadownload.FileOpenSelector;
import com.tencent.pangu.mediadownload.b;
import com.tencent.pangu.mediadownload.c;
import com.tencent.pangu.mediadownload.e;
import com.tencent.pangu.mediadownload.o;
import com.tencent.pangu.mediadownload.q;
import com.tencent.pangu.mediadownload.r;
import com.tencent.pangu.model.AbstractDownloadInfo;
import com.tencent.pangu.model.d;
import com.tencent.pangu.module.a.i;
import com.tencent.pangu.utils.installuninstall.InstallUninstallTaskBean;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@SuppressLint({"NewApi"})
/* compiled from: ProGuard */
public class DownloadInfoMultiAdapter extends BaseExpandableListAdapter implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    public boolean f3402a = false;
    public View.OnClickListener b = new l(this);
    private LayoutInflater c;
    private final List<DownloadInfoWrapper> d = new ArrayList();
    private final List<DownloadInfoWrapper> e = new ArrayList();
    /* access modifiers changed from: private */
    public final List<DownloadInfoWrapper> f = new ArrayList();
    /* access modifiers changed from: private */
    public final List<DownloadInfoWrapper> g = new ArrayList();
    private Comparator<DownloadInfoWrapper> h = new ae();
    private Comparator<DownloadInfoWrapper> i = new ag();
    private Map<String, View> j = new HashMap();
    private CreatingTaskStatusEnum k = CreatingTaskStatusEnum.NONE;
    private AstApp l;
    /* access modifiers changed from: private */
    public Context m;
    private ad n = null;
    private View.OnClickListener o = null;
    private CommonViewInvalidater p;
    private ay q = new ay();
    private boolean r = false;
    /* access modifiers changed from: private */
    public boolean s = false;
    private TXExpandableListView t;
    /* access modifiers changed from: private */
    public DownloadListFooterView u;
    /* access modifiers changed from: private */
    public ApkMetaInfoLoader v = new ApkMetaInfoLoader();
    private i w = new k(this);
    private ViewInvalidateMessageHandler x = new h(this);

    /* compiled from: ProGuard */
    public enum CreatingTaskStatusEnum {
        NONE,
        CREATING,
        FAIL
    }

    public void a(boolean z) {
        this.r = z;
    }

    public void a(CommonViewInvalidater commonViewInvalidater) {
        this.p = commonViewInvalidater;
    }

    public void a(DownloadListFooterView downloadListFooterView) {
        this.u = downloadListFooterView;
    }

    public DownloadInfoMultiAdapter(Context context, TXExpandableListView tXExpandableListView, View.OnClickListener onClickListener) {
        this.m = context;
        this.t = tXExpandableListView;
        this.c = LayoutInflater.from(context);
        this.l = AstApp.i();
        this.o = onClickListener;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState
     arg types: [com.tencent.pangu.download.DownloadInfo, int, int]
     candidates:
      com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, com.tencent.assistant.plugin.PluginDownloadInfo, com.tencent.assistant.plugin.PluginInfo):com.tencent.assistant.AppConst$AppState
      com.tencent.assistant.module.k.a(java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem>, com.tencent.assistant.module.m, int):java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>
      com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState */
    public void a() {
        k();
        if (this.f.size() > 50) {
            synchronized (this.f) {
                ArrayList arrayList = new ArrayList();
                for (int i2 = 0; i2 < this.f.size(); i2++) {
                    DownloadInfoWrapper downloadInfoWrapper = this.f.get(i2);
                    if (downloadInfoWrapper.f3748a != DownloadInfoWrapper.InfoType.App) {
                        arrayList.add(Integer.valueOf(i2));
                    } else if (k.a(downloadInfoWrapper.b, true, false) == AppConst.AppState.INSTALLED) {
                        arrayList.add(Integer.valueOf(i2));
                    }
                }
                if (arrayList.size() > 50) {
                    for (int size = arrayList.size() - 1; size >= 50; size--) {
                        int intValue = ((Integer) arrayList.get(size)).intValue();
                        this.f.remove(intValue);
                        a(this.f.get(intValue));
                    }
                }
            }
        }
        l();
        notifyDataSetChanged();
        if (this.f3402a) {
            this.t.postDelayed(new d(this), 500);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState
     arg types: [com.tencent.pangu.download.DownloadInfo, int, int]
     candidates:
      com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, com.tencent.assistant.plugin.PluginDownloadInfo, com.tencent.assistant.plugin.PluginInfo):com.tencent.assistant.AppConst$AppState
      com.tencent.assistant.module.k.a(java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem>, com.tencent.assistant.module.m, int):java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>
      com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState */
    /* access modifiers changed from: package-private */
    public void b() {
        ArrayList arrayList = new ArrayList();
        for (DownloadInfoWrapper next : this.f) {
            AppConst.AppState a2 = k.a(next.b, true, false);
            if (next.f3748a == DownloadInfoWrapper.InfoType.App && !ak.a().b(next.b) && a2 == AppConst.AppState.DOWNLOADED && next.b.isDownloadFileExist()) {
                arrayList.add(next.b);
            }
        }
        if (arrayList.size() >= 5) {
            a((ArrayList<DownloadInfo>) arrayList);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.manager.DownloadProxy.b(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.pangu.manager.DownloadProxy.b(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>):java.lang.String
      com.tencent.pangu.manager.DownloadProxy.b(java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.mediadownload.e.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.pangu.mediadownload.e.a(com.tencent.pangu.mediadownload.e, java.lang.String):void
      com.tencent.pangu.mediadownload.e.a(java.lang.String, boolean):boolean */
    /* access modifiers changed from: private */
    public void a(DownloadInfoWrapper downloadInfoWrapper) {
        if (downloadInfoWrapper.f3748a == DownloadInfoWrapper.InfoType.App) {
            DownloadProxy.a().b(downloadInfoWrapper.b.downloadTicket, false);
        } else if (downloadInfoWrapper.f3748a == DownloadInfoWrapper.InfoType.Video) {
            r.c().b(downloadInfoWrapper.c.m);
        } else if (downloadInfoWrapper.f3748a == DownloadInfoWrapper.InfoType.Book) {
            b.a().a(downloadInfoWrapper.d.c);
        } else if (downloadInfoWrapper.f3748a == DownloadInfoWrapper.InfoType.CommonFile) {
            e.c().a(downloadInfoWrapper.e.m, false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.download.SimpleDownloadInfo$DownloadType, boolean):java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>
     arg types: [com.tencent.pangu.download.SimpleDownloadInfo$DownloadType, int]
     candidates:
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.manager.DownloadProxy, android.app.Dialog):android.app.Dialog
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.manager.DownloadProxy, com.tencent.assistant.net.APN):com.tencent.assistant.net.APN
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.manager.DownloadProxy, com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager):com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>):java.lang.String
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.download.DownloadInfo, com.tencent.pangu.download.SimpleDownloadInfo$DownloadState):void
      com.tencent.pangu.manager.DownloadProxy.a(java.lang.String, int):com.tencent.pangu.download.DownloadInfo
      com.tencent.pangu.manager.DownloadProxy.a(java.lang.String, boolean):void
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.download.SimpleDownloadInfo$DownloadType, boolean):java.util.ArrayList<com.tencent.pangu.download.DownloadInfo> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.manager.DownloadProxy.b(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.pangu.manager.DownloadProxy.b(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>):java.lang.String
      com.tencent.pangu.manager.DownloadProxy.b(java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState
     arg types: [com.tencent.pangu.download.DownloadInfo, int, int]
     candidates:
      com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, com.tencent.assistant.plugin.PluginDownloadInfo, com.tencent.assistant.plugin.PluginInfo):com.tencent.assistant.AppConst$AppState
      com.tencent.assistant.module.k.a(java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem>, com.tencent.assistant.module.m, int):java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>
      com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState */
    private void k() {
        AppUpdateInfo a2;
        this.d.clear();
        this.f.clear();
        ArrayList<DownloadInfo> a3 = DownloadProxy.a().a(SimpleDownloadInfo.DownloadType.APK, true);
        List<q> a4 = r.c().a();
        List<d> a5 = e.c().a();
        List<c> b2 = b.a().b();
        if (a3 != null && a3.size() > 0) {
            try {
                Collections.sort(a3);
            } catch (Exception e2) {
            }
            HashSet hashSet = new HashSet(a3.size());
            ArrayList<DownloadInfo> arrayList = new ArrayList<>(a3.size());
            for (int i2 = 0; i2 < a3.size(); i2++) {
                DownloadInfo downloadInfo = a3.get(i2);
                if (hashSet.contains(Long.valueOf(downloadInfo.appId))) {
                    DownloadProxy.a().b(downloadInfo.downloadTicket, false);
                } else {
                    hashSet.add(Long.valueOf(downloadInfo.appId));
                    arrayList.add(downloadInfo);
                }
            }
            for (DownloadInfo downloadInfo2 : arrayList) {
                if (downloadInfo2 != null && downloadInfo2.needReCreateInfo() && (a2 = j.b().a(downloadInfo2.packageName)) != null && a2.d == downloadInfo2.versionCode && a2.w == downloadInfo2.grayVersionCode) {
                    k.a(a2, downloadInfo2);
                }
                switch (j.f3451a[k.a(downloadInfo2, true, true).ordinal()]) {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                        this.d.add(new DownloadInfoWrapper(downloadInfo2));
                        break;
                    default:
                        if (ak.a().b(downloadInfo2)) {
                            downloadInfo2.downloadState = SimpleDownloadInfo.DownloadState.INSTALLED;
                        }
                        this.f.add(new DownloadInfoWrapper(downloadInfo2));
                        break;
                }
            }
            if (this.f.size() > 0) {
                synchronized (this.f) {
                    ArrayList arrayList2 = new ArrayList();
                    for (int i3 = 0; i3 < this.f.size(); i3++) {
                        DownloadInfoWrapper downloadInfoWrapper = this.f.get(i3);
                        if (downloadInfoWrapper.f3748a == DownloadInfoWrapper.InfoType.App) {
                            AppConst.AppState a6 = k.a(downloadInfoWrapper.b, true, true);
                            PackageInfo d2 = com.tencent.assistant.utils.e.d(downloadInfoWrapper.b.packageName, 0);
                            if (a6 == AppConst.AppState.INSTALLED && !ak.a().b(downloadInfoWrapper.b) && d2 == null) {
                                arrayList2.add(Integer.valueOf(i3));
                            }
                        }
                    }
                    if (arrayList2.size() > 0) {
                        for (int size = arrayList2.size() - 1; size >= 0; size--) {
                            int intValue = ((Integer) arrayList2.get(size)).intValue();
                            this.f.remove(intValue);
                            DownloadProxy.a().b(this.f.get(intValue).b.downloadTicket, false);
                        }
                    }
                }
            }
        }
        if (a4 != null && a4.size() > 0) {
            for (q next : a4) {
                if (next.s == AbstractDownloadInfo.DownState.SUCC) {
                    this.f.add(new DownloadInfoWrapper(next));
                } else {
                    this.d.add(new DownloadInfoWrapper(next));
                }
            }
        }
        if (b2 != null && b2.size() > 0) {
            for (c downloadInfoWrapper2 : b2) {
                this.f.add(new DownloadInfoWrapper(downloadInfoWrapper2));
            }
        }
        if (a5 != null && a5.size() > 0) {
            for (d next2 : a5) {
                if (next2.s == AbstractDownloadInfo.DownState.SUCC) {
                    this.f.add(new DownloadInfoWrapper(next2));
                } else {
                    this.d.add(new DownloadInfoWrapper(next2));
                }
            }
        }
        try {
            Collections.sort(this.d, this.h);
            Collections.sort(this.f, this.i);
        } catch (Exception e3) {
        }
    }

    /* access modifiers changed from: private */
    public void l() {
        DownloadInfoWrapper downloadInfoWrapper;
        this.e.clear();
        this.e.addAll(this.d);
        if (!this.r) {
            if (this.f != null) {
                this.g.clear();
                this.g.addAll(this.f);
                if (!this.s && !this.g.isEmpty()) {
                    int i2 = 0;
                    Iterator<DownloadInfoWrapper> it = this.g.iterator();
                    while (true) {
                        int i3 = i2;
                        if (it.hasNext()) {
                            DownloadInfoWrapper next = it.next();
                            if (next.f3748a != DownloadInfoWrapper.InfoType.App || next.d()) {
                                i2 = i3 + 1;
                            } else {
                                i2 = i3;
                            }
                            if (i2 > 2) {
                                it.remove();
                            }
                        } else {
                            return;
                        }
                    }
                }
            }
        } else if (this.s) {
            if (this.f != null && !this.f.isEmpty()) {
                this.e.addAll(this.f);
            }
        } else if (this.f != null && !this.f.isEmpty()) {
            Iterator<DownloadInfoWrapper> it2 = this.f.iterator();
            DownloadInfoWrapper downloadInfoWrapper2 = null;
            DownloadInfoWrapper downloadInfoWrapper3 = null;
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                DownloadInfoWrapper next2 = it2.next();
                if (downloadInfoWrapper3 != null) {
                    break;
                }
                if (next2.c()) {
                    DownloadInfoWrapper downloadInfoWrapper4 = downloadInfoWrapper2;
                    downloadInfoWrapper = next2;
                    next2 = downloadInfoWrapper4;
                } else if (downloadInfoWrapper2 == null) {
                    downloadInfoWrapper = downloadInfoWrapper3;
                } else {
                    next2 = downloadInfoWrapper2;
                    downloadInfoWrapper = downloadInfoWrapper3;
                }
                if (downloadInfoWrapper != null && next2 != null) {
                    downloadInfoWrapper3 = downloadInfoWrapper;
                    downloadInfoWrapper2 = next2;
                    break;
                }
                downloadInfoWrapper3 = downloadInfoWrapper;
                downloadInfoWrapper2 = next2;
            }
            if (downloadInfoWrapper3 != null) {
                this.e.add(downloadInfoWrapper3);
            } else if (downloadInfoWrapper2 != null) {
                this.e.add(downloadInfoWrapper2);
            }
        }
    }

    public void a(String str, InstalledAppItem installedAppItem) {
        DownloadInfo downloadInfo;
        InstalledAppItem installedAppItem2;
        int i2;
        DownloadInfo downloadInfo2;
        if (this.u != null) {
            if (this.e.size() > 0) {
                Iterator<DownloadInfoWrapper> it = this.e.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        downloadInfo2 = null;
                        break;
                    }
                    DownloadInfoWrapper next = it.next();
                    if (next.f3748a == DownloadInfoWrapper.InfoType.App) {
                        downloadInfo2 = next.b;
                        break;
                    }
                }
                downloadInfo = downloadInfo2;
            } else {
                if (this.g.size() > 0) {
                    Iterator<DownloadInfoWrapper> it2 = this.g.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            break;
                        }
                        DownloadInfoWrapper next2 = it2.next();
                        if (next2.f3748a == DownloadInfoWrapper.InfoType.App) {
                            downloadInfo = next2.b;
                            break;
                        }
                    }
                }
                downloadInfo = null;
            }
            if (installedAppItem == null) {
                InstalledAppItem installedAppItem3 = new InstalledAppItem();
                installedAppItem3.b = downloadInfo != null ? downloadInfo.appId : 0;
                installedAppItem3.f1395a = downloadInfo != null ? downloadInfo.packageName : Constants.STR_EMPTY;
                if (downloadInfo != null) {
                    i2 = downloadInfo.versionCode;
                } else {
                    i2 = 0;
                }
                installedAppItem3.c = i2;
                installedAppItem2 = installedAppItem3;
            } else {
                installedAppItem2 = installedAppItem;
            }
            ArrayList<InstalledAppItem> a2 = a(this.e, 5);
            ArrayList<InstalledAppItem> a3 = a(this.g, 2);
            this.q.register(this.w);
            this.q.a(2, installedAppItem2, a3, a2, (byte) 0, str);
        }
    }

    /* access modifiers changed from: private */
    public ArrayList<RecommendAppInfo> a(List<RecommendAppInfo> list) {
        ArrayList<RecommendAppInfo> arrayList = new ArrayList<>();
        for (RecommendAppInfo next : list) {
            List<DownloadInfo> e2 = DownloadProxy.a().e(next.a());
            if (e2 == null || e2.isEmpty()) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    public ArrayList<RecommendAppInfo> b(List<RecommendAppInfo> list) {
        ArrayList<RecommendAppInfo> arrayList = new ArrayList<>();
        for (RecommendAppInfo next : list) {
            if (ApkResourceManager.getInstance().getLocalApkInfo(next.a()) == null) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    private ArrayList<InstalledAppItem> a(List<DownloadInfoWrapper> list, int i2) {
        if (list == null || list.isEmpty()) {
            return null;
        }
        ArrayList<InstalledAppItem> arrayList = new ArrayList<>();
        int i3 = 0;
        for (DownloadInfoWrapper next : list) {
            if (next != null && next.f3748a == DownloadInfoWrapper.InfoType.App) {
                if (i3 >= i2) {
                    break;
                }
                InstalledAppItem installedAppItem = new InstalledAppItem();
                installedAppItem.a(next.b.appId);
                installedAppItem.a(next.b.packageName);
                installedAppItem.a(next.b.versionCode);
                arrayList.add(installedAppItem);
                i3++;
            }
        }
        return arrayList;
    }

    private int m() {
        return this.d.size() + this.f.size();
    }

    public int c() {
        return this.e.size() + this.g.size();
    }

    public void d() {
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_INSTALL, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING, this);
        this.l.k().addUIEventListener(1007, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING_START, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_ADD, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_START, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOADING, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_PAUSE, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_QUEUING, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_FAIL, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_DELETE, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_SUCC, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_ADD, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_START, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOADING, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_PAUSE, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_QUEUING, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_FAIL, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_DELETE, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_SUCC, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_FILENAME, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC, this);
        this.l.k().addUIEventListener(1027, this);
    }

    public void e() {
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_INSTALL, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING, this);
        this.l.k().removeUIEventListener(1007, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING_START, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_ADD, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_START, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOADING, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_PAUSE, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_QUEUING, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_FAIL, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_DELETE, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_SUCC, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_ADD, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_START, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOADING, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_PAUSE, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_QUEUING, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_FAIL, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_DELETE, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_SUCC, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_FILENAME, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC, this);
        this.l.k().removeUIEventListener(1027, this);
    }

    public int getGroupCount() {
        int i2;
        int i3 = 1;
        if (this.e.size() > 0 || this.k != CreatingTaskStatusEnum.NONE) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        if (this.g.size() <= 0) {
            i3 = 0;
        }
        return i2 + i3;
    }

    public int a(int i2) {
        if (i2 == 0 && (this.e.size() > 0 || this.k != CreatingTaskStatusEnum.NONE)) {
            return 0;
        }
        if (i2 != -1) {
            return 1;
        }
        return -1;
    }

    public int getChildrenCount(int i2) {
        if (a(i2) == 0) {
            return this.e.size() + 1;
        }
        return this.g.size();
    }

    public Object getGroup(int i2) {
        return Integer.valueOf(i2);
    }

    @SuppressLint({"Override"})
    public int getChildType(int i2, int i3) {
        if (a(i2) == 0 && i3 == 0) {
            return 1;
        }
        DownloadInfoWrapper downloadInfoWrapper = (DownloadInfoWrapper) getChild(i2, i3);
        if (downloadInfoWrapper == null) {
            return 0;
        }
        if (downloadInfoWrapper.f3748a == DownloadInfoWrapper.InfoType.Video) {
            return 2;
        }
        if (downloadInfoWrapper.f3748a == DownloadInfoWrapper.InfoType.Book) {
            return 3;
        }
        return downloadInfoWrapper.f3748a == DownloadInfoWrapper.InfoType.CommonFile ? 4 : 0;
    }

    @SuppressLint({"Override"})
    public int getChildTypeCount() {
        return 5;
    }

    private int c(int i2) {
        return i2 - 1;
    }

    public Object getChild(int i2, int i3) {
        if (a(i2) == 0) {
            int c2 = c(i3);
            if (c2 < 0 || c2 >= this.e.size()) {
                return null;
            }
            return this.e.get(c2);
        } else if (this.g.size() > i3) {
            return this.g.get(i3);
        } else {
            return null;
        }
    }

    public long getGroupId(int i2) {
        return (long) i2;
    }

    public long getChildId(int i2, int i3) {
        return (long) i3;
    }

    public boolean hasStableIds() {
        return false;
    }

    public View getGroupView(int i2, boolean z, View view, ViewGroup viewGroup) {
        aa aaVar;
        if (i2 < 0) {
            return null;
        }
        if (view == null || view.getTag() == null || !(view.getTag() instanceof aa) || ((aa) view.getTag()).f3409a == null) {
            view = this.c.inflate((int) R.layout.downloadapp_group_item, (ViewGroup) null);
            ah ahVar = new ah(this, null);
            ahVar.f3412a = view.findViewById(R.id.group_layout);
            ahVar.b = (TextView) view.findViewById(R.id.group_title);
            ahVar.c = (TextView) view.findViewById(R.id.group_title_num);
            ahVar.d = (Button) view.findViewById(R.id.group_action);
            aaVar = new aa();
            aaVar.f3409a = ahVar;
            view.setTag(aaVar);
        } else {
            aaVar = (aa) view.getTag();
        }
        ah ahVar2 = aaVar.f3409a;
        String[] b2 = b(i2);
        if (b2 != null) {
            ahVar2.b.setText(b2[0]);
            ahVar2.c.setText(" " + b2[1]);
        }
        if (a(i2) == 1) {
            ahVar2.d.setText(this.m.getResources().getString(R.string.down_page_group_clear_text));
            ahVar2.d.setVisibility(0);
            ahVar2.d.setOnClickListener(this.b);
        } else {
            ahVar2.d.setVisibility(8);
        }
        if (!this.r) {
            return view;
        }
        ahVar2.f3412a.setVisibility(8);
        return view;
    }

    public String[] b(int i2) {
        if (a(i2) == 0) {
            int size = this.e.size();
            String[] strArr = new String[2];
            strArr[0] = this.m.getResources().getString(R.string.down_page_group_downloading_title);
            StringBuilder append = new StringBuilder().append("(");
            if (this.k != CreatingTaskStatusEnum.NONE) {
                size++;
            }
            strArr[1] = append.append(size).append(")").toString();
            return strArr;
        }
        return new String[]{this.m.getResources().getString(R.string.down_page_group_finish_title), "(" + this.f.size() + ")"};
    }

    public View getChildView(int i2, int i3, boolean z, View view, ViewGroup viewGroup) {
        ak akVar;
        View view2;
        XLog.d("DownloadActivity", "getchildView:childposition:" + i3 + ",group:" + i2);
        boolean z2 = i2 == getGroupCount() + -1;
        int childType = getChildType(i2, i3);
        DownloadInfoWrapper downloadInfoWrapper = (DownloadInfoWrapper) getChild(i2, i3);
        if (downloadInfoWrapper == null && childType != 1) {
            return null;
        }
        String a2 = a(i2, i3);
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.m, 200);
        buildSTInfo.slotId = a2;
        if (view == null || view.getTag() == null || !(view.getTag() instanceof aa)) {
            aa aaVar = new aa();
            akVar = new ak();
            if (childType == 3) {
                ac r2 = r();
                akVar.d = r2;
                view2 = r2.f;
            } else if (childType == 2) {
                ai p2 = p();
                akVar.c = p2;
                view2 = p2.f;
            } else if (childType == 1) {
                ad n2 = n();
                if (n2 == null) {
                    return new View(this.m);
                }
                akVar.b = n2;
                view2 = n2.f3411a;
            } else if (childType == 4) {
                af q2 = q();
                akVar.e = q2;
                view2 = q2.f;
            } else {
                aj o2 = o();
                akVar.f3413a = o2;
                view2 = o2.f;
            }
            aaVar.b = akVar;
            if (view2 == null) {
                return new View(this.m);
            }
            view2.setTag(aaVar);
        } else {
            aa aaVar2 = (aa) view.getTag();
            if (aaVar2.b == null) {
                aaVar2.b = new ak();
            }
            ak akVar2 = aaVar2.b;
            if (childType == 0 && akVar2.f3413a == null) {
                akVar2.f3413a = o();
            } else if (childType == 1 && akVar2.b == null) {
                akVar2.b = n();
            } else if (childType == 2 && akVar2.c == null) {
                akVar2.c = p();
            } else if (childType == 3 && akVar2.d == null) {
                akVar2.d = r();
            } else if (childType == 4 && akVar2.e == null) {
                akVar2.e = q();
            }
            ak akVar3 = akVar2;
            view2 = akVar2.a(childType);
            akVar = akVar3;
        }
        if (childType == 1) {
            akVar.b.e.setBackgroundResource(R.drawable.common_download_item_selector);
            a(akVar.b, this.k);
            if (z) {
                akVar.b.f.setVisibility(8);
            } else {
                akVar.b.f.setVisibility(0);
            }
        } else {
            a(downloadInfoWrapper, view2);
            if (childType == 0) {
                DownloadInfo downloadInfo = downloadInfoWrapper.b;
                aj ajVar = akVar.f3413a;
                ajVar.f3410a = i3;
                ajVar.b = i2;
                ajVar.o = downloadInfo.downloadTicket;
                a(ajVar, downloadInfo, buildSTInfo);
                ajVar.g.setBackgroundResource(R.drawable.common_download_item_selector);
                a(i3, z, z2, ajVar);
            } else if (childType == 2) {
                q qVar = downloadInfoWrapper.c;
                ai aiVar = akVar.c;
                aiVar.f3410a = i3;
                aiVar.b = i2;
                aiVar.o = qVar.m;
                a(aiVar, qVar, buildSTInfo);
                aiVar.g.setBackgroundResource(R.drawable.common_download_item_selector);
                a(i3, z, z2, aiVar);
            } else if (childType == 4) {
                d dVar = downloadInfoWrapper.e;
                af afVar = akVar.e;
                afVar.f3410a = i3;
                afVar.b = i2;
                afVar.o = dVar.m;
                a(afVar, dVar, buildSTInfo);
                afVar.g.setBackgroundResource(R.drawable.common_download_item_selector);
                a(i3, z, z2, afVar);
            } else if (childType == 3) {
                c cVar = downloadInfoWrapper.d;
                ac acVar = akVar.d;
                acVar.f3410a = i3;
                acVar.b = i2;
                acVar.l = cVar.c;
                a(acVar, cVar, buildSTInfo);
                acVar.g.setBackgroundResource(R.drawable.common_download_item_selector);
                a(i3, z, z2, acVar);
            }
        }
        if (z) {
            view2.setPadding(0, 0, 0, by.a(this.m, 5.0f));
        } else {
            view2.setPadding(0, 0, 0, 0);
        }
        return view2;
    }

    private void a(int i2, boolean z, boolean z2, ab abVar) {
        abVar.c.setVisibility(8);
        if (z) {
            boolean z3 = c() >= m();
            if (!z2 || this.s || z3) {
                abVar.e.setVisibility(8);
                return;
            }
            abVar.e.setVisibility(0);
            abVar.c.setVisibility(0);
            abVar.c.setOnClickListener(new n(this, abVar));
        } else if (i2 == 0) {
            abVar.e.setVisibility(0);
        } else {
            abVar.e.setVisibility(0);
        }
    }

    private void a(DownloadInfoWrapper downloadInfoWrapper, View view) {
        if (downloadInfoWrapper != null && view != null) {
            if (downloadInfoWrapper.f3748a == DownloadInfoWrapper.InfoType.App) {
                this.j.put("app|" + downloadInfoWrapper.b.downloadTicket, view);
            } else if (downloadInfoWrapper.f3748a == DownloadInfoWrapper.InfoType.Video) {
                this.j.put("video|" + downloadInfoWrapper.c.m, view);
            } else if (downloadInfoWrapper.f3748a == DownloadInfoWrapper.InfoType.Book) {
                this.j.put("book|" + downloadInfoWrapper.d.c, view);
            } else if (downloadInfoWrapper.f3748a == DownloadInfoWrapper.InfoType.CommonFile) {
                this.j.put("file|" + downloadInfoWrapper.e.m, view);
            }
        }
    }

    private View a(String str) {
        if (this.j != null) {
            return this.j.get("app|" + str);
        }
        return null;
    }

    private ai b(String str) {
        View view;
        if (!(str == null || (view = this.j.get("video|" + str)) == null || !(view.getTag() instanceof aa))) {
            aa aaVar = (aa) view.getTag();
            if (aaVar == null || aaVar.b == null || aaVar.b.c == null) {
                return null;
            }
            ai aiVar = aaVar.b.c;
            if (aiVar.o != null && aiVar.o.equals(str)) {
                return aiVar;
            }
        }
        return null;
    }

    private af c(String str) {
        View view;
        if (!(str == null || (view = this.j.get("file|" + str)) == null || !(view.getTag() instanceof aa))) {
            aa aaVar = (aa) view.getTag();
            if (aaVar == null || aaVar.b == null || aaVar.b.e == null) {
                return null;
            }
            af afVar = aaVar.b.e;
            if (afVar.o != null && afVar.o.equals(str)) {
                return afVar;
            }
        }
        return null;
    }

    private void a(ad adVar, CreatingTaskStatusEnum creatingTaskStatusEnum) {
        if (adVar == null) {
            return;
        }
        if (creatingTaskStatusEnum == CreatingTaskStatusEnum.NONE) {
            adVar.e.setVisibility(8);
            adVar.f3411a.setLayoutParams(new AbsListView.LayoutParams(0, 0));
            return;
        }
        switch (j.b[creatingTaskStatusEnum.ordinal()]) {
            case 1:
                adVar.b.setImageResource(R.drawable.small_morefunction_installion);
                adVar.b.startAnimation(AnimationUtils.loadAnimation(this.m, R.anim.circle));
                adVar.c.setText(this.m.getResources().getString(R.string.down_page_pop_loading));
                adVar.d.setVisibility(4);
                break;
            case 2:
                adVar.b.setAnimation(null);
                adVar.d.setText(this.m.getResources().getString(R.string.down_page_pop_retry));
                adVar.b.setImageResource(R.drawable.common_icon_appdetail_didnotpast);
                adVar.c.setText(this.m.getResources().getString(R.string.down_page_pop_fail));
                adVar.d.setVisibility(0);
                adVar.d.setTextColor(this.m.getResources().getColor(R.color.state_update));
                break;
        }
        adVar.f3411a.setLayoutParams(new AbsListView.LayoutParams(-1, -2));
        adVar.e.setVisibility(0);
    }

    private String a(int i2, int i3) {
        if (a(i2) == 0) {
            return a.a("01", i3);
        }
        return a.a("02", i3);
    }

    private void a(ai aiVar, q qVar, STInfoV2 sTInfoV2) {
        if (aiVar != null && qVar != null) {
            aiVar.h.updateImageView(qVar.c, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            aiVar.j.setText(qVar.f3868a);
            if (sTInfoV2 != null) {
                sTInfoV2.resourceType = com.tencent.assistantv2.st.page.d.d;
                sTInfoV2.packageName = qVar.m;
            }
            aiVar.i.a(qVar, sTInfoV2);
            a(aiVar, qVar);
            aiVar.p.setOnClickListener(new o(this, qVar, sTInfoV2));
        }
    }

    private void a(af afVar, d dVar, STInfoV2 sTInfoV2) {
        if (afVar != null && dVar != null) {
            if (sTInfoV2 != null) {
                sTInfoV2.resourceType = com.tencent.assistantv2.st.page.d.g;
                sTInfoV2.packageName = dVar.m;
            }
            afVar.i.a(dVar, sTInfoV2);
            a(afVar, dVar);
            afVar.p.setOnClickListener(new r(this, dVar, sTInfoV2));
        }
    }

    private void a(TXImageView tXImageView, TextView textView, o oVar, d dVar) {
        tXImageView.setTag(dVar.m);
        textView.setText(dVar.g());
        switch (j.c[oVar.d.ordinal()]) {
            case 1:
                tXImageView.setImageResource(R.drawable.icon_file_music);
                return;
            case 2:
                tXImageView.setImageResource(R.drawable.icon_file_video);
                return;
            case 3:
                tXImageView.setImageResource(R.drawable.icon_file_txt);
                return;
            case 4:
                tXImageView.setImageResource(R.drawable.icon_file_pic);
                return;
            case 5:
                tXImageView.setImageResource(R.drawable.icon_file_zip);
                return;
            case 6:
                if (dVar.s == AbstractDownloadInfo.DownState.SUCC) {
                    ApkMetaInfoLoader.MetaInfo metaInfoFromCache = this.v.getMetaInfoFromCache(dVar.r);
                    if (metaInfoFromCache == null || metaInfoFromCache.icon == null) {
                        TemporaryThreadManager.get().start(new u(this, dVar, tXImageView, textView));
                        return;
                    }
                    if (metaInfoFromCache.icon != null) {
                        tXImageView.setImageDrawable(metaInfoFromCache.icon);
                    }
                    if (!TextUtils.isEmpty(metaInfoFromCache.name)) {
                        textView.setText(metaInfoFromCache.name);
                        return;
                    }
                    return;
                }
                tXImageView.setImageResource(R.drawable.icon_file_apk);
                return;
            default:
                tXImageView.setImageResource(R.drawable.icon_file_other);
                return;
        }
    }

    private void a(ac acVar, c cVar, STInfoV2 sTInfoV2) {
        String str;
        if (acVar != null && cVar != null) {
            if (sTInfoV2 != null) {
                sTInfoV2.resourceType = com.tencent.assistantv2.st.page.d.b;
                sTInfoV2.packageName = cVar.c;
            }
            acVar.h.updateImageView(cVar.d, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            acVar.j.setText(cVar.f3855a);
            acVar.i.a(cVar, sTInfoV2);
            if (cVar.f <= -1) {
                str = "全本";
            } else {
                str = "共下载" + (cVar.f == 0 ? 1 : cVar.f) + "章";
            }
            acVar.k.setText(String.format(this.m.getResources().getString(R.string.down_page_book_desc), str, bo.g(cVar.j)));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.utils.at.a(float, boolean):java.lang.String
     arg types: [float, int]
     candidates:
      com.tencent.assistant.utils.at.a(float, float):java.lang.String
      com.tencent.assistant.utils.at.a(long, int):java.lang.String
      com.tencent.assistant.utils.at.a(float, boolean):java.lang.String */
    private void a(ai aiVar, q qVar) {
        float f2;
        if (aiVar != null && qVar != null) {
            if (qVar.s != AbstractDownloadInfo.DownState.SUCC) {
                float f3 = qVar.u == null ? 0.0f : (float) qVar.u.b;
                if (qVar.s != AbstractDownloadInfo.DownState.DOWNLOADING) {
                    aiVar.n.mySetText(f3, at.a(f3, qVar.u.f3881a == 0 ? (float) qVar.n : (float) qVar.u.f3881a));
                } else if (qVar.u != null) {
                    DownloadNumView downloadNumView = aiVar.n;
                    float f4 = (float) qVar.u.f3881a;
                    StringBuilder append = new StringBuilder().append("/");
                    if (qVar.u.f3881a == 0) {
                        f2 = (float) qVar.n;
                    } else {
                        f2 = (float) qVar.u.f3881a;
                    }
                    downloadNumView.showWithAnimation(f3, f4, Constants.STR_EMPTY, append.append(at.a(f2, true)).toString());
                }
                aiVar.n.setVisibility(0);
                int c2 = qVar.c();
                aiVar.k.setVisibility(0);
                if (qVar.s == AbstractDownloadInfo.DownState.DOWNLOADING) {
                    aiVar.m.setText(String.format(this.m.getResources().getString(R.string.down_page_downloading), qVar.u.c));
                    aiVar.m.setVisibility(0);
                    aiVar.k.b(c2);
                } else if (qVar.s == AbstractDownloadInfo.DownState.QUEUING) {
                    aiVar.m.setVisibility(0);
                    aiVar.m.setText(this.m.getResources().getString(R.string.down_page_queuing));
                    aiVar.k.setProgress(c2);
                } else {
                    aiVar.k.setProgress(c2);
                    aiVar.m.setVisibility(0);
                    aiVar.m.setText(this.m.getResources().getString(R.string.down_page_pause));
                }
                aiVar.m.setTextSize(0, (float) this.m.getResources().getDimensionPixelSize(R.dimen.app_down_item_gray_text_size));
                aiVar.p.setVisibility(0);
            } else {
                aiVar.p.setVisibility(8);
                aiVar.n.setVisibility(8);
                aiVar.k.setVisibility(8);
                aiVar.m.setVisibility(0);
                aiVar.m.setText(String.format(this.m.getResources().getString(R.string.down_page_video_name), bo.g(qVar.p)));
                aiVar.m.setTextSize(0, (float) this.m.getResources().getDimensionPixelSize(R.dimen.app_down_item_end_time_text_size));
            }
            aiVar.i.a();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.utils.at.a(float, boolean):java.lang.String
     arg types: [float, int]
     candidates:
      com.tencent.assistant.utils.at.a(float, float):java.lang.String
      com.tencent.assistant.utils.at.a(long, int):java.lang.String
      com.tencent.assistant.utils.at.a(float, boolean):java.lang.String */
    private void a(af afVar, d dVar) {
        float f2;
        if (afVar != null && dVar != null) {
            o b2 = FileOpenSelector.b(dVar.r);
            a(afVar.h, afVar.j, b2, dVar);
            if (dVar.s != AbstractDownloadInfo.DownState.SUCC) {
                float f3 = (float) dVar.u.b;
                if (dVar.s != AbstractDownloadInfo.DownState.DOWNLOADING) {
                    afVar.n.mySetText(f3, at.a(f3, dVar.u.f3881a == 0 ? (float) dVar.n : (float) dVar.u.f3881a));
                } else if (dVar.u != null) {
                    DownloadNumView downloadNumView = afVar.n;
                    float f4 = (float) dVar.u.f3881a;
                    StringBuilder append = new StringBuilder().append("/");
                    if (dVar.u.f3881a == 0) {
                        f2 = (float) dVar.n;
                    } else {
                        f2 = (float) dVar.u.f3881a;
                    }
                    downloadNumView.showWithAnimation(f3, f4, Constants.STR_EMPTY, append.append(at.a(f2, true)).toString());
                }
                afVar.n.setVisibility(0);
                int c2 = dVar.c();
                afVar.k.setVisibility(0);
                if (dVar.s == AbstractDownloadInfo.DownState.DOWNLOADING) {
                    afVar.k.b(c2);
                    afVar.m.setText(String.format(this.m.getResources().getString(R.string.down_page_downloading), dVar.u.c));
                    afVar.m.setVisibility(0);
                } else if (dVar.s == AbstractDownloadInfo.DownState.QUEUING) {
                    afVar.k.setProgress(c2);
                    afVar.m.setVisibility(0);
                    afVar.m.setText(this.m.getResources().getString(R.string.down_page_queuing));
                } else {
                    afVar.k.setProgress(c2);
                    afVar.m.setVisibility(0);
                    afVar.m.setText(this.m.getResources().getString(R.string.down_page_pause));
                }
                afVar.m.setTextSize(0, (float) this.m.getResources().getDimensionPixelSize(R.dimen.app_down_item_gray_text_size));
                afVar.p.setVisibility(0);
            } else {
                afVar.p.setVisibility(8);
                afVar.n.setVisibility(8);
                afVar.k.setVisibility(8);
                afVar.m.setVisibility(0);
                afVar.m.setText(a(b2, dVar));
                afVar.m.setTextSize(0, (float) this.m.getResources().getDimensionPixelSize(R.dimen.app_down_item_end_time_text_size));
            }
            afVar.i.a();
        }
    }

    private String a(o oVar, d dVar) {
        String str;
        switch (j.c[oVar.d.ordinal()]) {
            case 1:
                str = "音乐(来自外部)";
                break;
            case 2:
                str = "视频(来自外部)";
                break;
            case 3:
            default:
                str = "其他(来自外部)";
                break;
            case 4:
                str = "图片(来自外部)";
                break;
            case 5:
                str = "压缩包(来自外部)";
                break;
            case 6:
                str = "应用(来自外部)";
                break;
            case 7:
                str = "文档(来自外部)";
                break;
        }
        return str + "　" + bo.g(dVar.p);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState
     arg types: [com.tencent.pangu.download.DownloadInfo, int, int]
     candidates:
      com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, com.tencent.assistant.plugin.PluginDownloadInfo, com.tencent.assistant.plugin.PluginInfo):com.tencent.assistant.AppConst$AppState
      com.tencent.assistant.module.k.a(java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem>, com.tencent.assistant.module.m, int):java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>
      com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.adapter.aj, boolean):void
     arg types: [com.tencent.pangu.adapter.aj, int]
     candidates:
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(int, int):java.lang.String
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.mediadownload.o, com.tencent.pangu.model.d):java.lang.String
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.adapter.DownloadInfoMultiAdapter, java.util.List):java.util.ArrayList
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(java.util.List<com.tencent.pangu.download.DownloadInfoWrapper>, int):java.util.ArrayList<com.tencent.assistant.protocol.jce.InstalledAppItem>
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.adapter.DownloadInfoMultiAdapter, com.tencent.assistant.component.invalidater.ViewInvalidateMessage):void
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.adapter.DownloadInfoMultiAdapter, com.tencent.pangu.download.DownloadInfo):void
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.adapter.DownloadInfoMultiAdapter, com.tencent.pangu.download.DownloadInfoWrapper):void
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.adapter.ad, com.tencent.pangu.adapter.DownloadInfoMultiAdapter$CreatingTaskStatusEnum):void
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.adapter.af, com.tencent.pangu.model.d):void
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.adapter.ai, com.tencent.pangu.mediadownload.q):void
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.download.DownloadInfoWrapper, android.view.View):void
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.adapter.DownloadInfoMultiAdapter, boolean):boolean
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(java.util.List<com.tencent.pangu.download.DownloadInfoWrapper>, long):boolean
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(java.util.List<com.tencent.pangu.download.DownloadInfoWrapper>, com.tencent.pangu.download.DownloadInfoWrapper):boolean
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(java.lang.String, com.tencent.assistant.protocol.jce.InstalledAppItem):void
      com.tencent.pangu.adapter.DownloadInfoMultiAdapter.a(com.tencent.pangu.adapter.aj, boolean):void */
    private void a(aj ajVar, DownloadInfo downloadInfo, STInfoV2 sTInfoV2) {
        if (ajVar == null) {
            return;
        }
        if (downloadInfo != null) {
            AppConst.AppState a2 = k.a(downloadInfo, true, true);
            if (sTInfoV2 != null) {
                if (downloadInfo.statInfo != null) {
                    String str = downloadInfo.statInfo.callerUin;
                    String str2 = downloadInfo.statInfo.callerVia;
                    if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
                        sTInfoV2.updateWithExternalPara(str2, str, downloadInfo.hostPackageName, downloadInfo.hostVersionCode, downloadInfo.statInfo.traceId);
                    }
                }
                sTInfoV2.updateWithDownloadInfo(downloadInfo);
            }
            if (!(ajVar.h == null || downloadInfo == null)) {
                if (!TextUtils.isEmpty(downloadInfo.iconUrl)) {
                    ajVar.h.updateImageView(downloadInfo.iconUrl, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
                } else if (downloadInfo.isUpdate == 1) {
                    ajVar.h.updateImageView(downloadInfo.packageName, R.drawable.pic_defaule, TXImageView.TXImageViewType.INSTALL_APK_ICON);
                }
            }
            ajVar.i.a(downloadInfo);
            ajVar.i.setOnClickListener(new w(this, downloadInfo, sTInfoV2));
            ajVar.p.setOnClickListener(new x(this, downloadInfo, sTInfoV2));
            ajVar.j.setText(downloadInfo.name);
            a(ajVar, downloadInfo, a2);
            a(ajVar, true);
            return;
        }
        a(ajVar, false);
    }

    private void a(aj ajVar, boolean z) {
        if (z) {
            ajVar.f.setVisibility(0);
        } else {
            ajVar.f.setVisibility(8);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.utils.at.a(float, boolean):java.lang.String
     arg types: [float, int]
     candidates:
      com.tencent.assistant.utils.at.a(float, float):java.lang.String
      com.tencent.assistant.utils.at.a(long, int):java.lang.String
      com.tencent.assistant.utils.at.a(float, boolean):java.lang.String */
    private void a(aj ajVar, DownloadInfo downloadInfo, AppConst.AppState appState) {
        float f2 = 0.0f;
        switch (j.f3451a[appState.ordinal()]) {
            case 2:
                int a2 = k.a(downloadInfo, appState);
                ajVar.k.setVisibility(0);
                ajVar.k.b(a2);
                ajVar.n.setVisibility(0);
                ajVar.l.setText(String.format(this.m.getString(R.string.down_page_download_percent), Integer.valueOf(a2)));
                if (!(downloadInfo == null || downloadInfo.response == null)) {
                    f2 = (float) downloadInfo.response.f3766a;
                }
                ajVar.n.showWithAnimation(f2, (float) SimpleDownloadInfo.getDownloadDisplayByteSize(downloadInfo), Constants.STR_EMPTY, "/" + at.a((float) SimpleDownloadInfo.getDownloadDisplayByteSize(downloadInfo), true));
                TextView textView = ajVar.m;
                String string = this.m.getResources().getString(R.string.down_page_downloading);
                Object[] objArr = new Object[1];
                objArr[0] = (downloadInfo == null || downloadInfo.response == null) ? 0 : downloadInfo.response.c;
                textView.setText(String.format(string, objArr));
                ajVar.m.setTextSize(0, (float) this.m.getResources().getDimensionPixelSize(R.dimen.app_down_item_gray_text_size));
                ajVar.p.setVisibility(0);
                b(ajVar);
                return;
            case 3:
            case 6:
                int a3 = k.a(downloadInfo, appState);
                ajVar.k.setVisibility(0);
                ajVar.n.setVisibility(0);
                ajVar.k.a(a3);
                ajVar.l.setText(String.format(this.m.getString(R.string.down_page_download_percent), Integer.valueOf(a3)));
                if (!(downloadInfo == null || downloadInfo.response == null)) {
                    f2 = (float) downloadInfo.response.f3766a;
                }
                ajVar.n.mySetText(f2, at.a(f2, false) + "/" + at.a((float) SimpleDownloadInfo.getDownloadDisplayByteSize(downloadInfo), true));
                ajVar.m.setText(this.m.getResources().getString(R.string.down_page_pause));
                ajVar.m.setTextSize(0, (float) this.m.getResources().getDimensionPixelSize(R.dimen.app_down_item_gray_text_size));
                ajVar.p.setVisibility(0);
                b(ajVar);
                return;
            case 4:
                int a4 = k.a(downloadInfo, appState);
                ajVar.k.setVisibility(0);
                ajVar.n.setVisibility(0);
                ajVar.k.a(a4);
                ajVar.l.setText(String.format(this.m.getString(R.string.down_page_download_percent), Integer.valueOf(a4)));
                if (!(downloadInfo == null || downloadInfo.response == null)) {
                    f2 = (float) downloadInfo.response.f3766a;
                }
                ajVar.n.mySetText(f2, at.a(f2, false) + "/" + at.a((float) SimpleDownloadInfo.getDownloadDisplayByteSize(downloadInfo), true));
                ajVar.m.setText(this.m.getResources().getString(R.string.down_page_queuing));
                ajVar.m.setTextSize(0, (float) this.m.getResources().getDimensionPixelSize(R.dimen.app_down_item_gray_text_size));
                ajVar.p.setVisibility(0);
                b(ajVar);
                return;
            case 5:
            default:
                b(ajVar);
                ajVar.k.setVisibility(4);
                ajVar.n.setVisibility(4);
                if (ak.a().c(downloadInfo.packageName)) {
                    ajVar.m.setText(String.format(this.m.getString(R.string.down_page_download_qube_finish), bo.g(downloadInfo.downloadEndTime)));
                } else {
                    ajVar.m.setText(String.format(this.m.getString(R.string.down_page_download_finish), bo.g(downloadInfo.downloadEndTime)));
                }
                ajVar.m.setTextSize(0, (float) this.m.getResources().getDimensionPixelSize(R.dimen.app_down_item_end_time_text_size));
                ajVar.p.setVisibility(8);
                return;
            case 7:
                a(ajVar);
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState
     arg types: [com.tencent.pangu.download.DownloadInfo, int, int]
     candidates:
      com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, com.tencent.assistant.plugin.PluginDownloadInfo, com.tencent.assistant.plugin.PluginInfo):com.tencent.assistant.AppConst$AppState
      com.tencent.assistant.module.k.a(java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem>, com.tencent.assistant.module.m, int):java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>
      com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState */
    /* access modifiers changed from: private */
    public void a(DownloadInfo downloadInfo) {
        switch (j.f3451a[k.a(downloadInfo, true, true).ordinal()]) {
            case 1:
            case 5:
                com.tencent.pangu.download.a.a().a(downloadInfo);
                return;
            case 2:
            case 4:
                com.tencent.pangu.download.a.a().b(downloadInfo.downloadTicket);
                return;
            case 3:
                com.tencent.pangu.download.a.a().b(downloadInfo);
                return;
            case 6:
            case 10:
                com.tencent.pangu.download.a.a().a(downloadInfo);
                return;
            case 7:
                Toast.makeText(this.m, (int) R.string.tips_slicent_install, 0).show();
                return;
            case 8:
                if (!downloadInfo.isApkFileExist()) {
                    AppConst.TwoBtnDialogInfo b2 = b(downloadInfo);
                    if (b2 != null) {
                        DialogUtils.show2BtnDialog(b2);
                        return;
                    }
                    return;
                }
                com.tencent.pangu.download.a.a().d(downloadInfo);
                return;
            case 9:
                String str = downloadInfo.packageName;
                if (TextUtils.isEmpty(str)) {
                    return;
                }
                if (ak.a().b(downloadInfo)) {
                    ak.a().e(downloadInfo);
                    return;
                } else if (AstApp.i().getPackageManager().getLaunchIntentForPackage(str) != null) {
                    com.tencent.pangu.download.a.a().c(str);
                    return;
                } else if (com.tencent.assistant.utils.e.d(str, 0) != null) {
                    e eVar = new e(this, downloadInfo);
                    eVar.titleRes = this.m.getResources().getString(R.string.down_uninstall_title);
                    eVar.contentRes = this.m.getResources().getString(R.string.down_cannot_open_tips);
                    eVar.btnTxtRes = this.m.getResources().getString(R.string.down_uninstall_tips_close);
                    DialogUtils.show1BtnDialog(eVar);
                    return;
                } else {
                    boolean isSuccApkFileExist = downloadInfo.isSuccApkFileExist();
                    f fVar = new f(this, isSuccApkFileExist, downloadInfo);
                    fVar.hasTitle = true;
                    fVar.titleRes = this.m.getResources().getString(R.string.down_open_title);
                    if (isSuccApkFileExist) {
                        fVar.contentRes = this.m.getResources().getString(R.string.down_open_install_content);
                        fVar.rBtnTxtRes = this.m.getResources().getString(R.string.down_page_dialog_right_install);
                    } else {
                        fVar.contentRes = this.m.getResources().getString(R.string.down_open_download_content);
                        fVar.rBtnTxtRes = this.m.getResources().getString(R.string.down_page_dialog_right_down);
                    }
                    fVar.lBtnTxtRes = this.m.getResources().getString(R.string.down_page_dialog_left_del);
                    DialogUtils.show2BtnDialog(fVar);
                    return;
                }
            case 11:
                Toast.makeText(this.m, (int) R.string.unsupported, 0).show();
                return;
            case 12:
                Toast.makeText(this.m, (int) R.string.tips_slicent_uninstall, 0).show();
                return;
            default:
                return;
        }
    }

    private AppConst.TwoBtnDialogInfo b(DownloadInfo downloadInfo) {
        g gVar = new g(this, downloadInfo);
        gVar.hasTitle = true;
        gVar.titleRes = this.m.getResources().getString(R.string.down_page_dialog_title);
        gVar.contentRes = this.m.getResources().getString(R.string.down_page_dialog_content);
        gVar.lBtnTxtRes = this.m.getResources().getString(R.string.down_page_dialog_left_del);
        gVar.rBtnTxtRes = this.m.getResources().getString(R.string.down_page_dialog_right_down);
        return gVar;
    }

    public boolean isChildSelectable(int i2, int i3) {
        return true;
    }

    public void f() {
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING, this);
    }

    public void g() {
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING, this);
    }

    public void h() {
        d();
    }

    public void i() {
        e();
        this.q.unregister(this.w);
    }

    public void handleUIEvent(Message message) {
        this.p.sendMessage(new ViewInvalidateMessage(message.what, message.obj, this.x));
    }

    /* access modifiers changed from: private */
    public void a(ViewInvalidateMessage viewInvalidateMessage) {
        DownloadInfo c2;
        DownloadInfoWrapper downloadInfoWrapper;
        boolean z;
        DownloadInfo downloadInfo;
        String str = Constants.STR_EMPTY;
        if (viewInvalidateMessage.params instanceof String) {
            str = (String) viewInvalidateMessage.params;
            if (!TextUtils.isEmpty(str)) {
                DownloadInfo d2 = DownloadProxy.a().d(str);
                if (d2 != null && d2.isUiTypeWiseDownload()) {
                    return;
                }
            } else {
                return;
            }
        }
        String str2 = str;
        if (!(viewInvalidateMessage.params instanceof DownloadInfo) || (downloadInfo = (DownloadInfo) viewInvalidateMessage.params) == null || !downloadInfo.isUiTypeWiseDownload()) {
            try {
                switch (viewInvalidateMessage.what) {
                    case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING:
                    case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING_START:
                    case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC:
                    case 1007:
                    case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING:
                        DownloadInfo c3 = DownloadProxy.a().c(str2);
                        if (c3 != null) {
                            if (!b(new DownloadInfoWrapper(c3))) {
                                c(c3);
                                break;
                            } else {
                                notifyDataSetChanged();
                                break;
                            }
                        }
                        break;
                    case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE:
                        DownloadInfo c4 = DownloadProxy.a().c(str2);
                        if (c4 != null) {
                            c(c4);
                            break;
                        }
                        break;
                    case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE:
                    case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_DELETE:
                    case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_DELETE:
                        if (viewInvalidateMessage.params instanceof DownloadInfo) {
                            downloadInfoWrapper = new DownloadInfoWrapper((DownloadInfo) viewInvalidateMessage.params);
                        } else if (viewInvalidateMessage.params instanceof q) {
                            downloadInfoWrapper = new DownloadInfoWrapper((q) viewInvalidateMessage.params);
                        } else if (viewInvalidateMessage.params instanceof d) {
                            downloadInfoWrapper = new DownloadInfoWrapper((d) viewInvalidateMessage.params);
                        } else {
                            downloadInfoWrapper = null;
                        }
                        if (downloadInfoWrapper != null) {
                            synchronized (this.e) {
                                boolean a2 = a(this.e, downloadInfoWrapper);
                                boolean a3 = a(this.d, downloadInfoWrapper);
                                if (a2 || a3) {
                                    z = true;
                                } else {
                                    z = false;
                                }
                            }
                            synchronized (this.g) {
                                boolean a4 = a(this.g, downloadInfoWrapper);
                                boolean a5 = a(this.f, downloadInfoWrapper);
                                if (a4 || a5) {
                                    z = true;
                                }
                            }
                            if (z) {
                                l();
                                notifyDataSetChanged();
                                break;
                            }
                        }
                        break;
                    case EventDispatcherEnum.UI_EVENT_APP_INSTALL:
                        notifyDataSetChanged();
                        break;
                    case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD:
                        DownloadInfo downloadInfo2 = (DownloadInfo) viewInvalidateMessage.params;
                        if (downloadInfo2 != null) {
                            a(this.e, downloadInfo2.appId);
                            a(this.d, downloadInfo2.appId);
                            a(this.g, downloadInfo2.appId);
                            a(this.f, downloadInfo2.appId);
                            a(this.e, downloadInfo2, 0);
                            a(this.d, downloadInfo2, 0);
                            notifyDataSetChanged();
                            break;
                        }
                        break;
                    case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START:
                    case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC:
                    case 1027:
                        String str3 = ((InstallUninstallTaskBean) viewInvalidateMessage.params).downloadTicket;
                        if (!TextUtils.isEmpty(str3) && (c2 = DownloadProxy.a().c(str3)) != null) {
                            c(c2);
                            break;
                        }
                    case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_START:
                    case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOADING:
                    case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_PAUSE:
                    case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_QUEUING:
                    case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_FAIL:
                    case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_SUCC:
                        q qVar = (q) viewInvalidateMessage.params;
                        if (qVar != null) {
                            a(b(qVar.m), qVar);
                            break;
                        }
                        break;
                    case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_ADD:
                        q qVar2 = (q) viewInvalidateMessage.params;
                        if (qVar2 != null) {
                            DownloadInfoWrapper downloadInfoWrapper2 = new DownloadInfoWrapper(qVar2);
                            a(this.e, downloadInfoWrapper2);
                            a(this.d, downloadInfoWrapper2);
                            a(this.g, downloadInfoWrapper2);
                            a(this.f, downloadInfoWrapper2);
                            a(this.e, downloadInfoWrapper2, 0);
                            a(this.d, downloadInfoWrapper2, 0);
                            notifyDataSetChanged();
                            break;
                        }
                        break;
                    case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_START:
                    case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_FILENAME:
                    case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOADING:
                    case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_PAUSE:
                    case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_QUEUING:
                    case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_FAIL:
                    case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_SUCC:
                        d dVar = (d) viewInvalidateMessage.params;
                        if (dVar != null) {
                            a(c(dVar.m), dVar);
                            break;
                        }
                        break;
                    case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_ADD:
                        d dVar2 = (d) viewInvalidateMessage.params;
                        if (dVar2 != null) {
                            DownloadInfoWrapper downloadInfoWrapper3 = new DownloadInfoWrapper(dVar2);
                            a(this.e, downloadInfoWrapper3);
                            a(this.d, downloadInfoWrapper3);
                            a(this.g, downloadInfoWrapper3);
                            a(this.f, downloadInfoWrapper3);
                            a(this.e, downloadInfoWrapper3, 0);
                            a(this.d, downloadInfoWrapper3, 0);
                            notifyDataSetChanged();
                            break;
                        }
                        break;
                    default:
                        l();
                        notifyDataSetChanged();
                        break;
                }
                for (int i2 = 0; i2 < getGroupCount(); i2++) {
                    this.t.expandGroup(i2);
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState
     arg types: [com.tencent.pangu.download.DownloadInfo, int, int]
     candidates:
      com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, com.tencent.assistant.plugin.PluginDownloadInfo, com.tencent.assistant.plugin.PluginInfo):com.tencent.assistant.AppConst$AppState
      com.tencent.assistant.module.k.a(java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem>, com.tencent.assistant.module.m, int):java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>
      com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState */
    private void c(DownloadInfo downloadInfo) {
        View a2;
        aa aaVar;
        ak akVar;
        aj ajVar;
        if (downloadInfo != null && (a2 = a(downloadInfo.downloadTicket)) != null && (aaVar = (aa) a2.getTag()) != null && (akVar = aaVar.b) != null && (ajVar = akVar.f3413a) != null && bm.a(downloadInfo.downloadTicket, ajVar.o)) {
            a(ajVar, downloadInfo, k.a(downloadInfo, true, true));
        }
    }

    private boolean b(DownloadInfoWrapper downloadInfoWrapper) {
        boolean z;
        if (downloadInfoWrapper == null) {
            return false;
        }
        synchronized (this.g) {
            z = a(this.g, downloadInfoWrapper) || a(this.f, downloadInfoWrapper);
        }
        if (!this.e.contains(downloadInfoWrapper)) {
            a(this.e, downloadInfoWrapper, 0);
            z = true;
        }
        if (!this.d.contains(downloadInfoWrapper)) {
            a(this.d, downloadInfoWrapper, 0);
            z = true;
        }
        if (!z) {
            return z;
        }
        notifyDataSetChanged();
        return z;
    }

    private boolean a(List<DownloadInfoWrapper> list, DownloadInfo downloadInfo, int i2) {
        if (list == null || list.size() < 0 || downloadInfo == null || i2 < 0) {
            return false;
        }
        list.add(i2, new DownloadInfoWrapper(downloadInfo));
        return false;
    }

    private boolean a(List<DownloadInfoWrapper> list, DownloadInfoWrapper downloadInfoWrapper, int i2) {
        if (list == null || list.size() < 0 || downloadInfoWrapper == null || i2 < 0) {
            return false;
        }
        list.add(i2, downloadInfoWrapper);
        return false;
    }

    private boolean a(List<DownloadInfoWrapper> list, DownloadInfoWrapper downloadInfoWrapper) {
        if (list == null || list.size() <= 0 || downloadInfoWrapper == null) {
            return false;
        }
        int i2 = 0;
        while (true) {
            if (i2 >= list.size()) {
                i2 = -1;
                break;
            } else if (list.get(i2).equals(downloadInfoWrapper)) {
                break;
            } else {
                i2++;
            }
        }
        if (i2 < 0) {
            return false;
        }
        list.remove(i2);
        return true;
    }

    private boolean a(List<DownloadInfoWrapper> list, long j2) {
        if (list == null || list.size() <= 0) {
            return false;
        }
        int i2 = 0;
        while (true) {
            if (i2 >= list.size()) {
                i2 = -1;
                break;
            }
            DownloadInfoWrapper downloadInfoWrapper = list.get(i2);
            if (downloadInfoWrapper != null && downloadInfoWrapper.f3748a == DownloadInfoWrapper.InfoType.App && downloadInfoWrapper.b != null && downloadInfoWrapper.b.downloadTicket != null && downloadInfoWrapper.b.appId == j2) {
                break;
            }
            i2++;
        }
        if (i2 < 0) {
            return false;
        }
        list.remove(i2);
        return true;
    }

    private ad n() {
        if (this.n == null) {
            try {
                View inflate = View.inflate(this.m, R.layout.appinfo_download_creating_item, null);
                this.n = new ad(this, null);
                this.n.f3411a = inflate;
                this.n.e = inflate.findViewById(R.id.loading_container);
                this.n.b = (ImageView) inflate.findViewById(R.id.app_icon);
                this.n.c = (TextView) inflate.findViewById(R.id.task_status);
                this.n.d = (Button) inflate.findViewById(R.id.retry_btn);
                this.n.d.setOnClickListener(this.o);
                this.n.f = (ImageView) inflate.findViewById(R.id.last_line);
            } catch (Throwable th) {
                t.a().b();
                th.printStackTrace();
                return null;
            }
        }
        return this.n;
    }

    private aj o() {
        aj ajVar = new aj();
        try {
            View inflate = View.inflate(this.m, R.layout.appinfo_download_item, null);
            ajVar.f = inflate;
            ajVar.g = (RelativeLayout) inflate.findViewById(R.id.layout_item);
            ajVar.i = (DownloadButton) inflate.findViewById(R.id.down_btn);
            ajVar.i.a(true);
            ajVar.i.b(true);
            ajVar.h = (TXImageView) inflate.findViewById(R.id.app_icon);
            ajVar.j = (TextView) inflate.findViewById(R.id.app_name);
            ajVar.l = (TextView) inflate.findViewById(R.id.down_percent);
            ajVar.m = (TextView) inflate.findViewById(R.id.down_speed);
            ajVar.n = (DownloadNumView) inflate.findViewById(R.id.down_size);
            ajVar.k = (FPSProgressBar) inflate.findViewById(R.id.down_progress);
            ajVar.c = (RelativeLayout) inflate.findViewById(R.id.view_all_layout);
            ajVar.d = (RelativeLayout) inflate.findViewById(R.id.view_all_btn_layout);
            ajVar.e = (ImageView) inflate.findViewById(R.id.last_line);
            ajVar.p = (ImageView) inflate.findViewById(R.id.delete_img);
            ajVar.q = (MovingProgressBar) inflate.findViewById(R.id.download_listitem_install_cursor);
            ajVar.q.a(this.m.getResources().getDimensionPixelSize(R.dimen.app_detail_pic_gap));
            ajVar.q.b(this.m.getResources().getDimensionPixelSize(R.dimen.install_progress_bar_width));
        } catch (Throwable th) {
            t.a().b();
        }
        return ajVar;
    }

    private ai p() {
        ai aiVar = new ai();
        View inflate = View.inflate(this.m, R.layout.video_download_item, null);
        aiVar.f = inflate;
        aiVar.g = (RelativeLayout) inflate.findViewById(R.id.layout_item);
        aiVar.i = (VideoDownloadButton) inflate.findViewById(R.id.down_btn);
        aiVar.h = (TXImageView) inflate.findViewById(R.id.app_icon);
        aiVar.j = (TextView) inflate.findViewById(R.id.app_name);
        aiVar.l = (TextView) inflate.findViewById(R.id.down_percent);
        aiVar.m = (TextView) inflate.findViewById(R.id.down_speed);
        aiVar.n = (DownloadNumView) inflate.findViewById(R.id.down_size);
        aiVar.k = (FPSProgressBar) inflate.findViewById(R.id.down_progress);
        aiVar.c = (RelativeLayout) inflate.findViewById(R.id.view_all_layout);
        aiVar.d = (RelativeLayout) inflate.findViewById(R.id.view_all_btn_layout);
        aiVar.e = (ImageView) inflate.findViewById(R.id.last_line);
        aiVar.p = (ImageView) inflate.findViewById(R.id.delete_img);
        return aiVar;
    }

    private af q() {
        af afVar = new af();
        View inflate = View.inflate(this.m, R.layout.file_download_item, null);
        afVar.f = inflate;
        afVar.g = (RelativeLayout) inflate.findViewById(R.id.layout_item);
        afVar.i = (FileDownloadButton) inflate.findViewById(R.id.down_btn);
        afVar.h = (TXImageView) inflate.findViewById(R.id.app_icon);
        afVar.j = (TextView) inflate.findViewById(R.id.app_name);
        afVar.l = (TextView) inflate.findViewById(R.id.down_percent);
        afVar.m = (TextView) inflate.findViewById(R.id.down_speed);
        afVar.n = (DownloadNumView) inflate.findViewById(R.id.down_size);
        afVar.k = (FPSProgressBar) inflate.findViewById(R.id.down_progress);
        afVar.c = (RelativeLayout) inflate.findViewById(R.id.view_all_layout);
        afVar.d = (RelativeLayout) inflate.findViewById(R.id.view_all_btn_layout);
        afVar.e = (ImageView) inflate.findViewById(R.id.last_line);
        afVar.p = (ImageView) inflate.findViewById(R.id.delete_img);
        return afVar;
    }

    private ac r() {
        ac acVar = new ac();
        View inflate = View.inflate(this.m, R.layout.book_download_item, null);
        acVar.f = inflate;
        acVar.g = (RelativeLayout) inflate.findViewById(R.id.layout_item);
        acVar.i = (BookReadButton) inflate.findViewById(R.id.down_btn);
        acVar.h = (TXImageView) inflate.findViewById(R.id.book_icon);
        acVar.j = (TextView) inflate.findViewById(R.id.book_name);
        acVar.k = (TextView) inflate.findViewById(R.id.book_desc_text);
        acVar.c = (RelativeLayout) inflate.findViewById(R.id.view_all_layout);
        acVar.d = (RelativeLayout) inflate.findViewById(R.id.view_all_btn_layout);
        acVar.e = (ImageView) inflate.findViewById(R.id.last_line);
        return acVar;
    }

    public CreatingTaskStatusEnum j() {
        return this.k;
    }

    public void a(CreatingTaskStatusEnum creatingTaskStatusEnum) {
        this.k = creatingTaskStatusEnum;
    }

    public void b(CreatingTaskStatusEnum creatingTaskStatusEnum) {
        this.k = creatingTaskStatusEnum;
        a(this.n, this.k);
    }

    private void a(aj ajVar) {
        ajVar.m.setText(this.m.getResources().getString(R.string.install_show_message));
        ajVar.m.setVisibility(0);
        ajVar.l.setVisibility(8);
        ajVar.n.setVisibility(8);
        ajVar.k.setVisibility(8);
        ajVar.q.setVisibility(0);
        ajVar.q.a();
    }

    private void b(aj ajVar) {
        ajVar.q.b();
        ajVar.q.setVisibility(8);
    }

    /* access modifiers changed from: package-private */
    public void a(ArrayList<DownloadInfo> arrayList) {
        i iVar = new i(this, arrayList);
        iVar.hasTitle = true;
        iVar.titleRes = this.m.getString(R.string.dialog_title_check_apk_installed);
        iVar.contentRes = this.m.getString(R.string.downloaded_app_install_content, Integer.valueOf(arrayList.size()));
        iVar.lBtnTxtRes = this.m.getString(R.string.down_page_dialog_left_del);
        iVar.rBtnTxtRes = this.m.getString(R.string.downloaded_app_install_right_btn);
        DialogUtils.show2BtnDialog(iVar);
        l.a(new STInfoV2(STConst.ST_PAGE_DOWNLOAD, SecondNavigationTitleView.TMA_ST_NAVBAR_HOME_TAG, 0, STConst.ST_DEFAULT_SLOT, 100));
    }
}
