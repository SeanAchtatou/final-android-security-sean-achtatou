package com.tencent.android.qqdownloader.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.utils.XLog;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.modelmsg.SendMessageToWX;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.nucleus.socialcontact.login.s;
import com.tencent.pangu.c.o;

/* compiled from: ProGuard */
public class WXEntryActivity extends Activity implements IWXAPIEventHandler {
    private static String b = "WXEntryActivity";

    /* renamed from: a  reason: collision with root package name */
    private IWXAPI f378a;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        XLog.d(b, "WXEntryActivity--onCreate");
        this.f378a = WXAPIFactory.createWXAPI(this, "wx3909f6add1206543", false);
        this.f378a.registerApp("wx3909f6add1206543");
        try {
            this.f378a.handleIntent(getIntent(), this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        try {
            this.f378a.handleIntent(intent, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onReq(BaseReq baseReq) {
    }

    public void onResp(BaseResp baseResp) {
        int i;
        int i2 = 4;
        boolean z = true;
        XLog.d(b, "WXEntryActivity--onResp");
        if (s.h().i()) {
            s.h().a(baseResp);
            s.h().j();
        }
        if (baseResp instanceof SendMessageToWX.Resp) {
            if (o.a() != 1) {
                z = false;
            }
            if (baseResp.errCode == 0) {
                Message obtainMessage = AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_SHARE_SUCCESS);
                if (z) {
                    i = 4;
                } else {
                    i = 3;
                }
                obtainMessage.obj = Integer.valueOf(i);
                AstApp.i().j().sendMessage(obtainMessage);
            } else {
                Message obtainMessage2 = AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_SHARE_FAIL);
                if (!z) {
                    i2 = 3;
                }
                obtainMessage2.obj = Integer.valueOf(i2);
                AstApp.i().j().sendMessage(obtainMessage2);
            }
        }
        finish();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        XLog.d(b, "WXEntryActivity--onDestroy--configuration " + getChangingConfigurations());
        if (this.f378a != null) {
            this.f378a.unregisterApp();
        }
    }
}
