package com.tencent.open.web.security;

import android.webkit.WebView;
import com.tencent.open.a.n;
import com.tencent.open.d;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class c extends d {
    private static final String d = (n.d + ".SL");
    private String e;

    public c(WebView webView, long j, String str, String str2) {
        super(webView, j, str);
        this.e = str2;
    }

    public void a(Object obj) {
        n.b(d, "-->onComplete, result: " + obj);
    }

    public void a() {
        n.b(d, "-->onNoMatchMethod...");
    }

    public void a(String str) {
        n.b(d, "-->onCustomCallback, js: " + str);
        JSONObject jSONObject = new JSONObject();
        int i = 0;
        if (!com.tencent.open.c.c.f3263a) {
            i = -4;
        }
        try {
            jSONObject.put("result", i);
            jSONObject.put("sn", this.b);
            jSONObject.put("data", str);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        b(jSONObject.toString());
    }

    private void b(String str) {
        WebView webView = (WebView) this.f3264a.get();
        if (webView != null) {
            StringBuffer stringBuffer = new StringBuffer("javascript:");
            stringBuffer.append("if(!!").append(this.e).append("){");
            stringBuffer.append(this.e);
            stringBuffer.append("(");
            stringBuffer.append(str);
            stringBuffer.append(")}");
            String stringBuffer2 = stringBuffer.toString();
            n.b(n.d, "-->callback, callback: " + stringBuffer2);
            webView.loadUrl(stringBuffer2);
        }
    }
}
