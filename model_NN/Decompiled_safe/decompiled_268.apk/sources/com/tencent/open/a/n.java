package com.tencent.open.a;

import android.os.Environment;
import android.text.TextUtils;
import com.tencent.open.utils.Global;
import java.io.File;

/* compiled from: ProGuard */
public class n {

    /* renamed from: a  reason: collision with root package name */
    public static n f3245a = null;
    protected static final b c = new b(c(), f.s, f.m, f.n, f.h, (long) f.o, 10, f.k, f.t);
    public static final String d = f.f3242a;
    protected a b = new a(c);

    public static n a() {
        if (f3245a == null) {
            synchronized (n.class) {
                if (f3245a == null) {
                    f3245a = new n();
                }
            }
        }
        return f3245a;
    }

    private n() {
    }

    /* access modifiers changed from: protected */
    public void a(int i, String str, String str2, Throwable th) {
        m.f3244a.b(i, Thread.currentThread(), System.currentTimeMillis(), str, str2, th);
        if (h.a(f.c, i) && this.b != null) {
            this.b.b(i, Thread.currentThread(), System.currentTimeMillis(), str, str2, th);
        }
    }

    public static final void a(String str, String str2) {
        a().a(1, str, str2, null);
    }

    public static final void b(String str, String str2) {
        a().a(2, str, str2, null);
    }

    public static final void a(String str, String str2, Throwable th) {
        a().a(2, str, str2, th);
    }

    public static final void c(String str, String str2) {
        a().a(4, str, str2, null);
    }

    public static final void d(String str, String str2) {
        a().a(8, str, str2, null);
    }

    public static final void e(String str, String str2) {
        a().a(16, str, str2, null);
    }

    public static final void b(String str, String str2, Throwable th) {
        a().a(16, str, str2, th);
    }

    public static void b() {
        synchronized (n.class) {
            a().d();
            if (f3245a != null) {
                f3245a = null;
            }
        }
    }

    protected static File c() {
        boolean z;
        String packageName = Global.getPackageName();
        if (TextUtils.isEmpty(packageName)) {
            packageName = "default";
        }
        String str = f.i + File.separator + packageName;
        k b2 = j.b();
        if (b2 == null || b2.c() <= f.l) {
            z = false;
        } else {
            z = true;
        }
        if (z) {
            return new File(Environment.getExternalStorageDirectory(), str);
        }
        return new File(Global.getFilesDir(), str);
    }

    /* access modifiers changed from: protected */
    public void d() {
        if (this.b != null) {
            this.b.a();
            this.b.b();
            this.b = null;
        }
    }
}
