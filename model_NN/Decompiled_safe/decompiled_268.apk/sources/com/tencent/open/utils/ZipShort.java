package com.tencent.open.utils;

import android.support.v4.view.MotionEventCompat;

/* compiled from: ProGuard */
public final class ZipShort implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private int f3286a;

    public ZipShort(byte[] bArr) {
        this(bArr, 0);
    }

    public ZipShort(byte[] bArr, int i) {
        this.f3286a = (bArr[i + 1] << 8) & MotionEventCompat.ACTION_POINTER_INDEX_MASK;
        this.f3286a += bArr[i] & 255;
    }

    public ZipShort(int i) {
        this.f3286a = i;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof ZipShort) || this.f3286a != ((ZipShort) obj).getValue()) {
            return false;
        }
        return true;
    }

    public byte[] getBytes() {
        return new byte[]{(byte) (this.f3286a & 255), (byte) ((this.f3286a & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8)};
    }

    public int getValue() {
        return this.f3286a;
    }

    public int hashCode() {
        return this.f3286a;
    }
}
