package com.tencent.connector.ipc;

import com.qq.AppService.AuthorReceiver;
import com.qq.AppService.ParcelObject;
import com.qq.AppService.aa;
import com.qq.b.a;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.model.OnlinePCListItemModel;

/* compiled from: ProGuard */
class c extends aa {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f2419a;

    c(a aVar) {
        this.f2419a = aVar;
    }

    public void a(String str) {
    }

    public void a(String str, String str2, String str3) {
    }

    public void a() {
    }

    public void a(int i) {
        ConnectionType connectionType = ConnectionType.NONE;
        if (i == 1) {
            connectionType = ConnectionType.USB;
        } else if (i == 2) {
            connectionType = ConnectionType.WIFI;
        } else if (i == 3) {
            connectionType = ConnectionType.WIFI;
        }
        if (a.f2417a) {
            a.f2417a = false;
            this.f2419a.f.sendMessage(this.f2419a.f.obtainMessage(EventDispatcherEnum.UI_EVENT_PHOTOBACKUP_FAILED));
        }
        a.b = null;
        this.f2419a.f.sendMessage(this.f2419a.f.obtainMessage(5002, connectionType));
    }

    public void b(int i) {
    }

    public void c(int i) {
    }

    public void a(String str, int i) {
    }

    public void a(int i, int i2, String str, ParcelObject parcelObject) {
    }

    public void a(int i, int i2, ParcelObject parcelObject) {
    }

    public void d(int i) {
    }

    public void e(int i) {
    }

    public void b() {
        if (a.f2417a) {
            a.f2417a = false;
            this.f2419a.f.sendMessage(this.f2419a.f.obtainMessage(EventDispatcherEnum.UI_EVENT_PHOTOBACKUP_FAILED));
        }
        this.f2419a.f.sendMessage(this.f2419a.f.obtainMessage(5001));
        a.b = null;
    }

    public void a(boolean z) {
    }

    public void c() {
    }

    public void d() {
    }

    public void a(String str, String str2, String str3, boolean z) {
        OnlinePCListItemModel onlinePCListItemModel = new OnlinePCListItemModel();
        onlinePCListItemModel.f936a = str;
        onlinePCListItemModel.b = str2;
        onlinePCListItemModel.d = str3;
        this.f2419a.f.sendMessage(this.f2419a.f.obtainMessage(EventDispatcherEnum.CONNECTION_EVENT_PC_PING, onlinePCListItemModel));
    }

    public void a(long j, int i) {
        if (i == 0 && !a.f2417a) {
            a.f2417a = true;
            this.f2419a.f.sendMessage(this.f2419a.f.obtainMessage(EventDispatcherEnum.UI_EVENT_PHOTOBACKUP_START));
        } else if (i == -1 && a.f2417a) {
            a.b = null;
            a.f2417a = false;
            this.f2419a.f.sendMessage(this.f2419a.f.obtainMessage(EventDispatcherEnum.UI_EVENT_PHOTOBACKUP_FAILED));
        } else if (i == 1 && a.f2417a) {
            a.b = null;
            a.f2417a = false;
            this.f2419a.f.sendMessage(this.f2419a.f.obtainMessage(EventDispatcherEnum.UI_EVENT_PHOTOBACKUP_FINISH));
        }
    }

    public void a(String str, String str2, String str3, String str4) {
        if (str != null) {
            a.c = true;
        }
        AuthorReceiver.b(str);
        if (str2 != null) {
            a.b = true;
        }
        AuthorReceiver.a(str2);
    }
}
