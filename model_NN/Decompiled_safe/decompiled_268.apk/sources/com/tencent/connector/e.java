package com.tencent.connector;

import android.os.Handler;
import android.os.Message;
import com.qq.AppService.AppService;
import com.qq.l.p;
import com.tencent.assistant.model.OnlinePCListItemModel;
import com.tencent.connector.ConnectionActivity;
import com.tencent.wcs.c.b;

/* compiled from: ProGuard */
class e extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ConnectionActivity f2411a;

    e(ConnectionActivity connectionActivity) {
        this.f2411a = connectionActivity;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 10:
                if (this.f2411a.f2376a == ConnectionActivity.Status.WAIT_CONNECTION) {
                    AppService.BusinessConnectionType businessConnectionType = (AppService.BusinessConnectionType) message.obj;
                    if (businessConnectionType == AppService.BusinessConnectionType.QQ) {
                        this.f2411a.E();
                        return;
                    } else if (businessConnectionType == AppService.BusinessConnectionType.QRCODE) {
                        this.f2411a.b(5);
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case 11:
                if (this.f2411a.f2376a == ConnectionActivity.Status.WAIT_CONNECTION) {
                    AppService.BusinessConnectionType businessConnectionType2 = (AppService.BusinessConnectionType) message.obj;
                    if (businessConnectionType2 == AppService.BusinessConnectionType.QQ) {
                        this.f2411a.E();
                        return;
                    } else if (businessConnectionType2 == AppService.BusinessConnectionType.QRCODE) {
                        this.f2411a.b(5);
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case 12:
                p.m().b(p.m().p().b(), 0, -1);
                this.f2411a.a((AppService.BusinessConnectionType) message.obj);
                return;
            case 20:
                if (this.f2411a.f2376a == ConnectionActivity.Status.WAIT_CONNECTION) {
                    AppService.BusinessConnectionType businessConnectionType3 = (AppService.BusinessConnectionType) message.obj;
                    if (businessConnectionType3 == AppService.BusinessConnectionType.QQ) {
                        this.f2411a.E();
                        return;
                    } else if (businessConnectionType3 == AppService.BusinessConnectionType.QRCODE) {
                        this.f2411a.b(5);
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case 30:
                this.f2411a.E();
                return;
            case 31:
                i unused = this.f2411a.q = (i) null;
                OnlinePCListItemModel unused2 = this.f2411a.p = (OnlinePCListItemModel) message.obj;
                this.f2411a.b(this.f2411a.p.b, AppService.K(), AppService.L());
                if (AppService.w()) {
                    AppService.R();
                    if (AppService.x()) {
                        this.f2411a.a(this.f2411a.p.f936a, AppService.M());
                        return;
                    } else if (AppService.y()) {
                        b.a("invite qq, but long connection disable, reconnect,waiting...");
                        return;
                    } else {
                        b.a("invite qq, but long connection disable, not reconnect...");
                        this.f2411a.y();
                        return;
                    }
                } else {
                    AppService.z();
                    return;
                }
            default:
                return;
        }
    }
}
