package com.tencent.connector;

import android.graphics.Rect;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import com.tencent.connector.component.ViewfinderView;

/* compiled from: ProGuard */
class a implements ViewfinderView.LaserAnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CaptureActivity f2379a;

    a(CaptureActivity captureActivity) {
        this.f2379a = captureActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.connector.CaptureActivity.a(com.tencent.connector.CaptureActivity, boolean):boolean
     arg types: [com.tencent.connector.CaptureActivity, int]
     candidates:
      com.tencent.connector.CaptureActivity.a(com.tencent.connector.CaptureActivity, android.media.MediaPlayer):android.media.MediaPlayer
      com.tencent.connector.CaptureActivity.a(com.tencent.connector.CaptureActivity, android.view.animation.Animation):android.view.animation.Animation
      com.tencent.connector.CaptureActivity.a(com.google.zxing.h, android.graphics.Bitmap):void
      com.tencent.connector.CaptureActivity.a(com.tencent.connector.CaptureActivity, boolean):boolean */
    public void onStartAnimation(Rect rect, int i) {
        if (!this.f2379a.n) {
            boolean unused = this.f2379a.n = true;
            if (rect != null) {
                this.f2379a.l.setLayoutParams(new LinearLayout.LayoutParams(rect.width() - (i * 2), -2));
                Animation unused2 = this.f2379a.m = new TranslateAnimation(0.0f, 0.0f, (float) (rect.bottom - this.f2379a.l.getHeight()), (float) rect.top);
                this.f2379a.m.setDuration(3000);
                this.f2379a.m.setRepeatMode(1);
                this.f2379a.m.setInterpolator(new AccelerateDecelerateInterpolator());
                this.f2379a.m.setRepeatCount(-1);
                this.f2379a.l.setAnimation(this.f2379a.m);
                this.f2379a.m.startNow();
            }
        }
    }
}
