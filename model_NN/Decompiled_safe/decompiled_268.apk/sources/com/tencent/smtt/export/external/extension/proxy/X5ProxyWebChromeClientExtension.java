package com.tencent.smtt.export.external.extension.proxy;

import com.tencent.smtt.export.external.WebViewWizardBase;
import com.tencent.smtt.export.external.extension.interfaces.IX5WebChromeClientExtension;

public abstract class X5ProxyWebChromeClientExtension extends ProxyWebChromeClientExtension {
    public X5ProxyWebChromeClientExtension(WebViewWizardBase wizard) {
        this.mWebChromeClient = (IX5WebChromeClientExtension) wizard.newInstance(wizard.isDynamicMode(), "com.tencent.smtt.webkit.WebChromeClientExtension");
    }
}
