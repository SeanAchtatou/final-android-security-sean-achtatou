package com.tencent.smtt.sdk;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.connect.common.Constants;
import com.tencent.smtt.export.external.WebViewWizardBase;
import com.tencent.smtt.export.external.libwebp;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Properties;

class SDKEngine {
    public static final String COUNT_PROPERTIES = "count.prop";
    private static final String GET_CALLED_COUNT_KEY = "getCalledCount";
    private static final String LOGTAG = "SDKEngine";
    private static final int MSG_CHECK_X5CORE_INSTALLED = 101;
    private static final int MSG_CLEAR_DEXOPT_THREAD = 100;
    private static final String MTT_SIG = "3082023f308201a8a00302010202044c46914a300d06092a864886f70d01010505003064310b30090603550406130238363110300e060355040813074265696a696e673110300e060355040713074265696a696e673110300e060355040a130754656e63656e74310c300a060355040b13035753443111300f0603550403130873616d75656c6d6f301e170d3130303732313036313835305a170d3430303731333036313835305a3064310b30090603550406130238363110300e060355040813074265696a696e673110300e060355040713074265696a696e673110300e060355040a130754656e63656e74310c300a060355040b13035753443111300f0603550403130873616d75656c6d6f30819f300d06092a864886f70d010101050003818d0030818902818100c209077044bd0d63ea00ede5b839914cabcc912a87f0f8b390877e0f7a2583f0d5933443c40431c35a4433bc4c965800141961adc44c9625b1d321385221fd097e5bdc2f44a1840d643ab59dc070cf6c4b4b4d98bed5cbb8046e0a7078ae134da107cdf2bfc9b440fe5cb2f7549b44b73202cc6f7c2c55b8cfb0d333a021f01f0203010001300d06092a864886f70d010105050003818100b007db9922774ef4ccfee81ba514a8d57c410257e7a2eba64bfa17c9e690da08106d32f637ac41fbc9f205176c71bde238c872c3ee2f8313502bee44c80288ea4ef377a6f2cdfe4d3653c145c4acfedbfbadea23b559d41980cc3cdd35d79a68240693739aabf5c5ed26148756cf88264226de394c8a24ac35b712b120d4d23a";
    private static final String STR_TIMESTAMP = "timestamp";
    public static final String X5QQBROWSER_PKG_NAME = ((System.getProperty("os.arch").toLowerCase().contains("x86") || System.getProperty("os.arch").toLowerCase().contains("i686")) ? "com.tencent.mtt.x86" : "com.tencent.mtt");
    public static final String X5_ASSETS_CONF_PATH = "webkit/qbx5.conf";
    public static final String X5_CONF = "qbx5.conf";
    public static final String X5_CONF_CORE_VER = "browser_core_version";
    public static final String X5_CONF_TIMESTAMP = "browser_core_build_number";
    public static final String X5_CORE_FOLDER_NAME = "x5core";
    public static final String X5_FILE_DEX = "webview_dex.jar";
    public static final String X5_FILE_INTERFACES_DEX = "webview_interfaces_dex.jar";
    public static final String X5_PROPERTIES = "x5.prop";
    public static final String X5_SHARE_FOLDER_NAME = "x5_share";
    public static final String X5_SO = "libmttwebcore.so";
    private static int checkTimeDelay = EventDispatcherEnum.CONNECTION_EVENT_START;
    private static int checkTimesCount = 0;
    /* access modifiers changed from: private */
    public static int checkTimesMax = 5;
    private static boolean getX5CoreNeedRebootCalled = false;
    private static int iInitCount = 0;
    private static boolean isCompatibleChecked = false;
    private static int mCoreVersion = 0;
    private static String mCoreVersionString = null;
    /* access modifiers changed from: private */
    public static boolean mDexDone = false;
    private static SDKEngine mInstance = null;
    private static boolean mQBTimesTampDiff = false;
    private static String mQBTimestampFinish = Constants.STR_EMPTY;
    private static String mQBTimestampStart = Constants.STR_EMPTY;
    private static int mRebootCalledTimes = 1;
    private static int mRebootCalledTimesMax = 5;
    /* access modifiers changed from: private */
    public static boolean mX5CoreInstalled = false;
    private static final int sMinSupportCoreVersion = 25102;
    private File mBrowserX5CorePath;
    private File mBrowserX5SharePath;
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public Handler mDexHandler;
    /* access modifiers changed from: private */
    public HandlerThread mDexHandlerThread;
    private File mSoPath;
    /* access modifiers changed from: private */
    public Handler mUIHandler;
    private boolean mUsedX5;
    private WebViewWizardBase mWizard;
    /* access modifiers changed from: private */
    public File mX5CorePath;

    static /* synthetic */ int access$608() {
        int i = checkTimesCount;
        checkTimesCount = i + 1;
        return i;
    }

    public static SDKEngine getInstance(boolean autoCreate) {
        if (mInstance == null && autoCreate) {
            mInstance = new SDKEngine();
        }
        return mInstance;
    }

    public SDKEngine() {
        this.mWizard = null;
        this.mX5CorePath = null;
        this.mUsedX5 = false;
        this.mBrowserX5CorePath = null;
        this.mBrowserX5SharePath = null;
        this.mSoPath = null;
        this.mDexHandlerThread = null;
        this.mDexHandler = null;
        this.mUIHandler = null;
        this.mContext = null;
        this.mUIHandler = new Handler(Looper.getMainLooper()) {
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 100:
                        Properties prop = new Properties();
                        prop.setProperty(SDKEngine.STR_TIMESTAMP, QbSdk.getX5CoreTimestamp());
                        try {
                            prop.store(new FileOutputStream(new File(SDKEngine.this.mX5CorePath, SDKEngine.X5_PROPERTIES)), (String) null);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e2) {
                            e2.printStackTrace();
                        }
                        if (SDKEngine.this.mDexHandlerThread != null) {
                            SDKEngine.this.mDexHandlerThread.quit();
                            HandlerThread unused = SDKEngine.this.mDexHandlerThread = null;
                        }
                        if (SDKEngine.this.mDexHandler != null) {
                            Handler unused2 = SDKEngine.this.mDexHandler = null;
                        }
                        boolean unused3 = SDKEngine.mDexDone = true;
                        QbSdkLog.d("X5CoreNeedReboot", "SDKEngine--MSG_CLEAR_DEXOPT_THREAD mDexDone=" + SDKEngine.mDexDone);
                        return;
                    case 101:
                        boolean unused4 = SDKEngine.mX5CoreInstalled = QbSdk.checkX5CoreInstalled(SDKEngine.this.mContext);
                        QbSdkLog.d("X5CoreNeedReboot", "SDKEngine--MSG_CHECK_X5CORE_INSTALLED mX5CoreInstalled=" + SDKEngine.mX5CoreInstalled);
                        if (!SDKEngine.mX5CoreInstalled && SDKEngine.access$608() < SDKEngine.checkTimesMax) {
                            SDKEngine.this.checkX5CoreInstalled();
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        };
    }

    private static boolean isCompatible(int coreVersion, String x5CorePath) {
        boolean z = true;
        boolean ret = true;
        try {
            if (!isCompatibleChecked) {
                if (coreVersion <= 25206 && coreVersion >= 25200) {
                    ret = false;
                }
                z = true;
            }
            isCompatibleChecked = z;
            return ret;
        } catch (Throwable th) {
            th.printStackTrace();
            return false;
        } finally {
            isCompatibleChecked = z;
        }
    }

    public synchronized void init(Context context) {
        this.mContext = context.getApplicationContext();
        iInitCount++;
        this.mX5CorePath = context.getDir(X5_CORE_FOLDER_NAME, 0);
        boolean canLoadX5 = QbSdk.canLoadX5(context);
        initX5CoreVersionIfNeed(context);
        if (mCoreVersion < sMinSupportCoreVersion) {
            canLoadX5 = false;
            if (mCoreVersion != 0) {
                QbSdkLog.e("QbSdk", "sys WebView: version too low");
            } else {
                QbSdkLog.i("QbSdk", "sys WebView: no x5");
            }
        }
        if (!isCompatible(mCoreVersion, this.mX5CorePath.toString())) {
            canLoadX5 = false;
            QbSdk.forceSysWebViewInner();
            QbSdkLog.e("QbSdk", "sys WebView: not Compatible");
        }
        if (!canLoadX5) {
            this.mWizard = new WebViewWizardBase();
            this.mUsedX5 = false;
            this.mWizard.setWizardMode(false, false);
            QbSdkLog.e("QbSdk", "canload5 is false.");
        } else if (needDexOptimize(context)) {
            doDexOptimize(context);
            this.mWizard = new WebViewWizardBase();
            this.mUsedX5 = false;
            this.mWizard.setWizardMode(false, false);
            QbSdkLog.e("QbSdk", "sys WebView: need dexopt");
        } else if (!this.mUsedX5) {
            try {
                if (!context.getPackageManager().getPackageInfo(X5QQBROWSER_PKG_NAME, 64).signatures[0].toCharsString().equals(MTT_SIG)) {
                    this.mUsedX5 = false;
                    this.mWizard = new WebViewWizardBase();
                    this.mWizard.setWizardMode(false, false);
                    QbSdk.forceSysWebViewInner();
                    QbSdkLog.e("QbSdk", "sys WebView: signature check failed");
                } else {
                    this.mWizard = new WebViewWizardBase();
                    this.mWizard.setWizardMode(true, true);
                    Context x5pkgContext = getPackageContext(context, X5QQBROWSER_PKG_NAME);
                    if (x5pkgContext == null) {
                        this.mUsedX5 = false;
                        this.mWizard.setWizardMode(false, false);
                        QbSdk.forceSysWebViewInner();
                        QbSdkLog.e("QbSdk", "sys WebView: null x5 context");
                    } else {
                        boolean loadSucc = false;
                        try {
                            this.mBrowserX5SharePath = x5pkgContext.getDir(X5_SHARE_FOLDER_NAME, 0);
                            this.mBrowserX5CorePath = new File(this.mBrowserX5SharePath, X5_CORE_FOLDER_NAME);
                            this.mWizard.setDexLoader(context, new String[]{new File(this.mBrowserX5SharePath, (String) X5_FILE_INTERFACES_DEX).getAbsolutePath(), new File(this.mBrowserX5CorePath, (String) X5_FILE_DEX).getAbsolutePath()}, this.mX5CorePath.getAbsolutePath());
                            this.mSoPath = new File(getCurrentNativeLibraryDir(x5pkgContext));
                            libwebp.loadWepLibraryIfNeed(context, this.mSoPath.getAbsolutePath());
                            this.mWizard.setContextHolderDevelopMode(true);
                            loadSucc = this.mWizard.setContextHolderParams(context, this.mSoPath.getAbsolutePath());
                        } catch (Throwable thr) {
                            thr.printStackTrace();
                            QbSdkLog.e("QbSdk", "sys WebView: exception while loading so.");
                        }
                        if (!loadSucc) {
                            this.mUsedX5 = false;
                            this.mWizard.setContextHolderDevelopMode(false);
                            this.mWizard.setWizardMode(false, false);
                            QbSdk.forceSysWebViewInner();
                            QbSdkLog.e("QbSdk", "sys WebView: failed to load so.");
                        } else if (iInitCount == 1) {
                            this.mWizard.setContextHolderDevelopMode(true);
                            this.mWizard.setSdkVersion(1);
                            this.mWizard.initCookieModule(context);
                            this.mWizard.SmttResource_UpdateContext(x5pkgContext);
                            this.mUsedX5 = true;
                        } else {
                            this.mWizard = new WebViewWizardBase();
                            this.mUsedX5 = false;
                            this.mWizard.setWizardMode(false, false);
                            QbSdk.forceSysWebViewInner();
                            QbSdkLog.e("QbSdk", "sys WebView: not first time.");
                            this.mWizard.SmttResource_UpdateContext(null);
                        }
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                this.mUsedX5 = false;
                this.mWizard = new WebViewWizardBase();
                this.mWizard.setWizardMode(false, false);
                QbSdk.forceSysWebViewInner();
                QbSdkLog.e("QbSdk", "sys WebView: check sig exception");
            }
        }
        if (!this.mUsedX5) {
            mQBTimestampStart = getQBAssetTimestamp(context);
            QbSdkLog.d("X5CoreNeedReboot", "SDKEngine--init()--mQBTimestampStart=" + mQBTimestampStart);
        }
    }

    @TargetApi(9)
    private String getCurrentNativeLibraryDir(Context context) {
        int sdk_version = Build.VERSION.SDK_INT;
        if (sdk_version >= 9) {
            return context.getApplicationInfo().nativeLibraryDir;
        }
        if (sdk_version > 4) {
            return context.getApplicationInfo().dataDir + "/lib";
        }
        return context.getFilesDir().getParentFile().getPath() + "/lib";
    }

    private CharSequence getCurrentRuntimeValue() {
        try {
            Class<?> systemProperties = Class.forName("android.os.SystemProperties");
            try {
                Method get = systemProperties.getMethod("get", String.class, String.class);
                if (get == null) {
                    return "WTF?!";
                }
                try {
                    String value = (String) get.invoke(systemProperties, "persist.sys.dalvik.vm.lib", "Dalvik");
                    if ("libdvm.so".equals(value)) {
                        return "Dalvik";
                    }
                    if ("libart.so".equals(value)) {
                        return "ART";
                    }
                    if ("libartd.so".equals(value)) {
                        return "ART debug build";
                    }
                    return value;
                } catch (IllegalAccessException e) {
                    return "IllegalAccessException";
                } catch (IllegalArgumentException e2) {
                    return "IllegalArgumentException";
                } catch (InvocationTargetException e3) {
                    return "InvocationTargetException";
                }
            } catch (NoSuchMethodException e4) {
                return "SystemProperties.get(String key, String def) method is not found";
            }
        } catch (ClassNotFoundException e5) {
            return "SystemProperties class is not found";
        }
    }

    private void doDexOptimize(final Context context) {
        if (this.mDexHandlerThread == null) {
            this.mDexHandlerThread = new HandlerThread("X5DexOptimize");
            this.mDexHandlerThread.start();
        }
        if (this.mDexHandler == null) {
            this.mDexHandler = new Handler(this.mDexHandlerThread.getLooper());
            int delayMillis = 0;
            if (getCurrentRuntimeValue().toString().contains("ART")) {
                delayMillis = 2000;
            }
            this.mDexHandler.postDelayed(new Runnable() {
                public void run() {
                    WebViewWizardBase wizard = new WebViewWizardBase();
                    wizard.setWizardMode(true, true);
                    Context x5pkgContext = SDKEngine.getPackageContext(context, SDKEngine.X5QQBROWSER_PKG_NAME);
                    if (x5pkgContext != null) {
                        File browserX5SharePath = x5pkgContext.getDir(SDKEngine.X5_SHARE_FOLDER_NAME, 0);
                        File browserX5CorePath = new File(browserX5SharePath, SDKEngine.X5_CORE_FOLDER_NAME);
                        wizard.setDexLoader(context, new String[]{new File(browserX5SharePath, (String) SDKEngine.X5_FILE_INTERFACES_DEX).getAbsolutePath(), new File(browserX5CorePath, (String) SDKEngine.X5_FILE_DEX).getAbsolutePath()}, SDKEngine.this.mX5CorePath.getAbsolutePath());
                        SDKEngine.this.mUIHandler.removeMessages(100);
                        SDKEngine.this.mUIHandler.sendEmptyMessage(100);
                    }
                }
            }, (long) delayMillis);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0059 A[SYNTHETIC, Splitter:B:29:0x0059] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0065 A[SYNTHETIC, Splitter:B:35:0x0065] */
    /* JADX WARNING: Removed duplicated region for block: B:49:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean needDexOptimize(android.content.Context r11) {
        /*
            r10 = this;
            r7 = 1
            java.io.File r8 = r10.mX5CorePath
            java.io.File[] r2 = r8.listFiles()
            if (r2 == 0) goto L_0x000c
            int r8 = r2.length
            if (r8 > r7) goto L_0x000d
        L_0x000c:
            return r7
        L_0x000d:
            java.io.File r6 = new java.io.File
            java.io.File r8 = r10.mX5CorePath
            java.lang.String r9 = "x5.prop"
            r6.<init>(r8, r9)
            boolean r8 = r6.exists()
            if (r8 == 0) goto L_0x000c
            r3 = 0
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0053 }
            r4.<init>(r6)     // Catch:{ Exception -> 0x0053 }
            java.util.Properties r5 = new java.util.Properties     // Catch:{ Exception -> 0x0071, all -> 0x006e }
            r5.<init>()     // Catch:{ Exception -> 0x0071, all -> 0x006e }
            r5.load(r4)     // Catch:{ Exception -> 0x0071, all -> 0x006e }
            java.lang.String r8 = "timestamp"
            java.lang.String r0 = r5.getProperty(r8)     // Catch:{ Exception -> 0x0071, all -> 0x006e }
            if (r0 == 0) goto L_0x003c
            java.lang.String r8 = com.tencent.smtt.sdk.QbSdk.getX5CoreTimestamp()     // Catch:{ Exception -> 0x0071, all -> 0x006e }
            boolean r8 = r0.equalsIgnoreCase(r8)     // Catch:{ Exception -> 0x0071, all -> 0x006e }
            if (r8 != 0) goto L_0x0047
        L_0x003c:
            if (r4 == 0) goto L_0x000c
            r4.close()     // Catch:{ IOException -> 0x0042 }
            goto L_0x000c
        L_0x0042:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x000c
        L_0x0047:
            if (r4 == 0) goto L_0x004c
            r4.close()     // Catch:{ IOException -> 0x004e }
        L_0x004c:
            r7 = 0
            goto L_0x000c
        L_0x004e:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x004c
        L_0x0053:
            r1 = move-exception
        L_0x0054:
            r1.printStackTrace()     // Catch:{ all -> 0x0062 }
            if (r3 == 0) goto L_0x000c
            r3.close()     // Catch:{ IOException -> 0x005d }
            goto L_0x000c
        L_0x005d:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x000c
        L_0x0062:
            r7 = move-exception
        L_0x0063:
            if (r3 == 0) goto L_0x0068
            r3.close()     // Catch:{ IOException -> 0x0069 }
        L_0x0068:
            throw r7
        L_0x0069:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0068
        L_0x006e:
            r7 = move-exception
            r3 = r4
            goto L_0x0063
        L_0x0071:
            r1 = move-exception
            r3 = r4
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.smtt.sdk.SDKEngine.needDexOptimize(android.content.Context):boolean");
    }

    /* access modifiers changed from: private */
    public static Context getPackageContext(Context appContext, String pkgName) {
        try {
            return appContext.createPackageContext(pkgName, 2);
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public WebViewWizardBase wizard() {
        return this.mWizard;
    }

    public boolean isX5Core() {
        return this.mUsedX5;
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0091 A[SYNTHETIC, Splitter:B:50:0x0091] */
    /* JADX WARNING: Removed duplicated region for block: B:64:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void initX5CoreVersionIfNeed(android.content.Context r10) {
        /*
            int r8 = com.tencent.smtt.sdk.SDKEngine.mCoreVersion
            if (r8 == 0) goto L_0x0005
        L_0x0004:
            return
        L_0x0005:
            r3 = 0
            java.lang.String r8 = com.tencent.smtt.sdk.SDKEngine.X5QQBROWSER_PKG_NAME     // Catch:{ Exception -> 0x0077 }
            android.content.Context r7 = getPackageContext(r10, r8)     // Catch:{ Exception -> 0x0077 }
            if (r7 != 0) goto L_0x0019
            if (r3 == 0) goto L_0x0004
            r3.close()     // Catch:{ IOException -> 0x0014 }
            goto L_0x0004
        L_0x0014:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0004
        L_0x0019:
            java.lang.String r8 = "x5_share"
            r9 = 0
            java.io.File r1 = r7.getDir(r8, r9)     // Catch:{ Exception -> 0x0077 }
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x0077 }
            java.lang.String r8 = "x5core"
            r0.<init>(r1, r8)     // Catch:{ Exception -> 0x0077 }
            java.io.File r6 = new java.io.File     // Catch:{ Exception -> 0x0077 }
            java.lang.String r8 = "qbx5.conf"
            r6.<init>(r0, r8)     // Catch:{ Exception -> 0x0077 }
            if (r6 == 0) goto L_0x0073
            boolean r8 = r6.exists()     // Catch:{ Exception -> 0x0077 }
            if (r8 == 0) goto L_0x0073
            java.util.Properties r5 = new java.util.Properties     // Catch:{ Exception -> 0x0077 }
            r5.<init>()     // Catch:{ Exception -> 0x0077 }
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0077 }
            r4.<init>(r6)     // Catch:{ Exception -> 0x0077 }
            r5.load(r4)     // Catch:{ Exception -> 0x009d, all -> 0x009a }
            r4.close()     // Catch:{ Exception -> 0x009d, all -> 0x009a }
            java.lang.String r8 = "browser_core_version"
            java.lang.String r8 = r5.getProperty(r8)     // Catch:{ Exception -> 0x009d, all -> 0x009a }
            com.tencent.smtt.sdk.SDKEngine.mCoreVersionString = r8     // Catch:{ Exception -> 0x009d, all -> 0x009a }
            java.lang.String r8 = com.tencent.smtt.sdk.SDKEngine.mCoreVersionString     // Catch:{ Exception -> 0x009d, all -> 0x009a }
            if (r8 != 0) goto L_0x005d
            if (r4 == 0) goto L_0x0004
            r4.close()     // Catch:{ IOException -> 0x0058 }
            goto L_0x0004
        L_0x0058:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0004
        L_0x005d:
            java.lang.String r8 = com.tencent.smtt.sdk.SDKEngine.mCoreVersionString     // Catch:{ Exception -> 0x009d, all -> 0x009a }
            int r8 = java.lang.Integer.parseInt(r8)     // Catch:{ Exception -> 0x009d, all -> 0x009a }
            com.tencent.smtt.sdk.SDKEngine.mCoreVersion = r8     // Catch:{ Exception -> 0x009d, all -> 0x009a }
            r3 = r4
        L_0x0066:
            if (r3 == 0) goto L_0x006b
            r3.close()     // Catch:{ IOException -> 0x0089 }
        L_0x006b:
            int r8 = com.tencent.smtt.sdk.SDKEngine.mCoreVersion
            if (r8 != 0) goto L_0x0004
            com.tencent.smtt.sdk.QbSdk.installX5(r10)
            goto L_0x0004
        L_0x0073:
            r8 = 0
            com.tencent.smtt.sdk.SDKEngine.mCoreVersion = r8     // Catch:{ Exception -> 0x0077 }
            goto L_0x0066
        L_0x0077:
            r2 = move-exception
        L_0x0078:
            r2.printStackTrace()     // Catch:{ all -> 0x008e }
            r8 = 0
            com.tencent.smtt.sdk.SDKEngine.mCoreVersion = r8     // Catch:{ all -> 0x008e }
            if (r3 == 0) goto L_0x006b
            r3.close()     // Catch:{ IOException -> 0x0084 }
            goto L_0x006b
        L_0x0084:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x006b
        L_0x0089:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x006b
        L_0x008e:
            r8 = move-exception
        L_0x008f:
            if (r3 == 0) goto L_0x0094
            r3.close()     // Catch:{ IOException -> 0x0095 }
        L_0x0094:
            throw r8
        L_0x0095:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0094
        L_0x009a:
            r8 = move-exception
            r3 = r4
            goto L_0x008f
        L_0x009d:
            r2 = move-exception
            r3 = r4
            goto L_0x0078
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.smtt.sdk.SDKEngine.initX5CoreVersionIfNeed(android.content.Context):void");
    }

    public static int getQQBrowserCoreVersion(Context context) {
        initX5CoreVersionIfNeed(context);
        return mCoreVersion;
    }

    public boolean getX5CoreNeedReboot() {
        boolean needReboot;
        if (isX5Core()) {
            return false;
        }
        if (mQBTimesTampDiff || mDexDone || mX5CoreInstalled) {
            needReboot = true;
        } else {
            needReboot = false;
        }
        QbSdkLog.d("X5CoreNeedReboot", "SDKEngine--getX5CoreNeedReboot  mQBTimesTampDiff = " + mQBTimesTampDiff + " mDexDone = " + mDexDone + " mX5CoreInstalled = " + mX5CoreInstalled);
        if (needReboot) {
            if (!getX5CoreNeedRebootCalled) {
                getX5CoreNeedRebootCalled = true;
                if (!isRebootCalledTimesOK()) {
                    return false;
                }
            } else if (mRebootCalledTimes >= mRebootCalledTimesMax) {
                return false;
            }
        }
        return needReboot;
    }

    private boolean isRebootCalledTimesOK() {
        if (getCalledCountKey() == null) {
            return true;
        }
        File propFile = new File(this.mX5CorePath, COUNT_PROPERTIES);
        if (!propFile.exists()) {
            setCountProp(1);
            return true;
        }
        try {
            FileInputStream fos = new FileInputStream(propFile);
            try {
                Properties prop = new Properties();
                prop.load(fos);
                int count_int = Integer.valueOf(prop.getProperty(getCalledCountKey(), "1")).intValue();
                QbSdkLog.d("X5CoreNeedReboot", "SDKEngine--isRebootCalledTimesOK count_int = " + count_int);
                mRebootCalledTimes = count_int;
                int count_int2 = count_int + 1;
                if (count_int > mRebootCalledTimesMax) {
                    return false;
                }
                prop.setProperty(getCalledCountKey(), String.valueOf(count_int2));
                prop.store(new FileOutputStream(new File(this.mX5CorePath, COUNT_PROPERTIES)), (String) null);
                return true;
            } catch (Exception e) {
                e = e;
                e.printStackTrace();
                return false;
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            return false;
        }
    }

    private void setCountProp(int i) {
        QbSdkLog.d("X5CoreNeedReboot", "SDKEngine--setCountProp i = " + i);
        String count = String.valueOf(i);
        Properties prop = new Properties();
        prop.setProperty(getCalledCountKey(), count);
        try {
            prop.store(new FileOutputStream(new File(this.mX5CorePath, COUNT_PROPERTIES)), (String) null);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    private String getCalledCountKey() {
        if (mQBTimestampStart.equals(Constants.STR_EMPTY) || mQBTimestampFinish.equals(Constants.STR_EMPTY)) {
            return null;
        }
        return mQBTimestampStart + mQBTimestampFinish;
    }

    public static void compareQBTimestampForReboot(Context context) {
        if (context != null) {
            mQBTimestampFinish = getQBAssetTimestamp(context);
            QbSdkLog.d("X5CoreNeedReboot", "SDKEngine--compareQBTimestampForReboot mQBTimestampFinish = " + mQBTimestampFinish);
            if (mQBTimestampFinish.equals(Constants.STR_EMPTY)) {
                mQBTimesTampDiff = false;
            } else if (!mQBTimestampFinish.equals(mQBTimestampStart)) {
                mQBTimesTampDiff = true;
            } else {
                mQBTimesTampDiff = false;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void checkX5CoreInstalled() {
        mX5CoreInstalled = false;
        this.mUIHandler.sendEmptyMessageDelayed(101, (long) checkTimeDelay);
        QbSdkLog.d("X5CoreNeedReboot", "SDKEngine--checkX5CoreInstalled()--MSG_CHECK_X5CORE_INSTALLED");
    }

    public static String getQBAssetTimestamp(Context context) {
        if (context == null) {
            return Constants.STR_EMPTY;
        }
        try {
            Context x5pkgContext = getPackageContext(context, X5QQBROWSER_PKG_NAME);
            if (x5pkgContext == null) {
                return Constants.STR_EMPTY;
            }
            Properties baseProp = getX5Config(x5pkgContext.getAssets().open(X5_ASSETS_CONF_PATH));
            if (baseProp == null) {
                return Constants.STR_EMPTY;
            }
            return baseProp.getProperty(X5_CONF_TIMESTAMP, Constants.STR_EMPTY);
        } catch (IOException e) {
            e.printStackTrace();
            return Constants.STR_EMPTY;
        }
    }

    private static Properties getX5Config(InputStream in) {
        Properties prop = new Properties();
        InputStream ins = in;
        try {
            prop.load(ins);
            ins.close();
            return prop;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return null;
    }
}
