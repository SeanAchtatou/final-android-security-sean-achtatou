package com.tencent.smtt.sdk;

import android.graphics.Bitmap;
import com.tencent.smtt.export.external.interfaces.IX5WebHistoryItem;

public class WebHistoryItem {
    private android.webkit.WebHistoryItem mSysWebHistoryItem = null;
    private IX5WebHistoryItem mWebHistoryItemImpl = null;

    private WebHistoryItem() {
    }

    static WebHistoryItem wrap(IX5WebHistoryItem item) {
        if (item == null) {
            return null;
        }
        WebHistoryItem result = new WebHistoryItem();
        result.mWebHistoryItemImpl = item;
        return result;
    }

    static WebHistoryItem wrap(android.webkit.WebHistoryItem item) {
        if (item == null) {
            return null;
        }
        WebHistoryItem result = new WebHistoryItem();
        result.mSysWebHistoryItem = item;
        return result;
    }

    public String getUrl() {
        if (this.mWebHistoryItemImpl != null) {
            return this.mWebHistoryItemImpl.getUrl();
        }
        return this.mSysWebHistoryItem.getUrl();
    }

    public String getOriginalUrl() {
        if (this.mWebHistoryItemImpl != null) {
            return this.mWebHistoryItemImpl.getOriginalUrl();
        }
        return this.mSysWebHistoryItem.getOriginalUrl();
    }

    public String getTitle() {
        if (this.mWebHistoryItemImpl != null) {
            return this.mWebHistoryItemImpl.getTitle();
        }
        return this.mSysWebHistoryItem.getTitle();
    }

    public Bitmap getFavicon() {
        if (this.mWebHistoryItemImpl != null) {
            return this.mWebHistoryItemImpl.getFavicon();
        }
        return this.mSysWebHistoryItem.getFavicon();
    }
}
