package com.tencent.feedback.common;

import com.tencent.feedback.proguard.ac;
import java.util.Locale;

/* compiled from: ProGuard */
public final class g {

    /* renamed from: a  reason: collision with root package name */
    private static h f2545a = null;

    private static synchronized h a() {
        h hVar;
        synchronized (g.class) {
            hVar = f2545a;
        }
        return hVar;
    }

    public static synchronized void a(h hVar) {
        synchronized (g.class) {
            f2545a = hVar;
        }
    }

    private static boolean a(int i, String str, Object... objArr) {
        h a2 = a();
        if (a2 == null) {
            return false;
        }
        if (str == null) {
            str = "null";
        } else if (!(objArr == null || objArr.length == 0)) {
            str = String.format(Locale.US, str, objArr);
        }
        switch (i) {
            case 0:
                a2.a(str);
                return true;
            case 1:
                a2.b(str);
                return true;
            case 2:
                a2.c(str);
                return true;
            case 3:
                a2.d(str);
                return true;
            default:
                return false;
        }
    }

    public static boolean a(String str, Object... objArr) {
        return a(0, str, objArr);
    }

    public static boolean b(String str, Object... objArr) {
        return a(1, str, objArr);
    }

    public static boolean c(String str, Object... objArr) {
        return a(2, str, objArr);
    }

    public static boolean a(Throwable th) {
        h a2 = a();
        if (a2 == null) {
            return false;
        }
        a2.c(ac.a(th));
        return true;
    }

    public static boolean d(String str, Object... objArr) {
        return a(3, str, objArr);
    }
}
