package com.tencent.feedback.proguard;

import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Collection;

public final class T extends C0008j {
    private static ArrayList<V> h;
    private static W i;

    /* renamed from: a  reason: collision with root package name */
    public ArrayList<V> f2576a = null;
    public W b = null;
    public int c = 0;
    public int d = 0;
    public String e = Constants.STR_EMPTY;
    public boolean f = true;
    public boolean g = true;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.ArrayList<com.tencent.feedback.proguard.V>, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
     arg types: [com.tencent.feedback.proguard.W, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.eup.jni.c, java.nio.ByteBuffer):int
      com.tencent.feedback.proguard.ag.a(int, boolean):boolean */
    public final void a(ag agVar) {
        if (h == null) {
            h = new ArrayList<>();
            h.add(new V());
        }
        this.f2576a = (ArrayList) agVar.a((Object) h, 0, true);
        if (i == null) {
            i = new W();
        }
        this.b = (W) agVar.a((C0008j) i, 1, true);
        this.c = agVar.a(this.c, 2, true);
        this.d = agVar.a(this.d, 3, true);
        this.e = agVar.b(4, true);
        boolean z = this.f;
        this.f = agVar.a(5, false);
        boolean z2 = this.g;
        this.g = agVar.a(6, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ah.a(java.util.Collection, int):void
     arg types: [java.util.ArrayList<com.tencent.feedback.proguard.V>, int]
     candidates:
      com.tencent.feedback.proguard.ah.a(byte, int):void
      com.tencent.feedback.proguard.ah.a(float, int):void
      com.tencent.feedback.proguard.ah.a(int, int):void
      com.tencent.feedback.proguard.ah.a(long, int):void
      com.tencent.feedback.proguard.ah.a(com.tencent.feedback.proguard.j, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.Object, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.String, int):void
      com.tencent.feedback.proguard.ah.a(java.util.Map, int):void
      com.tencent.feedback.proguard.ah.a(short, int):void
      com.tencent.feedback.proguard.ah.a(boolean, int):void
      com.tencent.feedback.proguard.ah.a(byte[], int):void
      com.tencent.feedback.proguard.ah.a(java.util.Collection, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ah.a(com.tencent.feedback.proguard.j, int):void
     arg types: [com.tencent.feedback.proguard.W, int]
     candidates:
      com.tencent.feedback.proguard.ah.a(byte, int):void
      com.tencent.feedback.proguard.ah.a(float, int):void
      com.tencent.feedback.proguard.ah.a(int, int):void
      com.tencent.feedback.proguard.ah.a(long, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.Object, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.String, int):void
      com.tencent.feedback.proguard.ah.a(java.util.Collection, int):void
      com.tencent.feedback.proguard.ah.a(java.util.Map, int):void
      com.tencent.feedback.proguard.ah.a(short, int):void
      com.tencent.feedback.proguard.ah.a(boolean, int):void
      com.tencent.feedback.proguard.ah.a(byte[], int):void
      com.tencent.feedback.proguard.ah.a(com.tencent.feedback.proguard.j, int):void */
    public final void a(ah ahVar) {
        ahVar.a((Collection) this.f2576a, 0);
        ahVar.a((C0008j) this.b, 1);
        ahVar.a(this.c, 2);
        ahVar.a(this.d, 3);
        ahVar.a(this.e, 4);
        ahVar.a(this.f, 5);
        ahVar.a(this.g, 6);
    }

    public final void a(StringBuilder sb, int i2) {
    }
}
