package com.tencent.feedback.b;

import android.content.Context;

/* compiled from: ProGuard */
public abstract class e {

    /* renamed from: a  reason: collision with root package name */
    private static e f2537a = null;

    public abstract byte[] a(String str, byte[] bArr, d dVar);

    public static synchronized e a(Context context) {
        e eVar;
        synchronized (e.class) {
            if (f2537a == null) {
                if (context != null) {
                    Context applicationContext = context.getApplicationContext();
                    if (applicationContext != null) {
                        context = applicationContext;
                    }
                }
                f2537a = new f(context);
            }
            eVar = f2537a;
        }
        return eVar;
    }
}
