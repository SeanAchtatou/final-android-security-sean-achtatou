package com.tencent.feedback.eup;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Process;
import android.util.Log;
import com.tencent.feedback.common.b;
import com.tencent.feedback.common.g;
import com.tencent.feedback.proguard.ac;
import java.io.UnsupportedEncodingException;

/* compiled from: ProGuard */
public class BuglyBroadcastRecevier extends BroadcastReceiver {
    private static BuglyBroadcastRecevier c = null;

    /* renamed from: a  reason: collision with root package name */
    private IntentFilter f2552a = new IntentFilter();
    private Context b;
    private final f d = null;

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
        if (this.b != null) {
            this.b.unregisterReceiver(this);
        }
    }

    public final void onReceive(Context context, Intent intent) {
        if (!a(context, intent)) {
            d a2 = c.a();
            if (a2 == null) {
                g.d("magic! no crash stategy,no notify return ?", new Object[0]);
            } else if (!a2.s()) {
                g.a("close proc receiver!", new Object[0]);
            } else {
                if (b(context, intent) || c(context, intent)) {
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final synchronized boolean a(Context context, Intent intent) {
        return false;
    }

    protected static void a(Context context) {
        if (context != null) {
            try {
                d a2 = c.a();
                if (a2 == null) {
                    g.d("magic! no crash stategy,no notify return ?", new Object[0]);
                } else if (!a2.r()) {
                    g.a("close brocast!", new Object[0]);
                } else {
                    g.a("notify launch !", new Object[0]);
                    String b2 = b.b(context);
                    byte[] a3 = ac.a(b2.getBytes("utf8"), 1, "feedback");
                    Intent intent = new Intent("com.tencent.feedback.A01");
                    intent.putExtra("com.tencent.feedback.P12", a3);
                    Bundle bundle = new Bundle();
                    bundle.putString("com.tencent.feedback.P01", b2);
                    bundle.putString("com.tencent.feedback.P02", b.a(context, Process.myPid()));
                    bundle.putBoolean("com.tencent.feedback.P03", b.g(context));
                    intent.putExtra("com.tencent.feedback.B01", bundle);
                    context.sendBroadcast(intent);
                }
            } catch (Throwable th) {
                if (!g.a(th)) {
                    Log.w("eup", "something error " + th.getClass().getName());
                }
            }
        }
    }

    public static void a(Context context, e eVar) {
        if (context != null && eVar != null) {
            try {
                d a2 = c.a();
                if (a2 == null) {
                    g.d("magic! no crash stategy,no notify return ?", new Object[0]);
                } else if (!a2.r()) {
                    g.a("close brocast!", new Object[0]);
                } else {
                    g.a("notify crash !", new Object[0]);
                    String b2 = b.b(context);
                    byte[] a3 = ac.a(b2.getBytes("utf8"), 1, "feedback");
                    Intent intent = new Intent("com.tencent.feedback.A02");
                    intent.putExtra("com.tencent.feedback.P12", a3);
                    Bundle bundle = new Bundle();
                    bundle.putString("com.tencent.feedback.P01", b2);
                    bundle.putString("com.tencent.feedback.P02", eVar.s());
                    bundle.putString("com.tencent.feedback.P04", eVar.t());
                    bundle.putByte("com.tencent.feedback.P05", eVar.S());
                    bundle.putLong("com.tencent.feedback.P13", eVar.J());
                    bundle.putLong("com.tencent.feedback.P06", eVar.K());
                    bundle.putLong("com.tencent.feedback.P07", eVar.I());
                    bundle.putString("com.tencent.feedback.P08", eVar.e());
                    bundle.putString("com.tencent.feedback.P09", eVar.f());
                    bundle.putString("com.tencent.feedback.P010", eVar.h());
                    bundle.putBoolean("com.tencent.feedback.P03", b.g(context));
                    bundle.putLong("com.tencent.feedback.P011", eVar.T());
                    intent.putExtra("com.tencent.feedback.B02", bundle);
                    context.sendBroadcast(intent);
                }
            } catch (Throwable th) {
                if (!g.a(th)) {
                    Log.w("eup", "something error " + th.getClass().getName());
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final synchronized boolean b(Context context, Intent intent) {
        boolean z = true;
        synchronized (this) {
            if (!(context == null || intent == null)) {
                if (intent.getAction().equals("com.tencent.feedback.A01")) {
                    if (this.d == null) {
                        g.a("no moniter handler", new Object[0]);
                    } else {
                        try {
                            String packageName = context.getPackageName();
                            String i = b.i(context);
                            byte[] byteArrayExtra = intent.getByteArrayExtra("com.tencent.feedback.P12");
                            Bundle bundleExtra = intent.getBundleExtra("com.tencent.feedback.B01");
                            if (byteArrayExtra == null || bundleExtra == null) {
                                g.c("args fail other proc launch %s %s", new StringBuilder().append(byteArrayExtra).toString(), new StringBuilder().append(bundleExtra).toString());
                            } else {
                                String str = new String(ac.b(byteArrayExtra, 1, "feedback"), "utf8");
                                if (!str.equals(bundleExtra.getString("com.tencent.feedback.P01"))) {
                                    g.c("args fail other proc launch inner %s %s", str, bundleExtra.getString("com.tencent.feedback.P01"));
                                } else if ((str + bundleExtra.getString("com.tencent.feedback.P02")).equals(packageName + i)) {
                                    g.a("current proc not need to notify", new Object[0]);
                                } else {
                                    g.a("notify other app lau %s", str);
                                    this.d.a(str, bundleExtra.getString("com.tencent.feedback.P02"), bundleExtra.getBoolean("com.tencent.feedback.P03"), bundleExtra);
                                    g.a("notify other app lau %s end", str);
                                }
                            }
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            z = false;
        }
        return z;
    }

    public final synchronized boolean c(Context context, Intent intent) {
        boolean z;
        if (!(context == null || intent == null)) {
            if (intent.getAction().equals("com.tencent.feedback.A02")) {
                if (this.d == null) {
                    g.a("no moniter handler", new Object[0]);
                    z = true;
                } else {
                    try {
                        String packageName = context.getPackageName();
                        String i = b.i(context);
                        byte[] byteArrayExtra = intent.getByteArrayExtra("com.tencent.feedback.P12");
                        Bundle bundleExtra = intent.getBundleExtra("com.tencent.feedback.B02");
                        if (byteArrayExtra == null || bundleExtra == null) {
                            g.c("args fail other proc cra %s %s", new StringBuilder().append(byteArrayExtra).toString(), new StringBuilder().append(bundleExtra).toString());
                            z = true;
                        } else {
                            String str = new String(ac.b(byteArrayExtra, 1, "feedback"), "utf8");
                            if (!str.equals(bundleExtra.getString("com.tencent.feedback.P01"))) {
                                g.c("args fail other proc cra inner %s %s", str, bundleExtra.getString("com.tencent.feedback.P01"));
                                z = true;
                            } else if ((str + bundleExtra.getString("com.tencent.feedback.P02")).equals(packageName + i)) {
                                g.a("current proc not need to notify", new Object[0]);
                                z = true;
                            } else {
                                g.a("notify other app cra %s", str);
                                this.d.a(str, bundleExtra.getString("com.tencent.feedback.P02"), bundleExtra.getString("com.tencent.feedback.P04"), bundleExtra.getByte("com.tencent.feedback.P05"), bundleExtra.getLong("com.tencent.feedback.P13"), bundleExtra.getLong("com.tencent.feedback.P06"), bundleExtra.getLong("com.tencent.feedback.P07"), bundleExtra.getString("com.tencent.feedback.P08"), bundleExtra.getString("com.tencent.feedback.P09"), bundleExtra.getString("com.tencent.feedback.P010"), bundleExtra.getBoolean("com.tencent.feedback.P03"), bundleExtra.getLong("com.tencent.feedback.P011"), bundleExtra);
                                g.a("notify other app cra %s end", str);
                                z = true;
                            }
                        }
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        z = false;
        return z;
    }
}
