package com.tencent.cloud.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HeaderViewListAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.RankRefreshGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.module.callback.a;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.cloud.b.g;
import com.tencent.nucleus.manager.component.SwitchButton;
import com.tencent.pangu.adapter.RankNormalListAdapter;
import com.tencent.pangu.component.appdetail.ad;

/* compiled from: ProGuard */
public class CftAppRankListView extends RankRefreshGetMoreListView implements ITXRefreshListViewListener {
    /* access modifiers changed from: private */
    public static String[] K;
    TextView A;
    ad B;
    boolean[] C = new boolean[10];
    int D = 0;
    protected final int E = 1;
    protected final int F = 2;
    protected final String G = "isFirstPage";
    protected final String H = "key_data";
    protected a I = new m(this);
    protected ViewInvalidateMessageHandler J = new n(this);
    protected g e = null;
    protected RankNormalListAdapter f = null;
    protected q u;
    protected int v = 1;
    protected ListViewScrollListener w;
    LinearLayout x;
    View y;
    SwitchButton z;

    public void a(ListViewScrollListener listViewScrollListener) {
        this.w = listViewScrollListener;
        setOnScrollerListener(this.w);
    }

    public void a(q qVar) {
        this.u = qVar;
    }

    public void a(g gVar) {
        this.e = gVar;
        this.e.register(this.I);
        setRefreshListViewListener(this);
    }

    public CftAppRankListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        q();
    }

    public ListView getListView() {
        return (ListView) this.s;
    }

    /* access modifiers changed from: protected */
    public void q() {
        ListAdapter adapter = ((ListView) this.s).getAdapter();
        if (adapter instanceof HeaderViewListAdapter) {
            this.f = (RankNormalListAdapter) ((HeaderViewListAdapter) adapter).getWrappedAdapter();
        } else {
            this.f = (RankNormalListAdapter) ((ListView) this.s).getAdapter();
        }
        if (this.f != null && K == null) {
            K = AstApp.i().getResources().getStringArray(R.array.logo_tips3);
        }
    }

    public void r() {
        if (this.f == null) {
            q();
        }
        if (this.f.getCount() > 0) {
            this.w.sendMessage(new ViewInvalidateMessage(2, null, this.J));
            return;
        }
        this.e.c();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.cloud.component.CftAppRankListView.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.assistant.component.txscrollview.RankRefreshGetMoreListView.a(com.tencent.assistant.component.txscrollview.RankRefreshGetMoreListView, boolean):boolean
      com.tencent.assistant.component.txscrollview.RankRefreshGetMoreListView.a(android.content.Context, com.tencent.assistant.component.txscrollview.TXScrollViewBase$ScrollMode):com.tencent.assistant.component.txscrollview.TXLoadingLayoutBase
      com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase.a(android.content.Context, com.tencent.assistant.component.txscrollview.TXScrollViewBase$ScrollMode):com.tencent.assistant.component.txscrollview.TXLoadingLayoutBase
      com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase.a(int, int):void
      com.tencent.assistant.component.txscrollview.TXScrollViewBase.a(android.content.Context, android.view.View):void
      com.tencent.assistant.component.txscrollview.TXScrollViewBase.a(int, int):void
      com.tencent.cloud.component.CftAppRankListView.a(boolean, boolean):void */
    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        if (scrollState == TXScrollViewBase.ScrollState.ScrollState_FromEnd) {
            this.e.e();
        } else if (scrollState != TXScrollViewBase.ScrollState.ScrollState_FromStart) {
        } else {
            if (this.y != null) {
                a(true, true);
                this.g.findViewById(R.id.tips).setVisibility(0);
                onTopRefreshCompleteNoAnimation();
                return;
            }
            onTopRefreshComplete();
        }
    }

    public void recycleData() {
        super.recycleData();
        this.e.unregister(this.I);
    }

    public void s() {
        if (this.y == null) {
            this.x = (LinearLayout) LayoutInflater.from(getContext()).inflate((int) R.layout.v5_hide_installed_apps_area, (ViewGroup) null);
            ((ListView) this.s).addHeaderView(this.x);
            this.y = this.x.findViewById(R.id.header_container);
            this.z = (SwitchButton) this.x.findViewById(R.id.hide_btn);
            this.z.setClickable(false);
            this.A = (TextView) this.x.findViewById(R.id.tips);
            this.z.a(com.tencent.assistantv2.manager.a.a().b().c());
            setRankHeaderPaddingBottomAdded(-this.z.getResources().getDimensionPixelSize(R.dimen.sliding_drawer_group_icon_down_margin));
            this.y.setOnClickListener(new o(this));
        }
    }

    public void a(boolean z2, boolean z3) {
        if (this.y == null) {
            return;
        }
        if (!z2) {
            this.y.setClickable(false);
            if (!z3) {
                this.y.setVisibility(8);
                this.isHideInstalledAppAreaAdded = false;
                this.A.setVisibility(8);
                if (this.B != null) {
                    this.B.f3563a = false;
                }
            } else if (this.isHideInstalledAppAreaAdded) {
                this.isHideInstalledAppAreaAdded = false;
                if (this.B == null) {
                    this.B = new ad(this.y);
                }
                this.B.f3563a = true;
                this.y.postDelayed(this.B, 5);
            }
        } else if (!this.isHideInstalledAppAreaAdded) {
            if (this.B != null) {
                this.B.f3563a = false;
            }
            this.y.setBackgroundResource(R.drawable.v2_button_background_selector);
            this.y.setPressed(false);
            this.y.setVisibility(0);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.y.getLayoutParams();
            if (layoutParams.height != getResources().getDimensionPixelSize(R.dimen.hide_installed_apps_area_height)) {
                layoutParams.height = getResources().getDimensionPixelSize(R.dimen.hide_installed_apps_area_height);
                this.y.setLayoutParams(layoutParams);
            }
            this.y.setClickable(true);
            this.isHideInstalledAppAreaAdded = true;
            t();
        }
    }

    /* access modifiers changed from: private */
    public void a(int i, int i2, boolean z2) {
        if (i2 == 0) {
            this.u.d();
            if (this.f == null) {
                q();
            }
            if (this.f.getCount() == 0) {
                this.u.a(10);
                return;
            }
            this.f.notifyDataSetChanged();
            onRefreshComplete(this.e.g(), true);
        } else if (!z2) {
            onRefreshComplete(this.e.g(), false);
            this.u.e();
        } else if (-800 == i2) {
            this.u.a(30);
        } else if (this.v <= 0) {
            this.u.a(20);
        } else {
            this.v--;
            this.e.d();
        }
    }

    /* access modifiers changed from: package-private */
    public void t() {
        STInfoV2 sTInfoV2 = new STInfoV2(((BaseActivity) getContext()).f(), "08", ((BaseActivity) getContext()).f(), "08", 100);
        sTInfoV2.slotId = com.tencent.assistantv2.st.page.a.a("08", 0);
        if (!com.tencent.assistantv2.manager.a.a().b().c()) {
            sTInfoV2.status = "01";
        } else {
            sTInfoV2.status = "02";
        }
        l.a(sTInfoV2);
    }
}
