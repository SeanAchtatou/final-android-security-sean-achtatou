package com.tencent.cloud.component;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.tencent.assistant.adapter.AppCategoryListAdapter;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import com.tencent.assistant.component.invalidater.ViewPageScrollListener;
import com.tencent.assistant.component.txscrollview.TXGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.protocol.jce.AppCategory;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.business.CostTimeSTManager;
import com.tencent.assistantv2.st.l;
import com.tencent.cloud.b.a.a;
import java.util.List;

/* compiled from: ProGuard */
public class CategoryListView extends TXGetMoreListView {
    private final int A;
    private final int B;
    private final int C;
    private final int D;
    private final int E;
    private a F;
    /* access modifiers changed from: private */
    public ViewInvalidateMessageHandler G;
    private com.tencent.cloud.b.a u;
    /* access modifiers changed from: private */
    public long v;
    /* access modifiers changed from: private */
    public j w;
    private int x;
    /* access modifiers changed from: private */
    public AppCategoryListAdapter y;
    /* access modifiers changed from: private */
    public ViewPageScrollListener z;

    public void a(j jVar) {
        this.w = jVar;
    }

    public CategoryListView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.u = null;
        this.v = 0;
        this.x = 3;
        this.A = 1;
        this.B = 2;
        this.C = 0;
        this.D = 1;
        this.E = 2;
        this.F = new h(this);
        this.G = new i(this);
        this.u = com.tencent.cloud.b.a.a();
        this.u.register(this.F);
        g();
        setDivider(null);
    }

    public CategoryListView(Context context) {
        super(context);
        this.u = null;
        this.v = 0;
        this.x = 3;
        this.A = 1;
        this.B = 2;
        this.C = 0;
        this.D = 1;
        this.E = 2;
        this.F = new h(this);
        this.G = new i(this);
        this.u = com.tencent.cloud.b.a.a();
        this.u.register(this.F);
        g();
        setDivider(null);
    }

    public CategoryListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.u = null;
        this.v = 0;
        this.x = 3;
        this.A = 1;
        this.B = 2;
        this.C = 0;
        this.D = 1;
        this.E = 2;
        this.F = new h(this);
        this.G = new i(this);
        this.u = com.tencent.cloud.b.a.a();
        this.u.register(this.F);
        g();
        setDivider(null);
    }

    public CategoryListView(Context context, TXScrollViewBase.ScrollMode scrollMode) {
        super(context);
        this.u = null;
        this.v = 0;
        this.x = 3;
        this.A = 1;
        this.B = 2;
        this.C = 0;
        this.D = 1;
        this.E = 2;
        this.F = new h(this);
        this.G = new i(this);
        this.u = com.tencent.cloud.b.a.a();
        this.u.register(this.F);
        g();
        setDivider(null);
    }

    public void a(ViewPageScrollListener viewPageScrollListener) {
        this.z = viewPageScrollListener;
    }

    public void g() {
        ListAdapter adapter = ((ListView) this.s).getAdapter();
        if (adapter instanceof HeaderViewListAdapter) {
            this.y = (AppCategoryListAdapter) ((HeaderViewListAdapter) adapter).getWrappedAdapter();
        } else {
            this.y = (AppCategoryListAdapter) ((ListView) this.s).getAdapter();
        }
    }

    public void q() {
    }

    public void recycleData() {
        super.recycleData();
        this.u.unregister(this.F);
    }

    public void r() {
        if (this.y == null) {
            g();
        }
        if (this.y == null || this.y.getCount() <= 0) {
            this.u.c();
            return;
        }
        this.z.sendMessage(new ViewInvalidateMessage(2, null, this.G));
    }

    /* access modifiers changed from: private */
    public void a(int i, int i2, List<ColorCardItem> list, List<AppCategory> list2, List<AppCategory> list3) {
        if (i2 == 0) {
            this.w.a();
            if (!(this.y == null || list3 == null)) {
                this.y.a(list, list2, list3);
                l.a((int) STConst.ST_PAGE_SOFTWARE_CATEGORY, CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
            }
        } else {
            l.a((int) STConst.ST_PAGE_SOFTWARE_CATEGORY, CostTimeSTManager.TIMETYPE.CANCEL, System.currentTimeMillis());
            if (-800 == i2) {
                this.w.a(30);
                return;
            } else if (this.x <= 0) {
                this.w.a(20);
                return;
            } else {
                this.u.d();
                this.x--;
            }
        }
        if (this.y == null) {
            g();
        }
        if (this.y != null) {
            this.y.notifyDataSetChanged();
        }
    }

    public void a(long j) {
        this.v = j;
    }

    public ListView getListView() {
        return (ListView) this.s;
    }
}
