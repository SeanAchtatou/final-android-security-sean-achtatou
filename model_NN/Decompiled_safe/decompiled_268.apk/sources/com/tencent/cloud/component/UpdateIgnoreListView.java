package com.tencent.cloud.component;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXGetMoreListView;
import com.tencent.assistant.protocol.jce.StatUpdateManageAction;
import com.tencent.assistant.utils.by;
import com.tencent.cloud.adapter.AppUpdateIgnoreListAdapter;

/* compiled from: ProGuard */
public class UpdateIgnoreListView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    public AppUpdateIgnoreListAdapter f2268a;
    private Context b;
    private AstApp c;
    private LayoutInflater d;
    private View e;
    private TXGetMoreListView f;
    private StatUpdateManageAction g;
    private final int h;
    private final int i;
    private Handler j;

    public UpdateIgnoreListView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, null);
    }

    public UpdateIgnoreListView(Context context, StatUpdateManageAction statUpdateManageAction) {
        this(context, null, statUpdateManageAction);
    }

    public UpdateIgnoreListView(Context context, AttributeSet attributeSet, StatUpdateManageAction statUpdateManageAction) {
        super(context, attributeSet);
        this.h = 1;
        this.i = 2;
        this.j = new t(this);
        this.g = statUpdateManageAction;
        this.c = AstApp.i();
        this.b = context;
        this.c.j();
        this.d = (LayoutInflater) this.b.getSystemService("layout_inflater");
        c();
    }

    private void c() {
        this.e = this.d.inflate((int) R.layout.updateignorelist_component, this);
        this.f = (TXGetMoreListView) this.e.findViewById(R.id.updateignorelist);
        this.f2268a = new AppUpdateIgnoreListAdapter(this.b, this.f, this.g);
        ImageView imageView = new ImageView(getContext());
        imageView.setLayoutParams(new AbsListView.LayoutParams(-1, by.a(getContext(), 4.0f)));
        imageView.setBackgroundColor(getResources().getColor(17170445));
        this.f.addHeaderView(imageView);
        this.f.setDivider(null);
        this.f.setAdapter(this.f2268a);
    }

    public void a() {
        if (this.f2268a != null) {
            this.f2268a.notifyDataSetChanged();
        }
    }

    public void a(String str) {
        this.f2268a.a(str);
        Message obtainMessage = this.j.obtainMessage();
        obtainMessage.what = 1;
        this.j.removeMessages(1);
        this.j.sendMessage(obtainMessage);
    }

    public void b() {
        Message obtainMessage = this.j.obtainMessage();
        obtainMessage.what = 2;
        this.j.removeMessages(2);
        this.j.sendMessage(obtainMessage);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return false;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return true;
    }
}
