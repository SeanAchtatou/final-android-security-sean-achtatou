package com.tencent.cloud.adapter;

import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.pangu.module.wisedownload.s;
import java.util.Comparator;

/* compiled from: ProGuard */
class q implements Comparator<SimpleAppModel> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ g f2226a;

    q(g gVar) {
        this.f2226a = gVar;
    }

    /* renamed from: a */
    public int compare(SimpleAppModel simpleAppModel, SimpleAppModel simpleAppModel2) {
        if (!(simpleAppModel == null || simpleAppModel2 == null)) {
            boolean b = s.b(simpleAppModel);
            boolean b2 = s.b(simpleAppModel2);
            if (b) {
                return -1;
            }
            if (b2) {
                return 1;
            }
        }
        return 0;
    }
}
