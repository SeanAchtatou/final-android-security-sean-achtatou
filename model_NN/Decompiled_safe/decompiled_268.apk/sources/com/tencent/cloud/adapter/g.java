package com.tencent.cloud.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Pair;
import android.util.SparseBooleanArray;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.b.c;
import com.tencent.assistant.b.i;
import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.assistant.component.txscrollview.TXExpandableListView;
import com.tencent.assistant.m;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.d;
import com.tencent.assistant.module.k;
import com.tencent.assistant.module.update.j;
import com.tencent.assistant.protocol.jce.LbsData;
import com.tencent.assistant.protocol.jce.StatUpdateManageAction;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.page.STExternalInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.cloud.b.r;
import com.tencent.cloud.updaterec.a;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.activity.AppDetailActivityV5;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class g extends BaseExpandableListAdapter {
    static a f;

    /* renamed from: a  reason: collision with root package name */
    public View f2216a;
    public boolean b = false;
    public SparseIntArray c = new SparseIntArray();
    public SparseIntArray d = new SparseIntArray();
    public int e = -1;
    private Context g;
    private LayoutInflater h;
    private final int i = 1;
    private final int j = 2;
    private final int[] k = {1, 2};
    /* access modifiers changed from: private */
    public Map<Long, Integer> l;
    /* access modifiers changed from: private */
    public ArrayList<Integer> m = new ArrayList<>();
    /* access modifiers changed from: private */
    public LinkedHashMap<Integer, ArrayList<SimpleAppModel>> n = new LinkedHashMap<>();
    /* access modifiers changed from: private */
    public ArrayList<SimpleAppModel> o = new ArrayList<>();
    /* access modifiers changed from: private */
    public SparseBooleanArray p = new SparseBooleanArray();
    /* access modifiers changed from: private */
    public String q = null;
    private StatUpdateManageAction r = null;
    private String s = Constants.STR_EMPTY;
    private STExternalInfo t;
    private Activity u;
    private b v = null;
    private Comparator<SimpleAppModel> w = new q(this);
    /* access modifiers changed from: private */
    public c x = null;
    /* access modifiers changed from: private */
    public LbsData y = null;
    private i z = new i(this);

    public g(Context context, View view, StatUpdateManageAction statUpdateManageAction) {
        AstApp.i();
        this.g = context;
        this.h = LayoutInflater.from(this.g);
        this.r = statUpdateManageAction;
        this.f2216a = view;
        this.x = new c(AstApp.i().getApplicationContext(), this.z);
        TemporaryThreadManager.get().start(new h(this));
    }

    private int b(int i2) {
        switch (i2) {
            case 1:
            case 2:
                return 1;
            default:
                return 2;
        }
    }

    public void a(STExternalInfo sTExternalInfo, String str) {
        this.t = sTExternalInfo;
        this.s = str;
    }

    public void a(Map<Long, Integer> map) {
        this.l = map;
    }

    public void a(List<SimpleAppModel> list, boolean z2) {
        ArrayList arrayList;
        if (list == null) {
            list = new ArrayList<>();
        }
        if (this.o.size() == 0 || z2) {
            this.o.clear();
            this.o.addAll(list);
            if (this.c != null) {
                this.c.clear();
            }
            if (this.d != null) {
                this.d.clear();
            }
        } else {
            ArrayList arrayList2 = new ArrayList();
            Iterator<SimpleAppModel> it = this.o.iterator();
            while (it.hasNext()) {
                arrayList2.add(Integer.valueOf(it.next().c.hashCode()));
            }
            ArrayList arrayList3 = new ArrayList();
            int i2 = 0;
            Iterator<SimpleAppModel> it2 = list.iterator();
            while (true) {
                int i3 = i2;
                if (!it2.hasNext()) {
                    break;
                }
                SimpleAppModel next = it2.next();
                arrayList3.add(Integer.valueOf(next.c.hashCode()));
                if (!arrayList2.contains(Integer.valueOf(next.c.hashCode()))) {
                    this.o.add(i3, next);
                }
                i2 = i3 + 1;
            }
            Iterator<SimpleAppModel> it3 = this.o.iterator();
            while (it3.hasNext()) {
                SimpleAppModel next2 = it3.next();
                if (!arrayList3.contains(Integer.valueOf(next2.c.hashCode()))) {
                    if (!(this.c != null && this.c.indexOfKey(next2.c.hashCode()) >= 0)) {
                        it3.remove();
                    }
                }
            }
        }
        this.m.clear();
        this.n.clear();
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        ArrayList arrayList4 = null;
        Iterator<SimpleAppModel> it4 = this.o.iterator();
        while (it4.hasNext()) {
            SimpleAppModel next3 = it4.next();
            int a2 = a(next3);
            if (!this.m.contains(Integer.valueOf(a2))) {
                this.m.add(Integer.valueOf(a2));
            }
            ArrayList arrayList5 = this.n.get(Integer.valueOf(a2));
            if (arrayList5 == null) {
                arrayList5 = new ArrayList();
                this.n.put(Integer.valueOf(a2), arrayList5);
            }
            AppConst.AppState d2 = k.d(next3);
            boolean z3 = this.c != null && this.c.indexOfKey(next3.c.hashCode()) >= 0;
            boolean z4 = this.d != null && this.d.indexOfKey(next3.c.hashCode()) >= 0;
            boolean z5 = AppConst.AppState.DOWNLOADED == d2 && (!z3 || z4);
            boolean z6 = AppConst.AppState.INSTALLING == d2 && (!z3 || z4);
            boolean z7 = AppConst.AppState.INSTALLED == d2 && z4;
            if (z5 || z6 || z7) {
                ArrayList arrayList6 = (ArrayList) linkedHashMap.get(Integer.valueOf(a2));
                if (arrayList6 == null) {
                    arrayList6 = new ArrayList(5);
                    linkedHashMap.put(Integer.valueOf(a2), arrayList6);
                }
                arrayList6.add(next3);
                if (this.d != null && this.d.indexOfKey(next3.c.hashCode()) < 0) {
                    this.d.put(next3.c.hashCode(), 0);
                }
            } else if (this.l == null || !this.l.containsKey(Long.valueOf(next3.f938a))) {
                arrayList5.add(next3);
            } else {
                if (arrayList4 == null) {
                    arrayList = new ArrayList(4);
                } else {
                    arrayList = arrayList4;
                }
                arrayList.add(next3);
                arrayList4 = arrayList;
            }
        }
        if (!linkedHashMap.isEmpty()) {
            for (Integer intValue : linkedHashMap.keySet()) {
                int intValue2 = intValue.intValue();
                ArrayList arrayList7 = (ArrayList) linkedHashMap.get(Integer.valueOf(intValue2));
                if (!arrayList7.isEmpty()) {
                    Collections.sort(arrayList7, this.w);
                    this.n.get(Integer.valueOf(intValue2)).addAll(0, arrayList7);
                }
            }
        }
        if (arrayList4 != null) {
            Collections.sort(arrayList4, new j(this));
            ArrayList arrayList8 = this.n.get(1);
            if (arrayList8 == null) {
                arrayList8 = new ArrayList(5);
                this.n.put(1, arrayList8);
            }
            arrayList8.addAll(0, arrayList4);
        }
        try {
            Collections.sort(this.m);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        notifyDataSetChanged();
    }

    private int a(SimpleAppModel simpleAppModel) {
        int i2;
        try {
            ArrayList<Integer> c2 = j.b().c(simpleAppModel.c);
            if (c2 == null || c2.size() <= 0) {
                i2 = 0;
            } else {
                i2 = c2.get(0).intValue();
            }
            try {
                if (this.l != null && this.l.containsKey(Long.valueOf(simpleAppModel.f938a))) {
                    i2 = 1;
                }
            } catch (Exception e2) {
            }
        } catch (Exception e3) {
            i2 = 0;
        }
        return b(i2);
    }

    public void a() {
    }

    public void b() {
        if (this.p != null) {
            this.p.clear();
            this.p = null;
        }
        if (this.c != null) {
            this.c.clear();
            this.c = null;
        }
        if (this.d != null) {
            this.d.clear();
            this.d = null;
        }
    }

    public int getGroupCount() {
        if (this.m != null) {
            return this.m.size();
        }
        return 0;
    }

    public int getChildrenCount(int i2) {
        ArrayList arrayList;
        if (this.n == null || this.m == null || getGroup(i2) == null || (arrayList = this.n.get((Integer) getGroup(i2))) == null || arrayList.isEmpty()) {
            return 0;
        }
        return arrayList.size();
    }

    public Object getGroup(int i2) {
        if (i2 < 0 || i2 >= this.m.size()) {
            return null;
        }
        return this.m.get(i2);
    }

    private int c(int i2) {
        Object group = getGroup(i2);
        if (group != null) {
            return ((Integer) group).intValue();
        }
        return -1;
    }

    public Object getChild(int i2, int i3) {
        ArrayList arrayList;
        if (this.n == null || this.m == null || getGroup(i2) == null || (arrayList = this.n.get(getGroup(i2))) == null || arrayList.size() <= i3) {
            return null;
        }
        return arrayList.get(i3);
    }

    public long getGroupId(int i2) {
        return (long) i2;
    }

    public long getChildId(int i2, int i3) {
        return (long) i3;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isChildSelectable(int i2, int i3) {
        return true;
    }

    public View getGroupView(int i2, boolean z2, View view, ViewGroup viewGroup) {
        r rVar;
        if (view == null || !(view.getTag() instanceof r)) {
            view = this.h.inflate((int) R.layout.updatelist_group_item, (ViewGroup) null);
            r rVar2 = new r(this, null);
            rVar2.f2227a = view;
            rVar2.b = (RelativeLayout) view.findViewById(R.id.layout_group);
            rVar2.c = (TextView) view.findViewById(R.id.group_title);
            rVar2.d = (TextView) view.findViewById(R.id.group_title_num);
            view.setTag(rVar2);
            rVar = rVar2;
        } else {
            rVar = (r) view.getTag();
        }
        String[] a2 = a(c(i2));
        if (a2 != null) {
            rVar.c.setText(a2[0]);
            rVar.d.setText(" " + a2[1]);
        }
        return view;
    }

    public String[] a(int i2) {
        int i3;
        if (i2 == 1) {
            return new String[]{this.g.getResources().getString(R.string.app_update_group_recommend), "(" + (this.n.get(Integer.valueOf(i2)) != null ? a(this.n.get(Integer.valueOf(i2))) : 0) + ")"};
        } else if (i2 != 2) {
            return null;
        } else {
            if (this.n.get(Integer.valueOf(i2)) != null) {
                i3 = a(this.n.get(Integer.valueOf(i2)));
            } else {
                i3 = 0;
            }
            return new String[]{this.g.getResources().getString(R.string.app_update_group_other), "(" + i3 + ")"};
        }
    }

    private int a(List<SimpleAppModel> list) {
        List<SimpleAppModel> a2;
        int i2;
        int i3 = 0;
        if (list != null && !list.isEmpty() && (a2 = k.a(this.b)) != null && !a2.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            for (SimpleAppModel simpleAppModel : a2) {
                arrayList.add(Integer.valueOf(simpleAppModel.c.hashCode()));
            }
            for (SimpleAppModel simpleAppModel2 : list) {
                if (arrayList.contains(Integer.valueOf(simpleAppModel2.c.hashCode()))) {
                    i2 = i3 + 1;
                } else {
                    i2 = i3;
                }
                i3 = i2;
            }
        }
        return i3;
    }

    public View getChildView(int i2, int i3, boolean z2, View view, ViewGroup viewGroup) {
        SimpleAppModel simpleAppModel = (SimpleAppModel) getChild(i2, i3);
        Pair<View, b> a2 = a.a(this.g, this.h, view, 1);
        b bVar = (b) a2.second;
        View view2 = (View) a2.first;
        STInfoV2 b2 = b(simpleAppModel, i2, i3);
        if (simpleAppModel != null) {
            a(bVar, simpleAppModel, i2, i3, b2);
            bVar.n.f = simpleAppModel.f938a + Constants.STR_EMPTY;
            a(simpleAppModel, bVar);
        }
        this.e = i3;
        return view2;
    }

    private String a(int i2, int i3) {
        if (c(i2) == 1) {
            return com.tencent.assistantv2.st.page.a.a("04", i3);
        }
        return com.tencent.assistantv2.st.page.a.a("05", i3);
    }

    private void a(b bVar, SimpleAppModel simpleAppModel, int i2, int i3, STInfoV2 sTInfoV2) {
        if (bVar != null && simpleAppModel != null) {
            bVar.f2211a.setOnClickListener(new k(this, simpleAppModel, sTInfoV2));
            a.a(this.g, bVar, simpleAppModel, sTInfoV2);
            AppConst.AppState d2 = k.d(simpleAppModel);
            if (d2 == AppConst.AppState.INSTALLED || this.b) {
                bVar.l.setVisibility(8);
            } else {
                bVar.l.setVisibility(0);
                bVar.l.setOnClickListener(new l(this, simpleAppModel, i2, i3, sTInfoV2));
            }
            if (this.b && !bVar.f.f1926a) {
                bVar.f.a();
            } else if (!this.b && bVar.f.f1926a) {
                bVar.f.b();
            }
            boolean a2 = com.tencent.pangu.module.wisedownload.j.a(simpleAppModel.c);
            if (d2 == AppConst.AppState.QUEUING || d2 == AppConst.AppState.DOWNLOADING || d2 == AppConst.AppState.INSTALLING || a2) {
                this.c.put(simpleAppModel.c.hashCode(), simpleAppModel.g);
            }
            String str = simpleAppModel.o;
            if (TextUtils.isEmpty(str)) {
                bVar.i.setVisibility(8);
            } else {
                String format = String.format(this.g.getResources().getString(R.string.update_feature), "\n" + str);
                bVar.k.setText(format);
                String replace = format.replaceAll("\r\n", "\n").replaceAll("\n\n", "\n").replace("\n", Constants.STR_EMPTY);
                bVar.i.setVisibility(0);
                bVar.i.setOnClickListener(new m(this, simpleAppModel, bVar, sTInfoV2));
                bVar.j.setTag(Long.valueOf(simpleAppModel.f938a));
                bVar.j.a(new n(this, bVar, simpleAppModel.f938a));
                bVar.j.setText(replace);
                if (this.q == null || !this.q.equals(simpleAppModel.c)) {
                    bVar.j.setVisibility(0);
                    bVar.k.setVisibility(8);
                    bVar.m.setImageResource(R.drawable.icon_open);
                } else {
                    bVar.k.setVisibility(0);
                    bVar.j.setVisibility(8);
                    bVar.m.setImageResource(R.drawable.icon_close);
                    notifyDataSetChanged();
                }
            }
            bVar.f.a(sTInfoV2, new o(this, simpleAppModel, i2, i3), (d) null, bVar.f, bVar.g);
        }
    }

    /* access modifiers changed from: private */
    public void a(SimpleAppModel simpleAppModel, View view) {
        boolean j2 = simpleAppModel.j();
        if (f == null) {
            f = new a(view.getContext(), ((TXExpandableListView) this.f2216a).getContentView());
            r.a().register(f);
        } else {
            f.a(view.getContext(), ((TXExpandableListView) this.f2216a).getContentView());
        }
        com.tencent.cloud.e.a.a(view.getContext(), simpleAppModel, ((TXExpandableListView) this.f2216a).getContentView(), (View) view.getParent(), !j2, this.y);
    }

    /* access modifiers changed from: private */
    public void b(SimpleAppModel simpleAppModel) {
        if (this.p != null && simpleAppModel != null && simpleAppModel.c != null && this.p.indexOfKey(simpleAppModel.c.hashCode()) > 0) {
            this.p.clear();
        }
    }

    /* access modifiers changed from: private */
    public void c(SimpleAppModel simpleAppModel) {
        StatUpdateManageAction statUpdateManageAction = this.r;
        statUpdateManageAction.e = (short) (statUpdateManageAction.e + 1);
        Intent intent = new Intent(this.g, AppDetailActivityV5.class);
        intent.putExtra("simpleModeInfo", simpleAppModel);
        this.g.startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void a(SimpleAppModel simpleAppModel, int i2, int i3) {
        ArrayList arrayList;
        if (simpleAppModel != null) {
            StatUpdateManageAction statUpdateManageAction = this.r;
            statUpdateManageAction.d = (short) (statUpdateManageAction.d + 1);
            if (!j.b().a(simpleAppModel)) {
                ArrayList arrayList2 = this.n.get(getGroup(i2));
                if (arrayList2 != null && arrayList2.size() > i3) {
                    arrayList2.remove(i3);
                }
                if (getGroup(i2) != null && ((arrayList = this.n.get((Integer) getGroup(i2))) == null || arrayList.isEmpty())) {
                    this.m.remove(i2);
                }
            }
            b(simpleAppModel);
            this.o.remove(simpleAppModel);
            this.c.delete(simpleAppModel.c.hashCode());
            notifyDataSetChanged();
            if (m.a().ab()) {
                m.a().ac();
                p pVar = new p(this);
                pVar.titleRes = this.g.getResources().getString(R.string.ignore_update_tips_title);
                pVar.contentRes = this.g.getResources().getString(R.string.ignore_update_tips_content);
                DialogUtils.show1BtnDialog(this.u, pVar);
            }
        }
    }

    public void a(Activity activity) {
        this.u = activity;
    }

    public List<SimpleAppModel> c() {
        return this.o;
    }

    private STInfoV2 b(SimpleAppModel simpleAppModel, int i2, int i3) {
        if (simpleAppModel == null) {
            return null;
        }
        if (this.v == null) {
            this.v = new b();
        }
        AppConst.AppState d2 = k.d(simpleAppModel);
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.g, simpleAppModel, a(i2, i3), 100, com.tencent.assistantv2.st.page.a.b(d2));
        if (buildSTInfo != null) {
            buildSTInfo.updateWithExternalPara(this.t);
            buildSTInfo.pushInfo = this.s;
        }
        this.v.exposure(buildSTInfo);
        return buildSTInfo;
    }

    public void d() {
        this.b = true;
    }

    public void e() {
        this.b = false;
    }

    /* access modifiers changed from: package-private */
    public void a(SimpleAppModel simpleAppModel, b bVar) {
        if (com.tencent.cloud.e.a.a(simpleAppModel.f938a + Constants.STR_EMPTY) == null || this.b) {
            com.tencent.cloud.e.a.a(bVar.n);
        } else {
            com.tencent.cloud.e.a.a(this.g, bVar.n, com.tencent.cloud.e.a.a(simpleAppModel.f938a + Constants.STR_EMPTY));
        }
    }
}
