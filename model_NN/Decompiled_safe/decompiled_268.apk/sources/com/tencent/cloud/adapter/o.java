package com.tencent.cloud.adapter;

import android.view.View;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.component.k;
import com.tencent.cloud.e.a;
import com.tencent.cloud.updaterec.f;
import java.util.ArrayList;

/* compiled from: ProGuard */
class o extends k {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f2224a;
    final /* synthetic */ int b;
    final /* synthetic */ int c;
    final /* synthetic */ g d;

    o(g gVar, SimpleAppModel simpleAppModel, int i, int i2) {
        this.d = gVar;
        this.f2224a = simpleAppModel;
        this.b = i;
        this.c = i2;
    }

    public void a(View view) {
        this.d.b(this.f2224a);
    }

    public void a(View view, AppConst.AppState appState) {
        f a2;
        if ((appState == AppConst.AppState.DOWNLOAD || appState == AppConst.AppState.UPDATE || appState == AppConst.AppState.PAUSED) && (a2 = a.a(((View) view.getParent()).getTag())) != null && a2.e != 2 && !this.d.b) {
            this.d.a(this.f2224a, view);
        }
        if (appState == AppConst.AppState.DOWNLOAD || appState == AppConst.AppState.UPDATE || appState == AppConst.AppState.DOWNLOADING || appState == AppConst.AppState.QUEUING || appState == AppConst.AppState.PAUSED || appState == AppConst.AppState.DOWNLOADED) {
            this.d.c.put(this.f2224a.c.hashCode(), this.f2224a.g);
        } else if (appState == AppConst.AppState.INSTALLED) {
            this.d.o.remove(this.f2224a);
            this.d.c.delete(this.f2224a.c.hashCode());
            ArrayList arrayList = (ArrayList) this.d.n.get(this.d.getGroup(this.b));
            if (arrayList != null && arrayList.size() > this.c) {
                arrayList.remove(this.c);
            }
            if (this.d.getGroup(this.b) != null) {
                ArrayList arrayList2 = (ArrayList) this.d.n.get((Integer) this.d.getGroup(this.b));
                if (arrayList2 == null || arrayList2.isEmpty()) {
                    this.d.m.remove(this.b);
                }
            }
            this.d.notifyDataSetChanged();
        } else if (appState == AppConst.AppState.INSTALLING) {
            this.d.notifyDataSetChanged();
        }
    }

    public int a() {
        return 1;
    }
}
