package com.tencent.cloud.adapter;

import android.content.Context;
import android.view.View;
import com.tencent.assistant.model.b;
import com.tencent.assistantv2.adapter.smartlist.SmartListAdapter;

/* compiled from: ProGuard */
public class SoftwareListPageAdapter extends SmartListAdapter {
    public SoftwareListPageAdapter(Context context, View view, b bVar) {
        super(context, view, bVar);
    }

    /* access modifiers changed from: protected */
    public SmartListAdapter.SmartListType o() {
        return SmartListAdapter.SmartListType.AppPage;
    }

    /* access modifiers changed from: protected */
    public String b(int i) {
        return "07";
    }
}
