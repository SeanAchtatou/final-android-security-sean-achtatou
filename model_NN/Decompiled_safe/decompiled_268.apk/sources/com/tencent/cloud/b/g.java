package com.tencent.cloud.b;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.i;
import com.tencent.assistant.model.QuickEntranceNotify;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.b;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.module.ag;
import com.tencent.assistant.module.ak;
import com.tencent.assistant.module.callback.a;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.CftGetAppListRequest;
import com.tencent.assistant.protocol.jce.CftGetAppListResponse;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class g extends BaseEngine<a> implements ak {

    /* renamed from: a  reason: collision with root package name */
    public long f2239a;
    public int b;
    private short c;
    /* access modifiers changed from: private */
    public boolean d;
    /* access modifiers changed from: private */
    public long e;
    private List<SimpleAppModel> f;
    private byte[] g;
    private boolean h;
    private int i;
    private int j;
    private List<SimpleAppModel> k;
    private byte[] l;
    private boolean m;
    private m n;
    private b o;
    /* access modifiers changed from: private */
    public int p;

    public g(long j2, int i2) {
        this.d = false;
        this.e = -1;
        this.h = true;
        this.i = -1;
        this.j = -1;
        this.k = new ArrayList();
        this.m = true;
        this.o = new b();
        this.p = 0;
        this.f2239a = j2;
        this.b = i2;
        this.d = true;
        ag.b().a(this);
        this.n = new m(this);
    }

    public g(long j2, int i2, short s) {
        this(j2, i2);
        this.c = s;
    }

    public b a() {
        this.o.b(this.e);
        this.o.b(f());
        return this.o;
    }

    public CftGetAppListResponse b() {
        return i.y().b(this.f2239a, this.b, null);
    }

    public void c() {
        TemporaryThreadManager.get().start(new h(this));
    }

    /* access modifiers changed from: private */
    public boolean h() {
        CftGetAppListResponse b2;
        boolean z = false;
        if (!this.d || (b2 = b()) == null) {
            return false;
        }
        long a2 = m.a().a((byte) 5);
        if (a2 != -11 && b2.e != a2) {
            if (b2.e != a2) {
            }
            return false;
        } else if (b2.b == null || b2.b.size() <= 0) {
            return false;
        } else {
            this.e = b2.e;
            this.o.b(this.e);
            if (b2.d == 1) {
                z = true;
            }
            this.h = z;
            this.g = b2.c;
            this.f = k.b(b2.b);
            this.k.clear();
            this.k.addAll(this.f);
            this.p = this.k.get(this.k.size() - 1).al;
            this.m = this.h;
            this.l = this.g;
            ArrayList arrayList = new ArrayList(this.f);
            if (this.f != null && this.f.size() > 0) {
                notifyDataChangedInMainThread(new i(this, arrayList));
                if (this.m) {
                    this.n.a(this.l);
                }
            }
            return true;
        }
    }

    public void onLocalDataHasUpdate() {
        if (this.d) {
            if (this.e != m.a().a((byte) 5)) {
                d();
            }
        }
    }

    public void onPromptHasNewNotify(ArrayList<QuickEntranceNotify> arrayList) {
    }

    public int d() {
        if (this.i > 0) {
            cancel(this.i);
        }
        this.p = 0;
        this.i = b((byte[]) null);
        return this.i;
    }

    /* access modifiers changed from: private */
    public int a(byte[] bArr) {
        if (this.j > 0) {
            cancel(this.j);
        }
        this.j = b(bArr);
        return this.j;
    }

    public int e() {
        if (this.l == null || this.l.length == 0) {
            return -1;
        }
        if (this.n.c()) {
            int a2 = this.n.a();
            this.n.b();
            return a2;
        } else if (this.l != this.n.d() || this.n.e() == null) {
            return a(this.l);
        } else {
            m j2 = this.n.clone();
            boolean z = this.e != j2.h();
            this.e = j2.h();
            if (this.e != m.a().a((byte) 5)) {
                return a(this.l);
            }
            int a3 = j2.a();
            this.o.b(this.e);
            if (z) {
                this.f = j2.e();
                this.g = j2.g();
                this.h = j2.f();
                this.k.clear();
            }
            ArrayList arrayList = new ArrayList(j2.e());
            this.k.addAll(arrayList);
            if (this.k.size() > 0) {
                this.p = this.k.get(this.k.size() - 1).al;
            }
            this.m = j2.f();
            this.l = j2.g();
            XLog.d("voken", "getNextPage mNextPageContext = " + this.l);
            notifyDataChangedInMainThread(new j(this, a3, z, arrayList));
            if (this.n.e) {
                this.n.a(this.n.g());
            }
            return this.n.a();
        }
    }

    private int b(byte[] bArr) {
        return a(-1, bArr);
    }

    /* access modifiers changed from: private */
    public int a(int i2, byte[] bArr) {
        CftGetAppListRequest cftGetAppListRequest = new CftGetAppListRequest();
        cftGetAppListRequest.f1186a = this.f2239a;
        cftGetAppListRequest.b = this.b;
        cftGetAppListRequest.c = this.c > 0 ? this.c : 30;
        cftGetAppListRequest.e = this.p;
        XLog.d("voken", "sendRequest mNextPageContext = " + this.l);
        if (bArr == null) {
            bArr = new byte[0];
        }
        cftGetAppListRequest.d = bArr;
        return send(i2, cftGetAppListRequest);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        boolean z = true;
        if (jceStruct2 != null) {
            CftGetAppListResponse cftGetAppListResponse = (CftGetAppListResponse) jceStruct2;
            CftGetAppListRequest cftGetAppListRequest = (CftGetAppListRequest) jceStruct;
            ArrayList<SimpleAppModel> b2 = k.b(cftGetAppListResponse.b);
            if (i2 == this.n.a()) {
                m mVar = this.n;
                long j2 = cftGetAppListResponse.e;
                if (cftGetAppListResponse.d != 1) {
                    z = false;
                }
                mVar.a(j2, b2, z, cftGetAppListResponse.c);
            } else if (this.l != cftGetAppListResponse.c) {
                boolean z2 = cftGetAppListResponse.e != this.e || cftGetAppListRequest.d == null || cftGetAppListRequest.d.length == 0;
                if (cftGetAppListResponse.d != 1) {
                    z = false;
                }
                byte[] bArr = cftGetAppListResponse.c;
                this.e = cftGetAppListResponse.e;
                this.o.b(this.e);
                if (z2) {
                    this.f = b2;
                    this.g = bArr;
                    this.h = z;
                    this.k.clear();
                }
                this.k.addAll(b2);
                if (this.k.size() > 0) {
                    this.p = this.k.get(this.k.size() - 1).al;
                    XLog.d("voken", "onRequestSuccessed pageContext = " + bArr);
                }
                this.m = z;
                this.l = bArr;
                notifyDataChangedInMainThread(new k(this, i2, z2, b2, cftGetAppListResponse));
                if (this.m) {
                    this.n.a(this.l);
                }
            }
            if (this.d) {
                i.y().a(this.f2239a, this.b, cftGetAppListRequest.d, cftGetAppListResponse);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i2, int i3, JceStruct jceStruct, JceStruct jceStruct2) {
        if (i2 != this.n.a()) {
            CftGetAppListRequest cftGetAppListRequest = (CftGetAppListRequest) jceStruct;
            notifyDataChangedInMainThread(new l(this, i2, i3, cftGetAppListRequest.d == null || cftGetAppListRequest.d.length == 0));
            return;
        }
        this.n.i();
    }

    public List<SimpleAppModel> f() {
        return this.k;
    }

    public boolean g() {
        return this.m;
    }

    public String toString() {
        return "categoryId:" + this.f2239a + " sortId:" + this.b;
    }
}
