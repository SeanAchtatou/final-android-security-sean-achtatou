package com.tencent.cloud.updaterec;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.protocol.jce.CardItem;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class g extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CardItem f2338a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ UpdateRecOneMoreListView c;

    g(UpdateRecOneMoreListView updateRecOneMoreListView, CardItem cardItem, STInfoV2 sTInfoV2) {
        this.c = updateRecOneMoreListView;
        this.f2338a = cardItem;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        b.b(this.c.getContext(), this.f2338a.z.f1480a);
    }

    public STInfoV2 getStInfo() {
        if (this.b != null) {
            this.b.actionId = 200;
            this.b.status = "01";
        }
        return this.b;
    }
}
