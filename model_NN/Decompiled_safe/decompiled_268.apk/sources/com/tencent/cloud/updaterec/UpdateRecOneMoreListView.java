package com.tencent.cloud.updaterec;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.CardItem;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.cloud.updaterec.UpdateRecListItemInfoView;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.component.appdetail.process.s;

/* compiled from: ProGuard */
public class UpdateRecOneMoreListView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    SimpleAppModel f2333a;
    j b = null;
    View c;
    int d = -1;
    private b e = new b();

    public UpdateRecOneMoreListView(Context context) {
        super(context);
    }

    public UpdateRecOneMoreListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public UpdateRecOneMoreListView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void a(CardItem cardItem, f fVar) {
        if (cardItem != null && fVar != null) {
            this.f2333a = k.a(cardItem);
            if (cardItem.f1183a == 4 || cardItem.f1183a == 5 || cardItem.f1183a == 6) {
                b(cardItem, fVar);
                this.d = cardItem.f1183a;
                return;
            }
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(getContext(), this.f2333a, a.a(Constants.VIA_REPORT_TYPE_WPA_STATE, 0), 100, null);
            if (buildSTInfo != null) {
                buildSTInfo.extraData = Constants.STR_EMPTY + fVar.f;
            }
            a();
            a(cardItem, fVar, buildSTInfo);
            this.d = cardItem.f1183a;
            this.e.exposure(buildSTInfo);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(CardItem cardItem, f fVar) {
        if (cardItem.z != null && !TextUtils.isEmpty(cardItem.z.b) && !TextUtils.isEmpty(cardItem.z.f1480a)) {
            String str = Constants.STR_EMPTY;
            switch (cardItem.f1183a) {
                case 4:
                    str = Constants.VIA_REPORT_TYPE_START_WAP;
                    break;
                case 5:
                    str = "18";
                    break;
                case 6:
                    str = "17";
                    break;
            }
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(getContext(), this.f2333a, a.a(str, 0), 100, null);
            if (buildSTInfo != null) {
                buildSTInfo.extraData = Constants.STR_EMPTY + fVar.f;
            }
            removeAllViews();
            this.c = LayoutInflater.from(getContext()).inflate((int) R.layout.updaterec_app_one_more_gift, this);
            if (cardItem.f1183a == 4) {
                ((ImageView) this.c.findViewById(R.id.gift_icon_img)).setImageResource(R.drawable.icon_updaterec_gift);
            } else {
                ((ImageView) this.c.findViewById(R.id.gift_icon_img)).setImageResource(R.drawable.icon_coupon);
            }
            ((TextView) this.c.findViewById(R.id.text_gift)).setText(cardItem.z.b);
            fVar.b.setBackgroundResource(R.drawable.bg_card_color_selector);
            fVar.b.setOnClickListener(new g(this, cardItem, buildSTInfo));
            this.e.exposure(buildSTInfo);
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        removeAllViews();
        this.c = LayoutInflater.from(getContext()).inflate((int) R.layout.updaterec_app_one_more_item, this);
        this.b = new j(this, null);
        this.b.f2341a = (TXAppIconView) this.c.findViewById(R.id.app_icon_img);
        this.b.b = (TextView) this.c.findViewById(R.id.app_name_txt);
        this.b.c = (DownloadButton) this.c.findViewById(R.id.state_app_btn);
        this.b.g = (UpdateRecListItemInfoView) this.c.findViewById(R.id.download_info);
        this.b.d = this.c.findViewById(R.id.app_updatesizeinfo);
        this.b.e = (TextView) this.c.findViewById(R.id.app_size_sumsize);
        this.b.f = (TextView) this.c.findViewById(R.id.app_score_truesize);
        this.c.setTag(this.b);
    }

    private void a(CardItem cardItem, f fVar, STInfoV2 sTInfoV2) {
        fVar.b.setOnClickListener(new h(this, sTInfoV2));
        if (this.f2333a != null) {
            this.b.b.setText(this.f2333a.d);
            this.b.f2341a.updateImageView(this.f2333a.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        }
        this.b.c.a(this.f2333a);
        this.b.g.a(UpdateRecListItemInfoView.InfoType.ONEMORE_DESC);
        this.b.g.a(this.f2333a);
        if (s.a(this.f2333a)) {
            this.b.c.setClickable(false);
            return;
        }
        this.b.c.setClickable(true);
        this.b.c.a(sTInfoV2, new i(this));
    }
}
