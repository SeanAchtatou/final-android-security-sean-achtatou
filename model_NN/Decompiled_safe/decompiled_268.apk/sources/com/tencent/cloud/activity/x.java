package com.tencent.cloud.activity;

import android.content.Intent;
import android.os.Build;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.activity.MainActivity;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class x extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UpdateListActivity f2205a;

    x(UpdateListActivity updateListActivity) {
        this.f2205a = updateListActivity;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.f2205a.n, MainActivity.class);
        if (Build.VERSION.SDK_INT >= 11) {
            intent.addFlags(32768);
        }
        this.f2205a.n.startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        return this.f2205a.b(a.a(Constants.VIA_REPORT_TYPE_SHARE_TO_QQ, "001"));
    }
}
