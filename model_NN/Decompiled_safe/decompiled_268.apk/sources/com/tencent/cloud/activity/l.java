package com.tencent.cloud.activity;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.protocol.jce.TagGroup;
import com.tencent.assistantv2.st.model.STCommonInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class l extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CategoryDetailActivity f2195a;

    l(CategoryDetailActivity categoryDetailActivity) {
        this.f2195a = categoryDetailActivity;
    }

    public void onTMAClick(View view) {
        TagGroup tagGroup = (TagGroup) view.getTag();
        if (tagGroup != null) {
            long unused = this.f2195a.A = tagGroup.a();
            this.f2195a.w.a(this.f2195a.f(), this.f2195a.y, this.f2195a.A);
            if (this.f2195a.v != null) {
                this.f2195a.v.j();
                this.f2195a.v.a(this.f2195a.A);
            }
        }
    }

    public STInfoV2 getStInfo(View view) {
        int i;
        try {
            i = ((Integer) view.getTag(R.id.category_detail_btn_index)).intValue();
        } catch (Exception e) {
            e.printStackTrace();
            i = 0;
        }
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f2195a, 200);
        buildSTInfo.updateContentId(STCommonInfo.ContentIdType.CATEGORY, this.f2195a.y + "_" + this.f2195a.A);
        buildSTInfo.slotId = a.a("05", i);
        return buildSTInfo;
    }
}
