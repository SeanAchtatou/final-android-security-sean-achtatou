package com.tencent.cloud.activity;

import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.manager.webview.component.TxWebViewContainer;
import com.tencent.assistant.manager.webview.component.b;
import com.tencent.assistant.manager.webview.js.l;
import com.tencent.assistant.net.c;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class EBookTabActivity extends RelativeLayout {
    private static ArrayList<String> i = new ArrayList<>();

    /* renamed from: a  reason: collision with root package name */
    private String f2163a = "EBOOK";
    private LayoutInflater b;
    private BaseActivity c;
    private TxWebViewContainer d;
    private String e;
    private String f = "/qqdownloader/3";
    private volatile boolean g = true;
    private int h = 0;

    static {
        i.add("HUAWEI_HUAWEI U9508");
    }

    public EBookTabActivity(BaseActivity baseActivity, String str) {
        super(baseActivity);
        this.c = baseActivity;
        this.e = str;
        d();
    }

    private void d() {
        XLog.i(this.f2163a, ">>init>>");
        this.b = LayoutInflater.from(this.c);
        this.b.inflate((int) R.layout.act6, this);
        if (!TextUtils.isEmpty(this.e)) {
            if (this.e.contains("accelerate=1")) {
                this.h = 1;
            }
            if (this.e.contains("accelerate=0")) {
                this.h = 2;
            }
        }
        e();
        b bVar = new b();
        bVar.b = this.h;
        if (!c.a()) {
            bVar.d = 1;
        } else {
            bVar.d = -1;
        }
        this.d.a(bVar);
        a(0, this.e);
    }

    private String a(int i2, String str) {
        String str2 = a(i2, 0) ? "ALL" : "NONE";
        if (!str2.equals("NONE") || TextUtils.isEmpty(str) || !l.a(Uri.parse(str).getHost())) {
            return str2;
        }
        return "ALL";
    }

    private boolean a(int i2, int i3) {
        return ((i2 >>> i3) & 1) == 1;
    }

    private void e() {
        this.d = (TxWebViewContainer) findViewById(R.id.webviewcontainer);
        this.d.n();
    }

    /* access modifiers changed from: protected */
    public void a() {
        XLog.i(this.f2163a, ">>onResume>>");
        l.a(this.c, this.e, "ALL");
        if (this.d != null) {
            this.d.a();
            if (this.g) {
                XLog.d("EBookTabActivity", "ebook url:" + this.e);
                this.d.a(this.e);
                this.g = false;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        if (this.d != null) {
            this.d.b();
        }
    }

    public void a(boolean z) {
        if (this.d != null) {
        }
    }

    public int c() {
        return STConst.ST_PAGE_EBOOK;
    }
}
