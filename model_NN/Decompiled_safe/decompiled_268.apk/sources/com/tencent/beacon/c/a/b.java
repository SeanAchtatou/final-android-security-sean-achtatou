package com.tencent.beacon.c.a;

import com.tencent.beacon.e.a;
import com.tencent.beacon.e.c;
import com.tencent.beacon.e.d;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public final class b extends c {
    private static byte[] m;

    /* renamed from: a  reason: collision with root package name */
    public byte f2111a = 0;
    public String b = Constants.STR_EMPTY;
    public String c = Constants.STR_EMPTY;
    public String d = Constants.STR_EMPTY;
    public String e = Constants.STR_EMPTY;
    public int f = 0;
    public byte[] g = null;
    public byte h = 0;
    public byte i = 0;
    public String j = Constants.STR_EMPTY;
    public String k = Constants.STR_EMPTY;
    public String l = Constants.STR_EMPTY;

    public final void a(d dVar) {
        dVar.a(this.f2111a, 0);
        dVar.a(this.b, 1);
        dVar.a(this.c, 2);
        dVar.a(this.d, 3);
        dVar.a(this.e, 4);
        dVar.a(this.f, 5);
        dVar.a(this.g, 6);
        dVar.a(this.h, 7);
        dVar.a(this.i, 8);
        if (this.j != null) {
            dVar.a(this.j, 9);
        }
        if (this.k != null) {
            dVar.a(this.k, 10);
        }
        if (this.l != null) {
            dVar.a(this.l, 11);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(int, int, boolean):int
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(byte, int, boolean):byte */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(int, int, boolean):int */
    public final void a(a aVar) {
        this.f2111a = aVar.a(this.f2111a, 0, true);
        this.b = aVar.b(1, true);
        this.c = aVar.b(2, true);
        this.d = aVar.b(3, true);
        this.e = aVar.b(4, true);
        this.f = aVar.a(this.f, 5, true);
        if (m == null) {
            byte[] bArr = new byte[1];
            m = bArr;
            bArr[0] = 0;
        }
        byte[] bArr2 = m;
        this.g = aVar.c(6, true);
        this.h = aVar.a(this.h, 7, true);
        this.i = aVar.a(this.i, 8, true);
        this.j = aVar.b(9, false);
        this.k = aVar.b(10, false);
        this.l = aVar.b(11, false);
    }
}
