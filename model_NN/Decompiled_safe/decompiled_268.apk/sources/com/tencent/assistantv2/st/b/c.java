package com.tencent.assistantv2.st.b;

import android.text.TextUtils;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.Arrays;

/* compiled from: ProGuard */
class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ STInfoV2 f2022a;
    final /* synthetic */ b b;

    c(b bVar, STInfoV2 sTInfoV2) {
        this.b = bVar;
        this.f2022a = sTInfoV2;
    }

    public void run() {
        String a2 = this.b.a(this.f2022a.appId, this.f2022a.scene, this.f2022a.slotId);
        if (!TextUtils.isEmpty(a2)) {
            STInfoV2 sTInfoV2 = this.b.f2021a.get(a2);
            if (sTInfoV2 == null || !Arrays.equals(sTInfoV2.recommendId, this.f2022a.recommendId)) {
                this.f2022a.hasExposure = true;
                this.f2022a.latestExposureTime = System.currentTimeMillis();
            } else {
                long currentTimeMillis = System.currentTimeMillis() - sTInfoV2.latestExposureTime;
                if (!sTInfoV2.hasExposure || currentTimeMillis >= 1200000) {
                    this.f2022a.hasExposure = true;
                    this.f2022a.latestExposureTime = System.currentTimeMillis();
                } else {
                    return;
                }
            }
            this.b.f2021a.put(a2, this.f2022a);
            l.a(this.f2022a);
        }
    }
}
