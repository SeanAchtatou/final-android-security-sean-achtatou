package com.tencent.assistantv2.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.tencent.assistant.manager.notification.a.a.c;
import com.tencent.assistant.manager.notification.a.a.g;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.adapter.smartlist.SmartListAdapter;
import com.tencent.pangu.component.banner.f;
import java.util.List;

/* compiled from: ProGuard */
public class QuickBannerView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    protected List<f> f1939a;
    protected List<ColorCardItem> b;
    protected long c;
    protected int d;
    protected int e;
    private g f;

    public QuickBannerView(Context context) {
        this(context, null, 0);
    }

    public QuickBannerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public String a() {
        return "06";
    }

    public QuickBannerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        this.e = 4;
        this.f = new g();
        this.d = i;
        if (i == SmartListAdapter.SmartListType.AppPage.ordinal() || i == SmartListAdapter.SmartListType.GamePage.ordinal()) {
            this.e = 5;
        }
        int a2 = by.a(getContext(), 5.0f);
        setPadding(a2, a2, a2, by.a(getContext(), 1.0f));
        this.f.a(new au(this));
        b();
    }

    /* access modifiers changed from: protected */
    public void b() {
        LinearLayout linearLayout;
        int i;
        if (this.f1939a != null && this.f1939a.size() != 0) {
            removeAllViews();
            this.f.a();
            setOrientation(1);
            LinearLayout c2 = c();
            addView(c2);
            LinearLayout linearLayout2 = c2;
            int i2 = 0;
            int i3 = 0;
            for (f next : this.f1939a) {
                if (i3 >= this.e) {
                    linearLayout = c();
                    addView(linearLayout);
                    i = 0;
                } else {
                    linearLayout = linearLayout2;
                    i = i3;
                }
                next.a(a());
                View b2 = next.b(getContext(), this, this.d, this.c, i2);
                if (b2 != null) {
                    XLog.d("banner", "**** 下发的数据 bgImgUrl=" + next.f3641a.g + " hide=" + ((int) next.f3641a.h));
                    a(next);
                    ViewGroup.LayoutParams layoutParams = b2.getLayoutParams();
                    if (layoutParams == null) {
                        layoutParams = new LinearLayout.LayoutParams(-2, -2);
                    }
                    if (layoutParams instanceof LinearLayout.LayoutParams) {
                        ((LinearLayout.LayoutParams) layoutParams).weight = (float) next.a();
                    }
                    b2.setLayoutParams(layoutParams);
                    linearLayout.addView(b2);
                }
                i2++;
                linearLayout2 = linearLayout;
                i3 = next.a() + i;
            }
            this.f.b();
        }
    }

    /* access modifiers changed from: protected */
    public LinearLayout c() {
        LinearLayout linearLayout = new LinearLayout(getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, d());
        linearLayout.setGravity(17);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(0);
        return linearLayout;
    }

    public void a(long j, List<f> list, List<ColorCardItem> list2) {
        this.c = j;
        this.f1939a = list;
        this.b = list2;
        b();
    }

    public int d() {
        return by.a(getContext(), 79.0f);
    }

    public void a(f fVar) {
        c b2;
        if (fVar != null && (b2 = fVar.b()) != null) {
            this.f.a(b2);
        }
    }

    public List<ColorCardItem> e() {
        return this.b;
    }

    public List<f> f() {
        return this.f1939a;
    }
}
