package com.tencent.assistantv2.component;

import android.view.animation.Animation;
import android.view.animation.Transformation;

/* compiled from: ProGuard */
public class aq extends Animation {

    /* renamed from: a  reason: collision with root package name */
    private float f1964a;
    private float b;
    private ar c;

    public aq(float f, float f2, ar arVar) {
        this.f1964a = f;
        this.b = f2;
        this.c = arVar;
    }

    /* access modifiers changed from: protected */
    public void applyTransformation(float f, Transformation transformation) {
        super.applyTransformation(f, transformation);
        if (this.c != null) {
            this.c.a(this.f1964a + ((this.b - this.f1964a) * f));
            if (f == 1.0f) {
                this.c.a();
            }
        }
    }

    public void a(float f, float f2) {
        this.f1964a = f;
        this.b = f2;
    }
}
