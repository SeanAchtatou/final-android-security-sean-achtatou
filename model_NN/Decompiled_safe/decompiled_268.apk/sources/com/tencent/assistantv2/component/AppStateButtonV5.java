package com.tencent.assistantv2.component;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.manager.a;
import com.tencent.assistant.manager.b;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.utils.ah;
import com.tencent.pangu.component.appdetail.ExchangeColorTextView;
import com.tencent.pangu.component.appdetail.process.s;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;

/* compiled from: ProGuard */
public class AppStateButtonV5 extends RelativeLayout implements b {

    /* renamed from: a  reason: collision with root package name */
    ImageView f1923a;
    private Context b;
    private LayoutInflater c;
    private SimpleAppModel d;
    private DownloadInfo e;
    private ProgressBar f;
    private Button g;
    private TextView h;
    private boolean i;
    private boolean j;
    private ExchangeColorTextView k;

    public AppStateButtonV5(Context context) {
        this(context, null);
    }

    public AppStateButtonV5(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.i = false;
        this.j = false;
        this.b = context;
        this.c = LayoutInflater.from(context);
        a();
    }

    private void a() {
        this.c.inflate((int) R.layout.component_appbutton_v5, this);
        this.g = (Button) findViewById(R.id.state_btn);
        this.f = (ProgressBar) findViewById(R.id.appdownload_progress_bar);
        this.h = (TextView) findViewById(R.id.app_floating_txt);
        this.k = (ExchangeColorTextView) findViewById(R.id.app_floating_txt_b);
        this.f1923a = (ImageView) findViewById(R.id.download_load_view);
        this.f1923a.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.circle));
    }

    public void a(int i2) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, i2);
        this.g.setLayoutParams(layoutParams);
        this.f.setLayoutParams(layoutParams);
        this.h.setLayoutParams(layoutParams);
    }

    public void a(CharSequence charSequence) {
        this.h.setText(charSequence);
        this.k.setText(charSequence.toString());
    }

    public void b(int i2) {
        this.h.setTextSize(2, (float) i2);
    }

    public void a(SimpleAppModel simpleAppModel) {
        this.d = simpleAppModel;
        b();
        a.a().a(this.d.q(), this);
    }

    private void b() {
        AppConst.AppState a2;
        if (this.d != null) {
            a2 = k.d(this.d);
        } else {
            a2 = k.a(this.e, this.i, this.j);
        }
        a(c(), a2);
    }

    public void onAppStateChange(String str, AppConst.AppState appState) {
        String c2;
        if (!TextUtils.isEmpty(str) && (c2 = c()) != null && c2.equals(str)) {
            ah.a().post(new a(this, str, appState));
        }
    }

    private void c(int i2) {
        if (this.f != null) {
            this.f.setVisibility(i2);
        }
    }

    private void d(int i2) {
        if (this.g != null) {
            this.g.setVisibility(i2);
        }
    }

    private boolean a(DownloadInfo downloadInfo, AppConst.AppState appState) {
        return SimpleDownloadInfo.isProgressShowFake(downloadInfo, appState);
    }

    private void a(int i2, int i3) {
        if (this.f != null) {
            this.f.setProgress(i2);
            this.f.setSecondaryProgress(i3);
            invalidate();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState
     arg types: [com.tencent.pangu.download.DownloadInfo, int, boolean]
     candidates:
      com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, com.tencent.assistant.plugin.PluginDownloadInfo, com.tencent.assistant.plugin.PluginInfo):com.tencent.assistant.AppConst$AppState
      com.tencent.assistant.module.k.a(java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem>, com.tencent.assistant.module.m, int):java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>
      com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState */
    /* access modifiers changed from: private */
    public void a(String str, AppConst.AppState appState) {
        if (appState == null || appState == AppConst.AppState.ILLEGAL) {
            if (this.d != null) {
                appState = k.d(this.d);
            } else {
                appState = k.a(this.e, false, this.j);
            }
        }
        c(appState);
        b(str, appState);
        d(8);
        a(appState);
        b(appState);
    }

    private void c(AppConst.AppState appState) {
        if (s.b(this.d) || s.c(this.d)) {
            this.h.setTextColor(this.b.getResources().getColor(R.color.list_vie_number_txt_color));
        } else if (appState == AppConst.AppState.INSTALLED) {
            this.h.setTextColor(this.b.getResources().getColor(R.color.white));
        } else {
            this.h.setTextColor(this.b.getResources().getColor(R.color.state_normal));
        }
    }

    private void b(String str, AppConst.AppState appState) {
        int i2;
        if (appState != null) {
            if (s.b(this.d)) {
                this.h.setText((int) R.string.jionbeta);
            } else if (s.c(this.d)) {
                this.h.setText((int) R.string.jionfirstrelease);
            } else {
                DownloadInfo d2 = DownloadProxy.a().d(str);
                if (d2 == null || d2.response == null) {
                    i2 = 0;
                } else {
                    if (a(d2, appState)) {
                        i2 = d2.response.f;
                    } else {
                        i2 = SimpleDownloadInfo.getPercent(d2);
                    }
                    a(i2, 0);
                }
                switch (b.f1972a[appState.ordinal()]) {
                    case 1:
                        if (this.d != null && this.d.c()) {
                            a(i2, this.b.getResources().getString(R.string.jionfirstrelease));
                            return;
                        } else if (this.d == null || !this.d.h()) {
                            a(i2, this.b.getResources().getString(R.string.appbutton_download));
                            return;
                        } else {
                            a(i2, this.b.getResources().getString(R.string.jionbeta));
                            return;
                        }
                    case 2:
                        a(i2, this.b.getResources().getString(R.string.appbutton_update));
                        return;
                    case 3:
                        a(i2, this.e != null ? this.b.getResources().getString(R.string.downloading_display_pause) : i2 + "%");
                        return;
                    case 4:
                        a(i2, this.b.getResources().getString(R.string.queuing));
                        return;
                    case 5:
                    case 6:
                        a(i2, this.b.getResources().getString(R.string.appbutton_continuing));
                        return;
                    case 7:
                        a(i2, this.b.getResources().getString(R.string.appbutton_install));
                        return;
                    case 8:
                        a(i2, this.b.getResources().getString(R.string.appbutton_open));
                        return;
                    case 9:
                        a(i2, this.b.getResources().getString(R.string.not_support));
                        return;
                    case 10:
                        return;
                    case 11:
                        a(i2, this.b.getResources().getString(R.string.installing));
                        return;
                    case 12:
                        a(i2, this.b.getResources().getString(R.string.uninstalling));
                        return;
                    default:
                        a(i2, this.b.getResources().getString(R.string.appbutton_unknown));
                        return;
                }
            }
        }
    }

    private void a(int i2, String str) {
        if ((i2 <= 0 || i2 >= 100) && (i2 != 100 || str == null || (!str.equals(this.b.getResources().getString(R.string.appbutton_continuing)) && !str.equals("100%")))) {
            this.h.setVisibility(0);
            this.k.setVisibility(8);
            this.h.setText(str);
            return;
        }
        this.h.setVisibility(8);
        this.k.setVisibility(0);
        this.k.setText(str);
        this.k.setTextWhiteLenth(((float) i2) / 100.0f);
        this.k.invalidate();
    }

    public void a(AppConst.AppState appState) {
        if (appState != null) {
            if (s.b(this.d) || s.c(this.d)) {
                this.g.setBackgroundColor(this.b.getResources().getColor(17170445));
                this.g.setPadding(0, 0, 0, 0);
                return;
            }
            switch (b.f1972a[appState.ordinal()]) {
                case 1:
                case 2:
                    this.g.setBackgroundResource(R.drawable.appstate_btn_selector);
                    return;
                case 3:
                case 4:
                case 5:
                case 6:
                    this.g.setBackgroundResource(R.drawable.appstate_btn_selector);
                    return;
                case 7:
                    this.g.setBackgroundResource(R.drawable.appstate_btn_selector);
                    return;
                case 8:
                    this.g.setBackgroundResource(R.drawable.appstate_btn_installed_selector);
                    return;
                case 9:
                default:
                    this.g.setBackgroundResource(R.drawable.appstate_btn_selector);
                    return;
                case 10:
                case 12:
                    return;
                case 11:
                    this.g.setBackgroundResource(R.drawable.appstate_btn_selector);
                    return;
            }
        }
    }

    public void b(AppConst.AppState appState) {
        switch (b.f1972a[appState.ordinal()]) {
            case 3:
            case 5:
            case 6:
                if (this.e == null) {
                    c(0);
                    d(8);
                    return;
                }
                c(8);
                d(0);
                return;
            case 4:
            default:
                c(8);
                d(0);
                return;
        }
    }

    /* access modifiers changed from: private */
    public String c() {
        if (this.d != null) {
            return this.d.q();
        }
        if (this.e != null) {
            return this.e.downloadTicket;
        }
        return null;
    }
}
