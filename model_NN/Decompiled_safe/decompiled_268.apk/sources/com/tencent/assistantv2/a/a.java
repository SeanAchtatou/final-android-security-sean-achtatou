package com.tencent.assistantv2.a;

import android.util.SparseArray;
import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.m;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.protocol.jce.GetNavigationRequest;
import com.tencent.assistant.protocol.jce.GetNavigationResponse;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.an;
import java.util.Iterator;

/* compiled from: ProGuard */
public class a extends BaseEngine<com.tencent.assistantv2.model.a.a> {

    /* renamed from: a  reason: collision with root package name */
    private static a f1855a = null;
    private static SparseArray<h> b = new SparseArray<>(4);
    /* access modifiers changed from: private */
    public static final int[] c = {0, 1, 2};
    /* access modifiers changed from: private */
    public static String[] d;

    public static synchronized a a() {
        a aVar;
        synchronized (a.class) {
            if (f1855a == null) {
                f1855a = new a();
            }
            aVar = f1855a;
        }
        return aVar;
    }

    private a() {
        d = AstApp.i().getResources().getStringArray(R.array.navigation_default_items);
        a(0);
        ah.a().postDelayed(new b(this), 5000);
    }

    private h d(int i) {
        GetNavigationResponse getNavigationResponse;
        byte[] f = m.a().f(i);
        if (f != null && f.length > 0) {
            try {
                getNavigationResponse = (GetNavigationResponse) an.b(f, GetNavigationResponse.class);
            } catch (Exception e) {
                XLog.e("GetNavigationEngine", "response to navi object fail.typeid:" + i + ".ex:" + e);
            }
            return h.a(i, getNavigationResponse);
        }
        getNavigationResponse = null;
        return h.a(i, getNavigationResponse);
    }

    public synchronized h a(int i) {
        h hVar;
        hVar = b.get(i);
        if (hVar == null) {
            hVar = d(i);
            b.put(i, hVar);
        }
        return hVar;
    }

    public void b() {
        if (b != null && b.size() > 0) {
            b.clear();
        }
    }

    public void b(int i) {
        TemporaryThreadManager.get().start(new d(this, i));
    }

    public int c(int i) {
        return m.a().a("key_navi_reminder_t_" + i, 0);
    }

    /* access modifiers changed from: private */
    public int a(long j, int i) {
        GetNavigationRequest getNavigationRequest = new GetNavigationRequest();
        getNavigationRequest.f1308a = j;
        getNavigationRequest.b = i;
        return send(getNavigationRequest);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(int, byte[]):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        String str;
        int i2;
        GetNavigationResponse getNavigationResponse = (GetNavigationResponse) jceStruct2;
        GetNavigationRequest getNavigationRequest = (GetNavigationRequest) jceStruct;
        if (getNavigationResponse == null || getNavigationRequest == null || getNavigationResponse.b == getNavigationRequest.a()) {
            XLog.e("GetNavigationEngine", "GetNavigationEngine has null value.seq:" + i + ",request:" + jceStruct + ",response:" + getNavigationResponse);
            return;
        }
        int b2 = getNavigationRequest.b();
        h b3 = h.b(b2, getNavigationResponse);
        m.a().b("key_navi_reminder_t_" + b2, (Object) 0);
        if (b3 != null) {
            byte[] a2 = an.a(getNavigationResponse);
            if (a2 != null) {
                m.a().a(b2, a2);
            }
            h hVar = b.get(b2);
            if (hVar == null || hVar.b == null || hVar.b == null || hVar.b.size() <= 0) {
                notifyDataChangedInMainThread(new f(this, i, b2, b3));
                return;
            }
            Iterator<i> it = hVar.b.iterator();
            while (true) {
                if (!it.hasNext()) {
                    str = null;
                    i2 = 0;
                    break;
                }
                i next = it.next();
                if (next.c > 0) {
                    i2 = next.c;
                    String str2 = next.f1863a;
                    m.a().b("key_navi_reminder_t_" + b2, Integer.valueOf(i2));
                    str = str2;
                    break;
                }
            }
            for (i next2 : hVar.b) {
                if (next2.f1863a == null || !next2.f1863a.equals(str)) {
                    next2.c = 0;
                } else {
                    next2.c = i2;
                }
            }
            notifyDataChangedInMainThread(new e(this, i, b2, hVar));
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        XLog.e("GetNavigationEngine", "GetNavigationEngine error code:" + i2 + ".seq:" + i + ",request:" + jceStruct + ",response:" + jceStruct2);
    }
}
