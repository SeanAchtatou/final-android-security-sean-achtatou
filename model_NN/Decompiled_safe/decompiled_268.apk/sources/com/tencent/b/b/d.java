package com.tencent.b.b;

import android.util.Log;
import com.tencent.b.d.k;

final class d implements a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f2073a;

    d(a aVar) {
        this.f2073a = aVar;
    }

    public void a(int i, String str) {
        Log.e("MID", "failed to get mid, errorcode:" + i + " ,msg:" + str);
        this.f2073a.a(i, str);
    }

    public void a(Object obj) {
        if (obj != null) {
            b a2 = b.a(obj.toString());
            k.a("success to get mid:" + a2.c());
            this.f2073a.a(a2.c());
        }
    }
}
