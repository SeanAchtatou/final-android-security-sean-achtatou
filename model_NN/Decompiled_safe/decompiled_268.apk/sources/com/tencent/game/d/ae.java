package com.tencent.game.d;

import com.tencent.assistant.manager.i;
import com.tencent.assistant.protocol.jce.GftGetTreasureBoxSettingResponse;
import com.tencent.assistant.protocol.jce.NpcListCfg;
import com.tencent.assistant.protocol.jce.TreasureBoxCfg;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.an;

/* compiled from: ProGuard */
class ae implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GftGetTreasureBoxSettingResponse f2681a;
    final /* synthetic */ aa b;

    ae(aa aaVar, GftGetTreasureBoxSettingResponse gftGetTreasureBoxSettingResponse) {
        this.b = aaVar;
        this.f2681a = gftGetTreasureBoxSettingResponse;
    }

    public void run() {
        try {
            for (TreasureBoxCfg next : this.f2681a.a()) {
                XLog.v("cfg", "cfg.type : " + next.f1595a);
                switch (next.f1595a) {
                    case 1:
                        NpcListCfg npcListCfg = (NpcListCfg) an.b(next.b, NpcListCfg.class);
                        if (npcListCfg == null) {
                            break;
                        } else {
                            i.y().b(npcListCfg);
                            break;
                        }
                }
            }
        } catch (Exception e) {
            XLog.e("GAME_FT", getClass().getSimpleName() + "getTreasureBoxSettingResponse fail.");
        }
    }
}
