package com.tencent.game.d;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.manager.i;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.protocol.jce.GftAppGiftInfo;
import com.tencent.assistant.protocol.jce.GftGetMyDesktopRequest;
import com.tencent.assistant.protocol.jce.GftGetMyDesktopResponse;
import com.tencent.assistant.protocol.jce.GftMydesktopOtherAppInfo;
import com.tencent.assistant.protocol.jce.IsplayingInfo;
import com.tencent.assistant.protocol.jce.SimpleAppInfo;
import com.tencent.assistant.protocol.scu.RequestResponePair;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.game.d.a.b;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class p extends BaseEngine<b> {
    public int a() {
        return send(new GftGetMyDesktopRequest());
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        b(i, 0, jceStruct2);
        if (jceStruct2 != null) {
            TemporaryThreadManager.get().start(new q(this, jceStruct2));
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        b(i, i2, null);
    }

    public int b() {
        GftGetMyDesktopRequest gftGetMyDesktopRequest = new GftGetMyDesktopRequest();
        ArrayList arrayList = new ArrayList();
        arrayList.add(gftGetMyDesktopRequest);
        return send(arrayList);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, List<RequestResponePair> list) {
        GftGetMyDesktopResponse gftGetMyDesktopResponse;
        GftGetMyDesktopResponse gftGetMyDesktopResponse2 = null;
        for (RequestResponePair next : list) {
            if (next == null || !(next.request instanceof GftGetMyDesktopRequest)) {
                gftGetMyDesktopResponse = gftGetMyDesktopResponse2;
            } else {
                gftGetMyDesktopResponse = (GftGetMyDesktopResponse) next.response;
            }
            gftGetMyDesktopResponse2 = gftGetMyDesktopResponse;
        }
        a(i, 0, gftGetMyDesktopResponse2);
        if (gftGetMyDesktopResponse2 != null) {
            TemporaryThreadManager.get().start(new r(this, gftGetMyDesktopResponse2));
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, List<RequestResponePair> list) {
        for (RequestResponePair next : list) {
            if (next != null && (next.request instanceof GftGetMyDesktopRequest)) {
                a(i, i2, i.y().s());
            }
        }
    }

    private void a(int i, int i2, JceStruct jceStruct) {
        TemporaryThreadManager.get().start(new s(this, jceStruct));
    }

    public void c() {
        int i;
        GftGetMyDesktopResponse s = i.y().s();
        if (s == null) {
            i = -861;
        } else {
            i = 0;
        }
        b(0, i, s);
    }

    private void b(int i, int i2, JceStruct jceStruct) {
        ArrayList<SimpleAppInfo> arrayList;
        if (jceStruct == null) {
            notifyDataChangedInMainThread(new t(this, i, i2));
            return;
        }
        GftGetMyDesktopResponse gftGetMyDesktopResponse = (GftGetMyDesktopResponse) jceStruct;
        IsplayingInfo a2 = gftGetMyDesktopResponse.a();
        GftMydesktopOtherAppInfo gftMydesktopOtherAppInfo = gftGetMyDesktopResponse.c;
        GftAppGiftInfo b = gftGetMyDesktopResponse.b();
        if (a2 != null) {
            arrayList = a2.a();
        } else {
            arrayList = null;
        }
        notifyDataChangedInMainThread(new u(this, i, i2, arrayList, b, gftMydesktopOtherAppInfo));
    }
}
