package com.tencent.securemodule.impl;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcelable;
import android.os.Process;
import com.tencent.securemodule.service.ICallback;
import com.tencent.securemodule.service.IControlService;
import com.tencent.securemodule.ui.SecureEventReceiver;
import java.io.File;
import java.io.Serializable;

public class SecureService extends Service {

    /* renamed from: a  reason: collision with root package name */
    private Context f4022a;
    /* access modifiers changed from: private */
    public int b = 0;
    private boolean c = false;
    /* access modifiers changed from: private */
    public boolean d = false;
    /* access modifiers changed from: private */
    public boolean e = false;
    private boolean f = false;
    private a g = new a();

    public class a extends Binder implements IControlService {
        public a() {
        }

        public void doRemoteTask(String str, ICallback iCallback) {
            if (ay.a(SecureService.this.getApplicationContext(), "sm_mq")) {
                synchronized (this) {
                    new Thread(new ab(this, str, iCallback)).start();
                }
            }
        }

        public void setIsShowingTips(boolean z, boolean z2) {
            boolean unused = SecureService.this.d = z;
            if (z2) {
                SecureService.this.stopSelf();
                SecureService.this.c();
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x004c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int a(java.lang.String r6) {
        /*
            r5 = this;
            r2 = -6
            int r0 = r5.b
            int r0 = r0 + 1
            r5.b = r0
            r1 = -2
            if (r6 != 0) goto L_0x000c
            r1 = r2
        L_0x000b:
            return r1
        L_0x000c:
            com.qq.jce.wup.UniAttribute r0 = new com.qq.jce.wup.UniAttribute
            r0.<init>()
            java.lang.String r3 = "UTF-8"
            r0.setEncodeName(r3)
            byte[] r3 = defpackage.ay.b(r6)
            r0.decode(r3)
            java.lang.String r3 = "data"
            e r4 = new e
            r4.<init>()
            java.lang.Object r0 = r0.getByClass(r3, r4)
            e r0 = (defpackage.e) r0
            if (r0 == 0) goto L_0x0032
            java.util.ArrayList r3 = r0.c()
            if (r3 != 0) goto L_0x0034
        L_0x0032:
            r1 = r2
            goto L_0x000b
        L_0x0034:
            java.util.ArrayList r2 = r0.c()
            android.content.Context r3 = r5.f4022a
            android.content.Context r3 = r3.getApplicationContext()
            w r3 = defpackage.w.a(r3)
            java.util.Iterator r2 = r2.iterator()
        L_0x0046:
            boolean r4 = r2.hasNext()
            if (r4 == 0) goto L_0x0058
            java.lang.Object r1 = r2.next()
            c r1 = (defpackage.c) r1
            int r1 = r3.a(r0, r1)
            if (r1 == 0) goto L_0x0046
        L_0x0058:
            int r0 = r5.b
            int r0 = r0 + -1
            r5.b = r0
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.securemodule.impl.SecureService.a(java.lang.String):int");
    }

    /* access modifiers changed from: private */
    public void a() {
        if (!this.f) {
            this.b++;
            this.f = true;
            new v(this.f4022a).a(new aa(this));
            this.b--;
            this.f = false;
        }
    }

    public static void a(Context context, String str) {
        Intent intent = new Intent(context, SecureService.class);
        intent.setAction(str);
        context.startService(intent);
    }

    /* access modifiers changed from: private */
    public void a(String str, Parcelable parcelable) {
        if (this.e) {
            Intent intent = new Intent(str);
            if (parcelable != null) {
                intent.putExtra("data", parcelable);
            }
            sendBroadcast(intent);
            return;
        }
        Intent intent2 = new Intent();
        intent2.setClass(this, SecureEventReceiver.class);
        intent2.setAction(str);
        if (parcelable != null) {
            intent2.putExtra("data", parcelable);
        }
        sendBroadcast(intent2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: as.a(android.content.Context, int, boolean):boolean
     arg types: [android.content.Context, int, int]
     candidates:
      as.a(android.content.Context, int, int):int
      as.a(android.content.Context, int, java.lang.String):java.lang.String
      as.a(android.content.Context, int, boolean):boolean */
    /* access modifiers changed from: private */
    public void a(String str, Serializable serializable) {
        if (as.a(this.f4022a, 10002, true)) {
            Intent intent = new Intent();
            intent.setClass(this, SecureEventReceiver.class);
            intent.setAction(str);
            if (serializable != null) {
                intent.putExtra("data", serializable);
            }
            sendBroadcast(intent);
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        this.b++;
        w a2 = w.a(this.f4022a);
        a2.b();
        this.d = a2.a();
        this.b--;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ad.a(java.lang.String, boolean):int
     arg types: [java.lang.String, int]
     candidates:
      ac.a(int, android.os.Bundle):void
      ac.a(java.lang.String, int):void
      ad.a(java.lang.String, boolean):int */
    /* access modifiers changed from: private */
    public void b(String str) {
        int a2;
        if (!this.c) {
            this.c = true;
            this.b++;
            a("1000024", (Parcelable) null);
            ad adVar = new ad(this.f4022a);
            adVar.a(new z(this));
            adVar.a(1);
            do {
                a2 = adVar.a(str, false);
            } while (a2 == -7);
            if (a2 == 0) {
                String b2 = adVar.b();
                Bundle bundle = new Bundle();
                bundle.putString("key_path", b2);
                a("1000027", bundle);
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.addFlags(268435456);
                intent.setDataAndType(Uri.fromFile(new File(b2)), "application/vnd.android.package-archive");
                this.f4022a.startActivity(intent);
            }
            this.b--;
            this.c = false;
            this.e = false;
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
        Process.killProcess(Process.myPid());
        System.exit(0);
    }

    /* access modifiers changed from: private */
    public void d() {
        ag.a(this.f4022a).a(ar.b(this.f4022a));
    }

    public IBinder onBind(Intent intent) {
        return this.g;
    }

    public void onCreate() {
        super.onCreate();
        this.f4022a = getApplicationContext();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onStart(Intent intent, int i) {
        super.onStart(intent, i);
        if (intent != null && !"1000012".equals(intent.getAction()) && ay.a(getApplicationContext(), "sm_mq")) {
            synchronized (this) {
                new Thread(new y(this, intent)).start();
            }
        }
    }

    public boolean onUnbind(Intent intent) {
        if (this.b == 0) {
            stopSelf();
            c();
        }
        return super.onUnbind(intent);
    }
}
