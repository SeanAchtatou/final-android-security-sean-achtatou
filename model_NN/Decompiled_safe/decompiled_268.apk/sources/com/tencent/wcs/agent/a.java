package com.tencent.wcs.agent;

import android.text.TextUtils;
import android.util.SparseArray;
import com.qq.taf.jce.JceInputStream;
import com.tencent.wcs.b.c;
import com.tencent.wcs.f.d;
import com.tencent.wcs.jce.ServiceTransData;
import com.tencent.wcs.proxy.b;
import com.tencent.wcs.proxy.b.k;
import com.tencent.wcs.proxy.b.q;
import com.tencent.wcs.proxy.c.f;
import com.tencent.wcs.proxy.l;
import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/* compiled from: ProGuard */
public class a implements p, b {

    /* renamed from: a  reason: collision with root package name */
    private final com.tencent.wcs.agent.config.a f4044a;
    private final l b;
    private final com.tencent.wcs.a.b c;
    private d d;
    private com.tencent.wcs.e.a e;
    /* access modifiers changed from: private */
    public SparseArray<h> f;
    private LinkedBlockingQueue<f> g;
    private Thread h = null;
    private volatile boolean i = false;

    public a(l lVar, com.tencent.wcs.a.b bVar, com.tencent.wcs.agent.config.a aVar, com.tencent.wcs.e.a aVar2) {
        this.f4044a = aVar;
        this.b = lVar;
        this.c = bVar;
        this.e = aVar2;
        this.f = new SparseArray<>();
        this.g = new LinkedBlockingQueue<>();
    }

    public void a() {
        if (!this.i) {
            this.i = true;
            if (this.h == null || !this.h.isAlive()) {
                if (c.f4064a) {
                    com.tencent.wcs.c.b.a("AgentService startSessionResponseThread");
                }
                this.h = new b(this, "hanlder_sessionResponse_thread");
                this.h.start();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        while (this.i && this.g != null) {
            try {
                f poll = this.g.poll(150, TimeUnit.SECONDS);
                if (poll != null) {
                    b(poll);
                }
            } catch (InterruptedException e2) {
                e2.printStackTrace();
                return;
            }
        }
    }

    private void b(f fVar) {
        int i2 = 0;
        JceInputStream jceInputStream = new JceInputStream(fVar.c());
        ServiceTransData serviceTransData = (ServiceTransData) k.b().c((Object[]) new Void[0]);
        try {
            serviceTransData.readFrom(jceInputStream);
            q.b().b((Object) fVar);
            if (this.e == null || this.e.a() == null || !this.e.a().equals(serviceTransData.a())) {
                com.tencent.wcs.c.b.a("AgentService remoteConnect sessionKey: " + serviceTransData.a() + " ,but pushSessionKey: " + ((this.e == null || TextUtils.isEmpty(this.e.a())) ? "null" : this.e.a()));
                k.b().b((Object) serviceTransData);
            } else if (this.c == null || this.c.c() || this.f4044a.a(serviceTransData.b()) == 14092) {
                if (!(serviceTransData.c() == null || serviceTransData.c().length <= 0 || this.f4044a.a(serviceTransData.b()) == 14087)) {
                    try {
                        serviceTransData.a(com.qq.util.k.b("TencentMolo&&##%%!!!1234".getBytes("UTF-8"), serviceTransData.c()));
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
                if (serviceTransData.c() != null && serviceTransData.c().length > 0 && serviceTransData.g() == 1) {
                    serviceTransData.a(com.tencent.wcs.proxy.e.d.b(serviceTransData.c()));
                }
                a(serviceTransData);
            } else {
                if (c.f4064a) {
                    StringBuilder append = new StringBuilder().append("AgentService reject data caused by security, type ").append(serviceTransData.f()).append(" , at seq ").append(serviceTransData.e()).append(" channel id ").append(serviceTransData.b()).append(" message id ").append(serviceTransData.d()).append(" size ");
                    if (serviceTransData.c() != null) {
                        i2 = serviceTransData.c().length;
                    }
                    com.tencent.wcs.c.b.a(append.append(i2).append(" port ").append(this.f4044a.a(serviceTransData.b())).append(" remote ").append(this.f4044a.a()).toString());
                }
                if (this.b != null) {
                    this.b.k();
                }
                k.b().b((Object) serviceTransData);
            }
        } catch (Throwable th) {
            com.tencent.wcs.c.b.a("AgentService jceRead failed ,cmdid " + fVar.d());
            q.b().b((Object) fVar);
            k.b().b((Object) serviceTransData);
        }
    }

    private void a(ServiceTransData serviceTransData) {
        h hVar;
        if (c.f4064a) {
            com.tencent.wcs.c.b.a("AgentService receive data type " + serviceTransData.f() + " , at seq " + serviceTransData.e() + " channel id " + serviceTransData.b() + " message id " + serviceTransData.d() + " size " + (serviceTransData.c() != null ? serviceTransData.c().length : 0) + " remote " + this.f4044a.a());
        }
        if (this.d == null) {
            this.d = new d(this);
        }
        int d2 = serviceTransData.d();
        synchronized (this.f) {
            hVar = this.f.get(d2);
            if (hVar == null) {
                int f2 = serviceTransData.f();
                if (f2 == 0 || f2 == 1) {
                    h hVar2 = new h(serviceTransData.b(), serviceTransData.d(), serviceTransData.a(), this.b, this.f4044a);
                    hVar2.a(this);
                    hVar2.a();
                    this.f.put(d2, hVar2);
                    hVar = hVar2;
                } else if (!(f2 == 2 || f2 == 4 || f2 == 5 || f2 != 3)) {
                    new h(serviceTransData.b(), serviceTransData.d(), serviceTransData.a(), this.b, this.f4044a).a(null, serviceTransData.e(), 5);
                }
            }
        }
        if (hVar != null) {
            switch (serviceTransData.f()) {
                case 3:
                    hVar.a(serviceTransData.e(), serviceTransData.c());
                    k.b().b((Object) serviceTransData);
                    break;
                case 4:
                    hVar.a(serviceTransData.e());
                    k.b().b((Object) serviceTransData);
                    break;
                default:
                    hVar.a(serviceTransData);
                    break;
            }
            if (hVar.d() != 14089 && hVar.d() != 14087 && this.d != null) {
                this.d.a(d2);
            }
        }
    }

    public void a(int i2) {
        if (this.d != null) {
            this.d.b(i2);
        }
        com.tencent.wcs.proxy.e.a.a().a(new c(this, i2), 5000, TimeUnit.MILLISECONDS);
    }

    public void a(f fVar) {
        a();
        if (this.g != null) {
            try {
                this.g.put(fVar);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        }
    }

    public void c() {
        if (c.f4064a) {
            com.tencent.wcs.c.b.a("AgentService onRemoteDisconnect");
        }
        this.i = false;
        if (this.h != null) {
            this.h.interrupt();
            try {
                this.h.join();
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
            this.h = null;
        }
        if (this.g != null) {
            this.g.clear();
        }
        if (this.d != null) {
            this.d.b();
        }
        com.tencent.wcs.proxy.e.a.a().a(new d(this), 5000, TimeUnit.MILLISECONDS);
    }

    public void d() {
        if (c.f4064a) {
            com.tencent.wcs.c.b.a("AgentService clearSessionImediately");
        }
        this.i = false;
        if (this.h != null) {
            this.h.interrupt();
            try {
                this.h.join();
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
            this.h = null;
        }
        if (this.g != null) {
            this.g.clear();
        }
        if (this.d != null) {
            this.d.b();
        }
        ArrayList arrayList = new ArrayList();
        synchronized (this.f) {
            int size = this.f.size();
            for (int i2 = 0; i2 < size; i2++) {
                h valueAt = this.f.valueAt(i2);
                if (valueAt != null) {
                    arrayList.add(valueAt);
                }
            }
            this.f.clear();
        }
        int size2 = arrayList.size();
        for (int i3 = 0; i3 < size2; i3++) {
            ((h) arrayList.get(i3)).b();
            ((h) arrayList.get(i3)).a((p) null);
        }
        arrayList.clear();
    }
}
