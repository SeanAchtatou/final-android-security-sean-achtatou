package com.tencent.wcs.f;

import android.util.SparseBooleanArray;

/* compiled from: ProGuard */
public abstract class f extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private volatile boolean f4073a;
    private final Object b = new Object();
    private final Object c = new Object();
    private final int d;
    private final SparseBooleanArray e;

    public abstract void a(int i);

    public f(int i) {
        this.d = i;
        this.f4073a = false;
        this.e = new SparseBooleanArray();
    }

    public void a() {
        this.f4073a = true;
        start();
    }

    public void b(int i) {
        synchronized (this.c) {
            this.e.put(i, true);
        }
    }

    public void c(int i) {
        synchronized (this.c) {
            this.e.delete(i);
        }
    }

    public void b() {
        synchronized (this.c) {
            this.e.clear();
        }
    }

    public void d(int i) {
        synchronized (this.c) {
            if (this.e.indexOfKey(i) >= 0) {
                this.e.put(i, true);
            }
        }
    }

    public void run() {
        super.run();
        while (this.f4073a) {
            c();
            synchronized (this.b) {
                try {
                    this.b.wait((long) (this.d * 1000));
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
            }
            if (this.f4073a) {
                synchronized (this.c) {
                    int size = this.e.size();
                    for (int i = 0; i < size; i++) {
                        int keyAt = this.e.keyAt(i);
                        if (!this.e.get(keyAt)) {
                            a(keyAt);
                        }
                    }
                }
            }
        }
    }

    private void c() {
        synchronized (this.c) {
            int size = this.e.size();
            for (int i = 0; i < size; i++) {
                this.e.put(this.e.keyAt(i), false);
            }
        }
    }
}
