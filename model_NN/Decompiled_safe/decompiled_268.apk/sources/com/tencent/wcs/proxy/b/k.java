package com.tencent.wcs.proxy.b;

import com.tencent.wcs.jce.ServiceTransData;

/* compiled from: ProGuard */
public class k extends a<ServiceTransData, Void> {
    public static k b() {
        return m.f4120a;
    }

    private k() {
    }

    /* access modifiers changed from: protected */
    public ServiceTransData a(Void... voidArr) {
        return new ServiceTransData();
    }

    /* access modifiers changed from: protected */
    public boolean b(Void... voidArr) {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean a(ServiceTransData serviceTransData) {
        return serviceTransData != null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void c(ServiceTransData serviceTransData) {
        if (serviceTransData != null) {
            serviceTransData.a((String) null);
            serviceTransData.a(0);
            serviceTransData.a((byte[]) null);
            serviceTransData.b(0);
            serviceTransData.a(0);
            serviceTransData.c(0);
            serviceTransData.d(0);
            serviceTransData.e(0);
        }
    }
}
