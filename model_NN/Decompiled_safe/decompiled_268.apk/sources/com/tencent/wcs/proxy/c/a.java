package com.tencent.wcs.proxy.c;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.wcs.b.c;
import com.tencent.wcs.c.b;
import com.tencent.wcs.proxy.b.n;
import com.tencent.wcs.proxy.l;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final byte[] f4125a = {JceStruct.SIMPLE_LIST, 10};
    private static final byte[] b = "POST / HTTP/1.1".getBytes();
    private static final byte[] c = "POST /version HTTP/1.1".getBytes();
    private volatile boolean d = false;
    private volatile boolean e = false;
    private volatile int f = 0;
    private boolean g = false;
    private final Object h = new Object();
    private final Object i = new Object();
    private final Object j = new Object();
    private URL k;
    private Socket l;
    private DataInputStream m;
    private InputStream n;
    private OutputStream o;
    private Thread p;
    private Thread q;
    private BlockingQueue<e> r;
    private d s;
    private l t;

    public a(d dVar, l lVar) {
        this.s = dVar;
        this.t = lVar;
    }

    public boolean a() {
        synchronized (this.h) {
            if (this.r == null) {
                this.r = new LinkedBlockingQueue();
            }
        }
        return c();
    }

    public void b() {
        d();
        synchronized (this.h) {
            if (this.r != null) {
                this.r.clear();
                this.r = null;
            }
        }
        this.g = false;
    }

    public boolean c() {
        if (this.e) {
            return true;
        }
        synchronized (this.h) {
            g();
            InetSocketAddress inetSocketAddress = new InetSocketAddress(this.k.getHost(), this.k.getPort());
            this.l = new Socket();
            try {
                b.a("ProxySession connecting " + inetSocketAddress.toString());
                this.l.setTcpNoDelay(true);
                this.l.setKeepAlive(false);
                this.l.setSoTimeout(15000);
                this.l.connect(inetSocketAddress, EventDispatcherEnum.CACHE_EVENT_START);
                if (this.l != null) {
                    try {
                        synchronized (this.i) {
                            this.n = this.l.getInputStream();
                            this.m = new DataInputStream(this.n);
                        }
                        synchronized (this.j) {
                            this.o = this.l.getOutputStream();
                        }
                    } catch (IOException e2) {
                        e2.printStackTrace();
                        b.a("ProxySession startConnection  create stream IOException!!!");
                        return false;
                    }
                }
                this.d = false;
                this.e = true;
                this.p = new b(this, "ProxySessionInputWorker");
                this.q = new c(this, "ProxySessionOutputWorker");
                this.p.start();
                this.q.start();
            } catch (SocketTimeoutException e3) {
                e3.printStackTrace();
                b.a("ProxySession startConnection  SocketTimeoutException!!!");
                return false;
            } catch (SocketException e4) {
                e4.printStackTrace();
                b.a("ProxySession startConnection  SocketException!!!");
                return false;
            } catch (IOException e5) {
                e5.printStackTrace();
                b.a("ProxySession startConnection  IOException!!!");
                return false;
            }
        }
        return true;
    }

    public void d() {
        if (this.e) {
            this.e = false;
            this.d = true;
            this.f = 0;
            synchronized (this.h) {
                synchronized (this.i) {
                    try {
                        if (this.m != null) {
                            this.m.close();
                            this.m = null;
                        }
                    } catch (IOException e2) {
                    }
                    try {
                        if (this.n != null) {
                            this.n.close();
                            this.n = null;
                        }
                    } catch (IOException e3) {
                    }
                    synchronized (this.j) {
                        try {
                            if (this.o != null) {
                                this.o.close();
                                this.o = null;
                            }
                        } catch (IOException e4) {
                        }
                        try {
                            if (this.l != null) {
                                this.l.close();
                                this.l = null;
                            }
                        } catch (IOException e5) {
                        }
                    }
                }
                try {
                    if (this.p != null) {
                        this.p.interrupt();
                        this.p.join();
                        this.p = null;
                    }
                } catch (InterruptedException e6) {
                }
                try {
                    if (this.q != null) {
                        this.q.interrupt();
                        this.q.join();
                        this.q = null;
                    }
                } catch (InterruptedException e7) {
                }
            }
        }
    }

    public boolean e() {
        return this.e;
    }

    public void a(e eVar) {
        if (this.r != null) {
            this.r.offer(eVar);
        }
    }

    public void f() {
        synchronized (this.j) {
            if (this.o != null) {
                try {
                    this.o.write(a(c, f4125a, f4125a));
                    this.o.flush();
                } catch (IOException e2) {
                }
            }
        }
    }

    private void g() {
        try {
            this.k = new URL(com.qq.h.b.a().j());
            if (this.g) {
                this.k = new URL("http://" + this.k.getHost() + ":" + 10000 + "/");
            }
        } catch (MalformedURLException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        r1 = r4.i;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0011, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r0 = com.tencent.wcs.proxy.c.f.a(r4.m);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0018, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0019, code lost:
        if (r0 == null) goto L_0x00b0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r4.f = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0020, code lost:
        if (r4.s == null) goto L_0x0027;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0022, code lost:
        r4.s.h();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x002d, code lost:
        if (r0.a() != 200) goto L_0x005f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0031, code lost:
        if (r4.s == null) goto L_0x0001;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0033, code lost:
        r4.s.a(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x003c, code lost:
        if (r4.f != 0) goto L_0x003e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x003e, code lost:
        monitor-enter(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0041, code lost:
        if (r4.f == 1) goto L_0x0043;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0043, code lost:
        r4.f++;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0054, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0055, code lost:
        com.tencent.wcs.c.b.a(r0);
        r0.printStackTrace();
        j();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0065, code lost:
        if (r0.a() == 204) goto L_0x0001;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x006d, code lost:
        if (r0.a() == 502) goto L_0x0077;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0075, code lost:
        if (r0.a() != 504) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0079, code lost:
        if (r4.g != false) goto L_0x0082;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x007b, code lost:
        r4.g = true;
        j();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0082, code lost:
        k();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0086, code lost:
        com.tencent.wcs.c.b.a("ProxySession got unexpected response with response code " + r0.a() + " and length " + r0.b());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00b0, code lost:
        com.tencent.wcs.c.b.a("ProxySession receive null response, disconnected by remote");
        j();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x00bd, code lost:
        if (r4.f > 1) goto L_0x00bf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x00bf, code lost:
        com.tencent.wcs.c.b.a("ProxySession receive heartbeat response timeout");
        j();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0001, code lost:
        continue;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0001, code lost:
        continue;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:?, code lost:
        return;
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void h() {
        /*
            r4 = this;
            r3 = 1
        L_0x0001:
            boolean r0 = r4.e
            if (r0 == 0) goto L_0x000d
            java.lang.Object r1 = r4.i
            monitor-enter(r1)
            java.io.DataInputStream r0 = r4.m     // Catch:{ all -> 0x004e }
            if (r0 != 0) goto L_0x000e
            monitor-exit(r1)     // Catch:{ all -> 0x004e }
        L_0x000d:
            return
        L_0x000e:
            monitor-exit(r1)     // Catch:{ all -> 0x004e }
            java.lang.Object r1 = r4.i     // Catch:{ SocketTimeoutException -> 0x0039, IOException -> 0x0054 }
            monitor-enter(r1)     // Catch:{ SocketTimeoutException -> 0x0039, IOException -> 0x0054 }
            java.io.DataInputStream r0 = r4.m     // Catch:{ all -> 0x0051 }
            com.tencent.wcs.proxy.c.f r0 = com.tencent.wcs.proxy.c.f.a(r0)     // Catch:{ all -> 0x0051 }
            monitor-exit(r1)     // Catch:{ all -> 0x0051 }
            if (r0 == 0) goto L_0x00b0
            r1 = 0
            r4.f = r1     // Catch:{ SocketTimeoutException -> 0x0039, IOException -> 0x0054 }
            com.tencent.wcs.proxy.c.d r1 = r4.s     // Catch:{ SocketTimeoutException -> 0x0039, IOException -> 0x0054 }
            if (r1 == 0) goto L_0x0027
            com.tencent.wcs.proxy.c.d r1 = r4.s     // Catch:{ SocketTimeoutException -> 0x0039, IOException -> 0x0054 }
            r1.h()     // Catch:{ SocketTimeoutException -> 0x0039, IOException -> 0x0054 }
        L_0x0027:
            int r1 = r0.a()     // Catch:{ SocketTimeoutException -> 0x0039, IOException -> 0x0054 }
            r2 = 200(0xc8, float:2.8E-43)
            if (r1 != r2) goto L_0x005f
            com.tencent.wcs.proxy.c.d r1 = r4.s     // Catch:{ SocketTimeoutException -> 0x0039, IOException -> 0x0054 }
            if (r1 == 0) goto L_0x0001
            com.tencent.wcs.proxy.c.d r1 = r4.s     // Catch:{ SocketTimeoutException -> 0x0039, IOException -> 0x0054 }
            r1.a(r0)     // Catch:{ SocketTimeoutException -> 0x0039, IOException -> 0x0054 }
            goto L_0x0001
        L_0x0039:
            r0 = move-exception
            int r0 = r4.f
            if (r0 == 0) goto L_0x0001
            monitor-enter(r4)
            int r0 = r4.f     // Catch:{ all -> 0x004b }
            if (r0 != r3) goto L_0x00ba
            int r0 = r4.f     // Catch:{ all -> 0x004b }
            int r0 = r0 + 1
            r4.f = r0     // Catch:{ all -> 0x004b }
            monitor-exit(r4)     // Catch:{ all -> 0x004b }
            goto L_0x0001
        L_0x004b:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x004b }
            throw r0
        L_0x004e:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x004e }
            throw r0
        L_0x0051:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0051 }
            throw r0     // Catch:{ SocketTimeoutException -> 0x0039, IOException -> 0x0054 }
        L_0x0054:
            r0 = move-exception
            com.tencent.wcs.c.b.a(r0)
            r0.printStackTrace()
            r4.j()
            goto L_0x000d
        L_0x005f:
            int r1 = r0.a()     // Catch:{ SocketTimeoutException -> 0x0039, IOException -> 0x0054 }
            r2 = 204(0xcc, float:2.86E-43)
            if (r1 == r2) goto L_0x0001
            int r1 = r0.a()     // Catch:{ SocketTimeoutException -> 0x0039, IOException -> 0x0054 }
            r2 = 502(0x1f6, float:7.03E-43)
            if (r1 == r2) goto L_0x0077
            int r1 = r0.a()     // Catch:{ SocketTimeoutException -> 0x0039, IOException -> 0x0054 }
            r2 = 504(0x1f8, float:7.06E-43)
            if (r1 != r2) goto L_0x0086
        L_0x0077:
            boolean r0 = r4.g     // Catch:{ SocketTimeoutException -> 0x0039, IOException -> 0x0054 }
            if (r0 != 0) goto L_0x0082
            r0 = 1
            r4.g = r0     // Catch:{ SocketTimeoutException -> 0x0039, IOException -> 0x0054 }
            r4.j()     // Catch:{ SocketTimeoutException -> 0x0039, IOException -> 0x0054 }
            goto L_0x000d
        L_0x0082:
            r4.k()     // Catch:{ SocketTimeoutException -> 0x0039, IOException -> 0x0054 }
            goto L_0x000d
        L_0x0086:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x0039, IOException -> 0x0054 }
            r1.<init>()     // Catch:{ SocketTimeoutException -> 0x0039, IOException -> 0x0054 }
            java.lang.String r2 = "ProxySession got unexpected response with response code "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SocketTimeoutException -> 0x0039, IOException -> 0x0054 }
            int r2 = r0.a()     // Catch:{ SocketTimeoutException -> 0x0039, IOException -> 0x0054 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SocketTimeoutException -> 0x0039, IOException -> 0x0054 }
            java.lang.String r2 = " and length "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SocketTimeoutException -> 0x0039, IOException -> 0x0054 }
            int r0 = r0.b()     // Catch:{ SocketTimeoutException -> 0x0039, IOException -> 0x0054 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ SocketTimeoutException -> 0x0039, IOException -> 0x0054 }
            java.lang.String r0 = r0.toString()     // Catch:{ SocketTimeoutException -> 0x0039, IOException -> 0x0054 }
            com.tencent.wcs.c.b.a(r0)     // Catch:{ SocketTimeoutException -> 0x0039, IOException -> 0x0054 }
            goto L_0x0001
        L_0x00b0:
            java.lang.String r0 = "ProxySession receive null response, disconnected by remote"
            com.tencent.wcs.c.b.a(r0)     // Catch:{ SocketTimeoutException -> 0x0039, IOException -> 0x0054 }
            r4.j()     // Catch:{ SocketTimeoutException -> 0x0039, IOException -> 0x0054 }
            goto L_0x000d
        L_0x00ba:
            monitor-exit(r4)     // Catch:{ all -> 0x004b }
            int r0 = r4.f
            if (r0 <= r3) goto L_0x0001
            java.lang.String r0 = "ProxySession receive heartbeat response timeout"
            com.tencent.wcs.c.b.a(r0)
            r4.j()
            goto L_0x0001
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.wcs.proxy.c.a.h():void");
    }

    /* access modifiers changed from: private */
    public void i() {
        while (this.e) {
            if (this.r != null) {
                try {
                    e poll = this.r.poll((long) (c.k + 15), TimeUnit.SECONDS);
                    if (poll == null) {
                        synchronized (this.t) {
                            this.t.g();
                        }
                    } else {
                        ArrayList arrayList = new ArrayList();
                        arrayList.add(poll);
                        int size = this.r.size();
                        for (int i2 = 0; i2 < size; i2++) {
                            e poll2 = this.r.poll();
                            if (poll2 != null) {
                                arrayList.add(poll2);
                            }
                        }
                        try {
                            synchronized (this.j) {
                                int size2 = arrayList.size();
                                for (int i3 = 0; i3 < size2; i3++) {
                                    e eVar = (e) arrayList.get(i3);
                                    if (this.o != null) {
                                        if (eVar.b() == 100) {
                                            this.f++;
                                        }
                                        this.o.write(a(b, f4125a, a(eVar.a()), f4125a, f4125a, eVar.d(), eVar.e()));
                                        this.o.flush();
                                    }
                                    n.b().b((Object) eVar);
                                }
                                arrayList.clear();
                            }
                        } catch (IOException e2) {
                            b.a(e2);
                            e2.printStackTrace();
                            j();
                            return;
                        }
                    }
                } catch (InterruptedException e3) {
                    e3.printStackTrace();
                    return;
                }
            }
        }
    }

    private void j() {
        synchronized (this.t) {
            if (!this.d) {
                b.a("ProxySession connection lost by exception, probably caused by network change");
                this.t.i();
                this.d = true;
            }
        }
    }

    private void k() {
        synchronized (this.t) {
            this.t.j();
        }
    }

    private byte[] a(int i2) {
        return ("Content-Length: " + i2).getBytes();
    }

    private byte[] a(byte[]... bArr) {
        if (bArr == null) {
            return new byte[0];
        }
        int i2 = 0;
        for (byte[] bArr2 : bArr) {
            if (bArr2 != null) {
                i2 += bArr2.length;
            }
        }
        byte[] bArr3 = new byte[i2];
        int i3 = 0;
        for (byte[] bArr4 : bArr) {
            if (bArr4 != null) {
                System.arraycopy(bArr4, 0, bArr3, i3, bArr4.length);
                i3 += bArr4.length;
            }
        }
        return bArr3;
    }
}
