package com.tencent.wcs.proxy;

import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistant.utils.ah;
import com.tencent.wcs.c.b;

/* compiled from: ProGuard */
class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f4130a;

    d(c cVar) {
        this.f4130a = cVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.wcs.proxy.c.a(com.tencent.wcs.proxy.c, boolean):boolean
     arg types: [com.tencent.wcs.proxy.c, int]
     candidates:
      com.tencent.wcs.proxy.c.a(com.tencent.wcs.proxy.c, int):int
      com.tencent.wcs.proxy.c.a(com.tencent.wcs.proxy.c, boolean):boolean */
    public void run() {
        synchronized (this.f4130a.l) {
            if (this.f4130a.b != null) {
                boolean c = this.f4130a.b.c();
                b.a("WanServiceManager start wireless service result: " + c);
                if (c) {
                    boolean unused = this.f4130a.m = true;
                    this.f4130a.b.a(100, this.f4130a.c);
                    this.f4130a.b.a(1997, this.f4130a.f);
                    this.f4130a.b.a((int) STConstAction.ACTION_HIT_PAUSE, this.f4130a.e);
                    this.f4130a.b.a((int) STConst.ST_PAGE_NECESSARY, this.f4130a.d);
                    this.f4130a.b.a(1996, this.f4130a);
                    this.f4130a.b.a(1994, this.f4130a);
                    this.f4130a.b.a((int) STConstAction.ACTION_HIT_EXPAND, this.f4130a.j);
                    this.f4130a.f();
                    int unused2 = this.f4130a.o = 0;
                    ah.a().post(new e(this));
                } else if (this.f4130a.o < 5) {
                    c.j(this.f4130a);
                    this.f4130a.b.d();
                    this.f4130a.r();
                } else {
                    ah.a().post(new f(this));
                }
            }
        }
    }
}
