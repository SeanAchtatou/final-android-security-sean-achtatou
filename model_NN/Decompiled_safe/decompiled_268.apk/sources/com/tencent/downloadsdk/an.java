package com.tencent.downloadsdk;

import com.tencent.downloadsdk.utils.f;

public class an {

    /* renamed from: a  reason: collision with root package name */
    public static String f2463a = "SegStruct";
    public String b;
    public long c;
    public String d;
    public volatile long e;
    public long f;
    public volatile long g;
    public boolean h = false;
    public volatile long i;
    public String j;
    public long k;
    public int l;
    public long m;

    public an() {
    }

    public an(String str, long j2, String str2, long j3, long j4) {
        this.b = str;
        this.c = j2;
        this.d = str2;
        this.e = j3;
        this.f = j4;
    }

    public boolean a() {
        return this.e <= this.i && this.e > 0;
    }

    public long b() {
        long j2 = this.e - this.i;
        if (j2 >= 0) {
            return j2;
        }
        return 0;
    }

    public void c() {
        f.b(f2463a, "segId: " + this.c);
        f.b(f2463a, "\tmStartPosition: " + this.f);
        f.b(f2463a, "\tmSegTotalLength: " + this.e);
        f.b(f2463a, "\tmSegSavedLen: " + this.g);
        f.b(f2463a, "\tmSrcURL: " + this.d);
    }
}
