package com.tencent.downloadsdk.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.os.StatFs;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.tencent.connect.common.Constants;
import java.io.File;

public class g {
    public static long a() {
        try {
            File dataDirectory = Environment.getDataDirectory();
            if (!dataDirectory.exists()) {
                return -1;
            }
            StatFs statFs = new StatFs(dataDirectory.getPath());
            return ((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize());
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static String a(Context context) {
        if (context == null) {
            return null;
        }
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        } catch (SecurityException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String b(Context context) {
        if (context == null) {
            return null;
        }
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
        } catch (SecurityException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean b() {
        try {
            return "mounted".equals(Environment.getExternalStorageState()) && Environment.getExternalStorageDirectory().canWrite();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static long c() {
        try {
            if (!b()) {
                return -1;
            }
            File externalStorageDirectory = Environment.getExternalStorageDirectory();
            if (!externalStorageDirectory.exists()) {
                return -1;
            }
            StatFs statFs = new StatFs(externalStorageDirectory.getPath());
            return ((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize());
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static String c(Context context) {
        if (context == null) {
            return null;
        }
        try {
            WifiInfo connectionInfo = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo();
            return connectionInfo != null ? connectionInfo.getMacAddress() : Constants.STR_EMPTY;
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return Constants.STR_EMPTY;
        } catch (SecurityException e2) {
            e2.printStackTrace();
            return Constants.STR_EMPTY;
        } catch (RuntimeException e3) {
            e3.printStackTrace();
            return Constants.STR_EMPTY;
        }
    }

    public static int d(Context context) {
        if (context == null) {
            return 0;
        }
        return j(context).versionCode;
    }

    public static String e(Context context) {
        return j(context).versionName;
    }

    public static String f(Context context) {
        if (context == null) {
            return null;
        }
        return j(context).packageName;
    }

    public static String g(Context context) {
        return context == null ? Constants.STR_EMPTY : ((TelephonyManager) context.getSystemService("phone")).getNetworkOperator();
    }

    public static int h(Context context) {
        NetworkInfo networkInfo;
        if (context == null) {
            return 0;
        }
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") == -1) {
            return 10;
        }
        try {
            networkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        } catch (Exception e) {
            networkInfo = null;
        }
        if (networkInfo == null) {
            return 10;
        }
        if (networkInfo.getType() == 1) {
            return 1;
        }
        String extraInfo = networkInfo.getExtraInfo();
        if (TextUtils.isEmpty(extraInfo)) {
            return 10;
        }
        if (extraInfo.equalsIgnoreCase("UN_DETECT")) {
            return 0;
        }
        if (extraInfo.equalsIgnoreCase("CMWAP")) {
            return 2;
        }
        if (extraInfo.equalsIgnoreCase("CMNET")) {
            return 3;
        }
        if (extraInfo.equalsIgnoreCase("UNIWAP")) {
            return 4;
        }
        if (extraInfo.equalsIgnoreCase("UNINET")) {
            return 5;
        }
        if (extraInfo.equalsIgnoreCase("WAP3G")) {
            return 6;
        }
        if (extraInfo.equalsIgnoreCase("NET3G")) {
            return 7;
        }
        if (extraInfo.equalsIgnoreCase("CTWAP")) {
            return 8;
        }
        return extraInfo.equalsIgnoreCase("CTNET") ? 9 : 10;
    }

    public static int i(Context context) {
        WifiManager wifiManager;
        WifiInfo connectionInfo;
        if (context == null || (wifiManager = (WifiManager) context.getSystemService("wifi")) == null || (connectionInfo = wifiManager.getConnectionInfo()) == null || connectionInfo.getBSSID() == null) {
            return -1;
        }
        return WifiManager.calculateSignalLevel(connectionInfo.getRssi(), 5);
    }

    private static PackageInfo j(Context context) {
        if (context == null) {
            return null;
        }
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            f.a("DeviceUtils", e.getMessage(), e);
            return null;
        }
    }
}
