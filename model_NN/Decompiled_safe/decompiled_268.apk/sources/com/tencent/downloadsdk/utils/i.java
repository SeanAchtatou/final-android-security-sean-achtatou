package com.tencent.downloadsdk.utils;

import android.os.Environment;
import android.os.StatFs;
import android.text.TextUtils;
import com.tencent.connect.common.Constants;
import java.io.File;

public class i {
    public static void a(String str, String str2) {
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            new File(str).renameTo(new File(str2));
        }
    }

    public static boolean a(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        File file = new File(str);
        if (!file.exists() || !file.isFile()) {
            return false;
        }
        return file.delete();
    }

    public static boolean a(String str, long j) {
        return b(str) >= ((long) (((float) j) * 1.5f));
    }

    public static long b(String str) {
        String c;
        if (str == null) {
            return 0;
        }
        try {
            if (str.equals(Constants.STR_EMPTY) || (c = c(str)) == null || c.equals(Constants.STR_EMPTY)) {
                return 0;
            }
            StatFs statFs = new StatFs(c);
            long availableBlocks = (((long) statFs.getAvailableBlocks()) - 4) * ((long) statFs.getBlockSize());
            if (availableBlocks >= 0) {
                return availableBlocks;
            }
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static String c(String str) {
        if (str == null || str.equals(Constants.STR_EMPTY)) {
            return null;
        }
        String absolutePath = Environment.getDataDirectory().getAbsolutePath();
        File externalStorageDirectory = Environment.getExternalStorageDirectory();
        File[] listFiles = externalStorageDirectory.getParentFile().listFiles();
        String absolutePath2 = externalStorageDirectory.getAbsolutePath();
        if (str.startsWith(absolutePath)) {
            return absolutePath;
        }
        if (str.startsWith(absolutePath2) && g.b()) {
            return absolutePath2;
        }
        for (File absolutePath3 : listFiles) {
            String absolutePath4 = absolutePath3.getAbsolutePath();
            if (str.startsWith(absolutePath4)) {
                return absolutePath4;
            }
        }
        return null;
    }
}
