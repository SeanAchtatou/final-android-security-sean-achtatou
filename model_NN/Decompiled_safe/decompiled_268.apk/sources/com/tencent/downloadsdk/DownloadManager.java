package com.tencent.downloadsdk;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import com.tencent.beacon.event.a;
import com.tencent.connect.common.Constants;
import com.tencent.downloadsdk.a.c;
import com.tencent.downloadsdk.storage.a.b;
import com.tencent.downloadsdk.storage.a.e;
import com.tencent.downloadsdk.storage.helper.SDKDBHelper;
import com.tencent.downloadsdk.utils.f;
import com.tencent.downloadsdk.utils.h;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.PriorityBlockingQueue;

public class DownloadManager {
    private static DownloadManager c = null;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ExecutorService f2444a;
    private DownloadTaskQueue b;
    private Context d;
    private HandlerThread e;
    private Handler f;

    class DownloadTaskQueue extends PriorityBlockingQueue<DownloadTask> implements Runnable, a {
        private int b = 2;
        private ConcurrentHashMap<String, DownloadTask> c = new ConcurrentHashMap<>();
        private Object d = new Object();
        private Object e = new Object();

        public DownloadTaskQueue() {
            super(10, new h());
        }

        public DownloadTask a(String str) {
            DownloadTask downloadTask;
            Iterator it = iterator();
            while (true) {
                if (!it.hasNext()) {
                    downloadTask = null;
                    break;
                }
                downloadTask = (DownloadTask) it.next();
                if (downloadTask.h().equals(str)) {
                    break;
                }
            }
            if (downloadTask != null) {
                super.remove(downloadTask);
            }
            return downloadTask;
        }

        public Collection<DownloadTask> a() {
            if (this.c == null) {
                return null;
            }
            return this.c.values();
        }

        public void a(DownloadTask downloadTask, int i, String str) {
            synchronized (this.c) {
                DownloadTask remove = this.c.remove(DownloadTask.a(i, str));
                if (!(remove == downloadTask || downloadTask == null)) {
                    f.d("DownloadManager", "===========处理游离线程======");
                    downloadTask.b();
                }
                f.b("DownloadManager", "runningTasks  removed task:" + remove + ",taskId:" + str);
                if (remove != null && this.c.size() < this.b) {
                    synchronized (this.d) {
                        this.d.notify();
                    }
                }
            }
        }

        public boolean a(String str, DownloadTask downloadTask) {
            DownloadTask downloadTask2;
            boolean z;
            synchronized (this.e) {
                Iterator it = iterator();
                while (true) {
                    if (!it.hasNext()) {
                        downloadTask2 = null;
                        break;
                    }
                    downloadTask2 = (DownloadTask) it.next();
                    if (downloadTask2.h().equals(str)) {
                        break;
                    }
                }
                if (downloadTask2 == null) {
                    super.put(downloadTask);
                    this.e.notify();
                    z = true;
                } else {
                    this.e.notify();
                    z = false;
                }
            }
            return z;
        }

        public boolean b(String str) {
            DownloadTask downloadTask;
            Iterator it = iterator();
            while (true) {
                if (!it.hasNext()) {
                    downloadTask = null;
                    break;
                }
                downloadTask = (DownloadTask) it.next();
                if (downloadTask.h().equals(str)) {
                    break;
                }
            }
            return downloadTask != null;
        }

        public DownloadTask c(String str) {
            return this.c.get(str);
        }

        /* access modifiers changed from: protected */
        public boolean d(String str) {
            if (this.c.get(str) == null) {
                return b(str);
            }
            return true;
        }

        public DownloadTask e(String str) {
            DownloadTask downloadTask;
            f.a("synchronized", "runningTasks was locked in pause by " + Thread.currentThread().getName());
            synchronized (this.c) {
                downloadTask = this.c.get(str);
                if (downloadTask == null) {
                    downloadTask = a(str);
                }
                if (downloadTask != null) {
                    downloadTask.b();
                }
            }
            f.b("synchronized", "runningTasks was unlocked in pause by " + Thread.currentThread().getName());
            return downloadTask;
        }

        public DownloadTask f(String str) {
            DownloadTask downloadTask;
            f.a("synchronized", "runningTasks was locked in cancel by " + Thread.currentThread().getName());
            synchronized (this.c) {
                downloadTask = this.c.get(str);
                if (downloadTask == null) {
                    downloadTask = a(str);
                }
                if (downloadTask != null) {
                    downloadTask.c();
                }
            }
            f.b("synchronized", "runningTasks was unlocked in cancel by " + Thread.currentThread().getName());
            return downloadTask;
        }

        public void run() {
            while (true) {
                synchronized (this.e) {
                    while (size() == 0) {
                        try {
                            this.e.wait();
                        } catch (InterruptedException e2) {
                            e2.printStackTrace();
                        }
                    }
                }
                synchronized (this.d) {
                    while (this.c.size() >= this.b) {
                        try {
                            this.d.wait();
                        } catch (InterruptedException e3) {
                            e3.printStackTrace();
                        }
                    }
                }
                try {
                    f.a("synchronized", "runningTasks was locked in run by " + Thread.currentThread().getName());
                    synchronized (this.c) {
                        if (size() > 0) {
                            DownloadTask downloadTask = (DownloadTask) take();
                            if (this.c.get(downloadTask.h()) != null) {
                                put(downloadTask);
                            } else {
                                downloadTask.a(this);
                                downloadTask.u = DownloadManager.this.f2444a.submit(downloadTask);
                                this.c.put(downloadTask.h(), downloadTask);
                                f.b("DownloadManager", "task " + downloadTask.e + downloadTask.m + " has submit to " + Thread.currentThread().getName() + " to run");
                            }
                        } else {
                            f.d("synchronized", "waitting queue is empty " + Thread.currentThread().getName());
                        }
                    }
                    f.d("synchronized", "runningTasks was unlocked in run by " + Thread.currentThread().getName());
                } catch (InterruptedException e4) {
                    e4.printStackTrace();
                } catch (Exception e5) {
                    e5.printStackTrace();
                }
            }
        }
    }

    private DownloadManager() {
        this.b = null;
        this.e = new HandlerThread("ShareBackTask", 10);
        this.b = new DownloadTaskQueue();
        this.f2444a = Executors.newCachedThreadPool();
        this.f2444a.submit(this.b);
        this.e.start();
        this.f = new Handler(this.e.getLooper());
    }

    public static synchronized DownloadManager a() {
        DownloadManager downloadManager;
        synchronized (DownloadManager.class) {
            if (c == null) {
                c = new DownloadManager();
            }
            downloadManager = c;
        }
        return downloadManager;
    }

    private boolean b(DownloadTask downloadTask) {
        return downloadTask.f();
    }

    public DownloadTask a(int i, String str) {
        f.b("DownloadManager", "pause type: " + i + " id: " + str);
        if (this.b.d(DownloadTask.a(i, str))) {
            f.b("DownloadManager", "Task has existed: " + str);
            return this.b.e(DownloadTask.a(i, str));
        }
        f.b("DownloadManager", "Task not existed: " + str);
        return null;
    }

    public ArrayList<ac> a(int i) {
        return new b().a(i);
    }

    public void a(int i, Class<?>[] clsArr) {
        ((SDKDBHelper) SDKDBHelper.getDBHelper(this.d)).setEntrustTable(i, clsArr);
    }

    public void a(String str) {
        b.e = str;
    }

    public void a(int[] iArr) {
        if (iArr != null && iArr.length > 0) {
            for (int b2 : iArr) {
                b(b2);
            }
        }
    }

    public boolean a(Context context) {
        if (context == null) {
            return false;
        }
        this.d = context;
        b.a(context);
        return true;
    }

    public boolean a(DownloadTask downloadTask) {
        o.a().c();
        if (downloadTask == null) {
            return false;
        }
        f.a("DownloadManager", "Add new Task id:" + downloadTask.d + " appId:" + downloadTask.e + " apkId:" + downloadTask.f);
        if (b(downloadTask)) {
            f.a("DownloadManager", "task already complete: " + downloadTask.d);
            downloadTask.e();
            return false;
        } else if (this.b.d(downloadTask.h())) {
            f.b("DownloadManager", "Task has existed: " + downloadTask.d);
            return false;
        } else {
            f.b("DownloadManager", "Task not existed: " + downloadTask.d);
            HashMap hashMap = new HashMap();
            hashMap.put("B110", Constants.STR_EMPTY + downloadTask.g());
            hashMap.put("B123", "start");
            a.a("TMDownloadSDKCheck", true, 0, 0, hashMap, true);
            downloadTask.h = System.currentTimeMillis();
            return this.b.a(downloadTask.h(), downloadTask);
        }
    }

    public Context b() {
        return this.d;
    }

    public void b(int i) {
        ArrayList arrayList = new ArrayList();
        Iterator it = this.b.iterator();
        while (it.hasNext()) {
            DownloadTask downloadTask = (DownloadTask) it.next();
            if (downloadTask.c == i) {
                arrayList.add(downloadTask);
            }
        }
        this.b.removeAll(arrayList);
        for (DownloadTask downloadTask2 : this.b.a()) {
            b(i, downloadTask2.d);
        }
    }

    public void b(int i, String str) {
        f.a("DownloadManager", "cancel type: " + i + " id: " + str);
        DownloadTask downloadTask = null;
        if (this.b.d(DownloadTask.a(i, str))) {
            downloadTask = this.b.f(DownloadTask.a(i, str));
        }
        if (downloadTask == null) {
            String a2 = DownloadTask.a(i, str);
            c.a(a2);
            new e().b(a2);
            g(i, str);
        }
        e(i, str);
    }

    public void b(Context context) {
        if (context != null) {
            this.d = context;
        }
    }

    public Handler c() {
        return this.f;
    }

    public boolean c(int i, String str) {
        return this.b.c(DownloadTask.a(i, str)) != null;
    }

    public boolean d() {
        Collection<DownloadTask> a2 = this.b.a();
        return a2 != null && !a2.isEmpty();
    }

    public boolean d(int i, String str) {
        f.d("DownloadManager", "delete type:" + i + " id:" + str);
        return f(i, str) && e(i, str);
    }

    public boolean e(int i, String str) {
        new b().b(i, str);
        String a2 = DownloadTask.a(i, str);
        c.a(a2);
        new e().b(a2);
        return true;
    }

    public boolean f(int i, String str) {
        af.c(new b().a(i, str));
        return true;
    }

    public boolean g(int i, String str) {
        af.d(new b().a(i, str));
        return true;
    }
}
