package com.tencent.downloadsdk;

import java.util.Iterator;

class ad implements ae {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadTask f2456a;

    private ad(DownloadTask downloadTask) {
        this.f2456a = downloadTask;
    }

    /* synthetic */ ad(DownloadTask downloadTask, r rVar) {
        this(downloadTask);
    }

    public void a(int i, String str) {
        Iterator it = this.f2456a.y.iterator();
        while (it.hasNext()) {
            try {
                ((ae) it.next()).a(i, str);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void a(int i, String str, int i2, byte[] bArr, String str2) {
        Iterator it = this.f2456a.y.iterator();
        while (it.hasNext()) {
            try {
                ((ae) it.next()).a(i, str, i2, bArr, str2);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void a(int i, String str, long j, long j2, double d) {
        Iterator it = this.f2456a.y.iterator();
        while (it.hasNext()) {
            try {
                ((ae) it.next()).a(i, str, j, j2, d);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void a(int i, String str, long j, String str2, String str3) {
        Iterator it = this.f2456a.y.iterator();
        while (it.hasNext()) {
            try {
                ((ae) it.next()).a(i, str, j, str2, str3);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void a(int i, String str, String str2) {
        Iterator it = this.f2456a.y.iterator();
        while (it.hasNext()) {
            try {
                ((ae) it.next()).a(i, str, str2);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void a(int i, String str, String str2, String str3) {
        Iterator it = this.f2456a.y.iterator();
        while (it.hasNext()) {
            try {
                ((ae) it.next()).a(i, str, str2, str3);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void b(int i, String str) {
        Iterator it = this.f2456a.y.iterator();
        while (it.hasNext()) {
            try {
                ((ae) it.next()).b(i, str);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void b(int i, String str, String str2) {
        Iterator it = this.f2456a.y.iterator();
        while (it.hasNext()) {
            try {
                ((ae) it.next()).b(i, str, str2);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
