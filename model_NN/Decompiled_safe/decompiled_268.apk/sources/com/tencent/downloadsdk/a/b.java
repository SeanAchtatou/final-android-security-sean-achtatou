package com.tencent.downloadsdk.a;

import com.tencent.downloadsdk.utils.f;

public class b {

    /* renamed from: a  reason: collision with root package name */
    public long f2451a;
    public long b;
    public long c;
    public long d;
    public long e;
    public long f;
    public long g;
    public long h;
    private long i;

    public b(long j) {
        f.d("speed", "SpeedProbe:" + this.f2451a);
        this.f2451a = j;
    }

    public long a(long j) {
        this.c += j;
        long currentTimeMillis = System.currentTimeMillis();
        long j2 = currentTimeMillis - this.i;
        if (this.e == 0) {
            this.e = (this.c * 1000) / j2;
        } else if (j2 >= this.f2451a) {
            this.e = (((this.c * 1000) / j2) + (this.e * 3)) / 4;
            this.c = 0;
            this.i = currentTimeMillis;
        }
        this.b += j;
        return this.e;
    }

    public void a() {
        this.f = System.currentTimeMillis();
        this.i = this.f;
    }

    public long b() {
        this.g = System.currentTimeMillis();
        long j = this.g - this.i;
        if (j != 0) {
            long j2 = (this.c * 1000) / j;
            if (this.e == 0) {
                this.e = j2;
            } else {
                this.e = (j2 + (this.e * 3)) / 4;
            }
        }
        this.h = this.g - this.f;
        if (this.h != 0) {
            this.d = (this.b * 1000) / this.h;
            f.b("SpeedProbe", "seg bytes:[" + this.b + "BYTE]  seg time:[" + this.h + "ms] seg speed:[" + (this.d / 1024) + "KB/s]");
        }
        return this.d;
    }
}
