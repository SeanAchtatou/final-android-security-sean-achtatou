package defpackage;

import android.content.Intent;
import com.tencent.securemodule.impl.SecureService;

/* renamed from: y  reason: default package */
public class y implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Intent f4165a;
    final /* synthetic */ SecureService b;

    public y(SecureService secureService, Intent intent) {
        this.b = secureService;
        this.f4165a = intent;
    }

    public void run() {
        try {
            if ("1000010".equals(this.f4165a.getAction())) {
                this.b.a();
                this.b.b();
                this.b.d();
            } else if ("1000011".equals(this.f4165a.getAction())) {
                boolean unused = this.b.e = this.f4165a.getBooleanExtra("key_download_listener", false);
                this.b.b("http://fwd.3g.qq.com:8080/forward.jsp?bid=882");
            }
            if (!this.b.d && this.b.b == 0) {
                this.b.stopSelf();
                this.b.c();
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }
}
