package defpackage;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import com.tencent.securemodule.impl.AppInfo;
import java.io.File;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: ar  reason: default package */
public class ar {
    public static AppInfo a(Context context, String str) {
        Exception exc;
        AppInfo appInfo;
        CertificateEncodingException certificateEncodingException;
        AppInfo appInfo2;
        PackageManager.NameNotFoundException nameNotFoundException;
        AppInfo appInfo3;
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(str, 64);
            AppInfo appInfo4 = new AppInfo();
            try {
                appInfo4.setPkgName(str);
                appInfo4.setVersionName(packageInfo.versionName);
                appInfo4.setVersionCode(packageInfo.versionCode);
                X509Certificate x509Certificate = (X509Certificate) a(packageInfo.signatures[0]);
                if (x509Certificate != null) {
                    appInfo4.setCertMd5(ay.a(ay.b(x509Certificate.getEncoded())));
                }
                return appInfo4;
            } catch (PackageManager.NameNotFoundException e) {
                PackageManager.NameNotFoundException nameNotFoundException2 = e;
                appInfo3 = appInfo4;
                nameNotFoundException = nameNotFoundException2;
                nameNotFoundException.printStackTrace();
                return appInfo3;
            } catch (CertificateEncodingException e2) {
                CertificateEncodingException certificateEncodingException2 = e2;
                appInfo2 = appInfo4;
                certificateEncodingException = certificateEncodingException2;
                certificateEncodingException.printStackTrace();
                return appInfo2;
            } catch (Exception e3) {
                Exception exc2 = e3;
                appInfo = appInfo4;
                exc = exc2;
                exc.printStackTrace();
                return appInfo;
            }
        } catch (PackageManager.NameNotFoundException e4) {
            nameNotFoundException = e4;
            appInfo3 = null;
            nameNotFoundException.printStackTrace();
            return appInfo3;
        } catch (CertificateEncodingException e5) {
            certificateEncodingException = e5;
            appInfo2 = null;
            certificateEncodingException.printStackTrace();
            return appInfo2;
        } catch (Exception e6) {
            exc = e6;
            appInfo = null;
            exc.printStackTrace();
            return appInfo;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:60:0x009e A[SYNTHETIC, Splitter:B:60:0x009e] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00a3 A[SYNTHETIC, Splitter:B:63:0x00a3] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x00b9 A[SYNTHETIC, Splitter:B:72:0x00b9] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x00be A[SYNTHETIC, Splitter:B:75:0x00be] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(java.lang.String r5) {
        /*
            r0 = 0
            r1 = 0
            java.util.zip.ZipFile r2 = new java.util.zip.ZipFile     // Catch:{ IOException -> 0x0096, all -> 0x00b3 }
            r2.<init>(r5)     // Catch:{ IOException -> 0x0096, all -> 0x00b3 }
            java.io.File r3 = new java.io.File     // Catch:{ IOException -> 0x00d2, all -> 0x00cc }
            r3.<init>(r5)     // Catch:{ IOException -> 0x00d2, all -> 0x00cc }
            boolean r3 = r3.exists()     // Catch:{ IOException -> 0x00d2, all -> 0x00cc }
            if (r3 != 0) goto L_0x0027
            if (r0 == 0) goto L_0x0017
            r1.close()     // Catch:{ IOException -> 0x001d }
        L_0x0017:
            if (r2 == 0) goto L_0x001c
            r2.close()     // Catch:{ IOException -> 0x0022 }
        L_0x001c:
            return r0
        L_0x001d:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0017
        L_0x0022:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x001c
        L_0x0027:
            java.lang.String r1 = "META-INF/MANIFEST.MF"
            java.util.zip.ZipEntry r1 = r2.getEntry(r1)     // Catch:{ IOException -> 0x00d2, all -> 0x00cc }
            if (r1 == 0) goto L_0x0080
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ IOException -> 0x00d2, all -> 0x00cc }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x00d2, all -> 0x00cc }
            java.io.InputStream r1 = r2.getInputStream(r1)     // Catch:{ IOException -> 0x00d2, all -> 0x00cc }
            r4.<init>(r1)     // Catch:{ IOException -> 0x00d2, all -> 0x00cc }
            r3.<init>(r4)     // Catch:{ IOException -> 0x00d2, all -> 0x00cc }
        L_0x003d:
            java.lang.String r1 = r3.readLine()     // Catch:{ IOException -> 0x00d5 }
            if (r1 == 0) goto L_0x0081
            java.lang.String r4 = "classes.dex"
            boolean r1 = r1.contains(r4)     // Catch:{ IOException -> 0x00d5 }
            if (r1 == 0) goto L_0x003d
            java.lang.String r1 = r3.readLine()     // Catch:{ IOException -> 0x00d5 }
            if (r1 == 0) goto L_0x003d
            java.lang.String r4 = "SHA1-Digest"
            boolean r4 = r1.contains(r4)     // Catch:{ IOException -> 0x00d5 }
            if (r4 == 0) goto L_0x003d
            java.lang.String r4 = ":"
            int r4 = r1.indexOf(r4)     // Catch:{ IOException -> 0x00d5 }
            if (r4 <= 0) goto L_0x003d
            int r4 = r4 + 1
            java.lang.String r1 = r1.substring(r4)     // Catch:{ IOException -> 0x00d5 }
            java.lang.String r0 = r1.trim()     // Catch:{ IOException -> 0x00d5 }
            if (r3 == 0) goto L_0x0070
            r3.close()     // Catch:{ IOException -> 0x007b }
        L_0x0070:
            if (r2 == 0) goto L_0x001c
            r2.close()     // Catch:{ IOException -> 0x0076 }
            goto L_0x001c
        L_0x0076:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x001c
        L_0x007b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0070
        L_0x0080:
            r3 = r0
        L_0x0081:
            if (r3 == 0) goto L_0x0086
            r3.close()     // Catch:{ IOException -> 0x0091 }
        L_0x0086:
            if (r2 == 0) goto L_0x001c
            r2.close()     // Catch:{ IOException -> 0x008c }
            goto L_0x001c
        L_0x008c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x001c
        L_0x0091:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0086
        L_0x0096:
            r1 = move-exception
            r2 = r0
            r3 = r0
        L_0x0099:
            r1.printStackTrace()     // Catch:{ all -> 0x00d0 }
            if (r3 == 0) goto L_0x00a1
            r3.close()     // Catch:{ IOException -> 0x00ae }
        L_0x00a1:
            if (r2 == 0) goto L_0x001c
            r2.close()     // Catch:{ IOException -> 0x00a8 }
            goto L_0x001c
        L_0x00a8:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x001c
        L_0x00ae:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00a1
        L_0x00b3:
            r1 = move-exception
            r2 = r0
            r3 = r0
            r0 = r1
        L_0x00b7:
            if (r3 == 0) goto L_0x00bc
            r3.close()     // Catch:{ IOException -> 0x00c2 }
        L_0x00bc:
            if (r2 == 0) goto L_0x00c1
            r2.close()     // Catch:{ IOException -> 0x00c7 }
        L_0x00c1:
            throw r0
        L_0x00c2:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00bc
        L_0x00c7:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00c1
        L_0x00cc:
            r1 = move-exception
            r3 = r0
            r0 = r1
            goto L_0x00b7
        L_0x00d0:
            r0 = move-exception
            goto L_0x00b7
        L_0x00d2:
            r1 = move-exception
            r3 = r0
            goto L_0x0099
        L_0x00d5:
            r1 = move-exception
            goto L_0x0099
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ar.a(java.lang.String):java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.security.cert.Certificate a(android.content.pm.Signature r3) {
        /*
            java.io.ByteArrayInputStream r2 = new java.io.ByteArrayInputStream
            byte[] r0 = r3.toByteArray()
            r2.<init>(r0)
            r1 = 0
            java.lang.String r0 = "X.509"
            java.security.cert.CertificateFactory r0 = java.security.cert.CertificateFactory.getInstance(r0)     // Catch:{ CertificateException -> 0x0021, Exception -> 0x0032 }
            java.security.cert.Certificate r0 = r0.generateCertificate(r2)     // Catch:{ CertificateException -> 0x0021, Exception -> 0x0032 }
            java.security.cert.X509Certificate r0 = (java.security.cert.X509Certificate) r0     // Catch:{ CertificateException -> 0x0021, Exception -> 0x0032 }
            if (r2 == 0) goto L_0x001b
            r2.close()     // Catch:{ IOException -> 0x001c }
        L_0x001b:
            return r0
        L_0x001c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x001b
        L_0x0021:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0043 }
            if (r2 == 0) goto L_0x004f
            r2.close()     // Catch:{ IOException -> 0x002c }
            r0 = r1
            goto L_0x001b
        L_0x002c:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r1
            goto L_0x001b
        L_0x0032:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0043 }
            if (r2 == 0) goto L_0x004f
            r2.close()     // Catch:{ IOException -> 0x003d }
            r0 = r1
            goto L_0x001b
        L_0x003d:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r1
            goto L_0x001b
        L_0x0043:
            r0 = move-exception
            if (r2 == 0) goto L_0x0049
            r2.close()     // Catch:{ IOException -> 0x004a }
        L_0x0049:
            throw r0
        L_0x004a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0049
        L_0x004f:
            r0 = r1
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ar.a(android.content.pm.Signature):java.security.cert.Certificate");
    }

    public static List<AppInfo> a(Context context, boolean z) {
        AppInfo a2;
        ArrayList arrayList = new ArrayList();
        PackageManager packageManager = context.getPackageManager();
        for (ApplicationInfo next : packageManager.getInstalledApplications(0)) {
            boolean z2 = (next.flags & 1) != 0;
            if ((!z2 || z) && (a2 = a(context, next.packageName)) != null) {
                a2.setSoftName(ay.a(packageManager.getApplicationLabel(next).toString()));
                a2.setAppType(z2 ? 1 : 0);
                a2.setFileSize(new File(next.sourceDir).length());
                a2.setApkPath(next.sourceDir);
                arrayList.add(a2);
                aw.a("CloudScan", "add app " + next.packageName);
            }
        }
        return arrayList;
    }

    public static boolean a(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 8192);
            return (applicationInfo == null || (applicationInfo.flags & 1) == 0) ? false : true;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e2) {
            return false;
        }
    }

    public static String b(Context context) {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
        StringBuffer stringBuffer = new StringBuffer();
        if (runningAppProcesses != null) {
            int i = 0;
            Iterator<ActivityManager.RunningAppProcessInfo> it = runningAppProcesses.iterator();
            while (true) {
                int i2 = i;
                if (!it.hasNext()) {
                    break;
                }
                ActivityManager.RunningAppProcessInfo next = it.next();
                if (next != null) {
                    stringBuffer.append(next.processName);
                    stringBuffer.append(";");
                    i = i2 + 1;
                    if (i > 100) {
                        break;
                    }
                } else {
                    i = i2;
                }
            }
            if (stringBuffer.length() > 0) {
                stringBuffer.deleteCharAt(stringBuffer.length() - 1);
            }
        }
        return stringBuffer.toString();
    }
}
