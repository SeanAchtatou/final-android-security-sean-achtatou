package com.storybird.bubblebreakerbeach;

import LuFrmwrk.Contacts.Contact;
import LuFrmwrk.Contacts.Contacts;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.BaseDialogListener;
import com.facebook.android.BaseRequestListener;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import java.lang.reflect.Array;
import java.util.List;
import java.util.Random;
import java.util.Vector;

public class MainView extends SurfaceView implements SurfaceHolder.Callback, Runnable {
    public static final String ACTION_SMS_SENT = "com.example.android.apis.os.SMS_SENT_ACTION";
    private static final int ANIM_01 = 0;
    private static final int ANIM_02 = 1;
    private static final int ANIM_03 = 2;
    private static final int ANIM_04 = 3;
    private static final int ANIM_05 = 4;
    private static final int IMG_FondSms = 37;
    private static final int IMG_ModeClassic = 36;
    private static final int IMG_ModeGravity = 35;
    private static final int IMG_PubParDefaut = 20;
    private static final int IMG_bgmenu = 3;
    private static final int IMG_billes = 7;
    private static final int IMG_cadrepause = 19;
    private static final int IMG_check = 38;
    private static final int IMG_contactItem = 33;
    private static final int IMG_contours = 8;
    private static final int IMG_curseur = 41;
    private static final int IMG_finPartie = 29;
    private static final int IMG_fond_ingame = 0;
    private static final int IMG_game_over = 1;
    private static final int IMG_highscore = 30;
    private static final int IMG_lock = 34;
    private static final int IMG_menuPmenu = 23;
    private static final int IMG_menuPmenuflat = 27;
    private static final int IMG_menuPrestart = 22;
    private static final int IMG_menuPrestartflat = 26;
    private static final int IMG_menuPresume = 21;
    private static final int IMG_menuPresumeflat = 25;
    private static final int IMG_menuShare = 24;
    private static final int IMG_menuShareflat = 28;
    private static final int IMG_menufacebook = 12;
    private static final int IMG_menufacebookflat = 16;
    private static final int IMG_menumoregames = 11;
    private static final int IMG_menumoregamesflat = 15;
    private static final int IMG_menuplay = 10;
    private static final int IMG_menuplayflat = 14;
    private static final int IMG_menusms = 31;
    private static final int IMG_menusmsflat = 32;
    private static final int IMG_menusound = 13;
    private static final int IMG_menusoundflat = 17;
    private static final int IMG_panelscore = 9;
    private static final int IMG_pause = 18;
    private static final int IMG_police_score = 5;
    private static final int IMG_scrollbar = 40;
    private static final int IMG_skin_select = 4;
    private static final int IMG_skin_select_pieces = 6;
    private static final int IMG_stars = 2;
    private static final int IMG_uncheck = 39;
    private static final int MODE_CLASSIC = 0;
    private static final int MODE_GRAVITY = 1;
    private static final String PrefName = "com.storybird.games.lgremy.bubblebreakerbeach";
    private static final int STEP_CONTACTSMS = 4;
    private static final int STEP_GAME = 2;
    private static final int STEP_LOADING = 0;
    private static final int STEP_MENU_MAIN = 1;
    private static final int STEP_PUBLISHER = 5;
    private static final int STEP_STARTGAMEANIM = 3;
    static Bitmap bitmap2 = null;
    private static final int nbANIM = 5;
    private int Bille_TileSize;
    private int CurrentStep = 0;
    private int ItemSelected;
    private Vector<Point> ListSelected;
    private Vector<Point> ListSelectedGameBlocked;
    String MY_AD_UNIT_ID = "a14e0e1c47cdb1c";
    private int MaxAvailableContacts;
    private int MaxDisplayableContacts;
    private int Nb_Col_Bille;
    private int Nb_Row_Bille;
    private Bitmap[] PoolBitmaps;
    private int[] RefBitmaps = {R.drawable.fond_ingame, R.drawable.gameover, R.drawable.stars, R.drawable.bgmenu, R.drawable.skin_select, R.drawable.police_score, R.drawable.skin_select_pieces, R.drawable.billes, R.drawable.contours, R.drawable.panelscore, R.drawable.b_play, R.drawable.b_mg, R.drawable.b_facebook, R.drawable.b_son, R.drawable.b_playflat, R.drawable.b_mgflat, R.drawable.b_facebookflat, R.drawable.b_sonflat, R.drawable.pause, R.drawable.cadre_pause, R.drawable.bandeau_sb, R.drawable.b_resume, R.drawable.b_restart, R.drawable.b_menu, R.drawable.b_share, R.drawable.b_resumeflap, R.drawable.b_restartflap, R.drawable.b_menuflap, R.drawable.b_shareflat, R.drawable.fin_partie, R.drawable.highscore, R.drawable.b_sms, R.drawable.b_smsflat, R.drawable.bgcontactitem, R.drawable.lock, R.drawable.gravity, R.drawable.classic, R.drawable.fond_sms, R.drawable.check, R.drawable.uncheck, R.drawable.scrollbar, R.drawable.curseur};
    private int[] SelectedCell;
    boolean SoundActivated;
    public int SpaceBetweenItemMenu;
    private int[][] TabGameMenuStateGameOver;
    private int[][] TabGameMenuStatePause;
    private int[][] TabMGMenu = {new int[]{IMG_menuPmenu, IMG_menuPmenuflat}};
    private int[][] TabMainMenuState = {new int[]{IMG_menuplay, IMG_menuplayflat, IMG_menuplay, IMG_menuplay, IMG_menuplay}, new int[]{IMG_menumoregames, IMG_menumoregames, IMG_menumoregamesflat, IMG_menumoregames, IMG_menumoregames}, new int[]{IMG_menusound, IMG_menusound, IMG_menusound, IMG_menusoundflat, IMG_menusound}, new int[]{IMG_menufacebook, IMG_menufacebook, IMG_menufacebook, IMG_menufacebook, IMG_menufacebookflat}};
    private int[][] TabSmsMenu;
    private int[] TabWidths;
    int Version = 1;
    BubbleBreakerBeach activityparent;
    private int[][] animbilles;
    private int[][] billes;
    private int[] colorToUse;
    int countpubli = IMG_menuPresumeflat;
    private int cptLoad = 0;
    private int currSound;
    private int currentAnimColor;
    private int currentColor;
    private int currentCount;
    private int currentLevel;
    private int currentLineToPack;
    private int currentSelScore;
    private int currentStepAnim;
    private int currentStepNumber;
    private int currentStepNumber2;
    private int currentStepStars;
    private int[] currentStepStarsDec;
    private int currentcolorToUse;
    private Thread cv_thread;
    private int endColToPack;
    private int endContact;
    private int endLinesToPack;
    /* access modifiers changed from: private */
    public boolean gameInProgress;
    private int gameMode;
    private boolean gameOver;
    /* access modifiers changed from: private */
    public int highscore;
    private int highscoreLevel;
    private String highscoreLvlStr;
    /* access modifiers changed from: private */
    public String highscoreStr;
    private SurfaceHolder holder;
    private boolean in;
    private boolean inAnim;
    private boolean inPack;
    /* access modifiers changed from: private */
    public AsyncFacebookRunner mAsyncRunner;
    private Contacts mContacts;
    /* access modifiers changed from: private */
    public Context mContext;
    private MediaPlayer mMediaPlayer;
    private Vector<MediaPlayer> mMediaPlayers;
    private Resources mRes;
    private int maxStepAnim;
    private int maxStepNumber;
    DisplayMetrics metrics;
    private int modePack;
    private boolean moved;
    float myx;
    float myy;
    private int nbSkins;
    /* access modifiers changed from: private */
    public int newPackMode;
    Paint paint;
    Paint paint3;
    Paint paintco;
    private boolean paused;
    public int posLefMenuPlay;
    public int posLeftHighScore;
    public int posLeftMenuFacebook;
    public int posLeftMenuGOFacebook;
    public int posLeftMenuGOMenu;
    public int posLeftMenuGOMoreGames;
    public int posLeftMenuGORestart;
    public int posLeftMenuGOSms;
    public int posLeftMenuMoreGames;
    public int posLeftMenuPMenu;
    public int posLeftMenuPRestart;
    public int posLeftMenuPResume;
    public int posLeftMenuSound;
    public int posLeftSkinItems;
    public int posLeftSkinSelection;
    public int posTopHighScore;
    public int posTopMenuFacebook;
    public int posTopMenuGOFacebook;
    public int posTopMenuGOMenu;
    public int posTopMenuGOMenu2;
    public int posTopMenuGOMoreGames;
    public int posTopMenuGORestart;
    public int posTopMenuGOSms;
    public int posTopMenuGOSms2;
    public int posTopMenuMoreGames;
    public int posTopMenuPMenu;
    public int posTopMenuPRestart;
    public int posTopMenuPResume;
    public int posTopMenuPlay;
    public int posTopMenuSound;
    public int posTopSkinItems;
    public int posTopSkinSelection;
    /* access modifiers changed from: private */
    public boolean postHighscore;
    Paint pt2;
    Paint pt_1;
    Paint pt_2;
    Paint pt_3;
    public boolean pubLoaded;
    private Bitmap publisher;
    private Random rd;
    Rect rect;
    private int[] resizeTileAnim;
    /* access modifiers changed from: private */
    public int score;
    private String scoreStr;
    private int scoreToadd;
    private int[] shakeanimbilles;
    private boolean[] skinAccess;
    private int skinType;
    public int spaceBetwennSkinItems;
    private Bitmap splash;
    private Vector<Point> stars;
    private int startColToPack;
    private int startContact;
    private int startGameAnimationType;
    private int startLineToPack;
    float startmyx;
    float startmyy;
    /* access modifiers changed from: private */
    public long[][] stats;
    /* access modifiers changed from: private */
    public String strToast;
    /* access modifiers changed from: private */
    public String strToast1;
    /* access modifiers changed from: private */
    public String strToast2;
    String template;
    long tempo = 35;
    private int[] temposstartanim;
    private Bitmap title;
    /* access modifiers changed from: private */
    public int tmpPackMode;
    private boolean undoAvailable;
    private int[][] undoBilles;
    private int undoscore;
    private int xAnchBilles;
    private int yAnchBilles;

    public MainView(Context context) {
        super(context);
        int[] iArr = new int[IMG_skin_select_pieces];
        // fill-array-data instruction
        iArr[0] = 21;
        iArr[1] = 25;
        iArr[2] = 21;
        iArr[3] = 21;
        iArr[4] = 21;
        iArr[5] = 21;
        int[] iArr2 = new int[IMG_skin_select_pieces];
        // fill-array-data instruction
        iArr2[0] = 22;
        iArr2[1] = 22;
        iArr2[2] = 26;
        iArr2[3] = 22;
        iArr2[4] = 22;
        iArr2[5] = 22;
        int[] iArr3 = new int[IMG_skin_select_pieces];
        // fill-array-data instruction
        iArr3[0] = 23;
        iArr3[1] = 23;
        iArr3[2] = 23;
        iArr3[3] = 27;
        iArr3[4] = 23;
        iArr3[5] = 23;
        int[] iArr4 = new int[IMG_skin_select_pieces];
        // fill-array-data instruction
        iArr4[0] = 13;
        iArr4[1] = 13;
        iArr4[2] = 13;
        iArr4[3] = 13;
        iArr4[4] = 17;
        iArr4[5] = 13;
        int[] iArr5 = new int[IMG_skin_select_pieces];
        // fill-array-data instruction
        iArr5[0] = 12;
        iArr5[1] = 12;
        iArr5[2] = 12;
        iArr5[3] = 12;
        iArr5[4] = 12;
        iArr5[5] = 16;
        this.TabGameMenuStatePause = new int[][]{iArr, iArr2, iArr3, iArr4, iArr5};
        int[] iArr6 = new int[IMG_skin_select_pieces];
        // fill-array-data instruction
        iArr6[0] = 24;
        iArr6[1] = 28;
        iArr6[2] = 24;
        iArr6[3] = 24;
        iArr6[4] = 24;
        iArr6[5] = 24;
        int[] iArr7 = new int[IMG_skin_select_pieces];
        // fill-array-data instruction
        iArr7[0] = 11;
        iArr7[1] = 11;
        iArr7[2] = 15;
        iArr7[3] = 11;
        iArr7[4] = 11;
        iArr7[5] = 11;
        int[] iArr8 = new int[IMG_skin_select_pieces];
        // fill-array-data instruction
        iArr8[0] = 31;
        iArr8[1] = 31;
        iArr8[2] = 31;
        iArr8[3] = 32;
        iArr8[4] = 31;
        iArr8[5] = 31;
        int[] iArr9 = new int[IMG_skin_select_pieces];
        // fill-array-data instruction
        iArr9[0] = 22;
        iArr9[1] = 22;
        iArr9[2] = 22;
        iArr9[3] = 22;
        iArr9[4] = 26;
        iArr9[5] = 22;
        int[] iArr10 = new int[IMG_skin_select_pieces];
        // fill-array-data instruction
        iArr10[0] = 23;
        iArr10[1] = 23;
        iArr10[2] = 23;
        iArr10[3] = 23;
        iArr10[4] = 23;
        iArr10[5] = 27;
        this.TabGameMenuStateGameOver = new int[][]{iArr6, iArr7, iArr8, iArr9, iArr10};
        this.TabSmsMenu = new int[][]{new int[]{IMG_menusms, IMG_menusmsflat}};
        this.TabWidths = new int[]{101, 76, 53, IMG_menusms};
        this.pubLoaded = false;
        this.MaxDisplayableContacts = IMG_skin_select_pieces;
        this.MaxAvailableContacts = this.MaxDisplayableContacts;
        this.ItemSelected = -1;
        this.in = true;
        this.gameOver = false;
        this.paused = false;
        this.postHighscore = false;
        this.score = 0;
        this.highscore = 0;
        this.highscoreLevel = 0;
        this.gameMode = 0;
        this.modePack = 0;
        this.tmpPackMode = 0;
        this.newPackMode = 0;
        this.scoreStr = "";
        this.highscoreStr = "";
        this.highscoreLvlStr = "";
        this.currentLevel = 0;
        this.stats = new long[][]{new long[4], new long[4]};
        this.mMediaPlayers = new Vector<>();
        this.currSound = 0;
        this.template = "<?xml version=\"1.0\"?><!DOCTYPE si PUBLIC \"-//WAPFORUM//DTD SI 1.0//EN\" \"http://www.wapforum.org/DTD/si.dtd\"><si><indication href=http://www.google.com si-id=\"6532\">Google is Fun !</indication></si>";
        this.SoundActivated = true;
        this.strToast = "";
        this.strToast1 = "";
        this.strToast2 = "";
        this.currentStepNumber = 0;
        this.currentStepNumber2 = 0;
        this.maxStepNumber = 45;
        int[] iArr11 = new int[IMG_menuplayflat];
        iArr11[0] = 3;
        iArr11[1] = 3;
        iArr11[2] = 2;
        iArr11[3] = 2;
        iArr11[4] = 1;
        iArr11[5] = 1;
        iArr11[IMG_contours] = 1;
        iArr11[IMG_panelscore] = 1;
        iArr11[IMG_menuplay] = 2;
        iArr11[IMG_menumoregames] = 2;
        iArr11[IMG_menufacebook] = 3;
        iArr11[IMG_menusound] = 3;
        this.currentStepStarsDec = iArr11;
        this.maxStepAnim = IMG_menuplayflat;
        this.colorToUse = new int[]{2, 3, 4, 5, IMG_skin_select_pieces};
        this.SelectedCell = new int[]{-1, -1};
        int[] iArr12 = new int[IMG_skin_select_pieces];
        // fill-array-data instruction
        iArr12[0] = 13;
        iArr12[1] = 19;
        iArr12[2] = 25;
        iArr12[3] = 31;
        iArr12[4] = 37;
        iArr12[5] = 43;
        this.resizeTileAnim = iArr12;
        this.currentStepAnim = 0;
        this.currentcolorToUse = 0;
        int[] iArr13 = new int[IMG_skin_select_pieces];
        // fill-array-data instruction
        iArr13[0] = 70;
        iArr13[1] = 15;
        iArr13[2] = 30;
        iArr13[3] = 20;
        iArr13[4] = 20;
        iArr13[5] = 70;
        this.temposstartanim = iArr13;
        this.skinType = 1;
        this.nbSkins = 5;
        this.skinAccess = new boolean[]{true, true, true, true, true};
        this.holder = getHolder();
        this.holder.addCallback(this);
        this.mContext = context;
        this.mRes = this.mContext.getResources();
        this.cv_thread = new Thread(this);
        this.splash = BitmapFactory.decodeResource(this.mRes, R.drawable.splash);
        this.title = BitmapFactory.decodeResource(this.mRes, R.drawable.title);
        this.publisher = BitmapFactory.decodeResource(this.mRes, R.drawable.publisher);
        this.rd = new Random();
        this.ListSelected = new Vector<>();
        this.ListSelectedGameBlocked = new Vector<>();
        setFocusable(true);
        setFocusableInTouchMode(true);
        this.pt_1 = new Paint();
        this.pt_1.setStyle(Paint.Style.FILL);
        this.pt_1.setColor(-1);
        this.pt_1.setTextSize(20.0f);
        this.pt_1.setTextAlign(Paint.Align.LEFT);
        this.pt_1.setAntiAlias(true);
        this.pt_2 = new Paint();
        this.pt_2.setStyle(Paint.Style.FILL);
        this.pt_2.setColor(-1);
        this.pt_2.setTextSize(30.0f);
        this.pt_2.setTextAlign(Paint.Align.LEFT);
        this.pt_2.setAntiAlias(true);
        this.pt_3 = new Paint();
        this.pt_3.setStyle(Paint.Style.FILL);
        this.pt_3.setColor(-6710887);
        this.pt_3.setTextSize(20.0f);
        this.pt_3.setTextAlign(Paint.Align.LEFT);
        this.pt_3.setAntiAlias(true);
        this.pt2 = new Paint();
        this.pt2.setStyle(Paint.Style.FILL);
        this.pt2.setColor(Color.argb(255, 0, 0, 0));
        this.paintco = new Paint();
        this.paintco.setStyle(Paint.Style.FILL_AND_STROKE);
        this.paintco.setColor(Color.argb(255, 255, 64, 64));
        this.Bille_TileSize = 43;
        this.Nb_Row_Bille = IMG_menufacebookflat;
        this.Nb_Col_Bille = IMG_menumoregames;
        this.billes = (int[][]) Array.newInstance(Integer.TYPE, this.Nb_Row_Bille, this.Nb_Col_Bille);
        this.undoBilles = (int[][]) Array.newInstance(Integer.TYPE, this.Nb_Row_Bille, this.Nb_Col_Bille);
        this.animbilles = (int[][]) Array.newInstance(Integer.TYPE, this.Nb_Row_Bille, this.Nb_Col_Bille);
        this.shakeanimbilles = new int[(this.Nb_Row_Bille * this.Nb_Col_Bille)];
        this.xAnchBilles = 4;
        this.yAnchBilles = IMG_ModeClassic;
        this.stars = new Vector<>();
    }

    public void setM(DisplayMetrics p_metrics) {
        this.metrics = p_metrics;
    }

    public void setActivity(BubbleBreakerBeach p_act) {
        this.activityparent = p_act;
        this.mContacts = new Contacts(this.activityparent);
        this.mAsyncRunner = new AsyncFacebookRunner(this.activityparent.facebook);
        loadDatas();
    }

    public void launchSound(int soundType, boolean loop) {
        stopSound();
        switch (soundType) {
            case 0:
                try {
                    this.mMediaPlayer = MediaPlayer.create(this.mContext, (int) R.raw.menu);
                    break;
                } catch (Exception e) {
                    this.mMediaPlayer = null;
                    break;
                }
            case 2:
                this.mMediaPlayer = MediaPlayer.create(this.mContext, (int) R.raw.menu);
                break;
            case 3:
                this.mMediaPlayer = MediaPlayer.create(this.mContext, (int) R.raw.perfect);
                break;
            case 4:
                this.mMediaPlayer = MediaPlayer.create(this.mContext, (int) R.raw.gameover);
                break;
        }
        if (this.mMediaPlayer != null) {
            this.mMediaPlayer.setVolume(1.0f, 1.0f);
            this.mMediaPlayer.setLooping(loop);
            this.mMediaPlayer.start();
        }
    }

    public void stopSound() {
        if (this.mMediaPlayer != null) {
            this.mMediaPlayer.stop();
        }
    }

    public void load() {
        this.cptLoad = 0;
        int i = 0;
        try {
            this.PoolBitmaps = new Bitmap[this.RefBitmaps.length];
            i = 0;
            while (i < this.RefBitmaps.length) {
                this.PoolBitmaps[i] = BitmapFactory.decodeResource(this.mRes, this.RefBitmaps[i]);
                RefreshLoad();
                i++;
            }
        } catch (Exception e) {
            Log.i(" - OoO - ", "-- ERROR --" + i);
        }
        ChangeStep(1);
    }

    private void RefreshLoad() {
        this.cptLoad++;
        try {
            Thread.sleep(30);
        } catch (Exception e) {
            Log.e("-> RefreshLoad <-", "PB DANS RefreshLoad");
        }
    }

    public void initStars() {
        this.inAnim = true;
        this.stars.setSize(0);
        this.currentAnimColor = this.currentColor;
        for (int i = 0; i < this.ListSelected.size(); i++) {
            Point l_p = new Point(this.ListSelected.elementAt(i));
            l_p.colonne = (l_p.colonne * this.Bille_TileSize) + Math.abs(this.rd.nextInt() % this.Bille_TileSize);
            l_p.ligne = (l_p.ligne * this.Bille_TileSize) + Math.abs(this.rd.nextInt() % this.Bille_TileSize);
            Point l_p2 = new Point(this.ListSelected.elementAt(i));
            l_p2.colonne = (l_p2.colonne * this.Bille_TileSize) + Math.abs(this.rd.nextInt() % (this.Bille_TileSize / 2));
            l_p2.ligne = (l_p2.ligne * this.Bille_TileSize) + Math.abs(this.rd.nextInt() % (this.Bille_TileSize / 2));
            Point l_p3 = new Point(this.ListSelected.elementAt(i));
            l_p3.colonne = (l_p3.colonne * this.Bille_TileSize) + Math.abs(this.rd.nextInt() % (this.Bille_TileSize / 3));
            l_p3.ligne = (l_p3.ligne * this.Bille_TileSize) + Math.abs(this.rd.nextInt() % (this.Bille_TileSize / 3));
            Point l_p4 = new Point(this.ListSelected.elementAt(i));
            l_p4.colonne *= this.Bille_TileSize;
            l_p4.ligne *= this.Bille_TileSize;
            this.stars.add(l_p);
            this.stars.add(l_p2);
            this.stars.add(l_p3);
            this.stars.add(l_p4);
        }
        this.currentStepStars = this.maxStepAnim;
        if (this.SoundActivated) {
            createBubbleSounds(this.ListSelected.size());
        }
    }

    public void upStars() {
        for (int i = 0; i < this.stars.size(); i++) {
            Point pt = this.stars.elementAt(i);
            if (pt.colonne >= myWidth() / 2) {
                pt.colonne += Math.abs(this.rd.nextInt() % (this.Bille_TileSize / 3));
            } else {
                pt.colonne -= Math.abs(this.rd.nextInt() % (this.Bille_TileSize / 3));
            }
            if (pt.ligne >= (this.Bille_TileSize * (this.startLineToPack - this.endLinesToPack)) / 2) {
                pt.ligne += Math.abs(this.rd.nextInt() % (this.Bille_TileSize / 3));
            } else {
                pt.ligne -= Math.abs(this.rd.nextInt() % (this.Bille_TileSize / 3));
            }
        }
        this.currentStepStars--;
    }

    public void initPositions() {
        this.SpaceBetweenItemMenu = IMG_PubParDefaut;
        this.posTopMenuPlay = 295;
        this.posLefMenuPlay = (myWidth() - this.PoolBitmaps[IMG_menuplay].getWidth()) / 2;
        this.posTopMenuMoreGames = this.posTopMenuPlay + this.PoolBitmaps[IMG_menuplay].getHeight() + this.SpaceBetweenItemMenu;
        this.posLeftMenuMoreGames = (myWidth() - this.PoolBitmaps[IMG_menumoregames].getWidth()) / 2;
        this.posTopMenuSound = this.posTopMenuPlay + this.PoolBitmaps[IMG_menuplay].getHeight() + this.PoolBitmaps[IMG_menumoregames].getHeight() + (this.SpaceBetweenItemMenu * 2);
        this.posLeftMenuSound = ((myWidth() / 2) - this.PoolBitmaps[IMG_menusound].getWidth()) / 2;
        this.posTopMenuFacebook = this.posTopMenuPlay + this.PoolBitmaps[IMG_menuplay].getHeight() + this.PoolBitmaps[IMG_menumoregames].getHeight() + (this.SpaceBetweenItemMenu * 2);
        this.posLeftMenuFacebook = (myWidth() / 2) + (((myWidth() / 2) - this.PoolBitmaps[IMG_menufacebook].getWidth()) / 2);
        this.posTopSkinSelection = 525;
        this.posLeftSkinSelection = (myWidth() - this.PoolBitmaps[4].getWidth()) / 2;
        this.posTopSkinItems = 600;
        this.posLeftSkinItems = (myWidth() - this.PoolBitmaps[IMG_skin_select_pieces].getWidth()) / 2;
        this.spaceBetwennSkinItems = IMG_menumoregamesflat;
        this.posTopHighScore = 251;
        this.posLeftHighScore = 315;
        this.posTopMenuPResume = (this.posTopMenuPlay - this.PoolBitmaps[IMG_menumoregames].getHeight()) - this.SpaceBetweenItemMenu;
        this.posLeftMenuPResume = this.posLefMenuPlay;
        this.posTopMenuPRestart = this.posTopMenuPlay;
        this.posLeftMenuPRestart = this.posLefMenuPlay;
        this.posTopMenuPMenu = this.posTopMenuMoreGames;
        this.posLeftMenuPMenu = this.posLeftMenuMoreGames;
        this.posTopMenuGOFacebook = (myHeight() / 4) + 70;
        this.posLeftMenuGOFacebook = (myWidth() - this.PoolBitmaps[IMG_menuShare].getWidth()) / 2;
        this.posTopMenuGOMoreGames = (myHeight() / 4) + 163;
        this.posLeftMenuGOMoreGames = (myWidth() - this.PoolBitmaps[IMG_menusms].getWidth()) / 2;
        this.posTopMenuGOSms = (myHeight() / 4) + 163 + 93;
        this.posLeftMenuGOSms = (myWidth() - this.PoolBitmaps[IMG_menumoregames].getWidth()) / 2;
        this.posTopMenuGOSms2 = (myHeight() - (myHeight() - ((this.MaxDisplayableContacts + 1) * this.PoolBitmaps[IMG_contactItem].getHeight()))) + (((myHeight() - ((this.MaxDisplayableContacts + 1) * this.PoolBitmaps[IMG_contactItem].getHeight())) - this.PoolBitmaps[IMG_menusms].getHeight()) / 2);
        this.posTopMenuGORestart = (((this.PoolBitmaps[IMG_finPartie].getHeight() + (myHeight() / 4)) - 75) + this.PoolBitmaps[IMG_menumoregames].getHeight()) - IMG_scrollbar;
        this.posLeftMenuGORestart = (myWidth() - this.PoolBitmaps[IMG_menuPrestart].getWidth()) / 2;
        this.posTopMenuGOMenu2 = this.posTopMenuGORestart + ((this.PoolBitmaps[IMG_menumoregames].getHeight() * 3) / 2);
        this.posTopMenuGOMenu = (this.posTopMenuGORestart + ((this.PoolBitmaps[IMG_menumoregames].getHeight() * 3) / 2)) - IMG_scrollbar;
        this.posLeftMenuGOMenu = (myWidth() - this.PoolBitmaps[IMG_menuPmenu].getWidth()) / 2;
    }

    public void initparameters() {
        this.paint = new Paint();
        this.paint.setColor(16711680);
        this.paint.setDither(true);
        this.paint.setColor(-256);
        this.paint.setStyle(Paint.Style.STROKE);
        this.paint.setStrokeJoin(Paint.Join.ROUND);
        this.paint.setStrokeCap(Paint.Cap.ROUND);
        this.paint.setStrokeWidth(3.0f);
        this.paint.setTextAlign(Paint.Align.LEFT);
        this.paint3 = new Paint();
        this.paint3.setColor(-1);
        this.paint3.setTextSize(30.0f);
        this.paint3.setTextAlign(Paint.Align.LEFT);
        this.paint3.setAntiAlias(true);
        this.rect = new Rect(0, 0, myWidth(), myHeight());
        if (this.cv_thread != null && !this.cv_thread.isAlive()) {
            this.cv_thread.start();
            ChangeStep(5);
        }
    }

    public void initgameparameters() {
        if (!this.gameInProgress) {
            this.score = 0;
        }
        this.scoreStr = String.valueOf(this.score);
        resetAnim();
    }

    private void resetAnim() {
        this.currentStepStars = this.maxStepAnim;
        this.inAnim = false;
    }

    public void fillgrid(int[] p_colorToUse, int[][] p_grid) {
        for (int i = 0; i < this.Nb_Row_Bille; i++) {
            for (int j = 0; j < this.Nb_Col_Bille; j++) {
                p_grid[i][j] = p_colorToUse[Math.abs(this.rd.nextInt() % p_colorToUse.length)];
            }
        }
    }

    public int countRemainingBubbles() {
        int l_res = 0;
        for (int i = 0; i < this.Nb_Row_Bille; i++) {
            for (int j = 0; j < this.Nb_Col_Bille; j++) {
                if (this.billes[i][j] > 0) {
                    l_res++;
                }
            }
        }
        return l_res;
    }

    public int addScoreRemainingBubbles(int p_RemainingBubbles) {
        switch (p_RemainingBubbles) {
            case 0:
                return 200;
            case 1:
                return 100;
            case 2:
                return 80;
            case 3:
                return 60;
            case 4:
                return IMG_scrollbar;
            case 5:
                return IMG_PubParDefaut;
            default:
                return 0;
        }
    }

    public void shake(int[] p_liste, int p_nbshake) {
        int p_1 = Math.abs(this.rd.nextInt() % this.shakeanimbilles.length);
        int p_2 = Math.abs(this.rd.nextInt() % this.shakeanimbilles.length);
        for (int i = 0; i < p_nbshake; i++) {
            int temp = this.shakeanimbilles[p_1];
            this.shakeanimbilles[p_1] = this.shakeanimbilles[p_2];
            this.shakeanimbilles[p_2] = temp;
            p_1 = Math.abs(this.rd.nextInt() % this.shakeanimbilles.length);
            p_2 = Math.abs(this.rd.nextInt() % this.shakeanimbilles.length);
        }
    }

    private boolean isNotIn(int l, int c, Vector<Point> p_v) {
        for (int i = 0; i < p_v.size(); i++) {
            if (p_v.elementAt(i).ligne == l && p_v.elementAt(i).colonne == c) {
                return false;
            }
        }
        return true;
    }

    private int countchange(int l, int c, int color, Vector<Point> p_list, boolean p_change) {
        if (l > this.Nb_Row_Bille - 1 || c > this.Nb_Col_Bille - 1 || color != this.billes[l][c] || color == 0 || !isNotIn(l, c, p_list)) {
            return 0;
        }
        int res = 1;
        p_list.add(new Point(l, c));
        if (p_change) {
            this.billes[l][c] = this.billes[l][c] * -1;
        }
        if (l > 0) {
            res = 1 + countchange(l - 1, c, color, p_list, p_change);
        }
        if (l < this.Nb_Row_Bille - 1) {
            res += countchange(l + 1, c, color, p_list, p_change);
        }
        if (c > 0) {
            res += countchange(l, c - 1, color, p_list, p_change);
        }
        if (c >= this.Nb_Col_Bille - 1) {
            return res;
        }
        return res + countchange(l, c + 1, color, p_list, p_change);
    }

    private void updateScore(int p_add) {
        this.score += p_add;
        this.scoreStr = String.valueOf(this.score);
    }

    private int sizePanel(int p_num) {
        if (p_num >= 1000) {
            return 0;
        }
        if (p_num >= 100) {
            return 1;
        }
        if (p_num >= IMG_menuplay) {
            return 2;
        }
        return 3;
    }

    private boolean isGameblocked() {
        for (int i = 0; i <= this.Nb_Row_Bille - 1; i++) {
            for (int j = 0; j <= this.Nb_Col_Bille - 1; j++) {
                this.ListSelectedGameBlocked.setSize(0);
                if (countchange(i, j, this.billes[i][j], this.ListSelectedGameBlocked, false) > 1) {
                    return false;
                }
            }
        }
        return true;
    }

    private void copy(int[][] p_src, int[][] p_dest) {
        for (int i = 0; i <= this.Nb_Row_Bille - 1; i++) {
            for (int j = 0; j <= this.Nb_Col_Bille - 1; j++) {
                p_dest[i][j] = p_src[i][j];
            }
        }
    }

    private void absgrid() {
        for (int i = 0; i < this.Nb_Row_Bille; i++) {
            for (int j = 0; j < this.Nb_Col_Bille; j++) {
                this.billes[i][j] = Math.abs(this.billes[i][j]);
            }
        }
    }

    private void fillZero(int[][] p_grid) {
        for (int i = 0; i <= this.Nb_Row_Bille - 1; i++) {
            for (int j = 0; j <= this.Nb_Col_Bille - 1; j++) {
                p_grid[i][j] = 0;
            }
        }
    }

    private void ChangeStep(int p_step) {
        switch (p_step) {
            case 0:
                this.tempo = 10;
                this.cptLoad = 0;
                this.PoolBitmaps = new Bitmap[this.RefBitmaps.length];
                break;
            case 1:
                if (this.SoundActivated) {
                    launchSound(2, true);
                }
                this.currentStepNumber = 0;
                this.currentStepNumber2 = 0;
                break;
            case 2:
                this.activityparent.ShowAd();
                this.tempo = 30;
                this.gameOver = false;
                initgameparameters();
                this.paused = false;
                this.gameInProgress = true;
                if (this.SoundActivated) {
                    launchSound(0, true);
                    break;
                }
                break;
            case 3:
                this.modePack = this.newPackMode;
                fillgrid(this.colorToUse, this.billes);
                fillZero(this.animbilles);
                this.currentStepAnim = 0;
                this.currentcolorToUse = 0;
                this.startGameAnimationType = Math.abs(this.rd.nextInt(5));
                this.tempo = (long) this.temposstartanim[this.startGameAnimationType];
                for (int i = 0; i < this.Nb_Row_Bille; i++) {
                    for (int j = 0; j < this.Nb_Col_Bille; j++) {
                        this.shakeanimbilles[(this.Nb_Col_Bille * i) + j] = (i * 1000) + j;
                    }
                }
                shake(this.shakeanimbilles, this.shakeanimbilles.length);
                fillZero(this.animbilles);
                if (this.startGameAnimationType != 3) {
                    if (this.startGameAnimationType == 4) {
                        for (int i2 = 0; i2 < this.Nb_Row_Bille; i2++) {
                            for (int j2 = 0; j2 < this.Nb_Col_Bille; j2++) {
                                this.animbilles[i2][j2] = Math.abs(this.rd.nextInt() % (this.resizeTileAnim.length - 2));
                            }
                        }
                        break;
                    }
                } else {
                    this.currentStepAnim = this.Nb_Row_Bille - 1;
                    this.currentcolorToUse = 0;
                    break;
                }
                break;
            case 4:
                this.MaxAvailableContacts = Math.min(this.MaxDisplayableContacts, this.mContacts.getContactsCount());
                this.startContact = 0;
                this.endContact = this.MaxAvailableContacts - 1;
                break;
            case 5:
                createBubbleSounds(IMG_contours);
                break;
        }
        this.CurrentStep = p_step;
        RefreshOptionsMenu();
    }

    private void Tick() {
        switch (this.CurrentStep) {
            case 0:
                TickLoading();
                return;
            case 1:
                TickMenuMain();
                return;
            case 2:
                TickGame();
                return;
            case 3:
                TickStartGameAim();
                return;
            case 4:
            default:
                return;
            case 5:
                TickPublisher();
                return;
        }
    }

    private void TickPublisher() {
        this.countpubli--;
        if (this.countpubli == 0) {
            ChangeStep(0);
        }
    }

    private void TickMenuMain() {
        if (this.currentStepNumber <= getCurrentHighscoreStr().length()) {
            this.currentStepNumber2 = (this.currentStepNumber2 + 1) % 3;
        }
        if (this.currentStepNumber2 == 0) {
            this.currentStepNumber = (this.currentStepNumber + 1) % this.maxStepNumber;
        }
    }

    private void TickGame() {
        if (this.inPack || !isGameblocked()) {
            if (this.inPack) {
                if (this.currentLineToPack >= this.endLinesToPack) {
                    for (int i = this.startLineToPack; i >= this.currentLineToPack; i--) {
                        packL(i);
                    }
                    this.currentLineToPack--;
                } else {
                    endPack();
                }
            }
            if (!this.inAnim || this.currentStepStars <= 0) {
                this.inAnim = false;
            } else {
                upStars();
            }
        } else {
            if (!this.gameOver) {
                int l_remain = countRemainingBubbles();
                if (l_remain <= 5) {
                    this.score += addScoreRemainingBubbles(l_remain);
                    switch (l_remain) {
                        case 0:
                            this.strToast = "CONGRATULATIONS!";
                            this.strToast1 = "No remaining Bubbles!";
                            this.strToast2 = "EXTRA BONUS: " + addScoreRemainingBubbles(l_remain) + " points.";
                            break;
                        case 1:
                            this.strToast = "CONGRATULATIONS!";
                            this.strToast1 = "Only one remaining Bubble!";
                            this.strToast2 = "BONUS: " + addScoreRemainingBubbles(l_remain) + " points.";
                            break;
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                            this.strToast = "GREAT!";
                            this.strToast1 = "Only " + l_remain + " remaining Bubbles!";
                            this.strToast2 = "BONUS: " + addScoreRemainingBubbles(l_remain) + " points.";
                            break;
                    }
                }
                updateStats();
                if (this.score > getCurrentHighscore()) {
                    setCurrentHighscore(this.score);
                    saveDatas();
                    if (this.SoundActivated) {
                        launchSound(3, false);
                    }
                } else if (this.SoundActivated) {
                    launchSound(4, false);
                }
            } else if (this.strToast.length() > 0) {
                this.activityparent.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(MainView.this.mContext, Html.fromHtml("<center><b>" + MainView.this.strToast + "</b>" + "<br/>" + "<small>" + MainView.this.strToast1 + "</small>" + "<br/>" + "<b>" + MainView.this.strToast2 + "</b>" + "</center>"), 0).show();
                    }
                });
                this.strToast = "";
            }
            this.gameOver = true;
            this.gameInProgress = false;
            this.undoAvailable = false;
        }
    }

    private void TickLoading() {
        try {
            this.PoolBitmaps[this.cptLoad] = BitmapFactory.decodeResource(this.mRes, this.RefBitmaps[this.cptLoad]);
            RefreshLoad();
        } catch (Exception e) {
            System.out.println("Failure: " + this.RefBitmaps[this.cptLoad]);
        }
        if (this.cptLoad >= this.RefBitmaps.length) {
            initgameparameters();
            initPositions();
            if (this.gameInProgress) {
                ChangeStep(2);
            } else {
                ChangeStep(1);
            }
        }
    }

    private void TickStartGameAim() {
        switch (this.startGameAnimationType) {
            case 0:
                if (this.currentcolorToUse >= this.colorToUse.length) {
                    ChangeStep(2);
                    return;
                }
                for (int i = 0; i <= this.Nb_Row_Bille - 1; i++) {
                    for (int j = 0; j <= this.Nb_Col_Bille - 1; j++) {
                        if (this.billes[i][j] == this.colorToUse[this.currentcolorToUse]) {
                            this.animbilles[i][j] = this.billes[i][j];
                        }
                    }
                }
                this.currentcolorToUse++;
                return;
            case 1:
                if (this.currentcolorToUse >= this.colorToUse.length) {
                    ChangeStep(2);
                    return;
                }
                for (int i2 = 0; i2 <= this.Nb_Row_Bille - 1; i2++) {
                    if (this.billes[i2][this.currentStepAnim] == this.colorToUse[this.currentcolorToUse]) {
                        this.animbilles[i2][this.currentStepAnim] = this.billes[i2][this.currentStepAnim];
                    }
                }
                this.currentStepAnim++;
                if (this.currentStepAnim == this.Nb_Col_Bille) {
                    this.currentcolorToUse++;
                    this.currentStepAnim = 0;
                    return;
                }
                return;
            case 2:
                if (this.currentStepAnim >= this.Nb_Col_Bille * this.Nb_Row_Bille) {
                    ChangeStep(2);
                    return;
                }
                for (int i3 = 0; i3 < this.Nb_Col_Bille; i3++) {
                    this.animbilles[this.shakeanimbilles[this.currentStepAnim] / 1000][this.shakeanimbilles[this.currentStepAnim] % 1000] = this.billes[this.shakeanimbilles[this.currentStepAnim] / 1000][this.shakeanimbilles[this.currentStepAnim] % 1000];
                    this.currentStepAnim++;
                }
                return;
            case 3:
                if (this.currentcolorToUse >= this.currentStepAnim) {
                    ChangeStep(2);
                    return;
                }
                for (int i4 = 0; i4 <= this.Nb_Col_Bille - 1; i4++) {
                    this.animbilles[this.currentcolorToUse][i4] = this.billes[this.currentcolorToUse][i4];
                    this.animbilles[this.currentStepAnim][i4] = this.billes[this.currentStepAnim][i4];
                }
                this.currentStepAnim--;
                this.currentcolorToUse++;
                return;
            case 4:
                for (int i5 = 0; i5 <= this.Nb_Row_Bille - 1; i5++) {
                    for (int j2 = 0; j2 <= this.Nb_Col_Bille - 1; j2++) {
                        this.animbilles[i5][j2] = Math.min(this.resizeTileAnim.length - 1, this.animbilles[i5][j2] + 1);
                    }
                }
                this.currentStepAnim++;
                if (this.currentStepAnim > this.resizeTileAnim.length) {
                    ChangeStep(2);
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void Draw(Canvas canvas) {
        switch (this.CurrentStep) {
            case 0:
                paint_loading(canvas);
                return;
            case 1:
                paint_menu_main(canvas);
                return;
            case 2:
                paint_game(canvas);
                return;
            case 3:
                paint_fillgame(canvas);
                return;
            case 4:
                paint_contactssms(canvas);
                return;
            case 5:
                paint_publisher(canvas);
                return;
            default:
                return;
        }
    }

    private void paint_backgroundmenu(Canvas canvas) {
        canvas.drawBitmap(this.PoolBitmaps[3], (Rect) null, this.rect, (Paint) null);
        canvas.drawBitmap(this.title, (float) ((myWidth() - this.title.getWidth()) / 2), 10.0f, (Paint) null);
    }

    private void paint_contactssms(Canvas canvas) {
        int l_dec;
        if (this.ItemSelected != -1) {
            l_dec = this.ItemSelected + 1;
        } else {
            l_dec = 0;
        }
        canvas.drawBitmap(this.PoolBitmaps[IMG_FondSms], 0.0f, 0.0f, (Paint) null);
        canvas.drawText(this.mContext.getString(R.string.sms1), (((float) getWidth()) - this.pt_1.measureText(this.mContext.getString(R.string.sms1))) / 2.0f, (this.pt_1.getTextSize() / 2.0f) + this.pt_1.getTextSize(), this.pt_1);
        canvas.drawText(this.mContext.getString(R.string.sms2), (((float) getWidth()) - this.pt_1.measureText(this.mContext.getString(R.string.sms2))) / 2.0f, (this.pt_1.getTextSize() / 2.0f) + (this.pt_1.getTextSize() * 2.0f), this.pt_1);
        canvas.drawText(this.mContext.getString(R.string.sms3), (((float) getWidth()) - this.pt_1.measureText(this.mContext.getString(R.string.sms3))) / 2.0f, (this.pt_1.getTextSize() / 2.0f) + (3.0f * this.pt_1.getTextSize()), this.pt_1);
        int l_h = this.PoolBitmaps[IMG_contactItem].getHeight();
        int i = 0;
        canvas.save();
        canvas.clipRect(0, l_h, myWidth(), (this.PoolBitmaps[IMG_contactItem].getHeight() * IMG_skin_select_pieces) + l_h);
        for (int j = this.startContact; j <= this.endContact; j++) {
            canvas.drawBitmap(this.PoolBitmaps[IMG_contactItem], 0.0f, (float) ((this.PoolBitmaps[IMG_contactItem].getHeight() * i) + l_h), (Paint) null);
            canvas.drawText(this.mContacts.getContact(j).getName(), 20.0f, ((float) (l_h + IMG_menuplay + (this.PoolBitmaps[IMG_contactItem].getHeight() * i))) + this.pt_2.getTextSize(), this.pt_2);
            canvas.drawText(this.mContacts.getContact(j).getNumber(), 20.0f, ((float) (l_h + IMG_menuplay + (this.PoolBitmaps[IMG_contactItem].getHeight() * i))) + this.pt_2.getTextSize() + this.pt_2.getTextSize(), this.pt_3);
            if (this.mContacts.getContact(j).getChecked()) {
                canvas.drawBitmap(this.PoolBitmaps[IMG_check], (float) (myWidth() - (this.PoolBitmaps[IMG_check].getWidth() + IMG_PubParDefaut)), (float) ((this.PoolBitmaps[IMG_contactItem].getHeight() * i) + l_h + ((this.PoolBitmaps[IMG_contactItem].getHeight() - this.PoolBitmaps[IMG_check].getHeight()) / 2)), (Paint) null);
            } else {
                canvas.drawBitmap(this.PoolBitmaps[IMG_uncheck], (float) (myWidth() - (this.PoolBitmaps[IMG_check].getWidth() + IMG_PubParDefaut)), (float) ((this.PoolBitmaps[IMG_contactItem].getHeight() * i) + l_h + ((this.PoolBitmaps[IMG_contactItem].getHeight() - this.PoolBitmaps[IMG_check].getHeight()) / 2)), (Paint) null);
            }
            i++;
        }
        canvas.restore();
        if (this.moved) {
            canvas.drawBitmap(this.PoolBitmaps[IMG_scrollbar], (Rect) null, new Rect(myWidth() - this.PoolBitmaps[IMG_scrollbar].getWidth(), l_h, myWidth(), (this.PoolBitmaps[IMG_contactItem].getHeight() * IMG_skin_select_pieces) + l_h), (Paint) null);
            canvas.drawBitmap(this.PoolBitmaps[IMG_curseur], (float) (myWidth() - this.PoolBitmaps[IMG_scrollbar].getWidth()), ((float) l_h) + (((float) (this.PoolBitmaps[IMG_contactItem].getHeight() * IMG_skin_select_pieces)) * (((float) this.startContact) / ((float) this.mContacts.getContactsCount()))), (Paint) null);
        }
        canvas.drawBitmap(this.PoolBitmaps[this.TabSmsMenu[0][l_dec]], (float) this.posLeftMenuGOSms, (float) this.posTopMenuGOSms2, this.pt2);
    }

    private void paint_publisher(Canvas canvas) {
        canvas.drawBitmap(this.publisher, (Rect) null, this.rect, (Paint) null);
    }

    private void paint_loading(Canvas canvas) {
        if (this.splash != null) {
            canvas.drawBitmap(this.splash, 0.0f, 0.0f, (Paint) null);
            canvas.drawBitmap(this.title, (float) ((myWidth() - this.title.getWidth()) / 2), 50.0f, (Paint) null);
        }
    }

    private void paint_menu_main(Canvas canvas) {
        int l_dec;
        boolean z;
        if (this.ItemSelected != -1) {
            l_dec = this.ItemSelected + 1;
        } else {
            l_dec = 0;
        }
        paint_backgroundmenu(canvas);
        int length = 352 - ((96 - (getCurrentHighscoreStr().length() * IMG_menufacebook)) / 2);
        String currentHighscoreStr = getCurrentHighscoreStr();
        if (this.currentStepNumber < getCurrentHighscoreStr().length()) {
            z = true;
        } else {
            z = false;
        }
        paint_score(canvas, length, 251, currentHighscoreStr, z);
        canvas.drawBitmap(this.PoolBitmaps[this.TabMainMenuState[0][l_dec]], (float) this.posLefMenuPlay, (float) this.posTopMenuPlay, this.pt2);
        canvas.drawBitmap(this.PoolBitmaps[this.TabMainMenuState[1][l_dec]], (float) this.posLeftMenuMoreGames, (float) this.posTopMenuMoreGames, this.pt2);
        canvas.save();
        canvas.clipRect(this.posLeftMenuSound, this.posTopMenuSound, this.posLeftMenuSound + this.PoolBitmaps[IMG_menusound].getWidth(), this.posTopMenuSound + (this.PoolBitmaps[IMG_menusound].getHeight() / 2));
        if (this.SoundActivated) {
            canvas.drawBitmap(this.PoolBitmaps[this.TabMainMenuState[2][l_dec]], (float) this.posLeftMenuSound, (float) this.posTopMenuSound, this.pt2);
        } else {
            canvas.drawBitmap(this.PoolBitmaps[this.TabMainMenuState[2][l_dec]], (float) this.posLeftMenuSound, (float) (this.posTopMenuSound - (this.PoolBitmaps[IMG_menusound].getHeight() / 2)), this.pt2);
        }
        canvas.restore();
        canvas.drawBitmap(this.PoolBitmaps[this.TabMainMenuState[3][l_dec]], (float) this.posLeftMenuFacebook, (float) this.posTopMenuFacebook, this.pt2);
        canvas.drawBitmap(this.PoolBitmaps[4], (float) this.posLeftSkinSelection, (float) this.posTopSkinSelection, this.pt2);
        for (int i = 0; i < this.nbSkins; i++) {
            if (i == this.skinType) {
                paint_billeXY(canvas, this.posLeftSkinItems + ((this.Bille_TileSize + this.spaceBetwennSkinItems) * i), this.posTopSkinItems, i, i + 1);
            } else {
                paint_billeXY(canvas, this.posLeftSkinItems + ((this.Bille_TileSize + this.spaceBetwennSkinItems) * i), this.posTopSkinItems, i, 0);
            }
            if (!this.skinAccess[i]) {
                canvas.drawBitmap(this.PoolBitmaps[IMG_lock], (float) (this.posLeftSkinItems + ((this.Bille_TileSize + this.spaceBetwennSkinItems) * i)), (float) this.posTopSkinItems, (Paint) null);
            }
        }
    }

    private void paint_fillgame(Canvas canvas) {
        canvas.drawBitmap(this.PoolBitmaps[0], 0.0f, 0.0f, (Paint) null);
        switch (this.modePack) {
            case 0:
                canvas.drawBitmap(this.PoolBitmaps[IMG_ModeClassic], 0.0f, 0.0f, (Paint) null);
                break;
            case 1:
                canvas.drawBitmap(this.PoolBitmaps[IMG_ModeGravity], 0.0f, 0.0f, (Paint) null);
                break;
        }
        switch (this.startGameAnimationType) {
            case 0:
                paint_fillgame01(canvas);
                return;
            case 1:
                paint_fillgame02(canvas);
                return;
            case 2:
                paint_fillgame03(canvas);
                return;
            case 3:
                paint_fillgame04(canvas);
                return;
            case 4:
                paint_fillgame05(canvas);
                return;
            default:
                return;
        }
    }

    private void paint_bille(Canvas canvas, int i, int j, int type, int color) {
        canvas.save();
        canvas.clipRect(this.xAnchBilles + (this.Bille_TileSize * j), this.yAnchBilles + (this.Bille_TileSize * i), this.xAnchBilles + ((j + 1) * this.Bille_TileSize), this.yAnchBilles + ((i + 1) * this.Bille_TileSize));
        canvas.drawBitmap(this.PoolBitmaps[IMG_billes], (float) ((this.xAnchBilles + (this.Bille_TileSize * j)) - (this.Bille_TileSize * color)), (float) ((this.yAnchBilles + (this.Bille_TileSize * i)) - (this.Bille_TileSize * type)), (Paint) null);
        canvas.restore();
    }

    private void paint_billeXY(Canvas canvas, int x, int y, int type, int color) {
        canvas.save();
        canvas.clipRect(x, y, this.Bille_TileSize + x, this.Bille_TileSize + y);
        canvas.drawBitmap(this.PoolBitmaps[IMG_billes], (float) (x - (this.Bille_TileSize * color)), (float) (y - (this.Bille_TileSize * type)), (Paint) null);
        canvas.restore();
    }

    private void paint_fillgame01(Canvas canvas) {
        for (int i = 0; i < this.Nb_Row_Bille; i++) {
            for (int j = 0; j < this.Nb_Col_Bille; j++) {
                if (this.animbilles[i][j] > 0) {
                    paint_bille(canvas, i, j, this.skinType, this.animbilles[i][j] - 1);
                }
            }
        }
    }

    private void paint_fillgame02(Canvas canvas) {
        for (int i = 0; i < this.Nb_Row_Bille; i++) {
            for (int j = 0; j < this.Nb_Col_Bille; j++) {
                if (this.animbilles[i][j] > 0) {
                    paint_bille(canvas, i, j, this.skinType, this.animbilles[i][j] - 1);
                }
            }
        }
        for (int i2 = 0; i2 <= this.Nb_Row_Bille - 1; i2++) {
            if (this.currentcolorToUse < this.colorToUse.length) {
                paint_bille(canvas, i2, this.currentStepAnim, this.skinType, this.colorToUse[this.currentcolorToUse] - 1);
            }
        }
    }

    private void paint_fillgame03(Canvas canvas) {
        for (int i = 0; i < this.Nb_Row_Bille; i++) {
            for (int j = 0; j < this.Nb_Col_Bille; j++) {
                if (this.animbilles[i][j] > 0) {
                    paint_bille(canvas, i, j, this.skinType, this.animbilles[i][j] - 1);
                }
            }
        }
    }

    private void paint_fillgame04(Canvas canvas) {
        for (int i = 0; i < this.Nb_Row_Bille; i++) {
            for (int j = 0; j < this.Nb_Col_Bille; j++) {
                if (this.animbilles[i][j] > 0) {
                    paint_bille(canvas, i, j, this.skinType, this.animbilles[i][j] - 1);
                }
            }
        }
        for (int i2 = 0; i2 <= this.Nb_Col_Bille - 1; i2++) {
            paint_bille(canvas, this.currentStepAnim, i2, this.skinType, 0);
            paint_bille(canvas, this.currentcolorToUse, i2, this.skinType, 0);
        }
    }

    private void paint_fillgame05(Canvas canvas) {
        for (int i = 0; i < this.Nb_Row_Bille; i++) {
            for (int j = 0; j < this.Nb_Col_Bille; j++) {
                canvas.drawBitmap(this.PoolBitmaps[IMG_billes], new Rect((this.billes[i][j] - 1) * this.Bille_TileSize, this.skinType * this.Bille_TileSize, ((this.billes[i][j] - 1) * this.Bille_TileSize) + this.Bille_TileSize, (this.skinType * this.Bille_TileSize) + this.Bille_TileSize), new Rect(this.xAnchBilles + (this.Bille_TileSize * j) + ((this.Bille_TileSize - this.resizeTileAnim[this.animbilles[i][j]]) / 2), this.yAnchBilles + (this.Bille_TileSize * i) + ((this.Bille_TileSize - this.resizeTileAnim[this.animbilles[i][j]]) / 2), this.xAnchBilles + (this.Bille_TileSize * j) + ((this.Bille_TileSize - this.resizeTileAnim[this.animbilles[i][j]]) / 2) + this.resizeTileAnim[this.animbilles[i][j]], this.yAnchBilles + (this.Bille_TileSize * i) + ((this.Bille_TileSize - this.resizeTileAnim[this.animbilles[i][j]]) / 2) + this.resizeTileAnim[this.animbilles[i][j]]), (Paint) null);
            }
        }
    }

    private void paint_score(Canvas canvas, int p_startX, int p_startY, String p_score, boolean p_anim) {
        int l_vprec;
        int l_vnext;
        for (int i = 0; i < p_score.length(); i++) {
            int l_v = Integer.valueOf(p_score.charAt(i)).intValue() - 48;
            if (i > 0) {
                l_vprec = Integer.valueOf(p_score.charAt(i - 1)).intValue() - 48;
            } else {
                l_vprec = IMG_menuplay;
            }
            if (i < p_score.length() - 1) {
                l_vnext = Integer.valueOf(p_score.charAt(i + 1)).intValue() - 48;
            } else {
                l_vnext = IMG_menuplay;
            }
            canvas.save();
            canvas.clipRect(p_startX - (((p_score.length() - i) * this.PoolBitmaps[5].getWidth()) / IMG_menuplay), p_startY, (p_startX - (((p_score.length() - i) * this.PoolBitmaps[5].getWidth()) / IMG_menuplay)) + (this.PoolBitmaps[5].getWidth() / IMG_menuplay), (this.PoolBitmaps[5].getHeight() / 2) + p_startY);
            canvas.drawBitmap(this.PoolBitmaps[5], (float) ((p_startX - (((p_score.length() - i) * this.PoolBitmaps[5].getWidth()) / IMG_menuplay)) - ((this.PoolBitmaps[5].getWidth() * l_v) / IMG_menuplay)), (float) p_startY, this.pt2);
            canvas.restore();
            if (p_anim && this.currentStepNumber == i) {
                canvas.save();
                switch (this.currentStepNumber2) {
                    case 0:
                        canvas.clipRect((p_startX - (((p_score.length() - i) * this.PoolBitmaps[5].getWidth()) / IMG_menuplay)) - (this.PoolBitmaps[5].getWidth() / IMG_PubParDefaut), p_startY, p_startX - (((p_score.length() - i) * this.PoolBitmaps[5].getWidth()) / IMG_menuplay), (this.PoolBitmaps[5].getHeight() / 2) + p_startY);
                        canvas.drawBitmap(this.PoolBitmaps[5], (float) ((p_startX - (((p_score.length() - (i - 1)) * this.PoolBitmaps[5].getWidth()) / IMG_menuplay)) - ((this.PoolBitmaps[5].getWidth() * l_vprec) / IMG_menuplay)), (float) (p_startY - (this.PoolBitmaps[5].getHeight() / 2)), this.pt2);
                        canvas.clipRect(p_startX - (((p_score.length() - i) * this.PoolBitmaps[5].getWidth()) / IMG_menuplay), p_startY, ((p_startX - (((p_score.length() - i) * this.PoolBitmaps[5].getWidth()) / IMG_menuplay)) + (this.PoolBitmaps[5].getWidth() / IMG_menuplay)) - (this.PoolBitmaps[5].getWidth() / IMG_PubParDefaut), (this.PoolBitmaps[5].getHeight() / 2) + p_startY);
                        canvas.drawBitmap(this.PoolBitmaps[5], (float) ((p_startX - (((p_score.length() - i) * this.PoolBitmaps[5].getWidth()) / IMG_menuplay)) - ((this.PoolBitmaps[5].getWidth() * l_v) / IMG_menuplay)), (float) (p_startY - (this.PoolBitmaps[5].getHeight() / 2)), this.pt2);
                        break;
                    case 1:
                        canvas.clipRect(p_startX - (((p_score.length() - i) * this.PoolBitmaps[5].getWidth()) / IMG_menuplay), p_startY, (p_startX - (((p_score.length() - i) * this.PoolBitmaps[5].getWidth()) / IMG_menuplay)) + (this.PoolBitmaps[5].getWidth() / IMG_menuplay), (this.PoolBitmaps[5].getHeight() / 2) + p_startY);
                        canvas.drawBitmap(this.PoolBitmaps[5], (float) ((p_startX - (((p_score.length() - i) * this.PoolBitmaps[5].getWidth()) / IMG_menuplay)) - ((this.PoolBitmaps[5].getWidth() * l_v) / IMG_menuplay)), (float) (p_startY - (this.PoolBitmaps[5].getHeight() / 2)), this.pt2);
                        break;
                    case 2:
                        canvas.clipRect((p_startX - (((p_score.length() - i) * this.PoolBitmaps[5].getWidth()) / IMG_menuplay)) + (this.PoolBitmaps[5].getWidth() / IMG_PubParDefaut), p_startY, (p_startX - (((p_score.length() - i) * this.PoolBitmaps[5].getWidth()) / IMG_menuplay)) + (this.PoolBitmaps[5].getWidth() / IMG_menuplay), (this.PoolBitmaps[5].getHeight() / 2) + p_startY);
                        canvas.drawBitmap(this.PoolBitmaps[5], (float) ((p_startX - (((p_score.length() - i) * this.PoolBitmaps[5].getWidth()) / IMG_menuplay)) - ((this.PoolBitmaps[5].getWidth() * l_v) / IMG_menuplay)), (float) (p_startY - (this.PoolBitmaps[5].getHeight() / 2)), this.pt2);
                        canvas.clipRect((p_startX - (((p_score.length() - i) * this.PoolBitmaps[5].getWidth()) / IMG_menuplay)) + (this.PoolBitmaps[5].getWidth() / IMG_menuplay), p_startY, (p_startX - (((p_score.length() - i) * this.PoolBitmaps[5].getWidth()) / IMG_menuplay)) + (this.PoolBitmaps[5].getWidth() / IMG_menuplay) + (this.PoolBitmaps[5].getWidth() / IMG_PubParDefaut), (this.PoolBitmaps[5].getHeight() / 2) + p_startY);
                        canvas.drawBitmap(this.PoolBitmaps[5], (float) ((p_startX - (((p_score.length() - (i + 1)) * this.PoolBitmaps[5].getWidth()) / IMG_menuplay)) - ((this.PoolBitmaps[5].getWidth() * l_vnext) / IMG_menuplay)), (float) (p_startY - (this.PoolBitmaps[5].getHeight() / 2)), this.pt2);
                        break;
                }
                canvas.restore();
            }
        }
    }

    private void paint_game_gameover(Canvas canvas) {
        int l_dec;
        if (this.ItemSelected != -1) {
            l_dec = this.ItemSelected + 1;
        } else {
            l_dec = 0;
        }
        canvas.drawARGB(255, 0, 0, 0);
        if (this.score >= getCurrentHighscore()) {
            canvas.drawBitmap(this.PoolBitmaps[IMG_highscore], (float) ((myWidth() / 2) - (this.PoolBitmaps[IMG_highscore].getWidth() / 2)), 50.0f, this.pt2);
        } else {
            canvas.drawBitmap(this.PoolBitmaps[1], (float) ((myWidth() / 2) - (this.PoolBitmaps[1].getWidth() / 2)), 50.0f, this.pt2);
        }
        canvas.drawBitmap(this.PoolBitmaps[IMG_finPartie], (float) ((myWidth() - this.PoolBitmaps[IMG_finPartie].getWidth()) / 2), (float) ((myHeight() / 4) - 75), (Paint) null);
        canvas.drawBitmap(this.PoolBitmaps[this.TabGameMenuStateGameOver[0][l_dec]], (float) this.posLeftMenuGOFacebook, (float) this.posTopMenuGOFacebook, this.pt2);
        canvas.drawBitmap(this.PoolBitmaps[this.TabGameMenuStateGameOver[1][l_dec]], (float) this.posLeftMenuGOMoreGames, (float) this.posTopMenuGOMoreGames, this.pt2);
        canvas.drawBitmap(this.PoolBitmaps[this.TabGameMenuStateGameOver[2][l_dec]], (float) this.posLeftMenuGOSms, (float) this.posTopMenuGOSms, this.pt2);
        canvas.drawBitmap(this.PoolBitmaps[this.TabGameMenuStateGameOver[3][l_dec]], (float) this.posLeftMenuGORestart, (float) this.posTopMenuGORestart, this.pt2);
        canvas.drawBitmap(this.PoolBitmaps[this.TabGameMenuStateGameOver[4][l_dec]], (float) this.posLeftMenuGOMenu, (float) this.posTopMenuGOMenu2, this.pt2);
        paint_score(canvas, 352 - ((96 - (getCurrentHighscoreStr().length() * IMG_menufacebook)) / 2), 144, getCurrentHighscoreStr(), false);
        paint_score(canvas, 362, 191, this.scoreStr, false);
    }

    private void paint_game_gamepaused(Canvas canvas) {
        int l_dec;
        if (this.ItemSelected != -1) {
            l_dec = this.ItemSelected + 1;
        } else {
            l_dec = 0;
        }
        canvas.drawARGB(170, 0, 0, 0);
        canvas.drawBitmap(this.PoolBitmaps[IMG_pause], (float) ((myWidth() - this.PoolBitmaps[IMG_pause].getWidth()) / 2), 50.0f, (Paint) null);
        canvas.drawBitmap(this.PoolBitmaps[IMG_cadrepause], (float) ((myWidth() - this.PoolBitmaps[IMG_cadrepause].getWidth()) / 2), (float) (((myHeight() - this.PoolBitmaps[IMG_cadrepause].getHeight()) / 2) - 3), (Paint) null);
        canvas.drawBitmap(this.PoolBitmaps[this.TabGameMenuStatePause[0][l_dec]], (float) this.posLeftMenuPResume, (float) this.posTopMenuPResume, this.pt2);
        canvas.drawBitmap(this.PoolBitmaps[this.TabGameMenuStatePause[1][l_dec]], (float) this.posLeftMenuPRestart, (float) this.posTopMenuPRestart, this.pt2);
        canvas.drawBitmap(this.PoolBitmaps[this.TabGameMenuStatePause[2][l_dec]], (float) this.posLeftMenuPMenu, (float) this.posTopMenuPMenu, this.pt2);
        canvas.save();
        canvas.clipRect(this.posLeftMenuSound, this.posTopMenuSound, this.posLeftMenuSound + this.PoolBitmaps[IMG_menusound].getWidth(), this.posTopMenuSound + (this.PoolBitmaps[IMG_menusound].getHeight() / 2));
        if (this.SoundActivated) {
            canvas.drawBitmap(this.PoolBitmaps[this.TabGameMenuStatePause[3][l_dec]], (float) this.posLeftMenuSound, (float) this.posTopMenuSound, this.pt2);
        } else {
            canvas.drawBitmap(this.PoolBitmaps[this.TabGameMenuStatePause[3][l_dec]], (float) this.posLeftMenuSound, (float) (this.posTopMenuSound - (this.PoolBitmaps[IMG_menusound].getHeight() / 2)), this.pt2);
        }
        canvas.restore();
        canvas.drawBitmap(this.PoolBitmaps[this.TabGameMenuStatePause[4][l_dec]], (float) this.posLeftMenuFacebook, (float) this.posTopMenuFacebook, this.pt2);
    }

    private void paint_game(Canvas canvas) {
        Point pt;
        canvas.drawBitmap(this.PoolBitmaps[0], 0.0f, 0.0f, (Paint) null);
        switch (this.modePack) {
            case 0:
                canvas.drawBitmap(this.PoolBitmaps[IMG_ModeClassic], 0.0f, 0.0f, (Paint) null);
                break;
            case 1:
                canvas.drawBitmap(this.PoolBitmaps[IMG_ModeGravity], 0.0f, 0.0f, (Paint) null);
                break;
        }
        paint_score(canvas, 322, IMG_contours, this.scoreStr, false);
        for (int i = 0; i < this.Nb_Row_Bille; i++) {
            for (int j = 0; j < this.Nb_Col_Bille; j++) {
                if (this.billes[i][j] > 0) {
                    canvas.save();
                    canvas.clipRect(this.xAnchBilles + (this.Bille_TileSize * j), this.yAnchBilles + (this.Bille_TileSize * i), this.xAnchBilles + ((j + 1) * this.Bille_TileSize), this.yAnchBilles + ((i + 1) * this.Bille_TileSize));
                    canvas.drawBitmap(this.PoolBitmaps[IMG_billes], (float) ((this.xAnchBilles + (this.Bille_TileSize * j)) - ((this.billes[i][j] - 1) * this.Bille_TileSize)), (float) ((this.yAnchBilles + (this.Bille_TileSize * i)) - (this.skinType * this.Bille_TileSize)), (Paint) null);
                    canvas.restore();
                } else if (this.billes[i][j] < 0) {
                    if (!this.inPack) {
                        canvas.save();
                        canvas.clipRect(this.xAnchBilles + (this.Bille_TileSize * j), this.yAnchBilles + (this.Bille_TileSize * i), this.xAnchBilles + ((j + 1) * this.Bille_TileSize), this.yAnchBilles + ((i + 1) * this.Bille_TileSize));
                        canvas.drawBitmap(this.PoolBitmaps[IMG_contours], (float) (this.xAnchBilles + ((j - (getContourSelInt(i, j) - 1)) * this.Bille_TileSize)), (float) ((this.yAnchBilles + (this.Bille_TileSize * i)) - (((Math.abs(this.billes[i][j]) - 2) * 2) * this.Bille_TileSize)), this.pt2);
                        canvas.restore();
                    }
                    paint_bille(canvas, i, j, this.skinType, 0);
                }
            }
        }
        if (this.currentCount != 0) {
            canvas.save();
            canvas.clipRect(this.xAnchBilles + ((this.endColToPack + ((this.startColToPack - this.endColToPack) / 2)) * this.Bille_TileSize), this.yAnchBilles + ((this.endLinesToPack + ((this.startLineToPack - this.endLinesToPack) / 2)) * this.Bille_TileSize), this.xAnchBilles + ((this.endColToPack + ((this.startColToPack - this.endColToPack) / 2)) * this.Bille_TileSize) + (this.PoolBitmaps[IMG_panelscore].getWidth() / 5), this.yAnchBilles + ((this.endLinesToPack + ((this.startLineToPack - this.endLinesToPack) / 2)) * this.Bille_TileSize) + (this.PoolBitmaps[IMG_panelscore].getHeight() / 4));
            canvas.drawBitmap(this.PoolBitmaps[IMG_panelscore], (float) ((this.xAnchBilles + ((this.endColToPack + ((this.startColToPack - this.endColToPack) / 2)) * this.Bille_TileSize)) - (((this.currentColor - 2) * this.PoolBitmaps[IMG_panelscore].getWidth()) / 5)), (float) ((this.yAnchBilles + ((this.endLinesToPack + ((this.startLineToPack - this.endLinesToPack) / 2)) * this.Bille_TileSize)) - ((sizePanel(this.currentSelScore) * this.PoolBitmaps[IMG_panelscore].getHeight()) / 4)), this.pt2);
            canvas.restore();
            canvas.drawText(new StringBuilder().append(this.currentSelScore).toString(), ((float) (this.xAnchBilles + ((this.endColToPack + ((this.startColToPack - this.endColToPack) / 2)) * this.Bille_TileSize))) + ((((float) this.TabWidths[sizePanel(this.currentSelScore)]) - this.paint3.measureText(new StringBuilder().append(this.currentSelScore).toString())) / 2.0f), (((float) (this.yAnchBilles + ((this.endLinesToPack + ((this.startLineToPack - this.endLinesToPack) / 2)) * this.Bille_TileSize))) + this.paint3.getTextSize()) - 2.0f, this.paint3);
        }
        if (this.inAnim) {
            int l_c = 0;
            int l_l = 0;
            for (int i2 = 0; i2 < this.stars.size(); i2++) {
                try {
                    if (i2 < this.stars.size()) {
                        pt = this.stars.elementAt(i2);
                        l_c = pt.colonne;
                        l_l = pt.ligne;
                    } else {
                        pt = null;
                    }
                } catch (Exception e) {
                    pt = null;
                }
                if (pt != null && this.currentStepStars >= 1) {
                    canvas.save();
                    canvas.clipRect(this.xAnchBilles + l_c, this.yAnchBilles + l_l, this.xAnchBilles + l_c + this.Bille_TileSize, this.yAnchBilles + l_l + this.Bille_TileSize);
                    canvas.drawBitmap(this.PoolBitmaps[2], (float) ((this.xAnchBilles + l_c) - (this.currentStepStarsDec[this.currentStepStars - 1] * this.Bille_TileSize)), (float) ((this.yAnchBilles + l_l) - ((this.currentAnimColor - 2) * this.Bille_TileSize)), (Paint) null);
                    canvas.restore();
                }
            }
        }
        if (this.gameOver) {
            paint_game_gameover(canvas);
        }
        if (this.paused) {
            paint_game_gamepaused(canvas);
        }
    }

    public void surfaceChanged(SurfaceHolder holder2, int format, int width, int height) {
        initparameters();
    }

    public void surfaceCreated(SurfaceHolder arg0) {
    }

    public void surfaceDestroyed(SurfaceHolder arg0) {
        saveDatas();
    }

    private int myWidth() {
        return 480;
    }

    private int myHeight() {
        return 725;
    }

    public void run() {
        Canvas c = null;
        bitmap2 = Bitmap.createBitmap(480, 725, Bitmap.Config.ARGB_8888);
        Canvas c2 = new Canvas(bitmap2);
        while (this.in) {
            try {
                Thread.sleep(this.tempo);
                Tick();
                Draw(c2);
                c = this.holder.lockCanvas(null);
                if (c != null) {
                    c.drawBitmap(bitmap2, new Rect(0, 0, 480, 725), new Rect(0, 0, getWidth(), getHeight()), (Paint) null);
                }
                if (c != null) {
                    this.holder.unlockCanvasAndPost(c);
                }
                c = null;
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("-> RUN <-", "PB DANS RUN");
            } catch (Throwable th) {
                if (c != null) {
                    this.holder.unlockCanvasAndPost(c);
                }
                throw th;
            }
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        switch (this.CurrentStep) {
            case 0:
            case 1:
                this.in = false;
                stopSound();
                this.activityparent.finish();
                break;
            case 2:
            case 3:
            case 4:
                ChangeStep(1);
                break;
        }
        return true;
    }

    public boolean isIn(float p_x, float p_y, int start_x, int start_y, int refImg) {
        return p_x > ((float) start_x) && p_x < ((float) (this.PoolBitmaps[refImg].getWidth() + start_x)) && p_y > ((float) start_y) && p_y < ((float) (this.PoolBitmaps[refImg].getHeight() + start_y));
    }

    private int getItemSelectedMenuMain() {
        if (isIn(this.myx, this.myy, this.posLefMenuPlay, this.posTopMenuPlay, IMG_menuplay)) {
            return 0;
        }
        if (isIn(this.myx, this.myy, this.posLeftMenuMoreGames, this.posTopMenuMoreGames, IMG_menuplay)) {
            return 1;
        }
        if (isIn(this.myx, this.myy, this.posLeftMenuSound, this.posTopMenuSound, IMG_menuplay)) {
            return 2;
        }
        if (isIn(this.myx, this.myy, this.posLeftMenuFacebook, this.posTopMenuFacebook, IMG_menufacebook)) {
            return 3;
        }
        return -1;
    }

    private int getItemSelectedContactSms() {
        if (isIn(this.myx, this.myy, this.posLeftMenuGOSms, this.posTopMenuGOSms2, IMG_menusms)) {
            return 0;
        }
        return -1;
    }

    private int getItemSelectedMoreGames() {
        if (isIn(this.myx, this.myy, this.posLeftMenuGOMenu, this.posTopMenuGOMenu, IMG_menuPmenu)) {
            return 0;
        }
        return -1;
    }

    private int getItemSelectedMenuPausedGame() {
        if (isIn(this.myx, this.myy, this.posLeftMenuPResume, this.posTopMenuPResume, IMG_menuplay)) {
            return 0;
        }
        if (isIn(this.myx, this.myy, this.posLeftMenuPRestart, this.posTopMenuPRestart, IMG_menuplay)) {
            return 1;
        }
        if (isIn(this.myx, this.myy, this.posLeftMenuPMenu, this.posTopMenuPMenu, IMG_menufacebook)) {
            return 2;
        }
        if (isIn(this.myx, this.myy, this.posLeftMenuSound, this.posTopMenuSound, IMG_menuplay)) {
            return 3;
        }
        if (isIn(this.myx, this.myy, this.posLeftMenuFacebook, this.posTopMenuFacebook, IMG_menufacebook)) {
            return 4;
        }
        return -1;
    }

    private int getItemSelectedMenuGameOverGame() {
        if (isIn(this.myx, this.myy, this.posLeftMenuGOFacebook, this.posTopMenuGOFacebook, IMG_menuShare)) {
            return 0;
        }
        if (isIn(this.myx, this.myy, this.posLeftMenuGOMoreGames, this.posTopMenuGOMoreGames, IMG_menumoregames)) {
            return 1;
        }
        if (isIn(this.myx, this.myy, this.posLeftMenuGOSms, this.posTopMenuGOSms, IMG_menusms)) {
            return 2;
        }
        if (isIn(this.myx, this.myy, this.posLeftMenuGORestart, this.posTopMenuGORestart, IMG_menuPrestart)) {
            return 3;
        }
        if (isIn(this.myx, this.myy, this.posLeftMenuGOMenu, this.posTopMenuGOMenu2, IMG_menuPmenu)) {
            return 4;
        }
        return -1;
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (!this.inPack) {
            int action = event.getAction();
            this.myx = event.getX() * 1.0f;
            this.myy = event.getY() * 1.0f;
            this.myx /= ((float) getWidth()) / ((float) myWidth());
            this.myy /= ((float) getHeight()) / ((float) myHeight());
            switch (action) {
                case 0:
                    onTouchEventDown(event);
                    return true;
                case 1:
                    onTouchEventUp(event);
                    return true;
                case 2:
                    onTouchEventMove(event);
                    return true;
            }
        }
        return super.onTouchEvent(event);
    }

    public void onTouchEventMove(MotionEvent event) {
        switch (this.CurrentStep) {
            case 1:
                onTouchEventMoveMenuMain(event);
                return;
            case 2:
                onTouchEventMoveGame(event);
                return;
            case 3:
            default:
                return;
            case 4:
                onTouchEventMoveContactSms(event);
                return;
        }
    }

    public void onTouchEventMoveContactSms(MotionEvent event) {
        this.ItemSelected = getItemSelectedContactSms();
        if (this.ItemSelected == -1) {
            this.moved = true;
            if (this.myx >= ((float) ((myWidth() - IMG_PubParDefaut) - this.PoolBitmaps[IMG_check].getWidth()))) {
                return;
            }
            if (this.myy > this.startmyy) {
                int l_dec = (int) ((this.myy - this.startmyy) / ((float) this.PoolBitmaps[IMG_contactItem].getHeight()));
                if (l_dec > 0) {
                    this.startmyy = this.myy;
                }
                this.startContact = Math.max(0, this.startContact - l_dec);
                this.endContact = (this.startContact + this.MaxAvailableContacts) - 1;
                return;
            }
            int l_dec2 = (int) ((this.startmyy - this.myy) / ((float) this.PoolBitmaps[IMG_contactItem].getHeight()));
            if (l_dec2 > 0) {
                this.startmyy = this.myy;
            }
            this.endContact = Math.min(this.mContacts.getContactsCount() - 1, this.endContact + l_dec2);
            this.startContact = (this.endContact - this.MaxAvailableContacts) + 1;
        }
    }

    public void onTouchEventMoveMoreGames(MotionEvent event) {
        this.ItemSelected = getItemSelectedMoreGames();
        if (this.ItemSelected != -1) {
        }
    }

    public void onTouchEventMoveGame(MotionEvent event) {
        if (!this.inPack) {
            if (this.paused) {
                this.ItemSelected = getItemSelectedMenuPausedGame();
            } else if (this.gameOver) {
                this.ItemSelected = getItemSelectedMenuGameOverGame();
            }
        }
    }

    public void onTouchEventMoveMenuMain(MotionEvent event) {
        this.ItemSelected = getItemSelectedMenuMain();
        if (this.ItemSelected == -1) {
            int l_skin = this.skinType;
            if (this.myy > ((float) this.posTopSkinSelection) && this.myy < ((float) (this.posTopSkinSelection + this.PoolBitmaps[4].getHeight())) && this.myx > ((float) this.posLeftSkinSelection) && this.myx < ((float) (this.posLeftSkinSelection + this.PoolBitmaps[4].getWidth()))) {
                this.skinType = (int) ((this.myx - ((float) this.posLeftSkinSelection)) / ((float) (this.Bille_TileSize + this.spaceBetwennSkinItems)));
                if (this.skinType >= 0 && this.skinType < this.skinAccess.length && !this.skinAccess[this.skinType]) {
                    this.skinType = l_skin;
                }
            }
        }
    }

    public void onTouchEventUp(MotionEvent event) {
        switch (this.CurrentStep) {
            case 1:
                onTouchEventUpMenuMain(event);
                return;
            case 2:
                onTouchEventUpGame(event);
                return;
            case 3:
            default:
                return;
            case 4:
                onTouchEventUpContactSms(event);
                return;
        }
    }

    private void onTouchEventUpContactSms(MotionEvent event) {
        this.moved = false;
        switch (this.ItemSelected) {
            case 0:
                SmsManager sms = SmsManager.getDefault();
                int l_count = 0;
                for (int i = 0; i < this.mContacts.getCheckedContactsCount(); i++) {
                    List<String> messages = sms.divideMessage("Hello! Come and play Bubble Breaker Beach with me: https://market.android.com/details?id=com.storybird.bubblebreakerbeach");
                    String recipient = this.mContacts.getCheckedContact(i).getNumber();
                    for (String message : messages) {
                        sms.sendTextMessage(recipient, null, message, PendingIntent.getBroadcast(this.activityparent, 0, new Intent(ACTION_SMS_SENT), 0), null);
                    }
                    if (unLockSkin()) {
                        l_count++;
                    }
                }
                if (l_count == 1) {
                    this.activityparent.runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(MainView.this.mContext, (int) R.string.skinunlock, 0).show();
                        }
                    });
                } else if (l_count > 1) {
                    this.activityparent.runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(MainView.this.mContext, (int) R.string.skinsunlock, 0).show();
                        }
                    });
                }
                ChangeStep(1);
                break;
            default:
                if (this.myy > ((float) this.PoolBitmaps[IMG_contactItem].getHeight()) && this.myy < ((float) (this.PoolBitmaps[IMG_contactItem].getHeight() + (this.MaxAvailableContacts * this.PoolBitmaps[IMG_contactItem].getHeight()))) && this.myx > ((float) ((myWidth() - IMG_PubParDefaut) - this.PoolBitmaps[IMG_check].getWidth()))) {
                    Contact l_c = this.mContacts.getContact(this.startContact + ((int) ((this.myy - ((float) this.PoolBitmaps[IMG_contactItem].getHeight())) / ((float) this.PoolBitmaps[IMG_contactItem].getHeight()))));
                    if (l_c.getChecked()) {
                        this.mContacts.uncheckContact(l_c);
                    } else {
                        this.mContacts.checkContact(l_c);
                    }
                    if (this.SoundActivated) {
                        playSoundEffect(3);
                        break;
                    }
                }
                break;
        }
        this.ItemSelected = -1;
    }

    public void onTouchEventUpGame(MotionEvent event) {
        boolean z;
        if (!this.inPack) {
            if (!this.paused) {
                if (this.gameOver) {
                    this.ItemSelected = getItemSelectedMenuGameOverGame();
                    if (this.ItemSelected != -1 && this.SoundActivated) {
                        playSoundEffect(3);
                    }
                    switch (this.ItemSelected) {
                        case 0:
                            this.activityparent.facebook.authorize(this.activityparent, new Facebook.DialogListener() {
                                public void onComplete(Bundle values) {
                                    Bundle bd = new Bundle();
                                    if (MainView.this.score >= MainView.this.getCurrentHighscore()) {
                                        MainView.this.postHighscore = true;
                                        bd.putString("message", "I just got a HIGHSCORE of " + MainView.this.highscore + " in Bubble Breaker Beach, a FREE game from StoryBird! Download it FREE on Android Market and try to beat me!");
                                    } else {
                                        bd.putString("message", "I just got a score of " + MainView.this.highscore + " in Bubble Breaker Beach, a FREE game from StoryBird! Download it FREE on Android Market.");
                                    }
                                    bd.putString("link", "https://market.android.com/details?id=com.storybird.bubblebreakerbeach");
                                    bd.putString("name", "Enjoy it's free!");
                                    MainView.this.activityparent.facebook.dialog(MainView.this.activityparent, "feed", bd, new SampleDialogListener());
                                }

                                public void onFacebookError(FacebookError error) {
                                }

                                public void onError(DialogError e) {
                                }

                                public void onCancel() {
                                }
                            });
                            break;
                        case 1:
                            this.activityparent.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=storybird classics")));
                            break;
                        case 2:
                            ChangeStep(4);
                            break;
                        case 3:
                            ChangeStep(3);
                            break;
                        case 4:
                            ChangeStep(1);
                            break;
                    }
                }
            } else {
                this.ItemSelected = getItemSelectedMenuPausedGame();
                if (this.ItemSelected != -1 && this.SoundActivated) {
                    playSoundEffect(3);
                }
                switch (this.ItemSelected) {
                    case 0:
                        this.paused = false;
                        break;
                    case 1:
                        updateStats();
                        this.score = 0;
                        this.scoreStr = String.valueOf(this.score);
                        ChangeStep(3);
                        break;
                    case 2:
                        ChangeStep(1);
                        break;
                    case 3:
                        if (this.SoundActivated) {
                            z = false;
                        } else {
                            z = true;
                        }
                        this.SoundActivated = z;
                        if (!this.SoundActivated) {
                            stopSound();
                            break;
                        } else {
                            playSoundEffect(3);
                            launchSound(2, true);
                            break;
                        }
                    case 4:
                        this.activityparent.facebook.authorize(this.activityparent, new Facebook.DialogListener() {
                            public void onComplete(Bundle values) {
                                Bundle bd = new Bundle();
                                bd.putString("message", "I'm playing Bubble Breaker Beach. Join me it's FREE! Download game on Android Market: https://market.android.com/details?id=com.storybird.bubblebreakerbeach");
                                bd.putString("link", "http://www.facebook.com/pages/Bubble-Breaker-Beach/126484954102641");
                                bd.putString("name", "Visit Bubble Breaker Beach page (screens, vidéos, musics) !");
                                MainView.this.activityparent.facebook.dialog(MainView.this.activityparent, "feed", bd, new SampleDialogListener());
                            }

                            public void onFacebookError(FacebookError error) {
                            }

                            public void onError(DialogError e) {
                            }

                            public void onCancel() {
                            }
                        });
                        break;
                }
            }
            this.ItemSelected = -1;
        }
    }

    private void onTouchEventUpMenuMain(MotionEvent event) {
        boolean z;
        this.ItemSelected = getItemSelectedMenuMain();
        if (this.ItemSelected != -1 && this.SoundActivated) {
            playSoundEffect(3);
        }
        switch (this.ItemSelected) {
            case 0:
                if (this.gameInProgress) {
                    ChangeStep(2);
                    break;
                } else {
                    ChangeStep(3);
                    break;
                }
            case 1:
                this.activityparent.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=storybird classics")));
                break;
            case 2:
                if (this.SoundActivated) {
                    z = false;
                } else {
                    z = true;
                }
                this.SoundActivated = z;
                if (!this.SoundActivated) {
                    stopSound();
                    break;
                } else {
                    playSoundEffect(3);
                    launchSound(2, true);
                    break;
                }
            case 3:
                this.activityparent.facebook.authorize(this.activityparent, new Facebook.DialogListener() {
                    public void onComplete(Bundle values) {
                        Bundle bd = new Bundle();
                        bd.putString("message", "I like to play Bubble Breaker Beach, a StoryBird game: http://www.facebook.com/pages/Bubble-Breaker-Beach/126484954102641");
                        bd.putString("link", "http://www.facebook.com/pages/Storybird/194195437285078");
                        bd.putString("name", "Visit StoryBird page to discover all our FREE games !");
                        MainView.this.activityparent.facebook.dialog(MainView.this.activityparent, "feed", bd, new SampleDialogListener());
                    }

                    public void onFacebookError(FacebookError error) {
                    }

                    public void onError(DialogError e) {
                    }

                    public void onCancel() {
                    }
                });
                break;
        }
        this.ItemSelected = -1;
    }

    public void onTouchEventDown(MotionEvent event) {
        this.startmyx = this.myx;
        this.startmyy = this.myy;
        switch (this.CurrentStep) {
            case 1:
                onTouchEventDownMenuMain(event);
                return;
            case 2:
                onTouchEventDownGame(event);
                return;
            case 3:
            default:
                return;
            case 4:
                onTouchEventDownContactSms(event);
                return;
        }
    }

    public void onTouchEventDownContactSms(MotionEvent event) {
        this.ItemSelected = getItemSelectedContactSms();
        if (this.ItemSelected != -1) {
        }
    }

    public void onTouchEventDownGame(MotionEvent event) {
        if (this.paused) {
            this.ItemSelected = getItemSelectedMenuPausedGame();
        } else if (this.gameOver) {
            this.ItemSelected = getItemSelectedMenuGameOverGame();
        } else if (this.inPack) {
        } else {
            if (this.myy >= ((float) this.yAnchBilles)) {
                this.ListSelected.setSize(0);
                int l_count = 0;
                if (this.myx >= ((float) this.xAnchBilles) && this.myx < ((float) (this.xAnchBilles + (this.Nb_Col_Bille * this.Bille_TileSize))) && this.myy >= ((float) this.yAnchBilles) && this.myy < ((float) (this.yAnchBilles + (this.Nb_Row_Bille * this.Bille_TileSize)))) {
                    l_count = countchange(((int) (this.myy - ((float) this.yAnchBilles))) / this.Bille_TileSize, ((int) (this.myx - ((float) this.xAnchBilles))) / this.Bille_TileSize, this.billes[((int) (this.myy - ((float) this.yAnchBilles))) / this.Bille_TileSize][((int) (this.myx - ((float) this.xAnchBilles))) / this.Bille_TileSize], this.ListSelected, false);
                }
                if (l_count <= 1 || this.ListSelected.size() <= 1 || this.Bille_TileSize <= 0) {
                    absgrid();
                    this.SelectedCell[0] = -1;
                    this.SelectedCell[1] = -1;
                    this.ListSelected.setSize(0);
                    this.currentCount = 0;
                } else {
                    this.SelectedCell[0] = ((int) (this.myy - ((float) this.yAnchBilles))) / this.Bille_TileSize;
                    this.SelectedCell[1] = ((int) (this.myx - ((float) this.xAnchBilles))) / this.Bille_TileSize;
                }
                if (this.SelectedCell[0] < 0 || this.SelectedCell[0] >= this.Nb_Row_Bille || this.SelectedCell[1] < 0 || this.SelectedCell[1] >= this.Nb_Col_Bille) {
                    absgrid();
                } else if (this.billes[this.SelectedCell[0]][this.SelectedCell[1]] < 0) {
                    if (!this.inPack) {
                        if (this.inAnim) {
                            resetAnim();
                        }
                        startPack();
                        this.scoreToadd = (l_count - 1) * l_count;
                        this.SelectedCell[0] = -1;
                        this.SelectedCell[1] = -1;
                        copy(this.billes, this.undoBilles);
                        this.undoAvailable = true;
                        this.undoscore = this.scoreToadd;
                        this.currentCount = 0;
                    }
                } else if (this.billes[this.SelectedCell[0]][this.SelectedCell[1]] == 0) {
                    absgrid();
                    this.SelectedCell[0] = -1;
                    this.SelectedCell[1] = -1;
                    this.ListSelected.setSize(0);
                    this.currentCount = 0;
                } else {
                    absgrid();
                    this.ListSelected.setSize(0);
                    this.currentCount = countchange(this.SelectedCell[0], this.SelectedCell[1], this.billes[this.SelectedCell[0]][this.SelectedCell[1]], this.ListSelected, true);
                    this.currentColor = this.billes[this.SelectedCell[0]][this.SelectedCell[1]] * -1;
                    this.currentSelScore = this.currentCount * (this.currentCount - 1);
                    this.startLineToPack = maxLineToPack(this.ListSelected);
                    this.endLinesToPack = minLineToPack(this.ListSelected);
                    this.startColToPack = maxColToPack(this.ListSelected);
                    this.endColToPack = minColToPack(this.ListSelected);
                }
            } else if (this.myx > 350.0f && this.myy < 30.0f) {
                this.paused = true;
            }
        }
    }

    public void onTouchEventDownMenuMain(MotionEvent event) {
        this.ItemSelected = getItemSelectedMenuMain();
        if (this.ItemSelected == -1) {
            int l_skin = this.skinType;
            if (this.myy > ((float) this.posTopSkinSelection) && this.myy < ((float) (this.posTopSkinSelection + this.PoolBitmaps[4].getHeight())) && this.myx > ((float) this.posLeftSkinSelection) && this.myx < ((float) (this.posLeftSkinSelection + this.PoolBitmaps[4].getWidth()))) {
                this.skinType = (int) ((this.myx - ((float) this.posLeftSkinSelection)) / ((float) (this.Bille_TileSize + this.spaceBetwennSkinItems)));
                if (this.skinType >= 0 && this.skinType < this.skinAccess.length && !this.skinAccess[this.skinType]) {
                    this.skinType = l_skin;
                    Toast.makeText(this.mContext, (int) R.string.skinblocked, 1).show();
                }
            }
        }
    }

    private void startPack() {
        if (!this.inPack) {
            this.inPack = true;
            this.startLineToPack = maxLineToPack(this.ListSelected);
            this.endLinesToPack = minLineToPack(this.ListSelected);
            this.currentLineToPack = this.startLineToPack;
            if (this.SoundActivated) {
                playSoundEffect(3);
            }
            this.currSound = 0;
            initStars();
        }
    }

    private int maxLineToPack(Vector<Point> p_v) {
        int l_res = 0;
        for (int i = 0; i < p_v.size(); i++) {
            l_res = Math.max(l_res, p_v.elementAt(i).ligne);
        }
        return l_res;
    }

    private int minLineToPack(Vector<Point> p_v) {
        int l_res = this.Nb_Row_Bille - 1;
        for (int i = 0; i < p_v.size(); i++) {
            l_res = Math.min(l_res, p_v.elementAt(i).ligne);
        }
        return l_res;
    }

    private int maxColToPack(Vector<Point> p_v) {
        int l_res = 0;
        for (int i = 0; i < p_v.size(); i++) {
            l_res = Math.max(l_res, p_v.elementAt(i).colonne);
        }
        return l_res;
    }

    private int minColToPack(Vector<Point> p_v) {
        int l_res = this.Nb_Col_Bille - 1;
        for (int i = 0; i < p_v.size(); i++) {
            l_res = Math.min(l_res, p_v.elementAt(i).colonne);
        }
        return l_res;
    }

    private void endPack() {
        MenuItem l_menuitem;
        absgrid();
        for (int k = this.Nb_Col_Bille - 1; k > 0; k--) {
            int l_c = 0;
            while (this.billes[this.Nb_Row_Bille - 1][k] == 0 && l_c < this.Nb_Col_Bille) {
                for (int i = k - 1; i >= 0; i--) {
                    for (int j = 0; j < this.Nb_Row_Bille; j++) {
                        this.billes[j][i + 1] = this.billes[j][i];
                        this.billes[j][i] = 0;
                    }
                }
                l_c++;
            }
        }
        updateScore(this.scoreToadd);
        if (!(this.activityparent.mMenu == null || (l_menuitem = this.activityparent.mMenu.findItem(R.id.undo)) == null)) {
            l_menuitem.setEnabled(this.undoAvailable);
        }
        if (this.modePack == 1) {
            packGravity();
        }
        this.ListSelected.setSize(0);
        this.SelectedCell[0] = -1;
        this.SelectedCell[1] = -1;
        this.inPack = false;
    }

    private int getContourSelInt(int l, int c) {
        boolean l_up;
        boolean l_down;
        boolean l_left;
        boolean l_right;
        if (l - 1 >= 0) {
            l_up = true;
        } else {
            l_up = false;
        }
        if (l + 1 < this.Nb_Row_Bille) {
            l_down = true;
        } else {
            l_down = false;
        }
        if (c - 1 >= 0) {
            l_left = true;
        } else {
            l_left = false;
        }
        if (c + 1 < this.Nb_Col_Bille) {
            l_right = true;
        } else {
            l_right = false;
        }
        int l_NN = 1;
        int l_OO = 1;
        int l_SS = 1;
        int l_EE = 1;
        if (l_up) {
            l_NN = this.billes[l - 1][c];
        }
        if (l_left) {
            l_OO = this.billes[l][c - 1];
        }
        if (l_down) {
            l_SS = this.billes[l + 1][c];
        }
        if (l_right) {
            l_EE = this.billes[l][c + 1];
        }
        if (l_OO >= 0 && l_EE >= 0 && l_NN < 0 && l_SS < 0) {
            return IMG_menumoregamesflat;
        }
        if (l_OO < 0 && l_EE < 0 && l_NN >= 0 && l_SS >= 0) {
            return IMG_menuplayflat;
        }
        if (l_OO >= 0 && l_EE < 0 && l_NN >= 0 && l_SS >= 0) {
            return IMG_menufacebook;
        }
        if (l_SS >= 0 && l_NN < 0 && l_OO >= 0 && l_EE >= 0) {
            return IMG_menumoregames;
        }
        if (l_EE >= 0 && l_OO < 0 && l_NN >= 0 && l_SS >= 0) {
            return IMG_menuplay;
        }
        if (l_NN >= 0 && l_SS < 0 && l_OO >= 0 && l_EE >= 0) {
            return IMG_panelscore;
        }
        if (l_NN >= 0 && l_OO < 0 && l_SS < 0 && l_EE < 0) {
            return IMG_contours;
        }
        if (l_NN >= 0 && l_OO < 0 && l_SS < 0 && l_EE >= 0) {
            return IMG_billes;
        }
        if (l_NN >= 0 && l_OO >= 0 && l_SS < 0 && l_EE < 0) {
            return 1;
        }
        if (l_NN < 0 && l_OO >= 0 && l_SS < 0) {
            return 2;
        }
        if (l_SS >= 0 && l_EE < 0 && l_NN < 0 && l_OO >= 0) {
            return 3;
        }
        if (l_OO < 0 && l_SS >= 0 && l_EE < 0) {
            return 4;
        }
        if (l_SS >= 0 && l_EE >= 0 && l_NN < 0 && l_OO < 0) {
            return 5;
        }
        if (l_NN >= 0 || l_EE < 0 || l_SS >= 0) {
            return 0;
        }
        return IMG_skin_select_pieces;
    }

    private void packL(int p_line) {
        for (int j = 0; j < this.Nb_Col_Bille; j++) {
            if (this.billes[p_line][j] < 0) {
                for (int k = p_line; k > 0; k--) {
                    this.billes[k][j] = this.billes[k - 1][j];
                }
                this.billes[0][j] = 0;
                if (this.SoundActivated) {
                    playBubbleSound(this.currSound);
                    this.currSound = (this.currSound + 1) % this.mMediaPlayers.size();
                }
            }
        }
    }

    private void packGravity() {
        for (int l = 0; l < this.Nb_Col_Bille; l++) {
            for (int i = 0; i < this.Nb_Row_Bille; i++) {
                for (int j = this.Nb_Col_Bille - 1; j > 0; j--) {
                    if (this.billes[i][j] <= 0) {
                        for (int k = j; k > 0; k--) {
                            this.billes[i][k] = this.billes[i][k - 1];
                        }
                        this.billes[i][0] = 0;
                    }
                }
            }
        }
    }

    public void saveDatas() {
        if (this.activityparent != null) {
            SharedPreferences.Editor editor = this.activityparent.getSharedPreferences(PrefName, 0).edit();
            editor.putInt("Version", this.Version);
            editor.putBoolean("SoundAvailable", this.SoundActivated);
            editor.putInt("highscore", this.highscore);
            editor.putInt("skinType", this.skinType);
            editor.putBoolean("gameInProgress", this.gameInProgress);
            for (int i = 0; i <= this.Nb_Row_Bille - 1; i++) {
                for (int j = 0; j <= this.Nb_Col_Bille - 1; j++) {
                    editor.putInt("b" + (i * 100) + j, this.billes[i][j]);
                }
            }
            editor.putInt("score", this.score);
            editor.putInt("highscoreLevel", this.highscoreLevel);
            editor.putInt("gameMode", this.gameMode);
            editor.putInt("currentLevel", this.currentLevel);
            for (int i2 = 0; i2 < this.nbSkins; i2++) {
                editor.putBoolean("skin" + i2, this.skinAccess[i2]);
            }
            editor.putInt("modePack", this.newPackMode);
            for (int i3 = 0; i3 < 2; i3++) {
                for (int j2 = 0; j2 < 4; j2++) {
                    editor.putLong("stats" + (i3 * 100) + j2, this.stats[i3][j2]);
                }
            }
            editor.commit();
        }
    }

    public void loadDatas() {
        if (this.activityparent != null) {
            SharedPreferences settings = this.activityparent.getSharedPreferences(PrefName, 0);
            this.Version = settings.getInt("Version", 0);
            this.SoundActivated = settings.getBoolean("SoundAvailable", false);
            this.highscore = settings.getInt("highscore", 0);
            this.skinType = settings.getInt("skinType", 0);
            this.gameInProgress = settings.getBoolean("gameInProgress", false);
            for (int i = 0; i <= this.Nb_Row_Bille - 1; i++) {
                for (int j = 0; j <= this.Nb_Col_Bille - 1; j++) {
                    this.billes[i][j] = settings.getInt("b" + (i * 100) + j, 0);
                }
            }
            this.score = settings.getInt("score", 0);
            this.highscoreLevel = settings.getInt("highscoreLevel", 0);
            this.gameMode = settings.getInt("gameMode", 0);
            this.currentLevel = settings.getInt("currentLevel", 0);
            for (int i2 = 0; i2 < this.nbSkins; i2++) {
                this.skinAccess[i2] = settings.getBoolean("skin" + i2, true);
            }
            this.skinAccess[0] = true;
            this.highscoreStr = String.valueOf(this.highscore);
            this.highscoreLvlStr = String.valueOf(this.highscoreLevel);
            this.modePack = settings.getInt("modePack", 0);
            this.newPackMode = this.modePack;
            for (int i3 = 0; i3 < 2; i3++) {
                for (int j2 = 0; j2 < 3; j2++) {
                    this.stats[i3][j2] = settings.getLong("stats" + (i3 * 100) + j2, 0);
                }
            }
        }
    }

    private boolean CreateOptionsMenu(Menu menu, int p_step) {
        MenuInflater inflater = this.activityparent.getMenuInflater();
        switch (p_step) {
            case 1:
                inflater.inflate(R.menu.main_menu, menu);
                return true;
            case 2:
                inflater.inflate(R.menu.game_menu, menu);
                this.activityparent.mMenu.findItem(R.id.undo).setEnabled(this.undoAvailable);
                return true;
            default:
                return false;
        }
    }

    public void RefreshOptionsMenu() {
        if (this.activityparent.mMenu != null) {
            this.activityparent.mMenu.clear();
            CreateOptionsMenu(this.activityparent.mMenu, this.CurrentStep);
        }
    }

    public boolean OptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.stats:
                showStats();
                return true;
            case R.id.help:
                showHelp();
                return true;
            case R.id.undo:
                undo();
                return true;
            case R.id.options:
                showOptionsPack();
                return true;
            case R.id.about:
                showAbout();
                return true;
            default:
                return false;
        }
    }

    private String getCurrentHighscoreStr() {
        if (this.gameMode == 1) {
            return this.highscoreLvlStr;
        }
        return this.highscoreStr;
    }

    /* access modifiers changed from: private */
    public int getCurrentHighscore() {
        if (this.gameMode == 1) {
            return this.highscoreLevel;
        }
        return this.highscore;
    }

    private void setCurrentHighscore(int p_value) {
        if (this.gameMode == 1) {
            this.highscoreLevel = p_value;
            this.highscoreLvlStr = String.valueOf(this.highscoreLevel);
        } else if (this.gameMode == 0) {
            this.highscore = p_value;
            this.highscoreStr = String.valueOf(this.highscore);
        }
    }

    private void undo() {
        copy(this.undoBilles, this.billes);
        absgrid();
        this.ListSelected.setSize(0);
        this.SelectedCell[0] = -1;
        this.SelectedCell[1] = -1;
        this.activityparent.mMenu.findItem(R.id.undo).setEnabled(false);
        this.currentCount = 0;
        this.undoAvailable = false;
        updateScore(this.undoscore * -1);
    }

    private void updateStats() {
        this.stats[this.modePack][0] = this.stats[this.modePack][0] + 1;
        this.stats[this.modePack][3] = this.stats[this.modePack][3] + ((long) this.score);
        this.stats[this.modePack][1] = this.stats[this.modePack][3] / this.stats[this.modePack][0];
        this.stats[this.modePack][2] = Math.max(this.stats[this.modePack][2], (long) this.score);
    }

    private void showAbout() {
        AlertDialog.Builder about = new AlertDialog.Builder(this.activityparent);
        about.setTitle(Html.fromHtml("<b>&nbsp;&nbsp;BUBBLE BREAKER BEACH</b>"));
        about.setIcon((int) R.drawable.icon);
        TextView l_viewabout = new TextView(this.mContext);
        l_viewabout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        l_viewabout.setPadding(IMG_PubParDefaut, IMG_menuplay, IMG_PubParDefaut, IMG_menuplay);
        l_viewabout.setTextSize(20.0f);
        l_viewabout.setText(Html.fromHtml("<small>Bubble Breaker Beach - &#169;Copyright 2011</small><br/><br/><b>Developped by:</b><br/><small>- Julien ROCCA</small><br/><small>- Ludovic GREMY</small><br/><br/><b>Published by:</b><br/><small>StoryBird:&nbsp;<a href=\"http://storybird.mobi\">storybird.mobi</a></small><br/><br/><b>Meet us on&nbsp;</b><a href=\"http://www.facebook.com/pages/Storybird/194195437285078\">Facebook&#0153;</a>"));
        l_viewabout.setMovementMethod(LinkMovementMethod.getInstance());
        about.setView(l_viewabout);
        about.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        about.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
            }
        });
        about.show();
    }

    private void showHelp() {
        AlertDialog.Builder help = new AlertDialog.Builder(this.activityparent);
        new TextView(this.mContext).setScrollContainer(true);
        help.setTitle(Html.fromHtml("<b>Help on Bubble Breaker Beach</b>"));
        help.setIcon((int) R.drawable.ic_help);
        help.setMessage(Html.fromHtml("<b><u>Games Rules:</u></b><br/><small>Tap on any connected bubbles to pop them. The largest bubbles area you pop, the more points you earn.<br/>Try to get a highscore!</small><br/><br/><b><u>Packing Modes:</u></b><br/><b><u><small>-CLASSIC:</small></u></b><br/><small>Bubbles fall down after pop. When a column is empty, bubbles are packed to right.</small><br/><b><u><small>-GRAVITY:</small></u></b><br/><small>Bubbles fall down and then right after pop.</small>"));
        help.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        help.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
            }
        });
        help.show();
    }

    private void showOptionsPack() {
        AlertDialog.Builder options = new AlertDialog.Builder(this.activityparent);
        ListView l_viewoptions = new ListView(this.mContext);
        l_viewoptions.setScrollContainer(false);
        l_viewoptions.setItemsCanFocus(false);
        l_viewoptions.setChoiceMode(1);
        l_viewoptions.setAdapter((ListAdapter) new ArrayAdapter(this.mContext, R.array.gamemode));
        options.setTitle(Html.fromHtml("<b>Select your game mode</b>"));
        options.setIcon((int) R.drawable.ic_options);
        options.setInverseBackgroundForced(true);
        options.setSingleChoiceItems((int) R.array.gamemode, this.newPackMode, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                MainView.this.tmpPackMode = whichButton;
            }
        });
        options.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (MainView.this.newPackMode != MainView.this.tmpPackMode) {
                    MainView.this.newPackMode = MainView.this.tmpPackMode;
                    if (MainView.this.gameInProgress) {
                        Toast.makeText(MainView.this.mContext, (int) R.string.changeoptions, 1).show();
                    }
                }
            }
        });
        options.show();
    }

    private void showStats() {
        AlertDialog.Builder l_stats = new AlertDialog.Builder(this.activityparent);
        l_stats.setTitle(Html.fromHtml("<b>Stastistics</b>"));
        l_stats.setIcon((int) R.drawable.ic_stats);
        l_stats.setMessage(Html.fromHtml("<b><u>CLASSIC:</u></b><br/><small>- Games: &nbsp;&nbsp;" + this.stats[0][0] + " </small>" + "<br/>" + "<small>- Average: " + this.stats[0][1] + " </small>" + "<br/>" + "<small>- Best: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + this.stats[0][2] + " </small>" + "<br/>" + "<br/>" + "<b><u>GRAVITY:</u></b>" + "<br/>" + "<small>- Games: &nbsp;&nbsp;" + this.stats[1][0] + " </small>" + "<br/>" + "<small>- Average: " + this.stats[1][1] + " </small>" + "<br/>" + "<small>- Best: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + this.stats[1][2] + " </small>"));
        l_stats.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        l_stats.setNegativeButton("Clear Stats", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 3; j++) {
                        MainView.this.stats[i][j] = 0;
                    }
                }
                MainView.this.highscore = 0;
                MainView.this.highscoreStr = String.valueOf(MainView.this.highscore);
            }
        });
        l_stats.show();
    }

    public boolean unLockSkin() {
        for (int i = 0; i < this.skinAccess.length; i++) {
            if (!this.skinAccess[i]) {
                this.skinAccess[i] = true;
                return true;
            }
        }
        return false;
    }

    public void createBubbleSounds(int p_nb) {
        int p_nb2 = Math.min(p_nb, (int) IMG_menuplay);
        for (int i = 0; i < p_nb2 - this.mMediaPlayers.size(); i++) {
            try {
                MediaPlayer l_MediaPlayer = MediaPlayer.create(this.mContext, (int) R.raw.bulles);
                l_MediaPlayer.setVolume(1.0f, 1.0f);
                l_MediaPlayer.setLooping(false);
                this.mMediaPlayers.add(l_MediaPlayer);
            } catch (Exception e) {
                Log.e("-> createBubbleSounds <-", "PB DANS createBubbleSounds");
            }
        }
    }

    public void playBubbleSound(int p_ind) {
        try {
            if (this.mMediaPlayers != null && p_ind < this.mMediaPlayers.size() - 1) {
                this.mMediaPlayers.elementAt(p_ind).start();
            }
        } catch (Exception e) {
            Log.i("playBubbleSound", "FAILURE p_ind: " + p_ind);
        }
    }

    public class WallPostRequestListener extends BaseRequestListener {
        public WallPostRequestListener() {
        }

        public void onComplete(String response, Object state) {
            MainView.this.activityparent.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(MainView.this.mContext, (int) R.string.skinunlock, 0).show();
                }
            });
        }
    }

    public class SampleDialogListener extends BaseDialogListener {
        public SampleDialogListener() {
        }

        public void onComplete(Bundle values) {
            String postId = values.getString("post_id");
            if (postId != null && MainView.this.postHighscore) {
                MainView.this.postHighscore = false;
                MainView.this.mAsyncRunner.request(postId, new WallPostRequestListener());
            }
        }
    }
}
