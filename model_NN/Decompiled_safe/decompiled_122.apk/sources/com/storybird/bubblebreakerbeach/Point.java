package com.storybird.bubblebreakerbeach;

public class Point {
    public int colonne;
    public int ligne;

    public Point(int l, int c) {
        this.ligne = l;
        this.colonne = c;
    }

    public Point(Point p) {
        this.ligne = p.ligne;
        this.colonne = p.colonne;
    }
}
