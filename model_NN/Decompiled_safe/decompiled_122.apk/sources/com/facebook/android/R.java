package com.facebook.android;

public final class R {

    public static final class array {
        public static final int gamemode = 2131034112;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int b_facebook = 2130837504;
        public static final int b_facebookflap = 2130837505;
        public static final int b_facebookflat = 2130837506;
        public static final int b_menu = 2130837507;
        public static final int b_menuflap = 2130837508;
        public static final int b_mg = 2130837509;
        public static final int b_mgflat = 2130837510;
        public static final int b_play = 2130837511;
        public static final int b_playflat = 2130837512;
        public static final int b_restart = 2130837513;
        public static final int b_restartflap = 2130837514;
        public static final int b_resume = 2130837515;
        public static final int b_resumeflap = 2130837516;
        public static final int b_share = 2130837517;
        public static final int b_shareflat = 2130837518;
        public static final int b_sms = 2130837519;
        public static final int b_smsflat = 2130837520;
        public static final int b_son = 2130837521;
        public static final int b_sonflat = 2130837522;
        public static final int bandeau_sb = 2130837523;
        public static final int bgcontactitem = 2130837524;
        public static final int bgmenu = 2130837525;
        public static final int billes = 2130837526;
        public static final int cadre_pause = 2130837527;
        public static final int cadre_titre = 2130837528;
        public static final int check = 2130837529;
        public static final int classic = 2130837530;
        public static final int contours = 2130837531;
        public static final int curseur = 2130837532;
        public static final int facebook_icon = 2130837533;
        public static final int fin_partie = 2130837534;
        public static final int fond_ingame = 2130837535;
        public static final int fond_sms = 2130837536;
        public static final int gameover = 2130837537;
        public static final int gravity = 2130837538;
        public static final int highscore = 2130837539;
        public static final int ic_about = 2130837540;
        public static final int ic_help = 2130837541;
        public static final int ic_options = 2130837542;
        public static final int ic_stats = 2130837543;
        public static final int ic_undo = 2130837544;
        public static final int icon = 2130837545;
        public static final int lock = 2130837546;
        public static final int panelscore = 2130837547;
        public static final int pause = 2130837548;
        public static final int pausebg = 2130837549;
        public static final int police_score = 2130837550;
        public static final int publisher = 2130837551;
        public static final int scrollbar = 2130837552;
        public static final int skin_select = 2130837553;
        public static final int skin_select_pieces = 2130837554;
        public static final int splash = 2130837555;
        public static final int stars = 2130837556;
        public static final int title = 2130837557;
        public static final int uncheck = 2130837558;
    }

    public static final class id {
        public static final int StatsView = 2131230720;
        public static final int about = 2131230726;
        public static final int help = 2131230723;
        public static final int mainLayout = 2131230721;
        public static final int options = 2131230725;
        public static final int stats = 2131230722;
        public static final int undo = 2131230724;
    }

    public static final class layout {
        public static final int helpview = 2130903040;
        public static final int main = 2130903041;
    }

    public static final class menu {
        public static final int game_menu = 2131165184;
        public static final int main_menu = 2131165185;
    }

    public static final class raw {
        public static final int bulles = 2130968576;
        public static final int gameover = 2130968577;
        public static final int menu = 2130968578;
        public static final int perfect = 2130968579;
    }

    public static final class string {
        public static final int about = 2131099649;
        public static final int app_name = 2131099648;
        public static final int av = 2131099661;
        public static final int best = 2131099662;
        public static final int changeoptions = 2131099655;
        public static final int help = 2131099650;
        public static final int mode1 = 2131099658;
        public static final int mode2 = 2131099659;
        public static final int nb = 2131099660;
        public static final int options = 2131099653;
        public static final int skinblocked = 2131099654;
        public static final int skinsunlock = 2131099657;
        public static final int skinunlock = 2131099656;
        public static final int sms1 = 2131099669;
        public static final int sms2 = 2131099670;
        public static final int sms3 = 2131099671;
        public static final int stats = 2131099651;
        public static final int undo = 2131099652;
        public static final int val_01 = 2131099663;
        public static final int val_02 = 2131099664;
        public static final int val_03 = 2131099665;
        public static final int val_11 = 2131099666;
        public static final int val_12 = 2131099667;
        public static final int val_13 = 2131099668;
    }
}
