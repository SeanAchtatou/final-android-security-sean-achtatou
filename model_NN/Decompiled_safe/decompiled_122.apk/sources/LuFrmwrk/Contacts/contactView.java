package LuFrmwrk.Contacts;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import com.storybird.bubblebreakerbeach.R;

public class contactView extends View {
    private static Bitmap Background;
    private static boolean loaded = false;
    private static Resources mRes;
    private static Paint paintMainSubText;
    private static Paint paintMainText;
    private boolean checked;
    private String mainText = "PIPO molo";
    private boolean selected;
    private String subText = "0612345678";

    public contactView(Context context) {
        super(context);
        mRes = context.getResources();
        initContactView();
    }

    public contactView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mRes = context.getResources();
        initContactView();
    }

    private static final void initContactView() {
        if (!loaded) {
            loaded = true;
            paintMainText = new Paint();
            paintMainSubText = new Paint();
            Background = BitmapFactory.decodeResource(mRes, R.drawable.splash);
            paintMainText.setAntiAlias(true);
            paintMainText.setTextSize(30.0f);
            paintMainText.setColor(-1);
            paintMainSubText.setAntiAlias(true);
            paintMainSubText.setTextSize(20.0f);
            paintMainSubText.setColor(-6710887);
        }
    }

    public void setSelected(boolean p_selected) {
        this.selected = p_selected;
    }

    public void setChecked(boolean p_checked) {
        this.checked = p_checked;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(measureWidth(widthMeasureSpec), measureHeight(heightMeasureSpec));
    }

    private int measureWidth(int measureSpec) {
        if (loaded) {
            return Background.getWidth();
        }
        return 0;
    }

    private int measureHeight(int measureSpec) {
        if (loaded) {
            return Background.getHeight();
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (loaded) {
            canvas.drawBitmap(Background, 0.0f, 0.0f, (Paint) null);
            canvas.drawText(this.mainText, (float) getPaddingLeft(), (float) getPaddingTop(), paintMainText);
            canvas.drawText(this.subText, (float) getPaddingLeft(), (float) (getPaddingTop() + (Background.getHeight() / 2)), paintMainSubText);
        }
    }
}
