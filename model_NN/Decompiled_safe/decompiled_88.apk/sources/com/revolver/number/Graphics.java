package com.revolver.number;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.SurfaceHolder;

public class Graphics {
    private Canvas canvas;
    private SurfaceHolder holder;
    private Paint paint = new Paint();

    public Graphics(SurfaceHolder holder2) {
        this.holder = holder2;
        this.paint.setAntiAlias(true);
    }

    public void lock() {
        this.canvas = this.holder.lockCanvas();
    }

    public void unlock() {
        this.holder.unlockCanvasAndPost(this.canvas);
    }

    public void setColor(int color) {
        this.paint.setColor(color);
    }

    public void setFontSize(int fontSize) {
        this.paint.setTextSize((float) fontSize);
    }

    public void setFontSize2(int fontSize) {
        this.paint.setTextSize((float) fontSize);
        this.paint.setTextAlign(Paint.Align.RIGHT);
    }

    public int stringWidth(String string) {
        return (int) this.paint.measureText(string);
    }

    public void fillRect(int x, int y, int w, int h) {
        this.paint.setStyle(Paint.Style.FILL);
        this.canvas.drawRect(new Rect(x, y, x + w, y + h), this.paint);
    }

    public void drawBitmap(Bitmap bitmap, int x, int y) {
        this.canvas.drawBitmap(bitmap, (float) x, (float) y, (Paint) null);
    }

    public void drawBigBitmap(Bitmap bitmap, int x, int y, int width, int height) {
        this.canvas.drawBitmap(bitmap, new Rect(x, y, 400, 600), new Rect(x, y, width, height), (Paint) null);
    }

    public void drawString(String string, int x, int y) {
        this.canvas.drawText(string, (float) x, (float) y, this.paint);
    }
}
