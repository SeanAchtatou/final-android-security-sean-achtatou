package com.revolver.number;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class Puzzle extends Activity {
    private static final String AD_UNIT_ID = "a14e4a976f604ae";
    public static final int CREATE_AD = 1;
    public static final int REMOVE_AD = 2;
    private static final String TEST_DEVICE_ID = "********************************";
    public static final String version = "1.00";
    public AdView mAdView = null;
    private Handler mHandler;
    private FrameLayout mLayout;
    private puzzleView mMainView = null;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        this.mLayout = new FrameLayout(this);
        Intent intent = getIntent();
        int intExtra = intent.getIntExtra("stage", 1);
        this.mMainView = new puzzleView(this);
        TextView tv = new TextView(this);
        try {
            HttpURLConnection http = (HttpURLConnection) new URL("http://www24.atpages.jp/spaceflash2/saru/saru_version.html").openConnection();
            http.setRequestMethod("GET");
            http.connect();
            InputStream in = http.getInputStream();
            byte[] b = new byte[1024];
            in.read(b);
            in.close();
            http.disconnect();
            String[] c = new String(b).split("version", 0);
            if (version.equals(c[1])) {
                this.mLayout.addView(this.mMainView);
            } else {
                tv.setText(c[1]);
                this.mLayout.addView(tv);
            }
        } catch (Exception e) {
            tv.setText("NOT WEB SERVICE!!");
            this.mLayout.addView(tv);
            this.mLayout.addView(this.mMainView);
        }
        requestWindowFeature(1);
        setRequestedOrientation(0);
        setContentView(this.mLayout);
        setResult(-1, intent);
        createAd();
    }

    public void createAd() {
        if (this.mAdView == null) {
            this.mAdView = new AdView(this, AdSize.BANNER, AD_UNIT_ID);
            AdRequest req = new AdRequest();
            req.addTestDevice(AdRequest.TEST_EMULATOR);
            this.mAdView.loadAd(req);
            this.mLayout.addView(this.mAdView, new FrameLayout.LayoutParams(-1, -2, 48));
        }
    }

    public void removeAd() {
        if (this.mAdView != null) {
            this.mLayout.removeView(this.mAdView);
            this.mAdView = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.mAdView.destroy();
        super.onDestroy();
    }
}
