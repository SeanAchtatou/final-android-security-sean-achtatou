package com.revolver.number;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int bk = 2130837504;
        public static final int game_bk = 2130837505;
        public static final int icon = 2130837506;
        public static final int logo = 2130837507;
        public static final int music_no = 2130837508;
        public static final int music_on = 2130837509;
        public static final int pr1 = 2130837510;
        public static final int pr2 = 2130837511;
        public static final int puzzle_0 = 2130837512;
        public static final int puzzle_1 = 2130837513;
        public static final int puzzle_2 = 2130837514;
        public static final int puzzle_3 = 2130837515;
        public static final int puzzle_4 = 2130837516;
        public static final int puzzle_5 = 2130837517;
        public static final int rank_menu = 2130837518;
        public static final int title = 2130837519;
        public static final int title_menu_bk = 2130837520;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class raw {
        public static final int back = 2130968576;
        public static final int fire1 = 2130968577;
        public static final int fire2 = 2130968578;
    }

    public static final class string {
        public static final int app_name = 2131034113;
        public static final int hello = 2131034112;
    }
}
