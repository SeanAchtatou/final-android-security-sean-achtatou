package com.revolver.number;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;

public class puzzleView extends SurfaceView implements SurfaceHolder.Callback, Runnable {
    public static Bitmap B_2 = null;
    public static Bitmap B_3 = null;
    public static Bitmap B_4 = null;
    public static Bitmap B_5 = null;
    private static final int B_HEIGHT = 40;
    private static final int B_WIDTH = 40;
    private static final int MAX_GME_OVER = 5;
    private static final int MAX_NUMBER = 9;
    private static final int POINTS = 100;
    public static Bitmap PR2Bmp = null;
    private static final int P_HEIGHT = 370;
    private static final int P_WIDTH = 250;
    private static final int P_X = 100;
    private static final int P_Y = 60;
    private static final int S_GAMEOVER = 3;
    private static final int S_KIDOU = 4;
    private static final int S_K_RANK = 1;
    private static final int S_PLAY = 2;
    private static final int S_PR1 = 6;
    private static final int S_PR2 = 7;
    private static final int S_SENDEN = 5;
    private static final int S_TITLE = 0;
    private static final int T_Time = 10;
    private static final int T_maxTime = 600;
    public static Bitmap gameBackBmp = null;
    public static Bitmap logoBmp = null;
    public static int maxMonsters = 50;
    private static final int music_def = 120;
    public static Bitmap music_off = null;
    public static Bitmap music_on = null;
    private static final int music_x = 580;
    private static final int music_y = 200;
    private static final int play_height = 100;
    private static final int play_start_x = 150;
    private static final int play_start_y = 230;
    private static final int play_width = 150;
    private static final int prev_height = 90;
    private static final int prev_start_x = 560;
    private static final int prev_start_y = 290;
    private static final int prev_width = 150;
    public static Bitmap rankBmp = null;
    private static final int rank_height = 100;
    private static final int rank_start_x = 330;
    private static final int rank_start_y = 230;
    private static final int rank_width = 150;
    public static Bitmap titleBackBmp;
    private MediaPlayer attackMusic1;
    private MediaPlayer attackMusic2;
    private MediaPlayer backMusic;
    public int bk_x;
    public int bk_y;
    private ArrayList<Block> blocks = new ArrayList<>();
    int ckFlg = S_TITLE;
    public int ckline = T_Time;
    public Context context;
    private String debug = "";
    public int firstFlg = 1;
    private Graphics g;
    public int gameOverTime = S_TITLE;
    private SurfaceHolder holder;
    private int init = S_TITLE;
    public int judgeFlg = S_TITLE;
    public int kidouTime = S_TITLE;
    public int leftRightFlg = S_TITLE;
    public int maxCkline = S_TITLE;
    public int moveDownFlg = S_TITLE;
    public int moveFlg = 1;
    public int musicChangeFlg = S_TITLE;
    public int musicFlg = 1;
    private Context myCon;
    public int newBlockFlg = 1;
    private ArrayList<Block> newBlocks = new ArrayList<>();
    private ArrayList<Block> nextBlocks = new ArrayList<>();
    public int ojyamaFlg = S_TITLE;
    private int rank1;
    private int rank2;
    private int rank3;
    private int rank4;
    private int rank5;
    public int renBlockFlg = S_TITLE;
    private int score = S_TITLE;
    public int simpleFlg = S_TITLE;
    private int stage = 1;
    private int state = S_KIDOU;
    private Thread thread;
    private int timer;
    public int turnFlg = 1;

    public puzzleView(Context context2) {
        super(context2);
        this.myCon = context2;
        Resources r = getResources();
        gameBackBmp = BitmapFactory.decodeResource(r, R.drawable.game_bk);
        titleBackBmp = BitmapFactory.decodeResource(r, R.drawable.title_menu_bk);
        logoBmp = BitmapFactory.decodeResource(r, R.drawable.pr1);
        rankBmp = BitmapFactory.decodeResource(r, R.drawable.rank_menu);
        music_on = BitmapFactory.decodeResource(r, R.drawable.music_on);
        music_off = BitmapFactory.decodeResource(r, R.drawable.music_no);
        PR2Bmp = BitmapFactory.decodeResource(r, R.drawable.pr2);
        B_4 = BitmapFactory.decodeResource(r, R.drawable.puzzle_4);
        B_2 = BitmapFactory.decodeResource(r, R.drawable.puzzle_2);
        B_3 = BitmapFactory.decodeResource(r, R.drawable.puzzle_3);
        B_5 = BitmapFactory.decodeResource(r, R.drawable.puzzle_0);
        this.holder = getHolder();
        this.holder.addCallback(this);
        this.g = new Graphics(this.holder);
        this.backMusic = MediaPlayer.create(context2, (int) R.raw.back);
        this.backMusic.setVolume(1.0f, 1.0f);
        this.attackMusic1 = MediaPlayer.create(context2, (int) R.raw.fire1);
        this.attackMusic1.setVolume(1.0f, 1.0f);
        this.attackMusic2 = MediaPlayer.create(context2, (int) R.raw.fire2);
        this.attackMusic2.setVolume(1.0f, 1.0f);
    }

    public void saveFile() {
        if (this.rank5 < this.score) {
            if (this.rank4 >= this.score) {
                this.rank5 = this.score;
            } else if (this.rank3 >= this.score) {
                this.rank5 = this.rank4;
                this.rank4 = this.score;
            } else if (this.rank2 >= this.score) {
                this.rank5 = this.rank4;
                this.rank4 = this.rank3;
                this.rank3 = this.score;
            } else if (this.rank1 < this.score) {
                this.rank5 = this.rank4;
                this.rank4 = this.rank3;
                this.rank3 = this.rank2;
                this.rank2 = this.rank1;
                this.rank1 = this.score;
            } else {
                this.rank5 = this.rank4;
                this.rank4 = this.rank3;
                this.rank3 = this.rank2;
                this.rank2 = this.score;
            }
        }
        SharedPreferences.Editor e = PreferenceManager.getDefaultSharedPreferences(this.myCon).edit();
        e.putInt("rank1", this.rank1);
        e.putInt("rank2", this.rank2);
        e.putInt("rank3", this.rank3);
        e.putInt("rank4", this.rank4);
        e.putInt("rank5", this.rank5);
        e.commit();
    }

    public void loadFile() {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this.myCon);
        this.rank1 = pref.getInt("rank1", S_TITLE);
        this.rank2 = pref.getInt("rank2", S_TITLE);
        this.rank3 = pref.getInt("rank3", S_TITLE);
        this.rank4 = pref.getInt("rank4", S_TITLE);
        this.rank5 = pref.getInt("rank5", S_TITLE);
    }

    public void surfaceCreated(SurfaceHolder holder2) {
        this.thread = new Thread(this);
        this.thread.start();
    }

    public void surfaceDestroyed(SurfaceHolder holder2) {
        this.thread = null;
        stop_music(this.backMusic);
    }

    public void surfaceChanged(SurfaceHolder holder2, int format, int w, int h) {
    }

    public void start_music(MediaPlayer mj) {
        if (!mj.isPlaying()) {
            mj.seekTo(S_TITLE);
            mj.start();
        }
    }

    public void stop_music(MediaPlayer mj) {
        if (mj.isPlaying()) {
            mj.pause();
        }
    }

    public void run() {
        Random random = new Random();
        while (this.thread != null) {
            if (this.state == S_KIDOU) {
                this.g.lock();
                this.g.drawBitmap(logoBmp, S_TITLE, S_TITLE);
                this.kidouTime++;
                if (this.kidouTime == 5) {
                    this.state = S_PR2;
                }
                this.g.unlock();
            } else if (this.state == S_PR2) {
                this.g.lock();
                Resources resources = getResources();
                if (this.ckFlg == 5) {
                    this.state = S_TITLE;
                    this.ckFlg = S_TITLE;
                }
                this.g.drawBitmap(titleBackBmp, S_TITLE, S_TITLE);
                this.ckFlg++;
                this.g.drawBitmap(PR2Bmp, 20, 100);
                this.g.unlock();
            } else if (this.state == 1) {
                this.g.lock();
                loadFile();
                this.g.setColor(-1);
                this.g.setFontSize2(36);
                this.g.drawBitmap(rankBmp, S_TITLE, S_TITLE);
                this.g.drawString(String.valueOf(this.rank1), 410, 173);
                this.g.drawString(String.valueOf(this.rank2), 410, 218);
                this.g.drawString(String.valueOf(this.rank3), 410, 260);
                this.g.drawString(String.valueOf(this.rank4), 410, 304);
                this.g.drawString(String.valueOf(this.rank5), 410, 345);
                this.g.unlock();
            } else if (this.state == 0) {
                this.g.lock();
                this.firstFlg = 1;
                this.g.drawBitmap(titleBackBmp, S_TITLE, S_TITLE);
                if (this.musicFlg == 1) {
                    this.g.drawBitmap(music_on, music_x, music_y);
                } else {
                    this.g.drawBitmap(music_off, music_x, music_y);
                }
                this.g.unlock();
            } else if (this.state == S_GAMEOVER) {
                stop_music(this.backMusic);
                this.gameOverTime++;
                if (this.gameOverTime > 5) {
                    saveFile();
                    this.state = 2;
                    this.newBlocks.clear();
                    this.blocks.clear();
                    this.renBlockFlg = S_TITLE;
                    this.newBlockFlg = 1;
                    this.ckline = T_Time;
                    this.moveFlg = 1;
                    this.moveDownFlg = S_TITLE;
                    this.leftRightFlg = S_TITLE;
                    this.simpleFlg = S_TITLE;
                    this.maxCkline = S_TITLE;
                    this.gameOverTime = S_TITLE;
                    this.score = S_TITLE;
                    this.firstFlg = 1;
                    this.state = S_TITLE;
                }
            } else if (this.state == 2) {
                if (this.musicFlg == 1) {
                    start_music(this.backMusic);
                }
                this.g.lock();
                this.g.drawBitmap(gameBackBmp, S_TITLE, S_TITLE);
                if (this.newBlockFlg == 1) {
                    if (this.score < 100 || this.ojyamaFlg != 0) {
                        this.ojyamaFlg = S_TITLE;
                    } else {
                        int point1 = random.nextInt(5);
                        if (point1 == 2) {
                            this.ojyamaFlg = 1;
                            this.renBlockFlg = S_KIDOU;
                            this.simpleFlg = 1;
                            int point12 = random.nextInt(S_PR1);
                            if (point12 == 0) {
                                point12 = 5;
                            }
                            this.newBlocks.clear();
                            this.newBlocks.add(new Block(point12, T_Time, S_TITLE));
                            this.newBlocks.add(new Block(point12 + 1, T_Time, S_TITLE));
                        } else if (point1 == S_GAMEOVER && this.score >= 2500) {
                            this.ojyamaFlg = 1;
                            this.renBlockFlg = S_KIDOU;
                            this.simpleFlg = 1;
                            int point13 = random.nextInt(S_PR1);
                            if (point13 == 0) {
                                point13 = 5;
                            }
                            this.newBlocks.clear();
                            this.newBlocks.add(new Block(point13, T_Time, S_TITLE));
                            this.newBlocks.add(new Block(point13 + 1, T_Time, S_TITLE));
                        }
                    }
                    if (this.ojyamaFlg == 0) {
                        if (this.firstFlg == 1) {
                            loadFile();
                            this.firstFlg = S_TITLE;
                            int point14 = random.nextInt(S_KIDOU);
                            if (point14 == 0) {
                                point14 = S_GAMEOVER;
                            } else if (point14 == 1) {
                                point14 = S_KIDOU;
                            }
                            int point2 = random.nextInt(S_KIDOU);
                            if (point2 == 0) {
                                point2 = S_GAMEOVER;
                            } else if (point2 == 1) {
                                point2 = S_KIDOU;
                            }
                            this.nextBlocks.clear();
                            this.nextBlocks.add(new Block(2, T_Time, point14));
                            this.nextBlocks.add(new Block(S_GAMEOVER, T_Time, point2));
                        }
                        this.newBlocks.clear();
                        this.newBlocks = (ArrayList) this.nextBlocks.clone();
                        int point15 = random.nextInt(S_KIDOU);
                        if (point15 == 0) {
                            point15 = S_GAMEOVER;
                        } else if (point15 == 1) {
                            point15 = S_KIDOU;
                        }
                        int point22 = random.nextInt(S_KIDOU);
                        if (point22 == 0) {
                            point22 = S_GAMEOVER;
                        } else if (point22 == 1) {
                            point22 = S_KIDOU;
                        }
                        this.nextBlocks.clear();
                        this.nextBlocks.add(new Block(2, T_Time, point15));
                        this.nextBlocks.add(new Block(S_GAMEOVER, T_Time, point22));
                    }
                }
                if (this.renBlockFlg == 0 || this.renBlockFlg == S_KIDOU) {
                    this.newBlockFlg = S_TITLE;
                    newBrockMove();
                } else if (this.renBlockFlg == 2) {
                    rensaBrockMove();
                }
                block_del();
                draw_block();
                this.g.setColor(-1);
                this.g.setFontSize(46);
                String buf = "";
                if (this.score < T_Time) {
                    buf = "        ";
                } else if (this.score < 100) {
                    buf = "     ";
                } else if (this.score < 1000) {
                    buf = "    ";
                } else if (this.score < 10000) {
                    buf = "  ";
                } else if (this.score < 100000) {
                    buf = " ";
                } else if (this.score < 1000000) {
                    buf = "";
                }
                this.g.drawString(String.valueOf(buf) + String.valueOf(this.score), 500, 136);
                this.g.unlock();
            }
            try {
                if (this.renBlockFlg == 0) {
                    Thread.sleep(800);
                }
                this.timer++;
                if (this.timer > T_maxTime) {
                    this.timer = S_TITLE;
                }
            } catch (Exception e) {
            }
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();
        int touchAction = event.getAction();
        if (touchAction == 0) {
            if (this.state == S_PR2) {
                this.myCon.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://market.android.com/search?q=pname:net.monkey")));
            } else if (this.state == 1 && ((int) event.getX()) > prev_start_x && ((int) event.getX()) < 710 && ((int) event.getY()) > prev_start_y && ((int) event.getY()) < 380) {
                this.state = S_TITLE;
            }
            if (this.state == 2) {
                this.bk_x = (int) event.getX();
                this.bk_y = (int) event.getY();
            } else if (this.state == 0) {
                if (((int) event.getX()) > rank_start_x && ((int) event.getX()) < 480 && ((int) event.getY()) > 230 && ((int) event.getY()) < rank_start_x) {
                    this.state = 1;
                }
                if (((int) event.getX()) > music_x && ((int) event.getX()) < 700 && ((int) event.getY()) > music_y && ((int) event.getY()) < 320) {
                    if (this.musicFlg == 1) {
                        this.musicFlg = S_TITLE;
                        this.musicChangeFlg = 1;
                    } else {
                        this.musicFlg = 1;
                        this.musicChangeFlg = 1;
                    }
                }
                if (((int) event.getX()) > 150 && ((int) event.getX()) < 300 && ((int) event.getY()) > 230 && ((int) event.getY()) < rank_start_x) {
                    this.state = 2;
                    this.turnFlg = S_TITLE;
                }
            }
        } else if (touchAction == 2) {
            if (this.simpleFlg == 0) {
                if (((float) this.bk_y) - event.getY() > 25.0f || ((float) this.bk_y) - event.getY() < -25.0f) {
                    this.leftRightFlg = S_GAMEOVER;
                    this.simpleFlg = 1;
                    this.turnFlg = S_TITLE;
                    this.bk_y = (int) event.getY();
                    this.bk_x = (int) event.getX();
                } else if (((float) this.bk_x) - event.getX() > 10.0f || ((float) this.bk_x) - event.getX() < -10.0f) {
                    if (((float) this.bk_x) > event.getX()) {
                        this.leftRightFlg = 1;
                        this.bk_y = (int) event.getY();
                        this.bk_x = (int) event.getX();
                        this.turnFlg = S_TITLE;
                    } else {
                        this.leftRightFlg = 2;
                        this.bk_y = (int) event.getY();
                        this.bk_x = (int) event.getX();
                        this.turnFlg = S_TITLE;
                    }
                }
            }
        } else if (touchAction == 1 && this.simpleFlg == 0) {
            if (this.turnFlg == 1 && ((((float) this.bk_x) - event.getX() < 25.0f || ((float) this.bk_x) - event.getX() > -25.0f) && (((float) this.bk_y) - event.getY() < 25.0f || ((float) this.bk_y) - event.getY() > -25.0f))) {
                this.leftRightFlg = S_KIDOU;
            }
            this.turnFlg = 1;
        }
        return true;
    }

    public void draw_block() {
        for (int i = S_TITLE; i < this.nextBlocks.size(); i++) {
            if (this.nextBlocks.get(i).getNo() == S_KIDOU) {
                this.g.drawBitmap(B_4, ((this.nextBlocks.get(i).getX() - 1) * 40) + 100 + 420, ((MAX_NUMBER - this.nextBlocks.get(i).getY()) * 40) + P_Y + 280);
            } else if (this.nextBlocks.get(i).getNo() == 2) {
                this.g.drawBitmap(B_2, ((this.nextBlocks.get(i).getX() - 1) * 40) + 100 + 420, ((MAX_NUMBER - this.nextBlocks.get(i).getY()) * 40) + P_Y + 280);
            } else {
                this.g.drawBitmap(B_3, ((this.nextBlocks.get(i).getX() - 1) * 40) + 100 + 420, ((MAX_NUMBER - this.nextBlocks.get(i).getY()) * 40) + P_Y + 280);
            }
        }
        for (int i2 = S_TITLE; i2 < this.newBlocks.size(); i2++) {
            if (this.newBlocks.get(i2).getNo() == S_KIDOU) {
                this.g.drawBitmap(B_4, ((this.newBlocks.get(i2).getX() - 1) * 40) + 100, ((MAX_NUMBER - this.newBlocks.get(i2).getY()) * 40) + P_Y);
            } else if (this.newBlocks.get(i2).getNo() == 2) {
                this.g.drawBitmap(B_2, ((this.newBlocks.get(i2).getX() - 1) * 40) + 100, ((MAX_NUMBER - this.newBlocks.get(i2).getY()) * 40) + P_Y);
            } else if (this.newBlocks.get(i2).getNo() == 0) {
                this.g.drawBitmap(B_5, ((this.newBlocks.get(i2).getX() - 1) * 40) + 100, ((MAX_NUMBER - this.newBlocks.get(i2).getY()) * 40) + P_Y);
            } else {
                this.g.drawBitmap(B_3, ((this.newBlocks.get(i2).getX() - 1) * 40) + 100, ((MAX_NUMBER - this.newBlocks.get(i2).getY()) * 40) + P_Y);
            }
        }
        for (int i3 = S_TITLE; i3 < this.blocks.size(); i3++) {
            if (this.blocks.get(i3).getNo() == S_KIDOU) {
                this.g.drawBitmap(B_4, ((this.blocks.get(i3).getX() - 1) * 40) + 100, ((MAX_NUMBER - this.blocks.get(i3).getY()) * 40) + P_Y);
            } else if (this.blocks.get(i3).getNo() == 2) {
                this.g.drawBitmap(B_2, ((this.blocks.get(i3).getX() - 1) * 40) + 100, ((MAX_NUMBER - this.blocks.get(i3).getY()) * 40) + P_Y);
            } else if (this.blocks.get(i3).getNo() == 0) {
                this.g.drawBitmap(B_5, ((this.blocks.get(i3).getX() - 1) * 40) + 100, ((MAX_NUMBER - this.blocks.get(i3).getY()) * 40) + P_Y);
            } else {
                this.g.drawBitmap(B_3, ((this.blocks.get(i3).getX() - 1) * 40) + 100, ((MAX_NUMBER - this.blocks.get(i3).getY()) * 40) + P_Y);
            }
        }
    }

    public void rensaBrockMove() {
        if (this.moveFlg == 0) {
            int ck_flg = S_TITLE;
            int ck_flg2 = S_TITLE;
            if (this.maxCkline == T_Time) {
                for (int i = S_TITLE; i < this.blocks.size(); i++) {
                    if (this.blocks.get(i).getY() == this.ckline) {
                        for (int j = S_TITLE; j < this.blocks.size(); j++) {
                            if (this.blocks.get(j).getY() == this.ckline - 1 && this.blocks.get(j).getX() == this.blocks.get(i).getX()) {
                                ck_flg = 1;
                            }
                        }
                        if (ck_flg == 0) {
                            this.blocks.get(i).setY(this.ckline - 1);
                            ck_flg2 = 1;
                            this.judgeFlg = 1;
                        } else {
                            ck_flg = S_TITLE;
                        }
                    }
                }
                if (ck_flg2 == 1 && this.musicFlg == 1) {
                    start_music(this.attackMusic2);
                }
                this.ckline++;
                if (this.ckline > this.maxCkline && this.judgeFlg == 0) {
                    this.renBlockFlg = S_TITLE;
                    this.newBlockFlg = 1;
                    this.ckline = T_Time;
                    this.moveFlg = 1;
                    this.moveDownFlg = S_TITLE;
                    this.leftRightFlg = S_TITLE;
                    this.simpleFlg = S_TITLE;
                    this.maxCkline = S_TITLE;
                    this.ojyamaFlg = S_TITLE;
                } else if (this.ckline > this.maxCkline) {
                    this.ckline = 2;
                    this.judgeFlg = S_TITLE;
                }
            } else {
                this.renBlockFlg = S_TITLE;
                this.newBlockFlg = 1;
                this.ckline = T_Time;
                this.moveFlg = 1;
                this.moveDownFlg = S_TITLE;
                this.leftRightFlg = S_TITLE;
                this.simpleFlg = S_TITLE;
                this.maxCkline = S_TITLE;
                this.ojyamaFlg = S_TITLE;
            }
        }
    }

    public void block_del() {
        int delCnt = S_TITLE;
        int[][] data = (int[][]) Array.newInstance(Integer.TYPE, S_PR1, T_Time);
        if (this.renBlockFlg == 1 || this.renBlockFlg == 2) {
            for (int line_T = S_TITLE; line_T < MAX_NUMBER; line_T++) {
                for (int line_Y = S_TITLE; line_Y < S_PR1; line_Y++) {
                    data[line_Y][line_T] = -1;
                }
            }
            for (int i = S_TITLE; i < this.blocks.size(); i++) {
                int x = this.blocks.get(i).getX();
                int y = this.blocks.get(i).getY();
                data[this.blocks.get(i).getX() - 1][this.blocks.get(i).getY() - 1] = i;
            }
            for (int line_T2 = S_TITLE; line_T2 < MAX_NUMBER; line_T2++) {
                for (int line_Y2 = S_TITLE; line_Y2 < 5; line_Y2++) {
                    if (data[line_Y2][line_T2] != -1) {
                        int ck_y = this.blocks.get(data[line_Y2][line_T2]).getNo();
                        int j = line_Y2 + 1;
                        while (true) {
                            if (j >= S_PR1) {
                                break;
                            } else if (data[line_Y2][line_T2] != -1 && data[j][line_T2] != -1) {
                                ck_y += this.blocks.get(data[j][line_T2]).getNo();
                                if (ck_y == MAX_NUMBER) {
                                    delCnt++;
                                    for (int i2 = line_Y2; i2 <= j; i2++) {
                                        this.blocks.get(data[i2][line_T2]).setState(1);
                                    }
                                } else if (ck_y > MAX_NUMBER) {
                                    break;
                                } else {
                                    j++;
                                }
                            }
                        }
                    }
                }
            }
            for (int line_Y3 = S_TITLE; line_Y3 < S_PR1; line_Y3++) {
                for (int line_T3 = S_TITLE; line_T3 < 8; line_T3++) {
                    if (data[line_Y3][line_T3] != -1) {
                        int ck_t = this.blocks.get(data[line_Y3][line_T3]).getNo();
                        int j2 = line_T3 + 1;
                        while (true) {
                            if (j2 >= MAX_NUMBER) {
                                break;
                            } else if (data[line_Y3][line_T3] != -1 && data[line_Y3][j2] != -1) {
                                ck_t += this.blocks.get(data[line_Y3][j2]).getNo();
                                if (ck_t == MAX_NUMBER) {
                                    delCnt++;
                                    for (int i3 = line_T3; i3 <= j2; i3++) {
                                        this.blocks.get(data[line_Y3][i3]).setState(1);
                                    }
                                } else if (ck_t > MAX_NUMBER) {
                                    break;
                                } else {
                                    j2++;
                                }
                            }
                        }
                    }
                }
            }
            if (delCnt == 0) {
                this.renBlockFlg = 2;
            } else {
                if (this.musicFlg == 1) {
                    start_music(this.attackMusic1);
                }
                int i4 = S_TITLE;
                while (i4 < this.blocks.size()) {
                    if (this.blocks.get(i4).getState() == 1) {
                        if (this.maxCkline < this.blocks.get(i4).getY()) {
                            this.maxCkline = this.blocks.get(i4).getY();
                        }
                        this.blocks.remove(i4);
                        i4--;
                    }
                    i4++;
                }
                this.maxCkline = T_Time;
                this.ckline = 2;
                this.renBlockFlg = 2;
            }
            this.score += delCnt * 100;
        }
    }

    public void newBrockMove() {
        int maxMoveY1;
        int maxMoveY2;
        this.moveFlg = 1;
        int moveX1 = this.newBlocks.get(S_TITLE).getX();
        int moveY1 = this.newBlocks.get(S_TITLE).getY();
        int moveX2 = this.newBlocks.get(1).getX();
        int moveY2 = this.newBlocks.get(1).getY();
        int maxMoveY12 = S_TITLE;
        int maxMoveY22 = S_TITLE;
        if (this.leftRightFlg == 1) {
            if (moveX1 > 1 && moveX2 > 1) {
                this.moveDownFlg = S_TITLE;
                moveX1--;
                moveX2--;
            }
        } else if (this.leftRightFlg == 2) {
            if (moveX1 < S_PR1 && moveX2 < S_PR1) {
                this.moveDownFlg = S_TITLE;
                moveX1++;
                moveX2++;
            }
        } else if (this.leftRightFlg == S_KIDOU) {
            if (moveX1 > moveX2) {
                if (moveY2 <= MAX_NUMBER) {
                    moveX2 = moveX1;
                    moveY2 = moveY1 + 1;
                } else {
                    moveX2 = moveX1 + 1;
                    moveY2 = moveY1;
                }
            } else if (moveX1 != moveX2) {
                moveX2 = moveX1;
                moveY2 = moveY1 - 1;
            } else if (moveY1 > moveY2) {
                if (moveX1 - 1 >= 1) {
                    moveX2 = moveX1 - 1;
                    moveY2 = moveY1;
                } else if (moveY2 <= MAX_NUMBER) {
                    moveX2 = moveX1;
                    moveY2 = moveY1 + 1;
                } else {
                    moveX2 = moveX1 + 1;
                    moveY2 = moveY1;
                }
            } else if (moveY1 < moveY2) {
                if (moveX1 + 1 <= S_PR1) {
                    moveX2 = moveX1 + 1;
                    moveY2 = moveY1;
                } else if (moveY2 >= 1) {
                    moveX2 = moveX1;
                    moveY2 = moveY1 - 1;
                } else {
                    moveX2 = moveX1 - 1;
                    moveY2 = moveY1;
                }
            }
        }
        for (int i = S_TITLE; i < this.blocks.size(); i++) {
            if (this.blocks.get(i).getX() == moveX1 && maxMoveY12 <= this.blocks.get(i).getY()) {
                maxMoveY12 = this.blocks.get(i).getY();
            }
            if (this.blocks.get(i).getX() == moveX2 && maxMoveY22 <= this.blocks.get(i).getY()) {
                maxMoveY22 = this.blocks.get(i).getY();
            }
        }
        if (moveX1 != moveX2) {
            maxMoveY1 = maxMoveY12 + 1;
            maxMoveY2 = maxMoveY22 + 1;
        } else if (moveY1 > moveY2) {
            maxMoveY1 = maxMoveY12 + 2;
            maxMoveY2 = maxMoveY22 + 1;
        } else {
            maxMoveY1 = maxMoveY12 + 1;
            maxMoveY2 = maxMoveY22 + 2;
        }
        if (this.leftRightFlg == S_GAMEOVER) {
            this.simpleFlg = 2;
            this.newBlocks.get(S_TITLE).setX(moveX1);
            this.newBlocks.get(S_TITLE).setY(maxMoveY1);
            this.newBlocks.get(1).setX(moveX2);
            this.newBlocks.get(1).setY(maxMoveY2);
        } else if (moveY1 <= maxMoveY1 && moveY2 <= maxMoveY2) {
            this.moveFlg = S_TITLE;
            this.simpleFlg = 2;
            this.newBlocks.get(S_TITLE).setX(moveX1);
            this.newBlocks.get(S_TITLE).setY(maxMoveY1);
            this.newBlocks.get(1).setX(moveX2);
            this.newBlocks.get(1).setY(maxMoveY2);
            this.blocks.add(this.newBlocks.get(S_TITLE));
            this.blocks.add(this.newBlocks.get(1));
            this.newBlocks.clear();
            this.renBlockFlg = 1;
            if (maxMoveY1 >= T_Time || maxMoveY2 >= T_Time) {
                this.state = S_GAMEOVER;
            } else if (this.musicFlg == 1) {
                start_music(this.attackMusic2);
            }
        } else if (moveY1 <= maxMoveY1 || moveY2 <= maxMoveY2) {
            this.simpleFlg = 1;
            if (moveY1 <= maxMoveY1) {
                this.newBlocks.get(S_TITLE).setX(moveX1);
                this.newBlocks.get(S_TITLE).setY(maxMoveY1);
                this.newBlocks.get(1).setX(moveX2);
                this.newBlocks.get(1).setY(moveY2 - 1);
            } else {
                this.newBlocks.get(S_TITLE).setX(moveX1);
                this.newBlocks.get(S_TITLE).setY(moveY1 - 1);
                this.newBlocks.get(1).setX(moveX2);
                this.newBlocks.get(1).setY(maxMoveY2);
            }
        } else {
            if (!(this.leftRightFlg == S_KIDOU || this.leftRightFlg == 1 || this.leftRightFlg == 2)) {
                moveY1--;
                moveY2--;
            }
            this.newBlocks.get(S_TITLE).setX(moveX1);
            this.newBlocks.get(S_TITLE).setY(moveY1);
            this.newBlocks.get(1).setX(moveX2);
            this.newBlocks.get(1).setY(moveY2);
        }
        this.moveDownFlg = S_TITLE;
        this.leftRightFlg = S_TITLE;
    }
}
