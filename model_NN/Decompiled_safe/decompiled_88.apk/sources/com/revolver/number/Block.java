package com.revolver.number;

public class Block {
    public int no;
    public int state = 0;
    public int x;
    public int y;

    public Block(int x2, int y2, int no2) {
        this.x = x2;
        this.y = y2;
        this.no = no2;
    }

    public int getX() {
        return this.x;
    }

    public void setX(int x2) {
        this.x = x2;
    }

    public int getState() {
        return this.state;
    }

    public void setState(int state2) {
        this.state = state2;
    }

    public int getY() {
        return this.y;
    }

    public void setY(int y2) {
        this.y = y2;
    }

    public int getNo() {
        return this.no;
    }

    public void setNo(int no2) {
        this.no = no2;
    }
}
