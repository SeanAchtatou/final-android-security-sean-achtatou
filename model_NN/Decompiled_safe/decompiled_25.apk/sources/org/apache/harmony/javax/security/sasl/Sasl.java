package org.apache.harmony.javax.security.sasl;

import java.security.Provider;
import java.security.Security;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import org.apache.harmony.javax.security.auth.callback.CallbackHandler;

public class Sasl {
    private static final String CLIENTFACTORYSRV = "SaslClientFactory";
    public static final String MAX_BUFFER = "javax.security.sasl.maxbuffer";
    public static final String POLICY_FORWARD_SECRECY = "javax.security.sasl.policy.forward";
    public static final String POLICY_NOACTIVE = "javax.security.sasl.policy.noactive";
    public static final String POLICY_NOANONYMOUS = "javax.security.sasl.policy.noanonymous";
    public static final String POLICY_NODICTIONARY = "javax.security.sasl.policy.nodictionary";
    public static final String POLICY_NOPLAINTEXT = "javax.security.sasl.policy.noplaintext";
    public static final String POLICY_PASS_CREDENTIALS = "javax.security.sasl.policy.credentials";
    public static final String QOP = "javax.security.sasl.qop";
    public static final String RAW_SEND_SIZE = "javax.security.sasl.rawsendsize";
    public static final String REUSE = "javax.security.sasl.reuse";
    private static final String SERVERFACTORYSRV = "SaslServerFactory";
    public static final String SERVER_AUTH = "javax.security.sasl.server.authentication";
    public static final String STRENGTH = "javax.security.sasl.strength";

    private Sasl() {
    }

    private static Object newInstance(String factoryName, Provider prv) throws SaslException {
        ClassLoader cl = prv.getClass().getClassLoader();
        if (cl == null) {
            cl = ClassLoader.getSystemClassLoader();
        }
        try {
            return Class.forName(factoryName, true, cl).newInstance();
        } catch (IllegalAccessException e) {
            throw new SaslException(String.valueOf("auth.31") + factoryName, e);
        } catch (ClassNotFoundException e2) {
            throw new SaslException(String.valueOf("auth.31") + factoryName, e2);
        } catch (InstantiationException e3) {
            throw new SaslException(String.valueOf("auth.31") + factoryName, e3);
        }
    }

    private static Collection<?> findFactories(String service) {
        HashSet<Object> fact = new HashSet<>();
        Provider[] pp = Security.getProviders();
        if (!(pp == null || pp.length == 0)) {
            HashSet<String> props = new HashSet<>();
            for (int i = 0; i < pp.length; i++) {
                String prName = pp[i].getName();
                Enumeration<Object> keys = pp[i].keys();
                while (keys.hasMoreElements()) {
                    String s = (String) keys.nextElement();
                    if (s.startsWith(service)) {
                        String prop = pp[i].getProperty(s);
                        try {
                            if (props.add(prName.concat(prop))) {
                                fact.add(newInstance(prop, pp[i]));
                            }
                        } catch (SaslException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        return fact;
    }

    public static Enumeration<SaslClientFactory> getSaslClientFactories() {
        return Collections.enumeration(findFactories(CLIENTFACTORYSRV));
    }

    public static Enumeration<SaslServerFactory> getSaslServerFactories() {
        return Collections.enumeration(findFactories(SERVERFACTORYSRV));
    }

    public static SaslServer createSaslServer(String mechanism, String protocol, String serverName, Map<String, ?> prop, CallbackHandler cbh) throws SaslException {
        SaslServer saslS;
        if (mechanism == null) {
            throw new NullPointerException("auth.32");
        }
        Collection<?> res = findFactories(SERVERFACTORYSRV);
        if (res.isEmpty()) {
            return null;
        }
        Iterator<?> iter = res.iterator();
        while (iter.hasNext()) {
            SaslServerFactory fact = (SaslServerFactory) iter.next();
            String[] mech = fact.getMechanismNames(null);
            boolean is = false;
            if (mech != null) {
                int j = 0;
                while (true) {
                    if (j >= mech.length) {
                        break;
                    } else if (mech[j].equals(mechanism)) {
                        is = true;
                        break;
                    } else {
                        j++;
                    }
                }
            }
            if (is && (saslS = fact.createSaslServer(mechanism, protocol, serverName, prop, cbh)) != null) {
                return saslS;
            }
        }
        return null;
    }

    public static SaslClient createSaslClient(String[] mechanisms, String authanticationID, String protocol, String serverName, Map<String, ?> prop, CallbackHandler cbh) throws SaslException {
        SaslClient saslC;
        if (mechanisms == null) {
            throw new NullPointerException("auth.33");
        }
        Collection<?> res = findFactories(CLIENTFACTORYSRV);
        if (res.isEmpty()) {
            return null;
        }
        Iterator<?> iter = res.iterator();
        while (iter.hasNext()) {
            SaslClientFactory fact = (SaslClientFactory) iter.next();
            String[] mech = fact.getMechanismNames(null);
            boolean is = false;
            if (mech != null) {
                for (String equals : mech) {
                    int n = 0;
                    while (true) {
                        if (n >= mechanisms.length) {
                            break;
                        } else if (equals.equals(mechanisms[n])) {
                            is = true;
                            break;
                        } else {
                            n++;
                        }
                    }
                }
            }
            if (is && (saslC = fact.createSaslClient(mechanisms, authanticationID, protocol, serverName, prop, cbh)) != null) {
                return saslC;
            }
        }
        return null;
    }
}
