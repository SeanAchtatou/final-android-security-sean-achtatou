package org.apache.harmony.javax.security.auth.callback;

import java.io.Serializable;
import java.util.Arrays;

public class PasswordCallback implements Callback, Serializable {
    private static final long serialVersionUID = 2267422647454909926L;
    boolean echoOn;
    private char[] inputPassword;
    private String prompt;

    private void setPrompt(String prompt2) throws IllegalArgumentException {
        if (prompt2 == null || prompt2.length() == 0) {
            throw new IllegalArgumentException("auth.14");
        }
        this.prompt = prompt2;
    }

    public PasswordCallback(String prompt2, boolean echoOn2) {
        setPrompt(prompt2);
        this.echoOn = echoOn2;
    }

    public String getPrompt() {
        return this.prompt;
    }

    public boolean isEchoOn() {
        return this.echoOn;
    }

    public void setPassword(char[] password) {
        if (password == null) {
            this.inputPassword = password;
            return;
        }
        this.inputPassword = new char[password.length];
        System.arraycopy(password, 0, this.inputPassword, 0, this.inputPassword.length);
    }

    public char[] getPassword() {
        if (this.inputPassword == null) {
            return null;
        }
        char[] tmp = new char[this.inputPassword.length];
        System.arraycopy(this.inputPassword, 0, tmp, 0, tmp.length);
        return tmp;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Arrays.fill(char[], char):void}
     arg types: [char[], int]
     candidates:
      ClspMth{java.util.Arrays.fill(double[], double):void}
      ClspMth{java.util.Arrays.fill(byte[], byte):void}
      ClspMth{java.util.Arrays.fill(long[], long):void}
      ClspMth{java.util.Arrays.fill(boolean[], boolean):void}
      ClspMth{java.util.Arrays.fill(short[], short):void}
      ClspMth{java.util.Arrays.fill(java.lang.Object[], java.lang.Object):void}
      ClspMth{java.util.Arrays.fill(int[], int):void}
      ClspMth{java.util.Arrays.fill(float[], float):void}
      ClspMth{java.util.Arrays.fill(char[], char):void} */
    public void clearPassword() {
        if (this.inputPassword != null) {
            Arrays.fill(this.inputPassword, 0);
        }
    }
}
