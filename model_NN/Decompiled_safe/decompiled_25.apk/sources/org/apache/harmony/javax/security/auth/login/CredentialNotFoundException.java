package org.apache.harmony.javax.security.auth.login;

public class CredentialNotFoundException extends CredentialException {
    private static final long serialVersionUID = -7779934467214319475L;

    public CredentialNotFoundException() {
    }

    public CredentialNotFoundException(String message) {
        super(message);
    }
}
