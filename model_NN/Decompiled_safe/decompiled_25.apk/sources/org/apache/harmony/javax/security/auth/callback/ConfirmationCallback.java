package org.apache.harmony.javax.security.auth.callback;

import java.io.Serializable;

public class ConfirmationCallback implements Callback, Serializable {
    public static final int CANCEL = 2;
    public static final int ERROR = 2;
    public static final int INFORMATION = 0;
    public static final int NO = 1;
    public static final int OK = 3;
    public static final int OK_CANCEL_OPTION = 2;
    public static final int UNSPECIFIED_OPTION = -1;
    public static final int WARNING = 1;
    public static final int YES = 0;
    public static final int YES_NO_CANCEL_OPTION = 1;
    public static final int YES_NO_OPTION = 0;
    private static final long serialVersionUID = -9095656433782481624L;
    private int defaultOption;
    private int messageType;
    private int optionType = -1;
    private String[] options;
    private String prompt;
    private int selection;

    public ConfirmationCallback(int messageType2, int optionType2, int defaultOption2) {
        if (messageType2 > 2 || messageType2 < 0) {
            throw new IllegalArgumentException("auth.16");
        }
        switch (optionType2) {
            case 0:
                if (!(defaultOption2 == 0 || defaultOption2 == 1)) {
                    throw new IllegalArgumentException("auth.17");
                }
            case 1:
                if (!(defaultOption2 == 0 || defaultOption2 == 1 || defaultOption2 == 2)) {
                    throw new IllegalArgumentException("auth.17");
                }
            case 2:
                if (!(defaultOption2 == 3 || defaultOption2 == 2)) {
                    throw new IllegalArgumentException("auth.17");
                }
            default:
                throw new IllegalArgumentException("auth.18");
        }
        this.messageType = messageType2;
        this.optionType = optionType2;
        this.defaultOption = defaultOption2;
    }

    public ConfirmationCallback(int messageType2, String[] options2, int defaultOption2) {
        if (messageType2 > 2 || messageType2 < 0) {
            throw new IllegalArgumentException("auth.16");
        } else if (options2 == null || options2.length == 0) {
            throw new IllegalArgumentException("auth.1A");
        } else {
            for (int i = 0; i < options2.length; i++) {
                if (options2[i] == null || options2[i].length() == 0) {
                    throw new IllegalArgumentException("auth.1A");
                }
            }
            if (defaultOption2 < 0 || defaultOption2 >= options2.length) {
                throw new IllegalArgumentException("auth.17");
            }
            this.options = options2;
            this.defaultOption = defaultOption2;
            this.messageType = messageType2;
        }
    }

    public ConfirmationCallback(String prompt2, int messageType2, int optionType2, int defaultOption2) {
        if (prompt2 == null || prompt2.length() == 0) {
            throw new IllegalArgumentException("auth.14");
        } else if (messageType2 > 2 || messageType2 < 0) {
            throw new IllegalArgumentException("auth.16");
        } else {
            switch (optionType2) {
                case 0:
                    if (!(defaultOption2 == 0 || defaultOption2 == 1)) {
                        throw new IllegalArgumentException("auth.17");
                    }
                case 1:
                    if (!(defaultOption2 == 0 || defaultOption2 == 1 || defaultOption2 == 2)) {
                        throw new IllegalArgumentException("auth.17");
                    }
                case 2:
                    if (!(defaultOption2 == 3 || defaultOption2 == 2)) {
                        throw new IllegalArgumentException("auth.17");
                    }
                default:
                    throw new IllegalArgumentException("auth.18");
            }
            this.prompt = prompt2;
            this.messageType = messageType2;
            this.optionType = optionType2;
            this.defaultOption = defaultOption2;
        }
    }

    public ConfirmationCallback(String prompt2, int messageType2, String[] options2, int defaultOption2) {
        if (prompt2 == null || prompt2.length() == 0) {
            throw new IllegalArgumentException("auth.14");
        } else if (messageType2 > 2 || messageType2 < 0) {
            throw new IllegalArgumentException("auth.16");
        } else if (options2 == null || options2.length == 0) {
            throw new IllegalArgumentException("auth.1A");
        } else {
            for (int i = 0; i < options2.length; i++) {
                if (options2[i] == null || options2[i].length() == 0) {
                    throw new IllegalArgumentException("auth.1A");
                }
            }
            if (defaultOption2 < 0 || defaultOption2 >= options2.length) {
                throw new IllegalArgumentException("auth.17");
            }
            this.options = options2;
            this.defaultOption = defaultOption2;
            this.messageType = messageType2;
            this.prompt = prompt2;
        }
    }

    public String getPrompt() {
        return this.prompt;
    }

    public int getMessageType() {
        return this.messageType;
    }

    public int getDefaultOption() {
        return this.defaultOption;
    }

    public String[] getOptions() {
        return this.options;
    }

    public int getOptionType() {
        return this.optionType;
    }

    public int getSelectedIndex() {
        return this.selection;
    }

    public void setSelectedIndex(int selection2) {
        if (this.options == null) {
            switch (this.optionType) {
                case 0:
                    if (!(selection2 == 0 || selection2 == 1)) {
                        throw new IllegalArgumentException("auth.19");
                    }
                case 1:
                    if (!(selection2 == 0 || selection2 == 1 || selection2 == 2)) {
                        throw new IllegalArgumentException("auth.19");
                    }
                case 2:
                    if (!(selection2 == 3 || selection2 == 2)) {
                        throw new IllegalArgumentException("auth.19");
                    }
            }
            this.selection = selection2;
        } else if (selection2 < 0 || selection2 > this.options.length) {
            throw new ArrayIndexOutOfBoundsException("auth.1B");
        } else {
            this.selection = selection2;
        }
    }
}
