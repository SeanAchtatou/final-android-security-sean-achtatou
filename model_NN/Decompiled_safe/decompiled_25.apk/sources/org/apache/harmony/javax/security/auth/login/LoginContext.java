package org.apache.harmony.javax.security.auth.login;

import java.io.IOException;
import java.security.AccessControlContext;
import java.security.AccessController;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.security.Security;
import java.util.HashMap;
import java.util.Map;
import org.apache.harmony.javax.security.auth.AuthPermission;
import org.apache.harmony.javax.security.auth.Subject;
import org.apache.harmony.javax.security.auth.callback.Callback;
import org.apache.harmony.javax.security.auth.callback.CallbackHandler;
import org.apache.harmony.javax.security.auth.callback.UnsupportedCallbackException;
import org.apache.harmony.javax.security.auth.login.AppConfigurationEntry;
import org.apache.harmony.javax.security.auth.spi.LoginModule;

public class LoginContext {
    private static final String DEFAULT_CALLBACK_HANDLER_PROPERTY = "auth.login.defaultCallbackHandler";
    private static final int OPTIONAL = 0;
    private static final int REQUIRED = 1;
    private static final int REQUISITE = 2;
    private static final int SUFFICIENT = 3;
    /* access modifiers changed from: private */
    public CallbackHandler callbackHandler;
    /* access modifiers changed from: private */
    public ClassLoader contextClassLoader;
    private boolean loggedIn;
    private Module[] modules;
    private Map<String, ?> sharedState;
    private Subject subject;
    /* access modifiers changed from: private */
    public AccessControlContext userContext;
    private boolean userProvidedConfig;
    private boolean userProvidedSubject;

    public LoginContext(String name) throws LoginException {
        init(name, null, null, null);
    }

    public LoginContext(String name, CallbackHandler cbHandler) throws LoginException {
        if (cbHandler == null) {
            throw new LoginException("auth.34");
        }
        init(name, null, cbHandler, null);
    }

    public LoginContext(String name, Subject subject2) throws LoginException {
        if (subject2 == null) {
            throw new LoginException("auth.03");
        }
        init(name, subject2, null, null);
    }

    public LoginContext(String name, Subject subject2, CallbackHandler cbHandler) throws LoginException {
        if (subject2 == null) {
            throw new LoginException("auth.03");
        } else if (cbHandler == null) {
            throw new LoginException("auth.34");
        } else {
            init(name, subject2, cbHandler, null);
        }
    }

    public LoginContext(String name, Subject subject2, CallbackHandler cbHandler, Configuration config) throws LoginException {
        init(name, subject2, cbHandler, config);
    }

    private void init(String name, Subject subject2, final CallbackHandler cbHandler, Configuration config) throws LoginException {
        this.subject = subject2;
        this.userProvidedSubject = subject2 != null;
        if (name == null) {
            throw new LoginException("auth.00");
        }
        if (config == null) {
            config = Configuration.getAccessibleConfiguration();
        } else {
            this.userProvidedConfig = true;
        }
        SecurityManager sm = System.getSecurityManager();
        if (sm != null && !this.userProvidedConfig) {
            sm.checkPermission(new AuthPermission("createLoginContext." + name));
        }
        AppConfigurationEntry[] entries = config.getAppConfigurationEntry(name);
        if (entries == null) {
            if (sm != null && !this.userProvidedConfig) {
                sm.checkPermission(new AuthPermission("createLoginContext.other"));
            }
            entries = config.getAppConfigurationEntry("other");
            if (entries == null) {
                throw new LoginException("auth.35 " + name);
            }
        }
        this.modules = new Module[entries.length];
        for (int i = 0; i < this.modules.length; i++) {
            this.modules[i] = new Module(entries[i]);
        }
        try {
            AccessController.doPrivileged(new PrivilegedExceptionAction<Void>() {
                public Void run() throws Exception {
                    LoginContext.this.contextClassLoader = Thread.currentThread().getContextClassLoader();
                    if (LoginContext.this.contextClassLoader == null) {
                        LoginContext.this.contextClassLoader = ClassLoader.getSystemClassLoader();
                    }
                    if (cbHandler == null) {
                        String klassName = Security.getProperty(LoginContext.DEFAULT_CALLBACK_HANDLER_PROPERTY);
                        if (!(klassName == null || klassName.length() == 0)) {
                            LoginContext.this.callbackHandler = (CallbackHandler) Class.forName(klassName, true, LoginContext.this.contextClassLoader).newInstance();
                        }
                    } else {
                        LoginContext.this.callbackHandler = cbHandler;
                    }
                    return null;
                }
            });
            if (this.userProvidedConfig) {
                this.userContext = AccessController.getContext();
            } else if (this.callbackHandler != null) {
                this.userContext = AccessController.getContext();
                this.callbackHandler = new ContextedCallbackHandler(this.callbackHandler);
            }
        } catch (PrivilegedActionException ex) {
            throw ((LoginException) new LoginException("auth.36").initCause(ex.getCause()));
        }
    }

    public Subject getSubject() {
        if (this.userProvidedSubject || this.loggedIn) {
            return this.subject;
        }
        return null;
    }

    public void login() throws LoginException {
        PrivilegedExceptionAction<Void> action = new PrivilegedExceptionAction<Void>() {
            public Void run() throws LoginException {
                LoginContext.this.loginImpl();
                return null;
            }
        };
        try {
            if (this.userProvidedConfig) {
                AccessController.doPrivileged(action, this.userContext);
            } else {
                AccessController.doPrivileged(action);
            }
        } catch (PrivilegedActionException ex) {
            throw ((LoginException) ex.getException());
        }
    }

    /* access modifiers changed from: private */
    public void loginImpl() throws LoginException {
        if (this.subject == null) {
            this.subject = new Subject();
        }
        if (this.sharedState == null) {
            this.sharedState = new HashMap();
        }
        Throwable firstProblem = null;
        int[] logged = new int[4];
        int[] total = new int[4];
        Module[] moduleArr = this.modules;
        int length = moduleArr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                break;
            }
            Module module = moduleArr[i];
            try {
                module.create(this.subject, this.callbackHandler, this.sharedState);
                if (module.module.login()) {
                    int flag = module.getFlag();
                    total[flag] = total[flag] + 1;
                    int flag2 = module.getFlag();
                    logged[flag2] = logged[flag2] + 1;
                    if (module.getFlag() == 3) {
                        break;
                    }
                } else {
                    continue;
                }
            } catch (Throwable ex) {
                if (firstProblem == null) {
                    firstProblem = ex;
                }
                if (module.klass == null) {
                    total[1] = total[1] + 1;
                    break;
                }
                int flag3 = module.getFlag();
                total[flag3] = total[flag3] + 1;
                if (module.getFlag() == 2) {
                    break;
                }
            }
            i++;
        }
        boolean fail = true;
        if (logged[1] == total[1] && logged[2] == total[2]) {
            if (total[1] != 0 || total[2] != 0) {
                fail = false;
            } else if (!(logged[0] == 0 && logged[3] == 0)) {
                fail = false;
            }
        }
        int[] commited = new int[4];
        total[3] = 0;
        total[2] = 0;
        total[1] = 0;
        total[0] = 0;
        if (!fail) {
            for (Module module2 : this.modules) {
                if (module2.klass != null) {
                    int flag4 = module2.getFlag();
                    total[flag4] = total[flag4] + 1;
                    try {
                        module2.module.commit();
                        int flag5 = module2.getFlag();
                        commited[flag5] = commited[flag5] + 1;
                    } catch (Throwable ex2) {
                        if (firstProblem == null) {
                            firstProblem = ex2;
                        }
                    }
                }
            }
        }
        boolean fail2 = true;
        if (commited[1] == total[1] && commited[2] == total[2]) {
            if (total[1] != 0 || total[2] != 0) {
                fail2 = false;
            } else if (!(commited[0] == 0 && commited[3] == 0)) {
                fail2 = false;
            }
        }
        if (fail2) {
            for (Module module3 : this.modules) {
                try {
                    module3.module.abort();
                } catch (Throwable ex3) {
                    if (firstProblem == null) {
                        firstProblem = ex3;
                    }
                }
            }
            if ((firstProblem instanceof PrivilegedActionException) && firstProblem.getCause() != null) {
                firstProblem = firstProblem.getCause();
            }
            if (firstProblem instanceof LoginException) {
                throw ((LoginException) firstProblem);
            }
            throw ((LoginException) new LoginException("auth.37").initCause(firstProblem));
        }
        this.loggedIn = true;
    }

    public void logout() throws LoginException {
        PrivilegedExceptionAction<Void> action = new PrivilegedExceptionAction<Void>() {
            public Void run() throws LoginException {
                LoginContext.this.logoutImpl();
                return null;
            }
        };
        try {
            if (this.userProvidedConfig) {
                AccessController.doPrivileged(action, this.userContext);
            } else {
                AccessController.doPrivileged(action);
            }
        } catch (PrivilegedActionException ex) {
            throw ((LoginException) ex.getException());
        }
    }

    /* access modifiers changed from: private */
    public void logoutImpl() throws LoginException {
        if (this.subject == null) {
            throw new LoginException("auth.38");
        }
        this.loggedIn = false;
        Throwable firstProblem = null;
        int total = 0;
        for (Module module : this.modules) {
            try {
                module.module.logout();
                total++;
            } catch (Throwable ex) {
                if (firstProblem == null) {
                    firstProblem = ex;
                }
            }
        }
        if (firstProblem != null || total == 0) {
            if ((firstProblem instanceof PrivilegedActionException) && firstProblem.getCause() != null) {
                firstProblem = firstProblem.getCause();
            }
            if (firstProblem instanceof LoginException) {
                throw ((LoginException) firstProblem);
            }
            throw ((LoginException) new LoginException("auth.37").initCause(firstProblem));
        }
    }

    private class ContextedCallbackHandler implements CallbackHandler {
        /* access modifiers changed from: private */
        public final CallbackHandler hiddenHandlerRef;

        ContextedCallbackHandler(CallbackHandler handler) {
            this.hiddenHandlerRef = handler;
        }

        public void handle(final Callback[] callbacks) throws IOException, UnsupportedCallbackException {
            try {
                AccessController.doPrivileged(new PrivilegedExceptionAction<Void>() {
                    public Void run() throws IOException, UnsupportedCallbackException {
                        ContextedCallbackHandler.this.hiddenHandlerRef.handle(callbacks);
                        return null;
                    }
                }, LoginContext.this.userContext);
            } catch (PrivilegedActionException ex) {
                if (ex.getCause() instanceof UnsupportedCallbackException) {
                    throw ((UnsupportedCallbackException) ex.getCause());
                }
                throw ((IOException) ex.getCause());
            }
        }
    }

    private final class Module {
        AppConfigurationEntry entry;
        int flag;
        Class<?> klass;
        LoginModule module;

        Module(AppConfigurationEntry entry2) {
            this.entry = entry2;
            AppConfigurationEntry.LoginModuleControlFlag flg = entry2.getControlFlag();
            if (flg == AppConfigurationEntry.LoginModuleControlFlag.OPTIONAL) {
                this.flag = 0;
            } else if (flg == AppConfigurationEntry.LoginModuleControlFlag.REQUISITE) {
                this.flag = 2;
            } else if (flg == AppConfigurationEntry.LoginModuleControlFlag.SUFFICIENT) {
                this.flag = 3;
            } else {
                this.flag = 1;
            }
        }

        /* access modifiers changed from: package-private */
        public int getFlag() {
            return this.flag;
        }

        /* access modifiers changed from: package-private */
        public void create(Subject subject, CallbackHandler callbackHandler, Map<String, ?> sharedState) throws LoginException {
            String klassName = this.entry.getLoginModuleName();
            if (this.klass == null) {
                try {
                    this.klass = Class.forName(klassName, false, LoginContext.this.contextClassLoader);
                } catch (ClassNotFoundException ex) {
                    throw ((LoginException) new LoginException("auth.39 " + klassName).initCause(ex));
                }
            }
            if (this.module == null) {
                try {
                    this.module = (LoginModule) this.klass.newInstance();
                    this.module.initialize(subject, callbackHandler, sharedState, this.entry.getOptions());
                } catch (IllegalAccessException ex2) {
                    throw ((LoginException) new LoginException("auth.3A " + klassName).initCause(ex2));
                } catch (InstantiationException ex3) {
                    throw ((LoginException) new LoginException("auth.3A" + klassName).initCause(ex3));
                }
            }
        }
    }
}
