package org.apache.harmony.javax.security.auth;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.security.Permission;
import java.security.PermissionCollection;
import java.security.Principal;
import java.util.Set;

public final class PrivateCredentialPermission extends Permission {
    private static final String READ = "read";
    private static final long serialVersionUID = 5284372143517237068L;
    private String credentialClass;
    private transient int offset;
    private transient CredOwner[] set;

    public PrivateCredentialPermission(String name, String action) {
        super(name);
        if (READ.equalsIgnoreCase(action)) {
            initTargetName(name);
            return;
        }
        throw new IllegalArgumentException("auth.11");
    }

    PrivateCredentialPermission(String credentialClass2, Set<Principal> principals) {
        super(credentialClass2);
        this.credentialClass = credentialClass2;
        this.set = new CredOwner[principals.size()];
        for (Principal p : principals) {
            CredOwner element = new CredOwner(p.getClass().getName(), p.getName());
            boolean found = false;
            int ii = 0;
            while (true) {
                if (ii >= this.offset) {
                    break;
                } else if (this.set[ii].equals(element)) {
                    found = true;
                    break;
                } else {
                    ii++;
                }
            }
            if (!found) {
                CredOwner[] credOwnerArr = this.set;
                int i = this.offset;
                this.offset = i + 1;
                credOwnerArr[i] = element;
            }
        }
    }

    private void initTargetName(String name) {
        if (name == null) {
            throw new NullPointerException("auth.0E");
        }
        String name2 = name.trim();
        if (name2.length() == 0) {
            throw new IllegalArgumentException("auth.0F");
        }
        int beg = name2.indexOf(32);
        if (beg == -1) {
            throw new IllegalArgumentException("auth.10");
        }
        this.credentialClass = name2.substring(0, beg);
        int beg2 = beg + 1;
        int count = 0;
        int nameLength = name2.length();
        while (beg2 < nameLength) {
            int i = name2.indexOf(32, beg2);
            int j = name2.indexOf(34, i + 2);
            if (i == -1 || j == -1 || name2.charAt(i + 1) != '\"') {
                throw new IllegalArgumentException("auth.10");
            }
            beg2 = j + 2;
            count++;
        }
        if (count < 1) {
            throw new IllegalArgumentException("auth.10");
        }
        int beg3 = name2.indexOf(32) + 1;
        this.set = new CredOwner[count];
        for (int index = 0; index < count; index++) {
            int i2 = name2.indexOf(32, beg3);
            int j2 = name2.indexOf(34, i2 + 2);
            CredOwner element = new CredOwner(name2.substring(beg3, i2), name2.substring(i2 + 2, j2));
            boolean found = false;
            int ii = 0;
            while (true) {
                if (ii >= this.offset) {
                    break;
                } else if (this.set[ii].equals(element)) {
                    found = true;
                    break;
                } else {
                    ii++;
                }
            }
            if (!found) {
                CredOwner[] credOwnerArr = this.set;
                int i3 = this.offset;
                this.offset = i3 + 1;
                credOwnerArr[i3] = element;
            }
            beg3 = j2 + 2;
        }
    }

    private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        ois.defaultReadObject();
        initTargetName(getName());
    }

    public String[][] getPrincipals() {
        String[][] s = (String[][]) Array.newInstance(String.class, this.offset, 2);
        for (int i = 0; i < s.length; i++) {
            s[i][0] = this.set[i].principalClass;
            s[i][1] = this.set[i].principalName;
        }
        return s;
    }

    public String getActions() {
        return READ;
    }

    public String getCredentialClass() {
        return this.credentialClass;
    }

    public int hashCode() {
        int hash = 0;
        for (int i = 0; i < this.offset; i++) {
            hash += this.set[i].hashCode();
        }
        return getCredentialClass().hashCode() + hash;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        PrivateCredentialPermission that = (PrivateCredentialPermission) obj;
        if (!this.credentialClass.equals(that.credentialClass) || this.offset != that.offset || !sameMembers(this.set, that.set, this.offset)) {
            return false;
        }
        return true;
    }

    public boolean implies(Permission permission) {
        if (permission == null || getClass() != permission.getClass()) {
            return false;
        }
        PrivateCredentialPermission that = (PrivateCredentialPermission) permission;
        if (!"*".equals(this.credentialClass) && !this.credentialClass.equals(that.getCredentialClass())) {
            return false;
        }
        if (that.offset == 0) {
            return true;
        }
        CredOwner[] thisCo = this.set;
        CredOwner[] thatCo = that.set;
        int thisPrincipalsSize = this.offset;
        int thatPrincipalsSize = that.offset;
        int i = 0;
        while (i < thisPrincipalsSize) {
            int j = 0;
            while (j < thatPrincipalsSize && !thisCo[i].implies(thatCo[j])) {
                j++;
            }
            if (j == thatCo.length) {
                return false;
            }
            i++;
        }
        return true;
    }

    public PermissionCollection newPermissionCollection() {
        return null;
    }

    private boolean sameMembers(Object[] ar1, Object[] ar2, int length) {
        if (ar1 == null && ar2 == null) {
            return true;
        }
        if (ar1 == null || ar2 == null) {
            return false;
        }
        for (int i = 0; i < length; i++) {
            boolean found = false;
            int j = 0;
            while (true) {
                if (j >= length) {
                    break;
                } else if (ar1[i].equals(ar2[j])) {
                    found = true;
                    break;
                } else {
                    j++;
                }
            }
            if (!found) {
                return false;
            }
        }
        return true;
    }

    private static final class CredOwner implements Serializable {
        private static final long serialVersionUID = -5607449830436408266L;
        private transient boolean isClassWildcard;
        private transient boolean isPNameWildcard;
        String principalClass;
        String principalName;

        CredOwner(String principalClass2, String principalName2) {
            if ("*".equals(principalClass2)) {
                this.isClassWildcard = true;
            }
            if ("*".equals(principalName2)) {
                this.isPNameWildcard = true;
            }
            if (!this.isClassWildcard || this.isPNameWildcard) {
                this.principalClass = principalClass2;
                this.principalName = principalName2;
                return;
            }
            throw new IllegalArgumentException("auth.12");
        }

        /* access modifiers changed from: package-private */
        public boolean implies(Object obj) {
            if (obj == this) {
                return true;
            }
            CredOwner co = (CredOwner) obj;
            if ((this.isClassWildcard || this.principalClass.equals(co.principalClass)) && (this.isPNameWildcard || this.principalName.equals(co.principalName))) {
                return true;
            }
            return false;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof CredOwner)) {
                return false;
            }
            CredOwner that = (CredOwner) obj;
            if (!this.principalClass.equals(that.principalClass) || !this.principalName.equals(that.principalName)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return this.principalClass.hashCode() + this.principalName.hashCode();
        }
    }
}
