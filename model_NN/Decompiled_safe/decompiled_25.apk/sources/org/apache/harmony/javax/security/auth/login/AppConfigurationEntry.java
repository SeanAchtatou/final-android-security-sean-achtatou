package org.apache.harmony.javax.security.auth.login;

import java.util.Collections;
import java.util.Map;

public class AppConfigurationEntry {
    private final LoginModuleControlFlag controlFlag;
    private final String loginModuleName;
    private final Map<String, ?> options;

    public AppConfigurationEntry(String loginModuleName2, LoginModuleControlFlag controlFlag2, Map<String, ?> options2) {
        if (loginModuleName2 == null || loginModuleName2.length() == 0) {
            throw new IllegalArgumentException("auth.26");
        } else if (controlFlag2 == null) {
            throw new IllegalArgumentException("auth.27");
        } else if (options2 == null) {
            throw new IllegalArgumentException("auth.1A");
        } else {
            this.loginModuleName = loginModuleName2;
            this.controlFlag = controlFlag2;
            this.options = Collections.unmodifiableMap(options2);
        }
    }

    public String getLoginModuleName() {
        return this.loginModuleName;
    }

    public LoginModuleControlFlag getControlFlag() {
        return this.controlFlag;
    }

    public Map<String, ?> getOptions() {
        return this.options;
    }

    public static class LoginModuleControlFlag {
        public static final LoginModuleControlFlag OPTIONAL = new LoginModuleControlFlag("LoginModuleControlFlag: optional");
        public static final LoginModuleControlFlag REQUIRED = new LoginModuleControlFlag("LoginModuleControlFlag: required");
        public static final LoginModuleControlFlag REQUISITE = new LoginModuleControlFlag("LoginModuleControlFlag: requisite");
        public static final LoginModuleControlFlag SUFFICIENT = new LoginModuleControlFlag("LoginModuleControlFlag: sufficient");
        private final String flag;

        private LoginModuleControlFlag(String flag2) {
            this.flag = flag2;
        }

        public String toString() {
            return this.flag;
        }
    }
}
