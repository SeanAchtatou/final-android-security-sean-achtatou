package org.jivesoftware.smack;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;
import org.xmlpull.v1.XmlPullParser;

public final class SmackConfiguration {
    private static final String SMACK_VERSION = "3.1.0";
    private static Vector<String> defaultMechs = new Vector<>();
    private static int keepAliveInterval;
    private static int packetReplyTimeout;

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    static {
        /*
            r12 = 1
            r8 = 5000(0x1388, float:7.006E-42)
            org.jivesoftware.smack.SmackConfiguration.packetReplyTimeout = r8
            r8 = 30000(0x7530, float:4.2039E-41)
            org.jivesoftware.smack.SmackConfiguration.keepAliveInterval = r8
            java.util.Vector r8 = new java.util.Vector
            r8.<init>()
            org.jivesoftware.smack.SmackConfiguration.defaultMechs = r8
            java.lang.ClassLoader[] r1 = getClassLoaders()     // Catch:{ Exception -> 0x00a3 }
            int r9 = r1.length     // Catch:{ Exception -> 0x00a3 }
            r8 = 0
        L_0x0016:
            if (r8 < r9) goto L_0x0019
        L_0x0018:
            return
        L_0x0019:
            r0 = r1[r8]     // Catch:{ Exception -> 0x00a3 }
            java.lang.String r10 = "META-INF/smack-config.xml"
            java.util.Enumeration r2 = r0.getResources(r10)     // Catch:{ Exception -> 0x00a3 }
        L_0x0021:
            boolean r10 = r2.hasMoreElements()     // Catch:{ Exception -> 0x00a3 }
            if (r10 != 0) goto L_0x002a
            int r8 = r8 + 1
            goto L_0x0016
        L_0x002a:
            java.lang.Object r7 = r2.nextElement()     // Catch:{ Exception -> 0x00a3 }
            java.net.URL r7 = (java.net.URL) r7     // Catch:{ Exception -> 0x00a3 }
            r6 = 0
            java.io.InputStream r6 = r7.openStream()     // Catch:{ Exception -> 0x007f }
            org.xmlpull.v1.XmlPullParserFactory r10 = org.xmlpull.v1.XmlPullParserFactory.newInstance()     // Catch:{ Exception -> 0x007f }
            org.xmlpull.v1.XmlPullParser r5 = r10.newPullParser()     // Catch:{ Exception -> 0x007f }
            java.lang.String r10 = "http://xmlpull.org/v1/doc/features.html#process-namespaces"
            r11 = 1
            r5.setFeature(r10, r11)     // Catch:{ Exception -> 0x007f }
            java.lang.String r10 = "UTF-8"
            r5.setInput(r6, r10)     // Catch:{ Exception -> 0x007f }
            int r4 = r5.getEventType()     // Catch:{ Exception -> 0x007f }
        L_0x004c:
            r10 = 2
            if (r4 != r10) goto L_0x005e
            java.lang.String r10 = r5.getName()     // Catch:{ Exception -> 0x007f }
            java.lang.String r11 = "className"
            boolean r10 = r10.equals(r11)     // Catch:{ Exception -> 0x007f }
            if (r10 == 0) goto L_0x006a
            parseClassToLoad(r5)     // Catch:{ Exception -> 0x007f }
        L_0x005e:
            int r4 = r5.next()     // Catch:{ Exception -> 0x007f }
            if (r4 != r12) goto L_0x004c
            r6.close()     // Catch:{ Exception -> 0x0068 }
            goto L_0x0021
        L_0x0068:
            r10 = move-exception
            goto L_0x0021
        L_0x006a:
            java.lang.String r10 = r5.getName()     // Catch:{ Exception -> 0x007f }
            java.lang.String r11 = "packetReplyTimeout"
            boolean r10 = r10.equals(r11)     // Catch:{ Exception -> 0x007f }
            if (r10 == 0) goto L_0x0089
            int r10 = org.jivesoftware.smack.SmackConfiguration.packetReplyTimeout     // Catch:{ Exception -> 0x007f }
            int r10 = parseIntProperty(r5, r10)     // Catch:{ Exception -> 0x007f }
            org.jivesoftware.smack.SmackConfiguration.packetReplyTimeout = r10     // Catch:{ Exception -> 0x007f }
            goto L_0x005e
        L_0x007f:
            r3 = move-exception
            r3.printStackTrace()     // Catch:{ all -> 0x009e }
            r6.close()     // Catch:{ Exception -> 0x0087 }
            goto L_0x0021
        L_0x0087:
            r10 = move-exception
            goto L_0x0021
        L_0x0089:
            java.lang.String r10 = r5.getName()     // Catch:{ Exception -> 0x007f }
            java.lang.String r11 = "keepAliveInterval"
            boolean r10 = r10.equals(r11)     // Catch:{ Exception -> 0x007f }
            if (r10 == 0) goto L_0x00a9
            int r10 = org.jivesoftware.smack.SmackConfiguration.keepAliveInterval     // Catch:{ Exception -> 0x007f }
            int r10 = parseIntProperty(r5, r10)     // Catch:{ Exception -> 0x007f }
            org.jivesoftware.smack.SmackConfiguration.keepAliveInterval = r10     // Catch:{ Exception -> 0x007f }
            goto L_0x005e
        L_0x009e:
            r8 = move-exception
            r6.close()     // Catch:{ Exception -> 0x00bf }
        L_0x00a2:
            throw r8     // Catch:{ Exception -> 0x00a3 }
        L_0x00a3:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x0018
        L_0x00a9:
            java.lang.String r10 = r5.getName()     // Catch:{ Exception -> 0x007f }
            java.lang.String r11 = "mechName"
            boolean r10 = r10.equals(r11)     // Catch:{ Exception -> 0x007f }
            if (r10 == 0) goto L_0x005e
            java.util.Vector<java.lang.String> r10 = org.jivesoftware.smack.SmackConfiguration.defaultMechs     // Catch:{ Exception -> 0x007f }
            java.lang.String r11 = r5.nextText()     // Catch:{ Exception -> 0x007f }
            r10.add(r11)     // Catch:{ Exception -> 0x007f }
            goto L_0x005e
        L_0x00bf:
            r9 = move-exception
            goto L_0x00a2
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smack.SmackConfiguration.<clinit>():void");
    }

    private SmackConfiguration() {
    }

    public static String getVersion() {
        return SMACK_VERSION;
    }

    public static int getPacketReplyTimeout() {
        if (packetReplyTimeout <= 0) {
            packetReplyTimeout = 5000;
        }
        return packetReplyTimeout;
    }

    public static void setPacketReplyTimeout(int timeout) {
        if (timeout <= 0) {
            throw new IllegalArgumentException();
        }
        packetReplyTimeout = timeout;
    }

    public static int getKeepAliveInterval() {
        return keepAliveInterval;
    }

    public static void setKeepAliveInterval(int interval) {
        keepAliveInterval = interval;
    }

    public static void addSaslMech(String mech) {
        if (!defaultMechs.contains(mech)) {
            defaultMechs.add(mech);
        }
    }

    public static void addSaslMechs(Collection<String> mechs) {
        for (String mech : mechs) {
            addSaslMech(mech);
        }
    }

    public static void removeSaslMech(String mech) {
        if (defaultMechs.contains(mech)) {
            defaultMechs.remove(mech);
        }
    }

    public static void removeSaslMechs(Collection<String> mechs) {
        for (String mech : mechs) {
            removeSaslMech(mech);
        }
    }

    public static List<String> getSaslMechs() {
        return defaultMechs;
    }

    private static void parseClassToLoad(XmlPullParser parser) throws Exception {
        String className = parser.nextText();
        try {
            Class.forName(className);
        } catch (ClassNotFoundException e) {
            System.err.println("Error! A startup class specified in smack-config.xml could not be loaded: " + className);
        }
    }

    private static int parseIntProperty(XmlPullParser parser, int defaultValue) throws Exception {
        try {
            return Integer.parseInt(parser.nextText());
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
            return defaultValue;
        }
    }

    private static ClassLoader[] getClassLoaders() {
        ClassLoader[] classLoaders = {SmackConfiguration.class.getClassLoader(), Thread.currentThread().getContextClassLoader()};
        List<ClassLoader> loaders = new ArrayList<>();
        for (ClassLoader classLoader : classLoaders) {
            if (classLoader != null) {
                loaders.add(classLoader);
            }
        }
        return (ClassLoader[]) loaders.toArray(new ClassLoader[loaders.size()]);
    }
}
