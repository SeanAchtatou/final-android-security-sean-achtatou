package org.jivesoftware.smack;

import com.amap.mapapi.poisearch.PoiTypeDef;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.Principal;
import java.security.cert.CertificateException;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.net.ssl.X509TrustManager;

class ServerTrustManager implements X509TrustManager {
    private static Pattern cnPattern = Pattern.compile("(?i)(cn=)([^,]*)");
    private ConnectionConfiguration configuration;
    private String server;
    private KeyStore trustStore;

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0039 A[SYNTHETIC, Splitter:B:13:0x0039] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0042 A[SYNTHETIC, Splitter:B:18:0x0042] */
    /* JADX WARNING: Removed duplicated region for block: B:30:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ServerTrustManager(java.lang.String r6, org.jivesoftware.smack.ConnectionConfiguration r7) {
        /*
            r5 = this;
            r5.<init>()
            r5.configuration = r7
            r5.server = r6
            r1 = 0
            java.lang.String r3 = r7.getTruststoreType()     // Catch:{ Exception -> 0x002f }
            java.security.KeyStore r3 = java.security.KeyStore.getInstance(r3)     // Catch:{ Exception -> 0x002f }
            r5.trustStore = r3     // Catch:{ Exception -> 0x002f }
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Exception -> 0x002f }
            java.lang.String r3 = r7.getTruststorePath()     // Catch:{ Exception -> 0x002f }
            r2.<init>(r3)     // Catch:{ Exception -> 0x002f }
            java.security.KeyStore r3 = r5.trustStore     // Catch:{ Exception -> 0x004e, all -> 0x004b }
            java.lang.String r4 = r7.getTruststorePassword()     // Catch:{ Exception -> 0x004e, all -> 0x004b }
            char[] r4 = r4.toCharArray()     // Catch:{ Exception -> 0x004e, all -> 0x004b }
            r3.load(r2, r4)     // Catch:{ Exception -> 0x004e, all -> 0x004b }
            if (r2 == 0) goto L_0x0051
            r2.close()     // Catch:{ IOException -> 0x0046 }
            r1 = r2
        L_0x002e:
            return
        L_0x002f:
            r0 = move-exception
        L_0x0030:
            r0.printStackTrace()     // Catch:{ all -> 0x003f }
            r3 = 0
            r7.setVerifyRootCAEnabled(r3)     // Catch:{ all -> 0x003f }
            if (r1 == 0) goto L_0x002e
            r1.close()     // Catch:{ IOException -> 0x003d }
            goto L_0x002e
        L_0x003d:
            r3 = move-exception
            goto L_0x002e
        L_0x003f:
            r3 = move-exception
        L_0x0040:
            if (r1 == 0) goto L_0x0045
            r1.close()     // Catch:{ IOException -> 0x0049 }
        L_0x0045:
            throw r3
        L_0x0046:
            r3 = move-exception
            r1 = r2
            goto L_0x002e
        L_0x0049:
            r4 = move-exception
            goto L_0x0045
        L_0x004b:
            r3 = move-exception
            r1 = r2
            goto L_0x0040
        L_0x004e:
            r0 = move-exception
            r1 = r2
            goto L_0x0030
        L_0x0051:
            r1 = r2
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smack.ServerTrustManager.<init>(java.lang.String, org.jivesoftware.smack.ConnectionConfiguration):void");
    }

    public X509Certificate[] getAcceptedIssuers() {
        return new X509Certificate[0];
    }

    public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
    }

    public void checkServerTrusted(X509Certificate[] x509Certificates, String arg1) throws CertificateException {
        int nSize = x509Certificates.length;
        List<String> peerIdentities = getPeerIdentity(x509Certificates[0]);
        if (this.configuration.isVerifyChainEnabled()) {
            Principal principalLast = null;
            for (int i = nSize - 1; i >= 0; i--) {
                X509Certificate x509certificate = x509Certificates[i];
                Principal principalIssuer = x509certificate.getIssuerDN();
                Principal principalSubject = x509certificate.getSubjectDN();
                if (principalLast != null) {
                    if (principalIssuer.equals(principalLast)) {
                        try {
                            x509Certificates[i].verify(x509Certificates[i + 1].getPublicKey());
                        } catch (GeneralSecurityException e) {
                            throw new CertificateException("signature verification failed of " + peerIdentities);
                        }
                    } else {
                        throw new CertificateException("subject/issuer verification failed of " + peerIdentities);
                    }
                }
                principalLast = principalSubject;
            }
        }
        if (this.configuration.isVerifyRootCAEnabled()) {
            boolean trusted = false;
            try {
                trusted = this.trustStore.getCertificateAlias(x509Certificates[nSize + -1]) != null;
                if (!trusted && nSize == 1 && this.configuration.isSelfSignedCertificateEnabled()) {
                    System.out.println("Accepting self-signed certificate of remote server: " + peerIdentities);
                    trusted = true;
                }
            } catch (KeyStoreException e2) {
                e2.printStackTrace();
            }
            if (!trusted) {
                throw new CertificateException("root certificate not trusted of " + peerIdentities);
            }
        }
        if (this.configuration.isNotMatchingDomainCheckEnabled()) {
            if (peerIdentities.size() == 1 && peerIdentities.get(0).startsWith("*.")) {
                if (!this.server.endsWith(peerIdentities.get(0).replace("*.", PoiTypeDef.All))) {
                    throw new CertificateException("target verification failed of " + peerIdentities);
                }
            } else if (!peerIdentities.contains(this.server)) {
                throw new CertificateException("target verification failed of " + peerIdentities);
            }
        }
        if (this.configuration.isExpiredCertificatesCheckEnabled()) {
            Date date = new Date();
            int i2 = 0;
            while (i2 < nSize) {
                try {
                    x509Certificates[i2].checkValidity(date);
                    i2++;
                } catch (GeneralSecurityException e3) {
                    throw new CertificateException("invalid date of " + this.server);
                }
            }
        }
    }

    public static List<String> getPeerIdentity(X509Certificate x509Certificate) {
        List<String> names = getSubjectAlternativeNames(x509Certificate);
        if (!names.isEmpty()) {
            return names;
        }
        String name = x509Certificate.getSubjectDN().getName();
        Matcher matcher = cnPattern.matcher(name);
        if (matcher.find()) {
            name = matcher.group(2);
        }
        List<String> names2 = new ArrayList<>();
        names2.add(name);
        return names2;
    }

    private static List<String> getSubjectAlternativeNames(X509Certificate certificate) {
        List<String> identities = new ArrayList<>();
        try {
            if (certificate.getSubjectAlternativeNames() == null) {
                return Collections.emptyList();
            }
            return identities;
        } catch (CertificateParsingException e) {
            e.printStackTrace();
            return identities;
        }
    }
}
