package org.jivesoftware.smack.filter;

import org.jivesoftware.smack.packet.Packet;

public class PacketExtensionFilter implements PacketFilter {
    private String elementName;
    private String namespace;

    public PacketExtensionFilter(String elementName2, String namespace2) {
        this.elementName = elementName2;
        this.namespace = namespace2;
    }

    public PacketExtensionFilter(String namespace2) {
        this(null, namespace2);
    }

    public boolean accept(Packet packet) {
        return packet.getExtension(this.elementName, this.namespace) != null;
    }
}
