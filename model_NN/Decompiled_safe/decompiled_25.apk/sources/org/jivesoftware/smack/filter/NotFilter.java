package org.jivesoftware.smack.filter;

import org.jivesoftware.smack.packet.Packet;

public class NotFilter implements PacketFilter {
    private PacketFilter filter;

    public NotFilter(PacketFilter filter2) {
        if (filter2 == null) {
            throw new IllegalArgumentException("Parameter cannot be null.");
        }
        this.filter = filter2;
    }

    public boolean accept(Packet packet) {
        return !this.filter.accept(packet);
    }
}
