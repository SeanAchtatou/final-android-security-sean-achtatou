package org.jivesoftware.smack;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Packet;

public class PacketCollector {
    private static final int MAX_PACKETS = 65536;
    private boolean cancelled = false;
    private Connection conection;
    private PacketFilter packetFilter;
    private LinkedBlockingQueue<Packet> resultQueue;

    protected PacketCollector(Connection conection2, PacketFilter packetFilter2) {
        this.conection = conection2;
        this.packetFilter = packetFilter2;
        this.resultQueue = new LinkedBlockingQueue<>(65536);
    }

    public void cancel() {
        if (!this.cancelled) {
            this.conection.removePacketCollector(this);
            this.cancelled = true;
        }
    }

    public PacketFilter getPacketFilter() {
        return this.packetFilter;
    }

    public Packet pollResult() {
        return this.resultQueue.poll();
    }

    public Packet nextResult() {
        while (true) {
            try {
                break;
            } catch (InterruptedException e) {
            }
        }
        return this.resultQueue.take();
    }

    public Packet nextResult(long timeout) {
        do {
            try {
                return this.resultQueue.poll(timeout, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                if (System.currentTimeMillis() >= System.currentTimeMillis() + timeout) {
                    return null;
                }
            }
        } while (System.currentTimeMillis() >= System.currentTimeMillis() + timeout);
        return null;
    }

    /* access modifiers changed from: protected */
    public synchronized void processPacket(Packet packet) {
        if (packet != null) {
            if (this.packetFilter == null || this.packetFilter.accept(packet)) {
                while (!this.resultQueue.offer(packet)) {
                    this.resultQueue.poll();
                }
            }
        }
    }
}
