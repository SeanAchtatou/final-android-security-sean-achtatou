package org.jivesoftware.smack.packet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import org.jivesoftware.smack.util.StringUtils;

public class RosterPacket extends IQ {
    private final List<Item> rosterItems = new ArrayList();
    private String version;

    public enum ItemType {
        none,
        to,
        from,
        both,
        remove
    }

    public void addRosterItem(Item item) {
        synchronized (this.rosterItems) {
            this.rosterItems.add(item);
        }
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version2) {
        this.version = version2;
    }

    public int getRosterItemCount() {
        int size;
        synchronized (this.rosterItems) {
            size = this.rosterItems.size();
        }
        return size;
    }

    public Collection<Item> getRosterItems() {
        List unmodifiableList;
        synchronized (this.rosterItems) {
            unmodifiableList = Collections.unmodifiableList(new ArrayList(this.rosterItems));
        }
        return unmodifiableList;
    }

    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<query xmlns=\"jabber:iq:roster\" ");
        if (this.version != null) {
            buf.append(" ver=\"" + this.version + "\" ");
        }
        buf.append(">");
        synchronized (this.rosterItems) {
            for (Item entry : this.rosterItems) {
                buf.append(entry.toXML());
            }
        }
        buf.append("</query>");
        return buf.toString();
    }

    public static class Item {
        private final Set<String> groupNames = new CopyOnWriteArraySet();
        private ItemStatus itemStatus = null;
        private ItemType itemType = null;
        private String name;
        private String user;

        public Item(String user2, String name2) {
            this.user = user2.toLowerCase();
            this.name = name2;
        }

        public String getUser() {
            return this.user;
        }

        public String getName() {
            return this.name;
        }

        public void setName(String name2) {
            this.name = name2;
        }

        public ItemType getItemType() {
            return this.itemType;
        }

        public void setItemType(ItemType itemType2) {
            this.itemType = itemType2;
        }

        public ItemStatus getItemStatus() {
            return this.itemStatus;
        }

        public void setItemStatus(ItemStatus itemStatus2) {
            this.itemStatus = itemStatus2;
        }

        public Set<String> getGroupNames() {
            return Collections.unmodifiableSet(this.groupNames);
        }

        public void addGroupName(String groupName) {
            this.groupNames.add(groupName);
        }

        public void removeGroupName(String groupName) {
            this.groupNames.remove(groupName);
        }

        public String toXML() {
            StringBuilder buf = new StringBuilder();
            buf.append("<item jid=\"").append(this.user).append("\"");
            if (this.name != null) {
                buf.append(" name=\"").append(StringUtils.escapeForXML(this.name)).append("\"");
            }
            if (this.itemType != null) {
                buf.append(" subscription=\"").append(this.itemType).append("\"");
            }
            if (this.itemStatus != null) {
                buf.append(" ask=\"").append(this.itemStatus).append("\"");
            }
            buf.append(">");
            for (String groupName : this.groupNames) {
                buf.append("<group>").append(StringUtils.escapeForXML(groupName)).append("</group>");
            }
            buf.append("</item>");
            return buf.toString();
        }
    }

    public static class ItemStatus {
        public static final ItemStatus SUBSCRIPTION_PENDING = new ItemStatus("subscribe");
        public static final ItemStatus UNSUBSCRIPTION_PENDING = new ItemStatus("unsubscribe");
        private String value;

        public static ItemStatus fromString(String value2) {
            if (value2 == null) {
                return null;
            }
            String value3 = value2.toLowerCase();
            if ("unsubscribe".equals(value3)) {
                return UNSUBSCRIPTION_PENDING;
            }
            if ("subscribe".equals(value3)) {
                return SUBSCRIPTION_PENDING;
            }
            return null;
        }

        private ItemStatus(String value2) {
            this.value = value2;
        }

        public String toString() {
            return this.value;
        }
    }
}
