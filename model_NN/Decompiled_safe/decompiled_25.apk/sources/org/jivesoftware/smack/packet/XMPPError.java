package org.jivesoftware.smack.packet;

import com.womenchild.hospital.parameter.HttpRequestParameters;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class XMPPError {
    private List<PacketExtension> applicationExtensions = null;
    private int code;
    private String condition;
    private String message;
    private Type type;

    public enum Type {
        WAIT,
        CANCEL,
        MODIFY,
        AUTH,
        CONTINUE
    }

    public XMPPError(Condition condition2) {
        init(condition2);
        this.message = null;
    }

    public XMPPError(Condition condition2, String messageText) {
        init(condition2);
        this.message = messageText;
    }

    public XMPPError(int code2) {
        this.code = code2;
        this.message = null;
    }

    public XMPPError(int code2, String message2) {
        this.code = code2;
        this.message = message2;
    }

    public XMPPError(int code2, Type type2, String condition2, String message2, List<PacketExtension> extension) {
        this.code = code2;
        this.type = type2;
        this.condition = condition2;
        this.message = message2;
        this.applicationExtensions = extension;
    }

    private void init(Condition condition2) {
        ErrorSpecification defaultErrorSpecification = ErrorSpecification.specFor(condition2);
        this.condition = condition2.value;
        if (defaultErrorSpecification != null) {
            this.type = defaultErrorSpecification.getType();
            this.code = defaultErrorSpecification.getCode();
        }
    }

    public String getCondition() {
        return this.condition;
    }

    public Type getType() {
        return this.type;
    }

    public int getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }

    public String toXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<error code=\"").append(this.code).append("\"");
        if (this.type != null) {
            buf.append(" type=\"");
            buf.append(this.type.name());
            buf.append("\"");
        }
        buf.append(">");
        if (this.condition != null) {
            buf.append("<").append(this.condition);
            buf.append(" xmlns=\"urn:ietf:params:xml:ns:xmpp-stanzas\"/>");
        }
        if (this.message != null) {
            buf.append("<text xml:lang=\"en\" xmlns=\"urn:ietf:params:xml:ns:xmpp-stanzas\">");
            buf.append(this.message);
            buf.append("</text>");
        }
        for (PacketExtension element : getExtensions()) {
            buf.append(element.toXML());
        }
        buf.append("</error>");
        return buf.toString();
    }

    public String toString() {
        StringBuilder txt = new StringBuilder();
        if (this.condition != null) {
            txt.append(this.condition);
        }
        txt.append("(").append(this.code).append(")");
        if (this.message != null) {
            txt.append(" ").append(this.message);
        }
        return txt.toString();
    }

    public synchronized List<PacketExtension> getExtensions() {
        List<PacketExtension> unmodifiableList;
        if (this.applicationExtensions == null) {
            unmodifiableList = Collections.emptyList();
        } else {
            unmodifiableList = Collections.unmodifiableList(this.applicationExtensions);
        }
        return unmodifiableList;
    }

    public synchronized PacketExtension getExtension(String elementName, String namespace) {
        PacketExtension ext;
        if (this.applicationExtensions != null && elementName != null && namespace != null) {
            Iterator<PacketExtension> it = this.applicationExtensions.iterator();
            while (true) {
                if (it.hasNext()) {
                    ext = it.next();
                    if (elementName.equals(ext.getElementName()) && namespace.equals(ext.getNamespace())) {
                        break;
                    }
                } else {
                    ext = null;
                    break;
                }
            }
        } else {
            ext = null;
        }
        return ext;
    }

    public synchronized void addExtension(PacketExtension extension) {
        if (this.applicationExtensions == null) {
            this.applicationExtensions = new ArrayList();
        }
        this.applicationExtensions.add(extension);
    }

    public synchronized void setExtension(List<PacketExtension> extension) {
        this.applicationExtensions = extension;
    }

    public static class Condition {
        public static final Condition bad_request = new Condition("bad-request");
        public static final Condition conflict = new Condition("conflict");
        public static final Condition feature_not_implemented = new Condition("feature-not-implemented");
        public static final Condition forbidden = new Condition("forbidden");
        public static final Condition gone = new Condition("gone");
        public static final Condition interna_server_error = new Condition("internal-server-error");
        public static final Condition item_not_found = new Condition("item-not-found");
        public static final Condition jid_malformed = new Condition("jid-malformed");
        public static final Condition no_acceptable = new Condition("not-acceptable");
        public static final Condition not_allowed = new Condition("not-allowed");
        public static final Condition not_authorized = new Condition("not-authorized");
        public static final Condition payment_required = new Condition("payment-required");
        public static final Condition recipient_unavailable = new Condition("recipient-unavailable");
        public static final Condition redirect = new Condition("redirect");
        public static final Condition registration_required = new Condition("registration-required");
        public static final Condition remote_server_error = new Condition("remote-server-error");
        public static final Condition remote_server_not_found = new Condition("remote-server-not-found");
        public static final Condition remote_server_timeout = new Condition("remote-server-timeout");
        public static final Condition request_timeout = new Condition("request-timeout");
        public static final Condition resource_constraint = new Condition("resource-constraint");
        public static final Condition service_unavailable = new Condition("service-unavailable");
        public static final Condition subscription_required = new Condition("subscription-required");
        public static final Condition undefined_condition = new Condition("undefined-condition");
        public static final Condition unexpected_request = new Condition("unexpected-request");
        /* access modifiers changed from: private */
        public String value;

        public Condition(String value2) {
            this.value = value2;
        }

        public String toString() {
            return this.value;
        }
    }

    private static class ErrorSpecification {
        private static Map<Condition, ErrorSpecification> instances = errorSpecifications();
        private int code;
        private Condition condition;
        private Type type;

        private ErrorSpecification(Condition condition2, Type type2, int code2) {
            this.code = code2;
            this.type = type2;
            this.condition = condition2;
        }

        private static Map<Condition, ErrorSpecification> errorSpecifications() {
            Map<Condition, ErrorSpecification> instances2 = new HashMap<>(22);
            instances2.put(Condition.interna_server_error, new ErrorSpecification(Condition.interna_server_error, Type.WAIT, 500));
            instances2.put(Condition.forbidden, new ErrorSpecification(Condition.forbidden, Type.AUTH, HttpRequestParameters.CANCEL_COMPANION_SUPPER_ORDER));
            instances2.put(Condition.bad_request, new ErrorSpecification(Condition.bad_request, Type.MODIFY, HttpRequestParameters.CANCEL_COMPANION_ORDER));
            instances2.put(Condition.item_not_found, new ErrorSpecification(Condition.item_not_found, Type.CANCEL, HttpRequestParameters.CANCEL_SUPPER_ORDER));
            instances2.put(Condition.conflict, new ErrorSpecification(Condition.conflict, Type.CANCEL, 409));
            instances2.put(Condition.feature_not_implemented, new ErrorSpecification(Condition.feature_not_implemented, Type.CANCEL, HttpRequestParameters.ADD_ONLINE_CONSULT));
            instances2.put(Condition.gone, new ErrorSpecification(Condition.gone, Type.MODIFY, HttpRequestParameters.LISTDEPT_BY_PALN));
            instances2.put(Condition.jid_malformed, new ErrorSpecification(Condition.jid_malformed, Type.MODIFY, HttpRequestParameters.CANCEL_COMPANION_ORDER));
            instances2.put(Condition.no_acceptable, new ErrorSpecification(Condition.no_acceptable, Type.MODIFY, HttpRequestParameters.COMPANION_ORDER));
            instances2.put(Condition.not_allowed, new ErrorSpecification(Condition.not_allowed, Type.CANCEL, HttpRequestParameters.CANCEL_REPORT_SUPPER_ORDER));
            instances2.put(Condition.not_authorized, new ErrorSpecification(Condition.not_authorized, Type.AUTH, HttpRequestParameters.CANCEL_ORDER));
            instances2.put(Condition.payment_required, new ErrorSpecification(Condition.payment_required, Type.AUTH, HttpRequestParameters.CANCEL_REPORT_ORDER));
            instances2.put(Condition.recipient_unavailable, new ErrorSpecification(Condition.recipient_unavailable, Type.WAIT, HttpRequestParameters.CANCEL_SUPPER_ORDER));
            instances2.put(Condition.redirect, new ErrorSpecification(Condition.redirect, Type.MODIFY, HttpRequestParameters.LISTDEPT_BY_PALN));
            instances2.put(Condition.registration_required, new ErrorSpecification(Condition.registration_required, Type.AUTH, HttpRequestParameters.OPC_CARD_TYPE));
            instances2.put(Condition.remote_server_not_found, new ErrorSpecification(Condition.remote_server_not_found, Type.CANCEL, HttpRequestParameters.CANCEL_SUPPER_ORDER));
            instances2.put(Condition.remote_server_timeout, new ErrorSpecification(Condition.remote_server_timeout, Type.WAIT, HttpRequestParameters.PATIENT_CARD_INFO));
            instances2.put(Condition.remote_server_error, new ErrorSpecification(Condition.remote_server_error, Type.CANCEL, HttpRequestParameters.CONSULTINFO_DETAIL));
            instances2.put(Condition.resource_constraint, new ErrorSpecification(Condition.resource_constraint, Type.WAIT, 500));
            instances2.put(Condition.service_unavailable, new ErrorSpecification(Condition.service_unavailable, Type.CANCEL, HttpRequestParameters.ADD_REPLY));
            instances2.put(Condition.subscription_required, new ErrorSpecification(Condition.subscription_required, Type.AUTH, HttpRequestParameters.OPC_CARD_TYPE));
            instances2.put(Condition.undefined_condition, new ErrorSpecification(Condition.undefined_condition, Type.WAIT, 500));
            instances2.put(Condition.unexpected_request, new ErrorSpecification(Condition.unexpected_request, Type.WAIT, HttpRequestParameters.CANCEL_COMPANION_ORDER));
            instances2.put(Condition.request_timeout, new ErrorSpecification(Condition.request_timeout, Type.CANCEL, HttpRequestParameters.ORDER_FIELD));
            return instances2;
        }

        protected static ErrorSpecification specFor(Condition condition2) {
            return instances.get(condition2);
        }

        /* access modifiers changed from: protected */
        public Condition getCondition() {
            return this.condition;
        }

        /* access modifiers changed from: protected */
        public Type getType() {
            return this.type;
        }

        /* access modifiers changed from: protected */
        public int getCode() {
            return this.code;
        }
    }
}
