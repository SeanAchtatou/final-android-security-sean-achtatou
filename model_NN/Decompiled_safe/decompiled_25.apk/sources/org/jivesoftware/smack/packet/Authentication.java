package org.jivesoftware.smack.packet;

import com.amap.mapapi.poisearch.PoiTypeDef;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.util.StringUtils;

public class Authentication extends IQ {
    private String digest = null;
    private String password = null;
    private String resource = null;
    private String username = null;

    public Authentication() {
        setType(IQ.Type.SET);
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username2) {
        this.username = username2;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password2) {
        this.password = password2;
    }

    public String getDigest() {
        return this.digest;
    }

    public void setDigest(String connectionID, String password2) {
        this.digest = StringUtils.hash(String.valueOf(connectionID) + password2);
    }

    public void setDigest(String digest2) {
        this.digest = digest2;
    }

    public String getResource() {
        return this.resource;
    }

    public void setResource(String resource2) {
        this.resource = resource2;
    }

    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<query xmlns=\"jabber:iq:auth\">");
        if (this.username != null) {
            if (this.username.equals(PoiTypeDef.All)) {
                buf.append("<username/>");
            } else {
                buf.append("<username>").append(this.username).append("</username>");
            }
        }
        if (this.digest != null) {
            if (this.digest.equals(PoiTypeDef.All)) {
                buf.append("<digest/>");
            } else {
                buf.append("<digest>").append(this.digest).append("</digest>");
            }
        }
        if (this.password != null && this.digest == null) {
            if (this.password.equals(PoiTypeDef.All)) {
                buf.append("<password/>");
            } else {
                buf.append("<password>").append(StringUtils.escapeForXML(this.password)).append("</password>");
            }
        }
        if (this.resource != null) {
            if (this.resource.equals(PoiTypeDef.All)) {
                buf.append("<resource/>");
            } else {
                buf.append("<resource>").append(this.resource).append("</resource>");
            }
        }
        buf.append("</query>");
        return buf.toString();
    }
}
