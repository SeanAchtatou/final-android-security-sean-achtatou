package org.jivesoftware.smack.packet;

public class PrivacyItem {
    private boolean allow;
    private boolean filterIQ = false;
    private boolean filterMessage = false;
    private boolean filterPresence_in = false;
    private boolean filterPresence_out = false;
    private int order;
    private PrivacyRule rule;

    public enum Type {
        group,
        jid,
        subscription
    }

    public PrivacyItem(String type, boolean allow2, int order2) {
        setRule(PrivacyRule.fromString(type));
        setAllow(allow2);
        setOrder(order2);
    }

    public boolean isAllow() {
        return this.allow;
    }

    private void setAllow(boolean allow2) {
        this.allow = allow2;
    }

    public boolean isFilterIQ() {
        return this.filterIQ;
    }

    public void setFilterIQ(boolean filterIQ2) {
        this.filterIQ = filterIQ2;
    }

    public boolean isFilterMessage() {
        return this.filterMessage;
    }

    public void setFilterMessage(boolean filterMessage2) {
        this.filterMessage = filterMessage2;
    }

    public boolean isFilterPresence_in() {
        return this.filterPresence_in;
    }

    public void setFilterPresence_in(boolean filterPresence_in2) {
        this.filterPresence_in = filterPresence_in2;
    }

    public boolean isFilterPresence_out() {
        return this.filterPresence_out;
    }

    public void setFilterPresence_out(boolean filterPresence_out2) {
        this.filterPresence_out = filterPresence_out2;
    }

    public int getOrder() {
        return this.order;
    }

    private void setOrder(int order2) {
        this.order = order2;
    }

    public void setValue(String value) {
        if (getRule() != null || value != null) {
            getRule().setValue(value);
        }
    }

    public Type getType() {
        if (getRule() == null) {
            return null;
        }
        return getRule().getType();
    }

    public String getValue() {
        if (getRule() == null) {
            return null;
        }
        return getRule().getValue();
    }

    public boolean isFilterEverything() {
        return !isFilterIQ() && !isFilterMessage() && !isFilterPresence_in() && !isFilterPresence_out();
    }

    private PrivacyRule getRule() {
        return this.rule;
    }

    private void setRule(PrivacyRule rule2) {
        this.rule = rule2;
    }

    public String toXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<item");
        if (isAllow()) {
            buf.append(" action=\"allow\"");
        } else {
            buf.append(" action=\"deny\"");
        }
        buf.append(" order=\"").append(getOrder()).append("\"");
        if (getType() != null) {
            buf.append(" type=\"").append(getType()).append("\"");
        }
        if (getValue() != null) {
            buf.append(" value=\"").append(getValue()).append("\"");
        }
        if (isFilterEverything()) {
            buf.append("/>");
        } else {
            buf.append(">");
            if (isFilterIQ()) {
                buf.append("<iq/>");
            }
            if (isFilterMessage()) {
                buf.append("<message/>");
            }
            if (isFilterPresence_in()) {
                buf.append("<presence-in/>");
            }
            if (isFilterPresence_out()) {
                buf.append("<presence-out/>");
            }
            buf.append("</item>");
        }
        return buf.toString();
    }

    public static class PrivacyRule {
        public static final String SUBSCRIPTION_BOTH = "both";
        public static final String SUBSCRIPTION_FROM = "from";
        public static final String SUBSCRIPTION_NONE = "none";
        public static final String SUBSCRIPTION_TO = "to";
        private Type type;
        private String value;

        protected static PrivacyRule fromString(String value2) {
            if (value2 == null) {
                return null;
            }
            PrivacyRule rule = new PrivacyRule();
            rule.setType(Type.valueOf(value2.toLowerCase()));
            return rule;
        }

        public Type getType() {
            return this.type;
        }

        private void setType(Type type2) {
            this.type = type2;
        }

        public String getValue() {
            return this.value;
        }

        /* access modifiers changed from: protected */
        public void setValue(String value2) {
            if (isSuscription()) {
                setSuscriptionValue(value2);
            } else {
                this.value = value2;
            }
        }

        private void setSuscriptionValue(String value2) {
            String setValue;
            if (SUBSCRIPTION_BOTH.equalsIgnoreCase(value2)) {
                setValue = SUBSCRIPTION_BOTH;
            } else if (SUBSCRIPTION_TO.equalsIgnoreCase(value2)) {
                setValue = SUBSCRIPTION_TO;
            } else if (SUBSCRIPTION_FROM.equalsIgnoreCase(value2)) {
                setValue = SUBSCRIPTION_FROM;
            } else if (SUBSCRIPTION_NONE.equalsIgnoreCase(value2)) {
                setValue = SUBSCRIPTION_NONE;
            } else {
                setValue = null;
            }
            this.value = setValue;
        }

        public boolean isSuscription() {
            return getType() == Type.subscription;
        }
    }
}
