package org.jivesoftware.smack;

import java.util.List;
import org.jivesoftware.smack.packet.PrivacyItem;

public class PrivacyList {
    private boolean isActiveList;
    private boolean isDefaultList;
    private List<PrivacyItem> items;
    private String listName;

    protected PrivacyList(boolean isActiveList2, boolean isDefaultList2, String listName2, List<PrivacyItem> privacyItems) {
        this.isActiveList = isActiveList2;
        this.isDefaultList = isDefaultList2;
        this.listName = listName2;
        this.items = privacyItems;
    }

    public boolean isActiveList() {
        return this.isActiveList;
    }

    public boolean isDefaultList() {
        return this.isDefaultList;
    }

    public List<PrivacyItem> getItems() {
        return this.items;
    }

    public String toString() {
        return this.listName;
    }
}
