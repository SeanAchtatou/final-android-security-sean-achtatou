package com.amap.mapapi.route;

import com.amap.mapapi.core.d;
import com.amap.mapapi.route.Route;

/* compiled from: RouteParam */
public class f {
    public Route.FromAndTo a;
    public int b;
    public String c;

    public void a(String str) {
        this.c = str;
    }

    public f(Route.FromAndTo fromAndTo, int i) {
        this.a = fromAndTo;
        this.b = i;
    }

    public double a() {
        return d.a(this.a.mFrom.a());
    }

    public double b() {
        return d.a(this.a.mTo.a());
    }

    public double c() {
        return d.a(this.a.mFrom.b());
    }

    public double d() {
        return d.a(this.a.mTo.b());
    }
}
