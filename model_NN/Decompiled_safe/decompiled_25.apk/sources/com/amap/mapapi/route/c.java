package com.amap.mapapi.route;

import com.amap.mapapi.core.AMapException;
import com.amap.mapapi.core.a;
import com.amap.mapapi.map.i;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.io.IOException;
import java.io.InputStream;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.LinkedList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Node;

/* compiled from: DriveHandler */
class c extends d {
    public c(f fVar, Proxy proxy, String str, String str2) {
        super(fVar, proxy, str, str2);
    }

    /* access modifiers changed from: protected */
    public void a(ArrayList<Route> arrayList) {
    }

    /* access modifiers changed from: protected */
    public void a(Node node, ArrayList<Route> arrayList) {
    }

    /* access modifiers changed from: protected */
    public boolean e() {
        return false;
    }

    /* access modifiers changed from: protected */
    public byte[] g() {
        StringBuilder sb = new StringBuilder();
        sb.append("config=R&resType=json&enc=utf-8&ver=2.0");
        sb.append("&x1=");
        sb.append(((f) this.b).a());
        sb.append("&y1=");
        sb.append(((f) this.b).c());
        sb.append("&x2=");
        sb.append(((f) this.b).b());
        sb.append("&y2=");
        sb.append(((f) this.b).d());
        sb.append("&routeType=");
        sb.append(((f) this.b).b % 10);
        sb.append("&per=");
        sb.append(50);
        a a = a.a(null);
        sb.append("&a_k=");
        sb.append(a.a());
        return sb.toString().getBytes();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ArrayList<Route> c(InputStream inputStream) throws AMapException {
        String str;
        if (e()) {
            return super.c(inputStream);
        }
        ArrayList<Route> arrayList = new ArrayList<>();
        try {
            str = new String(i.a(inputStream));
        } catch (Exception e) {
            e.printStackTrace();
            str = null;
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.getInt("count") > 0) {
                JSONArray jSONArray = jSONObject.getJSONArray("segmengList");
                Route route = new Route(((f) this.b).b);
                LinkedList linkedList = new LinkedList();
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                    DriveWalkSegment driveWalkSegment = new DriveWalkSegment();
                    String string = jSONObject2.getString("roadLength");
                    if (string.contains("千米")) {
                        string = ((int) (Double.parseDouble(string.substring(0, string.length() - 2)) * 1000.0d)) + PoiTypeDef.All;
                    } else if (string.contains("米")) {
                        string = string.substring(0, string.length() - 1);
                    } else if (string.contains("公里")) {
                        string = ((int) (Double.parseDouble(string.substring(0, string.length() - 2)) * 1000.0d)) + PoiTypeDef.All;
                    }
                    driveWalkSegment.setLength(Integer.parseInt(string));
                    driveWalkSegment.setRoadName(jSONObject2.getString("roadName"));
                    driveWalkSegment.setActionDescription(jSONObject2.getString("action"));
                    try {
                        driveWalkSegment.setShapes(a(jSONObject2.getString("coor").split(",")));
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    driveWalkSegment.setConsumeTime(jSONObject2.getString("driveTime"));
                    if (driveWalkSegment.getShapes().length != 0) {
                        linkedList.add(driveWalkSegment);
                    }
                }
                if (linkedList.size() == 0) {
                    return null;
                }
                route.a(linkedList);
                a(route);
                for (Segment route2 : route.a()) {
                    route2.setRoute(route);
                }
                if (route != null) {
                    route.setStartPlace(this.i);
                    route.setTargetPlace(this.j);
                }
                arrayList.add(route);
            }
        } catch (JSONException e3) {
            e3.printStackTrace();
        }
        if (arrayList.size() == 0) {
            throw new AMapException(AMapException.ERROR_IO);
        }
        a(arrayList);
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e4) {
                throw new AMapException(AMapException.ERROR_IO);
            }
        }
        return arrayList;
    }
}
