package com.amap.mapapi.route;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.support.v4.view.MotionEventCompat;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.amap.mapapi.core.AMapException;
import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.core.h;
import com.amap.mapapi.core.o;
import com.amap.mapapi.core.p;
import com.amap.mapapi.core.q;
import com.amap.mapapi.map.MapView;
import com.amap.mapapi.map.RouteMessageHandler;
import com.amap.mapapi.map.RouteOverlay;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.net.Proxy;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Route {
    public static final int BusDefault = 0;
    public static final int BusLeaseChange = 2;
    public static final int BusLeaseWalk = 3;
    public static final int BusMostComfortable = 4;
    public static final int BusSaveMoney = 1;
    public static final int DrivingDefault = 10;
    public static final int DrivingLeastDistance = 12;
    public static final int DrivingNoFastRoad = 13;
    public static final int DrivingSaveMoney = 11;
    private GeoPoint a = null;
    private GeoPoint b = null;
    private int c;
    public d mHelper;
    protected List<Segment> mSegs;
    protected String mStartPlace;
    protected String mTargetPlace;

    public int getMode() {
        return this.c;
    }

    public int getLength() {
        int i = 0;
        Iterator<Segment> it = this.mSegs.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            i = it.next().getLength() + i2;
        }
    }

    private void b() {
        int i = Integer.MIN_VALUE;
        int i2 = Integer.MAX_VALUE;
        int i3 = Integer.MAX_VALUE;
        for (Segment lowerLeftPoint : this.mSegs) {
            GeoPoint lowerLeftPoint2 = lowerLeftPoint.getLowerLeftPoint();
            int longitudeE6 = lowerLeftPoint2.getLongitudeE6();
            int latitudeE6 = lowerLeftPoint2.getLatitudeE6();
            if (longitudeE6 < i3) {
                i3 = longitudeE6;
            }
            if (latitudeE6 >= i2) {
                latitudeE6 = i2;
            }
            i2 = latitudeE6;
        }
        int i4 = Integer.MIN_VALUE;
        for (Segment upperRightPoint : this.mSegs) {
            GeoPoint upperRightPoint2 = upperRightPoint.getUpperRightPoint();
            int longitudeE62 = upperRightPoint2.getLongitudeE6();
            int latitudeE62 = upperRightPoint2.getLatitudeE6();
            if (longitudeE62 > i4) {
                i4 = longitudeE62;
            }
            if (latitudeE62 <= i) {
                latitudeE62 = i;
            }
            i = latitudeE62;
        }
        this.a = new GeoPoint(i2, i3);
        this.b = new GeoPoint(i, i4);
    }

    public GeoPoint getLowerLeftPoint() {
        if (this.a == null) {
            b();
        }
        return this.a;
    }

    public GeoPoint getUpperRightPoint() {
        if (this.b == null) {
            b();
        }
        return this.b;
    }

    public static List<Route> calculateRoute(Context context, FromAndTo fromAndTo, int i) throws AMapException {
        e cVar;
        com.amap.mapapi.core.a.a(context);
        f fVar = new f(fromAndTo, i);
        Proxy b2 = com.amap.mapapi.core.d.b(context);
        String a2 = com.amap.mapapi.core.d.a(context);
        fromAndTo.a(context, fromAndTo.mFrom, fromAndTo.mTo, fromAndTo.mTrans);
        double a3 = com.amap.mapapi.core.d.a(fromAndTo.mFrom.a());
        double a4 = com.amap.mapapi.core.d.a(fromAndTo.mFrom.b());
        if (isBus(i)) {
            String a5 = a(a3, a4, b2, a2);
            if (a5 == null || a5.equals(PoiTypeDef.All)) {
                return new ArrayList();
            }
            fVar.a(a5);
            cVar = new a(fVar, b2, a2, null);
        } else if (isWalk(i)) {
            cVar = new g(fVar, b2, a2, null);
        } else {
            cVar = new c(fVar, b2, a2, null);
        }
        return (List) cVar.j();
    }

    private static String a(double d2, double d3, Proxy proxy, String str) throws AMapException {
        List list = (List) new o(new p(d2, d3, 1, false), proxy, str, null).j();
        if (list.size() <= 0) {
            return PoiTypeDef.All;
        }
        Address address = (Address) list.get(0);
        String locality = address.getLocality();
        if (locality == null || locality.equals(PoiTypeDef.All)) {
            return address.getAdminArea();
        }
        return locality;
    }

    public String getStartPlace() {
        return this.mStartPlace;
    }

    public void setStartPlace(String str) {
        this.mStartPlace = str;
    }

    public String getTargetPlace() {
        return this.mTargetPlace;
    }

    public void setTargetPlace(String str) {
        this.mTargetPlace = str;
    }

    /* access modifiers changed from: package-private */
    public List<Segment> a() {
        return this.mSegs;
    }

    /* access modifiers changed from: package-private */
    public void a(List<Segment> list) {
        this.mSegs = list;
    }

    public GeoPoint getStartPos() {
        return this.mSegs.get(0).getFirstPoint();
    }

    public GeoPoint getTargetPos() {
        return this.mSegs.get(getStepCount() - 1).getLastPoint();
    }

    public int getStepCount() {
        return this.mSegs.size();
    }

    public Segment getStep(int i) {
        return this.mSegs.get(i);
    }

    public int getSegmentIndex(Segment segment) {
        return this.mSegs.indexOf(segment);
    }

    public String getStepedDescription(int i) {
        return this.mHelper.b(i);
    }

    public String getOverview() {
        return this.mHelper.a();
    }

    public static class FromAndTo {
        public static final int NoTrans = 0;
        public static final int TransBothPoint = 3;
        public static final int TransFromPoint = 1;
        public static final int TransToPoint = 2;
        public GeoPoint mFrom;
        public GeoPoint mTo;
        public int mTrans;

        public FromAndTo(GeoPoint geoPoint, GeoPoint geoPoint2, int i) {
            this.mFrom = geoPoint;
            this.mTo = geoPoint2;
            this.mTrans = i;
        }

        public FromAndTo(GeoPoint geoPoint, GeoPoint geoPoint2) {
            this(geoPoint, geoPoint2, 0);
        }

        /* access modifiers changed from: private */
        public void a(Context context, GeoPoint geoPoint, GeoPoint geoPoint2, int i) throws AMapException {
            switch (this.mTrans) {
                case 0:
                    this.mFrom = geoPoint;
                    this.mTo = geoPoint2;
                    return;
                case 1:
                    GeoPoint.b a = a(context, geoPoint);
                    this.mFrom = new GeoPoint(com.amap.mapapi.core.d.a(a.b), com.amap.mapapi.core.d.a(a.a));
                    return;
                case 2:
                    GeoPoint.b a2 = a(context, geoPoint2);
                    this.mTo = new GeoPoint(com.amap.mapapi.core.d.a(a2.b), com.amap.mapapi.core.d.a(a2.a));
                    return;
                case 3:
                    GeoPoint.b a3 = a(context, geoPoint);
                    this.mFrom = new GeoPoint(com.amap.mapapi.core.d.a(a3.b), com.amap.mapapi.core.d.a(a3.a));
                    GeoPoint.b a4 = a(context, geoPoint2);
                    this.mTo = new GeoPoint(com.amap.mapapi.core.d.a(a4.b), com.amap.mapapi.core.d.a(a4.a));
                    return;
                default:
                    return;
            }
        }

        private GeoPoint.b a(Context context, GeoPoint geoPoint) throws AMapException {
            return (GeoPoint.b) new h(new GeoPoint.b(com.amap.mapapi.core.d.a(geoPoint.a()), com.amap.mapapi.core.d.a(geoPoint.b())), com.amap.mapapi.core.d.b(context), com.amap.mapapi.core.d.a(context), null).j();
        }
    }

    public abstract class d {
        public abstract Paint a(int i);

        public abstract String a();

        /* access modifiers changed from: protected */
        public abstract Drawable f(int i);

        public d() {
        }

        public String b(int i) {
            return h(i);
        }

        private String h(int i) {
            if (i == 0) {
                return Route.this.mStartPlace;
            }
            if (i == Route.this.getStepCount()) {
                return Route.this.mTargetPlace;
            }
            return null;
        }

        public Spanned c(int i) {
            String h = h(i);
            if (h == null) {
                return null;
            }
            return com.amap.mapapi.core.d.c(com.amap.mapapi.core.d.a(h, "#000000"));
        }

        public int d(int i) {
            if (i >= Route.this.getStepCount()) {
                return -1;
            }
            return i + 1;
        }

        public int e(int i) {
            if (i <= 0) {
                return -1;
            }
            return i - 1;
        }

        public View a(MapView mapView, Context context, RouteMessageHandler routeMessageHandler, RouteOverlay routeOverlay, int i) {
            return null;
        }

        public GeoPoint g(int i) {
            if (i == Route.this.getStepCount()) {
                return Route.this.getStep(i - 1).getLastPoint();
            }
            return Route.this.getStep(i).getFirstPoint();
        }

        public View b(MapView mapView, Context context, RouteMessageHandler routeMessageHandler, RouteOverlay routeOverlay, int i) {
            if (i < 0 || i > Route.this.getStepCount()) {
                return null;
            }
            LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setOrientation(1);
            linearLayout.setBackgroundColor(Color.argb((int) MotionEventCompat.ACTION_MASK, (int) MotionEventCompat.ACTION_MASK, (int) MotionEventCompat.ACTION_MASK, (int) MotionEventCompat.ACTION_MASK));
            LinearLayout linearLayout2 = new LinearLayout(context);
            linearLayout2.setOrientation(0);
            linearLayout2.setBackgroundColor(-1);
            linearLayout2.setGravity(2);
            ImageView imageView = new ImageView(context);
            imageView.setBackgroundColor(-1);
            imageView.setImageDrawable(f(i));
            imageView.setPadding(3, 3, 0, 0);
            linearLayout2.addView(imageView, new LinearLayout.LayoutParams(-2, -2));
            TextView textView = new TextView(context);
            textView.setBackgroundColor(-1);
            String[] split = c(i).toString().split("\\n", 2);
            textView.setTextColor(-16777216);
            textView.setText(com.amap.mapapi.core.d.c(split[0]));
            textView.setPadding(3, 0, 0, 3);
            linearLayout2.addView(textView, new LinearLayout.LayoutParams(-1, -2));
            TextView textView2 = new TextView(context);
            textView2.setBackgroundColor(Color.rgb(165, 166, 165));
            textView2.setLayoutParams(new LinearLayout.LayoutParams(-1, 1));
            LinearLayout linearLayout3 = new LinearLayout(context);
            linearLayout3.setOrientation(1);
            linearLayout3.setBackgroundColor(-1);
            TextView textView3 = new TextView(context);
            textView3.setBackgroundColor(-1);
            if (split.length == 2) {
                linearLayout3.addView(textView2, new LinearLayout.LayoutParams(-1, 1));
                textView3.setText(com.amap.mapapi.core.d.c(split[1]));
                textView3.setTextColor(Color.rgb(82, 85, 82));
                linearLayout3.addView(textView3, new LinearLayout.LayoutParams(-1, -2));
            }
            LinearLayout linearLayout4 = new LinearLayout(context);
            linearLayout4.setOrientation(0);
            linearLayout4.setGravity(1);
            linearLayout4.setBackgroundColor(-1);
            linearLayout.addView(linearLayout2, new LinearLayout.LayoutParams(-1, -2));
            linearLayout.addView(linearLayout3, new LinearLayout.LayoutParams(-1, 1));
            linearLayout.addView(linearLayout4, new LinearLayout.LayoutParams(-1, -2));
            new DisplayMetrics();
            DisplayMetrics displayMetrics = context.getApplicationContext().getResources().getDisplayMetrics();
            if (((long) (displayMetrics.heightPixels * displayMetrics.widthPixels)) <= 153600) {
                return linearLayout;
            }
            TextView textView4 = new TextView(context);
            textView4.setText(PoiTypeDef.All);
            textView4.setHeight(5);
            textView4.setWidth(1);
            linearLayout.addView(textView4);
            return linearLayout;
        }
    }

    public class a extends d {
        public a() {
            super();
        }

        public Paint a(int i) {
            Paint paint = q.k;
            if (Route.this.getStep(i) instanceof BusSegment) {
                return q.l;
            }
            return paint;
        }

        public String a() {
            StringBuilder sb = new StringBuilder();
            int stepCount = Route.this.getStepCount();
            int i = 0;
            for (int i2 = 1; i2 < stepCount; i2 += 2) {
                BusSegment busSegment = (BusSegment) Route.this.getStep(i2);
                if (i2 != 1) {
                    sb.append(" -> ");
                }
                sb.append(busSegment.getLineName());
                i += busSegment.getLength();
            }
            if (i != 0) {
                sb.append("\n");
            }
            int i3 = 0;
            for (int i4 = 0; i4 < stepCount; i4 += 2) {
                i3 += Route.this.getStep(i4).getLength();
            }
            sb.append(String.format("%s%s  %s%s", "乘车", Route.a(i), "步行", Route.a(i3)));
            return sb.toString();
        }

        public String b(int i) {
            String b = super.b(i);
            if (b != null) {
                return b;
            }
            if (Route.this.getStep(i) instanceof BusSegment) {
                return i(i);
            }
            return h(i);
        }

        public Spanned c(int i) {
            Spanned c = super.c(i);
            if (c != null) {
                return c;
            }
            if (Route.this.getStep(i) instanceof BusSegment) {
                return k(i);
            }
            return j(i);
        }

        private String h(int i) {
            StringBuilder sb = new StringBuilder();
            sb.append("步行").append("去往");
            if (i == Route.this.getStepCount() - 1) {
                sb.append("目的地");
            } else {
                sb.append(((BusSegment) Route.this.getStep(i + 1)).getLineName() + "车站");
            }
            sb.append("\n大约" + Route.a(Route.this.getStep(i).getLength()));
            return sb.toString();
        }

        private String i(int i) {
            BusSegment busSegment = (BusSegment) Route.this.getStep(i);
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(String.format("%s ( %s -- %s ) - %s%s\n", busSegment.getLineName(), busSegment.getFirstStationName(), busSegment.getLastStationName(), busSegment.getLastStationName(), "方向"));
            stringBuffer.append("上车 : " + busSegment.getOnStationName() + "\n");
            stringBuffer.append("下车 : " + busSegment.getOffStationName() + "\n");
            stringBuffer.append(String.format("%s%d%s (%s)", "公交", Integer.valueOf(busSegment.getStopNumber() - 1), "站", "大约" + Route.a(busSegment.getLength())));
            return stringBuffer.toString();
        }

        private Spanned j(int i) {
            StringBuilder sb = new StringBuilder();
            sb.append("步行").append("去往");
            if (i == Route.this.getStepCount() - 1) {
                sb.append(com.amap.mapapi.core.d.a("目的地", "#808080"));
            } else {
                sb.append(com.amap.mapapi.core.d.a(((BusSegment) Route.this.getStep(i + 1)).getLineName() + "车站", "#808080"));
            }
            sb.append(com.amap.mapapi.core.d.c());
            sb.append("大约" + Route.a(Route.this.getStep(i).getLength()));
            return com.amap.mapapi.core.d.c(sb.toString());
        }

        private Spanned k(int i) {
            BusSegment busSegment = (BusSegment) Route.this.getStep(i);
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(com.amap.mapapi.core.d.a(busSegment.getLineName(), "#000000"));
            stringBuffer.append(com.amap.mapapi.core.d.c(3));
            stringBuffer.append(com.amap.mapapi.core.d.a(busSegment.getLastStationName(), "#000000"));
            stringBuffer.append("方向");
            stringBuffer.append(com.amap.mapapi.core.d.c());
            stringBuffer.append("上车 : ");
            stringBuffer.append(com.amap.mapapi.core.d.a(busSegment.getOnStationName(), "#000000"));
            stringBuffer.append(com.amap.mapapi.core.d.c(3));
            stringBuffer.append(com.amap.mapapi.core.d.c());
            stringBuffer.append("下车 : ");
            stringBuffer.append(com.amap.mapapi.core.d.a(busSegment.getOffStationName(), "#000000"));
            stringBuffer.append(com.amap.mapapi.core.d.c());
            stringBuffer.append(String.format("%s%d%s , ", "公交", Integer.valueOf(busSegment.getStopNumber() - 1), "站"));
            stringBuffer.append("大约" + Route.a(busSegment.getLength()));
            return com.amap.mapapi.core.d.c(stringBuffer.toString());
        }

        public int d(int i) {
            do {
                i++;
                if (i >= Route.this.getStepCount() - 1) {
                    break;
                }
            } while (!(Route.this.getStep(i) instanceof BusSegment));
            return i;
        }

        public int e(int i) {
            if (i == Route.this.getStepCount()) {
                return i - 1;
            }
            while (true) {
                int i2 = i - 1;
                if (i2 <= 0 || (Route.this.getStep(i2) instanceof BusSegment)) {
                    return i2;
                }
                i = i2;
            }
        }

        public View a(MapView mapView, Context context, RouteMessageHandler routeMessageHandler, RouteOverlay routeOverlay, int i) {
            String str;
            Drawable f = f(i);
            if (i == 0 || i == Route.this.getStepCount()) {
                return null;
            }
            Segment step = Route.this.getStep(i);
            if (step instanceof BusSegment) {
                str = ((BusSegment) step).getLineName();
            } else {
                str = null;
            }
            if (str == null && f == null) {
                return null;
            }
            LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setOrientation(0);
            ImageView imageView = new ImageView(context);
            imageView.setImageDrawable(f);
            imageView.setPadding(3, 3, 1, 5);
            linearLayout.addView(imageView, new LinearLayout.LayoutParams(-2, -2));
            if (str != null) {
                TextView textView = new TextView(context);
                textView.setText(str);
                textView.setTextColor(-16777216);
                textView.setPadding(3, 0, 3, 3);
                linearLayout.addView(textView, new LinearLayout.LayoutParams(-2, -2));
            }
            new b(mapView, routeMessageHandler, routeOverlay, i, 4).a(linearLayout);
            return linearLayout;
        }

        /* access modifiers changed from: protected */
        public Drawable f(int i) {
            if (i == Route.this.getStepCount() - 1) {
                return q.c;
            }
            if (i < Route.this.getStepCount() && (Route.this.getStep(i) instanceof BusSegment)) {
                return q.e;
            }
            if (i == 0) {
                return q.f;
            }
            if (i == Route.this.getStepCount()) {
                return q.g;
            }
            return null;
        }
    }

    abstract class e extends d {
        e() {
            super();
        }

        public Paint a(int i) {
            return q.m;
        }

        public String a() {
            String str;
            StringBuffer stringBuffer = new StringBuffer();
            String str2 = PoiTypeDef.All;
            int stepCount = Route.this.getStepCount();
            int i = 0;
            int i2 = 0;
            while (i < stepCount) {
                DriveWalkSegment driveWalkSegment = (DriveWalkSegment) Route.this.getStep(i);
                i2 += driveWalkSegment.getLength();
                if (com.amap.mapapi.core.d.a(driveWalkSegment.getRoadName()) || driveWalkSegment.getRoadName().equals(str2)) {
                    str = str2;
                } else {
                    if (!com.amap.mapapi.core.d.a(stringBuffer.toString())) {
                        stringBuffer.append(" -> ");
                    }
                    stringBuffer.append(driveWalkSegment.getRoadName());
                    str = driveWalkSegment.getRoadName();
                }
                i++;
                str2 = str;
            }
            if (!com.amap.mapapi.core.d.a(stringBuffer.toString())) {
                stringBuffer.append("\n");
            }
            stringBuffer.append(String.format("%s", "大约" + Route.a(i2)));
            return stringBuffer.toString();
        }

        public String b(int i) {
            String b = super.b(i);
            if (b != null) {
                return b;
            }
            String str = PoiTypeDef.All;
            DriveWalkSegment driveWalkSegment = (DriveWalkSegment) Route.this.getStep(i);
            if (!com.amap.mapapi.core.d.a(driveWalkSegment.getRoadName())) {
                str = driveWalkSegment.getRoadName() + " ";
            }
            return (str + driveWalkSegment.getActionDescription() + " ") + String.format("%s%s", "大约", Route.a(driveWalkSegment.getLength()));
        }

        public Spanned c(int i) {
            String str;
            Spanned c2 = super.c(i);
            if (c2 != null) {
                return c2;
            }
            DriveWalkSegment driveWalkSegment = (DriveWalkSegment) Route.this.getStep(i);
            if (com.amap.mapapi.core.d.a(driveWalkSegment.getRoadName()) || com.amap.mapapi.core.d.a(driveWalkSegment.getActionDescription())) {
                str = driveWalkSegment.getActionDescription() + driveWalkSegment.getRoadName();
            } else {
                str = driveWalkSegment.getActionDescription() + " --> " + driveWalkSegment.getRoadName();
            }
            return com.amap.mapapi.core.d.c((com.amap.mapapi.core.d.a(str, "#808080") + com.amap.mapapi.core.d.c()) + String.format("%s%s", "大约", Route.a(driveWalkSegment.getLength())));
        }
    }

    class c extends e {
        c() {
            super();
        }

        public Paint a(int i) {
            return q.k;
        }

        /* access modifiers changed from: protected */
        public Drawable f(int i) {
            return q.c;
        }
    }

    class b extends e {
        b() {
            super();
        }

        /* access modifiers changed from: protected */
        public Drawable f(int i) {
            return q.d;
        }
    }

    public Route(int i) {
        this.c = i;
        if (isBus(i)) {
            this.mHelper = new a();
        } else if (isDrive(i)) {
            this.mHelper = new b();
        } else if (isWalk(i)) {
            this.mHelper = new c();
        } else {
            throw new IllegalArgumentException("Unkown mode");
        }
    }

    public static boolean isDrive(int i) {
        return i >= 10 && i <= 13;
    }

    public static boolean isBus(int i) {
        return i >= 0 && i <= 4;
    }

    public static boolean isWalk(int i) {
        return false;
    }

    static String a(int i) {
        if (i > 10000) {
            return (i / 1000) + "公里";
        } else if (i > 1000) {
            return new DecimalFormat("##0.0").format((double) (((float) i) / 1000.0f)) + "公里";
        } else if (i > 100) {
            return ((i / 50) * 50) + "米";
        } else {
            int i2 = (i / 10) * 10;
            if (i2 == 0) {
                i2 = 10;
            }
            return i2 + "米";
        }
    }
}
