package com.amap.mapapi.offlinemap;

import android.util.Log;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/* compiled from: SiteFileFetch */
public class e extends Thread {
    f a = null;
    long[] b;
    long[] c;
    b[] d;
    long e;
    boolean f = true;
    boolean g = false;
    File h;
    DataOutputStream i;
    c j;
    g k;

    public e(f fVar, c cVar, g gVar) throws IOException {
        this.a = fVar;
        this.h = new File(fVar.b() + File.separator + fVar.c() + ".info");
        if (this.h.exists()) {
            this.f = false;
            d();
        } else {
            this.b = new long[fVar.d()];
            this.c = new long[fVar.d()];
        }
        this.j = cVar;
        this.k = gVar;
    }

    public void run() {
        try {
            if (this.f) {
                this.e = a();
                if (this.e == -1) {
                    h.a("File Length is not known!");
                } else if (this.e == -2) {
                    h.a("File is not access!");
                } else {
                    for (int i2 = 0; i2 < this.b.length; i2++) {
                        this.b[i2] = ((long) i2) * (this.e / ((long) this.b.length));
                    }
                    for (int i3 = 0; i3 < this.c.length - 1; i3++) {
                        this.c[i3] = this.b[i3 + 1];
                    }
                    this.c[this.c.length - 1] = this.e;
                }
            }
            this.d = new b[this.b.length];
            for (int i4 = 0; i4 < this.b.length; i4++) {
                this.d[i4] = new b(this.a.a(), this.a.b() + File.separator + this.a.c(), this.b[i4], this.c[i4], i4);
                h.a("Thread " + i4 + " , nStartPos = " + this.b[i4] + ", nEndPos = " + this.c[i4]);
                this.d[i4].start();
            }
            while (!this.g) {
                c();
                h.a(500);
                boolean z = true;
                int i5 = 0;
                while (true) {
                    if (i5 >= this.b.length) {
                        break;
                    } else if (!this.d[i5].e) {
                        z = false;
                        continue;
                        break;
                    } else {
                        i5++;
                    }
                }
                if (z) {
                    break;
                }
            }
            Log.e("file downloaded", "success");
            if (!this.g) {
                this.j.b(this.k);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public long a() {
        int i2;
        int i3 = -1;
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(this.a.a()).openConnection();
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode >= 400) {
                a(responseCode);
                return -2;
            }
            int i4 = 1;
            while (true) {
                String headerFieldKey = httpURLConnection.getHeaderFieldKey(i4);
                if (headerFieldKey == null) {
                    i2 = -1;
                    break;
                } else if (headerFieldKey.equals("Content-Length")) {
                    i2 = Integer.parseInt(httpURLConnection.getHeaderField(headerFieldKey));
                    break;
                } else {
                    i4++;
                }
            }
            i3 = i2;
            h.b(i3);
            return (long) i3;
        } catch (IOException e2) {
            e2.printStackTrace();
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    private void c() {
        try {
            this.i = new DataOutputStream(new FileOutputStream(this.h));
            this.i.writeInt(this.b.length);
            long j2 = 0;
            for (int i2 = 0; i2 < this.b.length; i2++) {
                if (i2 == 0) {
                    j2 += this.d[i2].b;
                } else {
                    j2 += this.d[i2].b - this.d[i2 - 1].c;
                }
                this.i.writeLong(this.d[i2].b);
                this.i.writeLong(this.d[i2].c);
            }
            this.i.close();
            if (this.e > 0) {
                this.j.a(this.k, 0, (int) ((j2 * 100) / this.e));
            }
        } catch (IOException e2) {
            e2.printStackTrace();
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    private void d() {
        try {
            DataInputStream dataInputStream = new DataInputStream(new FileInputStream(this.h));
            int readInt = dataInputStream.readInt();
            this.b = new long[readInt];
            this.c = new long[readInt];
            for (int i2 = 0; i2 < this.b.length; i2++) {
                this.b[i2] = dataInputStream.readLong();
                this.c[i2] = dataInputStream.readLong();
            }
            dataInputStream.close();
        } catch (IOException e2) {
            e2.printStackTrace();
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    private void a(int i2) {
        System.err.println("Error Code : " + i2);
    }

    public void b() {
        this.g = true;
        for (int i2 = 0; i2 < this.b.length; i2++) {
            this.d[i2].a();
        }
    }
}
