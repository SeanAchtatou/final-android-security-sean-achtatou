package com.amap.mapapi.offlinemap;

public class City implements Comparable {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;

    public City(String str, String str2, String str3, String str4, String str5) {
        setProvince(str);
        setCity(str2);
        setInitial(str4);
        this.c = str3;
        setPinyin(str5);
    }

    public void setProvince(String str) {
        this.a = str;
    }

    public String getProvince() {
        return this.a;
    }

    public void setCity(String str) {
        this.b = str;
    }

    public String getCity() {
        return this.b;
    }

    public void setInitial(String str) {
        this.d = str;
    }

    public String getInitial() {
        return this.d;
    }

    public void setCode(String str) {
        this.c = str;
    }

    public String getCode() {
        return this.c;
    }

    public int compareTo(Object obj) {
        String str = ((City) obj).d;
        if (str.charAt(0) > this.d.charAt(0)) {
            return -1;
        }
        if (str.charAt(0) < this.d.charAt(0)) {
            return 1;
        }
        return 0;
    }

    public void setPinyin(String str) {
        this.e = str;
    }

    public String getPinyin() {
        return this.e;
    }
}
