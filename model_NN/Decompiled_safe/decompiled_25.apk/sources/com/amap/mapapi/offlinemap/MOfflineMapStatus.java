package com.amap.mapapi.offlinemap;

public class MOfflineMapStatus {
    public static final int ERROR = -1;
    public static final int LOADING = 0;
    public static final int PAUSE = 3;
    public static final int STOP = 5;
    public static final int SUCCESS = 4;
    public static final int UNZIP = 1;
    public static final int WAITING = 2;
}
