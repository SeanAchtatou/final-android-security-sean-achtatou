package com.amap.mapapi.offlinemap;

import android.os.Handler;
import android.os.Message;
import com.amap.mapapi.location.LocationManagerProxy;

/* compiled from: OfflineMapManager */
class d extends Handler {
    final /* synthetic */ OfflineMapManager a;

    d(OfflineMapManager offlineMapManager) {
        this.a = offlineMapManager;
    }

    public void handleMessage(Message message) {
        this.a.b.onDownload(message.getData().getInt(LocationManagerProxy.KEY_STATUS_CHANGED), message.getData().getInt("completepercent"));
    }
}
