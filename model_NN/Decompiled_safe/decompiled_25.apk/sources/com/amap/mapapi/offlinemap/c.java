package com.amap.mapapi.offlinemap;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import com.amap.mapapi.core.w;
import com.amap.mapapi.location.LocationManagerProxy;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: MapDownloadManager */
public class c {
    private static String g = null;
    public ArrayList<g> a = new ArrayList<>();
    public ArrayList<g> b = new ArrayList<>();
    public ArrayList<i> c = new ArrayList<>();
    public ArrayList<City> d = new ArrayList<>();
    Handler e;
    e f;
    private Context h = null;

    public static String a() {
        return Environment.getExternalStorageDirectory() + "/amap/VMAP/";
    }

    public c(Context context, Handler handler) {
        this.h = context;
        this.e = handler;
        g = c();
    }

    private String c() {
        if (!Environment.getExternalStorageState().equals("mounted")) {
            return this.h.getCacheDir().toString() + "/";
        }
        File file = new File(Environment.getExternalStorageDirectory(), "amap");
        if (!file.exists()) {
            file.mkdir();
        }
        File file2 = new File(file, "mini_mapv2");
        if (!file2.exists()) {
            file2.mkdir();
        }
        File file3 = new File(file2, "vmap");
        if (!file3.exists()) {
            file3.mkdir();
        }
        return file3.toString() + "/";
    }

    public void a(int i) {
        if (this.b != null && this.b.size() > 0) {
            g gVar = this.b.get(i);
            try {
                this.f = new e(new f(gVar.c(), a(), (gVar.b() + ".zip") + ".tmp", 5), this, gVar);
                this.f.start();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            a(gVar, 0, 0);
        }
    }

    public void b(int i) {
        if (this.f != null) {
            this.f.b();
            Bundle bundle = new Bundle();
            bundle.putInt(LocationManagerProxy.KEY_STATUS_CHANGED, 3);
            bundle.putInt("completepercent", 100);
            Message obtainMessage = this.e.obtainMessage();
            obtainMessage.setData(bundle);
            this.e.sendMessage(obtainMessage);
        }
    }

    public void a(g gVar) {
        gVar.a = 3;
        synchronized (this.b) {
            this.b.remove(gVar);
        }
        synchronized (this.a) {
            this.a.remove(gVar);
        }
        b(gVar.d());
        String a2 = gVar.a();
        String str = a2 + ".dt";
        if (a2 != null && a2.length() > 0) {
            try {
                new File(a2).delete();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        if (str != null && str.length() > 0) {
            try {
                new File(str).delete();
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
        String str2 = a2 + ".info";
        if (str2 != null && str2.length() > 0) {
            try {
                new File(str2).delete();
            } catch (Exception e4) {
                e4.printStackTrace();
            }
        }
    }

    public void b() {
        if (this.f != null) {
            this.f.b();
            Bundle bundle = new Bundle();
            bundle.putInt(LocationManagerProxy.KEY_STATUS_CHANGED, 5);
            bundle.putInt("completepercent", 100);
            Message obtainMessage = this.e.obtainMessage();
            obtainMessage.setData(bundle);
            this.e.sendMessage(obtainMessage);
            this.b.clear();
            if (this.f != null) {
                this.f.b();
            }
        }
    }

    public void a(JSONObject jSONObject) {
        String str;
        long j;
        String str2;
        String str3;
        String str4;
        this.c.clear();
        if (jSONObject != null) {
            try {
                JSONArray jSONArray = jSONObject.getJSONArray("maps");
                if (jSONArray != null) {
                    int length = jSONArray.length();
                    for (int i = 0; i < length; i++) {
                        JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                        String str5 = PoiTypeDef.All;
                        String str6 = PoiTypeDef.All;
                        String str7 = PoiTypeDef.All;
                        try {
                            str5 = jSONObject2.getString("name");
                        } catch (Exception e2) {
                        }
                        try {
                            str6 = jSONObject2.getString("citycode");
                        } catch (Exception e3) {
                        }
                        try {
                            str = jSONObject2.getString("durl");
                        } catch (Exception e4) {
                            str = PoiTypeDef.All;
                        }
                        try {
                            j = jSONObject2.getLong("size");
                        } catch (Exception e5) {
                            j = 0;
                        }
                        try {
                            str7 = jSONObject2.getString("pinyin");
                        } catch (Exception e6) {
                        }
                        try {
                            str2 = jSONObject2.getString("version");
                        } catch (Exception e7) {
                            str2 = PoiTypeDef.All;
                        }
                        try {
                            str4 = jSONObject2.getString("jianpin");
                        } catch (Exception e8) {
                            str4 = str3;
                        }
                        if (str6.length() > 0 && str5.length() > 0 && str.length() > 0 && str7.length() > 0 && j > 0) {
                            i iVar = new i(PoiTypeDef.All, str5, str6, str7.substring(0, 1), str7);
                            iVar.a = str;
                            iVar.b = j;
                            iVar.d = str2;
                            iVar.e = str4;
                            this.d.add(iVar);
                            this.c.add(iVar);
                        }
                    }
                    Collections.sort(this.c);
                    Collections.sort(this.d);
                }
            } catch (Exception e9) {
                e9.toString();
            }
        }
    }

    public void a(g gVar, int i, int i2) {
        Bundle bundle = new Bundle();
        bundle.putInt(LocationManagerProxy.KEY_STATUS_CHANGED, i);
        bundle.putInt("completepercent", i2);
        Message obtainMessage = this.e.obtainMessage();
        obtainMessage.setData(bundle);
        this.e.sendMessage(obtainMessage);
        gVar.a = i;
        gVar.e();
    }

    public void b(g gVar) {
        String str = a() + gVar.b() + ".zip";
        gVar.e();
        File file = new File(str);
        new File(str + ".tmp").renameTo(file);
        Bundle bundle = new Bundle();
        bundle.putInt(LocationManagerProxy.KEY_STATUS_CHANGED, 1);
        bundle.putInt("completepercent", 100);
        Message obtainMessage = this.e.obtainMessage();
        obtainMessage.setData(bundle);
        this.e.sendMessage(obtainMessage);
        gVar.a = 1;
        gVar.e();
        if (file.exists()) {
            w.a(g, str);
            new File(str).delete();
            Bundle bundle2 = new Bundle();
            bundle2.putInt(LocationManagerProxy.KEY_STATUS_CHANGED, 4);
            bundle2.putInt("completepercent", 100);
            Message obtainMessage2 = this.e.obtainMessage();
            obtainMessage.setData(bundle2);
            this.e.sendMessage(obtainMessage2);
        }
        gVar.a = 4;
        synchronized (this.b) {
            this.b.remove(gVar);
        }
        gVar.e();
    }
}
