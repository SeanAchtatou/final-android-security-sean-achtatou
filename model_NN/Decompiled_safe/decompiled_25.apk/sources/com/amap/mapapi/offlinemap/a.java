package com.amap.mapapi.offlinemap;

import com.amap.mapapi.poisearch.PoiTypeDef;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

/* compiled from: FileAccessI */
public class a {
    RandomAccessFile a;
    long b;

    public a() throws IOException {
        this(PoiTypeDef.All, 0);
    }

    public a(String str, long j) throws IOException {
        File file = new File(str);
        if (!file.exists()) {
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            try {
                if (!file.exists()) {
                    file.createNewFile();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.a = new RandomAccessFile(str, "rw");
        this.b = j;
        this.a.seek(j);
    }

    public synchronized int a(byte[] bArr, int i, int i2) {
        try {
            this.a.write(bArr, i, i2);
        } catch (IOException e) {
            e.printStackTrace();
            i2 = -1;
        }
        return i2;
    }
}
