package com.amap.mapapi.offlinemap;

import android.content.Context;
import android.os.Handler;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONObject;

public final class OfflineMapManager {
    c a;
    OfflineMapDownloadListener b;
    ArrayList<City> c = new ArrayList<>();
    Handler d = new d(this);

    public interface OfflineMapDownloadListener {
        void onDownload(int i, int i2);
    }

    public OfflineMapManager(Context context, OfflineMapDownloadListener offlineMapDownloadListener) {
        this.a = new c(context, this.d);
        this.b = offlineMapDownloadListener;
        a(context);
    }

    private void a(Context context) {
        try {
            InputStreamReader inputStreamReader = new InputStreamReader(context.getAssets().open("1010.png"));
            StringBuffer stringBuffer = new StringBuffer();
            char[] cArr = new char[1024];
            while (inputStreamReader.read(cArr) > 0) {
                stringBuffer.append(cArr);
            }
            inputStreamReader.close();
            this.a.a(new JSONObject(stringBuffer.toString()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean downloadByCityCode(String str) {
        i itemByCityCode = getItemByCityCode(str);
        if (itemByCityCode == null) {
            return false;
        }
        a(itemByCityCode);
        return true;
    }

    public boolean downloadByCityName(String str) {
        i itemByCityName = getItemByCityName(str);
        if (itemByCityName == null) {
            return false;
        }
        a(itemByCityName);
        return true;
    }

    private void a(i iVar) {
        this.c.add(iVar);
        g gVar = new g(iVar);
        gVar.a(this.a.b.size());
        gVar.a = 2;
        this.a.b.add(gVar);
        this.a.a(this.a.b.size() - 1);
    }

    public i getItemByCityCode(String str) {
        Iterator<i> it = this.a.c.iterator();
        while (it.hasNext()) {
            i next = it.next();
            if (next.getCode().equals(str)) {
                return next;
            }
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:3:0x000e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.amap.mapapi.offlinemap.i getItemByCityName(java.lang.String r5) {
        /*
            r4 = this;
            com.amap.mapapi.offlinemap.c r0 = r4.a
            java.util.ArrayList<com.amap.mapapi.offlinemap.i> r0 = r0.c
            java.util.Iterator r1 = r0.iterator()
        L_0x0008:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0025
            java.lang.Object r0 = r1.next()
            com.amap.mapapi.offlinemap.i r0 = (com.amap.mapapi.offlinemap.i) r0
            java.lang.String r2 = r0.getCity()
            boolean r3 = r2.contains(r5)
            if (r3 != 0) goto L_0x0024
            boolean r2 = r5.contains(r2)
            if (r2 == 0) goto L_0x0008
        L_0x0024:
            return r0
        L_0x0025:
            r0 = 0
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.mapapi.offlinemap.OfflineMapManager.getItemByCityName(java.lang.String):com.amap.mapapi.offlinemap.i");
    }

    public void pause() {
        this.a.b(0);
    }

    public void remove(String str) {
        i itemByCityName = getItemByCityName(str);
        if (itemByCityName != null) {
            this.a.a(new g(itemByCityName));
        }
    }

    public ArrayList<City> getOfflineCityList() {
        return this.a.d;
    }

    public ArrayList<City> getDownloadingCityList() {
        return this.c;
    }

    public void getUpdateInfo(String str) {
    }

    public void stop() {
        this.a.b();
    }

    public void restart() {
        this.a.a(this.a.b.size() - 1);
    }
}
