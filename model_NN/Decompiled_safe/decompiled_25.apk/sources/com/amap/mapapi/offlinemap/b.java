package com.amap.mapapi.offlinemap;

import java.io.IOException;

/* compiled from: FileSplitterFetch */
public class b extends Thread {
    String a;
    long b;
    long c;
    int d;
    boolean e = false;
    boolean f = false;
    a g = null;

    public b(String str, String str2, long j, long j2, int i) throws IOException {
        this.a = str;
        this.b = j;
        this.c = j2;
        this.d = i;
        this.g = new a(str2, this.b);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        com.amap.mapapi.offlinemap.h.a("Thread " + r7.d + " is over!");
        r7.e = true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r7 = this;
        L_0x0000:
            long r0 = r7.b
            long r2 = r7.c
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 >= 0) goto L_0x009c
            boolean r0 = r7.f
            if (r0 != 0) goto L_0x009c
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x0074 }
            java.lang.String r1 = r7.a     // Catch:{ Exception -> 0x0074 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0074 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x0074 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0074 }
            java.lang.String r1 = "GET"
            r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x0074 }
            java.lang.String r1 = "Content-Type"
            java.lang.String r2 = "text/xml;"
            r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x0074 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0074 }
            r1.<init>()     // Catch:{ Exception -> 0x0074 }
            java.lang.String r2 = "bytes="
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0074 }
            long r2 = r7.b     // Catch:{ Exception -> 0x0074 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0074 }
            java.lang.String r2 = "-"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0074 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0074 }
            java.lang.String r2 = "RANGE"
            r0.setRequestProperty(r2, r1)     // Catch:{ Exception -> 0x0074 }
            com.amap.mapapi.offlinemap.h.a(r1)     // Catch:{ Exception -> 0x0074 }
            java.io.InputStream r0 = r0.getInputStream()     // Catch:{ Exception -> 0x0074 }
            r1 = 1024(0x400, float:1.435E-42)
            byte[] r1 = new byte[r1]     // Catch:{ Exception -> 0x0074 }
        L_0x0050:
            r2 = 0
            r3 = 1024(0x400, float:1.435E-42)
            int r2 = r0.read(r1, r2, r3)     // Catch:{ Exception -> 0x0074 }
            if (r2 <= 0) goto L_0x0079
            long r3 = r7.b     // Catch:{ Exception -> 0x0074 }
            long r5 = r7.c     // Catch:{ Exception -> 0x0074 }
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 >= 0) goto L_0x0079
            boolean r3 = r7.f     // Catch:{ Exception -> 0x0074 }
            if (r3 != 0) goto L_0x0079
            long r3 = r7.b     // Catch:{ Exception -> 0x0074 }
            com.amap.mapapi.offlinemap.a r5 = r7.g     // Catch:{ Exception -> 0x0074 }
            r6 = 0
            int r2 = r5.a(r1, r6, r2)     // Catch:{ Exception -> 0x0074 }
            long r5 = (long) r2     // Catch:{ Exception -> 0x0074 }
            long r2 = r3 + r5
            r7.b = r2     // Catch:{ Exception -> 0x0074 }
            goto L_0x0050
        L_0x0074:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0000
        L_0x0079:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0074 }
            r0.<init>()     // Catch:{ Exception -> 0x0074 }
            java.lang.String r1 = "Thread "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0074 }
            int r1 = r7.d     // Catch:{ Exception -> 0x0074 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0074 }
            java.lang.String r1 = " is over!"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0074 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0074 }
            com.amap.mapapi.offlinemap.h.a(r0)     // Catch:{ Exception -> 0x0074 }
            r0 = 1
            r7.e = r0     // Catch:{ Exception -> 0x0074 }
            goto L_0x0000
        L_0x009c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.mapapi.offlinemap.b.run():void");
    }

    public void a() {
        this.f = true;
    }
}
