package com.amap.mapapi.poisearch;

import com.amap.mapapi.core.AMapException;
import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.core.PoiItem;
import com.amap.mapapi.core.d;
import com.amap.mapapi.core.i;
import com.amap.mapapi.core.t;
import com.amap.mapapi.core.u;
import com.amap.mapapi.poisearch.PoiSearch;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.Proxy;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/* compiled from: PoiSearchServerHandler */
public class a extends t<b, PoiItem> {
    private int i = 1;
    private int j = 20;
    private int k = 0;
    private ArrayList<String> l = new ArrayList<>();

    public a(b bVar, Proxy proxy, String str, String str2) {
        super(bVar, proxy, str, str2);
    }

    public void a(int i2) {
        this.i = i2;
    }

    public void b(int i2) {
        int i3;
        int i4 = 20;
        if (i2 > 20) {
            i3 = 20;
        } else {
            i3 = i2;
        }
        if (i3 > 0) {
            i4 = i3;
        }
        this.j = i4;
    }

    public int a() {
        return this.j;
    }

    public int b() {
        return this.i;
    }

    public int c() {
        return this.k;
    }

    public PoiSearch.Query d() {
        return ((b) this.b).a;
    }

    public PoiSearch.SearchBound l() {
        return ((b) this.b).b;
    }

    public List<String> m() {
        return this.l;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amap.mapapi.core.u.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.amap.mapapi.core.u.a(java.lang.String, java.lang.String):java.lang.String
      com.amap.mapapi.core.u.a(java.lang.String, java.lang.Object):void */
    /* access modifiers changed from: protected */
    public String[] f() {
        if (((b) this.b).b == null) {
            String[] strArr = new String[6];
            String city = ((b) this.b).a.getCity();
            if (a(city)) {
                strArr[0] = "&cityCode=total";
            } else {
                try {
                    strArr[0] = "&cityCode=" + URLEncoder.encode(city, "utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            String queryString = ((b) this.b).a.getQueryString();
            try {
                queryString = URLEncoder.encode(queryString, "utf-8");
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace();
            }
            strArr[1] = "&searchName=" + queryString;
            String category = ((b) this.b).a.getCategory();
            try {
                category = URLEncoder.encode(category, "utf-8");
            } catch (UnsupportedEncodingException e3) {
                e3.printStackTrace();
            }
            strArr[2] = "&searchType=" + category;
            strArr[3] = "&number=" + (PoiTypeDef.All + this.j);
            strArr[4] = "&batch=" + (PoiTypeDef.All + this.i);
            strArr[5] = "&enc=utf-8";
            return strArr;
        }
        PoiSearch.SearchBound searchBound = ((b) this.b).b;
        if (PoiSearch.SearchBound.BOUND_SHAPE.equals(searchBound.getShape())) {
            String[] strArr2 = new String[9];
            strArr2[0] = "&cityCode=total";
            String queryString2 = ((b) this.b).a.getQueryString();
            try {
                queryString2 = URLEncoder.encode(queryString2, "utf-8");
            } catch (UnsupportedEncodingException e4) {
                e4.printStackTrace();
            }
            strArr2[1] = "&searchName=" + queryString2;
            String category2 = ((b) this.b).a.getCategory();
            try {
                category2 = URLEncoder.encode(category2, "utf-8");
            } catch (UnsupportedEncodingException e5) {
                e5.printStackTrace();
            }
            strArr2[2] = "&searchType=" + category2;
            strArr2[3] = "&cenX=" + (PoiTypeDef.All + ((((float) ((b) this.b).b.getCenter().a()) * 1.0f) / 1000000.0f));
            strArr2[4] = "&cenY=" + (PoiTypeDef.All + ((((float) ((b) this.b).b.getCenter().b()) * 1.0f) / 1000000.0f));
            strArr2[5] = "&number=" + (PoiTypeDef.All + this.j);
            strArr2[6] = "&batch=" + (PoiTypeDef.All + this.i);
            strArr2[7] = "&enc=utf-8";
            strArr2[8] = "&range=" + ((b) this.b).b.getRange();
            return strArr2;
        } else if (!PoiSearch.SearchBound.RECTANGLE_SHAPE.equals(searchBound.getShape())) {
            return null;
        } else {
            String queryString3 = ((b) this.b).a.getQueryString();
            String category3 = ((b) this.b).a.getCategory();
            GeoPoint lowerLeft = searchBound.getLowerLeft();
            GeoPoint upperRight = searchBound.getUpperRight();
            double a = d.a(lowerLeft.b());
            double a2 = d.a(lowerLeft.a());
            double a3 = d.a(upperRight.b());
            double a4 = d.a(upperRight.a());
            String valueOf = String.valueOf(this.i);
            String valueOf2 = String.valueOf(this.j);
            u uVar = new u("spatial_request");
            uVar.a("method", (Object) "searchPoiInGeoObject");
            u uVar2 = new u("searchName");
            uVar2.a(queryString3);
            uVar.a(uVar2);
            u uVar3 = new u("searchType");
            uVar3.a(category3);
            uVar.a(uVar3);
            u uVar4 = new u("pageNum");
            uVar4.a(valueOf2);
            uVar.a(uVar4);
            u uVar5 = new u("batch");
            uVar5.a(valueOf);
            uVar.a(uVar5);
            u uVar6 = new u("spatial_geos");
            u uVar7 = new u("spatial_geo");
            uVar7.a(com.umeng.common.a.b, (Object) searchBound.getShape());
            u uVar8 = new u("bounds");
            uVar8.a(a2 + ";" + a + ";" + a4 + ";" + a3);
            uVar7.a(uVar8);
            u uVar9 = new u("buffer");
            uVar9.a((Object) 0);
            uVar7.a(uVar9);
            uVar6.a(uVar7);
            uVar.a(uVar6);
            String a5 = uVar.a();
            try {
                a5 = URLEncoder.encode(a5, "utf-8");
            } catch (UnsupportedEncodingException e6) {
                e6.printStackTrace();
            }
            String[] strArr3 = new String[4];
            strArr3[0] = "&enc=utf-8";
            strArr3[1] = "&spatialXml=" + a5;
            strArr3[2] = "&enc=utf-8";
            return strArr3;
        }
    }

    private boolean a(String str) {
        if (str == null || str.equals(PoiTypeDef.All)) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public NodeList b(InputStream inputStream) throws AMapException {
        if (((b) this.b).b == null || !((b) this.b).b.getShape().equals(PoiSearch.SearchBound.RECTANGLE_SHAPE)) {
            return super.b(inputStream);
        }
        try {
            Element documentElement = d.b(d(inputStream)).getDocumentElement();
            if (documentElement.getNodeName().equals("searchresult")) {
                return documentElement.getChildNodes();
            }
            return documentElement.getFirstChild().getChildNodes();
        } catch (Exception e) {
            throw new AMapException(e.getMessage());
        }
    }

    /* access modifiers changed from: protected */
    public void a(ArrayList<PoiItem> arrayList) {
    }

    /* access modifiers changed from: protected */
    public void a(Node node, ArrayList<PoiItem> arrayList) {
        if (node.getNodeType() == 1) {
            int b = b();
            String nodeName = node.getNodeName();
            if (nodeName.equals("count") && b == 1) {
                b(node);
            }
            if (nodeName.equals("pinyin") && b == 1) {
                c(node);
            }
            if (nodeName.equals("list")) {
                NodeList childNodes = node.getChildNodes();
                for (int i2 = 0; i2 < childNodes.getLength(); i2++) {
                    Node item = childNodes.item(i2);
                    if (item.getNodeType() == 1 && item.getNodeName().equals("poi")) {
                        C0002a aVar = new C0002a();
                        a(item, aVar);
                        arrayList.add(aVar.a());
                    }
                }
            }
        }
    }

    private void b(Node node) {
        this.k = Integer.parseInt(a(node));
    }

    private void c(Node node) {
        NodeList childNodes = node.getChildNodes();
        int length = childNodes.getLength();
        for (int i2 = 0; i2 < length; i2++) {
            Node item = childNodes.item(i2);
            if (item.getNodeType() == 1 && item.getNodeName().equals("list")) {
                NodeList childNodes2 = item.getChildNodes();
                int length2 = childNodes2.getLength();
                for (int i3 = 0; i3 < length2; i3++) {
                    Node item2 = childNodes2.item(i3);
                    if (item2.getNodeType() == 1 && item2.getNodeName().equals("data")) {
                        this.l.add(a(item2));
                    }
                }
            }
        }
    }

    private void a(Node node, C0002a aVar) {
        NodeList childNodes = node.getChildNodes();
        for (int i2 = 0; i2 < childNodes.getLength(); i2++) {
            Node item = childNodes.item(i2);
            if (item.getNodeType() == 1) {
                try {
                    a(aVar, item.getNodeName(), a(item));
                } catch (Exception e) {
                }
            }
        }
    }

    private void a(C0002a aVar, String str, String str2) {
        if (str.equals("name")) {
            aVar.b = str2;
        } else if (str.equals("pguid")) {
            aVar.a = str2;
        } else if (str.equals("newtype")) {
            aVar.c = str2.substring(0, 4);
        } else if (str.equals(com.umeng.common.a.b)) {
            String[] split = str2.split(";");
            aVar.d = split[0] + PoiItem.DesSplit + split[1];
        } else if (str.equals("address")) {
            aVar.e = str2;
        } else if (str.equals("tel")) {
            aVar.f = str2;
        } else if (str.equals("code")) {
            aVar.g = str2;
        } else if (str.equals("x")) {
            aVar.h = Double.parseDouble(str2);
        } else if (str.equals("y")) {
            aVar.i = Double.parseDouble(str2);
        } else if (str.equals("xml")) {
            aVar.j = str2;
        }
    }

    /* renamed from: com.amap.mapapi.poisearch.a$a  reason: collision with other inner class name */
    /* compiled from: PoiSearchServerHandler */
    static class C0002a {
        String a;
        String b;
        String c;
        String d;
        String e;
        String f;
        String g;
        double h;
        double i;
        String j;

        C0002a() {
        }

        public PoiItem a() {
            PoiItem poiItem = new PoiItem(this.a, new GeoPoint(d.a(this.i), d.a(this.h)), this.b, this.e);
            poiItem.setAdCode(this.g);
            poiItem.setTel(this.f);
            poiItem.setTypeCode(this.c);
            poiItem.setTypeDes(this.d);
            poiItem.setXmlNode(this.j);
            return poiItem;
        }
    }

    /* access modifiers changed from: protected */
    public String h() {
        String str;
        if (((b) this.b).b == null) {
            str = i.a().d() + "?&config=BESN&resType=xml";
        } else {
            PoiSearch.SearchBound searchBound = ((b) this.b).b;
            if (searchBound.getShape().equals(PoiSearch.SearchBound.BOUND_SHAPE)) {
                str = i.a().d() + "?&config=BELSBXY&resType=xml";
            } else if (searchBound.getShape().equals(PoiSearch.SearchBound.RECTANGLE_SHAPE)) {
                str = i.a().d() + "?&config=SPAS&resType=xml";
            } else {
                str = null;
            }
        }
        return e() ? str : i.a().d();
    }

    /* renamed from: a */
    public ArrayList<PoiItem> c(InputStream inputStream) throws AMapException {
        String str;
        JSONArray jSONArray;
        JSONObject jSONObject;
        if (e()) {
            return super.c(inputStream);
        }
        ArrayList<PoiItem> arrayList = new ArrayList<>();
        try {
            str = new String(com.amap.mapapi.map.i.a(inputStream));
        } catch (Exception e) {
            e.printStackTrace();
            str = null;
        }
        try {
            JSONObject jSONObject2 = new JSONObject(str);
            if (((b) this.b).b == null || !((b) this.b).b.getShape().equals(PoiSearch.SearchBound.RECTANGLE_SHAPE)) {
                jSONArray = jSONObject2.getJSONArray("poilist");
                jSONObject = jSONObject2;
            } else {
                JSONObject jSONObject3 = jSONObject2.getJSONObject("searchresult");
                jSONArray = jSONObject3.getJSONArray("list");
                jSONObject = jSONObject3;
            }
            this.k = jSONObject.getInt("count");
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                JSONObject jSONObject4 = jSONArray.getJSONObject(i2);
                String string = jSONObject4.getString("pguid");
                double d = jSONObject4.getDouble("x");
                PoiItem poiItem = new PoiItem(string, new GeoPoint(d.a(jSONObject4.getDouble("y")), d.a(d)), jSONObject4.getString("name"), jSONObject4.getString("address"));
                poiItem.setAdCode(jSONObject4.getString("code"));
                poiItem.setTel(jSONObject4.getString("tel"));
                poiItem.setTypeCode(jSONObject4.getString("newtype").substring(0, 4));
                String[] split = jSONObject4.getString(com.umeng.common.a.b).split(";");
                String str2 = split[0];
                for (int i3 = 1; i3 < split.length; i3++) {
                    str2 = str2 + PoiItem.DesSplit + split[i3];
                }
                poiItem.setTypeDes(str2);
                poiItem.setXmlNode(jSONObject4.getString("xml"));
                arrayList.add(poiItem);
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public boolean e() {
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amap.mapapi.core.u.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.amap.mapapi.core.u.a(java.lang.String, java.lang.String):java.lang.String
      com.amap.mapapi.core.u.a(java.lang.String, java.lang.Object):void */
    /* access modifiers changed from: protected */
    public byte[] g() {
        StringBuilder sb = new StringBuilder();
        if (((b) this.b).b == null) {
            sb.append("config=BESN&cityCode=");
            String city = ((b) this.b).a.getCity();
            if (a(city)) {
                sb.append("total");
            } else {
                try {
                    city = URLEncoder.encode(city, "utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                sb.append(city);
            }
            String queryString = ((b) this.b).a.getQueryString();
            try {
                queryString = URLEncoder.encode(queryString, "utf-8");
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace();
            }
            sb.append("&searchName=");
            sb.append(queryString);
            String category = ((b) this.b).a.getCategory();
            try {
                category = URLEncoder.encode(category, "utf-8");
            } catch (UnsupportedEncodingException e3) {
                e3.printStackTrace();
            }
            sb.append("&searchType=");
            sb.append(category);
            sb.append("&number=");
            sb.append(this.j);
            sb.append("&batch=");
            sb.append(this.i);
        } else if (((b) this.b).b.getShape().equals(PoiSearch.SearchBound.BOUND_SHAPE)) {
            sb.append("config=BELSBXY&cityCode=total");
            String queryString2 = ((b) this.b).a.getQueryString();
            try {
                queryString2 = URLEncoder.encode(queryString2, "utf-8");
            } catch (UnsupportedEncodingException e4) {
                e4.printStackTrace();
            }
            sb.append("&searchName=");
            sb.append(queryString2);
            String category2 = ((b) this.b).a.getCategory();
            try {
                category2 = URLEncoder.encode(category2, "utf-8");
            } catch (UnsupportedEncodingException e5) {
                e5.printStackTrace();
            }
            sb.append("&searchType=");
            sb.append(category2);
            sb.append("&number=");
            sb.append(this.j);
            sb.append("&batch=");
            sb.append(this.i);
            sb.append("&cenX=");
            sb.append((((float) ((b) this.b).b.getCenter().a()) * 1.0f) / 1000000.0f);
            sb.append("&cenY=");
            sb.append((((float) ((b) this.b).b.getCenter().b()) * 1.0f) / 1000000.0f);
            sb.append("&range=");
            sb.append(((b) this.b).b.getRange());
        } else if (((b) this.b).b.getShape().equals(PoiSearch.SearchBound.RECTANGLE_SHAPE)) {
            sb.append("config=SPAS");
            GeoPoint lowerLeft = ((b) this.b).b.getLowerLeft();
            GeoPoint upperRight = ((b) this.b).b.getUpperRight();
            double a = d.a(lowerLeft.b());
            double a2 = d.a(lowerLeft.a());
            double a3 = d.a(upperRight.b());
            double a4 = d.a(upperRight.a());
            String queryString3 = ((b) this.b).a.getQueryString();
            String category3 = ((b) this.b).a.getCategory();
            u uVar = new u("spatial_request");
            uVar.a("method", (Object) "searchPoiInGeoObject");
            u uVar2 = new u("searchName");
            uVar2.a(queryString3);
            uVar.a(uVar2);
            u uVar3 = new u("searchType");
            uVar3.a(category3);
            uVar.a(uVar3);
            u uVar4 = new u("pageNum");
            uVar4.a(String.valueOf(this.j));
            uVar.a(uVar4);
            u uVar5 = new u("batch");
            uVar5.a(String.valueOf(this.i));
            uVar.a(uVar5);
            u uVar6 = new u("spatial_geos");
            u uVar7 = new u("spatial_geo");
            uVar7.a(com.umeng.common.a.b, (Object) ((b) this.b).b.getShape());
            u uVar8 = new u("bounds");
            uVar8.a(a2 + ";" + a + ";" + a4 + ";" + a3);
            uVar7.a(uVar8);
            u uVar9 = new u("buffer");
            uVar9.a((Object) 0);
            uVar7.a(uVar9);
            uVar6.a(uVar7);
            uVar.a(uVar6);
            String a5 = uVar.a();
            try {
                a5 = URLEncoder.encode(a5, "utf-8");
            } catch (UnsupportedEncodingException e6) {
                e6.printStackTrace();
            }
            sb.append("&spatialXml=");
            sb.append(a5);
        }
        sb.append("&resType=json&enc=utf-8");
        com.amap.mapapi.core.a a6 = com.amap.mapapi.core.a.a(null);
        sb.append("&a_k=");
        sb.append(a6.a());
        return sb.toString().getBytes();
    }
}
