package com.amap.mapapi.poisearch;

import android.app.Activity;
import android.content.Context;
import com.amap.mapapi.core.AMapException;
import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.core.a;
import com.amap.mapapi.core.b;
import com.amap.mapapi.core.d;
import com.amap.mapapi.map.MapView;
import java.util.ArrayList;

public class PoiSearch {
    private SearchBound a;
    private Query b;
    private Context c;
    private int d = 20;

    public PoiSearch(Activity activity, Query query) {
        a.a(activity);
        this.c = activity;
        setQuery(query);
    }

    public PoiSearch(Context context, String str, Query query) {
        a.a(context);
        this.c = context;
        setQuery(query);
    }

    public PoiPagedResult searchPOI() throws AMapException {
        a aVar = new a(new b(this.b, this.a), d.b(this.c), d.a(this.c), null);
        aVar.a(1);
        aVar.b(this.d);
        return PoiPagedResult.a(aVar, (ArrayList) aVar.j());
    }

    public void setPageSize(int i) {
        this.d = i;
    }

    @Deprecated
    public void setPoiNumber(int i) {
        setPageSize(i);
    }

    public void setQuery(Query query) {
        this.b = query;
    }

    public void setBound(SearchBound searchBound) {
        this.a = searchBound;
    }

    public Query getQuery() {
        return this.b;
    }

    public SearchBound getBound() {
        return this.a;
    }

    public static class Query {
        private String a;
        private String b;
        private String c;

        public Query(String str, String str2) {
            this(str, str2, null);
        }

        public Query(String str, String str2, String str3) {
            this.a = str;
            this.b = str2;
            this.c = str3;
            if (!b()) {
                throw new IllegalArgumentException("Empty  query and catagory");
            }
        }

        private boolean b() {
            return !d.a(this.a) || !d.a(this.b);
        }

        public String getQueryString() {
            return this.a;
        }

        public String getCategory() {
            if (this.b == null || this.b.equals("00") || this.b.equals("00|")) {
                return a();
            }
            return this.b;
        }

        /* access modifiers changed from: package-private */
        public String a() {
            return PoiTypeDef.All;
        }

        public String getCity() {
            return this.c;
        }
    }

    public static class SearchBound {
        public static final String BOUND_SHAPE = "bound";
        public static final String ELLIPSE_SHAPE = "Ellipse";
        public static final String POLYGON_SHAPE = "Polygon";
        public static final String RECTANGLE_SHAPE = "Rectangle";
        private GeoPoint a;
        private GeoPoint b;
        private int c;
        private GeoPoint d;
        private String e;

        public SearchBound(GeoPoint geoPoint, int i) {
            this.e = BOUND_SHAPE;
            this.c = i;
            this.d = geoPoint;
            a(geoPoint, d.b(i), d.b(i));
        }

        public SearchBound(GeoPoint geoPoint, GeoPoint geoPoint2) {
            this.e = RECTANGLE_SHAPE;
            a(geoPoint, geoPoint2);
        }

        public SearchBound(MapView mapView) {
            this.e = RECTANGLE_SHAPE;
            a(mapView.getProjection().fromPixels(0, b.j), mapView.getProjection().fromPixels(b.i, 0));
        }

        private void a(GeoPoint geoPoint, GeoPoint geoPoint2) {
            this.a = geoPoint;
            this.b = geoPoint2;
            if (this.a.b() >= this.b.b() || this.a.a() >= this.b.a()) {
                throw new IllegalArgumentException("invalid rect ");
            }
            this.d = new GeoPoint((this.a.b() + this.b.b()) / 2, (this.a.a() + this.b.a()) / 2);
        }

        private void a(GeoPoint geoPoint, int i, int i2) {
            int i3 = i / 2;
            int i4 = i2 / 2;
            long b2 = geoPoint.b();
            long a2 = geoPoint.a();
            a(new GeoPoint(b2 - ((long) i3), a2 - ((long) i4)), new GeoPoint(b2 + ((long) i3), ((long) i4) + a2));
        }

        public GeoPoint getLowerLeft() {
            return this.a;
        }

        public GeoPoint getUpperRight() {
            return this.b;
        }

        public GeoPoint getCenter() {
            return this.d;
        }

        public int getLonSpanInMeter() {
            return d.a(this.b.getLongitudeE6() - this.a.getLongitudeE6());
        }

        public int getLatSpanInMeter() {
            return d.a(this.b.getLatitudeE6() - this.a.getLatitudeE6());
        }

        public int getRange() {
            return this.c;
        }

        public String getShape() {
            return this.e;
        }
    }
}
