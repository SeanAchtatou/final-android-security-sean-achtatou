package com.amap.mapapi.poisearch;

import com.amap.mapapi.core.AMapException;
import com.amap.mapapi.core.PoiItem;
import com.amap.mapapi.poisearch.PoiSearch;
import java.util.ArrayList;
import java.util.List;

public final class PoiPagedResult {
    private int a;
    private ArrayList<ArrayList<PoiItem>> b;
    private a c;

    static PoiPagedResult a(a aVar, ArrayList<PoiItem> arrayList) {
        return new PoiPagedResult(aVar, arrayList);
    }

    private PoiPagedResult(a aVar, ArrayList<PoiItem> arrayList) {
        this.c = aVar;
        this.a = a(aVar.c());
        a(arrayList);
    }

    private int a(int i) {
        int a2 = this.c.a();
        int i2 = ((i + a2) - 1) / a2;
        if (i2 > 30) {
            return 30;
        }
        return i2;
    }

    private void a(ArrayList<PoiItem> arrayList) {
        this.b = new ArrayList<>();
        for (int i = 0; i <= this.a; i++) {
            this.b.add(null);
        }
        if (this.a > 0) {
            this.b.set(1, arrayList);
        }
    }

    public int getPageCount() {
        return this.a;
    }

    public PoiSearch.Query getQuery() {
        return this.c.d();
    }

    public PoiSearch.SearchBound getBound() {
        return this.c.l();
    }

    private boolean b(int i) {
        return i <= this.a && i > 0;
    }

    public List<PoiItem> getPageLocal(int i) {
        if (b(i)) {
            return this.b.get(i);
        }
        throw new IllegalArgumentException("page out of range");
    }

    public List<PoiItem> getPage(int i) throws AMapException {
        if (this.a == 0) {
            return null;
        }
        if (!b(i)) {
            throw new IllegalArgumentException("page out of range");
        }
        ArrayList arrayList = (ArrayList) getPageLocal(i);
        if (arrayList != null) {
            return arrayList;
        }
        this.c.a(i);
        ArrayList arrayList2 = (ArrayList) this.c.j();
        this.b.set(i, arrayList2);
        return arrayList2;
    }

    public List<String> getSearchSuggestions() {
        return this.c.m();
    }
}
