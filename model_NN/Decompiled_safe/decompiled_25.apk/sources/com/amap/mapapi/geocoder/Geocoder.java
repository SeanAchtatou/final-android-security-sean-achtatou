package com.amap.mapapi.geocoder;

import android.content.Context;
import android.location.Address;
import com.amap.mapapi.core.AMapException;
import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.core.a;
import com.amap.mapapi.core.d;
import com.amap.mapapi.core.h;
import com.amap.mapapi.core.o;
import com.amap.mapapi.core.p;
import java.util.ArrayList;
import java.util.List;

public final class Geocoder {
    public static final String Cross = "Cross";
    public static final String POI = "POI";
    public static final String Street_Road = "StreetAndRoad";
    private String a;
    private Context b;

    public Geocoder(Context context) {
        a.a(context);
        a(context, d.a(context));
    }

    public Geocoder(Context context, String str) {
        a.a(context);
        a(context, d.a(context));
    }

    private void a(Context context, String str) {
        this.b = context;
        this.a = str;
    }

    public List<Address> getFromLocation(double d, double d2, int i) throws AMapException {
        return a(d, d2, i, false);
    }

    public List<Address> getFromRawGpsLocation(double d, double d2, int i) throws AMapException {
        double d3;
        double d4;
        try {
            GeoPoint.b bVar = (GeoPoint.b) new h(new GeoPoint.b(d2, d), d.b(this.b), this.a, null).j();
            if (bVar != null) {
                d3 = bVar.a;
                d4 = bVar.b;
            } else {
                d3 = d2;
                d4 = d;
            }
            if (Double.valueOf(0.0d).doubleValue() == d3 && Double.valueOf(0.0d).doubleValue() == d4) {
                return null;
            }
            return a(d4, d3, i, false);
        } catch (Exception e) {
            return new ArrayList();
        }
    }

    private List<Address> a(double d, double d2, int i, boolean z) throws AMapException {
        if (d.a) {
            if (d < d.a(1000000L) || d > d.a(65000000L)) {
                throw new AMapException("无效的参数 - IllegalArgumentException latitude == " + d);
            } else if (d2 < d.a(50000000L) || d2 > d.a(145000000L)) {
                throw new AMapException("无效的参数 - IllegalArgumentException longitude == " + d2);
            }
        }
        if (i <= 0) {
            return new ArrayList();
        }
        return (List) new o(new p(d2, d, i, z), d.b(this.b), this.a, null).j();
    }

    public List<Address> getFromLocationName(String str, int i, double d, double d2, double d3, double d4) throws AMapException {
        if (str == null) {
            throw new IllegalArgumentException("locationName == null");
        }
        if (d.a) {
            if (d < d.a(1000000L) || d > d.a(65000000L)) {
                throw new AMapException("无效的参数 - IllegalArgumentException lowerLeftLatitude == " + d);
            } else if (d2 < d.a(50000000L) || d2 > d.a(145000000L)) {
                throw new AMapException("无效的参数 - IllegalArgumentException lowerLeftLongitude == " + d2);
            } else if (d3 < d.a(1000000L) || d3 > d.a(65000000L)) {
                throw new AMapException("无效的参数 - IllegalArgumentException upperRightLatitude == " + d3);
            } else if (d4 < d.a(50000000L) || d4 > d.a(145000000L)) {
                throw new AMapException("无效的参数 - IllegalArgumentException upperRightLongitude == " + d4);
            }
        }
        if (i <= 0) {
            return new ArrayList();
        }
        List<Address> list = (List) new a(new b(str, 15), d.b(this.b), this.a, null).j();
        if (d.a) {
            return a(list, d, d2, d3, d4, i);
        }
        return list;
    }

    private List<Address> a(List<Address> list, double d, double d2, double d3, double d4, int i) {
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (Address next : list) {
            double longitude = next.getLongitude();
            double latitude = next.getLatitude();
            if (longitude <= d4 && longitude >= d2 && latitude <= d3 && latitude >= d && arrayList.size() < i) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public List<Address> getFromLocationName(String str, int i) throws AMapException {
        return getFromLocationName(str, i, d.a(1000000L), d.a(50000000L), d.a(65000000L), d.a(145000000L));
    }
}
