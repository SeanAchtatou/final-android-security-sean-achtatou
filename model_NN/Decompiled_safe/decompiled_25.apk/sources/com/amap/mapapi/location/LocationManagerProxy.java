package com.amap.mapapi.location;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import com.amap.mapapi.core.d;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

public class LocationManagerProxy {
    public static final String GPS_PROVIDER = "gps";
    public static final String KEY_LOCATION_CHANGED = "location";
    public static final String KEY_PROVIDER_ENABLED = "providerEnabled";
    public static final String KEY_PROXIMITY_ENTERING = "entering";
    public static final String KEY_STATUS_CHANGED = "status";
    public static final String NETWORK_PROVIDER = "network";
    private static LocationManagerProxy b = null;
    private LocationManager a = null;
    private a c = null;
    /* access modifiers changed from: private */
    public Context d;
    private c e;
    private b f;
    /* access modifiers changed from: private */
    public ArrayList<PendingIntent> g = new ArrayList<>();
    private Hashtable<String, LocationProviderProxy> h = new Hashtable<>();
    private String i;
    /* access modifiers changed from: private */
    public double j;
    /* access modifiers changed from: private */
    public double k;
    /* access modifiers changed from: private */
    public boolean l = false;
    /* access modifiers changed from: private */
    public long m = 0;
    /* access modifiers changed from: private */
    public double n = 0.0d;
    private c o;
    private a p;
    /* access modifiers changed from: private */
    public ArrayList<PendingIntent> q = new ArrayList<>();

    private LocationManagerProxy(Context context) {
        a(context);
    }

    private LocationManagerProxy(Activity activity) {
        com.amap.mapapi.core.a.a(activity);
        a(activity.getApplicationContext());
    }

    private void a(Context context) {
        this.d = context;
        this.a = (LocationManager) context.getSystemService(KEY_LOCATION_CHANGED);
        this.c = a.a(context.getApplicationContext(), this.a);
        Thread thread = new Thread(this.c);
        thread.setDaemon(true);
        thread.start();
    }

    public static synchronized LocationManagerProxy getInstance(Activity activity) {
        LocationManagerProxy locationManagerProxy;
        synchronized (LocationManagerProxy.class) {
            if (b == null) {
                b = new LocationManagerProxy(activity);
            } else {
                b.destory();
                b = new LocationManagerProxy(activity);
            }
            locationManagerProxy = b;
        }
        return locationManagerProxy;
    }

    public static synchronized LocationManagerProxy getInstance(Context context) {
        LocationManagerProxy locationManagerProxy;
        synchronized (LocationManagerProxy.class) {
            if (b == null) {
                b = new LocationManagerProxy(context);
            } else {
                b.destory();
                b = new LocationManagerProxy(context);
            }
            locationManagerProxy = b;
        }
        return locationManagerProxy;
    }

    public void addProximityAlert(double d2, double d3, float f2, long j2, PendingIntent pendingIntent) {
        if (LocationProviderProxy.MapABCNetwork.equals(this.i)) {
            if (this.o == null) {
                this.o = new c(this);
            }
            if (this.p == null) {
                this.p = new a();
            }
            this.o.a(this.p, 10000, f2, this.i);
            this.l = true;
            this.j = d2;
            this.k = d3;
            this.n = (double) f2;
            if (j2 != -1) {
                this.m = d.a() + j2;
            }
            this.q.add(pendingIntent);
        } else if (this.a != null) {
            this.a.addProximityAlert(d2, d3, f2, j2, pendingIntent);
        }
    }

    public void removeProximityAlert(PendingIntent pendingIntent) {
        if (LocationProviderProxy.MapABCNetwork.equals(this.i)) {
            if (this.o != null) {
                this.o.a();
            }
            this.q.remove(pendingIntent);
            this.o = null;
            this.l = false;
            this.m = 0;
            this.n = 0.0d;
            this.j = 0.0d;
            this.k = 0.0d;
        } else if (this.a != null) {
            this.a.removeProximityAlert(pendingIntent);
        }
    }

    public Location getLastKnownLocation(String str) {
        if (LocationProviderProxy.MapABCNetwork.equals(str)) {
            this.i = str;
            return this.c.b();
        } else if (this.a != null) {
            return this.a.getLastKnownLocation(str);
        } else {
            return null;
        }
    }

    public void requestLocationUpdates(String str, long j2, float f2, LocationListener locationListener) {
        this.i = str;
        if (LocationProviderProxy.MapABCNetwork.equals(str)) {
            this.c.a(j2, f2, locationListener);
        } else if (GPS_PROVIDER.equals(str)) {
            this.c.a(j2, f2, locationListener);
        } else {
            this.a.requestLocationUpdates(str, j2, f2, locationListener);
        }
    }

    public void removeUpdates(LocationListener locationListener) {
        if (locationListener != null) {
            if (this.c != null) {
                this.c.a(locationListener);
            }
            this.a.removeUpdates(locationListener);
        }
    }

    public void requestLocationUpdates(String str, long j2, float f2, PendingIntent pendingIntent) {
        if (LocationProviderProxy.MapABCNetwork.equals(str)) {
            if (this.e == null) {
                this.e = new c(this);
            }
            if (this.f == null) {
                this.f = new b();
            }
            this.e.a(this.f, j2, f2);
            this.g.add(pendingIntent);
            return;
        }
        this.a.requestLocationUpdates(str, j2, f2, pendingIntent);
    }

    public void removeUpdates(PendingIntent pendingIntent) {
        if (this.e != null) {
            this.g.remove(pendingIntent);
            this.e.a();
        }
        this.e = null;
        this.a.removeUpdates(pendingIntent);
    }

    public List<String> getAllProviders() {
        List<String> allProviders = this.a.getAllProviders();
        if (allProviders == null) {
            ArrayList arrayList = new ArrayList();
            arrayList.add(LocationProviderProxy.MapABCNetwork);
            arrayList.addAll(this.a.getAllProviders());
            return arrayList;
        } else if (allProviders.contains(LocationProviderProxy.MapABCNetwork)) {
            return allProviders;
        } else {
            allProviders.add(LocationProviderProxy.MapABCNetwork);
            return allProviders;
        }
    }

    public List<String> getProviders(boolean z) {
        List<String> providers = this.a.getProviders(z);
        if (isProviderEnabled(LocationProviderProxy.MapABCNetwork)) {
            if (providers == null) {
                providers = new ArrayList<>();
            }
            providers.add(LocationProviderProxy.MapABCNetwork);
        }
        return providers;
    }

    public List<String> getProviders(Criteria criteria, boolean z) {
        List<String> providers = this.a.getProviders(criteria, z);
        if (LocationProviderProxy.MapABCNetwork.equals(getBestProvider(criteria, z))) {
            providers.add(LocationProviderProxy.MapABCNetwork);
        }
        return providers;
    }

    public String getBestProvider(Criteria criteria, boolean z) {
        String str = LocationProviderProxy.MapABCNetwork;
        if (criteria == null) {
            return str;
        }
        if (!getProvider(LocationProviderProxy.MapABCNetwork).meetsCriteria(criteria)) {
            str = this.a.getBestProvider(criteria, z);
        }
        if (!z || d.c(this.d)) {
            return str;
        }
        return this.a.getBestProvider(criteria, z);
    }

    public boolean isProviderEnabled(String str) {
        if (LocationProviderProxy.MapABCNetwork.equals(str)) {
            return d.c(this.d);
        }
        return this.a.isProviderEnabled(str);
    }

    public LocationProviderProxy getProvider(String str) {
        if (str == null) {
            throw new IllegalArgumentException("name不能为空！");
        } else if (this.h.containsKey(str)) {
            return this.h.get(str);
        } else {
            LocationProviderProxy a2 = LocationProviderProxy.a(this.a, str);
            this.h.put(str, a2);
            return a2;
        }
    }

    public GpsStatus getGpsStatus(GpsStatus gpsStatus) {
        if (this.a != null) {
            return this.a.getGpsStatus(gpsStatus);
        }
        return null;
    }

    public void removeGpsStatusListener(GpsStatus.Listener listener) {
        if (this.a != null) {
            this.a.removeGpsStatusListener(listener);
        }
    }

    public boolean addGpsStatusListener(GpsStatus.Listener listener) {
        if (this.a != null) {
            return this.a.addGpsStatusListener(listener);
        }
        return false;
    }

    public void addTestProvider(String str, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, int i2, int i3) {
        if (this.a != null) {
            this.a.addTestProvider(str, z, z2, z3, z4, z5, z6, z7, i2, i3);
        }
    }

    public void clearTestProviderEnabled(String str) {
        if (this.a != null) {
            this.a.clearTestProviderEnabled(str);
        }
    }

    public void clearTestProviderLocation(String str) {
        if (this.a != null) {
            this.a.clearTestProviderLocation(str);
        }
    }

    public void clearTestProviderStatus(String str) {
        if (this.a != null) {
            this.a.clearTestProviderStatus(str);
        }
    }

    public void destory() {
        if (this.c != null) {
            this.c.a();
        }
        if (this.h != null) {
            this.h.clear();
        }
        if (this.g != null) {
            this.g.clear();
        }
        if (this.q != null) {
            this.q.clear();
        }
        this.h = null;
        this.g = null;
        this.q = null;
        this.c = null;
        b = null;
    }

    class b implements LocationListener {
        b() {
        }

        public void onLocationChanged(Location location) {
            if (LocationManagerProxy.this.g != null && LocationManagerProxy.this.g.size() > 0) {
                Iterator it = LocationManagerProxy.this.g.iterator();
                while (it.hasNext()) {
                    PendingIntent pendingIntent = (PendingIntent) it.next();
                    Intent intent = new Intent();
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(LocationManagerProxy.KEY_LOCATION_CHANGED, location);
                    intent.putExtras(bundle);
                    try {
                        pendingIntent.send(LocationManagerProxy.this.d, 0, intent);
                    } catch (PendingIntent.CanceledException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        public void onStatusChanged(String str, int i, Bundle bundle) {
        }

        public void onProviderEnabled(String str) {
        }

        public void onProviderDisabled(String str) {
        }
    }

    class a implements LocationListener {
        a() {
        }

        public void onLocationChanged(Location location) {
            if (LocationManagerProxy.this.l && LocationManagerProxy.this.q.size() > 0) {
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();
                double abs = Math.abs(((latitude - LocationManagerProxy.this.j) * (latitude - LocationManagerProxy.this.j)) + ((longitude - LocationManagerProxy.this.k) * (longitude - LocationManagerProxy.this.k)));
                Iterator it = LocationManagerProxy.this.q.iterator();
                while (it.hasNext()) {
                    PendingIntent pendingIntent = (PendingIntent) it.next();
                    if (d.a() > LocationManagerProxy.this.m && LocationManagerProxy.this.m != 0) {
                        LocationManagerProxy.this.removeProximityAlert(pendingIntent);
                    } else if (Math.abs(abs - (LocationManagerProxy.this.n * LocationManagerProxy.this.n)) < 0.5d) {
                        Intent intent = new Intent();
                        Bundle bundle = new Bundle();
                        bundle.putParcelable(LocationManagerProxy.KEY_LOCATION_CHANGED, location);
                        intent.putExtras(bundle);
                        try {
                            pendingIntent.send(LocationManagerProxy.this.d, 0, intent);
                        } catch (PendingIntent.CanceledException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        public void onStatusChanged(String str, int i, Bundle bundle) {
        }

        public void onProviderEnabled(String str) {
        }

        public void onProviderDisabled(String str) {
        }
    }
}
