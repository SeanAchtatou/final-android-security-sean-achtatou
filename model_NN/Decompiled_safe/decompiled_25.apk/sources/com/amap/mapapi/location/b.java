package com.amap.mapapi.location;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

/* compiled from: IAPSManager */
class b implements LocationListener {
    final /* synthetic */ a a;

    b(a aVar) {
        this.a = aVar;
    }

    public void onLocationChanged(Location location) {
        Location unused = this.a.o = location;
    }

    public void onProviderDisabled(String str) {
    }

    public void onProviderEnabled(String str) {
    }

    public void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
