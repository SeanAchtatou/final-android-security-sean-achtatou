package com.amap.mapapi.location;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.amap.mapapi.core.d;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.autonavi.aps.api.APSFactory;
import com.autonavi.aps.api.IAPS;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import java.util.ArrayList;

/* compiled from: IAPSManager */
public class a implements Runnable {
    private static a b = null;
    /* access modifiers changed from: private */
    public static int f = 100;
    /* access modifiers changed from: private */
    public static int g = HttpRequestParameters.USER_REGISTER;
    /* access modifiers changed from: private */
    public static int h = HttpRequestParameters.EDIT_USER_PWD;
    /* access modifiers changed from: private */
    public static int i = HttpRequestParameters.GET_VERIFY_CODE;
    private IAPS a = null;
    private C0001a c = null;
    private volatile boolean d = true;
    private Thread e = null;
    /* access modifiers changed from: private */
    public ArrayList<b> j = null;
    private Location k = null;
    private Context l;
    /* access modifiers changed from: private */
    public LocationManager m;
    /* access modifiers changed from: private */
    public boolean n = false;
    /* access modifiers changed from: private */
    public Location o = null;
    /* access modifiers changed from: private */
    public float p = 20.0f;
    /* access modifiers changed from: private */
    public long q = 2000;
    private long r = 5000;
    /* access modifiers changed from: private */
    public LocationListener s = new b(this);

    private a(Context context, LocationManager locationManager) {
        this.l = context;
        this.m = locationManager;
        this.c = new C0001a();
        this.j = new ArrayList<>();
        this.a = APSFactory.getInstance(context.getApplicationContext());
        this.a.setProductName("autonavi");
        this.a.setLicence("401FFB6E52385325E41206A6AFF7A316");
    }

    public static synchronized a a(Context context, LocationManager locationManager) {
        a aVar;
        synchronized (a.class) {
            if (b == null) {
                b = new a(context, locationManager);
            }
            aVar = b;
        }
        return aVar;
    }

    public void a() {
        this.d = false;
        if (this.e != null) {
            this.e.interrupt();
        }
        if (this.m != null) {
            this.m.removeUpdates(this.s);
        }
        if (this.c != null) {
            this.c.removeMessages(f);
            this.c.removeMessages(g);
        }
        if (this.j != null) {
            this.j.clear();
        }
        if (this.a != null) {
            this.a.onDestroy();
        }
        this.a = null;
        b = null;
        this.k = null;
        this.c = null;
    }

    public void a(long j2, float f2, LocationListener locationListener) {
        b bVar = new b(j2, f2, locationListener);
        Message obtainMessage = this.c.obtainMessage();
        obtainMessage.what = h;
        obtainMessage.obj = bVar;
        this.c.sendMessage(obtainMessage);
    }

    public void a(LocationListener locationListener) {
        if (this.c != null) {
            Message obtainMessage = this.c.obtainMessage();
            obtainMessage.what = i;
            obtainMessage.obj = locationListener;
            this.c.sendMessage(obtainMessage);
        }
    }

    public Location b() {
        if (this.k != null) {
            return this.k;
        }
        return d.d(this.l);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:69:0x00e4, code lost:
        java.lang.Thread.currentThread().interrupt();
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r13 = this;
            r5 = 5000(0x1388, double:2.4703E-320)
            r2 = 0
        L_0x0003:
            boolean r0 = r13.d
            if (r0 == 0) goto L_0x00ed
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            r13.e = r0
            java.util.ArrayList<com.amap.mapapi.location.a$b> r0 = r13.j
            int r7 = r0.size()
            boolean r0 = r13.n
            if (r0 != 0) goto L_0x002c
            android.location.LocationManager r0 = r13.m
            java.lang.String r1 = "gps"
            boolean r0 = r0.isProviderEnabled(r1)
            if (r0 == 0) goto L_0x002c
            com.amap.mapapi.location.a$a r0 = r13.c
            if (r0 == 0) goto L_0x0003
            com.amap.mapapi.location.a$a r0 = r13.c
            int r1 = com.amap.mapapi.location.a.g
            r0.sendEmptyMessage(r1)
        L_0x002c:
            java.util.ArrayList<com.amap.mapapi.location.a$b> r8 = r13.j
            monitor-enter(r8)
            r1 = r2
        L_0x0030:
            if (r1 >= r7) goto L_0x003a
            java.util.ArrayList<com.amap.mapapi.location.a$b> r0 = r13.j     // Catch:{ all -> 0x0041 }
            int r0 = r0.size()     // Catch:{ all -> 0x0041 }
            if (r0 != 0) goto L_0x0044
        L_0x003a:
            long r0 = r13.r     // Catch:{ Exception -> 0x00e3 }
            java.lang.Thread.sleep(r0)     // Catch:{ Exception -> 0x00e3 }
        L_0x003f:
            monitor-exit(r8)     // Catch:{ all -> 0x0041 }
            goto L_0x0003
        L_0x0041:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x0041 }
            throw r0
        L_0x0044:
            java.util.ArrayList<com.amap.mapapi.location.a$b> r0 = r13.j     // Catch:{ all -> 0x0041 }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x0041 }
            com.amap.mapapi.location.a$b r0 = (com.amap.mapapi.location.a.b) r0     // Catch:{ all -> 0x0041 }
            long r9 = com.amap.mapapi.core.d.a()     // Catch:{ all -> 0x0041 }
            float r3 = r13.p     // Catch:{ all -> 0x0041 }
            float r4 = r0.b     // Catch:{ all -> 0x0041 }
            int r3 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
            if (r3 <= 0) goto L_0x008c
            float r3 = r0.b     // Catch:{ all -> 0x0041 }
        L_0x005a:
            r13.p = r3     // Catch:{ all -> 0x0041 }
            long r3 = r13.q     // Catch:{ all -> 0x0041 }
            long r11 = r0.a     // Catch:{ all -> 0x0041 }
            int r3 = (r3 > r11 ? 1 : (r3 == r11 ? 0 : -1))
            if (r3 <= 0) goto L_0x008f
            long r3 = r0.a     // Catch:{ all -> 0x0041 }
        L_0x0066:
            r13.q = r3     // Catch:{ all -> 0x0041 }
            if (r0 == 0) goto L_0x0088
            long r3 = r0.a     // Catch:{ all -> 0x0041 }
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 >= 0) goto L_0x0092
            r3 = r5
        L_0x0071:
            r13.r = r3     // Catch:{ all -> 0x0041 }
            long r3 = r0.d     // Catch:{ all -> 0x0041 }
            long r3 = r9 - r3
            long r11 = r0.a     // Catch:{ all -> 0x0041 }
            int r3 = (r3 > r11 ? 1 : (r3 == r11 ? 0 : -1))
            if (r3 < 0) goto L_0x0080
            r3 = 1
            r0.e = r3     // Catch:{ all -> 0x0041 }
        L_0x0080:
            boolean r3 = r0.e     // Catch:{ all -> 0x0041 }
            if (r3 == 0) goto L_0x0088
            com.amap.mapapi.location.a$a r3 = r13.c     // Catch:{ all -> 0x0041 }
            if (r3 != 0) goto L_0x0095
        L_0x0088:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x0030
        L_0x008c:
            float r3 = r13.p     // Catch:{ all -> 0x0041 }
            goto L_0x005a
        L_0x008f:
            long r3 = r13.q     // Catch:{ all -> 0x0041 }
            goto L_0x0066
        L_0x0092:
            long r3 = r0.a     // Catch:{ all -> 0x0041 }
            goto L_0x0071
        L_0x0095:
            com.amap.mapapi.location.a$a r3 = r13.c     // Catch:{ all -> 0x0041 }
            android.os.Message r4 = r3.obtainMessage()     // Catch:{ all -> 0x0041 }
            android.location.Location r3 = r13.o     // Catch:{ Exception -> 0x00de }
            if (r3 == 0) goto L_0x00d6
            com.autonavi.aps.api.IAPS r3 = r13.a     // Catch:{ Exception -> 0x00de }
            android.location.Location r11 = r13.o     // Catch:{ Exception -> 0x00de }
            com.autonavi.aps.api.Location r3 = r3.getCurrentLocation(r11)     // Catch:{ Exception -> 0x00de }
        L_0x00a7:
            r11 = 0
            r13.o = r11     // Catch:{ Exception -> 0x00de }
            if (r3 == 0) goto L_0x0088
            r11 = 0
            r0.e = r11     // Catch:{ all -> 0x0041 }
            r0.d = r9     // Catch:{ all -> 0x0041 }
            android.location.Location r3 = r13.a(r3)     // Catch:{ all -> 0x0041 }
            android.location.Location r9 = r13.k     // Catch:{ all -> 0x0041 }
            boolean r9 = r3.equals(r9)     // Catch:{ all -> 0x0041 }
            if (r9 != 0) goto L_0x0088
            r13.k = r3     // Catch:{ all -> 0x0041 }
            r0.f = r3     // Catch:{ all -> 0x0041 }
            int r9 = com.amap.mapapi.location.a.f     // Catch:{ all -> 0x0041 }
            r4.what = r9     // Catch:{ all -> 0x0041 }
            r4.obj = r0     // Catch:{ all -> 0x0041 }
            com.amap.mapapi.location.a$a r0 = r13.c     // Catch:{ all -> 0x0041 }
            if (r0 == 0) goto L_0x0088
            com.amap.mapapi.location.a$a r0 = r13.c     // Catch:{ all -> 0x0041 }
            r0.sendMessage(r4)     // Catch:{ all -> 0x0041 }
            android.content.Context r0 = r13.l     // Catch:{ all -> 0x0041 }
            com.amap.mapapi.core.d.a(r0, r3)     // Catch:{ all -> 0x0041 }
            goto L_0x0088
        L_0x00d6:
            com.autonavi.aps.api.IAPS r3 = r13.a     // Catch:{ Exception -> 0x00de }
            r11 = 0
            com.autonavi.aps.api.Location r3 = r3.getCurrentLocation(r11)     // Catch:{ Exception -> 0x00de }
            goto L_0x00a7
        L_0x00de:
            r3 = move-exception
            r3 = 1
            r0.e = r3     // Catch:{ all -> 0x0041 }
            goto L_0x0088
        L_0x00e3:
            r0 = move-exception
            java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x0041 }
            r0.interrupt()     // Catch:{ all -> 0x0041 }
            goto L_0x003f
        L_0x00ed:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.mapapi.location.a.run():void");
    }

    private Location a(com.autonavi.aps.api.Location location) {
        Location location2 = new Location(PoiTypeDef.All);
        location2.setProvider(LocationProviderProxy.MapABCNetwork);
        location2.setLatitude(location.getCeny());
        location2.setLongitude(location.getCenx());
        location2.setAccuracy((float) location.getRadius());
        Bundle bundle = new Bundle();
        bundle.putString("citycode", location.getCitycode());
        bundle.putString("desc", location.getDesc());
        location2.setExtras(bundle);
        return location2;
    }

    /* renamed from: com.amap.mapapi.location.a$a  reason: collision with other inner class name */
    /* compiled from: IAPSManager */
    class C0001a extends Handler {
        C0001a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.amap.mapapi.location.a.a(com.amap.mapapi.location.a, boolean):boolean
         arg types: [com.amap.mapapi.location.a, int]
         candidates:
          com.amap.mapapi.location.a.a(com.amap.mapapi.location.a, android.location.Location):android.location.Location
          com.amap.mapapi.location.a.a(android.content.Context, android.location.LocationManager):com.amap.mapapi.location.a
          com.amap.mapapi.location.a.a(com.amap.mapapi.location.a, boolean):boolean */
        public void handleMessage(Message message) {
            if (message.what == a.f) {
                int size = a.this.j.size();
                for (int i = 0; i < size; i++) {
                    b bVar = (b) a.this.j.get(i);
                    if (((b) message.obj).equals(bVar)) {
                        bVar.c.onLocationChanged(((b) message.obj).f);
                    }
                }
            } else if (message.what == a.g) {
                if (a.this.m != null) {
                    a.this.m.requestLocationUpdates(LocationManagerProxy.GPS_PROVIDER, a.this.q, a.this.p, a.this.s);
                    boolean unused = a.this.n = true;
                }
            } else if (message.what == a.h) {
                b bVar2 = (b) message.obj;
                if (!a.this.j.contains(bVar2)) {
                    a.this.j.add(bVar2);
                }
            } else if (message.what == a.i) {
                LocationListener locationListener = (LocationListener) message.obj;
                int size2 = a.this.j.size();
                for (int i2 = 0; i2 < size2; i2++) {
                    b bVar3 = (b) a.this.j.get(i2);
                    if (locationListener.equals(bVar3.c)) {
                        a.this.j.remove(bVar3);
                    }
                }
            }
        }
    }

    /* compiled from: IAPSManager */
    public static class b {
        public long a;
        public float b;
        public LocationListener c;
        public long d;
        public boolean e = true;
        public Location f = null;

        public b(long j, float f2, LocationListener locationListener) {
            this.a = j;
            this.b = f2;
            this.c = locationListener;
        }

        public int hashCode() {
            return (this.c == null ? 0 : this.c.hashCode()) + 31;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            if (this.c == null) {
                if (bVar.c != null) {
                    return false;
                }
                return true;
            } else if (!this.c.equals(bVar.c)) {
                return false;
            } else {
                return true;
            }
        }
    }
}
