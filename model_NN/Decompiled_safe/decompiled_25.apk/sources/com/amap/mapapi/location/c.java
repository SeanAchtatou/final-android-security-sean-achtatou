package com.amap.mapapi.location;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

/* compiled from: LocationListenerProxy */
public class c implements LocationListener {
    private LocationManagerProxy a;
    private LocationListener b = null;

    public c(LocationManagerProxy locationManagerProxy) {
        this.a = locationManagerProxy;
    }

    public boolean a(LocationListener locationListener, long j, float f, String str) {
        this.b = locationListener;
        if (!LocationProviderProxy.MapABCNetwork.equals(str)) {
            return false;
        }
        this.a.requestLocationUpdates(str, j, f, this);
        return true;
    }

    public boolean a(LocationListener locationListener, long j, float f) {
        boolean z = false;
        this.b = locationListener;
        for (String next : this.a.getProviders(true)) {
            if (LocationManagerProxy.GPS_PROVIDER.equals(next) || LocationManagerProxy.NETWORK_PROVIDER.equals(next)) {
                this.a.requestLocationUpdates(next, j, f, this);
                z = true;
            }
        }
        return z;
    }

    public void a() {
        if (this.a != null) {
            this.a.removeUpdates(this);
        }
        this.b = null;
    }

    public void onLocationChanged(Location location) {
        if (this.b != null) {
            this.b.onLocationChanged(location);
        }
    }

    public void onStatusChanged(String str, int i, Bundle bundle) {
        if (this.b != null) {
            this.b.onStatusChanged(str, i, bundle);
        }
    }

    public void onProviderEnabled(String str) {
        if (this.b != null) {
            this.b.onProviderEnabled(str);
        }
    }

    public void onProviderDisabled(String str) {
        if (this.b != null) {
            this.b.onProviderDisabled(str);
        }
    }
}
