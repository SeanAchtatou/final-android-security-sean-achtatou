package com.amap.mapapi.location;

import android.location.Criteria;
import android.location.LocationManager;
import android.location.LocationProvider;

public class LocationProviderProxy {
    public static final int AVAILABLE = 2;
    public static final String MapABCNetwork = "lbs";
    public static final int OUT_OF_SERVICE = 0;
    public static final int TEMPORARILY_UNAVAILABLE = 1;
    private LocationManager a;
    private String b;

    protected LocationProviderProxy(LocationManager locationManager, String str) {
        this.a = locationManager;
        this.b = str;
    }

    static LocationProviderProxy a(LocationManager locationManager, String str) {
        return new LocationProviderProxy(locationManager, str);
    }

    private LocationProvider a() {
        return this.a.getProvider(this.b);
    }

    public int getAccuracy() {
        if (MapABCNetwork.equals(this.b)) {
            return 2;
        }
        return a().getAccuracy();
    }

    public String getName() {
        if (MapABCNetwork.equals(this.b)) {
            return MapABCNetwork;
        }
        return a().getName();
    }

    public int getPowerRequirement() {
        if (MapABCNetwork.equals(this.b)) {
            return 2;
        }
        return a().getPowerRequirement();
    }

    public boolean hasMonetaryCost() {
        if (MapABCNetwork.equals(this.b)) {
            return false;
        }
        return a().hasMonetaryCost();
    }

    public boolean meetsCriteria(Criteria criteria) {
        if (!MapABCNetwork.equals(this.b)) {
            return a().meetsCriteria(criteria);
        }
        if (criteria == null) {
            return true;
        }
        if (criteria.isAltitudeRequired() || criteria.isBearingRequired() || criteria.isSpeedRequired() || criteria.getAccuracy() == 1) {
            return false;
        }
        return true;
    }

    public boolean requiresCell() {
        if (MapABCNetwork.equals(this.b)) {
            return true;
        }
        return a().requiresCell();
    }

    public boolean requiresNetwork() {
        if (MapABCNetwork.equals(this.b)) {
            return true;
        }
        return a().requiresNetwork();
    }

    public boolean requiresSatellite() {
        if (MapABCNetwork.equals(this.b)) {
            return false;
        }
        return a().requiresNetwork();
    }

    public boolean supportsAltitude() {
        if (MapABCNetwork.equals(this.b)) {
            return false;
        }
        return a().supportsAltitude();
    }

    public boolean supportsBearing() {
        if (MapABCNetwork.equals(this.b)) {
            return false;
        }
        return a().supportsBearing();
    }

    public boolean supportsSpeed() {
        if (MapABCNetwork.equals(this.b)) {
            return false;
        }
        return a().supportsSpeed();
    }
}
