package com.amap.mapapi.busline;

import android.app.Activity;
import android.content.Context;
import com.amap.mapapi.core.AMapException;
import com.amap.mapapi.core.a;
import com.amap.mapapi.core.d;
import java.util.ArrayList;

public class BusSearch {
    private Context a;
    private BusQuery b;
    private int c = 10;

    public BusSearch(Activity activity, BusQuery busQuery) {
        a.a(activity);
        this.a = activity;
        this.b = busQuery;
    }

    public BusSearch(Context context, String str, BusQuery busQuery) {
        a.a(context);
        this.a = context;
        this.b = busQuery;
    }

    public BusPagedResult searchBusLine() throws AMapException {
        a aVar = new a(this.b, d.b(this.a), d.a(this.a), null);
        aVar.a(1);
        aVar.b(this.c);
        return BusPagedResult.a(aVar, (ArrayList) aVar.j());
    }

    public void setPageSize(int i) {
        this.c = i;
    }

    public void setQuery(BusQuery busQuery) {
        this.b = busQuery;
    }

    public BusQuery getQuery() {
        return this.b;
    }
}
