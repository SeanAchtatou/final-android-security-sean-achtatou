package com.amap.mapapi.busline;

import com.amap.mapapi.core.GeoPoint;

public class BusStationItem {
    private String a;
    private GeoPoint b;
    private String c;
    private String d;
    private int e;

    public String getmName() {
        return this.a;
    }

    public void setmName(String str) {
        this.a = str;
    }

    public GeoPoint getmCoord() {
        return this.b;
    }

    public void setmCoord(GeoPoint geoPoint) {
        this.b = geoPoint;
    }

    public String getmSpell() {
        return this.c;
    }

    public void setmSpell(String str) {
        this.c = str;
    }

    public String getmCode() {
        return this.d;
    }

    public void setmCode(String str) {
        this.d = str;
    }

    public int getmStationNum() {
        return this.e;
    }

    public void setmStationNum(int i) {
        this.e = i;
    }

    public String toString() {
        return "Name: " + this.a + " Coord: " + this.b.toString() + " Spell: " + this.c + " Code: " + this.d + " StationNum: " + this.e;
    }
}
