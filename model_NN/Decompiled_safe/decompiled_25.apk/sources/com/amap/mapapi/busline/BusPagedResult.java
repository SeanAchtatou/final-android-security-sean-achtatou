package com.amap.mapapi.busline;

import com.amap.mapapi.core.AMapException;
import java.util.ArrayList;
import java.util.List;

public final class BusPagedResult {
    private int a;
    private ArrayList<ArrayList<BusLineItem>> b;
    private a c;

    static BusPagedResult a(a aVar, ArrayList<BusLineItem> arrayList) {
        return new BusPagedResult(aVar, arrayList);
    }

    private BusPagedResult(a aVar, ArrayList<BusLineItem> arrayList) {
        this.c = aVar;
        this.a = a(aVar.d());
        a(arrayList);
    }

    private int a(int i) {
        int a2 = this.c.a();
        int i2 = ((i + a2) - 1) / a2;
        if (i2 > 30) {
            return 30;
        }
        return i2;
    }

    private void a(ArrayList<BusLineItem> arrayList) {
        this.b = new ArrayList<>();
        for (int i = 0; i <= this.a; i++) {
            this.b.add(null);
        }
        if (this.a > 0) {
            this.b.set(1, arrayList);
        }
    }

    public int getPageCount() {
        return this.a;
    }

    public BusQuery getQuery() {
        return this.c.c();
    }

    private boolean b(int i) {
        return i <= this.a && i > 0;
    }

    public List<BusLineItem> getPageLocal(int i) {
        if (b(i)) {
            return this.b.get(i);
        }
        throw new IllegalArgumentException("page out of range");
    }

    public List<BusLineItem> getPage(int i) throws AMapException {
        if (this.a == 0) {
            return null;
        }
        if (!b(i)) {
            throw new IllegalArgumentException("page out of range");
        }
        ArrayList arrayList = (ArrayList) getPageLocal(i);
        if (arrayList != null) {
            return arrayList;
        }
        this.c.a(i);
        ArrayList arrayList2 = (ArrayList) this.c.j();
        this.b.set(i, arrayList2);
        return arrayList2;
    }
}
