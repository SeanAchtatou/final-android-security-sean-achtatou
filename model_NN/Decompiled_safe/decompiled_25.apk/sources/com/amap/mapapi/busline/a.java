package com.amap.mapapi.busline;

import android.util.Log;
import com.amap.mapapi.busline.BusQuery;
import com.amap.mapapi.core.AMapException;
import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.core.d;
import com.amap.mapapi.core.i;
import com.amap.mapapi.core.t;
import com.amap.mapapi.location.LocationManagerProxy;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.Proxy;
import java.net.URLEncoder;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/* compiled from: BusSearchServerHandler */
public class a extends t<BusQuery, BusLineItem> {
    private int i = 1;
    private int j = 10;
    private int k = 0;

    public a(BusQuery busQuery, Proxy proxy, String str, String str2) {
        super(busQuery, proxy, str, str2);
    }

    public void a(int i2) {
        this.i = i2;
    }

    public void b(int i2) {
        int i3 = 20;
        if (i2 <= 20) {
            i3 = i2;
        }
        if (i3 <= 0) {
            i3 = 10;
        }
        this.j = i3;
    }

    public int a() {
        return this.j;
    }

    public int b() {
        return this.i;
    }

    public BusQuery c() {
        return (BusQuery) this.b;
    }

    public int d() {
        return this.k;
    }

    private void b(Node node) {
        this.k = Integer.parseInt(a(node));
    }

    /* access modifiers changed from: protected */
    public void a(Node node, ArrayList<BusLineItem> arrayList) {
        if (node.getNodeType() == 1) {
            int b = b();
            String nodeName = node.getNodeName();
            if (nodeName.equals("count") && b == 1) {
                b(node);
            }
            if (nodeName.equals("list")) {
                NodeList childNodes = node.getChildNodes();
                for (int i2 = 0; i2 < childNodes.getLength(); i2++) {
                    Node item = childNodes.item(i2);
                    if (item.getNodeType() == 1 && item.getNodeName().equals("bus")) {
                        C0000a aVar = new C0000a();
                        a(item, aVar);
                        arrayList.add(aVar.a());
                    }
                }
            }
        }
    }

    private void a(Node node, C0000a aVar) {
        NodeList childNodes = node.getChildNodes();
        for (int i2 = 0; i2 < childNodes.getLength(); i2++) {
            Node item = childNodes.item(i2);
            if (item.getNodeType() == 1) {
                try {
                    a(aVar, item);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private ArrayList<GeoPoint> a(String str) {
        String[] split = str.split(",");
        ArrayList<GeoPoint> arrayList = new ArrayList<>();
        for (int i2 = 0; i2 < split.length - 1; i2 += 2) {
            arrayList.add(new GeoPoint((int) (Double.parseDouble(split[i2 + 1]) * 1000000.0d), (int) (Double.parseDouble(split[i2]) * 1000000.0d)));
        }
        return arrayList;
    }

    private String b(String str) {
        return str.replaceAll("&lt;\\?.*\\?&gt;", PoiTypeDef.All).replaceAll("(&lt;)([^&]*)(&gt;)", "<$2>");
    }

    private String c(Node node) {
        Node firstChild;
        if (node == null || (firstChild = node.getFirstChild()) == null || firstChild.getNodeType() != 4) {
            return null;
        }
        return firstChild.getNodeValue();
    }

    private ArrayList<BusStationItem> c(NodeList nodeList) {
        if (nodeList == null || nodeList.getLength() <= 0) {
            return null;
        }
        Node item = nodeList.item(0);
        ArrayList<BusStationItem> arrayList = new ArrayList<>();
        if (item.getNodeName().equals("CONTENT")) {
            NodeList childNodes = item.getChildNodes();
            for (int i2 = 0; i2 < childNodes.getLength(); i2++) {
                Node item2 = childNodes.item(i2);
                if (item2.getNodeType() == 1 && item2.getNodeName().equals("STATION")) {
                    BusStationItem busStationItem = new BusStationItem();
                    NodeList childNodes2 = item2.getChildNodes();
                    for (int i3 = 0; i3 < childNodes2.getLength(); i3++) {
                        Node item3 = childNodes2.item(i3);
                        if (item3.getNodeType() == 1) {
                            String nodeValue = item3.getAttributes().getNamedItem("NAME").getNodeValue();
                            String c = c(item3);
                            if (nodeValue.equals("NAME")) {
                                busStationItem.setmName(c);
                            } else if (nodeValue.equals("XY_COORDS")) {
                                String[] split = c.split(";");
                                if (split.length >= 2) {
                                    busStationItem.setmCoord(new GeoPoint((int) (Double.parseDouble(split[1]) * 1000000.0d), (int) (Double.parseDouble(split[0]) * 1000000.0d)));
                                }
                            } else if (nodeValue.equals("SPELL")) {
                                busStationItem.setmSpell(c);
                            } else if (nodeValue.equals("Code")) {
                                busStationItem.setmCode(c);
                            } else if (nodeValue.equals("STATION_NUM")) {
                                busStationItem.setmStationNum(Integer.parseInt(c));
                            }
                        }
                    }
                    arrayList.add(busStationItem);
                }
            }
        }
        return arrayList;
    }

    private void a(C0000a aVar, Node node) {
        String nodeName = node.getNodeName();
        String a = a(node);
        if (nodeName.equals("length") && a != null) {
            float unused = aVar.a = Float.parseFloat(a);
        } else if (nodeName.equals("name")) {
            String unused2 = aVar.b = a;
        } else if (nodeName.equals(com.umeng.common.a.b) && a != null) {
            int unused3 = aVar.c = Integer.parseInt(a);
        } else if (nodeName.equals("description")) {
            String unused4 = aVar.d = a;
        } else if (nodeName.equals(LocationManagerProxy.KEY_STATUS_CHANGED) && a != null) {
            int unused5 = aVar.e = Integer.parseInt(a);
        } else if (nodeName.equals("speed") && a != null) {
            float unused6 = aVar.f = Float.parseFloat(a);
        } else if (nodeName.equals("xys") && a != null && !a.equals(PoiTypeDef.All)) {
            ArrayList unused7 = aVar.g = a(a);
        } else if (nodeName.equals("line_id")) {
            String unused8 = aVar.h = a;
        } else if (nodeName.equals("key_name")) {
            String unused9 = aVar.i = a;
        } else if (nodeName.equals("front_name")) {
            String unused10 = aVar.j = a;
        } else if (nodeName.equals("terminal_name")) {
            String unused11 = aVar.k = a;
        } else if (nodeName.equals("start_time")) {
            String unused12 = aVar.l = a;
        } else if (nodeName.equals("end_time")) {
            String unused13 = aVar.m = a;
        } else if (nodeName.equals("company")) {
            String unused14 = aVar.n = a;
        } else if (nodeName.equals("basic_price") && a != null) {
            float unused15 = aVar.o = Float.parseFloat(a);
        } else if (nodeName.equals("total_price") && a != null) {
            float unused16 = aVar.p = Float.parseFloat(a);
        } else if (nodeName.equals("commutation_ticket") && a != null) {
            boolean unused17 = aVar.q = a.equals("1");
        } else if (nodeName.equals("auto") && a != null) {
            boolean unused18 = aVar.r = a.equals("1");
        } else if (nodeName.equals("ic_card") && a != null) {
            boolean unused19 = aVar.s = a.equals("1");
        } else if (nodeName.equals("loop") && a != null) {
            boolean unused20 = aVar.t = a.equals("1");
        } else if (nodeName.equals("double_deck") && a != null) {
            boolean unused21 = aVar.u = a.equals("1");
        } else if (nodeName.equals("data_source") && a != null) {
            int unused22 = aVar.v = Integer.parseInt(a);
        } else if (nodeName.equals("air") && a != null) {
            boolean unused23 = aVar.w = a.equals("1");
        } else if (nodeName.equals("front_spell")) {
            String unused24 = aVar.x = a;
        } else if (nodeName.equals("terminal_spell")) {
            String unused25 = aVar.y = a;
        } else if (nodeName.equals("express_way") && a != null) {
            boolean unused26 = aVar.z = a.equals("1");
        } else if (nodeName.equals("stationdes")) {
            ArrayList unused27 = aVar.A = c(node.getChildNodes());
        }
    }

    /* renamed from: com.amap.mapapi.busline.a$a  reason: collision with other inner class name */
    /* compiled from: BusSearchServerHandler */
    static class C0000a {
        /* access modifiers changed from: private */
        public ArrayList<BusStationItem> A;
        /* access modifiers changed from: private */
        public float a;
        /* access modifiers changed from: private */
        public String b;
        /* access modifiers changed from: private */
        public int c;
        /* access modifiers changed from: private */
        public String d;
        /* access modifiers changed from: private */
        public int e;
        /* access modifiers changed from: private */
        public float f;
        /* access modifiers changed from: private */
        public ArrayList<GeoPoint> g;
        /* access modifiers changed from: private */
        public String h;
        /* access modifiers changed from: private */
        public String i;
        /* access modifiers changed from: private */
        public String j;
        /* access modifiers changed from: private */
        public String k;
        /* access modifiers changed from: private */
        public String l;
        /* access modifiers changed from: private */
        public String m;
        /* access modifiers changed from: private */
        public String n;
        /* access modifiers changed from: private */
        public float o;
        /* access modifiers changed from: private */
        public float p;
        /* access modifiers changed from: private */
        public boolean q;
        /* access modifiers changed from: private */
        public boolean r;
        /* access modifiers changed from: private */
        public boolean s;
        /* access modifiers changed from: private */
        public boolean t;
        /* access modifiers changed from: private */
        public boolean u;
        /* access modifiers changed from: private */
        public int v;
        /* access modifiers changed from: private */
        public boolean w;
        /* access modifiers changed from: private */
        public String x;
        /* access modifiers changed from: private */
        public String y;
        /* access modifiers changed from: private */
        public boolean z;

        C0000a() {
        }

        public BusLineItem a() {
            BusLineItem busLineItem = new BusLineItem();
            busLineItem.setmLength(this.a);
            busLineItem.setmName(this.b);
            busLineItem.setmType(this.c);
            busLineItem.setmDescription(this.d);
            busLineItem.setmStatus(this.e);
            busLineItem.setmSpeed(this.f);
            busLineItem.setmXys(this.g);
            busLineItem.setmLineId(this.h);
            busLineItem.setmKeyName(this.i);
            busLineItem.setmFrontName(this.j);
            busLineItem.setmTerminalName(this.k);
            busLineItem.setmStartTime(this.l);
            busLineItem.setmEndTime(this.m);
            busLineItem.setmCompany(this.n);
            busLineItem.setmBasicPrice(this.o);
            busLineItem.setmTotalPrice(this.p);
            busLineItem.setmCommunicationTicket(this.q);
            busLineItem.setmAuto(this.r);
            busLineItem.setmIcCard(this.s);
            busLineItem.setmLoop(this.t);
            busLineItem.setmDoubleDeck(this.u);
            busLineItem.setmDataSource(this.v);
            busLineItem.setmAir(this.w);
            busLineItem.setmFrontSpell(this.x);
            busLineItem.setmTerminalSpell(this.y);
            busLineItem.setmExpressWay(this.z);
            busLineItem.setmStations(this.A);
            return busLineItem;
        }
    }

    /* access modifiers changed from: protected */
    public void a(ArrayList<BusLineItem> arrayList) {
    }

    /* access modifiers changed from: protected */
    public boolean e() {
        return true;
    }

    private boolean c(String str) {
        if (str == null || str.equals(PoiTypeDef.All)) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public String[] f() {
        String str;
        String[] strArr = new String[6];
        strArr[0] = "?config=BusLine";
        String city = ((BusQuery) this.b).getCity();
        if (c(city)) {
            strArr[1] = "&cityCode=total";
        } else {
            try {
                strArr[1] = "&cityCode=" + URLEncoder.encode(city, "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        String queryString = ((BusQuery) this.b).getQueryString();
        try {
            str = URLEncoder.encode(queryString, "utf-8");
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
            str = queryString;
        }
        if (((BusQuery) this.b).getCategory() == BusQuery.SearchType.BY_LINE_NAME) {
            strArr[2] = "&busName=" + str;
        } else if (((BusQuery) this.b).getCategory() == BusQuery.SearchType.BY_ID) {
            strArr[2] = "&ids=" + str;
        } else if (((BusQuery) this.b).getCategory() == BusQuery.SearchType.BY_STATION_NAME) {
            strArr[2] = "&stationName=" + str;
        }
        strArr[3] = "&number=" + (PoiTypeDef.All + this.j);
        strArr[4] = "&batch=" + (PoiTypeDef.All + this.i);
        strArr[5] = "&enc=utf-8";
        return strArr;
    }

    /* access modifiers changed from: protected */
    public byte[] g() {
        StringBuilder sb = new StringBuilder();
        sb.append("config=BusLine&resType=json&enc=utf-8&cityCode=");
        String city = ((BusQuery) this.b).getCity();
        if (c(city)) {
            sb.append("total");
        } else {
            try {
                sb.append(URLEncoder.encode(city, "utf-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        if (((BusQuery) this.b).getCategory() == BusQuery.SearchType.BY_LINE_NAME) {
            sb.append("&busName=");
        } else if (((BusQuery) this.b).getCategory() == BusQuery.SearchType.BY_ID) {
            sb.append("&ids=");
        } else if (((BusQuery) this.b).getCategory() == BusQuery.SearchType.BY_STATION_NAME) {
            sb.append("&stationName=");
        }
        String queryString = ((BusQuery) this.b).getQueryString();
        try {
            queryString = URLEncoder.encode(queryString, "utf-8");
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        sb.append(queryString);
        sb.append("&number=");
        sb.append(this.j);
        sb.append("&batch=");
        sb.append(this.i);
        com.amap.mapapi.core.a a = com.amap.mapapi.core.a.a(null);
        sb.append("&a_k=");
        sb.append(a.a());
        return sb.toString().getBytes();
    }

    /* access modifiers changed from: protected */
    public String h() {
        return i.a().d();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ArrayList<BusLineItem> c(InputStream inputStream) throws AMapException {
        String str;
        if (e()) {
            return super.c(inputStream);
        }
        ArrayList<BusLineItem> arrayList = new ArrayList<>();
        try {
            str = new String(com.amap.mapapi.map.i.a(inputStream));
        } catch (Exception e) {
            e.printStackTrace();
            str = null;
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            this.k = jSONObject.getInt("count");
            JSONArray jSONArray = jSONObject.getJSONArray("list");
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
                BusLineItem busLineItem = new BusLineItem();
                busLineItem.setmLength((float) jSONObject2.getDouble("length"));
                busLineItem.setmName(jSONObject2.getString("name"));
                busLineItem.setmType(jSONObject2.getInt(com.umeng.common.a.b));
                busLineItem.setmDescription(jSONObject2.getString("description"));
                busLineItem.setmStatus(jSONObject2.getInt(LocationManagerProxy.KEY_STATUS_CHANGED));
                if (!jSONObject2.getString("speed").equals(PoiTypeDef.All)) {
                    busLineItem.setmSpeed((float) jSONObject2.getDouble("speed"));
                }
                busLineItem.setmXys(a(jSONObject2.getString("xys")));
                busLineItem.setmLineId(jSONObject2.getString("line_id"));
                busLineItem.setmKeyName(jSONObject2.getString("key_name"));
                busLineItem.setmFrontName(jSONObject2.getString("front_name"));
                busLineItem.setmTerminalName(jSONObject2.getString("terminal_name"));
                busLineItem.setmStartTime(jSONObject2.getString("start_time"));
                busLineItem.setmEndTime(jSONObject2.getString("end_time"));
                busLineItem.setmCompany(jSONObject2.getString("company"));
                busLineItem.setmBasicPrice((float) jSONObject2.getDouble("basic_price"));
                busLineItem.setmTotalPrice((float) jSONObject2.getDouble("total_price"));
                busLineItem.setmCommunicationTicket(jSONObject2.getString("commutation_ticket").equals("1"));
                busLineItem.setmAuto(jSONObject2.getString("auto").equals("1"));
                busLineItem.setmIcCard(jSONObject2.getString("ic_card").equals("1"));
                busLineItem.setmLoop(jSONObject2.getString("loop").equals("1"));
                busLineItem.setmDoubleDeck(jSONObject2.getString("double_deck").equals("1"));
                busLineItem.setmDataSource(jSONObject2.getInt("data_source"));
                busLineItem.setmAir(jSONObject2.getString("air").equals("1"));
                busLineItem.setmFrontSpell(jSONObject2.getString("front_spell"));
                busLineItem.setmTerminalSpell(jSONObject2.getString("terminal_spell"));
                busLineItem.setmExpressWay(jSONObject2.getString("express_way").equals("1"));
                arrayList.add(busLineItem);
            }
            return arrayList;
        } catch (JSONException e2) {
            e2.printStackTrace();
            return arrayList;
        }
    }

    /* access modifiers changed from: protected */
    public NodeList b(InputStream inputStream) throws AMapException {
        String d = d(inputStream);
        Log.d("BUS", "XML string length = " + d.length());
        return d.b(b(d)).getDocumentElement().getChildNodes();
    }
}
