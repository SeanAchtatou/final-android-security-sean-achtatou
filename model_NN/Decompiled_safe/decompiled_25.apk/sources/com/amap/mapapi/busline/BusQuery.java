package com.amap.mapapi.busline;

import com.amap.mapapi.core.d;

public class BusQuery {
    private String a;
    private SearchType b;
    private String c;

    public enum SearchType {
        BY_ID,
        BY_LINE_NAME,
        BY_STATION_NAME
    }

    public BusQuery(String str, SearchType searchType) {
        this(str, searchType, null);
    }

    public BusQuery(String str, SearchType searchType, String str2) {
        this.a = str;
        this.b = searchType;
        this.c = str2;
        if (!a()) {
            throw new IllegalArgumentException("Empty query");
        }
    }

    private boolean a() {
        return !d.a(this.a);
    }

    public String getQueryString() {
        return this.a;
    }

    public SearchType getCategory() {
        return this.b;
    }

    public String getCity() {
        return this.c;
    }
}
