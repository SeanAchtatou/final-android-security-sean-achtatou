package com.amap.mapapi.busline;

import com.amap.mapapi.core.GeoPoint;
import java.util.ArrayList;

public class BusLineItem {
    private ArrayList<BusStationItem> A;
    private GeoPoint B = null;
    private GeoPoint C = null;
    private float a;
    private String b;
    private int c;
    private String d;
    private int e;
    private float f;
    private ArrayList<GeoPoint> g;
    private String h;
    private String i;
    private String j;
    private String k;
    private String l;
    private String m;
    private String n;
    private float o;
    private float p;
    private boolean q;
    private boolean r;
    private boolean s;
    private boolean t;
    private boolean u;
    private int v;
    private boolean w;
    private String x;
    private String y;
    private boolean z;

    private void a() {
        int i2 = 0;
        int i3 = Integer.MAX_VALUE;
        int i4 = Integer.MIN_VALUE;
        int i5 = Integer.MAX_VALUE;
        int i6 = Integer.MIN_VALUE;
        while (true) {
            int i7 = i2;
            if (i7 < this.g.size()) {
                GeoPoint geoPoint = this.g.get(i7);
                int longitudeE6 = geoPoint.getLongitudeE6();
                int latitudeE6 = geoPoint.getLatitudeE6();
                if (longitudeE6 < i5) {
                    i5 = longitudeE6;
                }
                if (latitudeE6 < i3) {
                    i3 = latitudeE6;
                }
                if (longitudeE6 > i4) {
                    i4 = longitudeE6;
                }
                if (latitudeE6 > i6) {
                    i6 = latitudeE6;
                }
                i2 = i7 + 1;
            } else {
                this.B = new GeoPoint(i3, i5);
                this.C = new GeoPoint(i6, i4);
                return;
            }
        }
    }

    public GeoPoint getLowerLeftPoint() {
        if (this.B == null) {
            a();
        }
        return this.B;
    }

    public GeoPoint getUpperRightPoint() {
        if (this.C == null) {
            a();
        }
        return this.C;
    }

    public float getmLength() {
        return this.a;
    }

    public void setmLength(float f2) {
        this.a = f2;
    }

    public String getmName() {
        return this.b;
    }

    public void setmName(String str) {
        this.b = str;
    }

    public int getmType() {
        return this.c;
    }

    public void setmType(int i2) {
        this.c = i2;
    }

    public String getmDescription() {
        return this.d;
    }

    public void setmDescription(String str) {
        this.d = str;
    }

    public int getmStatus() {
        return this.e;
    }

    public void setmStatus(int i2) {
        this.e = i2;
    }

    public float getmSpeed() {
        return this.f;
    }

    public void setmSpeed(float f2) {
        this.f = f2;
    }

    public ArrayList<GeoPoint> getmXys() {
        return this.g;
    }

    public void setmXys(ArrayList<GeoPoint> arrayList) {
        this.g = arrayList;
    }

    public String getmLineId() {
        return this.h;
    }

    public void setmLineId(String str) {
        this.h = str;
    }

    public String getmKeyName() {
        return this.i;
    }

    public void setmKeyName(String str) {
        this.i = str;
    }

    public String getmFrontName() {
        return this.j;
    }

    public void setmFrontName(String str) {
        this.j = str;
    }

    public String getmTerminalName() {
        return this.k;
    }

    public void setmTerminalName(String str) {
        this.k = str;
    }

    public String getmStartTime() {
        return this.l;
    }

    public void setmStartTime(String str) {
        this.l = str;
    }

    public String getmEndTime() {
        return this.m;
    }

    public void setmEndTime(String str) {
        this.m = str;
    }

    public String getmCompany() {
        return this.n;
    }

    public void setmCompany(String str) {
        this.n = str;
    }

    public float getmBasicPrice() {
        return this.o;
    }

    public void setmBasicPrice(float f2) {
        this.o = f2;
    }

    public float getmTotalPrice() {
        return this.p;
    }

    public void setmTotalPrice(float f2) {
        this.p = f2;
    }

    public boolean getmCommunicationTicket() {
        return this.q;
    }

    public void setmCommunicationTicket(boolean z2) {
        this.q = z2;
    }

    public boolean getmAuto() {
        return this.r;
    }

    public void setmAuto(boolean z2) {
        this.r = z2;
    }

    public boolean ismIcCard() {
        return this.s;
    }

    public void setmIcCard(boolean z2) {
        this.s = z2;
    }

    public boolean ismLoop() {
        return this.t;
    }

    public void setmLoop(boolean z2) {
        this.t = z2;
    }

    public boolean ismDoubleDeck() {
        return this.u;
    }

    public void setmDoubleDeck(boolean z2) {
        this.u = z2;
    }

    public int getmDataSource() {
        return this.v;
    }

    public void setmDataSource(int i2) {
        this.v = i2;
    }

    public boolean getmAir() {
        return this.w;
    }

    public void setmAir(boolean z2) {
        this.w = z2;
    }

    public String getmFrontSpell() {
        return this.x;
    }

    public void setmFrontSpell(String str) {
        this.x = str;
    }

    public String getmTerminalSpell() {
        return this.y;
    }

    public void setmTerminalSpell(String str) {
        this.y = str;
    }

    public boolean ismExpressWay() {
        return this.z;
    }

    public void setmExpressWay(boolean z2) {
        this.z = z2;
    }

    public ArrayList<BusStationItem> getmStations() {
        return this.A;
    }

    public void setmStations(ArrayList<BusStationItem> arrayList) {
        this.A = arrayList;
    }

    public String toString() {
        return this.b + " " + this.l + "-" + this.m;
    }
}
