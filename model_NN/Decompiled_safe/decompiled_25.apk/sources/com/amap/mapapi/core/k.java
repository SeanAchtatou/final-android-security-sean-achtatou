package com.amap.mapapi.core;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: PoiItem */
class k implements Parcelable.Creator<PoiItem> {
    k() {
    }

    /* renamed from: a */
    public PoiItem createFromParcel(Parcel parcel) {
        return new PoiItem(parcel, null);
    }

    /* renamed from: a */
    public PoiItem[] newArray(int i) {
        return new PoiItem[i];
    }
}
