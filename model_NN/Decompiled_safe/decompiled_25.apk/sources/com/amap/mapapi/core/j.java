package com.amap.mapapi.core;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: OverlayItem */
class j implements Parcelable.Creator<OverlayItem> {
    j() {
    }

    /* renamed from: a */
    public OverlayItem createFromParcel(Parcel parcel) {
        return new OverlayItem(parcel);
    }

    /* renamed from: a */
    public OverlayItem[] newArray(int i) {
        return new OverlayItem[i];
    }
}
