package com.amap.mapapi.core;

/* compiled from: IDRefPool */
public class g<T> extends s<T> {
    public void a(T t) {
        if (!this.a.contains(t)) {
            this.a.add(t);
        }
    }

    public void b(T t) {
        for (int i = 0; i < this.a.size(); i++) {
            if (t == this.a.get(i)) {
                this.a.remove(i);
                return;
            }
        }
    }
}
