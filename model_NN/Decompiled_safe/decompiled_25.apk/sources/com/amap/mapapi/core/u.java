package com.amap.mapapi.core;

import com.amap.mapapi.poisearch.PoiTypeDef;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: XmlObject */
public class u {
    private static String a = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
    private String b;
    private Object c;
    private List<a> d;
    private List<u> e;

    public u(String str) {
        this.b = str;
    }

    public final void a(Object obj) {
        this.c = obj;
    }

    public final void a(String str, Object obj) {
        a aVar;
        if (this.d == null) {
            this.d = new ArrayList();
        }
        Iterator<a> it = this.d.iterator();
        while (true) {
            if (!it.hasNext()) {
                aVar = null;
                break;
            }
            aVar = it.next();
            if (str.equalsIgnoreCase(aVar.a())) {
                break;
            }
        }
        if (aVar == null) {
            this.d.add(new a(str, obj));
            return;
        }
        aVar.a(obj);
    }

    public final void a(u uVar) {
        if (this.e == null) {
            this.e = new ArrayList();
        }
        this.e.add(uVar);
    }

    public final String a() {
        return a + a(PoiTypeDef.All, PoiTypeDef.All);
    }

    /* access modifiers changed from: protected */
    public final String a(String str, String str2) {
        return a(str, str2, 0);
    }

    private final String a(String str, String str2, int i) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i2 = 0; i2 < i; i2++) {
            stringBuffer.append(str);
        }
        StringBuffer stringBuffer2 = new StringBuffer();
        stringBuffer2.append(((Object) stringBuffer) + "<" + this.b);
        if (this.d != null) {
            for (a next : this.d) {
                stringBuffer2.append(" " + next.a() + "=\"" + next.b() + "\"");
            }
        }
        if (this.e != null) {
            stringBuffer2.append(">" + str2);
            int i3 = i + 1;
            for (u a2 : this.e) {
                stringBuffer2.append(a2.a(str, str2, i3));
            }
            stringBuffer2.append(((Object) stringBuffer) + "</" + this.b + ">" + str2);
        } else if (this.c == null) {
            stringBuffer2.append("/>" + str2);
        } else {
            stringBuffer2.append(">");
            stringBuffer2.append(this.c);
            stringBuffer2.append("</" + this.b + ">" + str2);
        }
        return stringBuffer2.toString();
    }

    /* compiled from: XmlObject */
    static class a {
        private String a;
        private Object b;

        public a(String str, Object obj) {
            this.a = str;
            this.b = obj;
        }

        public String a() {
            return this.a;
        }

        public Object b() {
            return this.b;
        }

        public void a(Object obj) {
            this.b = obj;
        }
    }
}
