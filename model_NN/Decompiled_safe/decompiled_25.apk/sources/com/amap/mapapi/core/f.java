package com.amap.mapapi.core;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;

/* compiled from: HttpTool */
public class f {
    public static HttpURLConnection a(String str, Proxy proxy) throws AMapException {
        HttpURLConnection httpURLConnection;
        if (str == null) {
            throw new AMapException(AMapException.ERROR_INVALID_PARAMETER);
        }
        try {
            URL url = new URL(str);
            if (proxy == null || b.o) {
                httpURLConnection = (HttpURLConnection) url.openConnection();
            } else {
                httpURLConnection = (HttpURLConnection) url.openConnection(proxy);
            }
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setConnectTimeout(30000);
            httpURLConnection.setReadTimeout(50000);
            httpURLConnection.setRequestProperty("Accept-Encoding", "gzip");
            httpURLConnection.connect();
            if (httpURLConnection.getResponseCode() == 200) {
                return httpURLConnection;
            }
            throw new AMapException(AMapException.ERROR_CONNECTION);
        } catch (UnknownHostException e) {
            throw new AMapException(AMapException.ERROR_UNKNOW_HOST);
        } catch (MalformedURLException e2) {
            throw new AMapException(AMapException.ERROR_URL);
        } catch (ProtocolException e3) {
            throw new AMapException(AMapException.ERROR_PROTOCOL);
        } catch (SocketTimeoutException e4) {
            throw new AMapException(AMapException.ERROR_SOCKE_TIME_OUT);
        } catch (IOException e5) {
            throw new AMapException(AMapException.ERROR_IO);
        }
    }

    public static HttpURLConnection a(String str, byte[] bArr, Proxy proxy) throws AMapException {
        HttpURLConnection httpURLConnection;
        if (str == null) {
            throw new AMapException(AMapException.ERROR_INVALID_PARAMETER);
        }
        try {
            URL url = new URL(str);
            if (proxy == null || b.o) {
                httpURLConnection = (HttpURLConnection) url.openConnection();
            } else {
                httpURLConnection = (HttpURLConnection) url.openConnection(proxy);
            }
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setInstanceFollowRedirects(true);
            httpURLConnection.setConnectTimeout(30000);
            httpURLConnection.setReadTimeout(50000);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            httpURLConnection.setRequestProperty("Content-Length", String.valueOf(bArr.length));
            httpURLConnection.setRequestProperty("Accept-Encoding", "gzip");
            httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
            httpURLConnection.connect();
            OutputStream outputStream = httpURLConnection.getOutputStream();
            outputStream.write(bArr);
            outputStream.flush();
            outputStream.close();
            if (httpURLConnection.getResponseCode() == 200) {
                return httpURLConnection;
            }
            throw new AMapException(AMapException.ERROR_CONNECTION);
        } catch (UnknownHostException e) {
            throw new AMapException(AMapException.ERROR_UNKNOW_HOST);
        } catch (MalformedURLException e2) {
            throw new AMapException(AMapException.ERROR_URL);
        } catch (ProtocolException e3) {
            throw new AMapException(AMapException.ERROR_PROTOCOL);
        } catch (SocketTimeoutException e4) {
            throw new AMapException(AMapException.ERROR_SOCKE_TIME_OUT);
        } catch (IOException e5) {
            throw new AMapException(AMapException.ERROR_IO);
        }
    }
}
