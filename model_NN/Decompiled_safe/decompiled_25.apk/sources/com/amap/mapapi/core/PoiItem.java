package com.amap.mapapi.core;

import android.os.Parcel;
import android.os.Parcelable;
import com.amap.mapapi.poisearch.PoiTypeDef;

public class PoiItem extends OverlayItem {
    public static final Parcelable.Creator<PoiItem> CREATOR = new k();
    public static final String DesSplit = " - ";
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;

    /* synthetic */ PoiItem(Parcel parcel, k kVar) {
        this(parcel);
    }

    public PoiItem(String str, GeoPoint geoPoint, String str2, String str3) {
        super(geoPoint, str2, str3);
        this.e = PoiTypeDef.All;
        this.a = str;
    }

    private PoiItem(Parcel parcel) {
        super(parcel);
        this.e = PoiTypeDef.All;
        this.a = parcel.readString();
        this.d = parcel.readString();
        this.c = parcel.readString();
        this.b = parcel.readString();
        this.e = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.a);
        parcel.writeString(this.d);
        parcel.writeString(this.c);
        parcel.writeString(this.b);
        parcel.writeString(this.e);
    }

    public String getTypeDes() {
        return this.e;
    }

    public void setTypeDes(String str) {
        this.e = str;
    }

    public String getXmlNode() {
        return this.f;
    }

    public void setXmlNode(String str) {
        this.f = str;
    }

    public String getTel() {
        return this.c;
    }

    public void setTel(String str) {
        this.c = str;
    }

    public String getAdCode() {
        return this.d;
    }

    public void setAdCode(String str) {
        this.d = str;
    }

    public String getPoiId() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (obj != null && obj.getClass() == getClass() && this.a == ((PoiItem) obj).a) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    public String toString() {
        return this.mTitle;
    }

    public String getTypeCode() {
        return this.b;
    }

    public void setTypeCode(String str) {
        this.b = str;
    }
}
