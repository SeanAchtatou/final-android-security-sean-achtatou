package com.amap.mapapi.core;

import java.io.IOException;
import java.io.InputStream;
import java.net.Proxy;
import java.util.ArrayList;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/* compiled from: XmlResultHandler */
public abstract class v<T, V> extends l<T, V> {
    private ArrayList<String> i = new ArrayList<>();

    /* access modifiers changed from: protected */
    public abstract V b(NodeList nodeList);

    public v(T t, Proxy proxy, String str, String str2) {
        super(t, proxy, str, str2);
    }

    /* access modifiers changed from: protected */
    public NodeList b(InputStream inputStream) throws AMapException {
        return d.b(d(inputStream)).getDocumentElement().getChildNodes();
    }

    /* access modifiers changed from: protected */
    public V c(InputStream inputStream) throws AMapException {
        V b = b(b(inputStream));
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e) {
                throw new AMapException(AMapException.ERROR_IO);
            }
        }
        return b;
    }

    /* access modifiers changed from: protected */
    public String a(Node node) {
        Node firstChild;
        if (node == null || (firstChild = node.getFirstChild()) == null || firstChild.getNodeType() != 3) {
            return null;
        }
        String nodeValue = firstChild.getNodeValue();
        int indexOf = nodeValue.indexOf("ppppppppShitJava");
        if (indexOf >= 0) {
            return this.i.get(Integer.parseInt(nodeValue.substring(indexOf + "ppppppppShitJava".length())));
        }
        return nodeValue;
    }
}
