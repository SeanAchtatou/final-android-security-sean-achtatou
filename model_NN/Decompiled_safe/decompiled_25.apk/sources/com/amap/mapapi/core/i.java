package com.amap.mapapi.core;

import com.amap.mapapi.poisearch.PoiTypeDef;
import java.util.Random;

/* compiled from: MapServerUrl */
public class i {
    private static i g;
    private String a = "http://webrd01.is.autonavi.com";
    private String b = "http://tm.mapabc.com";
    private String c = "http://api.amap.com:9090/sisserver";
    private String d = "http://ds.mapabc.com:8888";
    private String e = "http://mst01.is.autonavi.com";
    private String f = "http://tmds.mapabc.com";

    private i() {
    }

    public static synchronized i a() {
        i iVar;
        synchronized (i.class) {
            if (g == null) {
                g = new i();
            }
            iVar = g;
        }
        return iVar;
    }

    public String b() {
        String str = PoiTypeDef.All;
        switch (new Random(System.currentTimeMillis()).nextInt(100000) % 4) {
            case 0:
                str = "http://webrd01.is.autonavi.com";
                break;
            case 1:
                str = "http://webrd02.is.autonavi.com";
                break;
            case 2:
                str = "http://webrd03.is.autonavi.com";
                break;
            case 3:
                str = "http://webrd04.is.autonavi.com";
                break;
        }
        this.a = str;
        return this.a;
    }

    public String c() {
        return this.b;
    }

    public String d() {
        return this.c;
    }

    public String e() {
        String str = PoiTypeDef.All;
        switch (new Random(System.currentTimeMillis()).nextInt(100000) % 4) {
            case 0:
                str = "http://mst01.is.autonavi.com";
                break;
            case 1:
                str = "http://mst02.is.autonavi.com";
                break;
            case 2:
                str = "http://mst03.is.autonavi.com";
                break;
            case 3:
                str = "http://mst04.is.autonavi.com";
                break;
        }
        this.e = str;
        return this.e;
    }

    public void a(String str) {
        this.e = str;
    }

    public void b(String str) {
        this.f = str;
    }

    public String f() {
        return this.d;
    }

    public void c(String str) {
        this.a = str;
    }

    public void d(String str) {
        this.b = str;
    }

    public void e(String str) {
        this.c = str;
    }

    public void f(String str) {
        this.d = str;
    }
}
