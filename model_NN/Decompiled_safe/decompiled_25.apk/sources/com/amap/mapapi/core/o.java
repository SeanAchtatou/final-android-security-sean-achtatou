package com.amap.mapapi.core;

import android.location.Address;
import com.amap.mapapi.geocoder.Geocoder;
import com.amap.mapapi.map.i;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.Proxy;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/* compiled from: ReverseGeocodingHandler */
public class o extends t<p, Address> {
    private String i = null;
    private String j = null;
    private String k = null;
    private int l = 0;
    private ArrayList<Address> m;
    private ArrayList<Address> n;
    private ArrayList<Address> o;
    private boolean p;
    private boolean q;
    private boolean r;
    private String s = "<?xml version='1.0' encoding='utf-8' ?><spatial_request method='searchPoint'><x>%f</x><y>%f</y><poiNumber>%d</poiNumber><range>%d</range><pattern>0</pattern><roadLevel>0</roadLevel></spatial_request>";

    public o(p pVar, Proxy proxy, String str, String str2) {
        super(pVar, proxy, str, str2);
        this.l = pVar.c;
        this.m = new ArrayList<>();
        this.n = new ArrayList<>();
        this.o = new ArrayList<>();
    }

    /* access modifiers changed from: protected */
    public String[] f() {
        String str;
        String[] strArr = new String[2];
        strArr[0] = "&enc=utf-8";
        try {
            str = URLEncoder.encode(String.format(this.s, Double.valueOf(((p) this.b).a), Double.valueOf(((p) this.b).b), Integer.valueOf(((p) this.b).c), 500), "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            str = PoiTypeDef.All;
        }
        strArr[1] = "&spatialXml=" + str;
        return strArr;
    }

    /* access modifiers changed from: protected */
    public void a(Node node, ArrayList<Address> arrayList) {
        NodeList childNodes = node.getChildNodes();
        for (int i2 = 0; i2 < childNodes.getLength(); i2++) {
            b(childNodes.item(i2), arrayList);
        }
    }

    private void b(Node node, ArrayList<Address> arrayList) {
        String nodeName = node.getNodeName();
        if (nodeName.equals("Province")) {
            this.j = b(node);
        } else if (nodeName.equals("City")) {
            this.i = b(node);
        } else if (nodeName.equals("District")) {
            this.k = b(node);
        } else if (nodeName.equals("roadList")) {
            a(node, arrayList, "Road");
        } else if (nodeName.equals("poiList")) {
            a(node, arrayList, "poi");
        } else if (nodeName.equals("crossPoiList")) {
            a(node, arrayList, "cross");
            b(arrayList);
        }
    }

    private String b(Node node) {
        NodeList childNodes = node.getChildNodes();
        for (int i2 = 0; i2 < childNodes.getLength(); i2++) {
            Node item = childNodes.item(i2);
            if (item.getNodeType() == 1 && item.getNodeName().equals("name")) {
                return a(item);
            }
        }
        return null;
    }

    private void a(Node node, ArrayList<Address> arrayList, String str) {
        if (this.l > 0) {
            NodeList childNodes = node.getChildNodes();
            int min = Math.min(this.l, childNodes.getLength());
            for (int i2 = 0; i2 < min; i2++) {
                Node item = childNodes.item(i2);
                if (item.getNodeType() == 1) {
                    String nodeName = item.getNodeName();
                    if (nodeName.equals(str)) {
                        Address a = a(item, nodeName);
                        if (nodeName.equals("Road")) {
                            try {
                                Method method = a.getClass().getMethod("setPremises", String.class);
                                if (method != null) {
                                    method.invoke(a, Geocoder.Street_Road);
                                }
                            } catch (Exception e) {
                            }
                            if (a != null) {
                                this.m.add(a);
                                this.p = true;
                            }
                        } else if (nodeName.equals("poi")) {
                            try {
                                Method method2 = a.getClass().getMethod("setPremises", String.class);
                                if (method2 != null) {
                                    method2.invoke(a, Geocoder.POI);
                                }
                            } catch (Exception e2) {
                            }
                            if (a != null) {
                                this.n.add(a);
                                this.q = true;
                            }
                        } else if (nodeName.equals("cross")) {
                            try {
                                Method method3 = a.getClass().getMethod("setPremises", String.class);
                                if (method3 != null) {
                                    method3.invoke(a, Geocoder.Cross);
                                }
                            } catch (Exception e3) {
                            }
                            if (a != null) {
                                this.o.add(a);
                                this.r = true;
                            }
                        }
                    }
                }
            }
        }
    }

    private ArrayList<Address> b(ArrayList<Address> arrayList) {
        if (this.l > 0) {
            if (this.p) {
                arrayList.add(this.m.get(0));
                this.m.remove(0);
            }
            a(arrayList, this.n);
            if (this.l - arrayList.size() > 0 && this.r) {
                arrayList.add(this.o.get(0));
            }
            a(arrayList, this.m);
        }
        return arrayList;
    }

    private void a(ArrayList<Address> arrayList, ArrayList<Address> arrayList2) {
        int size = arrayList2.size();
        int size2 = this.l - arrayList.size();
        for (int i2 = 0; i2 < size2; i2++) {
            if (size > i2) {
                arrayList.add(arrayList2.get(i2));
            }
        }
    }

    private Address a(Node node, String str) {
        if (!node.hasChildNodes()) {
            return null;
        }
        Address b = d.b();
        NodeList childNodes = node.getChildNodes();
        for (int i2 = 0; i2 < childNodes.getLength(); i2++) {
            Node item = childNodes.item(i2);
            String nodeName = item.getNodeName();
            String a = a(item);
            if (item.getNodeType() == 1 && !d.a(a)) {
                a(nodeName, a, b);
            }
        }
        return b;
    }

    private void a(String str, String str2, Address address) {
        if (str.equals("address")) {
            address.setAddressLine(2, str2);
        } else if (str.equals("tel")) {
            address.setPhone(str2);
        } else if (str.equals("name")) {
            address.setFeatureName(str2);
        } else {
            b(str, str2, address);
        }
    }

    private void b(String str, String str2, Address address) {
        if (str.equals("x")) {
            address.setLongitude(Double.parseDouble(str2));
        } else if (str.equals("y")) {
            address.setLatitude(Double.parseDouble(str2));
        }
    }

    /* access modifiers changed from: protected */
    public void a(ArrayList<Address> arrayList) {
        if (this.j != null && arrayList.size() == 0) {
            arrayList.add(d.b());
        }
        Iterator<Address> it = arrayList.iterator();
        while (it.hasNext()) {
            Address next = it.next();
            next.setAdminArea(this.j);
            next.setLocality(this.i);
            try {
                Method method = next.getClass().getMethod("setSubLocality", String.class);
                if (method != null) {
                    method.invoke(next, this.k);
                }
            } catch (Exception e) {
            }
            next.setAddressLine(0, "中国");
            if (!this.j.equals(this.i)) {
                next.setAddressLine(1, this.j + this.i + this.k);
            } else {
                next.setAddressLine(1, this.i + this.k);
            }
        }
    }

    /* access modifiers changed from: protected */
    public String h() {
        if (e()) {
            return i.a().d() + "?&config=SPAS&resType=xml";
        }
        return i.a().d();
    }

    private String a(JSONObject jSONObject) throws JSONException {
        return jSONObject.getString("name");
    }

    private void a(JSONObject jSONObject, String str) throws Exception {
        String substring;
        String str2;
        int i2 = 0;
        JSONArray jSONArray = jSONObject.getJSONArray(str);
        if (str.equals("poiList")) {
            for (int i3 = 0; i3 < jSONArray.length(); i3++) {
                Address b = d.b();
                JSONObject jSONObject2 = jSONArray.getJSONObject(i3);
                b.setFeatureName(jSONObject2.getString("name"));
                b.setLatitude(jSONObject2.getDouble("y"));
                b.setLongitude(jSONObject2.getDouble("x"));
                String string = jSONObject2.getString("tel");
                if (string != null && !string.equals(PoiTypeDef.All)) {
                    b.setPhone(string);
                }
                String string2 = jSONObject2.getString("address");
                if (string2 != null && !string2.equals(PoiTypeDef.All)) {
                    if (Character.isDigit(string2.charAt(string2.length() - 1))) {
                        string2 = string2 + "号";
                    }
                    b.setAddressLine(2, string2);
                    int indexOf = string2.indexOf(36335);
                    if (indexOf != -1) {
                        str2 = string2.substring(0, indexOf + 1);
                        substring = string2.substring(indexOf + 1);
                    } else {
                        int indexOf2 = string2.indexOf(34903);
                        if (indexOf2 != -1) {
                            str2 = string2.substring(0, indexOf2 + 1);
                            substring = string2.substring(indexOf2 + 1);
                        } else {
                            int length = string2.length();
                            int i4 = 0;
                            while (true) {
                                if (i4 >= string2.length()) {
                                    i4 = length;
                                    break;
                                } else if (Character.isDigit(string2.charAt(i4))) {
                                    break;
                                } else {
                                    i4++;
                                }
                            }
                            String substring2 = string2.substring(0, i4);
                            substring = string2.substring(i4);
                            str2 = substring2;
                        }
                    }
                    b.setThoroughfare(str2);
                    try {
                        Method method = b.getClass().getMethod("setSubThoroughfare", String.class);
                        if (!(method == null || substring == null || substring.equals(PoiTypeDef.All))) {
                            method.invoke(b, substring);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                try {
                    Method method2 = b.getClass().getMethod("setPremises", String.class);
                    if (method2 != null) {
                        method2.invoke(b, Geocoder.POI);
                    }
                } catch (Exception e2) {
                }
                if (b != null) {
                    this.n.add(b);
                    this.q = true;
                }
            }
        } else if (str.equals("crossPoiList")) {
            while (i2 < jSONArray.length()) {
                Address b2 = d.b();
                JSONObject jSONObject3 = jSONArray.getJSONObject(i2);
                b2.setFeatureName(jSONObject3.getString("name"));
                b2.setLatitude(jSONObject3.getDouble("y"));
                b2.setLongitude(jSONObject3.getDouble("x"));
                try {
                    Method method3 = b2.getClass().getMethod("setPremises", String.class);
                    if (method3 != null) {
                        method3.invoke(b2, Geocoder.Cross);
                    }
                } catch (Exception e3) {
                }
                if (b2 != null) {
                    this.o.add(b2);
                    this.r = true;
                }
                i2++;
            }
        } else if (str.equals("roadList")) {
            while (i2 < jSONArray.length()) {
                Address b3 = d.b();
                JSONObject jSONObject4 = jSONArray.getJSONObject(i2);
                String string3 = jSONObject4.getString("name");
                b3.setFeatureName(string3);
                b3.setLatitude(jSONObject4.getDouble("y"));
                b3.setLongitude(jSONObject4.getDouble("x"));
                b3.setAddressLine(2, string3);
                b3.setThoroughfare(string3);
                try {
                    Method method4 = b3.getClass().getMethod("setPremises", String.class);
                    if (method4 != null) {
                        method4.invoke(b3, Geocoder.Street_Road);
                    }
                } catch (Exception e4) {
                }
                if (b3 != null) {
                    this.m.add(b3);
                    this.p = true;
                }
                i2++;
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ArrayList<Address> c(InputStream inputStream) throws AMapException {
        String str;
        if (e()) {
            return super.c(inputStream);
        }
        ArrayList<Address> arrayList = new ArrayList<>();
        try {
            str = new String(i.a(inputStream));
        } catch (Exception e) {
            e.printStackTrace();
            str = null;
        }
        try {
            JSONObject jSONObject = new JSONObject(str).getJSONObject("SpatialBean");
            this.k = a(jSONObject.getJSONObject("District"));
            this.j = a(jSONObject.getJSONObject("Province"));
            this.i = a(jSONObject.getJSONObject("City"));
            if (this.i == null || this.i.equals(PoiTypeDef.All)) {
                this.i = this.j;
            }
            try {
                a(jSONObject, "roadList");
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            if (this.m.size() > 0) {
                a(this.m);
            }
            try {
                a(jSONObject, "crossPoiList");
            } catch (Exception e3) {
                e3.printStackTrace();
            }
            if (this.o.size() > 0) {
                a(this.o);
            }
            try {
                a(jSONObject, "poiList");
            } catch (Exception e4) {
                e4.printStackTrace();
            }
            if (this.n.size() > 0) {
                a(this.n);
            }
        } catch (Exception e5) {
            e5.printStackTrace();
        }
        if (this.r || this.q || this.p) {
            return b(arrayList);
        }
        Address b = d.b();
        b.setAdminArea(this.j);
        b.setLocality(this.i);
        b.setFeatureName(this.k);
        b.setLatitude(((p) this.b).b);
        b.setLongitude(((p) this.b).a);
        try {
            Method method = b.getClass().getMethod("setSubLocality", String.class);
            if (method != null) {
                method.invoke(b, this.k);
            }
        } catch (Exception e6) {
        }
        b.setAddressLine(0, "中国");
        if (!this.j.equals(this.i)) {
            b.setAddressLine(1, this.j + this.i + this.k);
        } else {
            b.setAddressLine(1, this.i + this.k);
        }
        arrayList.add(b);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public boolean e() {
        return false;
    }

    /* access modifiers changed from: protected */
    public byte[] g() {
        String str;
        StringBuilder sb = new StringBuilder();
        sb.append("config=SPAS&resType=json&enc=utf-8&spatialXml=");
        try {
            str = String.format("<?xml version=\"1.0\" encoding=\"utf-8\"?><spatial_request method=\"searchPoint\"><x>%f</x><y>%f</y><poinumber>%d</poinumber><roadnumber>10</roadnumber><crossnumber>1</crossnumber><range>500</range><pattern>10</pattern></spatial_request>", Double.valueOf(((p) this.b).a), Double.valueOf(((p) this.b).b), Integer.valueOf(((p) this.b).c));
        } catch (Exception e) {
            e.printStackTrace();
            str = PoiTypeDef.All;
        }
        sb.append(str);
        a a = a.a(null);
        sb.append("&a_k=");
        sb.append(a.a());
        return sb.toString().getBytes();
    }
}
