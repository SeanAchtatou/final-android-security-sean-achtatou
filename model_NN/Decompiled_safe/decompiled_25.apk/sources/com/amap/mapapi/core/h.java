package com.amap.mapapi.core;

import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.map.i;
import java.io.InputStream;
import java.net.Proxy;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/* compiled from: LocTansServerHandler */
public class h extends v<GeoPoint.b, GeoPoint.b> {
    private GeoPoint.b i;

    public h(GeoPoint.b bVar, Proxy proxy, String str, String str2) {
        super(bVar, proxy, str, str2);
        this.i = bVar;
    }

    /* access modifiers changed from: protected */
    public String[] f() {
        return new String[]{"&enc=utf-8", "&x1=" + String.format("%f", Double.valueOf(((GeoPoint.b) this.b).a)), "&y1=" + String.format("%f", Double.valueOf(((GeoPoint.b) this.b).b))};
    }

    /* access modifiers changed from: protected */
    public String h() {
        if (e()) {
            return i.a().d() + "?config=RGC&resType=xml&flag=true";
        }
        return i.a().d();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public GeoPoint.b b(NodeList nodeList) {
        GeoPoint.b bVar = this.i;
        int length = nodeList.getLength();
        for (int i2 = 0; i2 < length; i2++) {
            a(nodeList.item(i2), bVar);
        }
        return bVar;
    }

    private GeoPoint.b a(Node node, GeoPoint.b bVar) {
        if (node.getNodeType() == 1 && node.getNodeName().equals("Item")) {
            NodeList childNodes = node.getChildNodes();
            for (int i2 = 0; i2 < childNodes.getLength(); i2++) {
                Node item = childNodes.item(i2);
                if (item.getNodeName().equals("x")) {
                    bVar.a = Double.parseDouble(a(item));
                } else if (item.getNodeName().equals("y")) {
                    bVar.b = Double.parseDouble(a(item));
                }
            }
        }
        return bVar;
    }

    /* access modifiers changed from: protected */
    public boolean e() {
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public GeoPoint.b c(InputStream inputStream) throws AMapException {
        String str;
        if (e()) {
            return (GeoPoint.b) super.c(inputStream);
        }
        GeoPoint.b bVar = this.i;
        try {
            str = new String(i.a(inputStream));
        } catch (Exception e) {
            e.printStackTrace();
            str = null;
        }
        try {
            JSONArray jSONArray = new JSONObject(str).getJSONArray("list");
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                JSONObject jSONObject = jSONArray.getJSONObject(i2);
                bVar.a = jSONObject.getDouble("x");
                bVar.b = jSONObject.getDouble("y");
            }
            return bVar;
        } catch (JSONException e2) {
            e2.printStackTrace();
            return bVar;
        }
    }

    /* access modifiers changed from: protected */
    public byte[] g() {
        StringBuilder sb = new StringBuilder();
        sb.append("config=RGC&resType=json&flag=true&enc=utf-8&coors=");
        sb.append(((GeoPoint.b) this.b).a);
        sb.append(",");
        sb.append(((GeoPoint.b) this.b).b);
        a a = a.a(null);
        sb.append("&a_k=");
        sb.append(a.a());
        return sb.toString().getBytes();
    }
}
