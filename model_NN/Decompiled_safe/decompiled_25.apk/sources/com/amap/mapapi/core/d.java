package com.amap.mapapi.core;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.Html;
import android.text.Spanned;
import com.amap.mapapi.location.LocationProviderProxy;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.Locale;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;

/* compiled from: CoreUtil */
public class d {
    public static boolean a = true;
    static float[] b = new float[9];
    private static String c = null;

    public static boolean a(String str) {
        return str == null || str.trim().length() == 0;
    }

    public static long a() {
        return (System.nanoTime() * 1000) / 1000000000;
    }

    public static int a(int i) {
        return (int) ((1117 * ((long) i)) / 10000);
    }

    public static int b(int i) {
        return (int) ((((long) i) * 1000000) / 111700);
    }

    public static Document a(InputStream inputStream) {
        try {
            return DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputStream);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Document b(String str) {
        return a(new ByteArrayInputStream(str.getBytes()));
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v6, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v17, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v18, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(android.content.Context r6) {
        /*
            r0 = 0
            java.lang.String r1 = com.amap.mapapi.core.d.c
            if (r1 != 0) goto L_0x008a
            r1 = 16
            char[] r3 = new char[r1]
            r3 = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70} // fill-array
            java.lang.String r1 = ""
            android.content.pm.PackageManager r2 = r6.getPackageManager()     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            java.lang.String r4 = r6.getPackageName()     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            r5 = 64
            android.content.pm.PackageInfo r2 = r2.getPackageInfo(r4, r5)     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            android.content.pm.Signature[] r2 = r2.signatures     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            java.lang.String r4 = "MD5"
            java.security.MessageDigest r4 = java.security.MessageDigest.getInstance(r4)     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            r5 = 0
            r2 = r2[r5]     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            byte[] r2 = r2.toByteArray()     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            r4.update(r2)     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            byte[] r4 = r4.digest()     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            r2 = r1
            r1 = r0
        L_0x0034:
            int r0 = r4.length     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            if (r1 >= r0) goto L_0x0088
            byte r0 = r4[r1]     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            if (r0 >= 0) goto L_0x0085
            byte r0 = r4[r1]     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            int r0 = r0 + 256
        L_0x003f:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            r5.<init>()     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            java.lang.StringBuilder r2 = r5.append(r2)     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            int r5 = r0 / 16
            char r5 = r3[r5]     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            java.lang.String r2 = r2.toString()     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            r5.<init>()     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            java.lang.StringBuilder r2 = r5.append(r2)     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            int r0 = r0 % 16
            char r0 = r3[r0]     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            java.lang.String r0 = r0.toString()     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            int r2 = r4.length     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            int r2 = r2 + -1
            if (r1 == r2) goto L_0x0081
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            r2.<init>()     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            java.lang.String r2 = ":"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            java.lang.String r0 = r0.toString()     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
        L_0x0081:
            int r1 = r1 + 1
            r2 = r0
            goto L_0x0034
        L_0x0085:
            byte r0 = r4[r1]     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            goto L_0x003f
        L_0x0088:
            com.amap.mapapi.core.d.c = r2     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
        L_0x008a:
            java.lang.String r0 = com.amap.mapapi.core.d.c
            return r0
        L_0x008d:
            r0 = move-exception
            goto L_0x008a
        L_0x008f:
            r0 = move-exception
            goto L_0x008a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.mapapi.core.d.a(android.content.Context):java.lang.String");
    }

    public static Proxy b(Context context) {
        String defaultHost;
        int defaultPort;
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null) {
            if (activeNetworkInfo.getType() == 1) {
                defaultHost = android.net.Proxy.getHost(context);
                defaultPort = android.net.Proxy.getPort(context);
            } else {
                defaultHost = android.net.Proxy.getDefaultHost();
                defaultPort = android.net.Proxy.getDefaultPort();
            }
            if (defaultHost != null) {
                return new Proxy(Proxy.Type.HTTP, new InetSocketAddress(defaultHost, defaultPort));
            }
        }
        return null;
    }

    public static long a(double d) {
        return (long) (1000000.0d * d);
    }

    public static double a(long j) {
        return ((double) j) / 1000000.0d;
    }

    public static Address b() {
        Address address = new Address(Locale.CHINA);
        address.setCountryCode("CN");
        address.setCountryName("中国");
        return address;
    }

    public static String c(int i) {
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 < i; i2++) {
            sb.append("&nbsp;");
        }
        return sb.toString();
    }

    public static String c() {
        return "<br />";
    }

    public static Spanned c(String str) {
        if (str == null) {
            return null;
        }
        return Html.fromHtml(str.replace("\n", "<br />"));
    }

    public static String a(String str, String str2) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("<font color=").append(str2).append(">").append(str).append("</font>");
        return stringBuffer.toString();
    }

    public static boolean c(Context context) {
        if (context == null) {
            return false;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            return false;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return false;
        }
        NetworkInfo.State state = activeNetworkInfo.getState();
        if (state == null || state == NetworkInfo.State.DISCONNECTED || state == NetworkInfo.State.DISCONNECTING) {
            return false;
        }
        return true;
    }

    public static Location d(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("last_know_location", 0);
        Location location = new Location(PoiTypeDef.All);
        location.setProvider(LocationProviderProxy.MapABCNetwork);
        double parseDouble = Double.parseDouble(sharedPreferences.getString("last_know_lat", "0.0"));
        double parseDouble2 = Double.parseDouble(sharedPreferences.getString("last_know_lng", "0.0"));
        location.setLatitude(parseDouble);
        location.setLongitude(parseDouble2);
        return location;
    }

    public static void a(Context context, Location location) {
        SharedPreferences.Editor edit = context.getSharedPreferences("last_know_location", 0).edit();
        edit.putString("last_know_lat", String.valueOf(location.getLatitude()));
        edit.putString("last_know_lng", String.valueOf(location.getLongitude()));
        edit.commit();
    }
}
