package com.amap.mapapi.core;

import com.amap.mapapi.poisearch.PoiTypeDef;
import com.umeng.common.util.e;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.UnknownHostException;
import java.net.UnknownServiceException;
import java.util.zip.GZIPInputStream;

/* compiled from: ProtocalHandler */
public abstract class l<T, V> {
    protected Proxy a;
    protected T b;
    protected int c = 1;
    protected int d = 20;
    protected int e = 0;
    protected int f = 0;
    protected String g;
    protected String h = PoiTypeDef.All;

    /* access modifiers changed from: protected */
    public abstract V c(InputStream inputStream) throws AMapException;

    /* access modifiers changed from: protected */
    public abstract boolean e();

    /* access modifiers changed from: protected */
    public abstract String[] f();

    /* access modifiers changed from: protected */
    public abstract byte[] g();

    /* access modifiers changed from: protected */
    public abstract String h();

    public l(T t, Proxy proxy, String str, String str2) {
        this.a = proxy;
        this.b = t;
        this.c = 1;
        this.d = 5;
        this.e = 2;
        this.g = str2;
    }

    /* access modifiers changed from: protected */
    public byte[] i() {
        return g();
    }

    private String a() {
        String[] f2 = f();
        if (f2 == null) {
            return PoiTypeDef.All;
        }
        StringBuilder sb = new StringBuilder();
        if (f2 != null) {
            for (String append : f2) {
                sb.append(append);
            }
        }
        return sb.toString();
    }

    public V j() throws AMapException {
        if (this.b != null) {
            return b();
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:52:0x00a8 A[SYNTHETIC, Splitter:B:52:0x00a8] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00ad A[SYNTHETIC, Splitter:B:55:0x00ad] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00b2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private V b() throws com.amap.mapapi.core.AMapException {
        /*
            r10 = this;
            r4 = 0
            r0 = 0
            r1 = r4
            r5 = r4
            r2 = r4
            r3 = r4
        L_0x0006:
            int r6 = r10.c
            if (r0 >= r6) goto L_0x00e7
            boolean r6 = r10.e()     // Catch:{ AMapException -> 0x0075, all -> 0x00e8 }
            if (r6 != 0) goto L_0x003f
            java.lang.String r6 = r10.h()     // Catch:{ AMapException -> 0x0075, all -> 0x00e8 }
            r10.h = r6     // Catch:{ AMapException -> 0x0075, all -> 0x00e8 }
            byte[] r6 = r10.i()     // Catch:{ AMapException -> 0x0075, all -> 0x00e8 }
            java.lang.String r7 = r10.h     // Catch:{ AMapException -> 0x0075, all -> 0x00e8 }
            java.net.Proxy r8 = r10.a     // Catch:{ AMapException -> 0x0075, all -> 0x00e8 }
            java.net.HttpURLConnection r3 = com.amap.mapapi.core.f.a(r7, r6, r8)     // Catch:{ AMapException -> 0x0075, all -> 0x00e8 }
        L_0x0022:
            java.io.InputStream r6 = r10.a(r3)     // Catch:{ AMapException -> 0x0075, all -> 0x00e8 }
            java.lang.Object r1 = r10.a(r6)     // Catch:{ AMapException -> 0x00ea }
            int r0 = r10.c     // Catch:{ AMapException -> 0x00ea }
            if (r6 == 0) goto L_0x00ee
            r6.close()     // Catch:{ IOException -> 0x0063 }
            r2 = r4
        L_0x0032:
            if (r4 == 0) goto L_0x0037
            r5.close()     // Catch:{ IOException -> 0x006c }
        L_0x0037:
            if (r3 == 0) goto L_0x003d
            r3.disconnect()
            r3 = r4
        L_0x003d:
            r5 = r4
            goto L_0x0006
        L_0x003f:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ AMapException -> 0x0075, all -> 0x00e8 }
            r6.<init>()     // Catch:{ AMapException -> 0x0075, all -> 0x00e8 }
            java.lang.String r7 = r10.h()     // Catch:{ AMapException -> 0x0075, all -> 0x00e8 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ AMapException -> 0x0075, all -> 0x00e8 }
            java.lang.String r7 = r10.a()     // Catch:{ AMapException -> 0x0075, all -> 0x00e8 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ AMapException -> 0x0075, all -> 0x00e8 }
            java.lang.String r6 = r6.toString()     // Catch:{ AMapException -> 0x0075, all -> 0x00e8 }
            r10.h = r6     // Catch:{ AMapException -> 0x0075, all -> 0x00e8 }
            java.lang.String r6 = r10.h     // Catch:{ AMapException -> 0x0075, all -> 0x00e8 }
            java.net.Proxy r7 = r10.a     // Catch:{ AMapException -> 0x0075, all -> 0x00e8 }
            java.net.HttpURLConnection r3 = com.amap.mapapi.core.f.a(r6, r7)     // Catch:{ AMapException -> 0x0075, all -> 0x00e8 }
            goto L_0x0022
        L_0x0063:
            r0 = move-exception
            com.amap.mapapi.core.AMapException r0 = new com.amap.mapapi.core.AMapException
            java.lang.String r1 = "IO 操作异常 - IOException"
            r0.<init>(r1)
            throw r0
        L_0x006c:
            r0 = move-exception
            com.amap.mapapi.core.AMapException r0 = new com.amap.mapapi.core.AMapException
            java.lang.String r1 = "IO 操作异常 - IOException"
            r0.<init>(r1)
            throw r0
        L_0x0075:
            r6 = move-exception
            r9 = r6
            r6 = r2
            r2 = r9
        L_0x0079:
            int r0 = r0 + 1
            int r7 = r10.c     // Catch:{ all -> 0x00a4 }
            if (r0 >= r7) goto L_0x00b6
            int r2 = r10.e     // Catch:{ InterruptedException -> 0x0099 }
            int r2 = r2 * 1000
            long r7 = (long) r2     // Catch:{ InterruptedException -> 0x0099 }
            java.lang.Thread.sleep(r7)     // Catch:{ InterruptedException -> 0x0099 }
            if (r6 == 0) goto L_0x00ec
            r6.close()     // Catch:{ IOException -> 0x00c3 }
            r2 = r4
        L_0x008d:
            if (r4 == 0) goto L_0x0092
            r5.close()     // Catch:{ IOException -> 0x00cc }
        L_0x0092:
            if (r3 == 0) goto L_0x003d
            r3.disconnect()
            r3 = r4
            goto L_0x003d
        L_0x0099:
            r0 = move-exception
            com.amap.mapapi.core.AMapException r1 = new com.amap.mapapi.core.AMapException     // Catch:{ all -> 0x00a4 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x00a4 }
            r1.<init>(r0)     // Catch:{ all -> 0x00a4 }
            throw r1     // Catch:{ all -> 0x00a4 }
        L_0x00a4:
            r0 = move-exception
            r2 = r6
        L_0x00a6:
            if (r2 == 0) goto L_0x00ab
            r2.close()     // Catch:{ IOException -> 0x00d5 }
        L_0x00ab:
            if (r4 == 0) goto L_0x00b0
            r5.close()     // Catch:{ IOException -> 0x00de }
        L_0x00b0:
            if (r3 == 0) goto L_0x00b5
            r3.disconnect()
        L_0x00b5:
            throw r0
        L_0x00b6:
            r10.k()     // Catch:{ all -> 0x00a4 }
            com.amap.mapapi.core.AMapException r0 = new com.amap.mapapi.core.AMapException     // Catch:{ all -> 0x00a4 }
            java.lang.String r1 = r2.getErrorMessage()     // Catch:{ all -> 0x00a4 }
            r0.<init>(r1)     // Catch:{ all -> 0x00a4 }
            throw r0     // Catch:{ all -> 0x00a4 }
        L_0x00c3:
            r0 = move-exception
            com.amap.mapapi.core.AMapException r0 = new com.amap.mapapi.core.AMapException
            java.lang.String r1 = "IO 操作异常 - IOException"
            r0.<init>(r1)
            throw r0
        L_0x00cc:
            r0 = move-exception
            com.amap.mapapi.core.AMapException r0 = new com.amap.mapapi.core.AMapException
            java.lang.String r1 = "IO 操作异常 - IOException"
            r0.<init>(r1)
            throw r0
        L_0x00d5:
            r0 = move-exception
            com.amap.mapapi.core.AMapException r0 = new com.amap.mapapi.core.AMapException
            java.lang.String r1 = "IO 操作异常 - IOException"
            r0.<init>(r1)
            throw r0
        L_0x00de:
            r0 = move-exception
            com.amap.mapapi.core.AMapException r0 = new com.amap.mapapi.core.AMapException
            java.lang.String r1 = "IO 操作异常 - IOException"
            r0.<init>(r1)
            throw r0
        L_0x00e7:
            return r1
        L_0x00e8:
            r0 = move-exception
            goto L_0x00a6
        L_0x00ea:
            r2 = move-exception
            goto L_0x0079
        L_0x00ec:
            r2 = r6
            goto L_0x008d
        L_0x00ee:
            r2 = r6
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.mapapi.core.l.b():java.lang.Object");
    }

    /* access modifiers changed from: protected */
    public InputStream a(HttpURLConnection httpURLConnection) throws AMapException {
        try {
            PushbackInputStream pushbackInputStream = new PushbackInputStream(httpURLConnection.getInputStream(), 2);
            byte[] bArr = new byte[2];
            pushbackInputStream.read(bArr);
            pushbackInputStream.unread(bArr);
            if (bArr[0] == 31 && bArr[1] == -117) {
                return new GZIPInputStream(pushbackInputStream);
            }
            return pushbackInputStream;
        } catch (ProtocolException e2) {
            throw new AMapException(AMapException.ERROR_PROTOCOL);
        } catch (UnknownHostException e3) {
            throw new AMapException(AMapException.ERROR_UNKNOW_HOST);
        } catch (UnknownServiceException e4) {
            throw new AMapException(AMapException.ERROR_UNKNOW_SERVICE);
        } catch (IOException e5) {
            throw new AMapException(AMapException.ERROR_IO);
        }
    }

    private V a(InputStream inputStream) throws AMapException {
        return c(inputStream);
    }

    /* access modifiers changed from: protected */
    public V k() {
        return null;
    }

    /* access modifiers changed from: protected */
    public String d(InputStream inputStream) throws AMapException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            byte[] bArr = new byte[1024];
            while (true) {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    break;
                }
                byteArrayOutputStream.write(bArr, 0, read);
                byteArrayOutputStream.flush();
            }
            if (byteArrayOutputStream != null) {
                try {
                    byteArrayOutputStream.close();
                } catch (IOException e2) {
                    throw new AMapException(AMapException.ERROR_IO);
                }
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e3) {
                    throw new AMapException(AMapException.ERROR_IO);
                }
            }
            try {
                return new String(byteArrayOutputStream.toByteArray(), e.f);
            } catch (UnsupportedEncodingException e4) {
                e4.printStackTrace();
                return new String();
            }
        } catch (IOException e5) {
            throw new AMapException(AMapException.ERROR_IO);
        } catch (Throwable th) {
            if (byteArrayOutputStream != null) {
                try {
                    byteArrayOutputStream.close();
                } catch (IOException e6) {
                    throw new AMapException(AMapException.ERROR_IO);
                }
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e7) {
                    throw new AMapException(AMapException.ERROR_IO);
                }
            }
            throw th;
        }
    }
}
