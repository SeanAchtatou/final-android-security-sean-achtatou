package com.amap.mapapi.core;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import com.amap.mapapi.poisearch.PoiTypeDef;

public class GeoPoint implements Parcelable {
    public static final Parcelable.Creator<GeoPoint> CREATOR = new e();
    static final double[] a = {0.7111111111111111d, 1.4222222222222223d, 2.8444444444444446d, 5.688888888888889d, 11.377777777777778d, 22.755555555555556d, 45.51111111111111d, 91.02222222222223d, 182.04444444444445d, 364.0888888888889d, 728.1777777777778d, 1456.3555555555556d, 2912.711111111111d, 5825.422222222222d, 11650.844444444445d, 23301.68888888889d, 46603.37777777778d, 93206.75555555556d, 186413.51111111112d, 372827.02222222224d, 745654.0444444445d};
    static final double[] b = {40.74366543152521d, 81.48733086305042d, 162.97466172610083d, 325.94932345220167d, 651.8986469044033d, 1303.7972938088067d, 2607.5945876176133d, 5215.189175235227d, 10430.378350470453d, 20860.756700940907d, 41721.51340188181d, 83443.02680376363d, 166886.05360752725d, 333772.1072150545d, 667544.214430109d, 1335088.428860218d, 2670176.857720436d, 5340353.715440872d, 1.0680707430881744E7d, 2.136141486176349E7d, 4.272282972352698E7d};
    static final a[] c = {new a(128, 128), new a(256, 256), new a(512, 512), new a(1024, 1024), new a(2048, 2048), new a(4096, 4096), new a(8192, 8192), new a(16384, 16384), new a(32768, 32768), new a(AccessibilityEventCompat.TYPE_VIEW_ACCESSIBILITY_FOCUS_CLEARED, AccessibilityEventCompat.TYPE_VIEW_ACCESSIBILITY_FOCUS_CLEARED), new a(AccessibilityEventCompat.TYPE_VIEW_TEXT_TRAVERSED_AT_MOVEMENT_GRANULARITY, AccessibilityEventCompat.TYPE_VIEW_TEXT_TRAVERSED_AT_MOVEMENT_GRANULARITY), new a(AccessibilityEventCompat.TYPE_GESTURE_DETECTION_START, AccessibilityEventCompat.TYPE_GESTURE_DETECTION_START), new a(AccessibilityEventCompat.TYPE_GESTURE_DETECTION_END, AccessibilityEventCompat.TYPE_GESTURE_DETECTION_END), new a(AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START, AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START), new a(AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_END, AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_END), new a(4194304, 4194304), new a(GravityCompat.RELATIVE_LAYOUT_DIRECTION, GravityCompat.RELATIVE_LAYOUT_DIRECTION), new a(16777216, 16777216), new a(33554432, 33554432), new a(67108864, 67108864), new a(134217728, 134217728)};
    private long d;
    private long e;
    private double f;
    private double g;

    public enum EnumMapProjection {
        projection_900913,
        projection_custBeijing54
    }

    /* synthetic */ GeoPoint(Parcel parcel, e eVar) {
        this(parcel);
    }

    public GeoPoint() {
        this.d = Long.MIN_VALUE;
        this.e = Long.MIN_VALUE;
        this.f = Double.MIN_VALUE;
        this.g = Double.MIN_VALUE;
        this.d = 0;
        this.e = 0;
    }

    public GeoPoint(int i, int i2) {
        this.d = Long.MIN_VALUE;
        this.e = Long.MIN_VALUE;
        this.f = Double.MIN_VALUE;
        this.g = Double.MIN_VALUE;
        this.d = (long) i;
        this.e = (long) i2;
    }

    public GeoPoint(long j, long j2) {
        this.d = Long.MIN_VALUE;
        this.e = Long.MIN_VALUE;
        this.f = Double.MIN_VALUE;
        this.g = Double.MIN_VALUE;
        this.d = j;
        this.e = j2;
    }

    public GeoPoint(double d2, double d3, boolean z) {
        this.d = Long.MIN_VALUE;
        this.e = Long.MIN_VALUE;
        this.f = Double.MIN_VALUE;
        this.g = Double.MIN_VALUE;
        if (z) {
            this.d = (long) (d2 * 1000000.0d);
            this.e = (long) (d3 * 1000000.0d);
            return;
        }
        this.f = d2;
        this.g = d3;
    }

    public void a(double d2) {
        this.g = d2;
    }

    public void b(double d2) {
        this.f = d2;
    }

    public GeoPoint(double d2, double d3, long j, long j2) {
        this.d = Long.MIN_VALUE;
        this.e = Long.MIN_VALUE;
        this.f = Double.MIN_VALUE;
        this.g = Double.MIN_VALUE;
        this.f = d2;
        this.g = d3;
        this.d = j;
        this.e = j2;
    }

    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        GeoPoint geoPoint = (GeoPoint) obj;
        if (this.f == geoPoint.f && this.g == geoPoint.g && this.d == geoPoint.d && this.e == geoPoint.e) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return (int) ((this.g * 7.0d) + (this.f * 11.0d));
    }

    public String toString() {
        return PoiTypeDef.All + this.d + "," + this.e;
    }

    public int getLongitudeE6() {
        return (int) this.e;
    }

    public int getLatitudeE6() {
        return (int) this.d;
    }

    public long a() {
        return this.e;
    }

    public long b() {
        return this.d;
    }

    public double c() {
        if (b.h != EnumMapProjection.projection_custBeijing54) {
            if (b.h == EnumMapProjection.projection_900913 && this.g == Double.MIN_VALUE) {
                this.g = (d.a(this.e) * 2.003750834E7d) / 180.0d;
            }
            return this.g;
        } else if (this.g != Double.MIN_VALUE) {
            return this.g;
        } else {
            return d.a(this.e);
        }
    }

    public double d() {
        if (b.h != EnumMapProjection.projection_custBeijing54) {
            if (b.h == EnumMapProjection.projection_900913 && this.f == Double.MIN_VALUE) {
                this.f = ((Math.log(Math.tan(((d.a(this.d) + 90.0d) * 3.141592653589793d) / 360.0d)) / 0.017453292519943295d) * 2.003750834E7d) / 180.0d;
            }
            return this.f;
        } else if (this.f != Double.MIN_VALUE) {
            return this.f;
        } else {
            return d.a(this.d);
        }
    }

    public GeoPoint e() {
        return new GeoPoint(this.f, this.g, this.d, this.e);
    }

    public String f() {
        return PoiTypeDef.All + d.a(this.d) + "," + d.a(this.e);
    }

    private GeoPoint(Parcel parcel) {
        this.d = Long.MIN_VALUE;
        this.e = Long.MIN_VALUE;
        this.f = Double.MIN_VALUE;
        this.g = Double.MIN_VALUE;
        this.d = parcel.readLong();
        this.e = parcel.readLong();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.d);
        parcel.writeLong(this.e);
    }

    public static class a {
        public int a;
        public int b;

        public a(int i, int i2) {
            this.a = i;
            this.b = i2;
        }
    }

    public static class b {
        public double a;
        public double b;

        public b(double d, double d2) {
            this.a = d;
            this.b = d2;
        }
    }
}
