package com.amap.mapapi.core;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: GeoPoint */
class e implements Parcelable.Creator<GeoPoint> {
    e() {
    }

    /* renamed from: a */
    public GeoPoint createFromParcel(Parcel parcel) {
        return new GeoPoint(parcel, (e) null);
    }

    /* renamed from: a */
    public GeoPoint[] newArray(int i) {
        return new GeoPoint[i];
    }
}
