package com.amap.mapapi.core;

import com.amap.mapapi.core.GeoPoint;

/* compiled from: ConfigableConst */
public class b {
    public static int a = 18;
    public static int b = 4;
    public static int c = 20;
    public static int d = 3;
    public static int e = 0;
    public static int f = 0;
    public static m g = null;
    public static GeoPoint.EnumMapProjection h = GeoPoint.EnumMapProjection.projection_900913;
    public static int i = 0;
    public static int j = 0;
    public static String k = "AMap API debug V1.1";
    public static String l = "AMap API V1.1.0";
    public static boolean m = true;
    public static boolean n = true;
    public static boolean o = false;

    /* compiled from: ConfigableConst */
    public enum a {
        enomap,
        ewatermark,
        emarker,
        ecompassback,
        ecommpasspoint,
        eloc1,
        eloc2,
        ezoomin,
        ezoomout,
        ezoomindisable,
        ezoomoutdisable,
        ezoominselected,
        ezoomoutselected
    }
}
