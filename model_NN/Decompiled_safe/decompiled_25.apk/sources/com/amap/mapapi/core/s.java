package com.amap.mapapi.core;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/* compiled from: SyncList */
public class s<T> implements List<T> {
    protected LinkedList<T> a = new LinkedList<>();

    public synchronized void c(T t) {
        add(t);
    }

    public synchronized void add(int i, T t) {
        this.a.add(i, t);
    }

    public synchronized boolean addAll(Collection<? extends T> collection) {
        return this.a.addAll(collection);
    }

    public synchronized boolean addAll(int i, Collection<? extends T> collection) {
        return this.a.addAll(i, collection);
    }

    public synchronized void clear() {
        this.a.clear();
    }

    public synchronized boolean contains(Object obj) {
        return this.a.contains(obj);
    }

    public synchronized boolean containsAll(Collection<?> collection) {
        return this.a.containsAll(collection);
    }

    public synchronized T get(int i) {
        T t;
        t = null;
        try {
            t = this.a.get(i);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return t;
    }

    public synchronized int indexOf(Object obj) {
        return this.a.indexOf(obj);
    }

    public synchronized boolean isEmpty() {
        return this.a.isEmpty();
    }

    public synchronized Iterator<T> iterator() {
        return this.a.listIterator();
    }

    public synchronized int lastIndexOf(Object obj) {
        return this.a.lastIndexOf(obj);
    }

    public synchronized ListIterator<T> listIterator() {
        return this.a.listIterator();
    }

    public synchronized ListIterator<T> listIterator(int i) {
        return this.a.listIterator(i);
    }

    public synchronized T remove(int i) {
        return this.a.remove(i);
    }

    public synchronized boolean remove(Object obj) {
        return this.a.remove(obj);
    }

    public synchronized boolean removeAll(Collection<?> collection) {
        return this.a.removeAll(collection);
    }

    public synchronized boolean retainAll(Collection<?> collection) {
        return this.a.retainAll(collection);
    }

    public synchronized T set(int i, T t) {
        return this.a.set(i, t);
    }

    public synchronized int size() {
        return this.a.size();
    }

    public synchronized List<T> subList(int i, int i2) {
        return this.a.subList(i, i2);
    }

    public synchronized Object[] toArray() {
        return this.a.toArray();
    }

    public synchronized <V> V[] toArray(V[] vArr) {
        return this.a.toArray(vArr);
    }

    public synchronized boolean add(T t) {
        return this.a.add(t);
    }
}
