package com.amap.mapapi.core;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.util.DisplayMetrics;
import java.io.InputStream;
import java.lang.reflect.Field;

/* compiled from: PublicResManager */
public class m {
    private Context a = null;
    private String[] b = {"nomap.png", "beta.png", "poi_1.png", "compass_bg.png", "compass_pointer.png", "loc1.png", "loc2.png", "zoom_in_true_HVGA.9.png", "zoom_out_true_HVGA.9.png", "zoom_in_disabled_HVGA.9.png", "zoom_out_disabled_HVGA.9.png", "zoom_in_selected_HVGA.9.png", "zoom_out_selected_HVGA.9.png"};
    private String[] c = {"nomap.png", "beta.png", "poi_1_WVGA.png", "compass_bg.png", "compass_pointer.png", "loc1.png", "loc2.png", "zoom_in_true_WVGA.9.png", "zoom_out_true_WVGA.9.png", "zoom_in_disabled_WVGA.9.png", "zoom_out_disabled_WVGA.9.png", "zoom_in_selected_WVGA.9.png", "zoom_out_selected_WVGA.9.png"};
    private String[] d = {"nomap.png", "beta.png", "poi_1_QVGA.png", "compass_bg__QVGA.png", "compass_pointer_QVGA.png", "loc1_QVGA.png", "loc2_QVGA.png", "zoom_in_true_QVGA.9.png", "zoom_out_true_QVGA.9.png", "zoom_in_disabled_QVGA.9.png", "zoom_out_disabled_QVGA.9.png", "zoom_in_selected_QVGA.9.png", "zoom_out_selected_QVGA.9.png"};
    private Bitmap[] e = null;

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: android.graphics.Bitmap.isRecycled():boolean in method: com.amap.mapapi.core.m.a(int):android.graphics.Bitmap, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at java.base/java.util.ArrayList.forEach(ArrayList.java:1540)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:59)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: android.graphics.Bitmap.isRecycled():boolean
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:540)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 6 more
        */
    public final android.graphics.Bitmap a(int r1) {
        /*
            r3 = this;
            android.graphics.Bitmap[] r0 = r3.e
            if (r0 != 0) goto L_0x000b
            java.lang.String[] r0 = r3.b
            int r0 = r0.length
            android.graphics.Bitmap[] r0 = new android.graphics.Bitmap[r0]
            r3.e = r0
        L_0x000b:
            android.graphics.Bitmap[] r0 = r3.e
            r0 = r0[r4]
            if (r0 == 0) goto L_0x0020
            android.graphics.Bitmap[] r0 = r3.e
            r0 = r0[r4]
            boolean r0 = r0.isRecycled()
            if (r0 != 0) goto L_0x0020
            android.graphics.Bitmap[] r0 = r3.e
            r0 = r0[r4]
        L_0x001f:
            return r0
        L_0x0020:
            java.lang.String r0 = ""
            int r1 = com.amap.mapapi.core.b.e
            r2 = 2
            if (r1 != r2) goto L_0x003c
            java.lang.String[] r0 = r3.c
            r0 = r0[r4]
        L_0x002b:
            android.content.Context r1 = r3.a
            android.graphics.Bitmap r0 = r3.a(r1, r0)
            if (r0 == 0) goto L_0x0037
            android.graphics.Bitmap[] r1 = r3.e
            r1[r4] = r0
        L_0x0037:
            android.graphics.Bitmap[] r0 = r3.e
            r0 = r0[r4]
            goto L_0x001f
        L_0x003c:
            int r1 = com.amap.mapapi.core.b.e
            r2 = 1
            if (r1 != r2) goto L_0x0046
            java.lang.String[] r0 = r3.d
            r0 = r0[r4]
            goto L_0x002b
        L_0x0046:
            int r1 = com.amap.mapapi.core.b.e
            r2 = 3
            if (r1 != r2) goto L_0x002b
            java.lang.String[] r0 = r3.b
            r0 = r0[r4]
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.mapapi.core.m.a(int):android.graphics.Bitmap");
    }

    public m(Context context) {
        this.a = context;
    }

    public void a() {
        if (this.e != null) {
            int length = this.e.length;
            for (int i = 0; i < length; i++) {
                if (this.e[i] != null) {
                    this.e[i].recycle();
                    this.e[i] = null;
                }
            }
            this.e = null;
        }
    }

    public final Bitmap a(Context context, String str) {
        Exception e2;
        Bitmap bitmap;
        try {
            InputStream open = context.getAssets().open(str);
            bitmap = BitmapFactory.decodeStream(open);
            try {
                open.close();
            } catch (Exception e3) {
                e2 = e3;
                e2.printStackTrace();
                return bitmap;
            }
        } catch (Exception e4) {
            Exception exc = e4;
            bitmap = null;
            e2 = exc;
        }
        return bitmap;
    }

    public final Drawable b(Context context, String str) {
        BitmapDrawable bitmapDrawable = new BitmapDrawable(a(context, str));
        bitmapDrawable.setBounds(0, 0, bitmapDrawable.getIntrinsicWidth(), bitmapDrawable.getIntrinsicHeight());
        return bitmapDrawable;
    }

    public final NinePatchDrawable a(Context context, String str, byte[] bArr, Rect rect) {
        return new NinePatchDrawable(a(context, str), bArr, rect, null);
    }

    public void b() {
        int i;
        new DisplayMetrics();
        DisplayMetrics displayMetrics = this.a.getApplicationContext().getResources().getDisplayMetrics();
        Field field = null;
        try {
            field = displayMetrics.getClass().getField("densityDpi");
        } catch (NoSuchFieldException | SecurityException e2) {
        }
        if (field != null) {
            long j = (long) (displayMetrics.widthPixels * displayMetrics.heightPixels);
            try {
                i = field.getInt(displayMetrics);
            } catch (IllegalArgumentException e3) {
                e3.printStackTrace();
                i = 160;
            } catch (IllegalAccessException e4) {
                e4.printStackTrace();
                i = 160;
            }
            if (i <= 120) {
                b.e = 1;
            } else if (i <= 160) {
                b.e = 3;
            } else if (i <= 240) {
                b.e = 2;
            } else if (j > 153600) {
                b.e = 2;
            } else if (j < 153600) {
                b.e = 1;
            } else {
                b.e = 3;
            }
        } else {
            long j2 = (long) (displayMetrics.widthPixels * displayMetrics.heightPixels);
            if (j2 > 153600) {
                b.e = 2;
            } else if (j2 < 153600) {
                b.e = 1;
            } else {
                b.e = 3;
            }
        }
    }
}
