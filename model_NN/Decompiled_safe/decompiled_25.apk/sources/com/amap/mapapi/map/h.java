package com.amap.mapapi.map;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.io.InputStream;
import java.util.List;

/* compiled from: BitmapManager */
class h {
    protected final a[] a;
    protected final int b;
    protected final int c;
    protected final a[] d;
    Paint e = null;
    Path f = null;
    private boolean g = false;
    private long h = 0;

    public h(int i, int i2, boolean z, long j) {
        this.b = i;
        this.c = i2;
        this.g = z;
        this.h = 1000000 * j;
        if (this.b > 0) {
            this.a = new a[this.b];
            this.d = new a[this.c];
            return;
        }
        this.a = null;
        this.d = null;
    }

    /* access modifiers changed from: protected */
    public int a(String str) {
        if (str.equals(PoiTypeDef.All)) {
            return -1;
        }
        int i = 0;
        while (i < this.b) {
            if (this.a[i] == null || !this.a[i].b.equals(str)) {
                i++;
            } else if (!this.a[i].c) {
                return -1;
            } else {
                if (!this.g || d() - this.a[i].f <= this.h) {
                    this.a[i].d = d();
                    return i;
                }
                this.a[i].c = false;
                return -1;
            }
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    public Bitmap a(int i) {
        if (i < 0 || i >= this.b || this.a[i] == null) {
            return null;
        }
        return this.a[i].a;
    }

    /* access modifiers changed from: protected */
    public synchronized int a(byte[] bArr, InputStream inputStream, boolean z, List<ba> list, String str) {
        int i = -1;
        synchronized (this) {
            if (!(bArr == null && inputStream == null && list == null)) {
                int b2 = b();
                if (b2 < 0) {
                    b2 = a();
                }
                if (b2 >= 0 && this.a != null) {
                    if (!(this.a[b2] == null || this.a[b2].a == null || this.a[b2].a.isRecycled())) {
                        this.a[b2].a.recycle();
                        this.a[b2].a = null;
                    }
                    if (this.a[b2].g != null) {
                        this.a[b2].g.clear();
                        this.a[b2].g = null;
                    }
                    if (z && bArr != null) {
                        try {
                            this.a[b2].a = BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
                        } catch (OutOfMemoryError e2) {
                        }
                    } else if (inputStream != null) {
                        try {
                            this.a[b2].a = BitmapFactory.decodeStream(inputStream);
                        } catch (OutOfMemoryError e3) {
                        }
                    }
                    if (list != null) {
                        this.a[b2].a = Bitmap.createBitmap(256, 256, Bitmap.Config.ARGB_4444);
                        a(this.a[b2].a, list);
                    }
                    if (!(this.a == null || (this.a[b2].a == null && this.a[b2].g == null))) {
                        if (this.a[b2] != null) {
                            this.a[b2].c = true;
                            this.a[b2].b = str;
                            this.a[b2].d = d();
                            if (this.g) {
                                this.a[b2].f = d();
                            }
                        }
                        i = b2;
                    }
                }
            }
        }
        return i;
    }

    private long d() {
        return System.nanoTime();
    }

    /* access modifiers changed from: protected */
    public int a() {
        a aVar;
        for (int i = 0; i < this.c; i++) {
            this.d[i] = null;
        }
        for (int i2 = 0; i2 < this.b; i2++) {
            a aVar2 = this.a[i2];
            int i3 = 0;
            while (true) {
                if (i3 >= this.c) {
                    break;
                } else if (this.d[i3] == null) {
                    this.d[i3] = aVar2;
                    break;
                } else {
                    if (this.d[i3].d > aVar2.d) {
                        aVar = this.d[i3];
                        this.d[i3] = aVar2;
                    } else {
                        aVar = aVar2;
                    }
                    i3++;
                    aVar2 = aVar;
                }
            }
        }
        int i4 = -1;
        for (int i5 = 0; i5 < this.c; i5++) {
            if (this.d[i5] != null) {
                this.d[i5].c = false;
                if (i4 < 0) {
                    i4 = this.d[i5].e;
                }
            }
        }
        return i4;
    }

    /* access modifiers changed from: protected */
    public int b() {
        int i = -1;
        for (int i2 = 0; i2 < this.b; i2++) {
            if (this.a[i2] == null) {
                this.a[i2] = new a();
                this.a[i2].e = i2;
                return i2;
            }
            if (!this.a[i2].c && i < 0) {
                i = i2;
            }
        }
        return i;
    }

    /* access modifiers changed from: protected */
    public void c() {
        for (int i = 0; i < this.b; i++) {
            if (this.a[i] != null) {
                if (this.a[i].a != null && !this.a[i].a.isRecycled()) {
                    this.a[i].a.recycle();
                }
                this.a[i].a = null;
            }
        }
    }

    private void a(Bitmap bitmap, final List<ba> list) {
        AnonymousClass1 r0 = new g() {
            public void a(Canvas canvas) {
                boolean z;
                if (h.this.e == null) {
                    h.this.e = new Paint();
                    h.this.e.setStyle(Paint.Style.STROKE);
                    h.this.e.setDither(true);
                    h.this.e.setAntiAlias(true);
                    h.this.e.setStrokeJoin(Paint.Join.ROUND);
                    h.this.e.setStrokeCap(Paint.Cap.ROUND);
                }
                if (h.this.f == null) {
                    h.this.f = new Path();
                }
                int size = list.size();
                for (int i = 0; i < size; i++) {
                    ba baVar = (ba) list.get(i);
                    h.this.e.setStrokeWidth(3.0f);
                    int b2 = baVar.b();
                    if (b2 == 1) {
                        h.this.e.setColor(-65536);
                    } else if (b2 == 2) {
                        h.this.e.setColor(-256);
                    } else if (b2 == 3) {
                        h.this.e.setColor(-16711936);
                    }
                    List<PointF> a2 = baVar.a();
                    int size2 = a2.size();
                    int i2 = 0;
                    boolean z2 = true;
                    while (i2 < size2) {
                        PointF pointF = a2.get(i2);
                        if (z2) {
                            h.this.f.moveTo(pointF.x, pointF.y);
                            z = false;
                        } else {
                            h.this.f.lineTo(pointF.x, pointF.y);
                            z = z2;
                        }
                        i2++;
                        z2 = z;
                    }
                    canvas.drawPath(h.this.f, h.this.e);
                    h.this.f.reset();
                }
            }
        };
        f fVar = new f(null);
        fVar.a(bitmap);
        fVar.a(r0);
    }

    /* compiled from: BitmapManager */
    class a {
        Bitmap a = null;
        String b = PoiTypeDef.All;
        boolean c = false;
        long d = 0;
        int e = -1;
        long f = 0;
        List<ba> g = null;

        a() {
        }
    }
}
