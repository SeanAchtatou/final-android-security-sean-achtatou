package com.amap.mapapi.map;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.support.v4.view.MotionEventCompat;
import android.util.FloatMath;
import android.view.MotionEvent;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* compiled from: MultiTouchGestureDetector */
abstract class ai {
    static float j = 1.0f;
    /* access modifiers changed from: private */
    public static Method o;
    /* access modifiers changed from: private */
    public static Method p;
    /* access modifiers changed from: private */
    public static boolean q = false;
    private static boolean r = false;
    b a;
    int b = 0;
    Matrix c = new Matrix();
    Matrix d = new Matrix();
    PointF e = new PointF();
    PointF f = new PointF();
    PointF g = new PointF();
    float h = 1.0f;
    float i = 1.0f;
    boolean k = false;
    boolean l = false;
    boolean m = false;
    public int n = 0;

    /* compiled from: MultiTouchGestureDetector */
    public interface b {
        boolean a(float f, float f2);

        boolean a(float f, PointF pointF);

        boolean a(Matrix matrix);

        boolean a(PointF pointF);

        boolean b(float f);

        boolean b(Matrix matrix);
    }

    public abstract boolean a(MotionEvent motionEvent);

    ai() {
    }

    public static ai a(Context context, b bVar) {
        a aVar = new a();
        aVar.a = bVar;
        return aVar;
    }

    /* access modifiers changed from: private */
    public static void c(MotionEvent motionEvent) {
        if (!r) {
            r = true;
            try {
                o = motionEvent.getClass().getMethod("getX", Integer.TYPE);
                p = motionEvent.getClass().getMethod("getY", Integer.TYPE);
                if (o != null && p != null) {
                    q = true;
                }
            } catch (Exception e2) {
            }
        }
    }

    /* compiled from: MultiTouchGestureDetector */
    private static class a extends ai {
        float o;
        float p;
        float q;
        float r;

        private a() {
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        public boolean a(MotionEvent motionEvent) {
            float f;
            boolean a;
            ai.c(motionEvent);
            if (!ai.q) {
                return false;
            }
            switch (motionEvent.getAction() & MotionEventCompat.ACTION_MASK) {
                case 0:
                    this.o = motionEvent.getX();
                    this.p = motionEvent.getY();
                    this.d.set(this.c);
                    this.e.set(this.o, this.p);
                    this.b = 1;
                    a = false;
                    break;
                case 1:
                    this.k = false;
                    this.b = 0;
                    a = false;
                    break;
                case 2:
                    if (this.b != 1) {
                        if (this.b == 2) {
                            float c = c(motionEvent);
                            this.i = 0.0f;
                            if (c > 10.0f && Math.abs(c - this.h) > 5.0f) {
                                this.c.set(this.d);
                                if (c > this.h) {
                                    f = c / this.h;
                                } else {
                                    f = this.h / c;
                                }
                                this.i = f;
                                j = c / this.h;
                                if (c < this.h) {
                                    this.i = -this.i;
                                }
                                a(this.g, motionEvent);
                                this.q = this.g.x;
                                this.r = this.g.y;
                                this.c.postScale(c / this.h, c / this.h, this.f.x, this.f.y);
                                a = this.a.a(this.g.x - this.q, this.g.y - this.r) | false | this.a.b(this.i) | this.a.b(this.c);
                                this.l = true;
                                break;
                            }
                        }
                        a = false;
                        break;
                    } else {
                        float x = motionEvent.getX();
                        float y = motionEvent.getY();
                        this.c.set(this.d);
                        this.c.postTranslate(motionEvent.getX() - this.e.x, motionEvent.getY() - this.e.y);
                        this.o = x;
                        this.p = y;
                        a = this.a.a(this.c) | false | this.a.a(x - this.o, y - this.p);
                        break;
                    }
                case 3:
                case 4:
                default:
                    a = false;
                    break;
                case 5:
                    this.n++;
                    if (this.n == 1) {
                        this.m = true;
                        j = 1.0f;
                        this.h = c(motionEvent);
                        if (this.h > 10.0f) {
                            this.c.reset();
                            this.d.reset();
                            this.d.set(this.c);
                            a(this.f, motionEvent);
                            this.b = 2;
                            this.k = true;
                            a = this.a.a(this.e) | false;
                            this.q = this.f.x;
                            this.r = this.f.y;
                            break;
                        }
                    }
                    a = false;
                    break;
                case 6:
                    this.n--;
                    if (this.n == 1) {
                        this.m = true;
                        this.b = 2;
                    }
                    if (this.n == 0) {
                        a(this.f, motionEvent);
                        this.l = false;
                        this.m = false;
                        if (this.k) {
                            this.k = false;
                            a = this.a.a(this.i, this.f) | false;
                            this.b = 0;
                            break;
                        }
                    }
                    a = false;
                    break;
            }
            return a;
        }

        private float c(MotionEvent motionEvent) {
            float f;
            float f2 = 0.0f;
            try {
                f = ((Float) ai.o.invoke(motionEvent, 0)).floatValue() - ((Float) ai.o.invoke(motionEvent, 1)).floatValue();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                f = 0.0f;
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
                f = 0.0f;
            } catch (InvocationTargetException e3) {
                e3.printStackTrace();
                f = 0.0f;
            }
            try {
                f2 = ((Float) ai.p.invoke(motionEvent, 0)).floatValue() - ((Float) ai.p.invoke(motionEvent, 1)).floatValue();
            } catch (IllegalArgumentException e4) {
                e4.printStackTrace();
            } catch (IllegalAccessException e5) {
                e5.printStackTrace();
            } catch (InvocationTargetException e6) {
                e6.printStackTrace();
            }
            return FloatMath.sqrt((f * f) + (f2 * f2));
        }

        private void a(PointF pointF, MotionEvent motionEvent) {
            float f;
            float f2 = 0.0f;
            try {
                f = ((Float) ai.o.invoke(motionEvent, 1)).floatValue() + ((Float) ai.o.invoke(motionEvent, 0)).floatValue();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                f = 0.0f;
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
                f = 0.0f;
            } catch (InvocationTargetException e3) {
                e3.printStackTrace();
                f = 0.0f;
            }
            try {
                f2 = ((Float) ai.p.invoke(motionEvent, 0)).floatValue() + ((Float) ai.p.invoke(motionEvent, 1)).floatValue();
            } catch (IllegalArgumentException e4) {
                e4.printStackTrace();
            } catch (IllegalAccessException e5) {
                e5.printStackTrace();
            } catch (InvocationTargetException e6) {
                e6.printStackTrace();
            }
            pointF.set(f / 2.0f, f2 / 2.0f);
        }
    }
}
