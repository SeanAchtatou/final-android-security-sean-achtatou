package com.amap.mapapi.map;

import android.os.Handler;
import android.os.Message;

/* compiled from: MapActivity */
class z extends Handler {
    final /* synthetic */ MapActivity a;

    z(MapActivity mapActivity) {
        this.a = mapActivity;
    }

    public void handleMessage(Message message) {
        ah a2;
        switch (message.what) {
            case 1:
                int size = this.a.f.size();
                for (int i = 0; i < size; i++) {
                    MapView mapView = (MapView) this.a.f.get(i);
                    if (!(mapView == null || (a2 = mapView.a()) == null)) {
                        a2.c.b();
                    }
                }
                return;
            default:
                return;
        }
    }
}
