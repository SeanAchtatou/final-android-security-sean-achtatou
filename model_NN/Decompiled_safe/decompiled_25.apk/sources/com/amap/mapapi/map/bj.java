package com.amap.mapapi.map;

import android.graphics.Matrix;
import android.graphics.Point;
import android.view.animation.Animation;
import com.amap.mapapi.core.GeoPoint;

/* compiled from: ZoomCtlAnim */
class bj extends a {
    static float h = 1.0f;
    public MapView e;
    public float f;
    public float g;
    public int i = -1;
    public boolean j = false;
    private Animation.AnimationListener k;
    private float l;
    private float m;
    private float n;
    private boolean o;
    private boolean p = false;

    public bj(MapView mapView, Animation.AnimationListener animationListener) {
        super(160, 40);
        this.e = mapView;
        this.k = animationListener;
    }

    /* access modifiers changed from: protected */
    public void a() {
        if (this.o) {
            h += this.n;
        } else {
            h -= this.n;
        }
        Matrix matrix = new Matrix();
        matrix.setScale(h, h, this.f, this.g);
        this.e.b().b(h);
        this.e.b().b(matrix);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amap.mapapi.map.ah.d.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.amap.mapapi.map.ah.d.a(android.view.View, com.amap.mapapi.map.MapView$LayoutParams):void
      com.amap.mapapi.map.ah.d.a(int, int):void
      com.amap.mapapi.map.ah.d.a(boolean, boolean):void */
    /* access modifiers changed from: protected */
    public void b() {
        if (!this.p) {
            this.e.a().d.e = false;
            if (this.j) {
                Point point = new Point((int) this.f, (int) this.g);
                GeoPoint fromPixels = this.e.getProjection().fromPixels((int) this.f, (int) this.g);
                this.e.a().f.j = this.e.a().f.a(fromPixels);
                this.e.a().f.a(point);
                this.e.a().b.a(false, false);
            }
            this.e.getController().setZoom(this.i);
            this.k.onAnimationEnd(null);
            if (this.j) {
                Point point2 = new Point(this.e.a().b.c() / 2, this.e.a().b.d() / 2);
                GeoPoint fromPixels2 = this.e.getProjection().fromPixels(this.e.a().b.c() / 2, this.e.a().b.d() / 2);
                this.e.a().f.j = this.e.a().f.a(fromPixels2);
                this.e.a().f.a(point2);
                this.e.a().b.a(false, false);
            }
            this.e.a(0);
            h = 1.0f;
            ai.j = 1.0f;
        }
    }

    public void a(float f2, int i2, boolean z, float f3, float f4) {
        this.o = z;
        this.f = f3;
        this.g = f4;
        this.l = f2;
        h = this.l;
        if (this.o) {
            this.n = (this.l * ((float) this.d)) / ((float) this.c);
            this.m = this.l * 2.0f;
            return;
        }
        this.n = ((this.l * 0.5f) * ((float) this.d)) / ((float) this.c);
        this.m = this.l * 0.5f;
    }

    public void a(int i2, boolean z, float f2, float f3) {
        this.e.c[0] = this.e.c[1];
        this.e.c[1] = i2;
        if (this.e.c[0] != this.e.c[1]) {
            this.e.getZoomMgr().d();
            if (!f()) {
                this.c = 160;
                a(this.e.b().b(), i2, z, f2, f3);
                this.e.a().d.a(true);
                this.e.a().d.e = true;
                this.k.onAnimationStart(null);
                super.c();
                return;
            }
            this.p = true;
            d();
            a(this.m, i2, z, f2, f3);
            this.e.a().d.a(true);
            this.e.a().d.e = true;
            this.k.onAnimationStart(null);
            super.c();
            this.p = false;
        }
    }
}
