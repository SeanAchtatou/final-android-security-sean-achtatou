package com.amap.mapapi.map;

import com.amap.mapapi.map.au;

/* compiled from: TaskPool */
class av extends as<au.a> {
    av() {
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x004d, code lost:
        b();
        r0 = r5;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.ArrayList<com.amap.mapapi.map.au.a> b(int r8, boolean r9) {
        /*
            r7 = this;
            r4 = 0
            r1 = 0
            monitor-enter(r7)
            java.util.LinkedList r0 = r7.a     // Catch:{ all -> 0x0066 }
            if (r0 != 0) goto L_0x000a
            r0 = r4
        L_0x0008:
            monitor-exit(r7)
            return r0
        L_0x000a:
            java.util.LinkedList r0 = r7.a     // Catch:{ all -> 0x0066 }
            int r3 = r0.size()     // Catch:{ all -> 0x0066 }
            if (r8 <= r3) goto L_0x0013
            r8 = r3
        L_0x0013:
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch:{ all -> 0x0066 }
            r5.<init>(r8)     // Catch:{ all -> 0x0066 }
            r2 = r1
        L_0x0019:
            if (r1 >= r3) goto L_0x004d
            java.util.LinkedList r0 = r7.a     // Catch:{ all -> 0x0066 }
            if (r0 != 0) goto L_0x0021
            r0 = r4
            goto L_0x0008
        L_0x0021:
            java.util.LinkedList r0 = r7.a     // Catch:{ all -> 0x0066 }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x0066 }
            com.amap.mapapi.map.au$a r0 = (com.amap.mapapi.map.au.a) r0     // Catch:{ all -> 0x0066 }
            if (r0 != 0) goto L_0x0034
            r0 = r1
            r1 = r2
            r2 = r3
        L_0x002e:
            int r0 = r0 + 1
            r3 = r2
            r2 = r1
            r1 = r0
            goto L_0x0019
        L_0x0034:
            int r6 = r0.a     // Catch:{ all -> 0x0066 }
            if (r9 == 0) goto L_0x0052
            if (r6 != 0) goto L_0x0069
            r5.add(r0)     // Catch:{ all -> 0x0066 }
            java.util.LinkedList r0 = r7.a     // Catch:{ all -> 0x0066 }
            r0.remove(r1)     // Catch:{ all -> 0x0066 }
            int r3 = r3 + -1
            int r1 = r1 + -1
            int r2 = r2 + 1
            r0 = r1
            r1 = r2
            r2 = r3
        L_0x004b:
            if (r1 < r8) goto L_0x002e
        L_0x004d:
            r7.b()     // Catch:{ all -> 0x0066 }
            r0 = r5
            goto L_0x0008
        L_0x0052:
            if (r6 >= 0) goto L_0x0069
            r5.add(r0)     // Catch:{ all -> 0x0066 }
            java.util.LinkedList r0 = r7.a     // Catch:{ all -> 0x0066 }
            r0.remove(r1)     // Catch:{ all -> 0x0066 }
            int r3 = r3 + -1
            int r1 = r1 + -1
            int r2 = r2 + 1
            r0 = r1
            r1 = r2
            r2 = r3
            goto L_0x004b
        L_0x0066:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        L_0x0069:
            r0 = r1
            r1 = r2
            r2 = r3
            goto L_0x004b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.mapapi.map.av.b(int, boolean):java.util.ArrayList");
    }
}
