package com.amap.mapapi.map;

/* compiled from: ConnectionManager */
class n extends Thread {
    am a = new am();
    int b = 30;
    volatile boolean c = true;
    Thread d;
    MapView e;

    n() {
    }

    public synchronized void a() {
        for (int i = 0; i < this.a.size(); i++) {
            ab abVar = (ab) this.a.get(i);
            for (int i2 = 0; i2 < abVar.b.size(); i2++) {
                this.e.tileDownloadCtrl.a(abVar.b.get(i));
            }
        }
        this.e.tileDownloadCtrl.a();
        for (int i3 = 0; i3 < this.a.size(); i3++) {
            ab abVar2 = (ab) this.a.get(i3);
            if (abVar2.j) {
                for (int i4 = 0; i4 < abVar2.b.size(); i4++) {
                    this.e.tileDownloadCtrl.c(abVar2.b.get(i3));
                }
            }
        }
        this.a.clear();
    }

    public synchronized void a(ab abVar) {
        this.a.insertElementAt(abVar, 0);
    }

    public void b() {
        this.c = false;
        if (this.d != null) {
            this.d.interrupt();
        }
    }

    public void run() {
        while (this.c) {
            this.d = Thread.currentThread();
            ab abVar = (ab) this.a.b();
            if (abVar == null) {
                try {
                    sleep((long) this.b);
                } catch (Exception e2) {
                    Thread.currentThread().interrupt();
                }
            } else if (System.currentTimeMillis() - abVar.d > 50) {
                abVar.b();
            }
        }
    }
}
