package com.amap.mapapi.map;

import android.content.Context;
import com.amap.mapapi.core.AMapException;
import com.amap.mapapi.core.s;
import com.amap.mapapi.map.au;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.List;

/* compiled from: TileServer */
class ax extends c<au.a, au.a> implements bh {
    private u g = new u();

    public void a_() {
    }

    public void a() {
        super.a();
        this.g.clear();
    }

    public ax(ah ahVar, Context context) {
        super(ahVar, context);
        this.c = new av();
        ahVar.b.a(this);
    }

    /* access modifiers changed from: protected */
    public ArrayList<au.a> a(ArrayList<au.a> arrayList, Proxy proxy) throws AMapException {
        ArrayList<au.a> arrayList2;
        if (arrayList == null || arrayList.size() == 0 || this.e == null || this.e.d == null || this.e.d.a == null || arrayList.get(0).e >= this.e.d.a.size()) {
            return null;
        }
        a((List<au.a>) arrayList);
        if (arrayList.size() == 0 || this.e.d.a.size() == 0) {
            return null;
        }
        if (this.e.d.a.get(arrayList.get(0).e).j != null) {
            ay ayVar = new ay(arrayList, proxy, this.e.e.a(), this.e.e.b());
            ayVar.a(this.e.d.a.get(arrayList.get(0).e));
            arrayList2 = (ArrayList) ayVar.j();
            ayVar.a((w) null);
        } else {
            arrayList2 = null;
        }
        b(arrayList);
        if (this.e == null || this.e.d == null) {
            return arrayList2;
        }
        this.e.d.d();
        return arrayList2;
    }

    public void a(List<au.a> list) {
        int size;
        int i;
        int i2;
        if (list != null && (size = list.size()) != 0) {
            int i3 = 0;
            while (i3 < size) {
                if (!this.g.b(list.get(i3))) {
                    list.remove(i3);
                    i = i3 - 1;
                    i2 = size - 1;
                } else {
                    i = i3;
                    i2 = size;
                }
                size = i2;
                i3 = i + 1;
            }
        }
    }

    private void b(ArrayList<au.a> arrayList) {
        int size;
        if (arrayList != null && this.g != null && (size = arrayList.size()) != 0) {
            for (int i = 0; i < size; i++) {
                this.g.a(arrayList.get(i));
            }
        }
    }

    private void a(ArrayList<au.a> arrayList, boolean z) {
        if (this.c != null && arrayList != null && arrayList.size() != 0) {
            this.c.a(arrayList, z);
        }
    }

    public void c() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amap.mapapi.map.ax.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.amap.mapapi.map.ax.a(java.util.ArrayList<com.amap.mapapi.map.au$a>, boolean):void
      com.amap.mapapi.map.ax.a(java.util.ArrayList<com.amap.mapapi.map.au$a>, java.net.Proxy):java.util.ArrayList<com.amap.mapapi.map.au$a>
      com.amap.mapapi.map.c.a(java.util.ArrayList, java.net.Proxy):java.util.ArrayList<T>
      com.amap.mapapi.map.ax.a(boolean, boolean):void */
    public void h() {
        a(false, false);
    }

    private boolean i() {
        if (this.e == null || this.e.d == null) {
            return false;
        }
        if (this.e.d.a == null) {
            return false;
        }
        int size = this.e.d.a.size();
        if (size <= 0) {
            return false;
        }
        for (int i = 0; i < size; i++) {
            w wVar = this.e.d.a.get(i);
            if (wVar != null && wVar.f) {
                return true;
            }
        }
        return false;
    }

    private ArrayList<au.a> a(ArrayList<au.a> arrayList, w wVar, int i, boolean z) {
        int size;
        if (arrayList == null || wVar == null || !wVar.f || wVar.o == null) {
            return null;
        }
        wVar.o.clear();
        if (i > wVar.b || i < wVar.c || (size = arrayList.size()) <= 0) {
            return null;
        }
        ArrayList<au.a> arrayList2 = new ArrayList<>();
        for (int i2 = 0; i2 < size; i2++) {
            au.a aVar = arrayList.get(i2);
            if (aVar != null) {
                int a = wVar.m.a(aVar.b + "-" + aVar.c + "-" + aVar.d);
                au.a aVar2 = new au.a(aVar.b, aVar.c, aVar.d, wVar.k);
                aVar2.g = a;
                aVar2.f = aVar.f;
                wVar.o.add(aVar2);
                if (a(aVar2) && !z && !this.g.contains(aVar2)) {
                    if (!wVar.g) {
                        aVar2.a = -1;
                    }
                    arrayList2.add(aVar2);
                }
            }
        }
        return arrayList2;
    }

    public void a(boolean z, boolean z2) {
        boolean z3;
        if (i()) {
            double mapAngle = (((double) this.e.b.g().getMapAngle()) * 3.141592653589793d) / 180.0d;
            ArrayList<au.a> a = this.e.f.a(this.e.f.j, this.e.f.g, (int) ((((double) this.e.b.c()) * Math.abs(Math.cos(mapAngle))) + (((double) this.e.b.d()) * Math.abs(Math.sin(mapAngle)))), (int) ((Math.abs(Math.cos(mapAngle)) * ((double) this.e.b.d())) + (((double) this.e.b.c()) * Math.abs(Math.sin(mapAngle)))));
            if (a != null && a.size() > 0) {
                int size = this.e.d.a.size();
                int i = 0;
                boolean z4 = true;
                while (i < size) {
                    ArrayList<au.a> a2 = a(a, this.e.d.a.get(i), this.e.b.e(), z2);
                    if (a2 != null) {
                        a(a2, z4);
                        if (z4) {
                            z3 = false;
                        } else {
                            z3 = z4;
                        }
                        a2.clear();
                    } else {
                        z3 = z4;
                    }
                    i++;
                    z4 = z3;
                }
                a.clear();
                this.e.b.g().invalidate();
            }
        }
    }

    private boolean a(au.a aVar) {
        return aVar == null || aVar.g < 0;
    }

    /* access modifiers changed from: protected */
    public int g() {
        return 1;
    }

    /* access modifiers changed from: protected */
    public int f() {
        return 3;
    }

    /* access modifiers changed from: protected */
    public ArrayList<au.a> a(ArrayList<au.a> arrayList) {
        ArrayList<au.a> arrayList2;
        int i;
        if (arrayList == null) {
            return null;
        }
        int size = arrayList.size();
        if (size == 0) {
            return null;
        }
        int i2 = 0;
        ArrayList<au.a> arrayList3 = null;
        while (i2 < size) {
            au.a aVar = arrayList.get(i2);
            if (aVar == null) {
                i = i2;
                arrayList2 = arrayList3;
            } else if (this.e == null || this.e.d == null || this.e.d.a == null) {
                return null;
            } else {
                if (aVar.e >= this.e.d.a.size()) {
                    i = i2;
                    arrayList2 = arrayList3;
                } else if (!this.e.d.a.get(aVar.e).g) {
                    i = i2;
                    arrayList2 = arrayList3;
                } else {
                    int a = this.e.d.a.get(aVar.e).n.a(aVar);
                    if (a >= 0) {
                        arrayList.remove(i2);
                        size--;
                        int i3 = i2 - 1;
                        s<au.a> sVar = this.e.d.a.get(aVar.e).o;
                        if (sVar == null) {
                            i = i3;
                            arrayList2 = arrayList3;
                        } else {
                            int size2 = sVar.size();
                            int i4 = 0;
                            while (true) {
                                if (i4 >= size2) {
                                    break;
                                }
                                au.a aVar2 = sVar.get(i4);
                                if (aVar2 != null && aVar2.equals(aVar)) {
                                    aVar2.g = a;
                                    this.e.d.d();
                                    break;
                                }
                                i4++;
                            }
                            i = i3;
                            arrayList2 = arrayList3;
                        }
                    } else {
                        if (arrayList3 == null) {
                            arrayList2 = new ArrayList<>();
                        } else {
                            arrayList2 = arrayList3;
                        }
                        au.a aVar3 = new au.a(aVar);
                        aVar3.a = -1;
                        arrayList2.add(aVar3);
                        i = i2;
                    }
                }
            }
            arrayList3 = arrayList2;
            size = size;
            i2 = i + 1;
        }
        return arrayList3;
    }
}
