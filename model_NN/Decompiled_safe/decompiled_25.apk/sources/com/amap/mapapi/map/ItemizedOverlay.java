package com.amap.mapapi.map;

import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.KeyEvent;
import android.view.MotionEvent;
import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.core.OverlayItem;
import com.amap.mapapi.core.b;
import com.amap.mapapi.map.Overlay;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

public abstract class ItemizedOverlay<Item extends OverlayItem> extends Overlay implements Overlay.Snappable {
    private static int d = -1;
    private boolean a = true;
    /* access modifiers changed from: private */
    public Drawable b;
    private Drawable c;
    private ItemizedOverlay<Item>.b e = null;
    private OnFocusChangeListener f = null;
    private int g = -1;
    private int h = -1;

    public interface OnFocusChangeListener {
        void onFocusChanged(ItemizedOverlay<?> itemizedOverlay, OverlayItem overlayItem);
    }

    enum a {
        Normal,
        Center,
        CenterBottom
    }

    /* access modifiers changed from: protected */
    public abstract Item createItem(int i);

    public abstract int size();

    public ItemizedOverlay(Drawable drawable) {
        this.b = drawable;
        if (this.b == null) {
            this.b = new BitmapDrawable(com.amap.mapapi.core.b.g.a(b.a.emarker.ordinal()));
        }
        this.b.setBounds(0, 0, this.b.getIntrinsicWidth(), this.b.getIntrinsicHeight());
        this.c = new ar().a(this.b);
        if (1 == d) {
            boundCenterBottom(this.b);
        } else if (2 == d) {
            boundCenter(this.b);
        } else {
            boundCenterBottom(this.b);
        }
    }

    public static Drawable boundCenterBottom(Drawable drawable) {
        d = 1;
        return a(drawable, a.CenterBottom);
    }

    public static Drawable boundCenter(Drawable drawable) {
        d = 2;
        return a(drawable, a.Center);
    }

    private static Drawable a(Drawable drawable, a aVar) {
        int i = 0;
        if (drawable == null || a.Normal == aVar) {
            return null;
        }
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        Rect bounds = drawable.getBounds();
        int width = bounds.width() / 2;
        int i2 = -bounds.height();
        if (aVar == a.Center) {
            i2 /= 2;
            i = -i2;
        }
        drawable.setBounds(-width, i2, width, i);
        return drawable;
    }

    /* access modifiers changed from: protected */
    public final void populate() {
        this.e = new b();
        this.g = -1;
        this.h = -1;
    }

    public GeoPoint getCenter() {
        return getItem(getIndexToDraw(0)).getPoint();
    }

    /* access modifiers changed from: protected */
    public int getIndexToDraw(int i) {
        return this.e.b(i);
    }

    public final Item getItem(int i) {
        return this.e.a(i);
    }

    public int getLatSpanE6() {
        return this.e.a(true);
    }

    public int getLonSpanE6() {
        return this.e.a(false);
    }

    public void setOnFocusChangeListener(OnFocusChangeListener onFocusChangeListener) {
        this.f = onFocusChangeListener;
    }

    public void setDrawFocusedItem(boolean z) {
        this.a = z;
    }

    public final int getLastFocusedIndex() {
        return this.g;
    }

    /* access modifiers changed from: protected */
    public void setLastFocusedIndex(int i) {
        this.g = i;
    }

    public Item getFocus() {
        if (this.h != -1) {
            return this.e.a(this.h);
        }
        return null;
    }

    public void setFocus(Item item) {
        if (item != null && this.h == this.e.a(item)) {
            return;
        }
        if (item != null || this.h == -1) {
            this.h = this.e.a(item);
            if (this.h != -1) {
                setLastFocusedIndex(this.h);
                if (this.f != null) {
                    this.f.onFocusChanged(this, item);
                    return;
                }
                return;
            }
            return;
        }
        if (this.f != null) {
            this.f.onFocusChanged(this, item);
        }
        this.h = -1;
    }

    public Item nextFocus(boolean z) {
        if (this.e.a() == 0) {
            return null;
        }
        if (this.g != -1) {
            int i = this.h == -1 ? this.g : this.h;
            return z ? b(i) : a(i);
        } else if (z) {
            return this.e.a(0);
        } else {
            return null;
        }
    }

    private Item a(int i) {
        if (i == 0) {
            return null;
        }
        return this.e.a(i - 1);
    }

    private Item b(int i) {
        if (i == this.e.a() - 1) {
            return null;
        }
        return this.e.a(i + 1);
    }

    public boolean onTap(GeoPoint geoPoint, MapView mapView) {
        return this.e.a(geoPoint, mapView);
    }

    /* access modifiers changed from: protected */
    public boolean hitTest(Item item, Drawable drawable, int i, int i2) {
        return drawable.getBounds().contains(i, i2);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent, MapView mapView) {
        return false;
    }

    public boolean onTrackballEvent(MotionEvent motionEvent, MapView mapView) {
        return false;
    }

    public boolean onTouchEvent(MotionEvent motionEvent, MapView mapView) {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean onTap(int i) {
        if (i == this.h) {
            return false;
        }
        setFocus(getItem(i));
        return false;
    }

    public boolean onSnapToItem(int i, int i2, Point point, MapView mapView) {
        for (int i3 = 0; i3 < this.e.a(); i3++) {
            Point pixels = mapView.getProjection().toPixels(this.e.a(i3).getPoint(), null);
            point.x = pixels.x;
            point.y = pixels.y;
            double d2 = (double) (i - pixels.x);
            double d3 = (double) (i2 - pixels.y);
            boolean z = (d2 * d2) + (d3 * d3) < 64.0d;
            if (z) {
                return z;
            }
        }
        return false;
    }

    public void draw(Canvas canvas, MapView mapView, boolean z) {
        for (int i = 0; i < this.e.a(); i++) {
            int indexToDraw = getIndexToDraw(i);
            if (indexToDraw != this.h) {
                a(canvas, mapView, z, getItem(indexToDraw), 0);
            }
        }
        OverlayItem focus = getFocus();
        if (this.a && focus != null) {
            a(canvas, mapView, true, focus, 4);
            a(canvas, mapView, false, focus, 4);
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: Item
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private void a(android.graphics.Canvas r6, com.amap.mapapi.map.MapView r7, boolean r8, Item r9, int r10) {
        /*
            r5 = this;
            android.graphics.drawable.Drawable r0 = r9.getMarker(r10)
            if (r0 != 0) goto L_0x0038
            r1 = 1
        L_0x0007:
            if (r0 != 0) goto L_0x003a
        L_0x0009:
            if (r1 == 0) goto L_0x0021
            if (r8 == 0) goto L_0x0041
            android.graphics.drawable.Drawable r0 = r5.c
            android.graphics.drawable.Drawable r2 = r5.c
            android.graphics.drawable.Drawable r3 = r5.b
            android.graphics.Rect r3 = r3.copyBounds()
            r2.setBounds(r3)
            android.graphics.drawable.Drawable r2 = r5.c
            android.graphics.drawable.Drawable r3 = r5.b
            com.amap.mapapi.map.ar.a(r2, r3)
        L_0x0021:
            com.amap.mapapi.map.Projection r2 = r7.getProjection()
            com.amap.mapapi.core.GeoPoint r3 = r9.getPoint()
            r4 = 0
            android.graphics.Point r2 = r2.toPixels(r3, r4)
            if (r1 == 0) goto L_0x0044
            int r1 = r2.x
            int r2 = r2.y
            com.amap.mapapi.map.Overlay.a(r6, r0, r1, r2)
        L_0x0037:
            return
        L_0x0038:
            r1 = 0
            goto L_0x0007
        L_0x003a:
            android.graphics.drawable.Drawable r1 = r5.b
            boolean r1 = r0.equals(r1)
            goto L_0x0009
        L_0x0041:
            android.graphics.drawable.Drawable r0 = r5.b
            goto L_0x0021
        L_0x0044:
            int r1 = r2.x
            int r2 = r2.y
            com.amap.mapapi.map.Overlay.drawAt(r6, r0, r1, r2, r8)
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.mapapi.map.ItemizedOverlay.a(android.graphics.Canvas, com.amap.mapapi.map.MapView, boolean, com.amap.mapapi.core.OverlayItem, int):void");
    }

    class b implements Comparator<Integer> {
        private ArrayList<Item> b;
        private ArrayList<Integer> c;

        public b() {
            int size = ItemizedOverlay.this.size();
            this.b = new ArrayList<>(size);
            this.c = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                this.c.add(Integer.valueOf(i));
                this.b.add(ItemizedOverlay.this.createItem(i));
            }
            Collections.sort(this.c, this);
        }

        public int a() {
            return this.b.size();
        }

        /* renamed from: a */
        public int compare(Integer num, Integer num2) {
            GeoPoint point = ((OverlayItem) this.b.get(num.intValue())).getPoint();
            GeoPoint point2 = ((OverlayItem) this.b.get(num2.intValue())).getPoint();
            if (point.getLatitudeE6() > point2.getLatitudeE6()) {
                return -1;
            }
            if (point.getLatitudeE6() < point2.getLatitudeE6()) {
                return 1;
            }
            if (point.getLongitudeE6() < point2.getLongitudeE6()) {
                return -1;
            }
            if (point.getLongitudeE6() > point2.getLongitudeE6()) {
                return 1;
            }
            return 0;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: Item
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public int a(Item r4) {
            /*
                r3 = this;
                r1 = -1
                if (r4 == 0) goto L_0x001a
                r0 = 0
            L_0x0004:
                int r2 = r3.a()
                if (r0 >= r2) goto L_0x001a
                java.util.ArrayList<Item> r2 = r3.b
                java.lang.Object r2 = r2.get(r0)
                boolean r2 = r4.equals(r2)
                if (r2 == 0) goto L_0x0017
            L_0x0016:
                return r0
            L_0x0017:
                int r0 = r0 + 1
                goto L_0x0004
            L_0x001a:
                r0 = r1
                goto L_0x0016
            */
            throw new UnsupportedOperationException("Method not decompiled: com.amap.mapapi.map.ItemizedOverlay.b.a(com.amap.mapapi.core.OverlayItem):int");
        }

        public Item a(int i) {
            return (OverlayItem) this.b.get(i);
        }

        public int b(int i) {
            return this.c.get(i).intValue();
        }

        public int a(boolean z) {
            if (this.b.size() == 0) {
                return 0;
            }
            int i = Integer.MAX_VALUE;
            Iterator<Item> it = this.b.iterator();
            int i2 = Integer.MIN_VALUE;
            while (true) {
                int i3 = i;
                if (!it.hasNext()) {
                    return i2 - i3;
                }
                GeoPoint point = ((OverlayItem) it.next()).getPoint();
                i = z ? point.getLatitudeE6() : point.getLongitudeE6();
                if (i > i2) {
                    i2 = i;
                }
                if (i >= i3) {
                    i = i3;
                }
            }
        }

        public boolean a(GeoPoint geoPoint, MapView mapView) {
            boolean z;
            Projection projection = mapView.getProjection();
            Point pixels = projection.toPixels(geoPoint, null);
            int i = -1;
            double d = Double.MAX_VALUE;
            int i2 = Integer.MAX_VALUE;
            for (int i3 = 0; i3 < a(); i3++) {
                double a2 = a((OverlayItem) this.b.get(i3), projection, pixels, i3);
                if (a2 >= 0.0d && a2 < d) {
                    i2 = b(i3);
                    d = a2;
                    i = i3;
                } else if (a2 == d && b(i3) > i2) {
                    i = i3;
                }
            }
            if (-1 != i) {
                z = ItemizedOverlay.this.onTap(i);
            } else {
                ItemizedOverlay.this.setFocus(null);
                z = false;
            }
            mapView.a().d.d();
            return z;
        }

        private double a(Item item, Projection projection, Point point, int i) {
            if (!b(item, projection, point, i)) {
                return -1.0d;
            }
            GeoPoint.a a2 = a(item, projection, point);
            return (double) ((a2.b * a2.b) + (a2.a * a2.a));
        }

        private GeoPoint.a a(Item item, Projection projection, Point point) {
            Point pixels = projection.toPixels(item.getPoint(), null);
            return new GeoPoint.a(point.x - pixels.x, point.y - pixels.y);
        }

        private boolean b(Item item, Projection projection, Point point, int i) {
            GeoPoint.a a2 = a(item, projection, point);
            Drawable drawable = item.getmMarker();
            if (drawable == null) {
                drawable = ItemizedOverlay.this.b;
            }
            return ItemizedOverlay.this.hitTest(item, drawable, a2.a, a2.b);
        }
    }

    /* access modifiers changed from: protected */
    public Drawable getDefaultMarker() {
        BitmapDrawable bitmapDrawable = null;
        if (0 == 0) {
            bitmapDrawable = new BitmapDrawable(com.amap.mapapi.core.b.g.a(b.a.emarker.ordinal()));
        }
        bitmapDrawable.setBounds(0, 0, bitmapDrawable.getIntrinsicWidth(), bitmapDrawable.getIntrinsicHeight());
        return bitmapDrawable;
    }
}
