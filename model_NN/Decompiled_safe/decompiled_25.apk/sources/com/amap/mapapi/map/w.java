package com.amap.mapapi.map;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import com.amap.mapapi.core.s;
import com.amap.mapapi.map.au;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.util.Iterator;

/* compiled from: LayerPropertys */
public class w extends x {
    public String a = PoiTypeDef.All;
    public int b = 18;
    public int c = 4;
    public boolean d = true;
    public boolean e = true;
    public boolean f = false;
    public boolean g = false;
    public boolean h = false;
    public long i = 0;
    public bd j = null;
    public int k = -1;
    public String l = PoiTypeDef.All;
    h m = null;
    j n = null;
    s<au.a> o = null;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof w)) {
            return false;
        }
        return this.a.equals(((w) obj).a);
    }

    public int hashCode() {
        return this.k;
    }

    public String toString() {
        return this.a;
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas) {
        Bitmap a2;
        if (this.o != null) {
            Iterator<au.a> it = this.o.iterator();
            while (it.hasNext()) {
                au.a next = it.next();
                if (next.g >= 0) {
                    a2 = this.m.a(next.g);
                } else if (this.e) {
                    a2 = au.c();
                }
                PointF pointF = next.f;
                if (!(a2 == null || pointF == null)) {
                    canvas.drawBitmap(a2, pointF.x, pointF.y, (Paint) null);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.n.a((h) null);
        this.m.c();
        this.o.clear();
    }
}
