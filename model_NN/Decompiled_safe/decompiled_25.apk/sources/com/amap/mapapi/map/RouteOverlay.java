package com.amap.mapapi.map;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.view.MotionEvent;
import android.view.View;
import com.amap.mapapi.core.AMapException;
import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.core.q;
import com.amap.mapapi.map.MapView;
import com.amap.mapapi.route.Route;
import java.util.ArrayList;
import java.util.List;

public class RouteOverlay extends Overlay {
    public static final int OnDetail = 1;
    public static final int OnIconClick = 4;
    public static final int OnNext = 3;
    public static final int OnOverview = 0;
    public static final int OnPrev = 2;
    List<aq> a = null;
    a b = new a();
    private ap c = null;
    private boolean d = true;
    /* access modifiers changed from: private */
    public boolean e = true;
    /* access modifiers changed from: private */
    public List<RouteMessageHandler> f = new ArrayList();
    private MapView g = null;
    private int h = 0;
    private boolean i = false;
    private boolean j = true;
    protected MapActivity mContext;
    protected Route mRoute = null;

    public RouteOverlay(MapActivity mapActivity, Route route) {
        q.a(mapActivity);
        this.mContext = mapActivity;
        this.mRoute = route;
    }

    public Route getRoute() {
        return this.mRoute;
    }

    public boolean onTrackballEvent(MotionEvent motionEvent, MapView mapView) {
        return onTouchEvent(motionEvent, mapView);
    }

    /* JADX WARNING: Removed duplicated region for block: B:3:0x000d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r4, com.amap.mapapi.map.MapView r5) {
        /*
            r3 = this;
            r0 = 0
            java.util.List<com.amap.mapapi.map.aq> r1 = r3.a
            java.util.Iterator r1 = r1.iterator()
        L_0x0007:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x0019
            java.lang.Object r0 = r1.next()
            com.amap.mapapi.map.aq r0 = (com.amap.mapapi.map.aq) r0
            boolean r0 = r0.a(r4, r5)
            if (r0 == 0) goto L_0x0007
        L_0x0019:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.mapapi.map.RouteOverlay.onTouchEvent(android.view.MotionEvent, com.amap.mapapi.map.MapView):boolean");
    }

    public void setBusLinePaint(Paint paint) {
        if (paint != null) {
            if (!Paint.Style.STROKE.equals(paint.getStyle())) {
                paint.setStyle(Paint.Style.STROKE);
            }
            q.l = paint;
        }
    }

    public void setFootLinePaint(Paint paint) {
        if (paint != null) {
            if (!Paint.Style.STROKE.equals(paint.getStyle())) {
                paint.setStyle(Paint.Style.STROKE);
            }
            q.k = paint;
        }
    }

    public void setCarLinePaint(Paint paint) {
        if (paint != null) {
            if (!Paint.Style.STROKE.equals(paint.getStyle())) {
                paint.setStyle(Paint.Style.STROKE);
            }
            q.m = paint;
        }
    }

    public void draw(Canvas canvas, MapView mapView, boolean z) {
        for (aq next : this.a) {
            if (next instanceof y) {
                next.a(canvas, mapView, z);
            }
        }
        for (aq next2 : this.a) {
            if (!(next2 instanceof y)) {
                next2.a(canvas, mapView, z);
            }
        }
    }

    public void enablePopup(boolean z) {
        this.d = z;
        if (!this.d) {
            closePopupWindow();
        }
    }

    public void showRouteButton(boolean z) {
        this.j = z;
    }

    public void enableDrag(boolean z) {
        this.e = z;
    }

    public void addToMap(MapView mapView) {
        this.g = mapView;
        a(this.g);
        if (!this.g.getOverlays().contains(this)) {
            this.g.getOverlays().add(this);
        }
        for (aq a2 : this.a) {
            a2.a(this.g);
        }
    }

    private void a(MapView mapView) {
        if (!this.i) {
            this.a = new ArrayList();
            this.a.add(new o(this, 0, this.mRoute.mHelper.g(0), ItemizedOverlay.boundCenterBottom(q.a), this.b, true));
            int stepCount = this.mRoute.getStepCount();
            for (int i2 = 0; i2 < stepCount; i2++) {
                if (i2 <= 0 || i2 >= stepCount - 1) {
                    this.a.add(new y(this, this.mRoute.getStep(i2).getShapes(), this.mRoute.mHelper.a(i2)));
                } else {
                    GeoPoint[] shapes = this.mRoute.getStep(i2).getShapes();
                    GeoPoint geoPoint = this.mRoute.getStep(i2 + 1).getShapes()[0];
                    GeoPoint[] geoPointArr = new GeoPoint[(shapes.length + 1)];
                    System.arraycopy(shapes, 0, geoPointArr, 0, shapes.length);
                    geoPointArr[geoPointArr.length - 1] = geoPoint;
                    this.a.add(new y(this, geoPointArr, this.mRoute.mHelper.a(i2)));
                }
                View a2 = this.mRoute.mHelper.a(mapView, this.mContext, this.b, this, i2);
                if (a2 != null) {
                    GeoPoint g2 = this.mRoute.mHelper.g(i2);
                    int i3 = i2;
                    GeoPoint geoPoint2 = g2;
                    this.a.add(new bg(this, i3, geoPoint2, a2, q.b(this.mContext), new MapView.LayoutParams(-2, -2, g2, 0, 0, 85)));
                } else if (Route.isDrive(this.mRoute.getMode())) {
                    this.a.add(new o(this, i2 + 1, this.mRoute.mHelper.g(i2 + 1), ItemizedOverlay.boundCenter(q.j), this.b, false));
                }
            }
            this.a.add(new o(this, stepCount, this.mRoute.mHelper.g(stepCount), ItemizedOverlay.boundCenterBottom(q.b), this.b, true));
            this.i = true;
        }
    }

    public void registerRouteMessage(RouteMessageHandler routeMessageHandler) {
        this.f.add(routeMessageHandler);
    }

    public void unregisterRouteMessage(RouteMessageHandler routeMessageHandler) {
        this.f.remove(routeMessageHandler);
    }

    /* access modifiers changed from: protected */
    public void takeDefaultAction(MapView mapView, int i2, int i3) {
        switch (i3) {
            case 0:
                closePopupWindow();
                a(mapView, i2);
                break;
            case 1:
                closePopupWindow();
                b(mapView, i2);
                break;
            case 2:
                i2 = this.mRoute.mHelper.e(i2);
                break;
            case 3:
                i2 = this.mRoute.mHelper.d(i2);
                break;
        }
        showPopupWindow(i2);
        this.h = i2;
    }

    public void showNextPopUpWindow() {
        if (this.mRoute != null && this.h < this.mRoute.getStepCount()) {
            takeDefaultAction(this.g, this.h, 3);
        }
    }

    public void showPrevPopUpWindow() {
        if (this.h > 0) {
            takeDefaultAction(this.g, this.h, 2);
        }
    }

    private void a(MapView mapView, int i2) {
        mapView.getController().zoomOut();
    }

    private void b(MapView mapView, int i2) {
        mapView.getController().zoomIn();
    }

    public boolean removeFromMap(MapView mapView) {
        boolean remove = mapView.getOverlays().remove(this);
        if (remove) {
            closePopupWindow();
            this.g = null;
            for (aq b2 : this.a) {
                b2.b(mapView);
            }
        }
        return remove;
    }

    public boolean showPopupWindow(int i2) {
        if (!this.d || isStartEndMoved()) {
            return false;
        }
        if (this.g == null) {
            throw new UnsupportedOperationException("routeoverlay must be added to map frist!");
        }
        View infoView = getInfoView(this.g, i2);
        if (infoView == null) {
            return false;
        }
        GeoPoint g2 = this.mRoute.mHelper.g(i2);
        if (this.g.mRouteCtrl.a) {
            b(this.g, g2);
        }
        this.c = new ap(this.g, infoView, g2, this, i2);
        this.c.a(this.j);
        return true;
    }

    public void closePopupWindow() {
        if (this.c != null) {
            this.c.c();
        }
        this.c = null;
    }

    /* access modifiers changed from: protected */
    public View getInfoView(MapView mapView, int i2) {
        return this.mRoute.mHelper.b(mapView, this.mContext, this.b, this, i2);
    }

    public boolean isStartEndMoved() {
        return !getStartPos().equals(this.mRoute.getStartPos()) || !getEndPos().equals(this.mRoute.getTargetPos());
    }

    public void restoreOverlay(MapView mapView) {
        removeFromMap(mapView);
        a(0).b = this.mRoute.getStartPos().e();
        a(this.mRoute.getStepCount()).b = this.mRoute.getTargetPos().e();
        addToMap(mapView);
    }

    public void renewOverlay(MapView mapView) throws AMapException {
        removeFromMap(mapView);
        this.i = false;
        if (isStartEndMoved()) {
            this.mRoute = Route.calculateRoute(this.mContext, new Route.FromAndTo(getStartPos(), getEndPos(), 0), this.mRoute.getMode()).get(0);
        }
        addToMap(mapView);
    }

    public GeoPoint getStartPos() {
        return a(0).b;
    }

    public GeoPoint getEndPos() {
        return a(this.mRoute.getStepCount()).b;
    }

    private void b(MapView mapView, GeoPoint geoPoint) {
        Point a2 = a(mapView, geoPoint);
        if (!a(mapView, a2, 30)) {
            a2.x -= mapView.getWidth() / 4;
            mapView.getController().animateTo(mapView.getProjection().fromPixels(a2.x, a2.y));
        }
    }

    static boolean a(MapView mapView, Point point, int i2) {
        if (point == null) {
            return false;
        }
        int width = mapView.getWidth() - i2;
        int height = mapView.getHeight() - i2;
        if (point.x <= i2 || point.x >= width || point.y <= i2 || point.y >= height) {
            return false;
        }
        return true;
    }

    static Point a(MapView mapView, GeoPoint geoPoint) {
        return mapView.getProjection().toPixels(geoPoint, null);
    }

    /* access modifiers changed from: private */
    public o a(int i2) {
        return i2 == 0 ? (o) this.a.get(0) : (o) this.a.get(this.a.size() - 1);
    }

    class a implements RouteMessageHandler {
        a() {
        }

        /* JADX WARNING: Removed duplicated region for block: B:3:0x0011  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean onRouteEvent(com.amap.mapapi.map.MapView r4, com.amap.mapapi.map.RouteOverlay r5, int r6, int r7) {
            /*
                r3 = this;
                r0 = 0
                com.amap.mapapi.map.RouteOverlay r1 = com.amap.mapapi.map.RouteOverlay.this
                java.util.List r1 = r1.f
                java.util.Iterator r1 = r1.iterator()
            L_0x000b:
                boolean r2 = r1.hasNext()
                if (r2 == 0) goto L_0x001d
                java.lang.Object r0 = r1.next()
                com.amap.mapapi.map.RouteMessageHandler r0 = (com.amap.mapapi.map.RouteMessageHandler) r0
                boolean r0 = r0.onRouteEvent(r4, r5, r6, r7)
                if (r0 == 0) goto L_0x000b
            L_0x001d:
                if (r0 != 0) goto L_0x0024
                com.amap.mapapi.map.RouteOverlay r1 = com.amap.mapapi.map.RouteOverlay.this
                r1.takeDefaultAction(r4, r6, r7)
            L_0x0024:
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.amap.mapapi.map.RouteOverlay.a.onRouteEvent(com.amap.mapapi.map.MapView, com.amap.mapapi.map.RouteOverlay, int, int):boolean");
        }

        private boolean a(MapView mapView, RouteOverlay routeOverlay, int i, GeoPoint geoPoint) {
            if (!RouteOverlay.this.e) {
                return false;
            }
            RouteOverlay.this.closePopupWindow();
            RouteOverlay.this.a(i).a(geoPoint);
            mapView.invalidate();
            return true;
        }

        public void onDragBegin(MapView mapView, RouteOverlay routeOverlay, int i, GeoPoint geoPoint) {
            if (a(mapView, routeOverlay, i, geoPoint)) {
                for (RouteMessageHandler onDragBegin : RouteOverlay.this.f) {
                    onDragBegin.onDragBegin(mapView, routeOverlay, i, geoPoint);
                }
            }
        }

        public void onDragEnd(MapView mapView, RouteOverlay routeOverlay, int i, GeoPoint geoPoint) {
            if (a(mapView, routeOverlay, i, geoPoint)) {
                for (RouteMessageHandler onDragEnd : RouteOverlay.this.f) {
                    onDragEnd.onDragEnd(mapView, routeOverlay, i, geoPoint);
                }
            }
        }

        public void onDrag(MapView mapView, RouteOverlay routeOverlay, int i, GeoPoint geoPoint) {
            if (a(mapView, routeOverlay, i, geoPoint)) {
                for (RouteMessageHandler onDrag : RouteOverlay.this.f) {
                    onDrag.onDrag(mapView, routeOverlay, i, geoPoint);
                }
            }
        }
    }
}
