package com.amap.mapapi.map;

import android.content.Context;
import com.amap.mapapi.core.a;

/* compiled from: OnMapViewLog */
public class aj {
    /* access modifiers changed from: private */
    public Context a;
    private Runnable b = new ak(this);

    public aj(Context context) {
        this.a = context;
    }

    public void a() {
        new Thread(this.b).start();
    }

    /* access modifiers changed from: private */
    public byte[] b() {
        a a2 = a.a(this.a);
        if (a2 != null) {
            return a2.a().getBytes();
        }
        return null;
    }
}
