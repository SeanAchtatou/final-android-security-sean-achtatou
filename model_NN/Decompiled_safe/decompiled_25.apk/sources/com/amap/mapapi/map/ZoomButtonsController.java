package com.amap.mapapi.map;

import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import com.amap.mapapi.map.MapView;

public class ZoomButtonsController implements View.OnTouchListener {
    private MapView.g a;
    private ViewGroup b;
    private OnZoomListener c;

    public interface OnZoomListener {
        void onVisibilityChanged(boolean z);

        void onZoom(boolean z);
    }

    public ZoomButtonsController(View view) {
        this.b = (MapView) view;
        this.a = ((MapView) view).getZoomMgr();
    }

    public ViewGroup getContainer() {
        return this.b;
    }

    public View getZoomControls() {
        return null;
    }

    public void setZoomSpeed(long j) {
    }

    public void setFocusable(boolean z) {
    }

    public boolean isAutoDismissed() {
        return false;
    }

    public void setAutoDismissed(boolean z) {
    }

    public void setZoomInEnabled(boolean z) {
        this.a.f().setEnabled(z);
    }

    public boolean isVisible() {
        return this.a.b();
    }

    public void setVisible(boolean z) {
        this.a.a(z);
    }

    public void setOnZoomListener(OnZoomListener onZoomListener) {
        this.c = onZoomListener;
    }

    public OnZoomListener getOnZoomListener() {
        return this.c;
    }

    public void setZoomOutEnabled(boolean z) {
        this.a.g().setEnabled(z);
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        return false;
    }
}
