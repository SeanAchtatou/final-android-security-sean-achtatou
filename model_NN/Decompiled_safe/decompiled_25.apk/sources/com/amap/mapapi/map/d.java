package com.amap.mapapi.map;

import com.amap.mapapi.core.AMapException;
import java.util.ArrayList;
import java.util.List;

/* compiled from: AsyncServer */
class d implements Runnable {
    final /* synthetic */ c a;

    d(c cVar) {
        this.a = cVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amap.mapapi.map.as.a(int, boolean):java.util.ArrayList<T>
     arg types: [int, int]
     candidates:
      com.amap.mapapi.map.as.a(java.util.List, boolean):void
      com.amap.mapapi.map.as.a(int, boolean):java.util.ArrayList<T> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amap.mapapi.map.as.a(java.util.List, boolean):void
     arg types: [java.util.ArrayList<T>, int]
     candidates:
      com.amap.mapapi.map.as.a(int, boolean):java.util.ArrayList<T>
      com.amap.mapapi.map.as.a(java.util.List, boolean):void */
    public void run() {
        ArrayList<T> arrayList = null;
        this.a.b.add(Thread.currentThread());
        ArrayList<T> arrayList2 = null;
        while (this.a.a) {
            if (this.a.e == null) {
                this.a.a = false;
            } else {
                if (this.a.c != null) {
                    arrayList2 = this.a.c.a(this.a.g(), false);
                }
                if (arrayList2 == null || arrayList2.size() != 0) {
                    if (this.a.a) {
                        if (arrayList2 != null) {
                            if (!this.a.a) {
                                return;
                            }
                            if (!(this.a.e == null || this.a.e.e == null)) {
                                try {
                                    arrayList = this.a.a(arrayList2, this.a.e.e.c());
                                } catch (AMapException e) {
                                    e.printStackTrace();
                                }
                                if (!(arrayList == null || this.a.c == null)) {
                                    this.a.c.a((List) arrayList, false);
                                }
                            }
                        }
                        if (this.a.a) {
                            try {
                                Thread.sleep(50);
                            } catch (Exception e2) {
                                Thread.currentThread().interrupt();
                            }
                        }
                    } else {
                        return;
                    }
                }
            }
        }
    }
}
