package com.amap.mapapi.map;

import java.util.ArrayList;

/* compiled from: RenderPool */
class an extends Thread {
    volatile boolean a = true;
    Thread b;
    MapView c;
    ArrayList<ao> d = new ArrayList<>();

    public an(MapView mapView) {
        this.c = mapView;
    }

    public void a() {
        this.a = false;
        if (this.b != null) {
            this.b.interrupt();
        }
    }

    public void run() {
        while (this.a) {
            this.b = Thread.currentThread();
            if (this.d.size() > 0) {
                ao aoVar = this.d.get(0);
                this.d.remove(0);
                if (this.c.a(aoVar.a)) {
                    aoVar.a(this.c.f);
                    this.c.postInvalidate();
                }
            }
            try {
                sleep(50);
            } catch (Exception e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    public void a(ao aoVar) {
        this.d.add(aoVar);
    }

    public boolean a(String str) {
        int i = 0;
        while (i < this.d.size()) {
            try {
                if (this.d.get(i).a.equals(str)) {
                    return true;
                }
                i++;
            } catch (Exception e) {
                return false;
            }
        }
        return false;
    }
}
