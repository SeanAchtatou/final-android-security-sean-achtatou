package com.amap.mapapi.map;

import android.content.Context;
import android.os.Environment;
import android.support.v4.view.MotionEventCompat;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.io.File;

/* compiled from: CachManager */
class j {
    private h a = null;
    private String b = "/sdcard/Amap/RMap";
    private final int c = 128;

    public j(Context context, boolean z, w wVar) {
        File file;
        if (wVar != null) {
            if (z) {
                this.b = context.getFilesDir().getPath();
            } else {
                boolean z2 = false;
                if (!wVar.l.equals(PoiTypeDef.All) && !(z2 = (file = new File(wVar.l)).exists())) {
                    z2 = file.mkdirs();
                }
                if (!z2) {
                    if (!Environment.getExternalStorageState().equals("mounted")) {
                        this.b = context.getFilesDir().getPath();
                    } else {
                        this.b = Environment.getExternalStorageDirectory().getPath();
                    }
                }
            }
            this.b += "/Amap/" + wVar.a;
        }
    }

    private String[] a(int i, int i2, int i3, boolean z) {
        int i4 = i / 128;
        int i5 = i2 / 128;
        String[] strArr = new String[2];
        StringBuilder sb = new StringBuilder();
        sb.append(this.b);
        sb.append("/");
        sb.append(i3);
        sb.append("/");
        sb.append(i4 / 10);
        sb.append("/");
        sb.append(i5 / 10);
        sb.append("/");
        if (!z) {
            File file = new File(sb.toString());
            if (!file.exists()) {
                file.mkdirs();
            }
        }
        sb.append(i3);
        sb.append("_");
        sb.append(i4);
        sb.append("_");
        sb.append(i5);
        strArr[0] = sb.toString() + ".idx";
        strArr[1] = sb.toString() + ".dat";
        return strArr;
    }

    public void a(h hVar) {
        this.a = hVar;
    }

    private byte[] a(int i) {
        return new byte[]{(byte) (i & MotionEventCompat.ACTION_MASK), (byte) ((65280 & i) >> 8), (byte) ((16711680 & i) >> 16), (byte) ((-16777216 & i) >> 24)};
    }

    private void a(byte[] bArr) {
        if (bArr != null && bArr.length == 4) {
            byte b2 = bArr[0];
            bArr[0] = bArr[3];
            bArr[3] = b2;
            byte b3 = bArr[1];
            bArr[1] = bArr[2];
            bArr[2] = b3;
        }
    }

    private int b(byte[] bArr) {
        return (bArr[0] & 255) | ((bArr[1] << 8) & 65280) | ((bArr[2] << 16) & 16711680) | ((bArr[3] << 24) & -16777216);
    }

    private int a(int i, int i2) {
        return ((i % 128) * 128) + (i2 % 128);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amap.mapapi.map.j.a(int, int, int, boolean):java.lang.String[]
     arg types: [int, int, int, int]
     candidates:
      com.amap.mapapi.map.j.a(byte[], int, int, int):boolean
      com.amap.mapapi.map.j.a(int, int, int, boolean):java.lang.String[] */
    /*  JADX ERROR: UnsupportedOperationException in pass: MethodInvokeVisitor
        java.lang.UnsupportedOperationException: ArgType.getObject(), call class: class jadx.core.dex.instructions.args.ArgType$ArrayArg
        	at jadx.core.dex.instructions.args.ArgType.getObject(ArgType.java:513)
        	at jadx.core.clsp.ClspGraph.getClsDetails(ClspGraph.java:76)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:100)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public int a(com.amap.mapapi.map.au.a r10) {
        /*
            r9 = this;
            r8 = 4
            r3 = 1
            r2 = 0
            r6 = 0
            r0 = -1
            int r1 = r10.b
            int r4 = r10.c
            int r5 = r10.d
            java.lang.String[] r4 = r9.a(r1, r4, r5, r3)
            if (r4 == 0) goto L_0x0027
            int r1 = r4.length
            r5 = 2
            if (r1 != r5) goto L_0x0027
            r1 = r4[r6]
            java.lang.String r5 = ""
            boolean r1 = r1.equals(r5)
            if (r1 != 0) goto L_0x0027
            java.lang.String r1 = ""
            boolean r1 = r4.equals(r1)
            if (r1 == 0) goto L_0x0028
        L_0x0027:
            return r0
        L_0x0028:
            java.io.File r5 = new java.io.File
            r1 = r4[r6]
            r5.<init>(r1)
            boolean r1 = r5.exists()
            if (r1 == 0) goto L_0x0027
            int r1 = r10.b
            int r6 = r10.c
            int r6 = r9.a(r1, r6)
            if (r6 < 0) goto L_0x0027
            java.io.RandomAccessFile r1 = new java.io.RandomAccessFile     // Catch:{ FileNotFoundException -> 0x0093 }
            java.lang.String r7 = "r"
            r1.<init>(r5, r7)     // Catch:{ FileNotFoundException -> 0x0093 }
        L_0x0046:
            if (r1 == 0) goto L_0x0027
            int r5 = r6 * 4
            long r5 = (long) r5
            r1.seek(r5)     // Catch:{ IOException -> 0x0099 }
        L_0x004e:
            byte[] r5 = new byte[r8]
            r6 = 0
            r7 = 4
            r1.read(r5, r6, r7)     // Catch:{ IOException -> 0x009e }
        L_0x0055:
            r9.a(r5)
            int r6 = r9.b(r5)
            r1.close()     // Catch:{ IOException -> 0x00a3 }
        L_0x005f:
            if (r6 < 0) goto L_0x0027
            java.io.File r7 = new java.io.File
            r1 = r4[r3]
            r7.<init>(r1)
            boolean r1 = r7.exists()
            if (r1 == 0) goto L_0x0027
            java.io.RandomAccessFile r1 = new java.io.RandomAccessFile     // Catch:{ FileNotFoundException -> 0x00a8 }
            java.lang.String r4 = "r"
            r1.<init>(r7, r4)     // Catch:{ FileNotFoundException -> 0x00a8 }
            r4 = r1
        L_0x0076:
            if (r4 == 0) goto L_0x0027
            long r6 = (long) r6
            r4.seek(r6)     // Catch:{ IOException -> 0x00ae }
        L_0x007c:
            r1 = 0
            r6 = 4
            r4.read(r5, r1, r6)     // Catch:{ IOException -> 0x00b3 }
        L_0x0081:
            r9.a(r5)
            int r5 = r9.b(r5)
            if (r5 > 0) goto L_0x00b8
            r4.close()     // Catch:{ IOException -> 0x008e }
            goto L_0x0027
        L_0x008e:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0027
        L_0x0093:
            r1 = move-exception
            r1.printStackTrace()
            r1 = r2
            goto L_0x0046
        L_0x0099:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x004e
        L_0x009e:
            r6 = move-exception
            r6.printStackTrace()
            goto L_0x0055
        L_0x00a3:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x005f
        L_0x00a8:
            r1 = move-exception
            r1.printStackTrace()
            r4 = r2
            goto L_0x0076
        L_0x00ae:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x007c
        L_0x00b3:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0081
        L_0x00b8:
            byte[] r1 = new byte[r5]
            r6 = 0
            r4.read(r1, r6, r5)     // Catch:{ IOException -> 0x00f0 }
        L_0x00be:
            r4.close()     // Catch:{ IOException -> 0x00f5 }
        L_0x00c1:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            int r5 = r10.b
            r4.append(r5)
            java.lang.String r5 = "-"
            r4.append(r5)
            int r5 = r10.c
            r4.append(r5)
            java.lang.String r5 = "-"
            r4.append(r5)
            int r5 = r10.d
            r4.append(r5)
            com.amap.mapapi.map.h r5 = r9.a
            if (r5 == 0) goto L_0x0027
            com.amap.mapapi.map.h r0 = r9.a
            java.lang.String r5 = r4.toString()
            r4 = r2
            int r0 = r0.a(r1, r2, r3, r4, r5)
            goto L_0x0027
        L_0x00f0:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x00be
        L_0x00f5:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x00c1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.mapapi.map.j.a(com.amap.mapapi.map.au$a):int");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amap.mapapi.map.j.a(int, int, int, boolean):java.lang.String[]
     arg types: [int, int, int, int]
     candidates:
      com.amap.mapapi.map.j.a(byte[], int, int, int):boolean
      com.amap.mapapi.map.j.a(int, int, int, boolean):java.lang.String[] */
    /*  JADX ERROR: UnsupportedOperationException in pass: MethodInvokeVisitor
        java.lang.UnsupportedOperationException: ArgType.getObject(), call class: class jadx.core.dex.instructions.args.ArgType$ArrayArg
        	at jadx.core.dex.instructions.args.ArgType.getObject(ArgType.java:513)
        	at jadx.core.clsp.ClspGraph.getClsDetails(ClspGraph.java:76)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:100)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    public synchronized boolean a(byte[] r12, int r13, int r14, int r15) {
        /*
            r11 = this;
            r3 = 0
            r1 = 1
            r4 = 0
            r0 = 0
            monitor-enter(r11)
            if (r12 != 0) goto L_0x000a
        L_0x0008:
            monitor-exit(r11)
            return r0
        L_0x000a:
            int r6 = r12.length     // Catch:{ all -> 0x00a2 }
            if (r6 <= 0) goto L_0x0008
            r2 = 0
            java.lang.String[] r9 = r11.a(r13, r14, r15, r2)     // Catch:{ all -> 0x00a2 }
            if (r9 == 0) goto L_0x0008
            int r2 = r9.length     // Catch:{ all -> 0x00a2 }
            r7 = 2
            if (r2 != r7) goto L_0x0008
            r2 = 0
            r2 = r9[r2]     // Catch:{ all -> 0x00a2 }
            java.lang.String r7 = ""
            boolean r2 = r2.equals(r7)     // Catch:{ all -> 0x00a2 }
            if (r2 != 0) goto L_0x0008
            java.lang.String r2 = ""
            boolean r2 = r9.equals(r2)     // Catch:{ all -> 0x00a2 }
            if (r2 != 0) goto L_0x0008
            java.io.File r7 = new java.io.File     // Catch:{ all -> 0x00a2 }
            r2 = 1
            r2 = r9[r2]     // Catch:{ all -> 0x00a2 }
            r7.<init>(r2)     // Catch:{ all -> 0x00a2 }
            boolean r2 = r7.exists()     // Catch:{ all -> 0x00a2 }
            if (r2 != 0) goto L_0x003f
            boolean r2 = r7.createNewFile()     // Catch:{ IOException -> 0x00a5 }
        L_0x003d:
            if (r2 == 0) goto L_0x0008
        L_0x003f:
            java.io.RandomAccessFile r2 = new java.io.RandomAccessFile     // Catch:{ FileNotFoundException -> 0x00ab }
            java.lang.String r8 = "rws"
            r2.<init>(r7, r8)     // Catch:{ FileNotFoundException -> 0x00ab }
        L_0x0046:
            if (r2 == 0) goto L_0x0008
            byte[] r10 = r11.a(r6)     // Catch:{ all -> 0x00a2 }
            r11.a(r10)     // Catch:{ all -> 0x00a2 }
            long r6 = r2.length()     // Catch:{ IOException -> 0x00b1 }
            r7 = r6
        L_0x0054:
            r2.seek(r7)     // Catch:{ IOException -> 0x00b7 }
        L_0x0057:
            r2.write(r10)     // Catch:{ IOException -> 0x00bc }
        L_0x005a:
            r2.write(r12)     // Catch:{ IOException -> 0x00c1 }
        L_0x005d:
            r2.close()     // Catch:{ IOException -> 0x00c6 }
        L_0x0060:
            java.io.File r6 = new java.io.File     // Catch:{ all -> 0x00a2 }
            r2 = 0
            r2 = r9[r2]     // Catch:{ all -> 0x00a2 }
            r6.<init>(r2)     // Catch:{ all -> 0x00a2 }
            boolean r2 = r6.exists()     // Catch:{ all -> 0x00a2 }
            if (r2 != 0) goto L_0x0074
            boolean r2 = r6.createNewFile()     // Catch:{ IOException -> 0x00cb }
        L_0x0072:
            if (r2 == 0) goto L_0x0008
        L_0x0074:
            java.io.RandomAccessFile r2 = new java.io.RandomAccessFile     // Catch:{ FileNotFoundException -> 0x00d1 }
            java.lang.String r9 = "rws"
            r2.<init>(r6, r9)     // Catch:{ FileNotFoundException -> 0x00d1 }
            r6 = r2
        L_0x007c:
            if (r6 == 0) goto L_0x0008
            long r2 = r6.length()     // Catch:{ IOException -> 0x00d7 }
        L_0x0082:
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 != 0) goto L_0x0091
            r2 = 65536(0x10000, float:9.18355E-41)
            byte[] r2 = new byte[r2]     // Catch:{ all -> 0x00a2 }
            r3 = -1
            java.util.Arrays.fill(r2, r3)     // Catch:{ all -> 0x00a2 }
            r6.write(r2)     // Catch:{ IOException -> 0x00dd }
        L_0x0091:
            int r2 = r11.a(r13, r14)     // Catch:{ all -> 0x00a2 }
            if (r2 >= 0) goto L_0x00e2
            r6.close()     // Catch:{ IOException -> 0x009c }
            goto L_0x0008
        L_0x009c:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x00a2 }
            goto L_0x0008
        L_0x00a2:
            r0 = move-exception
            monitor-exit(r11)
            throw r0
        L_0x00a5:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ all -> 0x00a2 }
            r2 = r0
            goto L_0x003d
        L_0x00ab:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ all -> 0x00a2 }
            r2 = r3
            goto L_0x0046
        L_0x00b1:
            r6 = move-exception
            r6.printStackTrace()     // Catch:{ all -> 0x00a2 }
            r7 = r4
            goto L_0x0054
        L_0x00b7:
            r6 = move-exception
            r6.printStackTrace()     // Catch:{ all -> 0x00a2 }
            goto L_0x0057
        L_0x00bc:
            r6 = move-exception
            r6.printStackTrace()     // Catch:{ all -> 0x00a2 }
            goto L_0x005a
        L_0x00c1:
            r6 = move-exception
            r6.printStackTrace()     // Catch:{ all -> 0x00a2 }
            goto L_0x005d
        L_0x00c6:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ all -> 0x00a2 }
            goto L_0x0060
        L_0x00cb:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ all -> 0x00a2 }
            r2 = r0
            goto L_0x0072
        L_0x00d1:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ all -> 0x00a2 }
            r6 = r3
            goto L_0x007c
        L_0x00d7:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ all -> 0x00a2 }
            r2 = r4
            goto L_0x0082
        L_0x00dd:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ all -> 0x00a2 }
            goto L_0x0091
        L_0x00e2:
            int r0 = r2 * 4
            long r2 = (long) r0
            r6.seek(r2)     // Catch:{ IOException -> 0x00f9 }
        L_0x00e8:
            int r0 = (int) r7
            byte[] r0 = r11.a(r0)     // Catch:{ all -> 0x00a2 }
            r11.a(r0)     // Catch:{ all -> 0x00a2 }
            r6.write(r0)     // Catch:{ IOException -> 0x00fe }
        L_0x00f3:
            r6.close()     // Catch:{ IOException -> 0x0103 }
        L_0x00f6:
            r0 = r1
            goto L_0x0008
        L_0x00f9:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x00a2 }
            goto L_0x00e8
        L_0x00fe:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x00a2 }
            goto L_0x00f3
        L_0x0103:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x00a2 }
            goto L_0x00f6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.mapapi.map.j.a(byte[], int, int, int):boolean");
    }
}
