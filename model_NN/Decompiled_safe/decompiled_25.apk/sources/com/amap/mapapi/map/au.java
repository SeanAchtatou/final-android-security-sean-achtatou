package com.amap.mapapi.map;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.PointF;
import com.womenchild.hospital.parameter.HttpRequestParameters;

/* compiled from: Tile */
class au {
    private static Paint a = null;
    private static Bitmap b = null;
    private static int c = Color.rgb(222, (int) HttpRequestParameters.LISTHOSPITAL_BY_AREAID, (int) HttpRequestParameters.LISTHOSPITAL);

    au() {
    }

    public static int a() {
        return c;
    }

    public static Paint b() {
        if (a == null) {
            a = new Paint();
            a.setColor(-7829368);
            a.setAlpha(90);
            a.setPathEffect(new DashPathEffect(new float[]{2.0f, 2.5f}, 1.0f));
        }
        return a;
    }

    public static Bitmap c() {
        if (b == null) {
            AnonymousClass1 r0 = new g() {
                public void a(Canvas canvas) {
                    Paint b = au.b();
                    canvas.drawColor(au.a());
                    int i = 0;
                    while (true) {
                        int i2 = i;
                        if (i2 < 235) {
                            canvas.drawLine((float) i2, 0.0f, (float) i2, 256.0f, b);
                            canvas.drawLine(0.0f, (float) i2, 256.0f, (float) i2, b);
                            i = i2 + 21;
                        } else {
                            return;
                        }
                    }
                }
            };
            f fVar = new f(Bitmap.Config.ARGB_4444);
            fVar.a(256, 256);
            fVar.a(r0);
            b = fVar.b();
        }
        return b;
    }

    /* compiled from: Tile */
    static class a {
        public int a = 0;
        public final int b;
        public final int c;
        public final int d;
        public final int e;
        public PointF f;
        public int g = -1;

        public a(int i, int i2, int i3, int i4) {
            this.b = i;
            this.c = i2;
            this.d = i3;
            this.e = i4;
        }

        public a(a aVar) {
            this.b = aVar.b;
            this.c = aVar.c;
            this.d = aVar.d;
            this.e = aVar.e;
            this.f = aVar.f;
            this.a = aVar.a;
        }

        /* renamed from: a */
        public a clone() {
            return new a(this);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            if (this.b == aVar.b && this.c == aVar.c && this.d == aVar.d && this.e == aVar.e) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return (this.b * 7) + (this.c * 11) + (this.d * 13) + this.e;
        }

        public String toString() {
            return this.b + "-" + this.c + "-" + this.d + "-" + this.e;
        }
    }
}
