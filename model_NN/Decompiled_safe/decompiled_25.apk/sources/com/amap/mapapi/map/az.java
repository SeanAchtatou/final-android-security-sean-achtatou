package com.amap.mapapi.map;

import android.view.MotionEvent;

/* compiled from: TrackballGestureDetector */
public class az {
    private static az n;
    private boolean a;
    private float b;
    private float c;
    private long d;
    private boolean e;
    private boolean f;
    private boolean g;
    private float h;
    private float i;
    private float j;
    private float k;
    private Runnable l;
    private Thread m;

    /* compiled from: TrackballGestureDetector */
    public interface a {
        void a(az azVar);
    }

    private az() {
    }

    public static az a() {
        if (n == null) {
            n = new az();
        }
        return n;
    }

    private void b() {
        if (this.l != null && this.e && this.m == null) {
            this.m = new Thread(this.l);
            this.m.start();
        }
    }

    public void a(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        float y = motionEvent.getY();
        float x = motionEvent.getX();
        this.f = false;
        this.g = false;
        switch (action) {
            case 0:
                this.h = x;
                this.i = y;
                this.b = x;
                this.c = y;
                this.d = motionEvent.getDownTime();
                this.a = true;
                this.e = false;
                break;
            case 1:
                if (this.a && motionEvent.getEventTime() - this.d < 300) {
                    this.g = true;
                    break;
                }
            case 2:
                if (!this.e) {
                    this.j = this.h - x;
                    this.k = this.i - y;
                    this.h = x;
                    this.i = y;
                    if (Math.abs(y - this.c) + Math.abs(x - this.b) > 0.0f) {
                        this.a = false;
                        this.f = true;
                        break;
                    }
                }
                break;
            default:
                return;
        }
        b();
    }
}
