package com.amap.mapapi.map;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import com.amap.mapapi.core.b;

/* compiled from: ImageOverlay */
abstract class s extends p {
    protected ah a;
    protected Bitmap b;

    /* access modifiers changed from: protected */
    public abstract Point a();

    public s(ah ahVar, Bitmap bitmap) {
        this.a = ahVar;
        this.b = bitmap;
    }

    public Rect c() {
        Point a2 = a();
        if (this.b == null) {
            this.b = b.g.a(b.a.ewatermark.ordinal());
        }
        return new Rect(a2.x, a2.y, a2.x + this.b.getWidth(), a2.y + this.b.getHeight());
    }

    public boolean draw(Canvas canvas, MapView mapView, boolean z, long j) {
        if (this.b != null && this.b.isRecycled()) {
            this.b = b.g.a(b.a.ewatermark.ordinal());
        }
        if (this.b == null) {
            this.b = b.g.a(b.a.ewatermark.ordinal());
        }
        canvas.drawBitmap(this.b, (float) c().left, (float) c().top, (Paint) null);
        return true;
    }
}
