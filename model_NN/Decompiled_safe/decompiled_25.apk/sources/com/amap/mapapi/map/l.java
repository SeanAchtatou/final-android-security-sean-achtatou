package com.amap.mapapi.map;

import android.graphics.Point;
import com.amap.mapapi.core.b;

/* compiled from: CompassOverlay */
class l extends s {
    private k c = new k(b.g.a(b.a.ecompassback.ordinal()), b.g.a(b.a.ecommpasspoint.ordinal()));
    private float d = 0.0f;

    public l(ah ahVar) {
        super(ahVar, null);
        d();
    }

    /* access modifiers changed from: protected */
    public Point a() {
        return new Point(15, 45);
    }

    public boolean a(float f) {
        boolean b = b(f);
        if (b) {
            this.d = f;
            d();
        }
        return b;
    }

    private void d() {
        this.b = this.c.a(this.d);
    }

    private boolean b(float f) {
        return Math.abs(f - this.d) > 3.0f;
    }

    public void b() {
        if (this.b != null && !this.b.isRecycled()) {
            this.b.recycle();
            this.b = null;
        }
    }
}
