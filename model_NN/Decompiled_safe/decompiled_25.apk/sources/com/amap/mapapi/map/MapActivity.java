package com.amap.mapapi.map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import com.amap.mapapi.core.a;
import com.amap.mapapi.core.b;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public abstract class MapActivity extends Activity {
    public static int MAP_MODE_BITMAP = 2;
    public static int MAP_MODE_VECTOR = 1;
    private static String b = null;
    boolean a = false;
    private Timer c = null;
    /* access modifiers changed from: private */
    public Handler d = new z(this);
    private TimerTask e = new aa(this);
    /* access modifiers changed from: private */
    public ArrayList<MapView> f = new ArrayList<>();
    private int g = MAP_MODE_BITMAP;

    public void setMapMode(int i) {
        this.g = i;
    }

    public void setCachInInstalledPackaget(boolean z) {
        this.a = z;
    }

    public int getMapMode() {
        return this.g;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a.a(this);
        if (this.c == null) {
            this.c = new Timer();
            this.c.schedule(this.e, 10000, 1000);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        b = null;
        if (this.c != null) {
            this.c.cancel();
            this.c = null;
        }
        int size = this.f.size();
        for (int i = 0; i < size; i++) {
            MapView mapView = this.f.get(0);
            if (mapView != null) {
                int childCount = mapView.getChildCount();
                for (int i2 = 0; i2 < childCount; i2++) {
                    if (mapView.getChildAt(i2) != null) {
                        if (mapView.getChildAt(i2).getBackground() != null) {
                            mapView.getChildAt(i2).getBackground().setCallback(null);
                        }
                        mapView.getChildAt(i2).setBackgroundDrawable(null);
                    }
                }
                ah a2 = mapView.a();
                if (a2 != null) {
                    a2.c.c();
                }
                mapView.d();
                if (mapView.a != null) {
                    try {
                        if (mapView.b != null && !mapView.b.isRecycled()) {
                            mapView.b.recycle();
                        }
                        mapView.b = null;
                    } catch (Throwable th) {
                        th.printStackTrace();
                    }
                }
                this.f.remove(0);
                b.f--;
            }
        }
        System.gc();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        ah a2;
        super.onPause();
        int size = this.f.size();
        for (int i = 0; i < size; i++) {
            MapView mapView = this.f.get(i);
            if (!(mapView == null || (a2 = mapView.a()) == null)) {
                a2.c.e();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        ah a2;
        super.onResume();
        int size = this.f.size();
        for (int i = 0; i < size; i++) {
            MapView mapView = this.f.get(i);
            if (!(mapView == null || (a2 = mapView.a()) == null)) {
                a2.c.d();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        ah a2;
        super.onRestart();
        int size = this.f.size();
        for (int i = 0; i < size; i++) {
            MapView mapView = this.f.get(i);
            if (!(mapView == null || (a2 = mapView.a()) == null)) {
                a2.c.f();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        ah a2;
        super.onStop();
        int size = this.f.size();
        for (int i = 0; i < size; i++) {
            MapView mapView = this.f.get(i);
            if (!(mapView == null || (a2 = mapView.a()) == null)) {
                a2.c.a();
            }
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    /* access modifiers changed from: package-private */
    public void a(MapView mapView, Context context, String str, String str2) {
        b.f++;
        mapView.a(context, str);
        this.f.add(mapView);
        if (b == null || b.length() < 15) {
            b = str2;
        }
        new aj(context).a();
    }

    public static String getMapApiKey() {
        return b;
    }

    /* access modifiers changed from: protected */
    public boolean isLocationDisplayed() {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean isRouteDisplayed() {
        return false;
    }
}
