package com.amap.mapapi.map;

/* compiled from: ThreadPool */
class at {
    private Thread[] a;

    public at(int i, Runnable runnable, Runnable runnable2) {
        this.a = new Thread[i];
        for (int i2 = 0; i2 < i; i2++) {
            if (i2 != 0 || i <= 1) {
                this.a[i2] = new Thread(runnable2);
            } else {
                this.a[i2] = new Thread(runnable);
            }
        }
    }

    public void a() {
        for (Thread thread : this.a) {
            thread.setDaemon(true);
            thread.start();
        }
    }

    public void b() {
        int length = this.a.length;
        if (length > 1) {
            try {
                this.a[0].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        for (int i = 1; i < length; i++) {
            try {
                this.a[i].join(300);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        }
    }

    public void c() {
        if (this.a != null) {
            int length = this.a.length;
            for (int i = 0; i < length; i++) {
                this.a[i].interrupt();
                this.a[i] = null;
            }
            this.a = null;
        }
    }
}
