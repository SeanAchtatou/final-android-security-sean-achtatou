package com.amap.mapapi.map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorListener;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.v4.view.MotionEventCompat;
import android.util.Log;
import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.core.b;
import com.amap.mapapi.core.d;
import com.amap.mapapi.location.LocationManagerProxy;
import com.amap.mapapi.location.LocationProviderProxy;
import com.amap.mapapi.location.c;
import com.amap.mapapi.map.Overlay;
import java.util.Iterator;
import java.util.LinkedList;

public class MyLocationOverlay extends Overlay implements SensorEventListener, SensorListener, LocationListener, Overlay.Snappable {
    private ah a;
    private m b;
    private boolean c = false;
    private boolean d = false;
    private float e = Float.NaN;
    private l f;
    private r g;
    private final LinkedList<Runnable> h = new LinkedList<>();
    private LocationManagerProxy i;
    private c j;
    private Criteria k;
    private Location l;
    private Context m;
    private String n;

    public MyLocationOverlay(Context context, MapView mapView) {
        if (mapView == null) {
            throw new RuntimeException("MapView 不能为空！");
        }
        this.m = context;
        this.a = mapView.a();
        this.b = (m) this.a.e.a(2);
        this.e = 0.0f;
        this.f = new l(this.a);
        this.g = new r(-1, 1000, this.a, new Bitmap[]{b.g.a(b.a.eloc1.ordinal()), b.g.a(b.a.eloc1.ordinal())});
        if (this.j != null) {
            disableMyLocation();
        }
        c();
        disableCompass();
    }

    public float getOrientation() {
        return this.e;
    }

    public void disableCompass() {
        this.b.e();
        this.d = false;
    }

    public boolean enableCompass() {
        if (!this.b.a(this)) {
            return false;
        }
        this.d = true;
        return true;
    }

    public boolean isCompassEnabled() {
        return this.d;
    }

    public boolean isMyLocationEnabled() {
        return this.c;
    }

    public void disableMyLocation() {
        if (this.j != null) {
            this.j.a();
        }
        this.j = null;
        this.c = false;
        if (this.i != null) {
            this.i.destory();
        }
        this.i = null;
    }

    /* access modifiers changed from: package-private */
    public void a() {
    }

    public boolean enableMyLocation() {
        boolean z = false;
        if (this.j == null) {
            if (this.i == null) {
                this.i = LocationManagerProxy.getInstance(this.m);
            }
            this.j = new c(this.i);
            this.n = b();
            if (LocationProviderProxy.MapABCNetwork.equals(this.n)) {
                z = this.j.a(this, 10000, 5.0f, this.n);
            } else {
                z = this.j.a(this, 10000, 5.0f);
            }
            if (z) {
                this.l = this.i.getLastKnownLocation(this.n);
                this.c = true;
            }
        }
        return z;
    }

    private String b() {
        String str;
        String bestProvider = this.i.getBestProvider(this.k, true);
        if (bestProvider == null) {
            Iterator<String> it = this.i.getProviders(true).iterator();
            while (true) {
                str = bestProvider;
                if (!it.hasNext()) {
                    break;
                }
                bestProvider = it.next();
                if (!LocationManagerProxy.GPS_PROVIDER.equals(bestProvider) && !LocationManagerProxy.NETWORK_PROVIDER.equals(bestProvider)) {
                    bestProvider = str;
                }
            }
        } else {
            str = bestProvider;
        }
        Log.d("MyLocationOverlay", "getProvider " + str);
        return str;
    }

    private void c() {
        this.k = new Criteria();
        this.k.setAccuracy(2);
        this.k.setAltitudeRequired(false);
        this.k.setBearingRequired(false);
        this.k.setPowerRequirement(2);
    }

    public Location getLastFix() {
        return this.l;
    }

    public GeoPoint getMyLocation() {
        return a(getLastFix());
    }

    private GeoPoint a(Location location) {
        if (location != null) {
            return new GeoPoint(d.a(location.getLatitude()), d.a(location.getLongitude()));
        }
        return null;
    }

    private Rect d() {
        GeoPoint myLocation = getMyLocation();
        if (myLocation == null) {
            return null;
        }
        int h2 = this.g.h() / 2;
        int i2 = this.g.i() / 2;
        Point pixels = this.a.a.toPixels(myLocation, null);
        return new Rect(pixels.x - h2, pixels.y - i2, h2 + pixels.x, pixels.y + i2);
    }

    /* access modifiers changed from: protected */
    public boolean dispatchTap() {
        return false;
    }

    public boolean onTap(GeoPoint geoPoint, MapView mapView) {
        Rect d2;
        if (!this.c || (d2 = d()) == null) {
            return false;
        }
        Point pixels = this.a.a.toPixels(geoPoint, null);
        if (d2.contains(pixels.x, pixels.y)) {
            return dispatchTap();
        }
        return false;
    }

    public boolean runOnFirstFix(Runnable runnable) {
        if (this.l == null || this.j == null) {
            this.h.addLast(runnable);
            return false;
        }
        new Thread(runnable).start();
        return true;
    }

    public boolean onSnapToItem(int i2, int i3, Point point, MapView mapView) {
        GeoPoint myLocation = getMyLocation();
        if (myLocation == null) {
            return false;
        }
        Point pixels = mapView.getProjection().toPixels(myLocation, null);
        point.x = pixels.x;
        point.y = pixels.y;
        double d2 = (double) (i2 - pixels.x);
        double d3 = (double) (i3 - pixels.y);
        if ((d2 * d2) + (d3 * d3) < 64.0d) {
            return true;
        }
        return false;
    }

    public void onLocationChanged(Location location) {
        Log.d("MyLocationOverlay", "onLocationChanged " + location.getLatitude() + "," + location.getLongitude() + "," + location.getAccuracy());
        if (location != null) {
            this.l = location;
            if (this.a.d != null) {
                this.a.d.d();
            }
            if (this.h != null && this.h.size() > 0) {
                Iterator<Runnable> it = this.h.iterator();
                while (it.hasNext()) {
                    Runnable next = it.next();
                    if (next != null) {
                        new Thread(next).start();
                    }
                }
                this.h.clear();
            }
        }
    }

    public void onStatusChanged(String str, int i2, Bundle bundle) {
        Log.d("MyLocationOverlay", "onStatusChanged " + str + " " + i2);
        if (this.c && str != null && str.equals(this.n)) {
            if (i2 == 0 || i2 == 1) {
                this.n = b();
                if (LocationProviderProxy.MapABCNetwork.equals(this.n)) {
                    this.j.a(this, 10000, 5.0f, this.n);
                } else {
                    this.j.a(this, 10000, 5.0f);
                }
            }
        }
    }

    public void onProviderEnabled(String str) {
        Log.d("MyLocationOverlay", "onProviderEnabled " + str);
    }

    public void onProviderDisabled(String str) {
        Log.d("MyLocationOverlay", "onProviderDisabled " + str);
    }

    public boolean draw(Canvas canvas, MapView mapView, boolean z, long j2) {
        Location lastFix;
        if (!z) {
            if (this.c && (lastFix = getLastFix()) != null) {
                drawMyLocation(canvas, this.a.b.g(), lastFix, a(lastFix), j2);
            }
            if (this.d) {
                drawCompass(canvas, this.e);
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void drawMyLocation(Canvas canvas, MapView mapView, Location location, GeoPoint geoPoint, long j2) {
        Point pixels = this.a.a.toPixels(geoPoint, null);
        float f2 = 500.0f;
        Paint paint = new Paint();
        paint.setColor(-16776961);
        paint.setAlpha(40);
        ai a2 = mapView.b().a();
        if (!location.equals(LocationProviderProxy.MapABCNetwork) && location.hasAccuracy() && location.getAccuracy() > 0.0f) {
            f2 = a2.m ? ai.j * location.getAccuracy() : bj.h * location.getAccuracy();
        }
        canvas.drawCircle((float) pixels.x, (float) pixels.y, (float) ((int) mapView.getProjection().metersToEquatorPixels(f2)), paint);
        paint.setAlpha(MotionEventCompat.ACTION_MASK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setAntiAlias(true);
        canvas.drawCircle((float) pixels.x, (float) pixels.y, (float) ((int) mapView.getProjection().metersToEquatorPixels(f2)), paint);
        this.g.a(canvas, pixels.x, pixels.y);
    }

    /* access modifiers changed from: protected */
    public void drawCompass(Canvas canvas, float f2) {
        this.f.a(f2);
        this.f.draw(canvas, this.a.b.g(), false, 0);
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        this.e = sensorEvent.values[0];
        if (this.a.d != null) {
            this.a.d.a(this.f.c().left, this.f.c().top, this.f.b.getWidth() + this.f.a().x, this.f.b.getHeight() + this.f.a().y);
        }
    }

    public void onAccuracyChanged(Sensor sensor, int i2) {
    }

    public void onAccuracyChanged(int i2, int i3) {
    }

    public void onSensorChanged(int i2, float[] fArr) {
        this.e = fArr[0];
        if (this.a.d != null) {
            this.a.d.a(this.f.c().left, this.f.c().top, this.f.b.getWidth() + this.f.a().x, this.f.b.getHeight() + this.f.a().y);
        }
    }
}
