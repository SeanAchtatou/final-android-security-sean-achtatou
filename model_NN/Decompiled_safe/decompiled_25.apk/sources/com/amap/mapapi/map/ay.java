package com.amap.mapapi.map;

import android.graphics.Bitmap;
import com.amap.mapapi.core.AMapException;
import com.amap.mapapi.core.l;
import com.amap.mapapi.core.s;
import com.amap.mapapi.map.au;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: TileServerHandler */
class ay extends l<ArrayList<au.a>, ArrayList<au.a>> {
    private w i = null;

    public ay(ArrayList<au.a> arrayList, Proxy proxy, String str, String str2) {
        super(arrayList, proxy, str, str2);
    }

    public void a(w wVar) {
        this.i = wVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ArrayList<au.a> k() {
        ArrayList<au.a> arrayList = new ArrayList<>();
        Iterator it = ((ArrayList) this.b).iterator();
        while (it.hasNext()) {
            arrayList.add(new au.a((au.a) it.next()));
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ArrayList<au.a> c(InputStream inputStream) throws AMapException {
        ArrayList<au.a> arrayList = null;
        if (this.b != null) {
            int size = ((ArrayList) this.b).size();
            int i2 = 0;
            while (i2 < size) {
                au.a aVar = (au.a) ((ArrayList) this.b).get(i2);
                if (a(inputStream, aVar) < 0) {
                    if (arrayList == null) {
                        arrayList = new ArrayList<>();
                    }
                    arrayList.add(new au.a(aVar));
                }
                i2++;
                arrayList = arrayList;
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new AMapException(AMapException.ERROR_IO);
                }
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public String[] f() {
        return null;
    }

    /* access modifiers changed from: protected */
    public String h() {
        return this.i.j.a(((au.a) ((ArrayList) this.b).get(0)).b, ((au.a) ((ArrayList) this.b).get(0)).c, ((au.a) ((ArrayList) this.b).get(0)).d);
    }

    public int a(InputStream inputStream, au.a aVar) {
        if (aVar == null || inputStream == null) {
            return -1;
        }
        if (this.i == null || this.i.m == null) {
            return -1;
        }
        int a = this.i.m.a(null, inputStream, false, null, aVar.b + "-" + aVar.c + "-" + aVar.d);
        if (a < 0) {
            return -1;
        }
        a(aVar, a);
        if (this.i == null || !this.i.g) {
            return a;
        }
        byte[] a2 = a(this.i.m.a(a));
        if (this.i == null || this.i.n == null) {
            return a;
        }
        this.i.n.a(a2, aVar.b, aVar.c, aVar.d);
        return a;
    }

    private byte[] a(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    private void a(au.a aVar, int i2) {
        if (aVar != null && i2 >= 0 && this.i != null && this.i.o != null) {
            s<au.a> sVar = this.i.o;
            int size = sVar.size();
            for (int i3 = 0; i3 < size; i3++) {
                au.a aVar2 = sVar.get(i3);
                if (aVar2 != null && aVar2.equals(aVar)) {
                    aVar2.g = i2;
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean e() {
        return true;
    }

    /* access modifiers changed from: protected */
    public byte[] g() {
        return null;
    }
}
