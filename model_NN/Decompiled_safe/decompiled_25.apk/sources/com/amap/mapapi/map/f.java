package com.amap.mapapi.map;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/* compiled from: BitmapDrawer */
class f {
    protected Bitmap a = null;
    protected Canvas b = null;
    protected Bitmap.Config c;

    public f(Bitmap.Config config) {
        this.c = config;
    }

    public void a(Bitmap bitmap) {
        this.a = bitmap;
        this.b = new Canvas(this.a);
    }

    public void a(int i, int i2) {
        a();
        this.a = Bitmap.createBitmap(i, i2, this.c);
        this.b = new Canvas(this.a);
    }

    public void a() {
        if (this.a != null) {
            this.a.recycle();
        }
        this.a = null;
        this.b = null;
    }

    public void a(g gVar) {
        this.b.save(1);
        gVar.a(this.b);
        this.b.restore();
    }

    public Bitmap b() {
        return this.a;
    }
}
