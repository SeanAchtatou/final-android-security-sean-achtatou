package com.amap.mapapi.map;

import java.util.Vector;

/* compiled from: ConnectionManager */
class am extends Vector {
    protected int a = -1;

    am() {
    }

    public synchronized Object a() {
        Object elementAt;
        if (c()) {
            elementAt = null;
        } else {
            elementAt = super.elementAt(0);
            super.removeElementAt(0);
        }
        return elementAt;
    }

    public synchronized Object b() {
        Object elementAt;
        if (c()) {
            elementAt = null;
        } else {
            elementAt = super.elementAt(0);
        }
        return elementAt;
    }

    public boolean c() {
        return super.isEmpty();
    }

    public synchronized void clear() {
        super.removeAllElements();
    }
}
