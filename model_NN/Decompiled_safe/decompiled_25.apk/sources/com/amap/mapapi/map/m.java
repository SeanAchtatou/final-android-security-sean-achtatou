package com.amap.mapapi.map;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

/* compiled from: CompassServer */
class m extends ae {
    private SensorManager a;
    private Sensor b;
    private SensorEventListener c = null;

    public m(ah ahVar, Context context) {
        super(ahVar, context);
        this.a = (SensorManager) context.getSystemService("sensor");
        this.b = this.a.getDefaultSensor(3);
    }

    public boolean a(SensorEventListener sensorEventListener) {
        g();
        this.c = sensorEventListener;
        return i();
    }

    public void e() {
        g();
        this.c = null;
    }

    public void a_() {
        i();
    }

    public void c() {
        g();
    }

    private void g() {
        if (this.c != null) {
            try {
                this.a.unregisterListener(this.c);
            } catch (Exception e) {
            }
        }
    }

    private boolean i() {
        if (this.c == null) {
            return false;
        }
        try {
            return this.a.registerListener(this.c, this.b, 1);
        } catch (Exception e) {
            return false;
        }
    }
}
