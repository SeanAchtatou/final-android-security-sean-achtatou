package com.amap.mapapi.map;

import android.os.Handler;
import android.os.Looper;

/* compiled from: AnimBase */
abstract class a {
    protected int a = 0;
    public boolean b = false;
    protected int c;
    protected int d;
    /* access modifiers changed from: private */
    public Handler e = null;
    private Runnable f = new b(this);

    /* access modifiers changed from: protected */
    public abstract void a();

    /* access modifiers changed from: protected */
    public abstract void b();

    public a(int i, int i2) {
        this.c = i;
        this.d = i2;
    }

    public void c() {
        if (!f()) {
            this.e = new Handler(Looper.getMainLooper());
            this.b = true;
            this.a = 0;
        }
        g();
    }

    public void d() {
        h();
        this.f.run();
    }

    private void h() {
        this.b = false;
    }

    /* access modifiers changed from: protected */
    public void e() {
        this.a += this.d;
        if (this.c != -1 && this.a > this.c) {
            h();
        }
    }

    public boolean f() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public void g() {
        this.e.post(this.f);
    }
}
