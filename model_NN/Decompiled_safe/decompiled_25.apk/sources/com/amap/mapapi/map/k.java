package com.amap.mapapi.map;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

/* compiled from: ComassMaker */
class k implements g {
    f a = new f(Bitmap.Config.ARGB_4444);
    private Drawable b;
    private Drawable c;
    private int d;
    private int e;
    private float f = 0.0f;

    public k(Bitmap bitmap, Bitmap bitmap2) {
        this.d = bitmap.getWidth();
        this.e = bitmap.getHeight();
        this.b = a(bitmap);
        this.c = a(bitmap2);
    }

    private Drawable a(Bitmap bitmap) {
        BitmapDrawable bitmapDrawable = new BitmapDrawable(bitmap);
        bitmapDrawable.setBounds(0, 0, this.d, this.e);
        return bitmapDrawable;
    }

    public Bitmap a(float f2) {
        this.f = f2;
        this.a.a(this.d, this.e);
        this.a.a(this);
        return this.a.b();
    }

    public void a(Canvas canvas) {
        this.b.draw(canvas);
        canvas.rotate(-this.f, (float) (this.d / 2), (float) (this.e / 2));
        this.c.draw(canvas);
    }
}
