package com.amap.mapapi.map;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;

/* compiled from: TaskPool */
class as<T> {
    protected LinkedList<T> a = new LinkedList<>();
    protected final Semaphore b = new Semaphore(0, false);
    protected boolean c = true;

    as() {
    }

    public void a() {
        this.c = false;
        this.b.release(100);
    }

    public synchronized void a(List list, boolean z) {
        if (this.a != null) {
            if (z) {
                this.a.clear();
            }
            if (list != null) {
                this.a.addAll(list);
            }
            b();
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        if (this.a != null && this.c && this.a.size() != 0) {
            this.b.release();
        }
    }

    public void c() {
        if (this.a != null) {
            this.a.clear();
        }
    }

    public ArrayList<T> a(int i, boolean z) {
        if (this.a == null) {
            return null;
        }
        try {
            this.b.acquire();
        } catch (InterruptedException e) {
        }
        if (this.c) {
            return b(i, z);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public synchronized ArrayList<T> b(int i, boolean z) {
        ArrayList<T> arrayList;
        synchronized (this) {
            if (this.a == null) {
                arrayList = null;
            } else {
                int size = this.a.size();
                if (i > size) {
                    i = size;
                }
                arrayList = new ArrayList<>(i);
                for (int i2 = 0; i2 < i; i2++) {
                    arrayList.add(this.a.get(0));
                    this.a.removeFirst();
                }
                b();
            }
        }
        return arrayList;
    }
}
