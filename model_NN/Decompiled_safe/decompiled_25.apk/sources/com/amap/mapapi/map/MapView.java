package com.amap.mapapi.map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.TimeUtils;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.support.v4.widget.ViewDragHelper;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Scroller;
import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.core.b;
import com.amap.mapapi.core.i;
import com.amap.mapapi.core.r;
import com.amap.mapapi.map.ZoomButtonsController;
import com.amap.mapapi.map.ai;
import com.amap.mapapi.map.az;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.amap.mapapi.route.Route;
import com.mapabc.minimap.map.vmap.NativeMap;
import com.mapabc.minimap.map.vmap.NativeMapEngine;
import com.mapabc.minimap.map.vmap.VMapProjection;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

public class MapView extends ViewGroup {
    public boolean VMapMode;
    public boolean VisInited;
    NativeMap a;
    Bitmap b;
    protected boolean bfirstDrawed;
    int[] c;
    public int centerX;
    public int centerY;
    ZoomButtonsController d;
    boolean e;
    NativeMapEngine f;
    ByteBuffer g;
    Bitmap h;
    public int height;
    n i;
    public boolean isInited;
    an j;
    /* access modifiers changed from: private */
    public MapActivity k;
    /* access modifiers changed from: private */
    public ah l;
    private c m;
    public e mRouteCtrl;
    public int mapAngle;
    public int mapLevel;
    /* access modifiers changed from: private */
    public MapController n;
    /* access modifiers changed from: private */
    public g o;
    private boolean p;
    private v q;
    private final int[] r;
    /* access modifiers changed from: private */
    public boolean s;
    private az t;
    public aw tileDownloadCtrl;
    private az.a u;
    private boolean v;
    /* access modifiers changed from: private */
    public int w;
    public int width;
    private d x;
    private Context y;

    public enum ReticleDrawMode {
        DRAW_RETICLE_NEVER,
        DRAW_RETICLE_OVER,
        DRAW_RETICLE_UNDER
    }

    public interface b {
        void a(int i);
    }

    public static abstract class d {
        public abstract void a(int i, int i2, int i3, int i4);
    }

    static /* synthetic */ int f(MapView mapView) {
        int i2 = mapView.w + 1;
        mapView.w = i2;
        return i2;
    }

    public ZoomButtonsController getZoomButtonsController() {
        if (this.d == null) {
            this.d = new ZoomButtonsController(this);
        }
        return this.d;
    }

    private void m() {
        Method method;
        Method[] methods = View.class.getMethods();
        int length = methods.length;
        int i2 = 0;
        while (true) {
            if (i2 >= length) {
                method = null;
                break;
            }
            method = methods[i2];
            if (method.getName().equals("setLayerType")) {
                break;
            }
            i2++;
        }
        if (method != null) {
            try {
                method.invoke(this, Integer.valueOf(View.class.getField("LAYER_TYPE_SOFTWARE").getInt(null)), null);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public MapView(Context context) {
        super(context);
        this.p = false;
        this.r = new int[]{3000000, 1500000, 800000, 400000, 200000, 100000, 50000, 25000, 12000, 6000, 3000, 1500, 800, HttpRequestParameters.CANCEL_COMPANION_ORDER, HttpRequestParameters.DEPT_DETAIL, 100, 50, 25};
        this.s = true;
        this.VMapMode = false;
        this.VisInited = false;
        this.c = new int[2];
        this.v = false;
        this.e = false;
        this.w = 0;
        this.x = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = null;
        this.tileDownloadCtrl = null;
        this.mapLevel = 12;
        this.mapAngle = 0;
        this.isInited = false;
        m();
        setClickable(true);
        if (context instanceof MapActivity) {
            ((MapActivity) context).a(this, context, null, null);
            return;
        }
        throw new IllegalArgumentException("MapViews can only be created inside instances of MapActivity.");
    }

    public MapView(Context context, String str) {
        super(context);
        this.p = false;
        this.r = new int[]{3000000, 1500000, 800000, 400000, 200000, 100000, 50000, 25000, 12000, 6000, 3000, 1500, 800, HttpRequestParameters.CANCEL_COMPANION_ORDER, HttpRequestParameters.DEPT_DETAIL, 100, 50, 25};
        this.s = true;
        this.VMapMode = false;
        this.VisInited = false;
        this.c = new int[2];
        this.v = false;
        this.e = false;
        this.w = 0;
        this.x = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = null;
        this.tileDownloadCtrl = null;
        this.mapLevel = 12;
        this.mapAngle = 0;
        this.isInited = false;
        m();
        setClickable(true);
        if (context instanceof MapActivity) {
            ((MapActivity) context).a(this, context, null, str);
            return;
        }
        throw new IllegalArgumentException("MapViews can only be created inside instances of MapActivity.");
    }

    public MapView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MapView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        String str;
        String str2;
        String str3 = null;
        this.p = false;
        this.r = new int[]{3000000, 1500000, 800000, 400000, 200000, 100000, 50000, 25000, 12000, 6000, 3000, 1500, 800, HttpRequestParameters.CANCEL_COMPANION_ORDER, HttpRequestParameters.DEPT_DETAIL, 100, 50, 25};
        this.s = true;
        this.VMapMode = false;
        this.VisInited = false;
        this.c = new int[2];
        this.v = false;
        this.e = false;
        this.w = 0;
        this.x = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = null;
        this.tileDownloadCtrl = null;
        this.mapLevel = 12;
        this.mapAngle = 0;
        this.isInited = false;
        m();
        this.y = context;
        int attributeCount = attributeSet.getAttributeCount();
        int i3 = 0;
        String str4 = null;
        while (i3 < attributeCount) {
            String lowerCase = attributeSet.getAttributeName(i3).toLowerCase();
            if (lowerCase.equals("amapapikey")) {
                String str5 = str3;
                str2 = attributeSet.getAttributeValue(i3);
                str = str5;
            } else if (lowerCase.equals("useragent")) {
                str = attributeSet.getAttributeValue(i3);
                str2 = str4;
            } else {
                if (lowerCase.equals("clickable")) {
                    this.p = attributeSet.getAttributeValue(i3).equals("true");
                }
                str = str3;
                str2 = str4;
            }
            i3++;
            str4 = str2;
            str3 = str;
        }
        str4 = (str4 == null || str4.length() < 15) ? context.obtainStyledAttributes(attributeSet, new int[]{16843281}).getString(0) : str4;
        if (context instanceof MapActivity) {
            ((MapActivity) context).a(this, context, str3, str4);
            return;
        }
        throw new IllegalArgumentException("MapViews can only be created inside instances of MapActivity.");
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, String str) {
        this.y = context;
        try {
            this.k = (MapActivity) context;
            n();
            this.e = this.k.a;
            if (this.k.getMapMode() == MapActivity.MAP_MODE_VECTOR) {
                this.VMapMode = true;
            }
            setBackgroundColor(Color.rgb(222, (int) HttpRequestParameters.LISTHOSPITAL_BY_AREAID, (int) HttpRequestParameters.LISTHOSPITAL));
            this.l = new ah(this.k, this, str);
            this.o = new g(this.y);
            this.mRouteCtrl = new e();
            this.n = new MapController(this.l);
            setEnabled(true);
        } catch (Exception e2) {
            throw new IllegalArgumentException("can only be created inside instances of MapActivity");
        }
    }

    public void setMapProjectSetting(ad adVar) {
        this.l.f.a(adVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amap.mapapi.map.ah.d.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.amap.mapapi.map.ah.d.a(android.view.View, com.amap.mapapi.map.MapView$LayoutParams):void
      com.amap.mapapi.map.ah.d.a(int, int):void
      com.amap.mapapi.map.ah.d.a(boolean, boolean):void */
    public void setScreenHotPoint(Point point) {
        this.l.f.a(point);
        this.l.b.a(false, false);
    }

    public void setMapMoveEnable(boolean z) {
        com.amap.mapapi.core.b.n = z;
    }

    public void setServerUrl(r rVar) {
        if (rVar != null) {
            if (rVar.d != null && !rVar.d.equals(PoiTypeDef.All)) {
                i.a().a(rVar.d);
            }
            if (rVar.e != null && !rVar.e.equals(PoiTypeDef.All)) {
                i.a().b(rVar.e);
            }
            if (rVar.c != null && !rVar.c.equals(PoiTypeDef.All)) {
                i.a().e(rVar.c);
            }
            if (rVar.a != null && !rVar.a.equals(PoiTypeDef.All)) {
                i.a().c(rVar.a);
            }
            if (rVar.b != null && !rVar.b.equals(PoiTypeDef.All)) {
                i.a().d(rVar.b);
            }
            if (rVar.f != null && !rVar.f.equals(PoiTypeDef.All)) {
                i.a().f(rVar.f);
            }
        }
    }

    public boolean isDoubleClickZooming() {
        return this.s;
    }

    public void setDoubleClickZooming(boolean z) {
        this.s = z;
    }

    public void registerTrackballListener(az.a aVar) {
        this.u = aVar;
    }

    public void setEnabled(boolean z) {
        com.amap.mapapi.core.b.m = z;
        super.setEnabled(z);
    }

    public g getZoomMgr() {
        return this.o;
    }

    public v getLayerMgr() {
        if (isVectorMap()) {
            return null;
        }
        if (this.q == null) {
            this.q = new v();
            this.q.a(this);
        }
        return this.q;
    }

    /* access modifiers changed from: package-private */
    public ah a() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public c b() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        this.w = i2;
    }

    public void setReticleDrawMode(ReticleDrawMode reticleDrawMode) {
        this.l.d.a(reticleDrawMode);
    }

    public GeoPoint getMapCenter() {
        return this.l.b.f();
    }

    public MapController getController() {
        return this.n;
    }

    public final List<Overlay> getOverlays() {
        return this.l.d.c();
    }

    public int getLatitudeSpan() {
        return this.l.a.a();
    }

    public int getLongitudeSpan() {
        return this.l.a.b();
    }

    public int getZoomLevel() {
        return this.l.b.e();
    }

    public int getMaxZoomLevel() {
        return this.l.b.a();
    }

    public int getMinZoomLevel() {
        return this.l.b.b();
    }

    public Projection getProjection() {
        return this.l.a;
    }

    public String getDebugVersion() {
        return com.amap.mapapi.core.b.k;
    }

    public String getReleaseVersion() {
        return com.amap.mapapi.core.b.l;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        this.o.e();
    }

    public boolean canCoverCenter() {
        return this.l.d.a(getMapCenter());
    }

    public void setBuiltInZoomControls(boolean z) {
        this.o.b(z);
    }

    public void displayZoomControls(boolean z) {
        this.o.c(z);
    }

    public void onFocusChanged(boolean z, int i2, Rect rect) {
        super.onFocusChanged(z, i2, rect);
    }

    private void n() {
        this.m = new c(this.k);
        addView(this.m, 0, new ViewGroup.LayoutParams(-1, -1));
    }

    /* access modifiers changed from: protected */
    public LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(-2, -2, 0, 0, 51);
    }

    public void preLoad() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amap.mapapi.map.ah.d.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.amap.mapapi.map.ah.d.a(android.view.View, com.amap.mapapi.map.MapView$LayoutParams):void
      com.amap.mapapi.map.ah.d.a(int, int):void
      com.amap.mapapi.map.ah.d.a(boolean, boolean):void */
    public void setVectorMap(boolean z) {
        int i2;
        if (this.VMapMode != z) {
            GeoPoint mapCenter = getMapCenter();
            int e2 = a().b.e();
            this.VMapMode = z;
            if (z) {
                a().b.b(com.amap.mapapi.core.b.c);
                a().b.c(com.amap.mapapi.core.b.d);
                a().f.a();
                ax axVar = (ax) a().e.a(0);
                axVar.b();
                axVar.c();
                axVar.a();
                a().e.a(null, 0);
                if (((bf) a().e.a(1)) == null) {
                    a().e.a(new bf(a(), getContext()), 1);
                }
                this.width = getWidth();
                this.height = getHeight();
                Vinit();
            } else {
                if (e2 <= com.amap.mapapi.core.b.b) {
                    i2 = com.amap.mapapi.core.b.b;
                } else {
                    i2 = e2;
                }
                if (i2 >= com.amap.mapapi.core.b.a) {
                    e2 = com.amap.mapapi.core.b.a;
                } else {
                    e2 = i2;
                }
                a().b.b(com.amap.mapapi.core.b.a);
                a().b.c(com.amap.mapapi.core.b.b);
                a().f.a();
                bf bfVar = (bf) a().e.a(1);
                if (bfVar != null) {
                    bfVar.b();
                    bfVar.c();
                    bfVar.a();
                    a().e.a(null, 1);
                    VdestoryMap();
                }
                if (((ax) a().e.a(0)) == null) {
                    ax axVar2 = new ax(a(), getContext());
                    axVar2.a_();
                    axVar2.d();
                    a().e.a(axVar2, 0);
                }
            }
            this.n.setCenter(mapCenter);
            a().b.a(e2);
            a().b.a(false, false);
            getZoomMgr().d();
        }
    }

    public boolean isVectorMap() {
        return this.VMapMode;
    }

    public void setMapviewSizeChangedListener(d dVar) {
        this.x = dVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amap.mapapi.map.ah.a.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.amap.mapapi.map.ah.a.a(com.amap.mapapi.map.p, boolean):void
      com.amap.mapapi.map.ah.a.a(int, android.view.KeyEvent):boolean
      com.amap.mapapi.map.ah.a.a(com.amap.mapapi.map.w, android.content.Context):boolean
      com.amap.mapapi.map.ah.a.a(java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amap.mapapi.map.ah.d.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.amap.mapapi.map.ah.d.a(android.view.View, com.amap.mapapi.map.MapView$LayoutParams):void
      com.amap.mapapi.map.ah.d.a(int, int):void
      com.amap.mapapi.map.ah.d.a(boolean, boolean):void */
    public void setSatellite(boolean z) {
        if (!isVectorMap() && isSatellite() != z) {
            boolean isTraffic = isTraffic();
            setTraffic(false);
            if (!z) {
                a().d.a(a().d.h, false);
                a().d.a(a().d.g, true);
                if (isTraffic) {
                    setTraffic(true);
                }
                a().b.a(false, false);
            } else if (a().d.a(a().d.h) != null) {
                a().d.a(a().d.h, true);
                if (isTraffic) {
                    setTraffic(true);
                }
                a().b.a(false, false);
            } else {
                w wVar = new w();
                wVar.j = new bd() {
                    public String a(int i, int i2, int i3) {
                        return i.a().e() + "/appmaptile?z=" + i3 + "&x=" + i + "&y=" + i2 + "&lang=zh_cn&size=1&scale=1&style=6";
                    }
                };
                wVar.a = a().d.h;
                wVar.e = true;
                wVar.d = true;
                wVar.f = true;
                wVar.g = true;
                wVar.b = com.amap.mapapi.core.b.a;
                wVar.c = com.amap.mapapi.core.b.b;
                a().d.a(wVar, getContext());
                a().d.a(a().d.h, true);
                if (isTraffic) {
                    setTraffic(true);
                }
                a().b.a(false, false);
            }
        }
    }

    public boolean isSatellite() {
        w a2 = a().d.a(a().d.h);
        if (a2 != null) {
            return a2.f;
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amap.mapapi.map.ah.a.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.amap.mapapi.map.ah.a.a(com.amap.mapapi.map.p, boolean):void
      com.amap.mapapi.map.ah.a.a(int, android.view.KeyEvent):boolean
      com.amap.mapapi.map.ah.a.a(com.amap.mapapi.map.w, android.content.Context):boolean
      com.amap.mapapi.map.ah.a.a(java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amap.mapapi.map.ah.d.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.amap.mapapi.map.ah.d.a(android.view.View, com.amap.mapapi.map.MapView$LayoutParams):void
      com.amap.mapapi.map.ah.d.a(int, int):void
      com.amap.mapapi.map.ah.d.a(boolean, boolean):void */
    public void setTraffic(boolean z) {
        boolean isSatellite;
        if (z != isTraffic() && !(isSatellite = isSatellite())) {
            String str = a().d.i;
            if (!z) {
                a().d.a(str, false);
                a().b.a(false, false);
            } else if (a().d.a(str) != null) {
                a().d.a(str, true);
                a().b.a(false, false);
            } else {
                if (isSatellite) {
                    w wVar = new w();
                    wVar.h = true;
                    wVar.i = 120000;
                    wVar.a = str;
                    wVar.e = false;
                    wVar.d = true;
                    wVar.f = true;
                    wVar.g = false;
                    wVar.b = 18;
                    wVar.c = 9;
                    a().d.a(wVar, getContext());
                } else {
                    w wVar2 = new w();
                    wVar2.h = true;
                    wVar2.i = 120000;
                    wVar2.j = new bd() {
                        public String a(int i, int i2, int i3) {
                            return i.a().c() + "/trafficengine/mapabc/traffictile?v=w2.61&zoom=" + (17 - i3) + "&x=" + i + "&y=" + i2;
                        }
                    };
                    wVar2.a = str;
                    wVar2.e = false;
                    wVar2.d = true;
                    wVar2.f = true;
                    wVar2.g = false;
                    wVar2.b = 18;
                    wVar2.c = 9;
                    a().d.a(wVar2, getContext());
                }
                a().d.a(str, true);
                a().b.a(false, false);
            }
        }
    }

    public boolean isTraffic() {
        String str;
        if (isVectorMap()) {
            return false;
        }
        if (!isSatellite()) {
            str = a().d.i;
        } else {
            str = a().d.j;
        }
        w a2 = a().d.a(str);
        if (a2 != null) {
            return a2.f;
        }
        return false;
    }

    public void setStreetView(boolean z) {
    }

    public boolean isStreetView() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public void d() {
        if (t.a != null) {
            t.a.c();
        }
        VdestoryMap();
        this.l.a();
        if (this.n != null) {
            this.n.stopAnimation(true);
            this.n.stopPanning();
        }
        this.n = null;
        this.m = null;
        this.l = null;
        if (this.mRouteCtrl != null) {
            this.mRouteCtrl.a();
            this.mRouteCtrl = null;
        }
        if (this.o != null) {
            this.o.c();
            this.o = null;
        }
    }

    /* access modifiers changed from: package-private */
    public int b(int i2) {
        if (i2 < this.l.b.b()) {
            i2 = this.l.b.b();
        }
        if (i2 > this.l.b.a()) {
            return this.l.b.a();
        }
        return i2;
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        if (-1 == indexOfChild(this.m)) {
            addView(this.m, 0, new ViewGroup.LayoutParams(-1, -1));
        }
        super.dispatchDraw(canvas);
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        if (this.l != null) {
            this.l.b.h();
        }
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new LayoutParams(layoutParams);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(this.k, attributeSet);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        super.onRestoreInstanceState(parcelable);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        return super.onSaveInstanceState();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        if (this.VMapMode && this.isInited) {
            VdestoryMap();
        }
        super.onDetachedFromWindow();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amap.mapapi.map.ah.d.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.amap.mapapi.map.ah.d.a(android.view.View, com.amap.mapapi.map.MapView$LayoutParams):void
      com.amap.mapapi.map.ah.d.a(int, int):void
      com.amap.mapapi.map.ah.d.a(boolean, boolean):void */
    public void computeScroll() {
        if (this.m.h.computeScrollOffset()) {
            int currX = this.m.h.getCurrX() - this.m.i;
            int currY = this.m.h.getCurrY() - this.m.j;
            int unused = this.m.i = this.m.h.getCurrX();
            int unused2 = this.m.j = this.m.h.getCurrY();
            GeoPoint fromPixels = this.l.a.fromPixels(currX + this.l.f.k.x, currY + this.l.f.k.y);
            if (this.m.h.isFinished()) {
                this.l.b.a(false, false);
            } else {
                this.l.b.b(fromPixels);
            }
        } else {
            super.computeScroll();
        }
    }

    public void setClickable(boolean z) {
        this.p = z;
        super.setClickable(z);
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (this.l == null) {
            return true;
        }
        if (!this.p) {
            return false;
        }
        if (this.l.d.a(i2, keyEvent) || this.n.onKey(this, i2, keyEvent)) {
            return true;
        }
        return false;
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (this.l == null) {
            return true;
        }
        if (!this.p) {
            return false;
        }
        if (this.l.d.b(i2, keyEvent) || this.n.onKey(this, i2, keyEvent)) {
            return true;
        }
        return false;
    }

    public boolean onTrackballEvent(MotionEvent motionEvent) {
        if (this.l == null) {
            return true;
        }
        if (!this.p || this.u == null) {
            return false;
        }
        this.t = az.a();
        this.t.a(motionEvent);
        this.u.a(this.t);
        if (!this.l.d.a(motionEvent)) {
            return this.m.a(motionEvent);
        }
        return true;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!com.amap.mapapi.core.b.m || this.l == null) {
            return true;
        }
        if (!this.p) {
            return false;
        }
        if (this.l.d.b(motionEvent)) {
            return true;
        }
        this.m.b(motionEvent);
        return super.onTouchEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        this.o.d();
        if (this.VMapMode) {
            if (this.isInited) {
                VdestoryMap();
            }
            this.width = getWidth();
            this.height = getHeight();
            Vinit();
            this.isInited = true;
        }
        this.l.f.a(new Point(i2 / 2, i3 / 2));
        this.o.a(i2, i3);
        this.mRouteCtrl.a(i2, i3);
        this.l.b.a(i2, i3);
        if (!(this.n.getReqLatSpan() == 0 || this.n.getReqLngSpan() == 0)) {
            this.n.zoomToSpan(this.n.getReqLatSpan(), this.n.getReqLngSpan());
            this.n.setReqLatSpan(0);
            this.n.setReqLngSpan(0);
        }
        if (this.x != null) {
            this.x.a(i2, i3, i4, i5);
        }
    }

    public int getScale(int i2) {
        if (i2 < getMinZoomLevel() || i2 > getMaxZoomLevel() || i2 < 0 || i2 > this.r.length) {
            return -1;
        }
        return this.r[i2 - 1];
    }

    public double getMetersPerPixel(int i2) {
        if (i2 < getMinZoomLevel() || i2 > getMaxZoomLevel()) {
            return 0.0d;
        }
        return a().f.h[i2];
    }

    public class e {
        public boolean a = false;
        /* access modifiers changed from: private */
        public ImageView[] c = null;
        private Drawable[] d = null;
        /* access modifiers changed from: private */
        public b e = null;
        private boolean f = false;
        private int g = 130;
        private int h = 85;
        private int i = 50;
        private int j = 35;
        private int k = 30;
        private int l = 25;
        private View.OnClickListener m = new af(this);

        public e() {
        }

        private void b(int i2, int i3) {
            String[] strArr = {"overview.png", "detail.png", "prev.png", "next.png", "overview_disable.png", "detail_disable.png", "prev_disable.png", "next_disable.png"};
            for (int i4 = 0; i4 < 8; i4++) {
                this.d[i4] = com.amap.mapapi.core.b.g.b(MapView.this.k, strArr[i4]);
            }
            for (int i5 = 0; i5 < 4; i5++) {
                this.c[i5] = new ImageView(MapView.this.k);
                this.c[i5].setImageDrawable(this.d[i5]);
                MapView.this.addView(this.c[i5], MapView.this.generateDefaultLayoutParams());
                this.c[i5].setVisibility(4);
                this.c[i5].setOnClickListener(this.m);
            }
        }

        public void a() {
            BitmapDrawable bitmapDrawable;
            Bitmap bitmap;
            if (this.d != null) {
                int length = this.d.length;
                for (int i2 = 0; i2 < length; i2++) {
                    if (!(this.d[i2] == null || (bitmapDrawable = (BitmapDrawable) this.d[i2]) == null || (bitmap = bitmapDrawable.getBitmap()) == null || bitmap.isRecycled())) {
                        bitmap.recycle();
                    }
                }
            }
            this.d = null;
            if (this.c != null) {
                int length2 = this.c.length;
                for (int i3 = 0; i3 < length2; i3++) {
                    this.c[i3] = null;
                }
                this.c = null;
            }
        }

        public void a(int i2, int i3) {
            int i4;
            if (this.c != null && this.d != null) {
                int intrinsicWidth = this.c[0].getDrawable().getIntrinsicWidth();
                int i5 = 0;
                if (MapView.this.k.getResources().getConfiguration().orientation == 1) {
                    i5 = (i2 / 2) - (intrinsicWidth + 8);
                    i4 = (i2 / 2) + intrinsicWidth + 8;
                } else if (MapView.this.k.getResources().getConfiguration().orientation == 2) {
                    i5 = (i2 / 2) - (intrinsicWidth + 15);
                    i4 = (i2 / 2) + intrinsicWidth + 15;
                } else {
                    i4 = 0;
                }
                LayoutParams layoutParams = new LayoutParams(-2, -2, i5, i3, 85);
                LayoutParams layoutParams2 = new LayoutParams(-2, -2, i5, i3, 83);
                LayoutParams layoutParams3 = new LayoutParams(-2, -2, i4, i3, 85);
                LayoutParams layoutParams4 = new LayoutParams(-2, -2, i4, i3, 83);
                MapView.this.updateViewLayout(this.c[0], layoutParams);
                MapView.this.updateViewLayout(this.c[1], layoutParams2);
                MapView.this.updateViewLayout(this.c[2], layoutParams3);
                MapView.this.updateViewLayout(this.c[3], layoutParams4);
            }
        }

        public void a(boolean z, b bVar) {
            int i2;
            boolean z2 = false;
            if (this.c == null || this.d == null) {
                this.c = new ImageView[4];
                this.d = new Drawable[8];
                if (com.amap.mapapi.core.b.e == 2) {
                    b(this.g, this.h);
                } else if (com.amap.mapapi.core.b.e == 1) {
                    b(this.k, this.l);
                } else {
                    b(this.i, this.j);
                }
                a(MapView.this.l.b.c(), MapView.this.l.b.d());
            }
            this.e = bVar;
            for (int i3 = 0; i3 < 4; i3++) {
                ImageView imageView = this.c[i3];
                if (z) {
                    i2 = 0;
                } else {
                    i2 = 4;
                }
                imageView.setVisibility(i2);
                if (z) {
                    this.c[i3].bringToFront();
                }
            }
            g c2 = MapView.this.o;
            if (!z) {
                z2 = true;
            }
            c2.a(z2);
            this.f = z;
        }

        public void a(boolean z) {
            a(2, z);
        }

        public void b(boolean z) {
            a(3, z);
        }

        public void c(boolean z) {
            a(1, z);
        }

        public void d(boolean z) {
            a(0, z);
        }

        public boolean b() {
            return this.f;
        }

        private void a(int i2, boolean z) {
            this.c[i2].setImageDrawable(this.d[z ? i2 : i2 + 4]);
        }
    }

    public class g {
        Bitmap a = null;
        Bitmap b = null;
        Bitmap c = null;
        Bitmap d = null;
        Bitmap e = null;
        Bitmap f = null;
        StateListDrawable g = null;
        StateListDrawable h = null;
        private f j;
        private f k;
        private q l;
        private boolean m = true;
        private boolean n = true;
        private Handler o = new Handler();
        private Runnable p = new ag(this);

        public void a(boolean z) {
            if (!this.m) {
                return;
            }
            if (MapView.this.mRouteCtrl == null || !MapView.this.mRouteCtrl.b()) {
                if (z) {
                    this.o.removeCallbacks(this.p);
                    this.o.postDelayed(this.p, 10000);
                }
                d();
                this.k.a(z);
                this.j.a(z);
                if (MapView.this.d != null && MapView.this.d.getOnZoomListener() != null && this.n != z) {
                    MapView.this.d.getOnZoomListener().onVisibilityChanged(z);
                    this.n = z;
                }
            }
        }

        public void a() {
            a(true);
        }

        public g(Context context) {
            h();
            b(false);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.amap.mapapi.map.ah.a.a(com.amap.mapapi.map.p, boolean):void
         arg types: [com.amap.mapapi.map.q, int]
         candidates:
          com.amap.mapapi.map.ah.a.a(int, android.view.KeyEvent):boolean
          com.amap.mapapi.map.ah.a.a(com.amap.mapapi.map.w, android.content.Context):boolean
          com.amap.mapapi.map.ah.a.a(java.lang.String, boolean):boolean
          com.amap.mapapi.map.ah.a.a(com.amap.mapapi.map.p, boolean):void */
        private void h() {
            this.k = new f(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            this.j = new f(4098);
            MapView.this.addView(this.k, MapView.this.generateDefaultLayoutParams());
            MapView.this.addView(this.j, MapView.this.generateDefaultLayoutParams());
            this.l = new q(new Rect(0, 0, 0, 0));
            MapView.this.l.d.a((p) this.l, true);
            this.g = i();
            this.h = j();
            this.j.setBackgroundDrawable(this.h);
            this.k.setBackgroundDrawable(this.g);
            d();
        }

        public void b(boolean z) {
            this.m = true;
            a(z);
            this.m = z;
            MapView.this.l.d.a(this.l, z);
        }

        public boolean b() {
            return this.k.isShown();
        }

        public void c(boolean z) {
            this.k.b(z);
            this.j.b(z);
        }

        public void c() {
            if (this.a != null) {
                this.a = null;
            }
            if (this.b != null) {
                this.b = null;
            }
            if (this.c != null) {
                this.c = null;
            }
            if (this.d != null) {
                this.d = null;
            }
            if (this.e != null) {
                this.e = null;
            }
            if (this.f != null) {
                this.f = null;
            }
            if (this.g != null) {
                this.g = null;
            }
            if (this.h != null) {
                this.h = null;
            }
        }

        private StateListDrawable i() {
            StateListDrawable stateListDrawable = new StateListDrawable();
            if (this.a == null || this.a.isRecycled()) {
                this.a = com.amap.mapapi.core.b.g.a(b.a.ezoomin.ordinal());
            }
            if (this.b == null || this.b.isRecycled()) {
                this.b = com.amap.mapapi.core.b.g.a(b.a.ezoomindisable.ordinal());
            }
            if (this.c == null || this.c.isRecycled()) {
                this.c = com.amap.mapapi.core.b.g.a(b.a.ezoominselected.ordinal());
            }
            BitmapDrawable bitmapDrawable = new BitmapDrawable(this.a);
            BitmapDrawable bitmapDrawable2 = new BitmapDrawable(this.c);
            BitmapDrawable bitmapDrawable3 = new BitmapDrawable(this.b);
            stateListDrawable.addState(MapView.PRESSED_ENABLED_STATE_SET, bitmapDrawable2);
            stateListDrawable.addState(MapView.ENABLED_STATE_SET, bitmapDrawable);
            stateListDrawable.addState(MapView.EMPTY_STATE_SET, bitmapDrawable3);
            return stateListDrawable;
        }

        private StateListDrawable j() {
            StateListDrawable stateListDrawable = new StateListDrawable();
            if (this.d == null || this.d.isRecycled()) {
                this.d = com.amap.mapapi.core.b.g.a(b.a.ezoomout.ordinal());
            }
            if (this.e == null || this.e.isRecycled()) {
                this.e = com.amap.mapapi.core.b.g.a(b.a.ezoomoutdisable.ordinal());
            }
            if (this.f == null || this.f.isRecycled()) {
                this.f = com.amap.mapapi.core.b.g.a(b.a.ezoomoutselected.ordinal());
            }
            BitmapDrawable bitmapDrawable = new BitmapDrawable(this.d);
            BitmapDrawable bitmapDrawable2 = new BitmapDrawable(this.f);
            BitmapDrawable bitmapDrawable3 = new BitmapDrawable(this.e);
            stateListDrawable.addState(MapView.PRESSED_ENABLED_STATE_SET, bitmapDrawable2);
            stateListDrawable.addState(MapView.ENABLED_STATE_SET, bitmapDrawable);
            stateListDrawable.addState(MapView.EMPTY_STATE_SET, bitmapDrawable3);
            return stateListDrawable;
        }

        public void d() {
            if (MapView.this.l.b.e() == MapView.this.l.b.b()) {
                this.j.setPressed(false);
                this.j.setEnabled(false);
            } else {
                this.j.setEnabled(true);
            }
            if (MapView.this.l.b.e() == MapView.this.l.b.a()) {
                this.k.setPressed(false);
                this.k.setEnabled(false);
                return;
            }
            this.k.setEnabled(true);
        }

        /* access modifiers changed from: private */
        public void a(int i2, int i3) {
            LayoutParams layoutParams = new LayoutParams(-2, -2, (i2 / 2) + 1, i3 - 8, 83);
            LayoutParams layoutParams2 = new LayoutParams(-2, -2, (i2 / 2) - 1, i3 - 8, 85);
            if (-1 == MapView.this.indexOfChild(this.k)) {
                MapView.this.addView(this.k, layoutParams);
            } else {
                MapView.this.updateViewLayout(this.k, layoutParams);
            }
            if (-1 == MapView.this.indexOfChild(this.j)) {
                MapView.this.addView(this.j, layoutParams2);
            } else {
                MapView.this.updateViewLayout(this.j, layoutParams2);
            }
        }

        public void e() {
            this.k.bringToFront();
            this.j.bringToFront();
        }

        public f f() {
            return this.k;
        }

        public f g() {
            return this.j;
        }
    }

    public class c extends ImageView implements GestureDetector.OnDoubleTapListener, GestureDetector.OnGestureListener, ai.b {
        private Point b = null;
        private GestureDetector c = new GestureDetector(this);
        private ai d;
        private boolean e = false;
        private ArrayList<GestureDetector.OnGestureListener> f = new ArrayList<>();
        private ArrayList<ai.b> g = new ArrayList<>();
        /* access modifiers changed from: private */
        public Scroller h;
        /* access modifiers changed from: private */
        public int i = 0;
        /* access modifiers changed from: private */
        public int j = 0;
        private Matrix k = new Matrix();
        private float l = 1.0f;
        private boolean m = false;
        private float n;
        private float o;
        private int p;
        private int q;
        private long r = 0;
        private int s = 0;
        private int t = 0;
        private final long u = 300;

        public c(Context context) {
            super(context);
            this.d = ai.a(context, this);
            this.h = new Scroller(context);
            new DisplayMetrics();
            DisplayMetrics displayMetrics = context.getApplicationContext().getResources().getDisplayMetrics();
            this.p = displayMetrics.widthPixels;
            this.q = displayMetrics.heightPixels;
            this.i = displayMetrics.widthPixels / 2;
            this.j = displayMetrics.heightPixels / 2;
        }

        public ai a() {
            return this.d;
        }

        /* access modifiers changed from: protected */
        public void onDraw(Canvas canvas) {
            MapView.this.l.d.a(canvas, this.k, this.n, this.o);
        }

        public void a(float f2) {
            this.l = f2;
        }

        public float b() {
            return this.l;
        }

        public void c() {
            this.n = 0.0f;
            this.o = 0.0f;
        }

        public boolean a(MotionEvent motionEvent) {
            MapView.this.o.a();
            int action = motionEvent.getAction();
            int x = (int) motionEvent.getX();
            int y = (int) motionEvent.getY();
            this.b = null;
            switch (action) {
                case 0:
                    this.b = new Point(x, y);
                    return false;
                case 1:
                default:
                    return false;
                case 2:
                    MapView.this.n.scrollBy((int) (motionEvent.getX() * 25.0f), (int) (motionEvent.getY() * 25.0f));
                    return false;
            }
        }

        private void e() {
            if (this.b != null) {
                int i2 = this.b.x - this.s;
                int i3 = this.b.y - this.t;
                this.b.x = this.s;
                this.b.y = this.t;
                MapView.this.n.scrollBy(i2, i3);
            }
        }

        private void a(int i2, int i3) {
            if (this.b != null) {
                this.s = i2;
                this.t = i3;
                e();
            }
        }

        public boolean b(MotionEvent motionEvent) {
            MapView.this.o.a();
            boolean a2 = this.d.a(motionEvent);
            if (!a2) {
                return this.c.onTouchEvent(motionEvent);
            }
            return a2;
        }

        public boolean onDown(MotionEvent motionEvent) {
            if (this.b == null) {
                this.b = new Point((int) motionEvent.getX(), (int) motionEvent.getY());
                return true;
            }
            this.b.set((int) motionEvent.getX(), (int) motionEvent.getY());
            return true;
        }

        public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
            this.h.fling(this.i, this.j, (((int) (-f2)) * 3) / 5, (((int) (-f3)) * 3) / 5, -this.p, this.p, -this.q, this.q);
            return true;
        }

        public void onLongPress(MotionEvent motionEvent) {
        }

        public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
            a((int) motionEvent2.getX(), (int) motionEvent2.getY());
            postInvalidate();
            return true;
        }

        public void onShowPress(MotionEvent motionEvent) {
        }

        public boolean onSingleTapUp(MotionEvent motionEvent) {
            MapView.this.l.d.c(motionEvent);
            Iterator<GestureDetector.OnGestureListener> it = this.f.iterator();
            while (it.hasNext()) {
                it.next().onSingleTapUp(motionEvent);
            }
            return false;
        }

        public void a(GestureDetector.OnGestureListener onGestureListener) {
            this.f.add(onGestureListener);
        }

        public void b(GestureDetector.OnGestureListener onGestureListener) {
            this.f.remove(onGestureListener);
        }

        public boolean a(float f2, float f3) {
            MapView.this.n.stopAnimation(true);
            if (this.m) {
                this.n += f2;
                this.o += f3;
            }
            invalidate();
            return this.m;
        }

        public boolean b(float f2) {
            a(f2);
            return false;
        }

        public boolean a(Matrix matrix) {
            return false;
        }

        public boolean b(Matrix matrix) {
            this.k.set(matrix);
            postInvalidate();
            return true;
        }

        public boolean a(float f2, PointF pointF) {
            MapView.this.l.d.f = false;
            a(f2, pointF, this.n, this.o);
            this.m = false;
            postInvalidateDelayed(8);
            return true;
        }

        public boolean a(PointF pointF) {
            MapView.this.l.d.a(true);
            MapView.this.l.d.f = true;
            this.m = true;
            return true;
        }

        public boolean onDoubleTap(MotionEvent motionEvent) {
            if (!MapView.this.s) {
                return true;
            }
            MapView.this.n.zoomInFixing((int) motionEvent.getX(), (int) motionEvent.getY());
            return true;
        }

        public boolean onDoubleTapEvent(MotionEvent motionEvent) {
            return false;
        }

        public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
            if (t.a == null || com.amap.mapapi.core.d.a() - d() <= 1000) {
                return false;
            }
            t.a.c();
            return false;
        }

        public void a(long j2) {
            this.r = j2;
        }

        public long d() {
            return this.r;
        }

        private void a(float f2, PointF pointF, float f3, float f4) {
            int floor;
            boolean z;
            int c2 = MapView.this.l.b.c() / 2;
            int d2 = MapView.this.l.b.d() / 2;
            if (f2 > 0.0f) {
                floor = (int) Math.floor((double) f2);
                z = true;
            } else if (f2 < 0.0f) {
                floor = (int) Math.floor((double) Math.abs(f2));
                z = false;
            } else {
                return;
            }
            ZoomButtonsController.OnZoomListener onZoomListener = MapView.this.getZoomButtonsController().getOnZoomListener();
            int b2 = MapView.this.b(z ? floor + MapView.this.l.b.e() : MapView.this.l.b.e() - floor);
            if (b2 != MapView.this.l.b.e()) {
                MapView.this.c[0] = MapView.this.c[1];
                MapView.this.c[1] = b2;
                if (MapView.this.c[0] != MapView.this.c[1]) {
                    if (onZoomListener != null) {
                        onZoomListener.onZoom(z);
                    }
                    GeoPoint fromPixels = MapView.this.l.a.fromPixels(c2, d2);
                    MapView.this.l.b.a(b2);
                    MapView.this.l.b.a(fromPixels);
                }
            }
        }
    }

    class f extends ImageView implements View.OnClickListener {
        private int b;

        public f(int i) {
            super(MapView.this.k);
            this.b = i;
            setClickable(true);
            setOnClickListener(this);
        }

        public void onClick(View view) {
            if (com.amap.mapapi.core.b.m) {
                MapView.f(MapView.this);
                if (4097 == this.b) {
                    if (MapView.this.l.b.g().VMapMode && MapView.this.l.b.e() < MapView.this.mapLevel && MapView.this.mapLevel < MapView.this.l.b.a()) {
                        MapView.this.mapLevel += MapView.this.w;
                    }
                    MapView.this.n.a(MapView.this.w);
                    if (!MapView.this.n.a()) {
                        int unused = MapView.this.w = 0;
                    }
                }
                if (4098 == this.b) {
                    if (MapView.this.l.b.g().VMapMode && MapView.this.l.b.e() < MapView.this.mapLevel && MapView.this.mapLevel > MapView.this.l.b.b()) {
                        MapView.this.mapLevel -= MapView.this.w;
                    }
                    MapView.this.n.b(MapView.this.w);
                    if (!MapView.this.n.a()) {
                        int unused2 = MapView.this.w = 0;
                    }
                }
            }
        }

        public void a(boolean z) {
            int i = z ? 0 : 4;
            if (getVisibility() != i) {
                setVisibility(i);
            }
        }

        public void b(boolean z) {
            setFocusable(z);
        }
    }

    public static class LayoutParams extends ViewGroup.LayoutParams {
        public static final int BOTTOM = 80;
        public static final int BOTTOM_CENTER = 81;
        public static final int CENTER = 17;
        public static final int CENTER_HORIZONTAL = 1;
        public static final int CENTER_VERTICAL = 16;
        public static final int LEFT = 3;
        public static final int MODE_MAP = 0;
        public static final int MODE_VIEW = 1;
        public static final int RIGHT = 5;
        public static final int TOP = 48;
        public static final int TOP_LEFT = 51;
        public int alignment;
        public int mode;
        public GeoPoint point;
        public int x;
        public int y;

        public LayoutParams(int i, int i2, GeoPoint geoPoint, int i3) {
            this(i, i2, geoPoint, 0, 0, i3);
        }

        public LayoutParams(int i, int i2, GeoPoint geoPoint, int i3, int i4, int i5) {
            super(i, i2);
            this.mode = 1;
            this.point = null;
            this.x = 0;
            this.y = 0;
            this.alignment = 51;
            this.mode = 0;
            this.point = geoPoint;
            this.x = i3;
            this.y = i4;
            this.alignment = i5;
        }

        public LayoutParams(int i, int i2, int i3, int i4, int i5) {
            super(i, i2);
            this.mode = 1;
            this.point = null;
            this.x = 0;
            this.y = 0;
            this.alignment = 51;
            this.x = i3;
            this.y = i4;
            this.alignment = i5;
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.mode = 1;
            this.point = null;
            this.x = 0;
            this.y = 0;
            this.alignment = 51;
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
            this.mode = 1;
            this.point = null;
            this.x = 0;
            this.y = 0;
            this.alignment = 51;
        }
    }

    /* access modifiers changed from: protected */
    public void Vinit() {
        if (!this.isInited) {
            this.g = ByteBuffer.allocate(AccessibilityEventCompat.TYPE_VIEW_TEXT_TRAVERSED_AT_MOVEMENT_GRANULARITY);
            this.h = Bitmap.createBitmap(256, 256, Bitmap.Config.RGB_565);
            this.f = new NativeMapEngine(getContext());
            this.f.initIconData(this.y);
            this.f.initStyleData(this.y);
            this.i = new n();
            this.tileDownloadCtrl = new aw(this);
            this.i.e = this;
            this.i.start();
            this.tileDownloadCtrl.start();
            this.j = new an(this);
            this.j.start();
            this.isInited = true;
        }
    }

    /* access modifiers changed from: protected */
    public void VdestoryMap() {
        if (this.i != null) {
            this.i.b();
        }
        if (this.tileDownloadCtrl != null) {
            this.tileDownloadCtrl.c();
        }
        if (this.j != null) {
            this.j.a();
            boolean z = true;
            while (z) {
                try {
                    this.j.join();
                    z = false;
                } catch (InterruptedException e2) {
                }
            }
        }
        if (this.h != null) {
            this.h.recycle();
            this.h = null;
            this.g = null;
        }
        this.isInited = false;
        if (this.f != null) {
            this.f.destory();
            this.f = null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amap.mapapi.map.ah.d.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.amap.mapapi.map.ah.d.a(android.view.View, com.amap.mapapi.map.MapView$LayoutParams):void
      com.amap.mapapi.map.ah.d.a(int, int):void
      com.amap.mapapi.map.ah.d.a(boolean, boolean):void */
    public void setMapAngle(int i2) {
        if (this.VMapMode) {
            this.mapAngle = i2;
            this.l.b.a(true, false);
        }
    }

    public int getMapAngle() {
        if (this.VMapMode) {
            return this.mapAngle;
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public boolean isGridInScreen(String str) {
        be beVar = new be();
        be beVar2 = new be();
        int c2 = c(this.mapLevel);
        int i2 = this.mapLevel - c2;
        a(beVar, beVar2);
        be PixelsToTile = VMapProjection.PixelsToTile(beVar.a >> i2, beVar.b >> i2);
        be PixelsToTile2 = VMapProjection.PixelsToTile(beVar2.a >> i2, beVar2.b >> i2);
        Point QuadKeyToTile = VMapProjection.QuadKeyToTile(str);
        if (str.length() == c2 && QuadKeyToTile.x >= PixelsToTile.a && QuadKeyToTile.x <= PixelsToTile2.a && QuadKeyToTile.y >= PixelsToTile.b && QuadKeyToTile.y <= PixelsToTile2.b) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean isAGridsInScreen(ArrayList<String> arrayList) {
        be beVar = new be();
        be beVar2 = new be();
        int c2 = c(this.mapLevel);
        int i2 = this.mapLevel - c2;
        a(beVar, beVar2);
        be PixelsToTile = VMapProjection.PixelsToTile(beVar.a >> i2, beVar.b >> i2);
        be PixelsToTile2 = VMapProjection.PixelsToTile(beVar2.a >> i2, beVar2.b >> i2);
        for (int i3 = 0; i3 < arrayList.size(); i3++) {
            if (arrayList.get(i3).length() == c2) {
                Point QuadKeyToTile = VMapProjection.QuadKeyToTile(arrayList.get(i3));
                if (QuadKeyToTile.x >= PixelsToTile.a && QuadKeyToTile.x <= PixelsToTile2.a && QuadKeyToTile.y >= PixelsToTile.b && QuadKeyToTile.y <= PixelsToTile2.b) {
                    return true;
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public synchronized void loadBMtilesData2(ArrayList arrayList, boolean z) {
        boolean z2;
        ArrayList arrayList2 = new ArrayList();
        for (int i2 = 0; i2 < arrayList.size(); i2++) {
            String str = (String) arrayList.get(i2);
            if (this.f == null || !this.f.hasGridData(str)) {
                z2 = false;
            } else {
                z2 = true;
            }
            if (!z2 && !this.tileDownloadCtrl.b(str)) {
                arrayList2.add(str);
            }
        }
        if (arrayList2.size() <= 0) {
            this.tileDownloadCtrl.a = 0;
        } else if (z) {
            this.tileDownloadCtrl.a = 0;
            this.tileDownloadCtrl.b();
            sendMapDataRequest(arrayList2);
        } else {
            this.tileDownloadCtrl.a += arrayList2.size();
            this.tileDownloadCtrl.b();
        }
    }

    /* access modifiers changed from: protected */
    public void sendMapDataRequest(ArrayList arrayList) {
        if (arrayList.size() != 0) {
            ab abVar = new ab(this);
            abVar.e = this.mapLevel;
            this.i.a();
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < arrayList.size()) {
                    String str = (String) arrayList.get(i3);
                    this.tileDownloadCtrl.c(str);
                    abVar.b(str);
                    i2 = i3 + 1;
                } else {
                    this.i.a(abVar);
                    return;
                }
            }
        }
    }

    public void paintVectorMap(Canvas canvas) {
        if (!this.v) {
            setBackgroundColor(-16777216 | this.f.getBKColor(this.mapLevel));
            this.v = true;
        }
        loadBMtilesData2(e(), false);
        ArrayList<String> f2 = f();
        canvas.save();
        Matrix matrix = canvas.getMatrix();
        matrix.preRotate((float) (-this.mapAngle), (float) (this.width / 2), (float) (this.height / 2));
        canvas.setMatrix(matrix);
        ArrayList arrayList = new ArrayList();
        Hashtable hashtable = new Hashtable();
        for (int i2 = 0; i2 < f2.size(); i2++) {
            String str = f2.get(i2);
            if (this.f.hasBitMapData(str)) {
                arrayList.add(str);
            } else {
                if (!this.j.a(str)) {
                    ao aoVar = new ao();
                    aoVar.a = str;
                    this.j.a(aoVar);
                }
                String substring = str.length() != 0 ? str.substring(0, str.length() - 1) : PoiTypeDef.All;
                if (this.f.hasBitMapData(substring)) {
                    hashtable.put(substring, substring);
                }
            }
        }
        if (hashtable.size() > 0) {
            Enumeration elements = hashtable.elements();
            while (elements.hasMoreElements()) {
                String str2 = (String) elements.nextElement();
                if (this.f.hasBitMapData(str2)) {
                    be QuadKeyToTitle = VMapProjection.QuadKeyToTitle(str2);
                    int length = (QuadKeyToTitle.a * 256) << (20 - str2.length());
                    int length2 = (QuadKeyToTitle.b * 256) << (20 - str2.length());
                    Point point = new Point();
                    getScreenPntBy20Pixel(length, length2, this.mapLevel - 1, point);
                    canvas.save();
                    Matrix matrix2 = canvas.getMatrix();
                    matrix2.preScale(2.0f, 2.0f, (float) (this.width / 2), (float) (this.height / 2));
                    canvas.setMatrix(matrix2);
                    try {
                        this.f.fillBitmapBufferData(str2, this.g.array());
                        this.h.copyPixelsFromBuffer(this.g);
                        canvas.drawBitmap(this.h, (float) point.x, (float) point.y, (Paint) null);
                    } catch (Exception e2) {
                    }
                    canvas.restore();
                }
            }
        }
        for (int i3 = 0; i3 < arrayList.size(); i3++) {
            String str3 = (String) arrayList.get(i3);
            if (this.f.hasBitMapData(str3)) {
                be QuadKeyToTitle2 = VMapProjection.QuadKeyToTitle(str3);
                int length3 = (QuadKeyToTitle2.a * 256) << (20 - str3.length());
                int length4 = (QuadKeyToTitle2.b * 256) << (20 - str3.length());
                Point point2 = new Point();
                getScreenPntBy20Pixel(length3, length4, this.mapLevel, point2);
                try {
                    this.f.fillBitmapBufferData(str3, this.g.array());
                    this.h.copyPixelsFromBuffer(this.g);
                    canvas.drawBitmap(this.h, (float) point2.x, (float) point2.y, (Paint) null);
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
            }
        }
        canvas.restore();
        int i4 = this.mapAngle;
        while (i4 < 0) {
            i4 += 360;
        }
        while (i4 > 360) {
            i4 -= 360;
        }
        NativeMap nativeMap = new NativeMap();
        nativeMap.initMap(null, this.width, this.height);
        nativeMap.setMapParameter(this.centerX, this.centerY, this.mapLevel, i4);
        nativeMap.paintMap(this.f, 0);
        nativeMap.paintLables(this.f, canvas, 2);
    }

    /* access modifiers changed from: protected */
    public int getGridLevelOff(int i2) {
        switch (i2) {
            case 2:
            case 6:
                return 4;
            case 10:
                return 2;
            case 12:
                return 2;
            case 14:
                return 7;
            default:
                return 0;
        }
    }

    private int c(int i2) {
        switch (i2) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                return 2;
            case 6:
                return 6;
            case 7:
                return 6;
            case 8:
                return 6;
            case MotionEventCompat.ACTION_HOVER_ENTER:
                return 6;
            case 10:
                return 10;
            case Route.DrivingSaveMoney /*11*/:
                return 10;
            case 12:
                return 12;
            case Route.DrivingNoFastRoad /*13*/:
                return 12;
            case 14:
                return 14;
            case ViewDragHelper.EDGE_ALL:
                return 14;
            case 16:
                return 14;
            case LayoutParams.CENTER /*17*/:
                return 14;
            case 18:
                return 14;
            case TimeUtils.HUNDRED_DAY_FIELD_LEN:
                return 14;
            case 20:
                return 14;
            default:
                return 0;
        }
    }

    /* access modifiers changed from: protected */
    public PointF toScreenPoint(PointF pointF) {
        PointF pointF2 = new PointF();
        int width2 = getWidth();
        int height2 = getHeight();
        float f2 = pointF.x - ((float) (width2 >> 1));
        float f3 = pointF.y - ((float) (height2 >> 1));
        double atan2 = Math.atan2((double) f3, (double) f2);
        double sqrt = Math.sqrt(Math.pow((double) f3, 2.0d) + Math.pow((double) f2, 2.0d));
        double mapAngle2 = atan2 - ((((double) getMapAngle()) * 3.141592653589793d) / 180.0d);
        pointF2.x = (float) ((Math.cos(mapAngle2) * sqrt) + ((double) (width2 >> 1)));
        pointF2.y = (float) (((double) (height2 >> 1)) + (sqrt * Math.sin(mapAngle2)));
        return pointF2;
    }

    /* access modifiers changed from: protected */
    public PointF fromScreenPoint(PointF pointF) {
        PointF pointF2 = new PointF();
        int width2 = getWidth();
        int height2 = getHeight();
        float f2 = pointF.x - ((float) (width2 >> 1));
        float f3 = pointF.y - ((float) (height2 >> 1));
        double atan2 = Math.atan2((double) f3, (double) f2);
        double sqrt = Math.sqrt(Math.pow((double) f3, 2.0d) + Math.pow((double) f2, 2.0d));
        double mapAngle2 = atan2 + ((((double) getMapAngle()) * 3.141592653589793d) / 180.0d);
        pointF2.x = (float) ((Math.cos(mapAngle2) * sqrt) + ((double) (width2 >> 1)));
        pointF2.y = (float) (((double) (height2 >> 1)) + (sqrt * Math.sin(mapAngle2)));
        return pointF2;
    }

    /* access modifiers changed from: protected */
    public void getScreenPnt(int i2, int i3, Point point) {
        getScreenPntBy20Pixel(i2 << (20 - this.mapLevel), i3 << (20 - this.mapLevel), point);
    }

    /* access modifiers changed from: protected */
    public void getScreenPntBy20Pixel(int i2, int i3, int i4, Point point) {
        int i5 = 20 - i4;
        point.x = i2 >> i5;
        point.y = i3 >> i5;
        point.x = ((this.width >> 1) - (this.centerX >> i5)) + point.x;
        point.y = ((this.height >> 1) - (this.centerY >> i5)) + point.y;
    }

    /* access modifiers changed from: protected */
    public void getScreenPntBy20Pixel(int i2, int i3, Point point) {
        int i4 = 20 - this.mapLevel;
        point.x = i2 >> i4;
        point.y = i3 >> i4;
        new be();
        point.x = ((this.width >> 1) - (this.centerX >> i4)) + point.x;
        point.y = ((this.height >> 1) - (this.centerY >> i4)) + point.y;
    }

    /* access modifiers changed from: protected */
    public void getPixelPnt(Point point, be beVar) {
        getPixel20Pnt2(point, beVar, this.centerX, this.centerY);
        beVar.a >>= 20 - this.mapLevel;
        beVar.b >>= 20 - this.mapLevel;
    }

    /* access modifiers changed from: protected */
    public void getPixel20Pnt2(Point point, be beVar, int i2, int i3) {
        int i4 = 20 - this.mapLevel;
        int i5 = (point.x - (this.width >> 1)) << i4;
        int i6 = (point.y - (this.height >> 1)) << i4;
        double atan2 = Math.atan2((double) i6, (double) i5);
        double sqrt = Math.sqrt(Math.pow((double) i6, 2.0d) + Math.pow((double) i5, 2.0d));
        double d2 = atan2 - ((((double) this.mapAngle) * 3.141592653589793d) / 180.0d);
        beVar.a = ((int) ((Math.cos(d2) * sqrt) + 0.5d)) + i2;
        beVar.b = ((int) ((sqrt * Math.sin(d2)) + 0.5d)) + i3;
    }

    /* access modifiers changed from: protected */
    public void getPixel20Pnt(Point point, be beVar, int i2, int i3) {
        int i4 = 20 - this.mapLevel;
        beVar.a = ((point.x - (this.width >> 1)) << i4) + i2;
        beVar.b = ((point.y - (this.height >> 1)) << i4) + i3;
    }

    /* access modifiers changed from: package-private */
    public void a(be beVar, be beVar2) {
        double d2 = (((double) this.mapAngle) * 3.141592653589793d) / 180.0d;
        int abs = (int) ((((double) this.width) * Math.abs(Math.cos(d2))) + (((double) this.height) * Math.abs(Math.sin(d2))));
        int abs2 = (int) ((Math.abs(Math.cos(d2)) * ((double) this.height)) + (((double) this.width) * Math.abs(Math.sin(d2))));
        be beVar3 = new be();
        getPixelPnt(new Point((this.width - abs) / 2, (this.height - abs2) / 2), beVar3);
        int min = Math.min(Integer.MAX_VALUE, beVar3.a);
        int min2 = Math.min(Integer.MAX_VALUE, beVar3.b);
        int max = Math.max(Integer.MIN_VALUE, beVar3.a);
        int max2 = Math.max(Integer.MIN_VALUE, beVar3.b);
        getPixelPnt(new Point((this.width + abs) / 2, (this.height - abs2) / 2), beVar3);
        int min3 = Math.min(min, beVar3.a);
        int min4 = Math.min(min2, beVar3.b);
        int max3 = Math.max(max, beVar3.a);
        int max4 = Math.max(max2, beVar3.b);
        getPixelPnt(new Point((this.width + abs) / 2, (this.height + abs2) / 2), beVar3);
        int min5 = Math.min(min3, beVar3.a);
        int min6 = Math.min(min4, beVar3.b);
        int max5 = Math.max(max3, beVar3.a);
        int max6 = Math.max(max4, beVar3.b);
        getPixelPnt(new Point((this.width - abs) / 2, (abs2 + this.height) / 2), beVar3);
        int min7 = Math.min(min5, beVar3.a);
        int min8 = Math.min(min6, beVar3.b);
        int max7 = Math.max(max5, beVar3.a);
        int max8 = Math.max(max6, beVar3.b);
        beVar.a = min7;
        beVar.b = min8;
        beVar2.a = max7;
        beVar2.b = max8;
    }

    /* access modifiers changed from: package-private */
    public ArrayList<String> e() {
        be beVar = new be();
        be beVar2 = new be();
        ArrayList<String> arrayList = new ArrayList<>();
        int c2 = c(this.mapLevel);
        int i2 = this.mapLevel - c2;
        a(beVar, beVar2);
        be PixelsToTile = VMapProjection.PixelsToTile(beVar.a >> i2, beVar.b >> i2);
        be PixelsToTile2 = VMapProjection.PixelsToTile(beVar2.a >> i2, beVar2.b >> i2);
        int i3 = PixelsToTile2.a - PixelsToTile.a;
        int i4 = PixelsToTile2.b - PixelsToTile.b;
        arrayList.clear();
        for (int i5 = 0; i5 <= i4; i5++) {
            for (int i6 = 0; i6 <= i3; i6++) {
                arrayList.add(VMapProjection.TileToQuadKey(PixelsToTile.a + i6, PixelsToTile.b + i5, c2));
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    public boolean a(String str) {
        be beVar = new be();
        be beVar2 = new be();
        if (this.mapLevel != str.length()) {
            return false;
        }
        a(beVar, beVar2);
        be PixelsToTile = VMapProjection.PixelsToTile(beVar.a, beVar.b);
        be PixelsToTile2 = VMapProjection.PixelsToTile(beVar2.a, beVar2.b);
        Point QuadKeyToTile = VMapProjection.QuadKeyToTile(str);
        if (QuadKeyToTile.x < PixelsToTile.a || QuadKeyToTile.x > PixelsToTile2.a || QuadKeyToTile.y < PixelsToTile.b || QuadKeyToTile.y > PixelsToTile2.b) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public ArrayList<String> f() {
        int i2 = 0;
        ArrayList arrayList = new ArrayList();
        ArrayList<String> arrayList2 = new ArrayList<>();
        be beVar = new be();
        be beVar2 = new be();
        be beVar3 = new be();
        a(beVar, beVar2);
        getPixelPnt(new Point(this.width / 2, this.height / 2), beVar3);
        be PixelsToTile = VMapProjection.PixelsToTile(beVar.a, beVar.b);
        be PixelsToTile2 = VMapProjection.PixelsToTile(beVar2.a, beVar2.b);
        be PixelsToTile3 = VMapProjection.PixelsToTile(beVar3.a, beVar3.b);
        int i3 = PixelsToTile2.a - PixelsToTile.a;
        int i4 = PixelsToTile2.b - PixelsToTile.b;
        a aVar = new a(PixelsToTile3.a, PixelsToTile3.b);
        for (int i5 = 0; i5 <= i4; i5++) {
            for (int i6 = 0; i6 <= i3; i6++) {
                arrayList.add(new be(PixelsToTile.a + i6, PixelsToTile.b + i5));
            }
        }
        Collections.sort(arrayList, aVar);
        while (true) {
            int i7 = i2;
            if (i7 >= arrayList.size()) {
                return arrayList2;
            }
            be beVar4 = (be) arrayList.get(i7);
            arrayList2.add(VMapProjection.TileToQuadKey(beVar4.a, beVar4.b, this.mapLevel));
            i2 = i7 + 1;
        }
    }

    /* access modifiers changed from: protected */
    public void setMapCenterScreen(int i2, int i3) {
        be beVar = new be();
        getPixel20Pnt2(new Point(i2, i3), beVar, this.centerX, this.centerY);
        setMapCenter(beVar.a, beVar.b);
    }

    /* access modifiers changed from: protected */
    public void setMapCenter(int i2, int i3) {
        if (i2 >= 0 && i2 <= 268435455 && i3 >= 20 && i3 <= 268435431) {
            this.centerX = i2;
            this.centerY = i3;
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0067 A[SYNTHETIC, Splitter:B:12:0x0067] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0049 A[Catch:{ IOException -> 0x006e }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.net.HttpURLConnection getConnection(java.lang.String r7) {
        /*
            r6 = this;
            r1 = 0
            android.content.Context r0 = r6.y
            java.lang.String r2 = "connectivity"
            java.lang.Object r0 = r0.getSystemService(r2)
            android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0
            android.net.NetworkInfo r0 = r0.getActiveNetworkInfo()
            if (r0 == 0) goto L_0x0071
            int r0 = r0.getType()
            r2 = 1
            if (r0 != r2) goto L_0x0050
            android.content.Context r0 = r6.y
            android.net.Proxy.getHost(r0)
            android.content.Context r0 = r6.y
            android.net.Proxy.getPort(r0)
            r0 = r1
        L_0x0023:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x006e }
            r2.<init>()     // Catch:{ IOException -> 0x006e }
            com.amap.mapapi.core.i r3 = com.amap.mapapi.core.i.a()     // Catch:{ IOException -> 0x006e }
            java.lang.String r3 = r3.f()     // Catch:{ IOException -> 0x006e }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x006e }
            java.lang.String r3 = "/bmserver/VMMV2?"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x006e }
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ IOException -> 0x006e }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x006e }
            java.net.URL r3 = new java.net.URL     // Catch:{ IOException -> 0x006e }
            r3.<init>(r2)     // Catch:{ IOException -> 0x006e }
            if (r0 == 0) goto L_0x0067
            java.net.URLConnection r0 = r3.openConnection(r0)     // Catch:{ IOException -> 0x006e }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x006e }
        L_0x004f:
            return r0
        L_0x0050:
            java.lang.String r2 = android.net.Proxy.getDefaultHost()
            int r3 = android.net.Proxy.getDefaultPort()
            if (r2 == 0) goto L_0x0071
            java.net.Proxy r0 = new java.net.Proxy
            java.net.Proxy$Type r4 = java.net.Proxy.Type.HTTP
            java.net.InetSocketAddress r5 = new java.net.InetSocketAddress
            r5.<init>(r2, r3)
            r0.<init>(r4, r5)
            goto L_0x0023
        L_0x0067:
            java.net.URLConnection r0 = r3.openConnection()     // Catch:{ IOException -> 0x006e }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x006e }
            goto L_0x004f
        L_0x006e:
            r0 = move-exception
            r0 = r1
            goto L_0x004f
        L_0x0071:
            r0 = r1
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.mapapi.map.MapView.getConnection(java.lang.String):java.net.HttpURLConnection");
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i2) {
        if (!this.VMapMode) {
            super.onWindowVisibilityChanged(i2);
            return;
        }
        if (i2 == 8) {
            if (this.isInited) {
                VdestoryMap();
            }
        } else if (i2 == 0) {
            this.bfirstDrawed = false;
            int width2 = getWidth();
            int height2 = getHeight();
            if (width2 > 0 && height2 > 0) {
                this.width = getWidth();
                this.height = getHeight();
                Vinit();
                this.isInited = true;
            }
        }
        super.onWindowVisibilityChanged(i2);
    }

    class a implements Comparator {
        int a;
        int b;

        public a(int i, int i2) {
            this.a = i;
            this.b = i2;
        }

        public int compare(Object obj, Object obj2) {
            be beVar = (be) obj;
            be beVar2 = (be) obj2;
            int i = beVar.a - this.a;
            int i2 = beVar.b - this.b;
            int i3 = beVar2.a - this.a;
            int i4 = beVar2.b - this.b;
            int i5 = (i * i) + (i2 * i2);
            int i6 = (i3 * i3) + (i4 * i4);
            if (i5 > i6) {
                return 1;
            }
            if (i5 < i6) {
                return -1;
            }
            return 0;
        }
    }
}
