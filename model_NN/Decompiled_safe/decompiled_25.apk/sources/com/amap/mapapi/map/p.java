package com.amap.mapapi.map;

import android.graphics.Canvas;

/* compiled from: FixPosOverlay */
abstract class p extends Overlay {
    public abstract void b();

    public abstract boolean draw(Canvas canvas, MapView mapView, boolean z, long j);

    p() {
    }
}
