package com.amap.mapapi.map;

import android.content.Context;
import com.amap.mapapi.core.AMapException;
import java.net.Proxy;
import java.util.ArrayList;

/* compiled from: AsyncServer */
abstract class c<T, V> extends ae {
    protected volatile boolean a = true;
    protected ArrayList<Thread> b = null;
    protected as<T> c;
    protected at d;
    private Runnable g = new d(this);
    private Runnable h = new e(this);

    /* access modifiers changed from: protected */
    public abstract ArrayList<T> a(ArrayList arrayList) throws AMapException;

    /* access modifiers changed from: protected */
    public abstract ArrayList<T> a(ArrayList arrayList, Proxy proxy) throws AMapException;

    /* access modifiers changed from: protected */
    public abstract int f();

    /* access modifiers changed from: protected */
    public abstract int g();

    public c(ah ahVar, Context context) {
        super(ahVar, context);
        if (this.b == null) {
            this.b = new ArrayList<>();
        }
        this.d = new at(f(), this.h, this.g);
        this.d.a();
    }

    public void a() {
        this.c.a();
        e();
        this.c.c();
        this.c = null;
        this.e = null;
        this.f = null;
    }

    public void b() {
        super.b();
        e();
    }

    public void c() {
        super.c();
        e();
    }

    public void d() {
        this.a = true;
        if (this.b == null) {
            this.b = new ArrayList<>();
        }
        if (this.d == null) {
            this.d = new at(f(), this.h, this.g);
            this.d.a();
        }
    }

    public void e() {
        this.a = false;
        if (this.b != null) {
            int size = this.b.size();
            for (int i = 0; i < size; i++) {
                Thread thread = this.b.get(0);
                if (thread != null) {
                    thread.interrupt();
                    this.b.remove(0);
                }
            }
            this.b = null;
        }
        if (this.d != null) {
            this.d.b();
            this.d.c();
            this.d = null;
        }
    }
}
