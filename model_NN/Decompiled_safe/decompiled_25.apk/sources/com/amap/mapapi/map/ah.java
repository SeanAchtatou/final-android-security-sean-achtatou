package com.amap.mapapi.map;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ListView;
import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.core.b;
import com.amap.mapapi.core.g;
import com.amap.mapapi.core.i;
import com.amap.mapapi.core.m;
import com.amap.mapapi.core.q;
import com.amap.mapapi.core.s;
import com.amap.mapapi.map.MapView;
import com.amap.mapapi.map.au;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: Mediator */
class ah {
    public e a;
    public d b;
    public b c;
    public a d;
    public c e;
    public ac f = null;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amap.mapapi.map.ah.d.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.amap.mapapi.map.ah.d.a(android.view.View, com.amap.mapapi.map.MapView$LayoutParams):void
      com.amap.mapapi.map.ah.d.a(int, int):void
      com.amap.mapapi.map.ah.d.a(boolean, boolean):void */
    public ah(MapActivity mapActivity, MapView mapView, String str) {
        com.amap.mapapi.core.b.h = GeoPoint.EnumMapProjection.projection_900913;
        this.b = new d(mapView);
        this.f = new ac(this.b);
        if (mapView.VMapMode) {
            this.b.b(com.amap.mapapi.core.b.c);
            this.b.c(com.amap.mapapi.core.b.d);
            this.b.a(this.b.e());
        }
        this.f.a();
        if (com.amap.mapapi.core.b.g == null) {
            com.amap.mapapi.core.b.g = new m(mapActivity.getApplicationContext());
            com.amap.mapapi.core.b.g.b();
        }
        this.e = new c(this, mapActivity, str);
        this.d = new a(mapActivity);
        this.a = new e();
        this.c = new b();
        a(this);
        this.b.a(false, false);
    }

    public void a() {
        this.d.a();
        this.d.b();
        if (com.amap.mapapi.core.b.f == 1) {
            q.a();
            com.amap.mapapi.core.b.g.a();
            com.amap.mapapi.core.b.g = null;
        }
        this.a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amap.mapapi.map.ah.a.a(com.amap.mapapi.map.p, boolean):void
     arg types: [com.amap.mapapi.map.bi, int]
     candidates:
      com.amap.mapapi.map.ah.a.a(int, android.view.KeyEvent):boolean
      com.amap.mapapi.map.ah.a.a(com.amap.mapapi.map.w, android.content.Context):boolean
      com.amap.mapapi.map.ah.a.a(java.lang.String, boolean):boolean
      com.amap.mapapi.map.ah.a.a(com.amap.mapapi.map.p, boolean):void */
    private void a(ah ahVar) {
        if (Math.random() > 0.1d) {
            this.d.a((p) new bi(ahVar, com.amap.mapapi.core.b.g.a(b.a.ewatermark.ordinal())), true);
        }
    }

    /* compiled from: Mediator */
    public class c {
        private String b = PoiTypeDef.All;
        private String c = "android";
        private final MapActivity d;
        private Proxy e;
        /* access modifiers changed from: private */
        public Map<Integer, ae> f = new HashMap();

        public c(ah ahVar, MapActivity mapActivity, String str) {
            this.d = mapActivity;
            if (str != null) {
                this.c = str;
            }
            if (com.amap.mapapi.core.b.e == 2) {
                this.c = "androidh";
            } else if (com.amap.mapapi.core.b.e == 1) {
                this.c = "androidl";
            } else {
                this.c = "android";
            }
            this.b = com.amap.mapapi.core.d.a(this.d);
            if (ahVar.b.c.VMapMode) {
                this.f.put(1, new bf(ahVar, mapActivity));
                this.f.put(2, new m(ahVar, mapActivity));
                return;
            }
            this.f.put(0, new ax(ahVar, mapActivity));
            this.f.put(2, new m(ahVar, mapActivity));
        }

        public String a() {
            return this.b;
        }

        public String b() {
            return this.c;
        }

        public Proxy c() {
            return this.e;
        }

        public ae a(int i) {
            return this.f.get(Integer.valueOf(i));
        }

        public void a(ae aeVar, int i) {
            this.f.put(Integer.valueOf(i), aeVar);
        }

        /* access modifiers changed from: private */
        public void d() {
            this.e = com.amap.mapapi.core.d.b(this.d);
        }

        /* access modifiers changed from: private */
        public void e() {
            this.e = null;
        }
    }

    /* compiled from: Mediator */
    public class b {
        int a = 0;

        public b() {
            f();
        }

        public void a() {
            for (Map.Entry value : ah.this.e.f.entrySet()) {
                ae aeVar = (ae) value.getValue();
                if (aeVar != null) {
                    aeVar.c();
                }
            }
            ah.this.e.e();
        }

        public void b() {
            if (ah.this.d.q) {
                ah.this.d.d();
            }
            this.a++;
            if (this.a >= 20 && this.a % 20 == 0) {
                for (Map.Entry value : ah.this.e.f.entrySet()) {
                    ae aeVar = (ae) value.getValue();
                    if (aeVar != null) {
                        aeVar.h();
                    }
                }
            }
        }

        public void c() {
            ah.this.b.a = false;
            for (Map.Entry value : ah.this.e.f.entrySet()) {
                ae aeVar = (ae) value.getValue();
                if (aeVar != null) {
                    aeVar.a();
                }
            }
            if (ah.this.d != null && ah.this.d.d != null) {
                ah.this.d.d.a();
            }
        }

        public void d() {
            for (Map.Entry value : ah.this.e.f.entrySet()) {
                ae aeVar = (ae) value.getValue();
                if (aeVar != null) {
                    aeVar.d();
                }
            }
        }

        public void e() {
            for (Map.Entry value : ah.this.e.f.entrySet()) {
                ae aeVar = (ae) value.getValue();
                if (aeVar != null) {
                    aeVar.b();
                }
            }
        }

        public void f() {
            for (Map.Entry value : ah.this.e.f.entrySet()) {
                ae aeVar = (ae) value.getValue();
                if (aeVar != null) {
                    aeVar.a_();
                }
            }
            ah.this.e.d();
        }
    }

    /* compiled from: Mediator */
    public class e implements Projection {
        private int b = 0;
        private HashMap<Float, Float> c = new HashMap<>();

        public e() {
        }

        public Point toPixels(GeoPoint geoPoint, Point point) {
            int i;
            int i2;
            int e = ah.this.b.e();
            PointF b2 = ah.this.f.b(geoPoint, ah.this.f.j, ah.this.f.k, ah.this.f.h[e]);
            ai a2 = ah.this.b.c.b().a();
            Point point2 = ah.this.b.c.a().f.k;
            if (!a2.m) {
                float f = ((float) point2.x) + (bj.h * ((float) (((int) b2.x) - point2.x)));
                float f2 = (((float) (((int) b2.y) - point2.y)) * bj.h) + ((float) point2.y);
                int i3 = (int) f;
                i = (int) f2;
                if (((double) f) >= ((double) i3) + 0.5d) {
                    i3++;
                }
                if (((double) f2) >= ((double) i) + 0.5d) {
                    i++;
                }
            } else if (a2.l) {
                float f3 = (ai.j * (((float) ((int) b2.x)) - a2.f.x)) + a2.f.x + (a2.g.x - a2.f.x);
                float f4 = ((((float) ((int) b2.y)) - a2.f.y) * ai.j) + a2.f.y + (a2.g.y - a2.f.y);
                i2 = (int) f3;
                i = (int) f4;
                if (((double) f3) >= ((double) i2) + 0.5d) {
                    i2++;
                }
                if (((double) f4) >= ((double) i) + 0.5d) {
                    i++;
                }
            } else {
                i2 = (int) b2.x;
                i = (int) b2.y;
            }
            Point point3 = new Point(i2, i);
            if (point != null) {
                point.x = point3.x;
                point.y = point3.y;
            }
            return point3;
        }

        public GeoPoint fromPixels(int i, int i2) {
            int e = ah.this.b.e();
            return ah.this.f.a(new PointF((float) i, (float) i2), ah.this.f.j, ah.this.f.k, ah.this.f.h[e], ah.this.f.l);
        }

        public float metersToEquatorPixels(float f) {
            int e = ah.this.b.e();
            if (this.c.size() > 30 || e != this.b) {
                this.b = e;
                this.c.clear();
            }
            if (!this.c.containsKey(Float.valueOf(f))) {
                float a2 = ah.this.f.a(fromPixels(0, 0), fromPixels(0, 10));
                if (a2 <= 0.0f) {
                    return 0.0f;
                }
                this.c.put(Float.valueOf(f), Float.valueOf(10.0f * (f / a2)));
            }
            return this.c.get(Float.valueOf(f)).floatValue();
        }

        public int a() {
            return a(false);
        }

        public int b() {
            return a(true);
        }

        private int a(boolean z) {
            double mapAngle = (((double) ah.this.b.g().getMapAngle()) * 3.141592653589793d) / 180.0d;
            GeoPoint fromPixels = fromPixels(0, (int) ((Math.abs(Math.cos(mapAngle)) * ((double) ah.this.b.d())) + (((double) ah.this.b.c()) * Math.abs(Math.sin(mapAngle)))));
            GeoPoint fromPixels2 = fromPixels((int) ((((double) ah.this.b.c()) * Math.abs(Math.cos(mapAngle))) + (((double) ah.this.b.d()) * Math.abs(Math.sin(mapAngle)))), 0);
            return z ? Math.abs(fromPixels.getLongitudeE6() - fromPixels2.getLongitudeE6()) : Math.abs(fromPixels.getLatitudeE6() - fromPixels2.getLatitudeE6());
        }
    }

    /* compiled from: Mediator */
    public class a {
        public s<w> a = null;
        public s<Overlay> b = new s<>();
        long c;
        f d;
        public boolean e = false;
        public boolean f = false;
        String g = "GridMap";
        String h = "SatelliteMap";
        String i = "GridTmc";
        String j = "SateliteTmc";
        private boolean l = false;
        private g<p> m = new g<>();
        private MapView.ReticleDrawMode n = MapView.ReticleDrawMode.DRAW_RETICLE_NEVER;
        private int o = 0;
        private int p = 0;
        /* access modifiers changed from: private */
        public boolean q = false;

        public a(MapActivity mapActivity) {
            if (mapActivity != null) {
                f();
                DisplayMetrics displayMetrics = new DisplayMetrics();
                mapActivity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int i2 = (displayMetrics.widthPixels / 256) + 2;
                int i3 = (displayMetrics.heightPixels / 256) + 2;
                this.o = i3 + i2 + (i2 * i3);
                this.p = (this.o / 8) + 1;
                if (this.p == 0) {
                    this.p = 1;
                } else if (this.p > 5) {
                    this.p = 5;
                }
                a(mapActivity);
            }
        }

        private void a(Context context) {
            if (this.a == null) {
                this.a = new s<>();
            }
            w wVar = new w();
            wVar.j = new bd() {
                public String a(int i, int i2, int i3) {
                    return i.a().b() + "/appmaptile?z=" + i3 + "&x=" + i + "&y=" + i2 + "&lang=zh_cn&size=1&scale=1&style=7";
                }
            };
            wVar.a = this.g;
            wVar.e = true;
            wVar.d = true;
            wVar.f = true;
            wVar.g = true;
            wVar.b = 18;
            wVar.c = 4;
            a(wVar, context);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.amap.mapapi.map.ah.d.a(boolean, boolean):void
         arg types: [int, int]
         candidates:
          com.amap.mapapi.map.ah.d.a(android.view.View, com.amap.mapapi.map.MapView$LayoutParams):void
          com.amap.mapapi.map.ah.d.a(int, int):void
          com.amap.mapapi.map.ah.d.a(boolean, boolean):void */
        /* access modifiers changed from: package-private */
        public boolean a(String str, boolean z) {
            if (str.equals(PoiTypeDef.All)) {
                return false;
            }
            int size = this.a.size();
            for (int i2 = 0; i2 < size; i2++) {
                w wVar = this.a.get(i2);
                if (wVar != null && wVar.a.equals(str)) {
                    wVar.f = z;
                    if (!wVar.e) {
                        return true;
                    }
                    if (z) {
                        if (wVar.b > wVar.c) {
                            ah.this.b.b(wVar.b);
                            ah.this.b.c(wVar.c);
                        }
                        b(str);
                        ah.this.b.a(false, false);
                        return true;
                    }
                }
            }
            return false;
        }

        private void b(String str) {
            if (!str.equals(PoiTypeDef.All)) {
                int size = this.a.size();
                for (int i2 = 0; i2 < size; i2++) {
                    w wVar = this.a.get(i2);
                    if (wVar != null && !wVar.a.equals(str) && wVar.e && wVar.f) {
                        wVar.f = false;
                    }
                }
            }
        }

        private boolean c(String str) {
            if (this.a == null) {
                return false;
            }
            int size = this.a.size();
            for (int i2 = 0; i2 < size; i2++) {
                w wVar = this.a.get(i2);
                if (wVar != null && wVar.a.equals(str)) {
                    return true;
                }
            }
            return false;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.amap.mapapi.map.ah.a.a(java.lang.String, boolean):boolean
         arg types: [java.lang.String, int]
         candidates:
          com.amap.mapapi.map.ah.a.a(com.amap.mapapi.map.p, boolean):void
          com.amap.mapapi.map.ah.a.a(int, android.view.KeyEvent):boolean
          com.amap.mapapi.map.ah.a.a(com.amap.mapapi.map.w, android.content.Context):boolean
          com.amap.mapapi.map.ah.a.a(java.lang.String, boolean):boolean */
        /* access modifiers changed from: package-private */
        public boolean a(w wVar, Context context) {
            boolean z;
            if (wVar == null || wVar.a.equals(PoiTypeDef.All) || c(wVar.a)) {
                return false;
            }
            wVar.o = new s<>();
            wVar.m = new h(this.o, this.p, wVar.h, wVar.i);
            wVar.n = new j(context, ah.this.b.c.e, wVar);
            wVar.n.a(wVar.m);
            int size = this.a.size();
            if (!wVar.e || size == 0) {
                z = this.a.add(wVar);
            } else {
                int i2 = size - 1;
                while (true) {
                    if (i2 < 0) {
                        z = false;
                        break;
                    }
                    w wVar2 = this.a.get(i2);
                    if (wVar2 != null && wVar2.e) {
                        this.a.add(i2, wVar);
                        z = false;
                        break;
                    }
                    i2--;
                }
            }
            e();
            if (wVar.f) {
                a(wVar.a, true);
            }
            return z;
        }

        private void e() {
            int size = this.a.size();
            for (int i2 = 0; i2 < size; i2++) {
                w wVar = this.a.get(i2);
                if (wVar != null) {
                    wVar.k = i2;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public w a(String str) {
            if (str.equals(PoiTypeDef.All) || this.a == null || this.a.size() == 0) {
                return null;
            }
            int size = this.a.size();
            for (int i2 = 0; i2 < size; i2++) {
                w wVar = this.a.get(i2);
                if (wVar != null && wVar.a.equals(str)) {
                    return wVar;
                }
            }
            return null;
        }

        private void f() {
            String str = Build.MODEL;
            if (str == null) {
                return;
            }
            if (str.indexOf("OMAP_SS") > -1 || str.indexOf("omap_ss") > -1 || str.indexOf("MT810") > -1 || str.indexOf("MT720") > -1 || str.indexOf("GT-I9008") > -1) {
                com.amap.mapapi.core.b.o = true;
            }
        }

        public void a(MapView.ReticleDrawMode reticleDrawMode) {
            this.n = reticleDrawMode;
        }

        public void a() {
            MyLocationOverlay myLocationOverlay;
            int size = this.b.size();
            for (int i2 = 0; i2 < size; i2++) {
                Overlay remove = this.b.remove(0);
                if ((remove instanceof MyLocationOverlay) && (myLocationOverlay = (MyLocationOverlay) remove) != null) {
                    myLocationOverlay.a();
                }
            }
            int size2 = this.m.size();
            for (int i3 = 0; i3 < size2; i3++) {
                p remove2 = this.m.remove(0);
                if (remove2 != null) {
                    remove2.b();
                }
            }
        }

        public void b() {
            int size = ah.this.d.a.size();
            for (int i2 = 0; i2 < size; i2++) {
                w wVar = ah.this.d.a.get(0);
                if (wVar != null) {
                    wVar.a();
                    ah.this.d.a.remove(0);
                }
            }
            ah.this.d.a = null;
        }

        public List<Overlay> c() {
            return this.b;
        }

        public boolean a(GeoPoint geoPoint) {
            s<au.a> sVar;
            boolean z;
            au.a b2 = ah.this.f.b();
            int size = this.a.size();
            int i2 = 0;
            while (true) {
                if (i2 >= size) {
                    sVar = null;
                    break;
                } else if (this.a.get(i2).e) {
                    sVar = this.a.get(i2).o;
                    break;
                } else {
                    i2++;
                }
            }
            if (sVar == null) {
                return false;
            }
            Iterator<au.a> it = sVar.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                au.a next = it.next();
                if (next.b == b2.b && next.c == b2.c && next.d == b2.d) {
                    if (next.g >= 0) {
                        z = true;
                    }
                }
            }
            z = false;
            return z;
        }

        public void a(int i2, int i3, int i4, int i5) {
            if (a(this.c)) {
                b(i2, i3, i4, i5);
            } else {
                this.q = true;
            }
        }

        public void d() {
            if (ah.this.b != null && ah.this.b.c != null) {
                ah.this.b.c.postInvalidate();
            }
        }

        public void b(int i2, int i3, int i4, int i5) {
            ah.this.b.c.postInvalidate(i2, i3, i4, i5);
            this.c = System.currentTimeMillis();
            this.q = false;
        }

        private boolean a(long j2) {
            return System.currentTimeMillis() - j2 > 300;
        }

        public void a(boolean z) {
            this.l = z;
        }

        public void a(p pVar, boolean z) {
            if (z) {
                this.m.a(pVar);
            } else {
                this.m.b(pVar);
            }
        }

        public void a(Canvas canvas, Matrix matrix, float f2, float f3) {
            if (this.l) {
                canvas.save();
                canvas.translate(f2, f3);
                canvas.concat(matrix);
                a(canvas);
                canvas.restore();
                if (!this.e && !this.f) {
                    a(false);
                    ah.this.b.c.b().b(new Matrix());
                    ah.this.b.c.b().b(1.0f);
                    ah.this.b.c.b().c();
                }
            } else {
                a(canvas);
            }
            ah.this.b.h();
            b(canvas);
        }

        private void a(Canvas canvas) {
            if (!ah.this.b.c.VMapMode) {
                int size = this.a.size();
                for (int i2 = 0; i2 < size; i2++) {
                    w wVar = this.a.get(i2);
                    if (wVar != null && wVar.f) {
                        wVar.a(canvas);
                    }
                }
            } else if (ah.this.b.c.isInited) {
                ah.this.b.c.paintVectorMap(canvas);
                canvas.save();
                Matrix matrix = canvas.getMatrix();
                matrix.preRotate((float) (-ah.this.b.c.getMapAngle()), (float) (ah.this.b.c() / 2), (float) (ah.this.b.d() / 2));
                canvas.setMatrix(matrix);
                int size2 = this.a.size();
                for (int i3 = 0; i3 < size2; i3++) {
                    w wVar2 = this.a.get(i3);
                    if (wVar2.a.equals("GridTmc") && wVar2.f) {
                        wVar2.a(canvas);
                    }
                }
                canvas.restore();
            }
        }

        private void b(Canvas canvas) {
            MyLocationOverlay myLocationOverlay;
            long a2 = com.amap.mapapi.core.d.a();
            Iterator<Overlay> it = this.b.iterator();
            RouteOverlay routeOverlay = null;
            MyLocationOverlay myLocationOverlay2 = null;
            while (it.hasNext()) {
                Overlay next = it.next();
                if (next != null) {
                    if (next instanceof RouteOverlay) {
                        routeOverlay = (RouteOverlay) next;
                    } else {
                        if (next instanceof MyLocationOverlay) {
                            myLocationOverlay = (MyLocationOverlay) next;
                        } else {
                            next.draw(canvas, ah.this.b.c, false, a2);
                            myLocationOverlay = myLocationOverlay2;
                        }
                        myLocationOverlay2 = myLocationOverlay;
                    }
                }
            }
            if (this.n == MapView.ReticleDrawMode.DRAW_RETICLE_OVER) {
                c(canvas);
            }
            if (myLocationOverlay2 != null) {
                myLocationOverlay2.draw(canvas, ah.this.b.c, false, a2);
            }
            if (routeOverlay != null) {
                routeOverlay.draw(canvas, ah.this.b.c, false, a2);
            }
            Iterator<p> it2 = this.m.iterator();
            while (it2.hasNext()) {
                it2.next().draw(canvas, ah.this.b.c, false, a2);
            }
        }

        public PointF a(au.a aVar) {
            if (aVar == null) {
                return null;
            }
            return aVar.f;
        }

        public void a(Canvas canvas, MapView mapView, au.a aVar) {
            PointF a2 = a(aVar);
            Rect rect = new Rect((int) a2.x, (int) a2.y, (int) (a2.x + 256.0f), (int) (a2.y + 256.0f));
            Paint paint = new Paint();
            paint.setStyle(Paint.Style.STROKE);
            paint.setColor(-7829368);
            canvas.drawRect(rect, paint);
        }

        private void c(Canvas canvas) {
            int size = this.a.size();
            for (int i2 = 0; i2 < size; i2++) {
                w wVar = this.a.get(i2);
                if (wVar != null && wVar.f && wVar.e) {
                    int size2 = wVar.o.size();
                    for (int i3 = 0; i3 < size2; i3++) {
                        a(canvas, ah.this.b.c, wVar.o.get(i3));
                    }
                }
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:3:0x000d  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean a(int r4, android.view.KeyEvent r5) {
            /*
                r3 = this;
                r0 = 0
                com.amap.mapapi.core.s<com.amap.mapapi.map.Overlay> r1 = r3.b
                java.util.Iterator r1 = r1.iterator()
            L_0x0007:
                boolean r2 = r1.hasNext()
                if (r2 == 0) goto L_0x0021
                java.lang.Object r0 = r1.next()
                com.amap.mapapi.map.Overlay r0 = (com.amap.mapapi.map.Overlay) r0
                com.amap.mapapi.map.ah r2 = com.amap.mapapi.map.ah.this
                com.amap.mapapi.map.ah$d r2 = r2.b
                com.amap.mapapi.map.MapView r2 = r2.c
                boolean r0 = r0.onKeyDown(r4, r5, r2)
                if (r0 == 0) goto L_0x0007
            L_0x0021:
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.amap.mapapi.map.ah.a.a(int, android.view.KeyEvent):boolean");
        }

        /* JADX WARNING: Removed duplicated region for block: B:3:0x000d  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean b(int r4, android.view.KeyEvent r5) {
            /*
                r3 = this;
                r0 = 0
                com.amap.mapapi.core.s<com.amap.mapapi.map.Overlay> r1 = r3.b
                java.util.Iterator r1 = r1.iterator()
            L_0x0007:
                boolean r2 = r1.hasNext()
                if (r2 == 0) goto L_0x0021
                java.lang.Object r0 = r1.next()
                com.amap.mapapi.map.Overlay r0 = (com.amap.mapapi.map.Overlay) r0
                com.amap.mapapi.map.ah r2 = com.amap.mapapi.map.ah.this
                com.amap.mapapi.map.ah$d r2 = r2.b
                com.amap.mapapi.map.MapView r2 = r2.c
                boolean r0 = r0.onKeyUp(r4, r5, r2)
                if (r0 == 0) goto L_0x0007
            L_0x0021:
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.amap.mapapi.map.ah.a.b(int, android.view.KeyEvent):boolean");
        }

        /* JADX WARNING: Removed duplicated region for block: B:3:0x000d  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean a(android.view.MotionEvent r4) {
            /*
                r3 = this;
                r0 = 0
                com.amap.mapapi.core.s<com.amap.mapapi.map.Overlay> r1 = r3.b
                java.util.Iterator r1 = r1.iterator()
            L_0x0007:
                boolean r2 = r1.hasNext()
                if (r2 == 0) goto L_0x0021
                java.lang.Object r0 = r1.next()
                com.amap.mapapi.map.Overlay r0 = (com.amap.mapapi.map.Overlay) r0
                com.amap.mapapi.map.ah r2 = com.amap.mapapi.map.ah.this
                com.amap.mapapi.map.ah$d r2 = r2.b
                com.amap.mapapi.map.MapView r2 = r2.c
                boolean r0 = r0.onTrackballEvent(r4, r2)
                if (r0 == 0) goto L_0x0007
            L_0x0021:
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.amap.mapapi.map.ah.a.a(android.view.MotionEvent):boolean");
        }

        public boolean b(MotionEvent motionEvent) {
            boolean z = false;
            Iterator<Overlay> it = this.b.iterator();
            while (true) {
                boolean z2 = z;
                if (!it.hasNext()) {
                    return z2;
                }
                Overlay next = it.next();
                if (next != null) {
                    z = next.onTouchEvent(motionEvent, ah.this.b.c);
                } else {
                    z = z2;
                }
                if (z) {
                    return z;
                }
            }
        }

        /* access modifiers changed from: protected */
        public boolean c(MotionEvent motionEvent) {
            GeoPoint fromPixels = ah.this.a.fromPixels((int) motionEvent.getX(), (int) motionEvent.getY());
            for (int size = this.b.size() - 1; size >= 0; size--) {
                Overlay overlay = this.b.get(size);
                if (overlay != null) {
                    if (!(overlay instanceof ItemizedOverlay)) {
                        if (overlay.onTap(fromPixels, ah.this.b.c)) {
                            break;
                        }
                    } else if (((ItemizedOverlay) overlay).onTap(fromPixels, ah.this.b.c)) {
                        break;
                    }
                }
            }
            return false;
        }
    }

    /* compiled from: Mediator */
    public class d {
        public boolean a = true;
        /* access modifiers changed from: private */
        public MapView c;
        private ArrayList<bh> d;

        public d(MapView mapView) {
            this.c = mapView;
            this.d = new ArrayList<>();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.amap.mapapi.map.ah.d.a(boolean, boolean):void
         arg types: [int, int]
         candidates:
          com.amap.mapapi.map.ah.d.a(android.view.View, com.amap.mapapi.map.MapView$LayoutParams):void
          com.amap.mapapi.map.ah.d.a(int, int):void
          com.amap.mapapi.map.ah.d.a(boolean, boolean):void */
        public void a(int i) {
            if (!this.c.VMapMode) {
                if (i != ah.this.f.g) {
                    ah.this.f.g = i;
                }
            } else if (i != this.c.mapLevel) {
                this.c.mapLevel = i;
            }
            a(false, false);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.amap.mapapi.map.ah.d.a(boolean, boolean):void
         arg types: [int, int]
         candidates:
          com.amap.mapapi.map.ah.d.a(android.view.View, com.amap.mapapi.map.MapView$LayoutParams):void
          com.amap.mapapi.map.ah.d.a(int, int):void
          com.amap.mapapi.map.ah.d.a(boolean, boolean):void */
        public void a(int i, int i2) {
            if (i != com.amap.mapapi.core.b.i || i2 != com.amap.mapapi.core.b.j) {
                com.amap.mapapi.core.b.i = i;
                com.amap.mapapi.core.b.j = i2;
                a(true, false);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.amap.mapapi.map.ah.d.a(boolean, boolean):void
         arg types: [int, int]
         candidates:
          com.amap.mapapi.map.ah.d.a(android.view.View, com.amap.mapapi.map.MapView$LayoutParams):void
          com.amap.mapapi.map.ah.d.a(int, int):void
          com.amap.mapapi.map.ah.d.a(boolean, boolean):void */
        public void a(GeoPoint geoPoint) {
            if (geoPoint != null) {
                if (com.amap.mapapi.core.b.n) {
                    ah.this.f.j = ah.this.f.a(geoPoint);
                }
                a(false, false);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.amap.mapapi.map.ah.d.a(boolean, boolean):void
         arg types: [int, int]
         candidates:
          com.amap.mapapi.map.ah.d.a(android.view.View, com.amap.mapapi.map.MapView$LayoutParams):void
          com.amap.mapapi.map.ah.d.a(int, int):void
          com.amap.mapapi.map.ah.d.a(boolean, boolean):void */
        public void b(GeoPoint geoPoint) {
            GeoPoint f = ah.this.b.f();
            if (geoPoint != null && !geoPoint.equals(f)) {
                if (com.amap.mapapi.core.b.n) {
                    ah.this.f.j = ah.this.f.a(geoPoint);
                }
                a(false, true);
            }
        }

        public int a() {
            return ah.this.f.f;
        }

        public void b(int i) {
            if (i > 0) {
                if (this.c.VMapMode) {
                    ac acVar = ah.this.f;
                    com.amap.mapapi.core.b.c = i;
                    acVar.f = i;
                    return;
                }
                ac acVar2 = ah.this.f;
                com.amap.mapapi.core.b.a = i;
                acVar2.f = i;
            }
        }

        public int b() {
            return ah.this.f.e;
        }

        public void c(int i) {
            if (i > 0) {
                if (this.c.VMapMode) {
                    ac acVar = ah.this.f;
                    com.amap.mapapi.core.b.d = i;
                    acVar.e = i;
                    return;
                }
                ac acVar2 = ah.this.f;
                com.amap.mapapi.core.b.b = i;
                acVar2.e = i;
            }
        }

        public int c() {
            return com.amap.mapapi.core.b.i;
        }

        public int d() {
            return com.amap.mapapi.core.b.j;
        }

        public int e() {
            if (!this.c.VMapMode) {
                return ah.this.f.g;
            }
            return this.c.mapLevel;
        }

        public GeoPoint f() {
            return ah.this.f.b(ah.this.f.j);
        }

        public void a(bh bhVar) {
            this.d.add(bhVar);
        }

        public void a(boolean z, boolean z2) {
            Iterator<bh> it = this.d.iterator();
            while (it.hasNext()) {
                it.next().a(z, z2);
            }
        }

        public MapView g() {
            return this.c;
        }

        public void h() {
            int childCount = this.c.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = this.c.getChildAt(i);
                if (childAt != null && (childAt.getLayoutParams() instanceof MapView.LayoutParams)) {
                    MapView.LayoutParams layoutParams = (MapView.LayoutParams) childAt.getLayoutParams();
                    if (layoutParams.mode == 0) {
                        a(childAt, layoutParams);
                    } else {
                        b(childAt, layoutParams);
                    }
                }
            }
            ah.this.b.c.c();
        }

        private void a(View view, MapView.LayoutParams layoutParams) {
            if (layoutParams.point != null) {
                Point pixels = ah.this.a.toPixels(layoutParams.point, null);
                pixels.x += layoutParams.x;
                pixels.y += layoutParams.y;
                a(view, layoutParams.width, layoutParams.height, pixels.x, pixels.y, layoutParams.alignment);
            }
        }

        private void b(View view, MapView.LayoutParams layoutParams) {
            a(view, layoutParams.width, layoutParams.height, layoutParams.x, layoutParams.y, layoutParams.alignment);
        }

        private void a(View view, int i, int i2, int i3, int i4, int i5) {
            View view2;
            if ((view instanceof ListView) && (view2 = (View) view.getParent()) != null) {
                i = view2.getWidth();
                i2 = view2.getHeight();
            }
            view.measure(i, i2);
            if (i == -2) {
                i = view.getMeasuredWidth();
            } else if (i == -1) {
                i = this.c.getMeasuredWidth();
            }
            if (i2 == -2) {
                i2 = view.getMeasuredHeight();
            } else if (i2 == -1) {
                i2 = this.c.getMeasuredHeight();
            }
            int i6 = i5 & 7;
            int i7 = i5 & HttpRequestParameters.ONLINE_QUESTION;
            if (i6 == 5) {
                i3 -= i;
            } else if (i6 == 1) {
                i3 -= i / 2;
            }
            if (i7 == 80) {
                i4 -= i2;
            } else if (i7 == 16) {
                i4 -= i2 / 2;
            }
            view.layout(i3, i4, i3 + i, i4 + i2);
        }
    }
}
