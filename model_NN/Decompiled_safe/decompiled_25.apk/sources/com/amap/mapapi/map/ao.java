package com.amap.mapapi.map;

import android.graphics.Point;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import com.mapabc.minimap.map.vmap.NativeMap;
import com.mapabc.minimap.map.vmap.NativeMapEngine;
import com.mapabc.minimap.map.vmap.VMapProjection;
import java.nio.ByteBuffer;

/* compiled from: RenderedMapTile */
class ao {
    public String a;
    boolean b = false;

    ao() {
    }

    public boolean a(NativeMapEngine nativeMapEngine) {
        try {
            ByteBuffer allocate = ByteBuffer.allocate(AccessibilityEventCompat.TYPE_VIEW_TEXT_TRAVERSED_AT_MOVEMENT_GRANULARITY);
            if (nativeMapEngine.hasBitMapData(this.a)) {
                nativeMapEngine.fillBitmapBufferData(this.a, allocate.array());
            } else {
                NativeMap nativeMap = new NativeMap();
                nativeMap.initMap(allocate.array(), 256, 256);
                Point QuadKeyToTile = VMapProjection.QuadKeyToTile(this.a);
                int length = this.a.length();
                nativeMap.setMapParameter(((QuadKeyToTile.x * 256) + 128) << (20 - length), ((QuadKeyToTile.y * 256) + 128) << (20 - length), length, 0);
                nativeMap.paintMap(nativeMapEngine, 1);
                nativeMapEngine.putBitmapData(this.a, allocate.array(), 256, 256);
            }
            this.b = true;
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
