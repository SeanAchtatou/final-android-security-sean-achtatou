package com.amap.mapapi.map;

import android.os.Handler;

/* compiled from: AnimBase */
class b implements Runnable {
    final /* synthetic */ a a;

    b(a aVar) {
        this.a = aVar;
    }

    public void run() {
        this.a.e();
        if (!this.a.f()) {
            this.a.e.removeCallbacks(this);
            Handler unused = this.a.e = null;
            this.a.b();
            return;
        }
        long currentTimeMillis = System.currentTimeMillis();
        this.a.a();
        this.a.g();
        long currentTimeMillis2 = System.currentTimeMillis();
        if (currentTimeMillis2 - currentTimeMillis < ((long) this.a.d)) {
            try {
                Thread.sleep(((long) this.a.d) - (currentTimeMillis2 - currentTimeMillis));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
