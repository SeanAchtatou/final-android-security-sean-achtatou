package com.amap.mapapi.map;

import android.support.v4.view.accessibility.AccessibilityEventCompat;
import com.amap.mapapi.core.c;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.zip.GZIPInputStream;

/* compiled from: MapLoader */
class ab {
    MapView a;
    public ArrayList<String> b = new ArrayList<>();
    boolean c = false;
    long d;
    int e;
    byte[] f;
    int g = 0;
    int h = 0;
    boolean i = false;
    boolean j = false;

    public String a() {
        StringBuffer stringBuffer = new StringBuffer();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.b.size()) {
                break;
            }
            stringBuffer.append(this.b.get(i3) + ";");
            i2 = i3 + 1;
        }
        if (stringBuffer.length() <= 0) {
            return null;
        }
        stringBuffer.deleteCharAt(stringBuffer.length() - 1);
        return "&cp=1&mesh=" + stringBuffer.toString();
    }

    public ab(MapView mapView) {
        this.a = mapView;
        this.d = System.currentTimeMillis();
    }

    /* JADX WARNING: Removed duplicated region for block: B:107:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00ee A[SYNTHETIC, Splitter:B:63:0x00ee] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x00f3  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0100 A[SYNTHETIC, Splitter:B:71:0x0100] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0105 A[SYNTHETIC, Splitter:B:74:0x0105] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x010a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b() {
        /*
            r9 = this;
            r6 = 1
            r4 = 0
            r2 = 0
            r9.j = r6
            boolean r0 = r9.c()
            if (r0 != 0) goto L_0x0016
            com.amap.mapapi.map.MapView r0 = r9.a
            com.amap.mapapi.map.n r0 = r0.i
            r0.a()
            r9.b(r9)
        L_0x0015:
            return
        L_0x0016:
            r1 = r2
            r3 = r2
        L_0x0018:
            java.util.ArrayList<java.lang.String> r0 = r9.b
            int r0 = r0.size()
            if (r1 >= r0) goto L_0x0054
            java.util.ArrayList<java.lang.String> r0 = r9.b
            java.lang.Object r0 = r0.get(r1)
            java.lang.String r0 = (java.lang.String) r0
            com.amap.mapapi.map.MapView r5 = r9.a
            com.mapabc.minimap.map.vmap.NativeMapEngine r5 = r5.f
            if (r5 == 0) goto L_0x0052
            com.amap.mapapi.map.MapView r5 = r9.a
            com.mapabc.minimap.map.vmap.NativeMapEngine r5 = r5.f
            boolean r5 = r5.hasGridData(r0)
            if (r5 == 0) goto L_0x0052
            r5 = r6
        L_0x0039:
            if (r5 == 0) goto L_0x004b
            java.util.ArrayList<java.lang.String> r5 = r9.b
            r5.remove(r1)
            int r1 = r1 + -1
            int r3 = r3 + 1
            com.amap.mapapi.map.MapView r5 = r9.a
            com.amap.mapapi.map.aw r5 = r5.tileDownloadCtrl
            r5.a(r0)
        L_0x004b:
            r0 = r1
            r1 = r3
            int r0 = r0 + 1
            r3 = r1
            r1 = r0
            goto L_0x0018
        L_0x0052:
            r5 = r2
            goto L_0x0039
        L_0x0054:
            java.util.ArrayList<java.lang.String> r0 = r9.b
            int r0 = r0.size()
            if (r0 != 0) goto L_0x0060
            r9.b(r9)
            goto L_0x0015
        L_0x0060:
            if (r3 <= 0) goto L_0x0067
            com.amap.mapapi.map.MapView r0 = r9.a
            r0.postInvalidate()
        L_0x0067:
            r3 = 0
            r0 = 0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0128, all -> 0x00f8 }
            r1.<init>()     // Catch:{ IOException -> 0x0128, all -> 0x00f8 }
            java.lang.String r5 = ""
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ IOException -> 0x0128, all -> 0x00f8 }
            java.lang.String r5 = r9.a()     // Catch:{ IOException -> 0x0128, all -> 0x00f8 }
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ IOException -> 0x0128, all -> 0x00f8 }
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x0128, all -> 0x00f8 }
            com.amap.mapapi.map.MapView r5 = r9.a     // Catch:{ IOException -> 0x0128, all -> 0x00f8 }
            java.net.HttpURLConnection r1 = r5.getConnection(r1)     // Catch:{ IOException -> 0x0128, all -> 0x00f8 }
            if (r1 != 0) goto L_0x009c
            r9.b(r9)
            if (r4 == 0) goto L_0x0090
            r3.close()     // Catch:{ IOException -> 0x010e }
        L_0x0090:
            if (r4 == 0) goto L_0x0095
            r0.close()     // Catch:{ IOException -> 0x0110 }
        L_0x0095:
            if (r1 == 0) goto L_0x0015
            r1.disconnect()
            goto L_0x0015
        L_0x009c:
            r0 = 15000(0x3a98, float:2.102E-41)
            r1.setConnectTimeout(r0)     // Catch:{ IOException -> 0x012c, all -> 0x011e }
            java.lang.String r0 = "GET"
            r1.setRequestMethod(r0)     // Catch:{ IOException -> 0x012c, all -> 0x011e }
            java.io.InputStream r0 = r1.getInputStream()     // Catch:{ IOException -> 0x012c, all -> 0x011e }
            r9.a(r9)     // Catch:{ IOException -> 0x00e3, all -> 0x0122 }
            r5 = 1024(0x400, float:1.435E-42)
            byte[] r5 = new byte[r5]     // Catch:{ IOException -> 0x00e3, all -> 0x0122 }
        L_0x00b1:
            int r6 = r0.read(r5)     // Catch:{ IOException -> 0x00e3, all -> 0x0122 }
            r7 = -1
            if (r6 <= r7) goto L_0x00c9
            boolean r7 = r9.c()     // Catch:{ IOException -> 0x00e3, all -> 0x0122 }
            if (r7 == 0) goto L_0x00c2
            boolean r7 = r9.c     // Catch:{ IOException -> 0x00e3, all -> 0x0122 }
            if (r7 == 0) goto L_0x00dd
        L_0x00c2:
            com.amap.mapapi.map.MapView r2 = r9.a     // Catch:{ IOException -> 0x00e3, all -> 0x0122 }
            com.amap.mapapi.map.n r2 = r2.i     // Catch:{ IOException -> 0x00e3, all -> 0x0122 }
            r2.a()     // Catch:{ IOException -> 0x00e3, all -> 0x0122 }
        L_0x00c9:
            r9.b(r9)
            if (r4 == 0) goto L_0x00d1
            r3.close()     // Catch:{ IOException -> 0x0112 }
        L_0x00d1:
            if (r0 == 0) goto L_0x00d6
            r0.close()     // Catch:{ IOException -> 0x0114 }
        L_0x00d6:
            if (r1 == 0) goto L_0x0015
            r1.disconnect()
            goto L_0x0015
        L_0x00dd:
            int r2 = r2 + r6
            r7 = 0
            r9.a(r9, r7, r5, r6)     // Catch:{ IOException -> 0x00e3, all -> 0x0122 }
            goto L_0x00b1
        L_0x00e3:
            r2 = move-exception
        L_0x00e4:
            r9.b(r9)
            if (r4 == 0) goto L_0x00ec
            r3.close()     // Catch:{ IOException -> 0x0116 }
        L_0x00ec:
            if (r0 == 0) goto L_0x00f1
            r0.close()     // Catch:{ IOException -> 0x0118 }
        L_0x00f1:
            if (r1 == 0) goto L_0x0015
            r1.disconnect()
            goto L_0x0015
        L_0x00f8:
            r0 = move-exception
            r1 = r4
            r2 = r4
        L_0x00fb:
            r9.b(r9)
            if (r4 == 0) goto L_0x0103
            r3.close()     // Catch:{ IOException -> 0x011a }
        L_0x0103:
            if (r1 == 0) goto L_0x0108
            r1.close()     // Catch:{ IOException -> 0x011c }
        L_0x0108:
            if (r2 == 0) goto L_0x010d
            r2.disconnect()
        L_0x010d:
            throw r0
        L_0x010e:
            r2 = move-exception
            goto L_0x0090
        L_0x0110:
            r0 = move-exception
            goto L_0x0095
        L_0x0112:
            r2 = move-exception
            goto L_0x00d1
        L_0x0114:
            r0 = move-exception
            goto L_0x00d6
        L_0x0116:
            r2 = move-exception
            goto L_0x00ec
        L_0x0118:
            r0 = move-exception
            goto L_0x00f1
        L_0x011a:
            r3 = move-exception
            goto L_0x0103
        L_0x011c:
            r1 = move-exception
            goto L_0x0108
        L_0x011e:
            r0 = move-exception
            r2 = r1
            r1 = r4
            goto L_0x00fb
        L_0x0122:
            r2 = move-exception
            r8 = r2
            r2 = r1
            r1 = r0
            r0 = r8
            goto L_0x00fb
        L_0x0128:
            r0 = move-exception
            r0 = r4
            r1 = r4
            goto L_0x00e4
        L_0x012c:
            r0 = move-exception
            r0 = r4
            goto L_0x00e4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.mapapi.map.ab.b():void");
    }

    public boolean c() {
        if (this.e != this.a.mapLevel) {
            return false;
        }
        return this.a.isAGridsInScreen(this.b);
    }

    public void a(String str) {
    }

    public void a(ab abVar) {
        this.f = new byte[AccessibilityEventCompat.TYPE_GESTURE_DETECTION_START];
        this.h = 0;
        this.g = 0;
        this.i = false;
        a("连接打开成功...");
    }

    public void b(String str) {
        this.b.add(str);
    }

    public void b(ab abVar) {
        int i2 = 0;
        this.f = null;
        this.h = 0;
        this.g = 0;
        a((String) null);
        while (true) {
            int i3 = i2;
            if (i3 >= abVar.b.size()) {
                break;
            }
            this.a.tileDownloadCtrl.a(abVar.b.get(i3));
            i2 = i3 + 1;
        }
        if (this.a.i.a.b() == this) {
            this.a.i.a.a();
        }
        this.a.postInvalidate();
    }

    public void a(ab abVar, int i2, byte[] bArr, int i3) {
        System.arraycopy(bArr, 0, this.f, this.g, i3);
        this.g += i3;
        if (!this.i) {
            if (this.g <= 7) {
                return;
            }
            if (c.a(this.f, 0) != 0) {
                abVar.c = true;
                return;
            }
            c.a(this.f, 4);
            c.a(this.f, 8, this.f, 0, i3 - 8);
            this.g -= 8;
            this.h = 0;
            this.i = true;
            d();
        }
        d();
    }

    private void d() {
        if (this.h == 0) {
            if (this.g >= 8) {
                this.h = c.a(this.f, 0) + 8;
                d();
            }
        } else if (this.g >= this.h) {
            int a2 = c.a(this.f, 0);
            int a3 = c.a(this.f, 4);
            if (a3 == 0) {
                a(this.f, 8, a2);
            } else {
                try {
                    GZIPInputStream gZIPInputStream = new GZIPInputStream(new ByteArrayInputStream(this.f, 8, a2));
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    byte[] bArr = new byte[128];
                    while (true) {
                        int read = gZIPInputStream.read(bArr);
                        if (read <= -1) {
                            break;
                        }
                        byteArrayOutputStream.write(bArr, 0, read);
                    }
                    a(byteArrayOutputStream.toByteArray(), 0, a3);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            c.a(this.f, this.h, this.f, 0, this.g - this.h);
            this.g -= this.h;
            this.h = 0;
            d();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.<init>(byte[], int, int):void}
     arg types: [byte[], int, byte]
     candidates:
      ClspMth{java.lang.String.<init>(int[], int, int):void}
      ClspMth{java.lang.String.<init>(char[], int, int):void}
      ClspMth{java.lang.String.<init>(byte[], int, int):void} */
    /* access modifiers changed from: package-private */
    public void a(byte[] bArr, int i2, int i3) {
        c.b(this.f, i2);
        int i4 = i2 + 2;
        c.b(this.f, i4);
        int i5 = i4 + 2;
        c.a(this.f, i5);
        int i6 = i5 + 4;
        int i7 = i6 + 1;
        byte b2 = bArr[i6];
        String str = new String(bArr, i7, (int) b2);
        int i8 = b2 + i7;
        if (this.a.f != null) {
            this.a.f.putGridData(bArr, i2, i3 - i2);
            this.a.f.removeBitmapData(str, this.a.getGridLevelOff(str.length()));
            if (this.a.isGridInScreen(str)) {
                this.a.postInvalidate();
            }
        }
    }
}
