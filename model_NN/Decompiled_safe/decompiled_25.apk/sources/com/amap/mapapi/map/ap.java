package com.amap.mapapi.map;

import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.view.View;
import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.map.MapView;

/* compiled from: RouteInfo */
public class ap extends t implements MapView.b {
    private RouteOverlay i;
    private int j;

    public /* bridge */ /* synthetic */ void a() {
        super.a();
    }

    public /* bridge */ /* synthetic */ void b() {
        super.b();
    }

    public /* bridge */ /* synthetic */ boolean onDown(MotionEvent motionEvent) {
        return super.onDown(motionEvent);
    }

    public /* bridge */ /* synthetic */ boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        return super.onFling(motionEvent, motionEvent2, f, f2);
    }

    public /* bridge */ /* synthetic */ void onLongPress(MotionEvent motionEvent) {
        super.onLongPress(motionEvent);
    }

    public /* bridge */ /* synthetic */ boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        return super.onScroll(motionEvent, motionEvent2, f, f2);
    }

    public /* bridge */ /* synthetic */ void onShowPress(MotionEvent motionEvent) {
        super.onShowPress(motionEvent);
    }

    public /* bridge */ /* synthetic */ boolean onSingleTapUp(MotionEvent motionEvent) {
        return super.onSingleTapUp(motionEvent);
    }

    public ap(MapView mapView, View view, GeoPoint geoPoint, RouteOverlay routeOverlay, int i2) {
        this(mapView, view, geoPoint, null, null, routeOverlay, i2);
    }

    public ap(MapView mapView, View view, GeoPoint geoPoint, Drawable drawable, MapView.LayoutParams layoutParams, RouteOverlay routeOverlay, int i2) {
        super(mapView, view, geoPoint, drawable, layoutParams);
        this.i = routeOverlay;
        this.j = i2;
    }

    public void a(boolean z) {
        boolean z2;
        boolean z3;
        boolean z4 = true;
        super.b();
        this.d.mRouteCtrl.a(z, this);
        this.d.mRouteCtrl.b(this.j < this.i.getRoute().getStepCount());
        MapView.e eVar = this.d.mRouteCtrl;
        if (this.j != 0) {
            z2 = true;
        } else {
            z2 = false;
        }
        eVar.a(z2);
        MapView.e eVar2 = this.d.mRouteCtrl;
        if (this.d.getMaxZoomLevel() != this.d.getZoomLevel()) {
            z3 = true;
        } else {
            z3 = false;
        }
        eVar2.c(z3);
        MapView.e eVar3 = this.d.mRouteCtrl;
        if (this.d.getMinZoomLevel() == this.d.getZoomLevel()) {
            z4 = false;
        }
        eVar3.d(z4);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amap.mapapi.map.MapView.e.a(boolean, com.amap.mapapi.map.MapView$b):void
     arg types: [int, com.amap.mapapi.map.ap]
     candidates:
      com.amap.mapapi.map.MapView.e.a(int, boolean):void
      com.amap.mapapi.map.MapView.e.a(int, int):void
      com.amap.mapapi.map.MapView.e.a(boolean, com.amap.mapapi.map.MapView$b):void */
    public void c() {
        super.c();
        this.d.mRouteCtrl.a(false, (MapView.b) this);
    }

    public void a(int i2) {
        this.i.b.onRouteEvent(this.d, this.i, this.j, i2);
    }
}
