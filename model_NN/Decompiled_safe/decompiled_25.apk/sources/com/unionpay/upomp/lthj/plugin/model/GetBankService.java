package com.unionpay.upomp.lthj.plugin.model;

public class GetBankService extends Data {
    public String creditCard;
    public String debitCard;
    public String panBank;
    public String panBankId;
    public String payTips;
    public String payType;
}
