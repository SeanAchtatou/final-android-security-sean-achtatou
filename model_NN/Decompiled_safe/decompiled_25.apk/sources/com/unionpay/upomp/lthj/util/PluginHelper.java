package com.unionpay.upomp.lthj.util;

import android.app.Activity;
import android.os.Bundle;
import com.lthj.unipay.plugin.as;
import com.lthj.unipay.plugin.el;

public class PluginHelper {
    public static Integer a = 0;

    public static void LaunchPlugin(Activity activity, Bundle bundle) {
        if (activity != null && bundle != null && !as.a().g) {
            as.a().g = true;
            new Thread(new el(activity, bundle)).start();
        }
    }
}
