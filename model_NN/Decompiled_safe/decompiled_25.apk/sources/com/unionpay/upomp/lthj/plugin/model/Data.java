package com.unionpay.upomp.lthj.plugin.model;

public class Data {
    public String application;
    public String misc;
    public String msgExt;
    public String pluginSerialNo;
    public String pluginVersion;
    public String respCode;
    public String respDesc;
    public int stateCode;
    public String terminalModel;
    public String terminalOs;
    public String terminalPhysicalNo;
    public int type;
    public String version;
    public String xmlcontent;
}
