package com.unionpay.upomp.lthj.widget;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.lthj.unipay.plugin.as;
import com.lthj.unipay.plugin.at;
import com.lthj.unipay.plugin.aw;
import com.lthj.unipay.plugin.db;
import com.lthj.unipay.plugin.ef;
import com.lthj.unipay.plugin.j;
import com.lthj.unipay.plugin.r;
import com.lthj.unipay.plugin.z;
import com.unionpay.upomp.lthj.link.PluginLink;
import com.unionpay.upomp.lthj.plugin.ui.UIResponseListener;

public class ValidateCodeView extends RelativeLayout implements View.OnClickListener, UIResponseListener {
    private Context a;
    private AttributeSet b;
    /* access modifiers changed from: private */
    public ImageView c;
    /* access modifiers changed from: private */
    public ProgressBar d;
    private EditText e;
    private Button f;
    /* access modifiers changed from: private */
    public Handler g = new ef(this);

    public ValidateCodeView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = context;
        this.b = attributeSet;
        e();
    }

    private void e() {
        RelativeLayout relativeLayout = (RelativeLayout) LayoutInflater.from(this.a).inflate(PluginLink.getLayoutupomp_lthj_validatcodeview(), this);
        this.c = (ImageView) relativeLayout.findViewById(PluginLink.getIdupomp_lthj_validatecode_img());
        this.c.setOnClickListener(this);
        this.d = (ProgressBar) relativeLayout.findViewById(PluginLink.getIdupomp_lthj_validatecode_progress());
        this.f = (Button) relativeLayout.findViewById(PluginLink.getIdupomp_lthj_get_imgcode());
        this.f.setVisibility(8);
        this.f.setOnClickListener(this);
        this.e = (EditText) relativeLayout.findViewById(PluginLink.getIdupomp_lthj_validatecode_edit());
        if (this.b == null) {
        }
    }

    public ImageView a() {
        return this.c;
    }

    public ProgressBar b() {
        return this.d;
    }

    public EditText c() {
        return this.e;
    }

    public String d() {
        return this.e.getText().toString();
    }

    public void errorCallBack(String str) {
        j.a(this.a, str);
        this.c.setVisibility(8);
        this.d.setVisibility(8);
        this.f.setVisibility(0);
    }

    public void onClick(View view) {
        if (view == this.c) {
            j.a(this);
        } else if (view == this.f) {
            this.f.setVisibility(8);
            j.a(this);
        }
    }

    public void responseCallBack(z zVar) {
        if (zVar != null && zVar.n() != null) {
            int e2 = zVar.e();
            int parseInt = Integer.parseInt(zVar.n());
            switch (e2) {
                case 8201:
                    r rVar = (r) zVar;
                    aw.a("VALIDATECODEVIEW", parseInt + " img");
                    if (parseInt == 0) {
                        StringBuffer stringBuffer = new StringBuffer();
                        stringBuffer.append(as.a().M.subSequence(0, as.a().M.indexOf("/", 7) + 1));
                        stringBuffer.append(rVar.a());
                        stringBuffer.append("?sessionId=");
                        stringBuffer.append(at.a);
                        new db(this, stringBuffer.toString()).start();
                        return;
                    }
                    j.a(this.a, rVar.o(), parseInt);
                    this.c.setVisibility(0);
                    this.c.setBackgroundColor(-1);
                    this.c.setImageBitmap(null);
                    this.d.setVisibility(8);
                    return;
                default:
                    return;
            }
        }
    }
}
