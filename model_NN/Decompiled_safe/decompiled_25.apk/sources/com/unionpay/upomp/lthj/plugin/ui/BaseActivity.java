package com.unionpay.upomp.lthj.plugin.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.lthj.unipay.plugin.aw;
import com.lthj.unipay.plugin.bt;
import com.lthj.unipay.plugin.en;
import com.lthj.unipay.plugin.eo;
import com.unionpay.upomp.lthj.link.PluginLink;

public class BaseActivity extends Activity {
    private String a;
    private View.OnClickListener b;
    /* access modifiers changed from: private */
    public bt c;

    public bt a() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public void a(String str, View.OnClickListener onClickListener) {
        this.a = str;
        this.b = onClickListener;
        aw.a("baseactivity", str + PoiTypeDef.All);
        this.c.changeTitleButton(str, onClickListener);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        if (getParent() instanceof bt) {
            this.c = (bt) getParent();
            a(null, null);
        }
        super.onCreate(bundle);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        quitNotice();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (this.c != null) {
            this.c.changeTitleButton(this.a, this.b);
        }
        this.c.aboutEnable(false);
        super.onResume();
    }

    public void quitNotice() {
        new AlertDialog.Builder(this).setTitle("退出交易").setMessage(PluginLink.getStringupomp_lthj_app_quitNotice_msg()).setNegativeButton(PluginLink.getStringupomp_lthj_cancel(), new eo(this)).setPositiveButton(PluginLink.getStringupomp_lthj_ok(), new en(this)).show();
    }
}
