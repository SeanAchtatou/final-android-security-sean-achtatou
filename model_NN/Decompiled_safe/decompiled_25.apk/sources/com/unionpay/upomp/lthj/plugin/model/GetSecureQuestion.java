package com.unionpay.upomp.lthj.plugin.model;

public class GetSecureQuestion extends Data {
    public String loginName;
    public String mobileMac;
    public String mobileNumber;
    public String secureQuestion;
    public String validateCode;
}
