package com.unionpay.upomp.lthj.plugin.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.widget.SeekBar;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.lthj.unipay.plugin.ad;
import com.lthj.unipay.plugin.ae;
import com.lthj.unipay.plugin.af;
import com.lthj.unipay.plugin.ag;
import com.lthj.unipay.plugin.ai;
import com.lthj.unipay.plugin.aj;
import com.lthj.unipay.plugin.ak;
import com.lthj.unipay.plugin.as;
import com.lthj.unipay.plugin.at;
import com.lthj.unipay.plugin.az;
import com.lthj.unipay.plugin.cc;
import com.lthj.unipay.plugin.co;
import com.lthj.unipay.plugin.cz;
import com.lthj.unipay.plugin.er;
import com.lthj.unipay.plugin.h;
import com.lthj.unipay.plugin.j;
import com.lthj.unipay.plugin.z;
import com.unionpay.upomp.lthj.link.PluginLink;
import com.unionpay.upomp.lthj.plugin.model.HeadData;
import com.unionpay.upomp.lthj.plugin.model.JNIInitBottomData;
import com.unionpay.upomp.lthj.util.PluginHelper;
import java.io.File;
import java.util.Calendar;
import java.util.Timer;

public class SplashActivity extends Activity implements UIResponseListener {
    public static Activity instance;
    Handler a = new aj(this);
    public int b;
    public Handler c = new ai(this);
    /* access modifiers changed from: private */
    public String d;
    /* access modifiers changed from: private */
    public ak e;
    /* access modifiers changed from: private */
    public Handler f = new ae(this);
    /* access modifiers changed from: private */
    public SeekBar g;

    /* access modifiers changed from: private */
    public void a() {
        try {
            if (as.a().a != null) {
                er erVar = new er(8198);
                erVar.a(HeadData.createHeadData("PluginInit.Req", this));
                erVar.b(as.a().a.a());
                erVar.a(as.a().i);
                co.a().a(erVar, this, this, false, true);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        Message message = new Message();
        message.obj = str;
        this.a.sendMessage(message);
    }

    /* access modifiers changed from: private */
    public boolean a(byte[] bArr) {
        if (bArr == null) {
            a(getString(PluginLink.getStringupomp_lthj_merchantId_Empty_prompt()));
            return false;
        }
        try {
            as.a().a(bArr);
            if (TextUtils.isEmpty(as.a().i)) {
                a(getString(PluginLink.getStringupomp_lthj_merchantId_Empty_prompt()));
                return false;
            } else if (as.a().i.length() < 15 || as.a().i.length() > 24) {
                a(getString(PluginLink.getStringupomp_lthj_merchantId_Length_prompt()));
                return false;
            } else if (TextUtils.isEmpty(as.a().l)) {
                a(getString(PluginLink.getStringupomp_lthj_merchantOrderId_Empty_prompt()));
                return false;
            } else if (TextUtils.isEmpty(as.a().m)) {
                a(getString(PluginLink.getStringupomp_lthj_merchantOrderTime_Empty_prompt()));
                return false;
            } else {
                if (TextUtils.isEmpty(j.c(as.a().m))) {
                    a(getString(PluginLink.getStringupomp_lthj_merchantOrderTime_error_prompt()));
                    return false;
                }
                return true;
            }
        } catch (Exception e2) {
            a(getString(PluginLink.getStringupomp_lthj_merchantXml_Format_prompt()));
        }
    }

    public void errorCallBack(String str) {
        if (!isFinishing()) {
            a(str);
        }
    }

    public void initBottomData(byte[] bArr) {
        boolean z = true;
        if (bArr == null) {
            File file = new File(this.d);
            this.e = new ak(this);
            if (!file.exists()) {
                byte[] a2 = as.a().e ? this.e.a(PluginLink.getRawupomp_lthj_config_test()) : this.e.a(PluginLink.getRawupomp_lthj_config_formal());
                bArr = JniMethod.getJniMethod().decryptConfig(a2, a2.length);
            } else {
                byte[] a3 = this.e.a(this.d);
                bArr = JniMethod.getJniMethod().decryptConfig(a3, a3.length);
                z = false;
            }
        }
        if (bArr == null) {
            try {
                errorCallBack(getString(PluginLink.getStringupomp_lthj_merchantXml_ReadError_prompt()));
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        } else {
            as.a().a.a(bArr);
            byte[] d2 = as.a().a.d();
            byte[] e3 = as.a().a.e();
            byte[] bArr2 = new byte[260];
            bArr2[0] = 0;
            bArr2[1] = 4;
            bArr2[2] = 0;
            bArr2[3] = 0;
            System.arraycopy(d2, 0, bArr2, 4, d2.length);
            System.arraycopy(e3, 0, bArr2, 257, e3.length);
            byte[] f2 = as.a().a.f();
            byte[] g2 = as.a().a.g();
            byte[] bArr3 = new byte[260];
            bArr3[0] = 0;
            bArr3[1] = 4;
            bArr3[2] = 0;
            bArr3[3] = 0;
            System.arraycopy(f2, 0, bArr3, 4, f2.length);
            System.arraycopy(g2, 0, bArr3, 257, g2.length);
            JNIInitBottomData jNIInitBottomData = new JNIInitBottomData();
            jNIInitBottomData.certVersion = as.a().a.b();
            jNIInitBottomData.phoneIMEI = j.a(this);
            jNIInitBottomData.imsi = j.b(this);
            if (as.a().e) {
                JniMethod.getJniMethod().initResource(1, jNIInitBottomData, bArr2, bArr3);
            } else {
                JniMethod.getJniMethod().initResource(0, jNIInitBottomData, bArr2, bArr3);
            }
            if (z) {
                this.e.a(this.d, JniMethod.getJniMethod().encryptConfig(bArr, bArr.length));
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.lthj.unipay.plugin.j.a(android.content.Context, boolean):boolean
     arg types: [com.unionpay.upomp.lthj.plugin.ui.SplashActivity, int]
     candidates:
      com.lthj.unipay.plugin.j.a(android.content.Context, java.lang.String):void
      com.lthj.unipay.plugin.j.a(java.lang.String, char):java.lang.String[]
      com.lthj.unipay.plugin.j.a(android.content.Context, boolean):boolean */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView(PluginLink.getLayoutupomp_lthj_splash());
        instance = this;
        getWindow().setSoftInputMode(18);
        ((TextView) findViewById(PluginLink.getIdupomp_lthj_textview_time())).setText("©2002-" + Calendar.getInstance().get(1));
        this.g = (SeekBar) findViewById(PluginLink.getIdupomp_lthj_splash_seekbar());
        this.g.setEnabled(false);
        this.b = 0;
        this.g.setProgress(this.b);
        new Timer().schedule(new ag(this), 0, 100);
        at.a = PoiTypeDef.All;
        as.a().h = j.a((Context) this, false);
        new Thread(new af(this)).start();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return 4 == i;
    }

    public void responseCallBack(z zVar) {
        if (!isFinishing() && zVar != null) {
            int parseInt = Integer.parseInt(zVar.n());
            switch (zVar.e()) {
                case 8198:
                    er erVar = (er) zVar;
                    if (parseInt != 0) {
                        a("初始化失败，" + erVar.o() + "，错误码为" + erVar.n());
                        return;
                    } else if (!erVar.d()) {
                        byte[] a2 = j.a(erVar.a().getBytes());
                        byte[] b2 = j.b((new String(a2) + j.a(this)).getBytes());
                        byte[] a3 = j.a(erVar.c().getBytes());
                        if (a3 == null || a3.length != 16) {
                            errorCallBack(getString(PluginLink.getStringupomp_lthj_merchantXml_HashFormat_prompt()));
                            return;
                        }
                        for (int i = 0; i < 16; i++) {
                            if (b2[i] != a3[i]) {
                                errorCallBack(getString(PluginLink.getStringupomp_lthj_merchantXml_MD5Error_prompt()));
                                return;
                            }
                        }
                        cc ccVar = new cc(this);
                        ccVar.a(a2);
                        ccVar.start();
                        return;
                    } else {
                        String l = erVar.l();
                        String str = PoiTypeDef.All;
                        h hVar = new h(this);
                        cz a4 = hVar.a(1);
                        if (!(a4 == null || a4.b() == null)) {
                            str = a4.b();
                            byte[] bytes = str.getBytes();
                            byte[] decryptConfig = JniMethod.getJniMethod().decryptConfig(bytes, bytes.length);
                            if (decryptConfig != null) {
                                str = new String(decryptConfig);
                            }
                        }
                        if (!l.equals(str)) {
                            byte[] bytes2 = l.getBytes();
                            String str2 = new String(JniMethod.getJniMethod().encryptConfig(bytes2, bytes2.length));
                            if (a4 == null) {
                                cz czVar = new cz();
                                czVar.a(str2);
                                hVar.a(czVar);
                            } else {
                                a4.a(str2);
                                hVar.b(a4);
                            }
                        }
                        as.a().f = true;
                        az azVar = new az(8223);
                        azVar.a(HeadData.createHeadData("CheckOrder.Req", this));
                        azVar.a(as.a().i);
                        azVar.b(as.a().l);
                        azVar.c(as.a().m);
                        azVar.d(as.a().s);
                        try {
                            co.a().a(azVar, this, this, false, true);
                            return;
                        } catch (Exception e2) {
                            e2.printStackTrace();
                            return;
                        }
                    }
                case 8223:
                    az azVar2 = (az) zVar;
                    if (parseInt == 0) {
                        as.a().j = azVar2.a();
                        as.a().k = azVar2.t();
                        as.a().n = azVar2.c();
                        as.a().o = azVar2.d();
                        as.a().q = azVar2.p();
                        as.a().r = azVar2.r();
                        as.a().p = azVar2.s();
                        as.a().L = azVar2.q();
                        synchronized (PluginHelper.a) {
                            PluginHelper.a.notify();
                        }
                        finish();
                        return;
                    }
                    a(azVar2.o() + "，错误码为" + azVar2.n());
                    return;
                default:
                    return;
            }
        }
    }

    public void showInitDialog(String str) {
        new AlertDialog.Builder(this).setTitle("提示").setMessage(str).setPositiveButton("确定", new ad(this)).show();
    }
}
