package com.unionpay.upomp.lthj.plugin.model;

public class Register extends Data {
    public String email;
    public String loginName;
    public String mobileMac;
    public String mobileNumber;
    public String password;
    public String secureAnswer;
    public String secureQuestion;
    public String validateCode;
    public String welcome;
}
