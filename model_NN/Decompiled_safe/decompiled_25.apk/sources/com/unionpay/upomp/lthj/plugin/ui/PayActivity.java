package com.unionpay.upomp.lthj.plugin.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.lthj.unipay.plugin.Cdo;
import com.lthj.unipay.plugin.aa;
import com.lthj.unipay.plugin.as;
import com.lthj.unipay.plugin.at;
import com.lthj.unipay.plugin.ba;
import com.lthj.unipay.plugin.bb;
import com.lthj.unipay.plugin.bc;
import com.lthj.unipay.plugin.bd;
import com.lthj.unipay.plugin.be;
import com.lthj.unipay.plugin.bg;
import com.lthj.unipay.plugin.bh;
import com.lthj.unipay.plugin.bi;
import com.lthj.unipay.plugin.bo;
import com.lthj.unipay.plugin.bw;
import com.lthj.unipay.plugin.bz;
import com.lthj.unipay.plugin.c;
import com.lthj.unipay.plugin.cd;
import com.lthj.unipay.plugin.co;
import com.lthj.unipay.plugin.cp;
import com.lthj.unipay.plugin.cu;
import com.lthj.unipay.plugin.dj;
import com.lthj.unipay.plugin.dk;
import com.lthj.unipay.plugin.dl;
import com.lthj.unipay.plugin.dm;
import com.lthj.unipay.plugin.dn;
import com.lthj.unipay.plugin.dp;
import com.lthj.unipay.plugin.dq;
import com.lthj.unipay.plugin.dr;
import com.lthj.unipay.plugin.ds;
import com.lthj.unipay.plugin.dt;
import com.lthj.unipay.plugin.ea;
import com.lthj.unipay.plugin.eg;
import com.lthj.unipay.plugin.ek;
import com.lthj.unipay.plugin.eq;
import com.lthj.unipay.plugin.f;
import com.lthj.unipay.plugin.j;
import com.lthj.unipay.plugin.k;
import com.lthj.unipay.plugin.l;
import com.lthj.unipay.plugin.n;
import com.lthj.unipay.plugin.z;
import com.unionpay.upomp.lthj.link.PluginLink;
import com.unionpay.upomp.lthj.plugin.model.GetBundleBankCardList;
import com.unionpay.upomp.lthj.plugin.model.HeadData;
import com.unionpay.upomp.lthj.plugin.model.SmsCodeVerfiyData;
import com.unionpay.upomp.lthj.widget.CustomInputView;
import com.unionpay.upomp.lthj.widget.LineFrameView;
import com.unionpay.upomp.lthj.widget.ValidateCodeView;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

public class PayActivity extends BaseActivity implements View.OnClickListener, UIResponseListener {
    private static String b = "PayActivity";
    private ImageButton A;
    private RelativeLayout B;
    private ValidateCodeView C;
    private View.OnClickListener D = new bi(this);
    private View.OnClickListener E = new bh(this);
    /* access modifiers changed from: private */
    public Handler F = new dr(this);
    private CustomInputView G;
    private CustomInputView H;
    private Dialog I;
    private Button J;
    private Button K;
    private Button L;
    private CheckBox M;
    private Button N;
    public int a = 60;
    public TimerTask aaTimerTask;
    private String c;
    private EditText d;
    private EditText e;
    private EditText f;
    private Button g;
    private String h;
    private String i;
    /* access modifiers changed from: private */
    public String j;
    private EditText k;
    /* access modifiers changed from: private */
    public LineFrameView l;
    /* access modifiers changed from: private */
    public LineFrameView m;
    private LineFrameView n;
    /* access modifiers changed from: private */
    public Button o;
    private Button p;
    /* access modifiers changed from: private */
    public CustomInputView q;
    /* access modifiers changed from: private */
    public CustomInputView r;
    private CustomInputView s;
    private Button t;
    /* access modifiers changed from: private */
    public String[] u;
    private Button v;
    private Button w;
    private Button x;
    private bw y;
    private Button z;

    private void a(cd cdVar) {
        setContentView(PluginLink.getLayoutupomp_lthj_quick_bind_result());
        LineFrameView lineFrameView = (LineFrameView) findViewById(PluginLink.getIdupomp_lthj_state_view());
        LineFrameView lineFrameView2 = (LineFrameView) findViewById(PluginLink.getIdupomp_lthj_error_desc());
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_username_view())).a(cdVar.a());
        if (Integer.parseInt(cdVar.n()) == 0) {
            findViewById(PluginLink.getIdupomp_lthj_desc_line()).setVisibility(8);
            lineFrameView2.setVisibility(8);
            lineFrameView.a(PluginLink.getStringupomp_lthj_bind_success());
            lineFrameView.b(PluginLink.getDrawableupomp_lthj_success_icon());
        } else {
            lineFrameView.a(PluginLink.getStringupomp_lthj_bind_fail());
            lineFrameView2.a(cdVar.o());
            lineFrameView.b(PluginLink.getDrawableupomp_lthj_fail_icon());
        }
        lineFrameView.a().setTextColor(-65536);
        ((Button) findViewById(PluginLink.getIdupomp_lthj_button_ok())).setOnClickListener(new ba(this));
    }

    private void a(cu cuVar) {
        if (Integer.parseInt(cuVar.n()) == 0) {
            g();
            return;
        }
        setContentView(PluginLink.getLayoutupomp_lthj_quick_reg_result());
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_reg_prompt())).a().setTextColor(-65536);
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_error_desc())).a().setText(cuVar.o());
        ((Button) findViewById(PluginLink.getIdupomp_lthj_button_ok())).setOnClickListener(new bb(this));
    }

    private void a(ea eaVar) {
        a(getString(PluginLink.getStringupomp_lthj_backtomerchant()), this.E);
        setContentView(PluginLink.getLayoutupomp_lthj_traderesult());
        LineFrameView lineFrameView = (LineFrameView) findViewById(PluginLink.getIdupomp_lthj_pay_state());
        LineFrameView lineFrameView2 = (LineFrameView) findViewById(PluginLink.getIdupomp_lthj_pay_desc());
        lineFrameView2.a().setTextColor(-65536);
        lineFrameView.a().setTextColor(-65536);
        int parseInt = Integer.parseInt(eaVar.n());
        ((TextView) findViewById(PluginLink.getIdupomp_lthj_merchant_tv())).setText(as.a().j);
        ((TextView) findViewById(PluginLink.getIdupomp_lthj_orderamt_tv())).setText(j.d(as.a().n));
        ((TextView) findViewById(PluginLink.getIdupomp_lthj_orderno_tv())).setText(as.a().l);
        ((TextView) findViewById(PluginLink.getIdupomp_lthj_ordertime_tv())).setText(j.c(as.a().m));
        ((Button) findViewById(PluginLink.getIdupomp_lthj_unfold_btn())).setOnClickListener(new dn(this));
        if (parseInt == 0) {
            j.d(this, as.a().w.toString());
            if (this.h == "1") {
                as.a().b.c(false);
            } else if (this.h == "3") {
                as.a().b.d(false);
            }
            lineFrameView.a(PluginLink.getStringupomp_lthj_pay_success());
            findViewById(PluginLink.getIdupomp_lthj_desc_line()).setVisibility(8);
            lineFrameView2.setVisibility(8);
            this.z = (Button) findViewById(PluginLink.getIdupomp_lthj_next_btn());
            if (this.h.equals("1")) {
                this.z.setOnClickListener(new dm(this));
                return;
            }
            lineFrameView2.a(eaVar.o());
            this.z.setText(PluginLink.getStringupomp_lthj_backtomerchant());
            this.z.setOnClickListener(new dl(this));
            return;
        }
        if (this.h == "1") {
            as.a().b.c(true);
        } else if (this.h == "3") {
            as.a().b.d(true);
        }
        lineFrameView.b(PluginLink.getDrawableupomp_lthj_fail_icon());
        lineFrameView.a(PluginLink.getStringupomp_lthj_pay_fail());
        lineFrameView2.a(eaVar.o() + "(" + eaVar.n() + ")");
        this.z = (Button) findViewById(PluginLink.getIdupomp_lthj_next_btn());
        this.z.setText(PluginLink.getStringupomp_lthj_repay());
        this.z.setOnClickListener(new dk(this));
    }

    private void b() {
        this.h = "1";
        setContentView(PluginLink.getLayoutupomp_lthj_commonpay());
        a(getString(PluginLink.getStringupomp_lthj_back()), new bg(this));
        this.C = (ValidateCodeView) findViewById(PluginLink.getIdupomp_lthj_validatecode_layout());
        if (as.a().b.c()) {
            this.C.setVisibility(0);
            j.a(this.C);
        }
        this.l = (LineFrameView) findViewById(PluginLink.getIdupomp_lthj_bankcard_info_view());
        this.m = (LineFrameView) findViewById(PluginLink.getIdupomp_lthj_mobile_number_view());
        this.n = (LineFrameView) findViewById(PluginLink.getIdupomp_lthj_safe_prompt_view());
        this.l.a(this.i + "-" + j.a(this.c) + "(" + as.a().w.substring(as.a().w.length() - 4) + ")");
        this.m.a(j.f(this.j));
        this.d = (EditText) findViewById(PluginLink.getIdupomp_lthj_cvn2_edit());
        this.e = (EditText) findViewById(PluginLink.getIdupomp_lthj_date_edit());
        this.f = (EditText) findViewById(PluginLink.getIdupomp_lthj_pin_edit());
        this.g = (Button) findViewById(PluginLink.getIdupomp_lthj_button_ok());
        this.g.setOnClickListener(this);
        this.K = (Button) findViewById(PluginLink.getIdupomp_lthj_cvn2_help());
        this.L = (Button) findViewById(PluginLink.getIdupomp_lthj_date_help());
        this.L.setOnClickListener(this);
        this.K.setOnClickListener(this);
        aa aaVar = new aa(0);
        this.f.setOnFocusChangeListener(aaVar);
        this.f.setOnTouchListener(aaVar);
        aa aaVar2 = new aa(2);
        this.d.setOnFocusChangeListener(aaVar2);
        this.d.setOnTouchListener(aaVar2);
        this.y = new bw();
        f fVar = new f(this.y);
        this.e.setOnFocusChangeListener(fVar);
        this.e.setOnTouchListener(fVar);
        this.k = (EditText) findViewById(PluginLink.getIdupomp_lthj_mobilemac_edit());
        this.o = (Button) findViewById(PluginLink.getIdupomp_lthj_get_mac_btn());
        this.o.setOnClickListener(new be(this));
        if ("00".equals(this.c)) {
            findViewById(PluginLink.getIdupomp_lthj_pin_layout()).setVisibility(8);
        } else if ("01".equals(this.c)) {
            findViewById(PluginLink.getIdupomp_lthj_cvn2_layout()).setVisibility(8);
            findViewById(PluginLink.getIdupomp_lthj_date_layout()).setVisibility(8);
        }
    }

    private void c() {
        this.h = "3";
        setContentView(PluginLink.getLayoutupomp_lthj_savecardpay());
        this.C = (ValidateCodeView) findViewById(PluginLink.getIdupomp_lthj_validatecode_layout());
        this.f = (EditText) findViewById(PluginLink.getIdupomp_lthj_pin_edit());
        this.l = (LineFrameView) findViewById(PluginLink.getIdupomp_lthj_bankcard_info_view());
        this.l.a(this.i + "-" + j.a(this.c) + "(" + as.a().w.substring(as.a().w.length() - 4) + ")");
        this.g = (Button) findViewById(PluginLink.getIdupomp_lthj_button_ok());
        this.g.setOnClickListener(this);
        aa aaVar = new aa(0);
        this.f.setOnFocusChangeListener(aaVar);
        this.f.setOnTouchListener(aaVar);
        if (as.a().b.d()) {
            this.C.setVisibility(0);
            j.a(this.C);
        }
    }

    private void d() {
        this.h = "2";
        cp.a(this, this);
        l();
    }

    private void e() {
        setContentView(PluginLink.getLayoutupomp_lthj_quick_register());
        ((CustomInputView) findViewById(PluginLink.getIdupomp_lthj_username_view())).a(this.j);
        this.G = (CustomInputView) findViewById(PluginLink.getIdupomp_lthj_password_edit());
        aa aaVar = new aa(8);
        this.G.b().setOnTouchListener(aaVar);
        this.G.b().setOnFocusChangeListener(aaVar);
        this.M = (CheckBox) findViewById(PluginLink.getIdupomp_lthj_user_pro_box());
        ((Button) findViewById(PluginLink.getIdupomp_lthj_reg_pro_btn())).setOnClickListener(new bd(this));
        this.H = (CustomInputView) findViewById(PluginLink.getIdupomp_lthj_confirm_pwd_input());
        aa aaVar2 = new aa(7);
        this.H.b().setOnTouchListener(aaVar2);
        this.H.b().setOnFocusChangeListener(aaVar2);
        this.J = (Button) findViewById(PluginLink.getIdupomp_lthj_next_btn());
        this.J.setOnClickListener(this);
    }

    private void f() {
        View inflate = View.inflate(this, PluginLink.getLayoutupomp_lthj_quick_reg_confirm(), null);
        this.q = (CustomInputView) inflate.findViewById(PluginLink.getIdupomp_lthj_welcome_view());
        this.N = (Button) inflate.findViewById(PluginLink.getIdupomp_lthj_auto_welcome_btn());
        this.N.setOnClickListener(new bc(this));
        this.q.a(Cdo.b[0]);
        this.r = (CustomInputView) inflate.findViewById(PluginLink.getIdupomp_lthj_safe_ask_view());
        this.s = (CustomInputView) inflate.findViewById(PluginLink.getIdupomp_lthj_safe_asw_view());
        this.r.a(PluginLink.getStringupomp_lthj_safe_ask_default());
        this.s.a(this.i + j.a(this.c) + j.h(as.a().w.toString()));
        this.p = (Button) inflate.findViewById(PluginLink.getIdupomp_lthj_button_ok());
        this.p.setOnClickListener(this);
        this.t = (Button) inflate.findViewById(PluginLink.getIdupomp_lthj_safe_ask_drop());
        this.t.setOnClickListener(this);
        this.I = new Dialog(this, PluginLink.getStyleupomp_lthj_common_dialog());
        Display defaultDisplay = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        WindowManager.LayoutParams attributes = getWindow().getAttributes();
        attributes.height = (int) (((double) defaultDisplay.getHeight()) * 0.5d);
        attributes.width = defaultDisplay.getWidth();
        attributes.x = 0;
        attributes.y = defaultDisplay.getHeight() - attributes.height;
        this.I.getWindow().setAttributes(attributes);
        this.I.setContentView(inflate);
        this.I.show();
    }

    private void g() {
        setContentView(PluginLink.getLayoutupomp_lthj_quick_bindcard());
        this.l = (LineFrameView) findViewById(PluginLink.getIdupomp_lthj_bankcard_info_view());
        this.m = (LineFrameView) findViewById(PluginLink.getIdupomp_lthj_username_view());
        this.l.a(this.i + "-" + j.a(this.c) + "(" + as.a().w.substring(as.a().w.length() - 4, as.a().w.length()) + ")");
        this.m.a(this.j);
        this.d = (EditText) findViewById(PluginLink.getIdupomp_lthj_cvn2_edit());
        this.e = (EditText) findViewById(PluginLink.getIdupomp_lthj_date_edit());
        this.f = (EditText) findViewById(PluginLink.getIdupomp_lthj_pin_edit());
        this.v = (Button) findViewById(PluginLink.getIdupomp_lthj_button_ok());
        this.v.setOnClickListener(this);
        aa aaVar = new aa(0);
        this.f.setOnFocusChangeListener(aaVar);
        this.f.setOnTouchListener(aaVar);
        aa aaVar2 = new aa(2);
        this.d.setOnFocusChangeListener(aaVar2);
        this.d.setOnTouchListener(aaVar2);
        this.y = new bw();
        f fVar = new f(this.y);
        this.e.setOnFocusChangeListener(fVar);
        this.e.setOnTouchListener(fVar);
        if ("00".equals(this.c)) {
            findViewById(PluginLink.getIdupomp_lthj_pin_layout()).setVisibility(8);
        } else if ("01".equals(this.c)) {
            findViewById(PluginLink.getIdupomp_lthj_cvn2_layout()).setVisibility(8);
            findViewById(PluginLink.getIdupomp_lthj_date_layout()).setVisibility(8);
        }
    }

    private void h() {
        if (!c.a(this, this.k, this.o) || !c.a(this, this.C)) {
            return;
        }
        if (as.a().v) {
            ek ekVar = new ek(8227);
            ekVar.a(HeadData.createHeadData("PreAuth.Req", this));
            ekVar.a(this.h);
            if (this.h.equals("1")) {
                ekVar.f(as.a().w.toString());
                if ("00".equals(this.c)) {
                    if (c.j(this, this.d) && c.i(this, this.e)) {
                        ekVar.k(this.d.getText().toString());
                        StringBuffer stringBuffer = new StringBuffer();
                        stringBuffer.append(new String(JniMethod.getJniMethod().decryptConfig(this.y.b(), this.y.b().length)));
                        stringBuffer.append(new String(JniMethod.getJniMethod().decryptConfig(this.y.a(), this.y.a().length)));
                        ekVar.h(stringBuffer.toString());
                        stringBuffer.delete(0, stringBuffer.length());
                    } else {
                        return;
                    }
                } else if ("01".equals(this.c)) {
                    if (c.l(this, this.f)) {
                        ekVar.g(as.a().z.toString());
                    } else {
                        return;
                    }
                }
                ekVar.c(this.j);
            } else if (this.h.equals("2")) {
                ekVar.b(as.a().A.toString());
                ekVar.a(as.a().C);
                ekVar.f(as.a().E.pan);
                ekVar.c(as.a().E.mobileNumber);
            }
            if (this.C != null) {
                ekVar.e(this.C.d());
            }
            if (this.k != null) {
                ekVar.d(this.k.getText().toString());
            }
            ekVar.m(as.a().i);
            ekVar.l(as.a().j);
            ekVar.p(as.a().n);
            ekVar.q(as.a().o);
            ekVar.n(as.a().l);
            ekVar.o(as.a().m);
            ekVar.r(as.a().q);
            ekVar.t(as.a().r);
            ekVar.u(as.a().p);
            ekVar.s(as.a().L);
            try {
                co.a().a(ekVar, this, this, true, false);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        } else if (c.a(this, this.k, this.o)) {
            ea eaVar = new ea(8210);
            eaVar.a(HeadData.createHeadData("CommonPay.Req", this));
            eaVar.b(this.h);
            if (this.h.equals("1")) {
                SmsCodeVerfiyData smsCodeVerfiyData = new SmsCodeVerfiyData();
                smsCodeVerfiyData.smsCode = this.k.getText().toString();
                smsCodeVerfiyData.sessionID = at.a;
                smsCodeVerfiyData.codeMd5 = as.a().y.toString();
                if (c.a(this, smsCodeVerfiyData)) {
                    eaVar.h(as.a().w.toString());
                    if ("00".equals(this.c)) {
                        if (c.j(this, this.d) && c.i(this, this.e)) {
                            eaVar.m(this.d.getText().toString());
                            StringBuffer stringBuffer2 = new StringBuffer();
                            stringBuffer2.append(new String(JniMethod.getJniMethod().decryptConfig(this.y.b(), this.y.b().length)));
                            stringBuffer2.append(new String(JniMethod.getJniMethod().decryptConfig(this.y.a(), this.y.a().length)));
                            eaVar.l(stringBuffer2.toString());
                            stringBuffer2.delete(0, stringBuffer2.length());
                        } else {
                            return;
                        }
                    } else if ("01".equals(this.c)) {
                        if (c.l(this, this.f)) {
                            eaVar.k(as.a().z.toString());
                        } else {
                            return;
                        }
                    }
                    eaVar.e(this.j);
                } else {
                    return;
                }
            } else if (this.h.equals("2")) {
                eaVar.c(as.a().A.toString());
                eaVar.d(new String(as.a().C));
                eaVar.h(as.a().E.pan);
                eaVar.e(as.a().E.mobileNumber);
            }
            if (this.C != null) {
                eaVar.g(this.C.d());
            }
            if (this.k != null) {
                eaVar.f(this.k.getText().toString());
            }
            eaVar.a(as.a().k);
            eaVar.f(this.k.getText().toString());
            eaVar.o(as.a().i);
            eaVar.n(as.a().j);
            eaVar.r(as.a().n);
            eaVar.s(as.a().o);
            eaVar.p(as.a().l);
            eaVar.q(as.a().m);
            eaVar.u(as.a().q);
            eaVar.t(as.a().r);
            eaVar.v(as.a().p);
            eaVar.w(as.a().L);
            try {
                co.a().a(eaVar, this, this, true, false);
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        k kVar = new k(8222);
        kVar.a(HeadData.createHeadData("CheckUserExist.Req", this));
        kVar.a(this.j);
        try {
            co.a().a(kVar, this, this, true, true);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        if ("2" == this.h) {
            k();
            return;
        }
        a().changeSubActivity(new Intent(this, HomeActivity.class));
    }

    private void k() {
        if (as.a().D == null || as.a().D.size() == 0) {
            l();
            return;
        }
        a(getString(PluginLink.getStringupomp_lthj_back()), this.D);
        setContentView(PluginLink.getLayoutupomp_lthj_quickpay_hascard());
        this.w = (Button) findViewById(PluginLink.getIdupomp_lthj_account_mange_btn());
        this.w.setOnClickListener(this);
        this.l = (LineFrameView) findViewById(PluginLink.getIdupomp_lthj_bankcard_info_view());
        this.m = (LineFrameView) findViewById(PluginLink.getIdupomp_lthj_mobile_number_view());
        this.k = (EditText) findViewById(PluginLink.getIdupomp_lthj_mobilemac_edit());
        this.o = (Button) findViewById(PluginLink.getIdupomp_lthj_get_mac_btn());
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_username_view())).a(as.a().A.toString());
        LineFrameView lineFrameView = (LineFrameView) findViewById(PluginLink.getIdupomp_lthj_welcome_view());
        lineFrameView.a(as.a().G);
        lineFrameView.a().setTextColor(-65536);
        as.a().E = j.a(as.a().D);
        if (as.a().E == null) {
            as.a().E = (GetBundleBankCardList) as.a().D.get(0);
        }
        this.l.a(as.a().E.panBank + "-" + j.b(as.a().E.panType) + "(" + as.a().E.pan.substring(as.a().E.pan.length() - 4, as.a().E.pan.length()) + ")");
        this.m.a(j.f(as.a().E.mobileNumber));
        this.o.setOnClickListener(new dj(this));
        this.B = (RelativeLayout) findViewById(PluginLink.getIdupomp_lthj_quickpaycard_layout());
        this.B.setOnClickListener(this);
        this.g = (Button) findViewById(PluginLink.getIdupomp_lthj_button_ok());
        this.g.setOnClickListener(this);
    }

    private void l() {
        a(getString(PluginLink.getStringupomp_lthj_back()), this.D);
        setContentView(PluginLink.getLayoutupomp_lthj_quickpay_nocard());
        this.x = (Button) findViewById(PluginLink.getIdupomp_lthj_next_btn());
        this.x.setOnClickListener(this);
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_username_view())).a(as.a().A.toString());
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_welcome_view())).a(as.a().G);
        this.w = (Button) findViewById(PluginLink.getIdupomp_lthj_account_mange_btn());
        this.w.setOnClickListener(this);
        this.A = (ImageButton) findViewById(PluginLink.getIdupomp_lthj_refrush_btn());
        this.A.setOnClickListener(this);
    }

    private void m() {
        int size = as.a().D.size();
        String[] strArr = new String[size];
        for (int i2 = 0; i2 < size; i2++) {
            GetBundleBankCardList getBundleBankCardList = (GetBundleBankCardList) as.a().D.get(i2);
            strArr[i2] = getBundleBankCardList.panBank + "-" + j.b(getBundleBankCardList.panType) + "-" + getBundleBankCardList.pan.substring(getBundleBankCardList.pan.length() - 4);
        }
        new AlertDialog.Builder(this).setTitle(PluginLink.getStringupomp_lthj_choose_pay_card()).setItems(strArr, new eq(this)).setPositiveButton(getString(PluginLink.getStringupomp_lthj_add_card()), new ds(this)).setNegativeButton(getString(PluginLink.getStringupomp_lthj_close()), new dt(this)).create().show();
    }

    /* access modifiers changed from: private */
    public void n() {
        l.a().a(this, getString(PluginLink.getStringupomp_lthj_logout_prompt()), new eg(this));
    }

    public void backToMerchant() {
        a().backToMerchant();
    }

    public void errorCallBack(String str) {
        if (this.o != null) {
            this.o.setEnabled(true);
            this.o.setText(getString(PluginLink.getStringupomp_lthj_click_get_mac()));
        }
        j.a(this, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View view) {
        int i2 = 0;
        if (view == this.g) {
            h();
        } else if (view == this.w) {
            Intent intent = new Intent(this, AccountActivity.class);
            intent.addFlags(67108864);
            a().changeSubActivity(intent);
        } else if (view == this.J) {
            if (c.c(this, this.G.b()) && c.a(this) && c.a(this, this.M)) {
                f();
            }
        } else if (view == this.p) {
            if (c.f(this, this.q.b()) && c.g(this, this.r.b()) && c.h(this, this.s.b())) {
                this.I.dismiss();
                cp.b(this, this, this.j, this.q.a().toString(), this.r.a().toString(), this.s.a().toString());
            }
        } else if (view == this.v) {
            if (c.a(this, this.k, this.o)) {
                if ("00".equals(this.c)) {
                    if (!c.j(this, this.d) || !c.i(this, this.e)) {
                        return;
                    }
                } else if ("01".equals(this.c) && !c.l(this, this.f)) {
                    return;
                }
                cp.a(this, this, this.j, this.y, this.d.getText().toString());
            }
        } else if (view == this.x) {
            Intent intent2 = new Intent(this, BankCardInfoActivity.class);
            intent2.setFlags(67108864);
            intent2.putExtra("isQuickPayBind", true);
            a().changeSubActivity(intent2);
        } else if (view == this.A) {
            cp.a(this, this);
        } else if (view == this.B) {
            m();
        } else if (view == this.t) {
            Vector h2 = as.a().a.h();
            this.u = new String[(h2.size() + 1)];
            this.u[0] = getString(PluginLink.getStringupomp_lthj_custom_safeask());
            while (true) {
                int i3 = i2;
                if (i3 < h2.size()) {
                    this.u[i3 + 1] = (String) h2.get(i3);
                    i2 = i3 + 1;
                } else {
                    new AlertDialog.Builder(this).setItems(this.u, new bo(this)).setTitle(PluginLink.getStringupomp_lthj_choose_safeask()).show();
                    return;
                }
            }
        } else if (view == this.K || view == this.L) {
            j.c(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (getIntent().getBooleanExtra("isAcount", false)) {
            d();
            return;
        }
        this.c = getIntent().getStringExtra("panType");
        this.i = getIntent().getStringExtra("panBank");
        this.j = getIntent().getStringExtra("mobilenumber");
        if ("02".equals(this.c)) {
            c();
        } else {
            b();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (!(this.h != "2" || this.l == null || this.m == null || as.a().E == null)) {
            this.l.a(as.a().E.panBank + "-" + j.b(as.a().E.panType) + "-" + as.a().E.pan.substring(as.a().E.pan.length() - 4, as.a().E.pan.length()));
            this.m.a(j.f(as.a().E.mobileNumber));
        }
        super.onResume();
    }

    public void processRefreshConn() {
        if (this.aaTimerTask == null) {
            Timer timer = new Timer();
            this.aaTimerTask = new dq(this);
            timer.schedule(this.aaTimerTask, 0, (long) 1000);
        }
    }

    public void responseCallBack(z zVar) {
        if (zVar != null && zVar.n() != null && !isFinishing()) {
            int e2 = zVar.e();
            int parseInt = Integer.parseInt(zVar.n());
            if ("5309".equals(zVar.n())) {
                l.a().a(this, getString(PluginLink.getStringupomp_lthj_multiple_user_login_tip()), new dp(this));
                return;
            }
            switch (e2) {
                case 8200:
                    n nVar = (n) zVar;
                    if (parseInt == 0) {
                        Toast makeText = Toast.makeText(this, getString(PluginLink.getStringupomp_lthj_sendMessageSuccess()), 1);
                        makeText.setGravity(17, 0, 0);
                        makeText.show();
                        String a2 = nVar.a();
                        if (this.n != null) {
                            String str = getString(PluginLink.getStringupomp_lthj_secureinfo()) + "-" + a2;
                            SpannableString spannableString = new SpannableString(str);
                            spannableString.setSpan(new ForegroundColorSpan(-65536), str.length() - 4, str.length(), 33);
                            this.n.a(spannableString);
                        }
                        as.a().y.delete(0, as.a().y.length());
                        as.a().y.append(nVar.c());
                        processRefreshConn();
                        return;
                    }
                    Toast makeText2 = Toast.makeText(this, getString(PluginLink.getStringupomp_lthj_sendMessageLose()) + nVar.o() + ",错误码为：" + parseInt, 1);
                    makeText2.setGravity(17, 0, 0);
                    makeText2.show();
                    this.o.setEnabled(true);
                    this.o.setText(getString(PluginLink.getStringupomp_lthj_click_get_mac()));
                    return;
                case 8207:
                    bz bzVar = (bz) zVar;
                    if (parseInt == 0) {
                        as.a().D = j.a(bzVar);
                        if (as.a().D == null || as.a().D.size() == 0) {
                            l();
                            return;
                        } else {
                            k();
                            return;
                        }
                    } else {
                        j.a(this, bzVar.o(), parseInt);
                        return;
                    }
                case 8210:
                case 8227:
                    ea eaVar = (ea) zVar;
                    as.a().H = eaVar.d();
                    as.a().I = eaVar.c();
                    as.a().J = eaVar.p();
                    as.a().K = eaVar.q();
                    as.a().t = eaVar.n();
                    as.a().u = eaVar.o();
                    switch (parseInt) {
                        case 3010:
                        case 3011:
                        case 3021:
                            if (this.G != null) {
                                this.G.a((CharSequence) null);
                            }
                            j.a(this, eaVar.o(), parseInt);
                            return;
                        default:
                            a(eaVar);
                            return;
                    }
                case 8222:
                    k kVar = (k) zVar;
                    if (parseInt != 0) {
                        backToMerchant();
                        return;
                    } else if ("0".equals(kVar.a())) {
                        e();
                        return;
                    } else {
                        backToMerchant();
                        return;
                    }
                case 8225:
                    a((cu) zVar);
                    return;
                case 8229:
                    a((cd) zVar);
                    return;
                default:
                    return;
            }
        }
    }
}
