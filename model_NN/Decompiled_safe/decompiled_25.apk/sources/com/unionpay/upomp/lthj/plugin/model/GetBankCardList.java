package com.unionpay.upomp.lthj.plugin.model;

public class GetBankCardList extends Data {
    public String panBank;
    public String panBankId;
    public String panType;
}
