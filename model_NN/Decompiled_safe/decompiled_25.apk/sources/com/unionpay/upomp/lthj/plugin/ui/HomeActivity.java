package com.unionpay.upomp.lthj.plugin.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.lthj.unipay.plugin.aa;
import com.lthj.unipay.plugin.as;
import com.lthj.unipay.plugin.aw;
import com.lthj.unipay.plugin.ax;
import com.lthj.unipay.plugin.bf;
import com.lthj.unipay.plugin.bj;
import com.lthj.unipay.plugin.bk;
import com.lthj.unipay.plugin.bl;
import com.lthj.unipay.plugin.bm;
import com.lthj.unipay.plugin.bn;
import com.lthj.unipay.plugin.c;
import com.lthj.unipay.plugin.cg;
import com.lthj.unipay.plugin.ch;
import com.lthj.unipay.plugin.ci;
import com.lthj.unipay.plugin.cj;
import com.lthj.unipay.plugin.ck;
import com.lthj.unipay.plugin.cl;
import com.lthj.unipay.plugin.cm;
import com.lthj.unipay.plugin.cn;
import com.lthj.unipay.plugin.cp;
import com.lthj.unipay.plugin.cq;
import com.lthj.unipay.plugin.cs;
import com.lthj.unipay.plugin.de;
import com.lthj.unipay.plugin.dh;
import com.lthj.unipay.plugin.j;
import com.lthj.unipay.plugin.l;
import com.lthj.unipay.plugin.n;
import com.lthj.unipay.plugin.z;
import com.unionpay.upomp.lthj.link.PluginLink;
import com.unionpay.upomp.lthj.widget.LineFrameView;
import com.unionpay.upomp.lthj.widget.ValidateCodeView;
import java.util.Timer;
import java.util.TimerTask;

public class HomeActivity extends BaseActivity implements View.OnClickListener, UIResponseListener {
    public int a = 60;
    public TimerTask aaTimerTask;
    private final String b = "HomeActivity";
    private Button c;
    private Button d;
    private EditText e;
    private EditText f;
    private Button g;
    private EditText h;
    /* access modifiers changed from: private */
    public EditText i;
    private Button j;
    private ValidateCodeView k;
    private Button l;
    private Button m;
    private EditText n;
    /* access modifiers changed from: private */
    public Button o;
    private EditText p;
    private Button q;
    private View.OnClickListener r = new cs(this);
    /* access modifiers changed from: private */
    public Handler s = new bn(this);
    private StringBuffer t;
    private EditText u;
    private String v;
    /* access modifiers changed from: private */
    public cq w;

    private void a(bf bfVar) {
        a(getString(PluginLink.getStringupomp_lthj_back()), new bl(this));
        setContentView(PluginLink.getLayoutupomp_lthj_findpwd_next());
        this.p = (EditText) findViewById(PluginLink.getIdupomp_lthj_safe_asw_view());
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_username_view())).a(bfVar.a());
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_mobile_number_view())).a(j.f(bfVar.c()));
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_safe_ask_view())).a(bfVar.d());
        this.f = (EditText) findViewById(PluginLink.getIdupomp_lthj_password_edit());
        aa aaVar = new aa(8);
        this.f.setOnTouchListener(aaVar);
        this.f.setOnFocusChangeListener(aaVar);
        this.u = (EditText) findViewById(PluginLink.getIdupomp_lthj_confirm_pwd_input());
        aa aaVar2 = new aa(7);
        this.u.setOnTouchListener(aaVar2);
        this.u.setOnFocusChangeListener(aaVar2);
        this.v = bfVar.a();
        this.q = (Button) findViewById(PluginLink.getIdupomp_lthj_button_ok());
        this.q.setOnClickListener(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void a(cq cqVar, boolean z) {
        if (z && !cqVar.a().equalsIgnoreCase(j.d(this))) {
            j.c(this, cqVar.a());
        }
        as.a().A.setLength(0);
        as.a().A.append(cqVar.a());
        as.a().F = cqVar.d();
        as.a().G = cqVar.p();
        as.a().B.setLength(0);
        as.a().B.append(cqVar.c());
        Intent intent = new Intent(this, PayActivity.class);
        intent.addFlags(67108864);
        intent.putExtra("isAcount", true);
        a().changeSubActivity(intent);
    }

    /* access modifiers changed from: private */
    public void b() {
        setContentView(PluginLink.getLayoutupomp_lthj_homecardpay());
        d();
        if (as.a().b.b()) {
            this.k.setVisibility(0);
            j.a(this.k);
        }
        this.c = (Button) findViewById(PluginLink.getIdupomp_lthj_next_btn());
        this.c.setOnClickListener(this);
        this.h = (EditText) findViewById(PluginLink.getIdupomp_lthj_card_num_edit());
        this.j = (Button) findViewById(PluginLink.getIdupomp_lthj_help_btn());
        this.j.setOnClickListener(this);
        this.i = (EditText) findViewById(PluginLink.getIdupomp_lthj_mobile_num_edit());
        if (as.a().x.length() != 0) {
            this.i.setText(j.f(as.a().x.toString()));
        }
        String e2 = j.e(this);
        if (e2 != null || !PoiTypeDef.All.equals(e2)) {
            as.a().w.setLength(0);
            as.a().w.append(e2);
        }
        if (as.a().w.length() != 0) {
            this.h.setText(j.g(as.a().w.toString()));
        }
        this.h.addTextChangedListener(new ax(this.h));
        ((Button) findViewById(PluginLink.getIdupomp_lthj_use_quick_pay())).setOnClickListener(new ck(this));
    }

    /* access modifiers changed from: private */
    public void c() {
        setContentView(PluginLink.getLayoutupomp_lthj_homeaccountpay());
        d();
        if (as.a().b.a()) {
            this.k.setVisibility(0);
            j.a(this.k);
        }
        this.d = (Button) findViewById(PluginLink.getIdupomp_lthj_next_btn());
        this.d.setOnClickListener(this);
        this.e = (EditText) findViewById(PluginLink.getIdupomp_lthj_username_edit());
        this.e.setText(j.d(this));
        this.f = (EditText) findViewById(PluginLink.getIdupomp_lthj_password_edit());
        aa aaVar = new aa(1);
        this.f.setOnFocusChangeListener(aaVar);
        this.f.setOnTouchListener(aaVar);
        ((Button) findViewById(PluginLink.getIdupomp_lthj_use_card_pay())).setOnClickListener(new cn(this));
        this.l = (Button) findViewById(PluginLink.getIdupomp_lthj_forget_pwd());
        this.l.setOnClickListener(this);
    }

    private void d() {
        this.k = (ValidateCodeView) findViewById(PluginLink.getIdupomp_lthj_validatecode_layout());
        a(getString(PluginLink.getStringupomp_lthj_app_quitNotice_msg()), this.r);
        this.g = (Button) findViewById(PluginLink.getIdupomp_lthj_unfold_btn());
        this.g.setOnClickListener(this);
        ((TextView) findViewById(PluginLink.getIdupomp_lthj_merchant_tv())).setText(as.a().j);
        ((TextView) findViewById(PluginLink.getIdupomp_lthj_orderamt_tv())).setText(j.d(as.a().n));
        ((TextView) findViewById(PluginLink.getIdupomp_lthj_orderno_tv())).setText(as.a().l);
        ((TextView) findViewById(PluginLink.getIdupomp_lthj_ordertime_tv())).setText(j.c(as.a().m));
    }

    /* access modifiers changed from: private */
    public void e() {
        a(getString(PluginLink.getStringupomp_lthj_back()), new cm(this));
        setContentView(PluginLink.getLayoutupomp_lthj_findpwd_home());
        this.k = (ValidateCodeView) findViewById(PluginLink.getIdupomp_lthj_validatecode_layout());
        if (as.a().b.e()) {
            this.k.setVisibility(0);
            j.a(this.k);
        }
        this.e = (EditText) findViewById(PluginLink.getIdupomp_lthj_username_edit());
        this.e.setText(j.d(this));
        this.i = (EditText) findViewById(PluginLink.getIdupomp_lthj_mobile_num_edit());
        this.m = (Button) findViewById(PluginLink.getIdupomp_lthj_next_btn());
        this.m.setOnClickListener(this);
        this.n = (EditText) findViewById(PluginLink.getIdupomp_lthj_mobilemac_edit());
        this.o = (Button) findViewById(PluginLink.getIdupomp_lthj_get_mac_btn());
        this.o.setOnClickListener(new ch(this));
    }

    public void errorCallBack(String str) {
        j.b(this, str);
    }

    public void onClick(View view) {
        if (view == this.c) {
            if (c.b(this, this.h, as.a().w.toString()) && c.a(this, this.i, as.a().x.toString()) && c.a(this, this.k)) {
                if (this.i.getText().toString().contains("*")) {
                    aw.a("HomeActivity", "mobile has ****");
                } else {
                    as.a().x.setLength(0);
                    as.a().x.append(this.i.getText().toString());
                }
                this.t = new StringBuffer();
                if (this.h.getText().toString().contains("*")) {
                    this.t.append(as.a().w);
                } else {
                    this.t.delete(0, this.t.length());
                    this.t.append(this.h.getText().toString());
                    for (int i2 = 0; i2 < this.t.length(); i2++) {
                        if (this.t.charAt(i2) == ' ') {
                            this.t.deleteCharAt(i2);
                        }
                    }
                }
                new de().a(this, this.t.toString(), this.k.d().toString(), true, this);
            }
        } else if (view == this.d) {
            if (c.b(this, this.e) && c.c(this, this.f) && c.a(this, this.k)) {
                cp.d(this, this, this.e.getText().toString(), this.k.d());
                this.f.setText(PoiTypeDef.All);
            }
        } else if (view == this.g) {
            View findViewById = findViewById(PluginLink.getIdupomp_lthj_orderno_row());
            View findViewById2 = findViewById(PluginLink.getIdupomp_lthj_trantime_row());
            if (findViewById.getVisibility() == 8) {
                findViewById.setVisibility(0);
                findViewById2.setVisibility(0);
                view.setBackgroundResource(PluginLink.getDrawableupomp_lthj_info_up_btn());
                return;
            }
            findViewById.setVisibility(8);
            findViewById2.setVisibility(8);
            view.setBackgroundResource(PluginLink.getDrawableupomp_lthj_info_down_btn());
        } else if (view == this.j) {
            if (!this.h.getText().toString().contains("*") && this.h.getText().length() >= 19) {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append(this.h.getText().toString());
                for (int i3 = 0; i3 < stringBuffer.length(); i3++) {
                    if (stringBuffer.charAt(i3) == ' ') {
                        stringBuffer.deleteCharAt(i3);
                    }
                }
                as.a().w.setLength(0);
                as.a().w.append(stringBuffer);
                stringBuffer.setLength(0);
            }
            Intent intent = new Intent(this, SupportCardActivity.class);
            intent.putExtra("tranType", "1");
            intent.addFlags(67108864);
            a().changeSubActivity(intent);
        } else if (view == this.l) {
            e();
        } else if (view == this.m) {
            if (c.b(this, this.e) && c.a(this, this.i) && c.a(this, this.n, this.o) && c.a(this, this.k)) {
                cp.a(this, this, this.e.getText().toString(), this.i.getText().toString(), this.n.getText().toString(), this.k.d());
            }
        } else if (view == this.q && c.h(this, this.p) && c.c(this, this.f) && c.c(this, this.u) && c.a(this)) {
            cp.a(this, this, this.v, this.p.getText().toString());
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a(getString(PluginLink.getStringupomp_lthj_backtomerchant()), new cl(this));
        if (as.a().h) {
            c();
        } else {
            b();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (as.a().x.length() != 0) {
            this.i.setText(j.f(as.a().x.toString()));
        }
        if (as.a().w.length() != 0) {
            this.h.setText(j.g(as.a().w.toString()));
        }
        a().aboutEnable(true);
    }

    public void processRefreshConn() {
        if (this.aaTimerTask == null) {
            Timer timer = new Timer();
            this.aaTimerTask = new bm(this);
            timer.schedule(this.aaTimerTask, 0, (long) 1000);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.unionpay.upomp.lthj.plugin.ui.HomeActivity.a(com.lthj.unipay.plugin.cq, boolean):void
     arg types: [com.lthj.unipay.plugin.cq, int]
     candidates:
      com.unionpay.upomp.lthj.plugin.ui.BaseActivity.a(java.lang.String, android.view.View$OnClickListener):void
      com.unionpay.upomp.lthj.plugin.ui.HomeActivity.a(com.lthj.unipay.plugin.cq, boolean):void */
    public void responseCallBack(z zVar) {
        if (zVar != null && zVar.n() != null && !isFinishing()) {
            int e2 = zVar.e();
            int parseInt = Integer.parseInt(zVar.n());
            switch (e2) {
                case FragmentTransaction.TRANSIT_FRAGMENT_CLOSE:
                    this.w = (cq) zVar;
                    if (parseInt != 0) {
                        as.a().b.a(true);
                        j.a(this, this.w.o(), parseInt);
                        break;
                    } else {
                        as.a().b.a(false);
                        if (!j.d(this).equals(this.w.a())) {
                            l.a().a(this, getString(PluginLink.getStringupomp_lthj_save_account()), getString(PluginLink.getStringupomp_lthj_ok()), getString(PluginLink.getStringupomp_lthj_no_save()), new ci(this), new bj(this));
                            break;
                        } else {
                            a(this.w, false);
                            break;
                        }
                    }
                case 8195:
                    if (parseInt == 0) {
                        l.a().a(this, getString(PluginLink.getStringupomp_lthj_ok()), getString(PluginLink.getStringupomp_lthj_change_pwd_success()), new bk(this));
                    } else {
                        j.a(this, zVar.o(), parseInt);
                    }
                    as.a().d.d.setLength(0);
                    break;
                case 8200:
                    n nVar = (n) zVar;
                    if (parseInt != 0) {
                        Toast makeText = Toast.makeText(this, getString(PluginLink.getStringupomp_lthj_sendMessageLose()) + nVar.o() + ",错误码为：" + parseInt, 1);
                        makeText.setGravity(17, 0, 0);
                        makeText.show();
                        this.o.setEnabled(true);
                        this.o.setText(getString(PluginLink.getStringupomp_lthj_click_get_mac()));
                        break;
                    } else {
                        Toast makeText2 = Toast.makeText(this, getString(PluginLink.getStringupomp_lthj_sendMessageSuccess()), 1);
                        makeText2.setGravity(17, 0, 0);
                        makeText2.show();
                        as.a().y.delete(0, as.a().y.length());
                        as.a().y.append(nVar.c());
                        processRefreshConn();
                        break;
                    }
                case 8221:
                    bf bfVar = (bf) zVar;
                    if (parseInt != 0) {
                        as.a().b.e(true);
                        j.a(this, zVar.o(), parseInt);
                        break;
                    } else {
                        as.a().b.e(false);
                        a(bfVar);
                        break;
                    }
                case 8224:
                    dh dhVar = (dh) zVar;
                    if (parseInt != 0) {
                        as.a().b.b(true);
                        if (!"5110".equals(zVar.n())) {
                            j.a(this, zVar.o(), parseInt);
                            break;
                        } else {
                            l.a().a(this, j.b(this, zVar.o(), parseInt), getString(PluginLink.getStringupomp_lthj_ok()), getString(PluginLink.getStringupomp_lthj_check_support()), new cg(this), new cj(this));
                            break;
                        }
                    } else {
                        l.a().b();
                        as.a().w.setLength(0);
                        as.a().w.append(this.t.toString());
                        as.a().d.a.setLength(0);
                        as.a().d.a.append(this.t.toString());
                        as.a().b.b(false);
                        Intent intent = new Intent(this, PayActivity.class);
                        intent.setFlags(67108864);
                        intent.putExtra("panType", dhVar.a());
                        intent.putExtra("panBank", dhVar.c());
                        intent.putExtra("mobilenumber", as.a().x.toString());
                        a().changeSubActivity(intent);
                        break;
                    }
            }
            if (zVar.e() != 8200) {
                j.b(this.k);
            }
        }
    }
}
