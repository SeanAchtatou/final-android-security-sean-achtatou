package com.unionpay.upomp.lthj.plugin.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.MotionEventCompat;
import android.text.Editable;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.lthj.unipay.plugin.as;
import com.lthj.unipay.plugin.ax;
import com.lthj.unipay.plugin.j;
import com.unionpay.upomp.lthj.link.PluginLink;
import java.util.Date;
import java.util.Random;

public class KeyboardDialog extends Dialog implements View.OnClickListener, View.OnTouchListener, CompoundButton.OnCheckedChangeListener {
    private Button[] A;
    private int B;
    private Context a;
    private WindowManager b;
    private EditText c;
    private EditText d;
    private Button e;
    private Button f;
    private RadioButton g;
    private RadioButton h;
    private RadioButton i;
    private Button j;
    private View k;
    private View l;
    private View m;
    private Button[] n;
    private Button[] o;
    private int[] p = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    private Button q;
    private boolean r;
    private LinearLayout s;
    private PopupWindow t;
    private int u;
    private View v;
    private int w;
    private int x;
    private Object y;
    private String[] z;

    public KeyboardDialog(Context context) {
        super(context);
        this.b = (WindowManager) context.getSystemService("window");
        this.a = context;
    }

    public KeyboardDialog(Context context, int i2) {
        super(context, i2);
        this.b = (WindowManager) context.getSystemService("window");
        this.a = context;
    }

    public KeyboardDialog(Context context, boolean z2, DialogInterface.OnCancelListener onCancelListener) {
        super(context, z2, onCancelListener);
        this.b = (WindowManager) context.getSystemService("window");
        this.a = context;
    }

    private void a() {
        if (this.k != null) {
            this.e = (Button) this.k.findViewById(PluginLink.getIdupomp_lthj_keyboard_buttonC());
            this.e.setOnClickListener(this);
            this.e.setOnTouchListener(this);
            this.n = new Button[10];
            Button button = (Button) this.k.findViewById(PluginLink.getIdupomp_lthj_keyboard_button1());
            button.setOnClickListener(this);
            button.setOnTouchListener(this);
            this.n[0] = button;
            Button button2 = (Button) this.k.findViewById(PluginLink.getIdupomp_lthj_keyboard_button2());
            button2.setOnClickListener(this);
            button2.setOnTouchListener(this);
            this.n[1] = button2;
            Button button3 = (Button) this.k.findViewById(PluginLink.getIdupomp_lthj_keyboard_button3());
            button3.setOnClickListener(this);
            button3.setOnTouchListener(this);
            this.n[2] = button3;
            Button button4 = (Button) this.k.findViewById(PluginLink.getIdupomp_lthj_keyboard_button4());
            button4.setOnClickListener(this);
            button4.setOnTouchListener(this);
            this.n[3] = button4;
            Button button5 = (Button) this.k.findViewById(PluginLink.getIdupomp_lthj_keyboard_button5());
            button5.setOnClickListener(this);
            button5.setOnTouchListener(this);
            this.n[4] = button5;
            Button button6 = (Button) this.k.findViewById(PluginLink.getIdupomp_lthj_keyboard_button6());
            button6.setOnClickListener(this);
            button6.setOnTouchListener(this);
            this.n[5] = button6;
            Button button7 = (Button) this.k.findViewById(PluginLink.getIdupomp_lthj_keyboard_button7());
            button7.setOnClickListener(this);
            button7.setOnTouchListener(this);
            this.n[6] = button7;
            Button button8 = (Button) this.k.findViewById(PluginLink.getIdupomp_lthj_keyboard_button8());
            button8.setOnClickListener(this);
            button8.setOnTouchListener(this);
            this.n[7] = button8;
            Button button9 = (Button) this.k.findViewById(PluginLink.getIdupomp_lthj_keyboard_button9());
            button9.setOnClickListener(this);
            button9.setOnTouchListener(this);
            this.n[8] = button9;
            Button button10 = (Button) this.k.findViewById(PluginLink.getIdupomp_lthj_keyboard_button0());
            button10.setOnClickListener(this);
            button10.setOnTouchListener(this);
            this.n[9] = button10;
            if (!(this.u == 5 || this.u == 6 || this.u == 4)) {
                a(this.p);
            }
            if (this.p != null && this.p.length == 10) {
                for (int i2 = 0; i2 < this.p.length; i2++) {
                    this.n[i2].setText(this.p[i2] + PoiTypeDef.All);
                }
            }
        }
    }

    private void a(char c2) {
        int i2;
        int i3;
        int selectionStart = this.d.getSelectionStart();
        int selectionEnd = this.d.getSelectionEnd();
        if (selectionStart > selectionEnd) {
            i2 = selectionEnd;
        } else {
            i2 = selectionStart;
            selectionStart = selectionEnd;
        }
        Editable text = this.d.getText();
        if (c2 == 8) {
            if (i2 == selectionStart && i2 > 0) {
                i2--;
            }
            if (i2 + selectionStart != 0) {
                text.delete(i2, selectionStart);
                i3 = i2;
            } else {
                return;
            }
        } else if (this.u == 0 && text.length() >= 6) {
            return;
        } else {
            if (this.u == 2 && text.length() >= 3) {
                return;
            }
            if (this.u == 5 && text.length() >= 11) {
                return;
            }
            if (this.u == 6 && text.length() >= 6) {
                return;
            }
            if (this.u != 3 || text.length() < 6) {
                if (this.u == 4) {
                    if (this.d.getText().length() > 22) {
                        return;
                    }
                } else if (this.d.getText().length() > 11) {
                    return;
                }
                text.replace(i2, selectionStart, String.valueOf(c2), 0, 1);
                i3 = i2 + 1;
            } else {
                return;
            }
        }
        if (this.u != 4) {
            this.d.setSelection(i3);
        }
    }

    private void a(int i2) {
        for (int i3 = 0; i3 <= 10; i3++) {
            this.A[i3].setText(this.z[(this.A.length * i2) + i3]);
        }
    }

    private void a(boolean z2) {
        if (this.l != null) {
            if (this.o == null) {
                this.e = (Button) this.l.findViewById(PluginLink.getIdupomp_lthj_keyboard_buttonC());
                this.e.setOnClickListener(this);
                this.e.setOnTouchListener(this);
                this.o = new Button[26];
                Button button = (Button) this.l.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_a());
                button.setOnClickListener(this);
                button.setOnTouchListener(this);
                this.o[0] = button;
                Button button2 = (Button) this.l.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_b());
                button2.setOnClickListener(this);
                button2.setOnTouchListener(this);
                this.o[1] = button2;
                Button button3 = (Button) this.l.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_c());
                button3.setOnClickListener(this);
                button3.setOnTouchListener(this);
                this.o[2] = button3;
                Button button4 = (Button) this.l.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_d());
                button4.setOnClickListener(this);
                button4.setOnTouchListener(this);
                this.o[3] = button4;
                Button button5 = (Button) this.l.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_e());
                button5.setOnClickListener(this);
                button5.setOnTouchListener(this);
                this.o[4] = button5;
                Button button6 = (Button) this.l.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_f());
                button6.setOnClickListener(this);
                button6.setOnTouchListener(this);
                this.o[5] = button6;
                Button button7 = (Button) this.l.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_g());
                button7.setOnClickListener(this);
                button7.setOnTouchListener(this);
                this.o[6] = button7;
                Button button8 = (Button) this.l.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_h());
                button8.setOnClickListener(this);
                button8.setOnTouchListener(this);
                this.o[7] = button8;
                Button button9 = (Button) this.l.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_i());
                button9.setOnClickListener(this);
                button9.setOnTouchListener(this);
                this.o[8] = button9;
                Button button10 = (Button) this.l.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_j());
                button10.setOnClickListener(this);
                button10.setOnTouchListener(this);
                this.o[9] = button10;
                Button button11 = (Button) this.l.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_k());
                button11.setOnClickListener(this);
                button11.setOnTouchListener(this);
                this.o[10] = button11;
                Button button12 = (Button) this.l.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_l());
                button12.setOnClickListener(this);
                button12.setOnTouchListener(this);
                this.o[11] = button12;
                Button button13 = (Button) this.l.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_m());
                button13.setOnClickListener(this);
                button13.setOnTouchListener(this);
                this.o[12] = button13;
                Button button14 = (Button) this.l.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_n());
                button14.setOnClickListener(this);
                button14.setOnTouchListener(this);
                this.o[13] = button14;
                Button button15 = (Button) this.l.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_o());
                button15.setOnClickListener(this);
                button15.setOnTouchListener(this);
                this.o[14] = button15;
                Button button16 = (Button) this.l.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_p());
                button16.setOnClickListener(this);
                button16.setOnTouchListener(this);
                this.o[15] = button16;
                Button button17 = (Button) this.l.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_q());
                button17.setOnClickListener(this);
                button17.setOnTouchListener(this);
                this.o[16] = button17;
                Button button18 = (Button) this.l.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_r());
                button18.setOnClickListener(this);
                button18.setOnTouchListener(this);
                this.o[17] = button18;
                Button button19 = (Button) this.l.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_s());
                button19.setOnClickListener(this);
                button19.setOnTouchListener(this);
                this.o[18] = button19;
                Button button20 = (Button) this.l.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_t());
                button20.setOnClickListener(this);
                button20.setOnTouchListener(this);
                this.o[19] = button20;
                Button button21 = (Button) this.l.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_u());
                button21.setOnClickListener(this);
                button21.setOnTouchListener(this);
                this.o[20] = button21;
                Button button22 = (Button) this.l.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_v());
                button22.setOnClickListener(this);
                button22.setOnTouchListener(this);
                this.o[21] = button22;
                Button button23 = (Button) this.l.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_w());
                button23.setOnClickListener(this);
                button23.setOnTouchListener(this);
                this.o[22] = button23;
                Button button24 = (Button) this.l.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_x());
                button24.setOnClickListener(this);
                button24.setOnTouchListener(this);
                this.o[23] = button24;
                Button button25 = (Button) this.l.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_y());
                button25.setOnClickListener(this);
                button25.setOnTouchListener(this);
                this.o[24] = button25;
                Button button26 = (Button) this.l.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_z());
                button26.setOnClickListener(this);
                button26.setOnTouchListener(this);
                this.o[25] = button26;
            }
            for (int i2 = 0; i2 < this.o.length; i2++) {
                if (z2) {
                    this.o[i2].setText(((char) (i2 + 97)) + PoiTypeDef.All);
                } else {
                    this.o[i2].setText(((char) (i2 + 65)) + PoiTypeDef.All);
                }
            }
        }
    }

    private void a(int[] iArr) {
        int length = iArr.length;
        int[] a2 = a(1, length * length * length, length);
        for (int i2 = length - 1; i2 > 0; i2--) {
            for (int i3 = 0; i3 < i2; i3++) {
                if (a2[i3] > a2[i3 + 1]) {
                    int i4 = iArr[i3];
                    iArr[i3] = iArr[i3 + 1];
                    iArr[i3 + 1] = i4;
                    int i5 = a2[i3];
                    a2[i3] = a2[i3 + 1];
                    a2[i3 + 1] = i5;
                }
            }
        }
    }

    private int[] a(int i2, int i3, int i4) {
        if (i2 <= i3) {
            int i5 = i3;
            i3 = i2;
            i2 = i5;
        }
        Random random = new Random((long) new Date().getSeconds());
        int[] iArr = new int[i4];
        for (int i6 = 0; i6 < i4; i6++) {
            iArr[i6] = ((int) (random.nextDouble() * ((double) (Math.abs(i2 - i3) + 1)))) + i3;
        }
        return iArr;
    }

    private void b() {
        if (this.k != null) {
            this.e = (Button) this.m.findViewById(PluginLink.getIdupomp_lthj_keyboard_buttonC());
            this.e.setOnClickListener(this);
            this.e.setOnTouchListener(this);
            this.z = new String[]{"!", "@", "#", "$", "%", "&", "*", "+", "-", "_", "下一页", "/", "?", ":", ";", ",", ".", "'", "\"", "^", "上一页", "下一页", "[", "]", "{", "}", "=", "(", ")", "\\", "~", "上一页", "下一页", "|", "<", ">", "€", "￥", "￡", "《", "》", "。", "…", "上一页"};
            this.A = new Button[11];
            this.A[0] = (Button) this.m.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_sign1());
            this.A[1] = (Button) this.m.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_sign2());
            this.A[2] = (Button) this.m.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_sign3());
            this.A[3] = (Button) this.m.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_sign4());
            this.A[4] = (Button) this.m.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_sign5());
            this.A[5] = (Button) this.m.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_sign6());
            this.A[6] = (Button) this.m.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_sign7());
            this.A[7] = (Button) this.m.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_sign8());
            this.A[8] = (Button) this.m.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_sign9());
            this.A[9] = (Button) this.m.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_sign10());
            this.A[10] = (Button) this.m.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_signnext());
            for (int i2 = 0; i2 < this.A.length; i2++) {
                this.A[i2].setOnClickListener(this);
                this.A[i2].setOnTouchListener(this);
            }
            this.B = 0;
            a(this.B);
        }
    }

    public int getType() {
        return this.u;
    }

    public void onCheckedChanged(CompoundButton compoundButton, boolean z2) {
        if (!z2) {
            return;
        }
        if (compoundButton == this.h) {
            this.s.removeAllViews();
            if (this.l == null) {
                this.l = View.inflate(this.a, PluginLink.getLayoutupomp_lthj_keyboard_letter(), null);
                this.r = true;
                a(this.r);
                this.l.setLayoutParams(new WindowManager.LayoutParams(-1, -1));
                this.q = (Button) this.l.findViewById(PluginLink.getIdupomp_lthj_keyboard_button_shift());
                this.q.setOnTouchListener(this);
                this.q.setOnClickListener(this);
            }
            this.s.addView(this.l);
        } else if (compoundButton == this.g) {
            this.s.removeAllViews();
            if (this.k == null) {
                this.k = View.inflate(this.a, PluginLink.getLayoutupomp_lthj_keyboard_num(), null);
                a();
                this.k.setLayoutParams(new WindowManager.LayoutParams(-1, -1));
            }
            this.s.addView(this.k);
        } else if (compoundButton == this.i) {
            this.s.removeAllViews();
            if (this.m == null) {
                this.m = View.inflate(this.a, PluginLink.getLayoutupomp_lthj_keyboard_sign(), null);
                b();
                this.m.setLayoutParams(new WindowManager.LayoutParams(-1, -1));
            }
            this.s.addView(this.m);
        }
    }

    public void onClick(View view) {
        boolean z2 = true;
        if (view.getId() == PluginLink.getIdupomp_lthj_button_cancel()) {
            this.c.setClickable(true);
            dismiss();
            this.d.setText(PoiTypeDef.All);
            this.d = null;
        } else if (view == this.q) {
            if (this.r) {
                z2 = false;
            }
            this.r = z2;
            if (this.r) {
                this.q.setText("shift");
            } else {
                this.q.setText("SHIFT");
            }
            a(this.r);
        } else if (view.getId() == PluginLink.getIdupomp_lthj_keyboard_buttonC()) {
            a(8);
        } else if (view != this.f) {
            Button button = (Button) view;
            if (button.getText().equals(this.a.getString(PluginLink.getStringupomp_lthj_nextpage()))) {
                int i2 = this.B + 1;
                this.B = i2;
                a(i2);
            } else if (button.getText().equals(this.a.getString(PluginLink.getStringupomp_lthj_backpage()))) {
                int i3 = this.B - 1;
                this.B = i3;
                a(i3);
            } else {
                a(button.getText().charAt(0));
            }
        } else if (this.u == 0 && this.d.getText().length() != 6) {
            j.b(this.a, this.a.getString(PluginLink.getStringupomp_lthj_validateBankPassWord_Length_prompt()));
        } else if (this.u == 2 && this.d.getText().length() != 3) {
            j.b(this.a, this.a.getString(PluginLink.getStringupomp_lthj_validateCVN2_Format_prompt()));
        } else if (this.u == 1 && this.d.getText().length() < 6) {
            j.b(this.a, this.a.getString(PluginLink.getStringupomp_lthj_validatePassWord_Length_prompt()));
        } else if (this.u == 9 && this.d.getText().length() < 6) {
            j.b(this.a, this.a.getString(PluginLink.getStringupomp_lthj_validatePassWord_Length_prompt()));
        } else if (this.u == 7 && this.d.getText().length() < 6) {
            j.b(this.a, this.a.getString(PluginLink.getStringupomp_lthj_validatePassWord_Length_prompt()));
        } else if (this.u == 8 && this.d.getText().length() < 6) {
            j.b(this.a, this.a.getString(PluginLink.getStringupomp_lthj_validatePassWord_Length_prompt()));
        } else if (this.u == 5 && this.d.getText().length() < 11) {
            j.b(this.a, this.a.getString(PluginLink.getStringupomp_lthj_validateNum_Length_prompt()));
        } else if (this.u != 6 || this.d.getText().length() >= 6) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(this.d.getText().toString());
            if (this.u == 5 || this.u == 6 || this.u == 4) {
                this.c.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            } else {
                this.c.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
            switch (this.u) {
                case 0:
                    as.a().z.delete(0, as.a().z.length());
                    as.a().z.append(new String(JniMethod.getJniMethod().encryptPinBlock(as.a().d.a.toString().getBytes(), as.a().d.a.toString().getBytes().length, stringBuffer.toString().getBytes(), stringBuffer.toString().getBytes().length)));
                    for (int i4 = 1; i4 <= stringBuffer.length(); i4++) {
                        stringBuffer.replace(i4 - 1, i4, "*");
                    }
                    break;
                case 1:
                    byte[] encryptPasswdLogin = JniMethod.getJniMethod().encryptPasswdLogin(stringBuffer.toString().getBytes(), stringBuffer.toString().length());
                    as.a().C.setLength(0);
                    as.a().C.append(new String(encryptPasswdLogin));
                    for (int i5 = 1; i5 <= stringBuffer.length(); i5++) {
                        stringBuffer.replace(i5 - 1, i5, "*");
                    }
                    break;
                case 7:
                    if (!stringBuffer.toString().matches("^(\\d+)|([A-Za-z]+)$")) {
                        byte[] encryptPasswdLogin2 = JniMethod.getJniMethod().encryptPasswdLogin(stringBuffer.toString().getBytes(), stringBuffer.toString().length());
                        as.a().d.e.setLength(0);
                        as.a().d.e.append(new String(encryptPasswdLogin2));
                        for (int i6 = 1; i6 <= stringBuffer.length(); i6++) {
                            stringBuffer.replace(i6 - 1, i6, "*");
                        }
                        break;
                    } else {
                        j.b(getContext(), getContext().getString(PluginLink.getStringupomp_lthj_validatePassword_complex_prompt_kind()));
                        return;
                    }
                case 8:
                    if (!stringBuffer.toString().matches("^(\\d+)|([A-Za-z]+)$")) {
                        byte[] encryptPasswdLogin3 = JniMethod.getJniMethod().encryptPasswdLogin(stringBuffer.toString().getBytes(), stringBuffer.toString().length());
                        as.a().d.d.setLength(0);
                        as.a().d.d.append(new String(encryptPasswdLogin3));
                        for (int i7 = 1; i7 <= stringBuffer.length(); i7++) {
                            stringBuffer.replace(i7 - 1, i7, "*");
                        }
                        break;
                    } else {
                        j.b(getContext(), getContext().getString(PluginLink.getStringupomp_lthj_validatePassword_complex_prompt_kind()));
                        return;
                    }
                case MotionEventCompat.ACTION_HOVER_ENTER:
                    byte[] encryptPasswdLogin4 = JniMethod.getJniMethod().encryptPasswdLogin(stringBuffer.toString().getBytes(), stringBuffer.toString().length());
                    as.a().d.c.setLength(0);
                    as.a().d.c.append(new String(encryptPasswdLogin4));
                    for (int i8 = 1; i8 <= stringBuffer.length(); i8++) {
                        stringBuffer.replace(i8 - 1, i8, "*");
                    }
                    break;
            }
            dismiss();
            this.c.setText(stringBuffer.toString());
            if (stringBuffer != null && stringBuffer.toString().length() > 0) {
                this.c.setSelection(stringBuffer.length());
            }
            this.c.setClickable(true);
            this.c.dispatchKeyEvent(new KeyEvent(1, 66));
            stringBuffer.delete(0, stringBuffer.length());
            this.d.setText(PoiTypeDef.All);
            this.d = null;
        } else {
            j.b(this.a, this.a.getString(PluginLink.getStringupomp_lthj_mobileMacError_Length_prompt()));
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(PluginLink.getLayoutupomp_lthj_keyboard());
        TextView textView = (TextView) findViewById(PluginLink.getIdupomp_lthj_keyboardtitle());
        switch (this.u) {
            case 2:
                textView.setText(PluginLink.getStringupomp_lthj_cvn());
                break;
            case 7:
                textView.setText(PluginLink.getStringupomp_lthj_confirm_pwd());
                break;
            default:
                textView.setText(PluginLink.getStringupomp_lthj_password());
                break;
        }
        this.y = this.c.getTag();
        Display defaultDisplay = this.b.getDefaultDisplay();
        WindowManager.LayoutParams attributes = getWindow().getAttributes();
        attributes.height = (int) (((double) defaultDisplay.getHeight()) * 0.5d);
        attributes.width = defaultDisplay.getWidth();
        attributes.x = 0;
        attributes.y = defaultDisplay.getHeight() - attributes.height;
        getWindow().setAttributes(attributes);
        this.d = (EditText) findViewById(PluginLink.getIdupomp_lthj_keyboard_editText());
        this.d.setText(this.c.getText().toString());
        if (this.c.getHint() != null) {
            this.d.setHint(this.c.getHint());
        }
        if (this.d.getText() != null && this.d.getText().length() > 0) {
            this.d.setSelection(this.d.getText().length());
        }
        if (this.u == 5 || this.u == 6 || this.u == 4) {
            this.d.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else {
            this.d.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
        this.v = findViewById(PluginLink.getIdupomp_lthj_keyboard_view());
        this.j = (Button) findViewById(PluginLink.getIdupomp_lthj_button_cancel());
        this.j.setOnClickListener(this);
        this.j.setOnTouchListener(this);
        this.s = (LinearLayout) findViewById(PluginLink.getIdupomp_lthj_keyboard_layoutRight());
        this.s.removeAllViews();
        this.f = (Button) findViewById(PluginLink.getIdupomp_lthj_keyboard_buttonOK());
        this.f.setOnClickListener(this);
        this.f.setOnTouchListener(this);
        this.g = (RadioButton) findViewById(PluginLink.getIdupomp_lthj_keyboardButtonNum());
        this.h = (RadioButton) findViewById(PluginLink.getIdupomp_lthj_keyboard_buttonLetter());
        this.i = (RadioButton) findViewById(PluginLink.getIdupomp_lthj_keyboard_buttonSign());
        this.g.setChecked(true);
        this.g.setOnCheckedChangeListener(this);
        this.h.setOnCheckedChangeListener(this);
        this.i.setOnCheckedChangeListener(this);
        if (this.u == 4) {
            this.d.addTextChangedListener(new ax(this.d));
        }
        if (this.u == 0 || this.u == 2 || this.u == 3 || this.u == 5 || this.u == 6 || this.u == 4) {
            this.h.setEnabled(false);
            this.i.setEnabled(false);
        }
        if (this.k == null) {
            this.k = View.inflate(this.a, PluginLink.getLayoutupomp_lthj_keyboard_num(), null);
            a();
            this.k.setLayoutParams(new WindowManager.LayoutParams(-1, -1));
            this.s.addView(this.k);
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        this.c.setClickable(true);
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        this.c.setError(null);
        super.onStop();
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        this.w = (int) (((double) this.i.getWidth()) * 1.2d);
        this.x = (int) (((double) this.i.getHeight()) * 1.2d);
        Button button = (Button) view;
        if (button.getText().toString().length() <= 1) {
            int width = button.getWidth();
            int i2 = (int) (((double) width) * 1.2d);
            int height = (int) (((double) button.getHeight()) * 1.2d);
            if (i2 < this.w) {
                i2 = this.w;
            }
            if (height < this.x) {
                height = this.x;
            }
            float textSize = button.getTextSize() * 1.3f;
            int[] iArr = new int[2];
            button.getLocationInWindow(iArr);
            int i3 = iArr[0] - ((i2 - width) / 2);
            int i4 = iArr[1];
            if (motionEvent.getAction() == 0) {
                Button button2 = new Button(this.a);
                button2.setWidth(i2);
                button2.setHeight(height);
                button2.setGravity(17);
                button2.setText(button.getText().toString());
                button2.setBackgroundResource(PluginLink.getDrawableupomp_lthj_keybtn_enlarge());
                if (button2.getText().toString().length() < 2) {
                    button2.setTextSize(textSize);
                    button2.setTypeface(Typeface.DEFAULT_BOLD);
                    this.t = new PopupWindow(button2, -2, -2);
                    this.t.showAtLocation(this.v, 51, i3, i4 - ((int) (((double) height) * 1.3d)));
                }
            } else if (1 == motionEvent.getAction() && this.t != null) {
                this.t.dismiss();
            }
        }
        return false;
    }

    public void setInputText(EditText editText) {
        this.c = editText;
    }

    public void setType(int i2) {
        this.u = i2;
    }
}
