package com.unionpay.upomp.lthj.plugin.model;

public class BankCardBundle extends Data {
    public String cvn2;
    public String isDefault;
    public String loginName;
    public String mobileMac;
    public String mobileNumber;
    public String pan;
    public String panBankId;
    public String panDate;
    public String panType;
    public String password;
    public String pin;
    public String validateCode;
}
