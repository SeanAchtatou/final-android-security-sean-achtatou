package com.unionpay.upomp.lthj.plugin.model;

public class PanBank {
    private String a;
    private String b;
    private String c;

    public String getPanBank() {
        return this.b;
    }

    public String getPanBankId() {
        return this.a;
    }

    public String getPanType() {
        return this.c;
    }

    public void setPanBank(String str) {
        this.b = str;
    }

    public void setPanBankId(String str) {
        this.a = str;
    }

    public void setPanType(String str) {
        this.c = str;
    }
}
