package com.unionpay.upomp.lthj.plugin.ui;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import com.lthj.unipay.plugin.ar;
import com.lthj.unipay.plugin.cp;
import com.lthj.unipay.plugin.e;
import com.lthj.unipay.plugin.ee;
import com.lthj.unipay.plugin.j;
import com.lthj.unipay.plugin.o;
import com.lthj.unipay.plugin.x;
import com.lthj.unipay.plugin.y;
import com.lthj.unipay.plugin.z;
import com.unionpay.upomp.lthj.link.PluginLink;
import com.unionpay.upomp.lthj.plugin.model.GetBankService;
import com.unionpay.upomp.lthj.plugin.model.PanBank;
import java.util.Vector;

public class SupportCardActivity extends BaseActivity implements View.OnClickListener, AbsListView.OnScrollListener, UIResponseListener {
    private ListView a;
    private ListView b;
    private RelativeLayout c;
    private RelativeLayout d;
    /* access modifiers changed from: private */
    public Vector e;
    /* access modifiers changed from: private */
    public Vector f;
    /* access modifiers changed from: private */
    public int g = 15;
    private ee h;
    private View.OnClickListener i = new x(this);
    private View.OnClickListener j = new y(this);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.lthj.unipay.plugin.j.a(java.lang.String, char):java.lang.String[]
     arg types: [java.lang.String, int]
     candidates:
      com.lthj.unipay.plugin.j.a(android.content.Context, java.lang.String):void
      com.lthj.unipay.plugin.j.a(android.content.Context, boolean):boolean
      com.lthj.unipay.plugin.j.a(java.lang.String, char):java.lang.String[] */
    private Vector a(ar arVar) {
        String[] a2 = j.a(arVar.a(), '|');
        String[] a3 = j.a(arVar.c(), '|');
        String[] a4 = j.a(arVar.d(), '|');
        String[] a5 = j.a(arVar.p(), '|');
        String[] a6 = j.a(arVar.p(), '|');
        if (a2 == null || a3 == null || a4 == null || a5 == null || a6 == null) {
            return null;
        }
        Vector vector = new Vector();
        for (int i2 = 0; i2 < a2.length; i2++) {
            GetBankService getBankService = new GetBankService();
            getBankService.panBank = a2[i2];
            getBankService.panBankId = a3[i2];
            getBankService.creditCard = a4[i2];
            getBankService.debitCard = a5[i2];
            getBankService.payTips = a6[i2];
            vector.add(getBankService);
        }
        return vector;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.lthj.unipay.plugin.j.a(java.lang.String, char):java.lang.String[]
     arg types: [java.lang.String, int]
     candidates:
      com.lthj.unipay.plugin.j.a(android.content.Context, java.lang.String):void
      com.lthj.unipay.plugin.j.a(android.content.Context, boolean):boolean
      com.lthj.unipay.plugin.j.a(java.lang.String, char):java.lang.String[] */
    private void a(o oVar) {
        String[] a2 = j.a(oVar.a(), '|');
        String[] a3 = j.a(oVar.c(), '|');
        this.f = new Vector();
        for (int i2 = 0; i2 < a2.length; i2++) {
            PanBank panBank = new PanBank();
            panBank.setPanBankId(a2[i2]);
            panBank.setPanBank(a3[i2]);
            panBank.setPanType(oVar.d());
            this.f.add(panBank);
        }
    }

    private void b() {
        this.b.setAdapter((ListAdapter) new e(this));
        this.b.setVisibility(0);
    }

    private void c() {
        this.h = new ee(this);
        this.a.setAdapter((ListAdapter) this.h);
        this.a.setVisibility(0);
        findViewById(PluginLink.getIdupomp_lthj_support_creditcard_tv()).setVisibility(0);
        findViewById(PluginLink.getIdupomp_lthj_support_debitcard_tv()).setVisibility(0);
    }

    private void d() {
        if (this.g <= this.e.size()) {
            this.g += 15;
        } else {
            this.g = this.e.size();
        }
        if (this.h != null) {
            this.h.notifyDataSetChanged();
        }
    }

    public void errorCallBack(String str) {
        j.a(this, str);
    }

    public void onClick(View view) {
        if (view == this.c) {
            if (this.f == null) {
                cp.a("02", this, this);
            }
            if (this.b.getVisibility() == 0) {
                this.b.setVisibility(8);
                return;
            }
            this.a.setVisibility(8);
            this.b.setVisibility(0);
        } else if (view != this.d) {
        } else {
            if (this.a.getVisibility() == 0) {
                this.a.setVisibility(8);
                findViewById(PluginLink.getIdupomp_lthj_support_creditcard_tv()).setVisibility(8);
                findViewById(PluginLink.getIdupomp_lthj_support_debitcard_tv()).setVisibility(8);
                return;
            }
            this.a.setVisibility(0);
            this.b.setVisibility(8);
            findViewById(PluginLink.getIdupomp_lthj_support_creditcard_tv()).setVisibility(0);
            findViewById(PluginLink.getIdupomp_lthj_support_debitcard_tv()).setVisibility(0);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        String stringExtra = getIntent().getStringExtra("tranType");
        String str = TextUtils.isEmpty(stringExtra) ? "1" : stringExtra;
        if (str == "1") {
            a(getString(PluginLink.getStringupomp_lthj_back()), this.i);
        } else {
            a(getString(PluginLink.getStringupomp_lthj_back()), this.j);
        }
        setContentView(PluginLink.getLayoutupomp_lthj_supportcard());
        this.a = (ListView) findViewById(PluginLink.getIdupomp_lthj_bankcard_listview());
        this.b = (ListView) findViewById(PluginLink.getIdupomp_lthj_savecard_listview());
        this.d = (RelativeLayout) findViewById(PluginLink.getIdupomp_lthj_support_card_title1());
        this.c = (RelativeLayout) findViewById(PluginLink.getIdupomp_lthj_support_card_title2());
        this.c.setOnClickListener(this);
        this.d.setOnClickListener(this);
        this.a.setOnScrollListener(this);
        this.a.setVisibility(8);
        findViewById(PluginLink.getIdupomp_lthj_support_creditcard_tv()).setVisibility(8);
        findViewById(PluginLink.getIdupomp_lthj_support_debitcard_tv()).setVisibility(8);
        cp.c(this, this, str);
    }

    public void onScroll(AbsListView absListView, int i2, int i3, int i4) {
    }

    public void onScrollStateChanged(AbsListView absListView, int i2) {
        if (absListView.getLastVisiblePosition() == absListView.getCount() - 1) {
            d();
        }
    }

    public void responseCallBack(z zVar) {
        if (zVar != null && zVar.n() != null && !isFinishing()) {
            int e2 = zVar.e();
            int parseInt = Integer.parseInt(zVar.n());
            switch (e2) {
                case 8202:
                    if (parseInt == 0) {
                        a((o) zVar);
                        b();
                        return;
                    }
                    j.a(this, zVar.o(), parseInt);
                    return;
                case 8228:
                    if (parseInt == 0) {
                        this.e = a((ar) zVar);
                        c();
                        return;
                    }
                    j.a(this, zVar.o(), parseInt);
                    return;
                default:
                    return;
            }
        }
    }
}
