package com.unionpay.upomp.lthj.plugin.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.Toast;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.lthj.unipay.plugin.aa;
import com.lthj.unipay.plugin.as;
import com.lthj.unipay.plugin.ax;
import com.lthj.unipay.plugin.bw;
import com.lthj.unipay.plugin.bz;
import com.lthj.unipay.plugin.c;
import com.lthj.unipay.plugin.cf;
import com.lthj.unipay.plugin.cp;
import com.lthj.unipay.plugin.da;
import com.lthj.unipay.plugin.dc;
import com.lthj.unipay.plugin.dd;
import com.lthj.unipay.plugin.dh;
import com.lthj.unipay.plugin.du;
import com.lthj.unipay.plugin.dv;
import com.lthj.unipay.plugin.dw;
import com.lthj.unipay.plugin.dx;
import com.lthj.unipay.plugin.dy;
import com.lthj.unipay.plugin.dz;
import com.lthj.unipay.plugin.eb;
import com.lthj.unipay.plugin.ec;
import com.lthj.unipay.plugin.ed;
import com.lthj.unipay.plugin.eh;
import com.lthj.unipay.plugin.ei;
import com.lthj.unipay.plugin.f;
import com.lthj.unipay.plugin.j;
import com.lthj.unipay.plugin.l;
import com.lthj.unipay.plugin.n;
import com.lthj.unipay.plugin.z;
import com.unionpay.upomp.lthj.link.PluginLink;
import com.unionpay.upomp.lthj.plugin.model.GetBundleBankCardList;
import com.unionpay.upomp.lthj.widget.LineFrameView;
import com.unionpay.upomp.lthj.widget.ValidateCodeView;
import java.util.Timer;
import java.util.TimerTask;

public class BankCardInfoActivity extends BaseActivity implements View.OnClickListener, ExpandableListView.OnChildClickListener, UIResponseListener {
    public static final String TAG = "BankCardInfoActivity";
    private boolean A;
    private View B;
    ExpandableListView a;
    public TimerTask aaTimerTask;
    public int b = 60;
    private Button c;
    private EditText d;
    /* access modifiers changed from: private */
    public EditText e;
    private String f;
    private String g;
    private Button h;
    private Button i;
    private Button j;
    private View.OnClickListener k = new dc(this);
    /* access modifiers changed from: private */
    public Handler l = new da(this);
    private View.OnClickListener m = new du(this);
    private View.OnClickListener n = new dv(this);
    private ValidateCodeView o;
    private EditText p;
    private EditText q;
    private EditText r;
    private EditText s;
    private bw t;
    private ImageButton u;
    private GetBundleBankCardList v;
    /* access modifiers changed from: private */
    public Button w;
    private Button x;
    private Button y;
    /* access modifiers changed from: private */
    public boolean z;

    /* access modifiers changed from: private */
    public void b() {
        cp.a(this, this);
    }

    private void c() {
        a(getString(PluginLink.getStringupomp_lthj_back()), this.k);
        setContentView(PluginLink.getLayoutupomp_lthj_bankcard_list());
        this.a = (ExpandableListView) findViewById(PluginLink.getIdupomp_lthj_bankcard_listview());
        View findViewById = findViewById(PluginLink.getIdupomp_lthj_no_card_layout());
        this.u = (ImageButton) findViewById(PluginLink.getIdupomp_lthj_refrush_btn());
        if (as.a().D == null || as.a().D.size() == 0) {
            this.u.setOnClickListener(this);
            this.a.setVisibility(8);
            findViewById.setVisibility(0);
        } else {
            this.a.setVisibility(0);
            findViewById.setVisibility(8);
            this.a.setAdapter(new cf(this));
            this.a.setChoiceMode(1);
            this.a.setItemChecked(0, true);
            this.a.setCacheColorHint(0);
            this.a.setGroupIndicator(null);
            this.a.setOnChildClickListener(this);
        }
        this.h = (Button) findViewById(PluginLink.getIdupomp_lthj_add_bankcard_btn());
        this.h.setOnClickListener(this);
    }

    /* access modifiers changed from: private */
    public void d() {
        a(getString(PluginLink.getStringupomp_lthj_back()), this.m);
        setContentView(PluginLink.getLayoutupomp_lthj_bindcard_home());
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_username_view())).a(as.a().A.toString());
        this.o = (ValidateCodeView) findViewById(PluginLink.getIdupomp_lthj_validatecode_layout());
        this.d = (EditText) findViewById(PluginLink.getIdupomp_lthj_card_num_edit());
        this.d.addTextChangedListener(new ax(this.d));
        this.e = (EditText) findViewById(PluginLink.getIdupomp_lthj_mobile_num_edit());
        this.c = (Button) findViewById(PluginLink.getIdupomp_lthj_next_btn());
        this.c.setOnClickListener(this);
        this.j = (Button) findViewById(PluginLink.getIdupomp_lthj_help_btn());
        this.j.setOnClickListener(this);
        if (as.a().b.b()) {
            this.o.setVisibility(0);
            j.a(this.o);
        }
        this.B = findViewById(PluginLink.getIdupomp_lthj_myinfo_view());
        this.B.setOnClickListener(this);
    }

    private void e() {
        a(getString(PluginLink.getStringupomp_lthj_back()), this.n);
        setContentView(PluginLink.getLayoutupomp_lthj_bindcard_next());
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_bankcard_info_view())).a(this.f + "-" + j.a(this.g) + "(" + as.a().d.a.substring(as.a().d.a.length() - 4, as.a().d.a.length()) + ")");
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_mobile_number_view())).a(j.f(this.e.getText().toString()));
        this.q = (EditText) findViewById(PluginLink.getIdupomp_lthj_cvn2_edit());
        this.r = (EditText) findViewById(PluginLink.getIdupomp_lthj_date_edit());
        this.s = (EditText) findViewById(PluginLink.getIdupomp_lthj_pin_edit());
        this.i = (Button) findViewById(PluginLink.getIdupomp_lthj_button_ok());
        this.i.setOnClickListener(this);
        aa aaVar = new aa(0);
        this.s.setOnFocusChangeListener(aaVar);
        this.s.setOnTouchListener(aaVar);
        aa aaVar2 = new aa(2);
        this.q.setOnFocusChangeListener(aaVar2);
        this.q.setOnTouchListener(aaVar2);
        this.x = (Button) findViewById(PluginLink.getIdupomp_lthj_cvn2_help());
        this.y = (Button) findViewById(PluginLink.getIdupomp_lthj_date_help());
        this.y.setOnClickListener(this);
        this.x.setOnClickListener(this);
        this.t = new bw();
        f fVar = new f(this.t);
        this.r.setOnFocusChangeListener(fVar);
        this.r.setOnTouchListener(fVar);
        this.p = (EditText) findViewById(PluginLink.getIdupomp_lthj_mobilemac_edit());
        this.w = (Button) findViewById(PluginLink.getIdupomp_lthj_get_mac_btn());
        this.w.setOnClickListener(new dw(this));
        if ("00".equals(this.g)) {
            findViewById(PluginLink.getIdupomp_lthj_pin_layout()).setVisibility(8);
        } else if ("01".equals(this.g)) {
            findViewById(PluginLink.getIdupomp_lthj_cvn2_layout()).setVisibility(8);
            findViewById(PluginLink.getIdupomp_lthj_date_layout()).setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        cp.a(this, this, this.v.bindId);
    }

    /* access modifiers changed from: private */
    public void g() {
        cp.b(this, this, this.v.bindId, this.v.isDefault);
    }

    public void errorCallBack(String str) {
        if (this.w != null) {
            this.w.setEnabled(true);
            this.w.setText(getString(PluginLink.getStringupomp_lthj_click_get_mac()));
        }
        l.a().a(this, "确定", str);
    }

    public boolean onChildClick(ExpandableListView expandableListView, View view, int i2, int i3, long j2) {
        if (expandableListView != this.a) {
            return false;
        }
        this.v = (GetBundleBankCardList) as.a().D.get(i2);
        if (expandableListView.getExpandableListAdapter().getChildrenCount(i2) == 2 && i3 == 1) {
            i3++;
        }
        switch (i3) {
            case 0:
                as.a().E = this.v;
                a().changeSubActivity(new Intent(this, PayActivity.class));
                return false;
            case 1:
                l.a().a(this, getString(PluginLink.getStringupomp_lthj_set_default_prompt()), new dx(this));
                return false;
            case 2:
                l.a().a(this, getString(PluginLink.getStringupomp_lthj_unbind_prompt()), new dy(this));
                return false;
            default:
                return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.lthj.unipay.plugin.cp.a(com.unionpay.upomp.lthj.plugin.ui.UIResponseListener, android.content.Context, java.lang.String, java.lang.String, boolean):void
     arg types: [com.unionpay.upomp.lthj.plugin.ui.BankCardInfoActivity, com.unionpay.upomp.lthj.plugin.ui.BankCardInfoActivity, java.lang.String, java.lang.String, int]
     candidates:
      com.lthj.unipay.plugin.cp.a(com.unionpay.upomp.lthj.plugin.ui.UIResponseListener, android.content.Context, java.lang.String, com.lthj.unipay.plugin.bw, java.lang.String):void
      com.lthj.unipay.plugin.cp.a(com.unionpay.upomp.lthj.plugin.ui.UIResponseListener, android.content.Context, java.lang.String, java.lang.String, boolean):void */
    public void onClick(View view) {
        if (view == this.c) {
            if (c.k(this, this.d) && c.a(this, this.e) && c.a(this, this.o)) {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.delete(0, stringBuffer.length());
                stringBuffer.append(this.d.getText().toString());
                for (int i2 = 0; i2 < stringBuffer.length(); i2++) {
                    if (stringBuffer.charAt(i2) == ' ') {
                        stringBuffer.deleteCharAt(i2);
                    }
                }
                as.a().d.a.setLength(0);
                as.a().d.a.append(stringBuffer);
                stringBuffer.delete(0, stringBuffer.length());
                cp.a((UIResponseListener) this, (Context) this, as.a().d.a.toString(), this.o.d(), false);
            }
        } else if (view == this.h) {
            d();
        } else if (view == this.i) {
            if (c.a(this, this.p, this.w)) {
                if ("00".equals(this.g)) {
                    if (!c.j(this, this.q) || !c.i(this, this.r)) {
                        return;
                    }
                } else if (!c.l(this, this.s)) {
                    return;
                }
                cp.a(this, this, this.e.getText().toString(), this.p.getText().toString(), this.d.getText().toString().replace(" ", PoiTypeDef.All), this.t, this.q.getText().toString(), true);
            }
        } else if (view == this.u) {
            cp.a(this, this);
        } else if (view == this.x || view == this.y) {
            j.c(this);
        } else if (view == this.j) {
            Intent intent = new Intent(this, SupportCardActivity.class);
            intent.putExtra("tranType", "2");
            intent.addFlags(67108864);
            a().changeSubActivity(intent);
        } else if (view == this.B) {
            a().changeSubActivity(new Intent(this, AccountActivity.class));
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.z = getIntent().getBooleanExtra("isQuickPayBind", false);
        this.A = getIntent().getBooleanExtra("isBind", false);
        if (this.z || this.A) {
            d();
            return;
        }
        c();
        b();
    }

    public void processRefreshConn() {
        if (this.aaTimerTask == null) {
            Timer timer = new Timer();
            this.aaTimerTask = new dd(this);
            timer.schedule(this.aaTimerTask, 0, (long) 1000);
        }
    }

    public void responseCallBack(z zVar) {
        if (zVar != null && zVar.n() != null && !isFinishing()) {
            int e2 = zVar.e();
            int parseInt = Integer.parseInt(zVar.n());
            if ("5309".equals(zVar.n())) {
                l.a().a(this, getString(PluginLink.getStringupomp_lthj_multiple_user_login_tip()), new ei(this));
                return;
            }
            switch (e2) {
                case 8200:
                    n nVar = (n) zVar;
                    if (parseInt != 0) {
                        Toast makeText = Toast.makeText(this, getString(PluginLink.getStringupomp_lthj_sendMessageLose()) + nVar.o() + ",错误码为：" + parseInt, 1);
                        makeText.setGravity(17, 0, 0);
                        makeText.show();
                        this.w.setEnabled(true);
                        this.w.setText(getString(PluginLink.getStringupomp_lthj_click_get_mac()));
                        break;
                    } else {
                        Toast makeText2 = Toast.makeText(this, getString(PluginLink.getStringupomp_lthj_sendMessageSuccess()), 1);
                        makeText2.setGravity(17, 0, 0);
                        makeText2.show();
                        as.a().y.delete(0, as.a().y.length());
                        as.a().y.append(nVar.c());
                        processRefreshConn();
                        break;
                    }
                case 8203:
                    if (parseInt != 0) {
                        l.a().a(this, getString(PluginLink.getStringupomp_lthj_bind_fail()) + "(" + zVar.n() + ")", getString(PluginLink.getStringupomp_lthj_ok()), getString(PluginLink.getStringupomp_lthj_rebind()), new ed(this), new ec(this));
                        break;
                    } else {
                        l.a().a(this, getString(PluginLink.getStringupomp_lthj_ok()), getString(PluginLink.getStringupomp_lthj_bind_success()), new dz(this));
                        break;
                    }
                case 8204:
                    b();
                    break;
                case 8206:
                    b();
                    break;
                case 8207:
                    a(getString(PluginLink.getStringupomp_lthj_back()), this.k);
                    bz bzVar = (bz) zVar;
                    if (parseInt != 0) {
                        j.a(this, bzVar.o(), parseInt);
                        break;
                    } else {
                        as.a().D = j.a(bzVar);
                        c();
                        break;
                    }
                case 8224:
                    dh dhVar = (dh) zVar;
                    if (parseInt != 0) {
                        as.a().b.b(true);
                        if (!"5110".equals(zVar.n())) {
                            j.a(this, zVar.o(), parseInt);
                            break;
                        } else {
                            l.a().a(this, j.b(this, zVar.o(), parseInt), getString(PluginLink.getStringupomp_lthj_ok()), getString(PluginLink.getStringupomp_lthj_check_support()), new eh(this), new eb(this));
                            break;
                        }
                    } else {
                        this.f = dhVar.c();
                        this.g = dhVar.a();
                        e();
                        as.a().b.b(false);
                        break;
                    }
            }
            if (zVar.e() != 8200) {
                j.b(this.o);
            }
        }
    }
}
