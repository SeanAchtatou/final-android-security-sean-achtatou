package com.unionpay.upomp.lthj.plugin.ui;

import com.lthj.unipay.plugin.z;

public interface UIResponseListener {
    void errorCallBack(String str);

    void responseCallBack(z zVar);
}
