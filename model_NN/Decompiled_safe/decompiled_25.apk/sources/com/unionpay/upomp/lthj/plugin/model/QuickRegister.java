package com.unionpay.upomp.lthj.plugin.model;

public class QuickRegister extends Data {
    public String loginName;
    public String merchantId;
    public String merchantOrderId;
    public String merchantOrderTime;
    public String password;
    public String secureAnswer;
    public String secureQuestion;
    public String welcome;
}
