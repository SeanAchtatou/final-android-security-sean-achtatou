package com.unionpay.upomp.lthj.plugin.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.lthj.unipay.plugin.aa;
import com.lthj.unipay.plugin.ah;
import com.lthj.unipay.plugin.as;
import com.lthj.unipay.plugin.au;
import com.lthj.unipay.plugin.bp;
import com.lthj.unipay.plugin.bq;
import com.lthj.unipay.plugin.br;
import com.lthj.unipay.plugin.bu;
import com.lthj.unipay.plugin.bv;
import com.lthj.unipay.plugin.bx;
import com.lthj.unipay.plugin.by;
import com.lthj.unipay.plugin.c;
import com.lthj.unipay.plugin.ca;
import com.lthj.unipay.plugin.cb;
import com.lthj.unipay.plugin.cp;
import com.lthj.unipay.plugin.j;
import com.lthj.unipay.plugin.l;
import com.lthj.unipay.plugin.n;
import com.lthj.unipay.plugin.z;
import com.unionpay.upomp.lthj.link.PluginLink;
import com.unionpay.upomp.lthj.widget.CustomInputView;
import com.unionpay.upomp.lthj.widget.LineFrameView;
import java.util.Timer;
import java.util.TimerTask;

public class AccountActivity extends BaseActivity implements View.OnClickListener, UIResponseListener {
    public int a = 60;
    public TimerTask aaTimerTask;
    private View.OnClickListener b = new bp(this);
    private Button c;
    private Button d;
    private Button e;
    private Button f;
    private CustomInputView g;
    private CustomInputView h;
    private CustomInputView i;
    private EditText j;
    private Button k;
    /* access modifiers changed from: private */
    public EditText l;
    /* access modifiers changed from: private */
    public Button m;
    /* access modifiers changed from: private */
    public Handler n = new cb(this);

    private void b() {
        Intent intent = new Intent(this, BankCardInfoActivity.class);
        intent.addFlags(67108864);
        a().changeSubActivity(intent);
    }

    /* access modifiers changed from: private */
    public void c() {
        a(getString(PluginLink.getStringupomp_lthj_back()), new br(this));
        setContentView(PluginLink.getLayoutupomp_lthj_myinfo());
        LineFrameView lineFrameView = (LineFrameView) findViewById(PluginLink.getIdupomp_lthj_bind_number_view());
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_username_view())).a(as.a().A);
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_welcome_view())).a(as.a().G);
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_mobile_number_view())).a(j.f(as.a().B.toString()));
        this.e = (Button) findViewById(PluginLink.getIdupomp_lthj_button_bankcard_manage());
        if (as.a().D == null || as.a().D.size() == 0) {
            lineFrameView.a(PluginLink.getStringupomp_lthj_no_bind());
            this.e.setText(getString(PluginLink.getStringupomp_lthj_bind_now()));
            this.e.setOnClickListener(new bq(this));
        } else {
            lineFrameView.a(as.a().D.size() + "张(上限" + ((int) as.a().a.c()) + "张)");
            this.e.setOnClickListener(this);
        }
        this.c = (Button) findViewById(PluginLink.getIdupomp_lthj_button_change_pwd());
        this.c.setOnClickListener(this);
        this.d = (Button) findViewById(PluginLink.getIdupomp_lthj_button_change_mobile());
        this.d.setOnClickListener(this);
    }

    private void d() {
        a(getString(PluginLink.getStringupomp_lthj_back()), this.b);
        setContentView(PluginLink.getLayoutupomp_lthj_changepassword());
        this.g = (CustomInputView) findViewById(PluginLink.getIdupomp_lthj_old_pwd_input());
        this.h = (CustomInputView) findViewById(PluginLink.getIdupomp_lthj_new_pwd_input());
        this.i = (CustomInputView) findViewById(PluginLink.getIdupomp_lthj_confirm_pwd_input());
        aa aaVar = new aa(9);
        this.g.b().setOnTouchListener(aaVar);
        this.g.b().setOnFocusChangeListener(aaVar);
        aa aaVar2 = new aa(8);
        this.h.b().setOnTouchListener(aaVar2);
        this.h.b().setOnFocusChangeListener(aaVar2);
        aa aaVar3 = new aa(7);
        this.i.b().setOnTouchListener(aaVar3);
        this.i.b().setOnFocusChangeListener(aaVar3);
        this.j = (EditText) findViewById(PluginLink.getIdupomp_lthj_mobilemac_edit());
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_mobile_number_view())).a(j.f(as.a().B.toString()));
        this.f = (Button) findViewById(PluginLink.getIdupomp_lthj_button_ok());
        this.f.setOnClickListener(this);
        this.m = (Button) findViewById(PluginLink.getIdupomp_lthj_get_mac_btn());
        this.m.setOnClickListener(new by(this));
    }

    private void e() {
        a(getString(PluginLink.getStringupomp_lthj_back()), this.b);
        setContentView(PluginLink.getLayoutupomp_lthj_changemobile());
        this.l = (EditText) findViewById(PluginLink.getIdupomp_lthj_new_mobilenum_edit());
        this.j = (EditText) findViewById(PluginLink.getIdupomp_lthj_mobilemac_edit());
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_mobile_number_view())).a(as.a().B);
        this.k = (Button) findViewById(PluginLink.getIdupomp_lthj_button_ok());
        this.k.setOnClickListener(this);
        this.m = (Button) findViewById(PluginLink.getIdupomp_lthj_get_mac_btn());
        this.m.setOnClickListener(new bx(this));
    }

    public void errorCallBack(String str) {
        j.a(this, str);
    }

    public void onClick(View view) {
        if (view == this.c) {
            d();
        } else if (view == this.d) {
            e();
        } else if (view == this.e) {
            b();
        } else if (view == this.f) {
            if (c.d(this, this.g.b()) && c.e(this, this.h.b()) && c.c(this, this.i.b()) && c.a(this) && c.a(this, this.j, this.m)) {
                cp.b(this, this, this.j.getText().toString());
            }
        } else if (view == this.k && c.a(this, this.l) && c.a(this, this.j, this.m)) {
            cp.c(this, this, this.l.getText().toString(), this.j.getText().toString());
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        c();
    }

    public void processRefreshConn() {
        if (this.aaTimerTask == null) {
            Timer timer = new Timer();
            this.aaTimerTask = new ca(this);
            timer.schedule(this.aaTimerTask, 0, (long) 1000);
        }
    }

    public void responseCallBack(z zVar) {
        if (zVar != null && zVar.n() != null && !isFinishing()) {
            int e2 = zVar.e();
            int parseInt = Integer.parseInt(zVar.n());
            if ("5309".equals(zVar.n())) {
                l.a().a(this, getString(PluginLink.getStringupomp_lthj_multiple_user_login_tip()), new bv(this));
                return;
            }
            switch (e2) {
                case 8196:
                    if (parseInt == 0) {
                        as.a().C.setLength(0);
                        as.a().C.append(as.a().d.d);
                        as.a().d.d.setLength(0);
                        l.a().a(this, getString(PluginLink.getStringupomp_lthj_ok()), getString(PluginLink.getStringupomp_lthj_change_pwd_success()), new bu(this));
                        return;
                    }
                    j.a(this, zVar.o(), parseInt);
                    d();
                    return;
                case 8197:
                    if (parseInt == 0) {
                        as.a().B.setLength(0);
                        as.a().B.append(((au) zVar).a());
                        l.a().a(this, getString(PluginLink.getStringupomp_lthj_ok()), getString(PluginLink.getStringupomp_lthj_change_mobile_success()), new ah(this));
                        return;
                    }
                    j.a(this, zVar.o(), parseInt);
                    e();
                    return;
                case 8198:
                case 8199:
                default:
                    return;
                case 8200:
                    n nVar = (n) zVar;
                    if (parseInt == 0) {
                        Toast makeText = Toast.makeText(this, getString(PluginLink.getStringupomp_lthj_sendMessageSuccess()), 1);
                        makeText.setGravity(17, 0, 0);
                        makeText.show();
                        as.a().y.delete(0, as.a().y.length());
                        as.a().y.append(nVar.c());
                        processRefreshConn();
                        return;
                    }
                    Toast makeText2 = Toast.makeText(this, getString(PluginLink.getStringupomp_lthj_sendMessageLose()) + nVar.o() + ",错误码为：" + parseInt, 1);
                    makeText2.setGravity(17, 0, 0);
                    makeText2.show();
                    this.m.setEnabled(true);
                    this.m.setText(getString(PluginLink.getStringupomp_lthj_click_get_mac()));
                    return;
            }
        }
    }
}
