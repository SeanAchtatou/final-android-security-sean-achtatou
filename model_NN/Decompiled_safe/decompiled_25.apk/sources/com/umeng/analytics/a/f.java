package com.umeng.analytics.a;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;
import com.umeng.analytics.i;
import com.umeng.common.a;
import com.umeng.common.b;
import org.json.JSONObject;

/* compiled from: Header */
public class f implements g {
    private final String A = a.g;
    private final String B = a.d;
    private final String C = "device_id";
    private final String D = a.e;
    private final String E = "mc";
    private final String F = com.umeng.analytics.f.H;
    private final String G = "device_model";
    private final String H = "os";
    private final String I = "os_version";
    private final String J = "resolution";
    private final String K = "cpu";
    private final String L = "gpu_vender";
    private final String M = "gpu_renderer";
    private final String N = "app_version";
    private final String O = a.f;
    private final String P = "package_name";
    private final String Q = "sdk_type";
    private final String R = a.h;
    private final String S = "timezone";
    private final String T = "country";
    private final String U = "language";
    private final String V = "access";
    private final String W = "access_subtype";
    private final String X = "carrier";
    private final String Y = "wrapper_type";
    private final String Z = "wrapper_version";
    public String a;
    public String b;
    public String c;
    public String d;
    public String e;
    public long f;
    public String g;
    public String h;
    public String i;
    public String j;
    public String k;
    public String l;
    public String m;
    public String n;
    public String o;
    public String p;
    public String q;
    public String r;
    public int s;
    public String t;
    public String u;
    public String v;
    public String w;
    public String x;
    public String y;
    public String z;

    public f() {
    }

    public f(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    private void c(JSONObject jSONObject) throws Exception {
        this.a = jSONObject.getString(a.g);
        this.c = jSONObject.getString("device_id");
        this.d = jSONObject.getString(a.e);
        if (jSONObject.has("mc")) {
            this.e = jSONObject.getString("mc");
        }
        if (jSONObject.has(a.d)) {
            this.b = jSONObject.getString(a.d);
        }
        if (jSONObject.has(com.umeng.analytics.f.H)) {
            this.f = jSONObject.getLong(com.umeng.analytics.f.H);
        }
    }

    private void d(JSONObject jSONObject) throws Exception {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6 = null;
        this.g = jSONObject.has("device_model") ? jSONObject.getString("device_model") : null;
        if (jSONObject.has("os")) {
            str = jSONObject.getString("os");
        } else {
            str = null;
        }
        this.h = str;
        if (jSONObject.has("os_version")) {
            str2 = jSONObject.getString("os_version");
        } else {
            str2 = null;
        }
        this.i = str2;
        if (jSONObject.has("resolution")) {
            str3 = jSONObject.getString("resolution");
        } else {
            str3 = null;
        }
        this.j = str3;
        if (jSONObject.has("cpu")) {
            str4 = jSONObject.getString("cpu");
        } else {
            str4 = null;
        }
        this.k = str4;
        if (jSONObject.has("gpu_vender")) {
            str5 = jSONObject.getString("gpu_vender");
        } else {
            str5 = null;
        }
        this.l = str5;
        if (jSONObject.has("gpu_renderer")) {
            str6 = jSONObject.getString("gpu_renderer");
        }
        this.m = str6;
    }

    private void e(JSONObject jSONObject) throws Exception {
        String str;
        String str2;
        String str3 = null;
        if (jSONObject.has("app_version")) {
            str = jSONObject.getString("app_version");
        } else {
            str = null;
        }
        this.n = str;
        if (jSONObject.has(a.f)) {
            str2 = jSONObject.getString(a.f);
        } else {
            str2 = null;
        }
        this.o = str2;
        if (jSONObject.has("package_name")) {
            str3 = jSONObject.getString("package_name");
        }
        this.p = str3;
    }

    private void f(JSONObject jSONObject) throws Exception {
        this.q = jSONObject.getString("sdk_type");
        this.r = jSONObject.getString(a.h);
    }

    private void g(JSONObject jSONObject) throws Exception {
        String str;
        String str2 = null;
        this.s = jSONObject.has("timezone") ? jSONObject.getInt("timezone") : 8;
        if (jSONObject.has("country")) {
            str = jSONObject.getString("country");
        } else {
            str = null;
        }
        this.t = str;
        if (jSONObject.has("language")) {
            str2 = jSONObject.getString("language");
        }
        this.u = str2;
    }

    private void h(JSONObject jSONObject) throws Exception {
        String str;
        String str2;
        String str3 = null;
        if (jSONObject.has("access")) {
            str = jSONObject.getString("access");
        } else {
            str = null;
        }
        this.v = str;
        if (jSONObject.has("access_subtype")) {
            str2 = jSONObject.getString("access_subtype");
        } else {
            str2 = null;
        }
        this.w = str2;
        if (jSONObject.has("carrier")) {
            str3 = jSONObject.getString("carrier");
        }
        this.x = str3;
    }

    private void i(JSONObject jSONObject) throws Exception {
        String str;
        String str2 = null;
        if (jSONObject.has("wrapper_type")) {
            str = jSONObject.getString("wrapper_type");
        } else {
            str = null;
        }
        this.y = str;
        if (jSONObject.has("wrapper_version")) {
            str2 = jSONObject.getString("wrapper_version");
        }
        this.z = str2;
    }

    public void a(JSONObject jSONObject) throws Exception {
        if (jSONObject != null) {
            c(jSONObject);
            d(jSONObject);
            e(jSONObject);
            f(jSONObject);
            g(jSONObject);
            h(jSONObject);
            i(jSONObject);
        }
    }

    private void j(JSONObject jSONObject) throws Exception {
        jSONObject.put(a.g, this.a);
        jSONObject.put("device_id", this.c);
        jSONObject.put(a.e, this.d);
        if (this.b != null) {
            jSONObject.put(a.d, this.b);
        }
        if (this.e != null) {
            jSONObject.put("mc", this.e);
        }
        if (this.f > 0) {
            jSONObject.put(com.umeng.analytics.f.H, this.f);
        }
    }

    private void k(JSONObject jSONObject) throws Exception {
        if (this.g != null) {
            jSONObject.put("device_model", this.g);
        }
        if (this.h != null) {
            jSONObject.put("os", this.h);
        }
        if (this.i != null) {
            jSONObject.put("os_version", this.i);
        }
        if (this.j != null) {
            jSONObject.put("resolution", this.j);
        }
        if (this.k != null) {
            jSONObject.put("cpu", this.k);
        }
        if (this.l != null) {
            jSONObject.put("gpu_vender", this.l);
        }
        if (this.m != null) {
            jSONObject.put("gpu_vender", this.m);
        }
    }

    private void l(JSONObject jSONObject) throws Exception {
        if (this.n != null) {
            jSONObject.put("app_version", this.n);
        }
        if (this.o != null) {
            jSONObject.put(a.f, this.o);
        }
        if (this.p != null) {
            jSONObject.put("package_name", this.p);
        }
    }

    private void m(JSONObject jSONObject) throws Exception {
        jSONObject.put("sdk_type", this.q);
        jSONObject.put(a.h, this.r);
    }

    private void n(JSONObject jSONObject) throws Exception {
        jSONObject.put("timezone", this.s);
        if (this.t != null) {
            jSONObject.put("country", this.t);
        }
        if (this.u != null) {
            jSONObject.put("language", this.u);
        }
    }

    private void o(JSONObject jSONObject) throws Exception {
        if (this.v != null) {
            jSONObject.put("access", this.v);
        }
        if (this.w != null) {
            jSONObject.put("access_subtype", this.w);
        }
        if (this.x != null) {
            jSONObject.put("carrier", this.x);
        }
    }

    private void p(JSONObject jSONObject) throws Exception {
        if (this.y != null) {
            jSONObject.put("wrapper_type", this.y);
        }
        if (this.z != null) {
            jSONObject.put("wrapper_version", this.z);
        }
    }

    public void b(JSONObject jSONObject) throws Exception {
        j(jSONObject);
        k(jSONObject);
        l(jSONObject);
        m(jSONObject);
        n(jSONObject);
        o(jSONObject);
        p(jSONObject);
    }

    public boolean a() {
        if (this.a == null) {
            Log.e(com.umeng.analytics.f.q, "missing appkey ");
            return false;
        } else if (this.c != null && this.d != null) {
            return true;
        } else {
            Log.e(com.umeng.analytics.f.q, "missing device id");
            return false;
        }
    }

    public void a(Context context, String... strArr) {
        if (strArr != null && strArr.length == 2) {
            this.a = strArr[0];
            this.b = strArr[1];
        }
        if (this.a == null) {
            this.a = b.q(context);
        }
        if (this.b == null) {
            this.b = b.u(context);
        }
        this.c = b.g(context);
        this.d = b.h(context);
        this.e = b.r(context);
        SharedPreferences c2 = i.c(context);
        if (c2 != null) {
            this.f = c2.getLong(com.umeng.analytics.f.H, 0);
        }
    }

    public void a(Context context) {
        this.g = Build.MODEL;
        this.h = "Android";
        this.i = Build.VERSION.RELEASE;
        this.j = b.s(context);
        this.k = b.a();
    }

    public void b(Context context) {
        this.n = b.e(context);
        this.o = b.d(context);
        this.p = b.v(context);
    }

    public void c(Context context) {
        this.q = "Android";
        this.r = com.umeng.analytics.f.c;
    }

    public void d(Context context) {
        this.s = b.o(context);
        String[] p2 = b.p(context);
        this.t = p2[0];
        this.u = p2[1];
    }

    public void e(Context context) {
        String[] k2 = b.k(context);
        this.v = k2[0];
        this.w = k2[1];
        this.x = b.t(context);
    }

    public void b(Context context, String... strArr) {
        a(context, strArr);
        a(context);
        b(context);
        c(context);
        d(context);
        e(context);
    }

    public boolean b() {
        if (this.a == null || this.c == null) {
            return false;
        }
        return true;
    }
}
