package com.umeng.analytics;

import android.content.Context;
import android.content.SharedPreferences;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.umeng.analytics.a.g;
import com.umeng.common.Log;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: UmengStoreHelper */
public final class i {
    private static final String a = "cache_version";
    private static final String b = "error";
    private static final String c = "mobclick_agent_user_";
    private static final String d = "mobclick_agent_online_setting_";
    private static final String e = "mobclick_agent_header_";
    private static final String f = "mobclick_agent_update_";
    private static final String g = "mobclick_agent_state_";
    private static final String h = "mobclick_agent_cached_";

    static SharedPreferences a(Context context) {
        return context.getSharedPreferences(c + context.getPackageName(), 0);
    }

    public static SharedPreferences b(Context context) {
        return context.getSharedPreferences(d + context.getPackageName(), 0);
    }

    public static SharedPreferences c(Context context) {
        return context.getSharedPreferences(e + context.getPackageName(), 0);
    }

    static SharedPreferences d(Context context) {
        return context.getSharedPreferences(f + context.getPackageName(), 0);
    }

    public static SharedPreferences e(Context context) {
        return context.getSharedPreferences(g + context.getPackageName(), 0);
    }

    static String f(Context context) {
        return e + context.getPackageName();
    }

    static String g(Context context) {
        return h + context.getPackageName();
    }

    static JSONObject h(Context context) {
        JSONObject jSONObject = new JSONObject();
        SharedPreferences a2 = a(context);
        try {
            if (a2.getInt("gender", -1) != -1) {
                jSONObject.put("sex", a2.getInt("gender", -1));
            }
            if (a2.getInt("age", -1) != -1) {
                jSONObject.put("age", a2.getInt("age", -1));
            }
            if (!PoiTypeDef.All.equals(a2.getString("user_id", PoiTypeDef.All))) {
                jSONObject.put("id", a2.getString("user_id", PoiTypeDef.All));
            }
            if (!PoiTypeDef.All.equals(a2.getString("id_source", PoiTypeDef.All))) {
                jSONObject.put("url", URLEncoder.encode(a2.getString("id_source", PoiTypeDef.All)));
            }
            if (jSONObject.length() > 0) {
                return jSONObject;
            }
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static int[] i(Context context) {
        SharedPreferences b2 = b(context);
        int[] iArr = new int[2];
        if (b2.getInt(f.A, -1) != -1) {
            iArr[0] = b2.getInt(f.A, 1);
            iArr[1] = (int) b2.getLong(f.B, (long) f.h);
        } else {
            iArr[0] = b2.getInt(f.D, 1);
            iArr[1] = (int) b2.getLong(f.E, (long) f.h);
        }
        return iArr;
    }

    static JSONObject a(Context context, String str) {
        try {
            FileInputStream openFileInput = context.openFileInput(g(context));
            byte[] bArr = new byte[1024];
            String str2 = PoiTypeDef.All;
            while (true) {
                int read = openFileInput.read(bArr);
                if (read == -1) {
                    break;
                }
                str2 = str2 + new String(bArr, 0, read);
            }
            openFileInput.close();
            if (str2.length() == 0) {
                return null;
            }
            try {
                JSONObject jSONObject = new JSONObject(str2);
                if (!jSONObject.optString(a).equals(str)) {
                    jSONObject.remove(b);
                }
                jSONObject.remove(a);
                if (jSONObject.length() != 0) {
                    return jSONObject;
                }
                return null;
            } catch (JSONException e2) {
                j(context);
                e2.printStackTrace();
                return null;
            }
        } catch (FileNotFoundException | IOException e3) {
            return null;
        }
    }

    static void a(Context context, JSONObject jSONObject, String str) throws Exception {
        if (jSONObject != null) {
            String g2 = g(context);
            jSONObject.put(a, str);
            FileOutputStream openFileOutput = context.openFileOutput(g2, 0);
            openFileOutput.write(jSONObject.toString().getBytes());
            openFileOutput.flush();
            openFileOutput.close();
            Log.c(f.q, "cache buffer success");
        }
    }

    static void a(Context context, g gVar, String str) {
        if (gVar != null) {
            try {
                JSONObject jSONObject = new JSONObject();
                gVar.b(jSONObject);
                a(context, jSONObject, str);
            } catch (Exception e2) {
                Log.b(f.q, "cache message error", e2);
            }
        }
    }

    static void b(Context context, JSONObject jSONObject, String str) {
        if (jSONObject != null) {
            try {
                a(context, jSONObject.optJSONObject("body"), str);
            } catch (Exception e2) {
                Log.b(f.q, "cache message error", e2);
            }
        }
    }

    static void j(Context context) {
        context.deleteFile(f(context));
        context.deleteFile(g(context));
    }
}
