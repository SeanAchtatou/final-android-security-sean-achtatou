package com.umeng.analytics;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.umeng.analytics.a.d;
import com.umeng.analytics.a.h;
import com.umeng.analytics.a.l;
import com.umeng.analytics.a.m;
import com.umeng.analytics.a.o;
import com.umeng.common.Log;
import com.umeng.common.util.g;
import java.util.HashMap;

/* compiled from: InternalAgent */
class b extends d {
    String a = PoiTypeDef.All;
    String b = PoiTypeDef.All;
    private String l;
    private String m;
    private final int n = 0;
    private final int o = 1;
    private final String p = "start_millis";
    private final String q = "end_millis";
    private final String r = "last_fetch_location_time";
    private final long s = 10000;
    private final int t = 128;
    private final int u = 256;

    b() {
    }

    /* access modifiers changed from: package-private */
    public void a() {
    }

    /* access modifiers changed from: package-private */
    public void b() {
    }

    /* access modifiers changed from: package-private */
    public void a(String str, String str2) {
        this.d.y = str;
        this.d.z = str2;
    }

    /* access modifiers changed from: package-private */
    public void a(Context context) {
        if (context == null) {
            try {
                Log.b(f.q, "unexpected null context in onPause");
            } catch (Exception e) {
                Log.b(f.q, "Exception occurred in Mobclick.onRause(). ", e);
            }
        } else if (!context.getClass().getName().equals(this.l)) {
            Log.b(f.q, "onPause() called without context from corresponding onResume()");
        } else {
            new a(context, 0).start();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, String str) {
        if (!TextUtils.isEmpty(str)) {
            if (context == null) {
                Log.b(f.q, "unexpected null context in reportError");
                return;
            }
            this.c.a(new d(str));
            e(context);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, Throwable th) {
        if (context != null && th != null) {
            this.c.a(new d(th));
            e(context);
        }
    }

    /* compiled from: InternalAgent */
    private final class a extends Thread {
        private final Object b = new Object();
        private Context c;
        private int d;

        a(Context context, int i) {
            this.c = context.getApplicationContext();
            this.d = i;
        }

        public void run() {
            try {
                synchronized (this.b) {
                    switch (this.d) {
                        case 0:
                            b.this.j(this.c);
                            break;
                        case 1:
                            b.this.i(this.c);
                            break;
                    }
                }
            } catch (Exception e) {
                Log.b(f.q, "Exception occurred in invokehander.", e);
            }
        }
    }

    private void h(Context context) {
        if (context == null) {
            Log.b(f.q, "unexpected null context in onResume");
        } else {
            this.l = context.getClass().getName();
        }
    }

    /* access modifiers changed from: package-private */
    public void b(Context context) {
        try {
            h(context);
            getClass();
            new a(context, 1).start();
        } catch (Exception e) {
            Log.b(f.q, "Exception occurred in Mobclick.onResume(). ", e);
        }
    }

    /* access modifiers changed from: package-private */
    public void c(Context context) {
        try {
            getClass();
            a(context, 2);
        } catch (Exception e) {
            Log.b(f.q, "Exception occurred in Mobclick.flush(). ", e);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, String str, String str2, long j, int i) {
        if (context != null) {
            try {
                if (a(str, 128) && i > 0) {
                    if (this.m == null) {
                        Log.e(f.q, "can't call onEvent before session is initialized");
                        return;
                    }
                    if (str2 != null) {
                        if (!a(str2, 256)) {
                            Log.b(f.q, "invalid label in onEvent");
                            return;
                        }
                    }
                    this.c.a(this.m, str, str2, j, i);
                    e(context);
                    return;
                }
            } catch (Exception e) {
                Log.b(f.q, "Exception occurred in Mobclick.onEvent(). ", e);
                return;
            }
        }
        Log.b(f.q, "invalid params in onEvent");
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, String str, HashMap<String, String> hashMap, long j) {
        if (context != null) {
            try {
                if (!TextUtils.isEmpty(str)) {
                    if (!a(hashMap)) {
                        return;
                    }
                    if (this.m == null) {
                        Log.e(f.q, "can't call onEvent before session is initialized");
                        return;
                    }
                    this.c.a(this.m, str, hashMap, j);
                    e(context);
                    return;
                }
            } catch (Exception e) {
                Log.b(f.q, "Exception occurred in Mobclick.onEvent(). ", e);
                return;
            }
        }
        Log.b(f.q, "invalid params in onKVEventEnd");
    }

    /* access modifiers changed from: private */
    public synchronized void i(Context context) {
        SharedPreferences e = i.e(context);
        if (e != null) {
            if (a(e)) {
                this.m = b(context, e);
                Log.a(f.q, "Start new session: " + this.m);
            } else {
                this.m = c(context, e);
                Log.a(f.q, "Extend current session: " + this.m);
            }
        }
    }

    /* access modifiers changed from: private */
    public synchronized void j(Context context) {
        SharedPreferences e = i.e(context);
        if (e != null) {
            long j = e.getLong("start_millis", -1);
            if (j == -1) {
                Log.b(f.q, "onEndSession called before onStartSession");
            } else {
                long currentTimeMillis = System.currentTimeMillis();
                SharedPreferences.Editor a2 = m.a(e, this.l, j, currentTimeMillis);
                a2.putLong("start_millis", -1);
                a2.putLong("end_millis", currentTimeMillis);
                a2.commit();
            }
            a(context, e);
            a(context, 5);
        }
    }

    private void a(Context context, SharedPreferences sharedPreferences) {
        SharedPreferences.Editor a2;
        long currentTimeMillis = System.currentTimeMillis();
        if (f.i && currentTimeMillis - sharedPreferences.getLong("last_fetch_location_time", 0) >= 10000 && (a2 = o.a(context, sharedPreferences)) != null) {
            a2.putLong("last_fetch_location_time", currentTimeMillis);
            a2.commit();
        }
    }

    private boolean a(SharedPreferences sharedPreferences) {
        if (System.currentTimeMillis() - sharedPreferences.getLong("end_millis", -1) > f.d) {
            return true;
        }
        return false;
    }

    private String b(Context context, SharedPreferences sharedPreferences) {
        long currentTimeMillis = System.currentTimeMillis();
        String a2 = a(context, currentTimeMillis);
        h hVar = new h(context, a2);
        m a3 = m.a(context);
        this.c.a(hVar);
        this.c.a(a3);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(l.f, a2);
        edit.putLong("start_millis", currentTimeMillis);
        edit.putLong("end_millis", -1);
        edit.commit();
        getClass();
        a(context, 4);
        return a2;
    }

    private String a(Context context, long j) {
        String q2 = this.k == null ? com.umeng.common.b.q(context) : this.k;
        StringBuilder sb = new StringBuilder();
        sb.append(j).append(q2).append(g.b(com.umeng.common.b.g(context)));
        return g.a(sb.toString());
    }

    private String c(Context context, SharedPreferences sharedPreferences) {
        Long valueOf = Long.valueOf(System.currentTimeMillis());
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putLong("start_millis", valueOf.longValue());
        edit.putLong("end_millis", -1);
        edit.commit();
        return sharedPreferences.getString(l.f, null);
    }

    private void d(Context context, String str) {
        try {
            if (f.m) {
                this.c.a(str);
                return;
            }
            j a2 = j.a(context, str);
            a2.a(Long.valueOf(System.currentTimeMillis()));
            a2.a(context);
        } catch (Exception e) {
            Log.a(f.q, "exception in save event begin info");
        }
    }

    private int e(Context context, String str) {
        long longValue;
        try {
            if (f.m) {
                longValue = this.c.b(str);
            } else {
                longValue = j.a(context, str).a().longValue();
            }
            if (longValue > 0) {
                return (int) (System.currentTimeMillis() - longValue);
            }
            return -1;
        } catch (Exception e) {
            Log.a(f.q, "exception in get event duration", e);
            return -1;
        }
    }

    /* access modifiers changed from: package-private */
    public void b(Context context, String str) {
        if (context == null || !a(str, 128)) {
            Log.b(f.q, "invalid params in onEventBegin");
        } else {
            d(context, "_t" + str);
        }
    }

    /* access modifiers changed from: package-private */
    public void c(Context context, String str) {
        if (context == null || TextUtils.isEmpty(str)) {
            Log.a(f.q, "input Context is null or event_id is empty");
            return;
        }
        int e = e(context, "_t" + str);
        if (e < 0) {
            Log.b(f.q, "event duration less than 0 in onEventEnd");
            return;
        }
        a(context, str, null, (long) e, 1);
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, String str, String str2) {
        if (context == null || !a(str, 128) || !a(str2, 256)) {
            Log.b(f.q, "invalid params in onEventBegin");
        } else {
            d(context, "_tl" + str + str2);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(Context context, String str, String str2) {
        if (context == null || TextUtils.isEmpty(str2)) {
            Log.b(f.q, "invalid params in onEventEnd");
            return;
        }
        int e = e(context, "_tl" + str + str2);
        if (e < 0) {
            Log.b(f.q, "event duration less than 0 in onEvnetEnd");
            return;
        }
        a(context, str, str2, (long) e, 1);
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, String str, HashMap<String, String> hashMap, String str2) {
        if (context == null || TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            Log.b(f.q, "invalid params in onKVEventBegin");
        } else if (a(hashMap)) {
            try {
                String str3 = str + str2;
                this.c.a(str3, hashMap);
                this.c.a(str3);
            } catch (Exception e) {
                Log.e(f.q, "exception in save k-v event begin inof", e);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void c(Context context, String str, String str2) {
        if (context == null || TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            Log.b(f.q, "invalid params in onKVEventEnd");
            return;
        }
        String str3 = str + str2;
        int e = e(context, str3);
        if (e < 0) {
            Log.b(f.q, "event duration less than 0 in onEvnetEnd");
            return;
        }
        a(context, str, this.c.c(str3), (long) e);
    }

    /* access modifiers changed from: package-private */
    public boolean a(String str, int i) {
        int length;
        if (str == null || (length = str.getBytes().length) == 0 || length > i) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(java.util.Map<java.lang.String, java.lang.String> r8) {
        /*
            r7 = this;
            r3 = 1
            r2 = 0
            if (r8 == 0) goto L_0x000a
            boolean r0 = r8.isEmpty()
            if (r0 == 0) goto L_0x0013
        L_0x000a:
            java.lang.String r0 = "MobclickAgent"
            java.lang.String r1 = "map is null or empty in onEvent"
            com.umeng.common.Log.b(r0, r1)
            r0 = r2
        L_0x0012:
            return r0
        L_0x0013:
            java.util.Set r0 = r8.entrySet()
            java.util.Iterator r4 = r0.iterator()
        L_0x001b:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x005f
            java.lang.Object r0 = r4.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r1 = r0.getKey()
            java.lang.String r1 = (java.lang.String) r1
            r5 = 128(0x80, float:1.794E-43)
            boolean r1 = r7.a(r1, r5)
            if (r1 == 0) goto L_0x0043
            java.lang.Object r1 = r0.getValue()
            java.lang.String r1 = (java.lang.String) r1
            r5 = 256(0x100, float:3.59E-43)
            boolean r1 = r7.a(r1, r5)
            if (r1 != 0) goto L_0x001b
        L_0x0043:
            java.lang.String r1 = "MobclickAgent"
            java.lang.String r4 = "invalid key-<%s> or value-<%s> "
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]
            java.lang.Object r6 = r0.getKey()
            r5[r2] = r6
            java.lang.Object r0 = r0.getValue()
            r5[r3] = r0
            java.lang.String r0 = java.lang.String.format(r4, r5)
            com.umeng.common.Log.b(r1, r0)
            r0 = r2
            goto L_0x0012
        L_0x005f:
            r0 = r3
            goto L_0x0012
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.analytics.b.a(java.util.Map):boolean");
    }

    /* access modifiers changed from: package-private */
    public void d(Context context) {
        try {
            j(context);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void b(Context context, Throwable th) {
        try {
            this.c.a(new d(th));
            j(context);
        } catch (Exception e) {
            Log.a(f.q, "Exception in onAppCrash", e);
        }
    }
}
