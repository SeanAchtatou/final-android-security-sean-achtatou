package com.umeng.analytics.onlineconfig;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.umeng.analytics.f;
import com.umeng.analytics.i;
import com.umeng.common.Log;
import com.umeng.common.net.r;
import com.umeng.common.net.s;
import com.umeng.common.util.g;
import java.util.Iterator;
import org.json.JSONObject;

/* compiled from: OnlineConfigAgent */
public class a {
    private final String a = "last_config_time";
    private final String b = "report_policy";
    private final String c = "online_config";
    private String d = null;
    private String e = null;
    private UmengOnlineConfigureListener f = null;
    /* access modifiers changed from: private */
    public c g = null;

    public void a(Context context) {
        if (context == null) {
            try {
                Log.b(f.q, "unexpected null context in updateOnlineConfig");
            } catch (Exception e2) {
                Log.b(f.q, "exception in updateOnlineConfig");
            }
        } else {
            new Thread(new b(context)).start();
        }
    }

    public void a(Context context, String str, String str2) {
        this.d = str;
        this.e = str2;
        a(context);
    }

    public void a(UmengOnlineConfigureListener umengOnlineConfigureListener) {
        this.f = umengOnlineConfigureListener;
    }

    public void a() {
        this.f = null;
    }

    public void a(c cVar) {
        this.g = cVar;
    }

    public void b() {
        this.g = null;
    }

    /* access modifiers changed from: private */
    public void a(JSONObject jSONObject) {
        if (this.f != null) {
            this.f.onDataReceived(jSONObject);
        }
    }

    private String b(Context context) throws Exception {
        String str = this.d;
        if (str == null) {
            str = com.umeng.common.b.q(context);
        }
        if (str != null) {
            return str;
        }
        throw new Exception("none appkey exception");
    }

    private String c(Context context) {
        return this.e == null ? com.umeng.common.b.u(context) : this.e;
    }

    /* access modifiers changed from: private */
    public JSONObject d(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            getClass();
            jSONObject.put(com.umeng.common.a.b, "online_config");
            jSONObject.put(com.umeng.common.a.g, b(context));
            jSONObject.put(com.umeng.common.a.f, com.umeng.common.b.d(context));
            jSONObject.put(com.umeng.common.a.c, com.umeng.common.b.v(context));
            jSONObject.put(com.umeng.common.a.h, f.c);
            jSONObject.put(com.umeng.common.a.e, g.b(com.umeng.common.b.g(context)));
            jSONObject.put(com.umeng.common.a.d, c(context));
            jSONObject.put("report_policy", i.i(context)[0]);
            jSONObject.put("last_config_time", e(context));
            return jSONObject;
        } catch (Exception e2) {
            Log.b(f.q, "exception in onlineConfigInternal");
            return null;
        }
    }

    private String e(Context context) {
        return i.b(context).getString(f.C, PoiTypeDef.All);
    }

    /* access modifiers changed from: private */
    public void a(Context context, b bVar) {
        SharedPreferences.Editor edit = i.b(context).edit();
        if (!TextUtils.isEmpty(bVar.e)) {
            edit.putString(f.C, bVar.e);
        }
        if (bVar.c != -1) {
            edit.putInt(f.A, bVar.c);
            edit.putLong(f.B, (long) bVar.d);
        }
        edit.commit();
    }

    /* access modifiers changed from: private */
    public void b(Context context, b bVar) {
        if (bVar.a != null && bVar.a.length() != 0) {
            SharedPreferences.Editor edit = i.b(context).edit();
            try {
                JSONObject jSONObject = bVar.a;
                Iterator<String> keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    edit.putString(next, jSONObject.getString(next));
                }
                edit.commit();
                Log.a(f.q, "get online setting params: " + jSONObject);
            } catch (Exception e2) {
                Log.c(f.q, "save online config params", e2);
            }
        }
    }

    /* compiled from: OnlineConfigAgent */
    public class b extends r implements Runnable {
        Context a;

        public b(Context context) {
            this.a = context.getApplicationContext();
        }

        public void run() {
            try {
                b();
            } catch (Exception e) {
                a.this.a((JSONObject) null);
                Log.c(f.q, "reques update error", e);
            }
        }

        public boolean a() {
            return false;
        }

        private void b() {
            C0003a aVar = new C0003a(a.this.d(this.a));
            String[] strArr = f.s;
            b bVar = null;
            for (String a2 : strArr) {
                aVar.a(a2);
                bVar = (b) a(aVar, b.class);
                if (bVar != null) {
                    break;
                }
            }
            if (bVar == null) {
                a.this.a((JSONObject) null);
                return;
            }
            Log.a(f.q, "response : " + bVar.b);
            if (bVar.b) {
                if (a.this.g != null) {
                    a.this.g.a(bVar.c, (long) bVar.d);
                }
                a.this.a(this.a, bVar);
                a.this.b(this.a, bVar);
                a.this.a(bVar.a);
                return;
            }
            a.this.a((JSONObject) null);
        }
    }

    /* renamed from: com.umeng.analytics.onlineconfig.a$a  reason: collision with other inner class name */
    /* compiled from: OnlineConfigAgent */
    public class C0003a extends s {
        private JSONObject e;

        public C0003a(JSONObject jSONObject) {
            super(null);
            this.e = jSONObject;
        }

        public JSONObject a() {
            return this.e;
        }

        public String b() {
            return this.d;
        }
    }
}
