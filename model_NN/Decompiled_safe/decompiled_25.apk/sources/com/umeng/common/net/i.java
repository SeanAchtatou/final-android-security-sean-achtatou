package com.umeng.common.net;

import com.autonavi.aps.api.Constant;
import com.umeng.common.Log;
import com.umeng.common.util.g;
import java.io.IOException;
import java.util.Map;
import java.util.Random;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

/* compiled from: DownloadingService */
final class i implements Runnable {
    final /* synthetic */ String[] a;
    final /* synthetic */ boolean b;
    final /* synthetic */ Map c;

    i(String[] strArr, boolean z, Map map) {
        this.a = strArr;
        this.b = z;
        this.c = map;
    }

    public void run() {
        int nextInt = new Random().nextInt(1000);
        if (this.a == null) {
            Log.a(DownloadingService.o, nextInt + "service report: urls is null");
            return;
        }
        String[] strArr = this.a;
        int length = strArr.length;
        int i = 0;
        while (i < length) {
            String str = strArr[i];
            String a2 = g.a();
            String str2 = a2.split(" ")[0];
            String str3 = a2.split(" ")[1];
            long currentTimeMillis = System.currentTimeMillis();
            StringBuilder sb = new StringBuilder(str);
            sb.append("&data=" + str2);
            sb.append("&time=" + str3);
            sb.append("&ts=" + currentTimeMillis);
            if (this.b) {
                sb.append("&action_type=" + 1);
            } else {
                sb.append("&action_type=" + -2);
            }
            if (this.c != null) {
                for (String str4 : this.c.keySet()) {
                    sb.append("&" + str4 + "=" + ((String) this.c.get(str4)));
                }
            }
            try {
                Log.a(DownloadingService.o, nextInt + ": service report:\tget: " + sb.toString());
                HttpGet httpGet = new HttpGet(sb.toString());
                BasicHttpParams basicHttpParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(basicHttpParams, Constant.imeiMaxSalt);
                HttpConnectionParams.setSoTimeout(basicHttpParams, 20000);
                HttpResponse execute = new DefaultHttpClient(basicHttpParams).execute(httpGet);
                Log.a(DownloadingService.o, nextInt + ": service report:status code:  " + execute.getStatusLine().getStatusCode());
                if (execute.getStatusLine().getStatusCode() != 200) {
                    i++;
                } else {
                    return;
                }
            } catch (ClientProtocolException e) {
                Log.c(DownloadingService.o, nextInt + ": service report:\tClientProtocolException,Failed to send message." + str, e);
            } catch (IOException e2) {
                Log.c(DownloadingService.o, nextInt + ": service report:\tIOException,Failed to send message." + str, e2);
            }
        }
    }
}
