package com.umeng.common.net;

import android.app.Notification;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import com.umeng.common.Log;
import com.umeng.common.net.DownloadingService;
import com.umeng.common.net.a;

/* compiled from: DownloadingService */
class e implements DownloadingService.a {
    final /* synthetic */ DownloadingService a;

    e(DownloadingService downloadingService) {
        this.a = downloadingService;
    }

    public void a(int i) {
        int i2 = 0;
        if (DownloadingService.w.containsKey(Integer.valueOf(i))) {
            DownloadingService.d dVar = (DownloadingService.d) DownloadingService.w.get(Integer.valueOf(i));
            long[] jArr = dVar.f;
            if (jArr != null && jArr[1] > 0 && (i2 = (int) ((((float) jArr[0]) / ((float) jArr[1])) * 100.0f)) > 100) {
                i2 = 99;
            }
            Notification a2 = this.a.a(dVar.e, i, i2);
            dVar.b = a2;
            this.a.p.notify(i, a2);
        }
    }

    public void a(int i, int i2) {
        if (DownloadingService.w.containsKey(Integer.valueOf(i))) {
            DownloadingService.d dVar = (DownloadingService.d) DownloadingService.w.get(Integer.valueOf(i));
            a.C0004a aVar = dVar.e;
            Notification notification = dVar.b;
            notification.contentView.setProgressBar(com.umeng.common.a.a.c(this.a.r), 100, i2, false);
            notification.contentView.setTextViewText(com.umeng.common.a.a.b(this.a.r), String.valueOf(i2) + "%");
            this.a.p.notify(i, notification);
            Log.c(DownloadingService.o, String.format("%3$10s Notification: mNotificationId = %1$15s\t|\tprogress = %2$15s", Integer.valueOf(i), Integer.valueOf(i2), aVar.b));
        }
    }

    public void a(int i, String str) {
        DownloadingService.d dVar;
        if (DownloadingService.w.containsKey(Integer.valueOf(i)) && (dVar = (DownloadingService.d) DownloadingService.w.get(Integer.valueOf(i))) != null) {
            a.C0004a aVar = dVar.e;
            dVar.b.contentView.setTextViewText(com.umeng.common.a.a.b(this.a.r), String.valueOf(100) + "%");
            c.a(this.a.r).a(aVar.a, aVar.c, 100);
            Bundle bundle = new Bundle();
            bundle.putString("filename", str);
            if (aVar.a.equalsIgnoreCase("delta_update")) {
                Message obtain = Message.obtain();
                obtain.what = 6;
                obtain.arg1 = 1;
                obtain.obj = aVar;
                obtain.arg2 = i;
                obtain.setData(bundle);
                this.a.s.sendMessage(obtain);
                return;
            }
            Message obtain2 = Message.obtain();
            obtain2.what = 5;
            obtain2.arg1 = 1;
            obtain2.obj = aVar;
            obtain2.arg2 = i;
            obtain2.setData(bundle);
            this.a.s.sendMessage(obtain2);
            Message obtain3 = Message.obtain();
            obtain3.what = 5;
            obtain3.arg1 = 1;
            obtain3.arg2 = i;
            obtain3.setData(bundle);
            try {
                if (DownloadingService.v.get(aVar) != null) {
                    ((Messenger) DownloadingService.v.get(aVar)).send(obtain3);
                }
                this.a.a(i);
            } catch (RemoteException e) {
                this.a.a(i);
            }
        }
    }

    public void a(int i, Exception exc) {
        if (DownloadingService.w.containsKey(Integer.valueOf(i))) {
            DownloadingService.d dVar = (DownloadingService.d) DownloadingService.w.get(Integer.valueOf(i));
            a.C0004a aVar = dVar.e;
            Notification notification = dVar.b;
            notification.contentView.setTextViewText(com.umeng.common.a.a.d(this.a.r), aVar.b + com.umeng.common.a.l);
            this.a.p.notify(i, notification);
            this.a.a(i);
        }
    }
}
