package com.umeng.common.net;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Debug;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.umeng.common.Log;
import com.umeng.common.net.a;
import com.umeng.common.util.DeltaUpdate;
import com.umeng.common.util.g;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class DownloadingService extends Service {
    static final int a = 3;
    static final int b = 4;
    static final int c = 5;
    static final int d = 6;
    public static final int e = 0;
    public static final int f = 1;
    public static final int g = 2;
    public static final int h = 3;
    static final int i = 0;
    static final int j = 1;
    static final int k = 100;
    static final String l = "filename";
    public static boolean m = false;
    /* access modifiers changed from: private */
    public static final String o = DownloadingService.class.getName();
    private static final int q = 3;
    private static final long u = 8000;
    /* access modifiers changed from: private */
    public static Map<a.C0004a, Messenger> v = new HashMap();
    /* access modifiers changed from: private */
    public static Map<Integer, d> w = new HashMap();
    /* access modifiers changed from: private */
    public static Boolean y = false;
    final Messenger n = new Messenger(new c());
    /* access modifiers changed from: private */
    public NotificationManager p;
    /* access modifiers changed from: private */
    public Context r;
    /* access modifiers changed from: private */
    public Handler s;
    private a t;
    private BroadcastReceiver x;

    private interface a {
        void a(int i);

        void a(int i, int i2);

        void a(int i, Exception exc);

        void a(int i, String str);
    }

    class c extends Handler {
        c() {
        }

        public void handleMessage(Message message) {
            Log.c(DownloadingService.o, "IncomingHandler(msg.what:" + message.what + " msg.arg1:" + message.arg1 + " msg.arg2:" + message.arg2 + " msg.replyTo:" + message.replyTo);
            switch (message.what) {
                case 4:
                    Bundle data = message.getData();
                    Log.c(DownloadingService.o, "IncomingHandler(msg.getData():" + data);
                    a.C0004a a2 = a.C0004a.a(data);
                    if (DownloadingService.d(a2)) {
                        Log.a(DownloadingService.o, a2.b + " is already in downloading list. ");
                        Toast.makeText(DownloadingService.this.r, com.umeng.common.a.c.b(DownloadingService.this.r), 0).show();
                        Message obtain = Message.obtain();
                        obtain.what = 5;
                        obtain.arg1 = 2;
                        obtain.arg2 = 0;
                        try {
                            message.replyTo.send(obtain);
                            return;
                        } catch (RemoteException e) {
                            e.printStackTrace();
                            return;
                        }
                    } else {
                        DownloadingService.v.put(a2, message.replyTo);
                        DownloadingService.this.c(a2);
                        return;
                    }
                default:
                    super.handleMessage(message);
                    return;
            }
        }
    }

    public IBinder onBind(Intent intent) {
        Log.c(o, "onBind ");
        return this.n.getBinder();
    }

    public void onStart(Intent intent, int i2) {
        Log.c(o, "onStart ");
        a(getApplicationContext(), intent);
        super.onStart(intent, i2);
    }

    public void onCreate() {
        super.onCreate();
        if (m) {
            Log.LOG = true;
            Debug.waitForDebugger();
        }
        Log.c(o, "onCreate ");
        this.p = (NotificationManager) getSystemService("notification");
        this.r = this;
        this.s = new d(this);
        this.t = new e(this);
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        d dVar = w.get(Integer.valueOf(i2));
        if (dVar != null) {
            Log.c(o, "download service clear cache " + dVar.e.b);
            if (dVar.a != null) {
                dVar.a.a(2);
            }
            this.p.cancel(dVar.c);
            if (v.containsKey(dVar.e)) {
                v.remove(dVar.e);
            }
            dVar.b();
            e();
        }
    }

    private void d() {
        IntentFilter intentFilter = new IntentFilter(l.d);
        this.x = new f(this);
        registerReceiver(this.x, intentFilter);
    }

    /* access modifiers changed from: private */
    public void c(a.C0004a aVar) {
        Log.c(o, "startDownload([mComponentName:" + aVar.a + " mTitle:" + aVar.b + " mUrl:" + aVar.c + "])");
        int a2 = a(aVar);
        b bVar = new b(getApplicationContext(), aVar, a2, 0, this.t);
        d dVar = new d(aVar, a2);
        dVar.a();
        dVar.a = bVar;
        bVar.start();
        e();
        if (m) {
            for (Integer num : w.keySet()) {
                Log.c(o, "Running task " + w.get(num).e.b);
            }
        }
    }

    public static int a(a.C0004a aVar) {
        return Math.abs((int) (((long) ((aVar.b.hashCode() >> 2) + (aVar.c.hashCode() >> 3))) + System.currentTimeMillis()));
    }

    class b extends Thread {
        /* access modifiers changed from: private */
        public Context b;
        private String c;
        private int d = 0;
        private long e = -1;
        private long f = -1;
        private int g = -1;
        private int h;
        private a i;
        private a.C0004a j;

        public b(Context context, a.C0004a aVar, int i2, int i3, a aVar2) {
            long[] jArr;
            try {
                this.b = context;
                this.j = aVar;
                this.d = i3;
                if (DownloadingService.w.containsKey(Integer.valueOf(i2)) && (jArr = ((d) DownloadingService.w.get(Integer.valueOf(i2))).f) != null && jArr.length > 1) {
                    this.e = jArr[0];
                    this.f = jArr[1];
                }
                this.i = aVar2;
                this.h = i2;
                if (com.umeng.common.b.b()) {
                    this.c = Environment.getExternalStorageDirectory().getCanonicalPath();
                    new File(this.c).mkdirs();
                } else {
                    this.c = this.b.getFilesDir().getAbsolutePath();
                }
                this.c += "/download/.um/apk";
                new File(this.c).mkdirs();
            } catch (Exception e2) {
                Log.c(DownloadingService.o, e2.getMessage(), e2);
                this.i.a(this.h, e2);
            }
        }

        public void run() {
            boolean z = false;
            this.d = 0;
            try {
                if (this.i != null) {
                    this.i.a(this.h);
                }
                if (this.e > 0) {
                    z = true;
                }
                a(z);
                if (DownloadingService.v.size() <= 0) {
                    DownloadingService.this.stopSelf();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

        public void a(int i2) {
            this.g = i2;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
         arg types: [java.io.File, int]
         candidates:
          ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
          ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.umeng.common.net.DownloadingService.a(java.util.Map, boolean, java.lang.String[]):void
         arg types: [java.util.HashMap, int, java.lang.String[]]
         candidates:
          com.umeng.common.net.DownloadingService.a(com.umeng.common.net.a$a, int, int):android.app.Notification
          com.umeng.common.net.DownloadingService.a(com.umeng.common.net.DownloadingService, android.content.Context, android.content.Intent):boolean
          com.umeng.common.net.DownloadingService.a(java.util.Map, boolean, java.lang.String[]):void */
        /* JADX WARNING: Code restructure failed: missing block: B:58:0x0201, code lost:
            com.umeng.common.Log.b(com.umeng.common.net.DownloadingService.a(), java.lang.String.format("Service Client for downloading %1$15s is dead. Removing messenger from the service", r13.j.b));
            com.umeng.common.net.DownloadingService.b().put(r13.j, null);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:59:0x0223, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:60:0x0224, code lost:
            r1 = r0;
            r2 = r8;
            r3 = r9;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:181:0x0459 A[SYNTHETIC, Splitter:B:181:0x0459] */
        /* JADX WARNING: Removed duplicated region for block: B:184:0x045e A[SYNTHETIC, Splitter:B:184:0x045e] */
        /* JADX WARNING: Removed duplicated region for block: B:263:0x057e A[SYNTHETIC, Splitter:B:263:0x057e] */
        /* JADX WARNING: Removed duplicated region for block: B:266:0x0583 A[SYNTHETIC, Splitter:B:266:0x0583] */
        /* JADX WARNING: Removed duplicated region for block: B:277:0x05c3 A[Catch:{ InterruptedException -> 0x05d6 }] */
        /* JADX WARNING: Removed duplicated region for block: B:286:0x05e9 A[Catch:{ InterruptedException -> 0x05d6 }] */
        /* JADX WARNING: Removed duplicated region for block: B:365:? A[RETURN, SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:380:? A[RETURN, SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:59:0x0223 A[ExcHandler: IOException (r0v74 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:51:0x01ca] */
        /* JADX WARNING: Removed duplicated region for block: B:71:0x027b A[SYNTHETIC, Splitter:B:71:0x027b] */
        /* JADX WARNING: Removed duplicated region for block: B:74:0x0280 A[SYNTHETIC, Splitter:B:74:0x0280] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void a(boolean r14) {
            /*
                r13 = this;
                r2 = 0
                r1 = 0
                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x06c4, RemoteException -> 0x06b6, all -> 0x06a5 }
                r0.<init>()     // Catch:{ IOException -> 0x06c4, RemoteException -> 0x06b6, all -> 0x06a5 }
                com.umeng.common.net.a$a r3 = r13.j     // Catch:{ IOException -> 0x06c4, RemoteException -> 0x06b6, all -> 0x06a5 }
                java.lang.String r3 = r3.c     // Catch:{ IOException -> 0x06c4, RemoteException -> 0x06b6, all -> 0x06a5 }
                java.lang.String r3 = com.umeng.common.util.g.a(r3)     // Catch:{ IOException -> 0x06c4, RemoteException -> 0x06b6, all -> 0x06a5 }
                java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ IOException -> 0x06c4, RemoteException -> 0x06b6, all -> 0x06a5 }
                java.lang.String r3 = ".apk.tmp"
                java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ IOException -> 0x06c4, RemoteException -> 0x06b6, all -> 0x06a5 }
                java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x06c4, RemoteException -> 0x06b6, all -> 0x06a5 }
                com.umeng.common.net.a$a r3 = r13.j     // Catch:{ IOException -> 0x06c4, RemoteException -> 0x06b6, all -> 0x06a5 }
                java.lang.String r3 = r3.a     // Catch:{ IOException -> 0x06c4, RemoteException -> 0x06b6, all -> 0x06a5 }
                java.lang.String r4 = "delta_update"
                boolean r3 = r3.equalsIgnoreCase(r4)     // Catch:{ IOException -> 0x06c4, RemoteException -> 0x06b6, all -> 0x06a5 }
                if (r3 == 0) goto L_0x06df
                java.lang.String r3 = "apk"
                java.lang.String r4 = "patch"
                java.lang.String r0 = r0.replace(r3, r4)     // Catch:{ IOException -> 0x06c4, RemoteException -> 0x06b6, all -> 0x06a5 }
                r3 = r0
            L_0x0032:
                boolean r0 = com.umeng.common.b.b()     // Catch:{ IOException -> 0x06c4, RemoteException -> 0x06b6, all -> 0x06a5 }
                if (r0 == 0) goto L_0x018e
                java.io.File r0 = new java.io.File     // Catch:{ IOException -> 0x06c4, RemoteException -> 0x06b6, all -> 0x06a5 }
                java.lang.String r4 = r13.c     // Catch:{ IOException -> 0x06c4, RemoteException -> 0x06b6, all -> 0x06a5 }
                r0.<init>(r4, r3)     // Catch:{ IOException -> 0x06c4, RemoteException -> 0x06b6, all -> 0x06a5 }
                java.io.FileOutputStream r8 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x06c4, RemoteException -> 0x06b6, all -> 0x06a5 }
                r3 = 1
                r8.<init>(r0, r3)     // Catch:{ IOException -> 0x06c4, RemoteException -> 0x06b6, all -> 0x06a5 }
                r4 = r0
            L_0x0046:
                java.lang.String r0 = com.umeng.common.net.DownloadingService.o     // Catch:{ IOException -> 0x06d0, RemoteException -> 0x06c0, all -> 0x06af }
                java.lang.String r1 = "saveAPK: url = %1$15s\t|\tfilename = %2$15s"
                r3 = 2
                java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ IOException -> 0x06d0, RemoteException -> 0x06c0, all -> 0x06af }
                r5 = 0
                com.umeng.common.net.a$a r6 = r13.j     // Catch:{ IOException -> 0x06d0, RemoteException -> 0x06c0, all -> 0x06af }
                java.lang.String r6 = r6.c     // Catch:{ IOException -> 0x06d0, RemoteException -> 0x06c0, all -> 0x06af }
                r3[r5] = r6     // Catch:{ IOException -> 0x06d0, RemoteException -> 0x06c0, all -> 0x06af }
                r5 = 1
                java.lang.String r6 = r4.getAbsolutePath()     // Catch:{ IOException -> 0x06d0, RemoteException -> 0x06c0, all -> 0x06af }
                r3[r5] = r6     // Catch:{ IOException -> 0x06d0, RemoteException -> 0x06c0, all -> 0x06af }
                java.lang.String r1 = java.lang.String.format(r1, r3)     // Catch:{ IOException -> 0x06d0, RemoteException -> 0x06c0, all -> 0x06af }
                com.umeng.common.Log.c(r0, r1)     // Catch:{ IOException -> 0x06d0, RemoteException -> 0x06c0, all -> 0x06af }
                java.net.URL r0 = new java.net.URL     // Catch:{ IOException -> 0x06d0, RemoteException -> 0x06c0, all -> 0x06af }
                com.umeng.common.net.a$a r1 = r13.j     // Catch:{ IOException -> 0x06d0, RemoteException -> 0x06c0, all -> 0x06af }
                java.lang.String r1 = r1.c     // Catch:{ IOException -> 0x06d0, RemoteException -> 0x06c0, all -> 0x06af }
                r0.<init>(r1)     // Catch:{ IOException -> 0x06d0, RemoteException -> 0x06c0, all -> 0x06af }
                java.net.URLConnection r0 = r0.openConnection()     // Catch:{ IOException -> 0x06d0, RemoteException -> 0x06c0, all -> 0x06af }
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x06d0, RemoteException -> 0x06c0, all -> 0x06af }
                java.lang.String r1 = "GET"
                r0.setRequestMethod(r1)     // Catch:{ IOException -> 0x06d0, RemoteException -> 0x06c0, all -> 0x06af }
                java.lang.String r1 = "Accept-Encoding"
                java.lang.String r3 = "identity"
                r0.setRequestProperty(r1, r3)     // Catch:{ IOException -> 0x06d0, RemoteException -> 0x06c0, all -> 0x06af }
                java.lang.String r1 = "Connection"
                java.lang.String r3 = "keep-alive"
                r0.addRequestProperty(r1, r3)     // Catch:{ IOException -> 0x06d0, RemoteException -> 0x06c0, all -> 0x06af }
                r1 = 5000(0x1388, float:7.006E-42)
                r0.setConnectTimeout(r1)     // Catch:{ IOException -> 0x06d0, RemoteException -> 0x06c0, all -> 0x06af }
                r1 = 10000(0x2710, float:1.4013E-41)
                r0.setReadTimeout(r1)     // Catch:{ IOException -> 0x06d0, RemoteException -> 0x06c0, all -> 0x06af }
                boolean r1 = r4.exists()     // Catch:{ IOException -> 0x06d0, RemoteException -> 0x06c0, all -> 0x06af }
                if (r1 == 0) goto L_0x00c2
                long r5 = r4.length()     // Catch:{ IOException -> 0x06d0, RemoteException -> 0x06c0, all -> 0x06af }
                r9 = 0
                int r1 = (r5 > r9 ? 1 : (r5 == r9 ? 0 : -1))
                if (r1 <= 0) goto L_0x00c2
                java.lang.String r1 = "Range"
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x06d0, RemoteException -> 0x06c0, all -> 0x06af }
                r3.<init>()     // Catch:{ IOException -> 0x06d0, RemoteException -> 0x06c0, all -> 0x06af }
                java.lang.String r5 = "bytes="
                java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ IOException -> 0x06d0, RemoteException -> 0x06c0, all -> 0x06af }
                long r5 = r4.length()     // Catch:{ IOException -> 0x06d0, RemoteException -> 0x06c0, all -> 0x06af }
                java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ IOException -> 0x06d0, RemoteException -> 0x06c0, all -> 0x06af }
                java.lang.String r5 = "-"
                java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ IOException -> 0x06d0, RemoteException -> 0x06c0, all -> 0x06af }
                java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x06d0, RemoteException -> 0x06c0, all -> 0x06af }
                r0.setRequestProperty(r1, r3)     // Catch:{ IOException -> 0x06d0, RemoteException -> 0x06c0, all -> 0x06af }
            L_0x00c2:
                r0.connect()     // Catch:{ IOException -> 0x06d0, RemoteException -> 0x06c0, all -> 0x06af }
                java.io.InputStream r9 = r0.getInputStream()     // Catch:{ IOException -> 0x06d0, RemoteException -> 0x06c0, all -> 0x06af }
                if (r14 != 0) goto L_0x00ef
                r1 = 0
                r13.e = r1     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                int r0 = r0.getContentLength()     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                long r0 = (long) r0     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r13.f = r0     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r0 = com.umeng.common.net.DownloadingService.o     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r1 = "getContentLength: %1$15s"
                r2 = 1
                java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r3 = 0
                long r5 = r13.f     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r2[r3] = r5     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r1 = java.lang.String.format(r1, r2)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                com.umeng.common.Log.c(r0, r1)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
            L_0x00ef:
                r0 = 4096(0x1000, float:5.74E-42)
                byte[] r5 = new byte[r0]     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r0 = 0
                r6 = 50
                r1 = 1
                java.lang.String r2 = com.umeng.common.net.DownloadingService.o     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r3.<init>()     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                com.umeng.common.net.a$a r7 = r13.j     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r7 = r7.b     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.StringBuilder r3 = r3.append(r7)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r7 = "saveAPK getContentLength "
                java.lang.StringBuilder r3 = r3.append(r7)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                long r10 = r13.f     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r7 = java.lang.String.valueOf(r10)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.StringBuilder r3 = r3.append(r7)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                com.umeng.common.Log.c(r2, r3)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                android.content.Context r2 = r13.b     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                com.umeng.common.net.c r2 = com.umeng.common.net.c.a(r2)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                com.umeng.common.net.a$a r3 = r13.j     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r3 = r3.a     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                com.umeng.common.net.a$a r7 = r13.j     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r7 = r7.c     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r2.a(r3, r7)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
            L_0x0130:
                int r2 = r13.g     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                if (r2 >= 0) goto L_0x06dc
                int r2 = r9.read(r5)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                if (r2 <= 0) goto L_0x06dc
                r3 = 0
                r8.write(r5, r3, r2)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                long r10 = r13.e     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                long r2 = (long) r2     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                long r2 = r2 + r10
                r13.e = r2     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                int r3 = r0 + 1
                int r0 = r0 % r6
                if (r0 != 0) goto L_0x06d9
                android.content.Context r0 = r13.b     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                boolean r0 = com.umeng.common.b.n(r0)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                if (r0 != 0) goto L_0x01ad
                r0 = 0
            L_0x0152:
                r9.close()     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r8.close()     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                int r1 = r13.g     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r2 = 1
                if (r1 != r2) goto L_0x02ae
                java.util.Map r0 = com.umeng.common.net.DownloadingService.w     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                int r1 = r13.h     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.Object r0 = r0.get(r1)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                com.umeng.common.net.DownloadingService$d r0 = (com.umeng.common.net.DownloadingService.d) r0     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                long[] r1 = r0.f     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r2 = 0
                long r3 = r13.e     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r1[r2] = r3     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                long[] r1 = r0.f     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r2 = 1
                long r3 = r13.f     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r1[r2] = r3     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                long[] r0 = r0.f     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r1 = 2
                int r2 = r13.d     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                long r2 = (long) r2     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r0[r1] = r2     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                if (r9 == 0) goto L_0x0188
                r9.close()     // Catch:{ IOException -> 0x0291 }
            L_0x0188:
                if (r8 == 0) goto L_0x018d
                r8.close()     // Catch:{ IOException -> 0x028b }
            L_0x018d:
                return
            L_0x018e:
                android.content.Context r0 = r13.b     // Catch:{ IOException -> 0x06c4, RemoteException -> 0x06b6, all -> 0x06a5 }
                java.io.File r0 = r0.getFilesDir()     // Catch:{ IOException -> 0x06c4, RemoteException -> 0x06b6, all -> 0x06a5 }
                java.lang.String r0 = r0.getAbsolutePath()     // Catch:{ IOException -> 0x06c4, RemoteException -> 0x06b6, all -> 0x06a5 }
                r13.c = r0     // Catch:{ IOException -> 0x06c4, RemoteException -> 0x06b6, all -> 0x06a5 }
                android.content.Context r0 = r13.b     // Catch:{ IOException -> 0x06c4, RemoteException -> 0x06b6, all -> 0x06a5 }
                r4 = 32771(0x8003, float:4.5922E-41)
                java.io.FileOutputStream r1 = r0.openFileOutput(r3, r4)     // Catch:{ IOException -> 0x06c4, RemoteException -> 0x06b6, all -> 0x06a5 }
                android.content.Context r0 = r13.b     // Catch:{ IOException -> 0x06ca, RemoteException -> 0x06bb, all -> 0x06aa }
                java.io.File r0 = r0.getFileStreamPath(r3)     // Catch:{ IOException -> 0x06ca, RemoteException -> 0x06bb, all -> 0x06aa }
                r4 = r0
                r8 = r1
                goto L_0x0046
            L_0x01ad:
                long r10 = r13.e     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                float r0 = (float) r10     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r2 = 1120403456(0x42c80000, float:100.0)
                float r0 = r0 * r2
                long r10 = r13.f     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                float r2 = (float) r10     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                float r0 = r0 / r2
                int r0 = (int) r0     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r2 = 100
                if (r0 <= r2) goto L_0x06d6
                r0 = 99
                r2 = r0
            L_0x01bf:
                com.umeng.common.net.DownloadingService$a r0 = r13.i     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                if (r0 == 0) goto L_0x01ca
                com.umeng.common.net.DownloadingService$a r0 = r13.i     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                int r7 = r13.h     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r0.a(r7, r2)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
            L_0x01ca:
                java.util.Map r0 = com.umeng.common.net.DownloadingService.v     // Catch:{ DeadObjectException -> 0x0200 }
                com.umeng.common.net.a$a r7 = r13.j     // Catch:{ DeadObjectException -> 0x0200 }
                java.lang.Object r0 = r0.get(r7)     // Catch:{ DeadObjectException -> 0x0200 }
                if (r0 == 0) goto L_0x01ec
                java.util.Map r0 = com.umeng.common.net.DownloadingService.v     // Catch:{ DeadObjectException -> 0x0200 }
                com.umeng.common.net.a$a r7 = r13.j     // Catch:{ DeadObjectException -> 0x0200 }
                java.lang.Object r0 = r0.get(r7)     // Catch:{ DeadObjectException -> 0x0200 }
                android.os.Messenger r0 = (android.os.Messenger) r0     // Catch:{ DeadObjectException -> 0x0200 }
                r7 = 0
                r10 = 3
                r11 = 0
                android.os.Message r7 = android.os.Message.obtain(r7, r10, r2, r11)     // Catch:{ DeadObjectException -> 0x0200 }
                r0.send(r7)     // Catch:{ DeadObjectException -> 0x0200 }
            L_0x01ec:
                android.content.Context r0 = r13.b     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                com.umeng.common.net.c r0 = com.umeng.common.net.c.a(r0)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                com.umeng.common.net.a$a r7 = r13.j     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r7 = r7.a     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                com.umeng.common.net.a$a r10 = r13.j     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r10 = r10.c     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r0.a(r7, r10, r2)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r0 = r3
                goto L_0x0130
            L_0x0200:
                r0 = move-exception
                java.lang.String r0 = com.umeng.common.net.DownloadingService.o     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r7 = "Service Client for downloading %1$15s is dead. Removing messenger from the service"
                r10 = 1
                java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r11 = 0
                com.umeng.common.net.a$a r12 = r13.j     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r12 = r12.b     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r10[r11] = r12     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r7 = java.lang.String.format(r7, r10)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                com.umeng.common.Log.b(r0, r7)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.util.Map r0 = com.umeng.common.net.DownloadingService.v     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                com.umeng.common.net.a$a r7 = r13.j     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r10 = 0
                r0.put(r7, r10)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                goto L_0x01ec
            L_0x0223:
                r0 = move-exception
                r1 = r0
                r2 = r8
                r3 = r9
            L_0x0227:
                java.lang.String r0 = com.umeng.common.net.DownloadingService.o     // Catch:{ all -> 0x0579 }
                java.lang.String r4 = r1.getMessage()     // Catch:{ all -> 0x0579 }
                com.umeng.common.Log.c(r0, r4, r1)     // Catch:{ all -> 0x0579 }
                int r0 = r13.d     // Catch:{ all -> 0x0579 }
                int r0 = r0 + 1
                r13.d = r0     // Catch:{ all -> 0x0579 }
                r4 = 3
                if (r0 <= r4) goto L_0x05a1
                com.umeng.common.net.a$a r0 = r13.j     // Catch:{ all -> 0x0579 }
                boolean r0 = r0.g     // Catch:{ all -> 0x0579 }
                if (r0 != 0) goto L_0x05a1
                java.lang.String r0 = com.umeng.common.net.DownloadingService.o     // Catch:{ RemoteException -> 0x055b }
                java.lang.String r4 = "Download Fail out of max repeat count"
                com.umeng.common.Log.b(r0, r4)     // Catch:{ RemoteException -> 0x055b }
                java.util.Map r0 = com.umeng.common.net.DownloadingService.v     // Catch:{ RemoteException -> 0x055b }
                com.umeng.common.net.a$a r4 = r13.j     // Catch:{ RemoteException -> 0x055b }
                java.lang.Object r0 = r0.get(r4)     // Catch:{ RemoteException -> 0x055b }
                android.os.Messenger r0 = (android.os.Messenger) r0     // Catch:{ RemoteException -> 0x055b }
                r4 = 0
                r5 = 5
                r6 = 0
                r7 = 0
                android.os.Message r4 = android.os.Message.obtain(r4, r5, r6, r7)     // Catch:{ RemoteException -> 0x055b }
                r0.send(r4)     // Catch:{ RemoteException -> 0x055b }
                com.umeng.common.net.DownloadingService r0 = com.umeng.common.net.DownloadingService.this     // Catch:{ all -> 0x0579 }
                int r4 = r13.h     // Catch:{ all -> 0x0579 }
                r0.a(r4)     // Catch:{ all -> 0x0579 }
                r13.a(r1)     // Catch:{ all -> 0x0579 }
                com.umeng.common.net.DownloadingService r0 = com.umeng.common.net.DownloadingService.this     // Catch:{ all -> 0x0579 }
                android.os.Handler r0 = r0.s     // Catch:{ all -> 0x0579 }
                com.umeng.common.net.j r1 = new com.umeng.common.net.j     // Catch:{ all -> 0x0579 }
                r1.<init>(r13)     // Catch:{ all -> 0x0579 }
                r0.post(r1)     // Catch:{ all -> 0x0579 }
            L_0x0279:
                if (r3 == 0) goto L_0x027e
                r3.close()     // Catch:{ IOException -> 0x0648 }
            L_0x027e:
                if (r2 == 0) goto L_0x018d
                r2.close()     // Catch:{ IOException -> 0x0285 }
                goto L_0x018d
            L_0x0285:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x018d
            L_0x028b:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x018d
            L_0x0291:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ all -> 0x02a2 }
                if (r8 == 0) goto L_0x018d
                r8.close()     // Catch:{ IOException -> 0x029c }
                goto L_0x018d
            L_0x029c:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x018d
            L_0x02a2:
                r0 = move-exception
                if (r8 == 0) goto L_0x02a8
                r8.close()     // Catch:{ IOException -> 0x02a9 }
            L_0x02a8:
                throw r0
            L_0x02a9:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x02a8
            L_0x02ae:
                int r1 = r13.g     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r2 = 2
                if (r1 != r2) goto L_0x02fb
                com.umeng.common.net.DownloadingService r0 = com.umeng.common.net.DownloadingService.this     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                com.umeng.common.net.a$a r1 = r13.j     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                long r2 = r13.e     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                long r4 = r13.f     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                int r6 = r13.d     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                long r6 = (long) r6     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r0.a(r1, r2, r4, r6)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                com.umeng.common.net.DownloadingService r0 = com.umeng.common.net.DownloadingService.this     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                android.app.NotificationManager r0 = r0.p     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                int r1 = r13.h     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r0.cancel(r1)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                if (r9 == 0) goto L_0x02d1
                r9.close()     // Catch:{ IOException -> 0x02de }
            L_0x02d1:
                if (r8 == 0) goto L_0x018d
                r8.close()     // Catch:{ IOException -> 0x02d8 }
                goto L_0x018d
            L_0x02d8:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x018d
            L_0x02de:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ all -> 0x02ef }
                if (r8 == 0) goto L_0x018d
                r8.close()     // Catch:{ IOException -> 0x02e9 }
                goto L_0x018d
            L_0x02e9:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x018d
            L_0x02ef:
                r0 = move-exception
                if (r8 == 0) goto L_0x02f5
                r8.close()     // Catch:{ IOException -> 0x02f6 }
            L_0x02f5:
                throw r0
            L_0x02f6:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x02f5
            L_0x02fb:
                if (r0 != 0) goto L_0x0372
                java.lang.String r0 = com.umeng.common.net.DownloadingService.o     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r1.<init>()     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r2 = "Download Fail repeat count="
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                int r2 = r13.d     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                com.umeng.common.Log.b(r0, r1)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.util.Map r0 = com.umeng.common.net.DownloadingService.v     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                com.umeng.common.net.a$a r1 = r13.j     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.Object r0 = r0.get(r1)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                android.os.Messenger r0 = (android.os.Messenger) r0     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r1 = 0
                r2 = 5
                r3 = 0
                r4 = 0
                android.os.Message r1 = android.os.Message.obtain(r1, r2, r3, r4)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r0.send(r1)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                com.umeng.common.net.DownloadingService r0 = com.umeng.common.net.DownloadingService.this     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                int r1 = r13.h     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r0.a(r1)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                com.umeng.common.net.DownloadingService$a r0 = r13.i     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                if (r0 == 0) goto L_0x0343
                com.umeng.common.net.DownloadingService$a r0 = r13.i     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                int r1 = r13.h     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r2 = 0
                r0.a(r1, r2)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
            L_0x0343:
                if (r9 == 0) goto L_0x0348
                r9.close()     // Catch:{ IOException -> 0x0355 }
            L_0x0348:
                if (r8 == 0) goto L_0x018d
                r8.close()     // Catch:{ IOException -> 0x034f }
                goto L_0x018d
            L_0x034f:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x018d
            L_0x0355:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ all -> 0x0366 }
                if (r8 == 0) goto L_0x018d
                r8.close()     // Catch:{ IOException -> 0x0360 }
                goto L_0x018d
            L_0x0360:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x018d
            L_0x0366:
                r0 = move-exception
                if (r8 == 0) goto L_0x036c
                r8.close()     // Catch:{ IOException -> 0x036d }
            L_0x036c:
                throw r0
            L_0x036d:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x036c
            L_0x0372:
                com.umeng.common.net.a$a r0 = r13.j     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String[] r0 = r0.f     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                if (r0 == 0) goto L_0x03ad
                java.util.HashMap r0 = new java.util.HashMap     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r0.<init>()     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r1 = "dsize"
                long r2 = r13.f     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r0.put(r1, r2)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r1 = com.umeng.common.util.g.a()     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r2 = " "
                java.lang.String[] r1 = r1.split(r2)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r2 = 1
                r1 = r1[r2]     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r2 = "dtime"
                r0.put(r2, r1)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r1 = "ptimes"
                int r2 = r13.d     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r0.put(r1, r2)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r1 = 1
                com.umeng.common.net.a$a r2 = r13.j     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String[] r2 = r2.f     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                com.umeng.common.net.DownloadingService.b(r0, r1, r2)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
            L_0x03ad:
                java.io.File r0 = new java.io.File     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r1 = r4.getParent()     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r2 = r4.getName()     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r3 = ".tmp"
                java.lang.String r5 = ""
                java.lang.String r2 = r2.replace(r3, r5)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r0.<init>(r1, r2)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r4.renameTo(r0)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r1 = r0.getAbsolutePath()     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                com.umeng.common.net.a$a r2 = r13.j     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r2 = r2.d     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                if (r2 == 0) goto L_0x0521
                com.umeng.common.net.a$a r2 = r13.j     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r2 = r2.d     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r0 = com.umeng.common.util.g.a(r0)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                boolean r0 = r2.equalsIgnoreCase(r0)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                if (r0 != 0) goto L_0x0521
                com.umeng.common.net.a$a r0 = r13.j     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r0 = r0.a     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r2 = "delta_update"
                boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                if (r0 == 0) goto L_0x0486
                com.umeng.common.net.DownloadingService r0 = com.umeng.common.net.DownloadingService.this     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                android.app.NotificationManager r0 = r0.p     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                int r2 = r13.h     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r0.cancel(r2)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                android.os.Bundle r0 = new android.os.Bundle     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r0.<init>()     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r2 = "filename"
                r0.putString(r2, r1)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                android.os.Message r1 = android.os.Message.obtain()     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r2 = 5
                r1.what = r2     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r2 = 3
                r1.arg1 = r2     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                int r2 = r13.h     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r1.arg2 = r2     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r1.setData(r0)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.util.Map r0 = com.umeng.common.net.DownloadingService.v     // Catch:{ RemoteException -> 0x0443, IOException -> 0x0223 }
                com.umeng.common.net.a$a r2 = r13.j     // Catch:{ RemoteException -> 0x0443, IOException -> 0x0223 }
                java.lang.Object r0 = r0.get(r2)     // Catch:{ RemoteException -> 0x0443, IOException -> 0x0223 }
                if (r0 == 0) goto L_0x042a
                java.util.Map r0 = com.umeng.common.net.DownloadingService.v     // Catch:{ RemoteException -> 0x0443, IOException -> 0x0223 }
                com.umeng.common.net.a$a r2 = r13.j     // Catch:{ RemoteException -> 0x0443, IOException -> 0x0223 }
                java.lang.Object r0 = r0.get(r2)     // Catch:{ RemoteException -> 0x0443, IOException -> 0x0223 }
                android.os.Messenger r0 = (android.os.Messenger) r0     // Catch:{ RemoteException -> 0x0443, IOException -> 0x0223 }
                r0.send(r1)     // Catch:{ RemoteException -> 0x0443, IOException -> 0x0223 }
            L_0x042a:
                com.umeng.common.net.DownloadingService r0 = com.umeng.common.net.DownloadingService.this     // Catch:{ RemoteException -> 0x0443, IOException -> 0x0223 }
                int r1 = r13.h     // Catch:{ RemoteException -> 0x0443, IOException -> 0x0223 }
                r0.a(r1)     // Catch:{ RemoteException -> 0x0443, IOException -> 0x0223 }
            L_0x0431:
                if (r9 == 0) goto L_0x0436
                r9.close()     // Catch:{ IOException -> 0x0469 }
            L_0x0436:
                if (r8 == 0) goto L_0x018d
                r8.close()     // Catch:{ IOException -> 0x043d }
                goto L_0x018d
            L_0x043d:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x018d
            L_0x0443:
                r0 = move-exception
                com.umeng.common.net.DownloadingService r0 = com.umeng.common.net.DownloadingService.this     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                int r1 = r13.h     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r0.a(r1)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                goto L_0x0431
            L_0x044c:
                r0 = move-exception
            L_0x044d:
                com.umeng.common.net.DownloadingService r1 = com.umeng.common.net.DownloadingService.this     // Catch:{ all -> 0x06b3 }
                int r2 = r13.h     // Catch:{ all -> 0x06b3 }
                r1.a(r2)     // Catch:{ all -> 0x06b3 }
                r0.printStackTrace()     // Catch:{ all -> 0x06b3 }
                if (r9 == 0) goto L_0x045c
                r9.close()     // Catch:{ IOException -> 0x0665 }
            L_0x045c:
                if (r8 == 0) goto L_0x018d
                r8.close()     // Catch:{ IOException -> 0x0463 }
                goto L_0x018d
            L_0x0463:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x018d
            L_0x0469:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ all -> 0x047a }
                if (r8 == 0) goto L_0x018d
                r8.close()     // Catch:{ IOException -> 0x0474 }
                goto L_0x018d
            L_0x0474:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x018d
            L_0x047a:
                r0 = move-exception
                if (r8 == 0) goto L_0x0480
                r8.close()     // Catch:{ IOException -> 0x0481 }
            L_0x0480:
                throw r0
            L_0x0481:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x0480
            L_0x0486:
                java.util.Map r0 = com.umeng.common.net.DownloadingService.v     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                com.umeng.common.net.a$a r1 = r13.j     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.Object r0 = r0.get(r1)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                android.os.Messenger r0 = (android.os.Messenger) r0     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r1 = 0
                r2 = 5
                r3 = 0
                r4 = 0
                android.os.Message r1 = android.os.Message.obtain(r1, r2, r3, r4)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r0.send(r1)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                com.umeng.common.net.DownloadingService r0 = com.umeng.common.net.DownloadingService.this     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                int r1 = r13.h     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r0.a(r1)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                android.app.Notification r0 = new android.app.Notification     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r1 = 17301634(0x1080082, float:2.497962E-38)
                java.lang.String r2 = " 下载失败。"
                long r3 = java.lang.System.currentTimeMillis()     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r0.<init>(r1, r2, r3)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                android.content.Context r1 = r13.b     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r2 = 0
                android.content.Intent r3 = new android.content.Intent     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r3.<init>()     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r4 = 0
                android.app.PendingIntent r1 = android.app.PendingIntent.getActivity(r1, r2, r3, r4)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                android.content.Context r2 = r13.b     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                android.content.Context r3 = r13.b     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r3 = com.umeng.common.b.w(r3)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r4.<init>()     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                com.umeng.common.net.a$a r5 = r13.j     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r5 = r5.b     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r5 = " 下载失败。"
                java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                java.lang.String r4 = r4.toString()     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r0.setLatestEventInfo(r2, r3, r4, r1)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                int r1 = r0.flags     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r1 = r1 | 16
                r0.flags = r1     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                com.umeng.common.net.DownloadingService r1 = com.umeng.common.net.DownloadingService.this     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                android.app.NotificationManager r1 = r1.p     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                int r2 = r13.h     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r1.notify(r2, r0)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                if (r9 == 0) goto L_0x04f7
                r9.close()     // Catch:{ IOException -> 0x0504 }
            L_0x04f7:
                if (r8 == 0) goto L_0x018d
                r8.close()     // Catch:{ IOException -> 0x04fe }
                goto L_0x018d
            L_0x04fe:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x018d
            L_0x0504:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ all -> 0x0515 }
                if (r8 == 0) goto L_0x018d
                r8.close()     // Catch:{ IOException -> 0x050f }
                goto L_0x018d
            L_0x050f:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x018d
            L_0x0515:
                r0 = move-exception
                if (r8 == 0) goto L_0x051b
                r8.close()     // Catch:{ IOException -> 0x051c }
            L_0x051b:
                throw r0
            L_0x051c:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x051b
            L_0x0521:
                com.umeng.common.net.DownloadingService$a r0 = r13.i     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                if (r0 == 0) goto L_0x052c
                com.umeng.common.net.DownloadingService$a r0 = r13.i     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                int r2 = r13.h     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
                r0.a(r2, r1)     // Catch:{ IOException -> 0x0223, RemoteException -> 0x044c }
            L_0x052c:
                if (r9 == 0) goto L_0x0531
                r9.close()     // Catch:{ IOException -> 0x053e }
            L_0x0531:
                if (r8 == 0) goto L_0x018d
                r8.close()     // Catch:{ IOException -> 0x0538 }
                goto L_0x018d
            L_0x0538:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x018d
            L_0x053e:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ all -> 0x054f }
                if (r8 == 0) goto L_0x018d
                r8.close()     // Catch:{ IOException -> 0x0549 }
                goto L_0x018d
            L_0x0549:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x018d
            L_0x054f:
                r0 = move-exception
                if (r8 == 0) goto L_0x0555
                r8.close()     // Catch:{ IOException -> 0x0556 }
            L_0x0555:
                throw r0
            L_0x0556:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x0555
            L_0x055b:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ all -> 0x0587 }
                com.umeng.common.net.DownloadingService r0 = com.umeng.common.net.DownloadingService.this     // Catch:{ all -> 0x0579 }
                int r4 = r13.h     // Catch:{ all -> 0x0579 }
                r0.a(r4)     // Catch:{ all -> 0x0579 }
                r13.a(r1)     // Catch:{ all -> 0x0579 }
                com.umeng.common.net.DownloadingService r0 = com.umeng.common.net.DownloadingService.this     // Catch:{ all -> 0x0579 }
                android.os.Handler r0 = r0.s     // Catch:{ all -> 0x0579 }
                com.umeng.common.net.j r1 = new com.umeng.common.net.j     // Catch:{ all -> 0x0579 }
                r1.<init>(r13)     // Catch:{ all -> 0x0579 }
                r0.post(r1)     // Catch:{ all -> 0x0579 }
                goto L_0x0279
            L_0x0579:
                r0 = move-exception
                r8 = r2
                r9 = r3
            L_0x057c:
                if (r9 == 0) goto L_0x0581
                r9.close()     // Catch:{ IOException -> 0x0688 }
            L_0x0581:
                if (r8 == 0) goto L_0x0586
                r8.close()     // Catch:{ IOException -> 0x0682 }
            L_0x0586:
                throw r0
            L_0x0587:
                r0 = move-exception
                com.umeng.common.net.DownloadingService r4 = com.umeng.common.net.DownloadingService.this     // Catch:{ all -> 0x0579 }
                int r5 = r13.h     // Catch:{ all -> 0x0579 }
                r4.a(r5)     // Catch:{ all -> 0x0579 }
                r13.a(r1)     // Catch:{ all -> 0x0579 }
                com.umeng.common.net.DownloadingService r1 = com.umeng.common.net.DownloadingService.this     // Catch:{ all -> 0x0579 }
                android.os.Handler r1 = r1.s     // Catch:{ all -> 0x0579 }
                com.umeng.common.net.j r4 = new com.umeng.common.net.j     // Catch:{ all -> 0x0579 }
                r4.<init>(r13)     // Catch:{ all -> 0x0579 }
                r1.post(r4)     // Catch:{ all -> 0x0579 }
                throw r0     // Catch:{ all -> 0x0579 }
            L_0x05a1:
                java.lang.String r0 = com.umeng.common.net.DownloadingService.o     // Catch:{ all -> 0x0579 }
                java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0579 }
                r1.<init>()     // Catch:{ all -> 0x0579 }
                java.lang.String r4 = "wait for repeating Test network repeat count="
                java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ all -> 0x0579 }
                int r4 = r13.d     // Catch:{ all -> 0x0579 }
                java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ all -> 0x0579 }
                java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0579 }
                com.umeng.common.Log.c(r0, r1)     // Catch:{ all -> 0x0579 }
                com.umeng.common.net.a$a r0 = r13.j     // Catch:{ InterruptedException -> 0x05d6 }
                boolean r0 = r0.g     // Catch:{ InterruptedException -> 0x05d6 }
                if (r0 != 0) goto L_0x05e9
                r0 = 8000(0x1f40, double:3.9525E-320)
                java.lang.Thread.sleep(r0)     // Catch:{ InterruptedException -> 0x05d6 }
                long r0 = r13.f     // Catch:{ InterruptedException -> 0x05d6 }
                r4 = 1
                int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
                if (r0 >= 0) goto L_0x05e3
                r0 = 0
                r13.a(r0)     // Catch:{ InterruptedException -> 0x05d6 }
                goto L_0x0279
            L_0x05d6:
                r0 = move-exception
                r13.a(r0)     // Catch:{ all -> 0x0579 }
                com.umeng.common.net.DownloadingService r0 = com.umeng.common.net.DownloadingService.this     // Catch:{ all -> 0x0579 }
                int r1 = r13.h     // Catch:{ all -> 0x0579 }
                r0.a(r1)     // Catch:{ all -> 0x0579 }
                goto L_0x0279
            L_0x05e3:
                r0 = 1
                r13.a(r0)     // Catch:{ InterruptedException -> 0x05d6 }
                goto L_0x0279
            L_0x05e9:
                java.util.Map r0 = com.umeng.common.net.DownloadingService.w     // Catch:{ InterruptedException -> 0x05d6 }
                int r1 = r13.h     // Catch:{ InterruptedException -> 0x05d6 }
                java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ InterruptedException -> 0x05d6 }
                java.lang.Object r0 = r0.get(r1)     // Catch:{ InterruptedException -> 0x05d6 }
                com.umeng.common.net.DownloadingService$d r0 = (com.umeng.common.net.DownloadingService.d) r0     // Catch:{ InterruptedException -> 0x05d6 }
                long[] r1 = r0.f     // Catch:{ InterruptedException -> 0x05d6 }
                r4 = 0
                long r5 = r13.e     // Catch:{ InterruptedException -> 0x05d6 }
                r1[r4] = r5     // Catch:{ InterruptedException -> 0x05d6 }
                long[] r1 = r0.f     // Catch:{ InterruptedException -> 0x05d6 }
                r4 = 1
                long r5 = r13.f     // Catch:{ InterruptedException -> 0x05d6 }
                r1[r4] = r5     // Catch:{ InterruptedException -> 0x05d6 }
                long[] r0 = r0.f     // Catch:{ InterruptedException -> 0x05d6 }
                r1 = 2
                int r4 = r13.d     // Catch:{ InterruptedException -> 0x05d6 }
                long r4 = (long) r4     // Catch:{ InterruptedException -> 0x05d6 }
                r0[r1] = r4     // Catch:{ InterruptedException -> 0x05d6 }
                int r0 = r13.h     // Catch:{ InterruptedException -> 0x05d6 }
                java.lang.String r1 = "continue"
                java.lang.String r0 = com.umeng.common.net.l.a(r0, r1)     // Catch:{ InterruptedException -> 0x05d6 }
                android.content.Intent r1 = new android.content.Intent     // Catch:{ InterruptedException -> 0x05d6 }
                android.content.Context r4 = r13.b     // Catch:{ InterruptedException -> 0x05d6 }
                java.lang.Class<com.umeng.common.net.DownloadingService> r5 = com.umeng.common.net.DownloadingService.class
                r1.<init>(r4, r5)     // Catch:{ InterruptedException -> 0x05d6 }
                java.lang.String r4 = "com.umeng.broadcast.download.msg"
                r1.putExtra(r4, r0)     // Catch:{ InterruptedException -> 0x05d6 }
                com.umeng.common.net.DownloadingService r0 = com.umeng.common.net.DownloadingService.this     // Catch:{ InterruptedException -> 0x05d6 }
                android.content.Context r4 = r13.b     // Catch:{ InterruptedException -> 0x05d6 }
                boolean unused = r0.a(r4, r1)     // Catch:{ InterruptedException -> 0x05d6 }
                com.umeng.common.net.DownloadingService r0 = com.umeng.common.net.DownloadingService.this     // Catch:{ InterruptedException -> 0x05d6 }
                android.content.Context r1 = r13.b     // Catch:{ InterruptedException -> 0x05d6 }
                android.content.Context r4 = r13.b     // Catch:{ InterruptedException -> 0x05d6 }
                int r4 = com.umeng.common.a.c.c(r4)     // Catch:{ InterruptedException -> 0x05d6 }
                java.lang.String r1 = r1.getString(r4)     // Catch:{ InterruptedException -> 0x05d6 }
                r0.a(r1)     // Catch:{ InterruptedException -> 0x05d6 }
                java.lang.String r0 = com.umeng.common.net.DownloadingService.o     // Catch:{ InterruptedException -> 0x05d6 }
                java.lang.String r1 = "changed play state button on op-notification."
                com.umeng.common.Log.c(r0, r1)     // Catch:{ InterruptedException -> 0x05d6 }
                goto L_0x0279
            L_0x0648:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ all -> 0x0659 }
                if (r2 == 0) goto L_0x018d
                r2.close()     // Catch:{ IOException -> 0x0653 }
                goto L_0x018d
            L_0x0653:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x018d
            L_0x0659:
                r0 = move-exception
                if (r2 == 0) goto L_0x065f
                r2.close()     // Catch:{ IOException -> 0x0660 }
            L_0x065f:
                throw r0
            L_0x0660:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x065f
            L_0x0665:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ all -> 0x0676 }
                if (r8 == 0) goto L_0x018d
                r8.close()     // Catch:{ IOException -> 0x0670 }
                goto L_0x018d
            L_0x0670:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x018d
            L_0x0676:
                r0 = move-exception
                if (r8 == 0) goto L_0x067c
                r8.close()     // Catch:{ IOException -> 0x067d }
            L_0x067c:
                throw r0
            L_0x067d:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x067c
            L_0x0682:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x0586
            L_0x0688:
                r1 = move-exception
                r1.printStackTrace()     // Catch:{ all -> 0x0699 }
                if (r8 == 0) goto L_0x0586
                r8.close()     // Catch:{ IOException -> 0x0693 }
                goto L_0x0586
            L_0x0693:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x0586
            L_0x0699:
                r0 = move-exception
                if (r8 == 0) goto L_0x069f
                r8.close()     // Catch:{ IOException -> 0x06a0 }
            L_0x069f:
                throw r0
            L_0x06a0:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x069f
            L_0x06a5:
                r0 = move-exception
                r8 = r1
                r9 = r2
                goto L_0x057c
            L_0x06aa:
                r0 = move-exception
                r8 = r1
                r9 = r2
                goto L_0x057c
            L_0x06af:
                r0 = move-exception
                r9 = r2
                goto L_0x057c
            L_0x06b3:
                r0 = move-exception
                goto L_0x057c
            L_0x06b6:
                r0 = move-exception
                r8 = r1
                r9 = r2
                goto L_0x044d
            L_0x06bb:
                r0 = move-exception
                r8 = r1
                r9 = r2
                goto L_0x044d
            L_0x06c0:
                r0 = move-exception
                r9 = r2
                goto L_0x044d
            L_0x06c4:
                r0 = move-exception
                r3 = r2
                r2 = r1
                r1 = r0
                goto L_0x0227
            L_0x06ca:
                r0 = move-exception
                r3 = r2
                r2 = r1
                r1 = r0
                goto L_0x0227
            L_0x06d0:
                r0 = move-exception
                r1 = r0
                r3 = r2
                r2 = r8
                goto L_0x0227
            L_0x06d6:
                r2 = r0
                goto L_0x01bf
            L_0x06d9:
                r0 = r3
                goto L_0x0130
            L_0x06dc:
                r0 = r1
                goto L_0x0152
            L_0x06df:
                r3 = r0
                goto L_0x0032
            */
            throw new UnsupportedOperationException("Method not decompiled: com.umeng.common.net.DownloadingService.b.a(boolean):void");
        }

        private void a(Exception exc) {
            Log.b(DownloadingService.o, "can not install. " + exc.getMessage());
            if (this.i != null) {
                this.i.a(this.h, exc);
            }
            DownloadingService.this.a(this.j, this.e, this.f, (long) this.d);
        }
    }

    /* access modifiers changed from: private */
    public void a(a.C0004a aVar, long j2, long j3, long j4) {
        if (aVar.f != null) {
            HashMap hashMap = new HashMap();
            hashMap.put("dsize", String.valueOf(j2));
            hashMap.put("dtime", g.a().split(" ")[1]);
            float f2 = 0.0f;
            if (j3 > 0) {
                f2 = ((float) j2) / ((float) j3);
            }
            hashMap.put("dpcent", String.valueOf((int) (f2 * 100.0f)));
            hashMap.put("ptimes", String.valueOf(j4));
            b(hashMap, false, aVar.f);
        }
    }

    public void a(String str) {
        synchronized (y) {
            if (!y.booleanValue()) {
                Log.c(o, "show single toast.[" + str + "]");
                y = true;
                this.s.post(new g(this, str));
                this.s.postDelayed(new h(this), 1200);
            }
        }
    }

    /* access modifiers changed from: private */
    public static boolean b(Context context) {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
        if (runningAppProcesses == null) {
            return false;
        }
        String packageName = context.getPackageName();
        for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
            if (next.importance == 100 && next.processName.equals(packageName)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public static boolean d(a.C0004a aVar) {
        if (m) {
            int nextInt = new Random().nextInt(1000);
            if (v != null) {
                for (a.C0004a next : v.keySet()) {
                    Log.c(o, "_" + nextInt + " downling  " + next.b + "   " + next.c);
                }
            } else {
                Log.c(o, "_" + nextInt + "downling  null");
            }
        }
        if (v == null) {
            return false;
        }
        for (a.C0004a next2 : v.keySet()) {
            if (aVar.e != null && aVar.e.equals(next2.e)) {
                return true;
            }
            if (next2.c.equals(aVar.c)) {
                return true;
            }
        }
        return false;
    }

    public void onDestroy() {
        try {
            c.a(getApplicationContext()).a(259200);
            c.a(getApplicationContext()).finalize();
            if (this.x != null) {
                unregisterReceiver(this.x);
            }
        } catch (Exception e2) {
            Log.b(o, e2.getMessage());
        }
        super.onDestroy();
    }

    private static class d {
        b a;
        Notification b;
        int c;
        int d;
        a.C0004a e;
        long[] f = new long[3];

        public d(a.C0004a aVar, int i) {
            this.c = i;
            this.e = aVar;
        }

        public void a() {
            DownloadingService.w.put(Integer.valueOf(this.c), this);
        }

        public void b() {
            if (DownloadingService.w.containsKey(Integer.valueOf(this.c))) {
                DownloadingService.w.remove(Integer.valueOf(this.c));
            }
        }
    }

    /* access modifiers changed from: private */
    public Notification a(a.C0004a aVar, int i2, int i3) {
        Context applicationContext = getApplicationContext();
        Notification notification = new Notification(17301633, com.umeng.common.a.m, 1);
        RemoteViews remoteViews = new RemoteViews(applicationContext.getPackageName(), com.umeng.common.a.b.a(applicationContext));
        remoteViews.setProgressBar(com.umeng.common.a.a.c(applicationContext), 100, i3, false);
        remoteViews.setTextViewText(com.umeng.common.a.a.b(applicationContext), i3 + "%");
        remoteViews.setTextViewText(com.umeng.common.a.a.d(applicationContext), applicationContext.getResources().getString(com.umeng.common.a.c.g(applicationContext.getApplicationContext())) + aVar.b);
        remoteViews.setTextViewText(com.umeng.common.a.a.a(applicationContext), PoiTypeDef.All);
        remoteViews.setImageViewResource(com.umeng.common.a.a.e(applicationContext), 17301633);
        notification.contentView = remoteViews;
        notification.contentIntent = PendingIntent.getActivity(applicationContext, 0, new Intent(), 134217728);
        if (aVar.g) {
            notification.flags = 2;
            remoteViews.setOnClickPendingIntent(com.umeng.common.a.a.f(applicationContext), l.b(getApplicationContext(), l.a(i2, l.b)));
            remoteViews.setViewVisibility(com.umeng.common.a.a.f(applicationContext), 0);
            b(notification, i2);
            PendingIntent b2 = l.b(getApplicationContext(), l.a(i2, l.c));
            remoteViews.setViewVisibility(com.umeng.common.a.a.h(applicationContext), 0);
            remoteViews.setOnClickPendingIntent(com.umeng.common.a.a.h(applicationContext), b2);
        } else {
            notification.flags = 16;
            remoteViews.setViewVisibility(com.umeng.common.a.a.f(applicationContext), 8);
            remoteViews.setViewVisibility(com.umeng.common.a.a.h(applicationContext), 8);
        }
        return notification;
    }

    /* access modifiers changed from: private */
    public static final void b(Map<String, String> map, boolean z, String[] strArr) {
        new Thread(new i(strArr, z, map)).start();
    }

    private void a(Notification notification, int i2) {
        int f2 = com.umeng.common.a.a.f(this.r);
        notification.contentView.setTextViewText(f2, this.r.getResources().getString(com.umeng.common.a.c.e(this.r.getApplicationContext())));
        notification.contentView.setInt(f2, "setBackgroundResource", com.umeng.common.c.a(this.r).c("umeng_common_gradient_green"));
        this.p.notify(i2, notification);
    }

    private void b(Notification notification, int i2) {
        int f2 = com.umeng.common.a.a.f(this.r);
        notification.contentView.setTextViewText(f2, this.r.getResources().getString(com.umeng.common.a.c.d(this.r.getApplicationContext())));
        notification.contentView.setInt(f2, "setBackgroundResource", com.umeng.common.c.a(this.r).c("umeng_common_gradient_orange"));
        this.p.notify(i2, notification);
    }

    private void e() {
        if (m) {
            int size = v.size();
            int size2 = w.size();
            Log.a(o, "Client size =" + size + "   cacheSize = " + size2);
            if (size != size2) {
                throw new RuntimeException("Client size =" + size + "   cacheSize = " + size2);
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(android.content.Context r16, android.content.Intent r17) {
        /*
            r15 = this;
            android.os.Bundle r1 = r17.getExtras()     // Catch:{ Exception -> 0x00e3 }
            java.lang.String r2 = "com.umeng.broadcast.download.msg"
            java.lang.String r1 = r1.getString(r2)     // Catch:{ Exception -> 0x00e3 }
            java.lang.String r2 = ":"
            java.lang.String[] r1 = r1.split(r2)     // Catch:{ Exception -> 0x00e3 }
            r2 = 0
            r2 = r1[r2]     // Catch:{ Exception -> 0x00e3 }
            int r5 = java.lang.Integer.parseInt(r2)     // Catch:{ Exception -> 0x00e3 }
            r2 = 1
            r1 = r1[r2]     // Catch:{ Exception -> 0x00e3 }
            java.lang.String r2 = r1.trim()     // Catch:{ Exception -> 0x00e3 }
            if (r5 == 0) goto L_0x00e7
            boolean r1 = android.text.TextUtils.isEmpty(r2)     // Catch:{ Exception -> 0x00e3 }
            if (r1 != 0) goto L_0x00e7
            java.util.Map<java.lang.Integer, com.umeng.common.net.DownloadingService$d> r1 = com.umeng.common.net.DownloadingService.w     // Catch:{ Exception -> 0x00e3 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x00e3 }
            boolean r1 = r1.containsKey(r3)     // Catch:{ Exception -> 0x00e3 }
            if (r1 == 0) goto L_0x00e7
            java.util.Map<java.lang.Integer, com.umeng.common.net.DownloadingService$d> r1 = com.umeng.common.net.DownloadingService.w     // Catch:{ Exception -> 0x00e3 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x00e3 }
            java.lang.Object r1 = r1.get(r3)     // Catch:{ Exception -> 0x00e3 }
            r0 = r1
            com.umeng.common.net.DownloadingService$d r0 = (com.umeng.common.net.DownloadingService.d) r0     // Catch:{ Exception -> 0x00e3 }
            r14 = r0
            com.umeng.common.net.DownloadingService$b r1 = r14.a     // Catch:{ Exception -> 0x00e3 }
            java.lang.String r3 = "continue"
            boolean r3 = r3.equals(r2)     // Catch:{ Exception -> 0x00e3 }
            if (r3 == 0) goto L_0x00ae
            if (r1 != 0) goto L_0x0099
            java.lang.String r1 = com.umeng.common.net.DownloadingService.o     // Catch:{ Exception -> 0x00e3 }
            java.lang.String r2 = "Receive action do play click."
            com.umeng.common.Log.c(r1, r2)     // Catch:{ Exception -> 0x00e3 }
            java.lang.String r1 = "android.permission.ACCESS_NETWORK_STATE"
            r0 = r16
            boolean r1 = com.umeng.common.b.a(r0, r1)     // Catch:{ Exception -> 0x00e3 }
            if (r1 == 0) goto L_0x007f
            boolean r1 = com.umeng.common.b.n(r16)     // Catch:{ Exception -> 0x00e3 }
            if (r1 != 0) goto L_0x007f
            android.content.res.Resources r1 = r16.getResources()     // Catch:{ Exception -> 0x00e3 }
            android.content.Context r2 = r16.getApplicationContext()     // Catch:{ Exception -> 0x00e3 }
            int r2 = com.umeng.common.a.c.a(r2)     // Catch:{ Exception -> 0x00e3 }
            java.lang.String r1 = r1.getString(r2)     // Catch:{ Exception -> 0x00e3 }
            r2 = 1
            r0 = r16
            android.widget.Toast r1 = android.widget.Toast.makeText(r0, r1, r2)     // Catch:{ Exception -> 0x00e3 }
            r1.show()     // Catch:{ Exception -> 0x00e3 }
            r1 = 0
        L_0x007e:
            return r1
        L_0x007f:
            com.umeng.common.net.DownloadingService$b r1 = new com.umeng.common.net.DownloadingService$b     // Catch:{ Exception -> 0x00e3 }
            com.umeng.common.net.a$a r4 = r14.e     // Catch:{ Exception -> 0x00e3 }
            int r6 = r14.d     // Catch:{ Exception -> 0x00e3 }
            com.umeng.common.net.DownloadingService$a r7 = r15.t     // Catch:{ Exception -> 0x00e3 }
            r2 = r15
            r3 = r16
            r1.<init>(r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x00e3 }
            r14.a = r1     // Catch:{ Exception -> 0x00e3 }
            r1.start()     // Catch:{ Exception -> 0x00e3 }
            android.app.Notification r1 = r14.b     // Catch:{ Exception -> 0x00e3 }
            r15.b(r1, r5)     // Catch:{ Exception -> 0x00e3 }
            r1 = 1
            goto L_0x007e
        L_0x0099:
            java.lang.String r2 = com.umeng.common.net.DownloadingService.o     // Catch:{ Exception -> 0x00e3 }
            java.lang.String r3 = "Receive action do play click."
            com.umeng.common.Log.c(r2, r3)     // Catch:{ Exception -> 0x00e3 }
            r2 = 1
            r1.a(r2)     // Catch:{ Exception -> 0x00e3 }
            r1 = 0
            r14.a = r1     // Catch:{ Exception -> 0x00e3 }
            android.app.Notification r1 = r14.b     // Catch:{ Exception -> 0x00e3 }
            r15.a(r1, r5)     // Catch:{ Exception -> 0x00e3 }
            r1 = 1
            goto L_0x007e
        L_0x00ae:
            java.lang.String r3 = "cancel"
            boolean r2 = r3.equals(r2)     // Catch:{ Exception -> 0x00e3 }
            if (r2 == 0) goto L_0x00e7
            java.lang.String r2 = com.umeng.common.net.DownloadingService.o     // Catch:{ Exception -> 0x00e3 }
            java.lang.String r3 = "Receive action do stop click."
            com.umeng.common.Log.c(r2, r3)     // Catch:{ Exception -> 0x00e3 }
            if (r1 == 0) goto L_0x00c8
            r2 = 2
            r1.a(r2)     // Catch:{ Exception -> 0x00de, all -> 0x00e9 }
        L_0x00c3:
            r15.a(r5)     // Catch:{ Exception -> 0x00e3 }
        L_0x00c6:
            r1 = 1
            goto L_0x007e
        L_0x00c8:
            long[] r1 = r14.f     // Catch:{ Exception -> 0x00de, all -> 0x00e9 }
            r2 = 0
            r8 = r1[r2]     // Catch:{ Exception -> 0x00de, all -> 0x00e9 }
            long[] r1 = r14.f     // Catch:{ Exception -> 0x00de, all -> 0x00e9 }
            r2 = 1
            r10 = r1[r2]     // Catch:{ Exception -> 0x00de, all -> 0x00e9 }
            long[] r1 = r14.f     // Catch:{ Exception -> 0x00de, all -> 0x00e9 }
            r2 = 2
            r12 = r1[r2]     // Catch:{ Exception -> 0x00de, all -> 0x00e9 }
            com.umeng.common.net.a$a r7 = r14.e     // Catch:{ Exception -> 0x00de, all -> 0x00e9 }
            r6 = r15
            r6.a(r7, r8, r10, r12)     // Catch:{ Exception -> 0x00de, all -> 0x00e9 }
            goto L_0x00c3
        L_0x00de:
            r1 = move-exception
            r15.a(r5)     // Catch:{ Exception -> 0x00e3 }
            goto L_0x00c6
        L_0x00e3:
            r1 = move-exception
            r1.printStackTrace()
        L_0x00e7:
            r1 = 0
            goto L_0x007e
        L_0x00e9:
            r1 = move-exception
            r15.a(r5)     // Catch:{ Exception -> 0x00e3 }
            throw r1     // Catch:{ Exception -> 0x00e3 }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.common.net.DownloadingService.a(android.content.Context, android.content.Intent):boolean");
    }

    private class e extends AsyncTask<String, Void, Integer> {
        public int a;
        public String b;
        private a.C0004a d;

        public e(int i, a.C0004a aVar, String str) {
            this.a = i;
            this.d = aVar;
            this.b = str;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Integer doInBackground(String... strArr) {
            int a2 = DeltaUpdate.a(strArr[0], strArr[1], strArr[2]) + 1;
            new File(strArr[2]).delete();
            if (a2 != 1) {
                Log.a(DownloadingService.o, "file patch error");
            } else if (!g.a(new File(strArr[1])).equalsIgnoreCase(this.d.e)) {
                Log.a(DownloadingService.o, "file patch error");
                return 0;
            } else {
                Log.a(DownloadingService.o, "file patch success");
            }
            return Integer.valueOf(a2);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Integer num) {
            if (num.intValue() == 1) {
                Notification notification = new Notification(17301634, com.umeng.common.a.p, System.currentTimeMillis());
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.addFlags(268435456);
                intent.setDataAndType(Uri.fromFile(new File(this.b)), "application/vnd.android.package-archive");
                notification.setLatestEventInfo(DownloadingService.this.r, com.umeng.common.b.w(DownloadingService.this.r), com.umeng.common.a.p, PendingIntent.getActivity(DownloadingService.this.r, 0, intent, 134217728));
                notification.flags = 16;
                DownloadingService.this.p.notify(this.a + 1, notification);
                if (DownloadingService.b(DownloadingService.this.r)) {
                    DownloadingService.this.p.cancel(this.a + 1);
                    DownloadingService.this.r.startActivity(intent);
                }
                Bundle bundle = new Bundle();
                bundle.putString(DownloadingService.l, this.b);
                Message obtain = Message.obtain();
                obtain.what = 5;
                obtain.arg1 = 1;
                obtain.arg2 = this.a;
                obtain.setData(bundle);
                try {
                    if (DownloadingService.v.get(this.d) != null) {
                        ((Messenger) DownloadingService.v.get(this.d)).send(obtain);
                    }
                    DownloadingService.this.a(this.a);
                } catch (RemoteException e) {
                    DownloadingService.this.a(this.a);
                }
            } else {
                DownloadingService.this.p.cancel(this.a + 1);
                Bundle bundle2 = new Bundle();
                bundle2.putString(DownloadingService.l, this.b);
                Message obtain2 = Message.obtain();
                obtain2.what = 5;
                obtain2.arg1 = 3;
                obtain2.arg2 = this.a;
                obtain2.setData(bundle2);
                try {
                    if (DownloadingService.v.get(this.d) != null) {
                        ((Messenger) DownloadingService.v.get(this.d)).send(obtain2);
                    }
                    DownloadingService.this.a(this.a);
                } catch (RemoteException e2) {
                    DownloadingService.this.a(this.a);
                }
            }
        }
    }
}
