package com.umeng.common.net;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import com.umeng.common.Log;
import com.umeng.common.net.a;

/* compiled from: DownloadAgent */
class b implements ServiceConnection {
    final /* synthetic */ a a;

    b(a aVar) {
        this.a = aVar;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        Log.c(a.b, "ServiceConnection.onServiceConnected");
        Messenger unused = this.a.e = new Messenger(iBinder);
        if (this.a.d != null) {
            this.a.d.a();
        }
        try {
            Message obtain = Message.obtain((Handler) null, 4);
            a.C0004a aVar = new a.C0004a(this.a.f, this.a.g, this.a.h);
            aVar.d = this.a.i;
            aVar.e = this.a.j;
            aVar.f = this.a.k;
            aVar.g = this.a.l;
            obtain.setData(aVar.a());
            obtain.replyTo = this.a.a;
            this.a.e.send(obtain);
        } catch (RemoteException e) {
        }
    }

    public void onServiceDisconnected(ComponentName componentName) {
        Log.c(a.b, "ServiceConnection.onServiceDisconnected");
        Messenger unused = this.a.e = null;
    }
}
