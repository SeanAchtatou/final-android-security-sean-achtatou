package com.umeng.common.net;

import android.os.AsyncTask;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.umeng.common.Log;
import com.umeng.common.net.o;

/* compiled from: ReportClient */
public class m extends r {
    private static final String a = m.class.getName();

    /* compiled from: ReportClient */
    public interface a {
        void a();

        void a(o.a aVar);
    }

    public o.a a(n nVar) {
        o oVar = (o) a(nVar, o.class);
        return oVar == null ? o.a.FAIL : oVar.a;
    }

    public void a(n nVar, a aVar) {
        try {
            new b(nVar, aVar).execute(new Integer[0]);
        } catch (Exception e) {
            Log.b(a, PoiTypeDef.All, e);
            if (aVar != null) {
                aVar.a(o.a.FAIL);
            }
        }
    }

    /* compiled from: ReportClient */
    private class b extends AsyncTask<Integer, Integer, o.a> {
        private n b;
        private a c;

        public b(n nVar, a aVar) {
            this.b = nVar;
            this.c = aVar;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            if (this.c != null) {
                this.c.a();
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(o.a aVar) {
            if (this.c != null) {
                this.c.a(aVar);
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public o.a doInBackground(Integer... numArr) {
            return m.this.a(this.b);
        }
    }
}
