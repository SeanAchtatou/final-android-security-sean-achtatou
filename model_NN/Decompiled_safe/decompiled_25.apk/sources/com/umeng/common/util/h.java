package com.umeng.common.util;

import com.amap.mapapi.poisearch.PoiTypeDef;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Set;

/* compiled from: NetUtil */
public class h {
    public static String a(Map<String, Object> map, String str) {
        if (map == null || map.isEmpty()) {
            return str;
        }
        StringBuilder sb = new StringBuilder(str);
        Set<String> keySet = map.keySet();
        if (!str.endsWith("?")) {
            sb.append("?");
        }
        for (String next : keySet) {
            sb.append(URLEncoder.encode(next) + "=" + URLEncoder.encode(map.get(next) == null ? PoiTypeDef.All : map.get(next).toString()) + "&");
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }
}
