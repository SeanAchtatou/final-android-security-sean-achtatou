package com.mapabc.minimap.map.vmap;

import android.content.Context;
import com.amap.mapapi.map.be;
import java.nio.ByteBuffer;

public class NativeMap {
    public static final String MINIMAP_VERSION = "minimapv320";
    byte[] a;
    private int b;

    private static native int nativeCreate();

    private static native void nativeFinalizer(int i);

    private static native void nativeGetLabelStruct(int i, byte[] bArr);

    private static native int nativeGetMapAngle(int i);

    private static native int nativeGetMapCenterX(int i);

    private static native int nativeGetMapCenterY(int i);

    private static native int nativeGetMapHeight(int i);

    private static native int nativeGetMapLevel(int i);

    private static native int nativeGetMapWidth(int i);

    private static native void nativeGetScreenGridNames(int i, byte[] bArr);

    private static native void nativeInitMap(int i, byte[] bArr, int i2, int i3);

    private static native boolean nativePaint(int i, int i2, byte[] bArr, int i3);

    private static native void nativePx20ToScreen(int i, int i2, int i3, be beVar);

    private static native void nativePxToScreen(int i, int i2, int i3, be beVar);

    private static native void nativeResetLabelManager(int i);

    private static native void nativeScreenToPx(int i, int i2, int i3, be beVar);

    private static native void nativeScreenToPx20(int i, int i2, int i3, be beVar);

    private static native void nativeSetMapLevel(int i, int i2);

    private static native void nativeSetMapParameter(int i, int i2, int i3, int i4, int i5);

    static {
        try {
            System.loadLibrary(MINIMAP_VERSION);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public NativeMap(Context context) {
        this.b = 0;
        this.a = ByteBuffer.allocate(48000).array();
        this.b = nativeCreate();
    }

    public NativeMap() {
        this.b = 0;
        this.a = ByteBuffer.allocate(48000).array();
        this.b = nativeCreate();
    }

    public void initMap(byte[] bArr, int i, int i2) {
        nativeInitMap(this.b, bArr, i, i2);
    }

    public byte[] getLabelBuffer() {
        return this.a;
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        nativeFinalizer(this.b);
        this.b = 0;
        this.a = null;
    }

    public void resetLabelManager() {
        nativeResetLabelManager(this.b);
    }

    public void setDrawMode(int i) {
    }

    public boolean paint(NativeMapEngine nativeMapEngine, byte[] bArr, int i) {
        return nativePaint(nativeMapEngine.b, this.b, bArr, i);
    }

    public void setMapParameter(int i, int i2, int i3, int i4) {
        nativeSetMapParameter(this.b, i, i2, i3, i4);
    }

    public void px20ToScreen(int i, int i2, be beVar) {
        nativePx20ToScreen(this.b, i, i2, beVar);
    }

    public void pxToScreen(int i, int i2, be beVar) {
        nativePxToScreen(this.b, i, i2, beVar);
    }

    public void ScreenToPx20(int i, int i2, be beVar) {
        nativeScreenToPx20(this.b, i, i2, beVar);
    }

    public void ScreenToPx(int i, int i2, be beVar) {
        nativeScreenToPx(this.b, i, i2, beVar);
    }

    public int getMapWidth() {
        return nativeGetMapWidth(this.b);
    }

    public int getMapHeight() {
        return nativeGetMapHeight(this.b);
    }

    public int getMapAngle() {
        return nativeGetMapAngle(this.b);
    }

    public int getMapCenterX() {
        return nativeGetMapCenterX(this.b);
    }

    public int getMapCenterY() {
        return nativeGetMapCenterY(this.b);
    }

    public int getMapLevel() {
        return nativeGetMapLevel(this.b);
    }

    public void setMapLevel(int i) {
        nativeSetMapLevel(this.b, i);
    }

    public void getLabelStruct(byte[] bArr) {
        nativeGetLabelStruct(this.b, bArr);
    }

    /* JADX WARN: Type inference failed for: r4v79, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void paintLables(com.mapabc.minimap.map.vmap.NativeMapEngine r31, android.graphics.Canvas r32, int r33) {
        /*
            r30 = this;
            android.graphics.Paint r15 = new android.graphics.Paint
            r15.<init>()
            java.util.ArrayList r16 = new java.util.ArrayList
            r16.<init>()
            r17 = 7
            r18 = 4
            byte[] r6 = r30.getLabelBuffer()
            r3 = 0
            short r7 = com.amap.mapapi.core.c.b(r6, r3)
            r4 = 2
            r3 = 0
            r5 = r3
            r3 = r4
        L_0x001b:
            if (r5 >= r7) goto L_0x012f
            com.mapabc.minimap.map.vmap.b r8 = new com.mapabc.minimap.map.vmap.b
            r8.<init>()
            r0 = r16
            r0.add(r8)
            int r4 = r3 + 1
            byte r9 = r6[r3]
            java.lang.StringBuffer r10 = new java.lang.StringBuffer
            r10.<init>()
            r3 = 0
        L_0x0031:
            if (r3 >= r9) goto L_0x0040
            short r11 = com.amap.mapapi.core.c.b(r6, r4)
            char r11 = (char) r11
            r10.append(r11)
            int r4 = r4 + 2
            int r3 = r3 + 1
            goto L_0x0031
        L_0x0040:
            java.lang.String r3 = r10.toString()
            r8.a = r3
            int r3 = r4 + 1
            byte r4 = r6[r4]
            r8.p = r4
            int r4 = com.amap.mapapi.core.c.a(r6, r3)
            r9 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r4 = r4 | r9
            r8.l = r4
            int r3 = r3 + 4
            int r4 = com.amap.mapapi.core.c.a(r6, r3)
            r9 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r4 = r4 | r9
            r8.m = r4
            int r3 = r3 + 4
            short r4 = com.amap.mapapi.core.c.b(r6, r3)
            r8.b = r4
            int r3 = r3 + 4
            short r4 = com.amap.mapapi.core.c.b(r6, r3)
            r8.c = r4
            int r4 = r3 + 4
            int r3 = r4 + 1
            byte r4 = r6[r4]
            r8.n = r4
            int r4 = r8.n
            if (r4 != 0) goto L_0x0088
            short r4 = com.amap.mapapi.core.c.b(r6, r3)
            r8.d = r4
            int r3 = r3 + 4
        L_0x0084:
            int r4 = r5 + 1
            r5 = r4
            goto L_0x001b
        L_0x0088:
            int r4 = r8.n
            r9 = 1
            if (r4 != r9) goto L_0x0084
            int r4 = r3 + 1
            byte r3 = r6[r3]
            r8.g = r3
            int r3 = com.amap.mapapi.core.c.a(r6, r4)
            r8.j = r3
            int r3 = r4 + 4
            int r4 = com.amap.mapapi.core.c.a(r6, r3)
            r8.k = r4
            int r3 = r3 + 4
            int r4 = r3 + 1
            byte r3 = r6[r3]
            r8.o = r3
            short r3 = com.amap.mapapi.core.c.b(r6, r4)
            r8.e = r3
            int r3 = r4 + 2
            short r4 = com.amap.mapapi.core.c.b(r6, r3)
            r8.f = r4
            int r3 = r3 + 2
            int r4 = r8.o
            if (r4 <= 0) goto L_0x00d7
            int r4 = r8.g
            r0 = r31
            android.graphics.Bitmap r4 = r0.getIconBitmap(r4)
            if (r4 == 0) goto L_0x00d7
            int r9 = r8.b
            int r9 = r9 + -6
            float r9 = (float) r9
            int r10 = r8.c
            int r10 = r10 + -6
            float r10 = (float) r10
            r11 = 0
            r0 = r32
            r0.drawBitmap(r4, r9, r10, r11)
        L_0x00d7:
            int r4 = r8.o
            if (r4 != 0) goto L_0x00ed
            int r4 = r8.b
            int r9 = r8.e
            int r9 = r9 >> 1
            int r4 = r4 - r9
            r8.h = r4
            int r4 = r8.c
            int r9 = r8.f
            int r9 = r9 >> 1
            int r4 = r4 - r9
            r8.i = r4
        L_0x00ed:
            int r4 = r8.o
            r9 = 1
            if (r4 != r9) goto L_0x0100
            int r4 = r8.b
            int r9 = r8.e
            int r9 = r9 >> 1
            int r4 = r4 - r9
            r8.h = r4
            int r4 = r8.c
            r8.i = r4
            goto L_0x0084
        L_0x0100:
            int r4 = r8.o
            r9 = 2
            if (r4 != r9) goto L_0x0116
            int r4 = r8.b
            int r4 = r4 + 6
            r8.h = r4
            int r4 = r8.c
            int r9 = r8.f
            int r9 = r9 >> 1
            int r4 = r4 - r9
            r8.i = r4
            goto L_0x0084
        L_0x0116:
            int r4 = r8.o
            r9 = 3
            if (r4 != r9) goto L_0x0084
            int r4 = r8.b
            int r4 = r4 + -6
            int r9 = r8.e
            int r4 = r4 - r9
            r8.h = r4
            int r4 = r8.c
            int r9 = r8.f
            int r9 = r9 >> 1
            int r4 = r4 - r9
            r8.i = r4
            goto L_0x0084
        L_0x012f:
            r3 = 1
            r15.setAntiAlias(r3)
            android.graphics.Paint$FontMetrics r19 = new android.graphics.Paint$FontMetrics
            r19.<init>()
            android.graphics.Matrix r20 = new android.graphics.Matrix
            r20.<init>()
            android.graphics.Matrix r21 = new android.graphics.Matrix
            r21.<init>()
            r6 = 0
            r5 = 0
            r3 = 2
            float[] r0 = new float[r3]
            r22 = r0
            r3 = 2
            float[] r0 = new float[r3]
            r23 = r0
            r3 = 5
            int[][] r0 = new int[r3][]
            r24 = r0
            r3 = 0
            r4 = 2
            int[] r4 = new int[r4]
            r4 = {-1, -1} // fill-array
            r24[r3] = r4
            r3 = 1
            r4 = 2
            int[] r4 = new int[r4]
            r4 = {1, -1} // fill-array
            r24[r3] = r4
            r3 = 2
            r4 = 2
            int[] r4 = new int[r4]
            r4 = {-1, 1} // fill-array
            r24[r3] = r4
            r3 = 3
            r4 = 2
            int[] r4 = new int[r4]
            r4 = {1, 1} // fill-array
            r24[r3] = r4
            r3 = 4
            r4 = 2
            int[] r4 = new int[r4]
            r4 = {0, 0} // fill-array
            r24[r3] = r4
            int r25 = r16.size()
            r3 = 0
            r14 = r3
        L_0x0186:
            r3 = 5
            if (r14 >= r3) goto L_0x02dc
            r3 = 2
            r0 = r33
            if (r0 >= r3) goto L_0x019b
            r3 = 1
            if (r14 == r3) goto L_0x0197
            r3 = 2
            if (r14 == r3) goto L_0x0197
            r3 = 3
            if (r14 != r3) goto L_0x019b
        L_0x0197:
            int r3 = r14 + 1
            r14 = r3
            goto L_0x0186
        L_0x019b:
            r3 = 4
            if (r14 >= r3) goto L_0x0214
            r3 = 0
            r15.setAntiAlias(r3)
            r3 = 1
            r15.setFakeBoldText(r3)
        L_0x01a6:
            r3 = r24[r14]
            r4 = 0
            r26 = r3[r4]
            r3 = r24[r14]
            r4 = 1
            r27 = r3[r4]
            r3 = 0
            r13 = r3
        L_0x01b2:
            r0 = r25
            if (r13 >= r0) goto L_0x0197
            r0 = r16
            java.lang.Object r3 = r0.get(r13)
            com.mapabc.minimap.map.vmap.b r3 = (com.mapabc.minimap.map.vmap.b) r3
            int r4 = r3.l
            r7 = 4
            if (r14 >= r7) goto L_0x01c5
            int r4 = r3.m
        L_0x01c5:
            int r7 = r3.p
            float r7 = (float) r7
            r15.setTextSize(r7)
            r15.setColor(r4)
            r0 = r19
            r15.getFontMetrics(r0)
            r0 = r19
            float r4 = r0.bottom
            r0 = r19
            float r7 = r0.top
            float r4 = r4 - r7
            int r7 = (int) r4
            int r4 = r3.n
            r8 = 1
            if (r4 != r8) goto L_0x021d
            android.graphics.Paint$Align r4 = android.graphics.Paint.Align.LEFT
            r15.setTextAlign(r4)
            int r4 = r3.h
            int r5 = r4 + r26
            int r4 = r3.i
            int r4 = r4 + r27
            int r6 = r3.p
            int r4 = r4 + r6
        L_0x01f2:
            r20.reset()
            int r6 = r3.d
            int r8 = r3.n
            if (r8 <= 0) goto L_0x0277
            java.lang.String r6 = r3.a
            int r9 = r6.length()
            r0 = r17
            if (r9 > r0) goto L_0x022f
            java.lang.String r3 = r3.a
            float r6 = (float) r5
            float r7 = (float) r4
            r0 = r32
            r0.drawText(r3, r6, r7, r15)
        L_0x020e:
            int r3 = r13 + 1
            r13 = r3
            r6 = r5
            r5 = r4
            goto L_0x01b2
        L_0x0214:
            r3 = 1
            r15.setAntiAlias(r3)
            r3 = 0
            r15.setFakeBoldText(r3)
            goto L_0x01a6
        L_0x021d:
            int r4 = r3.n
            if (r4 != 0) goto L_0x02dd
            android.graphics.Paint$Align r4 = android.graphics.Paint.Align.CENTER
            r15.setTextAlign(r4)
            int r4 = r3.b
            int r5 = r4 + r26
            int r4 = r3.c
            int r4 = r4 + r27
            goto L_0x01f2
        L_0x022f:
            int r6 = r9 / r17
            int r7 = r9 % r17
            if (r7 <= 0) goto L_0x0237
            int r6 = r6 + 1
        L_0x0237:
            int r7 = r9 % r6
            if (r7 != 0) goto L_0x0272
            int r7 = r9 / r6
        L_0x023d:
            r10 = 0
            r8 = 0
            r11 = r10
            r12 = r4
            r10 = r8
        L_0x0242:
            if (r10 >= r6) goto L_0x020e
            int r8 = r11 + r7
            if (r8 < r9) goto L_0x0249
            r8 = r9
        L_0x0249:
            java.lang.String r0 = r3.a
            r28 = r0
            r0 = r28
            java.lang.String r11 = r0.substring(r11, r8)
            int r0 = r3.h
            r28 = r0
            r0 = r28
            float r0 = (float) r0
            r28 = r0
            float r0 = (float) r12
            r29 = r0
            r0 = r32
            r1 = r28
            r2 = r29
            r0.drawText(r11, r1, r2, r15)
            int r11 = r3.p
            int r11 = r11 + r18
            int r11 = r11 + r12
            int r10 = r10 + 1
            r12 = r11
            r11 = r8
            goto L_0x0242
        L_0x0272:
            int r7 = r9 / r6
            int r7 = r7 + 1
            goto L_0x023d
        L_0x0277:
            r8 = 1
            r0 = r33
            if (r0 < r8) goto L_0x020e
            r8 = -45
            if (r6 >= r8) goto L_0x02d5
            int r6 = r6 + 90
        L_0x0282:
            int r8 = -r6
            float r8 = (float) r8
            float r9 = (float) r5
            float r10 = (float) r4
            r0 = r20
            r0.postRotate(r8, r9, r10)
            r0 = r19
            float r8 = r0.top
            int r8 = (int) r8
            int r7 = r7 / 2
            int r7 = r7 + r8
            int r4 = r4 - r7
            r7 = 0
            float r5 = (float) r5
            r23[r7] = r5
            r5 = 1
            float r4 = (float) r4
            r23[r5] = r4
            r0 = r20
            r1 = r22
            r2 = r23
            r0.mapPoints(r1, r2)
            r4 = 0
            r4 = r22[r4]
            int r5 = (int) r4
            r4 = 1
            r4 = r22[r4]
            int r4 = (int) r4
            r32.save()
            r0 = r32
            r1 = r21
            r0.getMatrix(r1)
            int r6 = -r6
            float r6 = (float) r6
            float r7 = (float) r5
            float r8 = (float) r4
            r0 = r21
            r0.preRotate(r6, r7, r8)
            r0 = r32
            r1 = r21
            r0.setMatrix(r1)
            java.lang.String r3 = r3.a
            float r6 = (float) r5
            float r7 = (float) r4
            r0 = r32
            r0.drawText(r3, r6, r7, r15)
            r32.restore()
            goto L_0x020e
        L_0x02d5:
            r8 = 45
            if (r6 <= r8) goto L_0x0282
            int r6 = r6 + -90
            goto L_0x0282
        L_0x02dc:
            return
        L_0x02dd:
            r4 = r5
            r5 = r6
            goto L_0x01f2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mapabc.minimap.map.vmap.NativeMap.paintLables(com.mapabc.minimap.map.vmap.NativeMapEngine, android.graphics.Canvas, int):void");
    }

    public boolean paintMap(NativeMapEngine nativeMapEngine, int i) {
        return paint(nativeMapEngine, this.a, i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.<init>(byte[], int, int):void}
     arg types: [byte[], int, byte]
     candidates:
      ClspMth{java.lang.String.<init>(int[], int, int):void}
      ClspMth{java.lang.String.<init>(char[], int, int):void}
      ClspMth{java.lang.String.<init>(byte[], int, int):void} */
    public String[] getScreenGridNames() {
        byte[] bArr = new byte[2048];
        nativeGetScreenGridNames(this.b, bArr);
        int i = 1;
        byte b2 = bArr[0];
        String[] strArr = new String[b2];
        for (int i2 = 0; i2 < b2; i2++) {
            int i3 = i + 1;
            byte b3 = bArr[i];
            strArr[i2] = new String(bArr, i3, (int) b3);
            i = b3 + i3;
        }
        return strArr;
    }
}
