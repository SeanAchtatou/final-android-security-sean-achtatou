package com.mapabc.minimap.map.vmap;

import android.graphics.Point;
import com.amap.mapapi.map.MapView;
import com.amap.mapapi.map.be;

public class VMapProjection {
    public static final double EarthCircumferenceInMeters = 4.007501668557849E7d;
    public static final int EarthRadiusInMeters = 6378137;
    public static final int MAXZOOMLEVEL = 20;
    public static final double MaxLatitude = 85.0511287798d;
    public static final double MaxLongitude = 180.0d;
    public static final double MinLatitude = -85.0511287798d;
    public static final double MinLongitude = -180.0d;
    public static final int PixelsPerTile = 256;
    public static final int TileSplitLevel = 0;

    public static double Clip(double d, double d2, double d3) {
        return Math.min(Math.max(d, d2), d3);
    }

    public static be LatLongToPixels(int i, int i2, int i3) {
        return LatLongToPixels(((double) i2) / 3600000.0d, ((double) i) / 3600000.0d, i3);
    }

    public static be LatLongToPixels(double d, double d2, int i) {
        be beVar = new be();
        double sin = Math.sin((Clip(d, -85.0511287798d, 85.0511287798d) * 3.141592653589793d) / 180.0d);
        long j = 256 << i;
        double d3 = 4.007501668557849E7d / ((double) j);
        beVar.a = (int) Clip((((((Clip(d2, -180.0d, 180.0d) * 3.141592653589793d) / 180.0d) * 6378137.0d) + 2.0037508342789244E7d) / d3) + 0.5d, 0.0d, (double) (j - 1));
        beVar.b = (int) Clip((((double) ((long) (2.0037508342789244E7d - (3189068.0d * Math.log((1.0d + sin) / (1.0d - sin)))))) / d3) + 0.5d, 0.0d, (double) (j - 1));
        return beVar;
    }

    public static be PixelsToPixels(long j, long j2, int i, int i2) {
        int i3 = i2 - i;
        if (i3 > 0) {
            j >>= i3;
            j2 >>= i3;
        } else if (i3 < 0) {
            j <<= i3;
            j2 <<= i3;
        }
        be beVar = new be();
        beVar.a = (int) j;
        beVar.b = (int) j2;
        return beVar;
    }

    public static a PixelsToLatLong(long j, long j2, int i) {
        a aVar = new a();
        double d = 4.007501668557849E7d / ((double) ((1 << i) * 256));
        aVar.b = 1.5707963267948966d - (Math.atan(Math.exp((-(2.0037508342789244E7d - (d * ((double) j2)))) / 6378137.0d)) * 2.0d);
        aVar.b *= 57.29577951308232d;
        aVar.a = ((((double) j) * d) - 2.0037508342789244E7d) / 6378137.0d;
        aVar.a *= 57.29577951308232d;
        return aVar;
    }

    public static be PixelsToTile(int i, int i2) {
        be beVar = new be();
        beVar.a = i / 256;
        beVar.b = i2 / 256;
        return beVar;
    }

    public static String TileToQuadKey(int i, int i2, int i3) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i4 = i3 + 0; i4 > 0; i4--) {
            long j = (long) (1 << (i4 - 1));
            int i5 = 0;
            if ((((long) i) & j) != 0) {
                i5 = 1;
            }
            if ((j & ((long) i2)) != 0) {
                i5 += 2;
            }
            stringBuffer.append(i5);
        }
        return stringBuffer.toString();
    }

    public static be QuadKeyToTitle(String str) {
        int i = 0;
        int length = str.length();
        int i2 = 0;
        for (int i3 = 1; i3 <= length; i3++) {
            int i4 = 1 << (length - i3);
            switch (str.charAt(i3 - 1)) {
                case MapView.LayoutParams.TOP:
                    i2 &= i4 ^ -1;
                    i &= i4 ^ -1;
                    break;
                case '1':
                    i2 |= i4;
                    i &= i4 ^ -1;
                    break;
                case '2':
                    i2 &= i4 ^ -1;
                    i |= i4;
                    break;
                case MapView.LayoutParams.TOP_LEFT:
                    i2 |= i4;
                    i |= i4;
                    break;
            }
        }
        be beVar = new be();
        beVar.a = i2;
        beVar.b = i;
        return beVar;
    }

    public static Point QuadKeyToTile(String str) {
        int i = 0;
        int length = str.length();
        int i2 = 0;
        for (int i3 = 1; i3 <= length; i3++) {
            int i4 = 1 << (length - i3);
            switch (str.charAt(i3 - 1)) {
                case MapView.LayoutParams.TOP:
                    i2 &= i4 ^ -1;
                    i &= i4 ^ -1;
                    break;
                case '1':
                    i2 |= i4;
                    i &= i4 ^ -1;
                    break;
                case '2':
                    i2 &= i4 ^ -1;
                    i |= i4;
                    break;
                case MapView.LayoutParams.TOP_LEFT:
                    i2 |= i4;
                    i |= i4;
                    break;
            }
        }
        Point point = new Point();
        point.x = i2;
        point.y = i;
        return point;
    }
}
