package com.novell.sasl.client;

import com.umeng.common.util.e;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;

class DirectiveList {
    private static final int STATE_LOOKING_FOR_COMMA = 6;
    private static final int STATE_LOOKING_FOR_DIRECTIVE = 2;
    private static final int STATE_LOOKING_FOR_EQUALS = 4;
    private static final int STATE_LOOKING_FOR_FIRST_DIRECTIVE = 1;
    private static final int STATE_LOOKING_FOR_VALUE = 5;
    private static final int STATE_NO_UTF8_SUPPORT = 9;
    private static final int STATE_SCANNING_NAME = 3;
    private static final int STATE_SCANNING_QUOTED_STRING_VALUE = 7;
    private static final int STATE_SCANNING_TOKEN_VALUE = 8;
    private String m_curName;
    private int m_curPos = 0;
    private ArrayList m_directiveList = new ArrayList(10);
    private String m_directives;
    private int m_errorPos = -1;
    private int m_scanStart = 0;
    private int m_state = 1;

    DirectiveList(byte[] directives) {
        try {
            this.m_directives = new String(directives, e.f);
        } catch (UnsupportedEncodingException e) {
            this.m_state = 9;
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:86:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void parseDirectives() throws org.apache.harmony.javax.security.sasl.SaslException {
        /*
            r13 = this;
            r12 = 34
            r11 = 6
            r10 = 5
            r9 = 2
            r8 = 0
            r4 = 0
            r2 = 0
            java.lang.String r1 = "<no name>"
            int r5 = r13.m_state
            r6 = 9
            if (r5 != r6) goto L_0x0018
            org.apache.harmony.javax.security.sasl.SaslException r5 = new org.apache.harmony.javax.security.sasl.SaslException
            java.lang.String r6 = "No UTF-8 support on platform"
            r5.<init>(r6)
            throw r5
        L_0x0018:
            r3 = 0
        L_0x0019:
            int r5 = r13.m_curPos
            java.lang.String r6 = r13.m_directives
            int r6 = r6.length()
            if (r5 < r6) goto L_0x002b
        L_0x0023:
            if (r4 != 0) goto L_0x002a
            int r5 = r13.m_state
            switch(r5) {
                case 1: goto L_0x002a;
                case 2: goto L_0x013d;
                case 3: goto L_0x0145;
                case 4: goto L_0x0145;
                case 5: goto L_0x0145;
                case 6: goto L_0x002a;
                case 7: goto L_0x014d;
                case 8: goto L_0x0138;
                default: goto L_0x002a;
            }
        L_0x002a:
            return
        L_0x002b:
            java.lang.String r5 = r13.m_directives
            int r6 = r13.m_curPos
            char r0 = r5.charAt(r6)
            int r5 = r13.m_state
            switch(r5) {
                case 1: goto L_0x0042;
                case 2: goto L_0x0042;
                case 3: goto L_0x0062;
                case 4: goto L_0x0099;
                case 5: goto L_0x00b2;
                case 6: goto L_0x011e;
                case 7: goto L_0x010b;
                case 8: goto L_0x00e1;
                default: goto L_0x0038;
            }
        L_0x0038:
            if (r4 != 0) goto L_0x0023
            r3 = r0
            int r5 = r13.m_curPos
            int r5 = r5 + 1
            r13.m_curPos = r5
            goto L_0x0019
        L_0x0042:
            boolean r5 = r13.isWhiteSpace(r0)
            if (r5 != 0) goto L_0x0038
            boolean r5 = r13.isValidTokenChar(r0)
            if (r5 == 0) goto L_0x0056
            int r5 = r13.m_curPos
            r13.m_scanStart = r5
            r5 = 3
            r13.m_state = r5
            goto L_0x0038
        L_0x0056:
            int r5 = r13.m_curPos
            r13.m_errorPos = r5
            org.apache.harmony.javax.security.sasl.SaslException r5 = new org.apache.harmony.javax.security.sasl.SaslException
            java.lang.String r6 = "Parse error: Invalid name character"
            r5.<init>(r6)
            throw r5
        L_0x0062:
            boolean r5 = r13.isValidTokenChar(r0)
            if (r5 != 0) goto L_0x0038
            boolean r5 = r13.isWhiteSpace(r0)
            if (r5 == 0) goto L_0x007c
            java.lang.String r5 = r13.m_directives
            int r6 = r13.m_scanStart
            int r7 = r13.m_curPos
            java.lang.String r1 = r5.substring(r6, r7)
            r5 = 4
            r13.m_state = r5
            goto L_0x0038
        L_0x007c:
            r5 = 61
            if (r5 != r0) goto L_0x008d
            java.lang.String r5 = r13.m_directives
            int r6 = r13.m_scanStart
            int r7 = r13.m_curPos
            java.lang.String r1 = r5.substring(r6, r7)
            r13.m_state = r10
            goto L_0x0038
        L_0x008d:
            int r5 = r13.m_curPos
            r13.m_errorPos = r5
            org.apache.harmony.javax.security.sasl.SaslException r5 = new org.apache.harmony.javax.security.sasl.SaslException
            java.lang.String r6 = "Parse error: Invalid name character"
            r5.<init>(r6)
            throw r5
        L_0x0099:
            boolean r5 = r13.isWhiteSpace(r0)
            if (r5 != 0) goto L_0x0038
            r5 = 61
            if (r5 != r0) goto L_0x00a6
            r13.m_state = r10
            goto L_0x0038
        L_0x00a6:
            int r5 = r13.m_curPos
            r13.m_errorPos = r5
            org.apache.harmony.javax.security.sasl.SaslException r5 = new org.apache.harmony.javax.security.sasl.SaslException
            java.lang.String r6 = "Parse error: Expected equals sign '='."
            r5.<init>(r6)
            throw r5
        L_0x00b2:
            boolean r5 = r13.isWhiteSpace(r0)
            if (r5 != 0) goto L_0x0038
            if (r12 != r0) goto L_0x00c5
            int r5 = r13.m_curPos
            int r5 = r5 + 1
            r13.m_scanStart = r5
            r5 = 7
            r13.m_state = r5
            goto L_0x0038
        L_0x00c5:
            boolean r5 = r13.isValidTokenChar(r0)
            if (r5 == 0) goto L_0x00d5
            int r5 = r13.m_curPos
            r13.m_scanStart = r5
            r5 = 8
            r13.m_state = r5
            goto L_0x0038
        L_0x00d5:
            int r5 = r13.m_curPos
            r13.m_errorPos = r5
            org.apache.harmony.javax.security.sasl.SaslException r5 = new org.apache.harmony.javax.security.sasl.SaslException
            java.lang.String r6 = "Parse error: Unexpected character"
            r5.<init>(r6)
            throw r5
        L_0x00e1:
            boolean r5 = r13.isValidTokenChar(r0)
            if (r5 != 0) goto L_0x0038
            boolean r5 = r13.isWhiteSpace(r0)
            if (r5 == 0) goto L_0x00f4
            r13.addDirective(r1, r8)
            r13.m_state = r11
            goto L_0x0038
        L_0x00f4:
            r5 = 44
            if (r5 != r0) goto L_0x00ff
            r13.addDirective(r1, r8)
            r13.m_state = r9
            goto L_0x0038
        L_0x00ff:
            int r5 = r13.m_curPos
            r13.m_errorPos = r5
            org.apache.harmony.javax.security.sasl.SaslException r5 = new org.apache.harmony.javax.security.sasl.SaslException
            java.lang.String r6 = "Parse error: Invalid value character"
            r5.<init>(r6)
            throw r5
        L_0x010b:
            r5 = 92
            if (r5 != r0) goto L_0x0110
            r2 = 1
        L_0x0110:
            if (r12 != r0) goto L_0x0038
            r5 = 92
            if (r5 == r3) goto L_0x0038
            r13.addDirective(r1, r2)
            r2 = 0
            r13.m_state = r11
            goto L_0x0038
        L_0x011e:
            boolean r5 = r13.isWhiteSpace(r0)
            if (r5 != 0) goto L_0x0038
            r5 = 44
            if (r0 != r5) goto L_0x012c
            r13.m_state = r9
            goto L_0x0038
        L_0x012c:
            int r5 = r13.m_curPos
            r13.m_errorPos = r5
            org.apache.harmony.javax.security.sasl.SaslException r5 = new org.apache.harmony.javax.security.sasl.SaslException
            java.lang.String r6 = "Parse error: Expected a comma."
            r5.<init>(r6)
            throw r5
        L_0x0138:
            r13.addDirective(r1, r8)
            goto L_0x002a
        L_0x013d:
            org.apache.harmony.javax.security.sasl.SaslException r5 = new org.apache.harmony.javax.security.sasl.SaslException
            java.lang.String r6 = "Parse error: Trailing comma."
            r5.<init>(r6)
            throw r5
        L_0x0145:
            org.apache.harmony.javax.security.sasl.SaslException r5 = new org.apache.harmony.javax.security.sasl.SaslException
            java.lang.String r6 = "Parse error: Missing value."
            r5.<init>(r6)
            throw r5
        L_0x014d:
            org.apache.harmony.javax.security.sasl.SaslException r5 = new org.apache.harmony.javax.security.sasl.SaslException
            java.lang.String r6 = "Parse error: Missing closing quote."
            r5.<init>(r6)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.novell.sasl.client.DirectiveList.parseDirectives():void");
    }

    /* access modifiers changed from: package-private */
    public boolean isValidTokenChar(char c) {
        if ((c < 0 || c > ' ') && ((c < ':' || c > '@') && ((c < '[' || c > ']') && ',' != c && '%' != c && '(' != c && ')' != c && '{' != c && '}' != c && 127 != c))) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean isWhiteSpace(char c) {
        if (9 == c || 10 == c || 13 == c || ' ' == c) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void addDirective(String name, boolean haveQuotedPair) {
        String value;
        int type;
        if (!haveQuotedPair) {
            value = this.m_directives.substring(this.m_scanStart, this.m_curPos);
        } else {
            StringBuffer valueBuf = new StringBuffer(this.m_curPos - this.m_scanStart);
            int valueIndex = 0;
            int inputIndex = this.m_scanStart;
            while (inputIndex < this.m_curPos) {
                if ('\\' == this.m_directives.charAt(inputIndex)) {
                    inputIndex++;
                }
                valueBuf.setCharAt(valueIndex, this.m_directives.charAt(inputIndex));
                valueIndex++;
                inputIndex++;
            }
            value = new String(valueBuf);
        }
        if (this.m_state == 7) {
            type = 1;
        } else {
            type = 2;
        }
        this.m_directiveList.add(new ParsedDirective(name, value, type));
    }

    /* access modifiers changed from: package-private */
    public Iterator getIterator() {
        return this.m_directiveList.iterator();
    }
}
