package com.novell.sasl.client;

import org.apache.harmony.javax.security.sasl.SaslException;

class TokenParser {
    private static final int STATE_DONE = 6;
    private static final int STATE_LOOKING_FOR_COMMA = 4;
    private static final int STATE_LOOKING_FOR_FIRST_TOKEN = 1;
    private static final int STATE_LOOKING_FOR_TOKEN = 2;
    private static final int STATE_PARSING_ERROR = 5;
    private static final int STATE_SCANNING_TOKEN = 3;
    private int m_curPos = 0;
    private int m_scanStart = 0;
    private int m_state = 1;
    private String m_tokens;

    TokenParser(String tokens) {
        this.m_tokens = tokens;
    }

    /* access modifiers changed from: package-private */
    public String parseToken() throws SaslException {
        String token = null;
        if (this.m_state == 6) {
            return null;
        }
        while (this.m_curPos < this.m_tokens.length() && token == null) {
            char currChar = this.m_tokens.charAt(this.m_curPos);
            switch (this.m_state) {
                case 1:
                case 2:
                    if (isWhiteSpace(currChar)) {
                        continue;
                    } else if (isValidTokenChar(currChar)) {
                        this.m_scanStart = this.m_curPos;
                        this.m_state = 3;
                        break;
                    } else {
                        this.m_state = 5;
                        throw new SaslException("Invalid token character at position " + this.m_curPos);
                    }
                case 3:
                    if (isValidTokenChar(currChar)) {
                        continue;
                    } else if (isWhiteSpace(currChar)) {
                        token = this.m_tokens.substring(this.m_scanStart, this.m_curPos);
                        this.m_state = 4;
                        break;
                    } else if (',' == currChar) {
                        token = this.m_tokens.substring(this.m_scanStart, this.m_curPos);
                        this.m_state = 2;
                        break;
                    } else {
                        this.m_state = 5;
                        throw new SaslException("Invalid token character at position " + this.m_curPos);
                    }
                case 4:
                    if (isWhiteSpace(currChar)) {
                        continue;
                    } else if (currChar == ',') {
                        this.m_state = 2;
                        break;
                    } else {
                        this.m_state = 5;
                        throw new SaslException("Expected a comma, found '" + currChar + "' at postion " + this.m_curPos);
                    }
            }
            this.m_curPos++;
        }
        if (token == null) {
            switch (this.m_state) {
                case 2:
                    throw new SaslException("Trialing comma");
                case 3:
                    token = this.m_tokens.substring(this.m_scanStart);
                    this.m_state = 6;
                    break;
            }
        }
        return token;
    }

    /* access modifiers changed from: package-private */
    public boolean isValidTokenChar(char c) {
        if ((c < 0 || c > ' ') && ((c < ':' || c > '@') && ((c < '[' || c > ']') && ',' != c && '%' != c && '(' != c && ')' != c && '{' != c && '}' != c && 127 != c))) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean isWhiteSpace(char c) {
        if (9 == c || 10 == c || 13 == c || ' ' == c) {
            return true;
        }
        return false;
    }
}
