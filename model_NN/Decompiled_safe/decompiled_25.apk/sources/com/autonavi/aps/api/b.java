package com.autonavi.aps.api;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import com.amap.mapapi.location.LocationManagerProxy;

final class b implements LocationListener {
    b() {
    }

    public final void onLocationChanged(Location location) {
        if (location != null && location.getProvider().equalsIgnoreCase(LocationManagerProxy.NETWORK_PROVIDER)) {
            APS.n = location;
        }
    }

    public final void onProviderDisabled(String str) {
        if (str.equals(LocationManagerProxy.NETWORK_PROVIDER)) {
            APS.n = null;
        }
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onStatusChanged(String str, int i, Bundle bundle) {
        if (str.equalsIgnoreCase(LocationManagerProxy.NETWORK_PROVIDER) && i == 0) {
            APS.n = null;
        }
    }
}
