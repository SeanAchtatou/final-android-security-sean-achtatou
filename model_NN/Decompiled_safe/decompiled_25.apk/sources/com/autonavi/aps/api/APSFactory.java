package com.autonavi.aps.api;

import android.content.Context;

public class APSFactory {
    private static IAPS a = null;

    public static IAPS getInstance(Context context) {
        return getInstance(context, null);
    }

    public static IAPS getInstance(Context context, String str) {
        if (str == null || str.length() <= 0) {
            a = APS.getInstance(context);
        }
        return a;
    }
}
