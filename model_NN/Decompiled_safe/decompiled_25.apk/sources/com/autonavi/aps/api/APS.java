package com.autonavi.aps.api;

import android.content.Context;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Looper;
import android.telephony.CellLocation;
import android.telephony.NeighboringCellInfo;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import com.amap.mapapi.location.LocationManagerProxy;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class APS implements IAPS {
    private static String a = null;
    private static String b = null;
    private static APS c = null;
    private static Context d = null;
    private static TelephoneBean e = null;
    /* access modifiers changed from: private */
    public static int f = 0;
    private static ConnectivityManager g = null;
    /* access modifiers changed from: private */
    public static WifiManager h = null;
    /* access modifiers changed from: private */
    public static TelephonyManager i = null;
    private static LocationManager j = null;
    private static LocationListener k = null;
    private static LocationListener l = null;
    /* access modifiers changed from: private */
    public static Location m = null;
    /* access modifiers changed from: private */
    public static Location n = null;
    /* access modifiers changed from: private */
    public static ArrayList o = new ArrayList();
    /* access modifiers changed from: private */
    public static ArrayList p = new ArrayList();
    /* access modifiers changed from: private */
    public static List q = new ArrayList();
    private static Des r = new Des(Constant.apsEncryptKey);
    private static PhoneStateListener s = null;
    /* access modifiers changed from: private */
    public static int t = 10;
    private static f u = null;
    /* access modifiers changed from: private */
    public static WifiInfo v = null;
    private static String w = null;
    private static Location x = null;
    /* access modifiers changed from: private */
    public static long y = 0;
    private static boolean z = false;

    protected APS() {
    }

    private synchronized Location a(String str) {
        Location location;
        String decrypt;
        String doPostXmlAsString = NetManagerApache.getInstance(d).doPostXmlAsString(Constant.apsServer, str);
        if (doPostXmlAsString == null || doPostXmlAsString.length() <= 0 || (decrypt = r.decrypt(new ParserResponse().ParserSapsXml(doPostXmlAsString), "GBK")) == null || decrypt.length() <= 0 || (location = new ParserResponse().ParserLocationXml(decrypt)) == null || location.getCenx() <= 0.0d) {
            location = null;
        }
        return location;
    }

    /* access modifiers changed from: private */
    public static synchronized void b(CellLocation cellLocation, List list) {
        synchronized (APS.class) {
            if (cellLocation != null) {
                String networkOperator = i.getNetworkOperator();
                if (cellLocation instanceof GsmCellLocation) {
                    f = 1;
                    o.clear();
                    if (networkOperator != null && networkOperator.length() > 4) {
                        GsmCellBean gsmCellBean = new GsmCellBean();
                        gsmCellBean.setLac(((GsmCellLocation) cellLocation).getLac());
                        gsmCellBean.setCellid(((GsmCellLocation) cellLocation).getCid());
                        gsmCellBean.setSignal(t);
                        gsmCellBean.setMcc(networkOperator.substring(0, 3));
                        gsmCellBean.setMnc(networkOperator.substring(3, 5));
                        o.add(gsmCellBean);
                        if (list != null && list.size() > 0) {
                            for (int i2 = 0; i2 < list.size(); i2++) {
                                NeighboringCellInfo neighboringCellInfo = (NeighboringCellInfo) list.get(i2);
                                GsmCellBean gsmCellBean2 = new GsmCellBean();
                                gsmCellBean2.setSignal((neighboringCellInfo.getRssi() * 2) - 133);
                                gsmCellBean2.setLac(neighboringCellInfo.getLac());
                                gsmCellBean2.setCellid(neighboringCellInfo.getCid());
                                gsmCellBean2.setMcc(networkOperator.substring(0, 3));
                                gsmCellBean2.setMnc(networkOperator.substring(3, 5));
                                o.add(gsmCellBean2);
                            }
                        }
                    }
                } else {
                    try {
                        Class.forName("android.telephony.cdma.CdmaCellLocation");
                        f = 2;
                        p.clear();
                        if (networkOperator != null && networkOperator.length() > 4) {
                            CdmaCellBean cdmaCellBean = new CdmaCellBean();
                            CdmaCellLocation cdmaCellLocation = (CdmaCellLocation) cellLocation;
                            cdmaCellBean.setLat(cdmaCellLocation.getBaseStationLatitude());
                            cdmaCellBean.setLon(cdmaCellLocation.getBaseStationLongitude());
                            cdmaCellBean.setSid(cdmaCellLocation.getBaseStationId());
                            cdmaCellBean.setNid(cdmaCellLocation.getNetworkId());
                            cdmaCellBean.setBid(cdmaCellLocation.getSystemId());
                            cdmaCellBean.setSignal(t);
                            cdmaCellBean.setMcc(networkOperator.substring(0, 3));
                            cdmaCellBean.setMnc(networkOperator.substring(3, 5));
                            p.add(cdmaCellBean);
                        }
                    } catch (Exception e2) {
                    }
                }
            }
        }
    }

    public static String getCurrenttime() {
        return new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date());
    }

    public static APS getInstance(Context context) {
        if (c == null) {
            c = new APS();
            d = context;
            h = (WifiManager) context.getSystemService("wifi");
            APS aps = c;
            aps.getClass();
            u = new f(aps);
            if (h.getWifiState() == 3) {
                v = h.getConnectionInfo();
                new d().start();
            }
            d.registerReceiver(u, new IntentFilter("android.net.wifi.SCAN_RESULTS"));
            g = (ConnectivityManager) d.getSystemService("connectivity");
            TelephonyManager telephonyManager = (TelephonyManager) d.getSystemService("phone");
            i = telephonyManager;
            e = TelephoneBean.getInstance(telephonyManager, d, null);
            s = new e();
            i.listen(s, 256);
            i.listen(s, 16);
            j = (LocationManager) d.getSystemService(LocationManagerProxy.KEY_LOCATION_CHANGED);
            k = new b();
        }
        return c;
    }

    private static synchronized void h() {
        synchronized (APS.class) {
            if (z) {
                l = new a();
                j.requestLocationUpdates(LocationManagerProxy.GPS_PROVIDER, 5000, 0.0f, l, Looper.getMainLooper());
            } else {
                try {
                    if (l != null) {
                        j.removeUpdates(l);
                    }
                } catch (Exception e2) {
                }
                l = null;
                m = null;
            }
        }
    }

    private synchronized String i() {
        StringBuilder sb;
        if (a != null) {
            getProductName();
        }
        if (h.getWifiState() == 3) {
            new c(this).start();
        }
        s.onCellLocationChanged(i.getCellLocation());
        StringBuilder sb2 = new StringBuilder();
        sb2.append("<?xml version=\"1.0\" encoding=\"GBK\" ?>");
        sb2.append("<location>");
        sb2.append("<license>").append(a).append("</license>");
        sb2.append("<src>").append(getProductName()).append("</src>");
        sb2.append("<imei>").append(e.getDeviceId()).append("</imei>");
        NetworkInfo activeNetworkInfo = g.getActiveNetworkInfo();
        sb2.append("<network>");
        if (activeNetworkInfo != null && activeNetworkInfo.isAvailable() && activeNetworkInfo.isConnected()) {
            sb2.append(activeNetworkInfo.toString());
            sb2.append(", countryiso: ").append(i.getNetworkCountryIso()).append(", operatorname: ").append(i.getNetworkOperatorName()).append(", line1number: ").append(i.getLine1Number());
            if (h.getWifiState() == 3) {
                DhcpInfo dhcpInfo = h.getDhcpInfo();
                sb2.append(", wifidns1: " + NetManagerApache.intToIpAddr(dhcpInfo.dns1)).append(", wifidns2: " + NetManagerApache.intToIpAddr(dhcpInfo.dns2)).append(", wifigateway: " + NetManagerApache.intToIpAddr(dhcpInfo.gateway)).append(", wifiipaddr: " + NetManagerApache.intToIpAddr(dhcpInfo.ipAddress));
            }
        }
        sb2.append("</network>");
        if (f == 1) {
            sb2.append("<cdma>0</cdma>");
            if (o.size() > 0) {
                GsmCellBean gsmCellBean = (GsmCellBean) o.get(0);
                sb2.append("<mcc>").append(gsmCellBean.getMcc()).append("</mcc>");
                sb2.append("<mnc>").append(gsmCellBean.getMnc()).append("</mnc>");
                sb2.append("<lac>").append(gsmCellBean.getLac()).append("</lac>");
                sb2.append("<cellid>").append(gsmCellBean.getCellid()).append("</cellid>");
                sb2.append("<signal>").append(gsmCellBean.getSignal()).append("</signal>");
                if (o.size() > 1) {
                    StringBuffer stringBuffer = new StringBuffer();
                    for (int i2 = 1; i2 < o.size(); i2++) {
                        if (i2 > 1) {
                            stringBuffer.append("*");
                        }
                        GsmCellBean gsmCellBean2 = (GsmCellBean) o.get(i2);
                        stringBuffer.append(gsmCellBean2.getMnc()).append(",").append(gsmCellBean2.getLac()).append(",").append(gsmCellBean2.getCellid()).append(",").append(gsmCellBean2.getSignal());
                    }
                    sb2.append("<nb>").append(stringBuffer.toString()).append("</nb>");
                }
            }
        } else if (f == 2) {
            sb2.append("<cdma>1</cdma>");
            if (p.size() > 0) {
                CdmaCellBean cdmaCellBean = (CdmaCellBean) p.get(0);
                sb2.append("<mcc>").append(cdmaCellBean.getMcc()).append("</mcc>");
                sb2.append("<sid>").append(cdmaCellBean.getSid()).append("</sid>");
                sb2.append("<nid>").append(cdmaCellBean.getNid()).append("</nid>");
                sb2.append("<bid>").append(cdmaCellBean.getBid()).append("</bid>");
                sb2.append("<lon>").append(cdmaCellBean.getLon()).append("</lon>");
                sb2.append("<lat>").append(cdmaCellBean.getLat()).append("</lat>");
                sb2.append("<signal>").append(cdmaCellBean.getSignal()).append("</signal>");
                if (p.size() > 1) {
                    StringBuffer stringBuffer2 = new StringBuffer();
                    for (int i3 = 1; i3 < p.size(); i3++) {
                        if (i3 > 1) {
                            stringBuffer2.append("*");
                        }
                        CdmaCellBean cdmaCellBean2 = (CdmaCellBean) p.get(i3);
                        stringBuffer2.append(cdmaCellBean2.getSid()).append(",").append(cdmaCellBean2.getNid()).append(",").append(cdmaCellBean2.getBid()).append(",").append(cdmaCellBean2.getSignal());
                    }
                    sb2.append("<nb>" + stringBuffer2.toString() + "</nb>");
                }
            }
        }
        if (j.isProviderEnabled(LocationManagerProxy.GPS_PROVIDER)) {
            if (m != null) {
                double latitude = m.getLatitude();
                double longitude = m.getLongitude();
                if (latitude > 3.0d && longitude > 73.0d) {
                    sb2.append("<gps>1</gps>");
                    sb2.append("<glong>").append(longitude).append("</glong>");
                    sb2.append("<glat>").append(latitude).append("</glat>");
                    sb2.append("<gaccuracy>").append(m.getAccuracy()).append("</gaccuracy>");
                }
                m = null;
            }
        } else if (j.isProviderEnabled(LocationManagerProxy.NETWORK_PROVIDER)) {
            j.requestLocationUpdates(LocationManagerProxy.NETWORK_PROVIDER, 20000, 0.0f, k, Looper.getMainLooper());
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e2) {
            }
            if (n != null) {
                double latitude2 = n.getLatitude();
                double longitude2 = n.getLongitude();
                if (latitude2 > 3.0d && longitude2 > 73.0d) {
                    sb2.append("<gps>2</gps>");
                    sb2.append("<glong>").append(longitude2).append("</glong>");
                    sb2.append("<glat>").append(latitude2).append("</glat>");
                }
            }
            n = null;
            j.removeUpdates(k);
        }
        if (h.getWifiState() == 3) {
            if (q.size() > 0) {
                StringBuilder sb3 = new StringBuilder();
                int i4 = 0;
                while (i4 < q.size() && i4 <= 3) {
                    ScanResult scanResult = (ScanResult) q.get(i4);
                    sb3.append(scanResult.BSSID).append(",").append(scanResult.level).append("*");
                    i4++;
                }
                sb2.append("<macs>").append(sb3.toString()).append("</macs>");
            }
            WifiInfo connectionInfo = h.getConnectionInfo();
            v = connectionInfo;
            if (connectionInfo != null && v.getBSSID() != null) {
                if (q.size() <= 0) {
                    sb2.append("<macs>").append(v.getBSSID()).append(",").append(v.getRssi()).append("*").append("</macs>");
                }
                sb2.append("<mainmac>").append(v.getBSSID()).append(",").append(v.getRssi()).append("*").append("</mainmac>");
            } else if (v != null) {
                v.getBSSID();
            }
        }
        sb2.append("<clienttime>").append(getCurrenttime()).append("</clienttime>");
        sb2.append("</location>");
        sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"GBK\" ?>");
        sb.append("<saps>");
        sb.append("<src>").append(getProductName()).append("</src>");
        try {
            sb.append("<sreq>").append(r.encrypt(sb2.toString())).append("</sreq>");
        } catch (Exception e3) {
        }
        sb.append("</saps>");
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        try {
            super.finalize();
        } catch (Throwable th) {
        }
    }

    public Location getCurrentLocation(Location location) {
        if (System.currentTimeMillis() - y < 2000) {
            return x;
        }
        if (!z) {
            m = location;
        }
        String i2 = i();
        if (i2.equalsIgnoreCase(w) && x != null) {
            return x;
        }
        w = i2;
        x = a(w);
        y = System.currentTimeMillis();
        return x;
    }

    public String getProductName() {
        return b;
    }

    public String getVersion() {
        return Constant.version;
    }

    public void onDestroy() {
        try {
            d.unregisterReceiver(u);
        } catch (Exception e2) {
        }
        try {
            if (k != null) {
                j.removeUpdates(k);
            }
        } catch (Exception e3) {
        }
        k = null;
        try {
            if (l != null) {
                j.removeUpdates(l);
            }
        } catch (Exception e4) {
        }
        l = null;
        try {
            i.listen(s, 0);
        } catch (Exception e5) {
        }
        s = null;
        o.clear();
        p.clear();
        q.clear();
        v = null;
        i = null;
        if (c != null) {
            c.finalize();
        }
        c = null;
        System.gc();
    }

    public void setLicence(String str) {
        a = str;
    }

    public void setOpenGps(boolean z2) {
        try {
            Thread.sleep(1000);
            z = z2;
            h();
        } catch (InterruptedException e2) {
        }
    }

    public void setProductName(String str) {
        b = str;
    }
}
