package com.autonavi.aps.api;

import com.amap.mapapi.poisearch.PoiTypeDef;

public class Location {
    private String a = PoiTypeDef.All;
    private String b = PoiTypeDef.All;
    private double c = 0.0d;
    private double d = 0.0d;
    private double e = 0.0d;
    private String f = PoiTypeDef.All;
    private String g = PoiTypeDef.All;
    private String h = PoiTypeDef.All;

    public String getAdcode() {
        return this.h;
    }

    public double getCenx() {
        return this.c;
    }

    public double getCeny() {
        return this.d;
    }

    public String getCitycode() {
        return this.f;
    }

    public String getDesc() {
        return this.g;
    }

    public double getRadius() {
        return this.e;
    }

    public String getRdesc() {
        return this.b;
    }

    public String getResult() {
        return this.a;
    }

    public void setAdcode(String str) {
        this.h = str;
    }

    public void setCenx(double d2) {
        this.c = d2;
    }

    public void setCeny(double d2) {
        this.d = d2;
    }

    public void setCitycode(String str) {
        this.f = str;
    }

    public void setDesc(String str) {
        this.g = str;
    }

    public void setRadius(double d2) {
        this.e = d2;
    }

    public void setRdesc(String str) {
        this.b = str;
    }

    public void setResult(String str) {
        this.a = str;
    }

    public final String toString() {
        return "result:" + this.a + "\n" + "rdesc:" + this.b + "\n" + "cenx:" + this.c + "\n" + "ceny:" + this.d + "\n" + "radius:" + this.e + "\n" + "citycode:" + this.f + "\n" + "adcode:" + this.h + "\n" + "desc:" + this.g + "\n";
    }
}
