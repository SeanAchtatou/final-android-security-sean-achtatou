package com.autonavi.aps.api;

public class GsmCellBean {
    private String a;
    private String b;
    private int c;
    private int d;
    private int e = 10;

    public int getCellid() {
        return this.d;
    }

    public int getLac() {
        return this.c;
    }

    public String getMcc() {
        return this.a;
    }

    public String getMnc() {
        return this.b;
    }

    public int getSignal() {
        return this.e;
    }

    public void setCellid(int i) {
        this.d = i;
    }

    public void setLac(int i) {
        this.c = i;
    }

    public void setMcc(String str) {
        this.a = str;
    }

    public void setMnc(String str) {
        this.b = str;
    }

    public void setSignal(int i) {
        this.e = i;
    }
}
