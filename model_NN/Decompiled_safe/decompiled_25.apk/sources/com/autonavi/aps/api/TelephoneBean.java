package com.autonavi.aps.api;

import android.content.Context;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;
import com.amap.mapapi.poisearch.PoiTypeDef;

public class TelephoneBean {
    private static String a = null;
    private static TelephoneBean b = null;

    protected TelephoneBean() {
    }

    public static TelephoneBean getInstance(TelephonyManager telephonyManager, Context context, String str) {
        if (b == null) {
            b = new TelephoneBean();
            a = telephonyManager.getDeviceId();
            String str2 = PoiTypeDef.All;
            if (str == null || str.length() <= 0) {
                SharedPreferences sharedPreferences = context.getSharedPreferences(Constant.apsPreferencesName, 2);
                str2 = sharedPreferences.getString(Constant.imeiSaltPreferencesKey, PoiTypeDef.All);
                if (str2 == null || str2.length() <= 0 || str2.equalsIgnoreCase("null")) {
                    str2 = String.valueOf((int) (Math.random() * 10000.0d));
                    sharedPreferences.edit().putString(Constant.apsPreferencesName, a).commit();
                }
            } else if (str.equalsIgnoreCase("lenovodualcard")) {
                str2 = String.valueOf((int) Constant.imeiMaxSalt);
            }
            a = String.valueOf(a) + "." + str2;
        }
        return b;
    }

    public String getDeviceId() {
        return a;
    }
}
