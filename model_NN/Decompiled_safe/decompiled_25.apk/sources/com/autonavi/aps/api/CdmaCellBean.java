package com.autonavi.aps.api;

public class CdmaCellBean {
    private int a;
    private int b;
    private String c;
    private String d;
    private int e;
    private int f;
    private int g;
    private int h = 10;

    public int getBid() {
        return this.g;
    }

    public int getLat() {
        return this.a;
    }

    public int getLon() {
        return this.b;
    }

    public String getMcc() {
        return this.c;
    }

    public String getMnc() {
        return this.d;
    }

    public int getNid() {
        return this.f;
    }

    public int getSid() {
        return this.e;
    }

    public int getSignal() {
        return this.h;
    }

    public void setBid(int i) {
        this.g = i;
    }

    public void setLat(int i) {
        if (i < Integer.MAX_VALUE) {
            this.a = i;
        }
    }

    public void setLon(int i) {
        if (this.a < Integer.MAX_VALUE) {
            this.b = i;
        }
    }

    public void setMcc(String str) {
        this.c = str;
    }

    public void setMnc(String str) {
        this.d = str;
    }

    public void setNid(int i) {
        this.f = i;
    }

    public void setSid(int i) {
        this.e = i;
    }

    public void setSignal(int i) {
        this.h = i;
    }
}
