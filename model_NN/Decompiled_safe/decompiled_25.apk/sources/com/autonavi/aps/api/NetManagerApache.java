package com.autonavi.aps.api;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.os.Build;
import android.support.v4.view.MotionEventCompat;
import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;

public class NetManagerApache {
    private static NetManagerApache b = null;
    private Context a;

    private NetManagerApache() {
    }

    public static NetManagerApache getInstance(Context context) {
        if (b == null) {
            NetManagerApache netManagerApache = new NetManagerApache();
            b = netManagerApache;
            netManagerApache.a = context.getApplicationContext();
        }
        return b;
    }

    public static String intToIpAddr(int i) {
        return String.valueOf(i & MotionEventCompat.ACTION_MASK) + "." + ((i >> 8) & MotionEventCompat.ACTION_MASK) + "." + ((i >> 16) & MotionEventCompat.ACTION_MASK) + "." + (i >>> 24);
    }

    /* JADX WARNING: Removed duplicated region for block: B:63:0x0106  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x010b  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0110  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String doPostXmlAsString(java.lang.String r17, java.lang.String r18) {
        /*
            r16 = this;
            java.lang.StringBuffer r7 = new java.lang.StringBuffer
            r7.<init>()
            r4 = 0
            r3 = 0
            r2 = 0
            org.apache.http.client.HttpClient r5 = r16.getHttpClient()     // Catch:{ Exception -> 0x0124, all -> 0x0118 }
            org.apache.http.client.methods.HttpPost r1 = new org.apache.http.client.methods.HttpPost     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            r0 = r17
            r1.<init>(r0)     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            org.apache.http.entity.StringEntity r4 = new org.apache.http.entity.StringEntity     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            java.lang.String r6 = "UTF-8"
            r0 = r18
            r4.<init>(r0, r6)     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            java.lang.String r6 = "text/xml"
            r4.setContentType(r6)     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            java.lang.String r6 = "Content-Type"
            java.lang.String r8 = "application/soap+xml;charset=UTF-8"
            r1.setHeader(r6, r8)     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            r1.setEntity(r4)     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            org.apache.http.HttpResponse r8 = r5.execute(r1)     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            java.lang.String r1 = "utf-8"
            java.lang.String r4 = "Content-Type"
            org.apache.http.Header[] r9 = r8.getHeaders(r4)     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            if (r9 == 0) goto L_0x003e
            int r10 = r9.length     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            r4 = 0
            r6 = r4
        L_0x003c:
            if (r6 < r10) goto L_0x0097
        L_0x003e:
            r6 = r1
            r1 = 0
            java.lang.String r4 = "Content-Encoding"
            org.apache.http.Header[] r9 = r8.getHeaders(r4)     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            if (r9 == 0) goto L_0x004c
            int r10 = r9.length     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            r4 = 0
        L_0x004a:
            if (r4 < r10) goto L_0x00de
        L_0x004c:
            org.apache.http.StatusLine r4 = r8.getStatusLine()     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            int r4 = r4.getStatusCode()     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            r9 = 200(0xc8, float:2.8E-43)
            if (r4 != r9) goto L_0x0130
            org.apache.http.HttpEntity r4 = r8.getEntity()     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            java.io.InputStream r3 = r4.getContent()     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            if (r1 == 0) goto L_0x012d
            java.lang.String r4 = "gzip"
            boolean r1 = r1.equals(r4)     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            if (r1 == 0) goto L_0x012d
            java.util.zip.GZIPInputStream r4 = new java.util.zip.GZIPInputStream     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            r4.<init>(r3)     // Catch:{ Exception -> 0x0126, all -> 0x011b }
        L_0x006f:
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0129, all -> 0x011d }
            java.io.InputStreamReader r1 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0129, all -> 0x011d }
            r1.<init>(r4, r6)     // Catch:{ Exception -> 0x0129, all -> 0x011d }
            r3.<init>(r1)     // Catch:{ Exception -> 0x0129, all -> 0x011d }
        L_0x0079:
            java.lang.String r1 = r3.readLine()     // Catch:{ Exception -> 0x00fd, all -> 0x0120 }
            if (r1 != 0) goto L_0x00f8
        L_0x007f:
            if (r3 == 0) goto L_0x0084
            r3.close()
        L_0x0084:
            if (r4 == 0) goto L_0x0089
            r4.close()
        L_0x0089:
            if (r5 == 0) goto L_0x0092
            org.apache.http.conn.ClientConnectionManager r1 = r5.getConnectionManager()
            r1.shutdown()
        L_0x0092:
            java.lang.String r1 = r7.toString()
            return r1
        L_0x0097:
            r4 = r9[r6]     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            java.lang.String r4 = r4.getValue()     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            if (r4 == 0) goto L_0x00b4
            int r11 = r4.length()     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            if (r11 <= 0) goto L_0x00b4
            java.lang.String r11 = ";"
            java.lang.String[] r11 = r4.split(r11)     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            if (r11 == 0) goto L_0x00b4
            int r4 = r11.length     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            if (r4 <= 0) goto L_0x00b4
            int r12 = r11.length     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            r4 = 0
        L_0x00b2:
            if (r4 < r12) goto L_0x00b8
        L_0x00b4:
            int r4 = r6 + 1
            r6 = r4
            goto L_0x003c
        L_0x00b8:
            r13 = r11[r4]     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            java.lang.String r14 = r13.trim()     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            java.lang.String r14 = r14.toLowerCase()     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            java.lang.String r15 = "charset="
            boolean r14 = r14.startsWith(r15)     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            if (r14 == 0) goto L_0x00db
            java.lang.String r1 = r13.trim()     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            java.lang.String r1 = r1.toLowerCase()     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            java.lang.String r4 = "charset="
            java.lang.String r11 = ""
            java.lang.String r1 = r1.replace(r4, r11)     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            goto L_0x00b4
        L_0x00db:
            int r4 = r4 + 1
            goto L_0x00b2
        L_0x00de:
            r11 = r9[r4]     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            java.lang.String r12 = r11.getValue()     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            if (r12 == 0) goto L_0x00f4
            java.lang.String r12 = r11.getValue()     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            int r12 = r12.length()     // Catch:{ Exception -> 0x0126, all -> 0x011b }
            if (r12 <= 0) goto L_0x00f4
            java.lang.String r1 = r11.getValue()     // Catch:{ Exception -> 0x0126, all -> 0x011b }
        L_0x00f4:
            int r4 = r4 + 1
            goto L_0x004a
        L_0x00f8:
            r7.append(r1)     // Catch:{ Exception -> 0x00fd, all -> 0x0120 }
            goto L_0x0079
        L_0x00fd:
            r1 = move-exception
            r2 = r3
            r3 = r4
            r4 = r5
        L_0x0101:
            throw r1     // Catch:{ all -> 0x0102 }
        L_0x0102:
            r1 = move-exception
            r5 = r4
        L_0x0104:
            if (r2 == 0) goto L_0x0109
            r2.close()
        L_0x0109:
            if (r3 == 0) goto L_0x010e
            r3.close()
        L_0x010e:
            if (r5 == 0) goto L_0x0117
            org.apache.http.conn.ClientConnectionManager r2 = r5.getConnectionManager()
            r2.shutdown()
        L_0x0117:
            throw r1
        L_0x0118:
            r1 = move-exception
            r5 = r4
            goto L_0x0104
        L_0x011b:
            r1 = move-exception
            goto L_0x0104
        L_0x011d:
            r1 = move-exception
            r3 = r4
            goto L_0x0104
        L_0x0120:
            r1 = move-exception
            r2 = r3
            r3 = r4
            goto L_0x0104
        L_0x0124:
            r1 = move-exception
            goto L_0x0101
        L_0x0126:
            r1 = move-exception
            r4 = r5
            goto L_0x0101
        L_0x0129:
            r1 = move-exception
            r3 = r4
            r4 = r5
            goto L_0x0101
        L_0x012d:
            r4 = r3
            goto L_0x006f
        L_0x0130:
            r4 = r3
            r3 = r2
            goto L_0x007f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.autonavi.aps.api.NetManagerApache.doPostXmlAsString(java.lang.String, java.lang.String):java.lang.String");
    }

    public HttpClient getHttpClient() {
        String host;
        NetworkInfo activeNetworkInfo;
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        ConnectivityManager connectivityManager = (ConnectivityManager) this.a.getSystemService("connectivity");
        if (!(connectivityManager != null && (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) != null && activeNetworkInfo.isAvailable() && activeNetworkInfo.isConnected() && activeNetworkInfo.getTypeName().equalsIgnoreCase("wifi")) && (host = Proxy.getHost(this.a)) != null && host.length() > 0) {
            String str = Build.MODEL;
            if (!(str != null && (str.indexOf("OMAP_SS") >= 0 || str.indexOf("omap_ss") >= 0 || str.indexOf("MT810") >= 0 || str.indexOf("MT720") >= 0 || str.indexOf("GT-I9008") >= 0))) {
                defaultHttpClient.getParams().setParameter("http.route.default-proxy", new HttpHost(host, Proxy.getPort(this.a)));
            }
        }
        HttpConnectionParams.setConnectionTimeout(defaultHttpClient.getParams(), 20000);
        HttpConnectionParams.setSoTimeout(defaultHttpClient.getParams(), 20000);
        return defaultHttpClient;
    }
}
