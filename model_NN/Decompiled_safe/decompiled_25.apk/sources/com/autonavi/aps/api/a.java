package com.autonavi.aps.api;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import com.amap.mapapi.location.LocationManagerProxy;

final class a implements LocationListener {
    a() {
    }

    public final void onLocationChanged(Location location) {
        if (location != null && location.getProvider().equalsIgnoreCase(LocationManagerProxy.GPS_PROVIDER)) {
            APS.m = location;
        }
    }

    public final void onProviderDisabled(String str) {
        if (str.equals(LocationManagerProxy.GPS_PROVIDER)) {
            APS.m = (Location) null;
        }
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onStatusChanged(String str, int i, Bundle bundle) {
        if (str.equalsIgnoreCase(LocationManagerProxy.GPS_PROVIDER) && i == 0) {
            APS.m = (Location) null;
        }
    }
}
