package com.autonavi.aps.api;

import android.location.Location;

public interface IAPS {
    Location getCurrentLocation(Location location);

    String getVersion();

    void onDestroy();

    void setLicence(String str);

    void setOpenGps(boolean z);

    void setProductName(String str);
}
