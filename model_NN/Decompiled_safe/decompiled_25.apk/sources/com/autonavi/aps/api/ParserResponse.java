package com.autonavi.aps.api;

import java.io.ByteArrayInputStream;
import javax.xml.parsers.SAXParserFactory;

public class ParserResponse {
    public Location ParserLocationXml(String str) {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(str.getBytes());
        SAXParserFactory newInstance = SAXParserFactory.newInstance();
        k kVar = new k(this);
        try {
            newInstance.newSAXParser().parse(byteArrayInputStream, kVar);
        } catch (Exception e) {
        }
        return kVar.a;
    }

    public String ParserSapsXml(String str) {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(str.getBytes());
        SAXParserFactory newInstance = SAXParserFactory.newInstance();
        l lVar = new l(this);
        try {
            newInstance.newSAXParser().parse(byteArrayInputStream, lVar);
        } catch (Exception e) {
        }
        return lVar.a;
    }
}
