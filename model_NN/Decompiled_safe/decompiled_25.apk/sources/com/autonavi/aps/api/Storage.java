package com.autonavi.aps.api;

import android.os.Environment;
import android.os.StatFs;

public class Storage {
    private static Storage a;

    protected Storage() {
    }

    public static long availaleSize() {
        StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
        return ((((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize())) / 1024) / 1024;
    }

    public static Storage getInstance() {
        if (a == null) {
            a = new Storage();
        }
        return a;
    }

    public static boolean sdcardAvaliable() {
        return Environment.getExternalStorageState().equalsIgnoreCase("mounted");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x007a A[SYNTHETIC, Splitter:B:25:0x007a] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0086 A[SYNTHETIC, Splitter:B:31:0x0086] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void writeLog(java.lang.String r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            boolean r0 = sdcardAvaliable()     // Catch:{ all -> 0x008a }
            if (r0 == 0) goto L_0x0045
            long r0 = availaleSize()     // Catch:{ all -> 0x008a }
            r2 = 50
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x0045
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x008a }
            r0.<init>()     // Catch:{ all -> 0x008a }
            java.io.File r1 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ all -> 0x008a }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x008a }
            java.lang.String r1 = "/apsapi/tmp.dat"
            java.lang.String r2 = "/"
            java.lang.String r3 = java.io.File.separator     // Catch:{ all -> 0x008a }
            java.lang.String r1 = r1.replace(r2, r3)     // Catch:{ all -> 0x008a }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x008a }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x008a }
            java.io.File r2 = new java.io.File     // Catch:{ all -> 0x008a }
            r2.<init>(r0)     // Catch:{ all -> 0x008a }
            java.io.File r0 = r2.getParentFile()     // Catch:{ all -> 0x008a }
            boolean r1 = r0.exists()     // Catch:{ all -> 0x008a }
            if (r1 != 0) goto L_0x0047
            boolean r0 = r0.mkdirs()     // Catch:{ all -> 0x008a }
            if (r0 != 0) goto L_0x0045
        L_0x0045:
            monitor-exit(r5)
            return
        L_0x0047:
            r0 = 0
            boolean r1 = r2.exists()     // Catch:{ Exception -> 0x0077, all -> 0x0080 }
            if (r1 != 0) goto L_0x0051
            r2.createNewFile()     // Catch:{ Exception -> 0x0077, all -> 0x0080 }
        L_0x0051:
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0077, all -> 0x0080 }
            r3 = 1
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x0077, all -> 0x0080 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0091, all -> 0x008f }
            java.lang.String r2 = java.lang.String.valueOf(r6)     // Catch:{ Exception -> 0x0091, all -> 0x008f }
            r0.<init>(r2)     // Catch:{ Exception -> 0x0091, all -> 0x008f }
            java.lang.String r2 = "\r\n"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0091, all -> 0x008f }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0091, all -> 0x008f }
            byte[] r0 = r0.getBytes()     // Catch:{ Exception -> 0x0091, all -> 0x008f }
            r1.write(r0)     // Catch:{ Exception -> 0x0091, all -> 0x008f }
            r1.close()     // Catch:{ Exception -> 0x0075 }
            goto L_0x0045
        L_0x0075:
            r0 = move-exception
            goto L_0x0045
        L_0x0077:
            r1 = move-exception
        L_0x0078:
            if (r0 == 0) goto L_0x0045
            r0.close()     // Catch:{ Exception -> 0x007e }
            goto L_0x0045
        L_0x007e:
            r0 = move-exception
            goto L_0x0045
        L_0x0080:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0084:
            if (r1 == 0) goto L_0x0089
            r1.close()     // Catch:{ Exception -> 0x008d }
        L_0x0089:
            throw r0     // Catch:{ all -> 0x008a }
        L_0x008a:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x008d:
            r1 = move-exception
            goto L_0x0089
        L_0x008f:
            r0 = move-exception
            goto L_0x0084
        L_0x0091:
            r0 = move-exception
            r0 = r1
            goto L_0x0078
        */
        throw new UnsupportedOperationException("Method not decompiled: com.autonavi.aps.api.Storage.writeLog(java.lang.String):void");
    }
}
