package com.autonavi.aps.api;

import android.telephony.CellLocation;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;

final class i extends PhoneStateListener {
    i() {
    }

    public final void onCellLocationChanged(CellLocation cellLocation) {
    }

    public final void onServiceStateChanged(ServiceState serviceState) {
        super.onServiceStateChanged(serviceState);
    }

    public final void onSignalStrengthsChanged(SignalStrength signalStrength) {
        APSLENOVODUALCARD.q = (signalStrength.getGsmSignalStrength() * 2) - 113;
        if (APSLENOVODUALCARD.b() == 1 && APSLENOVODUALCARD.l.size() > 0) {
            ((GsmCellBean) APSLENOVODUALCARD.l.get(0)).setSignal(APSLENOVODUALCARD.q);
        } else if (APSLENOVODUALCARD.b() == 2 && APSLENOVODUALCARD.m.size() > 0) {
            ((CdmaCellBean) APSLENOVODUALCARD.m.get(0)).setSignal(APSLENOVODUALCARD.q);
        }
        super.onSignalStrengthsChanged(signalStrength);
    }
}
