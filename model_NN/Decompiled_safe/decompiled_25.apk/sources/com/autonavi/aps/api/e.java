package com.autonavi.aps.api;

import android.telephony.CellLocation;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;

final class e extends PhoneStateListener {
    e() {
    }

    public final void onCellLocationChanged(CellLocation cellLocation) {
        APS.b(cellLocation, APS.i.getNeighboringCellInfo());
        super.onCellLocationChanged(cellLocation);
    }

    public final void onServiceStateChanged(ServiceState serviceState) {
        super.onServiceStateChanged(serviceState);
    }

    public final void onSignalStrengthsChanged(SignalStrength signalStrength) {
        APS.t = (signalStrength.getGsmSignalStrength() * 2) - 113;
        if (APS.f == 1 && APS.o.size() > 0) {
            ((GsmCellBean) APS.o.get(0)).setSignal(APS.t);
        } else if (APS.f == 2 && APS.p.size() > 0) {
            ((CdmaCellBean) APS.p.get(0)).setSignal(APS.t);
        }
        super.onSignalStrengthsChanged(signalStrength);
    }
}
