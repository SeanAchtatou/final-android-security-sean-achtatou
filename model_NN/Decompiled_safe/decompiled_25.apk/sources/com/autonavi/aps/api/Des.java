package com.autonavi.aps.api;

import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

public class Des {
    private static String a = "DESede";
    private Cipher b = null;
    private Cipher c = null;

    public Des(String str) {
        try {
            SecureRandom secureRandom = new SecureRandom();
            SecretKey generateSecret = SecretKeyFactory.getInstance(a).generateSecret(new DESedeKeySpec(str.getBytes("utf-8")));
            this.b = Cipher.getInstance(a);
            this.b.init(1, generateSecret, secureRandom);
            this.c = Cipher.getInstance(a);
            this.c.init(2, generateSecret, secureRandom);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String byte2hex(byte[] bArr) {
        StringBuilder sb = new StringBuilder();
        for (byte b2 : bArr) {
            String hexString = Integer.toHexString(b2 & 255);
            if (hexString.length() == 1) {
                sb.append("0").append(hexString);
            } else {
                sb.append(hexString);
            }
        }
        return sb.toString();
    }

    public String decrypt(String str, String str2) {
        return new String(this.c.doFinal(hex2byte(str)), str2);
    }

    public String encrypt(String str) {
        return byte2hex(this.b.doFinal(str.getBytes("utf-8")));
    }

    public byte[] hex2byte(String str) {
        String trim;
        int length;
        if (str == null || (length = (trim = str.trim()).length()) == 0 || length % 2 == 1) {
            return null;
        }
        byte[] bArr = new byte[(length / 2)];
        int i = 0;
        while (i < trim.length()) {
            try {
                bArr[i / 2] = (byte) Integer.decode("0X" + trim.substring(i, i + 2)).intValue();
                i += 2;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return bArr;
    }
}
