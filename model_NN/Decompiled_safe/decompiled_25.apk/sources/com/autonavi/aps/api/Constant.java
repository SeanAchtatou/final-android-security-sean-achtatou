package com.autonavi.aps.api;

public class Constant {
    public static final String apsEncryptKey = "autonavi00spas$#@!666666";
    public static final int apsNetworkTimeout = 20;
    public static final String apsPreferencesName = "APS_Preferences";
    public static final String apsServer = "http://naps.amap.com/SAPS/r";
    public static final int defaultCgiSignal = 10;
    public static final boolean enableApsLog = false;
    public static final int googleNetworkTimeout = 3;
    public static final int gpsScanSpan = 5;
    public static final int imeiMaxSalt = 10000;
    public static final String imeiSaltPreferencesKey = "imeisalt";
    public static final int minOpenOrCloseGpsDuration = 1;
    public static final int minRequestDuration = 2;
    public static final long minSdcardAvaliableSizeToWrite = 50;
    public static final String sdcardLogPath = "/apsapi/tmp.dat";
    public static final String version = "1.0.201205041545";
}
