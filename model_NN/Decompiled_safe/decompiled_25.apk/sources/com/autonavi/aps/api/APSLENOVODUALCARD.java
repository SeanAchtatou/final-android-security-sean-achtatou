package com.autonavi.aps.api;

import android.content.Context;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import java.util.ArrayList;
import java.util.List;

public class APSLENOVODUALCARD implements IAPS {
    private static String a = null;
    private static String b = null;
    private static APSLENOVODUALCARD c = null;
    private static Context d = null;
    private static TelephoneBean e = null;
    private static int f = 0;
    private static ConnectivityManager g = null;
    /* access modifiers changed from: private */
    public static WifiManager h = null;
    private static TelephonyManager i = null;
    private static LocationManager j = null;
    private static LocationListener k = null;
    /* access modifiers changed from: private */
    public static ArrayList l = new ArrayList();
    /* access modifiers changed from: private */
    public static ArrayList m = new ArrayList();
    private static List n = new ArrayList();
    private static Des o = new Des(Constant.apsEncryptKey);
    private static PhoneStateListener p = null;
    /* access modifiers changed from: private */
    public static int q = 10;
    private static j r = null;
    private static WifiInfo s = null;
    private static String t = null;
    private static Location u = null;
    private static long v = 0;

    protected APSLENOVODUALCARD() {
    }

    private synchronized Location a(String str) {
        Location location;
        String decrypt;
        String doPostXmlAsString = NetManagerApache.getInstance(d).doPostXmlAsString(Constant.apsServer, str);
        if (doPostXmlAsString == null || doPostXmlAsString.length() <= 0 || (decrypt = o.decrypt(new ParserResponse().ParserSapsXml(doPostXmlAsString), "GBK")) == null || decrypt.length() <= 0 || (location = new ParserResponse().ParserLocationXml(decrypt)) == null || location.getCenx() <= 0.0d) {
            location = null;
        }
        return location;
    }

    static /* synthetic */ int b() {
        return 0;
    }

    private synchronized String f() {
        StringBuilder sb;
        if (a != null) {
            getProductName();
        }
        if (h.getWifiState() == 3) {
            new g(this).start();
        }
        p.onCellLocationChanged(i.getCellLocation());
        StringBuilder sb2 = new StringBuilder();
        sb2.append("<?xml version=\"1.0\" encoding=\"GBK\" ?>");
        sb2.append("<location>");
        sb2.append("<license>").append(a).append("</license>");
        sb2.append("<src>").append(getProductName()).append("</src>");
        sb2.append("<imei>").append(e.getDeviceId()).append("#").append(i.getNetworkOperatorName()).append("#").append(i.getLine1Number()).append("</imei>");
        NetworkInfo activeNetworkInfo = g.getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isAvailable() && activeNetworkInfo.isConnected()) {
            sb2.append("<network>").append(activeNetworkInfo.toString()).append("</network>");
        }
        if (n.size() > 0) {
            StringBuilder sb3 = new StringBuilder();
            int i2 = 0;
            while (i2 < n.size() && i2 <= 3) {
                ScanResult scanResult = (ScanResult) n.get(i2);
                sb3.append(scanResult.BSSID).append(",").append(scanResult.level).append("*");
                i2++;
            }
            sb2.append("<macs>").append(sb3.toString()).append("</macs>");
        } else if (h.getWifiState() == 3) {
            WifiInfo connectionInfo = h.getConnectionInfo();
            s = connectionInfo;
            if (connectionInfo != null && s.getBSSID() != null) {
                sb2.append("<macs>").append(s.getBSSID()).append(",").append(s.getRssi()).append("*").append("</macs>");
            } else if (s != null) {
                s.getBSSID();
            }
        }
        sb2.append("</location>");
        sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"GBK\" ?>");
        sb.append("<saps>");
        sb.append("<src>").append(getProductName()).append("</src>");
        try {
            sb.append("<sreq>").append(o.encrypt(sb2.toString())).append("</sreq>");
        } catch (Exception e2) {
        }
        sb.append("</saps>");
        return sb.toString();
    }

    public static APSLENOVODUALCARD getInstance(Context context) {
        if (c == null) {
            c = new APSLENOVODUALCARD();
            d = context;
            h = (WifiManager) context.getSystemService("wifi");
            APSLENOVODUALCARD apslenovodualcard = c;
            apslenovodualcard.getClass();
            r = new j(apslenovodualcard);
            if (h.getWifiState() == 3) {
                s = h.getConnectionInfo();
                new h().start();
            }
            d.registerReceiver(r, new IntentFilter("android.net.wifi.SCAN_RESULTS"));
            g = (ConnectivityManager) d.getSystemService("connectivity");
            TelephonyManager telephonyManager = (TelephonyManager) d.getSystemService("phone");
            i = telephonyManager;
            e = TelephoneBean.getInstance(telephonyManager, d, "lenovodualcard");
            p = new i();
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            d.registerReceiver(r, intentFilter);
        }
        return c;
    }

    public Location getCurrentLocation(Location location) {
        if (System.currentTimeMillis() - v < 2000) {
            return u;
        }
        String f2 = f();
        if (f2.equalsIgnoreCase(t) && u != null) {
            return u;
        }
        t = f2;
        u = a(t);
        v = System.currentTimeMillis();
        return u;
    }

    public String getProductName() {
        return b;
    }

    public String getVersion() {
        return Constant.version;
    }

    public void onDestroy() {
        try {
            d.unregisterReceiver(r);
        } catch (Exception e2) {
        }
        LocationManager locationManager = null;
        try {
            locationManager.removeUpdates((LocationListener) null);
        } catch (Exception e3) {
        }
        try {
            i.listen(p, 0);
        } catch (Exception e4) {
        }
        l.clear();
        m.clear();
        n.clear();
        i = null;
        k = null;
        p = null;
        s = null;
        c = null;
    }

    public void setLicence(String str) {
        a = str;
    }

    public void setOpenGps(boolean z) {
    }

    public void setProductName(String str) {
        b = str;
    }
}
