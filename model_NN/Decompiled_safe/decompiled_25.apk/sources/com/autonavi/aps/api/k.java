package com.autonavi.aps.api;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

final class k extends DefaultHandler {
    public Location a = new Location();
    private StringBuffer b = new StringBuffer();

    k(ParserResponse parserResponse) {
    }

    public final void characters(char[] cArr, int i, int i2) {
        this.b.append(new String(cArr, i, i2).toString());
        super.characters(cArr, i, i2);
    }

    public final void endElement(String str, String str2, String str3) {
        if (str2.equals("result")) {
            this.a.setResult(this.b.toString());
        } else if (str2.equals("rdesc")) {
            this.a.setRdesc(this.b.toString());
        } else if (str2.equals("cenx")) {
            try {
                this.a.setCenx(Double.valueOf(this.b.toString()).doubleValue());
            } catch (Exception e) {
                this.a.setCenx(0.0d);
            }
        } else if (str2.equals("ceny")) {
            try {
                this.a.setCeny(Double.valueOf(this.b.toString()).doubleValue());
            } catch (Exception e2) {
                this.a.setCeny(0.0d);
            }
        } else if (str2.equals("radius")) {
            try {
                this.a.setRadius(Double.valueOf(this.b.toString()).doubleValue());
            } catch (Exception e3) {
                this.a.setRadius(0.0d);
            }
        } else if (str2.equals("citycode")) {
            this.a.setCitycode(this.b.toString());
        } else if (str2.equals("desc")) {
            this.a.setDesc(this.b.toString());
        } else if (str2.equals("adcode")) {
            this.a.setAdcode(this.b.toString());
        }
        super.endElement(str, str2, str3);
    }

    public final void startElement(String str, String str2, String str3, Attributes attributes) {
        this.b.delete(0, this.b.toString().length());
        super.startElement(str, str2, str3, attributes);
    }
}
