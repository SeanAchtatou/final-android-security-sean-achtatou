package com.autonavi.aps.api;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class f extends BroadcastReceiver {
    /* synthetic */ f(APS aps) {
        this(aps, (byte) 0);
    }

    private f(APS aps, byte b) {
    }

    public final void onReceive(Context context, Intent intent) {
        if (intent.getAction().equalsIgnoreCase("android.net.wifi.SCAN_RESULTS")) {
            APS.q = APS.h.getScanResults();
        } else if (intent.getAction().equalsIgnoreCase("android.intent.action.TIME_SET") || intent.getAction().equalsIgnoreCase("android.intent.action.TIMEZONE_CHANGED")) {
            APS.y = 0;
        } else {
            switch (intent.getIntExtra("wifi_state", 4)) {
                case 0:
                    APS.q.clear();
                    APS.v = null;
                    return;
                case 1:
                    APS.q.clear();
                    APS.v = null;
                    return;
                case 2:
                case 3:
                default:
                    return;
                case 4:
                    APS.q.clear();
                    APS.v = null;
                    return;
            }
        }
    }
}
