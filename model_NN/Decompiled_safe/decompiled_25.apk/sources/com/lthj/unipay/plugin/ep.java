package com.lthj.unipay.plugin;

import com.unionpay.upomp.lthj.plugin.model.Data;
import com.unionpay.upomp.lthj.plugin.model.DeleteBankCard;

public class ep extends z {
    private String a;
    private StringBuffer b = new StringBuffer();
    private String c;
    private StringBuffer d = new StringBuffer();
    private String e;

    public ep(int i) {
        super(i);
    }

    public void a(Data data) {
        DeleteBankCard deleteBankCard = (DeleteBankCard) data;
        c(deleteBankCard);
        this.a = deleteBankCard.loginName;
        this.b.delete(0, this.b.length());
        this.b.append(deleteBankCard.mobileNumber);
    }

    public void a(String str) {
        this.a = str;
    }

    public Data b() {
        DeleteBankCard deleteBankCard = new DeleteBankCard();
        b(deleteBankCard);
        deleteBankCard.loginName = this.a;
        deleteBankCard.mobileNumber = this.b.toString();
        deleteBankCard.bindId = this.c;
        deleteBankCard.pan = this.d.toString();
        deleteBankCard.isDefault = this.e;
        return deleteBankCard;
    }

    public void b(String str) {
        this.b.delete(0, this.b.length());
        this.b.append(str);
    }

    public void c(String str) {
        this.e = str;
    }

    public void d(String str) {
        this.c = str;
    }
}
