package com.lthj.unipay.plugin;

import android.graphics.Bitmap;
import android.os.Message;
import com.unionpay.upomp.lthj.widget.ValidateCodeView;

public class db extends Thread {
    public String a;
    final /* synthetic */ ValidateCodeView b;

    public db(ValidateCodeView validateCodeView, String str) {
        this.b = validateCodeView;
        this.a = str;
    }

    public void run() {
        Bitmap e = j.e(this.a);
        Message obtain = Message.obtain();
        obtain.obj = e;
        this.b.g.sendMessage(obtain);
    }
}
