package com.lthj.unipay.plugin;

import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import com.amap.mapapi.poisearch.PoiTypeDef;

public class f implements View.OnFocusChangeListener, View.OnTouchListener {
    private bw a;

    public f(bw bwVar) {
        this.a = bwVar;
    }

    private void a(EditText editText) {
        ((InputMethodManager) editText.getContext().getSystemService("input_method")).hideSoftInputFromWindow(editText.getWindowToken(), 0);
        editText.setText(PoiTypeDef.All);
        editText.setInputType(0);
        editText.setClickable(false);
        j.a(editText.getContext(), editText, 0, this.a);
    }

    public void onFocusChange(View view, boolean z) {
        if (view instanceof EditText) {
            EditText editText = (EditText) view;
            if (z && editText.isClickable()) {
                a(editText);
            }
        }
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (!(view instanceof EditText)) {
            return false;
        }
        EditText editText = (EditText) view;
        if (motionEvent.getAction() != 1 || !editText.isClickable()) {
            return false;
        }
        a(editText);
        return false;
    }
}
