package com.lthj.unipay.plugin;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class h {
    public String[] a = {"pluginSerialNoTest", "pluginSerialNoProduct", "commonPayPhone"};
    public String[] b = {"text", "text", "text"};
    private ab c;
    private String d = "pluginfo";

    public h(Context context) {
        this.c = new ab(context, this.d, this.a, this.b);
    }

    public cz a(int i) {
        cz czVar = null;
        Cursor a2 = this.c.a(i);
        if (a2 != null) {
            if (a2.moveToNext()) {
                cz czVar2 = new cz();
                czVar2.a(i);
                String string = as.a().e ? a2.getString(a2.getColumnIndex(this.a[0])) : a2.getString(a2.getColumnIndex(this.a[1]));
                String string2 = a2.getString(a2.getColumnIndex(this.a[2]));
                czVar2.a(string);
                czVar2.b(string2);
                czVar = czVar2;
            }
            if (a2 != null) {
                a2.close();
                this.c.a();
            }
        }
        return czVar;
    }

    public boolean a(cz czVar) {
        if (czVar == null) {
            return false;
        }
        ContentValues contentValues = new ContentValues();
        if (as.a().e) {
            contentValues.put(this.a[0], czVar.b());
        } else {
            contentValues.put(this.a[1], czVar.b());
        }
        contentValues.put(this.a[2], czVar.c());
        return this.c.a(contentValues);
    }

    public boolean b(cz czVar) {
        boolean z;
        boolean z2 = true;
        ContentValues contentValues = new ContentValues();
        if (czVar.b() != null) {
            if (as.a().e) {
                contentValues.put(this.a[0], czVar.b());
            } else {
                contentValues.put(this.a[1], czVar.b());
            }
            z = true;
        } else {
            z = false;
        }
        if (czVar.c() != null) {
            contentValues.put(this.a[2], czVar.c());
        } else {
            z2 = z;
        }
        if (z2) {
            return this.c.a(czVar.a(), contentValues);
        }
        return false;
    }
}
