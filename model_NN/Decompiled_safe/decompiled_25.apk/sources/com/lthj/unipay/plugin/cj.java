package com.lthj.unipay.plugin;

import android.content.Intent;
import android.view.View;
import com.unionpay.upomp.lthj.plugin.ui.HomeActivity;
import com.unionpay.upomp.lthj.plugin.ui.SupportCardActivity;

public class cj implements View.OnClickListener {
    final /* synthetic */ HomeActivity a;

    public cj(HomeActivity homeActivity) {
        this.a = homeActivity;
    }

    public void onClick(View view) {
        Intent intent = new Intent(this.a, SupportCardActivity.class);
        intent.putExtra("tranType", "1");
        intent.addFlags(67108864);
        this.a.a().changeSubActivity(intent);
        l.a().b();
    }
}
