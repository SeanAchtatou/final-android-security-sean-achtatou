package com.lthj.unipay.plugin;

import android.content.Context;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.ProgressBar;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.unionpay.upomp.lthj.link.PluginLink;
import com.unionpay.upomp.lthj.plugin.model.GetBankService;
import com.unionpay.upomp.lthj.plugin.model.HeadData;
import com.unionpay.upomp.lthj.plugin.ui.JniMethod;
import com.unionpay.upomp.lthj.plugin.ui.UIResponseListener;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;

public class cp {
    public static void a(UIResponseListener uIResponseListener, Context context) {
        bz bzVar = new bz(8207);
        bzVar.a(HeadData.createHeadData("GetPanBankBindList.Req", context));
        bzVar.a(as.a().A.toString());
        try {
            co.a().a(bzVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(UIResponseListener uIResponseListener, Context context, String str) {
        al alVar = new al(8204);
        alVar.a(HeadData.createHeadData("DefaultPanSet.Req", context));
        alVar.a(as.a().A.toString());
        alVar.b(as.a().B.toString());
        alVar.c(str);
        try {
            co.a().a(alVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(UIResponseListener uIResponseListener, Context context, String str, bw bwVar, String str2) {
        cd cdVar = new cd(8229);
        cdVar.a(HeadData.createHeadData("PanBankBindExpress.Req", context));
        cdVar.a(str);
        cdVar.b(as.a().i);
        cdVar.c(as.a().l);
        cdVar.d(as.a().m);
        cdVar.e(as.a().w.toString());
        cdVar.f(as.a().z.toString());
        as.a().z.setLength(0);
        if (!TextUtils.isEmpty(str2)) {
            cdVar.h(str2);
        }
        if (!(bwVar == null || bwVar.b() == null || bwVar.a() == null)) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(new String(JniMethod.getJniMethod().decryptConfig(bwVar.b(), bwVar.b().length)));
            stringBuffer.append(new String(JniMethod.getJniMethod().decryptConfig(bwVar.a(), bwVar.a().length)));
            cdVar.g(stringBuffer.toString());
            stringBuffer.delete(0, stringBuffer.length());
        }
        try {
            co.a().a(cdVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(UIResponseListener uIResponseListener, Context context, String str, String str2) {
        m mVar = new m(8195);
        mVar.a(HeadData.createHeadData("UserResetPwd.Req", context));
        mVar.a(str);
        mVar.c(str2);
        mVar.b(as.a().d.d.toString());
        as.a().d.e.setLength(0);
        try {
            co.a().a(mVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(UIResponseListener uIResponseListener, Context context, String str, String str2, String str3, bw bwVar, String str4, boolean z) {
        t tVar = new t(8203);
        tVar.a(HeadData.createHeadData("PanBankBind.Req", context));
        tVar.a(as.a().A.toString());
        tVar.d(str3);
        tVar.e(as.a().z.toString());
        as.a().z.setLength(0);
        tVar.b(str);
        tVar.c(str2);
        if (str4 != null) {
            tVar.g(str4);
        }
        if (!(bwVar == null || bwVar.b() == null || bwVar.a() == null)) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(new String(JniMethod.getJniMethod().decryptConfig(bwVar.b(), bwVar.b().length)));
            stringBuffer.append(new String(JniMethod.getJniMethod().decryptConfig(bwVar.a(), bwVar.a().length)));
            tVar.f(stringBuffer.toString());
            stringBuffer.delete(0, stringBuffer.length());
        }
        if (z) {
            tVar.h("1");
        } else {
            tVar.h("0");
        }
        try {
            co.a().a(tVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(UIResponseListener uIResponseListener, Context context, String str, String str2, String str3, String str4) {
        bf bfVar = new bf(8221);
        bfVar.a(HeadData.createHeadData("GetSecureQuestion.Req", context));
        bfVar.a(str);
        bfVar.c(str2);
        bfVar.b(str3);
        bfVar.d(str4);
        try {
            co.a().a(bfVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(UIResponseListener uIResponseListener, Context context, String str, String str2, boolean z) {
        dh dhVar = new dh(8224);
        dhVar.a(HeadData.createHeadData("GetBankInfo.Req", context));
        if (z) {
            dhVar.c(as.a().k);
        }
        dhVar.a(str);
        dhVar.b(str2);
        try {
            co.a().a(dhVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(UIResponseListener uIResponseListener, ProgressBar progressBar) {
        if (progressBar != null) {
            progressBar.setVisibility(0);
            r rVar = new r(8201);
            rVar.a(HeadData.createHeadData("GetVerifyCode.Req", progressBar.getContext()));
            try {
                co.a().a(rVar, uIResponseListener, progressBar.getContext(), false, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void a(String str, UIResponseListener uIResponseListener, Context context) {
        o oVar = new o(8202);
        oVar.a(HeadData.createHeadData("GetBankList.Req", context));
        oVar.a(str);
        try {
            co.a().a(oVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(String str, String str2, int i, UIResponseListener uIResponseListener, Context context, Button button) {
        if (c.a(context, str)) {
            button.setText(PluginLink.getStringupomp_lthj_getting_mac());
            button.setEnabled(false);
            n nVar = new n(8200);
            nVar.b(str2);
            nVar.a(HeadData.createHeadData("GetMobileMac.Req", context));
            nVar.a(str);
            nVar.j(i + PoiTypeDef.All);
            try {
                co.a().a(nVar, uIResponseListener, context, false, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void b(UIResponseListener uIResponseListener, Context context, String str) {
        p pVar = new p(8196);
        pVar.a(HeadData.createHeadData("UserUpdatePwd.Req", context));
        pVar.a(as.a().A.toString());
        pVar.d(as.a().B.toString());
        pVar.e(str);
        pVar.b(as.a().d.c.toString());
        pVar.c(as.a().d.d.toString());
        as.a().d.e.setLength(0);
        try {
            co.a().a(pVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void b(UIResponseListener uIResponseListener, Context context, String str, String str2) {
        ep epVar = new ep(8206);
        epVar.a(HeadData.createHeadData("PanDelete.Req", context));
        epVar.a(as.a().A.toString());
        epVar.b(as.a().B.toString());
        epVar.d(str);
        epVar.c(str2);
        try {
            co.a().a(epVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void b(UIResponseListener uIResponseListener, Context context, String str, String str2, String str3, String str4) {
        cu cuVar = new cu(8225);
        cuVar.a(HeadData.createHeadData("UserRegisterExpress.Req", context));
        cuVar.a(str);
        cuVar.e(str2);
        cuVar.b(as.a().d.d.toString());
        cuVar.f(str3);
        cuVar.g(str4);
        cuVar.c(as.a().i);
        cuVar.d(as.a().l);
        cuVar.h(as.a().m);
        try {
            co.a().a(cuVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void c(UIResponseListener uIResponseListener, Context context, String str) {
        ar arVar = new ar(8228);
        arVar.a(HeadData.createHeadData("GetBanksService.Req", context));
        arVar.a(str);
        GetBankService getBankService = null;
        try {
            getBankService = ct.a(context, str);
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        ar arVar2 = new ar(8228);
        arVar2.a(getBankService);
        uIResponseListener.responseCallBack(arVar2);
    }

    public static void c(UIResponseListener uIResponseListener, Context context, String str, String str2) {
        au auVar = new au(8197);
        auVar.a(HeadData.createHeadData("UpdateMobileNumber.Req", context));
        auVar.a(as.a().A.toString());
        auVar.b(as.a().B.toString());
        auVar.c(str);
        auVar.d(str2);
        auVar.e(as.a().C.toString());
        try {
            co.a().a(auVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void d(UIResponseListener uIResponseListener, Context context, String str, String str2) {
        cq cqVar = new cq(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
        cqVar.a(HeadData.createHeadData("UserLogin.Req", context));
        cqVar.a(str);
        cqVar.b(as.a().C.toString());
        cqVar.c(str2);
        try {
            co.a().a(cqVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
