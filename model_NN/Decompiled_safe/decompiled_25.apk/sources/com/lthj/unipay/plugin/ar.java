package com.lthj.unipay.plugin;

import com.unionpay.upomp.lthj.plugin.model.Data;
import com.unionpay.upomp.lthj.plugin.model.GetBankService;

public class ar extends z {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;

    public ar(int i) {
        super(i);
    }

    public String a() {
        return this.b;
    }

    public void a(Data data) {
        GetBankService getBankService = (GetBankService) data;
        c(getBankService);
        this.b = getBankService.panBank;
        this.c = getBankService.panBankId;
        this.d = getBankService.creditCard;
        this.e = getBankService.debitCard;
        this.f = getBankService.payTips;
    }

    public void a(String str) {
        this.a = str;
    }

    public Data b() {
        GetBankService getBankService = new GetBankService();
        b(getBankService);
        getBankService.payType = this.a;
        return getBankService;
    }

    public String c() {
        return this.c;
    }

    public String d() {
        return this.d;
    }

    public String p() {
        return this.e;
    }
}
