package com.lthj.unipay.plugin;

import com.unionpay.upomp.lthj.plugin.model.Data;
import com.unionpay.upomp.lthj.plugin.model.SetDefaultBandCard;

public class al extends z {
    private String a;
    private StringBuffer b = new StringBuffer();
    private String c;
    private String d;
    private StringBuffer e = new StringBuffer();

    public al(int i) {
        super(i);
    }

    public void a(Data data) {
        SetDefaultBandCard setDefaultBandCard = (SetDefaultBandCard) data;
        c(setDefaultBandCard);
        this.a = setDefaultBandCard.loginName;
        this.b.delete(0, this.b.length());
        this.b.append(setDefaultBandCard.mobileNumber);
    }

    public void a(String str) {
        this.a = str;
    }

    public Data b() {
        SetDefaultBandCard setDefaultBandCard = new SetDefaultBandCard();
        b(setDefaultBandCard);
        setDefaultBandCard.loginName = this.a;
        setDefaultBandCard.mobileNumber = this.b.toString();
        setDefaultBandCard.bindId = this.c;
        setDefaultBandCard.panType = this.d;
        setDefaultBandCard.pan = this.e.toString();
        return setDefaultBandCard;
    }

    public void b(String str) {
        this.b.delete(0, this.b.length());
        this.b.append(str);
    }

    public void c(String str) {
        this.c = str;
    }
}
