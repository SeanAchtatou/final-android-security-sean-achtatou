package com.lthj.unipay.plugin;

import android.util.Xml;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.umeng.common.util.e;
import com.unionpay.upomp.lthj.plugin.model.GetBundleBankCardList;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Vector;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlSerializer;

public class as {
    private static as O = null;
    public StringBuffer A = new StringBuffer();
    public StringBuffer B = new StringBuffer();
    public StringBuffer C = new StringBuffer();
    public Vector D;
    public GetBundleBankCardList E;
    public String F;
    public String G;
    public String H;
    public String I;
    public String J;
    public String K;
    public String L;
    public String M;
    public String N;
    public di a;
    public i b;
    public u c;
    public ac d;
    public boolean e;
    public boolean f = false;
    public boolean g = false;
    public boolean h = false;
    public String i;
    public String j;
    public String k;
    public String l;
    public String m;
    public String n;
    public String o;
    public String p;
    public String q;
    public String r;
    public String s;
    public String t;
    public String u;
    public boolean v;
    public StringBuffer w = new StringBuffer();
    public StringBuffer x = new StringBuffer();
    public StringBuffer y = new StringBuffer();
    public StringBuffer z = new StringBuffer();

    private as() {
        e();
    }

    public static synchronized as a() {
        as asVar;
        synchronized (as.class) {
            if (O == null) {
                O = new as();
            }
            asVar = O;
        }
        return asVar;
    }

    private void e() {
        this.a = new di();
        this.b = new i();
        this.d = new ac();
        this.c = new u();
    }

    public void a(boolean z2) {
        this.e = z2;
        if (z2) {
            this.M = "http://211.154.166.219/qzjy/GateWay/deal.action";
            this.N = "upomp_lthj_config_test.xml";
            return;
        }
        this.M = "http://mobilepay.unionpaysecure.com/qzjy/GateWay/deal.action";
        this.N = "upomp_lthj_config_fromal.xml";
    }

    public void a(byte[] bArr) {
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
            XmlPullParser newPullParser = Xml.newPullParser();
            newPullParser.setInput(byteArrayInputStream, e.f);
            for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
                switch (eventType) {
                    case 2:
                        String name = newPullParser.getName();
                        if (!name.equalsIgnoreCase("merchantId")) {
                            if (!name.equalsIgnoreCase("merchantOrderId")) {
                                if (!name.equalsIgnoreCase("merchantOrderTime")) {
                                    if (!name.equalsIgnoreCase("sign")) {
                                        break;
                                    } else {
                                        this.s = newPullParser.nextText();
                                        break;
                                    }
                                } else {
                                    this.m = newPullParser.nextText();
                                    break;
                                }
                            } else {
                                this.l = newPullParser.nextText();
                                break;
                            }
                        } else {
                            this.i = newPullParser.nextText();
                            break;
                        }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            throw e2;
        }
    }

    public void b() {
        this.d.a();
        O = null;
    }

    public void c() {
        this.A.delete(0, this.A.length());
        this.B.delete(0, this.B.length());
        this.C.delete(0, this.C.length());
    }

    public byte[] d() {
        XmlSerializer newSerializer = Xml.newSerializer();
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            newSerializer.setOutput(byteArrayOutputStream, "utf-8");
            newSerializer.startDocument("utf-8", true);
            newSerializer.startTag(null, "upomp");
            newSerializer.attribute(null, "version", "1.0.0");
            newSerializer.startTag(null, "application");
            newSerializer.text("LanchPay.Rsp");
            newSerializer.endTag(null, "application");
            newSerializer.startTag(null, "merchantId");
            newSerializer.text(this.i);
            newSerializer.endTag(null, "merchantId");
            newSerializer.startTag(null, "merchantOrderId");
            newSerializer.text(this.l);
            newSerializer.endTag(null, "merchantOrderId");
            newSerializer.startTag(null, "merchantOrderTime");
            newSerializer.text(this.m);
            newSerializer.endTag(null, "merchantOrderTime");
            newSerializer.startTag(null, "respCode");
            if (this.t != null) {
                newSerializer.text(this.t);
            } else {
                newSerializer.text(PoiTypeDef.All);
            }
            newSerializer.endTag(null, "respCode");
            newSerializer.startTag(null, "respDesc");
            if (this.u != null) {
                newSerializer.text(this.u);
            } else {
                newSerializer.text(PoiTypeDef.All);
            }
            newSerializer.endTag(null, "respDesc");
            newSerializer.endTag(null, "upomp");
            newSerializer.endDocument();
            return byteArrayOutputStream.toByteArray();
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
