package com.lthj.unipay.plugin;

import android.content.Intent;
import android.view.View;
import com.unionpay.upomp.lthj.plugin.ui.AccountActivity;
import com.unionpay.upomp.lthj.plugin.ui.BankCardInfoActivity;

public class dc implements View.OnClickListener {
    final /* synthetic */ BankCardInfoActivity a;

    public dc(BankCardInfoActivity bankCardInfoActivity) {
        this.a = bankCardInfoActivity;
    }

    public void onClick(View view) {
        Intent intent = new Intent(this.a, AccountActivity.class);
        intent.addFlags(67108864);
        this.a.a().changeSubActivity(intent);
    }
}
