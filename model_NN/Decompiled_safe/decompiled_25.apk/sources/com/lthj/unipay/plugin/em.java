package com.lthj.unipay.plugin;

import android.content.Intent;
import android.view.View;
import com.unionpay.upomp.lthj.plugin.ui.PayActivity;
import com.unionpay.upomp.lthj.plugin.ui.UserProtocolActivity;

public class em implements View.OnClickListener {
    final /* synthetic */ UserProtocolActivity a;

    public em(UserProtocolActivity userProtocolActivity) {
        this.a = userProtocolActivity;
    }

    public void onClick(View view) {
        this.a.a().changeSubActivity(new Intent(this.a, PayActivity.class));
    }
}
