package com.lthj.unipay.plugin;

import android.os.Message;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.unionpay.upomp.lthj.plugin.ui.BankCardInfoActivity;
import java.util.TimerTask;

public class dd extends TimerTask {
    final /* synthetic */ BankCardInfoActivity a;

    public dd(BankCardInfoActivity bankCardInfoActivity) {
        this.a = bankCardInfoActivity;
    }

    public void run() {
        Message obtain = Message.obtain();
        obtain.obj = this.a.b + PoiTypeDef.All;
        this.a.l.sendMessage(obtain);
        if (this.a.b < 0) {
            this.a.aaTimerTask.cancel();
            this.a.aaTimerTask = null;
            this.a.b = 60;
            return;
        }
        BankCardInfoActivity bankCardInfoActivity = this.a;
        bankCardInfoActivity.b--;
    }
}
