package com.lthj.unipay.plugin;

import com.unionpay.upomp.lthj.plugin.model.Data;
import com.unionpay.upomp.lthj.plugin.model.RetrievePassword;

public class m extends z {
    private String a;
    private StringBuffer b = new StringBuffer();
    private String c;
    private String d;
    private String e;

    public m(int i) {
        super(i);
    }

    public void a(Data data) {
        RetrievePassword retrievePassword = (RetrievePassword) data;
        c(retrievePassword);
        this.a = retrievePassword.loginName;
        this.c = retrievePassword.email;
        this.e = retrievePassword.secureAnswer;
    }

    public void a(String str) {
        this.a = str;
    }

    public Data b() {
        RetrievePassword retrievePassword = new RetrievePassword();
        b(retrievePassword);
        retrievePassword.loginName = this.a;
        retrievePassword.newPassword = this.b.toString();
        retrievePassword.validateCode = this.d;
        retrievePassword.secureAnswer = this.e;
        return retrievePassword;
    }

    public void b(String str) {
        this.b.delete(0, this.b.length());
        this.b.append(str);
    }

    public void c(String str) {
        this.e = str;
    }
}
