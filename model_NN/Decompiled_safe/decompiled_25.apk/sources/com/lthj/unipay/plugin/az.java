package com.lthj.unipay.plugin;

import com.unionpay.upomp.lthj.plugin.model.Data;
import com.unionpay.upomp.lthj.plugin.model.OrderInfoCheck;

public class az extends z {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;
    private String i;
    private String j;
    private String k;
    private String l;

    public az(int i2) {
        super(i2);
    }

    public String a() {
        return this.a;
    }

    public void a(Data data) {
        OrderInfoCheck orderInfoCheck = (OrderInfoCheck) data;
        c(orderInfoCheck);
        this.a = orderInfoCheck.merchantName;
        this.b = orderInfoCheck.merchantId;
        this.e = orderInfoCheck.merchantOrderId;
        this.f = orderInfoCheck.merchantOrderTime;
        this.g = orderInfoCheck.merchantOrderAmt;
        this.h = orderInfoCheck.merchantOrderDesc;
        this.i = orderInfoCheck.transTimeout;
        this.j = orderInfoCheck.backEndUrl;
        this.k = orderInfoCheck.sign;
        this.l = orderInfoCheck.merchantPublicCert;
        this.c = orderInfoCheck.activityId;
        this.d = orderInfoCheck.isPreAuth;
    }

    public void a(String str) {
        this.b = str;
    }

    public Data b() {
        OrderInfoCheck orderInfoCheck = new OrderInfoCheck();
        b(orderInfoCheck);
        orderInfoCheck.merchantId = this.b;
        orderInfoCheck.merchantOrderId = this.e;
        orderInfoCheck.merchantOrderTime = this.f;
        orderInfoCheck.sign = this.k;
        orderInfoCheck.isPreAuth = this.d;
        return orderInfoCheck;
    }

    public void b(String str) {
        this.e = str;
    }

    public String c() {
        return this.g;
    }

    public void c(String str) {
        this.f = str;
    }

    public String d() {
        return this.h;
    }

    public void d(String str) {
        this.k = str;
    }

    public String p() {
        return this.i;
    }

    public String q() {
        return this.j;
    }

    public String r() {
        return this.k;
    }

    public String s() {
        return this.l;
    }

    public String t() {
        return this.c;
    }
}
