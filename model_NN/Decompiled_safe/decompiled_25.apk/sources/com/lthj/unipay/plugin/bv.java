package com.lthj.unipay.plugin;

import android.content.Intent;
import android.view.View;
import com.unionpay.upomp.lthj.plugin.ui.AccountActivity;
import com.unionpay.upomp.lthj.plugin.ui.HomeActivity;

public class bv implements View.OnClickListener {
    final /* synthetic */ AccountActivity a;

    public bv(AccountActivity accountActivity) {
        this.a = accountActivity;
    }

    public void onClick(View view) {
        Intent intent = new Intent(this.a, HomeActivity.class);
        intent.addFlags(67108864);
        this.a.a().changeSubActivity(intent);
        l.a().b();
    }
}
