package com.lthj.unipay.plugin;

import android.content.Intent;
import android.view.View;
import com.unionpay.upomp.lthj.plugin.ui.HomeActivity;
import com.unionpay.upomp.lthj.plugin.ui.SupportCardActivity;

public class x implements View.OnClickListener {
    final /* synthetic */ SupportCardActivity a;

    public x(SupportCardActivity supportCardActivity) {
        this.a = supportCardActivity;
    }

    public void onClick(View view) {
        this.a.a().changeSubActivity(new Intent(this.a, HomeActivity.class));
    }
}
