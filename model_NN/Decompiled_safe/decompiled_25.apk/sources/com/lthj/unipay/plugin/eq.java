package com.lthj.unipay.plugin;

import android.content.DialogInterface;
import com.unionpay.upomp.lthj.plugin.model.GetBundleBankCardList;
import com.unionpay.upomp.lthj.plugin.ui.PayActivity;

public class eq implements DialogInterface.OnClickListener {
    final /* synthetic */ PayActivity a;

    public eq(PayActivity payActivity) {
        this.a = payActivity;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        as.a().E = (GetBundleBankCardList) as.a().D.get(i);
        this.a.l.a(as.a().E.panBank + "-" + j.b(as.a().E.panType) + "-" + as.a().E.pan.substring(as.a().E.pan.length() - 4, as.a().E.pan.length()));
        this.a.m.a(j.f(as.a().E.mobileNumber));
    }
}
