package com.lthj.unipay.plugin;

import android.os.Handler;
import android.os.Message;
import com.unionpay.upomp.lthj.plugin.ui.SplashActivity;

public class ae extends Handler {
    final /* synthetic */ SplashActivity a;

    public ae(SplashActivity splashActivity) {
        this.a = splashActivity;
    }

    public void handleMessage(Message message) {
        if (message.obj != null) {
            this.a.initBottomData((byte[]) message.obj);
        } else {
            this.a.initBottomData(null);
        }
        this.a.a();
    }
}
