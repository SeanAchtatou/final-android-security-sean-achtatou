package com.lthj.unipay.plugin;

import android.os.Handler;
import android.os.Message;
import com.unionpay.upomp.lthj.link.PluginLink;
import com.unionpay.upomp.lthj.plugin.ui.AccountActivity;

public class cb extends Handler {
    final /* synthetic */ AccountActivity a;

    public cb(AccountActivity accountActivity) {
        this.a = accountActivity;
    }

    public void handleMessage(Message message) {
        String str = (String) message.obj;
        if (this.a.m == null) {
            return;
        }
        if (Integer.parseInt(str) < 0) {
            this.a.m.setEnabled(true);
            this.a.m.setText(this.a.getString(PluginLink.getStringupomp_lthj_click_get_mac()));
            return;
        }
        this.a.m.setEnabled(false);
        this.a.m.setText(str + this.a.getString(PluginLink.getStringupomp_lthj_after_getmobilemacAgain()));
    }
}
