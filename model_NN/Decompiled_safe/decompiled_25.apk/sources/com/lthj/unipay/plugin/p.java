package com.lthj.unipay.plugin;

import com.unionpay.upomp.lthj.plugin.model.Data;
import com.unionpay.upomp.lthj.plugin.model.ModifyPassword;

public class p extends z {
    private String a;
    private StringBuffer b = new StringBuffer();
    private StringBuffer c = new StringBuffer();
    private StringBuffer d = new StringBuffer();
    private String e;

    public p(int i) {
        super(i);
    }

    public void a(Data data) {
        ModifyPassword modifyPassword = (ModifyPassword) data;
        c(modifyPassword);
        this.a = modifyPassword.loginName;
        this.b.delete(0, this.b.length());
        this.b.append(modifyPassword.password);
        this.c.delete(0, this.c.length());
        this.c.append(modifyPassword.newPassword);
    }

    public void a(String str) {
        this.a = str;
    }

    public Data b() {
        ModifyPassword modifyPassword = new ModifyPassword();
        b(modifyPassword);
        modifyPassword.loginName = this.a;
        modifyPassword.password = this.b.toString();
        modifyPassword.newPassword = this.c.toString();
        modifyPassword.mobileNumber = this.d.toString();
        modifyPassword.mobileMac = this.e;
        return modifyPassword;
    }

    public void b(String str) {
        this.b.delete(0, this.b.length());
        this.b.append(str);
    }

    public void c(String str) {
        this.c.delete(0, this.c.length());
        this.c.append(str);
    }

    public void d(String str) {
        this.d.delete(0, this.d.length());
        this.d.append(str);
    }

    public void e(String str) {
        this.e = str;
    }
}
