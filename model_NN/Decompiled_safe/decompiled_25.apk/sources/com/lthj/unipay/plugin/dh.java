package com.lthj.unipay.plugin;

import com.unionpay.upomp.lthj.plugin.model.BankCardInfo;
import com.unionpay.upomp.lthj.plugin.model.Data;

public class dh extends z {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;

    public dh(int i) {
        super(i);
    }

    public String a() {
        return this.c;
    }

    public void a(Data data) {
        BankCardInfo bankCardInfo = (BankCardInfo) data;
        c(bankCardInfo);
        this.c = bankCardInfo.panType;
        this.d = bankCardInfo.panBank;
        this.e = bankCardInfo.payTips;
    }

    public void a(String str) {
        this.a = str;
    }

    public Data b() {
        BankCardInfo bankCardInfo = new BankCardInfo();
        b(bankCardInfo);
        bankCardInfo.pan = this.a;
        bankCardInfo.validateCode = this.b;
        bankCardInfo.activityId = this.f;
        return bankCardInfo;
    }

    public void b(String str) {
        this.b = str;
    }

    public String c() {
        return this.d;
    }

    public void c(String str) {
        this.f = str;
    }
}
