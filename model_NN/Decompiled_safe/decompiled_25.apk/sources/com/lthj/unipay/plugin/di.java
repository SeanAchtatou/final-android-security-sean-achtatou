package com.lthj.unipay.plugin;

import java.util.Vector;

public class di {
    private String a;
    private String b;
    private byte[] c;
    private byte[] d;
    private String e;
    private String f;
    private byte[] g;
    private byte[] h;
    private String i;
    private byte j;
    private long k;
    private String l;
    private Vector m;
    private Vector n;

    public String a() {
        return this.a;
    }

    /* JADX WARNING: Removed duplicated region for block: B:120:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x003d A[SYNTHETIC, Splitter:B:16:0x003d] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0083 A[SYNTHETIC, Splitter:B:33:0x0083] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(byte[] r8) {
        /*
            r7 = this;
            r2 = 0
            java.lang.String r3 = ""
            java.io.ByteArrayInputStream r1 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x01a8, all -> 0x01a4 }
            r1.<init>(r8)     // Catch:{ Exception -> 0x01a8, all -> 0x01a4 }
            org.xmlpull.v1.XmlPullParser r4 = android.util.Xml.newPullParser()     // Catch:{ Exception -> 0x0037 }
            java.lang.String r0 = "UTF-8"
            r4.setInput(r1, r0)     // Catch:{ Exception -> 0x0037 }
            int r0 = r4.getEventType()     // Catch:{ Exception -> 0x0037 }
            r2 = r3
        L_0x0016:
            r3 = 1
            if (r0 == r3) goto L_0x0194
            switch(r0) {
                case 0: goto L_0x001c;
                case 1: goto L_0x001c;
                case 2: goto L_0x0021;
                case 3: goto L_0x001c;
                default: goto L_0x001c;
            }     // Catch:{ Exception -> 0x0037 }
        L_0x001c:
            int r0 = r4.next()     // Catch:{ Exception -> 0x0037 }
            goto L_0x0016
        L_0x0021:
            java.lang.String r0 = r4.getName()     // Catch:{ Exception -> 0x0037 }
            java.lang.String r3 = "config"
            boolean r3 = r0.equalsIgnoreCase(r3)     // Catch:{ Exception -> 0x0037 }
            if (r3 == 0) goto L_0x0041
            r0 = 0
            java.lang.String r3 = "version"
            java.lang.String r0 = r4.getAttributeValue(r0, r3)     // Catch:{ Exception -> 0x0037 }
            r7.a = r0     // Catch:{ Exception -> 0x0037 }
            goto L_0x001c
        L_0x0037:
            r0 = move-exception
        L_0x0038:
            r0.printStackTrace()     // Catch:{ all -> 0x0080 }
            if (r1 == 0) goto L_0x0040
            r1.close()     // Catch:{ Exception -> 0x019e }
        L_0x0040:
            return
        L_0x0041:
            java.lang.String r3 = "frontPubKey"
            boolean r3 = r0.equalsIgnoreCase(r3)     // Catch:{ Exception -> 0x0037 }
            if (r3 == 0) goto L_0x0055
            r0 = 0
            java.lang.String r2 = "version"
            java.lang.String r0 = r4.getAttributeValue(r0, r2)     // Catch:{ Exception -> 0x0037 }
            r7.e = r0     // Catch:{ Exception -> 0x0037 }
            java.lang.String r2 = "frontPubKey"
            goto L_0x001c
        L_0x0055:
            java.lang.String r3 = "upopPubkey"
            boolean r3 = r0.equalsIgnoreCase(r3)     // Catch:{ Exception -> 0x0037 }
            if (r3 == 0) goto L_0x0069
            r0 = 0
            java.lang.String r2 = "version"
            java.lang.String r0 = r4.getAttributeValue(r0, r2)     // Catch:{ Exception -> 0x0037 }
            r7.i = r0     // Catch:{ Exception -> 0x0037 }
            java.lang.String r2 = "upopPubkey"
            goto L_0x001c
        L_0x0069:
            java.lang.String r3 = "oneline"
            boolean r3 = r0.equalsIgnoreCase(r3)     // Catch:{ Exception -> 0x0037 }
            if (r3 == 0) goto L_0x0096
            java.lang.String r0 = "frontPubKey"
            boolean r0 = r0.equals(r2)     // Catch:{ Exception -> 0x0037 }
            if (r0 == 0) goto L_0x0087
            java.lang.String r0 = r4.nextText()     // Catch:{ Exception -> 0x0037 }
            r7.b = r0     // Catch:{ Exception -> 0x0037 }
            goto L_0x001c
        L_0x0080:
            r0 = move-exception
        L_0x0081:
            if (r1 == 0) goto L_0x0086
            r1.close()     // Catch:{ Exception -> 0x01a1 }
        L_0x0086:
            throw r0
        L_0x0087:
            java.lang.String r0 = "upopPubkey"
            boolean r0 = r0.equals(r2)     // Catch:{ Exception -> 0x0037 }
            if (r0 == 0) goto L_0x001c
            java.lang.String r0 = r4.nextText()     // Catch:{ Exception -> 0x0037 }
            r7.f = r0     // Catch:{ Exception -> 0x0037 }
            goto L_0x001c
        L_0x0096:
            java.lang.String r3 = "n"
            boolean r3 = r0.equalsIgnoreCase(r3)     // Catch:{ Exception -> 0x0037 }
            if (r3 == 0) goto L_0x00c6
            java.lang.String r0 = "frontPubKey"
            boolean r0 = r0.equals(r2)     // Catch:{ Exception -> 0x0037 }
            if (r0 == 0) goto L_0x00b2
            java.lang.String r0 = r4.nextText()     // Catch:{ Exception -> 0x0037 }
            byte[] r0 = com.lthj.unipay.plugin.dg.a(r0)     // Catch:{ Exception -> 0x0037 }
            r7.c = r0     // Catch:{ Exception -> 0x0037 }
            goto L_0x001c
        L_0x00b2:
            java.lang.String r0 = "upopPubkey"
            boolean r0 = r0.equals(r2)     // Catch:{ Exception -> 0x0037 }
            if (r0 == 0) goto L_0x001c
            java.lang.String r0 = r4.nextText()     // Catch:{ Exception -> 0x0037 }
            byte[] r0 = com.lthj.unipay.plugin.dg.a(r0)     // Catch:{ Exception -> 0x0037 }
            r7.g = r0     // Catch:{ Exception -> 0x0037 }
            goto L_0x001c
        L_0x00c6:
            java.lang.String r3 = "e"
            boolean r3 = r0.equalsIgnoreCase(r3)     // Catch:{ Exception -> 0x0037 }
            if (r3 == 0) goto L_0x00f6
            java.lang.String r0 = "frontPubKey"
            boolean r0 = r0.equals(r2)     // Catch:{ Exception -> 0x0037 }
            if (r0 == 0) goto L_0x00e2
            java.lang.String r0 = r4.nextText()     // Catch:{ Exception -> 0x0037 }
            byte[] r0 = com.lthj.unipay.plugin.dg.a(r0)     // Catch:{ Exception -> 0x0037 }
            r7.d = r0     // Catch:{ Exception -> 0x0037 }
            goto L_0x001c
        L_0x00e2:
            java.lang.String r0 = "upopPubkey"
            boolean r0 = r0.equals(r2)     // Catch:{ Exception -> 0x0037 }
            if (r0 == 0) goto L_0x001c
            java.lang.String r0 = r4.nextText()     // Catch:{ Exception -> 0x0037 }
            byte[] r0 = com.lthj.unipay.plugin.dg.a(r0)     // Catch:{ Exception -> 0x0037 }
            r7.h = r0     // Catch:{ Exception -> 0x0037 }
            goto L_0x001c
        L_0x00f6:
            java.lang.String r3 = "bindCardCount"
            boolean r3 = r0.equalsIgnoreCase(r3)     // Catch:{ Exception -> 0x0037 }
            if (r3 == 0) goto L_0x010f
            java.lang.Byte r0 = new java.lang.Byte     // Catch:{ Exception -> 0x0037 }
            java.lang.String r3 = r4.nextText()     // Catch:{ Exception -> 0x0037 }
            r0.<init>(r3)     // Catch:{ Exception -> 0x0037 }
            byte r0 = r0.byteValue()     // Catch:{ Exception -> 0x0037 }
            r7.j = r0     // Catch:{ Exception -> 0x0037 }
            goto L_0x001c
        L_0x010f:
            java.lang.String r3 = "allowBalance"
            boolean r3 = r0.equalsIgnoreCase(r3)     // Catch:{ Exception -> 0x0037 }
            if (r3 == 0) goto L_0x011f
            java.lang.String r0 = r4.nextText()     // Catch:{ Exception -> 0x0037 }
            r7.l = r0     // Catch:{ Exception -> 0x0037 }
            goto L_0x001c
        L_0x011f:
            java.lang.String r3 = "paymentMethod"
            boolean r3 = r0.equalsIgnoreCase(r3)     // Catch:{ Exception -> 0x0037 }
            if (r3 == 0) goto L_0x0132
            java.util.Vector r0 = new java.util.Vector     // Catch:{ Exception -> 0x0037 }
            r0.<init>()     // Catch:{ Exception -> 0x0037 }
            r7.m = r0     // Catch:{ Exception -> 0x0037 }
            java.lang.String r2 = "paymentMethod"
            goto L_0x001c
        L_0x0132:
            java.lang.String r3 = "secureQuestion"
            boolean r3 = r0.equalsIgnoreCase(r3)     // Catch:{ Exception -> 0x0037 }
            if (r3 == 0) goto L_0x0145
            java.util.Vector r0 = new java.util.Vector     // Catch:{ Exception -> 0x0037 }
            r0.<init>()     // Catch:{ Exception -> 0x0037 }
            r7.n = r0     // Catch:{ Exception -> 0x0037 }
            java.lang.String r2 = "secureQuestion"
            goto L_0x001c
        L_0x0145:
            java.lang.String r3 = "item"
            boolean r3 = r0.equalsIgnoreCase(r3)     // Catch:{ Exception -> 0x0037 }
            if (r3 == 0) goto L_0x017b
            java.lang.String r0 = "paymentMethod"
            boolean r0 = r0.equals(r2)     // Catch:{ Exception -> 0x0037 }
            if (r0 == 0) goto L_0x0164
            java.util.Vector r0 = r7.m     // Catch:{ Exception -> 0x0037 }
            if (r0 == 0) goto L_0x001c
            java.util.Vector r0 = r7.m     // Catch:{ Exception -> 0x0037 }
            java.lang.String r3 = r4.nextText()     // Catch:{ Exception -> 0x0037 }
            r0.add(r3)     // Catch:{ Exception -> 0x0037 }
            goto L_0x001c
        L_0x0164:
            java.lang.String r0 = "secureQuestion"
            boolean r0 = r0.equals(r2)     // Catch:{ Exception -> 0x0037 }
            if (r0 == 0) goto L_0x001c
            java.util.Vector r0 = r7.n     // Catch:{ Exception -> 0x0037 }
            if (r0 == 0) goto L_0x001c
            java.util.Vector r0 = r7.n     // Catch:{ Exception -> 0x0037 }
            java.lang.String r3 = r4.nextText()     // Catch:{ Exception -> 0x0037 }
            r0.add(r3)     // Catch:{ Exception -> 0x0037 }
            goto L_0x001c
        L_0x017b:
            java.lang.String r3 = "serverTimeout"
            boolean r0 = r0.equalsIgnoreCase(r3)     // Catch:{ Exception -> 0x0037 }
            if (r0 == 0) goto L_0x001c
            java.lang.Long r0 = new java.lang.Long     // Catch:{ Exception -> 0x0037 }
            java.lang.String r3 = r4.nextText()     // Catch:{ Exception -> 0x0037 }
            r0.<init>(r3)     // Catch:{ Exception -> 0x0037 }
            long r5 = r0.longValue()     // Catch:{ Exception -> 0x0037 }
            r7.k = r5     // Catch:{ Exception -> 0x0037 }
            goto L_0x001c
        L_0x0194:
            if (r1 == 0) goto L_0x0040
            r1.close()     // Catch:{ Exception -> 0x019b }
            goto L_0x0040
        L_0x019b:
            r0 = move-exception
            goto L_0x0040
        L_0x019e:
            r0 = move-exception
            goto L_0x0040
        L_0x01a1:
            r1 = move-exception
            goto L_0x0086
        L_0x01a4:
            r0 = move-exception
            r1 = r2
            goto L_0x0081
        L_0x01a8:
            r0 = move-exception
            r1 = r2
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lthj.unipay.plugin.di.a(byte[]):void");
    }

    public String b() {
        return this.e;
    }

    public byte c() {
        return this.j;
    }

    public byte[] d() {
        return this.c;
    }

    public byte[] e() {
        return this.d;
    }

    public byte[] f() {
        return this.g;
    }

    public byte[] g() {
        return this.h;
    }

    public Vector h() {
        return this.n;
    }
}
