package com.lthj.unipay.plugin;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Message;
import android.util.Log;
import com.umeng.common.util.e;
import com.unionpay.upomp.lthj.plugin.ui.JniMethod;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;

class ay implements Runnable {
    final /* synthetic */ de a;
    private OutputStream b;
    private InputStream c;
    private HttpURLConnection d;
    private Proxy e;

    ay(de deVar) {
        this.a = deVar;
    }

    private void a(String str) {
        if (str.length() != 0) {
            byte[] chackPanDecrypt = JniMethod.getJniMethod().chackPanDecrypt(str.getBytes(), str.length());
            if (chackPanDecrypt == null) {
                throw new Exception("数据异常,jni解密错误");
            }
            String str2 = new String(chackPanDecrypt);
            String[] split = str2.split("\\|");
            aw.a("binnet", str2);
            if ("0000".equals(split[0])) {
                this.a.a.sendEmptyMessage(1);
            } else {
                b("(" + split[0] + ")" + split[1]);
            }
        } else {
            throw new Exception("服务器返回的数据错误");
        }
    }

    private void a(byte[] bArr, int i, int i2) {
        this.b.write(bArr, i, i2);
        this.b.flush();
        Log.d("---plugin-send data length=", " " + bArr.length);
    }

    private void b(String str) {
        Message obtain = Message.obtain();
        obtain.obj = str;
        obtain.what = 0;
        this.a.a.sendMessage(obtain);
    }

    private void c() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.a.j.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isAvailable() && activeNetworkInfo.getType() == 0) {
            String defaultHost = android.net.Proxy.getDefaultHost();
            int defaultPort = android.net.Proxy.getDefaultPort();
            if (defaultHost != null) {
                this.e = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(defaultHost, defaultPort));
            }
        }
    }

    public void a() {
        URL url = new URL(as.a().e ? "http://211.154.166.219/busplat/order/checkPan.action" : "http://auth.uupay.net/busplat/order/checkPan.action");
        if (this.e != null) {
            this.d = (HttpURLConnection) url.openConnection(this.e);
        } else {
            this.d = (HttpURLConnection) url.openConnection();
        }
        this.d.setRequestMethod("POST");
        this.d.setUseCaches(false);
        this.d.setRequestProperty("Charset", e.f);
        this.d.setRequestProperty("Content-Type", "text/plain");
        this.d.setDoInput(true);
        this.d.setDoOutput(true);
        this.d.setConnectTimeout(120000);
        this.d.setReadTimeout(120000);
        if (at.a != null) {
            this.d.setRequestProperty("sessionId", at.a);
            Log.d("---HttpNet-connect-sessionId-=", at.a);
        } else {
            Log.d("---HttpNet-connect-sessionId-is null=", "sessionId is null");
        }
        this.b = new DataOutputStream(this.d.getOutputStream());
        byte[] chackPanEncrypt = JniMethod.getJniMethod().chackPanEncrypt(this.a.k.getBytes(), this.a.k.length());
        a(chackPanEncrypt, 0, chackPanEncrypt.length);
        this.d.connect();
        int responseCode = this.d.getResponseCode();
        if (this.d.getResponseCode() != 200) {
            throw new Exception(responseCode == 504 ? "网络连接超时, 请检查您的网络设置, 或稍候重试!" : "网络连接失败, 请检查您的网络设置, 或稍候重试!\r\n错误号: " + Integer.toString(responseCode));
        }
    }

    public void b() {
        try {
            if (this.c != null) {
                this.c.close();
                this.c = null;
            }
            if (this.b != null) {
                this.b.close();
                this.b = null;
            }
            if (this.d != null) {
                this.d.disconnect();
                this.d = null;
            }
        } catch (Exception e2) {
            try {
                this.d = null;
                this.b = null;
                this.c = null;
            } catch (Exception e3) {
                Log.d("---HttpNet-disconnect-e=", e3.getMessage());
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0044 A[SYNTHETIC, Splitter:B:19:0x0044] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0049 A[Catch:{ IOException -> 0x0089 }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0092 A[SYNTHETIC, Splitter:B:45:0x0092] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0097 A[Catch:{ IOException -> 0x009e }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r5 = this;
            r2 = 0
            r5.c()
            r5.a()     // Catch:{ Exception -> 0x0050 }
        L_0x0007:
            java.net.HttpURLConnection r0 = r5.d     // Catch:{ Exception -> 0x00ac, all -> 0x008e }
            int r0 = r0.getResponseCode()     // Catch:{ Exception -> 0x00ac, all -> 0x008e }
            r1 = 200(0xc8, float:2.8E-43)
            if (r0 != r1) goto L_0x007a
            java.net.HttpURLConnection r0 = r5.d     // Catch:{ Exception -> 0x00ac, all -> 0x008e }
            java.io.InputStream r0 = r0.getInputStream()     // Catch:{ Exception -> 0x00ac, all -> 0x008e }
            r5.c = r0     // Catch:{ Exception -> 0x00ac, all -> 0x008e }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x00ac, all -> 0x008e }
            java.io.InputStream r0 = r5.c     // Catch:{ Exception -> 0x00ac, all -> 0x008e }
            r3.<init>(r0)     // Catch:{ Exception -> 0x00ac, all -> 0x008e }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ Exception -> 0x00af, all -> 0x00a3 }
            r0 = 8192(0x2000, float:1.14794E-41)
            r1.<init>(r3, r0)     // Catch:{ Exception -> 0x00af, all -> 0x00a3 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0036, all -> 0x00a7 }
            r0.<init>()     // Catch:{ Exception -> 0x0036, all -> 0x00a7 }
        L_0x002c:
            java.lang.String r2 = r1.readLine()     // Catch:{ Exception -> 0x0036, all -> 0x00a7 }
            if (r2 == 0) goto L_0x005c
            r0.append(r2)     // Catch:{ Exception -> 0x0036, all -> 0x00a7 }
            goto L_0x002c
        L_0x0036:
            r0 = move-exception
            r2 = r3
        L_0x0038:
            com.lthj.unipay.plugin.de r3 = r5.a     // Catch:{ all -> 0x00aa }
            java.lang.String r4 = "网络异常"
            r3.a(r4)     // Catch:{ all -> 0x00aa }
            r0.printStackTrace()     // Catch:{ all -> 0x00aa }
            if (r1 == 0) goto L_0x0047
            r1.close()     // Catch:{ IOException -> 0x0089 }
        L_0x0047:
            if (r2 == 0) goto L_0x004c
            r2.close()     // Catch:{ IOException -> 0x0089 }
        L_0x004c:
            r5.b()
        L_0x004f:
            return
        L_0x0050:
            r0 = move-exception
            r0.printStackTrace()
            com.lthj.unipay.plugin.de r0 = r5.a
            java.lang.String r1 = "网络连接失败,请检查网络"
            r0.a(r1)
            goto L_0x0007
        L_0x005c:
            java.lang.String r2 = "net data"
            java.lang.String r4 = r0.toString()     // Catch:{ Exception -> 0x0036, all -> 0x00a7 }
            com.lthj.unipay.plugin.aw.a(r2, r4)     // Catch:{ Exception -> 0x0036, all -> 0x00a7 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0036, all -> 0x00a7 }
            r5.a(r0)     // Catch:{ Exception -> 0x0036, all -> 0x00a7 }
        L_0x006c:
            if (r1 == 0) goto L_0x0071
            r1.close()     // Catch:{ IOException -> 0x0084 }
        L_0x0071:
            if (r3 == 0) goto L_0x0076
            r3.close()     // Catch:{ IOException -> 0x0084 }
        L_0x0076:
            r5.b()
            goto L_0x004f
        L_0x007a:
            com.lthj.unipay.plugin.de r0 = r5.a     // Catch:{ Exception -> 0x00ac, all -> 0x008e }
            java.lang.String r1 = "网络异常"
            r0.a(r1)     // Catch:{ Exception -> 0x00ac, all -> 0x008e }
            r1 = r2
            r3 = r2
            goto L_0x006c
        L_0x0084:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0076
        L_0x0089:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x004c
        L_0x008e:
            r0 = move-exception
            r1 = r2
        L_0x0090:
            if (r1 == 0) goto L_0x0095
            r1.close()     // Catch:{ IOException -> 0x009e }
        L_0x0095:
            if (r2 == 0) goto L_0x009a
            r2.close()     // Catch:{ IOException -> 0x009e }
        L_0x009a:
            r5.b()
            throw r0
        L_0x009e:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x009a
        L_0x00a3:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x0090
        L_0x00a7:
            r0 = move-exception
            r2 = r3
            goto L_0x0090
        L_0x00aa:
            r0 = move-exception
            goto L_0x0090
        L_0x00ac:
            r0 = move-exception
            r1 = r2
            goto L_0x0038
        L_0x00af:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lthj.unipay.plugin.ay.run():void");
    }
}
