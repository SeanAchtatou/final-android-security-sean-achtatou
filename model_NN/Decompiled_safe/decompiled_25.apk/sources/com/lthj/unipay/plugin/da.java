package com.lthj.unipay.plugin;

import android.os.Handler;
import android.os.Message;
import com.unionpay.upomp.lthj.link.PluginLink;
import com.unionpay.upomp.lthj.plugin.ui.BankCardInfoActivity;

public class da extends Handler {
    final /* synthetic */ BankCardInfoActivity a;

    public da(BankCardInfoActivity bankCardInfoActivity) {
        this.a = bankCardInfoActivity;
    }

    public void handleMessage(Message message) {
        String str = (String) message.obj;
        if (this.a.w == null) {
            return;
        }
        if (Integer.parseInt(str) < 0) {
            this.a.w.setEnabled(true);
            this.a.w.setText(this.a.getString(PluginLink.getStringupomp_lthj_click_get_mac()));
            return;
        }
        this.a.w.setEnabled(false);
        this.a.w.setText(str + this.a.getString(PluginLink.getStringupomp_lthj_after_getmobilemacAgain()));
    }
}
