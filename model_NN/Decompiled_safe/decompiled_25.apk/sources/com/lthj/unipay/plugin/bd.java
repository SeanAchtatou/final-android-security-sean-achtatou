package com.lthj.unipay.plugin;

import android.content.Intent;
import android.view.View;
import com.unionpay.upomp.lthj.plugin.ui.PayActivity;
import com.unionpay.upomp.lthj.plugin.ui.UserProtocolActivity;

public class bd implements View.OnClickListener {
    final /* synthetic */ PayActivity a;

    public bd(PayActivity payActivity) {
        this.a = payActivity;
    }

    public void onClick(View view) {
        Intent intent = new Intent(this.a, UserProtocolActivity.class);
        intent.addFlags(67108864);
        this.a.a().changeSubActivity(intent);
    }
}
