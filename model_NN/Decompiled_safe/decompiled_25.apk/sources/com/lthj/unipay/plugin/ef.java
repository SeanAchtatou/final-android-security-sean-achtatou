package com.lthj.unipay.plugin;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import com.unionpay.upomp.lthj.widget.ValidateCodeView;

public class ef extends Handler {
    final /* synthetic */ ValidateCodeView a;

    public ef(ValidateCodeView validateCodeView) {
        this.a = validateCodeView;
    }

    public void handleMessage(Message message) {
        Bitmap bitmap = (Bitmap) message.obj;
        if (bitmap != null) {
            this.a.c.setVisibility(0);
            this.a.c.setImageBitmap(bitmap);
            this.a.d.setVisibility(8);
        }
    }
}
