package com.lthj.unipay.plugin;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import com.unionpay.upomp.lthj.link.PluginLink;
import com.unionpay.upomp.lthj.plugin.model.Data;
import com.unionpay.upomp.lthj.plugin.model.JNIBottomBackData;
import com.unionpay.upomp.lthj.plugin.ui.JniMethod;
import com.unionpay.upomp.lthj.plugin.ui.UIResponseListener;
import java.util.Queue;

public class co implements df {
    private static co b;
    protected Context a;
    /* access modifiers changed from: private */
    public UIResponseListener c;
    /* access modifiers changed from: private */
    public z d;
    /* access modifiers changed from: private */
    public Queue e;
    /* access modifiers changed from: private */
    public at f;
    private Handler g = new cx(this);
    private Handler h = new cy(this);

    public static co a() {
        if (b == null) {
            b = new co();
        }
        return b;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.lthj.unipay.plugin.j.a(android.content.Context, boolean):boolean
     arg types: [android.content.Context, int]
     candidates:
      com.lthj.unipay.plugin.j.a(android.content.Context, java.lang.String):void
      com.lthj.unipay.plugin.j.a(java.lang.String, char):java.lang.String[]
      com.lthj.unipay.plugin.j.a(android.content.Context, boolean):boolean */
    /* access modifiers changed from: private */
    public void a(z zVar) {
        if ("0000".equals(zVar.n())) {
            switch (zVar.e()) {
                case FragmentTransaction.TRANSIT_FRAGMENT_CLOSE:
                case 8195:
                case 8225:
                    if (this.a != null) {
                        j.a(this.a, true);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: private */
    public void b(z zVar) {
        int i = 1;
        switch (zVar.e()) {
            case 8193:
            case 8195:
            case 8196:
            case 8197:
            case 8203:
                break;
            case 8210:
                if (!"1".equals(((ea) zVar).a())) {
                    if ("2".equals(((ea) zVar).a())) {
                        i = 4;
                        break;
                    }
                    i = 99;
                    break;
                } else {
                    i = 3;
                    break;
                }
            case 8227:
                if (!"1".equals(((ek) zVar).a())) {
                    if ("2".equals(((ek) zVar).a())) {
                        i = 9;
                        break;
                    }
                    i = 99;
                    break;
                } else {
                    i = 8;
                    break;
                }
            default:
                i = 99;
                break;
        }
        as.a().c.a(i, false);
    }

    public synchronized void a(ce ceVar) {
        JNIBottomBackData jNIBottomBackData;
        this.c = ceVar.b();
        this.d = ceVar.a();
        if (this.d != null) {
            try {
                jNIBottomBackData = JniMethod.getJniMethod().packAndEncrypt(this.d.b(), this.d.e());
            } catch (Exception e2) {
                e2.printStackTrace();
                jNIBottomBackData = null;
            }
            if (jNIBottomBackData != null) {
                if (jNIBottomBackData.stateCode == 0) {
                    Log.i("PayPlug", "---the pack data from bottom-=" + new String(jNIBottomBackData.sendData));
                    this.f = new at(this.a, as.a().M);
                    if (this.f != null) {
                        this.f.b();
                        this.f.a(jNIBottomBackData.sendData, 0, jNIBottomBackData.sendData.length, this, 120000);
                    }
                }
            }
            this.c.errorCallBack("打包出错！");
        }
    }

    public synchronized void a(z zVar, UIResponseListener uIResponseListener, Context context, boolean z, boolean z2) {
        this.a = context;
        if (!as.a().c.a(zVar)) {
            j.b(context, context.getString(PluginLink.getStringupomp_lthj_please_get_mac()));
        } else {
            if (z) {
                l.a().a(context, z2, new cw(this));
            }
            if (this.e != null) {
                this.e.clear();
                this.e = null;
            }
            a(new ce(zVar, uIResponseListener));
        }
    }

    public void a(String str, at atVar) {
        if (this.f == atVar) {
            try {
                Message obtain = Message.obtain();
                obtain.obj = str;
                this.g.sendMessage(obtain);
            } catch (Exception e2) {
                Log.d("---NetMediator-errorDistribution--", e2.getMessage());
            }
        }
    }

    public synchronized void a(byte[] bArr, int i, int i2) {
        try {
            Data unpackAndDecrypt = JniMethod.getJniMethod().unpackAndDecrypt(bArr, i2, this.d.e());
            if (unpackAndDecrypt != null && unpackAndDecrypt.stateCode == 0) {
                if (this.d != null) {
                    this.d.a(unpackAndDecrypt);
                    if (!"0".equals(this.d.n())) {
                        Log.i("PayPlug", "theCmd respCode=" + this.d.n() + ",theCmd.respDesc=" + this.d.o());
                    }
                }
                Message obtain = Message.obtain();
                obtain.obj = this.d;
                this.h.sendMessage(obtain);
            } else if (unpackAndDecrypt.respCode == null && unpackAndDecrypt.respDesc == null) {
                a("命令处理失败，错误码为" + unpackAndDecrypt.stateCode, this.f);
            } else {
                a(unpackAndDecrypt.respDesc + "，错误码为" + unpackAndDecrypt.respCode, this.f);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            Log.d("---plugin--NetMediator-response-e=", "--quotMediator--response=" + e2);
        }
        return;
    }

    public synchronized void a(byte[] bArr, at atVar) {
        if (this.f == atVar) {
            a(bArr, 0, bArr.length);
        }
    }
}
