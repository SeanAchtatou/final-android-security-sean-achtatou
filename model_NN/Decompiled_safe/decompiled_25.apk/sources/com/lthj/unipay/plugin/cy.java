package com.lthj.unipay.plugin;

import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentTransaction;
import com.unionpay.upomp.lthj.link.PluginLink;

class cy extends Handler {
    final /* synthetic */ co a;

    cy(co coVar) {
        this.a = coVar;
    }

    private void a(z zVar) {
        if ("0000".equals(zVar.n())) {
            switch (zVar.e()) {
                case FragmentTransaction.TRANSIT_FRAGMENT_CLOSE:
                case 8195:
                case 8196:
                case 8197:
                case 8200:
                case 8201:
                case 8203:
                case 8204:
                case 8206:
                    return;
                case 8198:
                case 8199:
                case 8202:
                case 8205:
                default:
                    l.a().b();
                    return;
            }
        } else {
            switch (zVar.e()) {
                case 8210:
                case 8222:
                case 8225:
                case 8227:
                case 8229:
                    l.a().b();
                    return;
                default:
                    return;
            }
        }
    }

    private void b(z zVar) {
        switch (zVar.e()) {
            case FragmentTransaction.TRANSIT_FRAGMENT_CLOSE:
            case 8221:
            case 8224:
                as.a().b.f(true);
                return;
            default:
                return;
        }
    }

    public void handleMessage(Message message) {
        ce ceVar;
        z zVar = (z) message.obj;
        if (!"3022".equals(this.a.d.n())) {
            a(zVar);
            this.a.a(zVar);
            this.a.b(zVar);
            if (zVar.e() == 8200) {
                as.a().c.a(Integer.parseInt(zVar.f()), true);
            }
            if (!"0000".equals(zVar.n())) {
                b(zVar);
            }
            this.a.c.responseCallBack(zVar);
            if (this.a.e != null && (ceVar = (ce) this.a.e.poll()) != null) {
                this.a.a(ceVar);
            }
        } else if (this.a.a != null) {
            l.a().a(this.a.a, this.a.a.getString(PluginLink.getStringupomp_lthj_session_timeout_tip()), new av(this));
        }
    }
}
