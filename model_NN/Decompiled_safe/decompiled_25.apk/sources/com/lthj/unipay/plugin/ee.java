package com.lthj.unipay.plugin;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import com.unionpay.upomp.lthj.link.PluginLink;
import com.unionpay.upomp.lthj.plugin.model.GetBankService;
import com.unionpay.upomp.lthj.plugin.ui.SupportCardActivity;

public class ee extends BaseAdapter {
    final /* synthetic */ SupportCardActivity a;

    public ee(SupportCardActivity supportCardActivity) {
        this.a = supportCardActivity;
    }

    public int getCount() {
        if (this.a.e == null) {
            return 0;
        }
        return this.a.g > this.a.e.size() ? this.a.e.size() : this.a.g;
    }

    public Object getItem(int i) {
        return null;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = LayoutInflater.from(this.a).inflate(PluginLink.getLayoutupomp_lthj_supportcard_bankitem(), (ViewGroup) null);
        TextView textView = (TextView) inflate.findViewById(PluginLink.getIdupomp_lthj_sup_bank_item());
        CheckBox checkBox = (CheckBox) inflate.findViewById(PluginLink.getIdupomp_lthj_sup_debit_item());
        CheckBox checkBox2 = (CheckBox) inflate.findViewById(PluginLink.getIdupomp_lthj_sup_credit_item());
        checkBox.setClickable(false);
        checkBox2.setClickable(false);
        if (this.a.e != null) {
            GetBankService getBankService = (GetBankService) this.a.e.get(i);
            textView.setText(getBankService.panBank);
            if (getBankService.creditCard.equals("1")) {
                checkBox2.setChecked(true);
            } else {
                checkBox2.setChecked(false);
            }
            if (getBankService.debitCard.equals("1")) {
                checkBox.setChecked(true);
            } else {
                checkBox.setChecked(false);
            }
        }
        return inflate;
    }
}
