package com.lthj.unipay.plugin;

import com.unionpay.upomp.lthj.plugin.model.Data;
import com.unionpay.upomp.lthj.plugin.model.Login;

public class cq extends z {
    private String a;
    private StringBuffer b = new StringBuffer();
    private String c;
    private String d;
    private String e;
    private String f;

    public cq(int i) {
        super(i);
    }

    public String a() {
        return this.a;
    }

    public void a(Data data) {
        Login login = (Login) data;
        c(login);
        this.a = login.loginName;
        this.d = login.mobileNumber;
        this.e = login.email;
        this.b.setLength(0);
        this.b.append(login.password);
        this.f = login.welcome;
    }

    public void a(String str) {
        this.a = str;
    }

    public Data b() {
        Login login = new Login();
        b(login);
        login.loginName = this.a;
        login.password = this.b.toString();
        this.b.delete(0, this.b.length());
        login.validateCode = this.c;
        login.mobileNumber = this.d;
        return login;
    }

    public void b(String str) {
        this.b.setLength(0);
        this.b.append(str);
    }

    public String c() {
        return this.d;
    }

    public void c(String str) {
        this.c = str;
    }

    public String d() {
        return this.e;
    }

    public String p() {
        return this.f;
    }
}
