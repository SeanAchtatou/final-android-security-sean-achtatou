package com.lthj.unipay.plugin;

import android.os.Message;
import com.unionpay.upomp.lthj.plugin.ui.SplashActivity;

public class cc extends Thread {
    final /* synthetic */ SplashActivity a;
    private byte[] b;

    public cc(SplashActivity splashActivity) {
        this.a = splashActivity;
    }

    public void a(byte[] bArr) {
        this.b = bArr;
    }

    public void run() {
        Message obtain = Message.obtain();
        obtain.obj = this.b;
        this.a.f.sendMessage(obtain);
    }
}
