package com.lthj.unipay.plugin;

import android.content.DialogInterface;
import com.unionpay.upomp.lthj.plugin.ui.SplashActivity;
import com.unionpay.upomp.lthj.util.PluginHelper;

public class ad implements DialogInterface.OnClickListener {
    final /* synthetic */ SplashActivity a;

    public ad(SplashActivity splashActivity) {
        this.a = splashActivity;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        as.a().f = false;
        synchronized (PluginHelper.a) {
            PluginHelper.a.notify();
        }
        j.c();
        this.a.finish();
    }
}
