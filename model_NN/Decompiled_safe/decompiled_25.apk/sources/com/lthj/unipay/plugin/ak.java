package com.lthj.unipay.plugin;

import android.content.Context;

public class ak {
    private Context a;

    public ak(Context context) {
        this.a = context;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x001d A[SYNTHETIC, Splitter:B:16:0x001d] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x002a A[SYNTHETIC, Splitter:B:24:0x002a] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0034 A[SYNTHETIC, Splitter:B:30:0x0034] */
    /* JADX WARNING: Removed duplicated region for block: B:39:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:40:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:21:0x0025=Splitter:B:21:0x0025, B:13:0x0018=Splitter:B:13:0x0018} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.String r4, byte[] r5) {
        /*
            r3 = this;
            if (r5 != 0) goto L_0x0003
        L_0x0002:
            return
        L_0x0003:
            r2 = 0
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0016, IOException -> 0x0023, all -> 0x0030 }
            r1.<init>(r4)     // Catch:{ FileNotFoundException -> 0x0016, IOException -> 0x0023, all -> 0x0030 }
            r0 = 0
            int r2 = r5.length     // Catch:{ FileNotFoundException -> 0x003e, IOException -> 0x003c }
            r1.write(r5, r0, r2)     // Catch:{ FileNotFoundException -> 0x003e, IOException -> 0x003c }
            if (r1 == 0) goto L_0x0002
            r1.close()     // Catch:{ Exception -> 0x0014 }
            goto L_0x0002
        L_0x0014:
            r0 = move-exception
            goto L_0x0002
        L_0x0016:
            r0 = move-exception
            r1 = r2
        L_0x0018:
            r0.printStackTrace()     // Catch:{ all -> 0x003a }
            if (r1 == 0) goto L_0x0002
            r1.close()     // Catch:{ Exception -> 0x0021 }
            goto L_0x0002
        L_0x0021:
            r0 = move-exception
            goto L_0x0002
        L_0x0023:
            r0 = move-exception
            r1 = r2
        L_0x0025:
            r0.printStackTrace()     // Catch:{ all -> 0x003a }
            if (r1 == 0) goto L_0x0002
            r1.close()     // Catch:{ Exception -> 0x002e }
            goto L_0x0002
        L_0x002e:
            r0 = move-exception
            goto L_0x0002
        L_0x0030:
            r0 = move-exception
            r1 = r2
        L_0x0032:
            if (r1 == 0) goto L_0x0037
            r1.close()     // Catch:{ Exception -> 0x0038 }
        L_0x0037:
            throw r0
        L_0x0038:
            r1 = move-exception
            goto L_0x0037
        L_0x003a:
            r0 = move-exception
            goto L_0x0032
        L_0x003c:
            r0 = move-exception
            goto L_0x0025
        L_0x003e:
            r0 = move-exception
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lthj.unipay.plugin.ak.a(java.lang.String, byte[]):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x005d A[SYNTHETIC, Splitter:B:25:0x005d] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x006c A[SYNTHETIC, Splitter:B:32:0x006c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte[] a(int r7) {
        /*
            r6 = this;
            r0 = 0
            android.content.Context r1 = r6.a     // Catch:{ NotFoundException -> 0x0020, Exception -> 0x0056, all -> 0x0066 }
            android.content.res.Resources r1 = r1.getResources()     // Catch:{ NotFoundException -> 0x0020, Exception -> 0x0056, all -> 0x0066 }
            java.io.InputStream r1 = r1.openRawResource(r7)     // Catch:{ NotFoundException -> 0x0020, Exception -> 0x0056, all -> 0x0066 }
            int r2 = r1.available()     // Catch:{ NotFoundException -> 0x007f, Exception -> 0x007a }
            byte[] r0 = new byte[r2]     // Catch:{ NotFoundException -> 0x007f, Exception -> 0x007a }
            r3 = 0
            r1.read(r0, r3, r2)     // Catch:{ NotFoundException -> 0x007f, Exception -> 0x007a }
            if (r1 == 0) goto L_0x001a
            r1.close()     // Catch:{ IOException -> 0x001b }
        L_0x001a:
            return r0
        L_0x001b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x001a
        L_0x0020:
            r1 = move-exception
            r1 = r0
        L_0x0022:
            android.app.AlertDialog$Builder r2 = new android.app.AlertDialog$Builder     // Catch:{ all -> 0x0075 }
            android.content.Context r3 = r6.a     // Catch:{ all -> 0x0075 }
            r2.<init>(r3)     // Catch:{ all -> 0x0075 }
            java.lang.String r3 = "提示"
            android.app.AlertDialog$Builder r2 = r2.setTitle(r3)     // Catch:{ all -> 0x0075 }
            android.content.Context r3 = r6.a     // Catch:{ all -> 0x0075 }
            int r4 = com.unionpay.upomp.lthj.link.PluginLink.getStringupomp_lthj_have_no_initfile()     // Catch:{ all -> 0x0075 }
            java.lang.String r3 = r3.getString(r4)     // Catch:{ all -> 0x0075 }
            android.app.AlertDialog$Builder r2 = r2.setMessage(r3)     // Catch:{ all -> 0x0075 }
            java.lang.String r3 = "确定"
            com.lthj.unipay.plugin.an r4 = new com.lthj.unipay.plugin.an     // Catch:{ all -> 0x0075 }
            r4.<init>(r6)     // Catch:{ all -> 0x0075 }
            android.app.AlertDialog$Builder r2 = r2.setPositiveButton(r3, r4)     // Catch:{ all -> 0x0075 }
            r2.show()     // Catch:{ all -> 0x0075 }
            if (r1 == 0) goto L_0x001a
            r1.close()     // Catch:{ IOException -> 0x0051 }
            goto L_0x001a
        L_0x0051:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x001a
        L_0x0056:
            r1 = move-exception
            r2 = r0
        L_0x0058:
            r1.printStackTrace()     // Catch:{ all -> 0x0077 }
            if (r2 == 0) goto L_0x001a
            r2.close()     // Catch:{ IOException -> 0x0061 }
            goto L_0x001a
        L_0x0061:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x001a
        L_0x0066:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x006a:
            if (r1 == 0) goto L_0x006f
            r1.close()     // Catch:{ IOException -> 0x0070 }
        L_0x006f:
            throw r0
        L_0x0070:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x006f
        L_0x0075:
            r0 = move-exception
            goto L_0x006a
        L_0x0077:
            r0 = move-exception
            r1 = r2
            goto L_0x006a
        L_0x007a:
            r2 = move-exception
            r5 = r2
            r2 = r1
            r1 = r5
            goto L_0x0058
        L_0x007f:
            r2 = move-exception
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lthj.unipay.plugin.ak.a(int):byte[]");
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0020 A[SYNTHETIC, Splitter:B:17:0x0020] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0030 A[SYNTHETIC, Splitter:B:25:0x0030] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x003a A[SYNTHETIC, Splitter:B:31:0x003a] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:14:0x001b=Splitter:B:14:0x001b, B:22:0x002b=Splitter:B:22:0x002b} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte[] a(java.lang.String r6) {
        /*
            r5 = this;
            r1 = 0
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x0016, IOException -> 0x0026, all -> 0x0036 }
            r2.<init>(r6)     // Catch:{ FileNotFoundException -> 0x0016, IOException -> 0x0026, all -> 0x0036 }
            int r3 = r2.available()     // Catch:{ FileNotFoundException -> 0x004b, IOException -> 0x0044 }
            byte[] r0 = new byte[r3]     // Catch:{ FileNotFoundException -> 0x004b, IOException -> 0x0044 }
            r1 = 0
            r2.read(r0, r1, r3)     // Catch:{ FileNotFoundException -> 0x0050, IOException -> 0x0049 }
            if (r2 == 0) goto L_0x0015
            r2.close()     // Catch:{ Exception -> 0x003e }
        L_0x0015:
            return r0
        L_0x0016:
            r0 = move-exception
            r2 = r1
            r4 = r0
            r0 = r1
            r1 = r4
        L_0x001b:
            r1.printStackTrace()     // Catch:{ all -> 0x0042 }
            if (r2 == 0) goto L_0x0015
            r2.close()     // Catch:{ Exception -> 0x0024 }
            goto L_0x0015
        L_0x0024:
            r1 = move-exception
            goto L_0x0015
        L_0x0026:
            r0 = move-exception
            r2 = r1
            r4 = r0
            r0 = r1
            r1 = r4
        L_0x002b:
            r1.printStackTrace()     // Catch:{ all -> 0x0042 }
            if (r2 == 0) goto L_0x0015
            r2.close()     // Catch:{ Exception -> 0x0034 }
            goto L_0x0015
        L_0x0034:
            r1 = move-exception
            goto L_0x0015
        L_0x0036:
            r0 = move-exception
            r2 = r1
        L_0x0038:
            if (r2 == 0) goto L_0x003d
            r2.close()     // Catch:{ Exception -> 0x0040 }
        L_0x003d:
            throw r0
        L_0x003e:
            r1 = move-exception
            goto L_0x0015
        L_0x0040:
            r1 = move-exception
            goto L_0x003d
        L_0x0042:
            r0 = move-exception
            goto L_0x0038
        L_0x0044:
            r0 = move-exception
            r4 = r0
            r0 = r1
            r1 = r4
            goto L_0x002b
        L_0x0049:
            r1 = move-exception
            goto L_0x002b
        L_0x004b:
            r0 = move-exception
            r4 = r0
            r0 = r1
            r1 = r4
            goto L_0x001b
        L_0x0050:
            r1 = move-exception
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lthj.unipay.plugin.ak.a(java.lang.String):byte[]");
    }
}
