package com.lthj.unipay.plugin;

import com.amap.mapapi.poisearch.PoiTypeDef;
import com.unionpay.upomp.lthj.plugin.model.Data;
import com.unionpay.upomp.lthj.plugin.model.Init;

public class er extends z {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private boolean f;

    public er(int i) {
        super(i);
    }

    public String a() {
        return this.d;
    }

    public void a(Data data) {
        Init init = (Init) data;
        c(init);
        if (init.configVersion != null && this.a != null) {
            if (init.configFile == null || PoiTypeDef.All.equals(init.configFile)) {
                this.f = true;
                this.b = init.sessionID;
                i(init.pluginSerialNo);
                at.a = this.b;
                return;
            }
            this.f = false;
            this.d = init.configFile;
            this.e = init.configFileHash;
        }
    }

    public void a(String str) {
        this.c = str;
    }

    public Data b() {
        Init init = new Init();
        b(init);
        init.merchantId = this.c;
        init.configVersion = this.a;
        return init;
    }

    public void b(String str) {
        this.a = str;
    }

    public String c() {
        return this.e;
    }

    public boolean d() {
        return this.f;
    }
}
