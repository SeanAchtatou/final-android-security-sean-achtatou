package com.lthj.unipay.plugin;

import com.unionpay.upomp.lthj.plugin.model.Data;
import com.unionpay.upomp.lthj.plugin.model.QuickBankBind;

public class cd extends z {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;

    public cd(int i) {
        super(i);
    }

    public String a() {
        return this.a;
    }

    public void a(Data data) {
        c(data);
    }

    public void a(String str) {
        this.a = str;
    }

    public Data b() {
        QuickBankBind quickBankBind = new QuickBankBind();
        b(quickBankBind);
        quickBankBind.loginName = this.a;
        quickBankBind.cvn2 = this.h;
        quickBankBind.merchantId = this.b;
        quickBankBind.merchantOrderId = this.c;
        quickBankBind.merchantOrderTime = this.d;
        quickBankBind.pan = this.e;
        quickBankBind.panDate = this.g;
        quickBankBind.pin = this.f;
        return quickBankBind;
    }

    public void b(String str) {
        this.b = str;
    }

    public void c(String str) {
        this.c = str;
    }

    public void d(String str) {
        this.d = str;
    }

    public void e(String str) {
        this.e = str;
    }

    public void f(String str) {
        this.f = str;
    }

    public void g(String str) {
        this.g = str;
    }

    public void h(String str) {
        this.h = str;
    }
}
