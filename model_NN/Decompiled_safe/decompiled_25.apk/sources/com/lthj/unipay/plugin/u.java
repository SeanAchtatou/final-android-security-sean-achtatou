package com.lthj.unipay.plugin;

import android.support.v4.view.MotionEventCompat;

public class u {
    private boolean a;
    private boolean b;
    private boolean c;
    private boolean d;
    private boolean e;
    private boolean f;
    private boolean g;
    private boolean h;
    private boolean i;

    public void a(int i2, boolean z) {
        switch (i2) {
            case 1:
                this.a = z;
                return;
            case 2:
                this.b = z;
                return;
            case 3:
                this.c = z;
                return;
            case 4:
                this.d = z;
                return;
            case 5:
                this.e = z;
                return;
            case 6:
                this.f = z;
                return;
            case 7:
                this.g = z;
                return;
            case 8:
                this.h = z;
                return;
            case MotionEventCompat.ACTION_HOVER_ENTER:
                this.i = z;
                return;
            default:
                return;
        }
    }

    public boolean a(z zVar) {
        switch (zVar.e()) {
            case 8193:
                return this.a;
            case 8195:
                return this.f;
            case 8196:
                return this.g;
            case 8197:
                return this.b;
            case 8203:
                return this.e;
            case 8210:
                if ("1".equals(((ea) zVar).a())) {
                    return this.c;
                }
                if ("2".equals(((ea) zVar).a())) {
                    return this.d;
                }
                break;
            case 8227:
                if ("1".equals(((ek) zVar).a())) {
                    return this.h;
                }
                if ("2".equals(((ek) zVar).a())) {
                    return this.i;
                }
                break;
        }
        return true;
    }
}
