package com.lthj.unipay.plugin;

import android.os.Message;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.unionpay.upomp.lthj.plugin.ui.AccountActivity;
import java.util.TimerTask;

public class ca extends TimerTask {
    final /* synthetic */ AccountActivity a;

    public ca(AccountActivity accountActivity) {
        this.a = accountActivity;
    }

    public void run() {
        Message obtain = Message.obtain();
        obtain.obj = this.a.a + PoiTypeDef.All;
        this.a.n.sendMessage(obtain);
        if (this.a.a < 0) {
            this.a.aaTimerTask.cancel();
            this.a.aaTimerTask = null;
            this.a.a = 60;
            return;
        }
        AccountActivity accountActivity = this.a;
        accountActivity.a--;
    }
}
