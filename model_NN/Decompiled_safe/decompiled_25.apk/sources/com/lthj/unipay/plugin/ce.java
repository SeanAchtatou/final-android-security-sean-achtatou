package com.lthj.unipay.plugin;

import com.unionpay.upomp.lthj.plugin.ui.UIResponseListener;

public class ce {
    private z a;
    private UIResponseListener b;

    public ce(z zVar, UIResponseListener uIResponseListener) {
        this.a = zVar;
        this.b = uIResponseListener;
    }

    public z a() {
        return this.a;
    }

    public UIResponseListener b() {
        return this.b;
    }
}
