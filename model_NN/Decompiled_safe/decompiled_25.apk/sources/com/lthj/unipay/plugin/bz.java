package com.lthj.unipay.plugin;

import com.unionpay.upomp.lthj.plugin.model.Data;
import com.unionpay.upomp.lthj.plugin.model.GetBundleBankCardList;

public class bz extends z {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private StringBuffer f = new StringBuffer();
    private StringBuffer g = new StringBuffer();
    private String h;

    public bz(int i) {
        super(i);
    }

    public String a() {
        return this.b;
    }

    public void a(Data data) {
        GetBundleBankCardList getBundleBankCardList = (GetBundleBankCardList) data;
        c(getBundleBankCardList);
        this.b = getBundleBankCardList.bindId;
        this.c = getBundleBankCardList.panType;
        this.e = getBundleBankCardList.panBank;
        this.d = getBundleBankCardList.panBankId;
        this.f.delete(0, this.f.length());
        this.f.append(getBundleBankCardList.pan);
        this.g.delete(0, this.g.length());
        this.g.append(getBundleBankCardList.mobileNumber);
        this.h = getBundleBankCardList.isDefault;
    }

    public void a(String str) {
        this.a = str;
    }

    public Data b() {
        GetBundleBankCardList getBundleBankCardList = new GetBundleBankCardList();
        b(getBundleBankCardList);
        getBundleBankCardList.loginName = this.a;
        return getBundleBankCardList;
    }

    public String c() {
        return this.c;
    }

    public String d() {
        return this.d;
    }

    public String p() {
        return this.e;
    }

    public String q() {
        return this.f.toString();
    }

    public String r() {
        return this.g.toString();
    }

    public String s() {
        return this.h;
    }
}
