package com.lthj.unipay.plugin;

import android.content.Context;
import android.util.Xml;
import com.umeng.common.util.e;
import com.unionpay.upomp.lthj.link.PluginLink;
import com.unionpay.upomp.lthj.plugin.model.GetBankService;
import com.unionpay.upomp.lthj.plugin.ui.JniMethod;
import java.io.ByteArrayInputStream;
import org.xmlpull.v1.XmlPullParser;

public class ct {
    public static GetBankService a(Context context, String str) {
        byte[] a = new ak(context).a("2".equals(str) ? PluginLink.getRawupomp_lthj_quicksupport() : PluginLink.getRawupomp_lthj_authsupport());
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(JniMethod.getJniMethod().decryptConfig(a, a.length));
        XmlPullParser newPullParser = Xml.newPullParser();
        newPullParser.setInput(byteArrayInputStream, e.f);
        StringBuffer stringBuffer = new StringBuffer();
        StringBuffer stringBuffer2 = new StringBuffer();
        StringBuffer stringBuffer3 = new StringBuffer();
        StringBuffer stringBuffer4 = new StringBuffer();
        GetBankService getBankService = new GetBankService();
        for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
            switch (eventType) {
                case 2:
                    String name = newPullParser.getName();
                    if (!"panItem".equalsIgnoreCase(name)) {
                        if (!"panBank".equalsIgnoreCase(name)) {
                            if (!"panBankId".equalsIgnoreCase(name)) {
                                if (!"creditCard".equalsIgnoreCase(name)) {
                                    if (!"debitCard".equalsIgnoreCase(name)) {
                                        break;
                                    } else {
                                        stringBuffer4.append("|");
                                        stringBuffer4.append(newPullParser.nextText());
                                        break;
                                    }
                                } else {
                                    stringBuffer3.append("|");
                                    stringBuffer3.append(newPullParser.nextText());
                                    break;
                                }
                            } else {
                                stringBuffer2.append("|");
                                stringBuffer2.append(newPullParser.nextText());
                                break;
                            }
                        } else {
                            stringBuffer.append("|");
                            stringBuffer.append(newPullParser.nextText());
                            break;
                        }
                    } else {
                        break;
                    }
            }
        }
        stringBuffer.deleteCharAt(0);
        stringBuffer2.deleteCharAt(0);
        stringBuffer3.deleteCharAt(0);
        stringBuffer4.deleteCharAt(0);
        getBankService.panBank = stringBuffer.toString();
        getBankService.panBankId = stringBuffer.toString();
        getBankService.creditCard = stringBuffer3.toString();
        getBankService.debitCard = stringBuffer4.toString();
        getBankService.respCode = "0000";
        return getBankService;
    }
}
