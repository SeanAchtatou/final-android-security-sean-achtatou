package com.lthj.unipay.plugin;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.unionpay.upomp.lthj.link.PluginLink;
import com.unionpay.upomp.lthj.plugin.model.GetBundleBankCardList;
import com.unionpay.upomp.lthj.plugin.ui.BankCardInfoActivity;
import com.unionpay.upomp.lthj.widget.LineFrameView;

public class cf extends BaseExpandableListAdapter {
    final /* synthetic */ BankCardInfoActivity a;

    public cf(BankCardInfoActivity bankCardInfoActivity) {
        this.a = bankCardInfoActivity;
    }

    /* renamed from: a */
    public GetBundleBankCardList getGroup(int i) {
        return (GetBundleBankCardList) as.a().D.get(i);
    }

    public Object getChild(int i, int i2) {
        return null;
    }

    public long getChildId(int i, int i2) {
        return (long) i2;
    }

    public View getChildView(int i, int i2, boolean z, View view, ViewGroup viewGroup) {
        View inflate = LayoutInflater.from(this.a).inflate(PluginLink.getLayoutupomp_lthj_bankcard_itemexpad(), (ViewGroup) null);
        TextView textView = (TextView) inflate.findViewById(PluginLink.getIdupomp_lthj_bankcard_listview_childtv());
        ImageView imageView = (ImageView) inflate.findViewById(PluginLink.getIdupomp_lthj_bankcard_listview_childicon());
        switch (i2) {
            case 0:
                textView.setText(PluginLink.getStringupomp_lthj_use_thecard());
                imageView.setImageResource(PluginLink.getDrawableupomp_lthj_pay_icon());
                break;
            case 1:
                if (!"1".equals(((GetBundleBankCardList) as.a().D.get(i)).isDefault)) {
                    textView.setText(PluginLink.getStringupomp_lthj_set_default());
                    imageView.setImageResource(PluginLink.getDrawableupomp_lthj_set_default_icon());
                    break;
                } else {
                    textView.setText(PluginLink.getStringupomp_lthj_unbind());
                    imageView.setImageResource(PluginLink.getDrawableupomp_lthj_unbind_icon());
                    break;
                }
            case 2:
                textView.setText(PluginLink.getStringupomp_lthj_unbind());
                imageView.setImageResource(PluginLink.getDrawableupomp_lthj_unbind_icon());
                break;
        }
        return inflate;
    }

    public int getChildrenCount(int i) {
        return ((GetBundleBankCardList) as.a().D.get(i)).isDefault.equals("1") ? 2 : 3;
    }

    public int getGroupCount() {
        return as.a().D.size();
    }

    public long getGroupId(int i) {
        return (long) i;
    }

    public View getGroupView(int i, boolean z, View view, ViewGroup viewGroup) {
        View inflate = LayoutInflater.from(this.a).inflate(PluginLink.getLayoutupomp_lthj_bankcard_item(), (ViewGroup) null);
        GetBundleBankCardList getBundleBankCardList = (GetBundleBankCardList) as.a().D.get(i);
        ((LineFrameView) inflate.findViewById(PluginLink.getIdupomp_lthj_bankcard_info_view())).a(getBundleBankCardList.panBank + j.b(getBundleBankCardList.panType) + ((Object) getBundleBankCardList.pan.subSequence(getBundleBankCardList.pan.length() - 4, getBundleBankCardList.pan.length())));
        ImageView imageView = (ImageView) inflate.findViewById(PluginLink.getIdupomp_lthj_card_menu_drop());
        if ("1".equals(getBundleBankCardList.isDefault)) {
            inflate.findViewById(PluginLink.getIdupomp_lthj_default_card()).setVisibility(0);
            imageView.setBackgroundResource(PluginLink.getDrawableupomp_lthj_default_drop());
        } else {
            inflate.findViewById(PluginLink.getIdupomp_lthj_default_card()).setVisibility(4);
            imageView.setBackgroundResource(PluginLink.getDrawableupomp_lthj_common_drop());
        }
        return inflate;
    }

    public boolean hasStableIds() {
        return false;
    }

    public boolean isChildSelectable(int i, int i2) {
        return true;
    }

    public void onGroupCollapsed(int i) {
        super.onGroupCollapsed(i);
    }

    public void onGroupExpanded(int i) {
        super.onGroupExpanded(i);
    }
}
