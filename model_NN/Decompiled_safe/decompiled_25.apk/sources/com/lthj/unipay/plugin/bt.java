package com.lthj.unipay.plugin;

import android.content.Intent;
import android.view.View;

public interface bt {
    void aboutEnable(boolean z);

    void backToMerchant();

    void changeSubActivity(Intent intent);

    void changeTitleButton(String str, View.OnClickListener onClickListener);

    void quitNotice();
}
