package com.lthj.unipay.plugin;

import java.io.OutputStream;

public class bs implements cv {
    protected final byte[] a = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102};
    protected final byte[] b = new byte[128];

    public bs() {
        a();
    }

    private boolean a(char c) {
        return c == 10 || c == 13 || c == 9 || c == ' ';
    }

    public int a(String str, OutputStream outputStream) {
        int i = 0;
        int length = str.length();
        while (length > 0 && a(str.charAt(length - 1))) {
            length--;
        }
        int i2 = 0;
        while (i < length) {
            int i3 = i;
            while (i3 < length && a(str.charAt(i3))) {
                i3++;
            }
            int i4 = i3 + 1;
            byte b2 = this.b[str.charAt(i3)];
            while (i4 < length && a(str.charAt(i4))) {
                i4++;
            }
            outputStream.write(this.b[str.charAt(i4)] | (b2 << 4));
            i2++;
            i = i4 + 1;
        }
        return i2;
    }

    /* access modifiers changed from: protected */
    public void a() {
        for (int i = 0; i < this.a.length; i++) {
            this.b[this.a[i]] = (byte) i;
        }
        this.b[65] = this.b[97];
        this.b[66] = this.b[98];
        this.b[67] = this.b[99];
        this.b[68] = this.b[100];
        this.b[69] = this.b[101];
        this.b[70] = this.b[102];
    }
}
