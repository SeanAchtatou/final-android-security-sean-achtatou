package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.entity.DoctorEntity;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.ClientLogUtil;
import com.womenchild.hospital.util.WebUtil;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FavDoctorActivity extends BaseRequestActivity implements View.OnClickListener {
    private static final String TAG = "FavDoctorActivity";
    private TextView CheckTv;
    private TextView departmentTv;
    private JSONObject doctor;
    private String doctorID;
    private String doctorLevel;
    private ArrayList<DoctorEntity> doctorList = new ArrayList<>();
    private TextView doctorName;
    private boolean favFalg = false;
    private String favID;
    private WebView introWebView;
    private Button iv_back;
    private TextView levelTv;
    private String name;
    private ProgressDialog pDialog;
    private TextView tv_submit;
    private String userID;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.fav_doctor_intro);
        initViewId();
        initClickListener();
        Intent intent = getIntent();
        if (intent != null) {
            this.doctorID = intent.getStringExtra("doctorID");
            if (intent.getStringExtra("favID") != null) {
                this.favID = intent.getStringExtra("favID");
                this.favFalg = true;
                if (!this.favFalg) {
                    this.tv_submit.setVisibility(8);
                } else {
                    this.tv_submit.setText(getResources().getString(R.string.remove_collect));
                    this.tv_submit.setPadding(2, 0, 0, 0);
                }
            }
            if (!intent.getBooleanExtra("planFlag", true)) {
                this.CheckTv.setVisibility(8);
            }
            if (intent.getStringExtra("userID") != null) {
                this.userID = intent.getStringExtra("userID");
            }
            this.pDialog = new ProgressDialog(this);
            this.pDialog.setMessage(getResources().getString(R.string.loading_ok));
            this.pDialog.show();
            sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.DOCTOR_DETAIL), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.DOCTOR_DETAIL)));
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void refreshActivity(Object... params) {
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject result = (JSONObject) params[2];
            int resultCode = result.optJSONObject("res").optInt("st");
            String msg = result.optJSONObject("res").optString("msg");
            if (resultCode == 0) {
                switch (requestType) {
                    case HttpRequestParameters.DOCTOR_DETAIL /*201*/:
                        this.doctor = result.optJSONObject("inf").optJSONObject("doctor");
                        loadData(requestType, result);
                        if (HomeActivity.loginFlag && (this.favID == null || PoiTypeDef.All.equals(this.favID))) {
                            sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.FAVORITES_DOCTOR_LIST), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.FAVORITES_DOCTOR_LIST)));
                            break;
                        }
                    case HttpRequestParameters.FAVORITES_DOCTOR_LIST /*507*/:
                        this.favFalg = false;
                        JSONArray doctors = result.optJSONArray("inf");
                        int i = 0;
                        while (true) {
                            if (i < doctors.length()) {
                                JSONObject jsonObject = doctors.optJSONObject(i);
                                if (jsonObject == null || !jsonObject.optString("doctorid").equals(this.doctorID)) {
                                    i++;
                                } else {
                                    this.favID = jsonObject.optString("favdoctorid");
                                    this.favFalg = true;
                                }
                            }
                        }
                        if (!this.favFalg) {
                            this.tv_submit.setText(getResources().getString(R.string.add_collect));
                            break;
                        } else {
                            this.tv_submit.setText(getResources().getString(R.string.remove_collect));
                            break;
                        }
                        break;
                    case HttpRequestParameters.ADD_FAVORITES_DOCTOR /*509*/:
                        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.FAVORITES_DOCTOR_LIST), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.FAVORITES_DOCTOR_LIST)));
                        this.tv_submit.setText(getResources().getString(R.string.remove_collect));
                        Toast.makeText(this, msg, 0).show();
                        this.favFalg = true;
                        break;
                    case HttpRequestParameters.DELETE_FAVORITES_DOCTOR /*510*/:
                        Toast.makeText(this, getResources().getString(R.string.ok_remove_collect), 0).show();
                        this.tv_submit.setText(getResources().getString(R.string.add_collect));
                        this.favFalg = false;
                        break;
                }
            }
        } else {
            Toast.makeText(this, (int) R.string.network_connect_failed_prompt, 0).show();
        }
        this.pDialog.dismiss();
    }

    private void createDoctor() {
        String gender;
        JSONObject temp = new JSONObject();
        try {
            temp.put("doctorid", this.doctorID);
            temp.put("doctorName", this.name);
            temp.put("hospital", this.doctor.optString("hospitalname"));
            temp.put("level", this.doctorLevel);
            if (getResources().getString(R.string.man).equals(this.doctor.optString("sex"))) {
                gender = "1";
            } else if (getResources().getString(R.string.woman).equals(this.doctor.optString("sex"))) {
                gender = "0";
            } else {
                gender = "2";
            }
            temp.put("gender", gender);
            this.doctor = temp;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void initViewId() {
        this.iv_back = (Button) findViewById(R.id.iv_back);
        this.tv_submit = (TextView) findViewById(R.id.tv_submit);
        this.doctorName = (TextView) findViewById(R.id.tv_doc_name);
        this.departmentTv = (TextView) findViewById(R.id.tv_department);
        this.levelTv = (TextView) findViewById(R.id.tv_level);
        this.CheckTv = (TextView) findViewById(R.id.tv_check);
        this.introWebView = (WebView) findViewById(R.id.wb_intro);
    }

    public void initClickListener() {
        this.iv_back.setOnClickListener(this);
        this.tv_submit.setOnClickListener(this);
        this.CheckTv.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_submit /*2131296445*/:
                if (!HomeActivity.loginFlag) {
                    Toast.makeText(this, getString(R.string.login_function), 0).show();
                    return;
                } else if (this.favFalg) {
                    sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.DELETE_FAVORITES_DOCTOR), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.DELETE_FAVORITES_DOCTOR)));
                    return;
                } else {
                    sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.ADD_FAVORITES_DOCTOR), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.ADD_FAVORITES_DOCTOR)));
                    return;
                }
            case R.id.iv_back /*2131296529*/:
                setResult(1);
                finish();
                return;
            case R.id.tv_check /*2131296746*/:
                Intent intent = new Intent(this, DoctorPlanActivity.class);
                intent.putExtra("doctorid", Integer.parseInt(this.doctorID));
                intent.putExtra("doctorName", this.doctorName.getText().toString());
                startActivity(intent);
                return;
            default:
                return;
        }
    }

    public void loadData(int requestType, Object data) {
        JSONObject doctor2;
        JSONObject json = (JSONObject) data;
        JSONObject res = json.optJSONObject("res");
        if (res != null) {
            try {
                if (res.getInt("st") == 0 && (doctor2 = json.optJSONObject("inf").optJSONObject("doctor")) != null) {
                    this.name = doctor2.optString("doctorname");
                    this.doctorLevel = doctor2.optString("clinicalgrade");
                    this.doctorName.setText(this.name);
                    this.departmentTv.setText(this.doctorLevel);
                    this.introWebView.loadDataWithBaseURL(null, WebUtil.sourceCreateHtml(doctor2.optString("desc")), "text/html", "utf-8", null);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                ClientLogUtil.i(TAG, e.getMessage());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.DOCTOR_DETAIL /*201*/:
                parameters.add("doctorid", this.doctorID);
                break;
            case HttpRequestParameters.FAVORITES_DOCTOR_LIST /*507*/:
                parameters.add("userid", UserEntity.getInstance().getInfo().getAccId());
                break;
            case HttpRequestParameters.ADD_FAVORITES_DOCTOR /*509*/:
                parameters.add("userid", UserEntity.getInstance().getInfo().getAccId());
                createDoctor();
                JSONArray doctors = new JSONArray();
                doctors.put(this.doctor);
                parameters.add("doctors", doctors.toString());
                break;
            case HttpRequestParameters.DELETE_FAVORITES_DOCTOR /*510*/:
                parameters.add("favdoctorid", this.favID);
                break;
        }
        return parameters;
    }
}
