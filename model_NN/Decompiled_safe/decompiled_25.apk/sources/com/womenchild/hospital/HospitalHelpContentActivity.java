package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.ClientLogUtil;
import com.womenchild.hospital.util.WebUtil;
import com.womenchild.hospital.view.SSLWebViewClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HospitalHelpContentActivity extends BaseRequestActivity implements View.OnClickListener {
    private static final String TAG = "HospitalHelpContentActivity";
    private String id;
    private Button iv_return_home;
    private ProgressDialog pDialog;
    private WebView wvhpcontent;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.hospital_help_content);
        initViewId();
        initClickListener();
        initData();
        this.iv_return_home.setBackgroundResource(R.drawable.back_selector);
        this.iv_return_home.setText("返回");
        this.pDialog = new ProgressDialog(this);
        this.pDialog.setMessage(getResources().getString(R.string.loading_ok));
        this.pDialog.show();
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.HOSPITAL_HELP_CONTTENT), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.HOSPITAL_HELP_CONTTENT)));
    }

    public void refreshActivity(Object... params) {
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            loadData(requestType, (JSONObject) params[2]);
        } else {
            Toast.makeText(this, getResources().getString(R.string.network_connect_failed_prompt), 0).show();
        }
    }

    public void initViewId() {
        this.wvhpcontent = (WebView) findViewById(R.id.wv_hp_content);
        this.iv_return_home = (Button) findViewById(R.id.iv_return_home);
        this.wvhpcontent.setWebViewClient(new SSLWebViewClient());
    }

    public void initClickListener() {
        this.iv_return_home.setOnClickListener(this);
    }

    public void loadData(int requestType, Object data) {
        JSONArray inf;
        this.pDialog.dismiss();
        JSONObject json = (JSONObject) data;
        JSONObject res = json.optJSONObject("res");
        if (res != null) {
            try {
                if (res.getInt("st") == 0 && (inf = json.optJSONArray("inf")) != null) {
                    this.wvhpcontent.loadDataWithBaseURL(null, WebUtil.sourceCreateHtml(inf.optJSONObject(0).optString("content")), "text/html", "utf-8", null);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                ClientLogUtil.i(TAG, e.getMessage());
            }
        }
    }

    public void onClick(View v) {
        if (this.iv_return_home == v) {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.id = getIntent().getExtras().getString("id");
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        parameters.add("id", this.id);
        return parameters;
    }
}
