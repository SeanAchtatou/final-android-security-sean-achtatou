package com.womenchild.hospital;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.umeng.common.a;
import com.womenchild.hospital.adapter.SelectClinicTimeAdapter;
import com.womenchild.hospital.adapter.SelectTimeExpandableAdapter;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.ClientLogUtil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SelectClinicTimeActivity extends BaseRequestActivity implements View.OnClickListener {
    private static SelectClinicTimeAdapter adapter;
    public static int ballNum;
    public static JSONObject json;
    private String TAG = "SelectClinicTimeActivity";
    private Button btnDoctorInfo;
    private Button btnNext;
    private String day;
    private int doctorid;
    private ExpandableListView elvContext;
    private Button ibtnReturn;
    /* access modifiers changed from: private */
    public Intent intent;
    private JSONObject jsonObjects;
    private ListView lvClinicTime;
    private ProgressDialog pDialog;
    private TextView tvClinicDate;
    private TextView tvSelectDoctor;
    private String type;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.select_clinic_time);
        initViewId();
        initClickListener();
        initData();
    }

    /* access modifiers changed from: protected */
    public void initData() {
        Bundle bundle = getIntent().getExtras();
        this.type = bundle.getString(a.b);
        try {
            json = new JSONObject(getIntent().getStringExtra("jsons"));
            Log.i("wjp", "返回 :" + json);
            this.jsonObjects = new JSONObject(bundle.getString("jsons"));
            this.day = json.optString("day");
            this.doctorid = json.optJSONArray("planlist").optJSONObject(0).optInt("doctorid");
            this.tvClinicDate.setText(this.day);
            this.tvSelectDoctor.setText(json.optJSONArray("planlist").optJSONObject(0).optString("doctorname"));
            adapter = new SelectClinicTimeAdapter(this, clearStopPlan(json.getJSONArray("planlist")));
            this.lvClinicTime.setAdapter((ListAdapter) adapter);
            this.pDialog = new ProgressDialog(this);
            this.pDialog.setMessage("正在加载号球数据...");
            this.pDialog.show();
            sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.READ_NUM_BALL), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.READ_NUM_BALL)));
        } catch (JSONException e) {
            e.printStackTrace();
            ClientLogUtil.e(this.TAG, e.getMessage());
        }
    }

    private JSONArray clearStopPlan(JSONArray jsonArray) {
        JSONArray jsons = new JSONArray();
        for (int i = 0; i < jsonArray.length(); i++) {
            if (!jsonArray.optJSONObject(i).optBoolean("isstop")) {
                jsons.put(jsonArray.optJSONObject(i));
            }
        }
        return jsons;
    }

    public void initViewId() {
        this.ibtnReturn = (Button) findViewById(R.id.ibtn_return);
        this.btnDoctorInfo = (Button) findViewById(R.id.btn_doctor_info);
        this.btnNext = (Button) findViewById(R.id.btn_next);
        this.lvClinicTime = (ListView) findViewById(R.id.lv_select_clinic_time);
        this.tvClinicDate = (TextView) findViewById(R.id.tv_clinic_date);
        this.tvSelectDoctor = (TextView) findViewById(R.id.tv_select_doctor);
        this.elvContext = (ExpandableListView) findViewById(R.id.elv_context);
    }

    public void initClickListener() {
        this.ibtnReturn.setOnClickListener(this);
        this.btnDoctorInfo.setOnClickListener(this);
        this.btnNext.setOnClickListener(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_return:
                finish();
                return;
            case R.id.btn_next:
                int selectedId = SelectClinicTimeAdapter.getSelectId();
                if (-1 == selectedId) {
                    Toast.makeText(this, getResources().getString(R.string.select_time), 0).show();
                    return;
                } else if (HomeActivity.loginFlag) {
                    JSONObject jsonObject = json.optJSONArray("planlist").optJSONObject(selectedId);
                    this.intent = new Intent(this, SelectPatientActivity.class);
                    this.intent.putExtra("plan", jsonObject.toString());
                    startActivity(this.intent);
                    return;
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("登录提示");
                    builder.setMessage("使用此功能前请您先登录！若无帐号，请先注册");
                    builder.setPositiveButton("免费注册", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            SelectClinicTimeActivity.this.intent = new Intent(SelectClinicTimeActivity.this, RegisterActivity.class);
                            SelectClinicTimeActivity.this.startActivity(SelectClinicTimeActivity.this.intent);
                        }
                    });
                    builder.setNegativeButton("登录", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            SelectClinicTimeActivity.this.intent = new Intent(SelectClinicTimeActivity.this, LoginActivity.class);
                            SelectClinicTimeActivity.this.startActivity(SelectClinicTimeActivity.this.intent);
                        }
                    });
                    builder.create().show();
                    return;
                }
            case R.id.btn_doctor_info:
                this.intent = new Intent(this, FavDoctorActivity.class);
                this.intent.putExtra("doctorID", json.optJSONArray("planlist").optJSONObject(0).optString("doctorid"));
                this.intent.putExtra("favFlag", true);
                this.intent.putExtra("planFlag", false);
                startActivity(this.intent);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        SelectClinicTimeAdapter.setSelectId(-1);
    }

    public static void refreshAdapter() {
        adapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        parameters.add("doctorid", Integer.valueOf(this.doctorid));
        parameters.add("day", this.day);
        parameters.add("timespan", this.type);
        return parameters;
    }

    /* access modifiers changed from: protected */
    public void loadData(int requestType, Object data) {
        JSONArray balls = (JSONArray) data;
        JSONArray plans = clearData(this.jsonObjects.optJSONArray("planlist"), this.type);
        JSONArray tmpJons = new JSONArray();
        for (int i = 0; i < plans.length(); i++) {
            JSONObject json2 = plans.optJSONObject(i);
            if (!json2.has("balls")) {
                try {
                    json2.put("balls", new JSONArray());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (balls.length() <= 0) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("ball", "0");
                    jsonObject.put("notice", "该排班随机分配号球");
                } catch (JSONException e2) {
                    e2.printStackTrace();
                }
                json2.optJSONArray("balls").put(jsonObject);
            } else {
                for (int j = 0; j < balls.length(); j++) {
                    json2.optJSONArray("balls").put(balls.optJSONObject(j));
                }
            }
            tmpJons.put(json2);
        }
        this.elvContext.setAdapter(new SelectTimeExpandableAdapter(this, tmpJons, this.day, this.doctorid));
    }

    public void refreshActivity(Object... objects) {
        this.pDialog.dismiss();
        int requestType = ((Integer) objects[0]).intValue();
        if (((Boolean) objects[1]).booleanValue()) {
            loadData(requestType, ((JSONObject) objects[2]).optJSONObject("inf").optJSONArray("ordernumbers"));
        }
    }

    private JSONArray clearData(JSONArray plans, String type2) {
        JSONArray jsons = new JSONArray();
        for (int i = 0; i < plans.length(); i++) {
            if (type2.equals(plans.optJSONObject(i).optString("timespan"))) {
                jsons.put(plans.optJSONObject(i));
            }
        }
        return jsons;
    }
}
