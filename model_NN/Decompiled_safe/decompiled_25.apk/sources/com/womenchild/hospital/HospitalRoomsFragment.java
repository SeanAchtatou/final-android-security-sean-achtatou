package com.womenchild.hospital;

import android.app.Activity;
import android.app.ProgressDialog;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.base.BaseRequestFragment;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.request.RequestTask;
import com.womenchild.hospital.util.ClientLogUtil;
import com.womenchild.hospital.util.WebUtil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HospitalRoomsFragment extends BaseRequestFragment implements View.OnClickListener {
    private static final String TAG = "HpRoomsFragment";
    private int currIndex = 0;
    private Button ibtnHome;
    private ImageView iv_check;
    private ImageView iv_take_medicine;
    private ImageView iv_vis;
    private ProgressDialog pDialog;
    private String[] strRooms;
    private String[] strRoomsTitle;
    private TextView tv_check;
    private TextView tv_take_medicine;
    private TextView tv_vis;
    private WebView wvhprooms;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ClientLogUtil.v(TAG, "onCreateView()");
        View view = inflater.inflate((int) R.layout.hospital_rooms, container, false);
        initViewId(view);
        initClickListener();
        this.pDialog = new ProgressDialog(getActivity());
        this.pDialog.setMessage(getResources().getString(R.string.loading_data));
        this.pDialog.show();
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.HOSPITAL_HELP_LIST), initRequestParams((int) HttpRequestParameters.HOSPITAL_HELP_LIST));
        return view;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ClientLogUtil.v(TAG, "onActivityCreated()");
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ClientLogUtil.v(TAG, "onAttach()");
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ClientLogUtil.v(TAG, "onCreate()");
    }

    public void onDestroy() {
        super.onDestroy();
        ClientLogUtil.v(TAG, "onDestroy()");
    }

    public void onDestroyView() {
        super.onDestroyView();
        ClientLogUtil.v(TAG, "onDestroyView()");
    }

    public void onDetach() {
        super.onDetach();
        ClientLogUtil.v(TAG, "onDetach()");
    }

    public void onPause() {
        super.onPause();
        ClientLogUtil.v(TAG, "onPause()");
    }

    public void onResume() {
        super.onResume();
        ClientLogUtil.v(TAG, "onResume()");
    }

    public void onStart() {
        super.onStart();
        ClientLogUtil.v(TAG, "onStart()");
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_home /*2131296435*/:
                getActivity().finish();
                return;
            default:
                return;
        }
    }

    public void initViewId() {
    }

    public void initClickListener() {
        this.tv_vis.setOnClickListener(new MyOnClickListener(0));
        this.tv_check.setOnClickListener(new MyOnClickListener(1));
        this.tv_take_medicine.setOnClickListener(new MyOnClickListener(3));
        this.ibtnHome.setOnClickListener(this);
    }

    public UriParameter initRequestParams(int requestType) {
        UriParameter parameters = new UriParameter();
        parameters.add("num", 2);
        return parameters;
    }

    public UriParameter initRequestParams(String requestType) {
        UriParameter parameters = new UriParameter();
        parameters.add("id", requestType);
        return parameters;
    }

    public void loadData(int requestType, Object data) {
        JSONArray jsonArray;
        JSONObject json = (JSONObject) data;
        JSONObject res = json.optJSONObject("res");
        String msg = res.optString("msg");
        if (res != null) {
            try {
                if (res.getInt("st") == 0) {
                    if (requestType == 121) {
                        JSONObject inf = json.optJSONObject("inf");
                        if (inf != null) {
                            JSONArray jsonArray2 = inf.getJSONArray("notices");
                            int length = jsonArray2.length();
                            if (jsonArray2 != null && length > 0) {
                                this.strRoomsTitle = new String[length];
                                this.strRooms = new String[length];
                                for (int i = 0; i < jsonArray2.length(); i++) {
                                    this.strRoomsTitle[i] = jsonArray2.getJSONObject(i).getString("title");
                                    this.strRooms[i] = jsonArray2.getJSONObject(i).getString("id");
                                }
                                this.tv_vis.setText(this.strRoomsTitle[0]);
                                switchImage(0);
                                this.tv_take_medicine.setText(this.strRoomsTitle[1]);
                                return;
                            }
                            return;
                        }
                        return;
                    } else if (requestType == 122 && (jsonArray = json.getJSONArray("inf")) != null && jsonArray.length() > 0) {
                        this.wvhprooms.setWebViewClient(new WebViewClient() {
                            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                                super.onReceivedSslError(view, handler, error);
                                handler.proceed();
                            }
                        });
                        if (0 < jsonArray.length()) {
                            this.wvhprooms.loadDataWithBaseURL(null, WebUtil.sourceCreateHtml(jsonArray.getJSONObject(0).getString("content")), "text/html", "utf-8", null);
                            return;
                        }
                        return;
                    } else {
                        return;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
                ClientLogUtil.i(TAG, e.getMessage());
                return;
            }
        }
        Toast.makeText(getActivity(), msg, 1).show();
    }

    public class MyOnClickListener implements View.OnClickListener {
        private int index = 0;

        public MyOnClickListener(int i) {
            this.index = i;
        }

        public void onClick(View v) {
            HospitalRoomsFragment.this.switchImage(this.index);
        }
    }

    /* access modifiers changed from: private */
    public void switchImage(int index) {
        switch (index) {
            case 0:
                this.tv_vis.setTextColor(getResources().getColor(R.color.white));
                this.tv_check.setTextColor(getResources().getColor(R.color.black));
                this.tv_take_medicine.setTextColor(getResources().getColor(R.color.black));
                this.iv_vis.setBackgroundResource(R.drawable.ygkz_number_blank_blue_1);
                this.iv_check.setBackgroundResource(R.drawable.ygkz_number_blank_withline_2);
                this.iv_take_medicine.setBackgroundResource(R.drawable.ygkz_number_blank_withline_3);
                sendHttpRequest((int) HttpRequestParameters.HOSPITAL_HELP_CONTTENT, this.strRooms[0]);
                return;
            case 1:
                this.tv_vis.setTextColor(getResources().getColor(R.color.black));
                this.tv_check.setTextColor(getResources().getColor(R.color.white));
                this.tv_take_medicine.setTextColor(getResources().getColor(R.color.black));
                this.iv_vis.setBackgroundResource(R.drawable.ygkz_number_blank_withline_1);
                this.iv_check.setBackgroundResource(R.drawable.ygkz_number_blank_blue_2);
                this.iv_take_medicine.setBackgroundResource(R.drawable.ygkz_number_blank_withline_3);
                return;
            case 2:
            default:
                return;
            case 3:
                this.tv_vis.setTextColor(getResources().getColor(R.color.black));
                this.tv_check.setTextColor(getResources().getColor(R.color.black));
                this.tv_take_medicine.setTextColor(getResources().getColor(R.color.white));
                this.iv_vis.setBackgroundResource(R.drawable.ygkz_number_blank_withline_1);
                this.iv_check.setBackgroundResource(R.drawable.ygkz_number_blank_withline_2);
                this.iv_take_medicine.setBackgroundResource(R.drawable.ygkz_number_blank_blue_3);
                sendHttpRequest((int) HttpRequestParameters.HOSPITAL_HELP_CONTTENT, this.strRooms[1]);
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void initViewId(View view) {
        this.ibtnHome = (Button) view.findViewById(R.id.ibtn_home);
        this.wvhprooms = (WebView) view.findViewById(R.id.wv_hp_rooms);
        this.tv_vis = (TextView) view.findViewById(R.id.tv_vis);
        this.tv_check = (TextView) view.findViewById(R.id.tv_check);
        this.tv_take_medicine = (TextView) view.findViewById(R.id.tv_take_medicine);
        this.iv_vis = (ImageView) view.findViewById(R.id.iv_vis);
        this.iv_check = (ImageView) view.findViewById(R.id.iv_check);
        this.iv_take_medicine = (ImageView) view.findViewById(R.id.iv_take_medicine);
        if (HospitalGuideActivity.currentIndex == 1) {
            this.ibtnHome.setText(PoiTypeDef.All);
            this.ibtnHome.setBackgroundResource(R.drawable.back_selector);
        }
    }

    /* access modifiers changed from: protected */
    public void initUIData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        return null;
    }

    public void refreshFragment(Object... params) {
        this.pDialog.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            loadData(requestType, (JSONObject) params[2]);
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.network_connect_failed_prompt), 0).show();
        }
    }

    public void sendHttpRequest(int requestType, String pam) {
        this.pDialog = new ProgressDialog(getActivity());
        this.pDialog.setMessage(getResources().getString(R.string.loading_data));
        this.pDialog.show();
        RequestTask.getInstance().sendHttpRequest(this, String.valueOf(requestType), initRequestParams(pam));
    }
}
