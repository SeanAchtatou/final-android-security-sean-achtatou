package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.upomp.pay.help.Create_MerchantX;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.entity.ErrorPayModel;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import org.json.JSONException;
import org.json.JSONObject;

public class DetailErrorPayActivity extends BaseRequestActivity implements View.OnClickListener {
    public static final int ACTIVITYRESULTCODE = 107;
    public static final String DETAILERRPAYMODEL = "DETAILERRPAYMODEL";
    private Button btnBack;
    private Button btnBackMoeny;
    private ErrorPayModel model;
    private ProgressDialog pdDialog;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.detail_errorpay);
        initViewId();
        initClickListener();
    }

    public void onClick(View arg0) {
        if (arg0 == this.btnBack) {
            finish();
        } else if (arg0 == this.btnBackMoeny) {
            this.pdDialog = new ProgressDialog(this);
            this.pdDialog.setMessage(getResources().getString(R.string.loading_confirm_condition));
            this.pdDialog.show();
            sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.BACK_MONEY), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.BACK_MONEY)));
        }
    }

    public void refreshActivity(Object... params) {
        this.pdDialog.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        boolean status = ((Boolean) params[1]).booleanValue();
        JSONObject result = (JSONObject) params[2];
        if (status) {
            loadData(requestType, result);
        } else {
            Toast.makeText(this, (int) R.string.network_connect_failed_prompt, 1).show();
        }
    }

    public void initViewId() {
        this.btnBack = (Button) findViewById(R.id.btn_dep_back);
        this.btnBackMoeny = (Button) findViewById(R.id.btn_dep_backmoney);
        this.model = (ErrorPayModel) getIntent().getSerializableExtra(DETAILERRPAYMODEL);
        String payway = this.model.getPayway();
        initItem("已支付金额", String.valueOf(String.valueOf(this.model.getPrice())) + " 元", (LinearLayout) findViewById(R.id.include_dep_item1));
        LinearLayout layout = (LinearLayout) findViewById(R.id.include_dep_item2);
        if ("alipay".equals(payway)) {
            initItem("支付宝订单号", this.model.getYlordernum(), layout);
            initItem("支付宝流水号", this.model.getCupsqid(), layout);
            this.btnBackMoeny.setVisibility(8);
        } else if ("sunpay".equals(payway)) {
            initItem("阳光订单号", this.model.getYlordernum(), layout);
            initItem("阳光流水号", this.model.getCupsqid(), layout);
            this.btnBackMoeny.setVisibility(0);
        } else {
            initItem("银联订单号", this.model.getYlordernum(), layout);
            initItem("银联流水号", this.model.getCupsqid(), layout);
            this.btnBackMoeny.setVisibility(0);
        }
        LinearLayout layout2 = (LinearLayout) findViewById(R.id.include_dep_item3);
        initItem("交易时间", this.model.getPaytime(), (LinearLayout) findViewById(R.id.include_dep_item4));
        initItem("订单状态", "异常无订单", (LinearLayout) findViewById(R.id.include_dep_item5));
    }

    public void initClickListener() {
        this.btnBack.setOnClickListener(this);
        this.btnBackMoeny.setOnClickListener(this);
    }

    public void loadData(int requestType, Object data) {
        JSONObject res = ((JSONObject) data).optJSONObject("res");
        if (res != null) {
            try {
                if (res.getInt("st") == 0) {
                    if (609 == requestType) {
                        Toast.makeText(this, "退款成功", 1).show();
                        Intent intent = new Intent();
                        intent.putExtra(DETAILERRPAYMODEL, this.model);
                        setResult(107, intent);
                        finish();
                        return;
                    }
                    return;
                }
            } catch (JSONException e) {
                e.printStackTrace();
                return;
            }
        }
        if (res != null) {
            Toast.makeText(this, res.getString("msg"), 1).show();
        } else {
            Toast.makeText(this, "退款失败", 1).show();
        }
    }

    private void initItem(String itemname, String itemvalue, LinearLayout layout) {
        ((TextView) layout.findViewById(R.id.txt_item_name)).setText(itemname);
        ((TextView) layout.findViewById(R.id.txt_item_value)).setText(itemvalue);
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.BACK_MONEY /*609*/:
                parameters.add("oldordernum", this.model.getYlordernum());
                parameters.add("ordernum", Create_MerchantX.createMerchantOrderId());
                parameters.add("ordertime", Create_MerchantX.createMerchantOrderTime());
                parameters.add("orderamt", String.valueOf(this.model.getPrice()));
                break;
        }
        return parameters;
    }
}
