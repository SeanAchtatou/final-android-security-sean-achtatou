package com.womenchild.hospital;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import com.womenchild.hospital.util.ShareUtil;

public class TestFragment extends Fragment implements View.OnClickListener {
    private ImageButton ibtn_department_guide;
    private ImageButton ibtn_overdue_help;
    private ImageButton ibtn_remind_set;
    private ImageButton ibtn_share;
    private Intent intent;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate((int) R.layout.test, container, false);
        this.ibtn_department_guide = (ImageButton) view.findViewById(R.id.ibtn_department_guide);
        this.ibtn_overdue_help = (ImageButton) view.findViewById(R.id.ibtn_overdue_help);
        this.ibtn_remind_set = (ImageButton) view.findViewById(R.id.ibtn_remind_set);
        this.ibtn_share = (ImageButton) view.findViewById(R.id.ibtn_share);
        this.ibtn_department_guide.setOnClickListener(this);
        this.ibtn_overdue_help.setOnClickListener(this);
        this.ibtn_remind_set.setOnClickListener(this);
        this.ibtn_share.setOnClickListener(this);
        return view;
    }

    public void onClick(View v) {
        if (v == this.ibtn_department_guide) {
            this.intent = new Intent(getActivity(), CheckInstructionsActivity.class);
            startActivity(this.intent);
        } else if (v == this.ibtn_overdue_help) {
            this.intent = new Intent(getActivity(), OverDueHelpActivity.class);
            this.intent.putExtra("index", 7);
            startActivity(this.intent);
        } else if (v == this.ibtn_remind_set) {
            this.intent = new Intent(getActivity(), RemindSetActivity.class);
            this.intent.putExtra("remindtype", 2);
            startActivityForResult(this.intent, 1);
        } else if (v == this.ibtn_share) {
            ShareUtil.share2Friend(getActivity(), R.string.app_name);
        }
    }
}
