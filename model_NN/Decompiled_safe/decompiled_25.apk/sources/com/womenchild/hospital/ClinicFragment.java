package com.womenchild.hospital;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import com.womenchild.hospital.base.BaseRequestFragment;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.ClientLogUtil;

public class ClinicFragment extends BaseRequestFragment {
    private static final String TAG = "ClinicFragment";
    public static ListView listView;
    public static TextView tv_clinic;
    private View view;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ClientLogUtil.v(TAG, "onCreateView()");
        this.view = inflater.inflate((int) R.layout.clinic, container, false);
        initViewId();
        initClickListener();
        return this.view;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ClientLogUtil.v(TAG, "onActivityCreated()");
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ClientLogUtil.v(TAG, "onAttach()");
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ClientLogUtil.v(TAG, "onCreate()");
    }

    public void onDestroy() {
        super.onDestroy();
        ClientLogUtil.v(TAG, "onDestroy()");
    }

    public void onDestroyView() {
        super.onDestroyView();
        ClientLogUtil.v(TAG, "onDestroyView()");
    }

    public void onDetach() {
        super.onDetach();
        ClientLogUtil.v(TAG, "onDetach()");
    }

    public void onPause() {
        super.onPause();
        ClientLogUtil.v(TAG, "onPause()");
    }

    public void onResume() {
        super.onResume();
        ClientLogUtil.v(TAG, "onResume()");
    }

    public void onStart() {
        super.onStart();
        ClientLogUtil.v(TAG, "onStart()");
    }

    public void initViewId() {
        listView = (ListView) this.view.findViewById(R.id.listview);
        tv_clinic = (TextView) this.view.findViewById(R.id.tips_clinic);
    }

    public void initClickListener() {
    }

    /* access modifiers changed from: protected */
    public void initViewId(View view2) {
    }

    /* access modifiers changed from: protected */
    public void initUIData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        return null;
    }

    public void refreshFragment(Object... objects) {
    }
}
