package com.womenchild.hospital;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.womenchild.hospital.adapter.ClinicAdapter;
import com.womenchild.hospital.adapter.NoClinicAdapter;
import com.womenchild.hospital.base.BaseRequestFragment;
import com.womenchild.hospital.entity.RecordEntity;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.AppParameters;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.ClientLogUtil;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public class NotClinicFragment extends BaseRequestFragment {
    private static final String TAG = "NotClinicFragment";
    public static List<RecordEntity> clinicList;
    public static ListView listview;
    public static List<RecordEntity> noClinicList;
    public static TextView tv_not_clinic;
    private ProgressDialog pd;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ClientLogUtil.v(TAG, "onCreateView()");
        View view = inflater.inflate((int) R.layout.not_clinic, container, false);
        initViewId(view);
        initClickListener();
        this.pd = new ProgressDialog(getActivity());
        this.pd.setCancelable(true);
        this.pd.setCanceledOnTouchOutside(false);
        this.pd.setMessage(getResources().getString(R.string.mark_wait));
        this.pd.show();
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.READ_REG_LIST), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.READ_REG_LIST)));
        return view;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ClientLogUtil.v(TAG, "onActivityCreated()");
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ClientLogUtil.v(TAG, "onAttach()");
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ClientLogUtil.v(TAG, "onCreate()");
    }

    public void onDestroy() {
        super.onDestroy();
        ClientLogUtil.v(TAG, "onDestroy()");
    }

    public void onDestroyView() {
        super.onDestroyView();
        ClientLogUtil.v(TAG, "onDestroyView()");
    }

    public void onDetach() {
        super.onDetach();
        ClientLogUtil.v(TAG, "onDetach()");
    }

    public void onPause() {
        super.onPause();
        ClientLogUtil.v(TAG, "onPause()");
    }

    public void onResume() {
        super.onResume();
        ClientLogUtil.v(TAG, "onResume()");
    }

    public void onStart() {
        super.onStart();
        ClientLogUtil.v(TAG, "onStart()");
    }

    public void initViewId() {
    }

    public void initClickListener() {
    }

    /* access modifiers changed from: protected */
    public void initViewId(View view) {
        listview = (ListView) view.findViewById(R.id.listview);
        tv_not_clinic = (TextView) view.findViewById(R.id.tips_not_clinic);
    }

    /* access modifiers changed from: protected */
    public void initUIData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.READ_REG_LIST /*125*/:
                parameters.add("memberid", String.valueOf(AppParameters.getAPPID()) + "_" + UserEntity.getInstance().getInfo().getUserid());
                break;
        }
        return parameters;
    }

    public void refreshFragment(Object... params) {
        this.pd.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject result = (JSONObject) params[2];
            ClientLogUtil.i(TAG, result.toString());
            switch (requestType) {
                case HttpRequestParameters.READ_REG_LIST /*125*/:
                    loadData(HttpRequestParameters.READ_REG_LIST, result);
                    return;
                default:
                    return;
            }
        } else {
            Toast.makeText(getActivity(), (int) R.string.network_connect_failed_prompt, 0).show();
        }
    }

    public void loadData(int requestType, Object data) {
        JSONObject result = (JSONObject) data;
        String st = result.optJSONObject("res").optString("st");
        String optString = result.optJSONObject("res").optString("msg");
        switch (requestType) {
            case HttpRequestParameters.READ_REG_LIST /*125*/:
                if ("0".equals(st)) {
                    bindView(RecordEntity.getList(result));
                    return;
                }
                ClinicFragment.tv_clinic.setVisibility(0);
                tv_not_clinic.setVisibility(0);
                return;
            default:
                return;
        }
    }

    private void bindView(List<RecordEntity> list) {
        if (list != null) {
            clinicList = new ArrayList();
            noClinicList = new ArrayList();
            long currentTime = System.currentTimeMillis();
            for (int i = 0; i < list.size(); i++) {
                RecordEntity entity = list.get(i);
                if (currentTime > Long.valueOf(entity.getPlanstoptime()).longValue()) {
                    clinicList.add(entity);
                } else {
                    noClinicList.add(entity);
                }
            }
            if (clinicList == null || clinicList.size() <= 0) {
                ClinicFragment.tv_clinic.setVisibility(0);
            } else {
                ClinicFragment.listView.setAdapter((ListAdapter) new ClinicAdapter(getActivity(), clinicList));
            }
            if (noClinicList == null || noClinicList.size() <= 0) {
                tv_not_clinic.setVisibility(0);
            } else {
                listview.setAdapter((ListAdapter) new NoClinicAdapter(getActivity(), noClinicList));
            }
            setOnItemListener();
        }
    }

    private void setOnItemListener() {
        ClinicFragment.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                Intent intent = new Intent(NotClinicFragment.this.getActivity(), ClinicDelActivity.class);
                intent.putExtra("position", arg2);
                NotClinicFragment.this.startActivity(intent);
            }
        });
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                Intent intent = new Intent(NotClinicFragment.this.getActivity(), NoClinicDelActivity.class);
                intent.putExtra("position", arg2);
                NotClinicFragment.this.startActivity(intent);
            }
        });
    }
}
