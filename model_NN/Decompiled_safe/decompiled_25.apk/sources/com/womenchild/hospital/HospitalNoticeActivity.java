package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.Toast;
import com.womenchild.hospital.adapter.HospitalNoticeAdapter;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import org.json.JSONArray;
import org.json.JSONObject;

public class HospitalNoticeActivity extends BaseRequestActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    private Button btn_dynamic;
    private Button btn_information;
    private Button btn_noticlist;
    private RadioGroup gdGroup;
    /* access modifiers changed from: private */
    public Intent intent;
    private Button iv_return;
    private JSONArray jsons;
    private ListView lv_notice;
    /* access modifiers changed from: private */
    public Context mContext = this;
    RadioGroup.OnCheckedChangeListener onCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        public void onCheckedChanged(RadioGroup arg0, int tabId) {
            switch (tabId) {
                case R.id.btn_dynamic /*2131297111*/:
                    HospitalNoticeActivity.this.intent = new Intent(HospitalNoticeActivity.this.mContext, HospitalDynamicActivity.class);
                    HospitalNoticeActivity.this.startActivity(HospitalNoticeActivity.this.intent);
                    HospitalNoticeActivity.this.finish();
                    return;
                case R.id.btn_noticlist /*2131297112*/:
                default:
                    return;
                case R.id.btn_information /*2131297113*/:
                    HospitalNoticeActivity.this.intent = new Intent(HospitalNoticeActivity.this.mContext, StopPlanActivity.class);
                    HospitalNoticeActivity.this.startActivity(HospitalNoticeActivity.this.intent);
                    HospitalNoticeActivity.this.finish();
                    return;
            }
        }
    };
    private ProgressDialog pdDialog;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.hospital_notice);
        initViewId();
        initClickListener();
        this.pdDialog = new ProgressDialog(this);
        this.pdDialog.setMessage(getResources().getString(R.string.loading_data));
        this.pdDialog.show();
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.HOSPITAL_HELP_LIST), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.HOSPITAL_HELP_LIST)));
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void refreshActivity(Object... params) {
        this.pdDialog.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject json = (JSONObject) params[2];
            if (json.optJSONObject("res").optInt("st") == 0) {
                loadData(requestType, json.optJSONObject("inf").optJSONArray("notices"));
            } else {
                Toast.makeText(this, json.optJSONObject("res").optString("msg"), 0).show();
            }
        } else {
            Toast.makeText(this, getResources().getString(R.string.service_ex), 0).show();
        }
    }

    public void initViewId() {
        this.iv_return = (Button) findViewById(R.id.iv_return);
        this.lv_notice = (ListView) findViewById(R.id.lv_notice);
        this.gdGroup = (RadioGroup) findViewById(R.id.rl_bottom);
        this.gdGroup.setOnCheckedChangeListener(this.onCheckedChangeListener);
        this.gdGroup.check(R.id.btn_noticlist);
    }

    public void initClickListener() {
        this.iv_return.setOnClickListener(this);
        this.lv_notice.setOnItemClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_return /*2131296513*/:
                finish();
                return;
            case R.id.btn_dynamic /*2131297111*/:
                this.intent = new Intent(this, HospitalDynamicActivity.class);
                startActivity(this.intent);
                finish();
                return;
            case R.id.btn_information /*2131297113*/:
                this.intent = new Intent(this, StopPlanActivity.class);
                startActivity(this.intent);
                finish();
                return;
            default:
                return;
        }
    }

    public void loadData(int requestType, Object data) {
        this.jsons = (JSONArray) data;
        switch (requestType) {
            case HttpRequestParameters.HOSPITAL_HELP_LIST /*121*/:
                this.lv_notice.setAdapter((ListAdapter) new HospitalNoticeAdapter(this, this.jsons));
                return;
            default:
                return;
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
        Intent intent2 = new Intent(this, HospitalNoticeContentActivity.class);
        intent2.putExtra("id", this.jsons.optJSONObject(arg2).optString("id"));
        startActivity(intent2);
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.HOSPITAL_HELP_LIST /*121*/:
                parameters.add("num", 1);
                break;
        }
        return parameters;
    }
}
