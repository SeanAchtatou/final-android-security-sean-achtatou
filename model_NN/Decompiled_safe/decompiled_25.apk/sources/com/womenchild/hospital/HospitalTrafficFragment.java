package com.womenchild.hospital;

import android.app.Activity;
import android.app.ProgressDialog;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;
import com.womenchild.hospital.base.BaseRequestFragment;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.ClientLogUtil;
import com.womenchild.hospital.util.WebUtil;
import org.json.JSONException;
import org.json.JSONObject;

public class HospitalTrafficFragment extends BaseRequestFragment implements View.OnClickListener {
    private static final String TAG = "HpTrafficFrag";
    private Button ibtnHome;
    private ProgressDialog pDialog;
    private WebView wvHpTraffic;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ClientLogUtil.v(TAG, "onCreateView()");
        View view = inflater.inflate((int) R.layout.hospital_traffic, container, false);
        initViewId(view);
        initClickListener();
        this.pDialog = new ProgressDialog(getActivity());
        this.pDialog.setMessage(getResources().getString(R.string.loading_data));
        this.pDialog.show();
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.HOSPITAL_TAFFIC_INFO), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.HOSPITAL_TAFFIC_INFO)));
        return view;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ClientLogUtil.v(TAG, "onActivityCreated()");
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ClientLogUtil.v(TAG, "onAttach()");
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ClientLogUtil.v(TAG, "onCreate()");
    }

    public void onDestroy() {
        super.onDestroy();
        ClientLogUtil.v(TAG, "onDestroy()");
    }

    public void onDestroyView() {
        super.onDestroyView();
        ClientLogUtil.v(TAG, "onDestroyView()");
    }

    public void onDetach() {
        super.onDetach();
        ClientLogUtil.v(TAG, "onDetach()");
    }

    public void onPause() {
        super.onPause();
        ClientLogUtil.v(TAG, "onPause()");
    }

    public void onResume() {
        super.onResume();
        ClientLogUtil.v(TAG, "onResume()");
    }

    public void onStart() {
        super.onStart();
        ClientLogUtil.v(TAG, "onStart()");
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_home /*2131296435*/:
                getActivity().finish();
                return;
            default:
                return;
        }
    }

    public void initViewId() {
    }

    public void initClickListener() {
        this.ibtnHome.setOnClickListener(this);
    }

    public void loadData(int requestType, Object data) {
        JSONObject jsonObject = (JSONObject) data;
        JSONObject res = jsonObject.optJSONObject("res");
        String msg = res.optString("msg");
        if (res != null) {
            try {
                if (res.getInt("st") == 0) {
                    JSONObject inf = jsonObject.optJSONObject("inf");
                    if (inf != null) {
                        this.wvHpTraffic.setWebViewClient(new WebViewClient() {
                            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                                super.onReceivedSslError(view, handler, error);
                                handler.proceed();
                            }
                        });
                        this.wvHpTraffic.loadDataWithBaseURL(null, WebUtil.sourceCreateHtml(inf.optString("noticecontent")), "text/html", "utf-8", null);
                        return;
                    }
                    return;
                }
            } catch (JSONException e) {
                e.printStackTrace();
                ClientLogUtil.i(TAG, e.getMessage());
                return;
            }
        }
        Toast.makeText(getActivity(), msg, 1).show();
    }

    /* access modifiers changed from: protected */
    public void initViewId(View view) {
        this.ibtnHome = (Button) view.findViewById(R.id.ibtn_home);
        this.wvHpTraffic = (WebView) view.findViewById(R.id.wv_hp_traffic);
    }

    /* access modifiers changed from: protected */
    public void initUIData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        parameters.add("num", 4);
        return parameters;
    }

    public void refreshFragment(Object... params) {
        this.pDialog.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            loadData(requestType, (JSONObject) params[2]);
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.network_connect_failed_prompt), 0).show();
        }
    }
}
