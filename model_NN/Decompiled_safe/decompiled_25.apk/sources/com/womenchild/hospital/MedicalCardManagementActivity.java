package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.entity.PatientCardEntity;
import com.womenchild.hospital.entity.PatientCardListEntity;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.ClientLogUtil;
import java.util.List;
import org.json.JSONObject;

public class MedicalCardManagementActivity extends BaseRequestActivity implements View.OnClickListener {
    private static final String TAG = "MedicalCardManagementActivity";
    public static List<PatientCardListEntity> list;
    private Button ibtn_return;
    private Intent intent;
    private ProgressDialog pd;
    private RelativeLayout rl_add_1;
    private RelativeLayout rl_add_2;
    private RelativeLayout rl_add_3;
    private RelativeLayout rl_card_1;
    private RelativeLayout rl_card_2;
    private RelativeLayout rl_card_3;
    private RelativeLayout rl_card_4;
    private RelativeLayout rl_card_5;
    private RelativeLayout rl_card_6;
    private RelativeLayout rl_card_7;
    private RelativeLayout rl_card_8;
    private RelativeLayout rl_card_9;
    private RelativeLayout rl_user_1;
    private RelativeLayout rl_user_2;
    private RelativeLayout rl_user_3;
    private TextView tv_card_1;
    private TextView tv_card_2;
    private TextView tv_card_3;
    private TextView tv_card_4;
    private TextView tv_card_5;
    private TextView tv_card_6;
    private TextView tv_card_7;
    private TextView tv_card_8;
    private TextView tv_card_9;
    private TextView tv_user_name_1;
    private TextView tv_user_name_2;
    private TextView tv_user_name_3;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.medical_card_management);
        initViewId();
        initClickListener();
        initData();
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.PATIENT_CARD_LIST), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.PATIENT_CARD_LIST)));
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.pd = new ProgressDialog(this);
        this.pd.setMessage(getResources().getString(R.string.inquiry));
    }

    public void refreshActivity(Object... params) {
        this.pd.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject result = (JSONObject) params[2];
            ClientLogUtil.i(TAG, result.toString());
            switch (requestType) {
                case HttpRequestParameters.PATIENT_CARD_LIST /*123*/:
                    loadData(HttpRequestParameters.PATIENT_CARD_LIST, result);
                    return;
                default:
                    return;
            }
        } else {
            this.rl_user_1.setVisibility(8);
            this.rl_user_2.setVisibility(8);
            this.rl_user_3.setVisibility(8);
            Toast.makeText(this, (int) R.string.network_connect_failed_prompt, 0).show();
        }
    }

    public void initViewId() {
        this.ibtn_return = (Button) findViewById(R.id.ibtn_return);
        this.rl_card_1 = (RelativeLayout) findViewById(R.id.rl_card_1);
        this.rl_card_2 = (RelativeLayout) findViewById(R.id.rl_card_2);
        this.rl_card_3 = (RelativeLayout) findViewById(R.id.rl_card_3);
        this.rl_card_4 = (RelativeLayout) findViewById(R.id.rl_card_4);
        this.rl_card_5 = (RelativeLayout) findViewById(R.id.rl_card_5);
        this.rl_card_6 = (RelativeLayout) findViewById(R.id.rl_card_6);
        this.rl_card_7 = (RelativeLayout) findViewById(R.id.rl_card_7);
        this.rl_card_8 = (RelativeLayout) findViewById(R.id.rl_card_8);
        this.rl_card_9 = (RelativeLayout) findViewById(R.id.rl_card_9);
        this.rl_add_1 = (RelativeLayout) findViewById(R.id.rl_add_1);
        this.rl_add_2 = (RelativeLayout) findViewById(R.id.rl_add_2);
        this.rl_add_3 = (RelativeLayout) findViewById(R.id.rl_add_3);
        this.rl_user_1 = (RelativeLayout) findViewById(R.id.rl_user_1);
        this.rl_user_2 = (RelativeLayout) findViewById(R.id.rl_user_2);
        this.rl_user_3 = (RelativeLayout) findViewById(R.id.rl_user_3);
        this.tv_user_name_1 = (TextView) findViewById(R.id.tv_user_name_1);
        this.tv_user_name_2 = (TextView) findViewById(R.id.tv_user_name_2);
        this.tv_user_name_3 = (TextView) findViewById(R.id.tv_user_name_3);
        this.tv_card_1 = (TextView) findViewById(R.id.tv_card_1);
        this.tv_card_2 = (TextView) findViewById(R.id.tv_card_2);
        this.tv_card_3 = (TextView) findViewById(R.id.tv_card_3);
        this.tv_card_4 = (TextView) findViewById(R.id.tv_card_4);
        this.tv_card_5 = (TextView) findViewById(R.id.tv_card_5);
        this.tv_card_6 = (TextView) findViewById(R.id.tv_card_6);
        this.tv_card_7 = (TextView) findViewById(R.id.tv_card_7);
        this.tv_card_8 = (TextView) findViewById(R.id.tv_card_8);
        this.tv_card_9 = (TextView) findViewById(R.id.tv_card_9);
    }

    public void initClickListener() {
        this.ibtn_return.setOnClickListener(this);
        this.rl_card_1.setOnClickListener(this);
        this.rl_card_2.setOnClickListener(this);
        this.rl_card_3.setOnClickListener(this);
        this.rl_card_4.setOnClickListener(this);
        this.rl_card_5.setOnClickListener(this);
        this.rl_card_6.setOnClickListener(this);
        this.rl_card_7.setOnClickListener(this);
        this.rl_card_8.setOnClickListener(this);
        this.rl_card_9.setOnClickListener(this);
        this.rl_add_1.setOnClickListener(this);
        this.rl_add_2.setOnClickListener(this);
        this.rl_add_3.setOnClickListener(this);
    }

    public void loadData(int requestType, Object data) {
        switch (requestType) {
            case HttpRequestParameters.PATIENT_CARD_LIST /*123*/:
                JSONObject result = (JSONObject) data;
                String st = result.optJSONObject("res").optString("st");
                String msg = result.optJSONObject("res").optString("msg");
                if ("0".equals(st)) {
                    list = PatientCardListEntity.getList(result);
                    bindView(list);
                    return;
                }
                Toast.makeText(this, msg, 0).show();
                return;
            default:
                return;
        }
    }

    private void bindView(List<PatientCardListEntity> list2) {
        int count = list2.size();
        this.rl_user_1.setVisibility(8);
        this.rl_user_2.setVisibility(8);
        this.rl_user_3.setVisibility(8);
        switch (count) {
            case 1:
                break;
            case 3:
                this.rl_user_3.setVisibility(0);
                this.tv_user_name_3.setText(list2.get(2).getPatientname());
                List<PatientCardEntity> cardList3 = list2.get(2).getCardList();
                this.rl_card_7.setVisibility(8);
                this.rl_card_8.setVisibility(8);
                this.rl_card_9.setVisibility(8);
                switch (cardList3.size()) {
                    case 3:
                        this.rl_card_9.setVisibility(0);
                        this.tv_card_9.setText(cardList3.get(2).getCard());
                    case 2:
                        this.rl_card_8.setVisibility(0);
                        this.tv_card_8.setText(cardList3.get(1).getCard());
                    case 1:
                        this.rl_card_7.setVisibility(0);
                        this.tv_card_7.setText(cardList3.get(0).getCard());
                }
            case 2:
                this.rl_user_2.setVisibility(0);
                this.tv_user_name_2.setText(list2.get(1).getPatientname());
                List<PatientCardEntity> cardList2 = list2.get(1).getCardList();
                this.rl_card_4.setVisibility(8);
                this.rl_card_5.setVisibility(8);
                this.rl_card_6.setVisibility(8);
                switch (cardList2.size()) {
                    case 3:
                        this.rl_card_6.setVisibility(0);
                        this.tv_card_6.setText(cardList2.get(2).getCard());
                    case 2:
                        this.rl_card_5.setVisibility(0);
                        this.tv_card_5.setText(cardList2.get(1).getCard());
                    case 1:
                        this.rl_card_4.setVisibility(0);
                        this.tv_card_4.setText(cardList2.get(0).getCard());
                        break;
                }
                break;
            default:
                return;
        }
        this.rl_user_1.setVisibility(0);
        this.tv_user_name_1.setText(list2.get(0).getPatientname());
        List<PatientCardEntity> cardList1 = list2.get(0).getCardList();
        this.rl_card_1.setVisibility(8);
        this.rl_card_2.setVisibility(8);
        this.rl_card_3.setVisibility(8);
        switch (cardList1.size()) {
            case 1:
                break;
            case 3:
                this.rl_card_3.setVisibility(0);
                this.tv_card_3.setText(cardList1.get(2).getCard());
            case 2:
                this.rl_card_2.setVisibility(0);
                this.tv_card_2.setText(cardList1.get(1).getCard());
                break;
            default:
                return;
        }
        this.rl_card_1.setVisibility(0);
        this.tv_card_1.setText(cardList1.get(0).getCard());
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_return /*2131296570*/:
                finish();
                return;
            case R.id.rl_card_1 /*2131296854*/:
                this.intent = new Intent(this, ModifyMedicalCardActivity.class);
                this.intent.putExtra("userno", 0);
                this.intent.putExtra("cardno", 0);
                startActivity(this.intent);
                return;
            case R.id.rl_card_2 /*2131296858*/:
                this.intent = new Intent(this, ModifyMedicalCardActivity.class);
                this.intent.putExtra("userno", 0);
                this.intent.putExtra("cardno", 1);
                startActivity(this.intent);
                return;
            case R.id.rl_card_3 /*2131296862*/:
                this.intent = new Intent(this, ModifyMedicalCardActivity.class);
                this.intent.putExtra("userno", 0);
                this.intent.putExtra("cardno", 2);
                startActivity(this.intent);
                return;
            case R.id.rl_add_1 /*2131296866*/:
                if (list.get(0).getCardList().size() < 3) {
                    this.intent = new Intent(this, AddMedicalCardActivity.class);
                    this.intent.putExtra("userno", 0);
                    startActivity(this.intent);
                    return;
                }
                Toast.makeText(this, getResources().getString(R.string.eedicare_card), 0).show();
                return;
            case R.id.rl_card_4 /*2131296870*/:
                this.intent = new Intent(this, ModifyMedicalCardActivity.class);
                this.intent.putExtra("userno", 1);
                this.intent.putExtra("cardno", 0);
                startActivity(this.intent);
                return;
            case R.id.rl_card_5 /*2131296874*/:
                this.intent = new Intent(this, ModifyMedicalCardActivity.class);
                this.intent.putExtra("userno", 1);
                this.intent.putExtra("cardno", 1);
                startActivity(this.intent);
                return;
            case R.id.rl_card_6 /*2131296878*/:
                this.intent = new Intent(this, ModifyMedicalCardActivity.class);
                this.intent.putExtra("userno", 1);
                this.intent.putExtra("cardno", 2);
                startActivity(this.intent);
                return;
            case R.id.rl_add_2 /*2131296882*/:
                if (list.get(1).getCardList().size() < 3) {
                    this.intent = new Intent(this, AddMedicalCardActivity.class);
                    this.intent.putExtra("userno", 1);
                    startActivity(this.intent);
                    return;
                }
                Toast.makeText(this, getResources().getString(R.string.eedicare_card), 0).show();
                return;
            case R.id.rl_card_7 /*2131296886*/:
                this.intent = new Intent(this, ModifyMedicalCardActivity.class);
                this.intent.putExtra("userno", 2);
                this.intent.putExtra("cardno", 0);
                startActivity(this.intent);
                return;
            case R.id.rl_card_8 /*2131296890*/:
                this.intent = new Intent(this, ModifyMedicalCardActivity.class);
                this.intent.putExtra("userno", 2);
                this.intent.putExtra("cardno", 1);
                startActivity(this.intent);
                return;
            case R.id.rl_card_9 /*2131296894*/:
                this.intent = new Intent(this, ModifyMedicalCardActivity.class);
                this.intent.putExtra("userno", 2);
                this.intent.putExtra("cardno", 2);
                startActivity(this.intent);
                return;
            case R.id.rl_add_3 /*2131296899*/:
                if (list.get(2).getCardList().size() < 3) {
                    this.intent = new Intent(this, AddMedicalCardActivity.class);
                    this.intent.putExtra("userno", 2);
                    startActivity(this.intent);
                    return;
                }
                Toast.makeText(this, getResources().getString(R.string.eedicare_card), 0).show();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (list != null) {
            list.clear();
            this.rl_user_1.setVisibility(8);
            this.rl_user_2.setVisibility(8);
            this.rl_user_3.setVisibility(8);
            sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.PATIENT_CARD_LIST), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.PATIENT_CARD_LIST)));
        }
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.PATIENT_CARD_LIST /*123*/:
                String userid = UserEntity.getInstance().getInfo().getAccId();
                parameters.add("patientid", PoiTypeDef.All);
                if (userid != null) {
                    parameters.add("userid", userid);
                    break;
                }
                break;
        }
        return parameters;
    }
}
