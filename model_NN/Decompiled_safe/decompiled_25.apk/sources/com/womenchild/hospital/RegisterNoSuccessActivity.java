package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.ClientLogUtil;
import com.womenchild.hospital.util.DateUtil;
import org.json.JSONException;
import org.json.JSONObject;

public class RegisterNoSuccessActivity extends BaseRequestActivity implements View.OnClickListener {
    public static String Hom = "CCLog";
    private Button btnRemind;
    private Button ibtnHome;
    private Intent intent;
    private ImageView iv_state;
    private String orderNum;
    private ProgressDialog pDialog;
    private int state;
    private TextView tvError;
    private TextView tvTitle;
    private TextView tv_clinic_room;
    private TextView tv_clinic_time;
    private TextView tv_notice_1;
    private TextView tv_order_no;
    private TextView tv_register_no_success;
    private TextView tv_register_no_success_doctor;
    private TextView tv_success_title;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.register_no_success);
        initViewId();
        initClickListener();
        initData();
    }

    public void refreshActivity(Object... params) {
        int intValue = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject result = (JSONObject) params[2];
            ClientLogUtil.i("RegisterNoSuccess", result.toString());
            if (result.optJSONObject("res").optInt("st") == 0) {
                String title = getResources().getString(R.string.ok_reservation);
                switch (result.optJSONObject("inf").optInt("orderstate")) {
                    case 0:
                        title = getResources().getString(R.string.ok_reservation);
                        this.iv_state.setBackgroundResource(R.drawable.ygkz_login_order_icon_cross);
                        break;
                    case 1:
                        title = getResources().getString(R.string.reservation_ok_data);
                        this.tvError.setText(getResources().getString(R.string.reservation_info));
                        this.iv_state.setBackgroundResource(R.drawable.ygkz_login_order_icon_cross);
                        break;
                    case 2:
                        title = getResources().getString(R.string.not_reservation);
                        this.tv_register_no_success.setText("您预约 ");
                        this.tv_register_no_success_doctor.setText(((Object) this.tv_register_no_success_doctor.getText()) + getResources().getString(R.string.reservation_info_doctor));
                        this.iv_state.setBackgroundResource(R.drawable.ygkz_login_order_icon_error);
                        this.tvError.setText(result.optJSONObject("res").optString("msg"));
                        break;
                }
                this.tvTitle.setText(title);
                this.tv_success_title.setText(title);
                this.iv_state.setVisibility(0);
            } else if (this.state == 0) {
                this.tvTitle.setText(getResources().getString(R.string.ok_reservation));
                this.tv_success_title.setText(getResources().getString(R.string.ok_reservation));
                this.iv_state.setBackgroundResource(R.drawable.ygkz_login_order_icon_cross);
                this.iv_state.setVisibility(0);
            } else {
                this.tvTitle.setText(getResources().getString(R.string.not_reservation));
                this.tv_success_title.setText(getResources().getString(R.string.not_reservation));
                this.tv_register_no_success.setText(getResources().getString(R.string.reservation_info_order));
                this.tv_register_no_success_doctor.setText(((Object) this.tv_register_no_success_doctor.getText()) + getResources().getString(R.string.reservation_info_doctor));
                this.iv_state.setBackgroundResource(R.drawable.ygkz_login_order_icon_error);
                this.iv_state.setVisibility(0);
            }
        } else if (this.state == 0) {
            this.tvTitle.setText(getResources().getString(R.string.ok_reservation));
            this.tv_success_title.setText(getResources().getString(R.string.ok_reservation));
            this.iv_state.setBackgroundResource(R.drawable.ygkz_login_order_icon_cross);
            this.iv_state.setVisibility(0);
        } else {
            this.tvTitle.setText(getResources().getString(R.string.not_reservation));
            this.tv_success_title.setText(getResources().getString(R.string.not_reservation));
            this.tv_register_no_success.setText(getResources().getString(R.string.reservation_info_order));
            this.tv_register_no_success_doctor.setText(((Object) this.tv_register_no_success_doctor.getText()) + getResources().getString(R.string.reservation_info_doctor));
            this.iv_state.setBackgroundResource(R.drawable.ygkz_login_order_icon_error);
            this.iv_state.setVisibility(0);
        }
        this.pDialog.dismiss();
    }

    public void initViewId() {
        this.ibtnHome = (Button) findViewById(R.id.ibtn_home);
        this.btnRemind = (Button) findViewById(R.id.btn_remind);
        this.tv_clinic_room = (TextView) findViewById(R.id.tv_clinic_room);
        this.tv_clinic_time = (TextView) findViewById(R.id.tv_clinic_time);
        this.tv_order_no = (TextView) findViewById(R.id.tv_order_no);
        this.tv_success_title = (TextView) findViewById(R.id.tv_success_title);
        this.iv_state = (ImageView) findViewById(R.id.iv_state);
        this.tv_register_no_success_doctor = (TextView) findViewById(R.id.tv_register_no_success_doctor);
        this.tvTitle = (TextView) findViewById(R.id.tv_title);
        this.tvError = (TextView) findViewById(R.id.tv_error);
        this.tv_notice_1 = (TextView) findViewById(R.id.tv_notice_1);
        this.tv_register_no_success = (TextView) findViewById(R.id.tv_register_no_success);
        this.pDialog = new ProgressDialog(this);
        this.pDialog.setMessage(getResources().getString(R.string.reservation_info_state));
        this.tv_order_no.setSelected(true);
    }

    public void initClickListener() {
        this.ibtnHome.setOnClickListener(this);
        this.btnRemind.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_home:
                ClientLogUtil.i(Hom, "ibtn_home");
                this.intent = new Intent(this, HomeActivity.class);
                this.intent.setFlags(67108864);
                startActivity(this.intent);
                finish();
                return;
            case R.id.btn_remind:
                this.intent = new Intent(this, RemindSetActivity.class);
                this.intent.putExtra("remindtype", 2);
                startActivity(this.intent);
                return;
            default:
                return;
        }
    }

    public void loadData(int requestType, Object data) {
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case 4:
                this.intent = new Intent(this, HomeActivity.class);
                this.intent.setFlags(67108864);
                startActivity(this.intent);
                finish();
                return true;
            default:
                return true;
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
        try {
            JSONObject json = new JSONObject(getIntent().getStringExtra("order"));
            this.tv_clinic_room.setText(json.optString("deptname"));
            this.tv_clinic_time.setText(String.valueOf(DateUtil.getTimeFromLong(json.optLong("planstarttime"))) + "-" + DateUtil.getTimeFromLong(json.optLong("planstoptime"), "kk:mm"));
            this.tv_order_no.setText(getIntent().getStringExtra("opcorderid"));
            this.tv_register_no_success_doctor.setText(String.valueOf(json.optString("doctorname")) + " 医师");
            this.tv_notice_1.setText(String.format(getResources().getString(R.string.tips_reach_address), json.optString("getorderaddress")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        parameters.add("ordernum", this.orderNum);
        parameters.add("state", Integer.valueOf(this.state));
        return parameters;
    }
}
