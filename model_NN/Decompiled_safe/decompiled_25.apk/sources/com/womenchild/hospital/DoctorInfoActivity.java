package com.womenchild.hospital;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.umeng.common.a;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.DateUtil;
import com.womenchild.hospital.view.DailyPlanView;
import org.json.JSONArray;
import org.json.JSONObject;

public class DoctorInfoActivity extends BaseRequestActivity implements View.OnClickListener {
    private DailyPlanView[] afternoonViews = new DailyPlanView[6];
    private Button commentDoctor;
    private int deptId;
    private String deptName;
    private String doctorDesc;
    private String doctorLevel;
    private String doctorName;
    private String doctorSex;
    private String doctorid;
    private boolean flag = false;
    private ImageView ivDoctorPhoto;
    private Button iv_back;
    private JSONArray jsons;
    private DailyPlanView[] morningViews = new DailyPlanView[6];
    private String photoStr;
    private TextView tvDocDesc;
    private TextView tvDoctorLevel;
    private TextView tvDoctorName;
    private TextView tvDoctorSex;
    private TextView tvTitle;
    private TextView[] tv_dates = new TextView[6];

    enum STATUS {
        STOP,
        FULL,
        AVAIL_MORNING,
        AVAIL_AFTERNOON,
        OTHER
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.doctor_info);
        initViewId();
        initClickListener();
        initData();
    }

    /* access modifiers changed from: protected */
    public void initViewId() {
        this.iv_back = (Button) findViewById(R.id.iv_back);
        this.commentDoctor = (Button) findViewById(R.id.btn_commentdoctor);
        this.tvTitle = (TextView) findViewById(R.id.tv_title);
        this.ivDoctorPhoto = (ImageView) findViewById(R.id.iv_doctor_photo);
        this.tvDoctorName = (TextView) findViewById(R.id.tv_doctor_name);
        this.tvDoctorSex = (TextView) findViewById(R.id.tv_doctor_sex);
        this.tvDoctorLevel = (TextView) findViewById(R.id.tv_doctor_level);
        this.tvDocDesc = (TextView) findViewById(R.id.tv_doc_desc);
        this.morningViews[0] = (DailyPlanView) findViewById(R.id.dailyPlanView1);
        this.morningViews[1] = (DailyPlanView) findViewById(R.id.dailyPlanView2);
        this.morningViews[2] = (DailyPlanView) findViewById(R.id.dailyPlanView3);
        this.morningViews[3] = (DailyPlanView) findViewById(R.id.dailyPlanView4);
        this.morningViews[4] = (DailyPlanView) findViewById(R.id.dailyPlanView5);
        this.morningViews[5] = (DailyPlanView) findViewById(R.id.dailyPlanView6);
        this.afternoonViews[0] = (DailyPlanView) findViewById(R.id.dailyPlanView11);
        this.afternoonViews[1] = (DailyPlanView) findViewById(R.id.dailyPlanView12);
        this.afternoonViews[2] = (DailyPlanView) findViewById(R.id.dailyPlanView13);
        this.afternoonViews[3] = (DailyPlanView) findViewById(R.id.dailyPlanView14);
        this.afternoonViews[4] = (DailyPlanView) findViewById(R.id.dailyPlanView15);
        this.afternoonViews[5] = (DailyPlanView) findViewById(R.id.dailyPlanView16);
        this.tv_dates[0] = (TextView) findViewById(R.id.tv_ar_data_1);
        this.tv_dates[1] = (TextView) findViewById(R.id.tv_ar_data_2);
        this.tv_dates[2] = (TextView) findViewById(R.id.tv_ar_data_3);
        this.tv_dates[3] = (TextView) findViewById(R.id.tv_ar_data_4);
        this.tv_dates[4] = (TextView) findViewById(R.id.tv_ar_data_5);
        this.tv_dates[5] = (TextView) findViewById(R.id.tv_ar_data_6);
        for (int i = 0; i < 6; i++) {
            this.morningViews[i].setContentVisible(4, 4);
            this.afternoonViews[i].setContentVisible(4, 4);
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.doctorid = getIntent().getStringExtra("doctorid");
        this.photoStr = getIntent().getStringExtra("photo");
        this.doctorDesc = getComment(getIntent().getStringExtra("desc"));
        this.deptName = getIntent().getStringExtra("deptName");
        this.deptId = getIntent().getIntExtra("deptid", 0);
        this.flag = getIntent().getBooleanExtra("flag", false);
        sendHttpRequest(307, initRequestParameter(307));
    }

    /* access modifiers changed from: protected */
    public void initClickListener() {
        this.iv_back.setOnClickListener(this);
        this.commentDoctor.setOnClickListener(this);
    }

    private class orderClickListener implements View.OnClickListener {
        private JSONObject jsonObject;
        private String type;

        public orderClickListener(JSONObject jsonObject2, String type2) {
            this.jsonObject = jsonObject2;
            this.type = type2;
        }

        public void onClick(View v) {
            Intent intent = new Intent(DoctorInfoActivity.this, SelectClinicTimeActivity.class);
            intent.putExtra("jsons", this.jsonObject.toString());
            intent.putExtra(a.b, this.type);
            DoctorInfoActivity.this.startActivity(intent);
        }
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        if (HomeActivity.loginFlag) {
            parameters.add("userid", UserEntity.getInstance().getInfo().getAccId());
        }
        parameters.add("doctorid", this.doctorid);
        return parameters;
    }

    public void loadData(int requestType, Object json) {
        JSONObject jsonSinglePerson = ((JSONArray) json).optJSONObject(0);
        this.doctorName = jsonSinglePerson.optString("doctorname");
        this.tvTitle.setText(this.doctorName);
        int gender = jsonSinglePerson.optInt("gender");
        if (gender == 1) {
            this.doctorSex = getResources().getString(R.string.man);
        } else if (gender == 0) {
            this.doctorSex = getResources().getString(R.string.woman);
        } else {
            this.doctorSex = getResources().getString(R.string.sex_unk);
        }
        this.doctorLevel = jsonSinglePerson.optString("doctorlevel");
        this.tvDoctorName.setText(this.doctorName);
        this.tvDoctorSex.setText(this.doctorSex);
        this.tvDoctorLevel.setText(this.doctorLevel);
        this.tvDocDesc.setText(this.doctorDesc);
        if (!(this.photoStr == null || this.photoStr.length() == 0)) {
            this.ivDoctorPhoto.setImageDrawable(new BitmapDrawable(stringtoBitmap(this.photoStr)));
            this.ivDoctorPhoto.setScaleType(ImageView.ScaleType.FIT_CENTER);
        }
        JSONArray daysplan = jsonSinglePerson.optJSONArray("daysplan");
        for (int i = 0; i < 6; i++) {
            JSONObject jsonObject = daysplan.optJSONObject(i);
            int morningNum = count(jsonObject.optJSONArray("planlist"), getResources().getString(R.string.morning));
            int afternoonNum = count(jsonObject.optJSONArray("planlist"), getResources().getString(R.string.afternoon));
            boolean isMorningStop = stop(jsonObject.optJSONArray("planlist"), getResources().getString(R.string.morning));
            boolean isAfternoonStop = stop(jsonObject.optJSONArray("planlist"), getResources().getString(R.string.afternoon));
            this.tv_dates[i].setText(DateUtil.replaceDate(jsonObject.optString("day")));
            if (isMorningStop) {
                setPlanView(this.morningViews[i], STATUS.STOP, 0, jsonObject);
            } else if (morningNum > 0) {
                setPlanView(this.morningViews[i], STATUS.AVAIL_MORNING, morningNum, jsonObject);
            } else if (morningNum == 0) {
                setPlanView(this.morningViews[i], STATUS.FULL, 0, jsonObject);
            } else {
                setPlanView(this.morningViews[i], STATUS.OTHER, 0, jsonObject);
            }
            if (isAfternoonStop) {
                setPlanView(this.afternoonViews[i], STATUS.STOP, 0, jsonObject);
            } else if (afternoonNum > 0) {
                setPlanView(this.afternoonViews[i], STATUS.AVAIL_AFTERNOON, afternoonNum, jsonObject);
            } else if (afternoonNum == 0) {
                setPlanView(this.afternoonViews[i], STATUS.FULL, 0, jsonObject);
            } else {
                setPlanView(this.afternoonViews[i], STATUS.OTHER, 0, jsonObject);
            }
        }
    }

    private void setPlanView(DailyPlanView view, STATUS status, int count, JSONObject jsonObject) {
        if (view != null) {
            if ((status == STATUS.AVAIL_AFTERNOON || status == STATUS.AVAIL_MORNING) && jsonObject == null) {
                view.setContentVisible(8, 8);
            } else if (status == STATUS.STOP) {
                view.setTextViewText(String.valueOf(getResources().getString(R.string.order_num)) + ":" + "0");
                view.setContentVisible(0, 0);
                view.setButtonDrawableBackground(getResources().getDrawable(R.drawable.ygkz_order_doctor_button_red));
                view.setButtonText(getResources().getString(R.string.stopping_cure));
                view.setClickable(false);
            } else if (status == STATUS.FULL) {
                view.setClickable(false);
                view.setTextViewText(getResources().getString(R.string.order_not));
                view.setButtonText(getResources().getString(R.string.full));
                view.setButtonDrawableBackground(getResources().getDrawable(R.drawable.ygkz_order_doctor_button_dark));
                view.setContentVisible(0, 0);
            } else if (status == STATUS.AVAIL_MORNING) {
                view.setTextViewText(String.valueOf(getResources().getString(R.string.order_num)) + count);
                view.setButtonText("查看");
                view.setClickable(true);
                view.setBtnClickListener(new orderClickListener(jsonObject, getResources().getString(R.string.morning)));
                view.setContentVisible(0, 0);
            } else if (status == STATUS.AVAIL_AFTERNOON) {
                view.setTextViewText(String.valueOf(getResources().getString(R.string.order_num)) + count);
                view.setButtonText("查看");
                view.setClickable(true);
                view.setBtnClickListener(new orderClickListener(jsonObject, getResources().getString(R.string.afternoon)));
                view.setContentVisible(0, 0);
            } else {
                view.setClickable(false);
                view.setContentVisible(4, 4);
            }
        }
    }

    private int count(JSONArray jsons2, String type) {
        int tmpCount = 0;
        boolean flag2 = false;
        for (int i = 0; i < jsons2.length(); i++) {
            if (type.equals(jsons2.optJSONObject(i).optString("timespan"))) {
                flag2 = true;
                tmpCount += jsons2.optJSONObject(i).optInt("availnum");
            }
        }
        if (flag2) {
            return tmpCount;
        }
        return -1;
    }

    private boolean stop(JSONArray jsons2, String type) {
        int i = 0;
        while (jsons2 != null && i < jsons2.length()) {
            if (type.equals(jsons2.optJSONObject(i).optString("timespan")) && jsons2.optJSONObject(i).optBoolean("isstop")) {
                return true;
            }
            i++;
        }
        return false;
    }

    public void refreshActivity(Object... params) {
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject result = (JSONObject) params[2];
            Log.i("result", result.toString());
            int resultCode = result.optJSONObject("res").optInt("st");
            String msg = result.optJSONObject("res").optString("msg");
            if (resultCode == 0) {
                switch (requestType) {
                    case 307:
                        this.jsons = result.optJSONObject("inf").optJSONArray("plans");
                        loadData(requestType, this.jsons);
                        return;
                    default:
                        return;
                }
            } else {
                Toast.makeText(this, msg, 0).show();
            }
        } else {
            Toast.makeText(this, getResources().getString(R.string.service_ex), 0).show();
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back /*2131296529*/:
                finish();
                return;
            case R.id.btn_commentdoctor /*2131296664*/:
                if (HomeActivity.loginFlag) {
                    Intent intent = new Intent(this, DoctorCommentActivity.class);
                    intent.putExtra("doctorid", this.doctorid);
                    intent.putExtra("deptName", this.deptName);
                    intent.putExtra("deptid", this.deptId);
                    intent.putExtra("doctorName", this.doctorName);
                    startActivity(intent);
                    return;
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("登录提示");
                builder.setMessage("使用此功能前请您先登录！若无帐号，请先注册");
                builder.setPositiveButton("免费注册", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        DoctorInfoActivity.this.startActivity(new Intent(DoctorInfoActivity.this, RegisterActivity.class));
                    }
                });
                builder.setNegativeButton("登录", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        DoctorInfoActivity.this.startActivity(new Intent(DoctorInfoActivity.this, LoginActivity.class));
                    }
                });
                builder.create().show();
                return;
            default:
                return;
        }
    }

    public Bitmap stringtoBitmap(String string) {
        try {
            byte[] bitmapArray = Base64.decode(string, 0);
            return BitmapFactory.decodeByteArray(bitmapArray, 0, bitmapArray.length);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private String getComment(String comment) {
        int end;
        StringBuffer sb = new StringBuffer(comment);
        while (true) {
            int start = sb.indexOf("<");
            if (start != -1 && (end = sb.indexOf(">")) != -1) {
                sb.delete(start, end + 1);
            }
        }
        return sb.toString().replace("&nbsp;", " ");
    }
}
