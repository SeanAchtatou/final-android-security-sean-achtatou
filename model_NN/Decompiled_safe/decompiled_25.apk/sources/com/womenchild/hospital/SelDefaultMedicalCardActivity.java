package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.womenchild.hospital.adapter.SelectDefaultPatientCardAdapter;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.entity.PatientCardEntity;
import com.womenchild.hospital.entity.PatientCardListEntity;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.ClientLogUtil;
import java.util.List;
import org.json.JSONObject;

public class SelDefaultMedicalCardActivity extends BaseRequestActivity implements View.OnClickListener {
    private static final String TAG = "MedicalCardManagementActivity";
    public static List<PatientCardListEntity> list;
    private SelectDefaultPatientCardAdapter adapter;
    private String cardId;
    private List<PatientCardListEntity> cardList;
    private Button ibtn_return;
    private Intent intent;
    private Context mContext = this;
    private ListView mListView;
    private List<PatientCardEntity> patientCardList;
    private String patientId;
    private String patientName;
    private ProgressDialog pd;
    private RelativeLayout rl_add_patient;
    private TextView submitTv;
    private String[] text = {"4", "2", "3"};
    private TextView userName;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.select_medical_card);
        this.patientId = getIntent().getStringExtra("patientId");
        this.patientName = getIntent().getStringExtra("patientName");
        initViewId();
        initClickListener();
        initData();
        this.pd.show();
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.PATIENT_CARD_LIST), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.PATIENT_CARD_LIST)));
    }

    public void refreshActivity(Object... params) {
        this.pd.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject result = (JSONObject) params[2];
            ClientLogUtil.i(TAG, result.toString());
            loadData(requestType, result);
            return;
        }
        Toast.makeText(this, getResources().getString(R.string.service_ex), 0).show();
    }

    public void initViewId() {
        this.ibtn_return = (Button) findViewById(R.id.ibtn_return);
        this.mListView = (ListView) findViewById(R.id.lv_patientList);
        this.submitTv = (TextView) findViewById(R.id.tv_submit);
        this.userName = (TextView) findViewById(R.id.tv_card_user_1);
        this.rl_add_patient = (RelativeLayout) findViewById(R.id.rl_add_patient);
    }

    public void initClickListener() {
        this.ibtn_return.setOnClickListener(this);
        this.submitTv.setOnClickListener(this);
        this.rl_add_patient.setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        this.pd.show();
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.PATIENT_CARD_LIST), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.PATIENT_CARD_LIST)));
    }

    public void loadData(int requestType, Object data) {
        JSONObject result = (JSONObject) data;
        String st = result.optJSONObject("res").optString("st");
        String msg = result.optJSONObject("res").optString("msg");
        switch (requestType) {
            case HttpRequestParameters.EDIT_DEFAULT_CARD /*119*/:
                if ("0".equals(st)) {
                    Toast.makeText(this, msg, 0).show();
                    Intent intent2 = new Intent();
                    String patientCard = this.patientCardList.get(this.adapter.getPositions()).getCard();
                    UserEntity.getInstance().getInfo().setDefaultPatientCard(patientCard);
                    intent2.putExtra("defaultPatientCard", patientCard);
                    setResult(-1, intent2);
                    finish();
                    return;
                }
                Toast.makeText(this, msg, 0).show();
                return;
            case HttpRequestParameters.PATIENT_CARD_LIST /*123*/:
                if ("0".equals(st)) {
                    this.cardList = PatientCardListEntity.getList(result);
                    if (this.cardList == null || this.cardList.size() <= 0) {
                        Toast.makeText(this, getResources().getString(R.string.consent_not), 0).show();
                        return;
                    }
                    this.patientCardList = this.cardList.get(0).getCardList();
                    this.adapter = new SelectDefaultPatientCardAdapter(this.mContext, this.patientCardList);
                    this.mListView.setAdapter((ListAdapter) this.adapter);
                    Log.i("aaaaa", "aaaa" + this.cardList.size());
                    if (this.patientCardList.size() >= 3) {
                        this.rl_add_patient.setVisibility(8);
                        Log.i("aaaaa", "3333");
                        return;
                    }
                    return;
                }
                Toast.makeText(this, msg, 0).show();
                return;
            default:
                return;
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_submit:
                if (this.patientCardList == null || this.adapter == null) {
                    finish();
                    return;
                } else if (this.adapter.getPositions() != -1) {
                    this.cardId = this.patientCardList.get(this.adapter.getPositions()).getCardid();
                    sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.EDIT_DEFAULT_CARD), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.EDIT_DEFAULT_CARD)));
                    if (this.pd == null) {
                        this.pd = new ProgressDialog(this);
                    }
                    this.pd.setMessage(getResources().getString(R.string.ok_submit_data));
                    return;
                } else {
                    finish();
                    return;
                }
            case R.id.ibtn_return:
                finish();
                return;
            case R.id.rl_add_patient:
                if (this.cardList.size() < 3) {
                    this.intent = new Intent(this, AddMedicalCardActivity.class);
                    this.intent.putExtra("patientid", this.patientId);
                    this.intent.putExtra("patientname", this.patientName);
                    startActivity(this.intent);
                    return;
                }
                Toast.makeText(this, getResources().getString(R.string.eedicare_card), 0).show();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.pd = new ProgressDialog(this);
        this.pd.setMessage(getResources().getString(R.string.inquiry));
        this.userName.setText(this.patientName);
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.EDIT_DEFAULT_CARD /*119*/:
                parameters.add("patientid", this.patientId);
                parameters.add("cardid", this.cardId);
                break;
            case HttpRequestParameters.PATIENT_CARD_LIST /*123*/:
                String userid = UserEntity.getInstance().getInfo().getUserid();
                parameters.add("patientid", this.patientId);
                if (userid != null) {
                    parameters.add("userid", userid);
                    break;
                }
                break;
        }
        return parameters;
    }
}
