package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.AppParameters;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.ClientLogUtil;
import org.json.JSONException;
import org.json.JSONObject;

public class CommitDoctor extends BaseRequestActivity implements View.OnClickListener {
    private static final String TAG = "CommitDoctor";
    private Button btn_cancle;
    private EditText et_commit_content;
    private ProgressDialog pd;
    private RatingBar rb_attitude;
    private RatingBar rb_condition_explanation;
    private RatingBar rb_curative_effect;
    private RatingBar rb_medical_skill;
    private RatingBar rb_whole;
    private TextView tv_submit;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.commit_doctor);
        initViewId();
        initClickListener();
        initData();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancle /*2131296444*/:
                finish();
                return;
            case R.id.tv_submit /*2131296445*/:
                this.pd.show();
                sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.ADD_COMPLAINT_INFO), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.ADD_COMPLAINT_INFO)));
                return;
            default:
                return;
        }
    }

    public void refreshActivity(Object... params) {
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject result = (JSONObject) params[2];
            ClientLogUtil.i(TAG, result.toString());
            switch (requestType) {
                case HttpRequestParameters.ADD_COMPLAINT_INFO /*124*/:
                    loadData(HttpRequestParameters.ADD_COMPLAINT_INFO, result);
                    return;
                default:
                    return;
            }
        }
    }

    public void initViewId() {
        this.btn_cancle = (Button) findViewById(R.id.btn_cancle);
        this.tv_submit = (TextView) findViewById(R.id.tv_submit);
        this.rb_whole = (RatingBar) findViewById(R.id.rb_whole);
        this.rb_curative_effect = (RatingBar) findViewById(R.id.rb_curative_effect);
        this.rb_attitude = (RatingBar) findViewById(R.id.rb_attitude);
        this.rb_condition_explanation = (RatingBar) findViewById(R.id.rb_condition_explanation);
        this.rb_medical_skill = (RatingBar) findViewById(R.id.rb_medical_skill);
        this.et_commit_content = (EditText) findViewById(R.id.et_commit_content);
    }

    public void initClickListener() {
        this.btn_cancle.setOnClickListener(this);
        this.tv_submit.setOnClickListener(this);
    }

    public void loadData(int requestType, Object data) {
        JSONObject result = (JSONObject) data;
        String st = result.optJSONObject("res").optString("st");
        String optString = result.optJSONObject("res").optString("msg");
        switch (requestType) {
            case HttpRequestParameters.ADD_COMPLAINT_INFO /*124*/:
                if ("0".equals(st)) {
                    Toast.makeText(this, getResources().getString(R.string.mark), 0).show();
                    setResult(1, getIntent());
                    finish();
                    break;
                }
                break;
        }
        this.pd.dismiss();
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.pd = new ProgressDialog(this);
        this.pd.setMessage(getResources().getString(R.string.mark_wait));
        this.pd.setCancelable(true);
        this.pd.setCanceledOnTouchOutside(false);
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.ADD_COMPLAINT_INFO /*124*/:
                try {
                    int rating = (int) this.rb_whole.getRating();
                    int effectScore = (int) this.rb_curative_effect.getRating();
                    int attitudeScore = (int) this.rb_attitude.getRating();
                    int explainScore = (int) this.rb_condition_explanation.getRating();
                    int skillScore = (int) this.rb_medical_skill.getRating();
                    String comment = this.et_commit_content.toString().trim();
                    parameters.add("appId", AppParameters.getAPPID());
                    parameters.add("order", ClinicDelActivity.entity.getOpcorderid());
                    parameters.add("name", ClinicDelActivity.person.getString(getResources().getString(R.string.user)));
                    parameters.add("mobile", ClinicDelActivity.person.getString(getResources().getString(R.string.user_phone)));
                    parameters.add("hospital", ClinicDelActivity.entity.getHospitalname());
                    parameters.add("doctor", ClinicDelActivity.entity.getDoctorname());
                    parameters.add("effectScore", Integer.valueOf(effectScore));
                    parameters.add("attitudeScore", Integer.valueOf(attitudeScore));
                    parameters.add("explainScore", Integer.valueOf(explainScore));
                    parameters.add("skillScore", Integer.valueOf(skillScore));
                    parameters.add("content", comment);
                    parameters.add("userid", UserEntity.getInstance().getInfo().getAccId());
                    break;
                } catch (JSONException e) {
                    e.printStackTrace();
                    break;
                }
        }
        return parameters;
    }
}
