package com.womenchild.hospital;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.amap.mapapi.location.LocationManagerProxy;
import com.womenchild.hospital.entity.RecordEntity;
import org.json.JSONObject;

public class PaymentDetailActivity extends Activity implements View.OnClickListener {
    public static RecordEntity entity;
    public static JSONObject person;
    private Button btn_back;
    private int position;
    private String realOrderNum;
    private TextView tv_name;
    private TextView tv_order_no;
    private TextView tv_order_status;
    private TextView tv_register_time;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.payment_detail);
        initViewId();
        initClickListener();
        initData();
    }

    private void initData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            int status = bundle.getInt(LocationManagerProxy.KEY_STATUS_CHANGED);
            this.tv_order_no.setText(bundle.getString("orderNum"));
            this.realOrderNum = bundle.getString("realOrderNum");
            this.tv_name.setText(String.valueOf(bundle.getDouble("price")) + "元");
            String payway = bundle.getString("payway");
            if (status == 1) {
                this.tv_order_status.setText(getResources().getString(R.string.ok_pay));
            } else if (status == 3) {
                this.tv_order_status.setText(getResources().getString(R.string.not_pay));
            }
            if ("sunpay".equals(payway)) {
                this.tv_register_time.setText("阳光账户支付");
            } else if ("alipay".equals(payway)) {
                this.tv_register_time.setText("支付宝快捷方式支付");
            } else {
                this.tv_register_time.setText("银联在线手机支付");
            }
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back /*2131296535*/:
                finish();
                return;
            default:
                return;
        }
    }

    public void initViewId() {
        this.btn_back = (Button) findViewById(R.id.btn_back);
        this.tv_order_no = (TextView) findViewById(R.id.tv_order_no);
        this.tv_name = (TextView) findViewById(R.id.tv_name);
        this.tv_order_status = (TextView) findViewById(R.id.tv_order_status);
        this.tv_register_time = (TextView) findViewById(R.id.tv_register_time);
    }

    public void initClickListener() {
        this.btn_back.setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    }
}
