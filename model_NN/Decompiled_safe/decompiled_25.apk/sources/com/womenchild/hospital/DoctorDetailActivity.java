package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.ClientLogUtil;
import com.womenchild.hospital.util.WebUtil;
import com.womenchild.hospital.view.SSLWebViewClient;
import org.json.JSONException;
import org.json.JSONObject;

public class DoctorDetailActivity extends BaseRequestActivity implements View.OnClickListener {
    private static final String TAG = "HospitalHelpContentActivity";
    private String doctorid;
    private ImageView iv_back;
    private ProgressDialog pDialog;
    private TextView tv_doctor_name;
    private TextView tv_hospital_name;
    private TextView tv_title;
    private WebView wv_desc_content;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.doctor_detail);
        initViewId();
        initClickListener();
        initData();
        this.pDialog = new ProgressDialog(this);
        this.pDialog.setMessage(getResources().getString(R.string.loading_data));
        this.pDialog.show();
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.DOCTOR_DETAIL), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.DOCTOR_DETAIL)));
    }

    public void refreshActivity(Object... params) {
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            loadData(requestType, (JSONObject) params[2]);
        } else {
            Toast.makeText(this, getResources().getString(R.string.network_connect_failed_prompt), 0).show();
        }
    }

    public void initViewId() {
        this.tv_doctor_name = (TextView) findViewById(R.id.tv_doctor_name);
        this.tv_hospital_name = (TextView) findViewById(R.id.tv_hospital_name);
        this.tv_title = (TextView) findViewById(R.id.tv_title);
        this.wv_desc_content = (WebView) findViewById(R.id.wv_desc_content);
        this.iv_back = (ImageView) findViewById(R.id.iv_back);
        this.wv_desc_content.setWebViewClient(new SSLWebViewClient());
    }

    public void initClickListener() {
        this.iv_back.setOnClickListener(this);
    }

    public void loadData(int requestType, Object data) {
        JSONObject doctor;
        this.pDialog.dismiss();
        JSONObject json = (JSONObject) data;
        JSONObject res = json.optJSONObject("res");
        if (res != null) {
            try {
                if (res.getInt("st") == 0 && (doctor = json.optJSONObject("inf").optJSONObject("doctor")) != null) {
                    this.tv_doctor_name.setText(doctor.optString("doctorname"));
                    this.tv_hospital_name.setText(doctor.optString("hospitalname"));
                    this.wv_desc_content.loadDataWithBaseURL(null, WebUtil.sourceCreateHtml(doctor.optString("desc")), "text/html", "utf-8", null);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                ClientLogUtil.i(TAG, e.getMessage());
            }
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back /*2131296529*/:
                finish();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.tv_title.setText(getResources().getString(R.string.doctor_info));
        this.doctorid = getIntent().getExtras().getString("doctorid");
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        parameters.add("doctorid", this.doctorid);
        return parameters;
    }
}
