package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.adapter.FindDoctorAdapter;
import com.womenchild.hospital.adapter.FindDoctorByRoomAdapter;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.configure.Constants;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.InputMsgUtil;
import org.json.JSONArray;
import org.json.JSONObject;

public class FindDoctorByRoomActivity extends BaseRequestActivity implements View.OnClickListener, AdapterView.OnItemClickListener, ExpandableListView.OnGroupClickListener, ExpandableListView.OnChildClickListener {
    private Button btn_search;
    private ExpandableListView elistView;
    private ListView elvfinddoctor;
    private int expandFlag = -1;
    private Button ibtnByCollection;
    private Button ibtnByRemain;
    private Button ibtnBySick;
    private Button ibtnHome;
    private Intent intent;
    private ImageView iv_search;
    private JSONArray jArray;
    private ProgressDialog pDialog;
    private LinearLayout rl_search;
    private String search;
    private boolean searchFlag = false;
    private EditText tv_search;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.find_doctor_by_room);
        initViewId();
        initClickListener();
        this.pDialog.show();
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.LISTDEPT_BY_HOSPITALID), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.LISTDEPT_BY_HOSPITALID)));
    }

    public void refreshActivity(Object... params) {
        this.pDialog.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject json = (JSONObject) params[2];
            if (json.optJSONObject("res").optInt("st") == 0) {
                loadData(requestType, json.optJSONObject("inf").optJSONArray("listobject"));
            } else {
                Toast.makeText(this, json.optJSONObject("res").optString("msg"), 0).show();
            }
        } else {
            Toast.makeText(this, getResources().getString(R.string.service_ex), 0).show();
        }
    }

    public void initViewId() {
        this.ibtnHome = (Button) findViewById(R.id.ibtn_home);
        this.iv_search = (ImageView) findViewById(R.id.iv_search);
        this.ibtnByRemain = (Button) findViewById(R.id.ibtn_by_remain);
        this.ibtnByCollection = (Button) findViewById(R.id.ibtn_by_collection);
        this.elistView = (ExpandableListView) findViewById(R.id.elv_find_doctor_by_rooms);
        this.rl_search = (LinearLayout) findViewById(R.id.rl_search);
        this.tv_search = (EditText) findViewById(R.id.tv_search);
        this.btn_search = (Button) findViewById(R.id.btn_search);
        this.elvfinddoctor = (ListView) findViewById(R.id.elv_find_doctor);
        this.pDialog = new ProgressDialog(this);
        this.pDialog.setMessage(getResources().getString(R.string.loading_data));
    }

    public void initClickListener() {
        this.ibtnHome.setOnClickListener(this);
        this.iv_search.setOnClickListener(this);
        this.ibtnByRemain.setOnClickListener(this);
        this.ibtnByCollection.setOnClickListener(this);
        this.rl_search.setOnClickListener(this);
        this.btn_search.setOnClickListener(this);
        this.elvfinddoctor.setOnItemClickListener(this);
        this.elistView.setOnGroupClickListener(this);
        this.elistView.setOnChildClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_home /*2131296435*/:
                finish();
                return;
            case R.id.iv_search /*2131296773*/:
                this.searchFlag = !this.searchFlag;
                if (this.searchFlag) {
                    this.elistView.setVisibility(8);
                    this.rl_search.setVisibility(0);
                    this.elvfinddoctor.setVisibility(0);
                    this.iv_search.setBackgroundResource(R.drawable.ygkz_search_bar_icon_search_click);
                    return;
                }
                this.rl_search.setVisibility(8);
                this.elistView.setVisibility(0);
                this.elvfinddoctor.setVisibility(8);
                this.iv_search.setBackgroundResource(R.drawable.ygkz_search_bar_icon_search);
                return;
            case R.id.btn_search /*2131296776*/:
                InputMsgUtil.hide(this.tv_search);
                search();
                return;
            case R.id.ibtn_by_remain /*2131296779*/:
                if (HomeActivity.loginFlag) {
                    this.intent = new Intent(this, SearchDoctorByOrderedActivity.class);
                    startActivity(this.intent);
                    return;
                }
                Toast.makeText(this, getString(R.string.login_function), 0).show();
                return;
            case R.id.ibtn_by_collection /*2131296780*/:
                if (HomeActivity.loginFlag) {
                    this.intent = new Intent(this, SearchDoctorByCollectedActivity.class);
                    startActivity(this.intent);
                    return;
                }
                Toast.makeText(this, getString(R.string.login_function), 0).show();
                return;
            default:
                return;
        }
    }

    public void loadData(int requestType, Object data) {
        this.jArray = (JSONArray) data;
        switch (requestType) {
            case HttpRequestParameters.LISTDEPT_BY_HOSPITALID /*208*/:
                FindDoctorByRoomAdapter fAdapter = new FindDoctorByRoomAdapter(this, this.jArray);
                this.elistView.setAdapter(fAdapter);
                if (fAdapter.getGroupCount() == 1) {
                    this.elistView.expandGroup(0);
                    return;
                }
                return;
            case HttpRequestParameters.LISTDEPT_BY_HOSPITAL_AND_DEPTNAME /*209*/:
            case HttpRequestParameters.LISTDOCTOR_BY_DEPTID /*210*/:
            default:
                return;
            case HttpRequestParameters.LISTDOCTOR_BY_DOCTORNAME /*211*/:
                this.elvfinddoctor.setAdapter((ListAdapter) new FindDoctorAdapter(this, this.jArray));
                return;
        }
    }

    public void search() {
        boolean flag = true;
        this.search = this.tv_search.getText().toString();
        if (this.search.equals(PoiTypeDef.All)) {
            Toast.makeText(this, getResources().getString(R.string.input_null), 0).show();
            flag = false;
            this.tv_search.findFocus();
        }
        if (flag) {
            this.pDialog.show();
            sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.LISTDOCTOR_BY_DOCTORNAME), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.LISTDOCTOR_BY_DOCTORNAME)));
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
        Intent intent2 = new Intent(this, FavDoctorActivity.class);
        intent2.putExtra("doctorID", this.jArray.optJSONObject(arg2).optString("doctorid"));
        startActivity(intent2);
    }

    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
        if (-1 == this.expandFlag) {
            parent.expandGroup(groupPosition);
            parent.setSelectedGroup(groupPosition);
            this.expandFlag = groupPosition;
            return true;
        } else if (this.expandFlag == groupPosition) {
            parent.collapseGroup(this.expandFlag);
            this.expandFlag = -1;
            return true;
        } else {
            parent.collapseGroup(this.expandFlag);
            parent.expandGroup(groupPosition);
            parent.setSelectedGroup(groupPosition);
            this.expandFlag = groupPosition;
            return true;
        }
    }

    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        JSONObject json = this.jArray.optJSONObject(groupPosition).optJSONArray("childs").optJSONObject(childPosition);
        Intent intent2 = new Intent(this, DeptInformationActivity.class);
        intent2.putExtra("deptName", json.optString("deptname"));
        intent2.putExtra("deptid", json.optInt("deptid"));
        startActivity(intent2);
        return true;
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.LISTDEPT_BY_HOSPITALID /*208*/:
                parameters.add("hospitalid", Constants.HOSPITALID);
                break;
            case HttpRequestParameters.LISTDOCTOR_BY_DOCTORNAME /*211*/:
                parameters.add("searchname", this.search);
                break;
        }
        return parameters;
    }
}
