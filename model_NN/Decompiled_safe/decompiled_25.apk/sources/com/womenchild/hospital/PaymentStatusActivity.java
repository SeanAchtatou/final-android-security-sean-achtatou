package com.womenchild.hospital;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.location.LocationManagerProxy;
import com.womenchild.hospital.util.ClientLogUtil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PaymentStatusActivity extends Activity implements View.OnClickListener {
    private Button ibtnHome;
    private ImageView ivState;
    private TextView tvPaymentInfo;
    private TextView tvSuccessTitle;
    private TextView tvTitle;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.payment_status);
        this.tvPaymentInfo = (TextView) findViewById(R.id.tv_payment_info);
        this.tvSuccessTitle = (TextView) findViewById(R.id.tv_success_title);
        this.tvTitle = (TextView) findViewById(R.id.tv_title);
        this.ivState = (ImageView) findViewById(R.id.iv_state);
        this.ibtnHome = (Button) findViewById(R.id.ibtn_home);
        this.ibtnHome.setOnClickListener(this);
        if (!getIntent().getBooleanExtra(LocationManagerProxy.KEY_STATUS_CHANGED, false)) {
            this.tvTitle.setText(getResources().getString(R.string.not_condition_fail));
            this.tvSuccessTitle.setText(getResources().getString(R.string.not_condition_fail));
            this.ivState.setBackgroundResource(R.drawable.ygkz_login_order_icon_error);
            this.tvPaymentInfo.setText(getResources().getString(R.string.condition_prompt));
            return;
        }
        this.tvTitle.setText(getResources().getString(R.string.ok_condition_fail));
        this.tvSuccessTitle.setText(getResources().getString(R.string.ok_condition_fail));
        this.ivState.setBackgroundResource(R.drawable.ygkz_login_order_icon_cross);
        StringBuffer stringBuffer = new StringBuffer();
        try {
            JSONObject jsonObject = new JSONObject(getIntent().getStringExtra("info"));
            ClientLogUtil.i("TEST", jsonObject.toString());
            JSONArray jsons = jsonObject.optJSONArray("guideInfo");
            for (int i = 0; i < jsons.length(); i++) {
                stringBuffer.append(String.valueOf(i + 1) + "." + jsons.optJSONObject(i).optString("Info"));
                stringBuffer.append("\n");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.tvPaymentInfo.setText(stringBuffer);
    }

    public void onClick(View arg0) {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.setFlags(67108864);
        startActivity(intent);
        finish();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return true;
        }
        Intent intent = new Intent(this, HomeActivity.class);
        intent.setFlags(67108864);
        startActivity(intent);
        return true;
    }
}
