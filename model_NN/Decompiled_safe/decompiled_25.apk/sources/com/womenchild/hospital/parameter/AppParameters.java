package com.womenchild.hospital.parameter;

import com.womenchild.hospital.util.ClientLogUtil;
import org.json.JSONException;
import org.json.JSONObject;

public class AppParameters {
    private static String ACCID = "fuerapp";
    private static String ACCPWD = "y275izs0";
    private static String APPID = "fezx";
    private static String TAG = "AppParameters";

    public static JSONObject appParameterToJson() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("appid", APPID);
            jsonObject.put("accid", ACCID);
            jsonObject.put("accpwd", ACCPWD);
            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
            ClientLogUtil.e(TAG, e.getMessage());
            return null;
        }
    }

    public static String getAPPID() {
        return APPID;
    }

    public static void setAPPID(String aPPID) {
        APPID = aPPID;
    }

    public static String getACCID() {
        return ACCID;
    }

    public static void setACCID(String aCCID) {
        ACCID = aCCID;
    }

    public static String getACCPWD() {
        return ACCPWD;
    }

    public static void setACCPWD(String aCCPWD) {
        ACCPWD = aCCPWD;
    }
}
