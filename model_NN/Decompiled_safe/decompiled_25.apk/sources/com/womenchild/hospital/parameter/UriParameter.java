package com.womenchild.hospital.parameter;

import android.net.Uri;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.umeng.common.util.e;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;

public class UriParameter {
    private ArrayList<String> mKeyList = new ArrayList<>();
    private ArrayList<String> mValueList = new ArrayList<>();

    public void add(String key, Object value) {
        if (isEmpty(key) && !isExist(key)) {
            this.mKeyList.add(key);
            this.mValueList.add(value == null ? PoiTypeDef.All : String.valueOf(value));
        }
    }

    public void remove(String key) {
        if (isExist(key)) {
            int index = this.mKeyList.indexOf(key);
            this.mKeyList.remove(index);
            this.mValueList.remove(index);
        }
    }

    public void remove(int index) {
        if (index >= 0 && index < this.mKeyList.size()) {
            this.mKeyList.remove(index);
            this.mValueList.remove(index);
        }
    }

    public int getLocation(String key) {
        if (this.mKeyList.contains(key)) {
            return this.mKeyList.indexOf(key);
        }
        return -1;
    }

    public String getKey(int location) {
        if (location < 0 || location >= this.mKeyList.size()) {
            return PoiTypeDef.All;
        }
        return this.mKeyList.get(location);
    }

    public String getValue(int location) {
        if (location < 0 || location >= this.mValueList.size()) {
            return PoiTypeDef.All;
        }
        return this.mValueList.get(location);
    }

    public String getValue(String key) {
        int index = getLocation(key);
        if (index != -1) {
            return this.mValueList.get(index);
        }
        return PoiTypeDef.All;
    }

    public void clear() {
        this.mKeyList.clear();
        this.mValueList.clear();
    }

    private boolean isEmpty(String s) {
        return s != null;
    }

    private boolean isExist(String s) {
        return this.mKeyList.contains(s);
    }

    public String createUri(String requestUri) {
        Uri.Builder uriBuilder = Uri.parse(requestUri).buildUpon();
        for (int i = 0; i < size(); i++) {
            uriBuilder.appendQueryParameter(this.mKeyList.get(i), this.mValueList.get(i));
        }
        return uriBuilder.build().toString();
    }

    public HttpEntity createEntity() {
        List<NameValuePair> params = new ArrayList<>();
        for (int i = 0; i < size(); i++) {
            params.add(new BasicNameValuePair(this.mKeyList.get(i), this.mValueList.get(i)));
        }
        try {
            return new UrlEncodedFormEntity(params, e.f);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public int size() {
        return this.mKeyList.size();
    }
}
