package com.womenchild.hospital;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import org.json.JSONObject;

public class DoctorCommentActivity extends BaseRequestActivity implements View.OnClickListener {
    private Button btn_submit;
    private String comment;
    private String deptName;
    private int deptid;
    private String doctorName;
    private String doctorid;
    private EditText et_comment;
    private Button iv_back;
    private RatingBar taidu_rating;
    private TextView tv_doctor_title;
    private RatingBar xiaoguo_rating;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.doctor_comment);
        initViewId();
        initClickListener();
        initData();
    }

    /* access modifiers changed from: protected */
    public void initViewId() {
        this.iv_back = (Button) findViewById(R.id.iv_back);
        this.btn_submit = (Button) findViewById(R.id.btn_submit);
        this.tv_doctor_title = (TextView) findViewById(R.id.tv_doctor_title);
        this.taidu_rating = (RatingBar) findViewById(R.id.taidu_rating);
        this.xiaoguo_rating = (RatingBar) findViewById(R.id.xiaoguo_rating);
        this.et_comment = (EditText) findViewById(R.id.et_comment);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.doctorid = getIntent().getStringExtra("doctorid");
        this.deptid = getIntent().getIntExtra("deptid", 0);
        this.deptName = getIntent().getStringExtra("deptName");
        this.doctorName = getIntent().getStringExtra("doctorName");
        this.tv_doctor_title.setText("您正在评价" + this.doctorName + "医生，您的评价将会帮助其他患者更好的选择医生");
    }

    /* access modifiers changed from: protected */
    public void initClickListener() {
        this.iv_back.setOnClickListener(this);
        this.btn_submit.setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.SUBMIT_COMMENT /*702*/:
                parameters.add("userid", UserEntity.getInstance().getInfo().getAccId());
                parameters.add("DoctorAttitude", Integer.valueOf(Float.valueOf(this.taidu_rating.getRating()).intValue()));
                parameters.add("CureEffect", Integer.valueOf(Float.valueOf(this.xiaoguo_rating.getRating()).intValue()));
                parameters.add("JudgeContent", this.comment);
                parameters.add("deptid", Integer.valueOf(this.deptid));
                parameters.add("deptname", this.deptName);
                parameters.add("BuzCode", "fezx-app");
                parameters.add("doctorid", this.doctorid);
                break;
        }
        return parameters;
    }

    /* access modifiers changed from: protected */
    public void loadData(int requestType, Object data) {
    }

    public void refreshActivity(Object... params) {
        int intValue = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject json = (JSONObject) params[2];
            if (json.optJSONObject("res").optInt("st") == 0) {
                Toast.makeText(this, "评论成功!", 0).show();
                finish();
                return;
            }
            Toast.makeText(this, json.optJSONObject("res").optString("msg"), 0).show();
            return;
        }
        Toast.makeText(this, "评价失败！请检查网络！", 0).show();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back /*2131296529*/:
                finish();
                return;
            case R.id.btn_submit /*2131296648*/:
                saveComment();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.btn_submit.setEnabled(true);
    }

    private void saveComment() {
        this.comment = this.et_comment.getText().toString();
        if (PoiTypeDef.All.equals(this.comment) || PoiTypeDef.All.equals(this.comment.trim())) {
            Toast.makeText(this, "请输入评价内容！", 0).show();
            return;
        }
        this.btn_submit.setEnabled(false);
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.SUBMIT_COMMENT), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.SUBMIT_COMMENT)));
    }
}
