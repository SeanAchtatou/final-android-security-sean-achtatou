package com.womenchild.hospital;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Xml;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.alipay.android.app.sdk.AliPay;
import com.amap.mapapi.location.LocationManagerProxy;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.amap.mapapi.route.Route;
import com.umeng.common.util.e;
import com.upomp.pay.Star_Upomp_Pay;
import com.upomp.pay.help.CreateOriginal;
import com.upomp.pay.help.Create_MerchantX;
import com.upomp.pay.help.GetValue;
import com.upomp.pay.help.Xmlpar;
import com.upomp.pay.httpservice.XmlHttpConnection;
import com.upomp.pay.info.Upomp_Pay_Info;
import com.upomp.pay.info.XmlDefinition;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.configure.Constants;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.AppParameters;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.ClientLogUtil;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;

public class RechargeActivity extends BaseRequestActivity implements View.OnClickListener {
    private static final int LIST_DIALOG = 0;
    private static final int RQF_PAY = 1;
    InputStream PrivateSign;
    private int currentIndex = 0;
    /* access modifiers changed from: private */
    public int currentMode = 0;
    XmlHttpConnection httpConnection;
    private Button ibtn_return;
    /* access modifiers changed from: private */
    public Dialog mDialog;
    /* access modifiers changed from: private */
    public MyHandler mMyHandler = new MyHandler(this, null);
    private int money;
    /* access modifiers changed from: private */
    public CharSequence[] num;
    private String opcorderid;
    private String orderId = null;
    private String patientName;
    private ProgressDialog progressDialog;
    private RelativeLayout rl_alipay_way;
    private RelativeLayout rl_select_1;
    private RelativeLayout rl_select_2;
    private RelativeLayout rl_select_3;
    private RelativeLayout rl_select_4;
    private RelativeLayout rl_yl_way;
    /* access modifiers changed from: private */
    public int selectPrice = 100;
    Star_Upomp_Pay star;
    /* access modifiers changed from: private */
    public TextView tv_account_other;
    private TextView tv_select_1;
    private TextView tv_select_2;
    private TextView tv_select_3;
    private TextView tv_select_4;
    GetValue values;
    Xmlpar xmlpar;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.recharge);
        initViewId();
        initClickListener();
        initData();
    }

    private void updateSelect() {
        switch (this.currentIndex) {
            case 0:
                this.selectPrice = 100;
                this.tv_select_1.setBackgroundResource(R.drawable.selected);
                this.tv_select_2.setBackgroundResource(R.drawable.unselect);
                this.tv_select_3.setBackgroundResource(R.drawable.unselect);
                this.tv_select_4.setBackgroundResource(R.drawable.unselect);
                return;
            case 1:
                this.selectPrice = HttpRequestParameters.PLAN_DETAIL;
                this.tv_select_1.setBackgroundResource(R.drawable.unselect);
                this.tv_select_2.setBackgroundResource(R.drawable.selected);
                this.tv_select_3.setBackgroundResource(R.drawable.unselect);
                this.tv_select_4.setBackgroundResource(R.drawable.unselect);
                return;
            case 2:
                this.selectPrice = 500;
                this.tv_select_1.setBackgroundResource(R.drawable.unselect);
                this.tv_select_2.setBackgroundResource(R.drawable.unselect);
                this.tv_select_3.setBackgroundResource(R.drawable.selected);
                this.tv_select_4.setBackgroundResource(R.drawable.unselect);
                return;
            case 3:
                this.tv_select_1.setBackgroundResource(R.drawable.unselect);
                this.tv_select_2.setBackgroundResource(R.drawable.unselect);
                this.tv_select_3.setBackgroundResource(R.drawable.unselect);
                this.tv_select_4.setBackgroundResource(R.drawable.selected);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void initViewId() {
        this.rl_select_1 = (RelativeLayout) findViewById(R.id.rl_select_1);
        this.rl_select_2 = (RelativeLayout) findViewById(R.id.rl_select_2);
        this.rl_select_3 = (RelativeLayout) findViewById(R.id.rl_select_3);
        this.rl_select_4 = (RelativeLayout) findViewById(R.id.rl_select_4);
        this.rl_yl_way = (RelativeLayout) findViewById(R.id.rl_yl_way);
        this.rl_alipay_way = (RelativeLayout) findViewById(R.id.rl_alipay_way);
        this.tv_select_1 = (TextView) findViewById(R.id.tv_select_1);
        this.tv_select_2 = (TextView) findViewById(R.id.tv_select_2);
        this.tv_select_3 = (TextView) findViewById(R.id.tv_select_3);
        this.tv_select_4 = (TextView) findViewById(R.id.tv_select_4);
        this.tv_account_other = (TextView) findViewById(R.id.tv_account_other);
        this.ibtn_return = (Button) findViewById(R.id.ibtn_return);
        this.ibtn_return.setOnClickListener(this);
        this.progressDialog = new ProgressDialog(this);
        this.progressDialog.setMessage("正在提交订单，请稍候...");
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.orderId = Create_MerchantX.createMerchantOrderId();
        this.money = getIntent().getIntExtra("money", this.money);
        this.currentMode = getIntent().getIntExtra("mode", 0);
        this.patientName = getIntent().getStringExtra("patientName");
        if (this.patientName == null || this.patientName.trim().equals(PoiTypeDef.All)) {
            this.patientName = UserEntity.getInstance().getUsername();
        }
        this.num = new CharSequence[]{"10", "20", "50", "100", "300", "500"};
        updateSelect();
    }

    /* access modifiers changed from: protected */
    public void initClickListener() {
        this.rl_select_1.setOnClickListener(this);
        this.rl_select_2.setOnClickListener(this);
        this.rl_select_3.setOnClickListener(this);
        this.rl_select_4.setOnClickListener(this);
        this.rl_yl_way.setOnClickListener(this);
        this.rl_alipay_way.setOnClickListener(this);
    }

    public Dialog onCreateDialog(int id, Bundle state) {
        switch (id) {
            case 0:
                AlertDialog.Builder b = new AlertDialog.Builder(this);
                b.setItems(this.num, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        RechargeActivity.this.selectPrice = Integer.parseInt(new StringBuilder().append((Object) RechargeActivity.this.num[which]).toString());
                        RechargeActivity.this.tv_account_other.setText(String.valueOf(RechargeActivity.this.selectPrice) + "元");
                    }
                });
                Dialog dialog = b.create();
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    public void onDismiss(DialogInterface dialog) {
                    }
                });
                return dialog;
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.SUBMIT_ORDER /*602*/:
                UriParameter parameter = FillInAppointmentInfoActivity.orderEntity.createParameters();
                parameter.add("ordercode", this.orderId);
                parameter.add("memberid", String.valueOf(AppParameters.getAPPID()) + "_" + UserEntity.getInstance().getInfo().getAccId());
                return parameter;
            case HttpRequestParameters.SUBMIT_PAY_ORDER /*604*/:
                UriParameter parameter2 = new UriParameter();
                parameter2.add("userid", UserEntity.getInstance().getInfo().getAccId());
                parameter2.add("ordernum", this.orderId);
                parameter2.add("ordertime", Create_MerchantX.createMerchantOrderTime());
                parameter2.add("paytime", Create_MerchantX.createMerchantOrderTime("yyyy-MM-dd kk:mm"));
                parameter2.add("orderamt", Integer.valueOf(this.selectPrice * 100));
                parameter2.add("name", this.patientName);
                return parameter2;
            case HttpRequestParameters.CHECK_YLRECORD /*611*/:
                UriParameter parameter3 = new UriParameter();
                parameter3.add("userid", UserEntity.getInstance().getInfo().getUserid());
                parameter3.add("price", Integer.valueOf(this.selectPrice * 100));
                return parameter3;
            case HttpRequestParameters.ALIPAY_APPLY_PAY /*613*/:
                UriParameter parameter4 = new UriParameter();
                parameter4.add("userid", UserEntity.getInstance().getInfo().getAccId());
                parameter4.add("ordernum", this.orderId);
                parameter4.add("price", Integer.valueOf(this.selectPrice * 100));
                parameter4.add("name", this.patientName);
                parameter4.add("hospitalid", Constants.HOSPITALID);
                return parameter4;
            case HttpRequestParameters.RECHARGE_TO_SUN /*615*/:
                UriParameter parameter5 = new UriParameter();
                parameter5.add("userid", UserEntity.getInstance().getInfo().getUserid());
                parameter5.add("price", Double.valueOf((double) this.selectPrice));
                parameter5.add("name", this.patientName);
                parameter5.add("hospitalid", Constants.HOSPITALID);
                parameter5.add("ordernum", this.orderId);
                return parameter5;
            case HttpRequestParameters.REQ_PAYBYSUN /*616*/:
                UriParameter parameter6 = new UriParameter();
                this.orderId = Create_MerchantX.createMerchantOrderId();
                parameter6.add("userid", UserEntity.getInstance().getInfo().getUserid());
                parameter6.add("hospitalid", Constants.HOSPITALID);
                parameter6.add("ordernum", this.orderId);
                parameter6.add("name", UserEntity.getInstance().getInfo().getName());
                parameter6.add("price", Double.valueOf(((double) this.money) / 100.0d));
                return parameter6;
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void loadData(int requestType, Object data) {
    }

    public void refreshActivity(Object... objects) {
        int requestType = ((Integer) objects[0]).intValue();
        boolean status = ((Boolean) objects[1]).booleanValue();
        JSONObject result = (JSONObject) objects[2];
        if (!status) {
            return;
        }
        if (result.optJSONObject("res").optInt("st") == 0) {
            switch (requestType) {
                case HttpRequestParameters.SUBMIT_ORDER /*602*/:
                    if (this.progressDialog.isShowing()) {
                        this.progressDialog.dismiss();
                    }
                    ClientLogUtil.i(getClass().getSimpleName(), result.optJSONObject("inf").toString());
                    this.opcorderid = result.optJSONObject("inf").optString("opcorderid");
                    Intent intent = new Intent(this, RegisterNoSuccessActivity.class);
                    intent.putExtra("ordernum", this.orderId);
                    intent.putExtra("opcorderid", this.opcorderid);
                    intent.putExtra("order", result.optJSONObject("inf").toString());
                    intent.putExtra(LocationManagerProxy.KEY_STATUS_CHANGED, 0);
                    startActivity(intent);
                    return;
                case HttpRequestParameters.SUBMIT_PAY_ORDER /*604*/:
                    verifyOrder(result.optJSONObject("inf"));
                    return;
                case HttpRequestParameters.CHECK_YLRECORD /*611*/:
                    this.orderId = result.optJSONObject("inf").optString("yloldordernum");
                    sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.RECHARGE_TO_SUN), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.RECHARGE_TO_SUN)));
                    return;
                case HttpRequestParameters.ALIPAY_APPLY_PAY /*613*/:
                    if (this.progressDialog.isShowing()) {
                        this.progressDialog.dismiss();
                    }
                    alipay(result.optJSONObject("res").optString("msg"));
                    return;
                case HttpRequestParameters.RECHARGE_TO_SUN /*615*/:
                    switch (this.currentMode) {
                        case 0:
                            Intent intent2 = new Intent(this, PersonalCenterActivity.class);
                            intent2.setFlags(67108864);
                            startActivity(intent2);
                            return;
                        case 1:
                            sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.REQ_PAYBYSUN), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.REQ_PAYBYSUN)));
                            return;
                        default:
                            return;
                    }
                case HttpRequestParameters.REQ_PAYBYSUN /*616*/:
                    if (this.progressDialog.isShowing()) {
                        this.progressDialog.dismiss();
                    }
                    sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.SUBMIT_ORDER), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.SUBMIT_ORDER)));
                    return;
                default:
                    return;
            }
        } else {
            switch (requestType) {
                case HttpRequestParameters.SUBMIT_ORDER /*602*/:
                    Toast.makeText(this, "预约失败，支付详情请查看支付记录", 1).show();
                    break;
                case HttpRequestParameters.SUBMIT_PAY_ORDER /*604*/:
                    Toast.makeText(this, "预约失败，请重试", 1).show();
                    break;
                case HttpRequestParameters.CHECK_YLRECORD /*611*/:
                    if (17 != result.optJSONObject("res").optInt("st")) {
                        Toast.makeText(getApplicationContext(), result.optJSONObject("res").optInt("msg"), 1).show();
                        break;
                    } else {
                        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.SUBMIT_PAY_ORDER), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.SUBMIT_PAY_ORDER)));
                        break;
                    }
                case HttpRequestParameters.ALIPAY_APPLY_PAY /*613*/:
                    if (this.progressDialog.isShowing()) {
                        this.progressDialog.dismiss();
                    }
                    Toast.makeText(this, "支付失败！" + result.optJSONObject("res").optString("msg"), 1).show();
                    break;
            }
            if (this.progressDialog.isShowing()) {
                this.progressDialog.dismiss();
            }
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_return:
                finish();
                break;
            case R.id.rl_select_1:
                this.currentIndex = 0;
                break;
            case R.id.rl_select_2:
                this.currentIndex = 1;
                break;
            case R.id.rl_select_3:
                this.currentIndex = 2;
                break;
            case R.id.rl_select_4:
                this.currentIndex = 3;
                showDialog(0);
                break;
            case R.id.rl_yl_way:
                if (this.selectPrice == 0) {
                    Toast.makeText(this, "请选择充值金额", 0).show();
                    break;
                } else {
                    this.progressDialog.show();
                    sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.CHECK_YLRECORD), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.CHECK_YLRECORD)));
                    break;
                }
            case R.id.rl_alipay_way:
                if (this.selectPrice == 0) {
                    Toast.makeText(this, "请选择充值金额", 0).show();
                    break;
                } else {
                    this.progressDialog.show();
                    sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.ALIPAY_APPLY_PAY), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.ALIPAY_APPLY_PAY)));
                    break;
                }
        }
        updateSelect();
    }

    private void alipay(final String orderInfo) {
        new Thread() {
            public void run() {
                String result = new AliPay(RechargeActivity.this, RechargeActivity.this.mMyHandler).pay(orderInfo);
                Message msg = new Message();
                msg.what = 1;
                msg.obj = result;
                RechargeActivity.this.mMyHandler.sendMessage(msg);
            }
        }.start();
    }

    private void verifyOrder(JSONObject json) {
        this.values = new GetValue();
        this.xmlpar = new Xmlpar();
        try {
            Upomp_Pay_Info.merchantId = json.optString("merchantid");
            Upomp_Pay_Info.merchantOrderId = json.optString("merchantorderid");
            Upomp_Pay_Info.merchantOrderTime = json.optString("merchantordertime");
            Upomp_Pay_Info.originalsign = CreateOriginal.CreateOriginal_Sign(3);
            ClientLogUtil.d(Upomp_Pay_Info.tag, "这是订单验证的3位原串===\n" + Upomp_Pay_Info.originalsign);
            try {
                this.PrivateSign = getFromAssets("898000000000002.p12");
            } catch (FileNotFoundException e) {
                ClientLogUtil.d(Upomp_Pay_Info.tag, "Exception is " + e);
            }
            Upomp_Pay_Info.xmlSign = json.optString("sign");
            ClientLogUtil.d(Upomp_Pay_Info.tag, "这是订单验证的3位签名===\n" + Upomp_Pay_Info.xmlSign);
            String LanchPay = XmlDefinition.ReturnXml(Upomp_Pay_Info.xmlSign, 3);
            ClientLogUtil.d(Upomp_Pay_Info.tag, "这是订单验证报文===\n" + LanchPay);
            this.star = new Star_Upomp_Pay();
            this.star.start_upomp_pay(this, LanchPay);
        } catch (Exception e2) {
            ClientLogUtil.d(Upomp_Pay_Info.tag, "**Exception is " + e2);
        }
    }

    public InputStream getFromAssets(String fileName) throws FileNotFoundException {
        try {
            this.PrivateSign = getResources().getAssets().open(fileName);
        } catch (Exception e) {
            ClientLogUtil.d(Upomp_Pay_Info.tag, "Exception is " + e);
        }
        return this.PrivateSign;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            byte[] xml = data.getExtras().getByteArray("xml");
            String respCode = null;
            String respDesc = null;
            try {
                XmlPullParser parser = Xml.newPullParser();
                parser.setInput(new ByteArrayInputStream(xml), e.f);
                for (int eventType = parser.getEventType(); eventType != 1; eventType = parser.next()) {
                    switch (eventType) {
                        case 2:
                            String tag = parser.getName();
                            if (!tag.equalsIgnoreCase("respCode")) {
                                if (!tag.equalsIgnoreCase("respDesc")) {
                                    break;
                                } else {
                                    respDesc = parser.nextText();
                                    break;
                                }
                            } else {
                                respCode = parser.nextText();
                                break;
                            }
                    }
                }
                ClientLogUtil.d(Upomp_Pay_Info.tag, "解析中:" + respCode + "->" + respDesc);
                if (respCode != null && "0000".equals(respCode)) {
                    if (!this.progressDialog.isShowing()) {
                        this.progressDialog.setMessage(PoiTypeDef.All);
                        this.progressDialog.show();
                    }
                    sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.RECHARGE_TO_SUN), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.RECHARGE_TO_SUN)));
                } else if (this.currentMode != 0) {
                    MyHandler myHandler = this.mMyHandler;
                    this.mMyHandler.getClass();
                    this.mMyHandler.sendMessage(myHandler.obtainMessage(11));
                }
            } catch (Exception e) {
            }
        }
    }

    private class MyHandler extends Handler {
        public final int SHOWDIALOGPAYFAIL;

        private MyHandler() {
            this.SHOWDIALOGPAYFAIL = 11;
        }

        /* synthetic */ MyHandler(RechargeActivity rechargeActivity, MyHandler myHandler) {
            this();
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    Result.sResult = (String) msg.obj;
                    if (!"9000".equals(Result.getResult())) {
                        if (RechargeActivity.this.currentMode != 0) {
                            MyHandler access$3 = RechargeActivity.this.mMyHandler;
                            RechargeActivity.this.mMyHandler.getClass();
                            RechargeActivity.this.mMyHandler.sendMessage(access$3.obtainMessage(11));
                            break;
                        }
                    } else {
                        RechargeActivity.this.sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.RECHARGE_TO_SUN), RechargeActivity.this.initRequestParameter(Integer.valueOf((int) HttpRequestParameters.RECHARGE_TO_SUN)));
                        break;
                    }
                    break;
                case Route.DrivingSaveMoney:
                    RechargeActivity.this.showDialogPayFail();
                    break;
            }
            super.handleMessage(msg);
        }
    }

    /* access modifiers changed from: private */
    public void showDialogPayFail() {
        this.mDialog = new AlertDialog.Builder(this).setTitle("挂号失败").setMessage("挂号失败，请选择重试或退出本次预约").setPositiveButton("退出预约", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                RechargeActivity.this.mDialog.dismiss();
                Intent intent = new Intent(RechargeActivity.this, HomeActivity.class);
                intent.setFlags(67108864);
                RechargeActivity.this.startActivity(intent);
                RechargeActivity.this.finish();
            }
        }).setNegativeButton("重试", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                RechargeActivity.this.mDialog.dismiss();
            }
        }).create();
        this.mDialog.show();
    }
}
