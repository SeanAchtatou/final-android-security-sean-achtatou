package com.womenchild.hospital;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.entity.RecordEntity;
import com.womenchild.hospital.parameter.UriParameter;
import org.json.JSONException;
import org.json.JSONObject;

public class ClinicDelActivity extends BaseRequestActivity implements View.OnClickListener {
    public static RecordEntity entity;
    public static JSONObject person;
    private Button btn_back;
    private ImageView iv_status;
    private int position;
    private TextView tv_appoint_doctor;
    private TextView tv_clinic_time;
    private TextView tv_doctor;
    private TextView tv_name;
    private TextView tv_order_no;
    private TextView tv_order_status;
    private TextView tv_register_time;
    private TextView tv_room;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.clinic_detail);
        initViewId();
        initClickListener();
        initData();
    }

    /* access modifiers changed from: protected */
    public void initData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            try {
                this.position = bundle.getInt("position");
                entity = NotClinicFragment.clinicList.get(this.position);
                person = new JSONObject(entity.getPerson());
                String clinicTime = entity.getOpctime();
                String registerTime = entity.getPaytime();
                this.tv_order_no.setText(entity.getOpcorderid());
                this.tv_name.setText(person.getString(getResources().getString(R.string.usr_women)));
                this.tv_room.setText(entity.getDeptname());
                this.tv_doctor.setText(entity.getDoctorname());
                if (PoiTypeDef.All.equals(clinicTime)) {
                    this.tv_clinic_time.setText("--");
                } else {
                    this.tv_clinic_time.setText(clinicTime);
                }
                if (PoiTypeDef.All.equals(registerTime)) {
                    this.tv_register_time.setText("--");
                } else {
                    this.tv_clinic_time.setText(registerTime);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back /*2131296535*/:
                finish();
                return;
            case R.id.tv_appoint_doctor /*2131296536*/:
                Intent intent = new Intent(this, FavDoctorActivity.class);
                intent.putExtra("doctorID", entity.getDoctorid());
                startActivity(intent);
                return;
            case R.id.tv_order_status_title /*2131296548*/:
                Intent intent2 = new Intent(this, CommitDoctor.class);
                intent2.putExtra("position", this.position);
                startActivityForResult(intent2, 0);
                return;
            default:
                return;
        }
    }

    public void refreshActivity(Object... params) {
    }

    public void initViewId() {
        this.btn_back = (Button) findViewById(R.id.btn_back);
        this.tv_order_no = (TextView) findViewById(R.id.tv_order_no);
        this.tv_name = (TextView) findViewById(R.id.tv_name);
        this.tv_room = (TextView) findViewById(R.id.tv_room);
        this.tv_doctor = (TextView) findViewById(R.id.tv_doctor);
        this.tv_clinic_time = (TextView) findViewById(R.id.tv_clinic_time);
        this.tv_register_time = (TextView) findViewById(R.id.tv_register_time);
        this.tv_order_status = (TextView) findViewById(R.id.tv_order_status_title);
        this.tv_appoint_doctor = (TextView) findViewById(R.id.tv_appoint_doctor);
        this.iv_status = (ImageView) findViewById(R.id.iv_status);
    }

    public void initClickListener() {
        this.btn_back.setOnClickListener(this);
        this.tv_appoint_doctor.setOnClickListener(this);
        this.tv_order_status.setOnClickListener(this);
    }

    public void loadData(int requestType, Object data) {
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode) {
        }
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        return null;
    }
}
