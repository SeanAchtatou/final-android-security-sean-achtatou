package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.mapapi.location.LocationManagerProxy;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.upomp.pay.help.Create_MerchantX;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.entity.RecordEntity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import org.json.JSONException;
import org.json.JSONObject;

public class NoClinicDelActivity extends BaseRequestActivity implements View.OnClickListener {
    private Button btn_back;
    private boolean cancelStatus = false;
    private int fee;
    private ProgressDialog pDialog;
    private TextView tvInfo;
    private TextView tv_clinic_time;
    private TextView tv_doctor;
    private TextView tv_name;
    private TextView tv_order_no;
    private TextView tv_order_status;
    private TextView tv_register_no;
    private TextView tv_register_time;
    private TextView tv_room;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.no_clinic_detail);
        initViewId();
        initClickListener();
        initData();
    }

    private void setCancelStatus(String status) {
        if (getResources().getString(R.string.order_ok).equals(status) || getResources().getString(R.string.wait_info).equals(status)) {
            this.cancelStatus = true;
        } else {
            this.cancelStatus = false;
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back /*2131296535*/:
                finish();
                return;
            case R.id.tv_register_no /*2131296921*/:
                if (this.cancelStatus) {
                    this.pDialog.show();
                    sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.CANCELORDER_AND_BACKMONEY), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.CANCELORDER_AND_BACKMONEY)));
                    return;
                }
                Intent intent = new Intent(this, QueueCallActivity.class);
                intent.putExtra("index", 1);
                startActivity(intent);
                return;
            default:
                return;
        }
    }

    public void refreshActivity(Object... params) {
        this.pDialog.dismiss();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject result = (JSONObject) params[2];
            if (!"99".equals(result.optJSONObject("res").opt("st"))) {
                this.tv_order_status.setText(result.optJSONObject("inf").optString(LocationManagerProxy.KEY_STATUS_CHANGED));
                this.tvInfo.setText(getResources().getString(R.string.order_cancel));
                this.tv_register_no.setVisibility(8);
                Intent intent = new Intent(this, MyRegisterNoRecordActivity.class);
                intent.setFlags(67108864);
                startActivity(intent);
            }
            Toast.makeText(this, result.optJSONObject("res").optString("msg"), 0).show();
            return;
        }
        Toast.makeText(this, (int) R.string.network_connect_failed_prompt, 0).show();
    }

    public void initViewId() {
        this.btn_back = (Button) findViewById(R.id.btn_back);
        this.tv_order_no = (TextView) findViewById(R.id.tv_order_no);
        this.tv_name = (TextView) findViewById(R.id.tv_name);
        this.tv_room = (TextView) findViewById(R.id.tv_room);
        this.tv_doctor = (TextView) findViewById(R.id.tv_doctor);
        this.tv_clinic_time = (TextView) findViewById(R.id.tv_clinic_time);
        this.tv_register_time = (TextView) findViewById(R.id.tv_register_time);
        this.tv_order_status = (TextView) findViewById(R.id.tv_order_status);
        this.tv_register_no = (TextView) findViewById(R.id.tv_register_no);
        this.pDialog = new ProgressDialog(this);
        this.pDialog.setMessage(getResources().getString(R.string.indent_cancel));
        this.tvInfo = (TextView) findViewById(R.id.tv_info);
    }

    public void initClickListener() {
        this.btn_back.setOnClickListener(this);
        this.tv_register_no.setOnClickListener(this);
    }

    public void loadData(int requestType, Object data) {
    }

    /* access modifiers changed from: protected */
    public void initData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            try {
                RecordEntity entity = NotClinicFragment.noClinicList.get(bundle.getInt("position"));
                setCancelStatus(entity.getStatus());
                JSONObject person = new JSONObject(entity.getPerson());
                String clinicTime = entity.getOpctime();
                String registerTime = entity.getPaytime();
                this.fee = entity.getFee();
                if (this.cancelStatus) {
                    this.tvInfo.setText(getResources().getString(R.string.order_cancel_t));
                    this.tv_register_no.setText(getResources().getString(R.string.order_cancel_y));
                } else if (getResources().getString(R.string.f4indent_baroness).equals(entity.getStatus())) {
                    this.tv_register_no.setText(getResources().getString(R.string.f5see_indent_baroness));
                } else {
                    this.tvInfo.setText(getResources().getString(R.string.order_cancel));
                    this.tv_register_no.setVisibility(8);
                }
                this.tv_order_no.setText(entity.getOpcorderid());
                this.tv_name.setText(person.getString(getResources().getString(R.string.usr_women)));
                this.tv_room.setText(entity.getDeptname());
                this.tv_doctor.setText(entity.getDoctorname());
                this.tv_order_status.setText(entity.getStatus());
                if (PoiTypeDef.All.equals(clinicTime)) {
                    this.tv_clinic_time.setText("--");
                } else {
                    this.tv_clinic_time.setText(clinicTime);
                }
                if (PoiTypeDef.All.equals(registerTime)) {
                    this.tv_register_time.setText("--");
                } else {
                    this.tv_clinic_time.setText(registerTime);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        parameters.add("ordernum", Create_MerchantX.createMerchantOrderId());
        parameters.add("orderamt", Integer.valueOf(this.fee));
        parameters.add("ordertime", Create_MerchantX.createMerchantOrderTime());
        parameters.add("orderid", this.tv_order_no.getText().toString());
        return parameters;
    }
}
