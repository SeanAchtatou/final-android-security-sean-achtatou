package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.mapapi.location.LocationManagerProxy;
import com.upomp.pay.help.Create_MerchantX;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.configure.Constants;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.AppParameters;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.ClientLogUtil;
import java.text.SimpleDateFormat;
import org.json.JSONException;
import org.json.JSONObject;

public class ConfirmAndPayActivity extends BaseRequestActivity implements View.OnClickListener {
    private Button btn_pay_now;
    private Button ibtn_return;
    private JSONObject json;
    private int money;
    private String opcorderid;
    private String orderId = null;
    private int pay_type = 0;
    private ProgressDialog progressDialog;
    private SimpleDateFormat sDformat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    private TextView tv_account_blance;
    private TextView tv_account_name;
    private TextView tv_dept;
    private TextView tv_doctor;
    private TextView tv_mobile;
    private TextView tv_money;
    private TextView tv_patient;
    private TextView tv_time;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.confirm_and_pay);
        initViewId();
        initClickListener();
        initData();
    }

    /* access modifiers changed from: protected */
    public void initViewId() {
        this.tv_patient = (TextView) findViewById(R.id.tv_patient);
        this.tv_doctor = (TextView) findViewById(R.id.tv_doctor);
        this.tv_dept = (TextView) findViewById(R.id.tv_dept);
        this.tv_time = (TextView) findViewById(R.id.tv_time);
        this.tv_mobile = (TextView) findViewById(R.id.tv_mobile);
        this.tv_money = (TextView) findViewById(R.id.tv_money);
        this.tv_account_name = (TextView) findViewById(R.id.tv_account_name);
        this.tv_account_blance = (TextView) findViewById(R.id.tv_account_blance);
        this.btn_pay_now = (Button) findViewById(R.id.btn_pay_now);
        this.btn_pay_now.setVisibility(8);
        this.btn_pay_now.setOnClickListener(this);
        this.ibtn_return = (Button) findViewById(R.id.ibtn_return);
        this.ibtn_return.setOnClickListener(this);
        this.progressDialog = new ProgressDialog(this);
        this.progressDialog.setMessage("正在提交订单，请稍候...");
    }

    /* access modifiers changed from: protected */
    public void initData() {
        try {
            JSONObject plan = new JSONObject(getIntent().getStringExtra("plan"));
            Log.i("plan", plan.toString());
            this.money = plan.optInt("fee");
            this.json = new JSONObject(getIntent().getStringExtra("order"));
            Log.i("order", this.json.toString());
            this.tv_dept.setText(this.json.optString("deptname"));
            this.tv_doctor.setText(this.json.optString("doctorname"));
            this.tv_mobile.setText(this.json.optString("mobile"));
            this.tv_money.setText(String.valueOf(((double) this.money) / 100.0d) + "元");
            this.tv_patient.setText(this.json.optString("name"));
            this.tv_account_name.setText(UserEntity.getInstance().getUsername());
            Long startTime = Long.valueOf(this.json.optLong("planstarttime"));
            if (startTime != null) {
                Time time = new Time();
                time.set(startTime.longValue());
                this.tv_time.setText(this.sDformat.format(Long.valueOf(time.toMillis(true))));
            }
            sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.CHECK_BLANCE_FOR_SUN), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.CHECK_BLANCE_FOR_SUN)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void initClickListener() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.SUBMIT_ORDER /*602*/:
                UriParameter parameter = FillInAppointmentInfoActivity.orderEntity.createParameters();
                parameter.add("ordercode", this.orderId);
                parameter.add("memberid", String.valueOf(AppParameters.getAPPID()) + "_" + UserEntity.getInstance().getInfo().getAccId());
                return parameter;
            case HttpRequestParameters.CHECK_BLANCE_FOR_SUN /*614*/:
                UriParameter parameter2 = new UriParameter();
                parameter2.add("userid", UserEntity.getInstance().getInfo().getUserid());
                parameter2.add("hospitalid", Constants.HOSPITALID);
                return parameter2;
            case HttpRequestParameters.REQ_PAYBYSUN /*616*/:
                UriParameter parameter3 = new UriParameter();
                this.orderId = Create_MerchantX.createMerchantOrderId();
                parameter3.add("userid", UserEntity.getInstance().getInfo().getUserid());
                parameter3.add("hospitalid", Constants.HOSPITALID);
                parameter3.add("ordernum", this.orderId);
                parameter3.add("name", UserEntity.getInstance().getInfo().getName());
                parameter3.add("price", Double.valueOf(((double) this.money) / 100.0d));
                return parameter3;
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void loadData(int requestType, Object data) {
    }

    public void refreshActivity(Object... objects) {
        int requestType = ((Integer) objects[0]).intValue();
        boolean status = ((Boolean) objects[1]).booleanValue();
        JSONObject result = (JSONObject) objects[2];
        if (!status) {
            switch (requestType) {
                case HttpRequestParameters.SUBMIT_ORDER /*602*/:
                    Toast.makeText(this, "预约失败，支付详情请查看支付记录", 1).show();
                    return;
                case HttpRequestParameters.CHECK_BLANCE_FOR_SUN /*614*/:
                    Toast.makeText(getApplicationContext(), "网络连接异常或服务器无法连接，请重试", 1).show();
                    return;
                case HttpRequestParameters.REQ_PAYBYSUN /*616*/:
                    Toast.makeText(this, "网络连接异常或服务器无法连接，请重试", 0).show();
                    return;
                default:
                    return;
            }
        } else if (result.optJSONObject("res").optInt("st") == 0) {
            switch (requestType) {
                case HttpRequestParameters.SUBMIT_ORDER /*602*/:
                    if (this.progressDialog.isShowing()) {
                        this.progressDialog.dismiss();
                    }
                    ClientLogUtil.i(getClass().getSimpleName(), result.optJSONObject("inf").toString());
                    this.opcorderid = result.optJSONObject("inf").optString("opcorderid");
                    Intent intent = new Intent(this, RegisterNoSuccessActivity.class);
                    intent.putExtra("ordernum", this.orderId);
                    intent.putExtra("opcorderid", this.opcorderid);
                    intent.putExtra("order", result.optJSONObject("inf").toString());
                    intent.putExtra(LocationManagerProxy.KEY_STATUS_CHANGED, 0);
                    startActivity(intent);
                    return;
                case HttpRequestParameters.CHECK_BLANCE_FOR_SUN /*614*/:
                    String data = result.optJSONObject("res").optString("msg");
                    this.tv_account_blance.setText(String.valueOf(data) + "元");
                    this.btn_pay_now.setVisibility(0);
                    if (Double.valueOf(Double.parseDouble(data)).doubleValue() < ((double) this.money) / 100.0d) {
                        this.tv_account_blance.setText(String.valueOf(data) + "元");
                        this.btn_pay_now.setText("充值并支付挂号费");
                        this.pay_type = 1;
                        return;
                    }
                    this.btn_pay_now.setText("支付挂号费");
                    this.pay_type = 0;
                    return;
                case HttpRequestParameters.REQ_PAYBYSUN /*616*/:
                    if (this.progressDialog.isShowing()) {
                        this.progressDialog.dismiss();
                    }
                    sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.SUBMIT_ORDER), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.SUBMIT_ORDER)));
                    return;
                default:
                    return;
            }
        } else {
            switch (requestType) {
                case HttpRequestParameters.SUBMIT_ORDER /*602*/:
                    Toast.makeText(this, "预约失败，支付详情请查看支付记录", 1).show();
                    return;
                case HttpRequestParameters.CHECK_BLANCE_FOR_SUN /*614*/:
                    Toast.makeText(this, "查询余额失败", 0).show();
                    return;
                case HttpRequestParameters.REQ_PAYBYSUN /*616*/:
                    Toast.makeText(this, "阳光账户支付失败", 0).show();
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        this.btn_pay_now.setEnabled(true);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_return /*2131296570*/:
                finish();
                return;
            case R.id.btn_pay_now /*2131296589*/:
                this.btn_pay_now.setEnabled(false);
                if (this.pay_type == 0) {
                    this.progressDialog.show();
                    sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.REQ_PAYBYSUN), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.REQ_PAYBYSUN)));
                    return;
                }
                this.progressDialog.show();
                Intent intent = new Intent(this, RechargeActivity.class);
                intent.putExtra("money", this.money);
                intent.putExtra("patientName", this.json.optString("name"));
                intent.putExtra("mode", 1);
                startActivity(intent);
                return;
            default:
                return;
        }
    }
}
