package com.womenchild.hospital.service.download;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import com.womenchild.hospital.R;
import java.io.File;
import java.util.Date;

public class UpdateService extends Service {
    private static final int DOWNLOAD_COMPLETE = 0;
    private static final int DOWNLOAD_FAIL = 1;
    static String key = "e0c27f07-337debd9-bcf972a4-03d8b5e8";
    static long serial = new Date().getTime();
    static String site = "9tian";
    private int downloadUrls = 0;
    private int titleId = 0;
    /* access modifiers changed from: private */
    public File updateDir = null;
    /* access modifiers changed from: private */
    public File updateFile = null;
    /* access modifiers changed from: private */
    public Handler updateHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    UpdateService.this.updateNotification.flags |= 16;
                    Uri uri = Uri.fromFile(UpdateService.this.updateFile);
                    Intent installIntent = new Intent("android.intent.action.VIEW");
                    installIntent.setDataAndType(uri, "application/vnd.android.package-archive");
                    UpdateService.this.updatePendingIntent = PendingIntent.getActivity(UpdateService.this, 0, installIntent, 0);
                    UpdateService.this.updateNotification.defaults = 1;
                    UpdateService.this.updateNotification.setLatestEventInfo(UpdateService.this, "医院APP", "下载完成,点击安装。", UpdateService.this.updatePendingIntent);
                    UpdateService.this.updateNotificationManager.notify(0, UpdateService.this.updateNotification);
                    UpdateService.this.stopSelf();
                case 1:
                    UpdateService.this.updateNotification.setLatestEventInfo(UpdateService.this, "医院APP", "下载完成,点击安装。", UpdateService.this.updatePendingIntent);
                    UpdateService.this.updateNotificationManager.notify(0, UpdateService.this.updateNotification);
                    break;
            }
            UpdateService.this.stopSelf();
        }
    };
    private Intent updateIntent = null;
    /* access modifiers changed from: private */
    public Notification updateNotification = null;
    /* access modifiers changed from: private */
    public NotificationManager updateNotificationManager = null;
    /* access modifiers changed from: private */
    public PendingIntent updatePendingIntent = null;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        this.titleId = intent.getIntExtra("titleId", 0);
        if ("mounted".equals(Environment.getExternalStorageState())) {
            this.updateDir = new File(Environment.getExternalStorageDirectory(), "app/download/");
            this.updateFile = new File(this.updateDir.getPath(), String.valueOf(getResources().getString(this.titleId)) + ".apk");
        }
        this.updateNotificationManager = (NotificationManager) getSystemService("notification");
        this.updateNotification = new Notification();
        this.updateNotification.icon = R.drawable.ic_launcher;
        this.updateNotification.tickerText = "开始下载";
        this.updateNotification.setLatestEventInfo(this, "医院APP", "0%", this.updatePendingIntent);
        this.updateNotificationManager.notify(0, this.updateNotification);
        new Thread(new updateRunnable()).start();
        return super.onStartCommand(intent, flags, startId);
    }

    class updateRunnable implements Runnable {
        Message message;

        updateRunnable() {
            this.message = UpdateService.this.updateHandler.obtainMessage();
        }

        public void run() {
            this.message.what = 0;
            try {
                if (!UpdateService.this.updateDir.exists()) {
                    UpdateService.this.updateDir.mkdirs();
                }
                if (!UpdateService.this.updateFile.exists()) {
                    UpdateService.this.updateFile.createNewFile();
                }
                if (UpdateService.this.downloadUpdateFile("http://www.9twan.com/api/download?id=86007&site=9tian&imei=&mac=&sign=0c5cfd4503ae25dfb1bc053914e53dfa", UpdateService.this.updateFile) > 0) {
                    UpdateService.this.updateHandler.sendMessage(this.message);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                this.message.what = 1;
                UpdateService.this.updateHandler.sendMessage(this.message);
            }
        }
    }

    /* JADX WARN: Type inference failed for: r17v2, types: [java.net.URLConnection] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0075  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x007a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long downloadUpdateFile(java.lang.String r22, java.io.File r23) throws java.lang.Exception {
        /*
            r21 = this;
            r7 = 0
            r6 = 0
            r13 = 0
            r15 = 0
            r10 = 0
            r11 = 0
            r8 = 0
            java.net.URL r16 = new java.net.URL     // Catch:{ all -> 0x006d }
            r0 = r16
            r1 = r22
            r0.<init>(r1)     // Catch:{ all -> 0x006d }
            java.net.URLConnection r17 = r16.openConnection()     // Catch:{ all -> 0x006d }
            r0 = r17
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ all -> 0x006d }
            r10 = r0
            java.lang.String r17 = "User-Agent"
            java.lang.String r18 = "PacificHttpClient"
            r0 = r17
            r1 = r18
            r10.setRequestProperty(r0, r1)     // Catch:{ all -> 0x006d }
            if (r6 <= 0) goto L_0x0047
            java.lang.String r17 = "RANGE"
            java.lang.StringBuilder r18 = new java.lang.StringBuilder     // Catch:{ all -> 0x006d }
            java.lang.String r19 = "bytes="
            r18.<init>(r19)     // Catch:{ all -> 0x006d }
            r0 = r18
            java.lang.StringBuilder r18 = r0.append(r6)     // Catch:{ all -> 0x006d }
            java.lang.String r19 = "-"
            java.lang.StringBuilder r18 = r18.append(r19)     // Catch:{ all -> 0x006d }
            java.lang.String r18 = r18.toString()     // Catch:{ all -> 0x006d }
            r0 = r17
            r1 = r18
            r10.setRequestProperty(r0, r1)     // Catch:{ all -> 0x006d }
        L_0x0047:
            r17 = 10000(0x2710, float:1.4013E-41)
            r0 = r17
            r10.setConnectTimeout(r0)     // Catch:{ all -> 0x006d }
            r17 = 20000(0x4e20, float:2.8026E-41)
            r0 = r17
            r10.setReadTimeout(r0)     // Catch:{ all -> 0x006d }
            int r15 = r10.getContentLength()     // Catch:{ all -> 0x006d }
            int r17 = r10.getResponseCode()     // Catch:{ all -> 0x006d }
            r18 = 404(0x194, float:5.66E-43)
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x007e
            java.lang.Exception r17 = new java.lang.Exception     // Catch:{ all -> 0x006d }
            java.lang.String r18 = "fail!"
            r17.<init>(r18)     // Catch:{ all -> 0x006d }
            throw r17     // Catch:{ all -> 0x006d }
        L_0x006d:
            r17 = move-exception
        L_0x006e:
            if (r10 == 0) goto L_0x0073
            r10.disconnect()
        L_0x0073:
            if (r11 == 0) goto L_0x0078
            r11.close()
        L_0x0078:
            if (r8 == 0) goto L_0x007d
            r8.close()
        L_0x007d:
            throw r17
        L_0x007e:
            java.io.InputStream r11 = r10.getInputStream()     // Catch:{ all -> 0x006d }
            java.io.FileOutputStream r9 = new java.io.FileOutputStream     // Catch:{ all -> 0x006d }
            r17 = 0
            r0 = r23
            r1 = r17
            r9.<init>(r0, r1)     // Catch:{ all -> 0x006d }
            r17 = 4096(0x1000, float:5.74E-42)
            r0 = r17
            byte[] r5 = new byte[r0]     // Catch:{ all -> 0x0115 }
            r12 = 0
        L_0x0094:
            int r12 = r11.read(r5)     // Catch:{ all -> 0x0115 }
            if (r12 > 0) goto L_0x00aa
            if (r10 == 0) goto L_0x009f
            r10.disconnect()
        L_0x009f:
            if (r11 == 0) goto L_0x00a4
            r11.close()
        L_0x00a4:
            if (r9 == 0) goto L_0x00a9
            r9.close()
        L_0x00a9:
            return r13
        L_0x00aa:
            r17 = 0
            r0 = r17
            r9.write(r5, r0, r12)     // Catch:{ all -> 0x0115 }
            long r0 = (long) r12     // Catch:{ all -> 0x0115 }
            r17 = r0
            long r13 = r13 + r17
            if (r7 == 0) goto L_0x00cc
            r17 = 100
            long r17 = r17 * r13
            long r0 = (long) r15     // Catch:{ all -> 0x0115 }
            r19 = r0
            long r17 = r17 / r19
            r0 = r17
            int r0 = (int) r0     // Catch:{ all -> 0x0115 }
            r17 = r0
            int r17 = r17 + -10
            r0 = r17
            if (r0 <= r7) goto L_0x0094
        L_0x00cc:
            int r7 = r7 + 10
            r0 = r21
            android.app.Notification r0 = r0.updateNotification     // Catch:{ all -> 0x0115 }
            r17 = r0
            java.lang.String r18 = "正在下载"
            java.lang.StringBuilder r19 = new java.lang.StringBuilder     // Catch:{ all -> 0x0115 }
            int r0 = (int) r13     // Catch:{ all -> 0x0115 }
            r20 = r0
            int r20 = r20 * 100
            int r20 = r20 / r15
            java.lang.String r20 = java.lang.String.valueOf(r20)     // Catch:{ all -> 0x0115 }
            r19.<init>(r20)     // Catch:{ all -> 0x0115 }
            java.lang.String r20 = "%"
            java.lang.StringBuilder r19 = r19.append(r20)     // Catch:{ all -> 0x0115 }
            java.lang.String r19 = r19.toString()     // Catch:{ all -> 0x0115 }
            r0 = r21
            android.app.PendingIntent r0 = r0.updatePendingIntent     // Catch:{ all -> 0x0115 }
            r20 = r0
            r0 = r17
            r1 = r21
            r2 = r18
            r3 = r19
            r4 = r20
            r0.setLatestEventInfo(r1, r2, r3, r4)     // Catch:{ all -> 0x0115 }
            r0 = r21
            android.app.NotificationManager r0 = r0.updateNotificationManager     // Catch:{ all -> 0x0115 }
            r17 = r0
            r18 = 0
            r0 = r21
            android.app.Notification r0 = r0.updateNotification     // Catch:{ all -> 0x0115 }
            r19 = r0
            r17.notify(r18, r19)     // Catch:{ all -> 0x0115 }
            goto L_0x0094
        L_0x0115:
            r17 = move-exception
            r8 = r9
            goto L_0x006e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.womenchild.hospital.service.download.UpdateService.downloadUpdateFile(java.lang.String, java.io.File):long");
    }
}
