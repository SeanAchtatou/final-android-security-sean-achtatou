package com.womenchild.hospital.service;

import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import com.womenchild.hospital.base.BaseRequestService;
import com.womenchild.hospital.configure.Constants;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.ClientLogUtil;

public class RemindService extends BaseRequestService {
    private static final String TAG = "RemindService";
    private final int PERIODIC_EVENT_TIMEOUT = 180000;
    /* access modifiers changed from: private */
    public Runnable doPeriodicTask = new Runnable() {
        public void run() {
            ClientLogUtil.i(RemindService.TAG, "doPeriodicTask run()");
            RemindService.this.sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.PATIENT_CARD_INFO), RemindService.this.initRequestParameter(UserEntity.getInstance().getInfo().getUserid()));
            RemindService.this.remindEventHandler.postDelayed(RemindService.this.doPeriodicTask, 180000);
        }
    };
    /* access modifiers changed from: private */
    public Handler remindEventHandler;

    public IBinder onBind(Intent intent) {
        ClientLogUtil.i(TAG, "onBind");
        return null;
    }

    public void onCreate() {
        ClientLogUtil.i(TAG, "onCreate");
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.PATIENT_CARD_INFO), initRequestParameter(UserEntity.getInstance().getInfo().getUserid()));
        this.remindEventHandler = new Handler();
        this.remindEventHandler.postDelayed(this.doPeriodicTask, 180000);
        super.onCreate();
    }

    public void onDestroy() {
        ClientLogUtil.i(TAG, "onDestroy");
        this.remindEventHandler.removeCallbacks(this.doPeriodicTask);
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        parameters.add("userid", requestCode);
        parameters.add("hospitalid", Constants.HOSPITAL_ID);
        return parameters;
    }

    public void refreshService(Object... objects) {
    }
}
