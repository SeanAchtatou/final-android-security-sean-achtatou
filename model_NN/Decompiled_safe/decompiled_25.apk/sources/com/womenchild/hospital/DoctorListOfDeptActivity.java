package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.adapter.DoctorListAdatper;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.entity.DocPlanByTimeContainer;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class DoctorListOfDeptActivity extends BaseRequestActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    private static String TAG = "SelectDoctorActivity";
    public static String doctorName;
    public static ProgressDialog pDialog;
    final String INITIALIZED = "initializedDialog";
    private Button btn_dept_info;
    private ArrayList<DocPlanByTimeContainer> containerArrayList;
    public int deptId;
    public String deptName;
    private int groupNum = 1;
    private Button ivBack;
    private JSONArray jsonArray;
    private ListView lvDoctorPlan;
    private Context mContext = this;
    private JSONObject planJson;
    private int sortNum = 2;
    Toast toast;
    private TextView tv_title;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.doctor_list);
        initViewId();
        initClickListener();
        initData();
    }

    public void refreshActivity(Object... params) {
        pDialog.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject result = (JSONObject) params[2];
            int resultCode = result.optJSONObject("res").optInt("st");
            String optString = result.optJSONObject("res").optString("msg");
            if (resultCode == 0) {
                switch (requestType) {
                    case HttpRequestParameters.LIST_DOCTOR_BY_DEPTID /*703*/:
                        loadData(requestType, result.optJSONObject("inf"));
                        return;
                    default:
                        return;
                }
            } else {
                Toast.makeText(this, (int) R.string.network_connect_failed_prompt, 0).show();
            }
        }
    }

    public void initViewId() {
        this.ivBack = (Button) findViewById(R.id.iv_back);
        this.lvDoctorPlan = (ListView) findViewById(R.id.lv_doctor_list);
        pDialog = new ProgressDialog(this);
        pDialog.setMessage(getResources().getString(R.string.loading));
        this.tv_title = (TextView) findViewById(R.id.tv_title);
        this.btn_dept_info = (Button) findViewById(R.id.btn_dept_info);
    }

    public void initClickListener() {
        this.ivBack.setOnClickListener(this);
        this.btn_dept_info.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back /*2131296529*/:
                finish();
                return;
            case R.id.btn_dept_info /*2131296690*/:
                Intent intent = new Intent(this, DeptInformationActivity.class);
                intent.putExtra("deptName", this.deptName);
                intent.putExtra("deptid", this.deptId);
                startActivity(intent);
                return;
            default:
                return;
        }
    }

    public void loadData(int requestType, Object data) {
        JSONObject json = (JSONObject) data;
        switch (requestType) {
            case HttpRequestParameters.LIST_DOCTOR_BY_DEPTID /*703*/:
                this.jsonArray = json.optJSONArray("listobject");
                this.lvDoctorPlan.setAdapter((ListAdapter) new DoctorListAdatper(this, this.jsonArray));
                this.lvDoctorPlan.setOnItemClickListener(this);
                return;
            default:
                return;
        }
    }

    public ArrayList<DocPlanByTimeContainer> splitList(int index) {
        ArrayList<DocPlanByTimeContainer> containerFirst = new ArrayList<>();
        switch (index) {
            case 0:
                for (int i = 0; i < 6; i++) {
                    containerFirst.add(this.containerArrayList.get(i));
                }
                break;
            case 1:
                for (int i2 = 6; i2 < 12; i2++) {
                    containerFirst.add(this.containerArrayList.get(i2));
                }
                break;
            case 2:
                for (int i3 = 12; i3 < 18; i3++) {
                    containerFirst.add(this.containerArrayList.get(i3));
                }
                break;
            case 3:
                for (int i4 = 18; i4 < 24; i4++) {
                    containerFirst.add(this.containerArrayList.get(i4));
                }
                break;
            case 4:
                for (int i5 = 24; i5 < 28; i5++) {
                    containerFirst.add(this.containerArrayList.get(i5));
                }
                break;
        }
        return containerFirst;
    }

    private int countAvailNum(ArrayList<JSONObject> record) {
        int tmpCount = 0;
        for (int i = 0; i < record.size(); i++) {
            int availNum = record.get(i).optInt("availnum");
            if (availNum == -1) {
                return -1;
            }
            tmpCount += availNum;
        }
        return tmpCount;
    }

    public String formatDate(int num) {
        if (num < 10) {
            return "0" + num;
        }
        return new StringBuilder().append(num).toString();
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.deptId = getIntent().getIntExtra("deptid", 0);
        this.deptName = getIntent().getStringExtra("deptName");
        this.tv_title.setText(this.deptName);
        pDialog.show();
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.LIST_DOCTOR_BY_DEPTID), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.LIST_DOCTOR_BY_DEPTID)));
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.LISTPLAN_BY_DEPTID /*304*/:
                parameters.add("userid", PoiTypeDef.All);
                parameters.add("deptid", Integer.valueOf(this.deptId));
                parameters.add("sortnum", Integer.valueOf(this.sortNum));
                parameters.add("groupnum", Integer.valueOf(this.groupNum));
                break;
            case HttpRequestParameters.LIST_DOCTOR_BY_DEPTID /*703*/:
                parameters.add("deptid", Integer.valueOf(this.deptId));
                break;
        }
        return parameters;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long lid) {
        JSONObject json = this.jsonArray.optJSONObject(position);
        String optString = json.optString("desc");
        Intent intent = new Intent(this.mContext, DoctorInfoActivity.class);
        intent.putExtra("doctorid", json.optString("doctorid"));
        intent.putExtra("desc", json.optString("desc"));
        intent.putExtra("photo", json.optString("photo"));
        intent.putExtra("deptName", this.deptName);
        intent.putExtra("deptid", this.deptId);
        startActivity(intent);
    }
}
