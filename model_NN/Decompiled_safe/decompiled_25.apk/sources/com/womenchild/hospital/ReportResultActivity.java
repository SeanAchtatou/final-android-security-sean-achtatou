package com.womenchild.hospital;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.womenchild.hospital.adapter.TestReportResultAdapter;
import com.womenchild.hospital.entity.TestPrjEntity;
import com.womenchild.hospital.entity.TestPrjSubEntity;
import java.util.ArrayList;

public class ReportResultActivity extends Activity implements View.OnClickListener {
    private static final String TAG = "ReportResultActivity";
    private Button addressBtn;
    private View headerView;
    private ImageView iv_back;
    private ListView lvPatient;
    TestPrjEntity mTestPrjEntity;
    private ProgressDialog pDialog;
    ArrayList<TestPrjSubEntity> testPrjSubEntityList;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.report_result);
        initViewId();
        initClickListener();
        this.mTestPrjEntity = (TestPrjEntity) getIntent().getExtras().getSerializable("entity");
        this.testPrjSubEntityList = this.mTestPrjEntity.getSubList();
        this.lvPatient.setAdapter((ListAdapter) new TestReportResultAdapter(this, this.lvPatient, this.testPrjSubEntityList));
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void initViewId() {
        this.iv_back = (ImageView) findViewById(R.id.iv_back);
        this.lvPatient = (ListView) findViewById(R.id.lv_select_test_prj);
        this.addressBtn = (Button) findViewById(R.id.btn_address);
        this.lvPatient.setSelector(new ColorDrawable(0));
        this.headerView = ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.city_selct_list_header, (ViewGroup) null);
        this.lvPatient.addFooterView(this.headerView);
    }

    public void initClickListener() {
        this.iv_back.setOnClickListener(this);
        this.addressBtn.setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onClick(View v) {
        if (this.iv_back == v) {
            finish();
        } else if (this.addressBtn == v) {
            startActivity(new Intent(this, TakeReportAddressActivity.class));
        }
    }
}
