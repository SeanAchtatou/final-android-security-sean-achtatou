package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.Toast;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.WebUtil;
import com.womenchild.hospital.view.SSLWebViewClient;
import org.json.JSONException;
import org.json.JSONObject;

public class OverDueHelpActivity extends BaseRequestActivity implements View.OnClickListener {
    private static final String TAG = "OverDueHelpActivity";
    private int index = 5;
    private ImageView iv_back;
    private ProgressDialog pDialog;
    private WebView wvhprooms;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.over_due_help);
        this.index = getIntent().getIntExtra("index", 5);
        initViewId();
        initClickListener();
        this.pDialog = new ProgressDialog(this);
        this.pDialog.setMessage(getResources().getString(R.string.loading_data));
        this.pDialog.show();
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.HOSPITAL_TAFFIC_INFO), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.HOSPITAL_TAFFIC_INFO)));
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void refreshActivity(Object... params) {
        this.pDialog.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            loadData(requestType, (JSONObject) params[2]);
        } else {
            Toast.makeText(this, (int) R.string.network_connect_failed_prompt, 0).show();
        }
    }

    public void initViewId() {
        this.iv_back = (ImageView) findViewById(R.id.iv_back);
        this.wvhprooms = (WebView) findViewById(R.id.wv_hp_rooms);
        this.wvhprooms.setWebViewClient(new SSLWebViewClient());
    }

    public void initClickListener() {
        this.iv_back.setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onClick(View v) {
        if (this.iv_back == v) {
            finish();
        }
    }

    public void loadData(int requestType, Object data) {
        JSONObject inf;
        JSONObject jsonObject = (JSONObject) data;
        JSONObject res = jsonObject.optJSONObject("res");
        if (res != null) {
            try {
                if (res.getInt("st") == 0 && (inf = jsonObject.optJSONObject("inf")) != null) {
                    this.wvhprooms.loadDataWithBaseURL(null, WebUtil.sourceCreateHtml(inf.optString("noticecontent")), "text/html", "utf-8", null);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        parameters.add("num", Integer.valueOf(this.index));
        return parameters;
    }
}
