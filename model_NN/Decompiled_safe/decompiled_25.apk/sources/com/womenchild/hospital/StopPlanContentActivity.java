package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.WebUtil;
import com.womenchild.hospital.view.SSLWebViewClient;
import org.json.JSONException;
import org.json.JSONObject;

public class StopPlanContentActivity extends BaseRequestActivity implements View.OnClickListener {
    private static final String TAG = "HospitalHelpContentActivity";
    private Button iv_back;
    private ProgressDialog pDialog;
    private TextView tv_greetings;
    private TextView tv_title;
    private WebView wv_content;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.stop_plan);
        initViewId();
        initClickListener();
        initData();
        this.pDialog = new ProgressDialog(this);
        this.pDialog.setMessage(getResources().getString(R.string.loading_ok));
    }

    public void refreshActivity(Object... params) {
    }

    public void initViewId() {
        this.wv_content = (WebView) findViewById(R.id.wv_content);
        this.iv_back = (Button) findViewById(R.id.iv_back);
        this.tv_greetings = (TextView) findViewById(R.id.tv_greetings);
        this.tv_title = (TextView) findViewById(R.id.tv_title);
        this.wv_content.setWebViewClient(new SSLWebViewClient());
    }

    public void initClickListener() {
        this.iv_back.setOnClickListener(this);
    }

    public void onClick(View v) {
        if (this.iv_back == v) {
            finish();
        }
    }

    public void loadData(int requestType, Object data) {
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.tv_title.setText((int) R.string.tips_stop_treat);
        try {
            JSONObject json = new JSONObject(getIntent().getExtras().getString("json"));
            this.tv_greetings.setText(json.optString("greetings"));
            this.wv_content.loadDataWithBaseURL(null, WebUtil.sourceCreateHtml(json.optString("content")), "text/html", "utf-8", null);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        return null;
    }
}
