package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.request.RequestTask;
import com.womenchild.hospital.util.ClientLogUtil;
import com.womenchild.hospital.util.WebUtil;
import com.womenchild.hospital.view.SSLWebViewClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OrderHelpActivity extends BaseRequestActivity implements View.OnClickListener {
    private static final String TAG = "OrderHelpActivity";
    private Button iv_back;
    private ProgressDialog pDialog;
    private WebView wvhprooms;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.order_help);
        initViewId();
        initClickListener();
        this.pDialog = new ProgressDialog(this);
        this.pDialog.setMessage(getResources().getString(R.string.loading_data));
        this.pDialog.show();
        sendHttpRequest(HttpRequestParameters.HOSPITAL_HELP_LIST);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void refreshActivity(Object... params) {
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            loadData(requestType, (JSONObject) params[2]);
        } else {
            Toast.makeText(this, getResources().getString(R.string.network_connect_failed_prompt), 0).show();
        }
    }

    public UriParameter initRequestParams(String requestType) {
        UriParameter parameters = new UriParameter();
        parameters.add("id", requestType);
        return parameters;
    }

    public void initViewId() {
        this.iv_back = (Button) findViewById(R.id.iv_back);
        this.wvhprooms = (WebView) findViewById(R.id.wv_hp_rooms);
        this.wvhprooms.setWebViewClient(new SSLWebViewClient());
    }

    public void initClickListener() {
        this.iv_back.setOnClickListener(this);
    }

    public UriParameter initRequestParams(int requestType) {
        UriParameter parameters = new UriParameter();
        parameters.add("num", 4);
        return parameters;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onClick(View v) {
        if (this.iv_back == v) {
            finish();
        }
    }

    public void loadData(int requestType, Object data) {
        JSONArray jsonArray;
        JSONArray jsonArray2;
        this.pDialog.dismiss();
        JSONObject json = (JSONObject) data;
        JSONObject res = json.optJSONObject("res");
        if (res != null) {
            try {
                if (res.getInt("st") != 0) {
                    return;
                }
                if (requestType == 121) {
                    JSONObject inf = json.optJSONObject("inf");
                    if (inf != null && (jsonArray2 = inf.getJSONArray("notices")) != null && jsonArray2.length() > 0 && 0 < jsonArray2.length()) {
                        sendHttpRequest((int) HttpRequestParameters.HOSPITAL_HELP_CONTTENT, jsonArray2.getJSONObject(0).getString("id"));
                    }
                } else if (requestType == 122 && (jsonArray = json.getJSONArray("inf")) != null && jsonArray.length() > 0 && 0 < jsonArray.length()) {
                    this.wvhprooms.loadDataWithBaseURL(null, WebUtil.sourceCreateHtml(jsonArray.getJSONObject(0).getString("content")), "text/html", "utf-8", null);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                ClientLogUtil.i(TAG, e.getMessage());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        return null;
    }

    public void sendHttpRequest(int requsetType) {
        this.pDialog = new ProgressDialog(this);
        this.pDialog.setMessage(getResources().getString(R.string.loading_data));
        this.pDialog.show();
        RequestTask.getInstance().sendHttpRequest(this, String.valueOf(requsetType), initRequestParams(requsetType));
    }

    public void sendHttpRequest(int requestType, String pam) {
        this.pDialog = new ProgressDialog(this);
        this.pDialog.setMessage(getResources().getString(R.string.loading_data));
        this.pDialog.show();
        RequestTask.getInstance().sendHttpRequest(this, String.valueOf(requestType), initRequestParams(pam));
    }
}
