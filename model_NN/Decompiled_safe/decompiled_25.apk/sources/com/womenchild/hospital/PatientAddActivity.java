package com.womenchild.hospital;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.configure.Constants;
import com.womenchild.hospital.entity.CardType;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.CardIDUtil;
import com.womenchild.hospital.util.ClientLogUtil;
import com.womenchild.hospital.util.DataVerifyUtil;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public class PatientAddActivity extends BaseRequestActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, View.OnFocusChangeListener {
    public static final int REQUEST_SELECT = 0;
    private static final String TAG = "PatientAddActivity";
    /* access modifiers changed from: private */
    public String cardNum;
    /* access modifiers changed from: private */
    public EditText cardNumEt;
    private List<CardType> cardTypes;
    private EditText citizenCardEt;
    /* access modifiers changed from: private */
    public EditText diagnosisCardEt;
    private LinearLayout diagnosisCardLy;
    private TextView diagnosisCardTv;
    private EditText emailEt;
    private EditText et_address;
    private EditText et_card_id;
    private EditText et_mobile_num;
    private EditText et_name;
    private boolean genderState;
    private ToggleButton genderTbtn;
    private EditText healthCardEt;
    private Intent intent;
    private boolean is_update;
    /* access modifiers changed from: private */
    public Spinner mSpinner;
    private String patientid;
    private ProgressDialog pdDialog;
    /* access modifiers changed from: private */
    public int selectPosition = 0;
    private String sex_stat;
    private EditText socialCardEt;
    private TextView submit;
    private TextView tv_back;
    private TextView tv_title;
    private boolean updateFlag;
    private String userid;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.patient_add);
        initViewId();
        initClickListener();
        initValues();
    }

    public void initViewId() {
        this.tv_back = (TextView) findViewById(R.id.tv_back);
        this.et_name = (EditText) findViewById(R.id.et_name);
        this.et_card_id = (EditText) findViewById(R.id.et_pid);
        this.et_mobile_num = (EditText) findViewById(R.id.et_phone);
        this.et_address = (EditText) findViewById(R.id.et_address);
        this.tv_title = (TextView) findViewById(R.id.tv_title);
        this.submit = (TextView) findViewById(R.id.tv_submit);
        this.genderTbtn = (ToggleButton) findViewById(R.id.tb_sex);
        this.socialCardEt = (EditText) findViewById(R.id.et_social_security_card);
        this.healthCardEt = (EditText) findViewById(R.id.et_resident_health_card);
        this.citizenCardEt = (EditText) findViewById(R.id.et_citizen_card);
        this.diagnosisCardEt = (EditText) findViewById(R.id.et_diagnosis_card);
        this.emailEt = (EditText) findViewById(R.id.et_email);
        this.diagnosisCardLy = (LinearLayout) findViewById(R.id.ly_diagnosis_card);
        this.diagnosisCardTv = (TextView) findViewById(R.id.tv_diagnosis_card_num);
    }

    private void initValues() {
        this.userid = UserEntity.getInstance().getInfo().getAccId();
        this.is_update = getIntent().getBooleanExtra("is_update", false);
        if (this.is_update) {
            this.tv_title.setText(getResources().getString(R.string.modify));
            this.et_name.setText(getIntent().getStringExtra("patientname"));
            this.et_mobile_num.setText(getIntent().getStringExtra("mobile"));
            this.et_address.setText(getIntent().getStringExtra("address"));
            this.et_card_id.setText(getIntent().getStringExtra("idcard"));
            this.patientid = getIntent().getStringExtra("patientid");
            this.sex_stat = getIntent().getStringExtra("gender");
            this.socialCardEt.setText(getIntent().getStringExtra("socialsecuritycard"));
            this.healthCardEt.setText(getIntent().getStringExtra("healthycard"));
            this.citizenCardEt.setText(getIntent().getStringExtra("citizzencard"));
            this.emailEt.setText(getIntent().getStringExtra("email"));
            this.diagnosisCardEt.setVisibility(8);
            this.diagnosisCardLy.setVisibility(0);
            this.diagnosisCardTv.setText(getIntent().getStringExtra("defaultpatientcard"));
            if (this.sex_stat.equals("1")) {
                this.genderState = true;
            } else {
                this.genderState = false;
            }
            this.genderTbtn.setChecked(this.genderState);
            return;
        }
        this.genderState = true;
        this.genderTbtn.setChecked(this.genderState);
    }

    public void initClickListener() {
        this.tv_back.setOnClickListener(this);
        this.submit.setOnClickListener(this);
        this.genderTbtn.setOnCheckedChangeListener(this);
        this.diagnosisCardLy.setOnClickListener(this);
        this.diagnosisCardEt.setOnFocusChangeListener(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_submit /*2131296445*/:
                checkSave();
                return;
            case R.id.tv_back /*2131296934*/:
                if (this.updateFlag) {
                    setResult(-1);
                }
                finish();
                return;
            case R.id.ly_diagnosis_card /*2131296941*/:
                this.intent = new Intent(this, SelDefaultMedicalCardActivity.class);
                this.intent.putExtra("patientId", this.patientid);
                this.intent.putExtra("patientName", this.et_name.getText().toString());
                startActivityForResult(this.intent, 0);
                return;
            default:
                return;
        }
    }

    private void checkSave() {
        boolean pass = true;
        CardIDUtil cardIDUtil = new CardIDUtil();
        if (PoiTypeDef.All.equals(this.et_name.getText().toString())) {
            this.et_name.setError(getResources().getString(R.string.not_null));
            pass = false;
            this.et_name.requestFocus();
        } else if (PoiTypeDef.All.equals(this.et_card_id.getText().toString())) {
            this.et_card_id.setError(getResources().getString(R.string.not_null));
            pass = false;
            this.et_card_id.requestFocus();
        } else if (this.et_card_id.getText().toString().length() != 18) {
            this.et_card_id.setError(getResources().getString(R.string.idcard_length_error));
            pass = false;
            this.et_card_id.requestFocus();
        } else if (!cardIDUtil.cardVerification(this.et_card_id.getText().toString())) {
            this.et_card_id.setError(getResources().getString(R.string.idcard_error));
            pass = false;
            this.et_card_id.requestFocus();
        } else if (PoiTypeDef.All.equals(this.et_mobile_num.getText().toString())) {
            this.et_mobile_num.setError(getResources().getString(R.string.not_null));
            pass = false;
            this.et_mobile_num.requestFocus();
        } else if (!DataVerifyUtil.isMobileNo(this.et_mobile_num.getText().toString())) {
            this.et_mobile_num.setError(getResources().getString(R.string.mobile_error));
            pass = false;
            this.et_mobile_num.requestFocus();
        }
        if (pass) {
            if (this.genderState) {
                this.sex_stat = "1";
            } else {
                this.sex_stat = "0";
            }
            if (this.is_update) {
                sendHttpRequest(107, initRequestParameter(107));
            } else {
                sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.ADD_PATIENT_INFO), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.ADD_PATIENT_INFO)));
            }
        }
    }

    public void refreshActivity(Object... params) {
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject result = (JSONObject) params[2];
            if (result != null) {
                ClientLogUtil.d(TAG, result.toString());
            }
            int resultCode = result.optJSONObject("res").optInt("st");
            String msg = result.optJSONObject("res").optString("msg");
            switch (resultCode) {
                case 0:
                    if (requestType == 126) {
                        this.cardTypes = CardType.getList(result);
                        showPatientCardTypeDialog(getResources().getString(R.string.choice_loadings_pa), this.cardTypes);
                        return;
                    }
                    Toast.makeText(getContext(), msg, 0).show();
                    setResult(-1);
                    finish();
                    return;
                case 1:
                    Toast.makeText(getContext(), msg, 0).show();
                    return;
                case 99:
                    Toast.makeText(getContext(), msg, 0).show();
                    return;
                default:
                    return;
            }
        } else {
            Toast.makeText(this, (int) R.string.network_connect_failed_prompt, 0).show();
        }
    }

    public void loadData(int requestType, Object json) {
    }

    private Context getContext() {
        return this;
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.tb_sex /*2131296749*/:
                if (isChecked) {
                    this.genderState = true;
                    return;
                } else {
                    this.genderState = false;
                    return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 0:
                if (resultCode == -1) {
                    this.updateFlag = true;
                    this.diagnosisCardTv.setText(data.getExtras().getString("defaultPatientCard"));
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.et_diagnosis_card /*2131296940*/:
                if (this.diagnosisCardEt.hasFocus()) {
                    sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.READ_CARD_TYPE), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.READ_CARD_TYPE)));
                    this.diagnosisCardEt.clearFocus();
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void showPatientCardTypeDialog(String content, List<CardType> cardTypes2) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        TextView titleTv = new TextView(this);
        titleTv.setTextSize(20.0f);
        titleTv.getPaint().setFakeBoldText(true);
        titleTv.setText(content);
        titleTv.setGravity(1);
        titleTv.setPadding(0, 10, 10, 0);
        builder.setCustomTitle(titleTv);
        View dialogaddrss = LayoutInflater.from(this).inflate((int) R.layout.dlg_paient_card_type, (ViewGroup) null);
        this.mSpinner = (Spinner) dialogaddrss.findViewById(R.id.spinner_type);
        this.cardNumEt = (EditText) dialogaddrss.findViewById(R.id.et_card_num);
        this.cardNumEt.setText(this.diagnosisCardEt.getText().toString().trim());
        ArrayList<String> strings = new ArrayList<>();
        strings.add(getResources().getString(R.string.choice_loadings));
        if (cardTypes2 != null) {
            int length = cardTypes2.size();
            for (int i = 0; i < length; i++) {
                strings.add(cardTypes2.get(i).getValue());
            }
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, (int) R.layout.spinner_item, strings);
        adapter.setDropDownViewResource(17367049);
        this.mSpinner.setAdapter((SpinnerAdapter) adapter);
        this.mSpinner.setSelection(this.selectPosition);
        builder.setView(dialogaddrss);
        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                PatientAddActivity.this.selectPosition = PatientAddActivity.this.mSpinner.getSelectedItemPosition();
                PatientAddActivity.this.cardNum = PatientAddActivity.this.cardNumEt.getText().toString().trim();
                if (PatientAddActivity.this.selectPosition == 0) {
                    Toast.makeText(PatientAddActivity.this, PatientAddActivity.this.getResources().getString(R.string.choice_loadings), 0).show();
                } else if (PoiTypeDef.All.equals(PatientAddActivity.this.cardNum)) {
                    Toast.makeText(PatientAddActivity.this, PatientAddActivity.this.getResources().getString(R.string.choice_ip_num), 0).show();
                    PatientAddActivity.this.cardNumEt.requestFocus();
                } else if (PatientAddActivity.this.cardNum.length() < 8) {
                    Toast.makeText(PatientAddActivity.this, PatientAddActivity.this.getResources().getString(R.string.ip_unm_maximum), 0).show();
                    PatientAddActivity.this.cardNumEt.requestFocus();
                } else {
                    PatientAddActivity.this.diagnosisCardEt.setText(PatientAddActivity.this.cardNum);
                    dialog.dismiss();
                }
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.ADD_PATIENT_INFO /*106*/:
                parameters.add("userid", this.userid);
                parameters.add("patientname", this.et_name.getText().toString());
                parameters.add("mobile", this.et_mobile_num.getText().toString());
                parameters.add("idcard", this.et_card_id.getText().toString());
                parameters.add("gender", this.sex_stat);
                parameters.add("address", this.et_address.getText().toString());
                parameters.add("socialsecuritycard", this.socialCardEt.getText().toString());
                parameters.add("healthycard", this.healthCardEt.getText().toString());
                parameters.add("citizzencard", this.citizenCardEt.getText().toString());
                parameters.add("email", this.emailEt.getText().toString());
                parameters.add("defaultpatientcard", this.diagnosisCardEt.getText().toString());
                if (!(this.cardTypes == null || this.selectPosition == 0)) {
                    parameters.add("patientcardtype", this.cardTypes.get(this.selectPosition - 1).getKey());
                    break;
                }
            case 107:
                parameters.add("userid", this.userid);
                parameters.add("patientid", this.patientid);
                parameters.add("patientname", this.et_name.getText().toString());
                parameters.add("mobile", this.et_mobile_num.getText().toString());
                parameters.add("idcard", this.et_card_id.getText().toString());
                parameters.add("gender", this.sex_stat);
                parameters.add("address", this.et_address.getText().toString());
                parameters.add("socialsecuritycard", this.socialCardEt.getText().toString());
                parameters.add("healthycard", this.healthCardEt.getText().toString());
                parameters.add("citizzencard", this.citizenCardEt.getText().toString());
                parameters.add("email", this.emailEt.getText().toString());
                break;
            case HttpRequestParameters.READ_CARD_TYPE /*126*/:
                parameters.add("hospitalid", Constants.HOSPITAL_ID);
                break;
        }
        return parameters;
    }
}
