package com.womenchild.hospital;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;
import com.womenchild.hospital.configure.Constants;

public class RemindSetActivity extends Activity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private static final String TAG = "RemindSetActivity";
    private ToggleButton checkAlertTb;
    private TextView checkLineTv;
    private RelativeLayout checkRlayout;
    /* access modifiers changed from: private */
    public TextView checkTimeSettingsTv;
    private TextView checkTipsTv;
    private TextView checkTitleTv;
    private Context context = this;
    private ImageView iv_back;
    /* access modifiers changed from: private */
    public CharSequence[] num = new CharSequence[10];
    /* access modifiers changed from: private */
    public SharedPreferences sharedPrefs;
    private ToggleButton testAlertTb;
    private TextView testLineTv;
    private RelativeLayout testRlayout;
    /* access modifiers changed from: private */
    public TextView testTimeSettingsTv;
    private TextView testTipsTv;
    private TextView testTitleTv;
    private TextView treatLineTv;
    private ToggleButton treatMentAlertTb;
    private RelativeLayout treatRlayout;
    /* access modifiers changed from: private */
    public TextView treatTimeSettingsTv;
    private TextView treatTipsTv;
    private TextView treatTitleTv;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.remind_set);
        initViewId();
        initClickListener();
        Intent intent = getIntent();
        if (intent != null) {
            showRemind(intent.getIntExtra("remindtype", 0));
        }
        for (int i = 0; i < this.num.length; i++) {
            this.num[i] = String.valueOf(i + 1);
        }
        this.sharedPrefs = this.context.getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, 0);
        if (this.sharedPrefs.getBoolean(Constants.TREAT_IS_ALERT, false)) {
            this.treatMentAlertTb.setChecked(true);
            int num2 = this.sharedPrefs.getInt(Constants.TREAT_SETTING_NUM, -1);
            if (num2 != -1) {
                this.treatTimeSettingsTv.setText(String.valueOf(num2) + getString(R.string.tips_set_unit));
            }
        } else {
            SharedPreferences.Editor editor = this.sharedPrefs.edit();
            editor.putInt(Constants.TREAT_SETTING_NUM, 3);
            editor.commit();
        }
        if (this.sharedPrefs.getBoolean(Constants.CHECK_IS_ALERT, false)) {
            this.checkAlertTb.setChecked(true);
            int num3 = this.sharedPrefs.getInt(Constants.CHECK_SETTING_NUM, -1);
            if (num3 != -1) {
                this.checkTimeSettingsTv.setText(String.valueOf(num3) + getString(R.string.tips_set_unit));
            }
        }
        if (this.sharedPrefs.getBoolean(Constants.TEST_IS_ALERT, false)) {
            this.testAlertTb.setChecked(true);
            int num4 = this.sharedPrefs.getInt(Constants.TEST_SETTING_NUM, -1);
            if (num4 != -1) {
                this.testTimeSettingsTv.setText(String.valueOf(num4) + getString(R.string.tips_set_unit));
            }
        }
        int num5 = this.sharedPrefs.getInt(Constants.TREAT_SETTING_NUM, -1);
        if (num5 != -1) {
            this.treatTimeSettingsTv.setText(String.valueOf(num5) + getString(R.string.tips_set_unit));
        }
        int num6 = this.sharedPrefs.getInt(Constants.CHECK_SETTING_NUM, -1);
        if (num6 != -1) {
            this.checkTimeSettingsTv.setText(String.valueOf(num6) + getString(R.string.tips_set_unit));
        }
        int num7 = this.sharedPrefs.getInt(Constants.TEST_SETTING_NUM, -1);
        if (num7 != -1) {
            this.testTimeSettingsTv.setText(String.valueOf(num7) + getString(R.string.tips_set_unit));
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void initViewId() {
        this.iv_back = (ImageView) findViewById(R.id.iv_back);
        this.treatMentAlertTb = (ToggleButton) findViewById(R.id.ibtn_queue_treatment);
        this.checkAlertTb = (ToggleButton) findViewById(R.id.ibtn_queue_check);
        this.testAlertTb = (ToggleButton) findViewById(R.id.ibtn_queue_test);
        this.treatTipsTv = (TextView) findViewById(R.id.tv_vis_remind_differ_time_title);
        this.treatTimeSettingsTv = (TextView) findViewById(R.id.tv_vis_remind_differ_time);
        this.treatLineTv = (TextView) findViewById(R.id.tv_vis_line);
        this.checkTipsTv = (TextView) findViewById(R.id.tv_check_remind_differ_time_title);
        this.checkTimeSettingsTv = (TextView) findViewById(R.id.tv_check_remind_differ_time);
        this.checkLineTv = (TextView) findViewById(R.id.tv_check_line);
        this.testTipsTv = (TextView) findViewById(R.id.tv_test_remind_differ_time_title);
        this.testTimeSettingsTv = (TextView) findViewById(R.id.tv_test_remind_differ_time);
        this.testLineTv = (TextView) findViewById(R.id.tv_test_line);
        this.treatTitleTv = (TextView) findViewById(R.id.tv_vis_remind_title);
        this.checkTitleTv = (TextView) findViewById(R.id.tv_check_remind_title);
        this.testTitleTv = (TextView) findViewById(R.id.tv_test_title);
        this.treatRlayout = (RelativeLayout) findViewById(R.id.layout_vis_remind);
        this.checkRlayout = (RelativeLayout) findViewById(R.id.layout_check_remind);
        this.testRlayout = (RelativeLayout) findViewById(R.id.layout_test_remind);
    }

    public void initClickListener() {
        this.iv_back.setOnClickListener(this);
        this.treatMentAlertTb.setOnCheckedChangeListener(this);
        this.checkAlertTb.setOnCheckedChangeListener(this);
        this.testAlertTb.setOnCheckedChangeListener(this);
        this.treatTimeSettingsTv.setOnClickListener(this);
        this.checkTimeSettingsTv.setOnClickListener(this);
        this.testTimeSettingsTv.setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onClick(View v) {
        if (this.iv_back == v) {
            finish();
        } else if (this.treatTimeSettingsTv == v) {
            showDialog(2);
        } else if (this.checkTimeSettingsTv == v) {
            showDialog(3);
        } else if (this.testTimeSettingsTv == v) {
            showDialog(4);
        }
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.ibtn_queue_treatment:
                if (isChecked) {
                    this.treatTipsTv.setVisibility(0);
                    this.treatTimeSettingsTv.setVisibility(0);
                    this.treatLineTv.setVisibility(0);
                    SharedPreferences.Editor editor = this.sharedPrefs.edit();
                    editor.putBoolean(Constants.TREAT_IS_ALERT, true);
                    editor.commit();
                    return;
                }
                this.treatTipsTv.setVisibility(8);
                this.treatTimeSettingsTv.setVisibility(8);
                this.treatLineTv.setVisibility(8);
                SharedPreferences.Editor editor2 = this.sharedPrefs.edit();
                editor2.putBoolean(Constants.TREAT_IS_ALERT, false);
                editor2.commit();
                return;
            case R.id.ibtn_queue_check:
                if (isChecked) {
                    this.checkTipsTv.setVisibility(0);
                    this.checkTimeSettingsTv.setVisibility(0);
                    this.checkLineTv.setVisibility(0);
                    SharedPreferences.Editor editor3 = this.sharedPrefs.edit();
                    editor3.putBoolean(Constants.CHECK_IS_ALERT, true);
                    editor3.commit();
                    return;
                }
                this.checkTipsTv.setVisibility(8);
                this.checkTimeSettingsTv.setVisibility(8);
                this.checkLineTv.setVisibility(8);
                SharedPreferences.Editor editor4 = this.sharedPrefs.edit();
                editor4.putBoolean(Constants.CHECK_IS_ALERT, false);
                editor4.commit();
                return;
            case R.id.ibtn_queue_test:
                if (isChecked) {
                    this.testTipsTv.setVisibility(0);
                    this.testTimeSettingsTv.setVisibility(0);
                    this.testLineTv.setVisibility(0);
                    SharedPreferences.Editor editor5 = this.sharedPrefs.edit();
                    editor5.putBoolean(Constants.TEST_IS_ALERT, true);
                    editor5.commit();
                    return;
                }
                this.testTipsTv.setVisibility(8);
                this.testTimeSettingsTv.setVisibility(8);
                this.testLineTv.setVisibility(8);
                SharedPreferences.Editor editor6 = this.sharedPrefs.edit();
                editor6.putBoolean(Constants.TEST_IS_ALERT, false);
                editor6.commit();
                return;
            default:
                return;
        }
    }

    private void showRemind(int currentRemind) {
        switch (currentRemind) {
            case 1:
                this.treatTitleTv.setVisibility(0);
                this.treatRlayout.setVisibility(0);
                this.checkTitleTv.setVisibility(0);
                this.checkRlayout.setVisibility(0);
                this.testTitleTv.setVisibility(0);
                this.testRlayout.setVisibility(0);
                return;
            case 2:
                this.treatTitleTv.setVisibility(0);
                this.treatRlayout.setVisibility(0);
                return;
            case 3:
                this.checkTitleTv.setVisibility(0);
                this.checkRlayout.setVisibility(0);
                return;
            case 4:
                this.testTitleTv.setVisibility(0);
                this.testRlayout.setVisibility(0);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 2:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(getString(R.string.tips_set_digit));
                builder.setItems(this.num, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        int settingNum = Integer.valueOf(RemindSetActivity.this.num[which].toString()).intValue();
                        RemindSetActivity.this.treatTimeSettingsTv.setText(String.valueOf(settingNum) + RemindSetActivity.this.getString(R.string.tips_set_unit));
                        SharedPreferences.Editor editor = RemindSetActivity.this.sharedPrefs.edit();
                        editor.putInt(Constants.TREAT_SETTING_NUM, settingNum);
                        editor.commit();
                    }
                });
                return builder.create();
            case 3:
                AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
                builder2.setTitle(getResources().getString(R.string.position));
                builder2.setItems(this.num, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        int settingNum = Integer.valueOf(RemindSetActivity.this.num[which].toString()).intValue();
                        RemindSetActivity.this.checkTimeSettingsTv.setText(String.valueOf(settingNum) + RemindSetActivity.this.getString(R.string.tips_set_unit));
                        SharedPreferences.Editor editor = RemindSetActivity.this.sharedPrefs.edit();
                        editor.putInt(Constants.CHECK_SETTING_NUM, settingNum);
                        editor.commit();
                    }
                });
                return builder2.create();
            case 4:
                AlertDialog.Builder builder3 = new AlertDialog.Builder(this);
                builder3.setTitle(getResources().getString(R.string.position));
                builder3.setItems(this.num, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        int settingNum = Integer.valueOf(RemindSetActivity.this.num[which].toString()).intValue();
                        RemindSetActivity.this.testTimeSettingsTv.setText(String.valueOf(settingNum) + RemindSetActivity.this.getString(R.string.tips_set_unit));
                        SharedPreferences.Editor editor = RemindSetActivity.this.sharedPrefs.edit();
                        editor.putInt(Constants.TEST_SETTING_NUM, settingNum);
                        editor.commit();
                    }
                });
                return builder3.create();
            default:
                return null;
        }
    }
}
