package com.womenchild.hospital;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import org.json.JSONException;
import org.json.JSONObject;

public class BillDetailActivity extends Activity {
    private Button ivBack;
    private TextView tvItemName;
    private TextView tvItemTotalFee;
    private TextView tvNumber;
    private TextView tvSelfFee;
    private TextView tvUnit;
    private TextView tvUnitPrice;
    private TextView tvUseMethod;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.bill_detail);
        this.ivBack = (Button) findViewById(R.id.ibtn_back);
        this.tvItemName = (TextView) findViewById(R.id.tv_item_name);
        this.tvItemTotalFee = (TextView) findViewById(R.id.tv_itemTotalFee_name);
        this.tvNumber = (TextView) findViewById(R.id.tv_number_name);
        this.tvUnit = (TextView) findViewById(R.id.tv_unit_name);
        this.tvUnitPrice = (TextView) findViewById(R.id.tv_unitPrice_name);
        this.tvUseMethod = (TextView) findViewById(R.id.tv_useMethod_name);
        this.tvSelfFee = (TextView) findViewById(R.id.tv_itemSelfFee_name);
        try {
            JSONObject jsonObject = new JSONObject(getIntent().getStringExtra("itemDetail"));
            if (jsonObject != null) {
                this.tvItemName.setText(jsonObject.optString("itemName"));
                this.tvNumber.setText(jsonObject.optString("number"));
                this.tvUnit.setText(jsonObject.optString("unit"));
                this.tvUnitPrice.setText(String.valueOf(jsonObject.optDouble("unitPrice") / 100.0d) + getResources().getString(R.string.price_rmb));
                this.tvItemTotalFee.setText(String.valueOf(jsonObject.optDouble("itemTotalFee") / 100.0d) + getResources().getString(R.string.price_rmb));
                this.tvSelfFee.setText(String.valueOf(jsonObject.optDouble("itemSelfFee") / 100.0d) + getResources().getString(R.string.price_rmb));
                this.tvUseMethod.setText(jsonObject.optString("useMethod"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.ivBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BillDetailActivity.this.finish();
            }
        });
    }
}
