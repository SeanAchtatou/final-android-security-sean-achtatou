package com.womenchild.hospital;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.configure.Constants;
import com.womenchild.hospital.entity.CardType;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.ClientLogUtil;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public class ModifyMedicalCardActivity extends BaseRequestActivity implements View.OnClickListener {
    private static final String TAG = "ModifyMedicalCardActivity";
    private Button btn_cancle;
    private Button btn_delete;
    private List<CardType> cardTypes;
    private String cardid;
    private int cardno;
    private String cards;
    private EditText et_card_no;
    private ProgressDialog pd;
    private int position;
    private Spinner sp_card_type;
    private TextView tv_submit;
    private int userno;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.modifiy_medical_card);
        initViewId();
        initClickListener();
        initData();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancle /*2131296444*/:
                finish();
                return;
            case R.id.tv_submit /*2131296445*/:
                if (!checkBlankBox()) {
                    ((InputMethodManager) this.et_card_no.getContext().getSystemService("input_method")).hideSoftInputFromWindow(this.et_card_no.getWindowToken(), 0);
                    sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.EDIT_PATIENT_CARD), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.EDIT_PATIENT_CARD)));
                    return;
                }
                return;
            case R.id.btn_delete /*2131296901*/:
                showAlterDialog();
                return;
            default:
                return;
        }
    }

    private void showAlterDialog() {
        new AlertDialog.Builder(this).setMessage(getResources().getString(R.string.f0dele_medicare)).setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ModifyMedicalCardActivity.this.sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.DELETE_PATIENT_CARD), ModifyMedicalCardActivity.this.initRequestParameter(Integer.valueOf((int) HttpRequestParameters.DELETE_PATIENT_CARD)));
            }
        }).setNegativeButton(getResources().getString(R.string.cancel), (DialogInterface.OnClickListener) null).create().show();
    }

    public void refreshActivity(Object... params) {
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject result = (JSONObject) params[2];
            ClientLogUtil.i(TAG, result.toString());
            switch (requestType) {
                case HttpRequestParameters.EDIT_PATIENT_CARD /*110*/:
                    loadData(HttpRequestParameters.EDIT_PATIENT_CARD, result);
                    break;
                case HttpRequestParameters.DELETE_PATIENT_CARD /*111*/:
                    loadData(HttpRequestParameters.DELETE_PATIENT_CARD, result);
                    break;
                case HttpRequestParameters.READ_CARD_TYPE /*126*/:
                    loadData(HttpRequestParameters.READ_CARD_TYPE, result);
                    break;
            }
        }
        this.pd.dismiss();
    }

    public void initViewId() {
        this.btn_cancle = (Button) findViewById(R.id.btn_cancle);
        this.btn_delete = (Button) findViewById(R.id.btn_delete);
        this.et_card_no = (EditText) findViewById(R.id.et_card_no);
        this.tv_submit = (TextView) findViewById(R.id.tv_submit);
        this.sp_card_type = (Spinner) findViewById(R.id.sp_card_type);
    }

    public void initClickListener() {
        this.btn_cancle.setOnClickListener(this);
        this.btn_delete.setOnClickListener(this);
        this.tv_submit.setOnClickListener(this);
    }

    public void loadData(int requestType, Object data) {
        JSONObject result = (JSONObject) data;
        String st = result.optJSONObject("res").optString("st");
        String msg = result.optJSONObject("res").optString("msg");
        switch (requestType) {
            case HttpRequestParameters.EDIT_PATIENT_CARD /*110*/:
                if ("0".equals(st)) {
                    Toast.makeText(this, msg, 0).show();
                    finish();
                    return;
                }
                Toast.makeText(this, msg, 0).show();
                return;
            case HttpRequestParameters.DELETE_PATIENT_CARD /*111*/:
                if ("0".equals(st)) {
                    Toast.makeText(this, msg, 0).show();
                    finish();
                    return;
                }
                Toast.makeText(this, msg, 0).show();
                return;
            case HttpRequestParameters.READ_CARD_TYPE /*126*/:
                this.cardTypes = CardType.getList(result);
                if (this.cardTypes == null) {
                    this.cardTypes = new ArrayList();
                    String[] cardType = getResources().getStringArray(R.array.cardtype);
                    String[] cardNo = getResources().getStringArray(R.array.cardtypeNo);
                    for (int i = 0; i < cardType.length; i++) {
                        CardType entity = new CardType();
                        entity.setKey(cardNo[i]);
                        entity.setValue(cardType[i]);
                        this.cardTypes.add(entity);
                    }
                }
                ininCardType(this.cardTypes);
                return;
            default:
                return;
        }
    }

    private void ininCardType(List<CardType> cardTypes2) {
        ArrayList<String> strings = new ArrayList<>();
        strings.add("选择诊疗卡类型");
        if (cardTypes2 != null) {
            int length = cardTypes2.size();
            for (int i = 0; i < length; i++) {
                strings.add(cardTypes2.get(i).getValue());
            }
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, (int) R.layout.spinner_item, strings);
        adapter.setDropDownViewResource(17367049);
        this.sp_card_type.setAdapter((SpinnerAdapter) adapter);
        String cardType = MedicalCardManagementActivity.list.get(this.userno).getCardList().get(this.cardno).getPatientcardtype();
        for (int i2 = 0; i2 < cardTypes2.size(); i2++) {
            if (cardType.equals(cardTypes2.get(i2).getKey())) {
                this.sp_card_type.setSelection(i2 + 1);
                return;
            }
        }
    }

    private boolean checkBlankBox() {
        String cardNo = this.et_card_no.getText().toString().trim();
        this.position = this.sp_card_type.getSelectedItemPosition();
        if (this.position == 0) {
            Toast.makeText(this, getResources().getString(R.string.select_medicare_type), 0).show();
            return true;
        } else if (PoiTypeDef.All.equals(cardNo)) {
            Toast.makeText(this, getResources().getString(R.string.f1ip_medicare_nur), 0).show();
            this.et_card_no.requestFocus();
            return true;
        } else if (cardNo.length() >= 7) {
            return false;
        } else {
            Toast.makeText(this, getResources().getString(R.string.f2medicare_length), 0).show();
            this.et_card_no.requestFocus();
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.pd = new ProgressDialog(this);
        Intent intent = getIntent();
        this.userno = intent.getIntExtra("userno", 0);
        this.cardno = intent.getIntExtra("cardno", 0);
        String card = MedicalCardManagementActivity.list.get(this.userno).getCardList().get(this.cardno).getCard();
        this.et_card_no.setText(card);
        this.et_card_no.setSelection(card.length());
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.READ_CARD_TYPE), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.READ_CARD_TYPE)));
        this.pd.show();
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.EDIT_PATIENT_CARD /*110*/:
                this.pd.setMessage(getResources().getString(R.string.ok_changing));
                String patientid = MedicalCardManagementActivity.list.get(this.userno).getPatientid();
                this.cardid = MedicalCardManagementActivity.list.get(this.userno).getCardList().get(this.cardno).getCardid();
                this.cards = this.et_card_no.getText().toString();
                String patientcardtype = this.cardTypes.get(this.position - 1).getKey();
                parameters.add("patientid", patientid);
                parameters.add("cardid", this.cardid);
                parameters.add("cards", this.cards);
                parameters.add("hospital", getResources().getString(R.string.add_womenchild));
                parameters.add("patientcardtype", patientcardtype);
                break;
            case HttpRequestParameters.DELETE_PATIENT_CARD /*111*/:
                this.pd.setMessage(getResources().getString(R.string.f3delect_medicare));
                this.cardid = MedicalCardManagementActivity.list.get(this.userno).getCardList().get(this.cardno).getCardid();
                parameters.add("cardid", this.cardid);
                break;
            case HttpRequestParameters.READ_CARD_TYPE /*126*/:
                this.pd.setMessage(getResources().getString(R.string.getmedicare));
                parameters.add("hospitalid", Constants.HOSPITAL_ID);
                break;
        }
        return parameters;
    }
}
