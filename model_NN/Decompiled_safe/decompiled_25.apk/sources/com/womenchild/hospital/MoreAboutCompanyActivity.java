package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.Toast;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.WebUtil;
import com.womenchild.hospital.view.SSLWebViewClient;
import org.json.JSONException;
import org.json.JSONObject;

public class MoreAboutCompanyActivity extends BaseRequestActivity implements View.OnClickListener {
    private ImageView ibtnHome;
    private Intent intent;
    private ProgressDialog pDialog;
    private WebView wvhprooms;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.about_yijiantong);
        initViewId();
        initClickListener();
        this.pDialog = new ProgressDialog(this);
        this.pDialog.setMessage(getResources().getString(R.string.loading_data));
        this.pDialog.show();
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.HOSPITAL_TAFFIC_INFO), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.HOSPITAL_TAFFIC_INFO)));
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_home /*2131296435*/:
                finish();
                return;
            default:
                return;
        }
    }

    public void refreshActivity(Object... params) {
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            loadData(requestType, (JSONObject) params[2]);
        } else {
            Toast.makeText(this, getResources().getString(R.string.network_connect_failed_prompt), 0).show();
        }
    }

    public void initViewId() {
        this.ibtnHome = (ImageView) findViewById(R.id.ibtn_home);
        this.wvhprooms = (WebView) findViewById(R.id.wv_hp_introduce);
        this.wvhprooms.setWebViewClient(new SSLWebViewClient());
    }

    public void initClickListener() {
        this.ibtnHome.setOnClickListener(this);
    }

    public void loadData(int requestType, Object data) {
        JSONObject inf;
        this.pDialog.dismiss();
        JSONObject jsonObject = (JSONObject) data;
        JSONObject res = jsonObject.optJSONObject("res");
        if (res != null) {
            try {
                if (res.getInt("st") == 0 && (inf = jsonObject.optJSONObject("inf")) != null) {
                    this.wvhprooms.loadDataWithBaseURL(null, WebUtil.sourceCreateHtml(inf.optString("noticecontent")), "text/html", "utf-8", null);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        parameters.add("num", 9);
        return parameters;
    }
}
