package com.womenchild.hospital;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.womenchild.hospital.base.BaseRequestFragment;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.ClientLogUtil;
import com.womenchild.hospital.util.WebUtil;
import org.json.JSONException;
import org.json.JSONObject;

public class HospitalIntroduceFragment extends BaseRequestFragment implements View.OnClickListener {
    private static final String TAG = "HpIntroduceFrag";
    private Button btnPhone;
    private Button btnPhone1;
    private Button btn_cancel;
    private Button ibtnHome;
    private ProgressDialog pDialog;
    private RelativeLayout rl_phone;
    private WebView wvhpintroduce;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ClientLogUtil.v(TAG, "onCreateView()");
        View view = inflater.inflate((int) R.layout.hospital_introduce, container, false);
        initViewId(view);
        initClickListener();
        this.pDialog = new ProgressDialog(getActivity());
        this.pDialog.setMessage(getResources().getString(R.string.loading_data));
        this.pDialog.show();
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.HOSPITAL_TAFFIC_INFO), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.HOSPITAL_TAFFIC_INFO)));
        return view;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ClientLogUtil.v(TAG, "onActivityCreated()");
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ClientLogUtil.v(TAG, "onAttach()");
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ClientLogUtil.v(TAG, "onCreate()");
    }

    public void onDestroy() {
        super.onDestroy();
        ClientLogUtil.v(TAG, "onDestroy()");
    }

    public void onDestroyView() {
        super.onDestroyView();
        ClientLogUtil.v(TAG, "onDestroyView()");
    }

    public void onDetach() {
        super.onDetach();
        ClientLogUtil.v(TAG, "onDetach()");
    }

    public void onPause() {
        super.onPause();
        ClientLogUtil.v(TAG, "onPause()");
    }

    public void onResume() {
        super.onResume();
        ClientLogUtil.v(TAG, "onResume()");
    }

    public void onStart() {
        super.onStart();
        ClientLogUtil.v(TAG, "onStart()");
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_home /*2131296435*/:
                getActivity().finish();
                return;
            case R.id.btn_phone1 /*2131296825*/:
                showResultDialog(getActivity().getString(R.string.tv_phone_num));
                return;
            case R.id.btn_phone /*2131296828*/:
                this.rl_phone.setVisibility(8);
                return;
            case R.id.btn_cancel /*2131296829*/:
                this.rl_phone.setVisibility(8);
                return;
            default:
                return;
        }
    }

    public void initClickListener() {
        this.ibtnHome.setOnClickListener(this);
        this.btnPhone.setOnClickListener(this);
        this.btn_cancel.setOnClickListener(this);
        this.btnPhone1.setOnClickListener(this);
    }

    public void loadData(int requestType, Object data) {
        JSONObject jsonObject = (JSONObject) data;
        JSONObject res = jsonObject.optJSONObject("res");
        String msg = res.optString("msg");
        if (res != null) {
            try {
                if (res.getInt("st") == 0) {
                    JSONObject inf = jsonObject.optJSONObject("inf");
                    if (inf != null) {
                        this.wvhpintroduce.setWebViewClient(new WebViewClient() {
                            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                                super.onReceivedSslError(view, handler, error);
                                handler.proceed();
                            }
                        });
                        this.wvhpintroduce.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
                        this.wvhpintroduce.loadDataWithBaseURL(null, WebUtil.sourceCreateHtml(inf.optString("noticecontent")), "text/html", "utf-8", null);
                        return;
                    }
                    return;
                }
            } catch (JSONException e) {
                e.printStackTrace();
                return;
            }
        }
        Toast.makeText(getActivity(), msg, 1).show();
    }

    public void showResultDialog(String content) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        TextView titleTv = new TextView(getActivity());
        titleTv.setTextSize(16.0f);
        titleTv.getPaint().setFakeBoldText(true);
        titleTv.setText(getResources().getString(R.string.ok_doctors_phone));
        titleTv.setGravity(1);
        titleTv.setPadding(0, 10, 0, 0);
        builder.setCustomTitle(titleTv);
        TextView contentTv = new TextView(getActivity());
        contentTv.setTextSize(20.0f);
        contentTv.setText(content);
        contentTv.setGravity(1);
        builder.setView(contentTv);
        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                try {
                    HospitalIntroduceFragment.this.callPhone();
                } catch (SecurityException e) {
                    e.printStackTrace();
                }
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    public void callPhone() throws SecurityException {
        startActivity(new Intent("android.intent.action.CALL", Uri.parse("tel:" + this.btnPhone.getText().toString())));
    }

    /* access modifiers changed from: protected */
    public void initViewId(View view) {
        this.ibtnHome = (Button) view.findViewById(R.id.ibtn_home);
        this.btnPhone = (Button) view.findViewById(R.id.btn_phone);
        this.wvhpintroduce = (WebView) view.findViewById(R.id.wv_hp_introduce);
        this.rl_phone = (RelativeLayout) view.findViewById(R.id.rl_phone);
        this.btn_cancel = (Button) view.findViewById(R.id.btn_cancel);
        this.btnPhone1 = (Button) view.findViewById(R.id.btn_phone1);
    }

    /* access modifiers changed from: protected */
    public void initUIData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        parameters.add("num", 1);
        return parameters;
    }

    public void refreshFragment(Object... params) {
        this.pDialog.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            loadData(requestType, (JSONObject) params[2]);
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.network_connect_failed_prompt), 0).show();
        }
    }

    /* access modifiers changed from: protected */
    public void initViewId() {
    }
}
