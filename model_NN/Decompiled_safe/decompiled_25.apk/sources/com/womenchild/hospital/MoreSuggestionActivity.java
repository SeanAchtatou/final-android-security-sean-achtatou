package com.womenchild.hospital;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import org.json.JSONObject;

public class MoreSuggestionActivity extends BaseRequestActivity implements View.OnClickListener {
    private static final String TAG = "MoreSuggestionActivity";
    private TextView calcelTv;
    private TextView commitTv;
    private EditText contentEdit;
    private String contentStr;
    /* access modifiers changed from: private */
    public Intent intent;
    private ProgressDialog pDialog;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.more_suggestion);
        initViewId();
        initClickListener();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_submit /*2131296445*/:
                if (!HomeActivity.loginFlag) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle(getResources().getString(R.string.login_hint));
                    builder.setMessage(getResources().getString(R.string.login_function));
                    builder.setPositiveButton(getResources().getString(R.string.free_register), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            MoreSuggestionActivity.this.intent = new Intent(MoreSuggestionActivity.this, RegisterActivity.class);
                            MoreSuggestionActivity.this.startActivity(MoreSuggestionActivity.this.intent);
                        }
                    });
                    builder.setNegativeButton(getResources().getString(R.string.login), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            MoreSuggestionActivity.this.intent = new Intent(MoreSuggestionActivity.this, LoginActivity.class);
                            MoreSuggestionActivity.this.startActivity(MoreSuggestionActivity.this.intent);
                        }
                    });
                    builder.create().show();
                    return;
                }
                commitContext();
                return;
            case R.id.tv_cancel /*2131296918*/:
                finish();
                return;
            default:
                return;
        }
    }

    public void refreshActivity(Object... params) {
        this.pDialog.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject result = (JSONObject) params[2];
            if (requestType != 601) {
                return;
            }
            if ("0".equals(result.optJSONObject("res").optString("st"))) {
                showResultDialog(result.optJSONObject("res").optString("msg"));
            } else {
                Toast.makeText(this, getResources().getString(R.string.submit_fail), 0).show();
            }
        } else {
            Toast.makeText(this, getResources().getString(R.string.network_connect_failed_prompt), 0).show();
        }
    }

    public void initViewId() {
        this.calcelTv = (TextView) findViewById(R.id.tv_cancel);
        this.commitTv = (TextView) findViewById(R.id.tv_submit);
        this.contentEdit = (EditText) findViewById(R.id.et_desc);
    }

    public void initClickListener() {
        this.calcelTv.setOnClickListener(this);
        this.commitTv.setOnClickListener(this);
    }

    public void loadData(int requestType, Object data) {
    }

    public void commitContext() {
        this.contentStr = this.contentEdit.getText().toString().trim();
        if (PoiTypeDef.All.equals(this.contentStr)) {
            Toast.makeText(this, getResources().getString(R.string.content_null_ip), 0).show();
            return;
        }
        this.pDialog = new ProgressDialog(this);
        this.pDialog.setMessage(getResources().getString(R.string.submit_pw));
        this.pDialog.show();
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.COMMIT_SUGGESTION), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.COMMIT_SUGGESTION)));
    }

    public void showResultDialog(String content) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        TextView titleTv = new TextView(this);
        titleTv.setTextSize(16.0f);
        titleTv.getPaint().setFakeBoldText(true);
        titleTv.setText(content);
        titleTv.setGravity(1);
        titleTv.setPadding(0, 10, 10, 0);
        builder.setCustomTitle(titleTv);
        TextView contentTv = new TextView(this);
        contentTv.setTextSize(14.0f);
        contentTv.setText(getResources().getString(R.string.feedback));
        contentTv.setGravity(1);
        builder.setView(contentTv);
        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                MoreSuggestionActivity.this.finish();
            }
        });
        builder.create().show();
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        parameters.add("writer", UserEntity.getInstance().getInfo().getName());
        parameters.add("content", this.contentStr);
        parameters.add("userid", UserEntity.getInstance().getInfo().getAccId());
        return parameters;
    }
}
