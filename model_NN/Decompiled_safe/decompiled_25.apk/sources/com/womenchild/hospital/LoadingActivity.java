package com.womenchild.hospital;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import com.umeng.analytics.MobclickAgent;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.parameter.UriParameter;

public class LoadingActivity extends BaseRequestActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.loading_screen);
        initViewId();
        initClickListener();
        initData();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    /* access modifiers changed from: protected */
    public void initViewId() {
    }

    /* access modifiers changed from: protected */
    public void initData() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                LoadingActivity.this.startActivity(new Intent(LoadingActivity.this, HomeActivity.class));
                LoadingActivity.this.finish();
                LoadingActivity.this.overridePendingTransition(17432576, 17432577);
            }
        }, 1500);
    }

    /* access modifiers changed from: protected */
    public void initClickListener() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        return null;
    }

    /* access modifiers changed from: protected */
    public void loadData(int requestType, Object data) {
    }

    public void refreshActivity(Object... objects) {
    }
}
