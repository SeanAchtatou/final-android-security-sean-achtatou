package com.womenchild.hospital;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.entity.OrderEntity;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.CardIDUtil;
import com.womenchild.hospital.util.ClientLogUtil;
import com.womenchild.hospital.util.DataVerifyUtil;
import com.womenchild.hospital.util.DateUtil;
import com.womenchild.hospital.util.SharedPreferencesUtil;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FillInAppointmentInfoActivity extends BaseRequestActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    public static OrderEntity orderEntity;
    private String TAG = "FillInAppointmentInfoActivity";
    private Button btnNext;
    private Button btnShow;
    private JSONArray cards;
    /* access modifiers changed from: private */
    public Calendar dateAndTime = Calendar.getInstance(Locale.CHINA);
    private DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            FillInAppointmentInfoActivity.this.dateAndTime.set(1, year);
            FillInAppointmentInfoActivity.this.dateAndTime.set(2, monthOfYear);
            FillInAppointmentInfoActivity.this.dateAndTime.set(5, dayOfMonth);
            SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            try {
                FillInAppointmentInfoActivity.this.tvBirth.setText(sDateFormat.format(sDateFormat.parse(FillInAppointmentInfoActivity.this.fmtDateAndTime.format(FillInAppointmentInfoActivity.this.dateAndTime.getTime()))));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    };
    private EditText etAddress;
    private EditText etIdcard;
    private EditText etName;
    private EditText etPhone;
    /* access modifiers changed from: private */
    public DateFormat fmtDateAndTime = DateFormat.getDateTimeInstance();
    private RadioGroup gdGroup;
    private Button ibtnReturn;
    private Intent intent;
    private Context mContext = this;
    /* access modifiers changed from: private */
    public CharSequence[] num;
    RadioGroup.OnCheckedChangeListener onCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        public void onCheckedChanged(RadioGroup arg0, int tabId) {
            switch (tabId) {
                case R.id.rb_custodian_yes /*2131296761*/:
                    FillInAppointmentInfoActivity.orderEntity.setGuardian("0");
                    return;
                case R.id.tv_custodian_yes /*2131296762*/:
                default:
                    return;
                case R.id.rb_custodian_no /*2131296763*/:
                    FillInAppointmentInfoActivity.orderEntity.setGuardian("1");
                    return;
            }
        }
    };
    private ProgressDialog pDialog;
    private String patient;
    private String patientCard;
    /* access modifiers changed from: private */
    public TextView patientCardTv;
    /* access modifiers changed from: private */
    public JSONObject patientJson;
    private boolean payFlag = true;
    private String plan;
    private SharedPreferencesUtil preferences;
    private int settingNum = -1;
    private int sex = 1;
    private ToggleButton tbPrePay;
    private ToggleButton tbSex;
    /* access modifiers changed from: private */
    public TextView tvBirth;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.fill_in_appointment_info);
        initViewId();
        initClickListener();
        initData();
    }

    public void refreshActivity(Object... params) {
        this.pDialog.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject result = (JSONObject) params[2];
            int resultCode = result.optJSONObject("res").optInt("st");
            String msg = result.optJSONObject("res").optString("msg");
            if (resultCode == 0) {
                switch (requestType) {
                    case HttpRequestParameters.PATIENT_CARD_LIST /*123*/:
                        loadData(requestType, result.optJSONObject("inf"));
                        return;
                    case HttpRequestParameters.ORDER_VERIFY /*600*/:
                        if (this.payFlag) {
                            this.intent = new Intent(this, ConfirmAndPayActivity.class);
                            this.intent.putExtra("order", result.optJSONObject("inf").toString());
                            this.intent.putExtra("plan", this.plan);
                            startActivity(this.intent);
                            return;
                        }
                        this.intent = new Intent(this, OrdersConfirmActivity.class);
                        this.intent.putExtra("order", result.optJSONObject("inf").toString());
                        this.intent.putExtra("plan", this.plan);
                        startActivity(this.intent);
                        return;
                    default:
                        return;
                }
            } else {
                Toast.makeText(this, msg, 0).show();
            }
        } else {
            switch (requestType) {
                case HttpRequestParameters.PATIENT_CARD_LIST /*123*/:
                    this.num = new CharSequence[]{getResources().getString(R.string.add_make)};
                    break;
            }
            Toast.makeText(this, (int) R.string.network_connect_failed_prompt, 0).show();
        }
    }

    public void initViewId() {
        this.ibtnReturn = (Button) findViewById(R.id.ibtn_return);
        this.btnNext = (Button) findViewById(R.id.btn_next);
        this.tvBirth = (TextView) findViewById(R.id.et_birth_date);
        this.etIdcard = (EditText) findViewById(R.id.et_idcard_no);
        this.etName = (EditText) findViewById(R.id.et_name);
        this.etPhone = (EditText) findViewById(R.id.et_phone_no);
        this.tbSex = (ToggleButton) findViewById(R.id.tb_sex);
        this.tbPrePay = (ToggleButton) findViewById(R.id.tb_pre_pay);
        this.etAddress = (EditText) findViewById(R.id.et_address);
        this.pDialog = new ProgressDialog(this);
        this.pDialog.setMessage(getResources().getString(R.string.loading_ok));
        this.patientCardTv = (TextView) findViewById(R.id.et_clinic_card_no);
        this.btnShow = (Button) findViewById(R.id.btn_show);
        this.gdGroup = (RadioGroup) findViewById(R.id.rg_custodian);
        this.gdGroup.check(R.id.rb_custodian_no);
    }

    public void initClickListener() {
        this.ibtnReturn.setOnClickListener(this);
        this.btnNext.setOnClickListener(this);
        this.tvBirth.setOnClickListener(this);
        this.tbSex.setOnCheckedChangeListener(this);
        this.tbPrePay.setOnCheckedChangeListener(this);
        this.btnShow.setOnClickListener(this);
        this.gdGroup.setOnCheckedChangeListener(this.onCheckedChangeListener);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_return /*2131296570*/:
                finish();
                return;
            case R.id.et_birth_date /*2131296751*/:
                new DatePickerDialog(this, this.dateSetListener, this.dateAndTime.get(1), this.dateAndTime.get(2), this.dateAndTime.get(5)).show();
                return;
            case R.id.btn_show /*2131296769*/:
                showDialog(1);
                return;
            case R.id.btn_next /*2131296772*/:
                if (verifyData()) {
                    this.pDialog.show();
                    sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.ORDER_VERIFY), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.ORDER_VERIFY)));
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void loadData(int requestType, Object data) {
        switch (requestType) {
            case HttpRequestParameters.PATIENT_CARD_LIST /*123*/:
                this.num = getString(((JSONObject) data).optJSONArray("listobject").optJSONObject(0).optJSONArray("patientcards"));
                return;
            default:
                return;
        }
    }

    private ArrayAdapter<String> getAdapter(JSONArray jsons) {
        this.cards = jsons;
        String[] adapter = new String[jsons.length()];
        for (int i = 0; i < jsons.length(); i++) {
            adapter[i] = jsons.optJSONObject(i).optString("card");
        }
        return new ArrayAdapter<>(this, (int) R.layout.spinner_item, adapter);
    }

    private String getCardType(String cardNo) {
        int i = 0;
        while (i < this.cards.length() && !cardNo.equals(this.cards.optJSONObject(i).optString("card"))) {
            i++;
        }
        if (i < this.cards.length()) {
            return this.cards.optJSONObject(i).optString("patientcardtype");
        }
        return PoiTypeDef.All;
    }

    private boolean verifyData() {
        if (PoiTypeDef.All.equals(this.etName.getText().toString().trim())) {
            Toast.makeText(this, getResources().getString(R.string.user_null), 0).show();
            return false;
        } else if (PoiTypeDef.All.equals(this.etPhone.getText().toString().trim())) {
            Toast.makeText(this, getResources().getString(R.string.users_phone), 0).show();
            return false;
        } else if (!DataVerifyUtil.isMobileNo(this.etPhone.getText().toString().trim())) {
            Toast.makeText(this, getResources().getString(R.string.phone_format), 0).show();
            return false;
        } else if (PoiTypeDef.All.equals(this.etIdcard.getText().toString().trim())) {
            Toast.makeText(this, getResources().getString(R.string.user_id), 0).show();
            return false;
        } else if (this.etIdcard.getText().toString().trim().length() < 18) {
            Toast.makeText(this, getResources().getString(R.string.user_id_length), 0).show();
            return false;
        } else {
            if ("1".equals(orderEntity.getGuardian())) {
                CardIDUtil cardIDUtil = new CardIDUtil();
                if (!cardIDUtil.cardVerification(this.etIdcard.getText().toString().trim())) {
                    Toast.makeText(this, getResources().getString(R.string.user_id_error), 0).show();
                    return false;
                } else if (cardIDUtil.getSexType(this.etIdcard.getText().toString().trim()) != this.sex) {
                    Toast.makeText(this, getResources().getString(R.string.inconformity_id_age), 0).show();
                    return false;
                } else if (!DateUtil.getBrithDate(this.etIdcard.getText().toString().trim()).equals(this.tvBirth.getText().toString())) {
                    Toast.makeText(this, getResources().getString(R.string.birth_id), 0).show();
                    return false;
                }
            }
            if (this.etAddress.getText().toString().length() <= 0) {
                Toast.makeText(this, getResources().getString(R.string.address_null), 0).show();
                return false;
            }
            orderEntity.setPatientName(this.etName.getText().toString().trim());
            orderEntity.setMobile(this.etPhone.getText().toString().trim());
            orderEntity.setCardid(this.etIdcard.getText().toString().trim());
            orderEntity.setBirthday(this.tvBirth.getText().toString().trim());
            orderEntity.setPatientCard(this.patientCardTv.getText().toString());
            orderEntity.setPatientCardType(getCardType(this.patientCardTv.getText().toString()));
            orderEntity.setAddress(this.etAddress.getText().toString());
            orderEntity.setOrdernum(SelectClinicTimeActivity.ballNum);
            this.preferences.save("patientCard", this.patientCardTv.getText().toString());
            return true;
        }
    }

    public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
        switch (arg0.getId()) {
            case R.id.tb_sex /*2131296749*/:
                if (arg1) {
                    this.sex = 1;
                    orderEntity.setGender("1");
                    return;
                }
                this.sex = 0;
                orderEntity.setGender("0");
                return;
            case R.id.tb_pre_pay /*2131296771*/:
                if (arg1) {
                    this.payFlag = true;
                    orderEntity.setPrePay("0");
                    return;
                }
                this.payFlag = false;
                orderEntity.setPrePay("1");
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setItems(this.num, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (FillInAppointmentInfoActivity.this.num != null) {
                            if (FillInAppointmentInfoActivity.this.getResources().getString(R.string.add_make).equals(FillInAppointmentInfoActivity.this.num[which].toString())) {
                                Intent temp = new Intent(FillInAppointmentInfoActivity.this, AddMedicalCardActivity.class);
                                temp.putExtra("patientname", FillInAppointmentInfoActivity.this.patientJson.optString("patientname"));
                                temp.putExtra("patientid", FillInAppointmentInfoActivity.this.patientJson.optString("patientid"));
                                FillInAppointmentInfoActivity.this.startActivity(temp);
                                return;
                            }
                            FillInAppointmentInfoActivity.this.patientCardTv.setText(FillInAppointmentInfoActivity.this.num[which].toString());
                        }
                    }
                });
                return builder.create();
            default:
                return null;
        }
    }

    private CharSequence[] getString(JSONArray jsons) {
        int arrayLength;
        this.cards = jsons;
        int length = jsons.length();
        if (length < 3) {
            arrayLength = length + 1;
        } else {
            arrayLength = length;
        }
        CharSequence[] arrayAdapter = new CharSequence[arrayLength];
        for (int i = 0; i < length; i++) {
            arrayAdapter[i] = jsons.optJSONObject(i).optString("card");
        }
        if (length < 3) {
            arrayAdapter[length] = getResources().getString(R.string.add_make);
        }
        return arrayAdapter;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.num != null) {
            this.num = null;
        }
        removeDialog(1);
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.PATIENT_CARD_LIST), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.PATIENT_CARD_LIST)));
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.PATIENT_CARD_LIST /*123*/:
                UriParameter parameters = new UriParameter();
                parameters.add("patientid", this.patientJson.optString("patientid"));
                parameters.add("userid", UserEntity.getInstance().getInfo().getAccId());
                return parameters;
            case HttpRequestParameters.ORDER_VERIFY /*600*/:
                UriParameter parameters2 = orderEntity.createParameters();
                parameters2.add("userid", UserEntity.getInstance().getInfo().getAccId());
                return parameters2;
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.preferences = new SharedPreferencesUtil(this);
        this.patientCard = this.preferences.get("patientCard");
        this.patientCardTv.setText(this.patientCard);
        orderEntity = new OrderEntity();
        this.plan = getIntent().getStringExtra("plan");
        this.patient = getIntent().getStringExtra("patient");
        try {
            this.patientJson = new JSONObject(this.patient);
            JSONObject planJson = new JSONObject(this.plan);
            ClientLogUtil.i(getClass().getSimpleName(), this.patientJson.toString());
            this.etName.setText(this.patientJson.optString("patientname"));
            this.etIdcard.setText(this.patientJson.optString("idcard"));
            this.etPhone.setText(this.patientJson.optString("mobile"));
            this.etAddress.setText(this.patientJson.optString("address"));
            if (this.patientJson.optInt("gender") == 0) {
                this.tbSex.setChecked(false);
            } else {
                this.tbSex.setChecked(true);
            }
            String brith = DateUtil.getBrithDate(this.patientJson.optString("idcard"));
            if (brith != null && !PoiTypeDef.All.equals(brith)) {
                this.tvBirth.setText(brith);
            }
            orderEntity.setPatientName(this.patientJson.optString("patientname"));
            orderEntity.setCardid(this.patientJson.optString("idcard"));
            orderEntity.setMobile(this.patientJson.optString("mobile"));
            orderEntity.setGender(String.valueOf(this.patientJson.optInt("gender")));
            orderEntity.setBirthday(brith);
            orderEntity.setAddress(this.patientJson.optString("address"));
            orderEntity.setPlanId(planJson.optLong("planid"));
        } catch (JSONException e) {
            e.printStackTrace();
            ClientLogUtil.e(this.TAG, e.getMessage());
        } catch (NullPointerException e2) {
        }
        this.pDialog.show();
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.PATIENT_CARD_LIST), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.PATIENT_CARD_LIST)));
    }
}
