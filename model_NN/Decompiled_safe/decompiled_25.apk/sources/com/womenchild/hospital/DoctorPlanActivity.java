package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.umeng.common.a;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.DateUtil;
import com.womenchild.hospital.util.ImageUtil;
import org.json.JSONArray;
import org.json.JSONObject;

public class DoctorPlanActivity extends BaseRequestActivity implements View.OnClickListener {
    /* access modifiers changed from: private */
    public Button btnDayForward;
    /* access modifiers changed from: private */
    public Button btnDayNext;
    private Button btnFavDoctor;
    /* access modifiers changed from: private */
    public Button btnOrder1;
    /* access modifiers changed from: private */
    public Button btnOrder2;
    /* access modifiers changed from: private */
    public Button btnOrder3;
    /* access modifiers changed from: private */
    public Button btnOrder4;
    /* access modifiers changed from: private */
    public Button btnOrder5;
    /* access modifiers changed from: private */
    public Button btnOrder6;
    private String doctorName;
    private int doctorid;
    private boolean flag = false;
    private Button ivBack;
    private ImageView ivDoctorPhoto;
    private JSONArray jsons;
    private ProgressDialog pDialog;
    private TextView tvDoctorInfo;
    private TextView tvDoctorName;
    private TextView tvDoctorSex;
    /* access modifiers changed from: private */
    public TextView tvOrder1;
    /* access modifiers changed from: private */
    public TextView tvOrder2;
    /* access modifiers changed from: private */
    public TextView tvOrder3;
    /* access modifiers changed from: private */
    public TextView tvOrder4;
    /* access modifiers changed from: private */
    public TextView tvOrder5;
    /* access modifiers changed from: private */
    public TextView tvOrder6;
    private TextView tvOrderCount;
    /* access modifiers changed from: private */
    public TextView tvTime1;
    /* access modifiers changed from: private */
    public TextView tvTime2;
    /* access modifiers changed from: private */
    public TextView tvTime3;
    private TextView tvTitle;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.doctor_plan);
        initViewId();
        initClickListener();
        initData();
    }

    public void onClick(View arg0) {
        switch (arg0.getId()) {
            case R.id.iv_back /*2131296529*/:
                finish();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public int count(JSONArray jsons2, String type) {
        int tmpCount = 0;
        boolean flag2 = false;
        for (int i = 0; i < jsons2.length(); i++) {
            if (type.equals(jsons2.optJSONObject(i).optString("timespan"))) {
                flag2 = true;
                tmpCount += jsons2.optJSONObject(i).optInt("availnum");
            }
        }
        if (flag2) {
            return tmpCount;
        }
        return -1;
    }

    public void refreshActivity(Object... params) {
        this.pDialog.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject result = (JSONObject) params[2];
            int resultCode = result.optJSONObject("res").optInt("st");
            String msg = result.optJSONObject("res").optString("msg");
            if (resultCode == 0) {
                switch (requestType) {
                    case 307:
                        this.jsons = result.optJSONObject("inf").optJSONArray("plans");
                        loadData(requestType, this.jsons);
                        return;
                    default:
                        return;
                }
            } else {
                Toast.makeText(this, msg, 0).show();
            }
        } else {
            Toast.makeText(this, getResources().getString(R.string.service_ex), 0).show();
        }
    }

    public void initViewId() {
        this.ivBack = (Button) findViewById(R.id.iv_back);
        this.tvTitle = (TextView) findViewById(R.id.tv_title);
        this.tvOrder1 = (TextView) findViewById(R.id.tv_order_1);
        this.tvOrder2 = (TextView) findViewById(R.id.tv_order_2);
        this.tvOrder3 = (TextView) findViewById(R.id.tv_order_3);
        this.tvOrder4 = (TextView) findViewById(R.id.tv_order_4);
        this.tvOrder5 = (TextView) findViewById(R.id.tv_order_5);
        this.tvOrder6 = (TextView) findViewById(R.id.tv_order_6);
        this.btnOrder1 = (Button) findViewById(R.id.btn_order_1);
        this.btnOrder2 = (Button) findViewById(R.id.btn_order_2);
        this.btnOrder3 = (Button) findViewById(R.id.btn_order_3);
        this.btnOrder4 = (Button) findViewById(R.id.btn_order_4);
        this.btnOrder5 = (Button) findViewById(R.id.btn_order_5);
        this.btnOrder6 = (Button) findViewById(R.id.btn_order_6);
        this.tvTime1 = (TextView) findViewById(R.id.tv_time_1);
        this.tvTime2 = (TextView) findViewById(R.id.tv_time_2);
        this.tvTime3 = (TextView) findViewById(R.id.tv_time_3);
        this.btnDayForward = (Button) findViewById(R.id.btn_day_forward);
        this.btnDayNext = (Button) findViewById(R.id.btn_day_next);
        this.btnFavDoctor = (Button) findViewById(R.id.btn_favdoctor);
        this.tvOrderCount = (TextView) findViewById(R.id.tv_order_count);
        this.tvDoctorInfo = (TextView) findViewById(R.id.tv_doctor_info);
        this.tvDoctorName = (TextView) findViewById(R.id.tv_doctor_name);
        this.ivDoctorPhoto = (ImageView) findViewById(R.id.iv_doctor_photo);
    }

    public void initClickListener() {
        this.ivBack.setOnClickListener(this);
    }

    public void loadData(int requestType, Object json) {
        JSONArray plan = (JSONArray) json;
        JSONArray daysplan = plan.optJSONObject(0).optJSONArray("daysplan");
        this.ivDoctorPhoto.setImageBitmap(ImageUtil.getBitmapFromByte(plan.optJSONObject(0).optString("doctorpic")));
        this.tvDoctorInfo.setText(plan.optJSONObject(0).optString("doctorlevel"));
        this.tvTitle.setText(plan.optJSONObject(0).optString("doctorname"));
        this.tvDoctorName.setText(plan.optJSONObject(0).optString("doctorname"));
        this.tvOrderCount.setText(String.valueOf(getResources().getString(R.string.order_num)) + plan.optJSONObject(0).optInt("totalavailnum"));
        this.btnDayForward.setOnClickListener(new PageClickListener(daysplan, 0));
        this.btnDayNext.setOnClickListener(new PageClickListener(daysplan, 1));
        if (daysplan.length() > 3) {
            this.btnDayNext.setBackgroundResource(R.drawable.day_next_selector);
            this.btnDayNext.setEnabled(true);
        }
        for (int i = 0; i < daysplan.length(); i++) {
            JSONObject jsonObject = daysplan.optJSONObject(i);
            switch (i) {
                case 0:
                    int num = count(jsonObject.optJSONArray("planlist"), getResources().getString(R.string.morning));
                    if (num > 0) {
                        this.tvOrder1.setText(String.valueOf(getResources().getString(R.string.order_num)) + num);
                        this.btnOrder1.setOnClickListener(new orderClickListener(jsonObject, getResources().getString(R.string.morning)));
                        this.btnOrder1.setVisibility(0);
                    } else if (num == 0) {
                        this.tvOrder1.setText(getResources().getString(R.string.order_not));
                        this.btnOrder1.setVisibility(0);
                        this.btnOrder1.setText(getResources().getString(R.string.full));
                        this.btnOrder1.setBackgroundResource(R.drawable.ygkz_order_doctor_button_dark);
                        this.btnOrder1.setEnabled(true);
                    } else {
                        this.tvOrder1.setText(PoiTypeDef.All);
                        this.btnOrder1.setVisibility(8);
                    }
                    int num2 = count(jsonObject.optJSONArray("planlist"), getResources().getString(R.string.afternoon));
                    if (num2 > 0) {
                        this.tvOrder4.setText(String.valueOf(getResources().getString(R.string.order_num)) + num2);
                        this.btnOrder4.setOnClickListener(new orderClickListener(jsonObject, getResources().getString(R.string.afternoon)));
                        this.btnOrder4.setVisibility(0);
                    } else if (num2 == 0) {
                        this.tvOrder4.setText(getResources().getString(R.string.order_not));
                        this.btnOrder4.setText(getResources().getString(R.string.full));
                        this.btnOrder4.setVisibility(0);
                        this.btnOrder4.setBackgroundResource(R.drawable.ygkz_order_doctor_button_dark);
                        this.btnOrder4.setEnabled(true);
                    } else {
                        this.tvOrder4.setText(PoiTypeDef.All);
                        this.btnOrder4.setVisibility(8);
                    }
                    this.tvTime1.setText(DateUtil.replaceDate(jsonObject.optString("day")));
                    break;
                case 1:
                    int num3 = count(jsonObject.optJSONArray("planlist"), getResources().getString(R.string.morning));
                    if (num3 > 0) {
                        this.tvOrder2.setText(String.valueOf(getResources().getString(R.string.order_num)) + num3);
                        this.btnOrder2.setOnClickListener(new orderClickListener(jsonObject, getResources().getString(R.string.morning)));
                        this.btnOrder2.setVisibility(0);
                    } else if (num3 == 0) {
                        this.tvOrder2.setText(getResources().getString(R.string.order_not));
                        this.btnOrder2.setVisibility(0);
                        this.btnOrder2.setText(getResources().getString(R.string.full));
                        this.btnOrder2.setBackgroundResource(R.drawable.ygkz_order_doctor_button_dark);
                        this.btnOrder2.setEnabled(true);
                    } else {
                        this.tvOrder2.setText(PoiTypeDef.All);
                        this.btnOrder2.setVisibility(8);
                    }
                    int num4 = count(jsonObject.optJSONArray("planlist"), getResources().getString(R.string.afternoon));
                    if (num4 > 0) {
                        this.tvOrder5.setText(String.valueOf(getResources().getString(R.string.order_num)) + num4);
                        this.btnOrder5.setOnClickListener(new orderClickListener(jsonObject, getResources().getString(R.string.afternoon)));
                        this.btnOrder5.setVisibility(0);
                    } else if (num4 == 0) {
                        this.tvOrder5.setText(getResources().getString(R.string.order_not));
                        this.btnOrder5.setVisibility(0);
                        this.btnOrder5.setText(getResources().getString(R.string.full));
                        this.btnOrder5.setBackgroundResource(R.drawable.ygkz_order_doctor_button_dark);
                        this.btnOrder5.setEnabled(true);
                    } else {
                        this.tvOrder5.setText(PoiTypeDef.All);
                        this.btnOrder5.setVisibility(8);
                    }
                    this.tvTime2.setText(DateUtil.replaceDate(jsonObject.optString("day")));
                    break;
                case 2:
                    int num5 = count(jsonObject.optJSONArray("planlist"), getResources().getString(R.string.morning));
                    if (num5 > 0) {
                        this.tvOrder3.setText(String.valueOf(getResources().getString(R.string.order_num)) + num5);
                        this.btnOrder3.setOnClickListener(new orderClickListener(jsonObject, getResources().getString(R.string.morning)));
                        this.btnOrder3.setVisibility(0);
                    } else if (num5 == 0) {
                        this.tvOrder3.setText(getResources().getString(R.string.order_not));
                        this.btnOrder3.setVisibility(0);
                        this.btnOrder3.setText(getResources().getString(R.string.full));
                        this.btnOrder3.setBackgroundResource(R.drawable.ygkz_order_doctor_button_dark);
                        this.btnOrder3.setEnabled(true);
                    } else {
                        this.tvOrder3.setText(PoiTypeDef.All);
                        this.btnOrder3.setVisibility(8);
                    }
                    int num6 = count(jsonObject.optJSONArray("planlist"), getResources().getString(R.string.afternoon));
                    if (num6 > 0) {
                        this.tvOrder6.setText(String.valueOf(getResources().getString(R.string.order_num)) + num6);
                        this.btnOrder6.setOnClickListener(new orderClickListener(jsonObject, getResources().getString(R.string.afternoon)));
                        this.btnOrder6.setVisibility(0);
                    } else if (num6 == 0) {
                        this.tvOrder6.setText(getResources().getString(R.string.order_not));
                        this.btnOrder6.setVisibility(0);
                        this.btnOrder6.setText(getResources().getString(R.string.full));
                        this.btnOrder6.setBackgroundResource(R.drawable.ygkz_order_doctor_button_dark);
                        this.btnOrder6.setEnabled(true);
                    } else {
                        this.tvOrder6.setText(PoiTypeDef.All);
                        this.btnOrder6.setVisibility(8);
                    }
                    this.tvTime3.setText(DateUtil.replaceDate(jsonObject.optString("day")));
                    break;
            }
        }
    }

    private class orderClickListener implements View.OnClickListener {
        private JSONObject jsonObject;
        private String type;

        public orderClickListener(JSONObject jsonObject2, String type2) {
            this.jsonObject = jsonObject2;
            this.type = type2;
        }

        public void onClick(View v) {
            Intent intent = new Intent(DoctorPlanActivity.this, SelectClinicTimeActivity.class);
            intent.putExtra("jsons", this.jsonObject.toString());
            intent.putExtra(a.b, this.type);
            DoctorPlanActivity.this.startActivity(intent);
        }
    }

    private class PageClickListener implements View.OnClickListener {
        private JSONArray jsons;
        private int type;

        public PageClickListener(JSONArray jsons2, int type2) {
            this.jsons = jsons2;
            this.type = type2;
        }

        public void onClick(View v) {
            setPlan();
        }

        private int indexTime() {
            String time1 = DoctorPlanActivity.this.tvTime1.getText().toString();
            String charSequence = DoctorPlanActivity.this.tvTime2.getText().toString();
            String time3 = DoctorPlanActivity.this.tvTime3.getText().toString();
            if (this.type == 0) {
                if (time1.length() > 0) {
                    for (int i = 0; i < this.jsons.length(); i++) {
                        if (time1.equals(DateUtil.replaceDate(this.jsons.optJSONObject(i).optString("day")))) {
                            return i;
                        }
                    }
                }
            } else if (time3.length() > 0) {
                for (int i2 = 0; i2 < this.jsons.length(); i2++) {
                    if (time3.equals(DateUtil.replaceDate(this.jsons.optJSONObject(i2).optString("day")))) {
                        return i2 + 1;
                    }
                }
            }
            return -1;
        }

        private void setData(int index, int i) {
            int flag;
            if (this.type != 0) {
                flag = i - index;
            } else if (i < 3) {
                flag = i;
            } else {
                flag = i - index;
            }
            switch (flag) {
                case 0:
                    int num = DoctorPlanActivity.this.count(this.jsons.optJSONObject(i).optJSONArray("planlist"), DoctorPlanActivity.this.getResources().getString(R.string.morning));
                    DoctorPlanActivity.this.tvTime1.setVisibility(0);
                    DoctorPlanActivity.this.tvTime1.setText(DateUtil.replaceDate(this.jsons.optJSONObject(i).optString("day")));
                    DoctorPlanActivity.this.tvOrder1.setVisibility(0);
                    if (num > 0) {
                        DoctorPlanActivity.this.tvOrder1.setText(String.valueOf(DoctorPlanActivity.this.getResources().getString(R.string.order_num)) + num);
                        DoctorPlanActivity.this.btnOrder1.setOnClickListener(new orderClickListener(this.jsons.optJSONObject(i), DoctorPlanActivity.this.getResources().getString(R.string.morning)));
                        DoctorPlanActivity.this.btnOrder1.setVisibility(0);
                    } else if (num == 0) {
                        DoctorPlanActivity.this.tvOrder1.setVisibility(0);
                        DoctorPlanActivity.this.tvOrder1.setText(DoctorPlanActivity.this.getResources().getString(R.string.order_not));
                        DoctorPlanActivity.this.btnOrder1.setVisibility(0);
                        DoctorPlanActivity.this.btnOrder1.setText(DoctorPlanActivity.this.getResources().getString(R.string.full));
                        DoctorPlanActivity.this.btnOrder1.setBackgroundResource(R.drawable.ygkz_order_doctor_button_dark);
                        DoctorPlanActivity.this.btnOrder1.setEnabled(true);
                    } else {
                        DoctorPlanActivity.this.tvOrder1.setText(PoiTypeDef.All);
                        DoctorPlanActivity.this.btnOrder1.setVisibility(8);
                    }
                    int num2 = DoctorPlanActivity.this.count(this.jsons.optJSONObject(i).optJSONArray("planlist"), DoctorPlanActivity.this.getResources().getString(R.string.afternoon));
                    DoctorPlanActivity.this.tvOrder4.setVisibility(0);
                    if (num2 > 0) {
                        DoctorPlanActivity.this.tvOrder4.setText(String.valueOf(DoctorPlanActivity.this.getResources().getString(R.string.order_num)) + num2);
                        DoctorPlanActivity.this.btnOrder4.setOnClickListener(new orderClickListener(this.jsons.optJSONObject(i), DoctorPlanActivity.this.getResources().getString(R.string.afternoon)));
                        DoctorPlanActivity.this.btnOrder4.setVisibility(0);
                    } else if (num2 == 0) {
                        DoctorPlanActivity.this.tvOrder4.setText(DoctorPlanActivity.this.getResources().getString(R.string.order_not));
                        DoctorPlanActivity.this.btnOrder4.setText(DoctorPlanActivity.this.getResources().getString(R.string.full));
                        DoctorPlanActivity.this.btnOrder4.setVisibility(0);
                        DoctorPlanActivity.this.btnOrder4.setBackgroundResource(R.drawable.ygkz_order_doctor_button_dark);
                        DoctorPlanActivity.this.btnOrder4.setEnabled(true);
                    } else {
                        DoctorPlanActivity.this.tvOrder4.setText(PoiTypeDef.All);
                        DoctorPlanActivity.this.btnOrder4.setVisibility(8);
                    }
                    DoctorPlanActivity.this.tvTime1.setText(DateUtil.replaceDate(this.jsons.optJSONObject(i).optString("day")));
                    return;
                case 1:
                    int num3 = DoctorPlanActivity.this.count(this.jsons.optJSONObject(i).optJSONArray("planlist"), DoctorPlanActivity.this.getResources().getString(R.string.morning));
                    DoctorPlanActivity.this.tvTime2.setVisibility(0);
                    DoctorPlanActivity.this.tvTime2.setText(DateUtil.replaceDate(this.jsons.optJSONObject(i).optString("day")));
                    DoctorPlanActivity.this.tvOrder2.setVisibility(0);
                    if (num3 > 0) {
                        DoctorPlanActivity.this.tvOrder2.setText(String.valueOf(DoctorPlanActivity.this.getResources().getString(R.string.order_num)) + num3);
                        DoctorPlanActivity.this.btnOrder2.setOnClickListener(new orderClickListener(this.jsons.optJSONObject(i), DoctorPlanActivity.this.getResources().getString(R.string.morning)));
                        DoctorPlanActivity.this.btnOrder2.setVisibility(0);
                    } else if (num3 == 0) {
                        DoctorPlanActivity.this.tvOrder2.setText(DoctorPlanActivity.this.getResources().getString(R.string.order_not));
                        DoctorPlanActivity.this.btnOrder2.setVisibility(0);
                        DoctorPlanActivity.this.btnOrder2.setText(DoctorPlanActivity.this.getResources().getString(R.string.full));
                        DoctorPlanActivity.this.btnOrder2.setBackgroundResource(R.drawable.ygkz_order_doctor_button_dark);
                        DoctorPlanActivity.this.btnOrder2.setEnabled(true);
                    } else {
                        DoctorPlanActivity.this.tvOrder2.setText(PoiTypeDef.All);
                        DoctorPlanActivity.this.btnOrder2.setVisibility(8);
                    }
                    int num4 = DoctorPlanActivity.this.count(this.jsons.optJSONObject(i).optJSONArray("planlist"), DoctorPlanActivity.this.getResources().getString(R.string.afternoon));
                    DoctorPlanActivity.this.tvOrder5.setVisibility(0);
                    if (num4 > 0) {
                        DoctorPlanActivity.this.tvOrder5.setText(String.valueOf(DoctorPlanActivity.this.getResources().getString(R.string.order_num)) + num4);
                        DoctorPlanActivity.this.btnOrder5.setOnClickListener(new orderClickListener(this.jsons.optJSONObject(i), DoctorPlanActivity.this.getResources().getString(R.string.afternoon)));
                        DoctorPlanActivity.this.btnOrder5.setVisibility(0);
                    } else if (num4 == 0) {
                        DoctorPlanActivity.this.tvOrder5.setText(DoctorPlanActivity.this.getResources().getString(R.string.order_not));
                        DoctorPlanActivity.this.btnOrder5.setVisibility(0);
                        DoctorPlanActivity.this.btnOrder5.setText(DoctorPlanActivity.this.getResources().getString(R.string.full));
                        DoctorPlanActivity.this.btnOrder5.setBackgroundResource(R.drawable.ygkz_order_doctor_button_dark);
                        DoctorPlanActivity.this.btnOrder5.setEnabled(true);
                    } else {
                        DoctorPlanActivity.this.tvOrder5.setText(PoiTypeDef.All);
                        DoctorPlanActivity.this.btnOrder5.setVisibility(8);
                    }
                    DoctorPlanActivity.this.tvTime2.setText(DateUtil.replaceDate(this.jsons.optJSONObject(i).optString("day")));
                    return;
                case 2:
                    int num5 = DoctorPlanActivity.this.count(this.jsons.optJSONObject(i).optJSONArray("planlist"), DoctorPlanActivity.this.getResources().getString(R.string.morning));
                    DoctorPlanActivity.this.tvTime3.setVisibility(0);
                    DoctorPlanActivity.this.tvTime3.setText(DateUtil.replaceDate(this.jsons.optJSONObject(i).optString("day")));
                    DoctorPlanActivity.this.tvOrder3.setVisibility(0);
                    if (num5 > 0) {
                        DoctorPlanActivity.this.tvOrder3.setText(String.valueOf(DoctorPlanActivity.this.getResources().getString(R.string.order_num)) + num5);
                        DoctorPlanActivity.this.btnOrder3.setOnClickListener(new orderClickListener(this.jsons.optJSONObject(i), DoctorPlanActivity.this.getResources().getString(R.string.morning)));
                        DoctorPlanActivity.this.btnOrder3.setVisibility(0);
                    } else if (num5 == 0) {
                        DoctorPlanActivity.this.tvOrder3.setText(DoctorPlanActivity.this.getResources().getString(R.string.order_not));
                        DoctorPlanActivity.this.btnOrder3.setVisibility(0);
                        DoctorPlanActivity.this.btnOrder3.setText(DoctorPlanActivity.this.getResources().getString(R.string.full));
                        DoctorPlanActivity.this.btnOrder3.setBackgroundResource(R.drawable.ygkz_order_doctor_button_dark);
                        DoctorPlanActivity.this.btnOrder3.setEnabled(true);
                    } else {
                        DoctorPlanActivity.this.tvOrder3.setText(PoiTypeDef.All);
                        DoctorPlanActivity.this.btnOrder3.setVisibility(8);
                    }
                    int num6 = DoctorPlanActivity.this.count(this.jsons.optJSONObject(i).optJSONArray("planlist"), DoctorPlanActivity.this.getResources().getString(R.string.afternoon));
                    DoctorPlanActivity.this.tvOrder6.setVisibility(0);
                    if (num6 > 0) {
                        DoctorPlanActivity.this.tvOrder6.setText(String.valueOf(DoctorPlanActivity.this.getResources().getString(R.string.order_num)) + num6);
                        DoctorPlanActivity.this.btnOrder6.setOnClickListener(new orderClickListener(this.jsons.optJSONObject(i), DoctorPlanActivity.this.getResources().getString(R.string.afternoon)));
                        DoctorPlanActivity.this.btnOrder6.setVisibility(0);
                    } else if (num6 == 0) {
                        DoctorPlanActivity.this.tvOrder6.setText(DoctorPlanActivity.this.getResources().getString(R.string.order_not));
                        DoctorPlanActivity.this.btnOrder6.setVisibility(0);
                        DoctorPlanActivity.this.btnOrder6.setText(DoctorPlanActivity.this.getResources().getString(R.string.full));
                        DoctorPlanActivity.this.btnOrder6.setBackgroundResource(R.drawable.ygkz_order_doctor_button_dark);
                        DoctorPlanActivity.this.btnOrder6.setEnabled(true);
                    } else {
                        DoctorPlanActivity.this.tvOrder6.setText(PoiTypeDef.All);
                        DoctorPlanActivity.this.btnOrder6.setVisibility(8);
                    }
                    DoctorPlanActivity.this.tvTime3.setText(DateUtil.replaceDate(this.jsons.optJSONObject(i).optString("day")));
                    return;
                default:
                    return;
            }
        }

        private void initData() {
            DoctorPlanActivity.this.tvOrder1.setVisibility(8);
            DoctorPlanActivity.this.tvOrder2.setVisibility(8);
            DoctorPlanActivity.this.tvOrder3.setVisibility(8);
            DoctorPlanActivity.this.tvOrder4.setVisibility(8);
            DoctorPlanActivity.this.tvOrder5.setVisibility(8);
            DoctorPlanActivity.this.tvOrder6.setVisibility(8);
            DoctorPlanActivity.this.btnOrder1.setVisibility(8);
            DoctorPlanActivity.this.btnOrder2.setVisibility(8);
            DoctorPlanActivity.this.btnOrder3.setVisibility(8);
            DoctorPlanActivity.this.btnOrder4.setVisibility(8);
            DoctorPlanActivity.this.btnOrder5.setVisibility(8);
            DoctorPlanActivity.this.btnOrder6.setVisibility(8);
            DoctorPlanActivity.this.tvTime1.setVisibility(8);
            DoctorPlanActivity.this.tvTime2.setVisibility(8);
            DoctorPlanActivity.this.tvTime3.setVisibility(8);
        }

        private void setPlan() {
            int index = indexTime();
            if (index != -1) {
                initData();
                if (this.type == 0) {
                    if (index - 3 <= 0) {
                        DoctorPlanActivity.this.btnDayForward.setBackgroundResource(R.drawable.yl_button_doctor_day_forward_none_ex_big);
                        DoctorPlanActivity.this.btnDayForward.setEnabled(false);
                    } else {
                        DoctorPlanActivity.this.btnDayForward.setBackgroundResource(R.drawable.day_forward_selector);
                        DoctorPlanActivity.this.btnDayForward.setEnabled(true);
                    }
                    DoctorPlanActivity.this.btnDayNext.setBackgroundResource(R.drawable.day_next_selector);
                    DoctorPlanActivity.this.btnDayNext.setVisibility(0);
                    DoctorPlanActivity.this.btnDayNext.setEnabled(true);
                    for (int i = index; i >= index - 3; i--) {
                        if (i >= 0) {
                            setData(index - 3, i);
                        }
                    }
                    return;
                }
                DoctorPlanActivity.this.btnDayForward.setBackgroundResource(R.drawable.day_forward_selector);
                DoctorPlanActivity.this.btnDayForward.setEnabled(true);
                if (index + 3 > this.jsons.length()) {
                    DoctorPlanActivity.this.btnDayNext.setBackgroundResource(R.drawable.yl_button_doctor_day_next_none_ex_big);
                    DoctorPlanActivity.this.btnDayNext.setEnabled(false);
                }
                for (int i2 = index; i2 < index + 3; i2++) {
                    if (i2 < this.jsons.length()) {
                        setData(index, i2);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.doctorid = getIntent().getIntExtra("doctorid", 0);
        this.doctorName = getIntent().getStringExtra("doctorName");
        this.tvTitle.setText(this.doctorName);
        this.flag = getIntent().getBooleanExtra("flag", false);
        if (!this.flag) {
            this.btnFavDoctor.setVisibility(8);
        }
        this.pDialog = new ProgressDialog(this);
        this.pDialog.setMessage(getResources().getString(R.string.loading_ok));
        this.pDialog.show();
        sendHttpRequest(307, initRequestParameter(307));
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        parameters.add("userid", UserEntity.getInstance().getInfo().getAccId());
        parameters.add("doctorid", Integer.valueOf(this.doctorid));
        return parameters;
    }
}
