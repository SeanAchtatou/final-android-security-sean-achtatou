package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.entity.UserInfoEntity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.request.RequestTask;
import com.womenchild.hospital.util.ClientLogUtil;
import org.json.JSONObject;

public class PersonSeekingMedicalServiceEditActivity extends BaseRequestActivity implements View.OnClickListener {
    private static final String TAG = "PSMSEAct";
    private String citizenCard;
    private String diagnosisCard;
    private String email;
    private EditText et_citizen_card;
    private EditText et_diagnosis_card;
    private EditText et_email;
    private EditText et_name;
    private EditText et_phone;
    private EditText et_pid;
    private EditText et_resident_health_card;
    private EditText et_social_security_card;
    private ImageView iv_return;
    private String name;
    private String patientid;
    private String phone;
    private String pid;
    private ProgressDialog progressDialog;
    private String residentHealthCard;
    private String socialSecurityCard;
    private TextView tv_submit;
    private UserEntity userEntity;
    private String userId;
    private UserInfoEntity userInfoEntity;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.person_seeking_medical_service_edit);
        initViewId();
        initClickListener();
        initData();
    }

    public void refreshActivity(Object... params) {
        this.progressDialog.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        boolean status = ((Boolean) params[1]).booleanValue();
        if (status) {
            JSONObject result = (JSONObject) params[2];
            ClientLogUtil.i(TAG, "refreshActivity() Request type:" + requestType);
            ClientLogUtil.i(TAG, "refreshActivity() Status:" + status);
            ClientLogUtil.i(TAG, "refreshActivity() Result:" + result);
            if (requestType == 105 || requestType == 104) {
                validateData(result);
                return;
            }
            return;
        }
        Toast.makeText(this, getResources().getString(R.string.network_connect_failed_prompt), 0).show();
    }

    private void validateData(JSONObject result) {
        try {
            if ("0".equals(result.getJSONObject("res").getString("st"))) {
                ClientLogUtil.d(TAG, "validateData() Result: " + result.toString());
                Toast.makeText(this, result.getJSONObject("res").getString("msg"), 0).show();
                this.userInfoEntity.setName(this.name);
                this.userInfoEntity.setMobile(this.phone);
                this.userInfoEntity.setSocialSecurity(this.socialSecurityCard);
                this.userInfoEntity.setEmail(this.email);
                this.userInfoEntity.setIdCard(this.pid);
                this.userInfoEntity.setHealthyCard(this.residentHealthCard);
                this.userInfoEntity.setCitizenCard(this.citizenCard);
                this.userInfoEntity.setDefaultPatientCard(this.diagnosisCard);
                this.userInfoEntity.setAddress(PoiTypeDef.All);
                finish();
                return;
            }
            Toast.makeText(this, result.getJSONObject("res").getString("msg"), 0).show();
        } catch (Exception ex) {
            ex.printStackTrace();
            ClientLogUtil.e(TAG, "Exception:" + ex.getMessage() + "," + ex.toString());
        }
    }

    public void initViewId() {
        this.iv_return = (ImageView) findViewById(R.id.iv_return);
        this.tv_submit = (TextView) findViewById(R.id.tv_submit);
        this.et_diagnosis_card = (EditText) findViewById(R.id.et_diagnosis_card);
        this.et_name = (EditText) findViewById(R.id.et_name);
        this.et_phone = (EditText) findViewById(R.id.et_phone);
        this.et_pid = (EditText) findViewById(R.id.et_pid);
        this.et_email = (EditText) findViewById(R.id.et_email);
        this.et_social_security_card = (EditText) findViewById(R.id.et_social_security_card);
        this.et_resident_health_card = (EditText) findViewById(R.id.et_resident_health_card);
        this.et_citizen_card = (EditText) findViewById(R.id.et_citizen_card);
        this.progressDialog = new ProgressDialog(this);
        this.progressDialog.setMessage(getResources().getString(R.string.submit_pw));
        this.progressDialog.setCancelable(false);
        this.progressDialog.setCanceledOnTouchOutside(false);
    }

    public void initClickListener() {
        this.iv_return.setOnClickListener(this);
        this.tv_submit.setOnClickListener(this);
    }

    public void loadData(int requestType, Object data) {
    }

    public void onClick(View v) {
        if (this.iv_return == v) {
            finish();
        } else if (this.tv_submit != v) {
        } else {
            if (this.et_name.getText().toString().trim().equals(PoiTypeDef.All)) {
                Toast.makeText(this, getResources().getString(R.string.name_null_prompt), 0).show();
                this.et_name.requestFocus();
                return;
            }
            this.progressDialog.show();
            this.name = this.et_name.getText().toString().trim();
            this.phone = this.et_phone.getText().toString().trim();
            this.socialSecurityCard = this.et_social_security_card.getText().toString().trim();
            this.email = this.et_email.getText().toString().trim();
            this.pid = this.et_pid.getText().toString().trim();
            this.residentHealthCard = this.et_resident_health_card.getText().toString().trim();
            this.citizenCard = this.et_citizen_card.getText().toString().trim();
            this.diagnosisCard = this.et_diagnosis_card.getText().toString().trim();
            if (!this.userInfoEntity.getPatientid().equals(PoiTypeDef.All)) {
                RequestTask.getInstance().sendHttpRequest(this, String.valueOf((int) HttpRequestParameters.EDIT_USER_INFO), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.EDIT_USER_INFO)));
            } else {
                RequestTask.getInstance().sendHttpRequest(this, String.valueOf((int) HttpRequestParameters.ADD_USER_INFO), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.ADD_USER_INFO)));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        return super.onKeyUp(keyCode, event);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.userEntity = UserEntity.getInstance();
        this.userInfoEntity = this.userEntity.getInfo();
        this.et_name.setText(this.userInfoEntity.getName());
        this.et_phone.setText(this.userInfoEntity.getMobile());
        this.et_pid.setText(this.userInfoEntity.getIdCard());
        this.et_email.setText(this.userInfoEntity.getEmail());
        this.et_social_security_card.setText(this.userInfoEntity.getSocialSecurity());
        this.et_resident_health_card.setText(this.userInfoEntity.getHealthyCard());
        this.et_citizen_card.setText(this.userInfoEntity.getCitizenCard());
        this.et_diagnosis_card.setText(this.userInfoEntity.getDefaultPatientCard());
        this.userId = this.userInfoEntity.getUserid();
        this.patientid = this.userInfoEntity.getPatientid();
        ClientLogUtil.d(TAG, "initData() UserName:" + this.userEntity.getUsername() + ",Pwd:" + this.userEntity.getPassword());
        ClientLogUtil.v(TAG, "initData() Name:" + this.userInfoEntity.getName() + ",UserID:" + this.userInfoEntity.getUserid() + ",Mobile:" + this.userInfoEntity.getMobile() + ",IdCard:" + this.userInfoEntity.getIdCard() + ",Email:" + this.userInfoEntity.getEmail() + ",SocialSecurity:" + this.userInfoEntity.getSocialSecurity() + ",HealthyCard:" + this.userInfoEntity.getHealthyCard() + ",CitizenCard:" + this.userInfoEntity.getCitizenCard() + ",DefaultPatientCard:" + this.userInfoEntity.getDefaultPatientCard() + ",PatientId:" + this.userInfoEntity.getPatientid());
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        if (Integer.parseInt(String.valueOf(requestCode)) == 105 || Integer.parseInt(String.valueOf(requestCode)) == 104) {
            if (this.patientid == null) {
                this.patientid = PoiTypeDef.All;
            }
            if (this.phone == null) {
                this.phone = PoiTypeDef.All;
            }
            if (this.socialSecurityCard == null) {
                this.socialSecurityCard = PoiTypeDef.All;
            }
            if (this.email == null) {
                this.email = PoiTypeDef.All;
            }
            if (this.pid == null) {
                this.pid = PoiTypeDef.All;
            }
            if (this.residentHealthCard == null) {
                this.residentHealthCard = PoiTypeDef.All;
            }
            if (this.citizenCard == null) {
                this.citizenCard = PoiTypeDef.All;
            }
            if (this.diagnosisCard == null) {
                this.diagnosisCard = PoiTypeDef.All;
            }
            parameters.add("userid", this.userId);
            parameters.add("patientid", this.patientid);
            parameters.add("patientname", this.name);
            parameters.add("mobile", this.phone);
            parameters.add("socialsecuritycard", this.socialSecurityCard);
            parameters.add("email", this.email);
            parameters.add("idcard", this.pid);
            parameters.add("healthycard", this.residentHealthCard);
            parameters.add("citizzencard", this.citizenCard);
            if (Integer.parseInt(String.valueOf(requestCode)) == 104) {
                parameters.add("defaultpatientcard", this.diagnosisCard);
            } else {
                parameters.add("defaultpatientcard", PoiTypeDef.All);
            }
            parameters.add("gender", "0");
            parameters.add("address", PoiTypeDef.All);
            for (int i = 0; i < parameters.size(); i++) {
                ClientLogUtil.d(TAG, "initRequestParams() " + parameters.getKey(i) + ":" + parameters.getValue(i));
            }
        }
        return parameters;
    }
}
