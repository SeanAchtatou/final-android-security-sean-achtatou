package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.womenchild.hospital.adapter.DoctorOrderedAdapter;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.entity.DoctorEntity;
import com.womenchild.hospital.entity.RecordEntity;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.AppParameters;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import org.json.JSONObject;

public class SearchDoctorByOrderedActivity extends BaseRequestActivity implements View.OnClickListener {
    private static final String TAG = "SDBOActivity";
    public static List<RecordEntity> clinicList;
    public static List<RecordEntity> noClinicList;
    public static List<RecordEntity> tempClinicList;
    private Button bakcIv;
    private Button collectIbtn;
    private DoctorOrderedAdapter dAdapter;
    private List<DoctorEntity> doctorList;
    private Intent intent;
    private ListView lv_ordered;
    /* access modifiers changed from: private */
    public Context mContext = this;
    private TextView noDataTv;
    private ProgressDialog pd;
    private ProgressDialog pdDialog;
    private Button roomIbtn;
    private ImageView searchIv;
    private Button sickIbtn;
    private String userID;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.search_doctor_by_ordered);
        initViewId();
        initClickListener();
        initData();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void refreshActivity(Object... params) {
        this.pd.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject result = (JSONObject) params[2];
            switch (requestType) {
                case HttpRequestParameters.READ_REG_LIST /*125*/:
                    loadData(HttpRequestParameters.READ_REG_LIST, result);
                    return;
                default:
                    return;
            }
        } else {
            Toast.makeText(this, (int) R.string.network_connect_failed_prompt, 0).show();
        }
    }

    public void initViewId() {
        this.lv_ordered = (ListView) findViewById(R.id.lv_ordered);
        this.bakcIv = (Button) findViewById(R.id.ibtn_home);
        this.searchIv = (ImageView) findViewById(R.id.iv_search);
        this.roomIbtn = (Button) findViewById(R.id.ibtn_room);
        this.collectIbtn = (Button) findViewById(R.id.ibtn_collect);
        this.noDataTv = (TextView) findViewById(R.id.tv_not_data);
    }

    public void initClickListener() {
        this.bakcIv.setOnClickListener(this);
        this.searchIv.setOnClickListener(this);
        this.roomIbtn.setOnClickListener(this);
        this.collectIbtn.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_home:
                finish();
                return;
            case R.id.iv_search:
                Toast.makeText(this, getResources().getString(R.string.not_started), 0).show();
                return;
            case R.id.ibtn_room:
                this.intent = new Intent(this, FindDoctorByRoomActivity.class);
                startActivity(this.intent);
                return;
            case R.id.ibtn_collect:
                this.intent = new Intent(this, SearchDoctorByCollectedActivity.class);
                startActivity(this.intent);
                return;
            default:
                return;
        }
    }

    public void loadData(int requestType, Object data) {
        JSONObject result = (JSONObject) data;
        String st = result.optJSONObject("res").optString("st");
        String optString = result.optJSONObject("res").optString("msg");
        switch (requestType) {
            case HttpRequestParameters.READ_REG_LIST /*125*/:
                if ("0".equals(st)) {
                    bindView(RecordEntity.getList(result));
                    return;
                } else {
                    this.noDataTv.setVisibility(0);
                    return;
                }
            default:
                return;
        }
    }

    private void bindView(List<RecordEntity> list) {
        if (list != null) {
            noClinicList = new ArrayList();
            LinkedHashSet<String> set = new LinkedHashSet<>();
            for (int i = 0; i < list.size(); i++) {
                RecordEntity entity = list.get(i);
                noClinicList.add(entity);
                set.add(entity.getDoctorname());
            }
            tempClinicList = new ArrayList();
            ArrayList<String> noSameList = new ArrayList<>(set);
            for (int j = 0; j < noSameList.size(); j++) {
                int k = 0;
                while (true) {
                    if (k >= noClinicList.size()) {
                        break;
                    } else if (noClinicList.get(k).getDoctorname().equals(noSameList.get(j))) {
                        tempClinicList.add(noClinicList.get(k));
                        break;
                    } else {
                        k++;
                    }
                }
            }
            this.dAdapter = new DoctorOrderedAdapter(this, tempClinicList);
            this.lv_ordered.setAdapter((ListAdapter) this.dAdapter);
            setOnItemListener();
        }
    }

    private void setOnItemListener() {
        this.lv_ordered.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(SearchDoctorByOrderedActivity.this.mContext, FavDoctorActivity.class);
                intent.putExtra("doctorID", SearchDoctorByOrderedActivity.tempClinicList.get(position).getDoctorid());
                SearchDoctorByOrderedActivity.this.startActivity(intent);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.pd = new ProgressDialog(this);
        this.pd.setCancelable(true);
        this.pd.setCanceledOnTouchOutside(false);
        this.pd.setMessage(getResources().getString(R.string.mark_wait));
        this.pd.show();
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.READ_REG_LIST), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.READ_REG_LIST)));
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.READ_REG_LIST /*125*/:
                parameters.add("memberid", String.valueOf(AppParameters.getAPPID()) + "_" + UserEntity.getInstance().getInfo().getUserid());
                break;
        }
        return parameters;
    }
}
