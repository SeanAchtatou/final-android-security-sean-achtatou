package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.womenchild.hospital.adapter.CollectedDoctorAdapter;
import com.womenchild.hospital.adapter.DoctorCollectedAdapter;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.entity.DoctorEntity;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SearchDoctorByCollectedActivity extends BaseRequestActivity implements View.OnClickListener {
    private static final String TAG = "SDBCActivity";
    private Button bakcIv;
    private DoctorCollectedAdapter dAdapter;
    /* access modifiers changed from: private */
    public ArrayList<DoctorEntity> doctorList = new ArrayList<>();
    private Intent intent;
    private AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
        public void onItemClick(AdapterView<?> adapterView, View view, int arg2, long arg3) {
            String doctorID = ((DoctorEntity) SearchDoctorByCollectedActivity.this.doctorList.get(arg2)).getDoctoryID();
            String favID = ((DoctorEntity) SearchDoctorByCollectedActivity.this.doctorList.get(arg2)).getFavID();
            Intent intent = new Intent(SearchDoctorByCollectedActivity.this.mContext, FavDoctorActivity.class);
            intent.putExtra("doctorID", doctorID);
            intent.putExtra("userID", SearchDoctorByCollectedActivity.this.userID);
            intent.putExtra("favID", favID);
            intent.putExtra("favFlag", true);
            SearchDoctorByCollectedActivity.this.startActivityForResult(intent, 0);
        }
    };
    private ListView lv_collected;
    /* access modifiers changed from: private */
    public Context mContext = this;
    private Button orderIbtn;
    private ProgressDialog pdDialog;
    private Button roomIbtn;
    private ImageView searchIv;
    private Button sickIbtn;
    private TextView tipsNullTv;
    /* access modifiers changed from: private */
    public String userID;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.search_doctor_by_collected);
        initViewId();
        initClickListener();
        if (UserEntity.getInstance().getInfo() != null) {
            this.pdDialog = new ProgressDialog(this);
            this.pdDialog.setMessage(getResources().getString(R.string.loading_data));
            this.userID = UserEntity.getInstance().getInfo().getUserid();
            this.pdDialog.show();
            sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.FAVORITES_DOCTOR_LIST), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.FAVORITES_DOCTOR_LIST)));
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void refreshActivity(Object... params) {
        this.pdDialog.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            loadData(requestType, (JSONObject) params[2]);
        } else {
            Toast.makeText(this, (int) R.string.network_connect_failed_prompt, 0).show();
        }
    }

    public void initViewId() {
        this.lv_collected = (ListView) findViewById(R.id.lv_collected);
        this.bakcIv = (Button) findViewById(R.id.ibtn_home);
        this.searchIv = (ImageView) findViewById(R.id.iv_search);
        this.roomIbtn = (Button) findViewById(R.id.ibtn_room);
        this.orderIbtn = (Button) findViewById(R.id.ibtn_order);
        this.tipsNullTv = (TextView) findViewById(R.id.tips_null);
    }

    public void initClickListener() {
        this.lv_collected.setOnItemClickListener(this.itemClickListener);
        this.bakcIv.setOnClickListener(this);
        this.searchIv.setOnClickListener(this);
        this.roomIbtn.setOnClickListener(this);
        this.orderIbtn.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_home:
                finish();
                return;
            case R.id.iv_search:
                Toast.makeText(this, getResources().getString(R.string.not_started), 0).show();
                return;
            case R.id.ibtn_room:
                this.intent = new Intent(this, FindDoctorByRoomActivity.class);
                startActivity(this.intent);
                return;
            case R.id.ibtn_order:
                this.intent = new Intent(this, SearchDoctorByOrderedActivity.class);
                startActivity(this.intent);
                return;
            default:
                return;
        }
    }

    public void loadData(int requestType, Object data) {
        JSONObject jsonObject = (JSONObject) data;
        JSONObject res = jsonObject.optJSONObject("res");
        if (res != null) {
            try {
                if (res.getInt("st") == 0) {
                    JSONArray jsonArray = jsonObject.getJSONArray("inf");
                    Log.d(TAG, new StringBuilder().append(jsonArray).toString());
                    if (jsonArray == null || jsonArray.length() <= 0) {
                        this.tipsNullTv.setVisibility(0);
                        return;
                    }
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Log.d(TAG, jsonArray.getJSONObject(i).getString("doctorname"));
                        DoctorEntity doctorEntity = new DoctorEntity();
                        doctorEntity.setName(jsonArray.getJSONObject(i).getString("doctorname"));
                        doctorEntity.setLevel(jsonArray.getJSONObject(i).getString("level"));
                        doctorEntity.setFavID(jsonArray.getJSONObject(i).getString("favdoctorid"));
                        doctorEntity.setDoctoryID(jsonArray.getJSONObject(i).getString("doctorid"));
                        jsonArray.getJSONObject(i).getString("gender");
                        jsonArray.getJSONObject(i).getString("hospital");
                        this.doctorList.add(doctorEntity);
                    }
                    this.lv_collected.setAdapter((ListAdapter) new CollectedDoctorAdapter(this, this.lv_collected, this.doctorList));
                    this.lv_collected.setVisibility(0);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (UserEntity.getInstance().getInfo() != null) {
            this.pdDialog = new ProgressDialog(this);
            this.pdDialog.setMessage(getResources().getString(R.string.loading_data));
            this.lv_collected.setVisibility(8);
            this.userID = UserEntity.getInstance().getInfo().getUserid();
            sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.FAVORITES_DOCTOR_LIST), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.FAVORITES_DOCTOR_LIST)));
            this.doctorList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        if (this.userID != null) {
            parameters.add("userid", this.userID);
        }
        return parameters;
    }
}
