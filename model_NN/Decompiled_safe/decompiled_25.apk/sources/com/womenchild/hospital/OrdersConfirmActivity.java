package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.AppParameters;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import org.json.JSONException;
import org.json.JSONObject;

public class OrdersConfirmActivity extends BaseRequestActivity implements View.OnClickListener {
    private Button btnSubmit;
    private ImageButton ibtnReturn;
    private JSONObject json;
    private double money;
    private ProgressDialog pDialog;
    private TextView tvDept;
    private TextView tvDoctor;
    private TextView tvMobile;
    private TextView tvMoney;
    private TextView tvPatient;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.order_confirm);
        initViewId();
        initClickListener();
        initData();
    }

    public void onClick(View arg0) {
        switch (arg0.getId()) {
            case R.id.ibtn_return /*2131296570*/:
                finish();
                return;
            case R.id.btn_submit /*2131296648*/:
                this.pDialog.show();
                sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.SUBMIT_ORDER), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.SUBMIT_ORDER)));
                return;
            default:
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void refreshActivity(Object... params) {
        this.pDialog.dismiss();
        int intValue = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject result = (JSONObject) params[2];
            if (result.optJSONObject("res").optInt("st") == 0) {
                Intent intent = new Intent(this, RegisterNoSuccessActivity.class);
                intent.putExtra("order", result.optJSONObject("inf").toString());
                intent.putExtra("money", this.money);
                intent.putExtra("pay", false);
                startActivity(intent);
                return;
            }
            return;
        }
        Toast.makeText(this, (int) R.string.network_connect_failed_prompt, 0).show();
    }

    public void initViewId() {
        this.ibtnReturn = (ImageButton) findViewById(R.id.ibtn_return);
        this.tvDept = (TextView) findViewById(R.id.tv_dept);
        this.tvDoctor = (TextView) findViewById(R.id.tv_doctor);
        this.tvMobile = (TextView) findViewById(R.id.tv_mobile);
        this.tvMoney = (TextView) findViewById(R.id.tv_money);
        this.tvPatient = (TextView) findViewById(R.id.tv_patient);
        this.btnSubmit = (Button) findViewById(R.id.btn_submit);
        this.pDialog = new ProgressDialog(this);
        this.pDialog.setMessage(getResources().getString(R.string.substance_order_yes));
    }

    public void initClickListener() {
        this.ibtnReturn.setOnClickListener(this);
        this.btnSubmit.setOnClickListener(this);
    }

    public void loadData(int requestType, Object data) {
    }

    /* access modifiers changed from: protected */
    public void initData() {
        try {
            this.json = new JSONObject(getIntent().getStringExtra("order"));
            this.money = ((double) new JSONObject(getIntent().getStringExtra("plan")).optInt("fee")) / 100.0d;
            this.tvDept.setText(this.json.optString("deptname"));
            this.tvDoctor.setText(this.json.optString("doctorname"));
            this.tvMobile.setText(this.json.optString("mobile"));
            this.tvMoney.setText(String.valueOf(this.money) + " 元");
            this.tvPatient.setText(this.json.optString("name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = FillInAppointmentInfoActivity.orderEntity.createParameters();
        parameters.add("memberid", String.valueOf(AppParameters.getAPPID()) + "_" + UserEntity.getInstance().getInfo().getAccId());
        return parameters;
    }
}
