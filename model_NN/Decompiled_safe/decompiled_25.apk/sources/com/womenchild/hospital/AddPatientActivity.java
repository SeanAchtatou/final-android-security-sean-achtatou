package com.womenchild.hospital;

import android.os.Bundle;
import android.view.View;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.parameter.UriParameter;

public class AddPatientActivity extends BaseRequestActivity implements View.OnClickListener {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.add_patient);
    }

    public void onClick(View v) {
    }

    public void refreshActivity(Object... params) {
    }

    public void initViewId() {
    }

    public void initClickListener() {
    }

    public void loadData(int requestType, Object data) {
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        return null;
    }
}
