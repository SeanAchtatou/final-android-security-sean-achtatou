package com.womenchild.hospital.ActivityUtil;

import android.app.Activity;
import android.content.Intent;
import android.widget.Toast;
import com.womenchild.hospital.util.ClientLogUtil;
import java.io.Serializable;
import java.util.Map;
import org.apache.http.NameValuePair;

public class ActivityUtil {
    public static boolean OnlineConsultActivity = false;
    public static String Util = "CCLog";

    public static void switchTo(Activity activity, Class<? extends Activity> targetActivity) {
        ClientLogUtil.i(Util, "activity:" + activity + "targetActivity:" + targetActivity);
        switchTo(activity, new Intent(activity, targetActivity));
    }

    public static void switchTo(Activity activity, Class<? extends Activity> targetActivity, int intentFlag) {
        ClientLogUtil.i(Util, "intentFlag:" + intentFlag);
        switchTo(activity, new Intent(activity, targetActivity), intentFlag);
    }

    public static void switchTo(Activity activity, Intent intent) {
        activity.startActivity(intent);
    }

    public static void switchTo(Activity activity, Intent intent, int intentFlag) {
        activity.startActivity(intent);
    }

    public static void switchTo(Activity activity, Class<? extends Activity> targetActivity, Map<String, Object> params) {
        if (params != null) {
            Intent intent = new Intent(activity, targetActivity);
            for (Map.Entry<String, Object> entry : params.entrySet()) {
                setValueToIntent(intent, (String) entry.getKey(), entry.getValue());
            }
            switchTo(activity, intent);
        }
    }

    public static void switchTo(Activity activity, Class<? extends Activity> target, NameValuePair... params) {
        if (params != null) {
            Intent intent = new Intent(activity, target);
            for (NameValuePair param : params) {
                setValueToIntent(intent, param.getName(), param.getValue());
            }
            switchTo(activity, intent);
        }
    }

    public static void toastShow(final Activity activity, final String message) {
        activity.runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(activity, message, 0).show();
            }
        });
    }

    /* JADX WARN: Type inference failed for: r3v1, types: [java.lang.Float[], java.io.Serializable] */
    /* JADX WARN: Type inference failed for: r3v3, types: [java.lang.Double[], java.io.Serializable] */
    /* JADX WARN: Type inference failed for: r3v5, types: [java.lang.Long[], java.io.Serializable] */
    /* JADX WARN: Type inference failed for: r3v7, types: [java.lang.Integer[], java.io.Serializable] */
    /* JADX WARN: Type inference failed for: r3v11, types: [java.lang.Boolean[], java.io.Serializable] */
    public static void setValueToIntent(Intent intent, String key, Object val) {
        if (val instanceof Boolean) {
            intent.putExtra(key, (Boolean) val);
        } else if (val instanceof Boolean[]) {
            intent.putExtra(key, (Serializable) ((Boolean[]) val));
        } else if (val instanceof String) {
            intent.putExtra(key, (String) val);
        } else if (val instanceof String[]) {
            intent.putExtra(key, (String[]) val);
        } else if (val instanceof Integer) {
            intent.putExtra(key, (Integer) val);
        } else if (val instanceof Integer[]) {
            intent.putExtra(key, (Serializable) ((Integer[]) val));
        } else if (val instanceof Long) {
            intent.putExtra(key, (Long) val);
        } else if (val instanceof Long[]) {
            intent.putExtra(key, (Serializable) ((Long[]) val));
        } else if (val instanceof Double) {
            intent.putExtra(key, (Double) val);
        } else if (val instanceof Double[]) {
            intent.putExtra(key, (Serializable) ((Double[]) val));
        } else if (val instanceof Float) {
            intent.putExtra(key, (Float) val);
        } else if (val instanceof Float[]) {
            intent.putExtra(key, (Serializable) ((Float[]) val));
        }
    }
}
