package com.womenchild.hospital;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.amap.mapapi.location.LocationManagerProxy;

public class BackMoneyActivity extends Activity {
    private Button btnHome;
    private TextView tvError;
    private TextView tvStatusTitle;
    private TextView tvTitle;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.back_money_status);
        this.tvTitle = (TextView) findViewById(R.id.tv_title);
        this.tvStatusTitle = (TextView) findViewById(R.id.tv_success_title);
        this.tvError = (TextView) findViewById(R.id.tv_error);
        this.btnHome = (Button) findViewById(R.id.ibtn_home);
        this.btnHome.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(BackMoneyActivity.this, HomeActivity.class);
                intent.setFlags(67108864);
                BackMoneyActivity.this.startActivity(intent);
            }
        });
        if (getIntent().getBooleanExtra(LocationManagerProxy.KEY_STATUS_CHANGED, true)) {
            this.tvTitle.setText(getString(R.string.title_back_money_success));
            this.tvStatusTitle.setText(getString(R.string.title_back_money_success));
            this.tvError.setText(getString(R.string.hint_back_money_success));
            return;
        }
        this.tvTitle.setText(getString(R.string.title_back_money_fail));
        this.tvStatusTitle.setText(getString(R.string.title_back_money_fail));
        this.tvError.setText(getString(R.string.hint_back_money_fail));
    }
}
