package com.womenchild.hospital;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Xml;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.mapapi.location.LocationManagerProxy;
import com.umeng.common.util.e;
import com.upomp.pay.Star_Upomp_Pay;
import com.upomp.pay.help.CreateOriginal;
import com.upomp.pay.help.Create_MerchantX;
import com.upomp.pay.help.GetValue;
import com.upomp.pay.help.Xmlpar;
import com.upomp.pay.httpservice.XmlHttpConnection;
import com.upomp.pay.info.Upomp_Pay_Info;
import com.upomp.pay.info.XmlDefinition;
import com.womenchild.hospital.adapter.BillOrderAdapter;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.entity.AutoPaymentEntity;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.ClientLogUtil;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import org.json.JSONArray;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;

public class PaymentOrderActivity extends BaseRequestActivity implements View.OnClickListener, ExpandableListView.OnGroupClickListener, ExpandableListView.OnChildClickListener {
    InputStream PrivateSign;
    private BillOrderAdapter adapter;
    private JSONArray billOrdes;
    private Button btnBack;
    private ExpandableListView elvOrder;
    private int expandFlag = -1;
    XmlHttpConnection httpConnection;
    private String orderId;
    /* access modifiers changed from: private */
    public ProgressDialog pDialog;
    Star_Upomp_Pay star;
    private TextView tvFee;
    private TextView tvSubmit;
    GetValue values;
    Xmlpar xmlpar;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.payment_order);
        initViewId();
        initClickListener();
        AutoPaymentEntity autoPaymentEntity = WaitPaymentOrderActivity.autoPayment;
        autoPaymentEntity.getPatientName();
        this.tvFee.setText(String.valueOf(getResources().getString(R.string.order_price)) + (Double.valueOf(autoPaymentEntity.getFee()).doubleValue() / 100.0d) + getResources().getString(R.string.price_rmb));
        this.billOrdes = autoPaymentEntity.getBillOrders();
        this.pDialog = new ProgressDialog(this);
        this.adapter = new BillOrderAdapter(this, this.billOrdes);
        this.elvOrder.setAdapter(this.adapter);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void refreshActivity(Object... params) {
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject result = (JSONObject) params[2];
            switch (requestType) {
                case HttpRequestParameters.ADD_ORDER_RECORD /*603*/:
                    sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.SUBMIT_PAY_ORDER), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.SUBMIT_PAY_ORDER)));
                    return;
                case HttpRequestParameters.SUBMIT_PAY_ORDER /*604*/:
                    this.pDialog.dismiss();
                    if (result.optJSONObject("res").optInt("st") == 0) {
                        verifyOrder(result.optJSONObject("inf"));
                        return;
                    }
                    Intent intent = new Intent(this, PaymentStatusActivity.class);
                    intent.putExtra(LocationManagerProxy.KEY_STATUS_CHANGED, false);
                    startActivity(intent);
                    return;
                case HttpRequestParameters.ORDER_STATUS /*605*/:
                case HttpRequestParameters.AUTO_PAY_ORDER /*606*/:
                case HttpRequestParameters.DELETE_PAY_RECORD /*608*/:
                default:
                    return;
                case HttpRequestParameters.AUTO_PAY_CONFIRM /*607*/:
                    this.pDialog.dismiss();
                    Intent intent2 = new Intent(this, PaymentStatusActivity.class);
                    if (result.optJSONObject("res").optInt("st") == 0) {
                        intent2.putExtra(LocationManagerProxy.KEY_STATUS_CHANGED, true);
                        intent2.putExtra("info", result.optJSONObject("inf").toString());
                    } else {
                        intent2.putExtra(LocationManagerProxy.KEY_STATUS_CHANGED, false);
                    }
                    startActivity(intent2);
                    return;
            }
        } else {
            Toast.makeText(this, getResources().getString(R.string.network_connect_failed_prompt), 0).show();
            this.pDialog.dismiss();
        }
    }

    public void initViewId() {
        this.btnBack = (Button) findViewById(R.id.ibtn_back);
        this.tvFee = (TextView) findViewById(R.id.tv_fee);
        this.elvOrder = (ExpandableListView) findViewById(R.id.elv_order);
        this.tvSubmit = (TextView) findViewById(R.id.tv_submit);
    }

    public void initClickListener() {
        this.btnBack.setOnClickListener(this);
        this.elvOrder.setOnGroupClickListener(this);
        this.elvOrder.setOnChildClickListener(this);
        this.tvSubmit.setOnClickListener(this);
    }

    public void loadData(int requestType, Object data) {
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_submit /*2131296445*/:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(getResources().getString(R.string.ok_amount));
                builder.setMessage(String.valueOf(getResources().getString(R.string.ok_amount_info)) + (Double.valueOf(WaitPaymentOrderActivity.autoPayment.getFee()).doubleValue() / 100.0d) + getResources().getString(R.string.confirm_amount));
                builder.setNegativeButton(getResources().getString(R.string.cancel), (DialogInterface.OnClickListener) null);
                builder.setPositiveButton(getResources().getString(R.string.amount), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        PaymentOrderActivity.this.pDialog.setMessage(PaymentOrderActivity.this.getResources().getString(R.string.ask_confirm_amount));
                        PaymentOrderActivity.this.pDialog.show();
                        PaymentOrderActivity.this.sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.ADD_ORDER_RECORD), PaymentOrderActivity.this.initRequestParameter(Integer.valueOf((int) HttpRequestParameters.ADD_ORDER_RECORD)));
                    }
                });
                builder.create().show();
                return;
            case R.id.ibtn_back /*2131296487*/:
                WaitPaymentOrderActivity.autoPayment = null;
                finish();
                return;
            default:
                return;
        }
    }

    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
        if (-1 == this.expandFlag) {
            parent.expandGroup(groupPosition);
            parent.setSelectedGroup(groupPosition);
            this.expandFlag = groupPosition;
            return true;
        } else if (this.expandFlag == groupPosition) {
            parent.collapseGroup(this.expandFlag);
            this.expandFlag = -1;
            return true;
        } else {
            parent.collapseGroup(this.expandFlag);
            parent.expandGroup(groupPosition);
            parent.setSelectedGroup(groupPosition);
            this.expandFlag = groupPosition;
            return true;
        }
    }

    public boolean onChildClick(ExpandableListView arg0, View arg1, int arg2, int arg3, long arg4) {
        Intent intent = new Intent(this, BillDetailActivity.class);
        intent.putExtra("itemDetail", this.billOrdes.optJSONObject(arg2).optJSONArray("items").optJSONObject(arg3).toString());
        startActivity(intent);
        return false;
    }

    private void verifyOrder(JSONObject json) {
        this.values = new GetValue();
        this.xmlpar = new Xmlpar();
        try {
            Upomp_Pay_Info.merchantId = json.optString("merchantid");
            Upomp_Pay_Info.merchantOrderId = json.optString("merchantorderid");
            Upomp_Pay_Info.merchantOrderTime = json.optString("merchantordertime");
            Upomp_Pay_Info.originalsign = CreateOriginal.CreateOriginal_Sign(3);
            Log.d(Upomp_Pay_Info.tag, "这是订单验证的3位原串===\n" + Upomp_Pay_Info.originalsign);
            try {
                this.PrivateSign = getFromAssets("898000000000002.p12");
            } catch (FileNotFoundException e) {
                Log.d(Upomp_Pay_Info.tag, "Exception is " + e);
            }
            Upomp_Pay_Info.xmlSign = json.optString("sign");
            Log.d(Upomp_Pay_Info.tag, "这是订单验证的3位签名===\n" + Upomp_Pay_Info.xmlSign);
            String LanchPay = XmlDefinition.ReturnXml(Upomp_Pay_Info.xmlSign, 3);
            Log.d(Upomp_Pay_Info.tag, "这是订单验证报文===\n" + LanchPay);
            this.star = new Star_Upomp_Pay();
            this.star.start_upomp_pay(this, LanchPay);
        } catch (Exception e2) {
            Log.d(Upomp_Pay_Info.tag, "**Exception is " + e2);
        }
    }

    public InputStream getFromAssets(String fileName) throws FileNotFoundException {
        try {
            this.PrivateSign = getResources().getAssets().open(fileName);
        } catch (Exception e) {
            Log.d(Upomp_Pay_Info.tag, "Exception is " + e);
        }
        return this.PrivateSign;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            byte[] xml = data.getExtras().getByteArray("xml");
            String respCode = null;
            String respDesc = null;
            try {
                XmlPullParser parser = Xml.newPullParser();
                parser.setInput(new ByteArrayInputStream(xml), e.f);
                for (int eventType = parser.getEventType(); eventType != 1; eventType = parser.next()) {
                    switch (eventType) {
                        case 2:
                            String tag = parser.getName();
                            ClientLogUtil.i(tag, tag);
                            if (!tag.equalsIgnoreCase("respCode")) {
                                if (!tag.equalsIgnoreCase("respDesc")) {
                                    break;
                                } else {
                                    respDesc = parser.nextText();
                                    break;
                                }
                            } else {
                                respCode = parser.nextText();
                                break;
                            }
                    }
                }
                Log.d(Upomp_Pay_Info.tag, "解析中:" + respCode + "->" + respDesc);
                if (respCode == null || !"0000".equals(respCode)) {
                    sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.DELETE_PAY_RECORD), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.DELETE_PAY_RECORD)));
                    Intent intent = new Intent(this, PaymentStatusActivity.class);
                    intent.putExtra(LocationManagerProxy.KEY_STATUS_CHANGED, false);
                    startActivity(intent);
                    return;
                }
                this.pDialog.setMessage(getResources().getString(R.string.ok_confirm_condition));
                this.pDialog.show();
                sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.AUTO_PAY_CONFIRM), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.AUTO_PAY_CONFIRM)));
            } catch (Exception e) {
                Log.d(Upomp_Pay_Info.tag, "Exception is " + e);
            }
        } else {
            Log.d(Upomp_Pay_Info.tag, "data is null");
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.ADD_ORDER_RECORD /*603*/:
                this.orderId = Create_MerchantX.createMerchantOrderId();
                parameters.add("userid", UserEntity.getInstance().getInfo().getAccId());
                parameters.add("ordernum", this.orderId);
                parameters.add("price", Integer.valueOf(WaitPaymentOrderActivity.autoPayment.getFee()));
                parameters.add("name", WaitPaymentOrderActivity.autoPayment.getPatientName());
                return parameters;
            case HttpRequestParameters.SUBMIT_PAY_ORDER /*604*/:
                parameters.add("ordernum", this.orderId);
                parameters.add("ordertime", Create_MerchantX.createMerchantOrderTime());
                parameters.add("orderamt", WaitPaymentOrderActivity.autoPayment.getFee());
                return parameters;
            case HttpRequestParameters.ORDER_STATUS /*605*/:
            case HttpRequestParameters.AUTO_PAY_ORDER /*606*/:
            default:
                return parameters;
            case HttpRequestParameters.AUTO_PAY_CONFIRM /*607*/:
                parameters.add("hospitalid", 2000005);
                parameters.add("hisPatientId", WaitPaymentOrderActivity.autoPayment.getHisPatientId());
                parameters.add("medicalCardNo", WaitPaymentOrderActivity.autoPayment.getPatientCard());
                parameters.add("cardType", WaitPaymentOrderActivity.autoPayment.getCardType());
                parameters.add("clinicTranLine", WaitPaymentOrderActivity.autoPayment.getClinicTranLine());
                parameters.add("realPayFee", WaitPaymentOrderActivity.autoPayment.getFee());
                parameters.add("settlementType", 2);
                parameters.add("payTradeLine", this.orderId);
                return parameters;
            case HttpRequestParameters.DELETE_PAY_RECORD /*608*/:
                UriParameter parameters2 = new UriParameter();
                parameters2.add("ordernum", this.orderId);
                return parameters2;
        }
    }
}
