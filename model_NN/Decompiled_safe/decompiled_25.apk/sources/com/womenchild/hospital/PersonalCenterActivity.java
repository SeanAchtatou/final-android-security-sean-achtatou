package com.womenchild.hospital;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.configure.Constants;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.service.RemindService;
import com.womenchild.hospital.util.ShareUtil;
import org.json.JSONObject;

public class PersonalCenterActivity extends BaseRequestActivity implements View.OnClickListener {
    private Button btn_exit;
    private Button ibtn_home;
    private Button ibtn_more;
    private Button ibtn_remind_setting;
    private Button ibtn_share;
    /* access modifiers changed from: private */
    public Intent intent;
    private Context mContext = this;
    private RelativeLayout rl_blance;
    private RelativeLayout rl_card_management;
    private RelativeLayout rl_check_record;
    private RelativeLayout rl_consult_record;
    private RelativeLayout rl_password;
    private RelativeLayout rl_patient_management;
    private RelativeLayout rl_pay_record;
    private RelativeLayout rl_personal_info;
    private RelativeLayout rl_register_record;
    /* access modifiers changed from: private */
    public SharedPreferences sharedPrefs;
    private TextView tv_account_blance;
    private TextView tv_name;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.personal_center);
        initViewId();
        initClickListener();
        initData();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        initData();
    }

    public void initData() {
        this.sharedPrefs = this.mContext.getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, 0);
        if (UserEntity.getInstance().getInfo() != null) {
            this.tv_name.setText(UserEntity.getInstance().getUsername());
        }
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.CHECK_BLANCE_FOR_SUN), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.CHECK_BLANCE_FOR_SUN)));
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.CHECK_BLANCE_FOR_SUN /*614*/:
                UriParameter parameter = new UriParameter();
                parameter.add("userid", UserEntity.getInstance().getInfo().getUserid());
                parameter.add("hospitalid", Constants.HOSPITALID);
                return parameter;
            default:
                return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_home /*2131296435*/:
                finish();
                return;
            case R.id.ibtn_share /*2131296527*/:
                ShareUtil.share2Friend(this, R.string.app_name);
                return;
            case R.id.ibtn_more /*2131296807*/:
                this.intent = new Intent(this, MoreActivity.class);
                this.intent.putExtra("backFlag", true);
                startActivity(this.intent);
                return;
            case R.id.btn_exit /*2131296991*/:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(getResources().getString(R.string.logout_login_user));
                builder.setPositiveButton(getResources().getString(R.string.logout), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        HomeActivity.loginFlag = false;
                        UserEntity.getInstance().setUsername(PoiTypeDef.All);
                        UserEntity.getInstance().setPassword(PoiTypeDef.All);
                        UserEntity.getInstance().setInfo(null);
                        SharedPreferences.Editor editor = PersonalCenterActivity.this.sharedPrefs.edit();
                        editor.putBoolean(Constants.TREAT_IS_ALERT, false);
                        editor.commit();
                        PersonalCenterActivity.this.intent = new Intent(PersonalCenterActivity.this, LoginActivity.class);
                        PersonalCenterActivity.this.startActivity(PersonalCenterActivity.this.intent);
                        PersonalCenterActivity.this.stopService(new Intent(PersonalCenterActivity.this, RemindService.class));
                        PersonalCenterActivity.this.finish();
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.cancel), (DialogInterface.OnClickListener) null);
                builder.create().show();
                return;
            case R.id.rl_personal_info /*2131296993*/:
                this.intent = new Intent(this, PersonalDetailEditActivity.class);
                startActivity(this.intent);
                return;
            case R.id.rl_blance /*2131296996*/:
                this.intent = new Intent(this, RechargeActivity.class);
                this.intent.putExtra("mode", 0);
                startActivity(this.intent);
                return;
            case R.id.rl_password /*2131297001*/:
                this.intent = new Intent(this, ChangePwdActivity.class);
                startActivity(this.intent);
                return;
            case R.id.rl_patient_management /*2131297005*/:
                this.intent = new Intent(this, PatientManageActivity.class);
                startActivity(this.intent);
                return;
            case R.id.rl_card_management /*2131297008*/:
                this.intent = new Intent(this, MedicalCardManagementActivity.class);
                startActivity(this.intent);
                return;
            case R.id.rl_consult_record /*2131297012*/:
                this.intent = new Intent(this, ConsultActivity.class);
                startActivity(this.intent);
                return;
            case R.id.rl_register_record /*2131297015*/:
                this.intent = new Intent(this, MyRegisterNoRecordActivity.class);
                startActivity(this.intent);
                return;
            case R.id.rl_pay_record /*2131297018*/:
                this.intent = new Intent(this, PaymentRecordActivity.class);
                startActivity(this.intent);
                return;
            case R.id.rl_check_record /*2131297021*/:
                this.intent = new Intent(this, TestReportActivity.class);
                this.intent.putExtra("index", 1);
                startActivity(this.intent);
                return;
            default:
                return;
        }
    }

    public void initViewId() {
        this.ibtn_home = (Button) findViewById(R.id.ibtn_home);
        this.rl_personal_info = (RelativeLayout) findViewById(R.id.rl_personal_info);
        this.rl_password = (RelativeLayout) findViewById(R.id.rl_password);
        this.rl_patient_management = (RelativeLayout) findViewById(R.id.rl_patient_management);
        this.rl_card_management = (RelativeLayout) findViewById(R.id.rl_card_management);
        this.rl_consult_record = (RelativeLayout) findViewById(R.id.rl_consult_record);
        this.rl_register_record = (RelativeLayout) findViewById(R.id.rl_register_record);
        this.rl_pay_record = (RelativeLayout) findViewById(R.id.rl_pay_record);
        this.rl_check_record = (RelativeLayout) findViewById(R.id.rl_check_record);
        this.rl_blance = (RelativeLayout) findViewById(R.id.rl_blance);
        this.tv_name = (TextView) findViewById(R.id.tv_name);
        this.ibtn_share = (Button) findViewById(R.id.ibtn_share);
        this.ibtn_more = (Button) findViewById(R.id.ibtn_more);
        this.btn_exit = (Button) findViewById(R.id.btn_exit);
        this.tv_account_blance = (TextView) findViewById(R.id.tv_account_blance);
    }

    public void initClickListener() {
        this.ibtn_home.setOnClickListener(this);
        this.rl_personal_info.setOnClickListener(this);
        this.rl_password.setOnClickListener(this);
        this.rl_patient_management.setOnClickListener(this);
        this.rl_card_management.setOnClickListener(this);
        this.rl_consult_record.setOnClickListener(this);
        this.rl_register_record.setOnClickListener(this);
        this.rl_pay_record.setOnClickListener(this);
        this.rl_check_record.setOnClickListener(this);
        this.rl_blance.setOnClickListener(this);
        this.ibtn_share.setOnClickListener(this);
        this.ibtn_more.setOnClickListener(this);
        this.btn_exit.setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void loadData(int requestType, Object data) {
    }

    public void refreshActivity(Object... objects) {
        int requestType = ((Integer) objects[0]).intValue();
        boolean status = ((Boolean) objects[1]).booleanValue();
        JSONObject result = (JSONObject) objects[2];
        if (status && result.optJSONObject("res").optInt("st") == 0) {
            switch (requestType) {
                case HttpRequestParameters.CHECK_BLANCE_FOR_SUN /*614*/:
                    this.tv_account_blance.setText(String.valueOf(result.optJSONObject("res").optString("msg")) + "元");
                    return;
                default:
                    return;
            }
        }
    }
}
