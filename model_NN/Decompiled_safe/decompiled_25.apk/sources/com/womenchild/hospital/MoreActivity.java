package com.womenchild.hospital;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.ShareUtil;

public class MoreActivity extends BaseRequestActivity implements View.OnClickListener {
    private RelativeLayout aboutCompany;
    private RelativeLayout aboutHopitalRl;
    private Button backBtn;
    private RelativeLayout complaintRl;
    private Button ibtnHome;
    private Intent intent;
    private RelativeLayout rl_about_yangguankangzhong;
    private RelativeLayout rl_about_yijiankang;
    private RelativeLayout rl_complaint;
    private RelativeLayout rl_evaluate;
    private RelativeLayout rl_invitation;
    private RelativeLayout rl_suggestion;
    private RelativeLayout rl_version;
    private RelativeLayout shareFriendRl;
    private RelativeLayout suggestionRl;
    private RelativeLayout versionUpdateRl;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.more);
        initViewId();
        initClickListener();
        if (getIntent().getBooleanExtra("backFlag", false)) {
            this.backBtn.setVisibility(0);
            this.ibtnHome.setVisibility(8);
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_home /*2131296435*/:
                finish();
                return;
            case R.id.back_btn /*2131296902*/:
                finish();
                return;
            case R.id.rl_suggestion /*2131296903*/:
                Intent intent2 = new Intent();
                intent2.setClass(this, MoreSuggestionActivity.class);
                startActivity(intent2);
                return;
            case R.id.rl_invitation /*2131296906*/:
                ShareUtil.share2Friend(this, R.string.app_name);
                return;
            case R.id.rl_complaint /*2131296909*/:
                Intent intent3 = new Intent();
                intent3.setClass(this, MoreComplaintActivity.class);
                startActivity(intent3);
                return;
            case R.id.rl_version /*2131296912*/:
                Intent intent4 = new Intent();
                intent4.setClass(this, MoreVersionActivity.class);
                startActivity(intent4);
                return;
            case R.id.rl_about_yangguankangzhong /*2131296914*/:
                Intent intent5 = new Intent();
                intent5.setClass(this, MoreAboutHospitalActivity.class);
                startActivity(intent5);
                return;
            case R.id.rl_about_yijiankang /*2131296916*/:
                Intent intent6 = new Intent();
                intent6.setClass(this, MoreAboutCompanyActivity.class);
                startActivity(intent6);
                return;
            default:
                return;
        }
    }

    public void refreshActivity(Object... params) {
    }

    public void initViewId() {
        this.ibtnHome = (Button) findViewById(R.id.ibtn_home);
        this.backBtn = (Button) findViewById(R.id.back_btn);
        this.suggestionRl = (RelativeLayout) findViewById(R.id.rl_suggestion);
        this.shareFriendRl = (RelativeLayout) findViewById(R.id.rl_invitation);
        this.complaintRl = (RelativeLayout) findViewById(R.id.rl_complaint);
        this.versionUpdateRl = (RelativeLayout) findViewById(R.id.rl_version);
        this.aboutHopitalRl = (RelativeLayout) findViewById(R.id.rl_about_yangguankangzhong);
        this.aboutCompany = (RelativeLayout) findViewById(R.id.rl_about_yijiankang);
    }

    public void initClickListener() {
        this.ibtnHome.setOnClickListener(this);
        this.backBtn.setOnClickListener(this);
        this.suggestionRl.setOnClickListener(this);
        this.shareFriendRl.setOnClickListener(this);
        this.complaintRl.setOnClickListener(this);
        this.versionUpdateRl.setOnClickListener(this);
        this.aboutHopitalRl.setOnClickListener(this);
        this.aboutCompany.setOnClickListener(this);
    }

    public void loadData(int requestType, Object data) {
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        return null;
    }
}
