package com.womenchild.hospital.base;

import android.app.Service;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.request.RequestTask;

public abstract class BaseRequestService extends Service {
    /* access modifiers changed from: protected */
    public abstract UriParameter initRequestParameter(Object obj);

    public abstract void refreshService(Object... objArr);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.womenchild.hospital.request.RequestTask.sendHttpRequest(com.womenchild.hospital.base.BaseRequestService, java.lang.String, com.womenchild.hospital.parameter.UriParameter, boolean):void
     arg types: [com.womenchild.hospital.base.BaseRequestService, java.lang.String, com.womenchild.hospital.parameter.UriParameter, int]
     candidates:
      com.womenchild.hospital.request.RequestTask.sendHttpRequest(com.womenchild.hospital.base.BaseRequestActivity, java.lang.String, com.womenchild.hospital.parameter.UriParameter, boolean):void
      com.womenchild.hospital.request.RequestTask.sendHttpRequest(com.womenchild.hospital.base.BaseRequestFragment, java.lang.String, com.womenchild.hospital.parameter.UriParameter, boolean):void
      com.womenchild.hospital.request.RequestTask.sendHttpRequest(com.womenchild.hospital.base.BaseRequestFragmentActivity, java.lang.String, com.womenchild.hospital.parameter.UriParameter, boolean):void
      com.womenchild.hospital.request.RequestTask.sendHttpRequest(com.womenchild.hospital.base.BaseRequestService, java.lang.String, com.womenchild.hospital.parameter.UriParameter, boolean):void */
    /* access modifiers changed from: protected */
    public void sendHttpRequest(Object requestCode, UriParameter parameter) {
        RequestTask.getInstance().sendHttpRequest(this, String.valueOf(requestCode), parameter, true);
    }
}
