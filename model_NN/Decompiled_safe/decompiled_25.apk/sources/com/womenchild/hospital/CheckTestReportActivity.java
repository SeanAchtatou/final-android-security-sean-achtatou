package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.adapter.TestReportAdapter;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.configure.Constants;
import com.womenchild.hospital.entity.TestPrjEntity;
import com.womenchild.hospital.entity.TestPrjSubEntity;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CheckTestReportActivity extends BaseRequestActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    private Button btnBack;
    private String date;
    private String endDate;
    private ListView lvCheckTestPrj;
    private TextView noDataTv;
    private ProgressDialog pDialog;
    private String startDate;
    private ArrayList<TestPrjEntity> testPrjEntityList = new ArrayList<>();
    private TextView tvTitle;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.check_test_report);
        initViewId();
        initClickListener();
    }

    public void refreshActivity(Object... params) {
        this.pDialog.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            loadData(requestType, (JSONObject) params[2]);
        } else {
            Toast.makeText(this, (int) R.string.network_connect_failed_prompt, 0).show();
        }
    }

    public void initViewId() {
        this.btnBack = (Button) findViewById(R.id.iv_back);
        this.lvCheckTestPrj = (ListView) findViewById(R.id.lv_check_test_prj);
        this.noDataTv = (TextView) findViewById(R.id.tv_not_data);
        this.tvTitle = (TextView) findViewById(R.id.tv_title);
        if ("CheckReport".equals(getIntent().getAction())) {
            this.tvTitle.setText(getResources().getString(R.string.Unknow));
            this.noDataTv.setVisibility(0);
            this.noDataTv.setText(getResources().getString(R.string.not_started));
            return;
        }
        this.startDate = getIntent().getStringExtra("startdate");
        this.endDate = getIntent().getStringExtra("enddate");
        this.tvTitle.setText(getResources().getString(R.string.Unknow));
        this.pDialog = new ProgressDialog(this);
        this.pDialog.setMessage(getResources().getString(R.string.loading_data));
        this.pDialog.show();
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.QUEUE_TEST_REPORT), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.QUEUE_TEST_REPORT)));
    }

    public void initClickListener() {
        this.btnBack.setOnClickListener(this);
        this.lvCheckTestPrj.setOnItemClickListener(this);
    }

    public void loadData(int requestType, Object data) {
        JSONObject json = (JSONObject) data;
        JSONObject res = json.optJSONObject("res");
        if (res != null) {
            try {
                if (res.getInt("st") == 0) {
                    JSONArray jsonArray = json.optJSONObject("inf").getJSONArray("reports");
                    if (jsonArray == null || jsonArray.length() <= 0) {
                        this.noDataTv.setVisibility(0);
                        return;
                    }
                    for (int i = 0; i < jsonArray.length(); i++) {
                        TestPrjEntity mTestPrjEntity = new TestPrjEntity();
                        ArrayList<TestPrjSubEntity> testPrjSubEntityList = new ArrayList<>();
                        mTestPrjEntity.setProjectName(jsonArray.getJSONObject(i).getString("sampletype"));
                        mTestPrjEntity.setPatientName(jsonArray.getJSONObject(i).getString("patientname"));
                        mTestPrjEntity.setOrganName(jsonArray.getJSONObject(i).getString("deptname"));
                        mTestPrjEntity.setUpload(true);
                        String[] date2 = jsonArray.getJSONObject(i).getString("sampledate").split("T");
                        mTestPrjEntity.setUpdateDate(date2[0]);
                        mTestPrjEntity.setUpdateTime(date2[1]);
                        JSONArray jsonArrayItem = jsonArray.getJSONObject(i).getJSONArray("items");
                        if (jsonArrayItem != null && jsonArrayItem.length() > 0) {
                            for (int j = 0; j < jsonArrayItem.length(); j++) {
                                TestPrjSubEntity mTestPrjSubEntity = new TestPrjSubEntity();
                                mTestPrjSubEntity.setItemName(jsonArrayItem.getJSONObject(j).getString("itemname"));
                                mTestPrjSubEntity.setUnit(jsonArrayItem.getJSONObject(j).getString("unit"));
                                mTestPrjSubEntity.setResult(jsonArrayItem.getJSONObject(j).getString("result"));
                                mTestPrjSubEntity.setReference(jsonArrayItem.getJSONObject(j).getString("reference"));
                                testPrjSubEntityList.add(mTestPrjSubEntity);
                            }
                        }
                        mTestPrjEntity.setSubList(testPrjSubEntityList);
                        this.testPrjEntityList.add(mTestPrjEntity);
                    }
                    this.lvCheckTestPrj.setAdapter((ListAdapter) new TestReportAdapter(this, this.lvCheckTestPrj, this.testPrjEntityList));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void onClick(View arg0) {
        switch (arg0.getId()) {
            case R.id.iv_back /*2131296529*/:
                finish();
                return;
            default:
                return;
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
        Intent intent = new Intent(this, ReportResultActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("entity", this.testPrjEntityList.get(arg2));
        intent.putExtras(bundle);
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        parameters.add("userid", UserEntity.getInstance().getInfo().getAccId());
        parameters.add("hospitalid", Constants.HOSPITAL_ID);
        parameters.add("visitdate", PoiTypeDef.All);
        parameters.add("startdate", this.startDate);
        parameters.add("enddate", this.endDate);
        return parameters;
    }
}
