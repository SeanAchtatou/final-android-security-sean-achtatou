package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.adapter.TestReportAdapter;
import com.womenchild.hospital.base.BaseRequestFragment;
import com.womenchild.hospital.configure.Constants;
import com.womenchild.hospital.entity.TestPrjEntity;
import com.womenchild.hospital.entity.TestPrjSubEntity;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.ClientLogUtil;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TestReportFragment extends BaseRequestFragment implements View.OnClickListener {
    private static final String TAG = "TestReportFragment";
    private AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View view, int arg2, long arg3) {
            Intent intent = new Intent(TestReportFragment.this.getActivity(), ReportResultActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("entity", (TestPrjEntity) TestReportFragment.this.testPrjEntityList.get(arg2));
            intent.putExtras(bundle);
            TestReportFragment.this.getActivity().startActivity(intent);
        }
    };
    private ListView lvPatient;
    private TextView noDataTv;
    private ProgressDialog pDialog;
    private String patientCardID;
    /* access modifiers changed from: private */
    public ArrayList<TestPrjEntity> testPrjEntityList = new ArrayList<>();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ClientLogUtil.v(TAG, "onCreate()");
        this.pDialog = new ProgressDialog(getActivity());
        this.pDialog.setMessage("正在加载数据...");
        this.pDialog.show();
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.QUEUE_TEST_REPORT), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.QUEUE_TEST_REPORT)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ClientLogUtil.v(TAG, "onCreateView()");
        View view = inflater.inflate((int) R.layout.test_report_frag, container, false);
        this.lvPatient = (ListView) view.findViewById(R.id.lv_select_test_prj);
        this.lvPatient.setOnItemClickListener(this.itemClickListener);
        this.noDataTv = (TextView) view.findViewById(R.id.tv_not_data);
        return view;
    }

    public void onClick(View v) {
    }

    public void initViewId() {
    }

    public void initClickListener() {
    }

    public void loadData(int requestType, Object data) {
        JSONObject json = (JSONObject) data;
        JSONObject res = json.optJSONObject("res");
        if (res != null) {
            try {
                if (res.getInt("st") == 0) {
                    JSONArray jsonArray = json.optJSONObject("inf").getJSONArray("reports");
                    if (jsonArray == null || jsonArray.length() <= 0) {
                        this.noDataTv.setVisibility(0);
                        return;
                    }
                    for (int i = 0; i < jsonArray.length(); i++) {
                        TestPrjEntity mTestPrjEntity = new TestPrjEntity();
                        ArrayList<TestPrjSubEntity> testPrjSubEntityList = new ArrayList<>();
                        mTestPrjEntity.setProjectName(jsonArray.getJSONObject(i).getString("sampletype"));
                        mTestPrjEntity.setPatientName(jsonArray.getJSONObject(i).getString("patientname"));
                        mTestPrjEntity.setOrganName(jsonArray.getJSONObject(i).getString("deptname"));
                        mTestPrjEntity.setUpload(true);
                        String[] date = jsonArray.getJSONObject(i).getString("sampledate").split("T");
                        mTestPrjEntity.setUpdateDate(date[0]);
                        mTestPrjEntity.setUpdateTime(date[1]);
                        JSONArray jsonArrayItem = jsonArray.getJSONObject(i).getJSONArray("items");
                        if (jsonArrayItem != null && jsonArrayItem.length() > 0) {
                            for (int j = 0; j < jsonArrayItem.length(); j++) {
                                TestPrjSubEntity mTestPrjSubEntity = new TestPrjSubEntity();
                                mTestPrjSubEntity.setItemName(jsonArrayItem.getJSONObject(j).getString("itemname"));
                                mTestPrjSubEntity.setUnit(jsonArrayItem.getJSONObject(j).getString("unit"));
                                mTestPrjSubEntity.setResult(jsonArrayItem.getJSONObject(j).getString("result"));
                                mTestPrjSubEntity.setReference(jsonArrayItem.getJSONObject(j).getString("reference"));
                                testPrjSubEntityList.add(mTestPrjSubEntity);
                            }
                        }
                        mTestPrjEntity.setSubList(testPrjSubEntityList);
                        this.testPrjEntityList.add(mTestPrjEntity);
                    }
                    this.lvPatient.setAdapter((ListAdapter) new TestReportAdapter(getActivity(), this.lvPatient, this.testPrjEntityList));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void initViewId(View view) {
    }

    /* access modifiers changed from: protected */
    public void initUIData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        parameters.add("userid", UserEntity.getInstance().getInfo().getUserid());
        parameters.add("hospitalid", Constants.HOSPITAL_ID);
        parameters.add("visitdate", PoiTypeDef.All);
        return parameters;
    }

    public void refreshFragment(Object... params) {
        this.pDialog.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            loadData(requestType, (JSONObject) params[2]);
        } else {
            Toast.makeText(getActivity(), (int) R.string.network_connect_failed_prompt, 0).show();
        }
    }
}
