package com.womenchild.hospital.view;

import android.app.Activity;
import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import com.womenchild.hospital.entity.DocPlanByTimeContainer;
import com.womenchild.hospital.entity.DoctorPlanJsonEntity;
import com.womenchild.hospital.view.DoctorCalPageItemView;
import java.util.ArrayList;

public class DoctorCalPageAdapter extends PagerAdapter {
    private static final int CAL_VIEW_COUNT = 5;
    private ArrayList<DocPlanByTimeContainer> containerAllData = null;
    private Activity mContext;
    /* access modifiers changed from: private */
    public OnDoctorButtonPageLongClickListener onDoctorBtnPageLongClickListener = null;

    public interface OnDoctorButtonPageLongClickListener {
        void onDoctorButtonPageLongClick(DoctorPlanJsonEntity doctorPlanJsonEntity);
    }

    public DoctorCalPageAdapter(Context context, ArrayList<DocPlanByTimeContainer> containerFirst) {
        this.mContext = (Activity) context;
        this.containerAllData = containerFirst;
    }

    public int getCount() {
        return 5;
    }

    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == arg1;
    }

    public void destroyItem(View arg0, int arg1, Object arg2) {
        ((ViewPager) arg0).removeView((View) arg2);
    }

    public void finishUpdate(View arg0) {
    }

    public Object instantiateItem(ViewGroup arg0, int arg1) {
        DoctorCalPageItemView view = new DoctorCalPageItemView(this.mContext);
        view.setData(splitList(arg1));
        view.setOnDoctorBtnLongClickListener(new DoctorCalPageItemView.OnDoctorButtonLongClickListener() {
            public void onDoctorButtonLongClick(DoctorPlanJsonEntity doctorPlanRecord) {
                DoctorCalPageAdapter.this.onDoctorBtnPageLongClickListener.onDoctorButtonPageLongClick(doctorPlanRecord);
            }
        });
        arg0.addView(view);
        return view;
    }

    public void restoreState(Parcelable arg0, ClassLoader arg1) {
    }

    public Parcelable saveState() {
        return null;
    }

    public void startUpdate(View arg0) {
    }

    public ArrayList<DocPlanByTimeContainer> splitList(int index) {
        ArrayList<DocPlanByTimeContainer> containerFirst = new ArrayList<>();
        switch (index) {
            case 0:
                for (int i = 0; i < 6; i++) {
                    containerFirst.add(this.containerAllData.get(i));
                }
                break;
            case 1:
                for (int i2 = 6; i2 < 12; i2++) {
                    containerFirst.add(this.containerAllData.get(i2));
                }
                break;
            case 2:
                for (int i3 = 12; i3 < 18; i3++) {
                    containerFirst.add(this.containerAllData.get(i3));
                }
                break;
            case 3:
                for (int i4 = 18; i4 < 24; i4++) {
                    containerFirst.add(this.containerAllData.get(i4));
                }
                break;
            case 4:
                for (int i5 = 24; i5 < 28; i5++) {
                    containerFirst.add(this.containerAllData.get(i5));
                }
                break;
        }
        return containerFirst;
    }

    public OnDoctorButtonPageLongClickListener getOnDoctorBtnPageLongClickListener() {
        return this.onDoctorBtnPageLongClickListener;
    }

    public void setOnDoctorBtnPageLongClickListener(OnDoctorButtonPageLongClickListener onDoctorBtnPageLongClickListener2) {
        this.onDoctorBtnPageLongClickListener = onDoctorBtnPageLongClickListener2;
    }
}
