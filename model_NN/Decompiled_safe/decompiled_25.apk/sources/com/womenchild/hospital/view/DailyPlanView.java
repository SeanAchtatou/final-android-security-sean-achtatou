package com.womenchild.hospital.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.womenchild.hospital.R;

public class DailyPlanView extends LinearLayout {
    private Button btn_order;
    private TextView tv_order;

    public DailyPlanView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray style = context.obtainStyledAttributes(attrs, R.styleable.DailyPlanView);
        String string = style.getString(0);
        float tv_order_textSize = style.getFloat(2, 15.0f);
        int color = style.getColor(1, R.color.black);
        String btn_order_text = style.getString(5);
        Drawable btn_order_background = style.getDrawable(4);
        int btn_order_textColor = style.getColor(3, R.color.black);
        style.recycle();
        this.tv_order = new TextView(context);
        this.tv_order.setText("可预约数");
        this.tv_order.setPadding(0, 0, 0, 5);
        this.tv_order.setTextSize(tv_order_textSize);
        this.btn_order = new Button(context);
        this.btn_order.setText(btn_order_text);
        this.btn_order.setBackgroundDrawable(btn_order_background);
        this.btn_order.setTextColor(btn_order_textColor);
        this.btn_order.setPadding(0, 0, 0, 0);
        LinearLayout.LayoutParams llp_btn = new LinearLayout.LayoutParams(-2, -2);
        LinearLayout.LayoutParams llp_tv = new LinearLayout.LayoutParams(-2, -2);
        llp_btn.gravity = 1;
        llp_tv.gravity = 1;
        addView(this.tv_order, llp_btn);
        addView(this.btn_order, llp_tv);
        setOrientation(1);
        setPadding(10, 10, 10, 10);
    }

    public void setBtnClickListener(View.OnClickListener listener) {
        this.btn_order.setOnClickListener(listener);
    }

    public void setContentVisible(int tvVi, int btnVi) {
        this.tv_order.setVisibility(tvVi);
        this.btn_order.setVisibility(btnVi);
    }

    public void setButtonClickable(boolean clickable) {
        this.btn_order.setClickable(clickable);
    }

    public void setButtonDrawableBackground(Drawable background) {
        this.btn_order.setBackgroundDrawable(background);
    }

    public void setButtonText(String text) {
        this.btn_order.setText(text);
    }

    public void setTextViewText(String text) {
        this.tv_order.setText(text);
    }
}
