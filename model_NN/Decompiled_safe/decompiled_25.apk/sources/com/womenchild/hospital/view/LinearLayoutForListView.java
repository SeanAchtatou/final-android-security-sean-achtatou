package com.womenchild.hospital.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.womenchild.hospital.R;

public class LinearLayoutForListView extends LinearLayout {
    private AdapterForLinearLayout adapter;
    private View.OnClickListener onClickListener = null;

    public void bindLinearLayout() {
        int count = this.adapter.getCount();
        for (int i = 0; i < count; i++) {
            View v = this.adapter.getView(i, null, null);
            v.setTag(Integer.valueOf(i));
            v.setOnClickListener(this.onClickListener);
            if (i == count - 1) {
                LinearLayout ly = (LinearLayout) v;
                ly.removeView((TextView) ly.findViewById(R.id.tv_line_1));
            }
            addView(v, i);
        }
        Log.v("countTAG", new StringBuilder().append(count).toString());
    }

    public LinearLayoutForListView(Context context) {
        super(context);
    }

    public LinearLayoutForListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AdapterForLinearLayout getAdpater() {
        return this.adapter;
    }

    public void setAdapter(AdapterForLinearLayout adpater) {
        this.adapter = adpater;
        bindLinearLayout();
    }

    public View.OnClickListener getOnclickListner() {
        return this.onClickListener;
    }

    public void setOnclickLinstener(View.OnClickListener onClickListener2) {
        this.onClickListener = onClickListener2;
    }
}
