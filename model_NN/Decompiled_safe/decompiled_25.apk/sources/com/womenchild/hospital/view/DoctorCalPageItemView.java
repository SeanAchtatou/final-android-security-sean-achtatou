package com.womenchild.hospital.view;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import com.umeng.common.a;
import com.womenchild.hospital.R;
import com.womenchild.hospital.SelectClinicTimeActivity;
import com.womenchild.hospital.entity.DocPlanByTimeContainer;
import com.womenchild.hospital.entity.DoctorPlanJsonEntity;
import com.womenchild.hospital.util.ClientLogUtil;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DoctorCalPageItemView extends LinearLayout {
    private String TAG = getClass().getSimpleName();
    private LinearLayout dateAm1;
    private LinearLayout dateAm2;
    private LinearLayout dateAm3;
    private LinearLayout datePm1;
    private LinearLayout datePm2;
    private LinearLayout datePm3;
    private Button doctorBtn;
    /* access modifiers changed from: private */
    public Context mContext = null;
    /* access modifiers changed from: private */
    public OnDoctorButtonLongClickListener onDoctorBtnLongClickListener = null;

    public interface OnDoctorButtonLongClickListener {
        void onDoctorButtonLongClick(DoctorPlanJsonEntity doctorPlanJsonEntity);
    }

    public DoctorCalPageItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public DoctorCalPageItemView(Context context) {
        super(context);
        initView(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.womenchild.hospital.view.DoctorCalPageItemView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void initView(Context context) {
        this.mContext = context;
        LayoutInflater.from(this.mContext).inflate((int) R.layout.doctor_cal_page_item, (ViewGroup) this, true);
        this.dateAm1 = (LinearLayout) findViewById(R.id.ll_date_am_f);
        this.dateAm2 = (LinearLayout) findViewById(R.id.ll_date_am_s);
        this.dateAm3 = (LinearLayout) findViewById(R.id.ll_date_am_t);
        this.datePm1 = (LinearLayout) findViewById(R.id.ll_date_pm_f);
        this.datePm2 = (LinearLayout) findViewById(R.id.ll_date_pm_s);
        this.datePm3 = (LinearLayout) findViewById(R.id.ll_date_pm_t);
    }

    public void setData(ArrayList<DocPlanByTimeContainer> containerList) {
        this.dateAm1.removeAllViews();
        this.datePm1.removeAllViews();
        this.dateAm2.removeAllViews();
        this.datePm2.removeAllViews();
        this.dateAm3.removeAllViews();
        this.datePm3.removeAllViews();
        int dateSize = containerList.size();
        for (int i = 0; i < dateSize; i++) {
            int size = containerList.get(i).getDoctorNum().size();
            ClientLogUtil.d(this.TAG, "doctor size:" + size);
            switch (i) {
                case 0:
                    for (int j = 0; j < size; j++) {
                        this.doctorBtn = new Button(this.mContext);
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
                        layoutParams.bottomMargin = 8;
                        this.doctorBtn.setLayoutParams(layoutParams);
                        this.doctorBtn.setBackgroundResource(R.drawable.btn_order_selector);
                        int orderNum = containerList.get(i).getDoctorNum().get(j).getCanOrderNum();
                        boolean isStop = containerList.get(i).getDoctorNum().get(j).isStop();
                        if (orderNum == 0) {
                            this.doctorBtn.setBackgroundResource(R.drawable.ygkz_order_doctor_button_dark);
                        } else if (orderNum == -1) {
                            this.doctorBtn.setBackgroundResource(R.drawable.btn_order_unknown_selector);
                            this.doctorBtn.setEnabled(true);
                        }
                        if (isStop) {
                            this.doctorBtn.setBackgroundResource(R.drawable.ygkz_order_doctor_button_red);
                        }
                        this.doctorBtn.setTextSize(12.0f);
                        this.doctorBtn.setTextColor(-1);
                        this.doctorBtn.setOnClickListener(new OrderDoctorClickListener(containerList.get(i).getDoctorNum().get(j), getResources().getString(R.string.morning), isStop));
                        this.doctorBtn.setOnLongClickListener(new CollectDoctorLongClickListener(containerList.get(i).getDoctorNum().get(j)));
                        this.doctorBtn.setText(containerList.get(i).getDoctorNum().get(j).getDoctorname());
                        this.dateAm1.addView(this.doctorBtn);
                    }
                    break;
                case 1:
                    for (int j2 = 0; j2 < size; j2++) {
                        this.doctorBtn = new Button(this.mContext);
                        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
                        layoutParams2.bottomMargin = 8;
                        this.doctorBtn.setLayoutParams(layoutParams2);
                        this.doctorBtn.setBackgroundResource(R.drawable.btn_order_selector);
                        int orderNum2 = containerList.get(i).getDoctorNum().get(j2).getCanOrderNum();
                        boolean isStop2 = containerList.get(i).getDoctorNum().get(j2).isStop();
                        if (orderNum2 == 0) {
                            this.doctorBtn.setBackgroundResource(R.drawable.ygkz_order_doctor_button_dark);
                        } else if (orderNum2 == -1) {
                            this.doctorBtn.setBackgroundResource(R.drawable.btn_order_unknown_selector);
                            this.doctorBtn.setEnabled(true);
                        }
                        if (isStop2) {
                            this.doctorBtn.setBackgroundResource(R.drawable.ygkz_order_doctor_button_red);
                        }
                        this.doctorBtn.setTextSize(12.0f);
                        this.doctorBtn.setTextColor(-1);
                        this.doctorBtn.setOnClickListener(new OrderDoctorClickListener(containerList.get(i).getDoctorNum().get(j2), getResources().getString(R.string.afternoon), isStop2));
                        this.doctorBtn.setOnLongClickListener(new CollectDoctorLongClickListener(containerList.get(i).getDoctorNum().get(j2)));
                        this.doctorBtn.setText(containerList.get(i).getDoctorNum().get(j2).getDoctorname());
                        this.datePm1.addView(this.doctorBtn);
                    }
                    break;
                case 2:
                    for (int j3 = 0; j3 < size; j3++) {
                        this.doctorBtn = new Button(this.mContext);
                        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-2, -2);
                        layoutParams3.bottomMargin = 8;
                        this.doctorBtn.setLayoutParams(layoutParams3);
                        this.doctorBtn.setBackgroundResource(R.drawable.btn_order_selector);
                        int orderNum3 = containerList.get(i).getDoctorNum().get(j3).getCanOrderNum();
                        boolean isStop3 = containerList.get(i).getDoctorNum().get(j3).isStop();
                        if (orderNum3 == 0) {
                            this.doctorBtn.setBackgroundResource(R.drawable.ygkz_order_doctor_button_dark);
                        } else if (orderNum3 == -1) {
                            this.doctorBtn.setBackgroundResource(R.drawable.btn_order_unknown_selector);
                            this.doctorBtn.setEnabled(true);
                        }
                        if (isStop3) {
                            this.doctorBtn.setBackgroundResource(R.drawable.ygkz_order_doctor_button_red);
                        }
                        this.doctorBtn.setTextSize(12.0f);
                        this.doctorBtn.setTextColor(-1);
                        this.doctorBtn.setOnClickListener(new OrderDoctorClickListener(containerList.get(i).getDoctorNum().get(j3), getResources().getString(R.string.morning), isStop3));
                        this.doctorBtn.setOnLongClickListener(new CollectDoctorLongClickListener(containerList.get(i).getDoctorNum().get(j3)));
                        this.doctorBtn.setText(containerList.get(i).getDoctorNum().get(j3).getDoctorname());
                        this.dateAm2.addView(this.doctorBtn);
                    }
                    break;
                case 3:
                    for (int j4 = 0; j4 < size; j4++) {
                        this.doctorBtn = new Button(this.mContext);
                        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(-2, -2);
                        layoutParams4.bottomMargin = 8;
                        this.doctorBtn.setLayoutParams(layoutParams4);
                        this.doctorBtn.setBackgroundResource(R.drawable.btn_order_selector);
                        int orderNum4 = containerList.get(i).getDoctorNum().get(j4).getCanOrderNum();
                        boolean isStop4 = containerList.get(i).getDoctorNum().get(j4).isStop();
                        if (orderNum4 == 0) {
                            this.doctorBtn.setBackgroundResource(R.drawable.ygkz_order_doctor_button_dark);
                        } else if (orderNum4 == -1) {
                            this.doctorBtn.setBackgroundResource(R.drawable.btn_order_unknown_selector);
                            this.doctorBtn.setEnabled(true);
                        }
                        if (isStop4) {
                            this.doctorBtn.setBackgroundResource(R.drawable.ygkz_order_doctor_button_red);
                        }
                        this.doctorBtn.setTextSize(12.0f);
                        this.doctorBtn.setTextColor(-1);
                        this.doctorBtn.setOnClickListener(new OrderDoctorClickListener(containerList.get(i).getDoctorNum().get(j4), getResources().getString(R.string.afternoon), isStop4));
                        this.doctorBtn.setOnLongClickListener(new CollectDoctorLongClickListener(containerList.get(i).getDoctorNum().get(j4)));
                        this.doctorBtn.setText(containerList.get(i).getDoctorNum().get(j4).getDoctorname());
                        this.datePm2.addView(this.doctorBtn);
                    }
                    break;
                case 4:
                    for (int j5 = 0; j5 < size; j5++) {
                        this.doctorBtn = new Button(this.mContext);
                        LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(-2, -2);
                        layoutParams5.bottomMargin = 8;
                        this.doctorBtn.setLayoutParams(layoutParams5);
                        this.doctorBtn.setBackgroundResource(R.drawable.btn_order_selector);
                        int orderNum5 = containerList.get(i).getDoctorNum().get(j5).getCanOrderNum();
                        boolean isStop5 = containerList.get(i).getDoctorNum().get(j5).isStop();
                        if (orderNum5 == 0) {
                            this.doctorBtn.setBackgroundResource(R.drawable.ygkz_order_doctor_button_dark);
                        } else if (orderNum5 == -1) {
                            this.doctorBtn.setBackgroundResource(R.drawable.btn_order_unknown_selector);
                            this.doctorBtn.setEnabled(true);
                        }
                        if (isStop5) {
                            this.doctorBtn.setBackgroundResource(R.drawable.ygkz_order_doctor_button_red);
                        }
                        this.doctorBtn.setTextSize(12.0f);
                        this.doctorBtn.setTextColor(-1);
                        this.doctorBtn.setOnClickListener(new OrderDoctorClickListener(containerList.get(i).getDoctorNum().get(j5), getResources().getString(R.string.morning), isStop5));
                        this.doctorBtn.setOnLongClickListener(new CollectDoctorLongClickListener(containerList.get(i).getDoctorNum().get(j5)));
                        this.doctorBtn.setText(containerList.get(i).getDoctorNum().get(j5).getDoctorname());
                        this.dateAm3.addView(this.doctorBtn);
                    }
                    break;
                case 5:
                    for (int j6 = 0; j6 < size; j6++) {
                        this.doctorBtn = new Button(this.mContext);
                        LinearLayout.LayoutParams layoutParams6 = new LinearLayout.LayoutParams(-2, -2);
                        layoutParams6.bottomMargin = 8;
                        this.doctorBtn.setLayoutParams(layoutParams6);
                        this.doctorBtn.setBackgroundResource(R.drawable.btn_order_selector);
                        int orderNum6 = containerList.get(i).getDoctorNum().get(j6).getCanOrderNum();
                        boolean isStop6 = containerList.get(i).getDoctorNum().get(j6).isStop();
                        if (orderNum6 == 0) {
                            this.doctorBtn.setBackgroundResource(R.drawable.ygkz_order_doctor_button_dark);
                        } else if (orderNum6 == -1) {
                            this.doctorBtn.setBackgroundResource(R.drawable.btn_order_unknown_selector);
                            this.doctorBtn.setEnabled(true);
                        }
                        if (isStop6) {
                            this.doctorBtn.setBackgroundResource(R.drawable.ygkz_order_doctor_button_red);
                        }
                        this.doctorBtn.setTextSize(12.0f);
                        this.doctorBtn.setTextColor(-1);
                        this.doctorBtn.setOnClickListener(new OrderDoctorClickListener(containerList.get(i).getDoctorNum().get(j6), getResources().getString(R.string.afternoon), isStop6));
                        this.doctorBtn.setOnLongClickListener(new CollectDoctorLongClickListener(containerList.get(i).getDoctorNum().get(j6)));
                        this.doctorBtn.setText(containerList.get(i).getDoctorNum().get(j6).getDoctorname());
                        this.datePm3.addView(this.doctorBtn);
                    }
                    break;
            }
        }
    }

    private class OrderDoctorClickListener implements View.OnClickListener {
        private DoctorPlanJsonEntity doctorPlanRecord;
        private boolean isStop;
        private String type;

        public OrderDoctorClickListener(DoctorPlanJsonEntity json, String type2, boolean isStop2) {
            this.doctorPlanRecord = json;
            this.type = type2;
            this.isStop = isStop2;
        }

        public void onClick(View v) {
            if (this.doctorPlanRecord.getCanOrderNum() != 0 && !this.isStop) {
                JSONObject jsonObj = new JSONObject();
                try {
                    JSONArray jArray = new JSONArray();
                    for (int i = 0; i < this.doctorPlanRecord.getRecord().size(); i++) {
                        jArray.put(this.doctorPlanRecord.getRecord().get(i));
                    }
                    jsonObj.put("day", this.doctorPlanRecord.getDate());
                    jsonObj.put("planlist", jArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Intent intent = new Intent(DoctorCalPageItemView.this.mContext, SelectClinicTimeActivity.class);
                intent.putExtra("jsons", jsonObj.toString());
                intent.putExtra(a.b, this.type);
                DoctorCalPageItemView.this.mContext.startActivity(intent);
            }
        }
    }

    private class CollectDoctorLongClickListener implements View.OnLongClickListener {
        private DoctorPlanJsonEntity doctorPlanRecord;

        public CollectDoctorLongClickListener(DoctorPlanJsonEntity json) {
            this.doctorPlanRecord = json;
        }

        public boolean onLongClick(View arg0) {
            DoctorCalPageItemView.this.onDoctorBtnLongClickListener.onDoctorButtonLongClick(this.doctorPlanRecord);
            return true;
        }
    }

    public OnDoctorButtonLongClickListener getOnDoctorBtnLongClickListener() {
        return this.onDoctorBtnLongClickListener;
    }

    public void setOnDoctorBtnLongClickListener(OnDoctorButtonLongClickListener onDoctorBtnLongClickListener2) {
        this.onDoctorBtnLongClickListener = onDoctorBtnLongClickListener2;
    }
}
