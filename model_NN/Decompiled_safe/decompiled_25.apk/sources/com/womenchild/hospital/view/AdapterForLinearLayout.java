package com.womenchild.hospital.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.util.List;
import java.util.Map;

public class AdapterForLinearLayout extends BaseAdapter {
    private List<? extends Map<String, ?>> data;
    private String[] from;
    private LayoutInflater mInflater;
    private int resource;
    private int[] to;

    public AdapterForLinearLayout(Context context, List<? extends Map<String, ?>> data2, int resouce, String[] from2, int[] to2) {
        this.data = data2;
        this.resource = resouce;
        this.data = data2;
        this.from = from2;
        this.to = to2;
        this.mInflater = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    public int getCount() {
        return this.data.size();
    }

    public Object getItem(int position) {
        return this.data.get(position);
    }

    public String get(int position, Object key) {
        return ((Map) getItem(position)).get(key).toString();
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View convertView2 = this.mInflater.inflate(this.resource, (ViewGroup) null);
        Map<String, ?> item = (Map) this.data.get(position);
        int count = this.to.length;
        for (int i = 0; i < count; i++) {
            bindView(convertView2.findViewById(this.to[i]), item, this.from[i]);
        }
        convertView2.setTag(Integer.valueOf(position));
        return convertView2;
    }

    private void bindView(View view, Map<String, ?> item, String from2) {
        Object data2 = item.get(from2);
        if (view instanceof TextView) {
            ((TextView) view).setText(data2 == null ? PoiTypeDef.All : data2.toString());
        }
    }
}
