package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.configure.Constants;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.ClientLogUtil;
import com.womenchild.hospital.util.ImageLoadUtil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HospitalDynamicActivity extends BaseRequestActivity implements View.OnClickListener {
    private static final String TAG = "HospitalDynamicActivity";
    private Button btn_dynamic;
    private Button btn_information;
    private Button btn_noticlist;
    String buttomString;
    String centerString;
    private RadioGroup gdGroup;
    /* access modifiers changed from: private */
    public Intent intent;
    private Button iv_back;
    private ImageView iv_img_bottom;
    private ImageView iv_img_center;
    private ImageView iv_img_top;
    private JSONObject json;
    private JSONArray jsonArray;
    /* access modifiers changed from: private */
    public Context mContext = this;
    RadioGroup.OnCheckedChangeListener onCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        public void onCheckedChanged(RadioGroup arg0, int tabId) {
            switch (tabId) {
                case R.id.btn_dynamic /*2131297111*/:
                default:
                    return;
                case R.id.btn_noticlist /*2131297112*/:
                    HospitalDynamicActivity.this.intent = new Intent(HospitalDynamicActivity.this.mContext, HospitalNoticeActivity.class);
                    HospitalDynamicActivity.this.startActivity(HospitalDynamicActivity.this.intent);
                    HospitalDynamicActivity.this.finish();
                    return;
                case R.id.btn_information /*2131297113*/:
                    HospitalDynamicActivity.this.intent = new Intent(HospitalDynamicActivity.this.mContext, StopPlanActivity.class);
                    HospitalDynamicActivity.this.startActivity(HospitalDynamicActivity.this.intent);
                    HospitalDynamicActivity.this.finish();
                    return;
            }
        }
    };
    private ProgressDialog pDialog;
    String topString;
    private TextView tv_img_bottom_title;
    private TextView tv_img_center_title;
    private TextView tv_img_top_title;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.hospital_dynamic);
        initViewId();
        initClickListener();
        this.pDialog = new ProgressDialog(this);
        this.pDialog.setMessage(getResources().getString(R.string.loading_data));
        this.pDialog.show();
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.HOSPITAL_NOTICE), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.HOSPITAL_NOTICE)));
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void refreshActivity(Object... params) {
        this.pDialog.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            loadData(requestType, (JSONObject) params[2]);
        } else {
            Toast.makeText(this, (int) R.string.network_connect_failed_prompt, 0).show();
        }
    }

    public void initViewId() {
        this.iv_back = (Button) findViewById(R.id.iv_back);
        this.iv_img_top = (ImageView) findViewById(R.id.tv_img_top);
        this.tv_img_top_title = (TextView) findViewById(R.id.tv_img_top_title);
        this.iv_img_center = (ImageView) findViewById(R.id.tv_img_center);
        this.tv_img_center_title = (TextView) findViewById(R.id.tv_img_center_title);
        this.iv_img_bottom = (ImageView) findViewById(R.id.tv_img_bottom);
        this.tv_img_bottom_title = (TextView) findViewById(R.id.tv_img_bottom_title);
        this.gdGroup = (RadioGroup) findViewById(R.id.rl_bottom);
        this.gdGroup.setOnCheckedChangeListener(this.onCheckedChangeListener);
        this.gdGroup.check(R.id.btn_dynamic);
    }

    public void initClickListener() {
        this.iv_back.setOnClickListener(this);
        this.iv_img_top.setOnClickListener(this);
    }

    private boolean stringLength(String str) {
        return str.length() > 1;
    }

    public void loadData(int requestType, Object data) {
        JSONArray clicknotices;
        this.json = (JSONObject) data;
        try {
            if (this.json.optJSONObject("res").optInt("st") == 0 && (clicknotices = this.json.optJSONObject("inf").getJSONArray("notices")) != null) {
                this.tv_img_top_title.setText(clicknotices.optJSONObject(0).optString("title"));
                this.tv_img_top_title.setOnClickListener(new topClickListener(clicknotices.optJSONObject(0).optString("id")));
                this.tv_img_center_title.setText(clicknotices.optJSONObject(1).optString("title"));
                this.tv_img_center_title.setOnClickListener(new topClickListener(clicknotices.optJSONObject(1).optString("id")));
                this.tv_img_bottom_title.setText(clicknotices.optJSONObject(2).optString("title"));
                this.tv_img_bottom_title.setOnClickListener(new topClickListener(clicknotices.optJSONObject(2).optString("id")));
                this.topString = clicknotices.optJSONObject(0).optString("hotpic");
                if (stringLength(this.topString)) {
                    this.topString.substring(1);
                    new ImageLoadUtil(this.iv_img_top).execute(((String) Constants.REQUEST_URI) + this.topString);
                    ClientLogUtil.i(getClass().getSimpleName(), this.topString.substring(1));
                }
                this.iv_img_top.setOnClickListener(new topClickListener(clicknotices.optJSONObject(0).optString("id")));
                this.centerString = clicknotices.optJSONObject(1).optString("hotpic");
                if (stringLength(this.centerString)) {
                    this.centerString.substring(1);
                    new ImageLoadUtil(this.iv_img_center).execute(((String) Constants.REQUEST_URI) + this.centerString);
                }
                this.iv_img_center.setOnClickListener(new topClickListener(clicknotices.optJSONObject(1).optString("id")));
                this.buttomString = clicknotices.optJSONObject(2).optString("hotpic");
                if (stringLength(this.buttomString)) {
                    this.buttomString.substring(1);
                    new ImageLoadUtil(this.iv_img_bottom).execute(((String) Constants.REQUEST_URI) + this.buttomString);
                }
                this.iv_img_bottom.setOnClickListener(new topClickListener(clicknotices.optJSONObject(2).optString("id")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ClientLogUtil.i(TAG, e.getMessage());
        } catch (NullPointerException e2) {
            e2.printStackTrace();
            ClientLogUtil.i(TAG, e2.getMessage());
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back /*2131296529*/:
                finish();
                return;
            default:
                return;
        }
    }

    private class topClickListener implements View.OnClickListener {
        private String id;

        public topClickListener(String id2) {
            this.id = id2;
        }

        public void onClick(View v) {
            Intent intent = new Intent(HospitalDynamicActivity.this, DynamicContentActivity.class);
            intent.putExtra("id", this.id);
            HospitalDynamicActivity.this.startActivity(intent);
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        parameters.add("num", 1);
        return parameters;
    }
}
