package com.womenchild.hospital.entity;

import java.io.Serializable;
import org.json.JSONObject;

public class ErrorPayModel implements Serializable {
    private static final long serialVersionUID = 8010361591123763198L;
    private String cupsqid;
    private String paytime;
    private String payway;
    private int price;
    private String ylordernum;

    public String getPayway() {
        return this.payway;
    }

    public void setPayway(String payway2) {
        this.payway = payway2;
    }

    public void initModel(JSONObject mJsonObject) {
        this.cupsqid = mJsonObject.optString("cupsqid");
        this.price = mJsonObject.optInt("price");
        this.ylordernum = mJsonObject.optString("ylordernum");
        this.paytime = mJsonObject.optString("paytime");
        this.payway = mJsonObject.optString("payWay");
    }

    public String getCupsqid() {
        return this.cupsqid;
    }

    public void setCupsqid(String cupsqid2) {
        this.cupsqid = cupsqid2;
    }

    public int getPrice() {
        return this.price;
    }

    public void setPrice(int price2) {
        this.price = price2;
    }

    public String getYlordernum() {
        return this.ylordernum;
    }

    public void setYlordernum(String ylordernum2) {
        this.ylordernum = ylordernum2;
    }

    public String getPaytime() {
        return this.paytime;
    }

    public void setPaytime(String paytime2) {
        this.paytime = paytime2;
    }
}
