package com.womenchild.hospital.entity;

import java.util.ArrayList;
import org.json.JSONException;
import org.json.JSONObject;

public class DoctorPlanJsonEntity extends ObjectEntity {
    private static final long serialVersionUID = 1;
    private int canOrderNum;
    private String date;
    private String doctorname = null;
    private boolean isStop;
    private ArrayList<JSONObject> record;
    private String timeSpan;

    public DoctorPlanJsonEntity(JSONObject data) throws JSONException {
        parse(data);
    }

    public DoctorPlanJsonEntity() {
    }

    public void parse(JSONObject data) throws JSONException {
        this.doctorname = data.getString("doctorname");
        this.timeSpan = data.getString("timespan");
    }

    public String getDoctorname() {
        return this.doctorname;
    }

    public void setDoctorname(String doctorname2) {
        this.doctorname = doctorname2;
    }

    public ArrayList<JSONObject> getRecord() {
        return this.record;
    }

    public void setRecord(ArrayList<JSONObject> record2) {
        this.record = record2;
    }

    public String getTimeSpan() {
        return this.timeSpan;
    }

    public void setTimeSpan(String timeSpan2) {
        this.timeSpan = timeSpan2;
    }

    public int getCanOrderNum() {
        return this.canOrderNum;
    }

    public void setCanOrderNum(int canOrderNum2) {
        this.canOrderNum = canOrderNum2;
    }

    public boolean isStop() {
        return this.isStop;
    }

    public void setStop(boolean isStop2) {
        this.isStop = isStop2;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String date2) {
        this.date = date2;
    }
}
