package com.womenchild.hospital.entity;

import com.womenchild.hospital.util.ClientLogUtil;
import org.json.JSONObject;

public class VersionInfo {
    private String url;
    private String version;

    public VersionInfo(JSONObject result) {
        if (result != null) {
            try {
                JSONObject inf = result.optJSONObject("inf");
                ClientLogUtil.i("versioninfo", inf.toString());
                this.version = inf.optString("version");
                this.url = inf.optString("url");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version2) {
        this.version = version2;
    }

    public String toString() {
        return "VersionInfo [version=" + this.version + ", addr=" + this.url + "]";
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }
}
