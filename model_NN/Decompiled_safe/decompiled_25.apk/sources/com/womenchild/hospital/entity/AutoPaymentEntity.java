package com.womenchild.hospital.entity;

import org.json.JSONArray;

public class AutoPaymentEntity {
    private JSONArray billOrders;
    private String cardType;
    private String clinicTranLine;
    private String deptName;
    private String fee;
    private String hisPatientId;
    private String patientCard;
    private String patientName;
    private String scheduleTime;
    private String state;

    public AutoPaymentEntity(String patientName2, String patientCard2, String cardType2, String scheduleTime2, String deptName2, String fee2, String clinicTranLine2, String hisPatientId2, JSONArray billOrders2, String state2) {
        this.patientName = patientName2;
        this.patientCard = patientCard2;
        this.scheduleTime = scheduleTime2;
        this.deptName = deptName2;
        this.fee = fee2;
        this.billOrders = billOrders2;
        this.clinicTranLine = clinicTranLine2;
        this.hisPatientId = hisPatientId2;
        this.cardType = cardType2;
        this.state = state2;
    }

    public String getCardType() {
        return this.cardType;
    }

    public void setCardType(String cardType2) {
        this.cardType = cardType2;
    }

    public String getClinicTranLine() {
        return this.clinicTranLine;
    }

    public void setClinicTranLine(String clinicTranLine2) {
        this.clinicTranLine = clinicTranLine2;
    }

    public String getHisPatientId() {
        return this.hisPatientId;
    }

    public void setHisPatientId(String hisPatientId2) {
        this.hisPatientId = hisPatientId2;
    }

    public String getDeptName() {
        return this.deptName;
    }

    public void setDeptName(String deptName2) {
        this.deptName = deptName2;
    }

    public String getPatientName() {
        return this.patientName;
    }

    public void setPatientName(String patientName2) {
        this.patientName = patientName2;
    }

    public String getPatientCard() {
        return this.patientCard;
    }

    public void setPatientCard(String patientCard2) {
        this.patientCard = patientCard2;
    }

    public String getScheduleTime() {
        return this.scheduleTime;
    }

    public void setScheduleTime(String scheduleTime2) {
        this.scheduleTime = scheduleTime2;
    }

    public String getFee() {
        return this.fee;
    }

    public void setFee(String fee2) {
        this.fee = fee2;
    }

    public JSONArray getBillOrders() {
        return this.billOrders;
    }

    public void setBillOrders(JSONArray billOrders2) {
        this.billOrders = billOrders2;
    }

    public String getState() {
        return this.state;
    }

    public void setState(String state2) {
        this.state = state2;
    }
}
