package com.womenchild.hospital.entity;

import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.AddMedicalCardActivity;
import com.womenchild.hospital.parameter.UriParameter;

public class OrderEntity {
    private String address;
    private String birthday;
    private String cardType = "0";
    private String cardid;
    private String gender;
    private String guardian = "1";
    private String mobile;
    private int ordernum = 0;
    private String outPatientType = "0";
    private String patientCard;
    private String patientCardType = AddMedicalCardActivity.PATIENTCARDTYPE;
    private String patientName;
    private String phone;
    private long planId;
    private String prePay = "0";
    private String sceneServiceIds = "0";

    public UriParameter createParameters() {
        UriParameter parameters = new UriParameter();
        if (this.phone == null || PoiTypeDef.All.equals(this.phone)) {
            this.phone = this.mobile;
        }
        parameters.add("phone", this.phone);
        parameters.add("mobile", this.mobile);
        parameters.add("gender", this.gender);
        parameters.add("cardid", this.cardid);
        parameters.add("cardtype", this.cardType);
        parameters.add("patientname", this.patientName);
        parameters.add("patientcard", this.patientCard);
        parameters.add("patientcardtype", this.patientCardType);
        parameters.add("outpatienttype", this.outPatientType);
        parameters.add("address", this.address);
        parameters.add("planid", Long.valueOf(this.planId));
        parameters.add("sceneserviceids", this.sceneServiceIds);
        parameters.add("prepay", this.prePay);
        parameters.add("birthday", this.birthday);
        parameters.add("guardian", this.guardian);
        parameters.add("ordernum", Integer.valueOf(this.ordernum));
        return parameters;
    }

    public int getOrdernum() {
        return this.ordernum;
    }

    public void setOrdernum(int ordernum2) {
        this.ordernum = ordernum2;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String mobile2) {
        this.mobile = mobile2;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone2) {
        this.phone = phone2;
    }

    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender2) {
        this.gender = gender2;
    }

    public String getCardid() {
        return this.cardid;
    }

    public void setCardid(String cardid2) {
        this.cardid = cardid2;
    }

    public String getCardType() {
        return this.cardType;
    }

    public void setCardType(String cardType2) {
        this.cardType = cardType2;
    }

    public String getPatientName() {
        return this.patientName;
    }

    public void setPatientName(String patientName2) {
        this.patientName = patientName2;
    }

    public String getPatientCard() {
        return this.patientCard;
    }

    public void setPatientCard(String patientCard2) {
        this.patientCard = patientCard2;
    }

    public String getPatientCardType() {
        return this.patientCardType;
    }

    public void setPatientCardType(String patientCardType2) {
        if (patientCardType2 != null && !PoiTypeDef.All.equals(patientCardType2)) {
            this.patientCardType = patientCardType2;
        }
    }

    public String getOutPatientType() {
        return this.outPatientType;
    }

    public void setOutPatientType(String outPatientType2) {
        this.outPatientType = outPatientType2;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address2) {
        this.address = address2;
    }

    public long getPlanId() {
        return this.planId;
    }

    public void setPlanId(long planId2) {
        this.planId = planId2;
    }

    public String getSceneServiceIds() {
        return this.sceneServiceIds;
    }

    public void setSceneServiceIds(String sceneServiceIds2) {
        this.sceneServiceIds = sceneServiceIds2;
    }

    public String getPrePay() {
        return this.prePay;
    }

    public void setPrePay(String prePay2) {
        this.prePay = prePay2;
    }

    public String getBirthday() {
        return this.birthday;
    }

    public void setBirthday(String birthday2) {
        this.birthday = birthday2;
    }

    public String getGuardian() {
        return this.guardian;
    }

    public void setGuardian(String guardian2) {
        this.guardian = guardian2;
    }
}
