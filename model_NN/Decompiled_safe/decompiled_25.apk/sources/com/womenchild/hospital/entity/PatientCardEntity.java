package com.womenchild.hospital.entity;

import org.json.JSONObject;

public class PatientCardEntity {
    private String card;
    private String cardid;
    private String hospital;
    private int patientCardDefault;
    private String patientcardtype;
    private String userid;

    public PatientCardEntity() {
    }

    public PatientCardEntity(String userid2, String patientcardtype2, String card2, String hospital2, String cardid2, int defaultType) {
        this.userid = userid2;
        this.patientcardtype = patientcardtype2;
        this.card = card2;
        this.hospital = hospital2;
        this.cardid = cardid2;
        this.patientCardDefault = defaultType;
    }

    public PatientCardEntity(JSONObject result) {
        this.userid = result.optString("userid");
        this.patientcardtype = result.optString("patientcardtype");
        this.card = result.optString("card");
        this.hospital = result.optString("hospital");
        this.cardid = result.optString("cardid");
        this.patientCardDefault = result.optInt("patientcarddefault");
    }

    public String getUserid() {
        return this.userid;
    }

    public void setUserid(String userid2) {
        this.userid = userid2;
    }

    public String getPatientcardtype() {
        return this.patientcardtype;
    }

    public void setPatientcardtype(String patientcardtype2) {
        this.patientcardtype = patientcardtype2;
    }

    public String getCard() {
        return this.card;
    }

    public void setCard(String card2) {
        this.card = card2;
    }

    public String getHospital() {
        return this.hospital;
    }

    public void setHospital(String hospital2) {
        this.hospital = hospital2;
    }

    public String getCardid() {
        return this.cardid;
    }

    public void setCardid(String cardid2) {
        this.cardid = cardid2;
    }

    public String toString() {
        return "PatientCardEntity [userid=" + this.userid + ", patientcardtype=" + this.patientcardtype + ", card=" + this.card + ", hospital=" + this.hospital + ", cardid=" + this.cardid + "]";
    }

    public int getPatientCardDefault() {
        return this.patientCardDefault;
    }

    public void setPatientCardDefault(int patientCardDefault2) {
        this.patientCardDefault = patientCardDefault2;
    }
}
