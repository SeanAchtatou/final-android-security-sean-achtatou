package com.womenchild.hospital.entity;

import android.content.Context;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PaymentEntity extends ObjectEntity {
    private static final long serialVersionUID = 1;
    private String feeForm;
    private String ordernum;
    private String paydate;
    private String payway;
    private double price;
    private String realOrderNum;
    private int state;
    private String userid;

    public String getPayway() {
        return this.payway;
    }

    public void setPayway(String payway2) {
        this.payway = payway2;
    }

    public PaymentEntity() {
    }

    public PaymentEntity(JSONObject data) throws JSONException {
        parse(data);
    }

    public int getState() {
        return this.state;
    }

    public void setState(int state2) {
        this.state = state2;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price2) {
        this.price = price2;
    }

    public String getOrdernum() {
        return this.ordernum;
    }

    public void setOrdernum(String ordernum2) {
        this.ordernum = ordernum2;
    }

    public String getUserid() {
        return this.userid;
    }

    public void setUserid(String userid2) {
        this.userid = userid2;
    }

    public String getPaydate() {
        return this.paydate;
    }

    public void setPaydate(String paydate2) {
        this.paydate = paydate2;
    }

    public static ArrayList<PaymentEntity> setJSONtoPaymentEntityList(Context context, JSONArray jsonArray) throws JSONException {
        ArrayList<PaymentEntity> list = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            list.add(new PaymentEntity((JSONObject) jsonArray.get(i)));
        }
        return list;
    }

    public void parse(JSONObject data) throws JSONException {
        this.price = data.optDouble("orderpay");
        this.ordernum = data.optString("opcorderid");
        this.state = data.optInt("paystatus");
        this.userid = data.optString("doctorid");
        this.paydate = data.optString("paytime");
        this.feeForm = data.optString("deptname");
        this.realOrderNum = data.optString("ordernum");
        this.payway = data.optString("payWay");
    }

    public String getFeeForm() {
        return this.feeForm;
    }

    public void setFeeForm(String feeForm2) {
        this.feeForm = feeForm2;
    }

    public String getRealOrderNum() {
        return this.realOrderNum;
    }

    public void setRealOrderNum(String realOrderNum2) {
        this.realOrderNum = realOrderNum2;
    }
}
