package com.womenchild.hospital.entity;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class CardType {
    private String key;
    private String value;

    public static List<CardType> getList(JSONObject result) {
        JSONObject inf = result.optJSONObject("inf");
        if (inf != null) {
            JSONArray opccardtypes = inf.optJSONArray("opccardtypes");
            int length = opccardtypes.length();
            if (opccardtypes != null && length > 0) {
                List<CardType> list = new ArrayList<>(length);
                for (int i = 0; i < length; i++) {
                    JSONObject object = opccardtypes.optJSONObject(i);
                    CardType cardType = new CardType();
                    cardType.key = object.optString("key");
                    cardType.value = object.optString("value");
                    list.add(cardType);
                }
                return list;
            }
        }
        return null;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value2) {
        this.value = value2;
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String key2) {
        this.key = key2;
    }

    public String toString() {
        return "CardType [value=" + this.value + ", key=" + this.key + "]";
    }
}
