package com.womenchild.hospital.entity;

import com.womenchild.hospital.util.ClientLogUtil;
import org.json.JSONException;
import org.json.JSONObject;

public class PatientEntity {
    private String TAG = "PatientEntity";
    private String accId = null;
    private String address = null;
    private String citizenCard = null;
    private String defaultPatientCard = null;
    private String email = null;
    private String healthyCard = null;
    private String idCard = null;
    private String mobile = null;
    private String name = null;
    private String socialSecurity = null;
    private String userid = null;

    public PatientEntity() {
    }

    public PatientEntity(JSONObject jsonObject) {
        try {
            this.accId = jsonObject.getString("accid");
            this.citizenCard = jsonObject.getString("citizzencard");
            this.defaultPatientCard = jsonObject.getString("defaultpatientcard");
            this.email = jsonObject.getString("email");
            this.healthyCard = jsonObject.getString("healthycard");
            this.idCard = jsonObject.getString("idcard");
            this.name = jsonObject.getString("patientname");
            this.socialSecurity = jsonObject.getString("socialsecuritycard");
            this.mobile = jsonObject.getString("mobile");
            this.address = jsonObject.getString("address");
            this.userid = jsonObject.getString("userid");
        } catch (JSONException e) {
            e.printStackTrace();
            ClientLogUtil.e(this.TAG, e.getMessage());
        }
    }

    public PatientEntity(String accId2, String name2, String idCard2, String email2, String socialSecurity2, String healthyCard2, String citizenCard2, String defaultPatientCard2, String mobile2, String address2, String userId) {
        this.accId = accId2;
        this.citizenCard = citizenCard2;
        this.defaultPatientCard = defaultPatientCard2;
        this.email = email2;
        this.healthyCard = healthyCard2;
        this.idCard = idCard2;
        this.name = name2;
        this.socialSecurity = socialSecurity2;
        this.mobile = mobile2;
        this.address = address2;
        this.userid = userId;
    }

    public String getAccId() {
        return this.accId;
    }

    public void setAccId(String accId2) {
        this.accId = accId2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getIdCard() {
        return this.idCard;
    }

    public void setIdCard(String idCard2) {
        this.idCard = idCard2;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email2) {
        this.email = email2;
    }

    public String getSocialSecurity() {
        return this.socialSecurity;
    }

    public void setSocialSecurity(String socialSecurity2) {
        this.socialSecurity = socialSecurity2;
    }

    public String getHealthyCard() {
        return this.healthyCard;
    }

    public void setHealthyCard(String healthyCard2) {
        this.healthyCard = healthyCard2;
    }

    public String getCitizenCard() {
        return this.citizenCard;
    }

    public void setCitizenCard(String citizenCard2) {
        this.citizenCard = citizenCard2;
    }

    public String getDefaultPatientCard() {
        return this.defaultPatientCard;
    }

    public void setDefaultPatientCard(String defaultPatientCard2) {
        this.defaultPatientCard = defaultPatientCard2;
    }

    public String getUserid() {
        return this.userid;
    }

    public void setUserid(String userid2) {
        this.userid = userid2;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String mobile2) {
        this.mobile = mobile2;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address2) {
        this.address = address2;
    }
}
