package com.womenchild.hospital.entity;

import java.io.Serializable;

public class TestPrjSubEntity implements Serializable {
    private static final long serialVersionUID = 1;
    private String itemName;
    private String reference;
    private String result;
    private String unit;

    public String getItemName() {
        return this.itemName;
    }

    public void setItemName(String itemName2) {
        this.itemName = itemName2;
    }

    public String getUnit() {
        return this.unit;
    }

    public void setUnit(String unit2) {
        this.unit = unit2;
    }

    public String getResult() {
        return this.result;
    }

    public void setResult(String result2) {
        this.result = result2;
    }

    public String getReference() {
        return this.reference;
    }

    public void setReference(String reference2) {
        this.reference = reference2;
    }

    public String toString() {
        return new StringBuilder(this.itemName).toString();
    }
}
