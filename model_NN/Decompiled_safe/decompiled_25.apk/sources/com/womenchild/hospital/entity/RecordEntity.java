package com.womenchild.hospital.entity;

import com.amap.mapapi.location.LocationManagerProxy;
import com.womenchild.hospital.util.DateUtil;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class RecordEntity {
    private String deptname;
    private String doctorid;
    private String doctorname;
    private int fee;
    private String hospitalname;
    private String opcorderid;
    private String opctime;
    private String paytime;
    private String person;
    private String planstarttime;
    private String planstoptime;
    private String status;

    public static List<RecordEntity> getList(JSONObject result) {
        JSONObject infObj1;
        JSONObject infObj2;
        JSONArray infArray;
        if (result == null || (infObj1 = result.optJSONObject("inf")) == null || (infObj2 = infObj1.optJSONObject("inf")) == null || (infArray = infObj2.optJSONArray("reglist")) == null) {
            return null;
        }
        int length = infArray.length();
        List<RecordEntity> lists = new ArrayList<>(length);
        for (int i = 0; i < length; i++) {
            JSONObject object = infArray.optJSONObject(i);
            RecordEntity entity = new RecordEntity();
            entity.opcorderid = object.optString("opcorderid");
            entity.deptname = object.optString("deptname");
            entity.doctorname = object.optString("doctorname");
            entity.planstarttime = object.optString("planstarttime");
            entity.planstoptime = object.optString("planstoptime");
            if (DateUtil.isSameDate(object.optLong("planstarttime"))) {
                entity.status = "正在排队就诊";
            } else {
                entity.status = object.optString(LocationManagerProxy.KEY_STATUS_CHANGED);
            }
            entity.hospitalname = object.optString("hospitalname");
            entity.person = object.optString("person");
            entity.opctime = object.optString("opctime");
            entity.paytime = object.optString("paytime");
            entity.doctorid = object.optString("doctorid");
            entity.fee = object.optInt("traderpayamount");
            lists.add(entity);
        }
        return lists;
    }

    public String getOpcorderid() {
        return this.opcorderid;
    }

    public void setOpcorderid(String opcorderid2) {
        this.opcorderid = opcorderid2;
    }

    public String getDeptname() {
        return this.deptname;
    }

    public void setDeptname(String deptname2) {
        this.deptname = deptname2;
    }

    public String getDoctorname() {
        return this.doctorname;
    }

    public void setDoctorname(String doctorname2) {
        this.doctorname = doctorname2;
    }

    public String getPlanstarttime() {
        return this.planstarttime;
    }

    public void setPlanstarttime(String planstarttime2) {
        this.planstarttime = planstarttime2;
    }

    public String getPlanstoptime() {
        return this.planstoptime;
    }

    public void setPlanstoptime(String planstoptime2) {
        this.planstoptime = planstoptime2;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status2) {
        this.status = status2;
    }

    public String getHospitalname() {
        return this.hospitalname;
    }

    public void setHospitalname(String hospitalname2) {
        this.hospitalname = hospitalname2;
    }

    public String getPerson() {
        return this.person;
    }

    public void setPerson(String person2) {
        this.person = person2;
    }

    public String getOpctime() {
        return this.opctime;
    }

    public void setOpctime(String opctime2) {
        this.opctime = opctime2;
    }

    public String getPaytime() {
        return this.paytime;
    }

    public void setPaytime(String paytime2) {
        this.paytime = paytime2;
    }

    public String getDoctorid() {
        return this.doctorid;
    }

    public void setDoctorid(String doctorid2) {
        this.doctorid = doctorid2;
    }

    public String toString() {
        return super.toString();
    }

    public int getFee() {
        return this.fee;
    }

    public void setFee(int fee2) {
        this.fee = fee2;
    }
}
