package com.womenchild.hospital.entity;

import java.io.Serializable;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class ObjectEntity implements Serializable {
    private static final long serialVersionUID = 1;

    public abstract void parse(JSONObject jSONObject) throws JSONException;
}
