package com.womenchild.hospital.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class QueueCallVisEntity implements Serializable {
    private static final long serialVersionUID = 1;
    private String currentNum;
    private String doctorname;
    private String frontWaiting;
    private String hispatientid;
    private String patientSerialNo;

    public static List<QueueCallVisEntity> getList(JSONObject result) {
        JSONObject infObj1;
        JSONArray infArray;
        if (result == null || (infObj1 = result.optJSONObject("inf")) == null || (infArray = infObj1.optJSONArray("queues")) == null) {
            return null;
        }
        int length = infArray.length();
        List<QueueCallVisEntity> lists = new ArrayList<>(length);
        for (int i = 0; i < length; i++) {
            JSONObject object = infArray.optJSONObject(i);
            QueueCallVisEntity entity = new QueueCallVisEntity();
            entity.doctorname = object.optString("doctorname");
            entity.frontWaiting = object.optString("frontwaiting");
            entity.patientSerialNo = object.optString("ordernum");
            entity.currentNum = object.optString("currentordernum");
            entity.hispatientid = object.optString("hispatientid");
            lists.add(entity);
        }
        return lists;
    }

    public String getDoctorname() {
        return this.doctorname;
    }

    public void setDoctorname(String doctorname2) {
        this.doctorname = doctorname2;
    }

    public String getFrontWaiting() {
        return this.frontWaiting;
    }

    public void setFrontWaiting(String frontWaiting2) {
        this.frontWaiting = frontWaiting2;
    }

    public String getPatientSerialNo() {
        return this.patientSerialNo;
    }

    public void setPatientSerialNo(String patientSerialNo2) {
        this.patientSerialNo = patientSerialNo2;
    }

    public String toString() {
        return super.toString();
    }

    public String getHispatientid() {
        return this.hispatientid;
    }

    public void setHispatientid(String hispatientid2) {
        this.hispatientid = hispatientid2;
    }

    public String getCurrentNum() {
        return this.currentNum;
    }

    public void setCurrentNum(String currentNum2) {
        this.currentNum = currentNum2;
    }
}
