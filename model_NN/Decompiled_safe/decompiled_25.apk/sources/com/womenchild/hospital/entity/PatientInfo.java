package com.womenchild.hospital.entity;

import java.io.Serializable;

public class PatientInfo implements Serializable {
    private static final long serialVersionUID = 1;
    private String address;
    private String citizzencard;
    private String defaultPatientCard;
    private int defaultnum;
    private String gender;
    private String healthyCard;
    private String idcard;
    private String mEmail;
    private String mobile;
    private String patientid;
    private String patientname;
    private String socialSecurityCard;
    private String userid;

    public String getPatientid() {
        return this.patientid;
    }

    public void setPatientid(String patientid2) {
        this.patientid = patientid2;
    }

    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender2) {
        this.gender = gender2;
    }

    public int getDefaultnum() {
        return this.defaultnum;
    }

    public void setDefaultnum(int defaultnum2) {
        this.defaultnum = defaultnum2;
    }

    public String getPatientname() {
        return this.patientname;
    }

    public void setPatientname(String patientname2) {
        this.patientname = patientname2;
    }

    public String getUserid() {
        return this.userid;
    }

    public void setUserid(String userid2) {
        this.userid = userid2;
    }

    public String getIdcard() {
        return this.idcard;
    }

    public void setIdcard(String idcard2) {
        this.idcard = idcard2;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address2) {
        this.address = address2;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String mobile2) {
        this.mobile = mobile2;
    }

    public String getCitizzencard() {
        return this.citizzencard;
    }

    public void setCitizzencard(String citizzencard2) {
        this.citizzencard = citizzencard2;
    }

    public String getmEmail() {
        return this.mEmail;
    }

    public void setmEmail(String mEmail2) {
        this.mEmail = mEmail2;
    }

    public String getSocialSecurityCard() {
        return this.socialSecurityCard;
    }

    public void setSocialSecurityCard(String socialSecurityCard2) {
        this.socialSecurityCard = socialSecurityCard2;
    }

    public String getHealthyCard() {
        return this.healthyCard;
    }

    public void setHealthyCard(String healthyCard2) {
        this.healthyCard = healthyCard2;
    }

    public String getDefaultPatientCard() {
        return this.defaultPatientCard;
    }

    public void setDefaultPatientCard(String defaultPatientCard2) {
        this.defaultPatientCard = defaultPatientCard2;
    }
}
