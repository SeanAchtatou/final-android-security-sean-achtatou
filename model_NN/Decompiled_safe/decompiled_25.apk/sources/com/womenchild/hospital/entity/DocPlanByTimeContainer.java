package com.womenchild.hospital.entity;

import java.util.ArrayList;
import org.json.JSONException;
import org.json.JSONObject;

public class DocPlanByTimeContainer extends ObjectEntity {
    private static final long serialVersionUID = 1;
    private boolean amOrPm;
    private ArrayList<DoctorPlanJsonEntity> childList;
    private ArrayList<DoctorPlanJsonEntity> doctorNum;
    private String planTime;

    public DocPlanByTimeContainer(JSONObject data) throws JSONException {
        parse(data);
    }

    public void parse(JSONObject data) throws JSONException {
        this.planTime = data.getString("day");
    }

    public String getPlanTime() {
        return this.planTime;
    }

    public void setPlanTime(String planTime2) {
        this.planTime = planTime2;
    }

    public ArrayList<DoctorPlanJsonEntity> getChildList() {
        return this.childList;
    }

    public void setChildList(ArrayList<DoctorPlanJsonEntity> childList2) {
        this.childList = childList2;
    }

    public boolean isAmOrPm() {
        return this.amOrPm;
    }

    public void setAmOrPm(boolean amOrPm2) {
        this.amOrPm = amOrPm2;
    }

    public ArrayList<DoctorPlanJsonEntity> getDoctorNum() {
        return this.doctorNum;
    }

    public void setDoctorNum(ArrayList<DoctorPlanJsonEntity> doctorNum2) {
        this.doctorNum = doctorNum2;
    }
}
