package com.womenchild.hospital.entity;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PatientCardListEntity {
    private List<PatientCardEntity> cardList;
    private String patientid;
    private String patientname;

    public static List<PatientCardListEntity> getList(JSONObject result) {
        JSONArray listobject;
        if (result != null) {
            try {
                JSONObject inf = result.optJSONObject("inf");
                if (!(inf == null || (listobject = inf.optJSONArray("listobject")) == null)) {
                    int length = listobject.length();
                    List<PatientCardListEntity> patientCardList = new ArrayList<>(length);
                    for (int i = 0; i < length; i++) {
                        JSONObject object = listobject.getJSONObject(i);
                        PatientCardListEntity entity = new PatientCardListEntity();
                        entity.patientname = object.optString("patientname");
                        entity.patientid = object.optString("patientid");
                        JSONArray array = object.optJSONArray("patientcards");
                        if (array != null) {
                            entity.cardList = new ArrayList(array.length());
                            for (int j = 0; j < array.length(); j++) {
                                entity.cardList.add(new PatientCardEntity(array.getJSONObject(j)));
                            }
                        }
                        patientCardList.add(entity);
                    }
                    return patientCardList;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public String getPatientname() {
        return this.patientname;
    }

    public void setPatientname(String patientname2) {
        this.patientname = patientname2;
    }

    public String getPatientid() {
        return this.patientid;
    }

    public void setPatientid(String patientid2) {
        this.patientid = patientid2;
    }

    public List<PatientCardEntity> getCardList() {
        return this.cardList;
    }

    public void setCardList(List<PatientCardEntity> cardList2) {
        this.cardList = cardList2;
    }

    public String toString() {
        return "PatientCardListEntity [patientname=" + this.patientname + ", patientid=" + this.patientid + ", cardList=" + this.cardList + "]";
    }
}
