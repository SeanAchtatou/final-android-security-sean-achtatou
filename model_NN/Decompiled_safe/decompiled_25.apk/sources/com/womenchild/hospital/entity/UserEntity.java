package com.womenchild.hospital.entity;

import com.amap.mapapi.poisearch.PoiTypeDef;

public class UserEntity {
    private static UserEntity currentUser = null;
    private UserInfoEntity info;
    private String password = PoiTypeDef.All;
    private String username = PoiTypeDef.All;

    private UserEntity() {
    }

    public static UserEntity getInstance() {
        if (currentUser == null) {
            currentUser = new UserEntity();
        }
        return currentUser;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username2) {
        this.username = username2;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password2) {
        this.password = password2;
    }

    public UserInfoEntity getInfo() {
        return this.info;
    }

    public void setInfo(UserInfoEntity info2) {
        this.info = info2;
    }
}
