package com.womenchild.hospital.entity;

public class DoctorEntity {
    private String department;
    private String doctoryID;
    private String favID;
    private String hospital;
    private String level;
    private String name;
    private String orderedDate;

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getHospital() {
        return this.hospital;
    }

    public void setHospital(String hospital2) {
        this.hospital = hospital2;
    }

    public String getDepartment() {
        return this.department;
    }

    public void setDepartment(String department2) {
        this.department = department2;
    }

    public String getOrderedDate() {
        return this.orderedDate;
    }

    public void setOrderedDate(String orderedDate2) {
        this.orderedDate = orderedDate2;
    }

    public String getLevel() {
        return this.level;
    }

    public void setLevel(String level2) {
        this.level = level2;
    }

    public String getDoctoryID() {
        return this.doctoryID;
    }

    public void setDoctoryID(String doctoryID2) {
        this.doctoryID = doctoryID2;
    }

    public String getFavID() {
        return this.favID;
    }

    public void setFavID(String favID2) {
        this.favID = favID2;
    }
}
