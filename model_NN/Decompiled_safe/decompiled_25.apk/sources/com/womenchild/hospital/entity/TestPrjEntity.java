package com.womenchild.hospital.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class TestPrjEntity implements Serializable {
    private static final long serialVersionUID = 1;
    private boolean isUpload;
    private String organName;
    private String patientName;
    private String projectName;
    private ArrayList<TestPrjSubEntity> subList;
    private String updateDate;
    private String updateTime;

    public String getPatientName() {
        return this.patientName;
    }

    public void setPatientName(String patientName2) {
        this.patientName = patientName2;
    }

    public String getProjectName() {
        return this.projectName;
    }

    public void setProjectName(String projectName2) {
        this.projectName = projectName2;
    }

    public String getUpdateDate() {
        return this.updateDate;
    }

    public void setUpdateDate(String updateDate2) {
        this.updateDate = updateDate2;
    }

    public String getUpdateTime() {
        return this.updateTime;
    }

    public void setUpdateTime(String updateTime2) {
        this.updateTime = updateTime2;
    }

    public boolean isUpload() {
        return this.isUpload;
    }

    public void setUpload(boolean isUpload2) {
        this.isUpload = isUpload2;
    }

    public String toString() {
        return new StringBuilder(this.projectName).toString();
    }

    public String getOrganName() {
        return this.organName;
    }

    public void setOrganName(String organName2) {
        this.organName = organName2;
    }

    public ArrayList<TestPrjSubEntity> getSubList() {
        return this.subList;
    }

    public void setSubList(ArrayList<TestPrjSubEntity> subList2) {
        this.subList = subList2;
    }
}
