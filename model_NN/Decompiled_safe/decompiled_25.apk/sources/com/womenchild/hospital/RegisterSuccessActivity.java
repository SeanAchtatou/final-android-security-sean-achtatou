package com.womenchild.hospital;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.configure.Constants;
import com.womenchild.hospital.entity.CardType;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.entity.UserInfoEntity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.request.RequestTask;
import com.womenchild.hospital.util.ClientLogUtil;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public class RegisterSuccessActivity extends BaseRequestActivity implements View.OnClickListener, View.OnFocusChangeListener {
    private static final String TAG = "RSActivity";
    private String address = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public String cardNum;
    /* access modifiers changed from: private */
    public EditText cardNumEt;
    private List<CardType> cardTypes;
    private String citizzencard = PoiTypeDef.All;
    private String defaultpatientcard = PoiTypeDef.All;
    private String email = PoiTypeDef.All;
    private EditText et_address;
    private EditText et_citizen_card;
    /* access modifiers changed from: private */
    public EditText et_diagnosis_card;
    private EditText et_email;
    private EditText et_name;
    private EditText et_pid;
    private EditText et_resident_health_card;
    private EditText et_social_security_card;
    private String healthycard = PoiTypeDef.All;
    private String idcard = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public Spinner mSpinner;
    private String mobile = PoiTypeDef.All;
    private String patientname = PoiTypeDef.All;
    private ProgressDialog progressDialog;
    /* access modifiers changed from: private */
    public int selectPosition = 0;
    private String socialsecuritycard = PoiTypeDef.All;
    private TextView tv_return_home;
    private TextView tv_submit;
    private TextView tv_welcome_user;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.register_success);
        initViewId();
        initClickListener();
        this.tv_welcome_user.setText(UserEntity.getInstance().getUsername());
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void refreshActivity(Object... params) {
        this.progressDialog.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        boolean status = ((Boolean) params[1]).booleanValue();
        if (status) {
            JSONObject result = (JSONObject) params[2];
            ClientLogUtil.i(TAG, "refreshActivity() Request type:" + requestType);
            ClientLogUtil.i(TAG, "refreshActivity() Status:" + status);
            ClientLogUtil.i(TAG, "refreshActivity() Result:" + result);
            int resultCode = result.optJSONObject("res").optInt("st");
            String msg = result.optJSONObject("res").optString("msg");
            switch (resultCode) {
                case 0:
                    if (requestType == 126) {
                        this.cardTypes = CardType.getList(result);
                        showPatientCardTypeDialog("选择诊疗卡", this.cardTypes);
                        return;
                    } else if (requestType == 104) {
                        validateData(result);
                        return;
                    } else {
                        return;
                    }
                case 1:
                    Toast.makeText(this, msg, 0).show();
                    return;
                case 99:
                    Toast.makeText(this, msg, 0).show();
                    return;
                default:
                    return;
            }
        } else {
            Toast.makeText(this, (int) R.string.network_connect_failed_prompt, 0).show();
        }
    }

    public void initViewId() {
        this.tv_return_home = (TextView) findViewById(R.id.tv_return_home);
        this.tv_welcome_user = (TextView) findViewById(R.id.tv_welcome_user);
        this.tv_submit = (TextView) findViewById(R.id.tv_submit);
        this.et_name = (EditText) findViewById(R.id.et_name);
        this.et_pid = (EditText) findViewById(R.id.et_pid);
        this.et_diagnosis_card = (EditText) findViewById(R.id.et_diagnosis_card);
        this.et_email = (EditText) findViewById(R.id.et_email);
        this.et_social_security_card = (EditText) findViewById(R.id.et_social_security_card);
        this.et_resident_health_card = (EditText) findViewById(R.id.et_resident_health_card);
        this.et_citizen_card = (EditText) findViewById(R.id.et_citizen_card);
        this.et_address = (EditText) findViewById(R.id.et_address);
    }

    public void initClickListener() {
        this.tv_return_home.setOnClickListener(this);
        this.tv_submit.setOnClickListener(this);
        this.et_diagnosis_card.setOnFocusChangeListener(this);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        return super.onKeyUp(keyCode, event);
    }

    private void validateData(JSONObject result) {
        try {
            if ("0".equals(result.getJSONObject("res").getString("st"))) {
                Toast.makeText(this, result.getJSONObject("res").getString("msg"), 0).show();
                UserInfoEntity info = UserEntity.getInstance().getInfo();
                info.setCitizenCard(this.citizzencard);
                info.setDefaultPatientCard(this.defaultpatientcard);
                info.setEmail(this.email);
                info.setHealthyCard(this.healthycard);
                info.setIdCard(this.idcard);
                info.setName(this.patientname);
                info.setAddress(this.address);
                info.setSocialSecurity(this.socialsecuritycard);
                Toast.makeText(this, getResources().getString(R.string.submit_ok), 0).show();
                setResult(1, getIntent());
                finish();
                return;
            }
            Toast.makeText(this, result.getJSONObject("res").getString("msg"), 0).show();
        } catch (Exception ex) {
            ClientLogUtil.e(TAG, "Exception:" + ex.toString());
        }
    }

    public void onClick(View v) {
        if (this.tv_return_home == v) {
            setResult(1, getIntent());
            finish();
        } else if (this.tv_submit == v) {
            this.patientname = this.et_name.getText().toString().trim();
            this.defaultpatientcard = this.et_diagnosis_card.getText().toString().trim();
            this.socialsecuritycard = this.et_social_security_card.getText().toString().trim();
            this.email = this.et_email.getText().toString().trim();
            this.idcard = this.et_pid.getText().toString().trim();
            this.healthycard = this.et_resident_health_card.getText().toString().trim();
            this.citizzencard = this.et_citizen_card.getText().toString().trim();
            this.address = this.et_address.getText().toString().trim();
            if (this.patientname == null || PoiTypeDef.All.equals(this.patientname)) {
                Toast.makeText(this, getResources().getString(R.string.user_null), 0).show();
                return;
            }
            this.progressDialog = new ProgressDialog(this);
            this.progressDialog.setMessage(getResources().getString(R.string.submit_pw));
            this.progressDialog.setCancelable(true);
            this.progressDialog.setCanceledOnTouchOutside(false);
            this.progressDialog.show();
            RequestTask.getInstance().sendHttpRequest(this, String.valueOf((int) HttpRequestParameters.ADD_USER_INFO), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.ADD_USER_INFO)));
        }
    }

    public void loadData(int requestType, Object data) {
    }

    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.et_diagnosis_card:
                if (this.et_diagnosis_card.hasFocus()) {
                    sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.READ_CARD_TYPE), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.READ_CARD_TYPE)));
                    this.et_diagnosis_card.clearFocus();
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void showPatientCardTypeDialog(String content, List<CardType> cardTypes2) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        TextView titleTv = new TextView(this);
        titleTv.setTextSize(20.0f);
        titleTv.getPaint().setFakeBoldText(true);
        titleTv.setText(content);
        titleTv.setGravity(1);
        titleTv.setPadding(0, 10, 10, 0);
        builder.setCustomTitle(titleTv);
        View dialogaddrss = LayoutInflater.from(this).inflate((int) R.layout.dlg_paient_card_type, (ViewGroup) null);
        this.mSpinner = (Spinner) dialogaddrss.findViewById(R.id.spinner_type);
        this.cardNumEt = (EditText) dialogaddrss.findViewById(R.id.et_card_num);
        this.cardNumEt.setText(this.et_diagnosis_card.getText().toString().trim());
        ArrayList<String> strings = new ArrayList<>();
        strings.add("选择诊疗卡类型");
        if (cardTypes2 != null) {
            int length = cardTypes2.size();
            for (int i = 0; i < length; i++) {
                strings.add(cardTypes2.get(i).getValue());
            }
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, (int) R.layout.spinner_item, strings);
        adapter.setDropDownViewResource(17367049);
        this.mSpinner.setAdapter((SpinnerAdapter) adapter);
        this.mSpinner.setSelection(this.selectPosition);
        builder.setView(dialogaddrss);
        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                RegisterSuccessActivity.this.selectPosition = RegisterSuccessActivity.this.mSpinner.getSelectedItemPosition();
                RegisterSuccessActivity.this.cardNum = RegisterSuccessActivity.this.cardNumEt.getText().toString().trim();
                if (RegisterSuccessActivity.this.selectPosition == 0) {
                    Toast.makeText(RegisterSuccessActivity.this, RegisterSuccessActivity.this.getResources().getString(R.string.select_medicare_type), 0).show();
                } else if (PoiTypeDef.All.equals(RegisterSuccessActivity.this.cardNum)) {
                    Toast.makeText(RegisterSuccessActivity.this, RegisterSuccessActivity.this.getResources().getString(R.string.f1ip_medicare_nur), 0).show();
                    RegisterSuccessActivity.this.cardNumEt.requestFocus();
                } else if (RegisterSuccessActivity.this.cardNum.length() < 8) {
                    Toast.makeText(RegisterSuccessActivity.this, RegisterSuccessActivity.this.getResources().getString(R.string.f2medicare_length), 0).show();
                    RegisterSuccessActivity.this.cardNumEt.requestFocus();
                } else {
                    RegisterSuccessActivity.this.et_diagnosis_card.setText(RegisterSuccessActivity.this.cardNum);
                    dialog.dismiss();
                }
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        ClientLogUtil.i(TAG, "ID:" + UserEntity.getInstance().getInfo().getAccId());
        ClientLogUtil.i(TAG, "Mobile:" + UserEntity.getInstance().getUsername());
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.ADD_USER_INFO /*104*/:
                ClientLogUtil.i(TAG, "姓名:" + this.patientname);
                ClientLogUtil.i(TAG, "身份证:" + this.idcard);
                ClientLogUtil.i(TAG, "诊疗卡号:" + this.defaultpatientcard);
                ClientLogUtil.i(TAG, "邮箱:" + this.email);
                ClientLogUtil.i(TAG, "社保卡:" + this.socialsecuritycard);
                ClientLogUtil.i(TAG, "居民健康卡:" + this.healthycard);
                ClientLogUtil.i(TAG, "市民卡:" + this.citizzencard);
                if (this.defaultpatientcard == null) {
                    this.defaultpatientcard = PoiTypeDef.All;
                }
                if (this.patientname == null) {
                    this.patientname = PoiTypeDef.All;
                }
                if (this.socialsecuritycard == null) {
                    this.socialsecuritycard = PoiTypeDef.All;
                }
                if (this.email == null) {
                    this.email = PoiTypeDef.All;
                }
                if (this.idcard == null) {
                    this.idcard = PoiTypeDef.All;
                }
                if (this.healthycard == null) {
                    this.healthycard = PoiTypeDef.All;
                }
                if (this.citizzencard == null) {
                    this.citizzencard = PoiTypeDef.All;
                }
                parameters.add("userid", UserEntity.getInstance().getInfo().getAccId());
                parameters.add("patientid", PoiTypeDef.All);
                parameters.add("defaultpatientcard", this.defaultpatientcard);
                parameters.add("patientname", this.patientname);
                parameters.add("mobile", UserEntity.getInstance().getUsername());
                parameters.add("socialsecuritycard", this.socialsecuritycard);
                parameters.add("email", this.email);
                parameters.add("idcard", this.idcard);
                parameters.add("healthycard", this.healthycard);
                parameters.add("citizzencard", this.citizzencard);
                if (!(this.cardTypes == null || this.selectPosition == 0)) {
                    parameters.add("patientcardtype", this.cardTypes.get(this.selectPosition - 1).getKey());
                }
                parameters.add("address", this.address);
                break;
            case HttpRequestParameters.READ_CARD_TYPE /*126*/:
                this.progressDialog = new ProgressDialog(this);
                this.progressDialog.setMessage(getResources().getString(R.string.getmedicare));
                this.progressDialog.setCancelable(true);
                this.progressDialog.setCanceledOnTouchOutside(false);
                this.progressDialog.show();
                parameters.add("hospitalid", Constants.HOSPITAL_ID);
                break;
        }
        return parameters;
    }
}
