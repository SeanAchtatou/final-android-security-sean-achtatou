package com.womenchild.hospital;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.parameter.UriParameter;

public class RegisterNoActivity extends BaseRequestActivity implements View.OnClickListener {
    private Button backBtn;
    private int currentIndex = 0;
    private Button ibtnHome;
    private Button ibtn_register_no_clinic;
    private Button ibtn_register_no_help;
    private Button ibtn_register_no_personal_center;
    /* access modifiers changed from: private */
    public Intent intent;
    private Button iv_find_doctor_by_collection;
    private Button iv_find_doctor_by_remain;
    private Button iv_find_doctor_by_rooms;
    private Button iv_find_doctor_by_sick;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.regiter_no);
        this.intent = getIntent();
        if (this.intent != null) {
            this.currentIndex = this.intent.getIntExtra("index", 0);
        }
        initViewId();
        initClickListener();
        if (getIntent().getBooleanExtra("backFlag", false)) {
            this.backBtn.setVisibility(0);
            this.ibtnHome.setVisibility(8);
        }
    }

    public void refreshActivity(Object... params) {
    }

    public void initViewId() {
        this.ibtnHome = (Button) findViewById(R.id.ibtn_home);
        this.backBtn = (Button) findViewById(R.id.back_btn);
        this.iv_find_doctor_by_rooms = (Button) findViewById(R.id.iv_find_doctor_by_rooms);
        this.iv_find_doctor_by_remain = (Button) findViewById(R.id.iv_find_doctor_by_remain);
        this.iv_find_doctor_by_collection = (Button) findViewById(R.id.iv_find_doctor_by_collection);
        this.ibtn_register_no_help = (Button) findViewById(R.id.ibtn_register_no_help);
        this.ibtn_register_no_personal_center = (Button) findViewById(R.id.ibtn_register_no_personal_center);
        if (this.currentIndex == 1) {
            this.ibtnHome.setText(PoiTypeDef.All);
            this.ibtnHome.setBackgroundResource(R.drawable.back_selector);
        }
    }

    public void initClickListener() {
        this.ibtnHome.setOnClickListener(this);
        this.backBtn.setOnClickListener(this);
        this.iv_find_doctor_by_rooms.setOnClickListener(this);
        this.iv_find_doctor_by_remain.setOnClickListener(this);
        this.iv_find_doctor_by_collection.setOnClickListener(this);
        this.ibtn_register_no_help.setOnClickListener(this);
        this.ibtn_register_no_personal_center.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_home:
                finish();
                return;
            case R.id.back_btn:
                finish();
                return;
            case R.id.iv_find_doctor_by_rooms:
                this.intent = new Intent(this, SelectHospitalActivity.class);
                startActivity(this.intent);
                return;
            case R.id.iv_find_doctor_by_remain:
                if (HomeActivity.loginFlag) {
                    this.intent = new Intent(this, SearchDoctorByOrderedActivity.class);
                    startActivity(this.intent);
                    return;
                }
                Toast.makeText(this, getString(R.string.login_function), 0).show();
                return;
            case R.id.iv_find_doctor_by_collection:
                if (HomeActivity.loginFlag) {
                    this.intent = new Intent(this, SearchDoctorByCollectedActivity.class);
                    startActivity(this.intent);
                    return;
                }
                Toast.makeText(this, getString(R.string.login_function), 0).show();
                return;
            case R.id.ibtn_register_no_help:
                this.intent = new Intent(this, OrderHelpActivity.class);
                startActivity(this.intent);
                return;
            case R.id.ibtn_register_no_personal_center:
                if (HomeActivity.loginFlag) {
                    this.intent = new Intent(this, PersonalCenterActivity.class);
                    startActivity(this.intent);
                    return;
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("登录提示");
                builder.setMessage("使用此功能前请您先登录！若无帐号，请先注册");
                builder.setPositiveButton("免费注册", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        RegisterNoActivity.this.intent = new Intent(RegisterNoActivity.this, RegisterActivity.class);
                        RegisterNoActivity.this.startActivity(RegisterNoActivity.this.intent);
                    }
                });
                builder.setNegativeButton("登录", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        RegisterNoActivity.this.intent = new Intent(RegisterNoActivity.this, LoginActivity.class);
                        RegisterNoActivity.this.startActivity(RegisterNoActivity.this.intent);
                    }
                });
                builder.create().show();
                return;
            default:
                return;
        }
    }

    public void loadData(int requestType, Object data) {
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        return null;
    }
}
