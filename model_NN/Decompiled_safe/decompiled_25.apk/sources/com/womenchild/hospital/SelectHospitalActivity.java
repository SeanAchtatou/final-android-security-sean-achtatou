package com.womenchild.hospital;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.configure.Constants;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import org.json.JSONObject;

public class SelectHospitalActivity extends BaseRequestActivity implements View.OnClickListener {
    private Button ibtn_return;
    private Intent intent;
    private Context mContext = this;
    private int mode = 0;
    private RelativeLayout rl_hospital1;
    private RelativeLayout rl_hospital2;
    private RelativeLayout rl_hospital3;
    private TextView tv_hospital1;
    private TextView tv_hospital1_address;
    private TextView tv_hospital2;
    private TextView tv_hospital2_address;
    private TextView tv_hospital3;
    private TextView tv_hospital3_address;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.select_hospital);
        initViewId();
        initClickListener();
        initData();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        initData();
    }

    public void refreshActivity(Object... params) {
        int requestType = ((Integer) params[0]).intValue();
        boolean status = ((Boolean) params[1]).booleanValue();
        JSONObject result = (JSONObject) params[2];
        Log.i("result", result.toString());
        if (status && result.optJSONObject("res").optInt("st") == 0) {
            switch (requestType) {
                case HttpRequestParameters.HOSPITAL_DETAIL /*202*/:
                    JSONObject inf = result.optJSONObject("inf");
                    String[] hids = inf.optString("hids").toString().split("--");
                    String[] addresses = inf.optString("addresses").toString().split("--");
                    String[] hospitalNames = inf.optString("hospitalNames").toString().split("--");
                    for (int i = 0; i < hids.length; i++) {
                        Constants.HOSPITALMAPS.put(hids[i], String.valueOf(addresses[i]) + "--" + hospitalNames[i]);
                    }
                    String info1 = Constants.HOSPITALMAPS.get(Constants.HOSPITAL_ID);
                    String info2 = Constants.HOSPITALMAPS.get("2000001");
                    String info3 = Constants.HOSPITALMAPS.get("2000003");
                    this.tv_hospital1.setText(info1.split("--")[1]);
                    this.tv_hospital1_address.setText(info1.split("--")[0]);
                    this.tv_hospital2.setText(info2.split("--")[1]);
                    this.tv_hospital2_address.setText(info2.split("--")[0]);
                    this.tv_hospital3.setText(info3.split("--")[1]);
                    this.tv_hospital3_address.setText(info3.split("--")[0]);
                    return;
                default:
                    return;
            }
        }
    }

    public void initViewId() {
        this.rl_hospital1 = (RelativeLayout) findViewById(R.id.rl_hospital1);
        this.rl_hospital2 = (RelativeLayout) findViewById(R.id.rl_hospital2);
        this.rl_hospital3 = (RelativeLayout) findViewById(R.id.rl_hospital3);
        this.tv_hospital1 = (TextView) findViewById(R.id.tv_hospital1);
        this.tv_hospital2 = (TextView) findViewById(R.id.tv_hospital2);
        this.tv_hospital3 = (TextView) findViewById(R.id.tv_hospital3);
        this.tv_hospital1_address = (TextView) findViewById(R.id.tv_hospital1_address);
        this.tv_hospital2_address = (TextView) findViewById(R.id.tv_hospital2_address);
        this.tv_hospital3_address = (TextView) findViewById(R.id.tv_hospital3_address);
        this.ibtn_return = (Button) findViewById(R.id.ibtn_return);
    }

    public void initClickListener() {
        this.rl_hospital1.setOnClickListener(this);
        this.rl_hospital2.setOnClickListener(this);
        this.rl_hospital3.setOnClickListener(this);
        this.ibtn_return.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_return:
                finish();
                return;
            case R.id.rl_hospital1:
                Constants.HOSPITALID = 2000002L;
                if (this.mode == 0) {
                    this.intent = new Intent(this, FindDoctorByRoomActivity.class);
                } else {
                    this.intent = new Intent(this, FindDoctorByRoom1Activity.class);
                }
                startActivity(this.intent);
                return;
            case R.id.rl_hospital2:
                Constants.HOSPITALID = 2000001L;
                if (this.mode == 0) {
                    this.intent = new Intent(this, FindDoctorByRoomActivity.class);
                } else {
                    this.intent = new Intent(this, FindDoctorByRoom1Activity.class);
                }
                startActivity(this.intent);
                return;
            case R.id.rl_hospital3:
                Constants.HOSPITALID = 2000003L;
                if (this.mode == 0) {
                    this.intent = new Intent(this, FindDoctorByRoomActivity.class);
                } else {
                    this.intent = new Intent(this, FindDoctorByRoom1Activity.class);
                }
                startActivity(this.intent);
                return;
            default:
                return;
        }
    }

    public void loadData(int requestType, Object data) {
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.mode = getIntent().getIntExtra("mode", 0);
        if (Constants.HOSPITALMAPS == null || Constants.HOSPITALMAPS.size() == 0) {
            sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.HOSPITAL_DETAIL), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.HOSPITAL_DETAIL)));
            return;
        }
        String info1 = Constants.HOSPITALMAPS.get(Constants.HOSPITAL_ID);
        String info2 = Constants.HOSPITALMAPS.get("2000001");
        String info3 = Constants.HOSPITALMAPS.get("2000003");
        this.tv_hospital1.setText(info1.split("--")[1]);
        this.tv_hospital1_address.setText(info1.split("--")[0]);
        this.tv_hospital2.setText(info2.split("--")[1]);
        this.tv_hospital2_address.setText(info2.split("--")[0]);
        this.tv_hospital3.setText(info3.split("--")[1]);
        this.tv_hospital3_address.setText(info3.split("--")[0]);
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.HOSPITAL_DETAIL /*202*/:
                parameters.add("hid", "2000002,2000001,2000003");
                break;
        }
        return parameters;
    }
}
