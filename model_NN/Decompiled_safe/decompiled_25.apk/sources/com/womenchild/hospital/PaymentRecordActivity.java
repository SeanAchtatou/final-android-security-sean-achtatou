package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.mapapi.location.LocationManagerProxy;
import com.womenchild.hospital.adapter.CancelPaymentOrderAdapter;
import com.womenchild.hospital.adapter.ErrorPayAdapter;
import com.womenchild.hospital.adapter.PaymentAdapter;
import com.womenchild.hospital.adapter.PaymentOrderAdapter;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.configure.Constants;
import com.womenchild.hospital.entity.AutoPaymentEntity;
import com.womenchild.hospital.entity.ErrorPayModel;
import com.womenchild.hospital.entity.PaymentEntity;
import com.womenchild.hospital.entity.PaymentOrderEntity;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.AppParameters;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PaymentRecordActivity extends BaseRequestActivity implements View.OnClickListener {
    private static final String TAG = "PaymentRecordActivity";
    private final int ACTIVITYREQUESTCODE = 107;
    /* access modifiers changed from: private */
    public ArrayList<ErrorPayModel> arrayErrorPayModels;
    /* access modifiers changed from: private */
    public ArrayList<AutoPaymentEntity> autoPaymentEntities;
    /* access modifiers changed from: private */
    public ArrayList<PaymentEntity> cancelRecordList;
    /* access modifiers changed from: private */
    public int currentIndex = 0;
    private Button iv_back;
    private ImageView iv_check;
    private ImageView iv_take_medicine;
    private ImageView iv_vis;
    private ListView lvPaymentOrderList;
    private ListView lvWaitPaymentOrderList;
    private ErrorPayAdapter mErrorPayAdapter;
    private TextView noCancelRecordTv;
    private TextView noPayRecordTv;
    private TextView noWaitRecordTv;
    private PaymentOrderAdapter orderAdapter;
    private List<PaymentOrderEntity> orderList;
    /* access modifiers changed from: private */
    public ArrayList<PaymentEntity> paiedRecordList;
    private ArrayList<PaymentEntity> paymentInfo;
    private ProgressDialog pdDialog;
    private RelativeLayout priceLayout;
    private double totalPrice;
    private TextView tv_check;
    private TextView tv_submit;
    private TextView tv_take_medicine;
    private TextView tv_total;
    private TextView tv_vis;
    private String userID;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.payment_record);
        initViewId();
        initClickListener();
        initData();
        switchImage(this.currentIndex);
        if (UserEntity.getInstance().getInfo() != null) {
            this.pdDialog = new ProgressDialog(this);
            this.pdDialog.setMessage(getResources().getString(R.string.loading_confirm_condition));
            this.pdDialog.show();
            this.userID = UserEntity.getInstance().getInfo().getUserid();
            sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.ERRORPAY_RECORD), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.ERRORPAY_RECORD)));
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void refreshActivity(Object... params) {
        this.pdDialog.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            loadData(requestType, (JSONObject) params[2]);
        } else {
            Toast.makeText(this, (int) R.string.network_connect_failed_prompt, 0).show();
        }
    }

    public void initViewId() {
        this.tv_total = (TextView) findViewById(R.id.tv_total);
        this.tv_submit = (TextView) findViewById(R.id.tv_submit);
        this.iv_back = (Button) findViewById(R.id.iv_back);
        this.lvPaymentOrderList = (ListView) findViewById(R.id.lv_payment_order_list);
        this.lvWaitPaymentOrderList = (ListView) findViewById(R.id.lv_wait_payment_order_list);
        this.tv_vis = (TextView) findViewById(R.id.tv_vis);
        this.tv_check = (TextView) findViewById(R.id.tv_check);
        this.tv_take_medicine = (TextView) findViewById(R.id.tv_take_medicine);
        this.iv_vis = (ImageView) findViewById(R.id.iv_vis);
        this.iv_check = (ImageView) findViewById(R.id.iv_check);
        this.iv_take_medicine = (ImageView) findViewById(R.id.iv_take_medicine);
        this.priceLayout = (RelativeLayout) findViewById(R.id.layout_main_middle);
        this.noWaitRecordTv = (TextView) findViewById(R.id.tv_no_wait_record);
        this.noPayRecordTv = (TextView) findViewById(R.id.tv_no_pay_record);
        this.noCancelRecordTv = (TextView) findViewById(R.id.tv_no_cancel_record);
        setOnItemListener();
    }

    public void initClickListener() {
        this.tv_submit.setOnClickListener(this);
        this.iv_back.setOnClickListener(this);
        this.tv_vis.setOnClickListener(new MyOnClickListener(0));
        this.tv_check.setOnClickListener(new MyOnClickListener(1));
        this.tv_take_medicine.setOnClickListener(new MyOnClickListener(3));
    }

    public void onClick(View v) {
        if (v == this.tv_submit) {
            Toast.makeText(this, "此功能暂未开放", 0).show();
        } else if (this.iv_back == v) {
            finish();
        }
    }

    public void loadData(int requestType, Object data) {
        JSONArray jsonArray;
        JSONObject jsonObject = (JSONObject) data;
        JSONObject res = jsonObject.optJSONObject("res");
        if (res != null) {
            try {
                if (res.getInt("st") == 0) {
                    if (612 == requestType) {
                        if (this.pdDialog != null) {
                            this.pdDialog.setMessage(getResources().getString(R.string.loading_confirm_refund));
                            this.pdDialog.show();
                        } else {
                            this.pdDialog = new ProgressDialog(this);
                            this.pdDialog.setMessage(getResources().getString(R.string.loading_confirm_refund));
                            this.pdDialog.show();
                        }
                        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.REGISTER_LIST), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.REGISTER_LIST)));
                        jsonArray = jsonObject.getJSONArray("inf");
                    } else {
                        jsonArray = jsonObject.optJSONObject("inf").getJSONArray("inf");
                    }
                    if (612 != requestType) {
                        this.paymentInfo = new ArrayList<>();
                        this.paymentInfo = PaymentEntity.setJSONtoPaymentEntityList(this, jsonArray);
                        bindView(this.paymentInfo);
                        if (this.paiedRecordList == null || this.paiedRecordList.size() <= 0) {
                            if (this.currentIndex != 0) {
                                this.noPayRecordTv.setVisibility(0);
                            }
                            this.lvPaymentOrderList.setVisibility(8);
                            return;
                        }
                        this.lvPaymentOrderList.setAdapter((ListAdapter) new CancelPaymentOrderAdapter(this, this.paiedRecordList));
                        if (this.currentIndex != 0) {
                            this.lvPaymentOrderList.setVisibility(0);
                        } else {
                            this.lvPaymentOrderList.setVisibility(8);
                        }
                        this.noPayRecordTv.setVisibility(8);
                        this.noCancelRecordTv.setVisibility(8);
                        return;
                    } else if (jsonArray == null || jsonArray.length() <= 0) {
                        this.noWaitRecordTv.setVisibility(0);
                        this.lvWaitPaymentOrderList.setVisibility(8);
                        this.priceLayout.setVisibility(8);
                        this.totalPrice = 0.0d;
                        return;
                    } else {
                        loadWaitPayment(jsonArray);
                        this.mErrorPayAdapter = new ErrorPayAdapter(this, this.arrayErrorPayModels);
                        this.lvWaitPaymentOrderList.setAdapter((ListAdapter) this.mErrorPayAdapter);
                        return;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
                return;
            }
        }
        if (this.pdDialog != null) {
            this.pdDialog.setMessage(getResources().getString(R.string.loading_confirm_refund));
            this.pdDialog.show();
        } else {
            this.pdDialog = new ProgressDialog(this);
            this.pdDialog.setMessage(getResources().getString(R.string.loading_confirm_refund));
            this.pdDialog.show();
        }
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.REGISTER_LIST), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.REGISTER_LIST)));
    }

    public class MyOnClickListener implements View.OnClickListener {
        private int index = 0;

        public MyOnClickListener(int i) {
            this.index = i;
        }

        public void onClick(View v) {
            PaymentRecordActivity.this.switchImage(this.index);
        }
    }

    /* access modifiers changed from: private */
    public void switchImage(int index) {
        switch (index) {
            case 0:
                this.currentIndex = 0;
                this.tv_vis.setTextColor(getResources().getColor(R.color.white));
                this.tv_check.setTextColor(getResources().getColor(R.color.black));
                this.tv_take_medicine.setTextColor(getResources().getColor(R.color.black));
                this.iv_vis.setBackgroundResource(R.drawable.ygkz_number_blank_blue_1);
                this.iv_check.setBackgroundResource(R.drawable.ygkz_number_blank_withline_2);
                this.iv_take_medicine.setBackgroundResource(R.drawable.ygkz_number_blank_withline_3);
                this.lvWaitPaymentOrderList.setVisibility(0);
                this.lvPaymentOrderList.setVisibility(8);
                this.noWaitRecordTv.setVisibility(8);
                this.noCancelRecordTv.setVisibility(8);
                this.noPayRecordTv.setVisibility(8);
                if (this.autoPaymentEntities == null || this.autoPaymentEntities.size() <= 0) {
                    this.noWaitRecordTv.setVisibility(0);
                    this.lvWaitPaymentOrderList.setVisibility(8);
                    return;
                }
                this.lvWaitPaymentOrderList.setAdapter((ListAdapter) new PaymentAdapter(this, this.autoPaymentEntities));
                this.noWaitRecordTv.setVisibility(8);
                return;
            case 1:
                this.currentIndex = 1;
                this.tv_vis.setTextColor(getResources().getColor(R.color.black));
                this.tv_check.setTextColor(getResources().getColor(R.color.white));
                this.tv_take_medicine.setTextColor(getResources().getColor(R.color.black));
                this.iv_vis.setBackgroundResource(R.drawable.ygkz_number_blank_withline_1);
                this.iv_check.setBackgroundResource(R.drawable.ygkz_number_blank_blue_2);
                this.iv_take_medicine.setBackgroundResource(R.drawable.ygkz_number_blank_withline_3);
                this.tv_submit.setVisibility(8);
                this.priceLayout.setVisibility(8);
                this.lvPaymentOrderList.setVisibility(0);
                this.lvWaitPaymentOrderList.setVisibility(8);
                this.noWaitRecordTv.setVisibility(8);
                this.noCancelRecordTv.setVisibility(8);
                this.noPayRecordTv.setVisibility(8);
                if (this.paiedRecordList == null || this.paiedRecordList.size() <= 0) {
                    this.noPayRecordTv.setVisibility(0);
                    this.lvPaymentOrderList.setVisibility(8);
                    return;
                }
                this.lvPaymentOrderList.setAdapter((ListAdapter) new CancelPaymentOrderAdapter(this, this.paiedRecordList));
                this.noPayRecordTv.setVisibility(8);
                this.noCancelRecordTv.setVisibility(8);
                return;
            case 2:
            default:
                return;
            case 3:
                this.currentIndex = 3;
                this.tv_vis.setTextColor(getResources().getColor(R.color.black));
                this.tv_check.setTextColor(getResources().getColor(R.color.black));
                this.tv_take_medicine.setTextColor(getResources().getColor(R.color.white));
                this.iv_vis.setBackgroundResource(R.drawable.ygkz_number_blank_withline_1);
                this.iv_check.setBackgroundResource(R.drawable.ygkz_number_blank_withline_2);
                this.iv_take_medicine.setBackgroundResource(R.drawable.ygkz_number_blank_blue_3);
                this.tv_submit.setVisibility(8);
                this.priceLayout.setVisibility(8);
                this.lvPaymentOrderList.setVisibility(0);
                this.lvWaitPaymentOrderList.setVisibility(8);
                this.noWaitRecordTv.setVisibility(8);
                this.noCancelRecordTv.setVisibility(8);
                this.noPayRecordTv.setVisibility(8);
                if (this.cancelRecordList == null || this.cancelRecordList.size() <= 0) {
                    this.noCancelRecordTv.setVisibility(0);
                    this.lvPaymentOrderList.setVisibility(8);
                    return;
                }
                this.lvPaymentOrderList.setAdapter((ListAdapter) new CancelPaymentOrderAdapter(this, this.cancelRecordList));
                this.noWaitRecordTv.setVisibility(8);
                this.noPayRecordTv.setVisibility(8);
                this.noCancelRecordTv.setVisibility(8);
                return;
        }
    }

    private void bindView(ArrayList<PaymentEntity> list) {
        if (list != null) {
            this.paiedRecordList = new ArrayList<>();
            this.cancelRecordList = new ArrayList<>();
            for (int i = 0; i < list.size(); i++) {
                PaymentEntity entity = list.get(i);
                int payState = entity.getState();
                if (payState == 0) {
                    this.paiedRecordList.add(entity);
                } else if (payState == 2) {
                    this.cancelRecordList.add(entity);
                }
            }
        }
    }

    private void setOnItemListener() {
        this.lvPaymentOrderList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                if (PaymentRecordActivity.this.currentIndex == 0) {
                    WaitPaymentOrderActivity.autoPayment = (AutoPaymentEntity) PaymentRecordActivity.this.autoPaymentEntities.get(arg2);
                    if (WaitPaymentOrderActivity.autoPayment != null) {
                        PaymentRecordActivity.this.startActivity(new Intent(PaymentRecordActivity.this, PaymentOrderActivity.class));
                        return;
                    }
                    return;
                }
                Intent intent = new Intent(PaymentRecordActivity.this, PaymentDetailActivity.class);
                if (PaymentRecordActivity.this.currentIndex == 1) {
                    Bundle bundle = new Bundle();
                    bundle.putDouble("price", ((PaymentEntity) PaymentRecordActivity.this.paiedRecordList.get(arg2)).getPrice());
                    bundle.putString("orderNum", ((PaymentEntity) PaymentRecordActivity.this.paiedRecordList.get(arg2)).getOrdernum());
                    bundle.putString("realOrderNum", ((PaymentEntity) PaymentRecordActivity.this.paiedRecordList.get(arg2)).getRealOrderNum());
                    bundle.putInt(LocationManagerProxy.KEY_STATUS_CHANGED, PaymentRecordActivity.this.currentIndex);
                    bundle.putString("payway", ((PaymentEntity) PaymentRecordActivity.this.paiedRecordList.get(arg2)).getPayway());
                    intent.putExtras(bundle);
                    PaymentRecordActivity.this.startActivity(intent);
                } else if (PaymentRecordActivity.this.currentIndex == 3) {
                    Bundle bundle2 = new Bundle();
                    bundle2.putDouble("price", ((PaymentEntity) PaymentRecordActivity.this.cancelRecordList.get(arg2)).getPrice());
                    bundle2.putString("orderNum", ((PaymentEntity) PaymentRecordActivity.this.cancelRecordList.get(arg2)).getOrdernum());
                    bundle2.putString("realOrderNum", ((PaymentEntity) PaymentRecordActivity.this.cancelRecordList.get(arg2)).getRealOrderNum());
                    bundle2.putInt(LocationManagerProxy.KEY_STATUS_CHANGED, PaymentRecordActivity.this.currentIndex);
                    bundle2.putString("payway", ((PaymentEntity) PaymentRecordActivity.this.cancelRecordList.get(arg2)).getPayway());
                    intent.putExtras(bundle2);
                    PaymentRecordActivity.this.startActivity(intent);
                }
            }
        });
        this.lvWaitPaymentOrderList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                if (PaymentRecordActivity.this.currentIndex == 0) {
                    Intent intent = new Intent(PaymentRecordActivity.this, DetailErrorPayActivity.class);
                    intent.putExtra(DetailErrorPayActivity.DETAILERRPAYMODEL, (ErrorPayModel) PaymentRecordActivity.this.arrayErrorPayModels.get(arg2));
                    PaymentRecordActivity.this.startActivityForResult(intent, 107);
                    return;
                }
                Intent intent2 = new Intent(PaymentRecordActivity.this, PaymentDetailActivity.class);
                if (PaymentRecordActivity.this.currentIndex == 1) {
                    Bundle bundle = new Bundle();
                    bundle.putDouble("price", ((PaymentEntity) PaymentRecordActivity.this.paiedRecordList.get(arg2)).getPrice());
                    bundle.putString("orderNum", ((PaymentEntity) PaymentRecordActivity.this.paiedRecordList.get(arg2)).getOrdernum());
                    bundle.putString("realOrderNum", ((PaymentEntity) PaymentRecordActivity.this.paiedRecordList.get(arg2)).getRealOrderNum());
                    bundle.putInt(LocationManagerProxy.KEY_STATUS_CHANGED, PaymentRecordActivity.this.currentIndex);
                    bundle.putString("payway", ((PaymentEntity) PaymentRecordActivity.this.paiedRecordList.get(arg2)).getPayway());
                    intent2.putExtras(bundle);
                    PaymentRecordActivity.this.startActivity(intent2);
                } else if (PaymentRecordActivity.this.currentIndex == 3) {
                    Bundle bundle2 = new Bundle();
                    bundle2.putDouble("price", ((PaymentEntity) PaymentRecordActivity.this.cancelRecordList.get(arg2)).getPrice());
                    bundle2.putString("orderNum", ((PaymentEntity) PaymentRecordActivity.this.cancelRecordList.get(arg2)).getOrdernum());
                    bundle2.putString("realOrderNum", ((PaymentEntity) PaymentRecordActivity.this.cancelRecordList.get(arg2)).getRealOrderNum());
                    bundle2.putInt(LocationManagerProxy.KEY_STATUS_CHANGED, PaymentRecordActivity.this.currentIndex);
                    bundle2.putString("payway", ((PaymentEntity) PaymentRecordActivity.this.cancelRecordList.get(arg2)).getPayway());
                    intent2.putExtras(bundle2);
                    PaymentRecordActivity.this.startActivity(intent2);
                }
            }
        });
    }

    private void loadWaitPayment(JSONArray jsonArray) {
        this.arrayErrorPayModels = analysisErrPayJson(jsonArray);
        if (this.arrayErrorPayModels.size() > 0) {
            this.tv_total.setText(String.valueOf(this.totalPrice));
            this.priceLayout.setVisibility(8);
            this.noWaitRecordTv.setVisibility(8);
            this.lvWaitPaymentOrderList.setVisibility(0);
            return;
        }
        this.noWaitRecordTv.setVisibility(0);
        this.priceLayout.setVisibility(8);
        this.lvWaitPaymentOrderList.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.tv_total.setText("0");
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.REGISTER_LIST /*114*/:
                parameters.add("userid", this.userID);
                parameters.add("memberid", String.valueOf(AppParameters.getAPPID()) + "_" + this.userID);
                break;
            case HttpRequestParameters.AUTO_PAY_ORDER /*606*/:
                parameters.add("hospitalid", Constants.HOSPITAL_ID);
                parameters.add("userid", this.userID);
                break;
            case HttpRequestParameters.ERRORPAY_RECORD /*612*/:
                parameters.add("userid", this.userID);
                break;
        }
        return parameters;
    }

    private ArrayList<ErrorPayModel> analysisErrPayJson(JSONArray jsonArray) {
        ArrayList<ErrorPayModel> array = new ArrayList<>();
        int len = jsonArray.length();
        for (int i = 0; i < len; i++) {
            JSONObject mJsonObject = jsonArray.optJSONObject(i);
            ErrorPayModel model = new ErrorPayModel();
            model.initModel(mJsonObject);
            if ("sunpay".equals(model.getPayway())) {
                array.add(model);
            }
        }
        return array;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        ErrorPayModel model;
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 107 && resultCode == 107 && (model = (ErrorPayModel) data.getSerializableExtra(DetailErrorPayActivity.DETAILERRPAYMODEL)) != null) {
            Iterator<ErrorPayModel> it = this.arrayErrorPayModels.iterator();
            while (true) {
                if (it.hasNext()) {
                    ErrorPayModel mModel = it.next();
                    if (mModel.getCupsqid().equals(model.getCupsqid()) && mModel.getYlordernum().equals(model.getYlordernum())) {
                        this.arrayErrorPayModels.remove(mModel);
                        break;
                    }
                } else {
                    break;
                }
            }
            if (this.mErrorPayAdapter != null) {
                this.mErrorPayAdapter.setArrayErrorPayModel(this.arrayErrorPayModels);
                this.mErrorPayAdapter.notifyDataSetChanged();
            }
        }
    }
}
