package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.womenchild.hospital.adapter.SelectPatientAdapter;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import org.json.JSONArray;
import org.json.JSONObject;

public class SelectPatientActivity extends BaseRequestActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    private static SelectPatientAdapter adapter;
    private Button btnAdd;
    private Button btnDoctorInfo;
    private Button btnNext;
    private int doctorId;
    private Button ibtnReturn;
    private Intent intent;
    private ListView lvPatient;
    private ProgressDialog pDialog;
    private JSONArray patients;
    private String plan;
    private TextView tvInfo;
    private TextView tvSelectPatientTitle;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.select_patient);
        initViewId();
        initClickListener();
        initData();
    }

    public void refreshActivity(Object... params) {
        this.pDialog.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject result = (JSONObject) params[2];
            int resultCode = result.optInt("st");
            String msg = result.optString("msg");
            if (resultCode == 0) {
                loadData(requestType, result.optJSONArray("inf"));
            } else {
                Toast.makeText(this, msg, 0).show();
            }
        } else {
            Toast.makeText(this, getResources().getString(R.string.network_connect_failed_prompt), 0).show();
        }
    }

    public void initViewId() {
        this.ibtnReturn = (Button) findViewById(R.id.ibtn_return);
        this.btnNext = (Button) findViewById(R.id.btn_next);
        this.lvPatient = (ListView) findViewById(R.id.lv_select_patient);
        this.btnAdd = (Button) findViewById(R.id.btn_add);
        this.tvInfo = (TextView) findViewById(R.id.tv_info);
        this.tvSelectPatientTitle = (TextView) findViewById(R.id.tv_select_patient_title);
        this.pDialog = new ProgressDialog(this);
        this.pDialog.setMessage(getResources().getString(R.string.loading_ok));
        this.btnDoctorInfo = (Button) findViewById(R.id.btn_doctor_info);
    }

    public void initClickListener() {
        this.ibtnReturn.setOnClickListener(this);
        this.btnNext.setOnClickListener(this);
        this.btnAdd.setOnClickListener(this);
        this.btnDoctorInfo.setOnClickListener(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_return:
                finish();
                return;
            case R.id.btn_next:
                if (!"下一步".equals(this.btnNext.getText())) {
                    this.intent = new Intent(this, PatientAddActivity.class);
                    startActivity(this.intent);
                    return;
                } else if (-1 == SelectPatientAdapter.getSelectedId()) {
                    Toast.makeText(this, getResources().getString(R.string.select_doctor), 0).show();
                    return;
                } else {
                    String patient = this.patients.optJSONObject(SelectPatientAdapter.getSelectedId()).toString();
                    this.intent = new Intent(this, FillInAppointmentInfoActivity.class);
                    this.intent.putExtra("plan", SelectClinicTimeActivity.json.toString());
                    this.intent.putExtra("patient", patient);
                    startActivity(this.intent);
                    return;
                }
            case R.id.btn_doctor_info:
                this.intent = new Intent(this, FavDoctorActivity.class);
                this.intent.putExtra("doctorID", String.valueOf(this.doctorId));
                this.intent.putExtra("favFlag", true);
                this.intent.putExtra("planFlag", false);
                startActivity(this.intent);
                return;
            case R.id.btn_add:
                this.intent = new Intent(this, PatientAddActivity.class);
                startActivityForResult(this.intent, 0);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        SelectPatientAdapter.setSelectedId(-1);
    }

    public void loadData(int requestType, Object data) {
        this.patients = (JSONArray) data;
        if (this.patients.length() == 0) {
            this.btnAdd.setVisibility(8);
            this.tvInfo.setVisibility(8);
            this.lvPatient.setVisibility(8);
            this.tvSelectPatientTitle.setText(getResources().getString(R.string.select_doctor_not));
            this.btnNext.setText(getResources().getString(R.string.add_doctor));
        } else if (this.patients.length() == 3) {
            this.btnAdd.setVisibility(8);
            this.tvInfo.setVisibility(8);
            this.tvSelectPatientTitle.setText(getResources().getString(R.string.select_doctor_current));
            this.lvPatient.setVisibility(0);
            this.btnNext.setText(getResources().getString(R.string.select_doctor_next));
        } else {
            this.tvSelectPatientTitle.setText(getResources().getString(R.string.select_doctor_current));
            this.lvPatient.setVisibility(0);
            this.btnAdd.setVisibility(0);
            this.tvInfo.setVisibility(0);
        }
        adapter = new SelectPatientAdapter(this, this.patients);
        this.lvPatient.setAdapter((ListAdapter) adapter);
        this.lvPatient.setOnItemClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == -1) {
            sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.PATIENT_LIST), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.PATIENT_LIST)));
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.plan = getIntent().getStringExtra("plan");
        this.doctorId = getIntent().getIntExtra("doctorId", 0);
        this.pDialog.show();
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.PATIENT_LIST), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.PATIENT_LIST)));
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        if (UserEntity.getInstance().getInfo().getAccId() != null) {
            parameters.add("userid", UserEntity.getInstance().getInfo().getAccId());
        }
        return parameters;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long lid) {
        String patient = this.patients.optJSONObject(position).toString();
        this.intent = new Intent(this, FillInAppointmentInfoActivity.class);
        this.intent.putExtra("plan", SelectClinicTimeActivity.json.toString());
        this.intent.putExtra("patient", patient);
        startActivity(this.intent);
    }
}
