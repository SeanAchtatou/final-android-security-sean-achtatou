package com.womenchild.hospital;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.umeng.analytics.MobclickAgent;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.entity.UserInfoEntity;
import com.womenchild.hospital.entity.VersionInfo;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.request.RequestTask;
import com.womenchild.hospital.service.RemindService;
import com.womenchild.hospital.util.AESEncryptor;
import com.womenchild.hospital.util.Base64Util;
import com.womenchild.hospital.util.ClientLogUtil;
import com.womenchild.hospital.util.UserCacheUtil;
import com.womenchild.hospital.util.VersionUtil;
import org.json.JSONObject;

public class HomeActivity extends BaseRequestActivity implements View.OnClickListener {
    public static String Hom = "CCLog";
    private static final String TAG = "HAct";
    public static boolean loginFlag = false;
    private Button ibtn_hospital_guide;
    private Button ibtn_hospitalize_help;
    private Button ibtn_more;
    private Button ibtn_parking;
    private Intent intent;
    private Button iv_check_result;
    private Button iv_diagnosis_consult;
    private Button iv_hospital_doctor_comment;
    private Button iv_hospital_dynamic_state;
    private Button iv_make_appointment_register;
    private Button iv_payment;
    private Button iv_queue_call;
    private Context mContext = this;
    private String phone;
    private ProgressDialog progressDialog;
    private String pwd;
    private TextView tv_login_register;
    private TextView tv_personal_centre;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.home);
        initViewId();
        initClickListener();
        initData();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        ClientLogUtil.i(TAG, "onResume()");
        if (UserEntity.getInstance().getPassword().equals(PoiTypeDef.All)) {
            ClientLogUtil.i(TAG, "onResume() 匿名状态");
            this.tv_personal_centre.setVisibility(8);
            this.tv_login_register.setVisibility(0);
            return;
        }
        ClientLogUtil.i(TAG, "onResume() 登录状态");
        this.tv_personal_centre.setVisibility(0);
        this.tv_login_register.setVisibility(8);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return true;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(((Object) getText(R.string.whether_exit)) + getString(R.string.title_activity_razzil_hp) + "？");
        builder.setPositiveButton(getText(R.string.yes_exit), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                HomeActivity.loginFlag = false;
                UserEntity.getInstance().setUsername(PoiTypeDef.All);
                UserEntity.getInstance().setPassword(PoiTypeDef.All);
                HomeActivity.this.stopService(new Intent(HomeActivity.this, RemindService.class));
                HomeActivity.this.finish();
            }
        });
        builder.setNegativeButton(getText(R.string.no_exit), (DialogInterface.OnClickListener) null);
        builder.create().show();
        return true;
    }

    public void refreshActivity(Object... params) {
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject result = (JSONObject) params[2];
            switch (requestType) {
                case HttpRequestParameters.USER_LOGIN /*100*/:
                    initUserData(result);
                    return;
                case HttpRequestParameters.UPDATE_VERSION /*999*/:
                    VersionInfo versionInfo = new VersionInfo(result);
                    VersionUtil.getInstance().updateVersion(this, versionInfo.getUrl(), "/WomenChild", versionInfo.getVersion());
                    return;
                default:
                    return;
            }
        } else if (loginFlag) {
            Toast.makeText(this, getResources().getString(R.string.network_connect_failed_prompt), 0).show();
        }
    }

    public void initViewId() {
        this.ibtn_hospital_guide = (Button) findViewById(R.id.ibtn_hospital_guide);
        this.ibtn_parking = (Button) findViewById(R.id.ibtn_parking);
        this.ibtn_more = (Button) findViewById(R.id.ibtn_more);
        this.tv_login_register = (TextView) findViewById(R.id.tv_login_register);
        this.iv_make_appointment_register = (Button) findViewById(R.id.iv_make_appointment_register);
        this.iv_hospital_dynamic_state = (Button) findViewById(R.id.iv_hospital_dynamic_state);
        this.tv_personal_centre = (TextView) findViewById(R.id.tv_personal_centre);
        this.iv_hospital_doctor_comment = (Button) findViewById(R.id.iv_hospital_doctor_comment);
    }

    public void initClickListener() {
        this.ibtn_hospital_guide.setOnClickListener(this);
        this.ibtn_parking.setOnClickListener(this);
        this.ibtn_more.setOnClickListener(this);
        this.tv_login_register.setOnClickListener(this);
        this.iv_make_appointment_register.setOnClickListener(this);
        this.iv_hospital_dynamic_state.setOnClickListener(this);
        this.tv_personal_centre.setOnClickListener(this);
        this.iv_hospital_doctor_comment.setOnClickListener(this);
    }

    public void onClick(View v) {
        if (v == this.ibtn_hospital_guide) {
            MobclickAgent.onEvent(this, "hospital_guide");
            this.intent = new Intent(this, HospitalGuideActivity.class);
            startActivity(this.intent);
        } else if (v == this.ibtn_parking) {
            MobclickAgent.onEvent(this, "parking");
            this.intent = new Intent();
            this.intent.setClass(this, ParkingActivity.class);
            startActivity(this.intent);
        } else if (v == this.ibtn_more) {
            MobclickAgent.onEvent(this, "more");
            this.intent = new Intent(this, MoreActivity.class);
            startActivity(this.intent);
        } else if (this.tv_login_register == v) {
            MobclickAgent.onEvent(this, "login");
            this.intent = new Intent(this, LoginActivity.class);
            startActivity(this.intent);
        } else if (v == this.iv_make_appointment_register) {
            MobclickAgent.onEvent(this, "register");
            this.intent = new Intent(this, SelectHospitalActivity.class);
            this.intent.putExtra("mode", 0);
            startActivity(this.intent);
        } else if (v == this.iv_hospital_dynamic_state) {
            MobclickAgent.onEvent(this, "hospital_dynamic");
            this.intent = new Intent(this, HospitalNoticeActivity.class);
            startActivity(this.intent);
        } else if (v == this.iv_hospital_doctor_comment) {
            this.intent = new Intent(this, SelectHospitalActivity.class);
            this.intent.putExtra("mode", 1);
            startActivity(this.intent);
        } else if (v == this.tv_personal_centre) {
            MobclickAgent.onEvent(this, "personal_center");
            this.intent = new Intent(this, PersonalCenterActivity.class);
            startActivity(this.intent);
        }
    }

    public void loadData(int requestType, Object data) {
    }

    private void initUserData(JSONObject result) {
        if ("0".equals(result.optJSONObject("res").optString("st"))) {
            ClientLogUtil.d(TAG, "initUserData() Result: " + result.toString());
            JSONObject object = result.optJSONObject("inf").optJSONObject("patient");
            UserEntity.getInstance().setUsername(this.phone);
            UserEntity.getInstance().setPassword(this.pwd);
            UserInfoEntity info = new UserInfoEntity();
            info.setAccId(result.optJSONObject("inf").optString("accid"));
            if (object != null && !object.toString().trim().equals(null) && !object.toString().trim().equals(PoiTypeDef.All)) {
                ClientLogUtil.v(TAG, "initUserData() Init user info: " + object.toString());
                info.setCitizenCard(object.optString("citizzencard"));
                info.setDefaultPatientCard(object.optString("defaultpatientcard"));
                info.setEmail(object.optString("email"));
                info.setHealthyCard(object.optString("healthycard"));
                info.setIdCard(object.optString("idcard"));
                info.setName(object.optString("patientname"));
                info.setSocialSecurity(object.optString("socialsecuritycard"));
                info.setMobile(object.optString("mobile"));
                info.setAddress(object.optString("address"));
                info.setUserid(object.optString("userid"));
                info.setPatientid(object.optString("patientid"));
            }
            UserEntity.getInstance().setInfo(info);
            loginFlag = true;
            Toast.makeText(this, result.optJSONObject("res").optString("msg"), 0).show();
            onResume();
            return;
        }
        Toast.makeText(this, result.optJSONObject("res").optString("msg"), 0).show();
    }

    /* access modifiers changed from: protected */
    public void initData() {
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.UPDATE_VERSION), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.UPDATE_VERSION)));
        if ("1".equals(UserCacheUtil.getAutoStat(this.mContext)) && "1".equals(UserCacheUtil.getPWDStat(this.mContext))) {
            try {
                this.pwd = AESEncryptor.decrypt("4561237", UserCacheUtil.getCachePassword(this.mContext));
            } catch (Exception e) {
                Toast.makeText(this, getResources().getString(R.string.get_pw_md5_error), 0).show();
                this.pwd = PoiTypeDef.All;
            }
            this.phone = UserCacheUtil.getCacheUserName(this.mContext);
            if (!loginFlag) {
                RequestTask.getInstance().sendHttpRequest(this, String.valueOf(100), initRequestParameter(100));
            }
        }
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.USER_LOGIN /*100*/:
                parameters.add("username", this.phone);
                parameters.add("password", Base64Util.MD5(this.pwd));
                break;
            case HttpRequestParameters.UPDATE_VERSION /*999*/:
                parameters.add("system", 0);
                break;
        }
        return parameters;
    }
}
