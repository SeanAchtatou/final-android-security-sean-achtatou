package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.womenchild.hospital.adapter.PaymentAdapter;
import com.womenchild.hospital.adapter.PaymentOrderAdapter;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.entity.AutoPaymentEntity;
import com.womenchild.hospital.entity.PaymentEntity;
import com.womenchild.hospital.entity.PaymentOrderEntity;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.AppParameters;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.Arith;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class WaitPaymentOrderActivity extends BaseRequestActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    private static final String TAG = "WPOActivity";
    public static AutoPaymentEntity autoPayment;
    private ArrayList<AutoPaymentEntity> autoPaymentEntities;
    private ArrayList<PaymentEntity> cancelRecordList;
    private Button iv_back;
    private ListView lvPaymentOrderList;
    private TextView noRecordTv;
    private ArrayList<PaymentEntity> nopaiedRecordList;
    private PaymentOrderAdapter orderAdapter;
    private List<PaymentOrderEntity> orderList;
    private ArrayList<PaymentEntity> paiedRecordList;
    private ArrayList<PaymentEntity> paymentInfo;
    private ProgressDialog pdDialog;
    private RelativeLayout priceLayout;
    private double totalPrice;
    private TextView tv_submit;
    private TextView tv_total;
    private String userID;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.wait_payment_order);
        initViewId();
        initClickListener();
        initData();
        if (UserEntity.getInstance().getInfo() != null) {
            this.pdDialog = new ProgressDialog(this);
            this.pdDialog.setMessage(getResources().getString(R.string.loading_data));
            this.pdDialog.show();
            this.userID = UserEntity.getInstance().getInfo().getUserid();
            sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.AUTO_PAY_ORDER), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.AUTO_PAY_ORDER)));
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void refreshActivity(Object... params) {
        this.pdDialog.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            loadData(requestType, (JSONObject) params[2]);
        } else {
            Toast.makeText(this, (int) R.string.network_connect_failed_prompt, 0).show();
        }
    }

    public void initViewId() {
        this.tv_total = (TextView) findViewById(R.id.tv_total);
        this.tv_submit = (TextView) findViewById(R.id.tv_submit);
        this.iv_back = (Button) findViewById(R.id.iv_back);
        this.lvPaymentOrderList = (ListView) findViewById(R.id.lv_payment_order_list);
        this.priceLayout = (RelativeLayout) findViewById(R.id.layout_main_middle);
        this.noRecordTv = (TextView) findViewById(R.id.tv_no_record);
    }

    public void initClickListener() {
        this.tv_submit.setOnClickListener(this);
        this.iv_back.setOnClickListener(this);
        this.lvPaymentOrderList.setOnItemClickListener(this);
    }

    public void onClick(View v) {
        if (v == this.tv_submit) {
            Toast.makeText(this, getResources().getString(R.string.not_started), 0).show();
        } else if (this.iv_back == v) {
            finish();
        }
    }

    public void loadData(int requestType, Object data) {
        JSONObject jsonObject = (JSONObject) data;
        JSONObject res = jsonObject.optJSONObject("res");
        if (res != null) {
            try {
                if (res.getInt("st") == 0) {
                    JSONArray jsonArray = jsonObject.optJSONObject("inf").getJSONArray("orderarray");
                    if (jsonArray == null || jsonArray.length() <= 0) {
                        if (res != null) {
                            if (res.getInt("st") == 99) {
                                Toast.makeText(this, res.optString("msg"), 0).show();
                                return;
                            }
                        }
                        this.noRecordTv.setVisibility(0);
                        this.priceLayout.setVisibility(8);
                        this.totalPrice = 0.0d;
                        return;
                    }
                    this.autoPaymentEntities = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject order = jsonArray.optJSONObject(i);
                        JSONArray orders = order.optJSONArray("orders");
                        for (int j = 0; j < orders.length(); j++) {
                            if (orders.optJSONObject(j).optString("payStatus").equals("0")) {
                                this.autoPaymentEntities.add(new AutoPaymentEntity(order.optString("patientname"), order.optString("card"), order.optString("cardtpye"), orders.optJSONObject(j).optString("scheduleTime"), orders.optJSONObject(j).optString("deptName"), orders.optJSONObject(j).optString("selfFee"), orders.optJSONObject(j).optString("clinicTranLine"), orders.optJSONObject(j).optString("hisPatientId"), orders.optJSONObject(j).optJSONArray("billOrders"), orders.optJSONObject(j).optString("payStatus")));
                            }
                        }
                    }
                    PaymentAdapter adapter = new PaymentAdapter(this, this.autoPaymentEntities);
                    this.lvPaymentOrderList.setAdapter((ListAdapter) adapter);
                    if (adapter.getCount() != 0) {
                        this.noRecordTv.setVisibility(8);
                    } else {
                        this.noRecordTv.setVisibility(0);
                    }
                    this.lvPaymentOrderList.setVisibility(0);
                    return;
                }
            } catch (JSONException e) {
                e.printStackTrace();
                return;
            }
        }
        this.noRecordTv.setVisibility(0);
        this.priceLayout.setVisibility(8);
        this.totalPrice = 0.0d;
    }

    private void bindView(ArrayList<PaymentEntity> list) {
        if (list != null) {
            this.nopaiedRecordList = new ArrayList<>();
            for (int i = 0; i < list.size(); i++) {
                PaymentEntity entity = list.get(i);
                if (entity.getState() == 1) {
                    this.nopaiedRecordList.add(entity);
                }
            }
            if (this.nopaiedRecordList == null || this.nopaiedRecordList.size() <= 0) {
                this.noRecordTv.setText(getResources().getString(R.string.user_pay_in));
                this.noRecordTv.setVisibility(0);
                this.lvPaymentOrderList.setVisibility(8);
                return;
            }
            for (int i2 = 0; i2 < this.nopaiedRecordList.size(); i2++) {
                this.totalPrice = Arith.add(this.totalPrice, this.nopaiedRecordList.get(i2).getPrice());
            }
            this.tv_total.setText(String.valueOf(this.totalPrice));
            this.priceLayout.setVisibility(0);
            this.lvPaymentOrderList.setAdapter((ListAdapter) new PaymentOrderAdapter(this, this.nopaiedRecordList));
            this.noRecordTv.setVisibility(8);
            this.lvPaymentOrderList.setVisibility(0);
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
        autoPayment = this.autoPaymentEntities.get(arg2);
        if (autoPayment != null) {
            startActivity(new Intent(this, PaymentOrderActivity.class));
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.tv_total.setText("0");
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.REGISTER_LIST /*114*/:
                parameters.add("userid", this.userID);
                parameters.add("memberid", String.valueOf(AppParameters.getAPPID()) + "_" + this.userID);
                break;
            case HttpRequestParameters.AUTO_PAY_ORDER /*606*/:
                parameters.add("hospitalid", 2000005);
                parameters.add("userid", this.userID);
                break;
        }
        return parameters;
    }
}
