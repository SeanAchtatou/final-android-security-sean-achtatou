package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.mapapi.location.LocationManagerProxy;
import com.womenchild.hospital.adapter.ConsultAdapter;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import org.json.JSONArray;
import org.json.JSONObject;

public class ConsultActivity extends BaseRequestActivity implements View.OnClickListener {
    private static final String TAG = "ConsultActivity";
    private Button iv_return_home;
    private ListView lv_consult_no_reply;
    private ListView lv_replycontent;
    /* access modifiers changed from: private */
    public JSONArray noReplyJsonArray;
    private ProgressDialog pDialog;
    /* access modifiers changed from: private */
    public JSONArray replyJsonArray;
    private TextView tv_now_consult;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.consult_list);
        initViewId();
        initClickListener();
        this.pDialog = new ProgressDialog(this);
        this.pDialog.setMessage(getResources().getString(R.string.loading_data));
        this.pDialog.show();
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.QUESTION_LIST), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.QUESTION_LIST)));
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void onClick(View v) {
        if (this.iv_return_home == v) {
            finish();
        } else if (this.tv_now_consult == v) {
            Intent intent = new Intent(this, OnlineConsultActivity.class);
            intent.putExtra("index", 1);
            startActivity(intent);
        }
    }

    public void refreshActivity(Object... params) {
        this.pDialog.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject result = (JSONObject) params[2];
            if (result.optJSONObject("res").optInt("st") == 0) {
                loadData(requestType, result.optJSONObject("inf"));
            } else {
                Toast.makeText(this, result.optJSONObject("res").optString("msg"), 0).show();
            }
        } else {
            Toast.makeText(this, getResources().getString(R.string.service_ex), 0).show();
        }
    }

    public void initViewId() {
        this.lv_replycontent = (ListView) findViewById(R.id.lv_replycontent);
        this.iv_return_home = (Button) findViewById(R.id.iv_return_home);
        this.tv_now_consult = (TextView) findViewById(R.id.tv_now_consult);
        this.lv_consult_no_reply = (ListView) findViewById(R.id.lv_no_replycontent);
    }

    public void initClickListener() {
        this.iv_return_home.setOnClickListener(this);
        this.tv_now_consult.setOnClickListener(this);
        this.lv_replycontent.setOnItemClickListener(new ReplyItemListener(this, null));
        this.lv_consult_no_reply.setOnItemClickListener(new NoReplyItemListener(this, null));
    }

    public void loadData(int requestType, Object data) {
        JSONObject json = (JSONObject) data;
        this.replyJsonArray = json.optJSONArray("reply");
        this.lv_replycontent.setAdapter((ListAdapter) new ConsultAdapter(this, this.replyJsonArray));
        this.noReplyJsonArray = json.optJSONArray("noreply");
        if (this.noReplyJsonArray.length() == 0) {
            OnlineConsultActivity.replyFlag = true;
            return;
        }
        this.lv_consult_no_reply.setAdapter((ListAdapter) new ConsultAdapter(this, this.noReplyJsonArray));
    }

    private class ReplyItemListener implements AdapterView.OnItemClickListener {
        private ReplyItemListener() {
        }

        /* synthetic */ ReplyItemListener(ConsultActivity consultActivity, ReplyItemListener replyItemListener) {
            this();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
        public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
            Intent intent = new Intent(ConsultActivity.this, ConsultReplyActivity.class);
            intent.putExtra(LocationManagerProxy.KEY_STATUS_CHANGED, true);
            intent.putExtra("data", ConsultActivity.this.replyJsonArray.optJSONObject(arg2).toString());
            ConsultActivity.this.startActivity(intent);
        }
    }

    private class NoReplyItemListener implements AdapterView.OnItemClickListener {
        private NoReplyItemListener() {
        }

        /* synthetic */ NoReplyItemListener(ConsultActivity consultActivity, NoReplyItemListener noReplyItemListener) {
            this();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
        public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
            Intent intent = new Intent(ConsultActivity.this, ConsultReplyActivity.class);
            intent.putExtra(LocationManagerProxy.KEY_STATUS_CHANGED, false);
            intent.putExtra("data", ConsultActivity.this.noReplyJsonArray.optJSONObject(arg2).toString());
            ConsultActivity.this.startActivity(intent);
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        parameters.add("accid", UserEntity.getInstance().getInfo().getAccId());
        return parameters;
    }
}
