package com.womenchild.hospital.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;
import java.util.ArrayList;

public class QueueCallFragmentPagerAdapter extends FragmentPagerAdapter {
    private static final String TAG = "QCFPAdapter";
    private ArrayList<Fragment> fragmentsList;
    private FragmentManager mFragmentManager;

    public QueueCallFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
        this.mFragmentManager = fm;
    }

    public QueueCallFragmentPagerAdapter(FragmentManager fm, ArrayList<Fragment> fragments) {
        super(fm);
        this.fragmentsList = fragments;
        this.mFragmentManager = fm;
    }

    public int getCount() {
        return this.fragmentsList.size();
    }

    public Fragment getItem(int position) {
        return this.fragmentsList.get(position);
    }

    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    public Object instantiateItem(ViewGroup container, int position) {
        return super.instantiateItem(container, position);
    }
}
