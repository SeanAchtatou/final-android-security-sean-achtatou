package com.womenchild.hospital.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.womenchild.hospital.R;
import com.womenchild.hospital.entity.AutoPaymentEntity;
import java.util.ArrayList;

public class PaymentAdapter extends BaseAdapter {
    private ArrayList<AutoPaymentEntity> autoPaymentEntities;
    private Context context;

    public PaymentAdapter(Context context2, ArrayList<AutoPaymentEntity> autoPaymentEntities2) {
        this.context = context2;
        this.autoPaymentEntities = autoPaymentEntities2;
    }

    public int getCount() {
        return this.autoPaymentEntities.size();
    }

    public Object getItem(int arg0) {
        return this.autoPaymentEntities.get(arg0);
    }

    public long getItemId(int arg0) {
        return (long) arg0;
    }

    public View getView(int arg0, View arg1, ViewGroup arg2) {
        ViewHolder holder;
        if (arg1 == null) {
            holder = new ViewHolder(this, null);
            arg1 = LayoutInflater.from(this.context).inflate((int) R.layout.payment_item, (ViewGroup) null);
            holder.tvPatientName = (TextView) arg1.findViewById(R.id.tv_patient);
            holder.tvDept = (TextView) arg1.findViewById(R.id.tv_dept);
            holder.tvFee = (TextView) arg1.findViewById(R.id.tv_fee);
            holder.tvScheduleTime = (TextView) arg1.findViewById(R.id.tv_scheduletime);
            arg1.setTag(holder);
        } else {
            holder = (ViewHolder) arg1.getTag();
        }
        AutoPaymentEntity autoPaymentEntity = this.autoPaymentEntities.get(arg0);
        holder.tvPatientName.setText(autoPaymentEntity.getPatientName());
        holder.tvScheduleTime.setText(autoPaymentEntity.getScheduleTime());
        holder.tvFee.setText(String.valueOf(Double.valueOf(autoPaymentEntity.getFee()).doubleValue() / 100.0d) + this.context.getResources().getString(R.string.price_rmb));
        holder.tvDept.setText(autoPaymentEntity.getDeptName());
        return arg1;
    }

    private class ViewHolder {
        TextView tvDept;
        TextView tvFee;
        TextView tvPatientName;
        TextView tvScheduleTime;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(PaymentAdapter paymentAdapter, ViewHolder viewHolder) {
            this();
        }
    }
}
