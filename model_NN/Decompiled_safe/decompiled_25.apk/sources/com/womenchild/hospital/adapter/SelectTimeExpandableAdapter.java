package com.womenchild.hospital.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.HomeActivity;
import com.womenchild.hospital.LoginActivity;
import com.womenchild.hospital.R;
import com.womenchild.hospital.RegisterActivity;
import com.womenchild.hospital.SelectClinicTimeActivity;
import com.womenchild.hospital.SelectPatientActivity;
import com.womenchild.hospital.util.DateUtil;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONObject;

public class SelectTimeExpandableAdapter extends BaseExpandableListAdapter {
    private HashMap<Integer, View> childMap = new HashMap<>();
    /* access modifiers changed from: private */
    public Context context;
    private String day;
    /* access modifiers changed from: private */
    public int doctorId;
    private JSONArray jsonArray;
    private HashMap<Integer, View> map = new HashMap<>();

    public SelectTimeExpandableAdapter(Context context2, JSONArray jsonArray2, String day2) {
        this.context = context2;
        this.jsonArray = jsonArray2;
        this.day = day2;
    }

    public SelectTimeExpandableAdapter(Context context2, JSONArray jsonArray2, String day2, int doctorId2) {
        this.context = context2;
        this.jsonArray = jsonArray2;
        this.day = day2;
        this.doctorId = doctorId2;
    }

    public Object getChild(int groupPosition, int childPosition) {
        return this.jsonArray.optJSONObject(groupPosition).optJSONArray("balls").optJSONObject(childPosition);
    }

    public long getChildId(int groupPosition, int childPosition) {
        return (long) childPosition;
    }

    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        BallsHolder holder;
        JSONObject json = this.jsonArray.optJSONObject(groupPosition).optJSONArray("balls").optJSONObject(childPosition);
        if (convertView == null) {
            holder = new BallsHolder(this, null);
            convertView = LayoutInflater.from(this.context).inflate((int) R.layout.select_balls_item, (ViewGroup) null);
            holder.tvDesc = (TextView) convertView.findViewById(R.id.tv_desc);
            holder.btnOrder = (Button) convertView.findViewById(R.id.btn_order);
            convertView.setTag(holder);
            this.childMap.put(Integer.valueOf(childPosition), convertView);
        } else {
            holder = (BallsHolder) convertView.getTag();
        }
        if ("0".equals(json.optString("ball"))) {
            holder.tvDesc.setText(json.optString("notice"));
        } else {
            holder.tvDesc.setText(json.optString("notice"));
        }
        holder.btnOrder.setOnClickListener(new OrderClickLister(this.jsonArray.optJSONObject(groupPosition), Integer.parseInt(json.optString("ball"))));
        return convertView;
    }

    public int getChildrenCount(int groupPosition) {
        return this.jsonArray.optJSONObject(groupPosition).optJSONArray("balls").length();
    }

    public Object getGroup(int groupPosition) {
        return this.jsonArray.optJSONObject(groupPosition);
    }

    public int getGroupCount() {
        return this.jsonArray.length();
    }

    public long getGroupId(int groupPosition) {
        return (long) groupPosition;
    }

    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View convertView2;
        ViewHolder holder;
        JSONObject json = this.jsonArray.optJSONObject(groupPosition);
        if (this.map.get(Integer.valueOf(groupPosition)) == null) {
            holder = new ViewHolder(this, null);
            convertView2 = LayoutInflater.from(this.context).inflate((int) R.layout.select_plan_time_item, (ViewGroup) null);
            holder.tvTime = (TextView) convertView2.findViewById(R.id.tv_time);
            holder.tvDate = (TextView) convertView2.findViewById(R.id.tv_date);
            holder.tvFee = (TextView) convertView2.findViewById(R.id.tv_fee);
            holder.btnOrder = (Button) convertView2.findViewById(R.id.btn_order);
            convertView2.setTag(holder);
            this.map.put(Integer.valueOf(groupPosition), convertView2);
        } else {
            convertView2 = this.map.get(Integer.valueOf(groupPosition));
            holder = (ViewHolder) convertView2.getTag();
        }
        holder.tvTime.setText(String.valueOf(DateUtil.longToString(json.optLong("start"))) + "-" + DateUtil.longToString(json.optLong("stop")));
        holder.tvDate.setText(String.valueOf(this.day) + " " + json.optString("timespan"));
        holder.tvFee.setText(String.valueOf(String.valueOf(((double) json.optInt("fee")) / 100.0d)) + "元");
        holder.btnOrder.setText(PoiTypeDef.All);
        if (isExpanded) {
            holder.btnOrder.setBackgroundResource(R.drawable.ygkz_main_list_arrow_open);
        } else {
            holder.btnOrder.setBackgroundResource(R.drawable.ygkz_main_list_arrow);
        }
        return convertView2;
    }

    public boolean hasStableIds() {
        return false;
    }

    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    private class OrderClickLister implements View.OnClickListener {
        private int ballNum;
        private JSONObject json;

        public OrderClickLister(JSONObject json2, int ballNum2) {
            this.json = json2;
            this.ballNum = ballNum2;
        }

        public void onClick(View v) {
            if (HomeActivity.loginFlag) {
                SelectClinicTimeActivity.json = this.json;
                SelectClinicTimeActivity.ballNum = this.ballNum;
                Intent intent = new Intent(SelectTimeExpandableAdapter.this.context, SelectPatientActivity.class);
                intent.setFlags(67108864);
                intent.putExtra("doctorId", SelectTimeExpandableAdapter.this.doctorId);
                SelectTimeExpandableAdapter.this.context.startActivity(intent);
                return;
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(SelectTimeExpandableAdapter.this.context);
            builder.setTitle("登录提示");
            builder.setMessage("使用此功能前请您先登录！若无帐号，请先注册");
            builder.setPositiveButton("免费注册", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    SelectTimeExpandableAdapter.this.context.startActivity(new Intent(SelectTimeExpandableAdapter.this.context, RegisterActivity.class));
                }
            });
            builder.setNegativeButton("登录", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    SelectTimeExpandableAdapter.this.context.startActivity(new Intent(SelectTimeExpandableAdapter.this.context, LoginActivity.class));
                }
            });
            builder.create().show();
        }
    }

    private class ViewHolder {
        Button btnOrder;
        TextView tvDate;
        TextView tvFee;
        TextView tvTime;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(SelectTimeExpandableAdapter selectTimeExpandableAdapter, ViewHolder viewHolder) {
            this();
        }
    }

    private class BallsHolder {
        Button btnOrder;
        TextView tvDesc;

        private BallsHolder() {
        }

        /* synthetic */ BallsHolder(SelectTimeExpandableAdapter selectTimeExpandableAdapter, BallsHolder ballsHolder) {
            this();
        }
    }
}
