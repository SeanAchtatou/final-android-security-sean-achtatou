package com.womenchild.hospital.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.womenchild.hospital.R;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.entity.PatientInfo;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.request.RequestTask;
import java.util.ArrayList;
import java.util.List;

public class PatientManageAdapter extends BaseAdapter {
    private static String TAG = "PatientManageAdapter";
    /* access modifiers changed from: private */
    public Context context = null;
    private boolean delete_stat;
    /* access modifiers changed from: private */
    public BaseRequestActivity iActivity;
    private List<PatientInfo> listPatient = new ArrayList();
    /* access modifiers changed from: private */
    public int positions;

    public List<PatientInfo> getListPatient() {
        return this.listPatient;
    }

    public void setListPatient(List<PatientInfo> listPatient2) {
        this.listPatient = listPatient2;
    }

    public PatientManageAdapter(Context context2, List<PatientInfo> list) {
        this.context = context2;
        this.listPatient = list;
    }

    public PatientManageAdapter(Context context2, List<PatientInfo> list, BaseRequestActivity iActivity2) {
        this.context = context2;
        this.listPatient = list;
        this.iActivity = iActivity2;
    }

    public boolean isDelete_stat() {
        return this.delete_stat;
    }

    public void setDelete_stat(boolean delete_stat2) {
        this.delete_stat = delete_stat2;
    }

    public int getCount() {
        return this.listPatient.size();
    }

    public Object getItem(int position) {
        return this.listPatient.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public int getPositions() {
        return this.positions;
    }

    public void setPositions(int positions2) {
        this.positions = positions2;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        PatientInfo info = this.listPatient.get(position);
        LayoutInflater mInflater = LayoutInflater.from(this.context);
        if (convertView == null) {
            holder = new ViewHolder(this, null);
            convertView = mInflater.inflate((int) R.layout.manage_item, (ViewGroup) null);
            holder.tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
            holder.iv_arrow = (ImageView) convertView.findViewById(R.id.iv_arrow);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (info != null) {
            setUIData(holder, info, position);
        }
        return convertView;
    }

    private void setUIData(ViewHolder holder, final PatientInfo info, final int position) {
        holder.tvTitle.setText(info.getPatientname());
        if (this.delete_stat) {
            holder.iv_arrow.setBackgroundResource(R.drawable.btn_delete_selector);
            holder.iv_arrow.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    PatientManageAdapter.this.positions = position;
                    AlertDialog.Builder title = new AlertDialog.Builder(PatientManageAdapter.this.context).setTitle(PatientManageAdapter.this.context.getResources().getString(R.string.y_n_delect_person));
                    String string = PatientManageAdapter.this.context.getResources().getString(R.string.y_delect_person);
                    final PatientInfo patientInfo = info;
                    title.setPositiveButton(string, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            UriParameter parameters = new UriParameter();
                            parameters.add("patientid", patientInfo.getPatientid());
                            RequestTask.getInstance().sendHttpRequest(PatientManageAdapter.this.iActivity, String.valueOf((int) HttpRequestParameters.DELETE_PATIENT_INFO), parameters);
                        }
                    }).setNegativeButton(PatientManageAdapter.this.context.getResources().getString(R.string.cancel), (DialogInterface.OnClickListener) null).show();
                }
            });
            return;
        }
        holder.iv_arrow.setBackgroundResource(R.drawable.ygkz_main_list_arrow);
        holder.iv_arrow.setOnClickListener(null);
    }

    private class ViewHolder {
        /* access modifiers changed from: private */
        public ImageView iv_arrow;
        public TextView tvTitle;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(PatientManageAdapter patientManageAdapter, ViewHolder viewHolder) {
            this();
        }
    }
}
