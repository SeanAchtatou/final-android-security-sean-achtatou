package com.womenchild.hospital.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.womenchild.hospital.R;
import com.womenchild.hospital.entity.PaymentEntity;
import java.util.HashMap;
import java.util.List;

public class PaymentOrderAdapter extends BaseAdapter {
    private static final String TAG = "POAdapter";
    private List<PaymentEntity> list;
    private HashMap<Integer, View> lmap = new HashMap<>();
    private Context mContext;
    private LayoutInflater mInflater;

    public PaymentOrderAdapter(Context context, List<PaymentEntity> orderList) {
        this.mInflater = LayoutInflater.from(context);
        this.list = orderList;
        this.mContext = context;
    }

    class ViewHolder {
        TextView feeFormTv;
        TextView tv_fee_category;
        TextView tv_order_date;
        TextView tv_order_no;
        TextView tv_payment_money;

        ViewHolder() {
        }
    }

    public int getCount() {
        if (this.list == null) {
            return 0;
        }
        return this.list.size();
    }

    public Object getItem(int arg0) {
        return this.list.get(arg0);
    }

    public long getItemId(int arg0) {
        return (long) arg0;
    }

    public View getView(int position, View convertView, ViewGroup arg2) {
        PaymentEntity entity = this.list.get(position);
        ViewHolder holder = new ViewHolder();
        View convertView2 = this.mInflater.inflate((int) R.layout.payment_order_item, (ViewGroup) null);
        holder.tv_order_no = (TextView) convertView2.findViewById(R.id.tv_order_no);
        holder.tv_payment_money = (TextView) convertView2.findViewById(R.id.tv_payment_money);
        holder.tv_order_date = (TextView) convertView2.findViewById(R.id.tv_order_date);
        holder.tv_fee_category = (TextView) convertView2.findViewById(R.id.tv_fee_form);
        holder.feeFormTv = (TextView) convertView2.findViewById(R.id.tv_payment_money_title);
        convertView2.setTag(holder);
        holder.tv_order_no.setText(entity.getOrdernum());
        holder.tv_payment_money.setText(String.valueOf(String.valueOf(entity.getPrice())) + this.mContext.getResources().getString(R.string.price_rmb));
        holder.tv_order_date.setText(entity.getPaydate());
        holder.tv_fee_category.setText(entity.getFeeForm());
        if (entity.getState() == 0) {
            holder.feeFormTv.setText(this.mContext.getResources().getString(R.string.y_pay_sum));
        } else if (entity.getState() == 1) {
            holder.feeFormTv.setText(this.mContext.getResources().getString(R.string.y_pay_wait));
        } else if (entity.getState() == 2) {
            holder.feeFormTv.setText(this.mContext.getResources().getString(R.string.y_pay_refund));
        }
        return convertView2;
    }
}
