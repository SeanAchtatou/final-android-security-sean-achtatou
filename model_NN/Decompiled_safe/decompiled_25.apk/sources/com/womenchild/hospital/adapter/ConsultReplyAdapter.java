package com.womenchild.hospital.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.umeng.common.a;
import com.womenchild.hospital.R;
import org.json.JSONArray;
import org.json.JSONObject;

public class ConsultReplyAdapter extends BaseAdapter {
    private Context context = null;
    private JSONArray jsons = new JSONArray();

    public ConsultReplyAdapter(Context context2, JSONArray jsonArray) {
        this.jsons = jsonArray;
        this.context = context2;
    }

    private class ViewHolder {
        public ImageView ivBottom;
        public RelativeLayout rlReply;
        public RelativeLayout rlTop;
        public TextView tvTime;
        public TextView tv_reply;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(ConsultReplyAdapter consultReplyAdapter, ViewHolder viewHolder) {
            this();
        }
    }

    public int getCount() {
        return this.jsons.length();
    }

    public Object getItem(int arg0) {
        return this.jsons.opt(arg0);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        int topId;
        int bottomId;
        int centerId;
        String timeString;
        JSONObject json = this.jsons.optJSONObject(position);
        if (json != null) {
            convertView = LayoutInflater.from(this.context).inflate((int) R.layout.consult_reply_item, (ViewGroup) null);
            holder = new ViewHolder(this, null);
            holder.tv_reply = (TextView) convertView.findViewById(R.id.tv_reply);
            holder.ivBottom = (ImageView) convertView.findViewById(R.id.iv_bottom);
            holder.rlTop = (RelativeLayout) convertView.findViewById(R.id.rl_top);
            holder.rlReply = (RelativeLayout) convertView.findViewById(R.id.rl_reply);
            holder.tvTime = (TextView) convertView.findViewById(R.id.tv_info);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (json.optInt(a.b) == 0) {
            topId = R.drawable.ygkz_remotion_dialog_bg_green_ex_top;
            bottomId = R.drawable.ygkz_remotion_dialog_bg_green_ex_bottom;
            centerId = R.drawable.ygkz_remotion_dialog_bg_green_ex_middle;
            timeString = String.valueOf(this.context.getResources().getString(R.string.reply_time)) + json.optString("time");
        } else {
            topId = R.drawable.ygkz_remotion_dialog_bg_grey_ex_top;
            bottomId = R.drawable.ygkz_remotion_dialog_bg_grey_ex_bottom_color;
            centerId = R.drawable.ygkz_remotion_dialog_bg_grey_ex_middle;
            timeString = String.valueOf(this.context.getResources().getString(R.string.closely_time)) + json.optString("time");
        }
        holder.tvTime.setText(timeString);
        holder.ivBottom.setBackgroundResource(bottomId);
        holder.rlTop.setBackgroundResource(topId);
        holder.rlReply.setBackgroundResource(centerId);
        holder.tv_reply.setText(json.optString("content"));
        return convertView;
    }
}
