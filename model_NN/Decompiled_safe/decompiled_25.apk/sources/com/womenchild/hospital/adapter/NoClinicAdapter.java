package com.womenchild.hospital.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.womenchild.hospital.R;
import com.womenchild.hospital.entity.RecordEntity;
import com.womenchild.hospital.util.DateUtil;
import java.util.List;

public class NoClinicAdapter extends BaseAdapter {
    private Context context;
    private List<RecordEntity> list;

    public NoClinicAdapter(Context context2, List<RecordEntity> list2) {
        this.context = context2;
        this.list = list2;
    }

    public int getCount() {
        return this.list.size();
    }

    public Object getItem(int position) {
        return this.list.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = View.inflate(this.context, R.layout.not_clinic_listview_item, null);
            viewHolder = new ViewHolder(null);
            viewHolder.tv_doctor_name = (TextView) convertView.findViewById(R.id.tv_doctor_name);
            viewHolder.tv_room = (TextView) convertView.findViewById(R.id.tv_room);
            viewHolder.tv_time = (TextView) convertView.findViewById(R.id.tv_time);
            viewHolder.tv_order_status = (TextView) convertView.findViewById(R.id.tv_order_status);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tv_doctor_name.setText(this.list.get(position).getDoctorname());
        viewHolder.tv_room.setText(this.list.get(position).getDeptname());
        viewHolder.tv_time.setText(DateUtil.getTimeFromLong(Long.valueOf(this.list.get(position).getPlanstarttime()).longValue()));
        viewHolder.tv_order_status.setText(this.list.get(position).getStatus());
        return convertView;
    }

    private static class ViewHolder {
        TextView tv_doctor_name;
        TextView tv_order_status;
        TextView tv_room;
        TextView tv_time;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(ViewHolder viewHolder) {
            this();
        }
    }
}
