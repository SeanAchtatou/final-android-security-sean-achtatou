package com.womenchild.hospital.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.womenchild.hospital.R;
import com.womenchild.hospital.entity.RecordEntity;
import com.womenchild.hospital.util.DateUtil;
import java.util.HashMap;
import java.util.List;

public class DoctorOrderedAdapter extends BaseAdapter {
    private static final String TAG = "DOAdapter";
    private List<RecordEntity> list;
    private HashMap<Integer, View> lmap = new HashMap<>();
    private LayoutInflater mInflater;

    public DoctorOrderedAdapter(Context context, List<RecordEntity> doctorList) {
        this.mInflater = LayoutInflater.from(context);
        this.list = doctorList;
    }

    class ViewHolder {
        TextView tv_department;
        TextView tv_doctor;
        TextView tv_hospital;
        TextView tv_ordered_date;

        ViewHolder() {
        }
    }

    public int getCount() {
        if (this.list == null) {
            return 0;
        }
        return this.list.size();
    }

    public Object getItem(int position) {
        return this.list.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        RecordEntity entity = this.list.get(position);
        ViewHolder holder = new ViewHolder();
        View convertView2 = this.mInflater.inflate((int) R.layout.ordered_item, (ViewGroup) null);
        holder.tv_hospital = (TextView) convertView2.findViewById(R.id.tv_hospital);
        holder.tv_department = (TextView) convertView2.findViewById(R.id.tv_department);
        holder.tv_ordered_date = (TextView) convertView2.findViewById(R.id.tv_order_date);
        convertView2.setTag(holder);
        holder.tv_hospital.setText(entity.getDoctorname());
        holder.tv_department.setText(entity.getDeptname());
        holder.tv_ordered_date.setText(DateUtil.getTimeFromLong(Long.valueOf(entity.getPlanstarttime()).longValue()));
        return convertView2;
    }
}
