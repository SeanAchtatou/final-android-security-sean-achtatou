package com.womenchild.hospital.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import com.womenchild.hospital.DoctorPlanActivity;
import com.womenchild.hospital.R;
import org.json.JSONArray;
import org.json.JSONObject;

public class FindDoctorAdapter extends BaseAdapter {
    /* access modifiers changed from: private */
    public Context context = null;
    private JSONArray jsons = new JSONArray();

    public FindDoctorAdapter(Context context2, JSONArray jsonArray) {
        this.jsons = jsonArray;
        this.context = context2;
    }

    private class ViewHolder {
        public TextView hopitalTv;
        public TextView levelTv;
        /* access modifiers changed from: private */
        public Button orderBtn;
        public TextView sexTv;
        public TextView tv_find_doctor;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(FindDoctorAdapter findDoctorAdapter, ViewHolder viewHolder) {
            this();
        }
    }

    public int getCount() {
        return this.jsons.length();
    }

    public Object getItem(int arg0) {
        return this.jsons.opt(arg0);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        JSONObject json = this.jsons.optJSONObject(position);
        if (json != null) {
            convertView = LayoutInflater.from(this.context).inflate((int) R.layout.find_doctor_item, (ViewGroup) null);
            holder = new ViewHolder(this, null);
            holder.tv_find_doctor = (TextView) convertView.findViewById(R.id.tv_find_doctor);
            holder.sexTv = (TextView) convertView.findViewById(R.id.tv_sex);
            holder.levelTv = (TextView) convertView.findViewById(R.id.tv_level);
            holder.hopitalTv = (TextView) convertView.findViewById(R.id.tv_hospital);
            holder.orderBtn = (Button) convertView.findViewById(R.id.btn_order);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        String name = json.optString("doctorname");
        holder.tv_find_doctor.setText(name);
        holder.sexTv.setText(json.optString("sex"));
        holder.levelTv.setText(json.optString("clinicalgrade"));
        holder.hopitalTv.setText(json.optString("hospitalname"));
        holder.orderBtn.setOnClickListener(new OrderClickListener(json.optInt("doctorid"), name));
        return convertView;
    }

    private class OrderClickListener implements View.OnClickListener {
        private int doctorId;
        private String name;

        public OrderClickListener(int doctorId2, String name2) {
            this.doctorId = doctorId2;
            this.name = name2;
        }

        public void onClick(View v) {
            Intent intent = new Intent(FindDoctorAdapter.this.context, DoctorPlanActivity.class);
            intent.putExtra("doctorid", this.doctorId);
            intent.putExtra("doctorName", this.name);
            FindDoctorAdapter.this.context.startActivity(intent);
        }
    }
}
