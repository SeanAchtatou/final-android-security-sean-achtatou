package com.womenchild.hospital.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.womenchild.hospital.R;
import com.womenchild.hospital.entity.DoctorEntity;
import java.util.ArrayList;

public class CollectedDoctorAdapter extends BaseAdapter {
    private static final String TAG = "CollectedDoctorAdapter";
    private Context context;
    ListView list;
    private LayoutInflater mInflater;
    private ArrayList<DoctorEntity> testPrjEntityList = new ArrayList<>();

    public CollectedDoctorAdapter(Context context2, ListView list2, ArrayList<DoctorEntity> testPrjEntitylist) {
        this.list = list2;
        this.context = context2;
        this.testPrjEntityList = testPrjEntitylist;
        this.mInflater = LayoutInflater.from(context2);
    }

    public void setList(ArrayList<DoctorEntity> testPrjEntitylist) {
        this.testPrjEntityList = testPrjEntitylist;
    }

    public int getCount() {
        return this.testPrjEntityList.size();
    }

    public Object getItem(int position) {
        return this.testPrjEntityList.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        DoctorEntity mTestPrjEntity = this.testPrjEntityList.get(position);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = this.mInflater.inflate((int) R.layout.collected_doctor_item, (ViewGroup) null);
            holder.name = (TextView) convertView.findViewById(R.id.tv_doctor);
            holder.departemnt = (TextView) convertView.findViewById(R.id.tv_department);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.name.setText(mTestPrjEntity.getName());
        holder.departemnt.setText(mTestPrjEntity.getLevel());
        return convertView;
    }

    class ViewHolder {
        TextView departemnt;
        TextView name;

        ViewHolder() {
        }
    }
}
