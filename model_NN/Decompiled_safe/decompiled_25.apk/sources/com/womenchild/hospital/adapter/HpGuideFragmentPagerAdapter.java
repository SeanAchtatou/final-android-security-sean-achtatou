package com.womenchild.hospital.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import java.util.ArrayList;

public class HpGuideFragmentPagerAdapter extends FragmentPagerAdapter {
    private static final String TAG = "FPAdapter";
    private FragmentManager fm;
    private ArrayList<Fragment> fragmentsList;

    public HpGuideFragmentPagerAdapter(FragmentManager fm2) {
        super(fm2);
        this.fm = fm2;
    }

    public HpGuideFragmentPagerAdapter(FragmentManager fm2, ArrayList<Fragment> fragments) {
        super(fm2);
        this.fm = fm2;
        this.fragmentsList = fragments;
    }

    public Fragment getItem(int arg0) {
        return this.fragmentsList.get(arg0);
    }

    public int getCount() {
        return this.fragmentsList.size();
    }
}
