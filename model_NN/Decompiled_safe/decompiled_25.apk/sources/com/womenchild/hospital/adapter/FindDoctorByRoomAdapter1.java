package com.womenchild.hospital.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.womenchild.hospital.R;
import com.womenchild.hospital.SelectDoctorActivity;
import java.util.HashMap;
import org.json.JSONArray;

public class FindDoctorByRoomAdapter1 extends BaseExpandableListAdapter {
    private HashMap<Integer, View> childMap = new HashMap<>();
    /* access modifiers changed from: private */
    public Context context;
    private JSONArray jsonArray;
    private HashMap<Integer, View> map = new HashMap<>();

    public HashMap<Integer, View> getMap() {
        return this.map;
    }

    public FindDoctorByRoomAdapter1(Context context2, JSONArray jsonArray2) {
        this.context = context2;
        this.jsonArray = jsonArray2;
    }

    public Object getChild(int arg0, int arg1) {
        return this.jsonArray.optJSONObject(arg0).optJSONArray("childs").optJSONObject(arg1);
    }

    public long getChildId(int groupPosition, int childPosition) {
        return (long) childPosition;
    }

    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        View convertView2;
        DeptViewHolder deptHolder;
        int optInt = this.jsonArray.optJSONObject(groupPosition).optJSONArray("childs").optJSONObject(childPosition).optInt("deptid");
        String optString = this.jsonArray.optJSONObject(groupPosition).optJSONArray("childs").optJSONObject(childPosition).optString("deptname");
        if (this.childMap.get(Integer.valueOf(childPosition)) == null) {
            convertView2 = LayoutInflater.from(this.context).inflate((int) R.layout.search_dept_item, (ViewGroup) null);
            deptHolder = new DeptViewHolder(this, null);
            deptHolder.tvTitle = (TextView) convertView2.findViewById(R.id.tv_title);
            deptHolder.btnOrder = (Button) convertView2.findViewById(R.id.btn_order);
            this.childMap.put(Integer.valueOf(childPosition), convertView2);
            convertView2.setTag(deptHolder);
        } else {
            convertView2 = this.childMap.get(Integer.valueOf(childPosition));
            deptHolder = (DeptViewHolder) convertView2.getTag();
        }
        deptHolder.tvTitle.setText(this.jsonArray.optJSONObject(groupPosition).optJSONArray("childs").optJSONObject(childPosition).optString("deptname"));
        deptHolder.btnOrder.setVisibility(8);
        return convertView2;
    }

    private class OrderClickListener implements View.OnClickListener {
        private int deptid;

        public OrderClickListener(int deptid2) {
            this.deptid = deptid2;
        }

        public void onClick(View v) {
            Intent intent = new Intent(FindDoctorByRoomAdapter1.this.context, SelectDoctorActivity.class);
            intent.putExtra("deptid", this.deptid);
            FindDoctorByRoomAdapter1.this.context.startActivity(intent);
        }
    }

    public int getChildrenCount(int groupPosition) {
        return this.jsonArray.optJSONObject(groupPosition).optJSONArray("childs").length();
    }

    public Object getGroup(int groupPosition) {
        return this.jsonArray.optJSONArray(groupPosition);
    }

    public int getGroupCount() {
        return this.jsonArray.length();
    }

    public long getGroupId(int groupPosition) {
        return (long) groupPosition;
    }

    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View convertView2;
        HospitalViewHolder hospitalHolder;
        if (this.map.get(Integer.valueOf(groupPosition)) == null) {
            convertView2 = LayoutInflater.from(this.context).inflate((int) R.layout.find_doctor_listview_item, (ViewGroup) null);
            hospitalHolder = new HospitalViewHolder(this, null);
            hospitalHolder.tvTitle = (TextView) convertView2.findViewById(R.id.tv_find_doctor_by_rooms);
            hospitalHolder.ivArrow = (ImageView) convertView2.findViewById(R.id.iv_arrow);
            this.map.put(Integer.valueOf(groupPosition), convertView2);
            convertView2.setTag(hospitalHolder);
        } else {
            convertView2 = this.map.get(Integer.valueOf(groupPosition));
            hospitalHolder = (HospitalViewHolder) convertView2.getTag();
        }
        if (isExpanded) {
            hospitalHolder.ivArrow.setBackgroundResource(R.drawable.ygkz_main_list_arrow_open);
        } else {
            hospitalHolder.ivArrow.setBackgroundResource(R.drawable.ygkz_main_list_arrow);
        }
        hospitalHolder.tvTitle.setText(this.jsonArray.optJSONObject(groupPosition).optString("deptname"));
        return convertView2;
    }

    public boolean hasStableIds() {
        return false;
    }

    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private class HospitalViewHolder {
        ImageView ivArrow;
        TextView tvTitle;

        private HospitalViewHolder() {
        }

        /* synthetic */ HospitalViewHolder(FindDoctorByRoomAdapter1 findDoctorByRoomAdapter1, HospitalViewHolder hospitalViewHolder) {
            this();
        }
    }

    private class DeptViewHolder {
        Button btnOrder;
        TextView tvTitle;

        private DeptViewHolder() {
        }

        /* synthetic */ DeptViewHolder(FindDoctorByRoomAdapter1 findDoctorByRoomAdapter1, DeptViewHolder deptViewHolder) {
            this();
        }
    }
}
