package com.womenchild.hospital.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.womenchild.hospital.R;

public class CardUserTwoAdapter extends BaseAdapter {
    private Context context;
    private String[] text;

    public CardUserTwoAdapter(Context context2, String[] text2) {
        this.context = context2;
        this.text = text2;
    }

    public int getCount() {
        return this.text.length;
    }

    public Object getItem(int arg0) {
        return this.text[arg0];
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder(null);
            View view = View.inflate(this.context, R.layout.card_list_item, null);
            viewHolder.tvCardTitle = (TextView) view.findViewById(R.id.tv_card_title);
            viewHolder.tvCard = (TextView) view.findViewById(R.id.tv_card);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tvCardTitle.setText(String.valueOf(this.context.getResources().getString(R.string.tips_patient_card)) + (position + 1) + ":");
        viewHolder.tvCard.setText(this.text[position]);
        return convertView;
    }

    private static class ViewHolder {
        TextView tvCard;
        TextView tvCardTitle;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(ViewHolder viewHolder) {
            this();
        }
    }
}
