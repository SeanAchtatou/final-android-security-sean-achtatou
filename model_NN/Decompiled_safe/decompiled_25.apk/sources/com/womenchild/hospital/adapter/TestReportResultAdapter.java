package com.womenchild.hospital.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.womenchild.hospital.R;
import com.womenchild.hospital.entity.TestPrjSubEntity;
import java.util.ArrayList;

public class TestReportResultAdapter extends BaseAdapter {
    private static final String TAG = "TestReportResultAdapter";
    private Context context;
    ListView list;
    private LayoutInflater mInflater;
    private ArrayList<TestPrjSubEntity> testPrjSubEntityList = new ArrayList<>();

    public TestReportResultAdapter(Context context2, ListView list2, ArrayList<TestPrjSubEntity> testPrjEntitylist) {
        this.list = list2;
        this.context = context2;
        this.testPrjSubEntityList = testPrjEntitylist;
        this.mInflater = LayoutInflater.from(context2);
    }

    public void setList(ArrayList<TestPrjSubEntity> testPrjEntitylist) {
        this.testPrjSubEntityList = testPrjEntitylist;
    }

    public int getCount() {
        return this.testPrjSubEntityList.size();
    }

    public Object getItem(int position) {
        return this.testPrjSubEntityList.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        TestPrjSubEntity mTestPrjEntity = this.testPrjSubEntityList.get(position);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = this.mInflater.inflate((int) R.layout.test_report_result_lv_item, (ViewGroup) null);
            holder.name = (TextView) convertView.findViewById(R.id.tv_first_test_pjt_name);
            holder.uploadResult = (TextView) convertView.findViewById(R.id.tv_first_test_result_name);
            holder.uploadDate = (TextView) convertView.findViewById(R.id.tv_first_test_date);
            holder.clinicNameTv = (TextView) convertView.findViewById(R.id.tv_first_test_clinic_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.name.setText(mTestPrjEntity.getItemName());
        holder.uploadResult.setText(mTestPrjEntity.getResult());
        holder.uploadDate.setText(mTestPrjEntity.getUnit());
        holder.clinicNameTv.setText(mTestPrjEntity.getReference());
        return convertView;
    }

    class ViewHolder {
        TextView clinicNameTv;
        TextView name;
        TextView uploadDate;
        TextView uploadResult;

        ViewHolder() {
        }
    }
}
