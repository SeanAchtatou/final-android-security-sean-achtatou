package com.womenchild.hospital.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.womenchild.hospital.R;
import com.womenchild.hospital.entity.QueueCallVisEntity;
import java.util.List;

public class QueueCallVisAdapter extends BaseAdapter {
    private Context context;
    private List<QueueCallVisEntity> list;

    public QueueCallVisAdapter(Context context2, List<QueueCallVisEntity> list2) {
        this.context = context2;
        this.list = list2;
    }

    public int getCount() {
        return this.list.size();
    }

    public Object getItem(int position) {
        return this.list.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        String frontWaiting;
        if (convertView == null) {
            convertView = View.inflate(this.context, R.layout.queue_vis_lv_item, null);
            viewHolder = new ViewHolder(null);
            viewHolder.tv_doctor_name = (TextView) convertView.findViewById(R.id.tv_doctor_name);
            viewHolder.patientCurrentNumTv = (TextView) convertView.findViewById(R.id.tv_vis_no);
            viewHolder.frontNumTv = (TextView) convertView.findViewById(R.id.tv_front_number);
            viewHolder.currentNumTv = (TextView) convertView.findViewById(R.id.tv_vis_current_num);
            viewHolder.frontNumTextTv = (TextView) convertView.findViewById(R.id.tv_front_number_title);
            viewHolder.frontNumUnitTv = (TextView) convertView.findViewById(R.id.tv_front_2);
            viewHolder.tipsTv = (TextView) convertView.findViewById(R.id.tv_vis_prompt);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        String shareText = String.format(this.context.getResources().getString(R.string.tips_order_doctor), this.list.get(position).getDoctorname());
        int patientNum = Integer.valueOf(this.list.get(position).getPatientSerialNo()).intValue();
        int currentNum = Integer.valueOf(this.list.get(position).getCurrentNum()).intValue();
        viewHolder.tv_doctor_name.setText(shareText);
        viewHolder.patientCurrentNumTv.setText(String.valueOf(patientNum) + this.context.getResources().getString(R.string.number));
        viewHolder.currentNumTv.setText(String.valueOf(currentNum) + this.context.getResources().getString(R.string.number));
        if (patientNum == currentNum) {
            frontWaiting = this.context.getResources().getString(R.string.tips_num_reach);
            viewHolder.frontNumTextTv.setVisibility(4);
            viewHolder.frontNumUnitTv.setVisibility(8);
            viewHolder.tipsTv.setVisibility(4);
            viewHolder.frontNumTv.setTextSize(22.0f);
        } else if (patientNum < currentNum) {
            frontWaiting = this.context.getResources().getString(R.string.tips_num_pass);
            viewHolder.frontNumTextTv.setVisibility(4);
            viewHolder.frontNumUnitTv.setVisibility(8);
            viewHolder.tipsTv.setVisibility(4);
            viewHolder.frontNumTv.setTextSize(22.0f);
        } else {
            frontWaiting = this.list.get(position).getFrontWaiting();
            viewHolder.frontNumTextTv.setVisibility(0);
            viewHolder.frontNumUnitTv.setVisibility(0);
            viewHolder.tipsTv.setVisibility(0);
            viewHolder.frontNumTv.setTextSize(76.0f);
        }
        viewHolder.frontNumTv.setText(frontWaiting);
        return convertView;
    }

    private static class ViewHolder {
        TextView currentNumTv;
        TextView frontNumTextTv;
        TextView frontNumTv;
        TextView frontNumUnitTv;
        TextView patientCurrentNumTv;
        TextView tipsTv;
        TextView tv_doctor_name;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(ViewHolder viewHolder) {
            this();
        }
    }
}
