package com.womenchild.hospital.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.womenchild.hospital.R;
import com.womenchild.hospital.entity.DoctorEntity;
import java.util.HashMap;
import java.util.List;

public class DoctorCollectedAdapter extends BaseAdapter {
    private static final String TAG = "DOAdapter";
    private List<DoctorEntity> list = null;
    private HashMap<Integer, View> lmap = new HashMap<>();
    private LayoutInflater mInflater;

    public DoctorCollectedAdapter(Context context, List<DoctorEntity> doctorList) {
        this.mInflater = LayoutInflater.from(context);
        this.list = doctorList;
    }

    class ViewHolder {
        TextView tv_department;
        TextView tv_doctor;

        ViewHolder() {
        }
    }

    public int getCount() {
        if (this.list == null) {
            return 0;
        }
        return this.list.size();
    }

    public Object getItem(int position) {
        return this.list.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        DoctorEntity doctorEntity = this.list.get(position);
        if (this.lmap.get(Integer.valueOf(position)) == null) {
            ViewHolder holder = new ViewHolder();
            View convertView2 = this.mInflater.inflate((int) R.layout.collected_item, (ViewGroup) null);
            this.lmap.put(Integer.valueOf(position), convertView2);
            convertView2.setTag(holder);
            return convertView2;
        }
        View convertView3 = this.lmap.get(Integer.valueOf(position));
        ViewHolder viewHolder = (ViewHolder) convertView3.getTag();
        return convertView3;
    }
}
