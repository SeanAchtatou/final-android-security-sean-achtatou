package com.womenchild.hospital.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.umeng.common.a;
import com.womenchild.hospital.HomeActivity;
import com.womenchild.hospital.R;
import com.womenchild.hospital.SelectClinicTimeActivity;
import com.womenchild.hospital.SelectDoctorActivity;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.request.RequestTask;
import com.womenchild.hospital.util.DateUtil;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DoctorPlanAdatper extends BaseAdapter {
    private String TAG = "DoctorPlanAdatper";
    /* access modifiers changed from: private */
    public Context context = null;
    private JSONArray jsons = new JSONArray();
    /* access modifiers changed from: private */
    public Map<Integer, View> viewMap = new HashMap();

    public DoctorPlanAdatper(Context context2, JSONArray jsonArray) {
        this.context = context2;
        this.jsons = jsonArray;
    }

    public int getCount() {
        return this.jsons.length();
    }

    public Object getItem(int arg0) {
        return this.jsons.opt(arg0);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View view, ViewGroup parent) {
        Log.d(this.TAG, "getView position" + position);
        JSONObject json = this.jsons.optJSONObject(position);
        View view2 = LayoutInflater.from(this.context).inflate((int) R.layout.doctor_plan_item, (ViewGroup) null);
        ViewHolder holder = new ViewHolder(this, null);
        holder.tvDoctorName = (TextView) view2.findViewById(R.id.tv_doctor_name);
        holder.tvDoctorInfo = (TextView) view2.findViewById(R.id.tv_doctor_info);
        holder.ivDoctorPhoto = (ImageView) view2.findViewById(R.id.iv_doctor_photo);
        holder.tvDoctorSex = (TextView) view2.findViewById(R.id.tv_doctor_sex);
        holder.tvTime1 = (TextView) view2.findViewById(R.id.tv_time_1);
        holder.tvTime2 = (TextView) view2.findViewById(R.id.tv_time_2);
        holder.tvTime3 = (TextView) view2.findViewById(R.id.tv_time_3);
        holder.tvOrderCount = (TextView) view2.findViewById(R.id.tv_order_count);
        holder.tvOrder1 = (TextView) view2.findViewById(R.id.tv_order_1);
        holder.tvOrder2 = (TextView) view2.findViewById(R.id.tv_order_2);
        holder.tvOrder3 = (TextView) view2.findViewById(R.id.tv_order_3);
        holder.tvOrder4 = (TextView) view2.findViewById(R.id.tv_order_4);
        holder.tvOrder5 = (TextView) view2.findViewById(R.id.tv_order_5);
        holder.tvOrder6 = (TextView) view2.findViewById(R.id.tv_order_6);
        holder.btnOrder1 = (Button) view2.findViewById(R.id.btn_order_1);
        holder.btnOrder2 = (Button) view2.findViewById(R.id.btn_order_2);
        holder.btnOrder3 = (Button) view2.findViewById(R.id.btn_order_3);
        holder.btnOrder4 = (Button) view2.findViewById(R.id.btn_order_4);
        holder.btnOrder5 = (Button) view2.findViewById(R.id.btn_order_5);
        holder.btnOrder6 = (Button) view2.findViewById(R.id.btn_order_6);
        holder.btnFavDoctor = (Button) view2.findViewById(R.id.btn_favdoctor);
        holder.btnDayForward = (Button) view2.findViewById(R.id.btn_day_forward);
        holder.btnDayNext = (Button) view2.findViewById(R.id.btn_day_next);
        view2.setTag(holder);
        this.viewMap.put(Integer.valueOf(position), view2);
        if ("0".equals(json.optString("gender"))) {
            holder.tvDoctorSex.setText(this.context.getResources().getString(R.string.woman));
        } else if ("1".equals(json.optString("gender"))) {
            holder.tvDoctorSex.setText(this.context.getResources().getString(R.string.man));
        } else {
            holder.tvDoctorSex.setText(this.context.getResources().getString(R.string.sex_unk));
        }
        if (HomeActivity.loginFlag) {
            holder.btnFavDoctor.setVisibility(0);
        } else {
            holder.btnFavDoctor.setVisibility(8);
        }
        JSONArray plan = json.optJSONArray("daysplan");
        if (json.optInt("favdoctor") == 0) {
            holder.btnFavDoctor.setBackgroundResource(R.drawable.ygkz_order_doctor_button_dark);
            holder.btnFavDoctor.setText(this.context.getResources().getString(R.string.ok_collect));
            holder.btnFavDoctor.setEnabled(false);
        } else {
            holder.btnFavDoctor.setBackgroundResource(R.drawable.blue_order_selector);
            holder.btnFavDoctor.setText(this.context.getResources().getString(R.string.collect));
            holder.btnFavDoctor.setEnabled(true);
            holder.btnFavDoctor.setOnClickListener(new FavDoctorClickListener(view2, json, position));
        }
        holder.btnDayNext.setVisibility(0);
        if (plan == null || plan.length() <= 3) {
            holder.btnDayNext.setBackgroundResource(R.drawable.yl_button_doctor_day_next_none_ex_big);
            holder.btnDayNext.setEnabled(false);
        } else {
            holder.btnDayNext.setBackgroundResource(R.drawable.day_next_selector);
            holder.btnDayNext.setEnabled(true);
        }
        initHolder(holder);
        for (int i = 0; i < plan.length(); i++) {
            JSONObject jsonObject = plan.optJSONObject(i);
            switch (i) {
                case 0:
                    holder.tvTime1.setText(DateUtil.replaceDate(json.optJSONArray("daysplan").optJSONObject(i).optString("day")));
                    if (!stop(plan.optJSONObject(i).optJSONArray("planlist"), this.context.getResources().getString(R.string.morning))) {
                        int num = count(plan.optJSONObject(i).optJSONArray("planlist"), this.context.getResources().getString(R.string.morning));
                        if (num > 0) {
                            holder.tvOrder1.setText(String.valueOf(this.context.getResources().getString(R.string.order_num)) + num);
                            holder.btnOrder1.setOnClickListener(new orderClickListener(jsonObject, this.context.getResources().getString(R.string.morning)));
                            holder.tvOrder1.setVisibility(0);
                            holder.btnOrder1.setVisibility(0);
                        } else if (num == 0) {
                            holder.tvOrder1.setText(String.valueOf(this.context.getResources().getString(R.string.order_num)) + ":" + "0");
                            holder.tvOrder1.setVisibility(0);
                            holder.btnOrder1.setVisibility(0);
                            holder.btnOrder1.setText(this.context.getResources().getString(R.string.full));
                            holder.btnOrder1.setBackgroundResource(R.drawable.ygkz_order_doctor_button_dark);
                            holder.btnOrder1.setEnabled(false);
                        } else {
                            holder.tvOrder1.setText(PoiTypeDef.All);
                            holder.btnOrder1.setVisibility(8);
                        }
                        if (!stop(plan.optJSONObject(i).optJSONArray("planlist"), this.context.getResources().getString(R.string.afternoon))) {
                            int num2 = count(plan.optJSONObject(i).optJSONArray("planlist"), "下午");
                            if (num2 <= 0) {
                                if (num2 != 0) {
                                    holder.tvOrder4.setText(PoiTypeDef.All);
                                    holder.btnOrder4.setVisibility(8);
                                    break;
                                } else {
                                    holder.tvOrder4.setText(String.valueOf(this.context.getResources().getString(R.string.order_num)) + ":" + "0");
                                    holder.btnOrder4.setText(this.context.getResources().getString(R.string.full));
                                    holder.tvOrder4.setVisibility(0);
                                    holder.btnOrder4.setVisibility(0);
                                    holder.btnOrder4.setBackgroundResource(R.drawable.ygkz_order_doctor_button_dark);
                                    holder.btnOrder4.setEnabled(false);
                                    break;
                                }
                            } else {
                                holder.tvOrder4.setText(String.valueOf(this.context.getResources().getString(R.string.order_num)) + num2);
                                holder.tvOrder4.setVisibility(0);
                                holder.btnOrder4.setOnClickListener(new orderClickListener(jsonObject, this.context.getResources().getString(R.string.afternoon)));
                                holder.btnOrder4.setVisibility(0);
                                break;
                            }
                        } else {
                            holder.tvOrder4.setText(String.valueOf(this.context.getResources().getString(R.string.order_num)) + ":" + "0");
                            holder.tvOrder4.setVisibility(0);
                            holder.btnOrder4.setVisibility(0);
                            holder.btnOrder4.setBackgroundResource(R.drawable.ygkz_order_doctor_button_red);
                            holder.btnOrder4.setText(this.context.getResources().getString(R.string.stopping_cure));
                            holder.btnOrder4.setEnabled(false);
                            break;
                        }
                    } else {
                        holder.tvOrder1.setText(String.valueOf(this.context.getResources().getString(R.string.order_num)) + ":" + "0");
                        holder.tvOrder1.setVisibility(0);
                        holder.btnOrder1.setVisibility(0);
                        holder.btnOrder1.setBackgroundResource(R.drawable.ygkz_order_doctor_button_red);
                        holder.btnOrder1.setText(this.context.getResources().getString(R.string.stopping_cure));
                        holder.btnOrder1.setEnabled(false);
                        break;
                    }
                case 1:
                    holder.tvTime2.setText(DateUtil.replaceDate(json.optJSONArray("daysplan").optJSONObject(i).optString("day")));
                    int num3 = count(plan.optJSONObject(i).optJSONArray("planlist"), "上午");
                    if (!stop(plan.optJSONObject(i).optJSONArray("planlist"), this.context.getResources().getString(R.string.morning))) {
                        if (num3 > 0) {
                            holder.tvOrder2.setText(String.valueOf(this.context.getResources().getString(R.string.order_num)) + ":" + num3);
                            holder.tvOrder2.setVisibility(0);
                            holder.btnOrder2.setOnClickListener(new orderClickListener(jsonObject, this.context.getResources().getString(R.string.morning)));
                            holder.btnOrder2.setVisibility(0);
                        } else if (num3 == 0) {
                            holder.tvOrder2.setText(String.valueOf(this.context.getResources().getString(R.string.order_num)) + ":" + "0");
                            holder.tvOrder2.setVisibility(0);
                            holder.btnOrder2.setVisibility(0);
                            holder.btnOrder2.setText(this.context.getResources().getString(R.string.full));
                            holder.btnOrder2.setBackgroundResource(R.drawable.ygkz_order_doctor_button_dark);
                            holder.btnOrder2.setEnabled(false);
                        } else {
                            holder.tvOrder2.setText(PoiTypeDef.All);
                            holder.btnOrder2.setVisibility(8);
                        }
                        if (!stop(plan.optJSONObject(i).optJSONArray("planlist"), this.context.getResources().getString(R.string.afternoon))) {
                            int num4 = count(plan.optJSONObject(i).optJSONArray("planlist"), this.context.getResources().getString(R.string.afternoon));
                            if (num4 <= 0) {
                                if (num4 != 0) {
                                    holder.tvOrder5.setText(PoiTypeDef.All);
                                    holder.btnOrder5.setVisibility(8);
                                    break;
                                } else {
                                    holder.tvOrder5.setText(String.valueOf(this.context.getResources().getString(R.string.order_num)) + ":" + "0");
                                    holder.tvOrder5.setVisibility(0);
                                    holder.btnOrder5.setVisibility(0);
                                    holder.btnOrder5.setText(this.context.getResources().getString(R.string.full));
                                    holder.btnOrder5.setBackgroundResource(R.drawable.ygkz_order_doctor_button_dark);
                                    holder.btnOrder5.setEnabled(false);
                                    break;
                                }
                            } else {
                                holder.tvOrder5.setText(String.valueOf(this.context.getResources().getString(R.string.order_num)) + ":" + num4);
                                holder.tvOrder5.setVisibility(0);
                                holder.btnOrder5.setOnClickListener(new orderClickListener(jsonObject, this.context.getResources().getString(R.string.afternoon)));
                                holder.btnOrder5.setVisibility(0);
                                break;
                            }
                        } else {
                            holder.tvOrder5.setText(String.valueOf(this.context.getResources().getString(R.string.order_num)) + ":" + "0");
                            holder.tvOrder5.setVisibility(0);
                            holder.btnOrder5.setVisibility(0);
                            holder.btnOrder5.setBackgroundResource(R.drawable.ygkz_order_doctor_button_red);
                            holder.btnOrder5.setText(this.context.getResources().getString(R.string.stopping_cure));
                            holder.btnOrder5.setEnabled(false);
                            break;
                        }
                    } else {
                        holder.tvOrder2.setText(String.valueOf(this.context.getResources().getString(R.string.order_num)) + ":" + "0");
                        holder.tvOrder2.setVisibility(0);
                        holder.btnOrder2.setVisibility(0);
                        holder.btnOrder2.setBackgroundResource(R.drawable.ygkz_order_doctor_button_red);
                        holder.btnOrder2.setText(this.context.getResources().getString(R.string.stopping_cure));
                        holder.btnOrder2.setEnabled(false);
                        break;
                    }
                case 2:
                    holder.tvTime3.setText(DateUtil.replaceDate(json.optJSONArray("daysplan").optJSONObject(i).optString("day")));
                    if (!stop(plan.optJSONObject(i).optJSONArray("planlist"), this.context.getResources().getString(R.string.morning))) {
                        int num5 = count(plan.optJSONObject(i).optJSONArray("planlist"), "上午");
                        if (num5 > 0) {
                            holder.tvOrder3.setText(String.valueOf(this.context.getResources().getString(R.string.order_num)) + num5);
                            holder.tvOrder3.setVisibility(0);
                            holder.btnOrder3.setOnClickListener(new orderClickListener(jsonObject, this.context.getResources().getString(R.string.morning)));
                            holder.btnOrder3.setVisibility(0);
                        } else if (num5 == 0) {
                            holder.tvOrder3.setText(String.valueOf(this.context.getResources().getString(R.string.order_num)) + ":" + "0");
                            holder.tvOrder3.setVisibility(0);
                            holder.btnOrder3.setVisibility(0);
                            holder.btnOrder3.setText(this.context.getResources().getString(R.string.full));
                            holder.btnOrder3.setBackgroundResource(R.drawable.ygkz_order_doctor_button_dark);
                            holder.btnOrder3.setEnabled(false);
                        } else {
                            holder.tvOrder3.setText(PoiTypeDef.All);
                            holder.btnOrder3.setVisibility(8);
                        }
                        if (!stop(plan.optJSONObject(i).optJSONArray("planlist"), this.context.getResources().getString(R.string.afternoon))) {
                            int num6 = count(plan.optJSONObject(i).optJSONArray("planlist"), "下午");
                            if (num6 <= 0) {
                                if (num6 != 0) {
                                    holder.tvOrder6.setText(PoiTypeDef.All);
                                    holder.btnOrder6.setVisibility(8);
                                    break;
                                } else {
                                    holder.tvOrder6.setText(String.valueOf(this.context.getResources().getString(R.string.order_num)) + ":" + "0");
                                    holder.tvOrder6.setVisibility(0);
                                    holder.btnOrder6.setVisibility(0);
                                    holder.btnOrder6.setText(this.context.getResources().getString(R.string.full));
                                    holder.btnOrder6.setBackgroundResource(R.drawable.ygkz_order_doctor_button_dark);
                                    holder.btnOrder6.setEnabled(false);
                                    break;
                                }
                            } else {
                                holder.tvOrder6.setText(String.valueOf(this.context.getResources().getString(R.string.order_num)) + num6);
                                holder.tvOrder6.setVisibility(0);
                                holder.btnOrder6.setOnClickListener(new orderClickListener(jsonObject, this.context.getResources().getString(R.string.afternoon)));
                                holder.btnOrder6.setVisibility(0);
                                break;
                            }
                        } else {
                            holder.tvOrder6.setText(String.valueOf(this.context.getResources().getString(R.string.order_num)) + ":" + "0");
                            holder.tvOrder6.setVisibility(0);
                            holder.btnOrder6.setVisibility(0);
                            holder.btnOrder6.setBackgroundResource(R.drawable.ygkz_order_doctor_button_red);
                            holder.btnOrder6.setText(this.context.getResources().getString(R.string.stopping_cure));
                            holder.btnOrder6.setEnabled(false);
                            break;
                        }
                    } else {
                        holder.tvOrder3.setText(String.valueOf(this.context.getResources().getString(R.string.order_num)) + ":" + "0");
                        holder.tvOrder3.setVisibility(0);
                        holder.btnOrder3.setVisibility(0);
                        holder.btnOrder3.setBackgroundResource(R.drawable.ygkz_order_doctor_button_red);
                        holder.btnOrder3.setText(this.context.getResources().getString(R.string.stopping_cure));
                        holder.btnOrder3.setEnabled(false);
                        break;
                    }
            }
        }
        holder.tvDoctorName.setText(json.optString("doctorname"));
        holder.tvDoctorInfo.setText(json.optString("doctorlevel"));
        holder.tvOrderCount.setText(String.valueOf(this.context.getResources().getString(R.string.order_num)) + ":" + json.optInt("totalavailnum"));
        Button button = holder.btnDayNext;
        button.setOnClickListener(new PageClickListener(view2, json.optJSONArray("daysplan"), 1, position));
        Button button2 = holder.btnDayForward;
        button2.setOnClickListener(new PageClickListener(view2, json.optJSONArray("daysplan"), 0, position));
        return view2;
    }

    private class ViewHolder {
        Button btnDayForward;
        Button btnDayNext;
        Button btnFavDoctor;
        Button btnOrder1;
        Button btnOrder2;
        Button btnOrder3;
        Button btnOrder4;
        Button btnOrder5;
        Button btnOrder6;
        public ImageView ivDoctorPhoto;
        public ListView lvDoctorPlan;
        TextView tvDoctorInfo;
        TextView tvDoctorName;
        TextView tvDoctorSex;
        TextView tvOrder1;
        TextView tvOrder2;
        TextView tvOrder3;
        TextView tvOrder4;
        TextView tvOrder5;
        TextView tvOrder6;
        TextView tvOrderCount;
        TextView tvTime1;
        TextView tvTime2;
        TextView tvTime3;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(DoctorPlanAdatper doctorPlanAdatper, ViewHolder viewHolder) {
            this();
        }
    }

    /* access modifiers changed from: private */
    public int count(JSONArray jsons2, String type) {
        int tmpCount = 0;
        boolean flag = false;
        for (int i = 0; i < jsons2.length(); i++) {
            if (type.equals(jsons2.optJSONObject(i).optString("timespan"))) {
                flag = true;
                tmpCount += jsons2.optJSONObject(i).optInt("availnum");
            }
        }
        if (flag) {
            return tmpCount;
        }
        return -1;
    }

    /* access modifiers changed from: private */
    public boolean stop(JSONArray jsons2, String type) {
        for (int i = 0; i < jsons2.length(); i++) {
            if (type.equals(jsons2.optJSONObject(i).optString("timespan")) && jsons2.optJSONObject(i).optBoolean("isstop")) {
                return true;
            }
        }
        return false;
    }

    private class FavDoctorClickListener implements View.OnClickListener {
        private JSONObject doctor;
        private JSONObject json;
        private int positsion;
        private View view;

        public FavDoctorClickListener(View view2, JSONObject json2, int position) {
            this.json = json2;
            this.view = view2;
            this.positsion = position;
            createDoctor();
        }

        private void createDoctor() {
            this.doctor = new JSONObject();
            try {
                this.doctor.put("doctorid", this.json.optInt("doctorid"));
                this.doctor.put("doctorName", this.json.optString("doctorname"));
                this.doctor.put("hospital", this.json.optString("hospitalname"));
                this.doctor.put("level", this.json.optString("doctorlevel"));
                this.doctor.put("gender", this.json.optString("gender"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        public void onClick(View v) {
            UriParameter parameters = new UriParameter();
            JSONArray doctors = new JSONArray();
            doctors.put(this.doctor);
            parameters.add("userid", UserEntity.getInstance().getInfo().getAccId());
            parameters.add("doctors", doctors.toString());
            SelectDoctorActivity.favPosition = this.positsion;
            SelectDoctorActivity.doctorName = this.json.optString("doctorname");
            SelectDoctorActivity.pDialog.show();
            RequestTask.getInstance().sendHttpRequest((BaseRequestActivity) DoctorPlanAdatper.this.context, String.valueOf((int) HttpRequestParameters.ADD_FAVORITES_DOCTOR), parameters);
        }
    }

    private class PageClickListener implements View.OnClickListener {
        ViewHolder holder;
        private JSONArray jsons;
        private int type;
        private View view;

        public PageClickListener(View contentView, JSONArray jsons2, int type2, int position) {
            this.holder = new ViewHolder(DoctorPlanAdatper.this, null);
            this.view = (View) DoctorPlanAdatper.this.viewMap.get(Integer.valueOf(position));
            this.jsons = jsons2;
            this.type = type2;
            this.holder.tvOrder1 = (TextView) this.view.findViewById(R.id.tv_order_1);
            this.holder.tvOrder2 = (TextView) this.view.findViewById(R.id.tv_order_2);
            this.holder.tvOrder3 = (TextView) this.view.findViewById(R.id.tv_order_3);
            this.holder.tvOrder4 = (TextView) this.view.findViewById(R.id.tv_order_4);
            this.holder.tvOrder5 = (TextView) this.view.findViewById(R.id.tv_order_5);
            this.holder.tvOrder6 = (TextView) this.view.findViewById(R.id.tv_order_6);
            this.holder.btnOrder1 = (Button) this.view.findViewById(R.id.btn_order_1);
            this.holder.btnOrder2 = (Button) this.view.findViewById(R.id.btn_order_2);
            this.holder.btnOrder3 = (Button) this.view.findViewById(R.id.btn_order_3);
            this.holder.btnOrder4 = (Button) this.view.findViewById(R.id.btn_order_4);
            this.holder.btnOrder5 = (Button) this.view.findViewById(R.id.btn_order_5);
            this.holder.btnOrder6 = (Button) this.view.findViewById(R.id.btn_order_6);
            this.holder.tvTime1 = (TextView) this.view.findViewById(R.id.tv_time_1);
            this.holder.tvTime2 = (TextView) this.view.findViewById(R.id.tv_time_2);
            this.holder.tvTime3 = (TextView) this.view.findViewById(R.id.tv_time_3);
            this.holder.btnDayForward = (Button) this.view.findViewById(R.id.btn_day_forward);
            this.holder.btnDayNext = (Button) this.view.findViewById(R.id.btn_day_next);
        }

        public void onClick(View v) {
            setPlan();
        }

        private int indexTime() {
            String time1 = this.holder.tvTime1.getText().toString();
            String charSequence = this.holder.tvTime2.getText().toString();
            String time3 = this.holder.tvTime3.getText().toString();
            if (this.type == 0) {
                if (time1.length() > 0) {
                    for (int i = 0; i < this.jsons.length(); i++) {
                        if (time1.equals(DateUtil.replaceDate(this.jsons.optJSONObject(i).optString("day")))) {
                            return i;
                        }
                    }
                }
            } else if (time3.length() > 0) {
                for (int i2 = 0; i2 < this.jsons.length(); i2++) {
                    if (time3.equals(DateUtil.replaceDate(this.jsons.optJSONObject(i2).optString("day")))) {
                        return i2 + 1;
                    }
                }
            }
            return -1;
        }

        private void setData(int index, int i) {
            int flag;
            if (this.type != 0) {
                flag = i - index;
            } else if (i < 3) {
                flag = i;
            } else {
                flag = i - index;
            }
            switch (flag) {
                case 0:
                    this.holder.tvTime1.setVisibility(0);
                    this.holder.tvTime1.setText(DateUtil.replaceDate(this.jsons.optJSONObject(i).optString("day")));
                    if (DoctorPlanAdatper.this.stop(this.jsons.optJSONObject(i).optJSONArray("planlist"), DoctorPlanAdatper.this.context.getResources().getString(R.string.morning))) {
                        this.holder.tvOrder1.setText(String.valueOf(DoctorPlanAdatper.this.context.getResources().getString(R.string.order_num)) + ":" + "0");
                        this.holder.tvOrder1.setVisibility(0);
                        this.holder.btnOrder1.setVisibility(0);
                        this.holder.btnOrder1.setBackgroundResource(R.drawable.ygkz_order_doctor_button_red);
                        this.holder.btnOrder1.setText(DoctorPlanAdatper.this.context.getResources().getString(R.string.stopping_cure));
                        this.holder.btnOrder1.setEnabled(false);
                        return;
                    }
                    int num = DoctorPlanAdatper.this.count(this.jsons.optJSONObject(i).optJSONArray("planlist"), DoctorPlanAdatper.this.context.getResources().getString(R.string.morning));
                    this.holder.tvOrder1.setVisibility(0);
                    if (num > 0) {
                        this.holder.tvOrder1.setText(String.valueOf(DoctorPlanAdatper.this.context.getResources().getString(R.string.order_num)) + num);
                        this.holder.btnOrder1.setOnClickListener(new orderClickListener(this.jsons.optJSONObject(i), DoctorPlanAdatper.this.context.getResources().getString(R.string.morning)));
                        this.holder.btnOrder1.setVisibility(0);
                    } else if (num == 0) {
                        this.holder.tvOrder1.setVisibility(0);
                        this.holder.tvOrder1.setText(String.valueOf(DoctorPlanAdatper.this.context.getResources().getString(R.string.order_num)) + ":" + "0");
                        this.holder.btnOrder1.setVisibility(0);
                        this.holder.btnOrder1.setText(DoctorPlanAdatper.this.context.getResources().getString(R.string.full));
                        this.holder.btnOrder1.setBackgroundResource(R.drawable.ygkz_order_doctor_button_dark);
                        this.holder.btnOrder1.setEnabled(true);
                    } else {
                        this.holder.tvOrder1.setText(PoiTypeDef.All);
                        this.holder.btnOrder1.setVisibility(8);
                    }
                    if (DoctorPlanAdatper.this.stop(this.jsons.optJSONObject(i).optJSONArray("planlist"), DoctorPlanAdatper.this.context.getResources().getString(R.string.afternoon))) {
                        this.holder.tvOrder4.setText(String.valueOf(DoctorPlanAdatper.this.context.getResources().getString(R.string.order_num)) + ":" + "0");
                        this.holder.tvOrder4.setVisibility(0);
                        this.holder.btnOrder4.setVisibility(0);
                        this.holder.btnOrder4.setBackgroundResource(R.drawable.ygkz_order_doctor_button_red);
                        this.holder.btnOrder4.setText(DoctorPlanAdatper.this.context.getResources().getString(R.string.stopping_cure));
                        this.holder.btnOrder4.setEnabled(false);
                        return;
                    }
                    int num2 = DoctorPlanAdatper.this.count(this.jsons.optJSONObject(i).optJSONArray("planlist"), "下午");
                    this.holder.tvOrder4.setVisibility(0);
                    if (num2 > 0) {
                        this.holder.tvOrder4.setText(String.valueOf(DoctorPlanAdatper.this.context.getResources().getString(R.string.order_num)) + ":" + num2);
                        this.holder.btnOrder4.setOnClickListener(new orderClickListener(this.jsons.optJSONObject(i), DoctorPlanAdatper.this.context.getResources().getString(R.string.afternoon)));
                        this.holder.btnOrder4.setVisibility(0);
                        return;
                    } else if (num2 == 0) {
                        this.holder.tvOrder4.setText(String.valueOf(DoctorPlanAdatper.this.context.getResources().getString(R.string.order_num)) + ":" + "0");
                        this.holder.btnOrder4.setText(DoctorPlanAdatper.this.context.getResources().getString(R.string.full));
                        this.holder.btnOrder4.setVisibility(0);
                        this.holder.btnOrder4.setBackgroundResource(R.drawable.ygkz_order_doctor_button_dark);
                        this.holder.btnOrder4.setEnabled(true);
                        return;
                    } else {
                        this.holder.tvOrder4.setText(PoiTypeDef.All);
                        this.holder.btnOrder4.setVisibility(8);
                        return;
                    }
                case 1:
                    this.holder.tvTime2.setText(DateUtil.replaceDate(this.jsons.optJSONObject(i).optString("day")));
                    this.holder.tvTime2.setVisibility(0);
                    int num3 = DoctorPlanAdatper.this.count(this.jsons.optJSONObject(i).optJSONArray("planlist"), DoctorPlanAdatper.this.context.getResources().getString(R.string.morning));
                    if (DoctorPlanAdatper.this.stop(this.jsons.optJSONObject(i).optJSONArray("planlist"), DoctorPlanAdatper.this.context.getResources().getString(R.string.morning))) {
                        this.holder.tvOrder2.setText(String.valueOf(DoctorPlanAdatper.this.context.getResources().getString(R.string.order_num)) + ":" + "0");
                        this.holder.tvOrder2.setVisibility(0);
                        this.holder.btnOrder2.setVisibility(0);
                        this.holder.btnOrder2.setBackgroundResource(R.drawable.ygkz_order_doctor_button_red);
                        this.holder.btnOrder2.setText(DoctorPlanAdatper.this.context.getResources().getString(R.string.stopping_cure));
                        this.holder.btnOrder2.setEnabled(false);
                        return;
                    }
                    this.holder.tvOrder2.setVisibility(0);
                    if (num3 > 0) {
                        this.holder.tvOrder2.setText(String.valueOf(DoctorPlanAdatper.this.context.getResources().getString(R.string.order_num)) + ":" + num3);
                        this.holder.btnOrder2.setOnClickListener(new orderClickListener(this.jsons.optJSONObject(i), DoctorPlanAdatper.this.context.getResources().getString(R.string.morning)));
                        this.holder.btnOrder2.setVisibility(0);
                    } else if (num3 == 0) {
                        this.holder.tvOrder2.setText(String.valueOf(DoctorPlanAdatper.this.context.getResources().getString(R.string.order_num)) + ":" + "0");
                        this.holder.btnOrder2.setVisibility(0);
                        this.holder.btnOrder2.setText(DoctorPlanAdatper.this.context.getResources().getString(R.string.full));
                        this.holder.btnOrder2.setBackgroundResource(R.drawable.ygkz_order_doctor_button_dark);
                        this.holder.btnOrder2.setEnabled(true);
                    } else {
                        this.holder.tvOrder2.setText(PoiTypeDef.All);
                        this.holder.btnOrder2.setVisibility(8);
                    }
                    int num4 = DoctorPlanAdatper.this.count(this.jsons.optJSONObject(i).optJSONArray("planlist"), "下午");
                    this.holder.tvOrder5.setVisibility(0);
                    if (DoctorPlanAdatper.this.stop(this.jsons.optJSONObject(i).optJSONArray("planlist"), DoctorPlanAdatper.this.context.getResources().getString(R.string.afternoon))) {
                        this.holder.tvOrder5.setText(String.valueOf(DoctorPlanAdatper.this.context.getResources().getString(R.string.order_num)) + ":" + "0");
                        this.holder.tvOrder5.setVisibility(0);
                        this.holder.btnOrder5.setVisibility(0);
                        this.holder.btnOrder5.setBackgroundResource(R.drawable.ygkz_order_doctor_button_red);
                        this.holder.btnOrder5.setText(DoctorPlanAdatper.this.context.getResources().getString(R.string.stopping_cure));
                        this.holder.btnOrder5.setEnabled(false);
                        return;
                    }
                    this.holder.tvOrder5.setVisibility(0);
                    if (num4 > 0) {
                        this.holder.tvOrder5.setText(String.valueOf(DoctorPlanAdatper.this.context.getResources().getString(R.string.order_num)) + ":" + num4);
                        this.holder.btnOrder5.setOnClickListener(new orderClickListener(this.jsons.optJSONObject(i), DoctorPlanAdatper.this.context.getResources().getString(R.string.afternoon)));
                        this.holder.btnOrder5.setVisibility(0);
                        return;
                    } else if (num4 == 0) {
                        this.holder.tvOrder5.setText(String.valueOf(DoctorPlanAdatper.this.context.getResources().getString(R.string.order_num)) + ":" + "0");
                        this.holder.btnOrder5.setVisibility(0);
                        this.holder.btnOrder5.setText(DoctorPlanAdatper.this.context.getResources().getString(R.string.full));
                        this.holder.btnOrder5.setBackgroundResource(R.drawable.ygkz_order_doctor_button_dark);
                        this.holder.btnOrder5.setEnabled(true);
                        return;
                    } else {
                        this.holder.tvOrder5.setText(PoiTypeDef.All);
                        this.holder.btnOrder5.setVisibility(8);
                        return;
                    }
                case 2:
                    this.holder.tvTime3.setText(DateUtil.replaceDate(this.jsons.optJSONObject(i).optString("day")));
                    this.holder.tvTime3.setVisibility(0);
                    int num5 = DoctorPlanAdatper.this.count(this.jsons.optJSONObject(i).optJSONArray("planlist"), DoctorPlanAdatper.this.context.getResources().getString(R.string.morning));
                    if (DoctorPlanAdatper.this.stop(this.jsons.optJSONObject(i).optJSONArray("planlist"), DoctorPlanAdatper.this.context.getResources().getString(R.string.morning))) {
                        this.holder.tvOrder3.setText(String.valueOf(DoctorPlanAdatper.this.context.getResources().getString(R.string.order_num)) + ":" + "0");
                        this.holder.tvOrder3.setVisibility(0);
                        this.holder.btnOrder3.setVisibility(0);
                        this.holder.btnOrder3.setBackgroundResource(R.drawable.ygkz_order_doctor_button_red);
                        this.holder.btnOrder3.setText(DoctorPlanAdatper.this.context.getResources().getString(R.string.stopping_cure));
                        this.holder.btnOrder3.setEnabled(false);
                        return;
                    }
                    this.holder.tvOrder3.setVisibility(0);
                    if (num5 > 0) {
                        this.holder.tvOrder3.setText(String.valueOf(DoctorPlanAdatper.this.context.getResources().getString(R.string.order_num)) + ":" + num5);
                        this.holder.btnOrder3.setOnClickListener(new orderClickListener(this.jsons.optJSONObject(i), DoctorPlanAdatper.this.context.getResources().getString(R.string.morning)));
                        this.holder.btnOrder3.setVisibility(0);
                    } else if (num5 == 0) {
                        this.holder.tvOrder3.setText(String.valueOf(DoctorPlanAdatper.this.context.getResources().getString(R.string.order_num)) + ":" + "0");
                        this.holder.btnOrder3.setVisibility(0);
                        this.holder.btnOrder3.setText(DoctorPlanAdatper.this.context.getResources().getString(R.string.full));
                        this.holder.btnOrder3.setBackgroundResource(R.drawable.ygkz_order_doctor_button_dark);
                        this.holder.btnOrder3.setEnabled(true);
                    } else {
                        this.holder.tvOrder3.setText(PoiTypeDef.All);
                        this.holder.btnOrder3.setVisibility(8);
                    }
                    int num6 = DoctorPlanAdatper.this.count(this.jsons.optJSONObject(i).optJSONArray("planlist"), "下午");
                    if (DoctorPlanAdatper.this.stop(this.jsons.optJSONObject(i).optJSONArray("planlist"), DoctorPlanAdatper.this.context.getResources().getString(R.string.afternoon))) {
                        this.holder.tvOrder6.setText(String.valueOf(DoctorPlanAdatper.this.context.getResources().getString(R.string.order_num)) + ":" + "0");
                        this.holder.tvOrder6.setVisibility(0);
                        this.holder.btnOrder6.setVisibility(0);
                        this.holder.btnOrder6.setBackgroundResource(R.drawable.ygkz_order_doctor_button_red);
                        this.holder.btnOrder6.setText(DoctorPlanAdatper.this.context.getResources().getString(R.string.stopping_cure));
                        this.holder.btnOrder6.setEnabled(false);
                        return;
                    }
                    this.holder.tvOrder6.setVisibility(0);
                    if (num6 > 0) {
                        this.holder.tvOrder6.setText(String.valueOf(DoctorPlanAdatper.this.context.getResources().getString(R.string.order_num)) + ":" + num6);
                        this.holder.btnOrder6.setOnClickListener(new orderClickListener(this.jsons.optJSONObject(i), DoctorPlanAdatper.this.context.getResources().getString(R.string.afternoon)));
                        this.holder.btnOrder6.setVisibility(0);
                        return;
                    } else if (num6 == 0) {
                        this.holder.tvOrder6.setText(String.valueOf(DoctorPlanAdatper.this.context.getResources().getString(R.string.order_num)) + ":" + "0");
                        this.holder.btnOrder6.setVisibility(0);
                        this.holder.btnOrder6.setText(DoctorPlanAdatper.this.context.getResources().getString(R.string.full));
                        this.holder.btnOrder6.setBackgroundResource(R.drawable.ygkz_order_doctor_button_dark);
                        this.holder.btnOrder6.setEnabled(true);
                        return;
                    } else {
                        this.holder.tvOrder6.setText(PoiTypeDef.All);
                        this.holder.btnOrder6.setVisibility(8);
                        return;
                    }
                default:
                    return;
            }
        }

        private void initData() {
            this.holder.tvOrder1.setVisibility(8);
            this.holder.tvOrder2.setVisibility(8);
            this.holder.tvOrder3.setVisibility(8);
            this.holder.tvOrder4.setVisibility(8);
            this.holder.tvOrder5.setVisibility(8);
            this.holder.tvOrder6.setVisibility(8);
            this.holder.btnOrder1.setVisibility(8);
            this.holder.btnOrder2.setVisibility(8);
            this.holder.btnOrder3.setVisibility(8);
            this.holder.btnOrder4.setVisibility(8);
            this.holder.btnOrder5.setVisibility(8);
            this.holder.btnOrder6.setVisibility(8);
            this.holder.tvTime1.setVisibility(8);
            this.holder.tvTime2.setVisibility(8);
            this.holder.tvTime3.setVisibility(8);
        }

        private void setPlan() {
            int index = indexTime();
            if (index != -1) {
                initData();
                if (this.type == 0) {
                    if (index - 3 <= 0) {
                        this.holder.btnDayForward.setBackgroundResource(R.drawable.yl_button_doctor_day_forward_none_ex_big);
                        this.holder.btnDayForward.setEnabled(false);
                    } else {
                        this.holder.btnDayForward.setBackgroundResource(R.drawable.day_forward_selector);
                        this.holder.btnDayForward.setEnabled(true);
                    }
                    this.holder.btnDayNext.setBackgroundResource(R.drawable.day_next_selector);
                    this.holder.btnDayNext.setVisibility(0);
                    this.holder.btnDayNext.setEnabled(true);
                    for (int i = index; i >= index - 3; i--) {
                        if (i >= 0) {
                            setData(index - 3, i);
                        }
                    }
                    return;
                }
                this.holder.btnDayForward.setBackgroundResource(R.drawable.day_forward_selector);
                this.holder.btnDayForward.setEnabled(true);
                if (index + 3 > this.jsons.length()) {
                    this.holder.btnDayNext.setBackgroundResource(R.drawable.yl_button_doctor_day_next_none_ex_big);
                    this.holder.btnDayNext.setEnabled(false);
                }
                for (int i2 = index; i2 < index + 3; i2++) {
                    if (i2 < this.jsons.length()) {
                        setData(index, i2);
                    }
                }
            }
        }
    }

    private class orderClickListener implements View.OnClickListener {
        private JSONObject jsonObject;
        private String type;

        public orderClickListener(JSONObject jsonObject2, String type2) {
            this.jsonObject = jsonObject2;
            this.type = type2;
        }

        public void onClick(View v) {
            Intent intent = new Intent(DoctorPlanAdatper.this.context, SelectClinicTimeActivity.class);
            intent.putExtra("jsons", this.jsonObject.toString());
            intent.putExtra(a.b, this.type);
            DoctorPlanAdatper.this.context.startActivity(intent);
        }
    }

    private void initHolder(ViewHolder holder) {
        holder.tvOrder1.setVisibility(8);
        holder.tvOrder2.setVisibility(8);
        holder.tvOrder3.setVisibility(8);
        holder.tvOrder4.setVisibility(8);
        holder.tvOrder5.setVisibility(8);
        holder.tvOrder6.setVisibility(8);
        holder.btnOrder1.setVisibility(8);
        holder.btnOrder2.setVisibility(8);
        holder.btnOrder3.setVisibility(8);
        holder.btnOrder4.setVisibility(8);
        holder.btnOrder5.setVisibility(8);
        holder.btnOrder6.setVisibility(8);
    }
}
