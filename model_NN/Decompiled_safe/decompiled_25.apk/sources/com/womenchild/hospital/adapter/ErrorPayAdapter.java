package com.womenchild.hospital.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.womenchild.hospital.R;
import com.womenchild.hospital.entity.ErrorPayModel;
import java.util.ArrayList;

public class ErrorPayAdapter extends BaseAdapter {
    private ArrayList<ErrorPayModel> arrayErrorPayModels;
    private Context context;

    public ErrorPayAdapter(Context context2, ArrayList<ErrorPayModel> arrayErrorPayModels2) {
        this.context = context2;
        this.arrayErrorPayModels = arrayErrorPayModels2;
    }

    public int getCount() {
        return this.arrayErrorPayModels.size();
    }

    public Object getItem(int position) {
        return this.arrayErrorPayModels.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder(this, null);
            convertView = LayoutInflater.from(this.context).inflate((int) R.layout.errorpay_item, (ViewGroup) null);
            holder.txt_paytime = (TextView) convertView.findViewById(R.id.txt_paytime_value);
            holder.txt_ylordernum = (TextView) convertView.findViewById(R.id.txt_ylordernum_value);
            holder.txt_price = (TextView) convertView.findViewById(R.id.txt_price_value);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        ErrorPayModel model = this.arrayErrorPayModels.get(position);
        holder.txt_paytime.setText(model.getPaytime());
        holder.txt_ylordernum.setText(model.getYlordernum());
        holder.txt_price.setText(Double.valueOf((double) model.getPrice()) + "元");
        return convertView;
    }

    public void setArrayErrorPayModel(ArrayList<ErrorPayModel> array) {
        this.arrayErrorPayModels = array;
    }

    private class ViewHolder {
        TextView txt_paytime;
        TextView txt_price;
        TextView txt_ylordernum;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(ErrorPayAdapter errorPayAdapter, ViewHolder viewHolder) {
            this();
        }
    }
}
