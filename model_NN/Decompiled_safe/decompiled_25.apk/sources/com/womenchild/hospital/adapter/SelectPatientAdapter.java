package com.womenchild.hospital.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.womenchild.hospital.R;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class SelectPatientAdapter extends BaseAdapter {
    private static int selectedId = -1;
    private Context context;
    private JSONArray jsons;
    private Map<Integer, View> viewMap = new HashMap();

    public SelectPatientAdapter(Context context2, JSONArray jsons2) {
        this.context = context2;
        this.jsons = jsons2;
    }

    public int getCount() {
        return this.jsons.length();
    }

    public Object getItem(int position) {
        return this.jsons.optJSONObject(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View convertView2;
        ViewHolder viewHolder;
        JSONObject json = this.jsons.optJSONObject(position);
        if (this.viewMap.get(Integer.valueOf(position)) == null) {
            convertView2 = View.inflate(this.context, R.layout.select_patient_listview_item, null);
            viewHolder = new ViewHolder(null);
            viewHolder.name = (TextView) convertView2.findViewById(R.id.tv_patient_name);
            convertView2.setTag(viewHolder);
            this.viewMap.put(Integer.valueOf(position), convertView2);
        } else {
            convertView2 = this.viewMap.get(Integer.valueOf(position));
            viewHolder = (ViewHolder) convertView2.getTag();
        }
        viewHolder.name.setText(json.optString("patientname"));
        return convertView2;
    }

    public static int getSelectedId() {
        return selectedId;
    }

    public static void setSelectedId(int id) {
        selectedId = id;
    }

    private static class ViewHolder {
        TextView name;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(ViewHolder viewHolder) {
            this();
        }
    }
}
