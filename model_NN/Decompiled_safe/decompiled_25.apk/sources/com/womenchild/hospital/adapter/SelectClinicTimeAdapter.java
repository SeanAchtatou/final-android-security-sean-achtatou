package com.womenchild.hospital.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import com.womenchild.hospital.R;
import com.womenchild.hospital.util.DateUtil;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class SelectClinicTimeAdapter extends BaseAdapter {
    /* access modifiers changed from: private */
    public static int selectedId = -1;
    private Context context;
    private JSONArray jsons;
    /* access modifiers changed from: private */
    public Map<Integer, View> viewMap = new HashMap();

    public SelectClinicTimeAdapter(Context context2, JSONArray jsons2) {
        this.context = context2;
        this.jsons = jsons2;
    }

    public int getCount() {
        return this.jsons.length();
    }

    public Object getItem(int position) {
        return this.jsons.optJSONObject(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        JSONObject json = this.jsons.optJSONObject(position);
        if (convertView == null) {
            convertView = View.inflate(this.context, R.layout.select_clinic_time_listview_item, null);
            viewHolder = new ViewHolder(this, null);
            viewHolder.tvClinicTime = (TextView) convertView.findViewById(R.id.tv_clinic_time);
            viewHolder.tvPrice = (TextView) convertView.findViewById(R.id.tv_price);
            viewHolder.tvFull = (TextView) convertView.findViewById(R.id.tv_full);
            viewHolder.ibtnIsSelect = (ImageButton) convertView.findViewById(R.id.ibtn_select_clinic_time);
            convertView.setTag(viewHolder);
            this.viewMap.put(Integer.valueOf(position), convertView);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tvClinicTime.setText(String.valueOf(DateUtil.getTimeFromLong(json.optLong("start"), "kk:mm")) + "-" + DateUtil.getTimeFromLong(json.optLong("stop"), "kk:mm"));
        if (json.optInt("availnum") == 0) {
            viewHolder.tvFull.setVisibility(0);
            viewHolder.ibtnIsSelect.setVisibility(8);
        } else {
            viewHolder.tvFull.setVisibility(8);
            viewHolder.ibtnIsSelect.setVisibility(0);
        }
        viewHolder.ibtnIsSelect.setOnClickListener(new isSelectClickListener(position));
        viewHolder.tvPrice.setText("挂号费" + (((double) json.optInt("fee")) / 100.0d) + "元");
        return convertView;
    }

    private class isSelectClickListener implements View.OnClickListener {
        int position;

        public isSelectClickListener(int position2) {
            this.position = position2;
        }

        public void onClick(View v) {
            if (SelectClinicTimeAdapter.selectedId != -1) {
                ((ViewHolder) ((View) SelectClinicTimeAdapter.this.viewMap.get(Integer.valueOf(SelectClinicTimeAdapter.selectedId))).getTag()).ibtnIsSelect.setBackgroundResource(R.drawable.ygkz_patient_icon_unselect);
            }
            ((ViewHolder) ((View) SelectClinicTimeAdapter.this.viewMap.get(Integer.valueOf(this.position))).getTag()).ibtnIsSelect.setBackgroundResource(R.drawable.ygkz_patient_icon_select);
            SelectClinicTimeAdapter.selectedId = this.position;
        }
    }

    public static void setSelectId(int id) {
        selectedId = id;
    }

    public static int getSelectId() {
        return selectedId;
    }

    private class ViewHolder {
        ImageButton ibtnIsSelect;
        TextView tvClinicTime;
        TextView tvFull;
        TextView tvPrice;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(SelectClinicTimeAdapter selectClinicTimeAdapter, ViewHolder viewHolder) {
            this();
        }
    }
}
