package com.womenchild.hospital.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.womenchild.hospital.R;
import com.womenchild.hospital.entity.TestPrjEntity;
import java.util.ArrayList;

public class TestReportAdapter extends BaseAdapter {
    private static final String TAG = "TestReportAdapter";
    private Context context;
    ListView list;
    private LayoutInflater mInflater;
    private ArrayList<TestPrjEntity> testPrjEntityList = new ArrayList<>();

    public TestReportAdapter(Context context2, ListView list2, ArrayList<TestPrjEntity> testPrjEntitylist) {
        this.list = list2;
        this.context = context2;
        this.testPrjEntityList = testPrjEntitylist;
        this.mInflater = LayoutInflater.from(context2);
    }

    public void setList(ArrayList<TestPrjEntity> testPrjEntitylist) {
        this.testPrjEntityList = testPrjEntitylist;
    }

    public int getCount() {
        return this.testPrjEntityList.size();
    }

    public Object getItem(int position) {
        return this.testPrjEntityList.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        TestPrjEntity mTestPrjEntity = this.testPrjEntityList.get(position);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = this.mInflater.inflate((int) R.layout.test_report_lv_item, (ViewGroup) null);
            holder.name = (TextView) convertView.findViewById(R.id.tv_first_test_pjt_name);
            holder.uploadResult = (TextView) convertView.findViewById(R.id.tv_first_test_result_name);
            holder.uploadDate = (TextView) convertView.findViewById(R.id.tv_first_test_date);
            holder.subIcon = (TextView) convertView.findViewById(R.id.tv_test_sub);
            holder.clinicNameTv = (TextView) convertView.findViewById(R.id.tv_first_test_clinic_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.name.setText(mTestPrjEntity.getProjectName());
        if (mTestPrjEntity.isUpload()) {
            holder.uploadResult.setText(mTestPrjEntity.getPatientName());
            holder.uploadDate.setText(mTestPrjEntity.getUpdateDate());
            holder.clinicNameTv.setText(mTestPrjEntity.getOrganName());
        } else {
            holder.uploadResult.setText((int) R.string.tips_not_upload);
            holder.uploadDate.setText(String.valueOf((int) R.string.tips_upload_date) + mTestPrjEntity.getUpdateDate());
            holder.subIcon.setVisibility(4);
        }
        return convertView;
    }

    class ViewHolder {
        TextView clinicNameTv;
        TextView name;
        TextView subIcon;
        TextView uploadDate;
        TextView uploadResult;

        ViewHolder() {
        }
    }
}
