package com.womenchild.hospital.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.womenchild.hospital.R;
import com.womenchild.hospital.entity.PatientCardEntity;
import java.util.List;

public class SelectDefaultPatientCardAdapter extends BaseAdapter {
    private static String TAG = "SelectDefaultPatientCardAdapter";
    private boolean firstRun;
    private Context mContext;
    private LayoutInflater mInflater;
    private List<PatientCardEntity> patientCardList;
    /* access modifiers changed from: private */
    public int positions = -1;

    public SelectDefaultPatientCardAdapter(Context context, List<PatientCardEntity> patientCardList2) {
        this.patientCardList = patientCardList2;
        this.mInflater = LayoutInflater.from(context);
        this.mContext = context;
        this.firstRun = true;
    }

    public int getCount() {
        return this.patientCardList.size();
    }

    public Object getItem(int position) {
        return this.patientCardList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public int getPositions() {
        return this.positions;
    }

    public void setPositions(int positions2) {
        this.positions = positions2;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        PatientCardEntity selectCard = this.patientCardList.get(position);
        ViewHolder holder = new ViewHolder(this, null);
        View convertView2 = this.mInflater.inflate((int) R.layout.sel_default_card_lv_item, (ViewGroup) null);
        holder.tvTitle = (TextView) convertView2.findViewById(R.id.tv_card_title);
        holder.tvNum = (TextView) convertView2.findViewById(R.id.tv_card_num);
        holder.iv_arrow = (ImageView) convertView2.findViewById(R.id.iv_arrow);
        convertView2.setTag(holder);
        if (selectCard != null) {
            setUIData(holder, selectCard, position);
        }
        return convertView2;
    }

    private void setUIData(ViewHolder holder, PatientCardEntity selectCard, final int position) {
        holder.tvTitle.setText(String.valueOf(this.mContext.getString(R.string.tips_patient_card)) + (position + 1) + " :");
        holder.tvNum.setText(selectCard.getCard());
        holder.tvNum.setSelected(true);
        if (this.positions == position) {
            holder.iv_arrow.setBackgroundResource(R.drawable.ygkz_patient_icon_select);
        } else {
            holder.iv_arrow.setBackgroundResource(R.drawable.ygkz_patient_icon_unselect);
        }
        Log.d(TAG, "firstRun:" + this.firstRun + " selectCard.getPatientCardDefault()" + selectCard.getPatientCardDefault());
        if (this.firstRun && selectCard.getPatientCardDefault() == 1) {
            holder.iv_arrow.setBackgroundResource(R.drawable.ygkz_patient_icon_select);
            this.firstRun = false;
        }
        holder.iv_arrow.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SelectDefaultPatientCardAdapter.this.positions = position;
                SelectDefaultPatientCardAdapter.this.notifyDataSetChanged();
            }
        });
    }

    private class ViewHolder {
        /* access modifiers changed from: private */
        public ImageView iv_arrow;
        public TextView tvNum;
        public TextView tvTitle;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(SelectDefaultPatientCardAdapter selectDefaultPatientCardAdapter, ViewHolder viewHolder) {
            this();
        }
    }
}
