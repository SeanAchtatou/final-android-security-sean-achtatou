package com.womenchild.hospital.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.R;
import org.json.JSONArray;
import org.json.JSONObject;

public class StopPlanAdapter extends BaseAdapter {
    private Context context = null;
    private JSONArray jsons = new JSONArray();

    public StopPlanAdapter(Context context2, JSONArray jsonArray) {
        this.jsons = jsonArray;
        this.context = context2;
    }

    private class ViewHolder {
        public TextView tv_content;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(StopPlanAdapter stopPlanAdapter, ViewHolder viewHolder) {
            this();
        }
    }

    public int getCount() {
        return this.jsons.length();
    }

    public Object getItem(int arg0) {
        return this.jsons.opt(arg0);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        JSONObject json = this.jsons.optJSONObject(position);
        if (json != null) {
            convertView = LayoutInflater.from(this.context).inflate((int) R.layout.stop_plan_item, (ViewGroup) null);
            holder = new ViewHolder(this, null);
            holder.tv_content = (TextView) convertView.findViewById(R.id.tv_content);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.tv_content.setText(json.optString("title").replace("暨南大学附属第一医院", PoiTypeDef.All));
        return convertView;
    }
}
