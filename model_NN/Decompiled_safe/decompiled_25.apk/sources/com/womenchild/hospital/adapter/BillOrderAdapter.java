package com.womenchild.hospital.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.womenchild.hospital.R;
import java.util.HashMap;
import org.json.JSONArray;

public class BillOrderAdapter extends BaseExpandableListAdapter {
    private JSONArray billOrders;
    private HashMap<Integer, View> childMap = new HashMap<>();
    private Context context;
    private HashMap<Integer, View> map = new HashMap<>();

    public BillOrderAdapter(Context context2, JSONArray billOrders2) {
        this.context = context2;
        this.billOrders = billOrders2;
    }

    public Object getChild(int arg0, int arg1) {
        return this.billOrders.optJSONObject(arg0).optJSONArray("items").optJSONObject(arg1);
    }

    public long getChildId(int arg0, int arg1) {
        return (long) arg1;
    }

    public View getChildView(int arg0, int arg1, boolean arg2, View arg3, ViewGroup arg4) {
        View arg32;
        ItemViewHolder holder;
        if (this.childMap.get(Integer.valueOf(arg1)) == null) {
            holder = new ItemViewHolder(this, null);
            arg32 = LayoutInflater.from(this.context).inflate((int) R.layout.bill_item, (ViewGroup) null);
            holder.btnOrder = (Button) arg32.findViewById(R.id.btn_order);
            holder.tvTitle = (TextView) arg32.findViewById(R.id.tv_title);
            arg32.setTag(holder);
            this.childMap.put(Integer.valueOf(arg1), arg32);
        } else {
            arg32 = this.childMap.get(Integer.valueOf(arg1));
            holder = (ItemViewHolder) arg32.getTag();
        }
        holder.tvTitle.setText(String.valueOf(this.billOrders.optJSONObject(arg0).optJSONArray("items").optJSONObject(arg1).optString("itemName")) + " ¥" + (this.billOrders.optJSONObject(arg0).optJSONArray("items").optJSONObject(arg1).optDouble("itemSelfFee") / 100.0d) + "元");
        holder.btnOrder.setOnClickListener(new orderListener(this.billOrders.optJSONObject(arg0).optJSONArray("items").optJSONObject(arg1).toString()));
        return arg32;
    }

    public int getChildrenCount(int arg0) {
        return this.billOrders.optJSONObject(arg0).optJSONArray("items").length();
    }

    public Object getGroup(int arg0) {
        return this.billOrders.opt(arg0);
    }

    public int getGroupCount() {
        return this.billOrders.length();
    }

    public long getGroupId(int arg0) {
        return (long) arg0;
    }

    public View getGroupView(int arg0, boolean arg1, View arg2, ViewGroup arg3) {
        View arg22;
        BillViewHolder holder;
        if (this.map.get(Integer.valueOf(arg0)) == null) {
            arg22 = LayoutInflater.from(this.context).inflate((int) R.layout.bill_order_item, (ViewGroup) null);
            holder = new BillViewHolder(this, null);
            holder.tvBillName = (TextView) arg22.findViewById(R.id.tv_bill_name);
            holder.ivArrow = (ImageView) arg22.findViewById(R.id.iv_arrow);
            arg22.setTag(holder);
            this.map.put(Integer.valueOf(arg0), arg22);
        } else {
            arg22 = this.map.get(Integer.valueOf(arg0));
            holder = (BillViewHolder) arg22.getTag();
        }
        if (arg1) {
            holder.ivArrow.setBackgroundResource(R.drawable.ygkz_main_list_arrow_open);
        } else {
            holder.ivArrow.setBackgroundResource(R.drawable.ygkz_main_list_arrow);
        }
        holder.tvBillName.setText(String.valueOf(this.billOrders.optJSONObject(arg0).optString("orderTypeName")) + " ¥" + (this.billOrders.optJSONObject(arg0).optDouble("billSelfFee") / 100.0d) + "元");
        return arg22;
    }

    public boolean hasStableIds() {
        return false;
    }

    public boolean isChildSelectable(int arg0, int arg1) {
        return true;
    }

    private class orderListener implements View.OnClickListener {
        String itemDetail;

        public orderListener(String itemDetail2) {
            this.itemDetail = itemDetail2;
        }

        public void onClick(View v) {
        }
    }

    private class BillViewHolder {
        ImageView ivArrow;
        TextView tvBillName;

        private BillViewHolder() {
        }

        /* synthetic */ BillViewHolder(BillOrderAdapter billOrderAdapter, BillViewHolder billViewHolder) {
            this();
        }
    }

    private class ItemViewHolder {
        Button btnOrder;
        TextView tvTitle;

        private ItemViewHolder() {
        }

        /* synthetic */ ItemViewHolder(BillOrderAdapter billOrderAdapter, ItemViewHolder itemViewHolder) {
            this();
        }
    }
}
