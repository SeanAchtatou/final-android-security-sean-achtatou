package com.womenchild.hospital.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.DoctorCommentActivity;
import com.womenchild.hospital.DoctorListOfDeptActivity;
import com.womenchild.hospital.HomeActivity;
import com.womenchild.hospital.LoginActivity;
import com.womenchild.hospital.R;
import com.womenchild.hospital.RegisterActivity;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class DoctorListAdatper extends BaseAdapter {
    private String TAG = "DoctorPlanAdatper";
    private String comment;
    /* access modifiers changed from: private */
    public Context context = null;
    private int h;
    private JSONArray jsons = new JSONArray();
    private Map<Integer, View> viewMap = new HashMap();
    private int w;

    public DoctorListAdatper(Context context2, JSONArray jsonArray) {
        this.context = context2;
        this.jsons = jsonArray;
        Drawable drawable = context2.getResources().getDrawable(R.drawable.yl_ic_doctor_photo_ex);
        this.w = drawable.getIntrinsicWidth();
        this.h = drawable.getIntrinsicHeight();
    }

    public int getCount() {
        return this.jsons.length();
    }

    public Object getItem(int arg0) {
        return this.jsons.opt(arg0);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View view, ViewGroup parent) {
        JSONObject json = this.jsons.optJSONObject(position);
        View view2 = LayoutInflater.from(this.context).inflate((int) R.layout.doctor_list_item, (ViewGroup) null);
        ViewHolder holder = new ViewHolder(this, null);
        holder.tvDoctorName = (TextView) view2.findViewById(R.id.tv_doctor_name);
        holder.tvDoctorInfo = (TextView) view2.findViewById(R.id.tv_doctor_info);
        holder.ivDoctorPhoto = (ImageView) view2.findViewById(R.id.iv_doctor_photo);
        holder.tvDoctorSex = (TextView) view2.findViewById(R.id.tv_doctor_sex);
        holder.btnFavDoctor = (Button) view2.findViewById(R.id.btn_favdoctor);
        holder.tv_about = (TextView) view2.findViewById(R.id.tv_doctor_about);
        view2.setTag(holder);
        this.viewMap.put(Integer.valueOf(position), view2);
        if ("0".equals(json.optString("gender"))) {
            holder.tvDoctorSex.setText(this.context.getResources().getString(R.string.woman));
        } else if ("1".equals(json.optString("gender"))) {
            holder.tvDoctorSex.setText(this.context.getResources().getString(R.string.man));
        } else {
            holder.tvDoctorSex.setText(this.context.getResources().getString(R.string.sex_unk));
        }
        holder.btnFavDoctor.setVisibility(0);
        String doctorid = json.optString("doctorid");
        String doctorName = json.optString("doctorname");
        holder.tvDoctorName.setText(doctorName);
        holder.btnFavDoctor.setOnClickListener(new CommentClickLister(doctorid, doctorName));
        holder.tvDoctorInfo.setText(json.optString("doctorlevel"));
        String photo = json.optString("photo");
        if (photo != null && !photo.equals(PoiTypeDef.All)) {
            holder.ivDoctorPhoto.setBackgroundDrawable(new BitmapDrawable(stringtoBitmap(photo)));
            holder.ivDoctorPhoto.setScaleType(ImageView.ScaleType.FIT_CENTER);
        }
        String about = json.optString("desc");
        if (about != null && !about.trim().equals(PoiTypeDef.All)) {
            about = getComment(about);
        }
        if (about == null || PoiTypeDef.All.equals(about)) {
            this.comment = "暂无简介";
        } else if (about.length() > 20) {
            this.comment = "简介：" + about.substring(0, 20) + "...";
        } else {
            this.comment = "简介：" + about;
        }
        holder.tv_about.setText(this.comment);
        return view2;
    }

    private String getComment(String comment2) {
        int end;
        StringBuffer sb = new StringBuffer(comment2);
        while (true) {
            int start = sb.indexOf("<");
            if (start != -1 && (end = sb.indexOf(">")) != -1) {
                sb.delete(start, end + 1);
            }
        }
        return sb.toString().replace("&nbsp;", " ");
    }

    public String getComment() {
        return this.comment;
    }

    public Bitmap stringtoBitmap(String string) {
        try {
            byte[] bitmapArray = Base64.decode(string, 0);
            return BitmapFactory.decodeByteArray(bitmapArray, 0, bitmapArray.length);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private class ViewHolder {
        Button btnFavDoctor;
        public ImageView ivDoctorPhoto;
        public ListView lvDoctorPlan;
        TextView tvDoctorInfo;
        TextView tvDoctorName;
        TextView tvDoctorSex;
        TextView tv_about;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(DoctorListAdatper doctorListAdatper, ViewHolder viewHolder) {
            this();
        }
    }

    private class CommentClickLister implements View.OnClickListener {
        private String doctorName;
        private String doctorid;

        public CommentClickLister(String doctorid2, String doctorName2) {
            this.doctorid = doctorid2;
            this.doctorName = doctorName2;
        }

        public void onClick(View v) {
            if (HomeActivity.loginFlag) {
                Intent intent = new Intent(DoctorListAdatper.this.context, DoctorCommentActivity.class);
                intent.putExtra("doctorid", this.doctorid);
                intent.putExtra("doctorName", this.doctorName);
                intent.putExtra("deptName", ((DoctorListOfDeptActivity) DoctorListAdatper.this.context).deptName);
                intent.putExtra("deptid", ((DoctorListOfDeptActivity) DoctorListAdatper.this.context).deptId);
                DoctorListAdatper.this.context.startActivity(intent);
                return;
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(DoctorListAdatper.this.context);
            builder.setTitle("登录提示");
            builder.setMessage("使用此功能前请您先登录！若无帐号，请先注册");
            builder.setPositiveButton("免费注册", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    DoctorListAdatper.this.context.startActivity(new Intent(DoctorListAdatper.this.context, RegisterActivity.class));
                }
            });
            builder.setNegativeButton("登录", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    DoctorListAdatper.this.context.startActivity(new Intent(DoctorListAdatper.this.context, LoginActivity.class));
                }
            });
            builder.create().show();
        }
    }
}
