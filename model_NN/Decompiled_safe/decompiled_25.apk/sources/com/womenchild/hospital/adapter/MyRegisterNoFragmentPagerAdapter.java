package com.womenchild.hospital.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import java.util.ArrayList;

public class MyRegisterNoFragmentPagerAdapter extends FragmentPagerAdapter {
    private FragmentManager fm;
    private ArrayList<Fragment> fragmentsList;

    public MyRegisterNoFragmentPagerAdapter(FragmentManager fm2) {
        super(fm2);
        this.fm = fm2;
    }

    public MyRegisterNoFragmentPagerAdapter(FragmentManager fm2, ArrayList<Fragment> fragments) {
        super(fm2);
        this.fm = fm2;
        this.fragmentsList = fragments;
    }

    public Fragment getItem(int arg0) {
        return this.fragmentsList.get(arg0);
    }

    public int getCount() {
        return this.fragmentsList.size();
    }
}
