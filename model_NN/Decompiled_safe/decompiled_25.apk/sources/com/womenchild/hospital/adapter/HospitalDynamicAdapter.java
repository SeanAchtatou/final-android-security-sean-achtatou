package com.womenchild.hospital.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.womenchild.hospital.R;
import org.json.JSONArray;
import org.json.JSONObject;

public class HospitalDynamicAdapter extends BaseAdapter {
    private Context context = null;
    private JSONArray jsons = new JSONArray();

    public HospitalDynamicAdapter(Context context2, JSONArray jsonArray) {
        this.jsons = jsonArray;
        this.context = context2;
    }

    private class ViewHolder {
        public ImageView iv_pic;
        public TextView tv_title;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(HospitalDynamicAdapter hospitalDynamicAdapter, ViewHolder viewHolder) {
            this();
        }
    }

    public int getCount() {
        return this.jsons.length();
    }

    public Object getItem(int arg0) {
        return this.jsons.opt(arg0);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        JSONObject json = this.jsons.optJSONObject(position);
        if (json != null) {
            convertView = LayoutInflater.from(this.context).inflate((int) R.layout.hospital_dynamic_item, (ViewGroup) null);
            holder = new ViewHolder(this, null);
            holder.tv_title = (TextView) convertView.findViewById(R.id.tv_title);
            convertView.setTag(holder);
            holder.iv_pic = (ImageView) convertView.findViewById(R.id.iv_pic);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.tv_title.setText(json.optString("title"));
        return convertView;
    }
}
