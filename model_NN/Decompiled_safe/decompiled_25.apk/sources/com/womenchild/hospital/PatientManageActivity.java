package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.adapter.PatientManageAdapter;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.entity.PatientInfo;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.ClientLogUtil;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PatientManageActivity extends BaseRequestActivity implements View.OnClickListener {
    private static String TAG = "PatientManageActivity";
    private AdapterView.OnItemClickListener ITEM_LSN = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long arg3) {
            PatientInfo info = (PatientInfo) PatientManageActivity.this.listPatient.get(position);
            Intent intent = new Intent(PatientManageActivity.this.getContext(), PatientAddActivity.class);
            PatientManageActivity.this.is_update = true;
            intent.putExtra("is_update", PatientManageActivity.this.is_update);
            intent.putExtra("patientname", info.getPatientname());
            intent.putExtra("patientid", info.getPatientid());
            intent.putExtra("address", info.getAddress());
            intent.putExtra("idcard", info.getIdcard());
            intent.putExtra("mobile", info.getMobile());
            intent.putExtra("gender", info.getGender());
            intent.putExtra("socialsecuritycard", info.getSocialSecurityCard());
            intent.putExtra("healthycard", info.getHealthyCard());
            intent.putExtra("citizzencard", info.getCitizzencard());
            intent.putExtra("defaultpatientcard", info.getDefaultPatientCard());
            intent.putExtra("email", info.getmEmail());
            PatientManageActivity.this.startActivityForResult(intent, 297);
        }
    };
    private final int REFRESH = 0;
    public final int REQUEST_ADD = 297;
    /* access modifiers changed from: private */
    public PatientManageAdapter adapter;
    private PatientInfo defaultPatient = null;
    private RelativeLayout defaultPatientRl;
    private TextView defaultPatientTv;
    private boolean delete_stat;
    /* access modifiers changed from: private */
    public boolean is_update;
    private ImageView iv_add_patient;
    private Button iv_back;
    /* access modifiers changed from: private */
    public List<PatientInfo> listPatient = new ArrayList();
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    PatientManageActivity.this.adapter.notifyDataSetChanged();
                    break;
            }
            super.handleMessage(msg);
        }
    };
    private ListView mListView;
    private TextView otherPatientTipsTv;
    private TextView otherPatientTitleTv;
    private ProgressDialog pdDialog;
    private RelativeLayout rl_add_patient;
    private TextView tv_add_patient;
    private TextView tv_delete;
    private String userid;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.patient_manage);
        initViewId();
        initClickListener();
        initValues();
        this.adapter = new PatientManageAdapter(getContext(), this.listPatient, this);
        this.mListView.setAdapter((ListAdapter) this.adapter);
        this.pdDialog = new ProgressDialog(this);
        this.pdDialog.setMessage(getResources().getString(R.string.loading_data));
        this.pdDialog.show();
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.PATIENT_LIST), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.PATIENT_LIST)));
    }

    private void initValues() {
        this.userid = UserEntity.getInstance().getInfo().getUserid();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back /*2131296529*/:
                finish();
                return;
            case R.id.tv_delete /*2131296952*/:
                if (!this.delete_stat) {
                    this.tv_delete.setText((int) R.string.tips_cacel);
                    this.delete_stat = true;
                    this.adapter.setDelete_stat(this.delete_stat);
                    this.mHandler.sendMessage(this.mHandler.obtainMessage(0));
                    this.rl_add_patient.setVisibility(8);
                    return;
                }
                this.delete_stat = false;
                this.tv_delete.setText((int) R.string.tips_del);
                this.adapter.setDelete_stat(this.delete_stat);
                this.mHandler.sendMessage(this.mHandler.obtainMessage(0));
                if (this.listPatient.size() < 2) {
                    this.iv_add_patient.setVisibility(0);
                    this.tv_add_patient.setText((int) R.string.tips_add_new_patient);
                    this.rl_add_patient.setVisibility(0);
                    return;
                }
                return;
            case R.id.layout_default_patient /*2131296954*/:
                if (this.defaultPatient != null) {
                    Intent intentDefault = new Intent(getContext(), PatientAddActivity.class);
                    this.is_update = true;
                    intentDefault.putExtra("is_update", this.is_update);
                    intentDefault.putExtra("patientname", this.defaultPatient.getPatientname());
                    intentDefault.putExtra("patientid", this.defaultPatient.getPatientid());
                    intentDefault.putExtra("address", this.defaultPatient.getAddress());
                    intentDefault.putExtra("idcard", this.defaultPatient.getIdcard());
                    intentDefault.putExtra("mobile", this.defaultPatient.getMobile());
                    intentDefault.putExtra("gender", this.defaultPatient.getGender());
                    intentDefault.putExtra("socialsecuritycard", this.defaultPatient.getSocialSecurityCard());
                    intentDefault.putExtra("healthycard", this.defaultPatient.getHealthyCard());
                    intentDefault.putExtra("citizzencard", this.defaultPatient.getCitizzencard());
                    intentDefault.putExtra("defaultpatientcard", this.defaultPatient.getDefaultPatientCard());
                    intentDefault.putExtra("email", this.defaultPatient.getmEmail());
                    startActivityForResult(intentDefault, 297);
                    return;
                }
                return;
            case R.id.rl_add_patient /*2131296960*/:
                if (this.listPatient.size() < 2) {
                    startActivityForResult(new Intent(getContext(), PatientAddActivity.class), 297);
                    return;
                }
                return;
            case R.id.iv_add_patient /*2131296963*/:
                startActivityForResult(new Intent(getContext(), PatientAddActivity.class), 297);
                return;
            default:
                return;
        }
    }

    public void initViewId() {
        this.iv_back = (Button) findViewById(R.id.iv_back);
        this.tv_delete = (TextView) findViewById(R.id.tv_delete);
        this.rl_add_patient = (RelativeLayout) findViewById(R.id.rl_add_patient);
        this.iv_add_patient = (ImageView) findViewById(R.id.iv_add_patient);
        this.tv_add_patient = (TextView) findViewById(R.id.tv_add_patient);
        this.mListView = (ListView) findViewById(R.id.lv_patientList);
        this.defaultPatientRl = (RelativeLayout) findViewById(R.id.layout_default_patient);
        this.defaultPatientTv = (TextView) findViewById(R.id.tv_default_patient);
        this.otherPatientTitleTv = (TextView) findViewById(R.id.tv_other_patient_title);
        this.otherPatientTipsTv = (TextView) findViewById(R.id.tv_other_patient_tips);
    }

    public void initClickListener() {
        this.iv_back.setOnClickListener(this);
        this.tv_delete.setOnClickListener(this);
        this.iv_add_patient.setOnClickListener(this);
        this.rl_add_patient.setOnClickListener(this);
        this.mListView.setOnItemClickListener(this.ITEM_LSN);
        this.defaultPatientRl.setOnClickListener(this);
    }

    public void refreshActivity(Object... params) {
        this.pdDialog.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject result = (JSONObject) params[2];
            if (result != null) {
                ClientLogUtil.d(TAG, result.toString());
            }
            int resultCode = result.optJSONObject("res").optInt("st");
            String msg = result.optJSONObject("res").optString("msg");
            switch (resultCode) {
                case 0:
                    if (requestType == 118) {
                        setUiData(HttpRequestParameters.PATIENT_LIST, result);
                        return;
                    } else if (requestType == 108) {
                        ClientLogUtil.d(TAG, "删除成功");
                        this.listPatient.remove(this.adapter.getPositions());
                        this.adapter.setListPatient(this.listPatient);
                        if (!this.delete_stat && this.listPatient.size() < 2) {
                            this.rl_add_patient.setVisibility(0);
                            this.tv_add_patient.setText((int) R.string.tips_add_new_patient);
                        }
                        this.mHandler.sendMessage(this.mHandler.obtainMessage(0));
                        return;
                    } else {
                        return;
                    }
                case 1:
                    Toast.makeText(getContext(), msg, 0).show();
                    return;
                case 99:
                    Toast.makeText(getContext(), msg, 0).show();
                    return;
                default:
                    return;
            }
        } else {
            Toast.makeText(this, getResources().getString(R.string.service_ex), 0).show();
        }
    }

    public void setUiData(int requestType, JSONObject result) {
        switch (requestType) {
            case HttpRequestParameters.PATIENT_LIST /*118*/:
                JSONArray array = null;
                try {
                    array = result.getJSONArray("inf");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (array == null || array.length() <= 0) {
                    this.defaultPatientRl.setVisibility(8);
                    this.otherPatientTitleTv.setVisibility(8);
                    this.otherPatientTipsTv.setVisibility(8);
                    this.mListView.setVisibility(8);
                    this.tv_add_patient.setText((int) R.string.tips_add_default_patient);
                } else {
                    this.defaultPatientRl.setVisibility(0);
                    this.otherPatientTitleTv.setVisibility(0);
                    this.otherPatientTipsTv.setVisibility(0);
                    this.mListView.setVisibility(0);
                    this.tv_add_patient.setText((int) R.string.tips_add_new_patient);
                    if (array.length() >= 3) {
                        this.iv_add_patient.setVisibility(8);
                        this.rl_add_patient.setVisibility(8);
                    }
                    this.listPatient = setInfoList(array);
                }
                this.adapter.setListPatient(this.listPatient);
                this.mHandler.sendMessage(this.mHandler.obtainMessage(0));
                return;
            default:
                return;
        }
    }

    public ArrayList<PatientInfo> setInfoList(JSONArray array) {
        ArrayList<PatientInfo> list = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            if (getJsonIntValus(array, i, "defaultnum") == 1) {
                this.defaultPatient = new PatientInfo();
                this.defaultPatient.setDefaultPatientCard(getJsonValus(array, i, "defaultpatientcard"));
                this.defaultPatient.setDefaultnum(getJsonIntValus(array, i, "defaultnum"));
                this.defaultPatient.setCitizzencard(getJsonValus(array, i, "citizzencard"));
                this.defaultPatient.setPatientid(getJsonValus(array, i, "patientid"));
                this.defaultPatient.setPatientname(getJsonValus(array, i, "patientname"));
                this.defaultPatient.setAddress(getJsonValus(array, i, "address"));
                this.defaultPatient.setmEmail(getJsonValus(array, i, "email"));
                this.defaultPatient.setUserid(getJsonValus(array, i, "userid"));
                this.defaultPatient.setGender(getJsonValus(array, i, "gender"));
                this.defaultPatient.setIdcard(getJsonValus(array, i, "idcard"));
                this.defaultPatient.setSocialSecurityCard(getJsonValus(array, i, "socialsecuritycard"));
                this.defaultPatient.setHealthyCard(getJsonValus(array, i, "healthycard"));
                this.defaultPatient.setMobile(getJsonValus(array, i, "mobile"));
                this.defaultPatientTv.setText(this.defaultPatient.getPatientname());
            } else {
                PatientInfo info = new PatientInfo();
                info.setDefaultPatientCard(getJsonValus(array, i, "defaultpatientcard"));
                info.setDefaultnum(getJsonIntValus(array, i, "defaultnum"));
                info.setCitizzencard(getJsonValus(array, i, "citizzencard"));
                info.setPatientid(getJsonValus(array, i, "patientid"));
                info.setPatientname(getJsonValus(array, i, "patientname"));
                info.setAddress(getJsonValus(array, i, "address"));
                info.setmEmail(getJsonValus(array, i, "email"));
                info.setUserid(getJsonValus(array, i, "userid"));
                info.setGender(getJsonValus(array, i, "gender"));
                info.setIdcard(getJsonValus(array, i, "idcard"));
                info.setSocialSecurityCard(getJsonValus(array, i, "socialsecuritycard"));
                info.setHealthyCard(getJsonValus(array, i, "healthycard"));
                info.setMobile(getJsonValus(array, i, "mobile"));
                list.add(info);
            }
        }
        return list;
    }

    public String getJsonValus(JSONArray array, int position, String key) {
        try {
            return array.getJSONObject(position).getString(key);
        } catch (JSONException e) {
            return PoiTypeDef.All;
        }
    }

    public void loadData(int requestType, Object json) {
        JSONArray jSONArray = (JSONArray) json;
        switch (requestType) {
        }
    }

    /* access modifiers changed from: private */
    public Context getContext() {
        return this;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 297:
                if (resultCode == -1) {
                    sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.PATIENT_LIST), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.PATIENT_LIST)));
                    return;
                }
                return;
            default:
                return;
        }
    }

    public int getJsonIntValus(JSONArray array, int position, String key) {
        try {
            return array.getJSONObject(position).getInt(key);
        } catch (JSONException e) {
            return -1;
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.PATIENT_LIST /*118*/:
                parameters.add("userid", this.userid);
                break;
        }
        return parameters;
    }
}
