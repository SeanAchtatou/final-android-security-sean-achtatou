package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.Toast;
import com.womenchild.hospital.adapter.StopPlanAdapter;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import org.json.JSONArray;
import org.json.JSONObject;

public class StopPlanActivity extends BaseRequestActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    private Button btn_dynamic;
    private Button btn_information;
    private Button btn_noticlist;
    private Button btn_register;
    private RadioGroup gdGroup;
    /* access modifiers changed from: private */
    public Intent intent;
    private Button iv_back;
    private JSONArray jArray;
    private ListView lv_content;
    /* access modifiers changed from: private */
    public Context mContext = this;
    RadioGroup.OnCheckedChangeListener onCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        public void onCheckedChanged(RadioGroup arg0, int tabId) {
            switch (tabId) {
                case R.id.btn_dynamic:
                    StopPlanActivity.this.intent = new Intent(StopPlanActivity.this.mContext, HospitalDynamicActivity.class);
                    StopPlanActivity.this.startActivity(StopPlanActivity.this.intent);
                    StopPlanActivity.this.finish();
                    return;
                case R.id.btn_noticlist:
                    StopPlanActivity.this.intent = new Intent(StopPlanActivity.this.mContext, HospitalNoticeActivity.class);
                    StopPlanActivity.this.startActivity(StopPlanActivity.this.intent);
                    StopPlanActivity.this.finish();
                    return;
                case R.id.btn_information:
                default:
                    return;
            }
        }
    };
    private ProgressDialog pDialog;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.stop_plan_list);
        initViewId();
        initClickListener();
        this.pDialog = new ProgressDialog(this);
        this.pDialog.setMessage(getResources().getString(R.string.loading_data));
        this.pDialog.show();
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.LIST_STOPPIAN), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.LIST_STOPPIAN)));
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void refreshActivity(Object... params) {
        this.pDialog.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject json = (JSONObject) params[2];
            if (json.optJSONObject("res").optInt("st") == 0) {
                Log.d("HIHI", json.optJSONArray("inf").toString());
                loadData(requestType, json.optJSONArray("inf"));
                return;
            }
            Toast.makeText(this, json.optJSONObject("res").optString("msg"), 0).show();
            return;
        }
        Toast.makeText(this, "服务器无法连接或网络异常", 0).show();
    }

    public void initViewId() {
        this.lv_content = (ListView) findViewById(R.id.lv_content);
        this.iv_back = (Button) findViewById(R.id.iv_back);
        this.btn_register = (Button) findViewById(R.id.btn_register);
        this.gdGroup = (RadioGroup) findViewById(R.id.rl_bottom);
        this.gdGroup.setOnCheckedChangeListener(this.onCheckedChangeListener);
        this.gdGroup.check(R.id.btn_information);
    }

    public void initClickListener() {
        this.iv_back.setOnClickListener(this);
        this.lv_content.setOnItemClickListener(this);
        this.btn_register.setOnClickListener(this);
    }

    public void loadData(int requestType, Object data) {
        this.jArray = (JSONArray) data;
        this.lv_content.setAdapter((ListAdapter) new StopPlanAdapter(this, this.jArray));
    }

    public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
        Intent intent2 = new Intent(this, StopPlanContentActivity.class);
        intent2.putExtra("json", this.jArray.optJSONObject(arg2).toString());
        startActivity(intent2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                return;
            case R.id.btn_register:
                this.intent = new Intent(this, RegisterNoActivity.class);
                this.intent.putExtra("backFlag", true);
                startActivity(this.intent);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        parameters.add("first", 0);
        parameters.add("pagesize", 15);
        return parameters;
    }
}
