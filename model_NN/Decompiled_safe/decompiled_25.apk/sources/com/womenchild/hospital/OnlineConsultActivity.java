package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.ClientLogUtil;
import com.womenchild.hospital.util.DialogUtil;
import com.womenchild.hospital.util.InputMsgUtil;
import org.json.JSONObject;

public class OnlineConsultActivity extends BaseRequestActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private static final String TAG = "OCActivity";
    public static boolean replyFlag = false;
    private String age;
    private String content;
    private int currentIndex = 0;
    private EditText et_age;
    private EditText et_desc;
    private EditText et_title;
    private int gender = 1;
    private ToggleButton ibtn_sex;
    private Button iv_back;
    private ProgressDialog pDialog;
    private String title;
    private TextView tv_submit;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.online_consult);
        Intent intent = getIntent();
        if (intent != null) {
            this.currentIndex = intent.getIntExtra("index", 0);
        }
        if (getIntent().getBooleanExtra("backFlag", false)) {
            this.iv_back.setBackgroundResource(R.drawable.back_selector);
            this.iv_back.setText(PoiTypeDef.All);
        }
        initViewId();
        initClickListener();
        this.pDialog = new ProgressDialog(this);
        this.pDialog.setMessage(getResources().getString(R.string.submit_pw));
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void refreshActivity(Object... params) {
        this.pDialog.dismiss();
        int intValue = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject result = (JSONObject) params[2];
            if (result != null) {
                ClientLogUtil.d(TAG, result.toString());
            }
            int resultCode = result.optJSONObject("res").optInt("st");
            String msg = result.optJSONObject("res").optString("msg");
            switch (resultCode) {
                case 0:
                    replyFlag = false;
                    new DialogUtil(this, getResources().getString(R.string.consult_submit), getResources().getString(R.string.consult_submit_info), getResources().getString(R.string.wait_info_y), getResources().getString(R.string.check_nur), "finish", "RegisterNoActivity").show();
                    return;
                case 1:
                    Toast.makeText(this, msg, 0).show();
                    return;
                case 99:
                    Toast.makeText(this, msg, 0).show();
                    return;
                default:
                    return;
            }
        } else {
            Toast.makeText(this, getResources().getString(R.string.service_ex), 0).show();
        }
    }

    public void initViewId() {
        this.iv_back = (Button) findViewById(R.id.iv_back);
        this.tv_submit = (TextView) findViewById(R.id.tv_submit);
        this.et_title = (EditText) findViewById(R.id.et_title);
        this.et_desc = (EditText) findViewById(R.id.et_desc);
        this.et_age = (EditText) findViewById(R.id.et_age);
        this.ibtn_sex = (ToggleButton) findViewById(R.id.ibtn_sex);
        if (this.currentIndex == 1) {
            this.iv_back.setText(PoiTypeDef.All);
            this.iv_back.setBackgroundResource(R.drawable.back_selector);
        }
    }

    public void initClickListener() {
        this.iv_back.setOnClickListener(this);
        this.tv_submit.setOnClickListener(this);
        this.ibtn_sex.setOnCheckedChangeListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_submit /*2131296445*/:
                commit();
                InputMsgUtil.hide(this.et_desc);
                return;
            case R.id.iv_back /*2131296529*/:
                setResult(1, getIntent());
                finish();
                return;
            default:
                return;
        }
    }

    public void loadData(int requestType, Object data) {
    }

    public void commit() {
        boolean flag = true;
        this.age = this.et_age.getText().toString();
        this.title = this.et_title.getText().toString();
        this.content = this.et_desc.getText().toString();
        if (PoiTypeDef.All.equals(this.age)) {
            Toast.makeText(this, getResources().getString(R.string.age_null), 0).show();
            flag = false;
            this.et_age.requestFocus();
        } else if (PoiTypeDef.All.equals(this.title)) {
            Toast.makeText(this, getResources().getString(R.string.title_null), 0).show();
            flag = false;
            this.et_title.requestFocus();
        } else if (PoiTypeDef.All.equals(this.content)) {
            Toast.makeText(this, getResources().getString(R.string.substance_null), 0).show();
            flag = false;
            this.et_desc.requestFocus();
        }
        if (flag) {
            this.pDialog.show();
            sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.ADD_ONLINE_CONSULT), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.ADD_ONLINE_CONSULT)));
        }
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            this.gender = 1;
        } else {
            this.gender = 0;
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        parameters.add("accid", UserEntity.getInstance().getInfo().getAccId());
        parameters.add("name", UserEntity.getInstance().getInfo().getName());
        parameters.add("mobile", UserEntity.getInstance().getInfo().getMobile());
        parameters.add("gender", Integer.valueOf(this.gender));
        parameters.add("age", Integer.valueOf(Integer.parseInt(this.age)));
        parameters.add("title", this.title);
        parameters.add("content", this.content);
        return parameters;
    }
}
