package com.womenchild.hospital;

import android.app.Activity;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.util.HashMap;
import java.util.Map;

public class Result extends Activity {
    private static final Map<String, String> sError = new HashMap();
    public static String sResult;

    static {
        sError.put("9000", "操作成功");
        sError.put("4000", "系统异常");
        sError.put("4001", "数据格式不正确");
        sError.put("4003", "该用户绑定的支付宝账户被冻结或不允许支付");
        sError.put("4004", "该用户已解除绑定");
        sError.put("4005", "绑定失败或没有绑定");
        sError.put("4006", "订单支付失败");
        sError.put("4010", "重新绑定账户");
        sError.put("6000", "支付服务正在进行升级操作");
        sError.put("6001", "用户中途取消支付操作");
        sError.put("7001", "网页支付失败");
    }

    public static String getResult1() {
        return getContent(sResult.replace("{", PoiTypeDef.All).replace("}", PoiTypeDef.All), "memo=", ";result");
    }

    public static String getResult() {
        return getContent(sResult.replace("{", PoiTypeDef.All).replace("}", PoiTypeDef.All), "resultStatus=", ";memo=");
    }

    private static String getContent(String src, String startTag, String endTag) {
        String content = src;
        int start = src.indexOf(startTag) + startTag.length();
        if (endTag == null) {
            return src.substring(start);
        }
        try {
            return src.substring(start, src.indexOf(endTag));
        } catch (Exception e) {
            e.printStackTrace();
            return content;
        }
    }
}
