package com.womenchild.hospital;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.adapter.DoctorPlanAdatper;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.entity.DocPlanByTimeContainer;
import com.womenchild.hospital.entity.DoctorPlanJsonEntity;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.ClientLogUtil;
import com.womenchild.hospital.util.DateUtil;
import com.womenchild.hospital.view.DoctorCalPageAdapter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SelectDoctorActivity extends BaseRequestActivity implements View.OnClickListener {
    /* access modifiers changed from: private */
    public static String TAG = "SelectDoctorActivity";
    public static String doctorName;
    public static int favPosition = -1;
    public static ProgressDialog pDialog;
    final String INITIALIZED = "initializedDialog";
    /* access modifiers changed from: private */
    public Button btnDayForward;
    /* access modifiers changed from: private */
    public Button btnDayNext;
    private Button btnSort;
    private ArrayList<DocPlanByTimeContainer> containerArrayList;
    private LinearLayout dateAm1;
    private LinearLayout dateAm2;
    private LinearLayout dateAm3;
    private DatePickerDialog dateDlg;
    private TextView dateFirstTv;
    /* access modifiers changed from: private */
    public String[] dateList;
    DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
            Log.d(SelectDoctorActivity.TAG, "Y M D :" + year + month + dayOfMonth);
            String selectDate = String.valueOf(SelectDoctorActivity.this.formatDate(year)) + "-" + SelectDoctorActivity.this.formatDate(month + 1) + "-" + SelectDoctorActivity.this.formatDate(dayOfMonth);
            boolean flag = true;
            int i = 0;
            while (true) {
                if (i >= SelectDoctorActivity.this.dateList.length) {
                    break;
                } else if (selectDate.equals(SelectDoctorActivity.this.dateList[i])) {
                    if (i < 3) {
                        SelectDoctorActivity.this.indexFlag = 0;
                        SelectDoctorActivity.this.btnDayForward.setEnabled(false);
                        SelectDoctorActivity.this.switchDateData(SelectDoctorActivity.this.splitList(SelectDoctorActivity.this.indexFlag));
                        SelectDoctorActivity.this.viewPager.setCurrentItem(SelectDoctorActivity.this.indexFlag);
                    } else if (i > 2 && i < 6) {
                        SelectDoctorActivity.this.indexFlag = 1;
                        SelectDoctorActivity.this.btnDayForward.setEnabled(true);
                        SelectDoctorActivity.this.btnDayNext.setEnabled(true);
                        SelectDoctorActivity.this.switchDateData(SelectDoctorActivity.this.splitList(SelectDoctorActivity.this.indexFlag));
                        SelectDoctorActivity.this.viewPager.setCurrentItem(SelectDoctorActivity.this.indexFlag);
                    } else if (i > 5 && i < 9) {
                        SelectDoctorActivity.this.indexFlag = 2;
                        SelectDoctorActivity.this.btnDayForward.setEnabled(true);
                        SelectDoctorActivity.this.btnDayNext.setEnabled(true);
                        SelectDoctorActivity.this.switchDateData(SelectDoctorActivity.this.splitList(SelectDoctorActivity.this.indexFlag));
                        SelectDoctorActivity.this.viewPager.setCurrentItem(SelectDoctorActivity.this.indexFlag);
                    } else if (i > 8 && i < 12) {
                        SelectDoctorActivity.this.indexFlag = 3;
                        SelectDoctorActivity.this.btnDayForward.setEnabled(true);
                        SelectDoctorActivity.this.btnDayNext.setEnabled(true);
                        SelectDoctorActivity.this.switchDateData(SelectDoctorActivity.this.splitList(SelectDoctorActivity.this.indexFlag));
                        SelectDoctorActivity.this.viewPager.setCurrentItem(SelectDoctorActivity.this.indexFlag);
                    } else if (i > 11) {
                        SelectDoctorActivity.this.indexFlag = 4;
                        SelectDoctorActivity.this.btnDayNext.setEnabled(false);
                        SelectDoctorActivity.this.switchDateData(SelectDoctorActivity.this.splitList(4));
                        SelectDoctorActivity.this.viewPager.setCurrentItem(SelectDoctorActivity.this.indexFlag);
                    }
                    flag = false;
                } else {
                    i++;
                }
            }
            if (flag) {
                Toast.makeText(SelectDoctorActivity.this.mContext, SelectDoctorActivity.this.getResources().getString(R.string.arrange_data), 0).show();
            }
        }
    };
    private LinearLayout datePm1;
    private LinearLayout datePm2;
    private LinearLayout datePm3;
    private TextView dateSecondTv;
    private TextView dateThirdTv;
    private int deptId;
    private JSONObject doctor;
    private Button doctorBtn;
    private int groupNum = 0;
    /* access modifiers changed from: private */
    public int indexFlag = 0;
    private Button ivBack;
    private ListView lvDoctorPlan;
    /* access modifiers changed from: private */
    public Context mContext = this;
    private RelativeLayout mIntroRlayout;
    private DoctorCalPageAdapter planAdapter;
    private JSONObject planJson;
    private LinearLayout rlBottom;
    private RelativeLayout rlTimePlan;
    private RelativeLayout rlType;
    private String someStringDialog;
    private boolean sortFlag = false;
    private int sortNum = 2;
    private Button threeDateBtn;
    Toast toast;
    /* access modifiers changed from: private */
    public ViewPager viewPager;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.select_doctor);
        initViewId();
        initClickListener();
        initData();
        tosast();
    }

    private void tosast() {
        Toast.makeText(this, "长按医生名字可以收藏该医生哦!", 0).show();
    }

    private void Shared() {
        SharedPreferences myPrefs = getSharedPreferences(this.someStringDialog, 0);
        if (!myPrefs.getBoolean("initializedDialog", true)) {
            this.someStringDialog = "some default value";
        }
        SharedPreferences.Editor editor = myPrefs.edit();
        editor.putBoolean("initializedDialog", false);
        editor.commit();
    }

    public void refreshActivity(Object... params) {
        pDialog.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject result = (JSONObject) params[2];
            int resultCode = result.optJSONObject("res").optInt("st");
            String msg = result.optJSONObject("res").optString("msg");
            ClientLogUtil.i("SelectDoctor", result.toString());
            if (resultCode == 0) {
                switch (requestType) {
                    case HttpRequestParameters.LISTPLAN_BY_DEPTID /*304*/:
                        loadData(requestType, result.optJSONObject("inf"));
                        return;
                    case HttpRequestParameters.ADD_FAVORITES_DOCTOR /*509*/:
                        Toast.makeText(this, msg, 0).show();
                        if (this.groupNum == 1) {
                            pDialog.show();
                            sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.LISTPLAN_BY_DEPTID), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.LISTPLAN_BY_DEPTID)));
                            return;
                        }
                        return;
                    default:
                        return;
                }
            } else {
                Toast.makeText(this, msg, 0).show();
                if (requestType == 304) {
                    this.btnDayNext.setEnabled(false);
                }
            }
        } else {
            Toast.makeText(this, (int) R.string.network_connect_failed_prompt, 0).show();
        }
    }

    public void initViewId() {
        this.ivBack = (Button) findViewById(R.id.iv_back);
        this.btnSort = (Button) findViewById(R.id.btn_sort);
        this.rlBottom = (LinearLayout) findViewById(R.id.rl_bottom);
        this.rlTimePlan = (RelativeLayout) findViewById(R.id.rl_time_plan);
        this.rlType = (RelativeLayout) findViewById(R.id.rl_type);
        this.lvDoctorPlan = (ListView) findViewById(R.id.lv_doctor_plan);
        this.lvDoctorPlan.setVisibility(8);
        pDialog = new ProgressDialog(this);
        pDialog.setMessage(getResources().getString(R.string.loading));
        this.btnDayForward = (Button) findViewById(R.id.btn_last);
        this.btnDayNext = (Button) findViewById(R.id.btn_next);
        this.dateFirstTv = (TextView) findViewById(R.id.iv_time_1);
        this.dateSecondTv = (TextView) findViewById(R.id.iv_time_2);
        this.dateThirdTv = (TextView) findViewById(R.id.iv_time_3);
        this.threeDateBtn = (Button) findViewById(R.id.tv_three_date);
        this.viewPager = (ViewPager) findViewById(R.id.viewpager);
        this.viewPager.setOnPageChangeListener(new PageChangeListener(this, null));
    }

    public void initClickListener() {
        this.ivBack.setOnClickListener(this);
        this.btnSort.setOnClickListener(this);
        this.btnDayForward.setOnClickListener(this);
        this.btnDayNext.setOnClickListener(this);
        this.threeDateBtn.setOnClickListener(this);
        this.btnDayForward.setEnabled(false);
        this.threeDateBtn.setEnabled(false);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                return;
            case R.id.btn_next:
                this.indexFlag++;
                this.viewPager.setCurrentItem(this.indexFlag);
                switchDateData(splitList(this.indexFlag));
                this.btnDayForward.setEnabled(true);
                if (this.indexFlag == 4) {
                    this.btnDayNext.setEnabled(false);
                    return;
                }
                return;
            case R.id.btn_sort:
                this.sortFlag = !this.sortFlag;
                if (this.sortFlag) {
                    this.btnSort.setText(getResources().getString(R.string.plan_time));
                    hide(0);
                    this.groupNum = 1;
                } else {
                    this.btnSort.setText(getResources().getString(R.string.plan_doctor));
                    hide(1);
                    this.groupNum = 0;
                }
                pDialog.show();
                sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.LISTPLAN_BY_DEPTID), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.LISTPLAN_BY_DEPTID)));
                return;
            case R.id.btn_last:
                this.indexFlag--;
                this.viewPager.setCurrentItem(this.indexFlag);
                switchDateData(splitList(this.indexFlag));
                this.btnDayNext.setEnabled(true);
                if (this.indexFlag == 0) {
                    this.btnDayForward.setEnabled(false);
                    return;
                }
                return;
            case R.id.tv_three_date:
                showDateDialg();
                return;
            default:
                return;
        }
    }

    private void hide(int type) {
        if (type == 0) {
            this.rlBottom.setVisibility(8);
            this.rlTimePlan.setVisibility(8);
            this.rlType.setVisibility(8);
            this.lvDoctorPlan.setVisibility(0);
            return;
        }
        this.rlBottom.setVisibility(0);
        this.rlType.setVisibility(0);
        this.rlTimePlan.setVisibility(0);
        this.lvDoctorPlan.setVisibility(8);
    }

    public void loadData(int requestType, Object data) {
        JSONObject json = (JSONObject) data;
        switch (requestType) {
            case HttpRequestParameters.LISTPLAN_BY_DEPTID /*304*/:
                if (this.groupNum == 0) {
                    JSONArray plan = json.optJSONArray("plans");
                    if (plan != null) {
                        orderByTimeJsonArray(plan);
                        return;
                    } else {
                        Toast.makeText(this, getResources().getString(R.string.arrange_data), 0).show();
                        return;
                    }
                } else {
                    if (json.optJSONArray("plans") != null) {
                        this.planJson = json;
                        this.lvDoctorPlan.setAdapter((ListAdapter) new DoctorPlanAdatper(this, json.optJSONArray("plans")));
                    } else {
                        Toast.makeText(this, getResources().getString(R.string.arrange_data), 0).show();
                    }
                    hide(0);
                    return;
                }
            case HttpRequestParameters.ADD_FAVORITES_DOCTOR /*509*/:
                try {
                    if (favPosition != -1) {
                        this.planJson.optJSONArray("plans").optJSONObject(favPosition).put("favdoctor", 0);
                        this.lvDoctorPlan.setAdapter((ListAdapter) new DoctorPlanAdatper(this, this.planJson.optJSONArray("plans")));
                        favPosition = -1;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(getResources().getString(R.string.add_collects));
                builder.setMessage(String.valueOf(getResources().getString(R.string.seek_collects)) + doctorName + getResources().getString(R.string.doctors));
                builder.setPositiveButton(getResources().getString(R.string.ok), (DialogInterface.OnClickListener) null);
                builder.create().show();
                return;
            default:
                return;
        }
    }

    public void orderByTimeJsonArray(JSONArray jsons) {
        int length = jsons.length();
        ClientLogUtil.d(TAG, "orderByTimeJsonArray():length:" + jsons.length());
        if (length > 0) {
            this.containerArrayList = new ArrayList<>();
            this.dateList = new String[length];
            if (length > 3) {
                this.threeDateBtn.setEnabled(true);
                this.btnDayNext.setEnabled(true);
                for (int planIndex = 0; planIndex < length; planIndex++) {
                    try {
                        DocPlanByTimeContainer docPlanByTimeContainer = new DocPlanByTimeContainer(jsons.optJSONObject(planIndex));
                        DocPlanByTimeContainer docPlanByTimeContainer2 = new DocPlanByTimeContainer(jsons.optJSONObject(planIndex));
                        this.dateList[planIndex] = docPlanByTimeContainer.getPlanTime();
                        docPlanByTimeContainer.setAmOrPm(true);
                        docPlanByTimeContainer2.setAmOrPm(false);
                        HashSet<String> set = new HashSet<>();
                        JSONArray planList = jsons.optJSONObject(planIndex).optJSONArray("planlist");
                        int lengthPlanList = planList.length();
                        ArrayList<JSONObject> recordList = new ArrayList<>();
                        for (int planListIndex = 0; planListIndex < lengthPlanList; planListIndex++) {
                            new JSONObject();
                            JSONObject record = planList.optJSONObject(planListIndex);
                            set.add(record.getString("doctorname"));
                            recordList.add(record);
                        }
                        ArrayList<String> noSameList = new ArrayList<>(set);
                        ArrayList<DoctorPlanJsonEntity> doctorRecordAll = new ArrayList<>();
                        ArrayList<DoctorPlanJsonEntity> doctorRecordAll2 = new ArrayList<>();
                        for (int k = 0; k < noSameList.size(); k++) {
                            ArrayList<JSONObject> recordListPer = new ArrayList<>();
                            DoctorPlanJsonEntity doctorRecord = new DoctorPlanJsonEntity();
                            String dName = (String) noSameList.get(k);
                            doctorRecord.setDoctorname(dName);
                            for (int z = 0; z < recordList.size(); z++) {
                                if (((JSONObject) recordList.get(z)).optString("doctorname").equalsIgnoreCase(dName)) {
                                    recordListPer.add((JSONObject) recordList.get(z));
                                }
                            }
                            doctorRecord.setRecord(recordListPer);
                            DoctorPlanJsonEntity doctorRecordAm = new DoctorPlanJsonEntity();
                            DoctorPlanJsonEntity doctorRecordPm = new DoctorPlanJsonEntity();
                            doctorRecordAm.setDoctorname(doctorRecord.getDoctorname());
                            doctorRecordPm.setDoctorname(doctorRecord.getDoctorname());
                            ArrayList<JSONObject> recordListPerTemp = new ArrayList<>();
                            ArrayList<JSONObject> recordListPerTemp2 = new ArrayList<>();
                            for (int y = 0; y < doctorRecord.getRecord().size(); y++) {
                                if (doctorRecord.getRecord().get(y).optString("timespan").equals(getResources().getString(R.string.morning))) {
                                    recordListPerTemp.add(doctorRecord.getRecord().get(y));
                                } else if (doctorRecord.getRecord().get(y).optString("timespan").equals(getResources().getString(R.string.afternoon))) {
                                    recordListPerTemp2.add(doctorRecord.getRecord().get(y));
                                }
                            }
                            doctorRecordAm.setRecord(recordListPerTemp);
                            doctorRecordAm.setCanOrderNum(countAvailNum(doctorRecordAm.getRecord()));
                            doctorRecordAm.setStop(isStop(doctorRecordAm.getRecord()));
                            doctorRecordAm.setDate(docPlanByTimeContainer.getPlanTime());
                            doctorRecordPm.setRecord(recordListPerTemp2);
                            doctorRecordPm.setCanOrderNum(countAvailNum(doctorRecordPm.getRecord()));
                            doctorRecordPm.setStop(isStop(doctorRecordPm.getRecord()));
                            doctorRecordPm.setDate(docPlanByTimeContainer.getPlanTime());
                            if (doctorRecordAm.getRecord().size() > 0) {
                                doctorRecordAll.add(doctorRecordAm);
                            }
                            if (doctorRecordPm.getRecord().size() > 0) {
                                doctorRecordAll2.add(doctorRecordPm);
                            }
                        }
                        docPlanByTimeContainer.setDoctorNum(doctorRecordAll);
                        docPlanByTimeContainer2.setDoctorNum(doctorRecordAll2);
                        this.containerArrayList.add(docPlanByTimeContainer);
                        this.containerArrayList.add(docPlanByTimeContainer2);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                switchDateData(splitList(this.indexFlag));
                this.planAdapter = new DoctorCalPageAdapter(this, this.containerArrayList);
                this.viewPager.setAdapter(this.planAdapter);
                this.planAdapter.setOnDoctorBtnPageLongClickListener(new DoctorCalPageAdapter.OnDoctorButtonPageLongClickListener() {
                    public void onDoctorButtonPageLongClick(DoctorPlanJsonEntity doctorPlanRecord) {
                        if (HomeActivity.loginFlag) {
                            SelectDoctorActivity.this.createDoctorData(doctorPlanRecord);
                            SelectDoctorActivity.pDialog.show();
                            SelectDoctorActivity.this.sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.ADD_FAVORITES_DOCTOR), SelectDoctorActivity.this.initRequestParameter(Integer.valueOf((int) HttpRequestParameters.ADD_FAVORITES_DOCTOR)));
                            return;
                        }
                        Toast.makeText(SelectDoctorActivity.this, SelectDoctorActivity.this.getString(R.string.login_function), 0).show();
                    }
                });
                this.viewPager.setCurrentItem(this.indexFlag);
                return;
            }
            for (int i = 0; i < length; i++) {
            }
        }
    }

    public ArrayList<DocPlanByTimeContainer> splitList(int index) {
        ArrayList<DocPlanByTimeContainer> containerFirst = new ArrayList<>();
        switch (index) {
            case 0:
                for (int i = 0; i < 6; i++) {
                    containerFirst.add(this.containerArrayList.get(i));
                }
                break;
            case 1:
                for (int i2 = 6; i2 < 12; i2++) {
                    containerFirst.add(this.containerArrayList.get(i2));
                }
                break;
            case 2:
                for (int i3 = 12; i3 < 18; i3++) {
                    containerFirst.add(this.containerArrayList.get(i3));
                }
                break;
            case 3:
                for (int i4 = 18; i4 < 24; i4++) {
                    containerFirst.add(this.containerArrayList.get(i4));
                }
                break;
            case 4:
                for (int i5 = 24; i5 < 28; i5++) {
                    containerFirst.add(this.containerArrayList.get(i5));
                }
                break;
        }
        return containerFirst;
    }

    private int countAvailNum(ArrayList<JSONObject> record) {
        int tmpCount = 0;
        for (int i = 0; i < record.size(); i++) {
            int availNum = record.get(i).optInt("availnum");
            if (availNum == -1) {
                return -1;
            }
            tmpCount += availNum;
        }
        return tmpCount;
    }

    private boolean isStop(ArrayList<JSONObject> record) {
        boolean isStop = false;
        int i = 0;
        while (i < record.size() && !(isStop = record.get(i).optBoolean("isstop"))) {
            i++;
        }
        return isStop;
    }

    public void switchDateData(ArrayList<DocPlanByTimeContainer> containerList) {
        this.dateThirdTv.setVisibility(0);
        int dateSize = containerList.size();
        for (int i = 0; i < dateSize; i++) {
            Log.d(TAG, "doctor size:" + containerList.get(i).getDoctorNum().size());
            switch (i) {
                case 0:
                    this.dateFirstTv.setText(DateUtil.replaceDate(containerList.get(i).getPlanTime()));
                    break;
                case 2:
                    this.dateSecondTv.setText(DateUtil.replaceDate(containerList.get(i).getPlanTime()));
                    break;
                case 4:
                    this.dateThirdTv.setText(DateUtil.replaceDate(containerList.get(i).getPlanTime()));
                    break;
            }
            if (dateSize > 4) {
                this.threeDateBtn.setText(((Object) this.dateFirstTv.getText()) + "-" + ((Object) this.dateThirdTv.getText()));
            } else if (dateSize > 0 && dateSize < 5) {
                this.threeDateBtn.setText(((Object) this.dateFirstTv.getText()) + "-" + ((Object) this.dateSecondTv.getText()));
                this.dateThirdTv.setVisibility(4);
            }
        }
    }

    private class OrderDoctorClickListener implements View.OnClickListener {
        private DoctorPlanJsonEntity doctorPlanRecord;
        private ArrayList<JSONObject> jsonObject;

        public OrderDoctorClickListener(DoctorPlanJsonEntity json) {
            this.doctorPlanRecord = json;
        }

        public void onClick(View v) {
            JSONObject jsonObj = new JSONObject();
            try {
                JSONArray jArray = new JSONArray();
                for (int i = 0; i < this.doctorPlanRecord.getRecord().size(); i++) {
                    jArray.put(this.doctorPlanRecord.getRecord().get(i));
                }
                jsonObj.put("day", this.doctorPlanRecord.getDate());
                jsonObj.put("planlist", jArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Intent intent = new Intent(SelectDoctorActivity.this.mContext, SelectClinicTimeActivity.class);
            intent.putExtra("jsons", jsonObj.toString());
            SelectDoctorActivity.this.mContext.startActivity(intent);
        }
    }

    public void showDateDialg() {
        Calendar calendar = Calendar.getInstance();
        this.dateDlg = new DatePickerDialog(this, this.dateListener, calendar.get(1), calendar.get(2), calendar.get(5) + 1);
        this.dateDlg.show();
    }

    public String formatDate(int num) {
        if (num < 10) {
            return "0" + num;
        }
        return new StringBuilder().append(num).toString();
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.deptId = getIntent().getIntExtra("deptid", 0);
        pDialog.show();
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.LISTPLAN_BY_DEPTID), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.LISTPLAN_BY_DEPTID)));
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.LISTPLAN_BY_DEPTID /*304*/:
                parameters.add("userid", PoiTypeDef.All);
                parameters.add("deptid", Integer.valueOf(this.deptId));
                parameters.add("sortnum", Integer.valueOf(this.sortNum));
                parameters.add("groupnum", Integer.valueOf(this.groupNum));
                break;
            case HttpRequestParameters.ADD_FAVORITES_DOCTOR /*509*/:
                JSONArray doctors = new JSONArray();
                parameters.add("userid", UserEntity.getInstance().getInfo().getAccId());
                doctors.put(this.doctor);
                parameters.add("doctors", doctors.toString());
                break;
        }
        return parameters;
    }

    private class PageChangeListener implements ViewPager.OnPageChangeListener {
        private PageChangeListener() {
        }

        /* synthetic */ PageChangeListener(SelectDoctorActivity selectDoctorActivity, PageChangeListener pageChangeListener) {
            this();
        }

        public void onPageScrollStateChanged(int arg0) {
        }

        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        public void onPageSelected(int arg0) {
            SelectDoctorActivity.this.indexFlag = arg0;
            SelectDoctorActivity.this.btnDayForward.setEnabled(true);
            SelectDoctorActivity.this.btnDayNext.setEnabled(true);
            if (SelectDoctorActivity.this.indexFlag == 0) {
                SelectDoctorActivity.this.btnDayForward.setEnabled(false);
            } else if (SelectDoctorActivity.this.indexFlag == 4) {
                SelectDoctorActivity.this.btnDayNext.setEnabled(false);
            }
            SelectDoctorActivity.this.switchDateData(SelectDoctorActivity.this.splitList(SelectDoctorActivity.this.indexFlag));
        }
    }

    /* access modifiers changed from: private */
    public void createDoctorData(DoctorPlanJsonEntity doctorPlanRecord) {
        JSONObject temp = new JSONObject();
        try {
            temp.put("doctorid", doctorPlanRecord.getRecord().get(0).getString("doctorid"));
            temp.put("doctorName", doctorPlanRecord.getDoctorname());
            temp.put("hospital", doctorPlanRecord.getRecord().get(0).getString("hospitalname"));
            temp.put("level", doctorPlanRecord.getRecord().get(0).getString("doctorlevel"));
            temp.put("gender", "2");
            this.doctor = temp;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
