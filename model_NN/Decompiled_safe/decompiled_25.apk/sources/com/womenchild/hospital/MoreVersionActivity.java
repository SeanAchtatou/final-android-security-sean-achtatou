package com.womenchild.hospital;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.parameter.UriParameter;

public class MoreVersionActivity extends BaseRequestActivity implements View.OnClickListener {
    private Button ibtnHome;
    private Intent intent;
    private TextView tvVersion;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.about_veison);
        initViewId();
        initClickListener();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_home /*2131296435*/:
                finish();
                return;
            default:
                return;
        }
    }

    public void refreshActivity(Object... params) {
    }

    public void initViewId() {
        this.ibtnHome = (Button) findViewById(R.id.ibtn_home);
        this.tvVersion = (TextView) findViewById(R.id.tv_num);
        try {
            this.tvVersion.setText("V" + getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void initClickListener() {
        this.ibtnHome.setOnClickListener(this);
    }

    public void loadData(int requestType, Object data) {
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        return null;
    }
}
