package com.womenchild.hospital;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Xml;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.mapapi.location.LocationManagerProxy;
import com.umeng.common.util.e;
import com.upomp.pay.Star_Upomp_Pay;
import com.upomp.pay.help.CreateOriginal;
import com.upomp.pay.help.Create_MerchantX;
import com.upomp.pay.help.GetValue;
import com.upomp.pay.help.Xmlpar;
import com.upomp.pay.httpservice.XmlHttpConnection;
import com.upomp.pay.info.Upomp_Pay_Info;
import com.upomp.pay.info.XmlDefinition;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.AppParameters;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;

public class SelectPayWayActivity extends BaseRequestActivity implements View.OnClickListener {
    InputStream PrivateSign;
    private boolean againRegister = false;
    private Button btnNotPayNow;
    private Button btnPayNow;
    XmlHttpConnection httpConnection;
    private Button ibtnReturn;
    private Intent intent;
    private JSONObject json;
    private int money;
    private String opcorderid;
    private String orderId;
    private boolean orderStatus = true;
    private ProgressDialog progressDialog;
    private boolean recordStatus = true;
    Star_Upomp_Pay star;
    private TextView tvDept;
    private TextView tvDoctor;
    private TextView tvMobile;
    private TextView tvMoney;
    private TextView tvPatient;
    private boolean updateStatus = false;
    GetValue values;
    private boolean verifyStatus = true;
    Xmlpar xmlpar;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.select_pay_way);
        initViewId();
        initClickListener();
        initData();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void refreshActivity(Object... params) {
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject result = (JSONObject) params[2];
            if (result.optJSONObject("res").optInt("st") == 0) {
                switch (requestType) {
                    case HttpRequestParameters.SUBMIT_ORDER /*602*/:
                        this.opcorderid = result.optJSONObject("inf").optString("opcorderid");
                        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.UPDATE_PAY_RECORD), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.UPDATE_PAY_RECORD)));
                        return;
                    case HttpRequestParameters.ADD_ORDER_RECORD /*603*/:
                        this.recordStatus = true;
                        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.SUBMIT_PAY_ORDER), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.SUBMIT_PAY_ORDER)));
                        return;
                    case HttpRequestParameters.SUBMIT_PAY_ORDER /*604*/:
                        this.progressDialog.dismiss();
                        this.verifyStatus = true;
                        verifyOrder(result.optJSONObject("inf"));
                        return;
                    case HttpRequestParameters.ORDER_STATUS /*605*/:
                    case HttpRequestParameters.AUTO_PAY_ORDER /*606*/:
                    case HttpRequestParameters.AUTO_PAY_CONFIRM /*607*/:
                    default:
                        return;
                    case HttpRequestParameters.DELETE_PAY_RECORD /*608*/:
                        Intent intent2 = new Intent(this, RegisterNoSuccessActivity.class);
                        intent2.putExtra("ordernum", this.orderId);
                        intent2.putExtra("opcorderid", this.opcorderid);
                        intent2.putExtra("order", this.json.toString());
                        intent2.putExtra(LocationManagerProxy.KEY_STATUS_CHANGED, 1);
                        startActivity(intent2);
                        return;
                    case HttpRequestParameters.BACK_MONEY /*609*/:
                        Intent intent3 = new Intent(this, BackMoneyActivity.class);
                        intent3.putExtra(LocationManagerProxy.KEY_STATUS_CHANGED, true);
                        startActivity(intent3);
                        return;
                    case HttpRequestParameters.UPDATE_PAY_RECORD /*610*/:
                        this.progressDialog.dismiss();
                        Intent intent4 = new Intent(this, RegisterNoSuccessActivity.class);
                        intent4.putExtra("ordernum", this.orderId);
                        intent4.putExtra("opcorderid", this.opcorderid);
                        intent4.putExtra("order", this.json.toString());
                        intent4.putExtra(LocationManagerProxy.KEY_STATUS_CHANGED, 0);
                        startActivity(intent4);
                        this.orderStatus = true;
                        return;
                }
            } else {
                this.progressDialog.dismiss();
                switch (requestType) {
                    case HttpRequestParameters.SUBMIT_ORDER /*602*/:
                        if (!this.againRegister) {
                            this.againRegister = true;
                            createDialog();
                            break;
                        } else {
                            Intent intent5 = new Intent(this, RegisterNoSuccessActivity.class);
                            intent5.putExtra("ordernum", this.orderId);
                            intent5.putExtra("opcorderid", this.opcorderid);
                            intent5.putExtra("order", this.json.toString());
                            intent5.putExtra(LocationManagerProxy.KEY_STATUS_CHANGED, 1);
                            startActivity(intent5);
                            break;
                        }
                    case HttpRequestParameters.ADD_ORDER_RECORD /*603*/:
                        this.recordStatus = false;
                        break;
                    case HttpRequestParameters.SUBMIT_PAY_ORDER /*604*/:
                        this.verifyStatus = false;
                        break;
                    case HttpRequestParameters.DELETE_PAY_RECORD /*608*/:
                        Intent intent6 = new Intent(this, RegisterNoSuccessActivity.class);
                        intent6.putExtra("ordernum", this.orderId);
                        intent6.putExtra("opcorderid", this.opcorderid);
                        intent6.putExtra("order", this.json.toString());
                        intent6.putExtra(LocationManagerProxy.KEY_STATUS_CHANGED, 1);
                        startActivity(intent6);
                        break;
                    case HttpRequestParameters.BACK_MONEY /*609*/:
                        Intent intent7 = new Intent(this, BackMoneyActivity.class);
                        intent7.putExtra(LocationManagerProxy.KEY_STATUS_CHANGED, false);
                        startActivity(intent7);
                        break;
                    case HttpRequestParameters.UPDATE_PAY_RECORD /*610*/:
                        this.progressDialog.setCancelable(false);
                        this.progressDialog.show();
                        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.UPDATE_PAY_RECORD), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.UPDATE_PAY_RECORD)));
                        this.recordStatus = false;
                        break;
                }
                if (this.updateStatus) {
                    Toast.makeText(this, result.optJSONObject("res").optString("msg"), 0).show();
                }
            }
        } else {
            this.progressDialog.dismiss();
            switch (requestType) {
                case HttpRequestParameters.SUBMIT_ORDER /*602*/:
                    if (this.againRegister) {
                        Intent intent8 = new Intent(this, RegisterNoSuccessActivity.class);
                        intent8.putExtra("ordernum", this.orderId);
                        intent8.putExtra("opcorderid", this.opcorderid);
                        intent8.putExtra("order", this.json.toString());
                        intent8.putExtra(LocationManagerProxy.KEY_STATUS_CHANGED, 1);
                        startActivity(intent8);
                    } else {
                        this.againRegister = true;
                        createDialog();
                    }
                    this.orderStatus = false;
                    return;
                case HttpRequestParameters.ADD_ORDER_RECORD /*603*/:
                    Toast.makeText(this, (int) R.string.network_connect_failed_prompt, 0).show();
                    this.recordStatus = false;
                    return;
                case HttpRequestParameters.SUBMIT_PAY_ORDER /*604*/:
                    this.verifyStatus = false;
                    return;
                case HttpRequestParameters.ORDER_STATUS /*605*/:
                case HttpRequestParameters.AUTO_PAY_ORDER /*606*/:
                case HttpRequestParameters.AUTO_PAY_CONFIRM /*607*/:
                default:
                    return;
                case HttpRequestParameters.DELETE_PAY_RECORD /*608*/:
                    Intent intent9 = new Intent(this, RegisterNoSuccessActivity.class);
                    intent9.putExtra("ordernum", this.orderId);
                    intent9.putExtra("opcorderid", this.opcorderid);
                    intent9.putExtra("order", this.json.toString());
                    intent9.putExtra(LocationManagerProxy.KEY_STATUS_CHANGED, 1);
                    startActivity(intent9);
                    return;
                case HttpRequestParameters.BACK_MONEY /*609*/:
                    Intent intent10 = new Intent(this, BackMoneyActivity.class);
                    intent10.putExtra(LocationManagerProxy.KEY_STATUS_CHANGED, false);
                    startActivity(intent10);
                    return;
                case HttpRequestParameters.UPDATE_PAY_RECORD /*610*/:
                    this.progressDialog.setCancelable(false);
                    this.progressDialog.show();
                    sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.UPDATE_PAY_RECORD), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.UPDATE_PAY_RECORD)));
                    return;
            }
        }
    }

    public void initViewId() {
        this.ibtnReturn = (Button) findViewById(R.id.ibtn_return);
        this.btnNotPayNow = (Button) findViewById(R.id.btn_not_pay_now);
        this.btnPayNow = (Button) findViewById(R.id.btn_pay_now);
        this.tvDept = (TextView) findViewById(R.id.tv_dept);
        this.tvDoctor = (TextView) findViewById(R.id.tv_doctor);
        this.tvMobile = (TextView) findViewById(R.id.tv_mobile);
        this.tvMoney = (TextView) findViewById(R.id.tv_money);
        this.tvPatient = (TextView) findViewById(R.id.tv_patient);
        this.progressDialog = new ProgressDialog(this);
        this.progressDialog.setMessage(getResources().getString(R.string.substance_order_yes));
    }

    public void initClickListener() {
        this.ibtnReturn.setOnClickListener(this);
        this.btnNotPayNow.setOnClickListener(this);
        this.btnPayNow.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_return:
                finish();
                return;
            case R.id.btn_pay_now:
                if (!this.orderStatus) {
                    this.orderStatus = true;
                    sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.SUBMIT_ORDER), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.SUBMIT_ORDER)));
                    return;
                } else if (!this.recordStatus) {
                    this.recordStatus = true;
                    sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.ADD_ORDER_RECORD), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.ADD_ORDER_RECORD)));
                    return;
                } else if (!this.verifyStatus) {
                    this.verifyStatus = true;
                    sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.SUBMIT_PAY_ORDER), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.SUBMIT_PAY_ORDER)));
                    return;
                } else {
                    sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.ADD_ORDER_RECORD), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.ADD_ORDER_RECORD)));
                    return;
                }
            case R.id.btn_not_pay_now:
                this.intent = new Intent(this, RegisterNoSuccessActivity.class);
                this.intent.putExtra("order", this.json.toString());
                this.intent.putExtra("opcorderid", this.opcorderid);
                startActivity(this.intent);
                return;
            default:
                return;
        }
    }

    public void loadData(int requestType, Object data) {
    }

    private void verifyOrder(JSONObject json2) {
        this.values = new GetValue();
        this.xmlpar = new Xmlpar();
        try {
            Upomp_Pay_Info.merchantId = json2.optString("merchantid");
            Upomp_Pay_Info.merchantOrderId = json2.optString("merchantorderid");
            Upomp_Pay_Info.merchantOrderTime = json2.optString("merchantordertime");
            Upomp_Pay_Info.originalsign = CreateOriginal.CreateOriginal_Sign(3);
            Log.d(Upomp_Pay_Info.tag, "这是订单验证的3位原串===\n" + Upomp_Pay_Info.originalsign);
            try {
                this.PrivateSign = getFromAssets("898000000000002.p12");
            } catch (FileNotFoundException e) {
                Log.d(Upomp_Pay_Info.tag, "Exception is " + e);
            }
            Upomp_Pay_Info.xmlSign = json2.optString("sign");
            Log.d(Upomp_Pay_Info.tag, "这是订单验证的3位签名===\n" + Upomp_Pay_Info.xmlSign);
            String LanchPay = XmlDefinition.ReturnXml(Upomp_Pay_Info.xmlSign, 3);
            Log.d(Upomp_Pay_Info.tag, "这是订单验证报文===\n" + LanchPay);
            this.star = new Star_Upomp_Pay();
            this.star.start_upomp_pay(this, LanchPay);
        } catch (Exception e2) {
            Log.d(Upomp_Pay_Info.tag, "**Exception is " + e2);
        }
    }

    public InputStream getFromAssets(String fileName) throws FileNotFoundException {
        try {
            this.PrivateSign = getResources().getAssets().open(fileName);
        } catch (Exception e) {
            Log.d(Upomp_Pay_Info.tag, "Exception is " + e);
        }
        return this.PrivateSign;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            byte[] xml = data.getExtras().getByteArray("xml");
            String respCode = null;
            String respDesc = null;
            try {
                XmlPullParser parser = Xml.newPullParser();
                parser.setInput(new ByteArrayInputStream(xml), e.f);
                for (int eventType = parser.getEventType(); eventType != 1; eventType = parser.next()) {
                    switch (eventType) {
                        case 2:
                            String tag = parser.getName();
                            if (!tag.equalsIgnoreCase("respCode")) {
                                if (!tag.equalsIgnoreCase("respDesc")) {
                                    break;
                                } else {
                                    respDesc = parser.nextText();
                                    break;
                                }
                            } else {
                                respCode = parser.nextText();
                                break;
                            }
                    }
                }
                Log.d(Upomp_Pay_Info.tag, "解析中:" + respCode + "->" + respDesc);
                this.progressDialog.setMessage(getResources().getString(R.string.ok_condition_update));
                if (respCode == null || !"0000".equals(respCode)) {
                    sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.DELETE_PAY_RECORD), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.DELETE_PAY_RECORD)));
                } else {
                    this.progressDialog.setMessage(getResources().getString(R.string.hint_register));
                    sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.SUBMIT_ORDER), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.SUBMIT_ORDER)));
                }
                this.progressDialog.show();
            } catch (Exception e) {
                Log.d(Upomp_Pay_Info.tag, "Exception is " + e);
            }
        } else {
            Log.d(Upomp_Pay_Info.tag, "data is null");
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
        try {
            this.money = new JSONObject(getIntent().getStringExtra("plan")).optInt("fee");
            this.json = new JSONObject(getIntent().getStringExtra("order"));
            this.tvDept.setText(this.json.optString("deptname"));
            this.tvDoctor.setText(this.json.optString("doctorname"));
            this.tvMobile.setText(this.json.optString("mobile"));
            this.tvMoney.setText(String.valueOf(((double) this.money) / 100.0d) + "元");
            this.tvPatient.setText(this.json.optString("name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.SUBMIT_ORDER /*602*/:
                this.progressDialog.show();
                UriParameter parameters = FillInAppointmentInfoActivity.orderEntity.createParameters();
                parameters.add("memberid", String.valueOf(AppParameters.getAPPID()) + "_" + UserEntity.getInstance().getInfo().getAccId());
                return parameters;
            case HttpRequestParameters.ADD_ORDER_RECORD /*603*/:
                if (this.orderId == null) {
                    this.orderId = Create_MerchantX.createMerchantOrderId();
                }
                UriParameter parameters2 = new UriParameter();
                parameters2.add("userid", UserEntity.getInstance().getInfo().getAccId());
                parameters2.add("ordernum", this.orderId);
                parameters2.add("price", Integer.valueOf(this.money));
                parameters2.add("name", this.json.optString("name"));
                return parameters2;
            case HttpRequestParameters.SUBMIT_PAY_ORDER /*604*/:
                UriParameter parameters3 = new UriParameter();
                parameters3.add("ordernum", this.orderId);
                parameters3.add("ordertime", Create_MerchantX.createMerchantOrderTime());
                parameters3.add("orderamt", Integer.valueOf(this.money));
                return parameters3;
            case HttpRequestParameters.ORDER_STATUS /*605*/:
            case HttpRequestParameters.AUTO_PAY_ORDER /*606*/:
            case HttpRequestParameters.AUTO_PAY_CONFIRM /*607*/:
            default:
                return null;
            case HttpRequestParameters.DELETE_PAY_RECORD /*608*/:
                UriParameter parameters4 = new UriParameter();
                parameters4.add("ordernum", this.orderId);
                return parameters4;
            case HttpRequestParameters.BACK_MONEY /*609*/:
                break;
            case HttpRequestParameters.UPDATE_PAY_RECORD /*610*/:
                this.progressDialog.setCancelable(false);
                this.progressDialog.setMessage(getResources().getString(R.string.ok_condition_update));
                this.progressDialog.show();
                break;
        }
        UriParameter parameters5 = new UriParameter();
        parameters5.add("opcorderid", this.opcorderid);
        parameters5.add("ordernum", this.orderId);
        return parameters5;
    }

    private void createDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.hint_title));
        builder.setMessage(getResources().getString(R.string.hint_dialog_info));
        builder.setPositiveButton(getResources().getString(R.string.btn_back_money), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                SelectPayWayActivity.this.backMoney();
            }
        });
        builder.setNeutralButton(getResources().getString(R.string.btn_register), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                SelectPayWayActivity.this.doRegister();
            }
        });
        builder.setCancelable(false);
        builder.create().show();
    }

    /* access modifiers changed from: private */
    public void backMoney() {
        this.progressDialog.setMessage(getResources().getString(R.string.hint_back_money));
        this.progressDialog.setCancelable(false);
        this.progressDialog.show();
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.BACK_MONEY), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.BACK_MONEY)));
    }

    /* access modifiers changed from: private */
    public void doRegister() {
        this.progressDialog.setMessage(getResources().getString(R.string.hint_register));
        this.progressDialog.setCancelable(false);
        this.progressDialog.show();
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.SUBMIT_ORDER), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.SUBMIT_ORDER)));
    }
}
