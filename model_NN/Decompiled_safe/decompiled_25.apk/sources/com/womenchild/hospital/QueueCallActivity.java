package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.adapter.QueueCallFragmentPagerAdapter;
import com.womenchild.hospital.base.BaseRequestFragmentActivity;
import com.womenchild.hospital.configure.Constants;
import com.womenchild.hospital.entity.QueueCallVisEntity;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.request.RequestTask;
import com.womenchild.hospital.util.ClientLogUtil;
import com.womenchild.hospital.util.Notifier;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONException;
import org.json.JSONObject;

public class QueueCallActivity extends BaseRequestFragmentActivity implements View.OnClickListener {
    private static final int COUNT_DOWN = 0;
    private static final String TAG = "QCActivity";
    public static ArrayList<Fragment> fragmentsList;
    public static ViewPager mPager;
    /* access modifiers changed from: private */
    public int currIndex = 0;
    private int currentIndex = 0;
    private ImageView iv_check;
    private Button iv_return_home;
    private ImageView iv_take_medicine;
    private ImageView iv_test;
    private ImageView iv_vis;
    private ProgressDialog pDialog;
    private SharedPreferences sharedPrefs;
    private TextView tv_check;
    /* access modifiers changed from: private */
    public Button tv_fresh;
    private TextView tv_take_medicine;
    private TextView tv_test;
    private TextView tv_vis;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        ClientLogUtil.v(TAG, "onCreate()");
        this.sharedPrefs = getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, 0);
        Intent intent = getIntent();
        if (intent != null) {
            this.currentIndex = intent.getIntExtra("index", 0);
        }
        setContentView((int) R.layout.queue_call);
        initViewId();
        initViewPager();
        initClickListener();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void initViewId() {
        this.iv_return_home = (Button) findViewById(R.id.iv_return_home);
        this.tv_fresh = (Button) findViewById(R.id.tv_fresh);
        this.tv_vis = (TextView) findViewById(R.id.tv_vis);
        this.tv_check = (TextView) findViewById(R.id.tv_check);
        this.tv_test = (TextView) findViewById(R.id.tv_test);
        this.tv_take_medicine = (TextView) findViewById(R.id.tv_take_medicine);
        this.iv_vis = (ImageView) findViewById(R.id.iv_vis);
        this.iv_check = (ImageView) findViewById(R.id.iv_check);
        this.iv_test = (ImageView) findViewById(R.id.iv_test);
        this.iv_take_medicine = (ImageView) findViewById(R.id.iv_take_medicine);
        if (this.currentIndex == 1) {
            this.iv_return_home.setText(PoiTypeDef.All);
            this.iv_return_home.setBackgroundResource(R.drawable.back_selector);
        }
    }

    public void initClickListener() {
        this.iv_return_home.setOnClickListener(this);
        this.tv_fresh.setOnClickListener(this);
        this.tv_vis.setOnClickListener(new MyOnClickListener(0));
        this.tv_check.setOnClickListener(new MyOnClickListener(1));
        this.tv_test.setOnClickListener(new MyOnClickListener(2));
        this.tv_take_medicine.setOnClickListener(new MyOnClickListener(3));
    }

    public class MyOnClickListener implements View.OnClickListener {
        private int index = 0;

        public MyOnClickListener(int i) {
            this.index = i;
        }

        public void onClick(View v) {
            QueueCallActivity.mPager.setCurrentItem(this.index);
            QueueCallActivity.this.switchImage(this.index);
        }
    }

    public class MyOnPageChangeListener implements ViewPager.OnPageChangeListener {
        public MyOnPageChangeListener() {
        }

        public void onPageSelected(int arg0) {
            QueueCallActivity.this.currIndex = arg0;
            QueueCallActivity.this.switchImage(QueueCallActivity.this.currIndex);
        }

        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        public void onPageScrollStateChanged(int arg0) {
        }
    }

    private void initViewPager() {
        mPager = (ViewPager) findViewById(R.id.vPager);
        fragmentsList = new ArrayList<>();
        Fragment visFragment = new VisFragment();
        Fragment checkFragment = new CheckFragment();
        Fragment testFragment = new TestFragment();
        Fragment takeFragement = new TakeMedicineFragment();
        fragmentsList.add(visFragment);
        fragmentsList.add(checkFragment);
        fragmentsList.add(testFragment);
        fragmentsList.add(takeFragement);
        mPager.setAdapter(new QueueCallFragmentPagerAdapter(getSupportFragmentManager(), fragmentsList));
        mPager.setCurrentItem(0);
        mPager.setOnPageChangeListener(new MyOnPageChangeListener());
        switchImage(0);
    }

    /* access modifiers changed from: private */
    public void switchImage(int index) {
        switch (index) {
            case 0:
                this.tv_vis.setTextColor(getResources().getColor(R.color.white));
                this.tv_check.setTextColor(getResources().getColor(R.color.black));
                this.tv_test.setTextColor(getResources().getColor(R.color.black));
                this.tv_take_medicine.setTextColor(getResources().getColor(R.color.black));
                this.iv_vis.setBackgroundResource(R.drawable.ygkz_number_blank_blue_1);
                this.iv_check.setBackgroundResource(R.drawable.ygkz_number_blank_withline_2);
                this.iv_test.setBackgroundResource(R.drawable.ygkz_number_blank_withline_2);
                this.iv_take_medicine.setBackgroundResource(R.drawable.ygkz_number_blank_withline_3);
                return;
            case 1:
                this.tv_vis.setTextColor(getResources().getColor(R.color.black));
                this.tv_check.setTextColor(getResources().getColor(R.color.white));
                this.tv_test.setTextColor(getResources().getColor(R.color.black));
                this.tv_take_medicine.setTextColor(getResources().getColor(R.color.black));
                this.iv_vis.setBackgroundResource(R.drawable.ygkz_number_blank_withline_1);
                this.iv_check.setBackgroundResource(R.drawable.ygkz_number_blank_blue_2);
                this.iv_test.setBackgroundResource(R.drawable.ygkz_number_blank_withline_2);
                this.iv_take_medicine.setBackgroundResource(R.drawable.ygkz_number_blank_withline_3);
                return;
            case 2:
                this.tv_vis.setTextColor(getResources().getColor(R.color.black));
                this.tv_check.setTextColor(getResources().getColor(R.color.black));
                this.tv_test.setTextColor(getResources().getColor(R.color.white));
                this.tv_take_medicine.setTextColor(getResources().getColor(R.color.black));
                this.iv_vis.setBackgroundResource(R.drawable.ygkz_number_blank_withline_1);
                this.iv_check.setBackgroundResource(R.drawable.ygkz_number_blank_withline_2);
                this.iv_test.setBackgroundResource(R.drawable.ygkz_number_blank_blue_2);
                this.iv_take_medicine.setBackgroundResource(R.drawable.ygkz_number_blank_withline_3);
                return;
            case 3:
                this.tv_vis.setTextColor(getResources().getColor(R.color.black));
                this.tv_check.setTextColor(getResources().getColor(R.color.black));
                this.tv_test.setTextColor(getResources().getColor(R.color.black));
                this.tv_take_medicine.setTextColor(getResources().getColor(R.color.white));
                this.iv_vis.setBackgroundResource(R.drawable.ygkz_number_blank_withline_1);
                this.iv_check.setBackgroundResource(R.drawable.ygkz_number_blank_withline_2);
                this.iv_test.setBackgroundResource(R.drawable.ygkz_number_blank_withline_2);
                this.iv_take_medicine.setBackgroundResource(R.drawable.ygkz_number_blank_blue_3);
                return;
            default:
                return;
        }
    }

    public void onClick(View v) {
        if (this.iv_return_home == v) {
            Intent intent = new Intent(this, HomeActivity.class);
            intent.setFlags(67108864);
            startActivity(intent);
            finish();
        } else if (v == this.tv_fresh) {
            this.tv_fresh.setEnabled(false);
            String patientCardID = UserEntity.getInstance().getInfo().getUserid();
            ClientLogUtil.i(TAG, "patientCardID:" + patientCardID);
            sendHttpRequest((int) HttpRequestParameters.PATIENT_CARD_INFO, patientCardID);
        } else {
            if (v == this.tv_vis || v == this.tv_check || v != this.tv_test) {
            }
        }
    }

    public void loadData(int requestType, Object data) {
        JSONObject json = (JSONObject) data;
        JSONObject res = json.optJSONObject("res");
        if (res != null) {
            try {
                if (res.getInt("st") == 0) {
                    if (requestType == 504) {
                        VisFragment.recordList = QueueCallVisEntity.getList(json);
                        if (VisFragment.recordList == null || VisFragment.recordList.size() <= 0) {
                            this.tv_fresh.setEnabled(true);
                            return;
                        }
                        Timer timer = new Timer();
                        timer.schedule(new TimerTask(timer) {
                            Handler mHandler;
                            int time = 180;

                            {
                                this.mHandler = new Handler() {
                                    public void handleMessage(Message msg) {
                                        switch (msg.what) {
                                            case 0:
                                                QueueCallActivity.this.tv_fresh.setText(String.valueOf(AnonymousClass1.this.time) + "s");
                                                QueueCallActivity.this.tv_fresh.setBackgroundResource(R.drawable.ibtn_home_selector);
                                                QueueCallActivity.this.tv_fresh.setTextColor(-1);
                                                QueueCallActivity.this.tv_fresh.setEnabled(false);
                                                if (AnonymousClass1.this.time == 0) {
                                                    r3.cancel();
                                                    QueueCallActivity.this.tv_fresh.setText(QueueCallActivity.this.getResources().getString(R.string.refurbish));
                                                    QueueCallActivity.this.tv_fresh.setEnabled(true);
                                                    return;
                                                }
                                                return;
                                            default:
                                                return;
                                        }
                                    }
                                };
                            }

                            public void run() {
                                this.time--;
                                this.mHandler.sendMessage(this.mHandler.obtainMessage(0));
                            }
                        }, 1000, 1000);
                        Message msg = new Message();
                        msg.what = 0;
                        VisFragment.visHandler.sendMessage(msg);
                        if (this.sharedPrefs.getBoolean(Constants.TREAT_IS_ALERT, false)) {
                            int settingNum = this.sharedPrefs.getInt(Constants.TREAT_SETTING_NUM, -1);
                            int i = 0;
                            for (QueueCallVisEntity entity : VisFragment.recordList) {
                                if (Integer.valueOf(entity.getFrontWaiting()).intValue() == settingNum) {
                                    new Notifier(this).notify(i, null, "您预约了" + entity.getDoctorname() + "医生", "您前面还有" + settingNum + "位,请提前15分钟到达诊室门口等候，谢谢", null);
                                }
                                i++;
                            }
                            return;
                        }
                        return;
                    }
                    return;
                }
            } catch (JSONException e) {
                e.printStackTrace();
                return;
            }
        }
        this.tv_fresh.setEnabled(true);
        Toast.makeText(this, res.getString("msg"), 0).show();
    }

    public void sendHttpRequest(int requsetType, String cardID) {
        this.pDialog = new ProgressDialog(this);
        this.pDialog.setMessage(getResources().getString(R.string.loading_data));
        this.pDialog.show();
        RequestTask.getInstance().sendHttpRequest(this, String.valueOf(requsetType), initRequestParams(cardID));
    }

    public UriParameter initRequestParams(String cardID) {
        UriParameter parameters = new UriParameter();
        parameters.add("userid", cardID);
        parameters.add("hospitalid", Constants.HOSPITAL_ID);
        return parameters;
    }

    /* access modifiers changed from: protected */
    public void initViewId(View view) {
    }

    /* access modifiers changed from: protected */
    public void initUIData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        return null;
    }

    public void refreshFragment(Object... params) {
        this.pDialog.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            loadData(requestType, (JSONObject) params[2]);
        } else {
            Toast.makeText(this, (int) R.string.network_connect_failed_prompt, 0).show();
        }
    }
}
