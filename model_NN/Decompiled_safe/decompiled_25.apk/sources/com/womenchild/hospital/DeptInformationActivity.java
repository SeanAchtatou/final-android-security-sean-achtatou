package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.umeng.common.util.e;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.WebUtil;
import com.womenchild.hospital.view.SSLWebViewClient;
import org.json.JSONObject;

public class DeptInformationActivity extends BaseRequestActivity implements View.OnClickListener {
    private String deptName;
    private int deptid;
    private Button ivBack;
    private ProgressDialog pDialog;
    private TextView tvDept;
    private TextView tvHospital;
    private TextView tvOrder;
    private WebView wvInfo;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.department_information);
        initViewId();
        initClickListener();
        initData();
    }

    public void refreshActivity(Object... params) {
        this.pDialog.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject result = (JSONObject) params[2];
            if (result.optJSONObject("res").optInt("st") == 0) {
                loadData(requestType, result.optJSONObject("inf"));
            } else {
                Toast.makeText(this, result.optJSONObject("res").optString("msg"), 0).show();
            }
        } else {
            Toast.makeText(this, getResources().getString(R.string.network_connect_failed_prompt), 0).show();
        }
    }

    public void initViewId() {
        this.tvHospital = (TextView) findViewById(R.id.tv_hospital);
        this.tvOrder = (TextView) findViewById(R.id.tv_order);
        this.ivBack = (Button) findViewById(R.id.iv_back);
        this.wvInfo = (WebView) findViewById(R.id.wv_content);
        this.wvInfo.setWebViewClient(new SSLWebViewClient());
        this.tvDept = (TextView) findViewById(R.id.tv_department);
    }

    public void initClickListener() {
        this.tvOrder.setOnClickListener(this);
        this.ivBack.setOnClickListener(this);
    }

    public void loadData(int requestType, Object json) {
        JSONObject inf = (JSONObject) json;
        this.tvHospital.setText(inf.optString("hospitalname"));
        this.tvDept.setText(this.deptName);
        this.wvInfo.loadDataWithBaseURL(null, WebUtil.sourceCreateHtml(inf.optString("desc")), "text/html", e.f, null);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back /*2131296529*/:
                finish();
                return;
            case R.id.tv_order /*2131296616*/:
                Intent intent = new Intent(this, SelectDoctorActivity.class);
                intent.putExtra("deptid", this.deptid);
                startActivity(intent);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.deptName = getIntent().getStringExtra("deptName");
        this.deptid = getIntent().getIntExtra("deptid", 0);
        this.pDialog = new ProgressDialog(this);
        this.pDialog.setMessage(getResources().getString(R.string.loading_data));
        this.pDialog.show();
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.DEPT_DETAIL), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.DEPT_DETAIL)));
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        parameters.add("deptid", Integer.valueOf(this.deptid));
        return parameters;
    }
}
