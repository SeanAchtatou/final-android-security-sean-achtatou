package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.womenchild.hospital.adapter.QueueCallVisAdapter;
import com.womenchild.hospital.base.BaseRequestFragment;
import com.womenchild.hospital.configure.Constants;
import com.womenchild.hospital.entity.QueueCallVisEntity;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.request.RequestTask;
import com.womenchild.hospital.util.ClientLogUtil;
import com.womenchild.hospital.util.Notifier;
import com.womenchild.hospital.util.ShareUtil;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class VisFragment extends BaseRequestFragment implements View.OnClickListener {
    private static final String TAG = "VFrag";
    private static ListView listView;
    /* access modifiers changed from: private */
    public static QueueCallVisAdapter noClinicAdapter;
    public static List<QueueCallVisEntity> recordList;
    public static Handler visHandler = new Handler() {
        public void handleMessage(Message msg) {
            ClientLogUtil.v(VisFragment.TAG, "visHandler handleMessage()");
            if (msg.what == 0) {
                VisFragment.noClinicAdapter.notifyDataSetChanged();
            }
        }
    };
    private View headerView;
    private ImageButton ibtn_department_guide;
    private ImageButton ibtn_overdue_help;
    private ImageButton ibtn_remind_set;
    private ImageButton ibtn_share;
    private Intent intent;
    private TextView noDataTv;
    private ProgressDialog pDialog;
    private SharedPreferences sharedPrefs;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ClientLogUtil.v(TAG, "onCreate()");
        this.sharedPrefs = getActivity().getBaseContext().getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ClientLogUtil.v(TAG, "onCreateView()");
        View view = inflater.inflate((int) R.layout.vis, container, false);
        this.ibtn_department_guide = (ImageButton) view.findViewById(R.id.ibtn_department_guide);
        this.ibtn_overdue_help = (ImageButton) view.findViewById(R.id.ibtn_overdue_help);
        this.ibtn_remind_set = (ImageButton) view.findViewById(R.id.ibtn_remind_set);
        this.ibtn_share = (ImageButton) view.findViewById(R.id.ibtn_share);
        listView = (ListView) view.findViewById(R.id.layout_1);
        listView.setSelector(new ColorDrawable(0));
        this.ibtn_department_guide.setOnClickListener(this);
        this.ibtn_overdue_help.setOnClickListener(this);
        this.ibtn_remind_set.setOnClickListener(this);
        this.ibtn_share.setOnClickListener(this);
        this.noDataTv = (TextView) view.findViewById(R.id.tv_not_data);
        this.headerView = inflater.inflate((int) R.layout.queue_call_tips_list_header, (ViewGroup) null);
        listView.addHeaderView(this.headerView);
        sendHttpRequest((int) HttpRequestParameters.PATIENT_CARD_INFO, UserEntity.getInstance().getInfo().getUserid());
        return view;
    }

    public void onClick(View v) {
        if (v == this.ibtn_department_guide) {
            this.intent = new Intent(getActivity(), HospitalGuideActivity.class);
            this.intent.putExtra("index", 1);
            startActivity(this.intent);
        } else if (v == this.ibtn_overdue_help) {
            this.intent = new Intent(getActivity(), OverDueHelpActivity.class);
            this.intent.putExtra("index", 5);
            startActivity(this.intent);
        } else if (v == this.ibtn_remind_set) {
            this.intent = new Intent(getActivity(), RemindSetActivity.class);
            this.intent.putExtra("remindtype", 2);
            startActivityForResult(this.intent, 1);
        } else if (v == this.ibtn_share) {
            ShareUtil.share2Friend(getActivity(), R.string.app_name);
        }
    }

    public void sendHttpRequest(int requsetType, String cardID) {
        this.pDialog = new ProgressDialog(getActivity());
        this.pDialog.setMessage(getResources().getString(R.string.loading_data));
        this.pDialog.show();
        RequestTask.getInstance().sendHttpRequest(this, String.valueOf(requsetType), initRequestParams(cardID));
    }

    public void initViewId() {
    }

    public void initClickListener() {
    }

    public UriParameter initRequestParams(String cardID) {
        UriParameter parameters = new UriParameter();
        parameters.add("userid", cardID);
        parameters.add("hospitalid", Constants.HOSPITAL_ID);
        return parameters;
    }

    public void loadData(int requestType, Object data) {
        JSONObject json = (JSONObject) data;
        JSONObject res = json.optJSONObject("res");
        if (res != null) {
            try {
                if (res.getInt("st") != 0) {
                    return;
                }
                if (requestType == 504) {
                    recordList = QueueCallVisEntity.getList(json);
                    noClinicAdapter = new QueueCallVisAdapter(getActivity(), recordList);
                    listView.setAdapter((ListAdapter) noClinicAdapter);
                    if (recordList == null || recordList.size() <= 0) {
                        this.noDataTv.setVisibility(0);
                    } else if (this.sharedPrefs.getBoolean(Constants.TREAT_IS_ALERT, false)) {
                        int settingNum = this.sharedPrefs.getInt(Constants.TREAT_SETTING_NUM, -1);
                        int i = 0;
                        for (QueueCallVisEntity entity : recordList) {
                            if (Integer.valueOf(entity.getFrontWaiting()).intValue() == settingNum) {
                                new Notifier(getActivity().getBaseContext()).notify(i, null, String.valueOf(getResources().getString(R.string.select_user_await)) + entity.getDoctorname() + getResources().getString(R.string.doctor), String.valueOf(getResources().getString(R.string.before_user)) + settingNum + getResources().getString(R.string.before_user_number), null);
                            }
                            i++;
                        }
                    }
                } else {
                    Toast.makeText(getActivity(), res.getString("msg"), 0).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void initViewId(View view) {
    }

    /* access modifiers changed from: protected */
    public void initUIData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        return null;
    }

    public void refreshFragment(Object... params) {
        this.pDialog.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            loadData(requestType, (JSONObject) params[2]);
        } else {
            Toast.makeText(getActivity(), (int) R.string.network_connect_failed_prompt, 0).show();
        }
    }
}
