package com.womenchild.hospital;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class TakeReportAddressActivity extends Activity implements View.OnClickListener {
    private static final String TAG = "TakeReportAddressActivity";
    private ImageView iv_back;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.take_report_address);
        initViewId();
        initClickListener();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void initViewId() {
        this.iv_back = (ImageView) findViewById(R.id.iv_back);
    }

    public void initClickListener() {
        this.iv_back.setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onClick(View v) {
        if (this.iv_back == v) {
            finish();
        }
    }
}
