package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.configure.Constants;
import com.womenchild.hospital.entity.CardType;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.ClientLogUtil;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public class AddMedicalCardActivity extends BaseRequestActivity implements View.OnClickListener {
    public static final String PATIENTCARDTYPE = "97";
    private static final String TAG = "AddMedicalCardActivity";
    private Button btn_cancle;
    private List<CardType> cardTypes;
    private String cards;
    private EditText et_card_no;
    private String patientId;
    private String patientName;
    private ProgressDialog pd;
    private int position;
    private Spinner sp_card_type;
    private TextView tv_name;
    private TextView tv_submit;
    private int userno;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.add_medical_card);
        initViewId();
        initClickListener();
        initData();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancle /*2131296444*/:
                finish();
                return;
            case R.id.tv_submit /*2131296445*/:
                if (!checkBlankBox()) {
                    hideSoftInput();
                    sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.ADD_PATIENT_CARD), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.ADD_PATIENT_CARD)));
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void hideSoftInput() {
        ((InputMethodManager) this.et_card_no.getContext().getSystemService("input_method")).hideSoftInputFromWindow(this.et_card_no.getWindowToken(), 0);
    }

    public void refreshActivity(Object... params) {
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject result = (JSONObject) params[2];
            ClientLogUtil.i(TAG, result.toString());
            switch (requestType) {
                case HttpRequestParameters.ADD_PATIENT_CARD /*109*/:
                    loadData(HttpRequestParameters.ADD_PATIENT_CARD, result);
                    break;
                case HttpRequestParameters.READ_CARD_TYPE /*126*/:
                    loadData(HttpRequestParameters.READ_CARD_TYPE, result);
                    break;
            }
        } else {
            switch (requestType) {
                case HttpRequestParameters.READ_CARD_TYPE /*126*/:
                    ininCardType(this.cardTypes);
                    break;
            }
            Toast.makeText(this, getResources().getString(R.string.network_connect_failed_prompt), 0).show();
        }
        this.pd.dismiss();
    }

    public void initViewId() {
        this.btn_cancle = (Button) findViewById(R.id.btn_cancle);
        this.tv_name = (TextView) findViewById(R.id.tv_name);
        this.tv_submit = (TextView) findViewById(R.id.tv_submit);
        this.et_card_no = (EditText) findViewById(R.id.et_card_no);
        this.sp_card_type = (Spinner) findViewById(R.id.sp_card_type);
    }

    public void initClickListener() {
        this.btn_cancle.setOnClickListener(this);
        this.tv_submit.setOnClickListener(this);
    }

    public UriParameter initRequestParameter(Object requestType) {
        UriParameter parameters = new UriParameter();
        switch (Integer.parseInt(String.valueOf(requestType))) {
            case HttpRequestParameters.ADD_PATIENT_CARD /*109*/:
                this.pd.setMessage(getResources().getString(R.string.add_loading));
                this.cards = this.et_card_no.getText().toString();
                String patientcardtype = this.cardTypes.get(this.position - 1).getKey();
                parameters.add("patientid", this.patientId);
                parameters.add("cardid", PoiTypeDef.All);
                parameters.add("cards", this.cards);
                parameters.add("hospital", getResources().getString(R.string.add_womenchild));
                parameters.add("patientcardtype", patientcardtype);
                break;
            case HttpRequestParameters.READ_CARD_TYPE /*126*/:
                this.pd.setMessage(getResources().getString(R.string.obtain_type));
                parameters.add("hospitalid", Constants.HOSPITAL_ID);
                break;
        }
        return parameters;
    }

    public void loadData(int requestType, Object data) {
        JSONObject result = (JSONObject) data;
        String st = result.optJSONObject("res").optString("st");
        String msg = result.optJSONObject("res").optString("msg");
        ClientLogUtil.i(TAG, result.toString());
        switch (requestType) {
            case HttpRequestParameters.ADD_PATIENT_CARD /*109*/:
                if ("0".equals(st)) {
                    Toast.makeText(this, msg, 0).show();
                    finish();
                    return;
                }
                Toast.makeText(this, msg, 0).show();
                return;
            case HttpRequestParameters.READ_CARD_TYPE /*126*/:
                if ("0".equals(st)) {
                    this.cardTypes = CardType.getList(result);
                    ininCardType(this.cardTypes);
                    return;
                }
                Toast.makeText(this, getResources().getString(R.string.network_connect_failed_prompt), 0).show();
                return;
            default:
                return;
        }
    }

    private void ininCardType(List<CardType> cardTypes2) {
        ArrayList<String> strings = new ArrayList<>();
        strings.add(getResources().getString(R.string.choice_loading));
        if (cardTypes2 != null) {
            int length = cardTypes2.size();
            for (int i = 0; i < length; i++) {
                strings.add(cardTypes2.get(i).getValue());
            }
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, (int) R.layout.spinner_item, strings);
        adapter.setDropDownViewResource(17367049);
        this.sp_card_type.setAdapter((SpinnerAdapter) adapter);
    }

    private boolean checkBlankBox() {
        String cardNo = this.et_card_no.getText().toString().trim();
        this.position = this.sp_card_type.getSelectedItemPosition();
        if (this.position == 0) {
            Toast.makeText(this, getResources().getString(R.string.choice_loadings), 0).show();
            return true;
        } else if (PoiTypeDef.All.equals(cardNo)) {
            Toast.makeText(this, getResources().getString(R.string.choice_ip_num), 0).show();
            this.et_card_no.requestFocus();
            return true;
        } else if (cardNo.length() >= 8) {
            return false;
        } else {
            Toast.makeText(this, getResources().getString(R.string.ip_unm_maximum), 0).show();
            this.et_card_no.requestFocus();
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.pd = new ProgressDialog(this);
        Intent intent = getIntent();
        this.patientId = intent.getStringExtra("patientid");
        this.patientName = intent.getStringExtra("patientname");
        if (this.patientId == null || PoiTypeDef.All.equals(this.patientId)) {
            this.userno = intent.getIntExtra("userno", 0);
            String userName = MedicalCardManagementActivity.list.get(this.userno).getPatientname();
            this.patientId = MedicalCardManagementActivity.list.get(this.userno).getPatientid();
            this.tv_name.setText(userName);
        } else {
            this.tv_name.setText(this.patientName);
        }
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.READ_CARD_TYPE), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.READ_CARD_TYPE)));
        this.pd.show();
    }
}
