package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import com.womenchild.hospital.adapter.HospitalHelpAdapter;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import org.json.JSONArray;
import org.json.JSONObject;

public class HospitalHelpActivity extends BaseRequestActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    private Button iv_return_home;
    private JSONArray jsons;
    private ListView lv_context;
    private ProgressDialog pdDialog;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.hospital_help);
        initViewId();
        initClickListener();
        this.pdDialog = new ProgressDialog(this);
        this.pdDialog.setMessage(getResources().getString(R.string.loading_data));
        this.pdDialog.show();
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.HOSPITAL_HELP_LIST), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.HOSPITAL_HELP_LIST)));
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void refreshActivity(Object... params) {
        this.pdDialog.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject json = (JSONObject) params[2];
            if (json.optJSONObject("res").optInt("st") == 0) {
                loadData(requestType, json.optJSONObject("inf").optJSONArray("notices"));
            } else {
                Toast.makeText(this, json.optJSONObject("res").optString("msg"), 0).show();
            }
        } else {
            Toast.makeText(this, getResources().getString(R.string.network_connect_failed_prompt), 0).show();
        }
    }

    public void initViewId() {
        this.iv_return_home = (Button) findViewById(R.id.iv_return_home);
        this.lv_context = (ListView) findViewById(R.id.lv_context);
    }

    public void initClickListener() {
        this.iv_return_home.setOnClickListener(this);
        this.lv_context.setOnItemClickListener(this);
    }

    public void onClick(View v) {
        if (this.iv_return_home == v) {
            finish();
        }
    }

    public void loadData(int requestType, Object data) {
        this.jsons = (JSONArray) data;
        switch (requestType) {
            case HttpRequestParameters.HOSPITAL_HELP_LIST /*121*/:
                this.lv_context.setAdapter((ListAdapter) new HospitalHelpAdapter(this, this.jsons));
                return;
            default:
                return;
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
        Intent intent = new Intent(this, HospitalHelpContentActivity.class);
        intent.putExtra("id", this.jsons.optJSONObject(arg2).optString("id"));
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.HOSPITAL_HELP_LIST /*121*/:
                parameters.add("num", 3);
                break;
        }
        return parameters;
    }
}
