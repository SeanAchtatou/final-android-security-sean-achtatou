package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.womenchild.hospital.adapter.ClinicAdapter;
import com.womenchild.hospital.adapter.MyRegisterNoFragmentPagerAdapter;
import com.womenchild.hospital.adapter.NoClinicAdapter;
import com.womenchild.hospital.base.BaseRequestFragmentActivity;
import com.womenchild.hospital.entity.RecordEntity;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.AppParameters;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.ClientLogUtil;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public class MyRegisterNoRecordActivity extends BaseRequestFragmentActivity implements View.OnClickListener {
    private static final String TAG = "MyRegisterNoRecordActivity";
    public static List<RecordEntity> clinicList;
    public static List<RecordEntity> noClinicList;
    private static ProgressDialog pd;
    public static boolean refresh = false;
    private Button btn_back;
    /* access modifiers changed from: private */
    public int currentIndex = 0;
    private ArrayList<Fragment> fragmentsList;
    private Resources resources;
    private TextView tv_clinic;
    private TextView tv_not_clinic;
    private TextView tv_register_no;
    private ViewPager viewPager;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.my_register_no_record);
        initViewId();
        initClickListener();
        initViewPager();
        initData();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (refresh) {
            Log.i("refresh", "refresh");
            refresh = false;
            pd.show();
            sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.READ_REG_LIST), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.READ_REG_LIST)));
        }
    }

    private void initData() {
        pd = new ProgressDialog(this);
        pd.setCancelable(true);
        pd.setCanceledOnTouchOutside(false);
        pd.setMessage(getResources().getString(R.string.mark_wait));
    }

    private void initViewPager() {
        this.resources = getResources();
        this.fragmentsList = new ArrayList<>(2);
        Fragment notClinicFragment = new NotClinicFragment();
        Fragment clicFragment = new ClinicFragment();
        this.fragmentsList.add(notClinicFragment);
        this.fragmentsList.add(clicFragment);
        this.viewPager.setAdapter(new MyRegisterNoFragmentPagerAdapter(getSupportFragmentManager(), this.fragmentsList));
        this.viewPager.setCurrentItem(this.currentIndex);
        this.viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageSelected(int arg0) {
                MyRegisterNoRecordActivity.this.currentIndex = arg0;
                MyRegisterNoRecordActivity.this.switchImage(MyRegisterNoRecordActivity.this.currentIndex);
            }

            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            public void onPageScrollStateChanged(int arg0) {
            }
        });
        switchImage(this.currentIndex);
    }

    /* access modifiers changed from: private */
    public void switchImage(int currentIndex2) {
        switch (currentIndex2) {
            case 0:
                this.tv_not_clinic.setTextColor(this.resources.getColor(R.color.white));
                this.tv_clinic.setTextColor(this.resources.getColor(R.color.black));
                this.tv_not_clinic.setBackgroundResource(R.drawable.ygkz_number_blank_blue_1);
                this.tv_clinic.setBackgroundResource(R.drawable.ygkz_number_blank_withline_3);
                return;
            case 1:
                this.tv_not_clinic.setTextColor(this.resources.getColor(R.color.black));
                this.tv_clinic.setTextColor(this.resources.getColor(R.color.white));
                this.tv_not_clinic.setBackgroundResource(R.drawable.ygkz_number_blank_withline_1);
                this.tv_clinic.setBackgroundResource(R.drawable.ygkz_number_blank_blue_3);
                return;
            default:
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back /*2131296535*/:
                finish();
                return;
            case R.id.tv_register_no /*2131296921*/:
                Intent intent = new Intent(this, RegisterNoActivity.class);
                intent.putExtra("index", 1);
                intent.putExtra("backFlag", true);
                startActivity(intent);
                return;
            case R.id.tv_not_clinic /*2131296923*/:
                switchImage(0);
                this.viewPager.setCurrentItem(0);
                return;
            case R.id.tv_clinic /*2131296924*/:
                switchImage(1);
                this.viewPager.setCurrentItem(1);
                return;
            default:
                return;
        }
    }

    public void initViewId() {
        this.btn_back = (Button) findViewById(R.id.btn_back);
        this.tv_register_no = (TextView) findViewById(R.id.tv_register_no);
        this.viewPager = (ViewPager) findViewById(R.id.viewPager);
        this.tv_not_clinic = (TextView) findViewById(R.id.tv_not_clinic);
        this.tv_clinic = (TextView) findViewById(R.id.tv_clinic);
    }

    public void initClickListener() {
        this.btn_back.setOnClickListener(this);
        this.tv_register_no.setOnClickListener(this);
        this.tv_not_clinic.setOnClickListener(this);
        this.tv_clinic.setOnClickListener(this);
    }

    public void loadData(int requestType, Object data) {
        if (pd.isShowing()) {
            pd.dismiss();
        }
        JSONObject result = (JSONObject) data;
        String st = result.optJSONObject("res").optString("st");
        String optString = result.optJSONObject("res").optString("msg");
        switch (requestType) {
            case HttpRequestParameters.READ_REG_LIST /*125*/:
                if ("0".equals(st)) {
                    bindView(RecordEntity.getList(result));
                    return;
                }
                ClinicFragment.tv_clinic.setVisibility(0);
                NotClinicFragment.tv_not_clinic.setVisibility(0);
                return;
            default:
                return;
        }
    }

    private void bindView(List<RecordEntity> list) {
        if (list != null) {
            clinicList = new ArrayList();
            noClinicList = new ArrayList();
            long currentTime = System.currentTimeMillis();
            for (int i = 0; i < list.size(); i++) {
                RecordEntity entity = list.get(i);
                if (currentTime > Long.valueOf(entity.getPlanstoptime()).longValue()) {
                    clinicList.add(entity);
                } else {
                    noClinicList.add(entity);
                }
            }
            if (clinicList == null || clinicList.size() <= 0) {
                ClinicFragment.tv_clinic.setVisibility(0);
            } else {
                ClinicFragment.listView.setAdapter((ListAdapter) new ClinicAdapter(this, clinicList));
            }
            if (noClinicList == null || noClinicList.size() <= 0) {
                NotClinicFragment.tv_not_clinic.setVisibility(0);
            } else {
                NotClinicFragment.listview.setAdapter((ListAdapter) new NoClinicAdapter(this, noClinicList));
            }
            setOnItemListener();
        }
    }

    private void setOnItemListener() {
        ClinicFragment.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                Intent intent = new Intent(MyRegisterNoRecordActivity.this, ClinicDelActivity.class);
                intent.putExtra("position", arg2);
                MyRegisterNoRecordActivity.this.startActivity(intent);
            }
        });
        NotClinicFragment.listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                Intent intent = new Intent(MyRegisterNoRecordActivity.this, NoClinicDelActivity.class);
                intent.putExtra("position", arg2);
                MyRegisterNoRecordActivity.this.startActivity(intent);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void initViewId(View view) {
    }

    /* access modifiers changed from: protected */
    public void initUIData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.READ_REG_LIST /*125*/:
                parameters.add("memberid", String.valueOf(AppParameters.getAPPID()) + "_" + UserEntity.getInstance().getInfo().getUserid());
                break;
        }
        return parameters;
    }

    public void refreshFragment(Object... params) {
        if (pd.isShowing()) {
            pd.dismiss();
        }
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject result = (JSONObject) params[2];
            ClientLogUtil.i(TAG, result.toString());
            switch (requestType) {
                case HttpRequestParameters.READ_REG_LIST /*125*/:
                    loadData(HttpRequestParameters.READ_REG_LIST, result);
                    return;
                default:
                    return;
            }
        } else {
            Toast.makeText(this, (int) R.string.network_connect_failed_prompt, 0).show();
        }
    }
}
