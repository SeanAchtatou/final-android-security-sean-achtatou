package com.womenchild.hospital.http;

import com.umeng.common.util.e;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;

public class HttpConnector {
    private static final int CONNECTION_TIMEOUT = 10000;
    private static final int SCOKET_TIMEOUT = 30000;
    private static HttpConnector httpConnector = null;

    private HttpConnector() {
    }

    public static HttpConnector getInstance() {
        if (httpConnector == null) {
            httpConnector = new HttpConnector();
        }
        return httpConnector;
    }

    private HttpUriRequest getHttpPost(String uri, HttpEntity entity) {
        HttpUriRequest httpPost = new HttpPost(uri);
        httpPost.setEntity(entity);
        return httpPost;
    }

    private HttpUriRequest getHttpGet(String uri) {
        return new HttpGet(uri);
    }

    private HttpResponse getResponse(HttpClient httpClient, HttpUriRequest request) {
        try {
            HttpResponse response = httpClient.execute(request);
            if (response.getStatusLine().getStatusCode() != 200) {
                return null;
            }
            return response;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            return null;
        } catch (SocketTimeoutException e2) {
            e2.printStackTrace();
            return null;
        } catch (IOException e3) {
            e3.printStackTrace();
            return null;
        }
    }

    private HttpClient getHttpClient() {
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 10000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, SCOKET_TIMEOUT);
        return new DefaultHttpClient(basicHttpParams);
    }

    private HttpClient getSSLHttpClient() {
        try {
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(basicHttpParams, 10000);
            HttpConnectionParams.setSoTimeout(basicHttpParams, SCOKET_TIMEOUT);
            HttpProtocolParams.setVersion(basicHttpParams, HttpVersion.HTTP_1_0);
            HttpProtocolParams.setContentCharset(basicHttpParams, e.f);
            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            keyStore.load(null, null);
            SSLSocketFactory ssf = new SSLSocketFactoryEx(keyStore);
            ssf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", ssf, 443));
            return new DefaultHttpClient(new ThreadSafeClientConnManager(basicHttpParams, registry), basicHttpParams);
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e2) {
            e2.printStackTrace();
        } catch (CertificateException e3) {
            e3.printStackTrace();
        } catch (IOException e4) {
            e4.printStackTrace();
        } catch (KeyManagementException e5) {
            e5.printStackTrace();
        } catch (UnrecoverableKeyException e6) {
            e6.printStackTrace();
        }
        return null;
    }

    public String sendRequest(String uri) {
        return getResponseResult(uri, null, false);
    }

    public String sendRequest(String uri, boolean SSLFlag) {
        return getResponseResult(uri, null, SSLFlag);
    }

    public String sendRequest(String uri, HttpEntity entity) {
        return getResponseResult(uri, entity, false);
    }

    public String sendRequest(String uri, HttpEntity entity, boolean SSLFlag) {
        return getResponseResult(uri, entity, SSLFlag);
    }

    private String getResponseResult(String uri, HttpEntity entity, boolean SSLFlag) {
        HttpUriRequest httpRequest;
        HttpClient httpClient;
        if (entity == null) {
            httpRequest = getHttpGet(uri);
        } else {
            httpRequest = getHttpPost(uri, entity);
        }
        if (SSLFlag) {
            httpClient = getSSLHttpClient();
        } else {
            httpClient = getHttpClient();
        }
        httpRequest.addHeader("User-Agent", "fezx_android_version_1.0.2");
        HttpResponse response = getResponse(httpClient, httpRequest);
        if (response != null) {
            try {
                return EntityUtils.toString(response.getEntity());
            } catch (ParseException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        return null;
    }

    public HttpClient getHttpClient(boolean sslFlag) {
        if (sslFlag) {
            return getSSLHttpClient();
        }
        return getHttpClient();
    }
}
