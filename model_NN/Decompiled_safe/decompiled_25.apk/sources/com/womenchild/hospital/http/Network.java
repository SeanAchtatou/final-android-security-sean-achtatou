package com.womenchild.hospital.http;

import android.content.Context;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import com.amap.mapapi.location.LocationManagerProxy;
import java.util.List;

public class Network {
    public static boolean isNetworkAvailable(Context context) {
        NetworkInfo[] infos;
        ConnectivityManager cManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (cManager == null || (infos = cManager.getAllNetworkInfo()) == null) {
            return false;
        }
        for (NetworkInfo state : infos) {
            if (state.getState() == NetworkInfo.State.CONNECTED) {
                return true;
            }
        }
        return false;
    }

    public static boolean isGpsEnabled(Context context) {
        List<String> accessibleProviders = ((LocationManager) context.getSystemService(LocationManagerProxy.KEY_LOCATION_CHANGED)).getAllProviders();
        return accessibleProviders != null && accessibleProviders.size() > 0;
    }

    public static boolean isWifiEnabled(Context context) {
        ConnectivityManager cManager = (ConnectivityManager) context.getSystemService("connectivity");
        return (cManager.getActiveNetworkInfo() != null && cManager.getActiveNetworkInfo().getState() == NetworkInfo.State.CONNECTED) || ((TelephonyManager) context.getSystemService("phone")).getNetworkType() == 3;
    }

    public static boolean is3GEnabled(Context context) {
        NetworkInfo networkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return networkInfo != null && networkInfo.getType() == 0;
    }

    public static boolean isWifiOr3G(Context context) {
        NetworkInfo networkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (networkInfo == null || networkInfo.getType() != 1) {
            return false;
        }
        return true;
    }
}
