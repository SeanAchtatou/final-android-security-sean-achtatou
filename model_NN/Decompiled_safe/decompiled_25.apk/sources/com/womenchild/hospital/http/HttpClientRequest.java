package com.womenchild.hospital.http;

import org.apache.http.HttpEntity;

public class HttpClientRequest {
    private static HttpClientRequest httpClientRequest = null;

    private HttpClientRequest() {
    }

    public static HttpClientRequest getInstance() {
        if (httpClientRequest == null) {
            httpClientRequest = new HttpClientRequest();
        }
        return httpClientRequest;
    }

    public String sendHttpRequest(String uri) {
        return HttpConnector.getInstance().sendRequest(uri);
    }

    public String sendHttpRequest(String uri, boolean SSLFlag) {
        return HttpConnector.getInstance().sendRequest(uri, SSLFlag);
    }

    public String sendHttpRequest(String uri, HttpEntity entity) {
        return HttpConnector.getInstance().sendRequest(uri, entity);
    }

    public String sendHttpRequest(String uri, HttpEntity entity, boolean SSLFlag) {
        return HttpConnector.getInstance().sendRequest(uri, entity, SSLFlag);
    }
}
