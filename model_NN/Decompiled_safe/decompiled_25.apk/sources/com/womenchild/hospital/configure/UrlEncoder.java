package com.womenchild.hospital.configure;

import android.annotation.SuppressLint;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.parameter.AppParameters;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.Base64Util;
import com.womenchild.hospital.util.DateUtil;
import com.womenchild.hospital.util.JsonUtil;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class UrlEncoder {
    @SuppressLint({"UseSparseArrays"})
    private Map<Integer, String> urlMap = new HashMap();

    public UrlEncoder() {
        initUrl();
    }

    public UriParameter initParams(String requestcode, UriParameter parameter) {
        JSONObject json = new JSONObject();
        try {
            UriParameter parameter2 = addValidate(parameter);
            json.put("appinf", AppParameters.appParameterToJson());
            json.put("inf", JsonUtil.getInstance().paramToJsonObject(parameter2));
            json.put("url", this.urlMap.get(Integer.valueOf(Integer.parseInt(requestcode))));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        UriParameter uriParameter = new UriParameter();
        uriParameter.add("params", json.toString());
        return uriParameter;
    }

    private void initUrl() {
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.FAVORITES_DOCTOR_LIST), PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.DELETE_FAVORITES_DOCTOR), PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.LISTPLAN_BY_DEPTID), "resource/listplanbydeptidandtimespan");
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.ADD_FAVORITES_DOCTOR), PoiTypeDef.All);
        this.urlMap.put(307, "resource/listplanbydoctorid");
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.HOSPITAL_TAFFIC_INFO), PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.HOSPITAL_HELP_LIST), PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.HOSPITAL_HELP_CONTTENT), PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.QUESTION_LIST), PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.CONSULTINFO_DETAIL), PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.ADD_REPLY), PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.PATIENT_CARD_INFO), "order/getqueneinfo");
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.QUEUE_TEST_REPORT), "order/getlaboratoryreport");
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.LISTDEPT_BY_HOSPITALID), "info/listdeptbyhospitalid");
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.LISTDOCTOR_BY_DOCTORNAME), "info/listdoctorbydoctorname");
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.DOCTOR_DETAIL), "info/getdoctordetail");
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.LIST_STOPPIAN), "resource/liststopplan");
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.HOSPITAL_NOTICE), PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.REGISTER_LIST), "order/listtraderorderbymember");
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.PAYMENT_LIST), PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.ADD_COMPLAINT_INFO), PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.READ_REG_LIST), "order/listtraderorderbymember");
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.DEPT_DETAIL), "info/getdeptdetail");
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.READ_CARD_TYPE), PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.ADD_PATIENT_INFO), PoiTypeDef.All);
        this.urlMap.put(107, PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.DELETE_PATIENT_INFO), PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.ADD_PATIENT_CARD), PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.EDIT_PATIENT_CARD), PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.DELETE_PATIENT_CARD), PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.PATIENT_LIST), PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.EDIT_DEFAULT_CARD), PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.PATIENT_CARD_LIST), PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.SUBMIT_ORDER), "order/orderandsunservice");
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.UPDATE_PAY_RECORD), PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.ADD_ORDER_RECORD), PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.DELETE_PAY_RECORD), PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.BACK_MONEY), PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.SUBMIT_PAY_ORDER), PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.ORDER_STATUS), "order/cancelorder");
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.CANCEL_ORDER), "order/cancelorder");
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.AUTO_PAY_CONFIRM), "order/confirmautopay");
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.AUTO_PAY_ORDER), "order/getautopayorders");
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.CANCELORDER_AND_BACKMONEY), "order/cancelorder");
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.USER_REGISTER), PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.EDIT_USER_PWD), PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.GET_VERIFY_CODE), PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.ADD_USER_INFO), PoiTypeDef.All);
        this.urlMap.put(100, PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.EDIT_USER_INFO), PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.UPDATE_VERSION), PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.ORDER_VERIFY), "order/validorderbysunservice");
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.READ_NUM_BALL), "resource/listplanbydoctorid");
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.CHECK_YLRECORD), PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.ERRORPAY_RECORD), PoiTypeDef.All);
        this.urlMap.put(Integer.valueOf((int) HttpRequestParameters.HOSPITAL_DETAIL), "info/gethospitaldetail");
    }

    private UriParameter addValidate(UriParameter parameter) {
        String time = DateUtil.getTime();
        String validatecode = Base64Util.MD5(String.valueOf("shbl7vIOLQ3kDZRk6NeJBAcGqKEUYvPNMrd") + time + "https://appsrv.968309.com/service.app/");
        parameter.add("time", time);
        parameter.add("validatecode", validatecode);
        return parameter;
    }
}
