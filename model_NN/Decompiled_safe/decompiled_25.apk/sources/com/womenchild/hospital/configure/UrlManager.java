package com.womenchild.hospital.configure;

import android.annotation.SuppressLint;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import java.util.HashMap;
import java.util.Map;

@SuppressLint({"UseSparseArrays"})
public class UrlManager {
    private static UrlManager urlManager = null;
    Map<String, String> urls = new HashMap();

    private UrlManager() {
    }

    public static UrlManager getInstance() {
        if (urlManager == null) {
            urlManager = new UrlManager();
            urlManager.initUrl();
        }
        return urlManager;
    }

    private void initUrl() {
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.ADD_FAVORITES_DOCTOR)), "favdoctor_addFav.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.FAVORITES_DOCTOR_LIST)), "favdoctor_readFavList.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.DELETE_FAVORITES_DOCTOR)), "favdoctor_deleteFav.action");
        this.urls.put(toString(307), "hospital_readDoctorPlan.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.HOSPITAL_TAFFIC_INFO)), "singleNotice_readNoticeByJson.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.HOSPITAL_HELP_LIST)), "notice_readNotice.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.HOSPITAL_HELP_CONTTENT)), "notice_readNotices.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.ADD_ONLINE_CONSULT)), "question_addquestion.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.QUESTION_LIST)), "question_readQuestionList.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.CONSULTINFO_DETAIL)), "question_readReplyList.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.ADD_REPLY)), "reply_addReply.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.PATIENT_CARD_INFO)), "hospital_readQueueInfo.action");
        this.urls.put(toString(-1), "hospital_readLaboratoryReport.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.LISTDEPT_BY_HOSPITALID)), "hospital_findDeptData.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.LISTDOCTOR_BY_DOCTORNAME)), "hospital_searchDoctorByName.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.DOCTOR_DETAIL)), "hospital_searchDoctorDetail.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.LIST_STOPPIAN)), "hospital_readStopPlanList.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.HOSPITAL_NOTICE)), "notice_readNoticeForHospital.action");
        this.urls.put(toString(-2), "hospital_readOrderList.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.PAYMENT_LIST)), "hospital_listPay.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.ADD_COMPLAINT_INFO)), "complaint_addComplaint.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.READ_REG_LIST)), "hospital_readRegList.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.DEPT_DETAIL)), "hospital_searchDeptDetail.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.READ_CARD_TYPE)), "hospital_initPatientCardData.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.COMMIT_SUGGESTION)), "suggest_addSuggest.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.ADD_PATIENT_INFO)), "patient_addOrUpdatePatient.action");
        this.urls.put(toString(107), "patient_addOrUpdatePatient.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.PATIENT_LIST)), "patient_readPatientList.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.DELETE_PATIENT_INFO)), "patient_deletePatient.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.ADD_PATIENT_CARD)), "patientcard_addPatientOrUpdateCard.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.EDIT_PATIENT_CARD)), "patientcard_addPatientOrUpdateCard.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.DELETE_PATIENT_CARD)), "patientcard_deletePatientCard.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.EDIT_DEFAULT_CARD)), "patientcard_editDefaultCard.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.PATIENT_CARD_LIST)), "patientcard_readPatientCardList.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.ADD_ORDER_RECORD)), "hospital_addPay.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.SUBMIT_ORDER)), "hospital_orderForSun.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.SUBMIT_PAY_ORDER)), "hospital_submitPayOrder.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.CANCEL_ORDER)), "hospital_cancelOrder.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.ORDER_STATUS)), "hospital_readOrderStatus.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.AUTO_PAY_CONFIRM)), "hospital_confirmautopay.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.AUTO_PAY_ORDER)), "hospital_getautopayorders.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.DELETE_PAY_RECORD)), "hospital_deletepay.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.UPDATE_PAY_RECORD)), "hospital_updateOpNumForPay.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.BACK_MONEY)), "hospital_backMoney.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.CANCELORDER_AND_BACKMONEY)), "hospital_cancelOrderAndBackMoney.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.LISTPLAN_BY_DEPTID)), "hospital_findDoctorPlanByDeptTime.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.DEPT_DETAIL)), "hospital_searchDeptDetail.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.LISTAREA_BY_PROVINCEID)), "hospital_findCityByProvince.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.LISTCITY_BY_PROVINCE)), "hospital_findCityByProvince.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.DOCTOR_DETAIL)), "hospital_searchDoctorDetail.action");
        this.urls.put(toString(307), "hospital_readDoctorPlan.action");
        this.urls.put(toString(100), "account_login.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.USER_REGISTER)), "account_regAccount_new.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.EDIT_USER_PWD)), "account_updatePassword.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.GET_VERIFY_CODE)), "account_readVaildateMessage.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.ADD_USER_INFO)), "patient_addOrUpdatePatient.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.EDIT_USER_INFO)), "patient_addOrUpdatePatient.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.ORDER_VERIFY)), "hospital_validatePayDetail.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.UPDATE_VERSION)), "updateVersion_readUpdateVersion.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.READ_NUM_BALL)), "hospital_readNumball.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.CHECK_YLRECORD)), "hospital_checkOldPay.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.ERRORPAY_RECORD)), "hospital_readerrorpaylist.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.REGISTER_LIST)), "hospital_readOrderList.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.ALIPAY_APPLY_PAY)), "hospital_reqPayInfo.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.CHECK_BLANCE_FOR_SUN)), "hospital_checkBlanceForSun.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.RECHARGE_TO_SUN)), "hospital_rechargeToSun.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.REQ_PAYBYSUN)), "hospital_reqPayBySun.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.HOSPITAL_DETAIL)), "hospital_readHospitalsDetail.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.SUBMIT_COMMENT)), "hospital_commentDoctor.action");
        this.urls.put(toString(Integer.valueOf((int) HttpRequestParameters.LIST_DOCTOR_BY_DEPTID)), "hospital_findDoctorsByDeptId.action");
    }

    public void setUrl(int requestType, String url) {
        if (!this.urls.containsKey(Integer.valueOf(requestType))) {
            this.urls.put(PoiTypeDef.All, url);
        }
    }

    public String getUrl(String requestCode, String uri) {
        return String.valueOf(uri) + this.urls.get(requestCode);
    }

    public String getOutterUrl(String requestCode) {
        return this.urls.get(requestCode);
    }

    private String toString(Object str) {
        return String.valueOf(str);
    }
}
