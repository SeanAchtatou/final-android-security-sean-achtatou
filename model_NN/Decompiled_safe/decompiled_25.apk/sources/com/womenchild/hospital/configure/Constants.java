package com.womenchild.hospital.configure;

import java.util.HashMap;

public class Constants {
    public static final String CHECK_CURRENT_NUM = "check_current_num";
    public static final String CHECK_IS_ALERT = "check_is_alert";
    public static final String CHECK_SETTING_NUM = "check_setting_alert";
    public static Long HOSPITALID = 2000002L;
    public static final String[] HOSPITALIDS = {HOSPITAL_ID, "2000001", "2000003"};
    public static HashMap<String, String> HOSPITALMAPS = new HashMap<>();
    public static final int HOSPITAL_HOME = 1;
    public static final String HOSPITAL_ID = "2000002";
    public static final int REQUEST_FAIL = 1;
    public static final int REQUEST_SUCCESS = 0;
    public static final String REQUEST_URI = "https://appsrv.968309.com/v2/service.app/";
    public static final String SHARED_PREFERENCES_NAME = "women_child";
    public static final String TEST_CURRENT_NUM = "test_current_num";
    public static final String TEST_IS_ALERT = "test_is_alert";
    public static final String TEST_SETTING_NUM = "test_setting_alert";
    public static final String TREAT_CURRENT_NUM = "treat_current_num";
    public static final String TREAT_IS_ALERT = "treat_is_alert";
    public static final String TREAT_SETTING_NUM = "treat_setting_alert";
    public static final int TYPE_ALL_CHECK_REMIND = 3;
    public static final int TYPE_ALL_REMIND = 1;
    public static final int TYPE_ALL_TEST_REMIND = 4;
    public static final int TYPE_ALL_VIS_REMIND = 2;
}
