package com.womenchild.hospital;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.util.Xml;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.alipay.android.app.sdk.AliPay;
import com.amap.mapapi.location.LocationManagerProxy;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.amap.mapapi.route.Route;
import com.umeng.common.util.e;
import com.upomp.pay.Star_Upomp_Pay;
import com.upomp.pay.help.CreateOriginal;
import com.upomp.pay.help.Create_MerchantX;
import com.upomp.pay.help.GetValue;
import com.upomp.pay.help.Xmlpar;
import com.upomp.pay.httpservice.XmlHttpConnection;
import com.upomp.pay.info.Upomp_Pay_Info;
import com.upomp.pay.info.XmlDefinition;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.configure.Constants;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.AppParameters;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.ClientLogUtil;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;

public class SelectPayActivity extends BaseRequestActivity implements View.OnClickListener {
    private static final int RQF_PAY = 1;
    InputStream PrivateSign;
    private Button btnNotPayNow;
    XmlHttpConnection httpConnection;
    private Button ibtnReturn;
    private Intent intent;
    private JSONObject json;
    /* access modifiers changed from: private */
    public Dialog mDialog;
    /* access modifiers changed from: private */
    public MyHandler mMyHandler = new MyHandler(this, null);
    private int money;
    private String opcorderid;
    private String orderId = null;
    private ProgressDialog progressDialog;
    private RelativeLayout rl_alipay_logo;
    private RelativeLayout rl_yl_logo;
    Star_Upomp_Pay star;
    private TextView tvDept;
    private TextView tvDoctor;
    private TextView tvMobile;
    private TextView tvMoney;
    private TextView tvPatient;
    GetValue values;
    Xmlpar xmlpar;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.select_pay_way);
        initViewId();
        initClickListener();
        initData();
    }

    /* access modifiers changed from: protected */
    public void initViewId() {
        this.ibtnReturn = (Button) findViewById(R.id.ibtn_return);
        this.btnNotPayNow = (Button) findViewById(R.id.btn_not_pay_now);
        this.tvDept = (TextView) findViewById(R.id.tv_dept);
        this.tvDoctor = (TextView) findViewById(R.id.tv_doctor);
        this.tvMobile = (TextView) findViewById(R.id.tv_mobile);
        this.rl_yl_logo = (RelativeLayout) findViewById(R.id.rl_yl_way);
        this.rl_alipay_logo = (RelativeLayout) findViewById(R.id.rl_alipay_way);
        this.tvMoney = (TextView) findViewById(R.id.tv_money);
        this.tvPatient = (TextView) findViewById(R.id.tv_patient);
        this.progressDialog = new ProgressDialog(this);
        this.progressDialog.setMessage("正在提交订单，请稍候...");
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.orderId = Create_MerchantX.createMerchantOrderId();
        try {
            this.money = new JSONObject(getIntent().getStringExtra("plan")).optInt("fee");
            this.json = new JSONObject(getIntent().getStringExtra("order"));
            this.tvDept.setText(this.json.optString("deptname"));
            this.tvDoctor.setText(this.json.optString("doctorname"));
            this.tvMobile.setText(this.json.optString("mobile"));
            this.tvMoney.setText(String.valueOf(((double) this.money) / 100.0d) + "元");
            this.tvPatient.setText(this.json.optString("name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void initClickListener() {
        this.ibtnReturn.setOnClickListener(this);
        this.btnNotPayNow.setOnClickListener(this);
        this.rl_yl_logo.setOnClickListener(this);
        this.rl_alipay_logo.setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.SUBMIT_ORDER /*602*/:
                UriParameter parameter = FillInAppointmentInfoActivity.orderEntity.createParameters();
                parameter.add("ordercode", this.orderId);
                parameter.add("memberid", String.valueOf(AppParameters.getAPPID()) + "_" + UserEntity.getInstance().getInfo().getAccId());
                return parameter;
            case HttpRequestParameters.SUBMIT_PAY_ORDER /*604*/:
                UriParameter parameter2 = new UriParameter();
                parameter2.add("userid", UserEntity.getInstance().getInfo().getAccId());
                parameter2.add("ordernum", this.orderId);
                parameter2.add("ordertime", Create_MerchantX.createMerchantOrderTime());
                parameter2.add("paytime", Create_MerchantX.createMerchantOrderTime("yyyy-MM-dd kk:mm"));
                parameter2.add("orderamt", Integer.valueOf(this.money));
                parameter2.add("name", this.json.optString("name"));
                return parameter2;
            case HttpRequestParameters.CHECK_YLRECORD /*611*/:
                UriParameter parameter3 = new UriParameter();
                parameter3.add("userid", UserEntity.getInstance().getInfo().getUserid());
                parameter3.add("price", Integer.valueOf(this.money));
                return parameter3;
            case HttpRequestParameters.ALIPAY_APPLY_PAY /*613*/:
                UriParameter parameter4 = new UriParameter();
                parameter4.add("userid", UserEntity.getInstance().getInfo().getAccId());
                parameter4.add("ordernum", this.orderId);
                parameter4.add("price", Integer.valueOf(this.money));
                parameter4.add("name", this.json.optString("name"));
                parameter4.add("hospitalid", Constants.HOSPITALID);
                return parameter4;
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void loadData(int requestType, Object data) {
    }

    public void refreshActivity(Object... objects) {
        int requestType = ((Integer) objects[0]).intValue();
        boolean status = ((Boolean) objects[1]).booleanValue();
        JSONObject result = (JSONObject) objects[2];
        if (!status) {
            switch (requestType) {
                case HttpRequestParameters.SUBMIT_ORDER /*602*/:
                    Toast.makeText(this, "预约失败，支付详情请查看支付记录", 1).show();
                    break;
                case HttpRequestParameters.SUBMIT_PAY_ORDER /*604*/:
                    Toast.makeText(this, "网络连接异常或服务器无法连接，请重试", 0).show();
                    break;
                case HttpRequestParameters.CHECK_YLRECORD /*611*/:
                    Toast.makeText(getApplicationContext(), "网络连接异常或服务器无法连接，请重试", 1).show();
                    break;
            }
            if (this.progressDialog.isShowing()) {
                this.progressDialog.dismiss();
            }
        } else if (result.optJSONObject("res").optInt("st") == 0) {
            switch (requestType) {
                case HttpRequestParameters.SUBMIT_ORDER /*602*/:
                    if (this.progressDialog.isShowing()) {
                        this.progressDialog.dismiss();
                    }
                    ClientLogUtil.i(getClass().getSimpleName(), result.optJSONObject("inf").toString());
                    this.opcorderid = result.optJSONObject("inf").optString("opcorderid");
                    Intent intent2 = new Intent(this, RegisterNoSuccessActivity.class);
                    intent2.putExtra("ordernum", this.orderId);
                    intent2.putExtra("opcorderid", this.opcorderid);
                    intent2.putExtra("order", result.optJSONObject("inf").toString());
                    intent2.putExtra(LocationManagerProxy.KEY_STATUS_CHANGED, 0);
                    startActivity(intent2);
                    return;
                case HttpRequestParameters.SUBMIT_PAY_ORDER /*604*/:
                    verifyOrder(result.optJSONObject("inf"));
                    return;
                case HttpRequestParameters.CHECK_YLRECORD /*611*/:
                    this.orderId = result.optJSONObject("inf").optString("yloldordernum");
                    sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.SUBMIT_ORDER), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.SUBMIT_ORDER)));
                    return;
                case HttpRequestParameters.ALIPAY_APPLY_PAY /*613*/:
                    if (this.progressDialog.isShowing()) {
                        this.progressDialog.dismiss();
                    }
                    alipay(result.optJSONObject("res").optString("msg"));
                    return;
                default:
                    return;
            }
        } else {
            switch (requestType) {
                case HttpRequestParameters.SUBMIT_ORDER /*602*/:
                    Toast.makeText(this, "预约失败，支付详情请查看支付记录", 1).show();
                    break;
                case HttpRequestParameters.SUBMIT_PAY_ORDER /*604*/:
                    Toast.makeText(this, "预约失败，请重试", 1).show();
                    break;
                case HttpRequestParameters.CHECK_YLRECORD /*611*/:
                    if (17 != result.optJSONObject("res").optInt("st")) {
                        Toast.makeText(getApplicationContext(), result.optJSONObject("res").optInt("msg"), 1).show();
                        break;
                    } else {
                        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.SUBMIT_PAY_ORDER), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.SUBMIT_PAY_ORDER)));
                        break;
                    }
                case HttpRequestParameters.ALIPAY_APPLY_PAY /*613*/:
                    if (this.progressDialog.isShowing()) {
                        this.progressDialog.dismiss();
                    }
                    Toast.makeText(this, "支付失败！" + result.optJSONObject("res").optString("msg"), 1).show();
                    break;
            }
            if (this.progressDialog.isShowing()) {
                this.progressDialog.dismiss();
            }
        }
    }

    private void alipay(final String orderInfo) {
        new Thread() {
            public void run() {
                String result = new AliPay(SelectPayActivity.this, SelectPayActivity.this.mMyHandler).pay(orderInfo);
                Log.i("result", result);
                Message msg = new Message();
                msg.what = 1;
                msg.obj = result;
                SelectPayActivity.this.mMyHandler.sendMessage(msg);
            }
        }.start();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_return:
                finish();
                return;
            case R.id.rl_yl_way:
                this.progressDialog.show();
                sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.CHECK_YLRECORD), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.CHECK_YLRECORD)));
                return;
            case R.id.rl_alipay_way:
                this.progressDialog.show();
                sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.ALIPAY_APPLY_PAY), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.ALIPAY_APPLY_PAY)));
                return;
            case R.id.btn_not_pay_now:
                this.intent = new Intent(this, RegisterNoSuccessActivity.class);
                this.intent.putExtra("order", this.json.toString());
                this.intent.putExtra("opcorderid", this.opcorderid);
                startActivity(this.intent);
                return;
            default:
                return;
        }
    }

    private void verifyOrder(JSONObject json2) {
        this.values = new GetValue();
        this.xmlpar = new Xmlpar();
        try {
            Upomp_Pay_Info.merchantId = json2.optString("merchantid");
            Upomp_Pay_Info.merchantOrderId = json2.optString("merchantorderid");
            Upomp_Pay_Info.merchantOrderTime = json2.optString("merchantordertime");
            Upomp_Pay_Info.originalsign = CreateOriginal.CreateOriginal_Sign(3);
            ClientLogUtil.d(Upomp_Pay_Info.tag, "这是订单验证的3位原串===\n" + Upomp_Pay_Info.originalsign);
            try {
                this.PrivateSign = getFromAssets("898000000000002.p12");
            } catch (FileNotFoundException e) {
                ClientLogUtil.d(Upomp_Pay_Info.tag, "Exception is " + e);
            }
            Upomp_Pay_Info.xmlSign = json2.optString("sign");
            ClientLogUtil.d(Upomp_Pay_Info.tag, "这是订单验证的3位签名===\n" + Upomp_Pay_Info.xmlSign);
            String LanchPay = XmlDefinition.ReturnXml(Upomp_Pay_Info.xmlSign, 3);
            ClientLogUtil.d(Upomp_Pay_Info.tag, "这是订单验证报文===\n" + LanchPay);
            this.star = new Star_Upomp_Pay();
            this.star.start_upomp_pay(this, LanchPay);
        } catch (Exception e2) {
            ClientLogUtil.d(Upomp_Pay_Info.tag, "**Exception is " + e2);
        }
    }

    public InputStream getFromAssets(String fileName) throws FileNotFoundException {
        try {
            this.PrivateSign = getResources().getAssets().open(fileName);
        } catch (Exception e) {
            ClientLogUtil.d(Upomp_Pay_Info.tag, "Exception is " + e);
        }
        return this.PrivateSign;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            byte[] xml = data.getExtras().getByteArray("xml");
            String respCode = null;
            String respDesc = null;
            try {
                XmlPullParser parser = Xml.newPullParser();
                parser.setInput(new ByteArrayInputStream(xml), e.f);
                for (int eventType = parser.getEventType(); eventType != 1; eventType = parser.next()) {
                    switch (eventType) {
                        case 2:
                            String tag = parser.getName();
                            if (!tag.equalsIgnoreCase("respCode")) {
                                if (!tag.equalsIgnoreCase("respDesc")) {
                                    break;
                                } else {
                                    respDesc = parser.nextText();
                                    break;
                                }
                            } else {
                                respCode = parser.nextText();
                                break;
                            }
                    }
                }
                ClientLogUtil.d(Upomp_Pay_Info.tag, "解析中:" + respCode + "->" + respDesc);
                if (respCode == null || !"0000".equals(respCode)) {
                    MyHandler myHandler = this.mMyHandler;
                    this.mMyHandler.getClass();
                    this.mMyHandler.sendMessage(myHandler.obtainMessage(11));
                    return;
                }
                if (!this.progressDialog.isShowing()) {
                    this.progressDialog.setMessage(PoiTypeDef.All);
                    this.progressDialog.show();
                }
                sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.SUBMIT_ORDER), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.SUBMIT_ORDER)));
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: private */
    public void showDialogPayFail() {
        this.mDialog = new AlertDialog.Builder(this).setTitle("挂号失败").setMessage("挂号失败，请选择重试或退出本次预约").setPositiveButton("退出预约", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                SelectPayActivity.this.mDialog.dismiss();
                Intent intent = new Intent(SelectPayActivity.this, HomeActivity.class);
                intent.setFlags(67108864);
                SelectPayActivity.this.startActivity(intent);
                SelectPayActivity.this.finish();
            }
        }).setNegativeButton("重试", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                SelectPayActivity.this.mDialog.dismiss();
            }
        }).create();
        this.mDialog.show();
    }

    private class MyHandler extends Handler {
        public final int SHOWDIALOGPAYFAIL;

        private MyHandler() {
            this.SHOWDIALOGPAYFAIL = 11;
        }

        /* synthetic */ MyHandler(SelectPayActivity selectPayActivity, MyHandler myHandler) {
            this();
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    Result.sResult = (String) msg.obj;
                    if (!"9000".equals(Result.getResult())) {
                        MyHandler access$2 = SelectPayActivity.this.mMyHandler;
                        SelectPayActivity.this.mMyHandler.getClass();
                        SelectPayActivity.this.mMyHandler.sendMessage(access$2.obtainMessage(11));
                        break;
                    } else {
                        SelectPayActivity.this.sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.SUBMIT_ORDER), SelectPayActivity.this.initRequestParameter(Integer.valueOf((int) HttpRequestParameters.SUBMIT_ORDER)));
                        break;
                    }
                case Route.DrivingSaveMoney:
                    SelectPayActivity.this.showDialogPayFail();
                    break;
            }
            super.handleMessage(msg);
        }
    }
}
