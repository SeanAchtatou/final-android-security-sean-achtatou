package com.womenchild.hospital.request;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;
import com.womenchild.hospital.base.BaseRequestFragment;
import com.womenchild.hospital.http.HttpClientRequest;
import com.womenchild.hospital.http.Network;
import com.womenchild.hospital.util.ClientLogUtil;
import org.json.JSONException;
import org.json.JSONObject;

public class FragmentRequestRunnable implements Runnable {
    /* access modifiers changed from: private */
    public BaseRequestFragment baseRequestFragment;
    @SuppressLint({"HandlerLeak"})
    private Handler handler;
    /* access modifiers changed from: private */
    public String requestCode;
    private boolean sslFlag;
    private String uri;

    public FragmentRequestRunnable(BaseRequestFragment baseRequestFragment2, String requestCode2, String uri2) {
        this.sslFlag = true;
        this.handler = new Handler() {
            public void handleMessage(Message msg) {
                RequestTask.getInstance().cancelHttpRequest(FragmentRequestRunnable.this.baseRequestFragment, FragmentRequestRunnable.this.requestCode);
                boolean status = true;
                if (msg.obj == null) {
                    status = false;
                }
                if (!FragmentRequestRunnable.this.baseRequestFragment.isHidden() && FragmentRequestRunnable.this.baseRequestFragment.isVisible()) {
                    if (Network.isNetworkAvailable(FragmentRequestRunnable.this.baseRequestFragment.getActivity())) {
                        JSONObject json = null;
                        try {
                            json = new JSONObject(String.valueOf(msg.obj));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        FragmentRequestRunnable.this.baseRequestFragment.refreshFragment(Integer.valueOf(Integer.parseInt(FragmentRequestRunnable.this.requestCode)), Boolean.valueOf(status), json);
                        return;
                    }
                    FragmentRequestRunnable.this.baseRequestFragment.refreshFragment(Integer.valueOf(Integer.parseInt(FragmentRequestRunnable.this.requestCode)), Boolean.valueOf(status));
                }
            }
        };
        this.baseRequestFragment = baseRequestFragment2;
        this.requestCode = requestCode2;
        this.uri = uri2;
    }

    public FragmentRequestRunnable(BaseRequestFragment baseRequestFragment2, String requestCode2, String uri2, boolean sslFlag2) {
        this(baseRequestFragment2, requestCode2, uri2);
        this.sslFlag = sslFlag2;
    }

    public void run() {
        String sendHttpRequest;
        Message msg = this.handler.obtainMessage();
        msg.what = 0;
        if (Network.isNetworkAvailable(this.baseRequestFragment.getActivity())) {
            ClientLogUtil.d("baseRequestFragment", this.uri);
            if (this.sslFlag) {
                sendHttpRequest = HttpClientRequest.getInstance().sendHttpRequest(this.uri, this.sslFlag);
            } else {
                sendHttpRequest = HttpClientRequest.getInstance().sendHttpRequest(this.uri);
            }
            msg.obj = sendHttpRequest;
        } else {
            msg.obj = null;
        }
        this.handler.sendMessage(msg);
    }
}
