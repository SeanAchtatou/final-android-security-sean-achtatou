package com.womenchild.hospital.request;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.http.HttpClientRequest;
import com.womenchild.hospital.http.Network;
import com.womenchild.hospital.util.ClientLogUtil;
import org.apache.http.HttpEntity;
import org.json.JSONException;
import org.json.JSONObject;

public class ActivityRequestRunnable implements Runnable {
    /* access modifiers changed from: private */
    public BaseRequestActivity baseRequestActivity;
    private HttpEntity entity;
    @SuppressLint({"HandlerLeak"})
    private Handler handler;
    /* access modifiers changed from: private */
    public String requestCode;
    private boolean sslFlag;
    private String uri;

    public void run() {
        String sendHttpRequest;
        Message msg = this.handler.obtainMessage();
        msg.what = 0;
        if (Network.isNetworkAvailable(this.baseRequestActivity)) {
            ClientLogUtil.i(getClass().getSimpleName(), this.uri);
            if (this.sslFlag) {
                sendHttpRequest = HttpClientRequest.getInstance().sendHttpRequest(this.uri, this.entity, this.sslFlag);
            } else {
                sendHttpRequest = HttpClientRequest.getInstance().sendHttpRequest(this.uri, this.entity);
            }
            msg.obj = sendHttpRequest;
        } else {
            msg.obj = null;
        }
        this.handler.sendMessage(msg);
    }

    public ActivityRequestRunnable(BaseRequestActivity baseRequestActivity2, String requestCode2, String uri2) {
        this.baseRequestActivity = null;
        this.sslFlag = true;
        this.handler = new Handler() {
            public void handleMessage(Message msg) {
                RequestTask.getInstance().cancelHttpRequest(ActivityRequestRunnable.this.baseRequestActivity, ActivityRequestRunnable.this.requestCode);
                boolean status = true;
                if (msg.obj == null) {
                    status = false;
                }
                if (ActivityRequestRunnable.this.baseRequestActivity.isFinishing()) {
                    return;
                }
                if (Network.isNetworkAvailable(ActivityRequestRunnable.this.baseRequestActivity)) {
                    JSONObject json = null;
                    try {
                        json = new JSONObject(String.valueOf(msg.obj));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    ActivityRequestRunnable.this.baseRequestActivity.refreshActivity(Integer.valueOf(Integer.parseInt(ActivityRequestRunnable.this.requestCode)), Boolean.valueOf(status), json);
                    return;
                }
                ActivityRequestRunnable.this.baseRequestActivity.refreshActivity(Integer.valueOf(Integer.parseInt(ActivityRequestRunnable.this.requestCode)), Boolean.valueOf(status));
            }
        };
        this.baseRequestActivity = baseRequestActivity2;
        this.requestCode = requestCode2;
        this.uri = uri2;
    }

    public ActivityRequestRunnable(BaseRequestActivity baseRequestActivity2, String requestCode2, String uri2, boolean SSLFlag) {
        this(baseRequestActivity2, requestCode2, uri2);
        this.sslFlag = SSLFlag;
    }

    public ActivityRequestRunnable(BaseRequestActivity baseRequestActivity2, String requestCode2, String createUri, HttpEntity createEntity, boolean sSLFlag2) {
        this(baseRequestActivity2, requestCode2, createUri);
        this.entity = createEntity;
        this.sslFlag = sSLFlag2;
    }
}
