package com.womenchild.hospital.request;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;
import com.womenchild.hospital.base.BaseRequestService;
import com.womenchild.hospital.http.HttpClientRequest;
import com.womenchild.hospital.http.Network;
import org.json.JSONException;
import org.json.JSONObject;

public class ServiceRequestRunnable implements Runnable {
    /* access modifiers changed from: private */
    public BaseRequestService baseRequestService;
    @SuppressLint({"HandlerLeak"})
    private Handler handler;
    /* access modifiers changed from: private */
    public String requestCode;
    private boolean sslFlag;
    private String uri;

    public ServiceRequestRunnable(BaseRequestService baseRequestService2, String requestCode2, String uri2) {
        this.sslFlag = true;
        this.handler = new Handler() {
            public void handleMessage(Message msg) {
                RequestTask.getInstance().cancelHttpRequest(ServiceRequestRunnable.this.baseRequestService, ServiceRequestRunnable.this.requestCode);
                boolean status = true;
                if (msg.obj == null) {
                    status = false;
                }
                if (Network.isNetworkAvailable(ServiceRequestRunnable.this.baseRequestService)) {
                    JSONObject json = null;
                    try {
                        json = new JSONObject(String.valueOf(msg.obj));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    ServiceRequestRunnable.this.baseRequestService.refreshService(Integer.valueOf(Integer.parseInt(ServiceRequestRunnable.this.requestCode)), Boolean.valueOf(status), json);
                    return;
                }
                ServiceRequestRunnable.this.baseRequestService.refreshService(Integer.valueOf(Integer.parseInt(ServiceRequestRunnable.this.requestCode)), Boolean.valueOf(status));
            }
        };
        this.baseRequestService = baseRequestService2;
        this.requestCode = requestCode2;
        this.uri = uri2;
    }

    public ServiceRequestRunnable(BaseRequestService baseRequestService2, String requestCode2, String uri2, boolean sslFlag2) {
        this(baseRequestService2, requestCode2, uri2);
        this.sslFlag = sslFlag2;
    }

    public void run() {
        String sendHttpRequest;
        Message msg = this.handler.obtainMessage();
        msg.what = 0;
        if (Network.isNetworkAvailable(this.baseRequestService)) {
            if (this.sslFlag) {
                sendHttpRequest = HttpClientRequest.getInstance().sendHttpRequest(this.uri, this.sslFlag);
            } else {
                sendHttpRequest = HttpClientRequest.getInstance().sendHttpRequest(this.uri);
            }
            msg.obj = sendHttpRequest;
        } else {
            msg.obj = null;
        }
        this.handler.sendMessage(msg);
    }
}
