package com.womenchild.hospital.request;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;
import com.womenchild.hospital.base.BaseRequestFragmentActivity;
import com.womenchild.hospital.http.HttpClientRequest;
import com.womenchild.hospital.http.Network;
import com.womenchild.hospital.util.ClientLogUtil;
import org.json.JSONException;
import org.json.JSONObject;

public class FragmentActivityRequestRunnable implements Runnable {
    /* access modifiers changed from: private */
    public BaseRequestFragmentActivity baseRequestFragmentActivity;
    @SuppressLint({"HandlerLeak"})
    private Handler handler;
    /* access modifiers changed from: private */
    public String requestCode;
    private boolean sslFlag;
    private String uri;

    public FragmentActivityRequestRunnable(BaseRequestFragmentActivity baseRequestFragmentActivity2, String requestCode2, String uri2) {
        this.sslFlag = true;
        this.handler = new Handler() {
            public void handleMessage(Message msg) {
                RequestTask.getInstance().cancelHttpRequest(FragmentActivityRequestRunnable.this.baseRequestFragmentActivity, FragmentActivityRequestRunnable.this.requestCode);
                boolean status = true;
                if (msg.obj == null) {
                    status = false;
                }
                if (!FragmentActivityRequestRunnable.this.baseRequestFragmentActivity.isFinishing() && FragmentActivityRequestRunnable.this.baseRequestFragmentActivity.isTaskRoot()) {
                    if (Network.isNetworkAvailable(FragmentActivityRequestRunnable.this.baseRequestFragmentActivity)) {
                        JSONObject json = null;
                        try {
                            json = new JSONObject(String.valueOf(msg.obj));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        FragmentActivityRequestRunnable.this.baseRequestFragmentActivity.refreshFragment(Integer.valueOf(Integer.parseInt(FragmentActivityRequestRunnable.this.requestCode)), Boolean.valueOf(status), json);
                        return;
                    }
                    FragmentActivityRequestRunnable.this.baseRequestFragmentActivity.refreshFragment(Integer.valueOf(Integer.parseInt(FragmentActivityRequestRunnable.this.requestCode)), Boolean.valueOf(status));
                }
            }
        };
        this.baseRequestFragmentActivity = baseRequestFragmentActivity2;
        this.requestCode = requestCode2;
        this.uri = uri2;
    }

    public FragmentActivityRequestRunnable(BaseRequestFragmentActivity baseRequestFragmentActivity2, String requestCode2, String uri2, boolean sslFlag2) {
        this(baseRequestFragmentActivity2, requestCode2, uri2);
        this.sslFlag = sslFlag2;
    }

    public void run() {
        String sendHttpRequest;
        Message msg = this.handler.obtainMessage();
        msg.what = 0;
        if (Network.isNetworkAvailable(this.baseRequestFragmentActivity)) {
            ClientLogUtil.i(getClass().getSimpleName(), this.uri);
            if (this.sslFlag) {
                sendHttpRequest = HttpClientRequest.getInstance().sendHttpRequest(this.uri, this.sslFlag);
            } else {
                sendHttpRequest = HttpClientRequest.getInstance().sendHttpRequest(this.uri);
            }
            msg.obj = sendHttpRequest;
        } else {
            msg.obj = null;
        }
        this.handler.sendMessage(msg);
    }
}
