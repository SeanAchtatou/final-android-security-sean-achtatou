package com.womenchild.hospital.request;

import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.base.BaseRequestFragment;
import com.womenchild.hospital.base.BaseRequestFragmentActivity;
import com.womenchild.hospital.base.BaseRequestService;
import com.womenchild.hospital.configure.Constants;
import com.womenchild.hospital.configure.UrlEncoder;
import com.womenchild.hospital.configure.UrlManager;
import com.womenchild.hospital.parameter.UriParameter;
import java.util.WeakHashMap;

public class RequestTask {
    private static RequestTask requestTask = null;
    private WeakHashMap<String, Thread> requestThreads = new WeakHashMap<>();

    private RequestTask() {
    }

    public static RequestTask getInstance() {
        if (requestTask == null) {
            requestTask = new RequestTask();
        }
        return requestTask;
    }

    public void sendHttpRequest(BaseRequestFragment baseRequestFragment, String requestCode, UriParameter uriParameter) {
        Thread thread = new Thread(new FragmentRequestRunnable(baseRequestFragment, requestCode, encoderParameter(requestCode, uriParameter).createUri(UrlManager.getInstance().getUrl(requestCode, Constants.REQUEST_URI))));
        this.requestThreads.put(String.valueOf(baseRequestFragment.getClass().getSimpleName()) + requestCode, thread);
        thread.start();
    }

    public void sendHttpRequest(BaseRequestFragmentActivity baseRequestFragmentActivity, String requestCode, UriParameter uriParameter) {
        Thread thread = new Thread(new FragmentActivityRequestRunnable(baseRequestFragmentActivity, requestCode, encoderParameter(requestCode, uriParameter).createUri(UrlManager.getInstance().getUrl(requestCode, Constants.REQUEST_URI))));
        this.requestThreads.put(String.valueOf(baseRequestFragmentActivity.getClass().getSimpleName()) + requestCode, thread);
        thread.start();
    }

    public void sendHttpRequest(BaseRequestFragmentActivity baseRequestFragmentActivity, String requestCode, UriParameter uriParameter, boolean sslFlag) {
        Thread thread = new Thread(new FragmentActivityRequestRunnable(baseRequestFragmentActivity, requestCode, encoderParameter(requestCode, uriParameter).createUri(UrlManager.getInstance().getUrl(requestCode, Constants.REQUEST_URI)), sslFlag));
        this.requestThreads.put(String.valueOf(baseRequestFragmentActivity.getClass().getSimpleName()) + requestCode, thread);
        thread.start();
    }

    public void sendHttpRequest(BaseRequestFragment baseRequestFragment, String requestCode, UriParameter uriParameter, boolean SSLFlag) {
        Thread thread = new Thread(new FragmentRequestRunnable(baseRequestFragment, requestCode, encoderParameter(requestCode, uriParameter).createUri(UrlManager.getInstance().getUrl(requestCode, Constants.REQUEST_URI)), SSLFlag));
        this.requestThreads.put(String.valueOf(baseRequestFragment.getClass().getSimpleName()) + requestCode, thread);
        thread.start();
    }

    public void sendHttpRequest(BaseRequestActivity baseRequestActivity, String requestCode, UriParameter uriParameter) {
        Thread thread = new Thread(new ActivityRequestRunnable(baseRequestActivity, requestCode, UrlManager.getInstance().getUrl(requestCode, Constants.REQUEST_URI), encoderParameter(requestCode, uriParameter).createEntity(), false));
        this.requestThreads.put(baseRequestActivity.getClass().getSimpleName(), thread);
        thread.start();
    }

    public void sendHttpRequest(BaseRequestActivity baseRequestActivity, String requestCode, UriParameter uriParameter, boolean SSLFlag) {
        Thread thread = new Thread(new ActivityRequestRunnable(baseRequestActivity, requestCode, UrlManager.getInstance().getUrl(requestCode, Constants.REQUEST_URI), encoderParameter(requestCode, uriParameter).createEntity(), SSLFlag));
        this.requestThreads.put(baseRequestActivity.getClass().getSimpleName(), thread);
        thread.start();
    }

    public void sendHttpRequest(BaseRequestService baseRequestService, String requestCode, UriParameter uriParameter, boolean SSLFlag) {
        Thread thread = new Thread(new ServiceRequestRunnable(baseRequestService, requestCode, encoderParameter(requestCode, uriParameter).createUri(UrlManager.getInstance().getUrl(requestCode, Constants.REQUEST_URI)), SSLFlag));
        this.requestThreads.put(String.valueOf(baseRequestService.getClass().getSimpleName()) + requestCode, thread);
        thread.start();
    }

    public void cancelHttpRequest(BaseRequestFragment baseRequestFragment, String requestCode) {
        doCancelHttpRequest(String.valueOf(baseRequestFragment.getClass().getSimpleName()) + requestCode);
    }

    public void cancelHttpRequest(BaseRequestActivity baseRequestActivity, String requestCode) {
        doCancelHttpRequest(String.valueOf(baseRequestActivity.getClass().getSimpleName()) + requestCode);
    }

    public void cancelHttpRequest(BaseRequestService baseRequestService, String requestCode) {
        doCancelHttpRequest(String.valueOf(baseRequestService.getClass().getSimpleName()) + requestCode);
    }

    public void cancelHttpRequest(BaseRequestFragmentActivity baseRequestFragmentActivity, String requestCode) {
        doCancelHttpRequest(String.valueOf(baseRequestFragmentActivity.getClass().getSimpleName()) + requestCode);
    }

    private void doCancelHttpRequest(String key) {
        if (this.requestThreads.containsKey(key)) {
            if (this.requestThreads.get(key) != null) {
                this.requestThreads.get(key).interrupt();
            }
            this.requestThreads.remove(key);
        }
    }

    private UriParameter encoderParameter(String requestCode, UriParameter parameter) {
        return new UrlEncoder().initParams(requestCode, parameter);
    }
}
