package com.womenchild.hospital.util;

import android.support.v4.view.MotionEventCompat;
import android.util.Base64;
import android.util.Log;
import com.umeng.common.util.e;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Base64Util {
    public static final String TAG = "BASE64";

    public static String stringEncoder(String string) {
        String string2 = stringToUtf8(string);
        if (string2 != null) {
            return Base64.encodeToString(string2.getBytes(), 0);
        }
        return null;
    }

    public static String stringDecoder(String string) {
        if (string != null) {
            try {
                return new String(Base64.decode(string, 0), e.f);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                ClientLogUtil.e(TAG, e.getMessage());
            }
        }
        return null;
    }

    public static String stringToUtf8(String str) {
        try {
            return new String(str.getBytes(e.f), e.f);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
            return null;
        }
    }

    public static String MD5(String str) {
        char[] charArray = str.toCharArray();
        byte[] byteArray = new byte[charArray.length];
        StringBuffer hexValue = new StringBuffer();
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            for (int i = 0; i < charArray.length; i++) {
                byteArray[i] = (byte) charArray[i];
            }
            byte[] md5Bytes = md5.digest(byteArray);
            for (byte b : md5Bytes) {
                int val = b & MotionEventCompat.ACTION_MASK;
                if (val < 16) {
                    hexValue.append("0");
                }
                hexValue.append(Integer.toHexString(val));
            }
            ClientLogUtil.i(TAG, hexValue.toString());
            return hexValue.toString().toUpperCase();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
            return null;
        }
    }
}
