package com.womenchild.hospital.util;

import android.content.Context;
import android.content.SharedPreferences;
import com.amap.mapapi.poisearch.PoiTypeDef;

public class SharedPreferencesUtil {
    private Context context;

    public SharedPreferencesUtil(Context context2) {
        this.context = context2;
    }

    public void save(String name, String value) {
        SharedPreferences.Editor edit = this.context.getSharedPreferences("fezx", 0).edit();
        edit.putString(name, value);
        edit.commit();
    }

    public String get(String name) {
        return this.context.getSharedPreferences("fezx", 0).getString(name, PoiTypeDef.All);
    }
}
