package com.womenchild.hospital.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.TextView;

public class DialogUtil {
    /* access modifiers changed from: private */
    public Activity activity;
    private String cancel;
    /* access modifiers changed from: private */
    public String cancelIntent;
    private Context context;
    private String enter;
    /* access modifiers changed from: private */
    public String enterIntent;
    private String msg;
    private String title;

    public DialogUtil(Context context2) {
        this.context = context2;
    }

    public DialogUtil(Context context2, String title2, String msg2) {
        this.context = context2;
        this.title = title2;
        this.msg = msg2;
    }

    public DialogUtil(Context context2, String title2, String msg2, String enter2, String cancel2) {
        this.context = context2;
        this.title = title2;
        this.msg = msg2;
        this.enter = enter2;
        this.cancel = cancel2;
    }

    public DialogUtil(Activity activity2, String title2, String msg2, String enter2, String cancel2, String enterIntent2, String cancelIntent2) {
        this.activity = activity2;
        this.title = title2;
        this.msg = msg2;
        this.enter = enter2;
        this.cancel = cancel2;
        this.cancelIntent = cancelIntent2;
        this.enterIntent = enterIntent2;
    }

    public void show() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.activity);
        builder.setTitle(this.title);
        TextView titleTv = new TextView(this.activity);
        titleTv.setTextSize(24.0f);
        titleTv.getPaint().setFakeBoldText(true);
        titleTv.setText(this.title);
        titleTv.setGravity(1);
        titleTv.setPadding(0, 10, 0, 0);
        builder.setCustomTitle(titleTv);
        builder.setMessage(this.msg);
        builder.setPositiveButton(this.enter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if ("finish".equals(DialogUtil.this.enterIntent)) {
                    DialogUtil.this.activity.finish();
                    return;
                }
                DialogUtil.this.activity.startActivity(new Intent(DialogUtil.this.enterIntent));
            }
        });
        builder.setNegativeButton(this.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                DialogUtil.this.activity.startActivity(new Intent(DialogUtil.this.cancelIntent));
            }
        });
        builder.create().show();
    }

    public Context getContext() {
        return this.context;
    }

    public void setContext(Context context2) {
        this.context = context2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg2) {
        this.msg = msg2;
    }

    public String getEnter() {
        return this.enter;
    }

    public void setEnter(String enter2) {
        this.enter = enter2;
    }

    public String getCancel() {
        return this.cancel;
    }

    public void setCancel(String cancel2) {
        this.cancel = cancel2;
    }

    public String getEnterIntent() {
        return this.enterIntent;
    }

    public void setEnterIntent(String enterIntent2) {
        this.enterIntent = enterIntent2;
    }

    public String getCancelIntent() {
        return this.cancelIntent;
    }

    public void setCancelIntent(String cancelIntent2) {
        this.cancelIntent = cancelIntent2;
    }
}
