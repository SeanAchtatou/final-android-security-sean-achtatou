package com.womenchild.hospital.util;

public class CardIDUtil {
    public boolean cardVerification(String idCard) {
        String tmpIdCard = new String(idCard);
        int y = Integer.parseInt(tmpIdCard.substring(6, 10));
        int m = Integer.parseInt(tmpIdCard.substring(10, 12));
        int d = Integer.parseInt(tmpIdCard.substring(12, 14));
        int[] xx = {2, 4, 8, 5, 10, 9, 7, 3, 6, 1, 2, 4, 8, 5, 10, 9, 7};
        char[] yy = {'1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2'};
        int mm = 0;
        char ch = tmpIdCard.charAt(17);
        int[] gg = new int[18];
        if (m < 1 || m > 12 || d < 1 || d > 31 || (((m == 4 || m == 6 || m == 9 || m == 11) && d > 30) || (m == 2 && (((y + 1900) % 4 > 0 && d > 28) || d > 29)))) {
            return false;
        }
        for (int i = 1; i < 18; i++) {
            int j = 17 - i;
            gg[i - 1] = Integer.parseInt(tmpIdCard.substring(j, j + 1));
        }
        for (int i2 = 0; i2 < 17; i2++) {
            mm += xx[i2] * gg[i2];
        }
        if (yy[mm % 11] == ch) {
            return true;
        }
        return false;
    }

    public int getSexType(String idCard) {
        if (!cardVerification(idCard)) {
            return -1;
        }
        return Integer.parseInt(idCard.substring(16, 17)) % 2;
    }
}
