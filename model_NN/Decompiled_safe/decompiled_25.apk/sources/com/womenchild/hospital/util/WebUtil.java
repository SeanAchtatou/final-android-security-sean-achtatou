package com.womenchild.hospital.util;

public class WebUtil {
    public static String sourceCreateHtml(String source) {
        StringBuffer sBuffer = new StringBuffer();
        sBuffer.append("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
        sBuffer.append("<head>");
        sBuffer.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />");
        sBuffer.append("<style>");
        sBuffer.append("p{overflow:hidden; zoom:1;}");
        sBuffer.append("a{display:inline-block; margin-right:10px; float:left;}");
        sBuffer.append("</style>");
        sBuffer.append("</head>");
        sBuffer.append("<body>");
        sBuffer.append("<div>");
        sBuffer.append(source);
        sBuffer.append("</div>");
        sBuffer.append("</body>");
        sBuffer.append("</html>");
        return sBuffer.toString();
    }
}
