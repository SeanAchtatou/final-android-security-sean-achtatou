package com.womenchild.hospital.util;

import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class InputMsgUtil {
    public static void hide(EditText text) {
        ((InputMethodManager) text.getContext().getSystemService("input_method")).hideSoftInputFromWindow(text.getApplicationWindowToken(), 0);
    }
}
