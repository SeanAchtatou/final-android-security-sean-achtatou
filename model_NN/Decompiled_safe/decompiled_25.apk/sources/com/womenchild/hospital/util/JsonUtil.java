package com.womenchild.hospital.util;

import com.womenchild.hospital.parameter.UriParameter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonUtil {
    private static JsonUtil jsonUtil = null;
    private final String TAG = "JsonUtil";

    private JsonUtil() {
    }

    public static JsonUtil getInstance() {
        if (jsonUtil == null) {
            jsonUtil = new JsonUtil();
        }
        return jsonUtil;
    }

    public JSONObject paramToJsonObject(UriParameter parameters) {
        JSONObject jsonObject = null;
        if (parameters != null && parameters.size() > 0) {
            jsonObject = new JSONObject();
            for (int i = 0; i < parameters.size(); i++) {
                try {
                    jsonObject.put(parameters.getKey(i), parameters.getValue(i));
                } catch (JSONException e) {
                    e.printStackTrace();
                    ClientLogUtil.e("JsonUtil", e.getMessage());
                }
            }
        }
        return jsonObject;
    }

    public JSONObject stringToJsonObject(String string) {
        if (string == null) {
            return null;
        }
        try {
            return new JSONObject(string);
        } catch (JSONException e) {
            e.printStackTrace();
            ClientLogUtil.e("JsonUtil", e.getMessage());
            return null;
        }
    }

    public JSONArray stringToJsonArray(String string) {
        try {
            return new JSONArray(string);
        } catch (JSONException e) {
            e.printStackTrace();
            ClientLogUtil.e("JsonUtil", e.getMessage());
            return null;
        }
    }
}
