package com.womenchild.hospital.util;

import android.util.Log;

public class ClientLogUtil {
    private static int TAG = 0;
    private static int flagD = 1;
    private static int flagE = 4;
    private static int flagI = 2;
    private static int flagV = 0;
    private static int flagW = 3;

    public static void v(String tag, String msg) {
        if (flagV > TAG && msg != null) {
            Log.v(tag, msg);
        }
    }

    public static void d(String tag, String msg) {
        if (flagD > TAG && msg != null) {
            Log.d(tag, msg);
        }
    }

    public static void i(String tag, String msg) {
        if (flagI > TAG && msg != null) {
            Log.i(tag, msg);
        }
    }

    public static void w(String tag, String msg) {
        if (flagW > TAG && msg != null) {
            Log.w(tag, msg);
        }
    }

    public static void e(String tag, String msg) {
        if (flagE > TAG && msg != null) {
            Log.e(tag, msg);
        }
    }
}
