package com.womenchild.hospital.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import com.womenchild.hospital.QueueCallActivity;
import com.womenchild.hospital.R;
import com.womenchild.hospital.configure.Constants;

public class Notifier {
    private static final String TAG = "Notifier";
    private Context context;
    private Intent intent = new Intent();
    private boolean isEnabled = false;
    private Notification notification;
    private NotificationManager notificationManager;
    private SharedPreferences sharedPrefs;

    public Notifier(Context context2) {
        this.context = context2;
        this.notification = new Notification();
        this.sharedPrefs = context2.getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, 0);
        this.notificationManager = (NotificationManager) context2.getSystemService("notification");
    }

    public void notify(int notificationId, String apiKey, String title, String message, String uri) {
        Log.d(TAG, "notify()...");
        Log.d(TAG, "notificationTitle=" + title);
        Log.d(TAG, "notificationMessage=" + message);
        if (isNotificationEnabled()) {
            this.notification.icon = R.drawable.ic_launcher;
            this.notification.defaults = 4;
            this.notification.defaults |= 1;
            this.notification.defaults |= 2;
            this.notification.flags |= 16;
            this.notification.when = System.currentTimeMillis();
            this.notification.tickerText = message;
            this.notification.setLatestEventInfo(this.context, title, message, PendingIntent.getActivity(this.context, 0, new Intent(this.context, QueueCallActivity.class), 134217728));
            this.notificationManager.notify(notificationId, this.notification);
        }
    }

    private boolean isNotificationEnabled() {
        return this.sharedPrefs.getBoolean(Constants.TREAT_IS_ALERT, false);
    }
}
