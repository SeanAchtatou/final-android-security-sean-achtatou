package com.womenchild.hospital.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.R;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;

public class VersionUtil {
    private static final int DOWNLOAD_CANCEL = 3;
    private static final int DOWNLOAD_ERROR_CANCEL = 4;
    private static final int DOWNLOAD_OVER = 2;
    private static final int DOWNLOAD_UPDATE = 1;
    public static boolean updateFlag = false;
    private static VersionUtil versionUtils = null;
    private AlertDialog.Builder DownloadDialogBuilder;
    /* access modifiers changed from: private */
    public boolean cancelFlag = false;
    /* access modifiers changed from: private */
    public Context context = null;
    /* access modifiers changed from: private */
    public AlertDialog downDialog;
    /* access modifiers changed from: private */
    public int downloadProgress = 0;
    /* access modifiers changed from: private */
    public String downloadUrl = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    VersionUtil.this.progressBar.setProgress(VersionUtil.this.downloadProgress);
                    VersionUtil.this.tvProgress.setText("当前下载进度为：" + VersionUtil.this.downloadProgress + "%");
                    ClientLogUtil.i(getClass().getSimpleName(), "update");
                    if (VersionUtil.this.cancelFlag) {
                        File apkFile = new File(VersionUtil.this.saveFileName);
                        if (apkFile.exists()) {
                            apkFile.delete();
                            return;
                        }
                        return;
                    }
                    return;
                case 2:
                    VersionUtil.this.installApk();
                    return;
                case 3:
                    VersionUtil.this.cancelFlag = true;
                    return;
                case 4:
                    VersionUtil.this.cancelFlag = true;
                    Toast.makeText(VersionUtil.this.context, VersionUtil.this.context.getString(R.string.network_connect_failed_prompt), 1).show();
                    VersionUtil.this.downDialog.dismiss();
                    return;
                default:
                    return;
            }
        }
    };
    private Context mCcontext = null;
    /* access modifiers changed from: private */
    public ProgressBar progressBar;
    /* access modifiers changed from: private */
    public String saveFileName = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public String savePath = PoiTypeDef.All;
    public String serviceVersion = null;
    /* access modifiers changed from: private */
    public TextView tvProgress;
    private String versionCode = null;
    private String versionName = null;

    private VersionUtil() {
    }

    public static VersionUtil getInstance() {
        if (versionUtils == null) {
            versionUtils = new VersionUtil();
        }
        return versionUtils;
    }

    public String getVersionName() {
        this.versionName = getInfo(0);
        return this.versionName;
    }

    public void setVersionName(String versionName2) {
        this.versionName = versionName2;
    }

    public String getVersionCode() {
        this.versionCode = getInfo(1);
        return this.versionCode;
    }

    public void setVersionCode(String versionCode2) {
        this.versionCode = versionCode2;
    }

    private PackageManager getPackageManager() {
        return this.context.getPackageManager();
    }

    private PackageInfo getPackageInfo() {
        try {
            return getPackageManager().getPackageInfo(this.context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String getInfo(int type) {
        PackageInfo packageInfo = getPackageInfo();
        if (packageInfo != null) {
            switch (type) {
                case 0:
                    return packageInfo.versionName;
                case 1:
                    return String.valueOf(packageInfo.versionCode);
            }
        }
        return null;
    }

    private boolean isVersionEquals(String serviceVersion2) {
        return getVersionName() != null && !this.versionName.equalsIgnoreCase(serviceVersion2);
    }

    public void updateVersion(Context context2, String downloadUrl2, String path, String serviceVersion2) {
        this.context = context2;
        this.downloadUrl = downloadUrl2;
        this.serviceVersion = serviceVersion2;
        if (!isVersionEquals(serviceVersion2)) {
            return;
        }
        if ("mounted".equals(Environment.getExternalStorageState())) {
            this.savePath = String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + path;
            showUpdateVersionDialog();
            return;
        }
        this.savePath = String.valueOf(context2.getFilesDir().toString()) + "/";
    }

    private void showUpdateVersionDialog() {
        new AlertDialog.Builder(this.context).setTitle("软件版本更新").setMessage("检测服务器有新的版本，最新的版本为" + this.serviceVersion + "，是否立即下载更新？").setPositiveButton("立即更新", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                VersionUtil.this.showDownloadSoftwardDialog();
            }
        }).setNegativeButton("以后再说", (DialogInterface.OnClickListener) null).create().show();
    }

    /* access modifiers changed from: private */
    public void showDownloadSoftwardDialog() {
        this.DownloadDialogBuilder = new AlertDialog.Builder(this.context);
        this.DownloadDialogBuilder.setMessage("软件正在下载...");
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(-1, -1);
        TextView tvProgress2 = new TextView(this.context);
        tvProgress2.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        ProgressBar cursomProgressBar = new ProgressBar(this.context, null, 16842872);
        cursomProgressBar.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        cursomProgressBar.setPadding(10, 10, 10, 10);
        tvProgress2.setPadding(10, 10, 10, 10);
        LinearLayout llProgress = new LinearLayout(this.context);
        llProgress.setLayoutParams(params);
        llProgress.addView(cursomProgressBar);
        llProgress.addView(tvProgress2);
        llProgress.setOrientation(1);
        this.progressBar = cursomProgressBar;
        this.tvProgress = tvProgress2;
        this.DownloadDialogBuilder.setView(llProgress);
        this.DownloadDialogBuilder.setNegativeButton("取消下载", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
                VersionUtil.this.cancelFlag = true;
            }
        }).create();
        this.downDialog = this.DownloadDialogBuilder.show();
        downloadApk();
    }

    private void downloadApk() {
        new Thread(new Runnable() {
            public void run() {
                try {
                    HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(VersionUtil.this.downloadUrl).openConnection();
                    httpURLConnection.setConnectTimeout(30000);
                    httpURLConnection.setReadTimeout(30000);
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.connect();
                    if (httpURLConnection.getResponseCode() == 200) {
                        int length = httpURLConnection.getContentLength();
                        InputStream inputStream = httpURLConnection.getInputStream();
                        File file = new File(VersionUtil.this.savePath);
                        if (!file.exists()) {
                            file.mkdir();
                        }
                        VersionUtil.this.saveFileName = String.valueOf(VersionUtil.this.savePath) + VersionUtil.this.downloadUrl.substring(VersionUtil.this.downloadUrl.lastIndexOf("/"), VersionUtil.this.downloadUrl.length());
                        FileOutputStream fos = new FileOutputStream(new File(VersionUtil.this.saveFileName));
                        int count = 0;
                        byte[] buffer = new byte[1024];
                        while (true) {
                            ClientLogUtil.i(getClass().getSimpleName(), "update");
                            int readNum = inputStream.read(buffer);
                            count += readNum;
                            VersionUtil.this.downloadProgress = (int) ((((float) count) / ((float) length)) * 100.0f);
                            VersionUtil.this.handler.sendEmptyMessage(1);
                            if (readNum > 0) {
                                fos.write(buffer, 0, readNum);
                                if (VersionUtil.this.cancelFlag) {
                                    break;
                                }
                            } else {
                                VersionUtil.this.handler.sendEmptyMessage(2);
                                break;
                            }
                        }
                        inputStream.close();
                        fos.close();
                        return;
                    }
                    VersionUtil.this.handler.sendEmptyMessage(4);
                } catch (MalformedURLException e) {
                    VersionUtil.this.handler.sendEmptyMessage(4);
                    e.printStackTrace();
                } catch (SocketTimeoutException e2) {
                    VersionUtil.this.handler.sendEmptyMessage(4);
                    e2.printStackTrace();
                } catch (Exception e3) {
                    VersionUtil.this.handler.sendEmptyMessage(4);
                    e3.printStackTrace();
                }
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public void installApk() {
        File apkFile = new File(this.saveFileName);
        if (apkFile.exists()) {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(Uri.parse("file://" + apkFile.toString()), "application/vnd.android.package-archive");
            this.context.startActivity(intent);
            this.downDialog.dismiss();
            updateFlag = true;
        }
    }
}
