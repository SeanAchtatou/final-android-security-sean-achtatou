package com.womenchild.hospital.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Base64;
import com.umeng.common.util.e;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class ImageUtil {
    public static final String TAG = "ImageUtil";

    public static Bitmap getBitmapFromByte(String image) {
        try {
            byte[] imageByte = string2Byte(image);
            if (imageByte != null) {
                return BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length);
            }
        } catch (IllegalArgumentException e) {
            ClientLogUtil.e(TAG, e.getMessage());
        }
        return null;
    }

    private static byte[] getBitmapByte(Bitmap bitmap) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
        try {
            bos.flush();
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
            ClientLogUtil.e(TAG, e.getMessage());
        }
        return bos.toByteArray();
    }

    public static Bitmap getBitmapFromString(String imageString) {
        byte[] imageBytes = Base64.decode(URLDecoder.decode(imageString).getBytes(), 0);
        if (imageBytes != null) {
            return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
        }
        return null;
    }

    public static String getBitmapString(Bitmap bitmap) {
        return byte2String(getBitmapByte(bitmap));
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap.Config config;
        int width = drawable.getIntrinsicWidth();
        int height = drawable.getIntrinsicHeight();
        if (drawable.getOpacity() != -1) {
            config = Bitmap.Config.ARGB_8888;
        } else {
            config = Bitmap.Config.RGB_565;
        }
        Bitmap bitmap = Bitmap.createBitmap(width, height, config);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, width, height);
        drawable.draw(canvas);
        return bitmap;
    }

    public static Drawable bitmapToDrawable(Bitmap bitmap) {
        return new BitmapDrawable(bitmap);
    }

    private static byte[] string2Byte(String strSource) {
        try {
            strSource = URLDecoder.decode(strSource, e.f);
            ClientLogUtil.i(TAG, strSource);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            ClientLogUtil.e(TAG, e.getMessage());
        }
        return Base64.decode(strSource, 0);
    }

    private static String byte2String(byte[] bytes) {
        return Base64.encodeToString(bytes, 0, bytes.length, 0);
    }
}
