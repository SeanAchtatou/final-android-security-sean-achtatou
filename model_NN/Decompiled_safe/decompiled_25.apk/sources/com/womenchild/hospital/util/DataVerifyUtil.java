package com.womenchild.hospital.util;

import java.util.regex.Pattern;

public class DataVerifyUtil {
    public static boolean isMobileNo(String mobile) {
        return Pattern.compile("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$").matcher(mobile).matches();
    }
}
