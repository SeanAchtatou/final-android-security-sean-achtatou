package com.womenchild.hospital.util;

import android.content.Context;
import android.content.SharedPreferences;
import com.amap.mapapi.poisearch.PoiTypeDef;
import org.json.JSONObject;

public class UserCacheUtil {
    private static final String PREFERENCES_AUTO = "AUTO_STAT";
    private static final String PREFERENCES_DATE = "LOGIN_DATA";
    private static final String PREFERENCES_LOGIN_STAT = "LOGIN_STAT";
    private static final String PREFERENCES_NAME = "MOBILE_NUM";
    private static final String PREFERENCES_PWD = "PWD_STAT";

    public static boolean saveCacheUserName(Context context, String username, String password) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERENCES_NAME, 32768).edit();
        editor.putString("username", username);
        editor.putString("password", password);
        return editor.commit();
    }

    public static void clearCacheUserName(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERENCES_NAME, 32768).edit();
        editor.clear();
        editor.commit();
    }

    public static void clearAuto(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERENCES_AUTO, 32768).edit();
        editor.clear();
        editor.commit();
    }

    public static String getCacheUserName(Context context) {
        return context.getSharedPreferences(PREFERENCES_NAME, 32768).getString("username", PoiTypeDef.All);
    }

    public static String getCachePassword(Context context) {
        return context.getSharedPreferences(PREFERENCES_NAME, 32768).getString("password", PoiTypeDef.All);
    }

    public static boolean saveLoginStat(Context context, String stat, JSONObject json) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERENCES_LOGIN_STAT, 32768).edit();
        editor.putString("stat", stat);
        editor.putString("json", json.toString());
        return editor.commit();
    }

    public static boolean saveLoginData(Context context, JSONObject json) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERENCES_DATE, 32768).edit();
        editor.putString("json", json.toString());
        return editor.commit();
    }

    public static void clearLoginData(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERENCES_DATE, 32768).edit();
        editor.clear();
        editor.commit();
    }

    public static boolean saveAutoStat(Context context, String stat) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERENCES_AUTO, 32768).edit();
        editor.putString("stat", stat);
        return editor.commit();
    }

    public static boolean savePwdStat(Context context, String stat) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERENCES_PWD, 32768).edit();
        editor.putString("stat", stat);
        return editor.commit();
    }

    public static void clearPWDData(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERENCES_PWD, 32768).edit();
        editor.clear();
        editor.commit();
    }

    public static String getPWDStat(Context context) {
        return context.getSharedPreferences(PREFERENCES_PWD, 32768).getString("stat", PoiTypeDef.All);
    }

    public static String getAutoStat(Context context) {
        return context.getSharedPreferences(PREFERENCES_AUTO, 32768).getString("stat", PoiTypeDef.All);
    }

    public static String getloginStat(Context context) {
        return context.getSharedPreferences(PREFERENCES_LOGIN_STAT, 32768).getString("stat", PoiTypeDef.All);
    }

    public static String getloginJson(Context context) {
        return context.getSharedPreferences(PREFERENCES_LOGIN_STAT, 32768).getString("json", PoiTypeDef.All);
    }

    public static String getloginJsonData(Context context) {
        return context.getSharedPreferences(PREFERENCES_DATE, 32768).getString("json", PoiTypeDef.All);
    }

    public static void clearLoginStat(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERENCES_LOGIN_STAT, 32768).edit();
        editor.clear();
        editor.commit();
    }
}
