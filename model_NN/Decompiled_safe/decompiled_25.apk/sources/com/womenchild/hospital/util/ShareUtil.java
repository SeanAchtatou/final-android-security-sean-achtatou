package com.womenchild.hospital.util;

import android.content.Context;
import android.content.Intent;
import com.womenchild.hospital.R;

public class ShareUtil {
    public static void share2Friend(Context context, int content) {
        String shareText = String.format(context.getResources().getString(R.string.share_to_friend), context.getString(content));
        Intent shareIntent = new Intent("android.intent.action.SEND");
        shareIntent.setType("text/plain");
        shareIntent.putExtra("android.intent.extra.TEXT", shareText);
        context.startActivity(shareIntent);
    }
}
