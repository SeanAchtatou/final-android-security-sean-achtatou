package com.womenchild.hospital.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.widget.ImageView;
import com.womenchild.hospital.http.HttpConnector;
import java.io.IOException;
import java.io.InputStream;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

public class ImageLoadUtil extends AsyncTask<String, Void, Bitmap> {
    private ImageView imageView;
    private String url;

    public ImageLoadUtil(ImageView imageView2) {
        this.imageView = imageView2;
    }

    public ImageView getImageView() {
        return this.imageView;
    }

    public void setImageView(ImageView imageView2) {
        this.imageView = imageView2;
    }

    private Bitmap downloadBitmap(String url2) {
        HttpEntity entity;
        InputStream inputStream;
        HttpClient client = HttpConnector.getInstance().getHttpClient(true);
        HttpGet getRequest = new HttpGet(url2);
        try {
            HttpResponse response = client.execute(getRequest);
            if (response.getStatusLine().getStatusCode() == 200) {
                entity = response.getEntity();
                if (entity != null) {
                    inputStream = null;
                    inputStream = entity.getContent();
                    Bitmap decodeStream = BitmapFactory.decodeStream(inputStream);
                    if (inputStream != null) {
                        inputStream.close();
                    }
                    entity.consumeContent();
                    if (client instanceof AndroidHttpClient) {
                        ((AndroidHttpClient) client).close();
                    }
                    return decodeStream;
                } else if (!(client instanceof AndroidHttpClient)) {
                    return null;
                } else {
                    ((AndroidHttpClient) client).close();
                    return null;
                }
            } else if (!(client instanceof AndroidHttpClient)) {
                return null;
            } else {
                ((AndroidHttpClient) client).close();
                return null;
            }
        } catch (ClientProtocolException e) {
            try {
                e.printStackTrace();
                getRequest.abort();
            } finally {
                if (client instanceof AndroidHttpClient) {
                    ((AndroidHttpClient) client).close();
                }
            }
        } catch (IOException e2) {
            e2.printStackTrace();
            getRequest.abort();
            if (!(client instanceof AndroidHttpClient)) {
                return null;
            }
            ((AndroidHttpClient) client).close();
            return null;
        } catch (Throwable th) {
            if (inputStream != null) {
                inputStream.close();
            }
            entity.consumeContent();
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Bitmap result) {
        if (this.imageView != null) {
            this.imageView.setBackgroundDrawable(null);
            this.imageView.setImageBitmap(result);
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
    }

    /* access modifiers changed from: protected */
    public Bitmap doInBackground(String... params) {
        this.url = params[0];
        return downloadBitmap(this.url);
    }
}
