package com.womenchild.hospital.util;

import android.text.format.DateFormat;
import com.amap.mapapi.poisearch.PoiTypeDef;

public class DateUtil {
    public static String getCurrentTime() {
        long dataTaken = System.currentTimeMillis();
        if (dataTaken != 0) {
            return DateFormat.format("yyyy-MM-dd kk:mm:ss", dataTaken).toString();
        }
        return null;
    }

    public static boolean isSameDate(long twoDate) {
        return DateFormat.format("yyyy-MM-dd", System.currentTimeMillis()).toString().equals(DateFormat.format("yyyy-MM-dd", twoDate).toString());
    }

    public static String replaceDate(String date) {
        if (date == null) {
            return " ";
        }
        String subDate = date.substring("2012-".length());
        return String.valueOf(subDate.substring(0, subDate.indexOf("-"))) + "月" + subDate.substring(subDate.indexOf("-") + 1) + "日";
    }

    public static String getTimeFromLong(long time) {
        if (time != 0) {
            return DateFormat.format("yyyy-MM-dd kk:mm", time).toString();
        }
        return null;
    }

    public static String getTimeFromLong(long time, String format) {
        if (time != 0) {
            return DateFormat.format(format, time).toString();
        }
        return null;
    }

    public static String getBrithDate(String idCard) {
        try {
            if (new CardIDUtil().cardVerification(idCard)) {
                return String.valueOf(new String()) + idCard.substring(6, 10) + "-" + idCard.substring(10, 12) + "-" + idCard.substring(12, 14);
            }
        } catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String longToString(long dataTaken, String format) {
        if (dataTaken != 0) {
            return DateFormat.format(format, dataTaken).toString();
        }
        return PoiTypeDef.All;
    }

    public static String longToString(long dataTaken) {
        if (dataTaken != 0) {
            return DateFormat.format("kk:mm", dataTaken).toString();
        }
        return PoiTypeDef.All;
    }

    public static String getTime() {
        long dataTaken = System.currentTimeMillis();
        if (dataTaken != 0) {
            return DateFormat.format("yyyyMMddhhmmss", dataTaken).toString();
        }
        return null;
    }
}
