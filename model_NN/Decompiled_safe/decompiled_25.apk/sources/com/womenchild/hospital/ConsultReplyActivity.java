package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.mapapi.location.LocationManagerProxy;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.umeng.common.a;
import com.womenchild.hospital.adapter.ConsultReplyAdapter;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ConsultReplyActivity extends BaseRequestActivity implements View.OnClickListener {
    private static final String TAG = "ConsultActivity";
    private Button btn_send;
    String content;
    private EditText et_reply_content;
    private View headerView;
    private ImageView iv_return_home;
    private ListView lv_reply;
    private ProgressDialog pDialog;
    private String queid;
    private int replayType = 0;
    private RelativeLayout rl_reply;
    private TextView tv_age;
    private TextView tv_now_consult;
    private TextView tv_questions;
    private TextView tv_questions_contents;
    private TextView tv_sex;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.consult_reply_list);
        initViewId();
        initClickListener();
        initData();
        this.pDialog = new ProgressDialog(this);
        this.pDialog.setMessage(getResources().getString(R.string.loading_data));
        this.pDialog.show();
        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.CONSULTINFO_DETAIL), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.CONSULTINFO_DETAIL)));
    }

    public void initData() {
        Bundle bundle = getIntent().getExtras();
        if (!bundle.getBoolean(LocationManagerProxy.KEY_STATUS_CHANGED)) {
            this.tv_now_consult.setVisibility(8);
        }
        try {
            JSONObject json = new JSONObject(bundle.getString("data"));
            this.queid = json.optString("id");
            this.tv_questions.setText(json.optString("title"));
            if (json.optInt("gender") == 0) {
                this.tv_sex.setText(getResources().getString(R.string.woman));
            } else {
                this.tv_sex.setText(getResources().getString(R.string.man));
            }
            this.tv_age.setText(String.valueOf(json.optString("age")) + getResources().getString(R.string.age));
            this.tv_questions_contents.setText(json.optString("content"));
            this.lv_reply.setHeaderDividersEnabled(false);
            this.lv_reply.addHeaderView(this.headerView);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void onClick(View v) {
        if (this.iv_return_home == v) {
            finish();
        } else if (this.tv_now_consult == v) {
            if (!"追加问题".equals(this.tv_now_consult.getText().toString())) {
                this.rl_reply.setVisibility(8);
                this.tv_now_consult.setText(getResources().getString(R.string.problems));
            } else if (this.replayType != 0) {
                Toast.makeText(this, getResources().getString(R.string.tips_wait_reply), 0).show();
            } else {
                this.rl_reply.setVisibility(0);
                this.tv_now_consult.setText(getResources().getString(R.string.tips_cacel));
            }
        } else if (this.btn_send == v) {
            send();
        }
    }

    public void refreshActivity(Object... params) {
        this.pDialog.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            JSONObject result = (JSONObject) params[2];
            switch (requestType) {
                case HttpRequestParameters.CONSULTINFO_DETAIL /*502*/:
                    if (result.optJSONObject("res").optInt("st") == 0) {
                        loadData(requestType, result.optJSONArray("inf"));
                        return;
                    } else {
                        Toast.makeText(this, result.optJSONObject("res").optString("msg"), 0).show();
                        return;
                    }
                case HttpRequestParameters.ADD_REPLY /*503*/:
                    int resultCode = result.optJSONObject("res").optInt("st");
                    String msg = result.optJSONObject("res").optString("msg");
                    switch (resultCode) {
                        case 0:
                            Toast.makeText(this, getResources().getString(R.string.submit_ok), 0).show();
                            finish();
                            return;
                        case 1:
                            Toast.makeText(this, msg, 0).show();
                            return;
                        case 99:
                            Toast.makeText(this, msg, 0).show();
                            return;
                        default:
                            return;
                    }
                default:
                    return;
            }
        } else {
            Toast.makeText(this, getResources().getString(R.string.service_ex), 0).show();
        }
    }

    public void initViewId() {
        this.lv_reply = (ListView) findViewById(R.id.lv_reply);
        this.iv_return_home = (ImageView) findViewById(R.id.iv_return_home);
        this.rl_reply = (RelativeLayout) findViewById(R.id.rl_reply);
        this.btn_send = (Button) findViewById(R.id.btn_send);
        this.et_reply_content = (EditText) findViewById(R.id.et_reply_content);
        this.tv_now_consult = (TextView) findViewById(R.id.tv_now_consult);
        this.headerView = ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.consult_reply_list_header, (ViewGroup) null);
        this.tv_questions = (TextView) this.headerView.findViewById(R.id.tv_questions);
        this.tv_age = (TextView) this.headerView.findViewById(R.id.tv_age);
        this.tv_sex = (TextView) this.headerView.findViewById(R.id.tv_sex);
        this.tv_questions_contents = (TextView) this.headerView.findViewById(R.id.tv_questions_contents);
    }

    public void initClickListener() {
        this.iv_return_home.setOnClickListener(this);
        this.btn_send.setOnClickListener(this);
        this.tv_now_consult.setOnClickListener(this);
    }

    public void loadData(int requestType, Object data) {
        JSONArray jsonArray = (JSONArray) data;
        int length = jsonArray.length();
        for (int i = 0; i < length; i++) {
            this.replayType = jsonArray.optJSONObject(i).optInt(a.b);
        }
        this.lv_reply.setAdapter((ListAdapter) new ConsultReplyAdapter(this, jsonArray));
    }

    public void send() {
        boolean flag = true;
        this.content = this.et_reply_content.getText().toString();
        if (this.content.equals(PoiTypeDef.All)) {
            Toast.makeText(this, getResources().getString(R.string.content_null), 0).show();
            this.et_reply_content.findFocus();
            flag = false;
        }
        if (flag) {
            sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.ADD_REPLY), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.ADD_REPLY)));
        }
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.CONSULTINFO_DETAIL /*502*/:
                parameters.add("queid", this.queid);
                break;
            case HttpRequestParameters.ADD_REPLY /*503*/:
                parameters.add("queid", this.queid);
                parameters.add("content", this.content);
                parameters.add(a.b, 1);
                break;
        }
        return parameters;
    }
}
