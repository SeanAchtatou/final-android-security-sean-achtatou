package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.request.RequestTask;
import com.womenchild.hospital.util.Base64Util;
import com.womenchild.hospital.util.ClientLogUtil;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONObject;

public class ForgetPwdActivity extends BaseRequestActivity implements View.OnClickListener {
    private static final String TAG = "ForgetPwdActivity";
    private final int COUNT_DOWN = 0;
    private Button btn_register;
    private EditText et_msg_identifying_code;
    private EditText et_phone;
    private EditText et_pwd;
    private EditText et_pwd_again;
    private Intent intent;
    private Button iv_return_home;
    private String newPwd;
    private ProgressDialog progressDialog;
    boolean regiserPass = true;
    /* access modifiers changed from: private */
    public String sFormat;
    /* access modifiers changed from: private */
    public TextView tv_receive_msg_identifying_code;
    private String userName;
    private String vedifyCode = PoiTypeDef.All;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.forget_pwd);
        initViewId();
        initClickListener();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void refreshActivity(Object... params) {
        this.progressDialog.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        boolean status = ((Boolean) params[1]).booleanValue();
        if (status) {
            JSONObject result = (JSONObject) params[2];
            ClientLogUtil.i(TAG, "refreshActivity() Request type:" + requestType);
            ClientLogUtil.i(TAG, "refreshActivity() Status:" + status);
            ClientLogUtil.i(TAG, "refreshActivity() Result:" + result);
            switch (requestType) {
                case HttpRequestParameters.EDIT_USER_PWD /*102*/:
                    validateData(result);
                    return;
                case HttpRequestParameters.GET_VERIFY_CODE /*103*/:
                    int resultCode = result.optJSONObject("res").optInt("st");
                    String msg = result.optJSONObject("res").optString("msg");
                    switch (resultCode) {
                        case 0:
                            this.vedifyCode = result.optJSONObject("inf").optString("code");
                            this.regiserPass = true;
                            Timer timer = new Timer();
                            timer.schedule(new TimerTask(timer) {
                                Handler mHandler;
                                int time = 60;

                                {
                                    this.mHandler = new Handler() {
                                        public void handleMessage(Message msg) {
                                            switch (msg.what) {
                                                case 0:
                                                    ForgetPwdActivity.this.tv_receive_msg_identifying_code.setText(String.format(ForgetPwdActivity.this.sFormat, Integer.valueOf(AnonymousClass1.this.time)));
                                                    ForgetPwdActivity.this.tv_receive_msg_identifying_code.setBackgroundResource(R.drawable.box_shape_blue);
                                                    ForgetPwdActivity.this.tv_receive_msg_identifying_code.setTextColor(-1);
                                                    if (AnonymousClass1.this.time == 0) {
                                                        r3.cancel();
                                                        ForgetPwdActivity.this.tv_receive_msg_identifying_code.setText((int) R.string.tips_get_auth_code);
                                                        ForgetPwdActivity.this.tv_receive_msg_identifying_code.setBackgroundResource(0);
                                                        ForgetPwdActivity.this.tv_receive_msg_identifying_code.setEnabled(true);
                                                        ForgetPwdActivity.this.tv_receive_msg_identifying_code.setTextColor(ForgetPwdActivity.this.getResources().getColor(R.color.light_blue));
                                                        ForgetPwdActivity.this.regiserPass = false;
                                                        return;
                                                    }
                                                    return;
                                                default:
                                                    return;
                                            }
                                        }
                                    };
                                }

                                public void run() {
                                    this.time--;
                                    this.mHandler.sendMessage(this.mHandler.obtainMessage(0));
                                }
                            }, 1000, 1000);
                            return;
                        case 1:
                            Toast.makeText(this, msg, 0).show();
                            this.tv_receive_msg_identifying_code.setEnabled(true);
                            return;
                        case 99:
                            Toast.makeText(this, msg, 0).show();
                            this.tv_receive_msg_identifying_code.setEnabled(true);
                            return;
                        default:
                            return;
                    }
                default:
                    return;
            }
        } else {
            Toast.makeText(this, getResources().getString(R.string.network_connect_failed_prompt), 0).show();
        }
    }

    private void validateData(JSONObject result) {
        try {
            if ("0".equals(result.getJSONObject("res").getString("st"))) {
                Toast.makeText(this, getResources().getString(R.string.change_password_ok), 0).show();
                finish();
                return;
            }
            Toast.makeText(this, getResources().getString(R.string.getback_pw_no), 0).show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void initViewId() {
        this.iv_return_home = (Button) findViewById(R.id.iv_return_home);
        this.et_phone = (EditText) findViewById(R.id.et_phone);
        this.et_pwd = (EditText) findViewById(R.id.et_pwd);
        this.et_pwd_again = (EditText) findViewById(R.id.et_pwd_again);
        this.et_msg_identifying_code = (EditText) findViewById(R.id.et_msg_identifying_code);
        this.tv_receive_msg_identifying_code = (TextView) findViewById(R.id.tv_receive_msg_identifying_code);
        this.btn_register = (Button) findViewById(R.id.btn_register);
    }

    public void initClickListener() {
        this.iv_return_home.setOnClickListener(this);
        this.tv_receive_msg_identifying_code.setOnClickListener(this);
        this.btn_register.setOnClickListener(this);
    }

    public void onClick(View v) {
        if (this.iv_return_home == v) {
            finish();
        } else if (this.tv_receive_msg_identifying_code == v) {
            boolean pass = true;
            if (PoiTypeDef.All.equals(this.et_phone.getText().toString())) {
                this.et_phone.setError(getResources().getString(R.string.not_null));
                pass = false;
                this.et_phone.requestFocus();
            } else if (this.et_phone.getText().toString().length() != 11) {
                this.et_phone.setError(getResources().getString(R.string.correct_phone));
                pass = false;
                this.et_phone.requestFocus();
            }
            if (pass) {
                this.sFormat = getString(R.string.tips_get_auth_code_again);
                this.userName = this.et_phone.getText().toString();
                this.tv_receive_msg_identifying_code.setEnabled(false);
                this.progressDialog = new ProgressDialog(this);
                this.progressDialog.setMessage(getResources().getString(R.string.identifying));
                this.progressDialog.setCancelable(true);
                this.progressDialog.setCanceledOnTouchOutside(false);
                this.progressDialog.show();
                sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.GET_VERIFY_CODE), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.GET_VERIFY_CODE)));
            }
        } else if (this.btn_register == v) {
            String code = this.et_msg_identifying_code.getText().toString().toString();
            if (code.length() <= 0) {
                Toast.makeText(this, getResources().getString(R.string.ip_identifying), 0).show();
                this.et_msg_identifying_code.requestFocus();
            } else if (!this.vedifyCode.equals(code)) {
                Toast.makeText(this, getResources().getString(R.string.ip_identifying_expire), 0).show();
                this.et_msg_identifying_code.requestFocus();
            } else if (this.et_phone.getText().toString().trim().equals(PoiTypeDef.All)) {
                Toast.makeText(this, getResources().getString(R.string.phone_null_prompt), 0).show();
                this.et_phone.requestFocus();
            } else if (!this.et_phone.getText().toString().trim().equals(this.userName)) {
                Toast.makeText(this, getResources().getString(R.string.ip_identifying_error), 0).show();
                this.et_phone.requestFocus();
            } else if (this.et_pwd.getText().toString().trim().equals(PoiTypeDef.All)) {
                Toast.makeText(this, getResources().getString(R.string.pwd_null_prompt), 0).show();
                this.et_pwd.requestFocus();
            } else {
                int length = this.et_pwd.getText().toString().trim().length();
                if (length < 6 || length > 16) {
                    Toast.makeText(this, getResources().getString(R.string.pw_length), 0).show();
                    this.et_pwd.requestFocus();
                } else if (this.et_pwd_again.getText().toString().trim().equals(PoiTypeDef.All) || !this.et_pwd.getText().toString().trim().equals(this.et_pwd_again.getText().toString().trim())) {
                    Toast.makeText(this, getResources().getString(R.string.pwd_not_equal_prompt), 0).show();
                    this.et_pwd_again.requestFocus();
                } else {
                    int length2 = this.et_pwd_again.getText().toString().trim().length();
                    if (length2 < 6 || length2 > 16) {
                        Toast.makeText(this, getResources().getString(R.string.ok_pw_length), 0).show();
                        this.et_pwd_again.requestFocus();
                        return;
                    }
                    this.newPwd = this.et_pwd.getText().toString().trim();
                    this.userName = this.et_phone.getText().toString().trim();
                    this.progressDialog = new ProgressDialog(this);
                    this.progressDialog.setMessage(getResources().getString(R.string.ok_submit_info));
                    this.progressDialog.setCancelable(true);
                    this.progressDialog.setCanceledOnTouchOutside(false);
                    this.progressDialog.show();
                    RequestTask.getInstance().sendHttpRequest(this, String.valueOf((int) HttpRequestParameters.EDIT_USER_PWD), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.EDIT_USER_PWD)));
                }
            }
        }
    }

    public void loadData(int requestType, Object data) {
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.EDIT_USER_PWD /*102*/:
                parameters.add("username", this.userName);
                parameters.add("password", Base64Util.MD5(this.newPwd));
                parameters.add("oldpassword", PoiTypeDef.All);
                parameters.add("updateflag", 0);
                break;
            case HttpRequestParameters.GET_VERIFY_CODE /*103*/:
                parameters.add("mobile", this.userName);
                break;
        }
        return parameters;
    }
}
