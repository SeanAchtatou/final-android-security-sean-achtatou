package com.womenchild.hospital;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.womenchild.hospital.adapter.QueueCallFragmentPagerAdapter;
import com.womenchild.hospital.util.ClientLogUtil;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class TestReportActivity extends FragmentActivity implements View.OnClickListener {
    private static final String TAG = "TestReportActivity";
    public static ArrayList<Fragment> fragmentsList;
    public static ViewPager mPager;
    private Button btnQuery;
    /* access modifiers changed from: private */
    public int currIndex = 0;
    /* access modifiers changed from: private */
    public int currentTime = 0;
    /* access modifiers changed from: private */
    public Calendar dateAndTime = Calendar.getInstance(Locale.CHINA);
    private DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            TestReportActivity.this.dateAndTime.set(1, year);
            TestReportActivity.this.dateAndTime.set(2, monthOfYear);
            TestReportActivity.this.dateAndTime.set(5, dayOfMonth);
            SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            try {
                if (TestReportActivity.this.currentTime == 0) {
                    TestReportActivity.this.tvTime.setText(sDateFormat.format(sDateFormat.parse(TestReportActivity.this.fmtDateAndTime.format(TestReportActivity.this.dateAndTime.getTime()))));
                } else {
                    TestReportActivity.this.tvEndTime.setText(sDateFormat.format(sDateFormat.parse(TestReportActivity.this.fmtDateAndTime.format(TestReportActivity.this.dateAndTime.getTime()))));
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    };
    /* access modifiers changed from: private */
    public DateFormat fmtDateAndTime = DateFormat.getDateTimeInstance();
    private Button ivBack;
    private ImageView iv_take_medicine;
    private ImageView iv_vis;
    private Spinner spTestType;
    private String[] testType = {"请选择查询类型", "检验结果", "检查结果"};
    /* access modifiers changed from: private */
    public TextView tvEndTime;
    /* access modifiers changed from: private */
    public TextView tvTime;
    private TextView tv_take_medicine;
    private TextView tv_vis;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        ClientLogUtil.v(TAG, "onCreate()");
        setContentView((int) R.layout.test_report);
        Intent intent = getIntent();
        if (intent != null) {
            this.currIndex = intent.getIntExtra("index", 0);
        }
        initViewId();
        initViewPager();
        initClickListener();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void initViewId() {
        this.ivBack = (Button) findViewById(R.id.iv_back);
        this.tv_vis = (TextView) findViewById(R.id.tv_vis);
        this.tv_take_medicine = (TextView) findViewById(R.id.tv_take_medicine);
        this.iv_vis = (ImageView) findViewById(R.id.iv_vis);
        this.iv_take_medicine = (ImageView) findViewById(R.id.iv_take_medicine);
        if (this.currIndex == 1) {
            this.ivBack.setText("返回");
            this.ivBack.setBackgroundResource(R.drawable.back_selector);
        }
        this.spTestType = (Spinner) findViewById(R.id.sp_test_type);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, (int) R.layout.spinner_item, this.testType);
        adapter.setDropDownViewResource(17367049);
        this.spTestType.setAdapter((SpinnerAdapter) adapter);
        this.tvTime = (TextView) findViewById(R.id.tv_start_time);
        this.btnQuery = (Button) findViewById(R.id.btn_query);
        this.tvEndTime = (TextView) findViewById(R.id.tv_end_time);
    }

    public void initClickListener() {
        this.ivBack.setOnClickListener(this);
        this.tv_vis.setOnClickListener(new MyOnClickListener(0));
        this.tv_take_medicine.setOnClickListener(new MyOnClickListener(3));
        this.tvTime.setOnClickListener(this);
        this.btnQuery.setOnClickListener(this);
        this.tvEndTime.setOnClickListener(this);
    }

    public class MyOnClickListener implements View.OnClickListener {
        private int index = 0;

        public MyOnClickListener(int i) {
            this.index = i;
        }

        public void onClick(View v) {
            TestReportActivity.mPager.setCurrentItem(this.index);
            TestReportActivity.this.switchImage(this.index);
        }
    }

    public class MyOnPageChangeListener implements ViewPager.OnPageChangeListener {
        public MyOnPageChangeListener() {
        }

        public void onPageSelected(int arg0) {
            TestReportActivity.this.currIndex = arg0;
            TestReportActivity.this.switchImage(TestReportActivity.this.currIndex);
        }

        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        public void onPageScrollStateChanged(int arg0) {
        }
    }

    private void initViewPager() {
        mPager = (ViewPager) findViewById(R.id.vPager);
        fragmentsList = new ArrayList<>();
        Fragment checkReportFragment = new CheckReportFragment();
        Fragment testReportFragement = new TestReportFragment();
        fragmentsList.add(checkReportFragment);
        fragmentsList.add(testReportFragement);
        mPager.setAdapter(new QueueCallFragmentPagerAdapter(getSupportFragmentManager(), fragmentsList));
        mPager.setCurrentItem(1);
        mPager.setOnPageChangeListener(new MyOnPageChangeListener());
        switchImage(1);
    }

    /* access modifiers changed from: private */
    public void switchImage(int index) {
        switch (index) {
            case 0:
                this.tv_vis.setTextColor(getResources().getColor(R.color.white));
                this.tv_take_medicine.setTextColor(getResources().getColor(R.color.black));
                this.iv_vis.setBackgroundResource(R.drawable.ygkz_number_blank_blue_1);
                this.iv_take_medicine.setBackgroundResource(R.drawable.ygkz_number_blank_withline_3);
                return;
            case 1:
                this.tv_vis.setTextColor(getResources().getColor(R.color.black));
                this.tv_take_medicine.setTextColor(getResources().getColor(R.color.white));
                this.iv_vis.setBackgroundResource(R.drawable.ygkz_number_blank_withline_1);
                this.iv_take_medicine.setBackgroundResource(R.drawable.ygkz_number_blank_blue_3);
                return;
            default:
                return;
        }
    }

    public void onClick(View v) {
        String action;
        if (this.ivBack == v) {
            finish();
        } else if (v != this.tv_vis && v != this.tv_take_medicine) {
            if (v.getId() == R.id.tv_start_time) {
                this.currentTime = 0;
                new DatePickerDialog(this, this.dateSetListener, this.dateAndTime.get(1), this.dateAndTime.get(2), this.dateAndTime.get(5)).show();
            } else if (v.getId() == R.id.btn_query) {
                int position = this.spTestType.getSelectedItemPosition();
                if (position == 0) {
                    Toast.makeText(this, this.testType[position], 0).show();
                } else if ("请选择检查检验起始时间".equals(this.tvTime.getText().toString())) {
                    Toast.makeText(this, "请选择检查检验起始时间", 0).show();
                } else if ("请选择检查检验结束时间".equals(this.tvEndTime.getText().toString())) {
                    Toast.makeText(this, "请选择检查检验结束时间", 0).show();
                } else {
                    if (position == 1) {
                        action = "TestReport";
                    } else {
                        action = "CheckReport";
                    }
                    Intent intent = new Intent();
                    intent.setAction(action);
                    intent.putExtra("startdate", this.tvTime.getText().toString());
                    intent.putExtra("enddate", this.tvEndTime.getText().toString());
                    startActivity(intent);
                }
            } else if (v.getId() == R.id.tv_end_time) {
                this.currentTime = 1;
                new DatePickerDialog(this, this.dateSetListener, this.dateAndTime.get(1), this.dateAndTime.get(2), this.dateAndTime.get(5)).show();
            }
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                ListAdapter listdapter = new ArrayAdapter(this, 17367043, (int) R.array.filter_test);
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("请选择");
                builder.setAdapter(listdapter, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                return builder.create();
            default:
                return null;
        }
    }
}
