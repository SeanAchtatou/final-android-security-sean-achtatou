package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.parameter.UriParameter;
import org.json.JSONException;
import org.json.JSONObject;

public class MoreAboutHospitalActivity extends BaseRequestActivity implements View.OnClickListener {
    private Button ibtnHome;
    private Intent intent;
    private ProgressDialog pDialog;
    private TextView tvContent;
    private TextView wv_hp_introduce_text;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.about_hospital);
        initViewId();
        initClickListener();
        initData();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_home /*2131296435*/:
                finish();
                return;
            default:
                return;
        }
    }

    public void refreshActivity(Object... params) {
        int requestType = ((Integer) params[0]).intValue();
        if (((Boolean) params[1]).booleanValue()) {
            loadData(requestType, (JSONObject) params[2]);
        } else {
            Toast.makeText(this, getResources().getString(R.string.network_connect_failed_prompt), 0).show();
        }
    }

    public void initViewId() {
        this.ibtnHome = (Button) findViewById(R.id.ibtn_home);
        this.tvContent = (TextView) findViewById(R.id.tv_content);
        this.wv_hp_introduce_text = (TextView) findViewById(R.id.wv_hp_introduce_text);
        this.wv_hp_introduce_text.setText((int) R.string.about_hospital);
    }

    public void initClickListener() {
        this.ibtnHome.setOnClickListener(this);
    }

    public void loadData(int requestType, Object data) {
        this.pDialog.dismiss();
        JSONObject jsonObject = (JSONObject) data;
        JSONObject res = jsonObject.optJSONObject("res");
        if (res != null) {
            try {
                if (res.getInt("st") == 0) {
                    jsonObject.optJSONObject("inf");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        parameters.add("num", 8);
        return parameters;
    }
}
