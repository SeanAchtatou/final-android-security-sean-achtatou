package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.Base64Util;
import com.womenchild.hospital.util.ClientLogUtil;
import org.json.JSONObject;

public class ChangePwdActivity extends BaseRequestActivity implements View.OnClickListener {
    private static final String TAG = "CPAct";
    private static final String UPDATE_FLAG = "1";
    private EditText et_confirm_pwd;
    private EditText et_new_pwd;
    private EditText et_original_pwd;
    private Button iv_return;
    private String newPwd;
    private String oldPwd;
    private ProgressDialog progressDialog;
    private String pwd;
    private TextView tv_submit;
    private String userName;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.change_pwd);
        initViewId();
        initClickListener();
        initData();
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.userName = UserEntity.getInstance().getUsername();
        this.pwd = UserEntity.getInstance().getPassword();
        ClientLogUtil.d(TAG, "initData() UserName:" + this.userName + ",Pwd:" + this.pwd);
    }

    public void onClick(View v) {
        if (this.iv_return == v) {
            finish();
        } else if (v != this.tv_submit) {
        } else {
            if (this.et_original_pwd.getText().toString().trim().equals(PoiTypeDef.All)) {
                Toast.makeText(this, getResources().getString(R.string.original_pwd_null_prompt), 0).show();
                this.et_original_pwd.requestFocus();
            } else if (this.et_new_pwd.getText().toString().trim().equals(PoiTypeDef.All)) {
                Toast.makeText(this, getResources().getString(R.string.new_pwd_null_prompt), 0).show();
                this.et_new_pwd.requestFocus();
            } else {
                int length = this.et_new_pwd.getText().toString().length();
                if (length < 6 || length > 16) {
                    Toast.makeText(this, getResources().getString(R.string.new_pw), 0).show();
                    this.et_new_pwd.requestFocus();
                } else if (this.et_confirm_pwd.getText().toString().trim().equals(PoiTypeDef.All)) {
                    Toast.makeText(this, getResources().getString(R.string.confirm_pwd_null_prompt), 0).show();
                    this.et_confirm_pwd.requestFocus();
                } else {
                    int length2 = this.et_confirm_pwd.getText().toString().length();
                    if (length2 < 6 || length2 > 16) {
                        Toast.makeText(this, getResources().getString(R.string.confirm_pw), 0).show();
                        this.et_confirm_pwd.requestFocus();
                    } else if (!this.et_new_pwd.getText().toString().trim().equals(this.et_confirm_pwd.getText().toString().trim())) {
                        Toast.makeText(this, getResources().getString(R.string.pwd_not_equal_prompt), 0).show();
                        this.et_confirm_pwd.requestFocus();
                    } else if (!this.pwd.equals(this.et_original_pwd.getText().toString().trim())) {
                        Toast.makeText(this, getResources().getString(R.string.original_pwd_right_prompt), 0).show();
                        this.et_original_pwd.requestFocus();
                        this.et_original_pwd.setSelection(0, this.et_original_pwd.getText().toString().trim().length());
                        this.et_original_pwd.setSelectAllOnFocus(true);
                    } else {
                        this.oldPwd = this.et_original_pwd.getText().toString().trim();
                        this.newPwd = this.et_new_pwd.getText().toString().trim();
                        this.progressDialog.show();
                        sendHttpRequest(Integer.valueOf((int) HttpRequestParameters.EDIT_USER_PWD), initRequestParameter(Integer.valueOf((int) HttpRequestParameters.EDIT_USER_PWD)));
                    }
                }
            }
        }
    }

    public void refreshActivity(Object... params) {
        this.progressDialog.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        boolean status = ((Boolean) params[1]).booleanValue();
        if (status) {
            JSONObject result = (JSONObject) params[2];
            ClientLogUtil.i(TAG, "refreshActivity() Request type:" + requestType);
            ClientLogUtil.i(TAG, "refreshActivity() Status:" + status);
            ClientLogUtil.i(TAG, "refreshActivity() Result:" + result);
            switch (requestType) {
                case HttpRequestParameters.EDIT_USER_PWD /*102*/:
                    validateData(result);
                    return;
                default:
                    return;
            }
        } else {
            Toast.makeText(this, getResources().getString(R.string.network_connect_failed_prompt), 0).show();
        }
    }

    private void validateData(JSONObject result) {
        try {
            if ("0".equals(result.getJSONObject("res").getString("st"))) {
                ClientLogUtil.d(TAG, "validateData() Result: " + result.toString());
                Toast.makeText(this, result.getJSONObject("res").getString("msg"), 0).show();
                this.pwd = this.newPwd;
                UserEntity.getInstance().setUsername(this.userName);
                UserEntity.getInstance().setPassword(this.pwd);
                finish();
                return;
            }
            Toast.makeText(this, result.getJSONObject("res").getString("msg"), 0).show();
        } catch (Exception ex) {
            ex.printStackTrace();
            ClientLogUtil.e(TAG, "Exception:" + ex.getMessage() + "," + ex.toString());
        }
    }

    public void initViewId() {
        this.iv_return = (Button) findViewById(R.id.iv_return);
        this.tv_submit = (TextView) findViewById(R.id.tv_submit);
        this.et_original_pwd = (EditText) findViewById(R.id.et_original_pwd);
        this.et_new_pwd = (EditText) findViewById(R.id.et_new_pwd);
        this.et_confirm_pwd = (EditText) findViewById(R.id.et_confirm_pwd);
        this.progressDialog = new ProgressDialog(this);
        this.progressDialog.setMessage(getResources().getString(R.string.submit_pw));
        this.progressDialog.setCancelable(false);
        this.progressDialog.setCanceledOnTouchOutside(false);
    }

    public void initClickListener() {
        this.iv_return.setOnClickListener(this);
        this.tv_submit.setOnClickListener(this);
    }

    public void loadData(int requestType, Object data) {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.EDIT_USER_PWD /*102*/:
                ClientLogUtil.i(TAG, "initRequestParams() 用户名：" + this.userName + ",原密码：" + this.oldPwd + ",新密码：" + this.newPwd + ",修改标志：" + UPDATE_FLAG);
                parameters.add("username", this.userName);
                parameters.add("password", Base64Util.MD5(this.newPwd));
                parameters.add("oldpassword", Base64Util.MD5(this.oldPwd));
                parameters.add("updateflag", UPDATE_FLAG);
                break;
        }
        return parameters;
    }
}
