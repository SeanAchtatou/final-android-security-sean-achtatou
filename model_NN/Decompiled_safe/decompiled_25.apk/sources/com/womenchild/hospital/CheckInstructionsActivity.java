package com.womenchild.hospital;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.parameter.UriParameter;

public class CheckInstructionsActivity extends BaseRequestActivity implements View.OnClickListener {
    private static final String TAG = "OverDueHelpActivity";
    private ImageView iv_back;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.check_and_test);
        initViewId();
        initClickListener();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void refreshActivity(Object... params) {
    }

    public void initViewId() {
        this.iv_back = (ImageView) findViewById(R.id.iv_back);
    }

    public void initClickListener() {
        this.iv_back.setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onClick(View v) {
        if (this.iv_back == v) {
            finish();
        }
    }

    public void loadData(int requestType, Object data) {
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        return null;
    }
}
