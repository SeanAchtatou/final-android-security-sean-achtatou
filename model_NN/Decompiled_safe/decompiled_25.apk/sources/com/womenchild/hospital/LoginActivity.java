package com.womenchild.hospital;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.base.BaseRequestActivity;
import com.womenchild.hospital.entity.UserEntity;
import com.womenchild.hospital.entity.UserInfoEntity;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.request.RequestTask;
import com.womenchild.hospital.util.AESEncryptor;
import com.womenchild.hospital.util.ClientLogUtil;
import com.womenchild.hospital.util.UserCacheUtil;
import org.json.JSONObject;

public class LoginActivity extends BaseRequestActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private static final String TAG = "LAct";
    private boolean autoLogin;
    private TextView btn_register;
    private EditText et_phone;
    private EditText et_pwd;
    private Button ibtn_login;
    private Intent intent;
    private Button iv_return_home;
    private Context mContext = this;
    private String phone;
    private ProgressDialog progressDialog;
    private String pwd;
    private boolean rememberPwd;
    private ToggleButton tb_auto_login;
    private ToggleButton tb_remember_pwd;
    private TextView tv_forget_pwd;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.login);
        initViewId();
        initClickListener();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        initData();
    }

    public void refreshActivity(Object... params) {
        this.progressDialog.dismiss();
        int requestType = ((Integer) params[0]).intValue();
        boolean status = ((Boolean) params[1]).booleanValue();
        if (status) {
            JSONObject result = (JSONObject) params[2];
            ClientLogUtil.i(TAG, "refreshActivity() Request type:" + requestType);
            ClientLogUtil.i(TAG, "refreshActivity() Status:" + status);
            ClientLogUtil.i(TAG, "refreshActivity() Result:" + result);
            switch (requestType) {
                case HttpRequestParameters.USER_LOGIN /*100*/:
                    initUserData(result);
                    return;
                default:
                    return;
            }
        } else {
            Toast.makeText(this, getResources().getString(R.string.network_connect_failed_prompt), 0).show();
        }
    }

    private void initUserData(JSONObject result) {
        String passWord;
        if ("0".equals(result.optJSONObject("res").optString("st"))) {
            ClientLogUtil.d(TAG, "initUserData() Result: " + result.toString());
            JSONObject object = result.optJSONObject("inf").optJSONObject("patient");
            UserEntity.getInstance().setUsername(this.phone);
            UserEntity.getInstance().setPassword(this.pwd);
            UserInfoEntity info = new UserInfoEntity();
            info.setAccId(result.optJSONObject("inf").optString("accid"));
            if (object != null && !object.toString().trim().equals(null) && !object.toString().trim().equals(PoiTypeDef.All)) {
                ClientLogUtil.v(TAG, "initUserData() Init user info: " + object.toString());
                info.setCitizenCard(object.optString("citizzencard"));
                info.setDefaultPatientCard(object.optString("defaultpatientcard"));
                info.setEmail(object.optString("email"));
                info.setHealthyCard(object.optString("healthycard"));
                info.setIdCard(object.optString("idcard"));
                info.setName(object.optString("patientname"));
                info.setSocialSecurity(object.optString("socialsecuritycard"));
                info.setMobile(object.optString("mobile"));
                info.setAddress(object.optString("address"));
                info.setUserid(object.optString("userid"));
                info.setPatientid(object.optString("patientid"));
            }
            UserEntity.getInstance().setInfo(info);
            Toast.makeText(this, result.optJSONObject("res").optString("msg"), 0).show();
            HomeActivity.loginFlag = true;
            UserCacheUtil.clearAuto(this.mContext);
            UserCacheUtil.saveCacheUserName(this.mContext, this.phone, PoiTypeDef.All);
            if (this.rememberPwd) {
                UserCacheUtil.clearPWDData(this.mContext);
                UserCacheUtil.clearCacheUserName(this.mContext);
                try {
                    passWord = AESEncryptor.encrypt("4561237", this.pwd);
                } catch (Exception e) {
                    Toast.makeText(this, getResources().getString(R.string.pw_encryption_error), 0).show();
                    passWord = PoiTypeDef.All;
                }
                UserCacheUtil.saveCacheUserName(this.mContext, this.phone, passWord);
                UserCacheUtil.savePwdStat(this.mContext, "1");
            } else {
                UserCacheUtil.clearPWDData(this.mContext);
            }
            if (this.autoLogin) {
                UserCacheUtil.clearAuto(this.mContext);
                UserCacheUtil.saveAutoStat(this.mContext, "1");
            } else {
                UserCacheUtil.clearAuto(this.mContext);
            }
            setResult(1, getIntent());
            finish();
            return;
        }
        Toast.makeText(this, result.optJSONObject("res").optString("msg"), 0).show();
    }

    public void initViewId() {
        this.iv_return_home = (Button) findViewById(R.id.iv_return_home);
        this.tv_forget_pwd = (TextView) findViewById(R.id.tv_forget_pwd);
        this.et_phone = (EditText) findViewById(R.id.et_phone);
        this.et_pwd = (EditText) findViewById(R.id.et_pwd);
        this.tb_remember_pwd = (ToggleButton) findViewById(R.id.tb_remember_pwd);
        this.tb_auto_login = (ToggleButton) findViewById(R.id.tb_auto_login);
        this.ibtn_login = (Button) findViewById(R.id.ibtn_login);
        this.btn_register = (TextView) findViewById(R.id.btn_register);
        this.progressDialog = new ProgressDialog(this);
        this.progressDialog.setMessage(getResources().getString(R.string.login_info));
        this.progressDialog.setCancelable(true);
        this.progressDialog.setCanceledOnTouchOutside(false);
        this.et_phone.setText(PoiTypeDef.All);
        this.et_pwd.setText(PoiTypeDef.All);
    }

    public void initClickListener() {
        this.iv_return_home.setOnClickListener(this);
        this.btn_register.setOnClickListener(this);
        this.tb_remember_pwd.setOnCheckedChangeListener(this);
        this.tb_auto_login.setOnCheckedChangeListener(this);
        this.ibtn_login.setOnClickListener(this);
        this.tv_forget_pwd.setOnClickListener(this);
    }

    public void onClick(View v) {
        if (v == this.iv_return_home) {
            Intent intent2 = new Intent(this, HomeActivity.class);
            intent2.setFlags(67108864);
            startActivity(intent2);
        } else if (v == this.tv_forget_pwd) {
            this.intent = new Intent(this, ForgetPwdActivity.class);
            startActivity(this.intent);
        } else if (v == this.ibtn_login) {
            if (this.et_phone.getText().toString().trim().equals(PoiTypeDef.All)) {
                Toast.makeText(this, getResources().getString(R.string.ip_phone), 0).show();
                this.et_phone.requestFocus();
            } else if (this.et_pwd.getText().toString().trim().equals(PoiTypeDef.All)) {
                Toast.makeText(this, getResources().getString(R.string.ip_pw), 0).show();
                this.et_pwd.requestFocus();
            } else {
                this.phone = this.et_phone.getText().toString().trim();
                this.pwd = this.et_pwd.getText().toString().trim();
                this.progressDialog.show();
                RequestTask.getInstance().sendHttpRequest(this, String.valueOf(100), initRequestParameter(100));
            }
        } else if (this.btn_register == v) {
            this.intent = new Intent(this, RegisterActivity.class);
            startActivityForResult(this.intent, 0);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ClientLogUtil.i(TAG, "onActivityResult() requestCode:" + requestCode + ",resultCode:" + resultCode);
        if (resultCode == 1) {
            finish();
        }
    }

    public void loadData(int requestType, Object data) {
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.tb_remember_pwd /*2131296845*/:
                if (isChecked) {
                    this.rememberPwd = true;
                    return;
                }
                this.rememberPwd = false;
                this.autoLogin = false;
                this.tb_auto_login.setChecked(this.autoLogin);
                UserCacheUtil.clearPWDData(this.mContext);
                UserCacheUtil.clearCacheUserName(this.mContext);
                return;
            case R.id.tv_auto_login_title /*2131296846*/:
            default:
                return;
            case R.id.tb_auto_login /*2131296847*/:
                if (isChecked) {
                    this.tb_remember_pwd.setChecked(true);
                    this.rememberPwd = true;
                    this.autoLogin = true;
                    UserCacheUtil.clearAuto(this.mContext);
                    UserCacheUtil.saveAutoStat(this.mContext, "1");
                    return;
                }
                this.autoLogin = false;
                UserCacheUtil.clearAuto(this.mContext);
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
        String passWord;
        String cacheName = UserCacheUtil.getCacheUserName(this.mContext);
        if (!PoiTypeDef.All.equals(cacheName)) {
            this.et_phone.setText(cacheName);
            String passWord2 = UserCacheUtil.getCachePassword(this.mContext);
            if (!PoiTypeDef.All.equals(passWord2)) {
                try {
                    passWord = AESEncryptor.decrypt("4561237", passWord2);
                } catch (Exception e) {
                    Toast.makeText(this, getResources().getString(R.string.pw_deciphering), 0).show();
                    passWord = PoiTypeDef.All;
                }
                this.et_pwd.setText(passWord);
            }
        }
        if ("1".equals(UserCacheUtil.getPWDStat(this.mContext))) {
            this.tb_remember_pwd.setChecked(true);
            this.rememberPwd = true;
        }
        if ("1".equals(UserCacheUtil.getAutoStat(this.mContext))) {
            this.tb_auto_login.setChecked(true);
            this.autoLogin = true;
        }
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        UriParameter parameters = new UriParameter();
        switch (Integer.parseInt(String.valueOf(requestCode))) {
            case HttpRequestParameters.USER_LOGIN /*100*/:
                parameters.add("username", this.phone);
                parameters.add("password", this.pwd);
                break;
        }
        return parameters;
    }
}
