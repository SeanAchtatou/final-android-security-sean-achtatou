package com.womenchild.hospital;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import com.womenchild.hospital.adapter.HpGuideFragmentPagerAdapter;
import com.womenchild.hospital.base.BaseRequestFragmentActivity;
import com.womenchild.hospital.parameter.UriParameter;
import com.womenchild.hospital.util.ClientLogUtil;
import java.util.ArrayList;

public class HospitalGuideActivity extends BaseRequestFragmentActivity implements View.OnClickListener {
    private static final String TAG = "HospitalGuideActivity";
    public static int currentIndex = 0;
    private ArrayList<Fragment> fragmentsList;
    private RadioGroup gdGroup;
    private ImageButton ibtnIntroduce;
    private ImageButton ibtnRooms;
    private ImageButton ibtnTraffic;
    private Context mContext = this;
    RadioGroup.OnCheckedChangeListener onCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        public void onCheckedChanged(RadioGroup arg0, int tabId) {
            switch (tabId) {
                case R.id.ibtn_hp_introduce /*2131296819*/:
                    HospitalGuideActivity.this.viewPager.setCurrentItem(0);
                    ClientLogUtil.i(HospitalGuideActivity.TAG, "ibtn_hp_introduce");
                    return;
                case R.id.ibtn_hp_rooms /*2131296820*/:
                    HospitalGuideActivity.this.viewPager.setCurrentItem(1);
                    ClientLogUtil.i(HospitalGuideActivity.TAG, "ibtn_hp_rooms");
                    return;
                case R.id.ibtn_hp_traffic /*2131296821*/:
                    HospitalGuideActivity.this.viewPager.setCurrentItem(2);
                    ClientLogUtil.i(HospitalGuideActivity.TAG, "ibtn_hp_traffic");
                    return;
                default:
                    return;
            }
        }
    };
    private Resources resources;
    /* access modifiers changed from: private */
    public ViewPager viewPager;

    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        ClientLogUtil.v(TAG, "onAttachFragment()");
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView((int) R.layout.hospital_guide);
        Intent intent = getIntent();
        if (intent != null) {
            currentIndex = intent.getIntExtra("index", 0);
        }
        initViewId();
        initClickListener();
        initViewPager();
        this.resources = getResources();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        ClientLogUtil.v(TAG, "onDestroy()");
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        ClientLogUtil.v(TAG, "onPause()");
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        ClientLogUtil.v(TAG, "onResume()");
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        ClientLogUtil.v(TAG, "onStart()");
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        ClientLogUtil.v(TAG, "onRestart()");
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        ClientLogUtil.v(TAG, "onStop()");
    }

    public void initViewId() {
        this.gdGroup = (RadioGroup) findViewById(R.id.rl_buttom_bar);
        this.gdGroup.setOnCheckedChangeListener(this.onCheckedChangeListener);
    }

    public void initClickListener() {
    }

    private void initViewPager() {
        this.viewPager = (ViewPager) findViewById(R.id.viewPager);
        this.fragmentsList = new ArrayList<>(3);
        Fragment hpIntroduceFragment = new HospitalIntroduceFragment();
        Fragment hpRoomsFragment = new HospitalRoomsFragment();
        Fragment hpTrafficFragment = new HospitalTrafficFragment();
        this.fragmentsList.add(hpIntroduceFragment);
        this.fragmentsList.add(hpRoomsFragment);
        this.fragmentsList.add(hpTrafficFragment);
        this.viewPager.setAdapter(new HpGuideFragmentPagerAdapter(getSupportFragmentManager(), this.fragmentsList));
        this.viewPager.setCurrentItem(currentIndex);
        this.viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageSelected(int arg0) {
                HospitalGuideActivity.currentIndex = arg0;
                HospitalGuideActivity.this.switchImage(HospitalGuideActivity.currentIndex);
            }

            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            public void onPageScrollStateChanged(int arg0) {
            }
        });
        switchImage(currentIndex);
    }

    /* access modifiers changed from: private */
    public void switchImage(int currentIndex2) {
        switch (currentIndex2) {
            case 0:
                this.gdGroup.check(R.id.ibtn_hp_introduce);
                return;
            case 1:
                this.gdGroup.check(R.id.ibtn_hp_rooms);
                return;
            case 2:
                this.gdGroup.check(R.id.ibtn_hp_traffic);
                return;
            default:
                return;
        }
    }

    public void onClick(View v) {
        v.getId();
    }

    public void loadData(int requestType, Object data) {
    }

    /* access modifiers changed from: protected */
    public void initViewId(View view) {
    }

    /* access modifiers changed from: protected */
    public void initUIData() {
    }

    /* access modifiers changed from: protected */
    public UriParameter initRequestParameter(Object requestCode) {
        return null;
    }

    public void refreshFragment(Object... objects) {
    }
}
