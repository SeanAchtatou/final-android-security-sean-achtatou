package com.womenchild.hospital;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;
import com.womenchild.hospital.entity.DiseaseEntity;
import com.womenchild.hospital.view.AdapterForLinearLayout;
import com.womenchild.hospital.view.LinearLayoutForListView;
import java.util.ArrayList;
import java.util.HashMap;

public class SearchDoctorByDiseaseActivity extends Activity implements View.OnClickListener {
    private static final String TAG = "SDBDActivity";
    private ImageView backIv;
    private ImageButton collectIbtn;
    private Intent intent;
    /* access modifiers changed from: private */
    public ArrayList<HashMap<String, Object>> list1 = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayList<HashMap<String, Object>> list2 = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayList<HashMap<String, Object>> list3 = new ArrayList<>();
    private LinearLayoutForListView lv_common_disease;
    private LinearLayoutForListView lv_eye_trouble;
    private LinearLayoutForListView lv_stomach_trouble;
    private ImageButton orderIbtn;
    private ImageButton roomIbtn;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.search_doctor_by_disease);
        initViewId();
        initClickListener();
        DiseaseEntity disease1 = new DiseaseEntity();
        disease1.setName(getResources().getString(R.string.disease_a));
        DiseaseEntity disease2 = new DiseaseEntity();
        disease2.setName(getResources().getString(R.string.disease_b));
        DiseaseEntity disease3 = new DiseaseEntity();
        disease3.setName(getResources().getString(R.string.disease_c));
        DiseaseEntity disease4 = new DiseaseEntity();
        disease4.setName(getResources().getString(R.string.disease_d));
        DiseaseEntity disease5 = new DiseaseEntity();
        disease5.setName(getResources().getString(R.string.disease_e));
        DiseaseEntity disease6 = new DiseaseEntity();
        disease6.setName(getResources().getString(R.string.disease_f));
        HashMap<String, Object> map1 = new HashMap<>();
        map1.put("disease_name", disease1.getName());
        this.list1.add(map1);
        HashMap<String, Object> map2 = new HashMap<>();
        map2.put("disease_name", disease2.getName());
        this.list1.add(map2);
        HashMap<String, Object> map3 = new HashMap<>();
        map3.put("disease_name", disease3.getName());
        this.list1.add(map3);
        HashMap<String, Object> map4 = new HashMap<>();
        map4.put("disease_name", disease4.getName());
        this.list1.add(map4);
        HashMap<String, Object> map5 = new HashMap<>();
        map5.put("disease_name", disease5.getName());
        this.list1.add(map5);
        HashMap<String, Object> map6 = new HashMap<>();
        map6.put("disease_name", disease6.getName());
        this.list1.add(map6);
        AdapterForLinearLayout layoutAdpater1 = new AdapterForLinearLayout(this, this.list1, R.layout.disease_item, new String[]{"disease_name"}, new int[]{R.id.tv_disease});
        this.lv_common_disease.setOnclickLinstener(new View.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(SearchDoctorByDiseaseActivity.this, String.valueOf(SearchDoctorByDiseaseActivity.this.getResources().getString(R.string.select_user_next)) + ((HashMap) SearchDoctorByDiseaseActivity.this.list1.get(((Integer) v.getTag()).intValue())).get("disease_name"), 0).show();
            }
        });
        this.lv_common_disease.setAdapter(layoutAdpater1);
        DiseaseEntity disease11 = new DiseaseEntity();
        disease11.setName(getResources().getString(R.string.disease_g));
        DiseaseEntity disease12 = new DiseaseEntity();
        disease12.setName(getResources().getString(R.string.disease_h));
        DiseaseEntity disease13 = new DiseaseEntity();
        disease13.setName(getResources().getString(R.string.disease_i));
        DiseaseEntity disease14 = new DiseaseEntity();
        disease14.setName(getResources().getString(R.string.disease_j));
        DiseaseEntity disease15 = new DiseaseEntity();
        disease15.setName(getResources().getString(R.string.disease_k));
        HashMap<String, Object> map11 = new HashMap<>();
        map11.put("disease_name", disease11.getName());
        this.list2.add(map11);
        HashMap<String, Object> map12 = new HashMap<>();
        map12.put("disease_name", disease12.getName());
        this.list2.add(map12);
        HashMap<String, Object> map13 = new HashMap<>();
        map13.put("disease_name", disease13.getName());
        this.list2.add(map13);
        HashMap<String, Object> map14 = new HashMap<>();
        map14.put("disease_name", disease14.getName());
        this.list2.add(map14);
        HashMap<String, Object> map15 = new HashMap<>();
        map15.put("disease_name", disease15.getName());
        this.list2.add(map15);
        AdapterForLinearLayout layoutAdpater2 = new AdapterForLinearLayout(this, this.list2, R.layout.disease_item, new String[]{"disease_name"}, new int[]{R.id.tv_disease});
        this.lv_stomach_trouble.setOnclickLinstener(new View.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(SearchDoctorByDiseaseActivity.this, String.valueOf(SearchDoctorByDiseaseActivity.this.getResources().getString(R.string.select_user_next)) + ((HashMap) SearchDoctorByDiseaseActivity.this.list2.get(((Integer) v.getTag()).intValue())).get("disease_name"), 0).show();
            }
        });
        this.lv_stomach_trouble.setAdapter(layoutAdpater2);
        DiseaseEntity disease21 = new DiseaseEntity();
        disease21.setName(getResources().getString(R.string.disease_l));
        DiseaseEntity disease22 = new DiseaseEntity();
        disease22.setName(getResources().getString(R.string.disease_m));
        DiseaseEntity disease23 = new DiseaseEntity();
        disease23.setName(getResources().getString(R.string.disease_n));
        DiseaseEntity disease24 = new DiseaseEntity();
        disease24.setName(getResources().getString(R.string.disease_o));
        DiseaseEntity disease25 = new DiseaseEntity();
        disease25.setName(getResources().getString(R.string.disease_p));
        DiseaseEntity disease26 = new DiseaseEntity();
        disease26.setName(getResources().getString(R.string.disease_r));
        DiseaseEntity disease27 = new DiseaseEntity();
        disease27.setName(getResources().getString(R.string.disease_t));
        HashMap<String, Object> map21 = new HashMap<>();
        map21.put("disease_name", disease21.getName());
        this.list3.add(map21);
        HashMap<String, Object> map22 = new HashMap<>();
        map22.put("disease_name", disease22.getName());
        this.list3.add(map22);
        HashMap<String, Object> map23 = new HashMap<>();
        map23.put("disease_name", disease23.getName());
        this.list3.add(map23);
        HashMap<String, Object> map24 = new HashMap<>();
        map24.put("disease_name", disease24.getName());
        this.list3.add(map24);
        HashMap<String, Object> map25 = new HashMap<>();
        map25.put("disease_name", disease25.getName());
        this.list3.add(map25);
        HashMap<String, Object> map26 = new HashMap<>();
        map26.put("disease_name", disease26.getName());
        this.list3.add(map26);
        HashMap<String, Object> map27 = new HashMap<>();
        map27.put("disease_name", disease27.getName());
        this.list3.add(map27);
        AdapterForLinearLayout layoutAdpater3 = new AdapterForLinearLayout(this, this.list3, R.layout.disease_item, new String[]{"disease_name"}, new int[]{R.id.tv_disease});
        this.lv_eye_trouble.setOnclickLinstener(new View.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(SearchDoctorByDiseaseActivity.this, String.valueOf(SearchDoctorByDiseaseActivity.this.getResources().getString(R.string.select_user_next)) + ((HashMap) SearchDoctorByDiseaseActivity.this.list3.get(((Integer) v.getTag()).intValue())).get("disease_name"), 0).show();
            }
        });
        this.lv_eye_trouble.setAdapter(layoutAdpater3);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void initViewId() {
        this.lv_common_disease = (LinearLayoutForListView) findViewById(R.id.lv_common_disease);
        this.lv_stomach_trouble = (LinearLayoutForListView) findViewById(R.id.lv_stomach_trouble);
        this.lv_eye_trouble = (LinearLayoutForListView) findViewById(R.id.lv_eye_trouble);
        this.roomIbtn = (ImageButton) findViewById(R.id.ibtn_room);
        this.orderIbtn = (ImageButton) findViewById(R.id.ibtn_order);
        this.collectIbtn = (ImageButton) findViewById(R.id.ibtn_collect);
        this.backIv = (ImageView) findViewById(R.id.iv_home);
    }

    public void initClickListener() {
        this.roomIbtn.setOnClickListener(this);
        this.orderIbtn.setOnClickListener(this);
        this.collectIbtn.setOnClickListener(this);
        this.backIv.setOnClickListener(this);
    }

    public void onClick(View v) {
        if (v == this.lv_common_disease) {
            Toast.makeText(this, String.valueOf(getResources().getString(R.string.select_user_next)) + this.list1.get(((Integer) v.getTag()).intValue()).get("disease_name"), 0).show();
        } else if (this.lv_stomach_trouble == v) {
            Toast.makeText(this, String.valueOf(getResources().getString(R.string.select_user_next)) + this.list2.get(((Integer) v.getTag()).intValue()).get("disease_name"), 0).show();
        } else if (this.lv_eye_trouble == v) {
            Toast.makeText(this, String.valueOf(getResources().getString(R.string.select_user_next)) + this.list3.get(((Integer) v.getTag()).intValue()).get("disease_name"), 0).show();
        }
        switch (v.getId()) {
            case R.id.ibtn_room:
                this.intent = new Intent(this, FindDoctorByRoomActivity.class);
                startActivity(this.intent);
                return;
            case R.id.ibtn_order:
                this.intent = new Intent(this, SearchDoctorByOrderedActivity.class);
                startActivity(this.intent);
                return;
            case R.id.iv_home:
                finish();
                return;
            case R.id.ibtn_collect:
                this.intent = new Intent(this, SearchDoctorByCollectedActivity.class);
                startActivity(this.intent);
                return;
            default:
                return;
        }
    }
}
