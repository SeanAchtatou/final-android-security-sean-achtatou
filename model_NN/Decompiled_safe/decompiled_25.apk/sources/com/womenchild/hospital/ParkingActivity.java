package com.womenchild.hospital;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.mapapi.core.AMapException;
import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.core.PoiItem;
import com.amap.mapapi.map.MapActivity;
import com.amap.mapapi.map.MapController;
import com.amap.mapapi.map.MapView;
import com.amap.mapapi.map.PoiOverlay;
import com.amap.mapapi.map.Projection;
import com.amap.mapapi.poisearch.PoiPagedResult;
import com.amap.mapapi.poisearch.PoiSearch;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.autonavi.aps.api.Constant;
import com.womenchild.hospital.util.ClientLogUtil;
import java.util.List;

public class ParkingActivity extends MapActivity implements View.OnClickListener, DialogInterface.OnDismissListener, DialogInterface.OnCancelListener {
    private static final String KEYWORD = "停车场";
    private static final double LATITUDE = 23.125398d;
    private static final double LONGITUDE = 113.320332d;
    private static final String TAG = "PAct";
    private static final int ZOOM = 17;
    /* access modifiers changed from: private */
    public int curpage = 1;
    /* access modifiers changed from: private */
    public GeoPoint geoPoint;
    /* access modifiers changed from: private */
    @SuppressLint({"HandlerLeak"})
    public Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            List<PoiItem> poiItems;
            ParkingActivity.this.progressDialog.dismiss();
            ParkingActivity.this.loadFlag = 0;
            if (msg.what == 1) {
                ClientLogUtil.d(ParkingActivity.TAG, "PioItem inited...");
                try {
                    if (ParkingActivity.this.result == null || (poiItems = ParkingActivity.this.result.getPage(1)) == null || poiItems.size() <= 0) {
                        if (ParkingActivity.this.poiOverlay != null) {
                            ParkingActivity.this.poiOverlay.removeFromMap();
                            ParkingActivity.this.mapView.postInvalidate();
                        }
                        Toast.makeText(ParkingActivity.this.getApplicationContext(), ParkingActivity.this.getResources().getString(R.string.result), 0).show();
                        return;
                    }
                    if (ParkingActivity.this.poiOverlay != null) {
                        ParkingActivity.this.poiOverlay.removeFromMap();
                    }
                    ParkingActivity.this.poiOverlay = new MyPoiOverlay(ParkingActivity.this.getResources().getDrawable(R.drawable.parking_query_blue_bubbling), poiItems);
                    ParkingActivity.this.poiOverlay.addToMap(ParkingActivity.this.mapView);
                    ParkingActivity.this.poiOverlay.displayNearest();
                } catch (AMapException e) {
                    Toast.makeText(ParkingActivity.this.getApplicationContext(), (int) R.string.network_connect_failed_prompt, 0).show();
                }
            } else if (msg.what == 2) {
                Toast.makeText(ParkingActivity.this.getApplicationContext(), ParkingActivity.this.getResources().getString(R.string.search_park_result), 0).show();
            } else if (msg.what == 3) {
                ParkingActivity parkingActivity = ParkingActivity.this;
                parkingActivity.curpage = parkingActivity.curpage + 1;
                try {
                    List<PoiItem> poiItems2 = ParkingActivity.this.result.getPage(ParkingActivity.this.curpage);
                    if (poiItems2 != null && poiItems2.size() > 0) {
                        if (ParkingActivity.this.poiOverlay != null) {
                            ParkingActivity.this.poiOverlay.removeFromMap();
                        }
                        ParkingActivity.this.poiOverlay = new MyPoiOverlay(ParkingActivity.this.getResources().getDrawable(R.drawable.parking_query_blue_bubbling), poiItems2);
                        ParkingActivity.this.poiOverlay.addToMap(ParkingActivity.this.mapView);
                        ParkingActivity.this.poiOverlay.displayNearest();
                        ParkingActivity.this.mapView.invalidate();
                        ParkingActivity.this.progressDialog.dismiss();
                        ParkingActivity.this.loadFlag = 0;
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    };
    private Button iv_return_home;
    /* access modifiers changed from: private */
    public int loadFlag = 0;
    /* access modifiers changed from: private */
    public MapController mMapController;
    /* access modifiers changed from: private */
    public MapView mapView;
    /* access modifiers changed from: private */
    public MyPoiOverlay poiOverlay;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog;
    /* access modifiers changed from: private */
    public PoiPagedResult result;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.parking);
        initViewId();
        initClickListener();
        initMapView();
        search();
    }

    private void initMapView() {
        this.mMapController = this.mapView.getController();
        this.mapView.setBuiltInZoomControls(true);
        this.mMapController.setZoom(17);
        this.geoPoint = new GeoPoint(23125398, 113320332);
        this.mMapController.animateTo(this.geoPoint);
        this.mMapController.setCenter(this.geoPoint);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void onClick(View v) {
        if (this.iv_return_home == v) {
            finish();
        }
    }

    public void initViewId() {
        this.iv_return_home = (Button) findViewById(R.id.iv_return_home);
        this.mapView = (MapView) findViewById(R.id.mapView);
    }

    public void initClickListener() {
        this.iv_return_home.setOnClickListener(this);
    }

    private void search() {
        this.progressDialog = new ProgressDialog(this);
        this.progressDialog.setMessage(getResources().getString(R.string.loading_ok));
        this.progressDialog.setCancelable(true);
        this.progressDialog.setOnDismissListener(this);
        this.progressDialog.setOnCancelListener(this);
        this.progressDialog.setCanceledOnTouchOutside(false);
        this.loadFlag = 1;
        this.progressDialog.show();
        new Thread(new Runnable() {
            public void run() {
                PoiSearch poiSearch = new PoiSearch(ParkingActivity.this, new PoiSearch.Query(PoiTypeDef.All, ParkingActivity.KEYWORD, "020"));
                poiSearch.setBound(new PoiSearch.SearchBound(ParkingActivity.this.geoPoint, (int) Constant.imeiMaxSalt));
                poiSearch.setPageSize(20);
                try {
                    ParkingActivity.this.result = poiSearch.searchPOI();
                    ParkingActivity.this.handler.sendEmptyMessage(1);
                } catch (AMapException e) {
                    ParkingActivity.this.handler.sendEmptyMessage(2);
                    e.printStackTrace();
                }
            }
        }).start();
    }

    class MyPoiOverlay extends PoiOverlay {
        private Drawable drawable;
        private List<PoiItem> poiItems;
        private View popView;
        private String str;

        public MyPoiOverlay(Drawable arg0, List<PoiItem> arg1) {
            super(boundCenterBottom(arg0), arg1);
            this.drawable = arg0;
            this.poiItems = arg1;
            populate();
        }

        public MyPoiOverlay(Drawable arg0, List<PoiItem> arg1, String arg2) {
            super(boundCenterBottom(arg0), arg1, arg2);
            this.drawable = arg0;
            this.poiItems = arg1;
            this.str = arg2;
            populate();
        }

        /* access modifiers changed from: protected */
        public PoiItem createItem(int arg0) {
            return this.poiItems.get(arg0);
        }

        public int size() {
            if (this.poiItems == null) {
                return 0;
            }
            return this.poiItems.size();
        }

        /* access modifiers changed from: protected */
        public Drawable getPopupMarker(PoiItem arg0) {
            return super.getPopupMarker(arg0);
        }

        /* access modifiers changed from: protected */
        public View getPopupView(PoiItem arg0) {
            return super.getPopupView(arg0);
        }

        public boolean showPopupWindow(int arg0) {
            return super.showPopupWindow(arg0);
        }

        public void draw(Canvas arg0, MapView arg1, boolean arg2) {
            Projection projection = ParkingActivity.this.mapView.getProjection();
            for (int index = 0; index < this.poiItems.size(); index++) {
                PoiItem overLayItem = this.poiItems.get(index);
                String title = overLayItem.getTitle();
                projection.toPixels(overLayItem.getPoint(), null);
            }
            super.draw(arg0, arg1, arg2);
            boundCenterBottom(this.drawable);
        }

        public void displayNearest() {
            PoiItem item = this.poiItems.get(0);
            ParkingActivity.this.mMapController.animateTo(item.getPoint());
            if (this.popView == null) {
                this.popView = LayoutInflater.from(ParkingActivity.this).inflate((int) R.layout.popup_keyword, (ViewGroup) null);
                this.popView.setVisibility(8);
                ParkingActivity.this.mapView.addView(this.popView, new MapView.LayoutParams(-2, -2, item.getPoint(), 81));
            }
            TextView tvPoiName = (TextView) this.popView.findViewById(R.id.PoiName);
            TextView tvPoiAddress = (TextView) this.popView.findViewById(R.id.PoiAddress);
            TextView tvCategory = (TextView) this.popView.findViewById(R.id.Category);
            ((ImageButton) this.popView.findViewById(R.id.ImageButtonRight)).setVisibility(8);
            String address = "不详";
            if (!item.getSnippet().trim().equals(PoiTypeDef.All)) {
                address = item.getSnippet().trim();
            }
            tvPoiName.setText(item.getTitle());
            tvPoiAddress.setText(address);
            tvCategory.setText(item.getTypeDes());
            this.popView.setVisibility(0);
            ParkingActivity.this.mapView.updateViewLayout(this.popView, new MapView.LayoutParams(-2, -2, item.getPoint(), 81));
        }

        public void clear() {
            if (this.popView != null) {
                this.popView.setVisibility(8);
                ParkingActivity.this.mapView.removeView(this.popView);
            }
        }

        /* access modifiers changed from: protected */
        public boolean onTap(int arg0) {
            ClientLogUtil.i(ParkingActivity.TAG, "onTap() position:" + arg0);
            if (arg0 != -1) {
                PoiItem item = this.poiItems.get(arg0);
                if (this.popView == null) {
                    this.popView = LayoutInflater.from(ParkingActivity.this).inflate((int) R.layout.popup_keyword, (ViewGroup) null);
                    this.popView.setVisibility(8);
                    ParkingActivity.this.mapView.addView(this.popView, new MapView.LayoutParams(-2, -2, item.getPoint(), 81));
                }
                TextView tvPoiName = (TextView) this.popView.findViewById(R.id.PoiName);
                TextView tvPoiAddress = (TextView) this.popView.findViewById(R.id.PoiAddress);
                TextView tvCategory = (TextView) this.popView.findViewById(R.id.Category);
                String address = "不详";
                if (!item.getSnippet().trim().equals(PoiTypeDef.All)) {
                    address = item.getSnippet().trim();
                }
                tvPoiName.setText(item.getTitle());
                tvPoiAddress.setText(address);
                tvCategory.setText(item.getTypeDes());
                this.popView.setVisibility(0);
                ParkingActivity.this.mapView.updateViewLayout(this.popView, new MapView.LayoutParams(-2, -2, item.getPoint(), 81));
                return true;
            } else if (this.popView == null) {
                return false;
            } else {
                this.popView.setVisibility(8);
                return false;
            }
        }

        public boolean onTap(GeoPoint arg0, MapView arg1) {
            ClientLogUtil.v(ParkingActivity.TAG, "onTap() ......");
            if (this.popView != null) {
                this.popView.setVisibility(8);
            }
            return super.onTap(arg0, arg1);
        }

        public boolean draw(Canvas arg0, MapView arg1, boolean arg2, long arg3) {
            return super.draw(arg0, arg1, arg2, arg3);
        }
    }

    public void onCancel(DialogInterface dialog) {
        if (this.loadFlag == 1) {
            finish();
        }
    }

    public void onDismiss(DialogInterface dialog) {
        if (this.loadFlag == 1) {
            finish();
        }
    }
}
