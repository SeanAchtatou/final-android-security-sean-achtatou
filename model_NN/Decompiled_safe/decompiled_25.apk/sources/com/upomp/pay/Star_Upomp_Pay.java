package com.upomp.pay;

import android.app.Activity;
import android.os.Bundle;
import com.unionpay.upomp.lthj.util.PluginHelper;
import com.upomp.pay.info.Upomp_Pay_Info;

public class Star_Upomp_Pay {
    public void start_upomp_pay(Activity thisActivity, String LanchPay) {
        byte[] to_upomp = LanchPay.getBytes();
        Bundle mbundle = new Bundle();
        mbundle.putByteArray("xml", to_upomp);
        mbundle.putString("action_cmd", Upomp_Pay_Info.CMD_PAY_PLUGIN);
        mbundle.putBoolean("test", false);
        PluginHelper.LaunchPlugin(thisActivity, mbundle);
    }
}
