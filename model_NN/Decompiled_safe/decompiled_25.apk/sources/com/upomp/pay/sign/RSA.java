package com.upomp.pay.sign;

import java.io.InputStream;
import java.security.KeyStore;
import java.security.PrivateKey;
import javax.crypto.Cipher;

public class RSA {
    static byte[] rsaEncode(byte[] signsRSA, String alias, String pwd, InputStream dataSign) {
        try {
            KeyStore store = KeyStore.getInstance("PKCS12");
            InputStream inStream = dataSign;
            store.load(inStream, pwd.toCharArray());
            inStream.close();
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(1, (PrivateKey) store.getKey(alias, pwd.toCharArray()));
            return cipher.doFinal(signsRSA);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
