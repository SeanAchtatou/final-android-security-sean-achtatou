package com.upomp.pay.httpservice;

import com.amap.mapapi.poisearch.PoiTypeDef;
import com.womenchild.hospital.parameter.HttpRequestParameters;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;

public class XmlHttpConnection implements Serializable {
    private static final long serialVersionUID = -8576553457942740493L;
    private final int RESPCODE_SUCCESS;
    private String URL;
    private int errCode;
    private String errMsg;
    InputStream in;
    private String recvMsg;
    private HttpURLConnection urlCon;

    public XmlHttpConnection(String url, int timeOut) {
        this(url, new StringBuilder(String.valueOf(timeOut)).toString());
    }

    public XmlHttpConnection(String url, String timeOut) {
        this.RESPCODE_SUCCESS = HttpRequestParameters.DEPT_DETAIL;
        this.URL = url;
        System.setProperty("sun.net.client.defaultConnectTimeout", timeOut);
        System.setProperty("sun.net.client.defaultReadTimeout", timeOut);
    }

    private boolean open() {
        try {
            this.urlCon = (HttpURLConnection) new URL(this.URL).openConnection();
            return true;
        } catch (Exception e) {
            this.errCode = -11;
            this.errMsg = PoiTypeDef.All;
            return false;
        }
    }

    public boolean sendMsg(String msgStr) {
        if (!open()) {
            return false;
        }
        OutputStream os = null;
        InputStream is = null;
        try {
            this.urlCon.setRequestMethod("POST");
            this.urlCon.setRequestProperty("content-type", "text/plain");
            this.urlCon.setDoOutput(true);
            this.urlCon.setDoInput(true);
            os = this.urlCon.getOutputStream();
            OutputStreamWriter writer = new OutputStreamWriter(os);
            writer.write(URLEncoder.encode(msgStr, "utf-8"));
            writer.flush();
            try {
                int respCode = this.urlCon.getResponseCode();
                if (200 != respCode) {
                    this.errCode = -31;
                    this.errMsg = "httpState=[" + respCode + "]";
                }
                is = this.urlCon.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"));
                StringBuilder responseBuilder = new StringBuilder();
                while (true) {
                    String line = reader.readLine();
                    if (line == null) {
                        this.recvMsg = URLDecoder.decode(responseBuilder.toString(), "utf-8");
                        this.in = StringToInputStream(this.recvMsg);
                        close(is);
                        close(os);
                        this.urlCon.disconnect();
                        this.urlCon = null;
                        return true;
                    }
                    responseBuilder.append(line);
                }
            } catch (Exception e) {
                this.errCode = -31;
                this.errMsg = PoiTypeDef.All;
                return false;
            }
        } catch (Exception e2) {
            this.errCode = -21;
            this.errMsg = PoiTypeDef.All;
            return false;
        } finally {
            close(is);
            close(os);
            this.urlCon.disconnect();
            this.urlCon = null;
        }
    }

    private void close(InputStream stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (Exception e) {
            }
        }
    }

    private void close(OutputStream stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (Exception e) {
            }
        }
    }

    public int getErrCode() {
        return this.errCode;
    }

    public String getErrMsg() {
        return this.errMsg;
    }

    public InputStream getRecvMsg() {
        return this.in;
    }

    public String getReMeg() {
        return this.recvMsg;
    }

    /* access modifiers changed from: package-private */
    public InputStream StringToInputStream(String recvMsg2) {
        return new ByteArrayInputStream(recvMsg2.getBytes());
    }
}
