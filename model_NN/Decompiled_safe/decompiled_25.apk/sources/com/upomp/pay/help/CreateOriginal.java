package com.upomp.pay.help;

import com.upomp.pay.info.Upomp_Pay_Info;

public class CreateOriginal {
    public static String CreateOriginal_Sign(int args) {
        switch (args) {
            case 3:
                StringBuilder os3 = new StringBuilder();
                os3.append("merchantId=").append(Upomp_Pay_Info.merchantId).append("&merchantOrderId=").append(Upomp_Pay_Info.merchantOrderId).append("&merchantOrderTime=").append(Upomp_Pay_Info.merchantOrderTime);
                return os3.toString();
            case 4:
                StringBuilder os4 = new StringBuilder();
                os4.append("transType=").append(Upomp_Pay_Info.type[1]);
                os4.append("&merchantId=").append(Upomp_Pay_Info.merchantId).append("&merchantOrderId=").append(Upomp_Pay_Info.merchantOrderId).append("&merchantOrderTime=").append(Upomp_Pay_Info.merchantOrderTime);
                return os4.toString();
            case 5:
            default:
                System.out.println("No Thing");
                return null;
            case 6:
                StringBuilder os6 = new StringBuilder();
                os6.append("merchantId=").append(Upomp_Pay_Info.merchantId).append("&merchantOrderId=").append(Upomp_Pay_Info.merchantOrderId).append("&merchantOrderTime=").append(Upomp_Pay_Info.merchantOrderTime).append("&merchantOrderAmt=").append(Upomp_Pay_Info.merchantOrderAmt).append("&cupsQid=").append(Upomp_Pay_Info.cupsQid).append("&backEndUrl=").append(Upomp_Pay_Info.backEndUrl);
                return os6.toString();
            case 7:
                StringBuilder os7 = new StringBuilder();
                os7.append("merchantName=").append(Upomp_Pay_Info.merchantName).append("&merchantId=").append(Upomp_Pay_Info.merchantId).append("&merchantOrderId=").append(Upomp_Pay_Info.merchantOrderId).append("&merchantOrderTime=").append(Upomp_Pay_Info.merchantOrderTime).append("&merchantOrderAmt=").append(Upomp_Pay_Info.merchantOrderAmt).append("&merchantOrderDesc=").append(Upomp_Pay_Info.merchantOrderDesc).append("&transTimeout=").append(Upomp_Pay_Info.transTimeout);
                return os7.toString();
            case 8:
                StringBuilder os8 = new StringBuilder();
                os8.append("merchantId=").append(Upomp_Pay_Info.merchantId).append("&merchantOrderId=").append(Upomp_Pay_Info.merchantOrderId).append("&merchantOrderTime=").append(Upomp_Pay_Info.merchantOrderTime).append("&merchantOrderAmt=").append(Upomp_Pay_Info.merchantOrderAmt).append("&cupsQid=").append(Upomp_Pay_Info.cupsQid).append("&backEndUrl=").append(Upomp_Pay_Info.backEndUrl);
                return os8.toString();
        }
    }
}
