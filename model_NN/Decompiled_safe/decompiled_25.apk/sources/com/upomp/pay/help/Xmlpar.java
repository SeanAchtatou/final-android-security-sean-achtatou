package com.upomp.pay.help;

import android.util.Log;
import android.util.Xml;
import com.umeng.common.util.e;
import java.io.InputStream;
import org.xmlpull.v1.XmlPullParser;

public class Xmlpar {
    public void XMLparser(InputStream inputStream) throws Exception {
        GetValue value = new GetValue();
        XmlPullParser parser = Xml.newPullParser();
        parser.setInput(inputStream, e.f);
        for (int event = parser.getEventType(); event != 1; event = parser.next()) {
            switch (event) {
                case 2:
                    if ("merchantId".equals(parser.getName())) {
                        value.setMerchantId(parser.nextText());
                        Log.d("Mymessage", "______-___________" + value.getMerchantId());
                    }
                    if (!"merchantOrderId".equals(parser.getName())) {
                        if (!"merchantOrderTime".equals(parser.getName())) {
                            break;
                        } else {
                            value.setMerchantOrderTime(parser.nextText());
                            Log.d("Mymessage", "______-___________" + value.getMerchantId());
                            break;
                        }
                    } else {
                        value.setMerchantOrderId(parser.nextText());
                        Log.d("Mymessage", "______-___________" + value.getMerchantId());
                        break;
                    }
            }
        }
        System.out.println(value);
    }
}
