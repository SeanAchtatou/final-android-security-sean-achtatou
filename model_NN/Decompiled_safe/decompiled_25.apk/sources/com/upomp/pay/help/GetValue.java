package com.upomp.pay.help;

public class GetValue {
    static String themerchantId = "NULL";
    static String themerchantOrderId = "NULL";
    static String themerchantOrderTime = "NULL";

    public String getMerchantId() {
        return themerchantId;
    }

    public void setMerchantId(String merchantId) {
        themerchantId = merchantId;
    }

    public String getMerchantOrderId() {
        return themerchantOrderId;
    }

    public void setMerchantOrderId(String merchantOrderId) {
        themerchantOrderId = merchantOrderId;
    }

    public String getMerchantOrderTime() {
        return themerchantOrderTime;
    }

    public void setMerchantOrderTime(String merchantOrderTime) {
        themerchantOrderTime = merchantOrderTime;
    }

    public String toString() {
        return String.valueOf(themerchantId) + ":" + themerchantOrderId + ":" + themerchantOrderTime;
    }
}
