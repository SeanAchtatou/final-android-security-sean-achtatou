package com.upomp.pay.help;

import android.text.format.DateFormat;
import android.util.Log;
import com.upomp.pay.info.Upomp_Pay_Info;
import java.util.Date;
import java.util.Random;

public class Create_MerchantX {
    public static String createMerchantOrderId() {
        Upomp_Pay_Info.merchantOrderId = new StringBuilder().append(Double.valueOf(new Random().nextDouble())).toString().substring(3, 15);
        Log.d(Upomp_Pay_Info.tag, "this is ========" + Upomp_Pay_Info.merchantOrderId);
        return Upomp_Pay_Info.merchantOrderId;
    }

    public static String createMerchantOrderTime() {
        Upomp_Pay_Info.merchantOrderTime = DateFormat.format("yyyyMMddkkmmss", new Date()).toString();
        Log.d(Upomp_Pay_Info.tag, "this is ========" + Upomp_Pay_Info.merchantOrderTime);
        return Upomp_Pay_Info.merchantOrderTime;
    }

    public static String createMerchantOrderTime(String format) {
        Upomp_Pay_Info.merchantOrderTime = DateFormat.format(format, new Date()).toString();
        Log.d(Upomp_Pay_Info.tag, "this is ========" + Upomp_Pay_Info.merchantOrderTime);
        return Upomp_Pay_Info.merchantOrderTime;
    }
}
