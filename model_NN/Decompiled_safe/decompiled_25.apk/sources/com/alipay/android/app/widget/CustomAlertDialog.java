package com.alipay.android.app.widget;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.alipay.android.app.lib.ResourceMap;
import java.lang.ref.WeakReference;

public class CustomAlertDialog extends Dialog implements DialogInterface {
    private boolean mBeyondHoneycomb;
    private View mButtonDivider;
    private View mButtonGroup;
    private View.OnClickListener mButtonHandler;
    private int mCheckedItem = -1;
    private FrameLayout mContentView;
    private DialogCache mDialogCache;
    /* access modifiers changed from: private */
    public DialogInterface mDialogInterface;
    private ImageView mDivider;
    /* access modifiers changed from: private */
    public Handler mHandler;
    private Button mLeftButton;
    private ListAdapter mListAdapter;
    private ListView mListView;
    private TextView mMessage;
    private DialogInterface.OnClickListener mOnClickListener;
    private Button mRightButton;
    private TextView mTitle;

    public CustomAlertDialog(DialogCache dialogCache) {
        super(dialogCache.mContext, ResourceMap.getStyle_alert_dialog());
        boolean z;
        if (Build.VERSION.SDK_INT >= 11) {
            z = true;
        } else {
            z = false;
        }
        this.mBeyondHoneycomb = z;
        this.mButtonHandler = new View.OnClickListener() {
            public void onClick(View v) {
                Message buttonMessage = (Message) v.getTag();
                if (buttonMessage == null) {
                    CustomAlertDialog.this.dismiss();
                    return;
                }
                Message m = Message.obtain(buttonMessage);
                if (m != null) {
                    m.sendToTarget();
                }
                CustomAlertDialog.this.mHandler.obtainMessage(1, CustomAlertDialog.this.mDialogInterface).sendToTarget();
            }
        };
        this.mDialogCache = dialogCache;
        this.mDialogInterface = this;
        this.mHandler = new ButtonHandler(this.mDialogInterface);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(ResourceMap.getLayout_alert_dialog());
        this.mLeftButton = (Button) findViewById(ResourceMap.getId_left_button());
        this.mRightButton = (Button) findViewById(ResourceMap.getId_right_button());
        this.mButtonDivider = findViewById(ResourceMap.getId_dialog_split_v());
        this.mTitle = (TextView) findViewById(ResourceMap.getId_dialog_title());
        this.mMessage = (TextView) findViewById(ResourceMap.getId_dialog_message());
        this.mDivider = (ImageView) findViewById(ResourceMap.getId_dialog_divider());
        this.mContentView = (FrameLayout) findViewById(ResourceMap.getId_dialog_content_view());
        this.mButtonGroup = findViewById(ResourceMap.getId_dialog_button_group());
        setupTitle();
        setupMessage();
        setupView();
        setupButtons();
    }

    private void setupTitle() {
        if (TextUtils.isEmpty(this.mDialogCache.mTitle)) {
            this.mTitle.setVisibility(8);
            this.mDivider.setVisibility(8);
            return;
        }
        this.mTitle.setVisibility(0);
        this.mDivider.setVisibility(0);
        if (this.mDialogCache.mIcon != null) {
            this.mTitle.setCompoundDrawablesWithIntrinsicBounds(this.mDialogCache.mIcon, (Drawable) null, (Drawable) null, (Drawable) null);
        }
        this.mTitle.setText(this.mDialogCache.mTitle);
    }

    private void setupMessage() {
        boolean showMessage;
        if (TextUtils.isEmpty(this.mDialogCache.mMessage) || this.mDialogCache.mView != null) {
            showMessage = false;
        } else {
            showMessage = true;
        }
        if (showMessage) {
            this.mMessage.setVisibility(0);
            this.mMessage.setText(this.mDialogCache.mMessage);
            return;
        }
        this.mMessage.setVisibility(8);
    }

    private void setupView() {
        if (this.mDialogCache.mView != null) {
            this.mContentView.removeAllViews();
            this.mContentView.addView(this.mDialogCache.mView);
        }
    }

    private boolean setupButtons() {
        int i;
        int i2 = 8;
        int buttonCount = 0;
        Button positiveButton = this.mBeyondHoneycomb ? this.mRightButton : this.mLeftButton;
        Button negativeButton = this.mBeyondHoneycomb ? this.mLeftButton : this.mRightButton;
        if (TextUtils.isEmpty(this.mDialogCache.mPositiveButton)) {
            positiveButton.setVisibility(8);
        } else {
            positiveButton.setVisibility(0);
            positiveButton.setText(this.mDialogCache.mPositiveButton);
            positiveButton.setOnClickListener(this.mButtonHandler);
            positiveButton.setTag(this.mDialogCache.mButtonPositiveMessage);
            buttonCount = 0 + 1;
        }
        if (TextUtils.isEmpty(this.mDialogCache.mNegativeButton)) {
            negativeButton.setVisibility(8);
        } else {
            negativeButton.setVisibility(0);
            negativeButton.setText(this.mDialogCache.mNegativeButton);
            negativeButton.setOnClickListener(this.mButtonHandler);
            negativeButton.setTag(this.mDialogCache.mButtonNegativeMessage);
            buttonCount++;
        }
        View view = this.mButtonDivider;
        if (buttonCount > 1) {
            i = 0;
        } else {
            i = 8;
        }
        view.setVisibility(i);
        View view2 = this.mButtonGroup;
        if (buttonCount != 0) {
            i2 = 0;
        }
        view2.setVisibility(i2);
        if (buttonCount != 0) {
            return true;
        }
        return false;
    }

    public void setButton(int whichButton, CharSequence text, DialogInterface.OnClickListener listener, Message msg) {
        if (msg == null && listener != null) {
            msg = this.mHandler.obtainMessage(whichButton, listener);
        }
        switch (whichButton) {
            case -2:
                this.mDialogCache.mNegativeButton = text;
                this.mDialogCache.mButtonNegativeMessage = msg;
                return;
            case -1:
                this.mDialogCache.mPositiveButton = text;
                this.mDialogCache.mButtonPositiveMessage = msg;
                return;
            default:
                throw new IllegalArgumentException("Button does not exist");
        }
    }

    private static final class ButtonHandler extends Handler {
        private static final int MSG_DISMISS_DIALOG = 1;
        private WeakReference<DialogInterface> mDialog;

        public ButtonHandler(DialogInterface dialog) {
            super(Looper.getMainLooper());
            this.mDialog = new WeakReference<>(dialog);
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case -3:
                case -2:
                case -1:
                    ((DialogInterface.OnClickListener) msg.obj).onClick(this.mDialog.get(), msg.what);
                    return;
                case 0:
                default:
                    return;
                case 1:
                    ((DialogInterface) msg.obj).dismiss();
                    return;
            }
        }
    }

    private static class DialogCache {
        ListAdapter mAdapter;
        Message mButtonNegativeMessage;
        Message mButtonPositiveMessage;
        boolean mCancelable;
        public int mCheckedItem;
        Context mContext;
        Drawable mIcon;
        boolean mIsListMode;
        CharSequence mMessage;
        CharSequence mNegativeButton;
        DialogInterface.OnClickListener mNegativeButtonListener;
        DialogInterface.OnCancelListener mOnCancelListener;
        DialogInterface.OnClickListener mOnClickListener;
        DialogInterface.OnKeyListener mOnKeyListener;
        CharSequence mPositiveButton;
        DialogInterface.OnClickListener mPositiveButtonListener;
        CharSequence mTitle;
        View mView;

        private DialogCache() {
            this.mCancelable = false;
            this.mCheckedItem = -1;
            this.mIsListMode = false;
        }

        /* synthetic */ DialogCache(DialogCache dialogCache) {
            this();
        }
    }

    public static class Builder {
        private final DialogCache mDialogCache = new DialogCache(null);

        public Builder(Context context) {
            this.mDialogCache.mContext = context;
        }

        public Builder setMessage(int resId) {
            this.mDialogCache.mMessage = this.mDialogCache.mContext.getText(resId);
            return this;
        }

        public Builder setMessage(CharSequence message) {
            this.mDialogCache.mMessage = message;
            return this;
        }

        public Builder setTitle(int resId) {
            return setTitle(this.mDialogCache.mContext.getText(resId));
        }

        public Builder setTitle(CharSequence title) {
            this.mDialogCache.mTitle = title;
            return this;
        }

        public Builder setIcon(int resId) {
            return setIcon(this.mDialogCache.mContext.getResources().getDrawable(resId));
        }

        public Builder setIcon(Drawable drawable) {
            this.mDialogCache.mIcon = drawable;
            return this;
        }

        public Builder setView(View view) {
            this.mDialogCache.mView = view;
            return this;
        }

        public Builder setPositiveButton(int resId, DialogInterface.OnClickListener listener) {
            return setPositiveButton(this.mDialogCache.mContext.getString(resId), listener);
        }

        public Builder setPositiveButton(CharSequence text, DialogInterface.OnClickListener listener) {
            this.mDialogCache.mPositiveButton = text;
            this.mDialogCache.mPositiveButtonListener = listener;
            return this;
        }

        public Builder setNegativeButton(int resId, DialogInterface.OnClickListener listener) {
            return setNegativeButton(this.mDialogCache.mContext.getText(resId), listener);
        }

        public Builder setNegativeButton(CharSequence text, DialogInterface.OnClickListener listener) {
            this.mDialogCache.mNegativeButton = text;
            this.mDialogCache.mNegativeButtonListener = listener;
            return this;
        }

        public Builder setSingleChoiceItems(ListAdapter adapter, int checkedItem, DialogInterface.OnClickListener listener) {
            this.mDialogCache.mIsListMode = true;
            this.mDialogCache.mAdapter = adapter;
            this.mDialogCache.mOnClickListener = listener;
            this.mDialogCache.mCheckedItem = checkedItem;
            return this;
        }

        public void apply(CustomAlertDialog dialog) {
            if (this.mDialogCache.mPositiveButton != null) {
                dialog.setButton(-1, this.mDialogCache.mPositiveButton, this.mDialogCache.mPositiveButtonListener, null);
            }
            if (this.mDialogCache.mNegativeButton != null) {
                dialog.setButton(-2, this.mDialogCache.mNegativeButton, this.mDialogCache.mNegativeButtonListener, null);
            }
        }

        public void setCancelable(boolean flag) {
            this.mDialogCache.mCancelable = flag;
        }

        public void setOnKeyListener(DialogInterface.OnKeyListener onKeyListener) {
            this.mDialogCache.mOnKeyListener = onKeyListener;
        }

        public CustomAlertDialog create() {
            CustomAlertDialog dialog = new CustomAlertDialog(this.mDialogCache);
            apply(dialog);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(this.mDialogCache.mCancelable);
            dialog.setOnCancelListener(this.mDialogCache.mOnCancelListener);
            if (this.mDialogCache.mOnKeyListener != null) {
                dialog.setOnKeyListener(this.mDialogCache.mOnKeyListener);
            }
            return dialog;
        }

        public CustomAlertDialog show() {
            CustomAlertDialog dialog = create();
            dialog.show();
            return dialog;
        }
    }
}
