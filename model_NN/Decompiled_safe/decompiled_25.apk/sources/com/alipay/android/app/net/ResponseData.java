package com.alipay.android.app.net;

import org.json.JSONException;
import org.json.JSONObject;

public class ResponseData {
    private JSONObject mParams;
    private String mSign;

    public ResponseData(String data) {
        try {
            JSONObject jsonData = new JSONObject(data).optJSONObject("data");
            this.mSign = jsonData.optString("sign");
            this.mParams = jsonData.optJSONObject("params");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getSign() {
        return this.mSign;
    }

    public JSONObject getParams() {
        return this.mParams;
    }
}
