package com.alipay.android.app.util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

final class FileFetch implements Runnable {
    private FileDownloader downloader;
    private long fileEnd;
    private long fileStart;
    private String fileUrl;
    /* access modifiers changed from: private */
    public String savePath;
    private boolean stop = false;

    public FileFetch(String fileUrl2, String savePath2, FileDownloader downloader2) {
        this.fileUrl = fileUrl2;
        this.savePath = savePath2;
        this.downloader = downloader2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:62:0x00e8, code lost:
        if (r9 == 0) goto L_0x00ea;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:?, code lost:
        r15.stop = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x00ed, code lost:
        if (r6 != null) goto L_0x00ef;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:?, code lost:
        r15.stop = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x00fb, code lost:
        if (r6 != null) goto L_0x00fd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:?, code lost:
        r15.stop = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0109, code lost:
        if (r6 != null) goto L_0x010b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x0113, code lost:
        r11 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x0114, code lost:
        if (r6 != null) goto L_0x0116;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x0119, code lost:
        throw r11;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0092  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00d9 A[SYNTHETIC, Splitter:B:55:0x00d9] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00e7 A[ExcHandler: SocketTimeoutException (e java.net.SocketTimeoutException), PHI: r6 r9 
      PHI: (r6v4 'input' java.io.InputStream) = (r6v0 'input' java.io.InputStream), (r6v5 'input' java.io.InputStream), (r6v0 'input' java.io.InputStream), (r6v0 'input' java.io.InputStream), (r6v0 'input' java.io.InputStream) binds: [B:12:0x002c, B:36:0x009c, B:28:0x008a, B:29:?, B:25:0x0080] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r9v1 'responseCode' int) = (r9v0 'responseCode' int), (r9v2 'responseCode' int), (r9v3 'responseCode' int), (r9v3 'responseCode' int), (r9v4 'responseCode' int), (r9v4 'responseCode' int) binds: [B:12:0x002c, B:36:0x009c, B:28:0x008a, B:29:?, B:25:0x0080, B:26:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:12:0x002c] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0105 A[ExcHandler: Exception (e java.lang.Exception), PHI: r6 
      PHI: (r6v2 'input' java.io.InputStream) = (r6v0 'input' java.io.InputStream), (r6v5 'input' java.io.InputStream), (r6v0 'input' java.io.InputStream), (r6v0 'input' java.io.InputStream), (r6v0 'input' java.io.InputStream) binds: [B:12:0x002c, B:36:0x009c, B:28:0x008a, B:29:?, B:25:0x0080] A[DONT_GENERATE, DONT_INLINE], Splitter:B:12:0x002c] */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x0078 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0022 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r15 = this;
            com.alipay.android.app.util.FileDownloader r11 = r15.downloader
            boolean r11 = r11.showProgress()
            if (r11 == 0) goto L_0x001c
            long r11 = r15.fileEnd
            r13 = 0
            int r11 = (r11 > r13 ? 1 : (r11 == r13 ? 0 : -1))
            if (r11 <= 0) goto L_0x0018
            long r11 = r15.fileStart
            long r13 = r15.fileEnd
            int r11 = (r11 > r13 ? 1 : (r11 == r13 ? 0 : -1))
            if (r11 < 0) goto L_0x001c
        L_0x0018:
            r11 = 1
            r15.stop = r11
        L_0x001b:
            return
        L_0x001c:
            r1 = 0
            com.alipay.android.app.util.FileFetch$FileAccess r3 = new com.alipay.android.app.util.FileFetch$FileAccess
            r3.<init>()
        L_0x0022:
            boolean r11 = r15.stop
            if (r11 == 0) goto L_0x002a
        L_0x0026:
            r3.close()
            goto L_0x001b
        L_0x002a:
            r6 = 0
            r9 = 0
            org.apache.http.client.methods.HttpGet r5 = new org.apache.http.client.methods.HttpGet     // Catch:{ IOException -> 0x0089, SocketTimeoutException -> 0x00e7, Exception -> 0x0105 }
            java.lang.String r11 = r15.fileUrl     // Catch:{ IOException -> 0x0089, SocketTimeoutException -> 0x00e7, Exception -> 0x0105 }
            r5.<init>(r11)     // Catch:{ IOException -> 0x0089, SocketTimeoutException -> 0x00e7, Exception -> 0x0105 }
            org.apache.http.impl.client.DefaultHttpClient r4 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ IOException -> 0x0089, SocketTimeoutException -> 0x00e7, Exception -> 0x0105 }
            r4.<init>()     // Catch:{ IOException -> 0x0089, SocketTimeoutException -> 0x00e7, Exception -> 0x0105 }
            com.alipay.android.app.util.FileDownloader r11 = r15.downloader     // Catch:{ IOException -> 0x0089, SocketTimeoutException -> 0x00e7, Exception -> 0x0105 }
            boolean r11 = r11.showProgress()     // Catch:{ IOException -> 0x0089, SocketTimeoutException -> 0x00e7, Exception -> 0x0105 }
            if (r11 == 0) goto L_0x0062
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0089, SocketTimeoutException -> 0x00e7, Exception -> 0x0105 }
            java.lang.String r12 = "bytes="
            r11.<init>(r12)     // Catch:{ IOException -> 0x0089, SocketTimeoutException -> 0x00e7, Exception -> 0x0105 }
            long r12 = r15.fileStart     // Catch:{ IOException -> 0x0089, SocketTimeoutException -> 0x00e7, Exception -> 0x0105 }
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ IOException -> 0x0089, SocketTimeoutException -> 0x00e7, Exception -> 0x0105 }
            java.lang.String r12 = "-"
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ IOException -> 0x0089, SocketTimeoutException -> 0x00e7, Exception -> 0x0105 }
            long r12 = r15.fileEnd     // Catch:{ IOException -> 0x0089, SocketTimeoutException -> 0x00e7, Exception -> 0x0105 }
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ IOException -> 0x0089, SocketTimeoutException -> 0x00e7, Exception -> 0x0105 }
            java.lang.String r7 = r11.toString()     // Catch:{ IOException -> 0x0089, SocketTimeoutException -> 0x00e7, Exception -> 0x0105 }
            java.lang.String r11 = "Range"
            r5.addHeader(r11, r7)     // Catch:{ IOException -> 0x0089, SocketTimeoutException -> 0x00e7, Exception -> 0x0105 }
        L_0x0062:
            org.apache.http.HttpResponse r8 = r4.execute(r5)     // Catch:{ IOException -> 0x0089, SocketTimeoutException -> 0x00e7, Exception -> 0x0105 }
            org.apache.http.StatusLine r11 = r8.getStatusLine()     // Catch:{ IOException -> 0x0089, SocketTimeoutException -> 0x00e7, Exception -> 0x0105 }
            int r9 = r11.getStatusCode()     // Catch:{ IOException -> 0x0089, SocketTimeoutException -> 0x00e7, Exception -> 0x0105 }
            switch(r9) {
                case 200: goto L_0x0080;
                case 201: goto L_0x0080;
                case 202: goto L_0x0080;
                case 203: goto L_0x0080;
                case 204: goto L_0x0080;
                case 205: goto L_0x0080;
                case 206: goto L_0x0080;
                case 207: goto L_0x0080;
                default: goto L_0x0071;
            }     // Catch:{ IOException -> 0x0089, SocketTimeoutException -> 0x00e7, Exception -> 0x0105 }
        L_0x0071:
            r11 = 1
            r15.stop = r11     // Catch:{ IOException -> 0x0089, SocketTimeoutException -> 0x00e7, Exception -> 0x0105 }
        L_0x0074:
            boolean r11 = r15.stop     // Catch:{ IOException -> 0x0089, SocketTimeoutException -> 0x00e7, Exception -> 0x0105 }
            if (r11 == 0) goto L_0x0090
            if (r6 == 0) goto L_0x0026
            r6.close()     // Catch:{ Exception -> 0x007e }
            goto L_0x0026
        L_0x007e:
            r11 = move-exception
            goto L_0x0026
        L_0x0080:
            org.apache.http.HttpEntity r11 = r8.getEntity()     // Catch:{ IOException -> 0x0089, SocketTimeoutException -> 0x00e7, Exception -> 0x0105 }
            java.io.InputStream r6 = r11.getContent()     // Catch:{ IOException -> 0x0089, SocketTimeoutException -> 0x00e7, Exception -> 0x0105 }
            goto L_0x0074
        L_0x0089:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ SocketTimeoutException -> 0x00e7, IOException -> 0x00f7, Exception -> 0x0105 }
            r11 = 1
            r15.stop = r11     // Catch:{ SocketTimeoutException -> 0x00e7, IOException -> 0x00f7, Exception -> 0x0105 }
        L_0x0090:
            if (r6 != 0) goto L_0x009a
            if (r6 == 0) goto L_0x0022
            r6.close()     // Catch:{ Exception -> 0x0098 }
            goto L_0x0022
        L_0x0098:
            r11 = move-exception
            goto L_0x0022
        L_0x009a:
            r11 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r11]     // Catch:{ SocketTimeoutException -> 0x00e7, IOException -> 0x00f7, Exception -> 0x0105 }
        L_0x009e:
            r11 = 0
            int r12 = r0.length     // Catch:{ SocketTimeoutException -> 0x00e7, IOException -> 0x00f7, Exception -> 0x0105 }
            int r10 = r6.read(r0, r11, r12)     // Catch:{ SocketTimeoutException -> 0x00e7, IOException -> 0x00f7, Exception -> 0x0105 }
            r11 = -1
            if (r10 == r11) goto L_0x00b7
            long r11 = r15.fileStart     // Catch:{ SocketTimeoutException -> 0x00e7, IOException -> 0x00f7, Exception -> 0x0105 }
            r13 = 0
            int r13 = r3.write(r0, r13, r10)     // Catch:{ SocketTimeoutException -> 0x00e7, IOException -> 0x00f7, Exception -> 0x0105 }
            long r13 = (long) r13     // Catch:{ SocketTimeoutException -> 0x00e7, IOException -> 0x00f7, Exception -> 0x0105 }
            long r11 = r11 + r13
            r15.fileStart = r11     // Catch:{ SocketTimeoutException -> 0x00e7, IOException -> 0x00f7, Exception -> 0x0105 }
            com.alipay.android.app.util.FileDownloader r11 = r15.downloader     // Catch:{ SocketTimeoutException -> 0x00e7, IOException -> 0x00f7, Exception -> 0x0105 }
            r11.writeTempFile()     // Catch:{ SocketTimeoutException -> 0x00e7, IOException -> 0x00f7, Exception -> 0x0105 }
        L_0x00b7:
            com.alipay.android.app.util.FileDownloader r11 = r15.downloader     // Catch:{ SocketTimeoutException -> 0x00e7, IOException -> 0x00f7, Exception -> 0x0105 }
            boolean r11 = r11.showProgress()     // Catch:{ SocketTimeoutException -> 0x00e7, IOException -> 0x00f7, Exception -> 0x0105 }
            if (r11 == 0) goto L_0x00e3
            long r11 = r15.fileStart     // Catch:{ SocketTimeoutException -> 0x00e7, IOException -> 0x00f7, Exception -> 0x0105 }
            long r13 = r15.fileEnd     // Catch:{ SocketTimeoutException -> 0x00e7, IOException -> 0x00f7, Exception -> 0x0105 }
            int r11 = (r11 > r13 ? 1 : (r11 == r13 ? 0 : -1))
            if (r11 >= 0) goto L_0x00e1
            r1 = 1
        L_0x00c8:
            boolean r11 = r15.stop     // Catch:{ SocketTimeoutException -> 0x00e7, IOException -> 0x00f7, Exception -> 0x0105 }
            if (r11 != 0) goto L_0x00e5
            if (r1 == 0) goto L_0x00e5
            r1 = 1
        L_0x00cf:
            r11 = -1
            if (r10 <= r11) goto L_0x00d4
            if (r1 != 0) goto L_0x009e
        L_0x00d4:
            r11 = 1
            r15.stop = r11     // Catch:{ SocketTimeoutException -> 0x00e7, IOException -> 0x00f7, Exception -> 0x0105 }
            if (r6 == 0) goto L_0x0022
            r6.close()     // Catch:{ Exception -> 0x00de }
            goto L_0x0022
        L_0x00de:
            r11 = move-exception
            goto L_0x0022
        L_0x00e1:
            r1 = 0
            goto L_0x00c8
        L_0x00e3:
            r1 = 1
            goto L_0x00c8
        L_0x00e5:
            r1 = 0
            goto L_0x00cf
        L_0x00e7:
            r2 = move-exception
            if (r9 != 0) goto L_0x00ed
            r11 = 1
            r15.stop = r11     // Catch:{ all -> 0x0113 }
        L_0x00ed:
            if (r6 == 0) goto L_0x0022
            r6.close()     // Catch:{ Exception -> 0x00f4 }
            goto L_0x0022
        L_0x00f4:
            r11 = move-exception
            goto L_0x0022
        L_0x00f7:
            r2 = move-exception
            r11 = 1
            r15.stop = r11     // Catch:{ all -> 0x0113 }
            if (r6 == 0) goto L_0x0022
            r6.close()     // Catch:{ Exception -> 0x0102 }
            goto L_0x0022
        L_0x0102:
            r11 = move-exception
            goto L_0x0022
        L_0x0105:
            r2 = move-exception
            r11 = 1
            r15.stop = r11     // Catch:{ all -> 0x0113 }
            if (r6 == 0) goto L_0x0022
            r6.close()     // Catch:{ Exception -> 0x0110 }
            goto L_0x0022
        L_0x0110:
            r11 = move-exception
            goto L_0x0022
        L_0x0113:
            r11 = move-exception
            if (r6 == 0) goto L_0x0119
            r6.close()     // Catch:{ Exception -> 0x011a }
        L_0x0119:
            throw r11
        L_0x011a:
            r12 = move-exception
            goto L_0x0119
        */
        throw new UnsupportedOperationException("Method not decompiled: com.alipay.android.app.util.FileFetch.run():void");
    }

    public final long getFileStart() {
        return this.fileStart;
    }

    public final void setFileStart(long fileStart2) {
        this.fileStart = fileStart2;
    }

    public final long getFileEnd() {
        return this.fileEnd;
    }

    public final void setFileEnd(long fileEnd2) {
        this.fileEnd = fileEnd2;
    }

    public final boolean isStop() {
        return this.stop;
    }

    public final void stop() {
        this.stop = true;
    }

    final class FileAccess {
        private FileOutputStream outStream;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
          ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException} */
        public FileAccess() {
            try {
                this.outStream = new FileOutputStream(FileFetch.this.savePath, true);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        public synchronized int write(byte[] b, int start, int len) throws IOException {
            this.outStream.write(b, start, len);
            return len;
        }

        public void close() {
            try {
                this.outStream.close();
            } catch (Exception e) {
            }
        }
    }
}
