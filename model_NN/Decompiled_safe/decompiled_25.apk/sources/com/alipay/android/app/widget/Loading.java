package com.alipay.android.app.widget;

import android.app.Activity;
import android.app.ProgressDialog;
import com.alipay.android.app.lib.ResourceMap;

public class Loading {
    /* access modifiers changed from: private */
    public Activity mContext;
    /* access modifiers changed from: private */
    public ProgressDialog mProgress;

    public Loading(Activity context) {
        this.mContext = context;
    }

    public boolean isShowing() {
        return this.mProgress != null && this.mProgress.isShowing();
    }

    public void show() {
        show(ResourceMap.getString_processing());
    }

    public void show(int messageResId) {
        show(this.mContext.getText(messageResId));
    }

    private void show(final CharSequence message) {
        this.mContext.runOnUiThread(new Runnable() {
            public void run() {
                if (Loading.this.mProgress == null || !Loading.this.mProgress.isShowing()) {
                    Loading.this.dismiss();
                    Loading.this.mProgress = new ProgressDialog(Loading.this.mContext);
                    Loading.this.mProgress.setCancelable(false);
                    Loading.this.mProgress.setMessage(message);
                    Loading.this.mProgress.show();
                    return;
                }
                Loading.this.mProgress.setMessage(message);
            }
        });
    }

    public void dismiss() {
        this.mContext.runOnUiThread(new Runnable() {
            public void run() {
                try {
                    if (Loading.this.isShowing()) {
                        Loading.this.mProgress.dismiss();
                        Loading.this.mProgress = null;
                    }
                } catch (Exception e) {
                }
            }
        });
    }
}
