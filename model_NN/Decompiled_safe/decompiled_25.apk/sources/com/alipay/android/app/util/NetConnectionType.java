package com.alipay.android.app.util;

import org.jivesoftware.smack.packet.PrivacyItem;

public enum NetConnectionType {
    WIFI(0, "WIFI"),
    NETWORK_TYPE_1(1, "unicom2G"),
    NETWORK_TYPE_2(2, "mobile2G"),
    NETWORK_TYPE_4(4, "telecom2G"),
    NETWORK_TYPE_5(5, "telecom3G"),
    NETWORK_TYPE_6(6, "telecom3G"),
    NETWORK_TYPE_12(12, "telecom3G"),
    NETWORK_TYPE_8(8, "unicom3G"),
    NETWORK_TYPE_3(3, "unicom3G"),
    NETWORK_TYPE_13(13, "LTE"),
    NETWORK_TYPE_11(11, "IDEN"),
    NETWORK_TYPE_9(9, "HSUPA"),
    NETWORK_TYPE_10(10, "HSPA"),
    NETWORK_TYPE_15(15, "HSPAP"),
    NONE(-1, PrivacyItem.PrivacyRule.SUBSCRIPTION_NONE);
    
    private int code;
    private String name;

    private NetConnectionType(int code2, String name2) {
        this.code = code2;
        this.name = name2;
    }

    public final int getCode() {
        return this.code;
    }

    public final String getName() {
        return this.name;
    }

    public final void setName(String name2) {
        this.name = name2;
    }

    public static NetConnectionType getTypeByCode(int code2) {
        for (NetConnectionType type : values()) {
            if (type.getCode() == code2) {
                return type;
            }
        }
        return NONE;
    }
}
