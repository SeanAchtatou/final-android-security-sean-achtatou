package com.alipay.android.app.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {
    private static final String ALIPAY_PACKAGE_NAME = "com.eg.android.AlipayGphone";
    private static final String ALIPAY_VERSION = "7.0.0.0602";
    private static final String MSP_PACKAGE_NAME = "com.alipay.android.app";
    private static final String MSP_VERSION = "5.0.0";

    public static boolean isExistMsp(Context context) {
        try {
            if (context.getPackageManager().getPackageInfo(MSP_PACKAGE_NAME, 128) == null) {
                return false;
            }
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static boolean isExistClient(Context context) {
        try {
            PackageInfo apkInfo = context.getPackageManager().getPackageInfo(ALIPAY_PACKAGE_NAME, 128);
            if (apkInfo != null && apkInfo.versionName.compareTo(ALIPAY_VERSION) >= 0) {
                return true;
            }
            return false;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static boolean isLatestMspVersion(Context context) {
        try {
            if (context.getPackageManager().getPackageInfo(MSP_PACKAGE_NAME, 128).versionName.compareTo(MSP_VERSION) >= 0) {
                return true;
            }
            return false;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static NetConnectionType getNetConnectionType(Context context) {
        NetworkInfo info = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (info != null && info.getType() == 0) {
            return NetConnectionType.getTypeByCode(info.getSubtype());
        }
        if (info == null || info.getType() != 1) {
            return NetConnectionType.NONE;
        }
        return NetConnectionType.WIFI;
    }

    public static boolean is2G(Context context) {
        switch (getNetConnectionType(context).getCode()) {
            case 1:
            case 2:
            case 4:
                return true;
            case 3:
            default:
                return false;
        }
    }

    public static String getCharset(String contentType) {
        int start = contentType.indexOf("charset=");
        if (start == -1) {
            return null;
        }
        int start2 = start + "charset=".length();
        int end = contentType.indexOf(";", start2);
        if (end != -1) {
            return contentType.substring(start2, end);
        }
        return contentType.substring(start2);
    }

    public static String getContentType(String acontentType) {
        int end = acontentType.indexOf(";");
        if (end > 0) {
            return acontentType.substring(0, end);
        }
        return acontentType;
    }

    public static void chmod(String permission, String path) {
        try {
            Runtime.getRuntime().exec("chmod " + permission + " " + path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getClientKey(Context context) {
        String clientKey = StoreUtils.getValue(context, "client_key");
        if (!TextUtils.isEmpty(clientKey)) {
            return clientKey;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 10; i++) {
            sb.append((int) (10.0d * Math.random()));
        }
        String clientKey2 = sb.toString();
        StoreUtils.putValue(context, "client_key", clientKey2);
        return clientKey2;
    }

    @TargetApi(3)
    public static String getClientId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), "android_id");
    }

    public static String getAlixTid(Context context) {
        if (isEmpty4AlixTid(context)) {
            return PoiTypeDef.All;
        }
        return StoreUtils.getValue(context, "alix_tid");
    }

    public static boolean isEmpty4AlixTid(Context context) {
        if (TextUtils.isEmpty(StoreUtils.getValue(context, "alix_tid"))) {
            return true;
        }
        return false;
    }

    public static String getUserAgent(Context context) {
        String fv = getOsInfo(context);
        String kv = getKernelVersion(context);
        String locale = getDefaultLocale(context);
        return " (" + fv + ";" + kv + ";" + locale + ";" + ";" + getScreenResolution(context) + ")" + "(sdk android)";
    }

    private static String getOsInfo(Context context) {
        return "Android " + Build.VERSION.RELEASE;
    }

    public static String getKernelVersion(Context context) {
        String fkv = getFormattedKernelVersion();
        int i = fkv.indexOf("-");
        if (i != -1) {
            fkv = fkv.substring(0, i);
        }
        int i2 = fkv.indexOf("\n");
        if (i2 != -1) {
            fkv = fkv.substring(0, i2);
        }
        return "Linux " + fkv;
    }

    public static String getFormattedKernelVersion() {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader("/proc/version"), 256);
            String procVersionStr = reader.readLine();
            reader.close();
            Matcher m = Pattern.compile("\\w+\\s+\\w+\\s+([^\\s]+)\\s+\\(([^\\s@]+(?:@[^\\s.]+)?)[^)]*\\)\\s+\\((?:[^(]*\\([^)]*\\))?[^)]*\\)\\s+([^\\s]+)\\s+(?:PREEMPT\\s+)?(.+)").matcher(procVersionStr);
            if (!m.matches()) {
                return "Unavailable";
            }
            if (m.groupCount() < 4) {
                return "Unavailable";
            }
            return m.group(1) + "\n" + m.group(2) + " " + m.group(3) + "\n" + m.group(4);
        } catch (IOException e) {
            return "Unavailable";
        } catch (Throwable th) {
            reader.close();
            throw th;
        }
    }

    public static String getDefaultLocale(Context context) {
        return context.getResources().getConfiguration().locale.toString();
    }

    public static String getScreenResolution(Context context) {
        DisplayMetrics displayMetrics = getDisplayMetrics(context);
        return displayMetrics.widthPixels + "*" + displayMetrics.heightPixels;
    }

    public static DisplayMetrics getDisplayMetrics(Context context) {
        DisplayMetrics metrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(metrics);
        return metrics;
    }

    public static boolean retrieveApkFromAssets(Context context, String fileName, String path) {
        try {
            InputStream is = context.getAssets().open(fileName);
            File file = new File(path);
            file.createNewFile();
            FileOutputStream fos = new FileOutputStream(file);
            byte[] temp = new byte[1024];
            while (true) {
                int i = is.read(temp);
                if (i <= 0) {
                    fos.close();
                    is.close();
                    return true;
                }
                fos.write(temp, 0, i);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static PackageInfo getApkInfo(Context context, String archiveFilePath) {
        return context.getPackageManager().getPackageArchiveInfo(archiveFilePath, 128);
    }

    public static void installApk(Activity context, String cachePath) {
        chmod("777", cachePath);
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addFlags(268435456);
        intent.setDataAndType(Uri.parse("file://" + cachePath), "application/vnd.android.package-archive");
        context.startActivity(intent);
    }

    public static int compareVersion(String srcVersion, String destVersion) {
        List<String> sv = new ArrayList<>();
        List<String> dv = new ArrayList<>();
        sv.addAll(Arrays.asList(srcVersion.split("\\.")));
        dv.addAll(Arrays.asList(destVersion.split("\\.")));
        int maxLen = Math.max(sv.size(), dv.size());
        while (sv.size() < maxLen) {
            sv.add("0");
        }
        while (dv.size() < maxLen) {
            dv.add("0");
        }
        for (int i = 0; i < maxLen; i++) {
            if (Integer.parseInt((String) sv.get(i)) != Integer.parseInt((String) dv.get(i))) {
                return Integer.parseInt((String) sv.get(i)) - Integer.parseInt((String) dv.get(i));
            }
        }
        return 0;
    }
}
