package com.alipay.android.app.sdk;

import com.amap.mapapi.poisearch.PoiTypeDef;

public enum ResultStatus {
    SUCCEEDED(9000, "支付成功"),
    FAILED(4000, "支付失败"),
    CANCELED(6001, "用户取消"),
    NETWORK_ERROR(6002, "网络连接异常"),
    UNKNOWN(6003, PoiTypeDef.All),
    UNWORK(7001, "网页支付失败"),
    PARAMS_ERROR(4001, "参数错误");
    
    private String msg;
    private int status;

    private ResultStatus(int status2, String msg2) {
        this.status = status2;
        this.msg = msg2;
    }

    public void setStatus(int status2) {
        this.status = status2;
    }

    public int getStatus() {
        return this.status;
    }

    public void setMsg(String msg2) {
        this.msg = msg2;
    }

    public String getMsg() {
        return this.msg;
    }

    public static ResultStatus getResultState(int status2) {
        switch (status2) {
            case 4001:
                return PARAMS_ERROR;
            case 6001:
                return CANCELED;
            case 6002:
                return NETWORK_ERROR;
            case 6003:
                return UNKNOWN;
            case 7001:
                return UNWORK;
            case 9000:
                return SUCCEEDED;
            default:
                return FAILED;
        }
    }
}
