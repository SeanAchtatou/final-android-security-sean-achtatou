package com.alipay.android.app.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import javax.net.ssl.SSLPeerUnverifiedException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

public class HttpClient {
    private Context mContext;
    private String mUrl;
    private AndroidHttpClient sAndroidHttpClient;

    public HttpClient(Context context) {
        this(context, null);
    }

    public HttpClient(Context context, String url) {
        this.sAndroidHttpClient = null;
        this.mContext = context;
        this.mUrl = url;
    }

    public void setUrl(String url) {
        this.mUrl = url;
    }

    public String getUrl() {
        return this.mUrl;
    }

    public URL getURL() {
        try {
            return new URL(this.mUrl);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private String getStatus(String s) {
        int startIndex = s.indexOf(" ") + 1;
        int endIndex = s.indexOf(" ", startIndex);
        if (startIndex == -1 || endIndex == -1) {
            return null;
        }
        return s.substring(startIndex, endIndex);
    }

    public String sendSynchronousRequest(String requestData, ArrayList<BasicHeader> arrayList) {
        try {
            return EntityUtils.toString(sendSynchronousRequestAsHttpResponse(requestData).getEntity());
        } catch (Exception e) {
            return null;
        }
    }

    public HttpResponse sendSynchronousRequestAsHttpResponse(String strReqData, ArrayList<BasicHeader> headers) {
        ArrayList<BasicNameValuePair> pairs = new ArrayList<>();
        pairs.add(new BasicNameValuePair("requestData", strReqData));
        try {
            return sendSynchronousRequestAsHttpResponse(pairs, headers);
        } catch (SSLPeerUnverifiedException e) {
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        return new com.alipay.android.app.net.WebResponseData("sslerror", com.amap.mapapi.poisearch.PoiTypeDef.All, com.umeng.common.util.e.f, null);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0031 A[ExcHandler: SSLPeerUnverifiedException (e javax.net.ssl.SSLPeerUnverifiedException), Splitter:B:1:0x0001] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.alipay.android.app.net.WebResponseData sendSynchronousRequest(java.util.ArrayList<org.apache.http.message.BasicNameValuePair> r14, java.util.ArrayList<org.apache.http.message.BasicHeader> r15) {
        /*
            r13 = this;
            r5 = 0
            org.apache.http.HttpResponse r4 = r13.sendSynchronousRequestAsHttpResponse(r14, r15)     // Catch:{ SSLPeerUnverifiedException -> 0x0031, Exception -> 0x003f }
            org.apache.http.HttpEntity r3 = r4.getEntity()     // Catch:{ SSLPeerUnverifiedException -> 0x0031, Exception -> 0x003f }
            java.lang.String r8 = org.apache.http.util.EntityUtils.toString(r3)     // Catch:{ SSLPeerUnverifiedException -> 0x0031, Exception -> 0x003f }
            r7 = 0
            org.apache.http.StatusLine r9 = r4.getStatusLine()     // Catch:{ Exception -> 0x0041, SSLPeerUnverifiedException -> 0x0031 }
            java.lang.String r7 = r9.toString()     // Catch:{ Exception -> 0x0041, SSLPeerUnverifiedException -> 0x0031 }
            java.lang.String r7 = r13.getStatus(r7)     // Catch:{ Exception -> 0x0041, SSLPeerUnverifiedException -> 0x0031 }
        L_0x001a:
            org.apache.http.Header r9 = r3.getContentType()     // Catch:{ SSLPeerUnverifiedException -> 0x0031, Exception -> 0x003f }
            java.lang.String r1 = r9.getValue()     // Catch:{ SSLPeerUnverifiedException -> 0x0031, Exception -> 0x003f }
            java.lang.String r0 = com.alipay.android.app.util.Utils.getCharset(r1)     // Catch:{ SSLPeerUnverifiedException -> 0x0031, Exception -> 0x003f }
            java.lang.String r1 = com.alipay.android.app.util.Utils.getContentType(r1)     // Catch:{ SSLPeerUnverifiedException -> 0x0031, Exception -> 0x003f }
            com.alipay.android.app.net.WebResponseData r6 = new com.alipay.android.app.net.WebResponseData     // Catch:{ SSLPeerUnverifiedException -> 0x0031, Exception -> 0x003f }
            r6.<init>(r8, r1, r0, r7)     // Catch:{ SSLPeerUnverifiedException -> 0x0031, Exception -> 0x003f }
            r5 = r6
        L_0x0030:
            return r5
        L_0x0031:
            r2 = move-exception
            com.alipay.android.app.net.WebResponseData r5 = new com.alipay.android.app.net.WebResponseData
            java.lang.String r9 = "sslerror"
            java.lang.String r10 = ""
            java.lang.String r11 = "UTF-8"
            r12 = 0
            r5.<init>(r9, r10, r11, r12)
            goto L_0x0030
        L_0x003f:
            r9 = move-exception
            goto L_0x0030
        L_0x0041:
            r9 = move-exception
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.alipay.android.app.net.HttpClient.sendSynchronousRequest(java.util.ArrayList, java.util.ArrayList):com.alipay.android.app.net.WebResponseData");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.alipay.android.app.net.AndroidHttpClient.execute(org.apache.http.HttpHost, org.apache.http.HttpRequest):org.apache.http.HttpResponse
     arg types: [org.apache.http.HttpHost, org.apache.http.client.methods.HttpPost]
     candidates:
      com.alipay.android.app.net.AndroidHttpClient.execute(org.apache.http.client.methods.HttpUriRequest, org.apache.http.client.ResponseHandler):T
      com.alipay.android.app.net.AndroidHttpClient.execute(org.apache.http.client.methods.HttpUriRequest, org.apache.http.protocol.HttpContext):org.apache.http.HttpResponse
      com.alipay.android.app.net.AndroidHttpClient.execute(org.apache.http.HttpHost, org.apache.http.HttpRequest):org.apache.http.HttpResponse */
    public HttpResponse sendSynchronousRequestAsHttpResponse(String request) {
        URL url = getURL();
        HttpRequest httpRequest = null;
        HttpHost target = null;
        try {
            if (this.sAndroidHttpClient == null) {
                this.sAndroidHttpClient = AndroidHttpClient.newInstance("alipay_sdk");
            }
            this.sAndroidHttpClient.getParams().setParameter("http.route.default-proxy", getProxy());
            String protocol = url.getProtocol();
            int port = url.getPort();
            if (port == -1) {
                port = url.getDefaultPort();
            }
            HttpHost target2 = new HttpHost(url.getHost(), port, protocol);
            try {
                HttpPost httpPost = new HttpPost(this.mUrl);
                try {
                    httpPost.setEntity(new ByteArrayEntity(request.getBytes()));
                    httpPost.addHeader("Content-type", "application/octet-stream");
                    return this.sAndroidHttpClient.execute(target2, (HttpRequest) httpPost);
                } catch (NullPointerException e) {
                    e = e;
                    target = target2;
                    httpRequest = httpPost;
                    e.printStackTrace();
                    try {
                        return this.sAndroidHttpClient.execute(target, httpRequest);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                        return null;
                    }
                } catch (Exception e2) {
                    return null;
                }
            } catch (NullPointerException e3) {
                e = e3;
                target = target2;
                e.printStackTrace();
                return this.sAndroidHttpClient.execute(target, httpRequest);
            } catch (Exception e4) {
                return null;
            }
        } catch (NullPointerException e5) {
            e = e5;
            e.printStackTrace();
            return this.sAndroidHttpClient.execute(target, httpRequest);
        } catch (Exception e6) {
            return null;
        }
    }

    public HttpResponse sendSynchronousRequestAsHttpResponse(ArrayList<BasicNameValuePair> pairs, ArrayList<BasicHeader> headers) throws SSLPeerUnverifiedException {
        URL url = getURL();
        HttpRequest httpRequest = null;
        HttpHost target = null;
        try {
            if (this.sAndroidHttpClient == null) {
                this.sAndroidHttpClient = AndroidHttpClient.newInstance("alipay_sdk");
            }
            this.sAndroidHttpClient.getParams().setParameter("http.route.default-proxy", getProxy());
            String protocol = url.getProtocol();
            int port = url.getPort();
            if (port == -1) {
                port = url.getDefaultPort();
            }
            HttpHost target2 = new HttpHost(url.getHost(), port, protocol);
            if (pairs != null) {
                try {
                    HttpPost httpPost = new HttpPost(this.mUrl);
                    try {
                    } catch (SSLPeerUnverifiedException e) {
                        throw new SSLPeerUnverifiedException("SSL error");
                    } catch (NullPointerException e2) {
                        e = e2;
                        target = target2;
                        httpRequest = httpPost;
                        e.printStackTrace();
                        try {
                            return this.sAndroidHttpClient.execute(target, httpRequest);
                        } catch (Exception e1) {
                            e1.printStackTrace();
                            return null;
                        }
                    } catch (Exception e3) {
                        return null;
                    }
                    try {
                        httpPost.setEntity(new UrlEncodedFormEntity(pairs, "utf-8"));
                        httpRequest = httpPost;
                    } catch (SSLPeerUnverifiedException e4) {
                        throw new SSLPeerUnverifiedException("SSL error");
                    } catch (NullPointerException e5) {
                        e = e5;
                        target = target2;
                        httpRequest = httpPost;
                        e.printStackTrace();
                        return this.sAndroidHttpClient.execute(target, httpRequest);
                    } catch (Exception e6) {
                        return null;
                    }
                } catch (SSLPeerUnverifiedException e7) {
                    throw new SSLPeerUnverifiedException("SSL error");
                } catch (NullPointerException e8) {
                    e = e8;
                    target = target2;
                    e.printStackTrace();
                    return this.sAndroidHttpClient.execute(target, httpRequest);
                } catch (Exception e9) {
                    return null;
                }
            } else {
                httpRequest = new HttpGet(this.mUrl);
            }
            httpRequest.addHeader("Content-type", "application/x-www-form-urlencoded;charset=utf-8");
            httpRequest.addHeader("Accept", "application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5");
            if (headers != null) {
                Iterator<BasicHeader> it = headers.iterator();
                while (it.hasNext()) {
                    httpRequest.addHeader(it.next());
                }
            }
            HttpHost httpHost = target2;
            return this.sAndroidHttpClient.execute(target2, httpRequest);
        } catch (SSLPeerUnverifiedException e10) {
            throw new SSLPeerUnverifiedException("SSL error");
        } catch (NullPointerException e11) {
            e = e11;
            e.printStackTrace();
            return this.sAndroidHttpClient.execute(target, httpRequest);
        } catch (Exception e12) {
            return null;
        }
    }

    private HttpHost getProxy() {
        NetworkInfo ni = getActiveNetworkInfo();
        if (ni == null || !ni.isAvailable() || ni.getType() != 0) {
            return null;
        }
        String proxyHost = Proxy.getDefaultHost();
        int port = Proxy.getDefaultPort();
        if (proxyHost != null) {
            return new HttpHost(proxyHost, port);
        }
        return null;
    }

    private NetworkInfo getActiveNetworkInfo() {
        try {
            return ((ConnectivityManager) this.mContext.getSystemService("connectivity")).getActiveNetworkInfo();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
