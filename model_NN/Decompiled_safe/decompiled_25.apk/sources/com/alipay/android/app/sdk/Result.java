package com.alipay.android.app.sdk;

public class Result {
    private static String sWapResult;

    public static void setWapResult(String result) {
        sWapResult = result;
    }

    public static String getWapResult() {
        return sWapResult;
    }

    public static String parseResult(int resultStatus, String memo, String result) {
        return "resultStatus={" + resultStatus + "};memo={" + memo + "};result={" + result + "}";
    }
}
