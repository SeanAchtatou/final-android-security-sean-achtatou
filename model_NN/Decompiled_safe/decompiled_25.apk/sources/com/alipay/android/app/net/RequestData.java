package com.alipay.android.app.net;

import com.amap.mapapi.location.LocationManagerProxy;
import com.amap.mapapi.poisearch.PoiTypeDef;
import org.json.JSONObject;

public class RequestData {
    private String mAlixTid;
    private String mClientId;
    private String mClientKey;
    private String mInstalledClient;
    private String mNetwork;
    private String mOrderInfo;

    public RequestData(String mClientKey2, String mClientId2, String mAlixTid2, String mInstalledClient2, String network, String mOrderInfo2) {
        this.mClientKey = mClientKey2;
        this.mClientId = mClientId2;
        this.mAlixTid = mAlixTid2;
        this.mInstalledClient = mInstalledClient2;
        this.mNetwork = network;
        this.mOrderInfo = mOrderInfo2;
    }

    public String toString() {
        try {
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("deviceVersion", "Simulator");
            jsonParams.put("clientKey", this.mClientKey);
            jsonParams.put("clientId", this.mClientId);
            jsonParams.put("alixtid", this.mAlixTid);
            jsonParams.put("installedClient", this.mInstalledClient);
            jsonParams.put(LocationManagerProxy.NETWORK_PROVIDER, this.mNetwork);
            jsonParams.put("orderInfo", this.mOrderInfo);
            jsonParams.put("platform", "ANDROID");
            JSONObject jsonData = new JSONObject();
            jsonData.put("namespace", "com.alipay.mcashier");
            jsonData.put("api_version", "1.0.0");
            jsonData.put("api_name", "sdk_pay");
            jsonData.put("params", jsonParams);
            JSONObject requestData = new JSONObject();
            requestData.put("data", jsonData);
            return requestData.toString();
        } catch (Exception e) {
            return PoiTypeDef.All;
        }
    }

    public String getClientKey() {
        return this.mClientKey;
    }

    public void setClientKey(String mClientKey2) {
        this.mClientKey = mClientKey2;
    }

    public String getClientId() {
        return this.mClientId;
    }

    public void setClientId(String mClientId2) {
        this.mClientId = mClientId2;
    }

    public String getAlixTid() {
        return this.mAlixTid;
    }

    public void setAlixTid(String mAlixTid2) {
        this.mAlixTid = mAlixTid2;
    }

    public String getInstalledClient() {
        return this.mInstalledClient;
    }

    public void setInstalledClient(String mInstalledClient2) {
        this.mInstalledClient = mInstalledClient2;
    }

    public String getOrderInfo() {
        return this.mOrderInfo;
    }

    public void setOrderInfo(String mOrderInfo2) {
        this.mOrderInfo = mOrderInfo2;
    }

    public String getNetwork() {
        return this.mNetwork;
    }

    public void setNetwork(String network) {
        this.mNetwork = network;
    }
}
