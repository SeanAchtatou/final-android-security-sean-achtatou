package com.alipay.android.app.sdk;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import com.alipay.android.app.lib.ResourceMap;
import com.alipay.android.app.net.HttpClient;
import com.alipay.android.app.net.RequestData;
import com.alipay.android.app.net.ResponseData;
import com.alipay.android.app.util.FileDownloader;
import com.alipay.android.app.util.LogUtils;
import com.alipay.android.app.util.PayHelper;
import com.alipay.android.app.util.StoreUtils;
import com.alipay.android.app.util.Utils;
import com.alipay.android.app.widget.CustomAlertDialog;
import com.alipay.android.app.widget.Loading;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.umeng.common.a;
import java.util.ArrayList;
import org.apache.http.message.BasicHeader;
import org.json.JSONObject;

public class AliPay {
    private static final String ALIPAY_APK_NAME = "alipay.apk";
    private static final String MSP_APK_NAME = "alipay_msp.apk";
    private static String URL = "https://mclient.alipay.com/gateway.do";
    protected static final Object sLock = new Object();
    /* access modifiers changed from: private */
    public boolean isInstall;
    /* access modifiers changed from: private */
    public Activity mContext;
    /* access modifiers changed from: private */
    public Runnable mDownloadFailRunnable = new Runnable() {
        public void run() {
            CustomAlertDialog.Builder alertDialog = new CustomAlertDialog.Builder(AliPay.this.mContext);
            alertDialog.setTitle(ResourceMap.getString_confirm_title());
            alertDialog.setMessage(ResourceMap.getString_download_fail());
            alertDialog.setNegativeButton(ResourceMap.getString_cancel(), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    synchronized (AliPay.sLock) {
                        ResultStatus status = ResultStatus.getResultState(6001);
                        Result.setWapResult(Result.parseResult(status.getStatus(), status.getMsg(), PoiTypeDef.All));
                        try {
                            AliPay.sLock.notify();
                        } catch (Exception e) {
                            LogUtils.printExceptionStackTrace(e);
                        }
                    }
                }
            });
            alertDialog.setPositiveButton(ResourceMap.getString_redo(), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    AliPay.this.downloadFile();
                }
            });
            alertDialog.show();
        }
    };
    /* access modifiers changed from: private */
    public String mDownloadType;
    private String mDownloadUrl;
    /* access modifiers changed from: private */
    public Handler mHandler;
    /* access modifiers changed from: private */
    public CustomAlertDialog mInstallMessageAlert;
    /* access modifiers changed from: private */
    public BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("android.intent.action.PACKAGE_ADDED")) {
                if (AliPay.this.mInstallMessageAlert != null) {
                    AliPay.this.mHandler.post(new Runnable() {
                        public void run() {
                            AliPay.this.mInstallMessageAlert.dismiss();
                        }
                    });
                }
                AliPay.this.isInstall = true;
                AliPay.this.mContext.unregisterReceiver(AliPay.this.mReceiver);
                synchronized (AliPay.sLock) {
                    AliPay.sLock.notify();
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public int mTimeout;
    /* access modifiers changed from: private */
    public String mUrl;

    public AliPay(Activity context, Handler handler) {
        this.mContext = context;
        this.mHandler = handler;
    }

    public String pay(String orderInfo) {
        Loading loading = new Loading(this.mContext);
        loading.show();
        String payResult = PoiTypeDef.All;
        String clientKey = Utils.getClientKey(this.mContext);
        String clientId = Utils.getClientId(this.mContext);
        String alixTid = Utils.getAlixTid(this.mContext);
        String network = Utils.getNetConnectionType(this.mContext).getName();
        StringBuilder sb = new StringBuilder(PoiTypeDef.All);
        if (Utils.isExistMsp(this.mContext)) {
            sb.append("safepay|");
        }
        if (Utils.isExistClient(this.mContext)) {
            sb.append("alipay");
        } else if (sb.indexOf("|") != -1) {
            sb.deleteCharAt(sb.indexOf("|"));
        }
        RequestData requestData = new RequestData(clientKey, clientId, alixTid, sb.toString(), network, orderInfo);
        HttpClient httpClient = new HttpClient(this.mContext);
        httpClient.setUrl(URL);
        LogUtils.i("sdk request:" + requestData.toString());
        String response = httpClient.sendSynchronousRequest(requestData.toString(), (ArrayList<BasicHeader>) null);
        loading.dismiss();
        LogUtils.i("sdk response:" + response);
        if (TextUtils.isEmpty(response)) {
            ResultStatus status = ResultStatus.getResultState(6002);
            return Result.parseResult(status.getStatus(), status.getMsg(), PoiTypeDef.All);
        }
        JSONObject jsonParams = new ResponseData(response).getParams();
        String state = jsonParams.optString("state");
        if (TextUtils.equals(state, "7001")) {
            return Result.parseResult(Integer.parseInt(state), jsonParams.optString("errorMessage"), PoiTypeDef.All);
        }
        if (TextUtils.equals(state, "9000")) {
            PayHelper payHelper = new PayHelper(this.mContext);
            String alixTid2 = jsonParams.optString("alixtid");
            if (!TextUtils.equals(alixTid2, Utils.getAlixTid(this.mContext))) {
                StoreUtils.putValue(this.mContext, "alix_tid", alixTid2);
            }
            String payConfig = jsonParams.optString("config");
            if (TextUtils.equals(payConfig, "safepay")) {
                return payHelper.pay4Msp(orderInfo);
            }
            if (TextUtils.equals(payConfig, "alipay")) {
                return payHelper.pay4Client(orderInfo);
            }
            if (TextUtils.equals(payConfig, "wap")) {
                int timeout = jsonParams.optInt("timeout", 15);
                String url = jsonParams.optString("url");
                Intent intent = new Intent(this.mContext, WapPayActivity.class);
                Bundle extras = new Bundle();
                extras.putString("url", url);
                extras.putInt("timeout", timeout);
                intent.putExtras(extras);
                this.mContext.startActivity(intent);
                synchronized (sLock) {
                    try {
                        sLock.wait();
                    } catch (InterruptedException e) {
                        LogUtils.printExceptionStackTrace(e);
                    }
                }
                String payResult2 = Result.getWapResult();
                if (TextUtils.isEmpty(payResult2)) {
                    ResultStatus status2 = ResultStatus.getResultState(6001);
                    payResult2 = Result.parseResult(status2.getStatus(), status2.getMsg(), PoiTypeDef.All);
                }
                return payResult2;
            } else if (TextUtils.equals(payConfig, "wap_sdk")) {
                this.mTimeout = jsonParams.optInt("timeout", 15);
                this.mDownloadUrl = jsonParams.optString("downloadUrl");
                String downloadMessage = jsonParams.optString("downloadMessage");
                this.mUrl = jsonParams.optString("url");
                this.mDownloadType = jsonParams.optString("downloadType");
                processInstall(true, downloadMessage, jsonParams);
                synchronized (sLock) {
                    try {
                        sLock.wait();
                    } catch (InterruptedException e2) {
                        LogUtils.printExceptionStackTrace(e2);
                    }
                }
                if (this.isInstall) {
                    if (TextUtils.equals(this.mDownloadType, "safepay")) {
                        return payHelper.pay4Msp(orderInfo);
                    }
                    if (TextUtils.equals(this.mDownloadType, "alipay")) {
                        return payHelper.pay4Client(orderInfo);
                    }
                }
            } else if (TextUtils.equals(payConfig, "download")) {
                this.mDownloadUrl = jsonParams.optString("downloadUrl");
                String downloadMessage2 = jsonParams.optString("downloadMessage");
                this.mDownloadType = jsonParams.optString("downloadType");
                processInstall(false, downloadMessage2, jsonParams);
                synchronized (sLock) {
                    try {
                        sLock.wait();
                    } catch (InterruptedException e3) {
                        LogUtils.printExceptionStackTrace(e3);
                    }
                }
                if (this.isInstall) {
                    if (TextUtils.equals(this.mDownloadType, "safepay")) {
                        return payHelper.pay4Msp(orderInfo);
                    }
                    if (TextUtils.equals(this.mDownloadType, "alipay")) {
                        return payHelper.pay4Client(orderInfo);
                    }
                }
            } else if (TextUtils.equals(payConfig, "exit")) {
                ResultStatus status3 = ResultStatus.getResultState(4000);
                return Result.parseResult(status3.getStatus(), status3.getMsg(), PoiTypeDef.All);
            }
        } else if (TextUtils.equals(state, "4001")) {
            ResultStatus status4 = ResultStatus.getResultState(Integer.parseInt(state));
            return Result.parseResult(status4.getStatus(), status4.getMsg(), PoiTypeDef.All);
        } else if (TextUtils.equals(state, "5001")) {
            return pay(orderInfo);
        }
        if (Result.getWapResult() != null) {
            payResult = Result.getWapResult();
        }
        LogUtils.i("sdk result:" + payResult);
        return payResult;
    }

    private void processInstall(boolean isWap, String downloadMessage, JSONObject jsonParams) {
        String cachePath = String.valueOf(this.mContext.getCacheDir().getAbsolutePath()) + "/temp.apk";
        if (TextUtils.equals(this.mDownloadType, "safepay")) {
            if (!Utils.retrieveApkFromAssets(this.mContext, MSP_APK_NAME, cachePath)) {
                doDownLoad(isWap, downloadMessage);
            } else if (Utils.is2G(this.mContext)) {
                doInstall(isWap, cachePath, downloadMessage);
            } else if (Utils.compareVersion(Utils.getApkInfo(this.mContext, cachePath).versionName, jsonParams.optString("downloadVersion", "3.5.4")) < 0) {
                doDownLoad(isWap, downloadMessage);
            } else {
                doInstall(isWap, cachePath, downloadMessage);
            }
        } else if (!TextUtils.equals(this.mDownloadType, "alipay")) {
        } else {
            if (!Utils.retrieveApkFromAssets(this.mContext, ALIPAY_APK_NAME, cachePath)) {
                doDownLoad(isWap, downloadMessage);
            } else if (Utils.is2G(this.mContext)) {
                doInstall(isWap, cachePath, downloadMessage);
            } else if (Utils.compareVersion(Utils.getApkInfo(this.mContext, cachePath).versionName, jsonParams.optString("downloadVersion", "7.1.0.0701")) < 0) {
                doDownLoad(isWap, downloadMessage);
            } else {
                doInstall(isWap, cachePath, downloadMessage);
            }
        }
    }

    private void doInstall(final boolean isWap, final String cachePath, final String downloadMessage) {
        this.mHandler.post(new Runnable() {
            public void run() {
                CustomAlertDialog.Builder alertDialog = new CustomAlertDialog.Builder(AliPay.this.mContext);
                alertDialog.setTitle(ResourceMap.getString_confirm_title());
                alertDialog.setMessage(downloadMessage);
                int string_cancel = ResourceMap.getString_cancel();
                final boolean z = isWap;
                alertDialog.setNegativeButton(string_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (z) {
                            Intent intent = new Intent(AliPay.this.mContext, WapPayActivity.class);
                            Bundle extras = new Bundle();
                            extras.putString("url", AliPay.this.mUrl);
                            extras.putInt("timeout", AliPay.this.mTimeout);
                            intent.putExtras(extras);
                            AliPay.this.mContext.startActivity(intent);
                            return;
                        }
                        synchronized (AliPay.sLock) {
                            ResultStatus status = ResultStatus.getResultState(6001);
                            Result.setWapResult(Result.parseResult(status.getStatus(), status.getMsg(), PoiTypeDef.All));
                            try {
                                AliPay.sLock.notify();
                            } catch (Exception e) {
                                LogUtils.printExceptionStackTrace(e);
                            }
                        }
                        return;
                    }
                });
                int string_ensure = ResourceMap.getString_ensure();
                final String str = cachePath;
                alertDialog.setPositiveButton(string_ensure, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Utils.installApk(AliPay.this.mContext, str);
                        IntentFilter filter = new IntentFilter();
                        filter.addAction("android.intent.action.PACKAGE_ADDED");
                        filter.addDataScheme(a.c);
                        AliPay.this.mContext.registerReceiver(AliPay.this.mReceiver, filter);
                        AliPay.this.showInstallMessage();
                    }
                });
                alertDialog.show();
            }
        });
    }

    private void doDownLoad(final boolean isWap, final String downloadMessage) {
        this.mHandler.post(new Runnable() {
            public void run() {
                CustomAlertDialog.Builder alertDialog = new CustomAlertDialog.Builder(AliPay.this.mContext);
                alertDialog.setTitle(ResourceMap.getString_confirm_title());
                alertDialog.setMessage(downloadMessage);
                int string_cancel = ResourceMap.getString_cancel();
                final boolean z = isWap;
                alertDialog.setNegativeButton(string_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (z) {
                            Intent intent = new Intent(AliPay.this.mContext, WapPayActivity.class);
                            Bundle extras = new Bundle();
                            extras.putString("url", AliPay.this.mUrl);
                            extras.putInt("timeout", AliPay.this.mTimeout);
                            intent.putExtras(extras);
                            AliPay.this.mContext.startActivity(intent);
                            return;
                        }
                        synchronized (AliPay.sLock) {
                            ResultStatus status = ResultStatus.getResultState(6001);
                            Result.setWapResult(Result.parseResult(status.getStatus(), status.getMsg(), PoiTypeDef.All));
                            try {
                                AliPay.sLock.notify();
                            } catch (Exception e) {
                                LogUtils.printExceptionStackTrace(e);
                            }
                        }
                        return;
                    }
                });
                alertDialog.setPositiveButton(ResourceMap.getString_ensure(), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        AliPay.this.downloadFile();
                    }
                });
                alertDialog.show();
            }
        });
    }

    /* access modifiers changed from: private */
    public void downloadFile() {
        final Loading loading = new Loading(this.mContext);
        loading.show();
        final String cachePath = String.valueOf(this.mContext.getCacheDir().getAbsolutePath()) + "/temp.apk";
        FileDownloader fd = new FileDownloader();
        fd.setFileUrl(this.mDownloadUrl);
        fd.setSavePath(cachePath);
        fd.setProgressOutput(new FileDownloader.IDownloadProgress() {
            public void downloadSucess() {
                loading.dismiss();
                AliPay.this.mHandler.removeCallbacks(AliPay.this.mDownloadFailRunnable);
                Utils.installApk(AliPay.this.mContext, cachePath);
                IntentFilter filter = new IntentFilter();
                filter.addAction("android.intent.action.PACKAGE_ADDED");
                filter.addDataScheme(a.c);
                AliPay.this.mContext.registerReceiver(AliPay.this.mReceiver, filter);
                AliPay.this.showInstallMessage();
            }

            public void downloadProgress(float progress) {
            }

            public void downloadFail() {
                AliPay.this.mHandler.removeCallbacks(AliPay.this.mDownloadFailRunnable);
                loading.dismiss();
                AliPay.this.mHandler.post(AliPay.this.mDownloadFailRunnable);
            }
        });
        fd.start();
        this.mHandler.postDelayed(this.mDownloadFailRunnable, 15000);
    }

    /* access modifiers changed from: private */
    public void showInstallMessage() {
        this.mHandler.post(new Runnable() {
            public void run() {
                CustomAlertDialog.Builder dialog = new CustomAlertDialog.Builder(AliPay.this.mContext);
                dialog.setTitle(ResourceMap.getString_confirm_title());
                if (TextUtils.equals(AliPay.this.mDownloadType, "safepay")) {
                    dialog.setMessage(ResourceMap.getString_cancelInstallTips());
                } else if (TextUtils.equals(AliPay.this.mDownloadType, "alipay")) {
                    dialog.setMessage(ResourceMap.getString_cancelInstallAlipayTips());
                }
                dialog.setPositiveButton(ResourceMap.getString_ensure(), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        AliPay.this.mContext.unregisterReceiver(AliPay.this.mReceiver);
                        AliPay.this.isInstall = false;
                        ResultStatus status = ResultStatus.getResultState(6001);
                        Result.setWapResult(Result.parseResult(status.getStatus(), status.getMsg(), PoiTypeDef.All));
                        synchronized (AliPay.sLock) {
                            try {
                                AliPay.sLock.notify();
                            } catch (Exception e) {
                                LogUtils.printExceptionStackTrace(e);
                            }
                        }
                    }
                });
                AliPay.this.mInstallMessageAlert = dialog.show();
            }
        });
    }
}
