package com.alipay.android.app.sdk;

public final class R {

    public static final class color {
        public static final int TextColorBlack = 2131034112;
        public static final int TextColorGray = 2131034114;
        public static final int TextColorWhite = 2131034113;
        public static final int ToastBgColor = 2131034115;
        public static final int bgColor = 2131034120;
        public static final int btnColor = 2131034116;
        public static final int dialog_tiltle_blue = 2131034126;
        public static final int downLoadBackFocus = 2131034124;
        public static final int downLoadBackNomal = 2131034123;
        public static final int downLoadBackPressed = 2131034125;
        public static final int downLoadTextNomal = 2131034121;
        public static final int downLoadTextPressed = 2131034122;
        public static final int secondbtntextColor = 2131034118;
        public static final int textColorforCheckBox = 2131034119;
        public static final int textColorforItemTitle = 2131034117;
    }

    public static final class drawable {
        public static final int dialog_bg_click = 2130837537;
        public static final int dialog_bg_normal = 2130837538;
        public static final int dialog_button_colorlist = 2130837539;
        public static final int dialog_button_submit = 2130837540;
        public static final int dialog_cut_line = 2130837541;
        public static final int dialog_split_h = 2130837542;
        public static final int dialog_split_v = 2130837543;
        public static final int popup_bg = 2130837597;
        public static final int refresh = 2130837603;
        public static final int refresh_button = 2130837604;
        public static final int refresh_push = 2130837605;
        public static final int title = 2130837623;
        public static final int title_background = 2130837624;
    }

    public static final class id {
        public static final int AlipayTitle = 2131296478;
        public static final int btn_refresh = 2131296479;
        public static final int dialog_button_group = 2131296633;
        public static final int dialog_content_view = 2131296632;
        public static final int dialog_divider = 2131296630;
        public static final int dialog_message = 2131296631;
        public static final int dialog_split_v = 2131296635;
        public static final int dialog_title = 2131296629;
        public static final int left_button = 2131296634;
        public static final int mainView = 2131296476;
        public static final int right_button = 2131296636;
        public static final int webView = 2131296477;
    }

    public static final class layout {
        public static final int alipay = 2130903048;
        public static final int alipay_title = 2130903049;
        public static final int dialog_alert = 2130903078;
    }

    public static final class string {
        public static final int cancel = 2131099650;
        public static final int cancel_install_alipay = 2131099657;
        public static final int cancel_install_msp = 2131099656;
        public static final int confirm_title = 2131099648;
        public static final int content_description_icon = 2131099651;
        public static final int download = 2131099654;
        public static final int download_fail = 2131099655;
        public static final int ensure = 2131099649;
        public static final int install_alipay = 2131099660;
        public static final int install_msp = 2131099659;
        public static final int processing = 2131099653;
        public static final int redo = 2131099658;
        public static final int refresh = 2131099652;
    }

    public static final class style {
        public static final int AlertDialog = 2131165186;
        public static final int AppBaseTheme = 2131165184;
        public static final int AppTheme = 2131165185;
    }
}
