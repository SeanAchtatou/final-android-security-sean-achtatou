package com.alipay.android.app.util;

import android.content.Context;
import com.amap.mapapi.poisearch.PoiTypeDef;

public class StoreUtils {
    private static final String SETTING_INFOS = "alipay_file";

    public static String getValue(Context context, String key) {
        return context.getSharedPreferences(SETTING_INFOS, 0).getString(key, PoiTypeDef.All);
    }

    public static void putValue(Context context, String key, String value) {
        context.getSharedPreferences(SETTING_INFOS, 0).edit().putString(key, value).commit();
    }
}
