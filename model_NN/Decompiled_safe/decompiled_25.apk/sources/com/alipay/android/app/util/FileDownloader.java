package com.alipay.android.app.util;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.ref.WeakReference;
import javax.net.ssl.SSLException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public final class FileDownloader {
    /* access modifiers changed from: private */
    public FileFetch fetch;
    /* access modifiers changed from: private */
    public String fileUrl;
    /* access modifiers changed from: private */
    public IDownloadProgress progressOutput;
    /* access modifiers changed from: private */
    public String savePath;
    public boolean showProgress;
    private String tmpPath;

    public interface IDownloadProgress {
        void downloadFail();

        void downloadProgress(float f);

        void downloadSucess();
    }

    public FileDownloader() {
        this.showProgress = false;
    }

    public FileDownloader(boolean showProgress2) {
        this.showProgress = showProgress2;
    }

    public final void setFileUrl(String fileUrl2) {
        this.fileUrl = fileUrl2;
    }

    public final void setShowProgress(boolean showProgress2) {
        this.showProgress = showProgress2;
    }

    /* access modifiers changed from: protected */
    public final boolean showProgress() {
        return this.showProgress;
    }

    public final void setSavePath(String savePath2) {
        this.savePath = savePath2;
        this.tmpPath = String.valueOf(savePath2) + ".tmp";
    }

    public final void setProgressOutput(IDownloadProgress progressOutput2) {
        if (progressOutput2 != null) {
            this.progressOutput = progressOutput2;
        }
    }

    public void start() {
        final ProgressOutput output = new ProgressOutput(Looper.getMainLooper(), this, null);
        new Thread(new Runnable() {
            public void run() {
                FileDownloader.this.fetch = new FileFetch(FileDownloader.this.fileUrl, FileDownloader.this.savePath, FileDownloader.this);
                long fileLen = -1;
                if (FileDownloader.this.showProgress) {
                    fileLen = FileDownloader.this.getFileSize();
                    if (fileLen <= 0) {
                        output.sendEmptyMessage(0);
                        return;
                    }
                } else {
                    FileDownloader.this.deleteFile();
                }
                if (FileDownloader.this.showProgress) {
                    FileDownloader.this.readTempFile();
                    if (FileDownloader.this.fetch.getFileEnd() != fileLen) {
                        FileDownloader.this.deleteFile();
                        FileDownloader.this.fetch.setFileStart(0);
                        FileDownloader.this.fetch.setFileEnd(fileLen);
                    }
                }
                new Thread(FileDownloader.this.fetch).start();
                output.isFinished = false;
                while (!FileDownloader.this.fetch.isStop()) {
                    try {
                        Thread.sleep(1500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    output.sendEmptyMessage(0);
                }
                output.sendEmptyMessage(0);
            }
        }).start();
    }

    public void stop() {
        this.fetch.stop();
    }

    /* access modifiers changed from: private */
    public long getFileSize() {
        try {
            return getHttpEntity(this.fileUrl, false).getContentLength();
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    /* access modifiers changed from: private */
    public void deleteFile() {
        File file = new File(this.savePath);
        if (file.exists()) {
            file.delete();
        }
        File file2 = new File(this.tmpPath);
        if (file2.exists()) {
            file2.delete();
        }
    }

    /* access modifiers changed from: protected */
    public void writeTempFile() {
        FileOutputStream out = null;
        ObjectOutputStream objOut = null;
        try {
            FileOutputStream out2 = new FileOutputStream(this.tmpPath);
            try {
                ObjectOutputStream objOut2 = new ObjectOutputStream(out2);
                try {
                    objOut2.writeLong(this.fetch.getFileStart());
                    objOut2.writeLong(this.fetch.getFileEnd());
                    objOut2.flush();
                    try {
                        out2.close();
                    } catch (Exception e) {
                    }
                    try {
                        objOut2.close();
                    } catch (Exception e2) {
                    }
                } catch (Exception e3) {
                    e = e3;
                    objOut = objOut2;
                    out = out2;
                    try {
                        e.printStackTrace();
                        try {
                            out.close();
                        } catch (Exception e4) {
                        }
                        try {
                            objOut.close();
                        } catch (Exception e5) {
                        }
                    } catch (Throwable th) {
                        th = th;
                        try {
                            out.close();
                        } catch (Exception e6) {
                        }
                        try {
                            objOut.close();
                        } catch (Exception e7) {
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    objOut = objOut2;
                    out = out2;
                    out.close();
                    objOut.close();
                    throw th;
                }
            } catch (Exception e8) {
                e = e8;
                out = out2;
                e.printStackTrace();
                out.close();
                objOut.close();
            } catch (Throwable th3) {
                th = th3;
                out = out2;
                out.close();
                objOut.close();
                throw th;
            }
        } catch (Exception e9) {
            e = e9;
            e.printStackTrace();
            out.close();
            objOut.close();
        }
    }

    /* access modifiers changed from: private */
    public void readTempFile() {
        FileInputStream in = null;
        ObjectInputStream objIn = null;
        try {
            FileInputStream in2 = new FileInputStream(this.tmpPath);
            try {
                ObjectInputStream objIn2 = new ObjectInputStream(in2);
                try {
                    this.fetch.setFileStart(objIn2.readLong());
                    this.fetch.setFileEnd(objIn2.readLong());
                    try {
                        in2.close();
                    } catch (Exception e) {
                    }
                    try {
                        objIn2.close();
                    } catch (Exception e2) {
                    }
                } catch (Exception e3) {
                    e = e3;
                    objIn = objIn2;
                    in = in2;
                    try {
                        e.printStackTrace();
                        try {
                            in.close();
                        } catch (Exception e4) {
                        }
                        try {
                            objIn.close();
                        } catch (Exception e5) {
                        }
                    } catch (Throwable th) {
                        th = th;
                        try {
                            in.close();
                        } catch (Exception e6) {
                        }
                        try {
                            objIn.close();
                        } catch (Exception e7) {
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    objIn = objIn2;
                    in = in2;
                    in.close();
                    objIn.close();
                    throw th;
                }
            } catch (Exception e8) {
                e = e8;
                in = in2;
                e.printStackTrace();
                in.close();
                objIn.close();
            } catch (Throwable th3) {
                th = th3;
                in = in2;
                in.close();
                objIn.close();
                throw th;
            }
        } catch (Exception e9) {
            e = e9;
            e.printStackTrace();
            in.close();
            objIn.close();
        }
    }

    public static HttpEntity getHttpEntity(String netAddress, boolean isZip) throws Exception {
        try {
            HttpGet httpGet = new HttpGet(netAddress);
            HttpClient httpClient = new DefaultHttpClient();
            if (isZip) {
                httpGet.addHeader("Accept-Encoding", "gzip");
            }
            HttpResponse response = httpClient.execute(httpGet);
            int code = response.getStatusLine().getStatusCode();
            if (code == 200) {
                return response.getEntity();
            }
            throw new Exception("net work exception,ErrorCode :" + code);
        } catch (SSLException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private static class ProgressOutput extends Handler {
        WeakReference<FileDownloader> fileDownloader;
        /* access modifiers changed from: private */
        public boolean isFinished;

        /* synthetic */ ProgressOutput(Looper looper, FileDownloader fileDownloader2, ProgressOutput progressOutput) {
            this(looper, fileDownloader2);
        }

        private ProgressOutput(Looper looper, FileDownloader fd) {
            super(looper);
            this.isFinished = false;
            this.fileDownloader = new WeakReference<>(fd);
        }

        public void handleMessage(Message msg) {
            if (this.fileDownloader.get().progressOutput != null) {
                float progress = 50.0f;
                try {
                    if (this.fileDownloader.get().showProgress) {
                        progress = (float) ((this.fileDownloader.get().fetch.getFileStart() * 100) / this.fileDownloader.get().fetch.getFileEnd());
                    } else if (this.fileDownloader.get().fetch.isStop()) {
                        progress = 100.0f;
                    }
                    if (!this.fileDownloader.get().fetch.isStop()) {
                        this.fileDownloader.get().progressOutput.downloadProgress(progress);
                    } else if (progress == 100.0f && !this.isFinished) {
                        this.fileDownloader.get().progressOutput.downloadSucess();
                        this.isFinished = true;
                    } else if (progress > 100.0f) {
                        this.fileDownloader.get().deleteFile();
                        this.fileDownloader.get().progressOutput.downloadFail();
                    } else if (!this.isFinished) {
                        this.fileDownloader.get().progressOutput.downloadFail();
                    }
                } catch (Exception e) {
                    this.fileDownloader.get().progressOutput.downloadFail();
                }
            }
        }
    }
}
