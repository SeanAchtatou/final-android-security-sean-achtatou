package com.alipay.android.app.net;

public class WebResponseData {
    private String charset;
    private String contentType;
    private String response;
    private String status;

    public WebResponseData(String response2, String contentType2, String charset2, String status2) {
        this.response = response2;
        this.contentType = contentType2;
        this.charset = charset2;
        this.status = status2;
    }

    public String getResponse() {
        return this.response;
    }

    public void setResponse(String response2) {
        this.response = response2;
    }

    public String getContentType() {
        return this.contentType;
    }

    public void setContentType(String contentType2) {
        this.contentType = contentType2;
    }

    public String getCharset() {
        return this.charset;
    }

    public void setCharset(String charset2) {
        this.charset = charset2;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status2) {
        this.status = status2;
    }
}
