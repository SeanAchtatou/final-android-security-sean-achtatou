package com.alipay.android.app.util;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import com.alipay.android.app.IAlixPay;
import com.alipay.android.app.IRemoteServiceCallback;
import com.amap.mapapi.poisearch.PoiTypeDef;

public class PayHelper {
    /* access modifiers changed from: private */
    public IAlixPay mAlixPay;
    private IRemoteServiceCallback mCallback = new IRemoteServiceCallback.Stub() {
        public boolean isHideLoadingScreen() throws RemoteException {
            return false;
        }

        public void payEnd(boolean arg0, String arg1) throws RemoteException {
        }

        public void startActivity(String packageName, String className, int callingPid, Bundle bundle) throws RemoteException {
            Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
            if (bundle == null) {
                bundle = new Bundle();
            }
            try {
                bundle.putInt("CallingPid", callingPid);
                intent.putExtras(bundle);
            } catch (Exception e) {
                e.printStackTrace();
            }
            intent.setClassName(packageName, className);
            PayHelper.this.mContext.startActivity(intent);
        }
    };
    /* access modifiers changed from: private */
    public Activity mContext;
    private boolean mIsPaying = false;
    /* access modifiers changed from: private */
    public Object mLock;
    private ServiceConnection mServiceConnection = new ServiceConnection() {
        public void onServiceDisconnected(ComponentName name) {
            PayHelper.this.mAlixPay = null;
        }

        public void onServiceConnected(ComponentName name, IBinder service) {
            synchronized (PayHelper.this.mLock) {
                PayHelper.this.mAlixPay = IAlixPay.Stub.asInterface(service);
                PayHelper.this.mLock.notify();
            }
        }
    };

    public PayHelper(Activity context) {
        this.mContext = context;
        this.mLock = new Object();
    }

    public String pay4Msp(String orderInfo) {
        return pay(orderInfo, new Intent(IAlixPay.class.getName()));
    }

    public String pay4Client(String orderInfo) {
        return pay(orderInfo, new Intent("com.eg.android.AlipayGphone.IAlixPay"));
    }

    private String pay(String orderInfo, Intent intent) {
        String result = null;
        if (this.mIsPaying) {
            return PoiTypeDef.All;
        }
        this.mIsPaying = true;
        if (this.mAlixPay == null) {
            this.mContext.getApplicationContext().bindService(intent, this.mServiceConnection, 1);
        }
        try {
            synchronized (this.mLock) {
                if (this.mAlixPay == null) {
                    this.mLock.wait();
                }
            }
            this.mAlixPay.registerCallback(this.mCallback);
            result = this.mAlixPay.Pay(orderInfo);
            this.mAlixPay.unregisterCallback(this.mCallback);
            this.mContext.unbindService(this.mServiceConnection);
            this.mIsPaying = false;
            this.mAlixPay = null;
        } catch (Exception e) {
            try {
                e.printStackTrace();
            } finally {
                this.mIsPaying = false;
                this.mAlixPay = null;
            }
        }
        return result;
    }
}
