package com.alipay.android.app.sdk;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import com.alipay.android.app.lib.ResourceMap;
import com.alipay.android.app.util.LogUtils;
import com.alipay.android.app.util.Utils;
import com.alipay.android.app.widget.Loading;
import com.amap.mapapi.poisearch.PoiTypeDef;

public class WapPayActivity extends Activity {
    private static final String PAY_RESULT_TAG = "sdk_result_code:";
    /* access modifiers changed from: private */
    public Runnable mDelayRunnable = new Runnable() {
        public void run() {
            if (!WapPayActivity.this.mRefreshButton.isEnabled()) {
                WapPayActivity.this.mRefreshButton.setEnabled(true);
            }
            WapPayActivity.this.dismissLoading();
        }
    };
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler();
    private Loading mLoading;
    /* access modifiers changed from: private */
    public Button mRefreshButton;
    /* access modifiers changed from: private */
    public int mTimeout;
    /* access modifiers changed from: private */
    public WebView mWebView;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        String url = intent.getExtras().getString("url");
        this.mTimeout = intent.getExtras().getInt("timeout", 15);
        setContentView(ResourceMap.getLayout_pay_main());
        this.mWebView = (WebView) findViewById(ResourceMap.getId_webView());
        WebSettings settings = this.mWebView.getSettings();
        settings.setUserAgentString(Utils.getUserAgent(this));
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);
        settings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        this.mWebView.setWebViewClient(new MyWebViewClient(this, null));
        this.mWebView.setWebChromeClient(new MyWebChromeClient(this, null));
        this.mWebView.addJavascriptInterface(new InJavaScriptLocalObj(), "local_obj");
        settings.setMinimumFontSize(settings.getMinimumFontSize() + 8);
        this.mWebView.loadUrl(url);
        this.mRefreshButton = (Button) findViewById(ResourceMap.getId_btn_refresh());
        this.mRefreshButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                WapPayActivity.this.mWebView.reload();
            }
        });
        this.mRefreshButton.setEnabled(false);
    }

    public void onBackPressed() {
        if (this.mWebView.canGoBack()) {
            this.mWebView.goBack();
            return;
        }
        ResultStatus status = ResultStatus.getResultState(6001);
        Result.setWapResult(Result.parseResult(status.getStatus(), status.getMsg(), PoiTypeDef.All));
        finish();
    }

    public void finish() {
        notifyCaller();
        super.finish();
    }

    public void notifyCaller() {
        Object lock = AliPay.sLock;
        synchronized (lock) {
            try {
                lock.notify();
            } catch (Exception e) {
                LogUtils.printExceptionStackTrace(e);
            }
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    final class InJavaScriptLocalObj {
        InJavaScriptLocalObj() {
        }

        @JavascriptInterface
        public void showSource(String html) {
            LogUtils.d(html);
            if (html.contains(WapPayActivity.PAY_RESULT_TAG)) {
                int startPos = html.indexOf(WapPayActivity.PAY_RESULT_TAG);
                Result.setWapResult(html.substring(WapPayActivity.PAY_RESULT_TAG.length() + startPos, html.indexOf("-->", startPos)).trim());
                WapPayActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        WapPayActivity.this.finish();
                    }
                });
            }
        }
    }

    private class MyWebViewClient extends WebViewClient {
        private MyWebViewClient() {
        }

        /* synthetic */ MyWebViewClient(WapPayActivity wapPayActivity, MyWebViewClient myWebViewClient) {
            this();
        }

        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            handler.proceed();
        }

        public void onFormResubmission(WebView view, Message dontResend, Message resend) {
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        public void onLoadResource(WebView view, String url) {
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            WapPayActivity.this.showLoading();
            WapPayActivity.this.mRefreshButton.setEnabled(false);
            WapPayActivity.this.mHandler.postDelayed(WapPayActivity.this.mDelayRunnable, (long) (WapPayActivity.this.mTimeout * 1000));
            super.onPageStarted(view, url, favicon);
        }

        public void onPageFinished(WebView view, String url) {
            WapPayActivity.this.dismissLoading();
            WapPayActivity.this.mRefreshButton.setEnabled(true);
            WapPayActivity.this.mHandler.removeCallbacks(WapPayActivity.this.mDelayRunnable);
            view.loadUrl("javascript:window.local_obj.showSource('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');");
        }
    }

    private class MyWebChromeClient extends WebChromeClient {
        private MyWebChromeClient() {
        }

        /* synthetic */ MyWebChromeClient(WapPayActivity wapPayActivity, MyWebChromeClient myWebChromeClient) {
            this();
        }

        public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
            new AlertDialog.Builder(WapPayActivity.this).setTitle(ResourceMap.getString_confirm_title()).setMessage(message).setPositiveButton(ResourceMap.getString_ensure(), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    result.confirm();
                }
            }).setNegativeButton(ResourceMap.getString_cancel(), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    result.cancel();
                }
            }).show();
            return true;
        }

        public boolean onJsConfirm(WebView view, String url, String message, final JsResult result) {
            new AlertDialog.Builder(WapPayActivity.this).setTitle(ResourceMap.getString_confirm_title()).setMessage(message).setPositiveButton(ResourceMap.getString_ensure(), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    result.confirm();
                }
            }).setNegativeButton(ResourceMap.getString_cancel(), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    result.cancel();
                }
            }).show();
            return true;
        }

        public boolean onJsPrompt(WebView view, String url, String message, String defaultValue, final JsPromptResult result) {
            new AlertDialog.Builder(WapPayActivity.this).setTitle(ResourceMap.getString_confirm_title()).setMessage(message).setPositiveButton(ResourceMap.getString_ensure(), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    result.confirm();
                }
            }).setNegativeButton(ResourceMap.getString_cancel(), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    result.cancel();
                }
            }).show();
            return true;
        }
    }

    /* access modifiers changed from: private */
    public void showLoading() {
        if (this.mLoading == null) {
            this.mLoading = new Loading(this);
        }
        this.mLoading.show();
    }

    /* access modifiers changed from: private */
    public void dismissLoading() {
        if (this.mLoading != null && this.mLoading.isShowing()) {
            this.mLoading.dismiss();
        }
        this.mLoading = null;
    }
}
