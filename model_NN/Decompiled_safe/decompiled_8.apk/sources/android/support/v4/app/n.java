package android.support.v4.app;

import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.c.a;
import android.support.v4.c.c;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.ScaleAnimation;
import com.mapabc.minimap.map.vmap.NativeMapEngine;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;

final class n extends m {
    private static boolean d = (Build.VERSION.SDK_INT >= 11);
    private static Interpolator v = new DecelerateInterpolator(2.5f);
    private static Interpolator w = new DecelerateInterpolator(1.5f);

    /* renamed from: a  reason: collision with root package name */
    ArrayList f342a;
    ArrayList b;
    int c = 0;
    private ArrayList e;
    private Runnable[] f;
    private boolean g;
    private ArrayList h;
    private ArrayList i;
    private ArrayList j;
    private ArrayList k;
    private ArrayList l;
    private g m;
    private l n;
    private Fragment o;
    private boolean p;
    private boolean q;
    private boolean r;
    private Bundle s = null;
    private SparseArray t = null;
    private Runnable u = new o(this);

    static {
        new AccelerateInterpolator(2.5f);
        new AccelerateInterpolator(1.5f);
    }

    n() {
    }

    private static Animation a(float f2, float f3) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(f2, f3);
        alphaAnimation.setInterpolator(w);
        alphaAnimation.setDuration(220);
        return alphaAnimation;
    }

    private static Animation a(float f2, float f3, float f4, float f5) {
        AnimationSet animationSet = new AnimationSet(false);
        ScaleAnimation scaleAnimation = new ScaleAnimation(f2, f3, f2, f3, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setInterpolator(v);
        scaleAnimation.setDuration(220);
        animationSet.addAnimation(scaleAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(f4, f5);
        alphaAnimation.setInterpolator(w);
        alphaAnimation.setDuration(220);
        animationSet.addAnimation(alphaAnimation);
        return animationSet;
    }

    private Animation a(Fragment fragment, int i2, boolean z, int i3) {
        Animation loadAnimation;
        int i4 = fragment.E;
        Fragment.j();
        if (fragment.E != 0 && (loadAnimation = AnimationUtils.loadAnimation(this.m, fragment.E)) != null) {
            return loadAnimation;
        }
        if (i2 == 0) {
            return null;
        }
        char c2 = 65535;
        switch (i2) {
            case 4097:
                if (!z) {
                    c2 = 2;
                    break;
                } else {
                    c2 = 1;
                    break;
                }
            case 4099:
                if (!z) {
                    c2 = 6;
                    break;
                } else {
                    c2 = 5;
                    break;
                }
            case 8194:
                if (!z) {
                    c2 = 4;
                    break;
                } else {
                    c2 = 3;
                    break;
                }
        }
        if (c2 < 0) {
            return null;
        }
        switch (c2) {
            case 1:
                g gVar = this.m;
                return a(1.125f, 1.0f, 0.0f, 1.0f);
            case 2:
                g gVar2 = this.m;
                return a(1.0f, 0.975f, 1.0f, 0.0f);
            case 3:
                g gVar3 = this.m;
                return a(0.975f, 1.0f, 0.0f, 1.0f);
            case 4:
                g gVar4 = this.m;
                return a(1.0f, 1.075f, 1.0f, 0.0f);
            case 5:
                g gVar5 = this.m;
                return a(0.0f, 1.0f);
            case 6:
                g gVar6 = this.m;
                return a(1.0f, 0.0f);
            default:
                if (i3 == 0 && this.m.getWindow() != null) {
                    i3 = this.m.getWindow().getAttributes().windowAnimations;
                }
                return i3 == 0 ? null : null;
        }
    }

    private void a(int i2, b bVar) {
        synchronized (this) {
            if (this.k == null) {
                this.k = new ArrayList();
            }
            int size = this.k.size();
            if (i2 < size) {
                this.k.set(i2, bVar);
            } else {
                while (size < i2) {
                    this.k.add(null);
                    if (this.l == null) {
                        this.l = new ArrayList();
                    }
                    this.l.add(Integer.valueOf(size));
                    size++;
                }
                this.k.add(bVar);
            }
        }
    }

    private void a(RuntimeException runtimeException) {
        Log.e("FragmentManager", runtimeException.getMessage());
        Log.e("FragmentManager", "Activity state:");
        PrintWriter printWriter = new PrintWriter(new a("FragmentManager"));
        if (this.m != null) {
            try {
                this.m.dump("  ", null, printWriter, new String[0]);
            } catch (Exception e2) {
                Log.e("FragmentManager", "Failed dumping state", e2);
            }
        } else {
            try {
                a("  ", (FileDescriptor) null, printWriter, new String[0]);
            } catch (Exception e3) {
                Log.e("FragmentManager", "Failed dumping state", e3);
            }
        }
        throw runtimeException;
    }

    public static int c(int i2) {
        switch (i2) {
            case 4097:
                return 8194;
            case 4099:
                return 4099;
            case 8194:
                return 4097;
            default:
                return 0;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.n.a(int, int, int, boolean):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.app.n.a(float, float, float, float):android.view.animation.Animation
      android.support.v4.app.n.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation
      android.support.v4.app.n.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.n.a(int, int, int, boolean):void */
    private void d(int i2) {
        a(i2, 0, 0, false);
    }

    private void d(Fragment fragment) {
        if (fragment.H != null) {
            if (this.t == null) {
                this.t = new SparseArray();
            } else {
                this.t.clear();
            }
            fragment.H.saveHierarchyState(this.t);
            if (this.t.size() > 0) {
                fragment.e = this.t;
                this.t = null;
            }
        }
    }

    private Bundle e(Fragment fragment) {
        Bundle bundle;
        if (this.s == null) {
            this.s = new Bundle();
        }
        fragment.f(this.s);
        if (!this.s.isEmpty()) {
            bundle = this.s;
            this.s = null;
        } else {
            bundle = null;
        }
        if (fragment.G != null) {
            d(fragment);
        }
        if (fragment.e != null) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putSparseParcelableArray("android:view_state", fragment.e);
        }
        if (!fragment.J) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putBoolean("android:user_visible_hint", fragment.J);
        }
        return bundle;
    }

    private void r() {
        if (this.f342a != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.f342a.size()) {
                    Fragment fragment = (Fragment) this.f342a.get(i3);
                    if (fragment != null) {
                        b(fragment);
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    private void s() {
        if (this.p) {
            throw new IllegalStateException("Can not perform this action after onSaveInstanceState");
        }
    }

    public final int a(b bVar) {
        int i2;
        synchronized (this) {
            if (this.l == null || this.l.size() <= 0) {
                if (this.k == null) {
                    this.k = new ArrayList();
                }
                i2 = this.k.size();
                this.k.add(bVar);
            } else {
                i2 = ((Integer) this.l.remove(this.l.size() - 1)).intValue();
                this.k.set(i2, bVar);
            }
        }
        return i2;
    }

    public final Fragment.SavedState a(Fragment fragment) {
        Bundle e2;
        if (fragment.f < 0) {
            a(new IllegalStateException("Fragment " + fragment + " is not currently in the FragmentManager"));
        }
        if (fragment.f328a <= 0 || (e2 = e(fragment)) == null) {
            return null;
        }
        return new Fragment.SavedState(e2);
    }

    public final Fragment a(int i2) {
        if (this.b != null) {
            for (int size = this.b.size() - 1; size >= 0; size--) {
                Fragment fragment = (Fragment) this.b.get(size);
                if (fragment != null && fragment.w == i2) {
                    return fragment;
                }
            }
        }
        if (this.f342a != null) {
            for (int size2 = this.f342a.size() - 1; size2 >= 0; size2--) {
                Fragment fragment2 = (Fragment) this.f342a.get(size2);
                if (fragment2 != null && fragment2.w == i2) {
                    return fragment2;
                }
            }
        }
        return null;
    }

    public final Fragment a(Bundle bundle, String str) {
        int i2 = bundle.getInt(str, -1);
        if (i2 == -1) {
            return null;
        }
        if (i2 >= this.f342a.size()) {
            a(new IllegalStateException("Fragement no longer exists for key " + str + ": index " + i2));
        }
        Fragment fragment = (Fragment) this.f342a.get(i2);
        if (fragment != null) {
            return fragment;
        }
        a(new IllegalStateException("Fragement no longer exists for key " + str + ": index " + i2));
        return fragment;
    }

    public final Fragment a(String str) {
        if (!(this.b == null || str == null)) {
            for (int size = this.b.size() - 1; size >= 0; size--) {
                Fragment fragment = (Fragment) this.b.get(size);
                if (fragment != null && str.equals(fragment.y)) {
                    return fragment;
                }
            }
        }
        if (!(this.f342a == null || str == null)) {
            for (int size2 = this.f342a.size() - 1; size2 >= 0; size2--) {
                Fragment fragment2 = (Fragment) this.f342a.get(size2);
                if (fragment2 != null && str.equals(fragment2.y)) {
                    return fragment2;
                }
            }
        }
        return null;
    }

    public final w a() {
        return new b(this);
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2, int i3, int i4, boolean z) {
        if (this.m == null && i2 != 0) {
            throw new IllegalStateException("No activity");
        } else if (z || this.c != i2) {
            this.c = i2;
            if (this.f342a != null) {
                for (int i5 = 0; i5 < this.f342a.size(); i5++) {
                    Fragment fragment = (Fragment) this.f342a.get(i5);
                    if (fragment != null) {
                        a(fragment, i2, i3, i4, false);
                        if (fragment.K != null) {
                            y yVar = fragment.K;
                            c.a();
                        }
                    }
                }
                r();
            }
        }
    }

    public final void a(Configuration configuration) {
        if (this.b != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.b.size()) {
                    Fragment fragment = (Fragment) this.b.get(i3);
                    if (fragment != null) {
                        fragment.a(configuration);
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public final void a(Bundle bundle, String str, Fragment fragment) {
        if (fragment.f < 0) {
            a(new IllegalStateException("Fragment " + fragment + " is not currently in the FragmentManager"));
        }
        bundle.putInt(str, fragment.f);
    }

    /* access modifiers changed from: package-private */
    public final void a(Parcelable parcelable, ArrayList arrayList) {
        if (parcelable != null) {
            FragmentManagerState fragmentManagerState = (FragmentManagerState) parcelable;
            if (fragmentManagerState.f330a != null) {
                if (arrayList != null) {
                    for (int i2 = 0; i2 < arrayList.size(); i2++) {
                        Fragment fragment = (Fragment) arrayList.get(i2);
                        FragmentState fragmentState = fragmentManagerState.f330a[fragment.f];
                        fragmentState.b = fragment;
                        fragment.e = null;
                        fragment.r = 0;
                        fragment.p = false;
                        fragment.l = false;
                        fragment.i = null;
                        if (fragmentState.f331a != null) {
                            fragmentState.f331a.setClassLoader(this.m.getClassLoader());
                            fragment.e = fragmentState.f331a.getSparseParcelableArray("android:view_state");
                        }
                    }
                }
                this.f342a = new ArrayList(fragmentManagerState.f330a.length);
                if (this.h != null) {
                    this.h.clear();
                }
                for (int i3 = 0; i3 < fragmentManagerState.f330a.length; i3++) {
                    FragmentState fragmentState2 = fragmentManagerState.f330a[i3];
                    if (fragmentState2 != null) {
                        this.f342a.add(fragmentState2.a(this.m, this.o));
                        fragmentState2.b = null;
                    } else {
                        this.f342a.add(null);
                        if (this.h == null) {
                            this.h = new ArrayList();
                        }
                        this.h.add(Integer.valueOf(i3));
                    }
                }
                if (arrayList != null) {
                    for (int i4 = 0; i4 < arrayList.size(); i4++) {
                        Fragment fragment2 = (Fragment) arrayList.get(i4);
                        if (fragment2.j >= 0) {
                            if (fragment2.j < this.f342a.size()) {
                                fragment2.i = (Fragment) this.f342a.get(fragment2.j);
                            } else {
                                Log.w("FragmentManager", "Re-attaching retained fragment " + fragment2 + " target no longer exists: " + fragment2.j);
                                fragment2.i = null;
                            }
                        }
                    }
                }
                if (fragmentManagerState.b != null) {
                    this.b = new ArrayList(fragmentManagerState.b.length);
                    for (int i5 = 0; i5 < fragmentManagerState.b.length; i5++) {
                        Fragment fragment3 = (Fragment) this.f342a.get(fragmentManagerState.b[i5]);
                        if (fragment3 == null) {
                            a(new IllegalStateException("No instantiated fragment for index #" + fragmentManagerState.b[i5]));
                        }
                        fragment3.l = true;
                        if (this.b.contains(fragment3)) {
                            throw new IllegalStateException("Already added!");
                        }
                        this.b.add(fragment3);
                    }
                } else {
                    this.b = null;
                }
                if (fragmentManagerState.c != null) {
                    this.i = new ArrayList(fragmentManagerState.c.length);
                    for (BackStackState a2 : fragmentManagerState.c) {
                        b a3 = a2.a(this);
                        this.i.add(a3);
                        if (a3.g >= 0) {
                            a(a3.g, a3);
                        }
                    }
                    return;
                }
                this.i = null;
            }
        }
    }

    public final void a(Fragment fragment, int i2, int i3) {
        boolean z = !fragment.b();
        if (!fragment.A || z) {
            if (this.b != null) {
                this.b.remove(fragment);
            }
            fragment.l = false;
            fragment.m = true;
            a(fragment, z ? 0 : 1, i2, i3, false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.n.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation
     arg types: [android.support.v4.app.Fragment, int, int, int]
     candidates:
      android.support.v4.app.n.a(float, float, float, float):android.view.animation.Animation
      android.support.v4.app.n.a(int, int, int, boolean):void
      android.support.v4.app.n.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.n.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation */
    /* access modifiers changed from: package-private */
    public final void a(Fragment fragment, int i2, int i3, int i4, boolean z) {
        ViewGroup viewGroup;
        if ((!fragment.l || fragment.A) && i2 > 1) {
            i2 = 1;
        }
        if (fragment.m && i2 > fragment.f328a) {
            i2 = fragment.f328a;
        }
        if (fragment.I && fragment.f328a < 4 && i2 > 3) {
            i2 = 3;
        }
        if (fragment.f328a >= i2) {
            if (fragment.f328a > i2) {
                switch (fragment.f328a) {
                    case 5:
                        if (i2 < 5) {
                            fragment.w();
                            fragment.n = false;
                        }
                    case 4:
                        if (i2 < 4) {
                            fragment.x();
                        }
                    case 3:
                        if (i2 < 3) {
                            fragment.y();
                        }
                    case 2:
                        if (i2 < 2) {
                            if (fragment.G != null && !this.m.isFinishing() && fragment.e == null) {
                                d(fragment);
                            }
                            fragment.z();
                            if (!(fragment.G == null || fragment.F == null)) {
                                Animation a2 = (this.c <= 0 || this.q) ? null : a(fragment, i3, false, i4);
                                if (a2 != null) {
                                    fragment.b = fragment.G;
                                    fragment.c = i2;
                                    a2.setAnimationListener(new p(this, fragment));
                                    fragment.G.startAnimation(a2);
                                }
                                fragment.F.removeView(fragment.G);
                            }
                            fragment.F = null;
                            fragment.G = null;
                            fragment.H = null;
                        }
                        break;
                    case 1:
                        if (i2 <= 0) {
                            if (this.q && fragment.b != null) {
                                View view = fragment.b;
                                fragment.b = null;
                                view.clearAnimation();
                            }
                            if (fragment.b == null) {
                                if (!fragment.C) {
                                    fragment.A();
                                }
                                fragment.D = false;
                                fragment.r();
                                if (fragment.D) {
                                    if (!z) {
                                        if (!fragment.C) {
                                            if (fragment.f >= 0) {
                                                this.f342a.set(fragment.f, null);
                                                if (this.h == null) {
                                                    this.h = new ArrayList();
                                                }
                                                this.h.add(Integer.valueOf(fragment.f));
                                                this.m.a(fragment.g);
                                                fragment.q();
                                                break;
                                            }
                                        } else {
                                            fragment.t = null;
                                            fragment.s = null;
                                            break;
                                        }
                                    }
                                } else {
                                    throw new aa("Fragment " + fragment + " did not call through to super.onDetach()");
                                }
                            } else {
                                fragment.c = i2;
                                i2 = 1;
                                break;
                            }
                        }
                        break;
                }
            }
        } else if (!fragment.o || fragment.p) {
            if (fragment.b != null) {
                fragment.b = null;
                a(fragment, fragment.c, 0, 0, true);
            }
            switch (fragment.f328a) {
                case 0:
                    if (fragment.d != null) {
                        fragment.e = fragment.d.getSparseParcelableArray("android:view_state");
                        fragment.i = a(fragment.d, "android:target_state");
                        if (fragment.i != null) {
                            fragment.k = fragment.d.getInt("android:target_req_state", 0);
                        }
                        fragment.J = fragment.d.getBoolean("android:user_visible_hint", true);
                        if (!fragment.J) {
                            fragment.I = true;
                            if (i2 > 3) {
                                i2 = 3;
                            }
                        }
                    }
                    fragment.t = this.m;
                    fragment.v = this.o;
                    fragment.s = this.o != null ? this.o.u : this.m.b;
                    fragment.D = false;
                    fragment.a(this.m);
                    if (!fragment.D) {
                        throw new aa("Fragment " + fragment + " did not call through to super.onAttach()");
                    }
                    if (fragment.v == null) {
                        g gVar = this.m;
                        g.f_();
                    }
                    if (!fragment.C) {
                        fragment.d(fragment.d);
                    }
                    fragment.C = false;
                    if (fragment.o) {
                        Bundle bundle = fragment.d;
                        LayoutInflater h2 = fragment.h();
                        Bundle bundle2 = fragment.d;
                        fragment.G = fragment.b(h2, null);
                        if (fragment.G != null) {
                            fragment.H = fragment.G;
                            fragment.G = z.a(fragment.G);
                            if (fragment.z) {
                                fragment.G.setVisibility(8);
                            }
                            View view2 = fragment.G;
                            Bundle bundle3 = fragment.d;
                            Fragment.k();
                        } else {
                            fragment.H = null;
                        }
                    }
                case 1:
                    if (i2 > 1) {
                        if (!fragment.o) {
                            if (fragment.x != 0) {
                                viewGroup = (ViewGroup) this.n.a(fragment.x);
                                if (viewGroup == null && !fragment.q) {
                                    a(new IllegalArgumentException("No view found for id 0x" + Integer.toHexString(fragment.x) + " (" + fragment.d().getResourceName(fragment.x) + ") for fragment " + fragment));
                                }
                            } else {
                                viewGroup = null;
                            }
                            fragment.F = viewGroup;
                            Bundle bundle4 = fragment.d;
                            LayoutInflater h3 = fragment.h();
                            Bundle bundle5 = fragment.d;
                            fragment.G = fragment.b(h3, viewGroup);
                            if (fragment.G != null) {
                                fragment.H = fragment.G;
                                fragment.G = z.a(fragment.G);
                                if (viewGroup != null) {
                                    Animation a3 = a(fragment, i3, true, i4);
                                    if (a3 != null) {
                                        fragment.G.startAnimation(a3);
                                    }
                                    viewGroup.addView(fragment.G);
                                }
                                if (fragment.z) {
                                    fragment.G.setVisibility(8);
                                }
                                View view3 = fragment.G;
                                Bundle bundle6 = fragment.d;
                                Fragment.k();
                            } else {
                                fragment.H = null;
                            }
                        }
                        fragment.e(fragment.d);
                        if (fragment.G != null) {
                            Bundle bundle7 = fragment.d;
                            fragment.a();
                        }
                        fragment.d = null;
                    }
                case 2:
                case 3:
                    if (i2 > 3) {
                        fragment.t();
                    }
                case 4:
                    if (i2 > 4) {
                        fragment.n = true;
                        fragment.c_();
                        fragment.d = null;
                        fragment.e = null;
                        break;
                    }
                    break;
            }
        } else {
            return;
        }
        fragment.f328a = i2;
    }

    public final void a(Fragment fragment, boolean z) {
        if (this.b == null) {
            this.b = new ArrayList();
        }
        if (fragment.f < 0) {
            if (this.h == null || this.h.size() <= 0) {
                if (this.f342a == null) {
                    this.f342a = new ArrayList();
                }
                fragment.a(this.f342a.size(), this.o);
                this.f342a.add(fragment);
            } else {
                fragment.a(((Integer) this.h.remove(this.h.size() - 1)).intValue(), this.o);
                this.f342a.set(fragment.f, fragment);
            }
        }
        if (fragment.A) {
            return;
        }
        if (this.b.contains(fragment)) {
            throw new IllegalStateException("Fragment already added: " + fragment);
        }
        this.b.add(fragment);
        fragment.l = true;
        fragment.m = false;
        if (z) {
            c(fragment);
        }
    }

    public final void a(g gVar, l lVar, Fragment fragment) {
        if (this.m != null) {
            throw new IllegalStateException("Already attached");
        }
        this.m = gVar;
        this.n = lVar;
        this.o = fragment;
    }

    public final void a(Runnable runnable, boolean z) {
        if (!z) {
            s();
        }
        synchronized (this) {
            if (this.m == null) {
                throw new IllegalStateException("Activity has been destroyed");
            }
            if (this.e == null) {
                this.e = new ArrayList();
            }
            this.e.add(runnable);
            if (this.e.size() == 1) {
                this.m.f337a.removeCallbacks(this.u);
                this.m.f337a.post(this.u);
            }
        }
    }

    public final void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        int size;
        int size2;
        int size3;
        int size4;
        int size5;
        int size6;
        String str2 = str + "    ";
        if (this.f342a != null && (size6 = this.f342a.size()) > 0) {
            printWriter.print(str);
            printWriter.print("Active Fragments in ");
            printWriter.print(Integer.toHexString(System.identityHashCode(this)));
            printWriter.println(":");
            for (int i2 = 0; i2 < size6; i2++) {
                Fragment fragment = (Fragment) this.f342a.get(i2);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i2);
                printWriter.print(": ");
                printWriter.println(fragment);
                if (fragment != null) {
                    fragment.a(str2, fileDescriptor, printWriter, strArr);
                }
            }
        }
        if (this.b != null && (size5 = this.b.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Added Fragments:");
            for (int i3 = 0; i3 < size5; i3++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i3);
                printWriter.print(": ");
                printWriter.println(((Fragment) this.b.get(i3)).toString());
            }
        }
        if (this.j != null && (size4 = this.j.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Fragments Created Menus:");
            for (int i4 = 0; i4 < size4; i4++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i4);
                printWriter.print(": ");
                printWriter.println(((Fragment) this.j.get(i4)).toString());
            }
        }
        if (this.i != null && (size3 = this.i.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Back Stack:");
            for (int i5 = 0; i5 < size3; i5++) {
                b bVar = (b) this.i.get(i5);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i5);
                printWriter.print(": ");
                printWriter.println(bVar.toString());
                bVar.a(str2, printWriter);
            }
        }
        synchronized (this) {
            if (this.k != null && (size2 = this.k.size()) > 0) {
                printWriter.print(str);
                printWriter.println("Back Stack Indices:");
                for (int i6 = 0; i6 < size2; i6++) {
                    printWriter.print(str);
                    printWriter.print("  #");
                    printWriter.print(i6);
                    printWriter.print(": ");
                    printWriter.println((b) this.k.get(i6));
                }
            }
            if (this.l != null && this.l.size() > 0) {
                printWriter.print(str);
                printWriter.print("mAvailBackStackIndices: ");
                printWriter.println(Arrays.toString(this.l.toArray()));
            }
        }
        if (this.e != null && (size = this.e.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Pending Actions:");
            for (int i7 = 0; i7 < size; i7++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i7);
                printWriter.print(": ");
                printWriter.println((Runnable) this.e.get(i7));
            }
        }
        printWriter.print(str);
        printWriter.println("FragmentManager misc state:");
        printWriter.print(str);
        printWriter.print("  mActivity=");
        printWriter.println(this.m);
        printWriter.print(str);
        printWriter.print("  mContainer=");
        printWriter.println(this.n);
        if (this.o != null) {
            printWriter.print(str);
            printWriter.print("  mParent=");
            printWriter.println(this.o);
        }
        printWriter.print(str);
        printWriter.print("  mCurState=");
        printWriter.print(this.c);
        printWriter.print(" mStateSaved=");
        printWriter.print(this.p);
        printWriter.print(" mDestroyed=");
        printWriter.println(this.q);
        if (this.h != null && this.h.size() > 0) {
            printWriter.print(str);
            printWriter.print("  mAvailIndices: ");
            printWriter.println(Arrays.toString(this.h.toArray()));
        }
    }

    public final boolean a(Menu menu) {
        if (this.b == null) {
            return false;
        }
        boolean z = false;
        for (int i2 = 0; i2 < this.b.size(); i2++) {
            Fragment fragment = (Fragment) this.b.get(i2);
            if (fragment != null && fragment.a(menu)) {
                z = true;
            }
        }
        return z;
    }

    public final boolean a(Menu menu, MenuInflater menuInflater) {
        boolean z;
        ArrayList arrayList = null;
        if (this.b != null) {
            int i2 = 0;
            z = false;
            while (i2 < this.b.size()) {
                Fragment fragment = (Fragment) this.b.get(i2);
                if (fragment != null && fragment.a(menu, menuInflater)) {
                    z = true;
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.add(fragment);
                }
                i2++;
                z = z;
            }
        } else {
            z = false;
        }
        if (this.j != null) {
            for (int i3 = 0; i3 < this.j.size(); i3++) {
                Fragment fragment2 = (Fragment) this.j.get(i3);
                if (arrayList == null || !arrayList.contains(fragment2)) {
                    Fragment.s();
                }
            }
        }
        this.j = arrayList;
        return z;
    }

    public final boolean a(MenuItem menuItem) {
        if (this.b == null) {
            return false;
        }
        for (int i2 = 0; i2 < this.b.size(); i2++) {
            Fragment fragment = (Fragment) this.b.get(i2);
            if (fragment != null && fragment.a(menuItem)) {
                return true;
            }
        }
        return false;
    }

    public final void b(int i2) {
        synchronized (this) {
            this.k.set(i2, null);
            if (this.l == null) {
                this.l = new ArrayList();
            }
            this.l.add(Integer.valueOf(i2));
        }
    }

    public final void b(Fragment fragment) {
        if (!fragment.I) {
            return;
        }
        if (this.g) {
            this.r = true;
            return;
        }
        fragment.I = false;
        a(fragment, this.c, 0, 0, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.n.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation
     arg types: [android.support.v4.app.Fragment, int, int, int]
     candidates:
      android.support.v4.app.n.a(float, float, float, float):android.view.animation.Animation
      android.support.v4.app.n.a(int, int, int, boolean):void
      android.support.v4.app.n.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.n.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation */
    public final void b(Fragment fragment, int i2, int i3) {
        if (!fragment.z) {
            fragment.z = true;
            if (fragment.G != null) {
                Animation a2 = a(fragment, i2, true, i3);
                if (a2 != null) {
                    fragment.G.startAnimation(a2);
                }
                fragment.G.setVisibility(8);
            }
            boolean z = fragment.l;
            Fragment.g();
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(b bVar) {
        if (this.i == null) {
            this.i = new ArrayList();
        }
        this.i.add(bVar);
    }

    public final void b(Menu menu) {
        if (this.b != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.b.size()) {
                    Fragment fragment = (Fragment) this.b.get(i3);
                    if (fragment != null) {
                        fragment.b(menu);
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public final boolean b() {
        return d();
    }

    public final boolean b(MenuItem menuItem) {
        if (this.b == null) {
            return false;
        }
        for (int i2 = 0; i2 < this.b.size(); i2++) {
            Fragment fragment = (Fragment) this.b.get(i2);
            if (fragment != null && fragment.b(menuItem)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public final void c(Fragment fragment) {
        a(fragment, this.c, 0, 0, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.n.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation
     arg types: [android.support.v4.app.Fragment, int, int, int]
     candidates:
      android.support.v4.app.n.a(float, float, float, float):android.view.animation.Animation
      android.support.v4.app.n.a(int, int, int, boolean):void
      android.support.v4.app.n.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.n.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation */
    public final void c(Fragment fragment, int i2, int i3) {
        if (fragment.z) {
            fragment.z = false;
            if (fragment.G != null) {
                Animation a2 = a(fragment, i2, true, i3);
                if (a2 != null) {
                    fragment.G.startAnimation(a2);
                }
                fragment.G.setVisibility(0);
            }
            boolean z = fragment.l;
            Fragment.g();
        }
    }

    public final boolean c() {
        int size;
        s();
        d();
        Handler handler = this.m.f337a;
        if (this.i == null || this.i.size() - 1 < 0) {
            return false;
        }
        ((b) this.i.remove(size)).d();
        return true;
    }

    public final void d(Fragment fragment, int i2, int i3) {
        if (!fragment.A) {
            fragment.A = true;
            if (fragment.l) {
                if (this.b != null) {
                    this.b.remove(fragment);
                }
                fragment.l = false;
                a(fragment, 1, i2, i3, false);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0080, code lost:
        r6.g = true;
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0083, code lost:
        if (r1 >= r3) goto L_0x0097;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0085, code lost:
        r6.f[r1].run();
        r6.f[r1] = null;
        r1 = r1 + 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean d() {
        /*
            r6 = this;
            r0 = 1
            r2 = 0
            boolean r1 = r6.g
            if (r1 == 0) goto L_0x000e
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Recursive entry to executePendingTransactions"
            r0.<init>(r1)
            throw r0
        L_0x000e:
            android.os.Looper r1 = android.os.Looper.myLooper()
            android.support.v4.app.g r3 = r6.m
            android.os.Handler r3 = r3.f337a
            android.os.Looper r3 = r3.getLooper()
            if (r1 == r3) goto L_0x0024
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Must be called from main thread of process"
            r0.<init>(r1)
            throw r0
        L_0x0024:
            r1 = r2
        L_0x0025:
            monitor-enter(r6)
            java.util.ArrayList r3 = r6.e     // Catch:{ all -> 0x0094 }
            if (r3 == 0) goto L_0x0032
            java.util.ArrayList r3 = r6.e     // Catch:{ all -> 0x0094 }
            int r3 = r3.size()     // Catch:{ all -> 0x0094 }
            if (r3 != 0) goto L_0x0057
        L_0x0032:
            monitor-exit(r6)     // Catch:{ all -> 0x0094 }
            boolean r0 = r6.r
            if (r0 == 0) goto L_0x00a0
            r3 = r2
        L_0x0038:
            java.util.ArrayList r0 = r6.f342a
            int r0 = r0.size()
            if (r3 >= r0) goto L_0x009b
            java.util.ArrayList r0 = r6.f342a
            java.lang.Object r0 = r0.get(r3)
            android.support.v4.app.Fragment r0 = (android.support.v4.app.Fragment) r0
            if (r0 == 0) goto L_0x0053
            android.support.v4.app.y r4 = r0.K
            if (r4 == 0) goto L_0x0053
            android.support.v4.app.y r0 = r0.K
            android.support.v4.c.c.a()
        L_0x0053:
            int r0 = r3 + 1
            r3 = r0
            goto L_0x0038
        L_0x0057:
            java.util.ArrayList r1 = r6.e     // Catch:{ all -> 0x0094 }
            int r3 = r1.size()     // Catch:{ all -> 0x0094 }
            java.lang.Runnable[] r1 = r6.f     // Catch:{ all -> 0x0094 }
            if (r1 == 0) goto L_0x0066
            java.lang.Runnable[] r1 = r6.f     // Catch:{ all -> 0x0094 }
            int r1 = r1.length     // Catch:{ all -> 0x0094 }
            if (r1 >= r3) goto L_0x006a
        L_0x0066:
            java.lang.Runnable[] r1 = new java.lang.Runnable[r3]     // Catch:{ all -> 0x0094 }
            r6.f = r1     // Catch:{ all -> 0x0094 }
        L_0x006a:
            java.util.ArrayList r1 = r6.e     // Catch:{ all -> 0x0094 }
            java.lang.Runnable[] r4 = r6.f     // Catch:{ all -> 0x0094 }
            r1.toArray(r4)     // Catch:{ all -> 0x0094 }
            java.util.ArrayList r1 = r6.e     // Catch:{ all -> 0x0094 }
            r1.clear()     // Catch:{ all -> 0x0094 }
            android.support.v4.app.g r1 = r6.m     // Catch:{ all -> 0x0094 }
            android.os.Handler r1 = r1.f337a     // Catch:{ all -> 0x0094 }
            java.lang.Runnable r4 = r6.u     // Catch:{ all -> 0x0094 }
            r1.removeCallbacks(r4)     // Catch:{ all -> 0x0094 }
            monitor-exit(r6)     // Catch:{ all -> 0x0094 }
            r6.g = r0
            r1 = r2
        L_0x0083:
            if (r1 >= r3) goto L_0x0097
            java.lang.Runnable[] r4 = r6.f
            r4 = r4[r1]
            r4.run()
            java.lang.Runnable[] r4 = r6.f
            r5 = 0
            r4[r1] = r5
            int r1 = r1 + 1
            goto L_0x0083
        L_0x0094:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        L_0x0097:
            r6.g = r2
            r1 = r0
            goto L_0x0025
        L_0x009b:
            r6.r = r2
            r6.r()
        L_0x00a0:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.app.n.d():boolean");
    }

    /* access modifiers changed from: package-private */
    public final ArrayList e() {
        ArrayList arrayList = null;
        if (this.f342a != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= this.f342a.size()) {
                    break;
                }
                Fragment fragment = (Fragment) this.f342a.get(i3);
                if (fragment != null && fragment.B) {
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.add(fragment);
                    fragment.C = true;
                    fragment.j = fragment.i != null ? fragment.i.f : -1;
                }
                i2 = i3 + 1;
            }
        }
        return arrayList;
    }

    public final void e(Fragment fragment, int i2, int i3) {
        if (fragment.A) {
            fragment.A = false;
            if (!fragment.l) {
                if (this.b == null) {
                    this.b = new ArrayList();
                }
                if (this.b.contains(fragment)) {
                    throw new IllegalStateException("Fragment already added: " + fragment);
                }
                this.b.add(fragment);
                fragment.l = true;
                a(fragment, this.c, i2, i3, false);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final Parcelable f() {
        int[] iArr;
        int size;
        int size2;
        boolean z;
        BackStackState[] backStackStateArr = null;
        d();
        if (d) {
            this.p = true;
        }
        if (this.f342a == null || this.f342a.size() <= 0) {
            return null;
        }
        int size3 = this.f342a.size();
        FragmentState[] fragmentStateArr = new FragmentState[size3];
        int i2 = 0;
        boolean z2 = false;
        while (i2 < size3) {
            Fragment fragment = (Fragment) this.f342a.get(i2);
            if (fragment != null) {
                if (fragment.f < 0) {
                    a(new IllegalStateException("Failure saving state: active " + fragment + " has cleared index: " + fragment.f));
                }
                FragmentState fragmentState = new FragmentState(fragment);
                fragmentStateArr[i2] = fragmentState;
                if (fragment.f328a <= 0 || fragmentState.f331a != null) {
                    fragmentState.f331a = fragment.d;
                } else {
                    fragmentState.f331a = e(fragment);
                    if (fragment.i != null) {
                        if (fragment.i.f < 0) {
                            a(new IllegalStateException("Failure saving state: " + fragment + " has target not in fragment manager: " + fragment.i));
                        }
                        if (fragmentState.f331a == null) {
                            fragmentState.f331a = new Bundle();
                        }
                        a(fragmentState.f331a, "android:target_state", fragment.i);
                        if (fragment.k != 0) {
                            fragmentState.f331a.putInt("android:target_req_state", fragment.k);
                            z = true;
                        }
                    }
                }
                z = true;
            } else {
                z = z2;
            }
            i2++;
            z2 = z;
        }
        if (!z2) {
            return null;
        }
        if (this.b == null || (size2 = this.b.size()) <= 0) {
            iArr = null;
        } else {
            iArr = new int[size2];
            for (int i3 = 0; i3 < size2; i3++) {
                iArr[i3] = ((Fragment) this.b.get(i3)).f;
                if (iArr[i3] < 0) {
                    a(new IllegalStateException("Failure saving state: active " + this.b.get(i3) + " has cleared index: " + iArr[i3]));
                }
            }
        }
        if (this.i != null && (size = this.i.size()) > 0) {
            backStackStateArr = new BackStackState[size];
            for (int i4 = 0; i4 < size; i4++) {
                backStackStateArr[i4] = new BackStackState((b) this.i.get(i4));
            }
        }
        FragmentManagerState fragmentManagerState = new FragmentManagerState();
        fragmentManagerState.f330a = fragmentStateArr;
        fragmentManagerState.b = iArr;
        fragmentManagerState.c = backStackStateArr;
        return fragmentManagerState;
    }

    public final void g() {
        this.p = false;
    }

    public final void h() {
        this.p = false;
        d(1);
    }

    public final void i() {
        this.p = false;
        d(2);
    }

    public final void j() {
        this.p = false;
        d(4);
    }

    public final void k() {
        this.p = false;
        d(5);
    }

    public final void l() {
        d(4);
    }

    public final void m() {
        this.p = true;
        d(3);
    }

    public final void n() {
        d(2);
    }

    public final void o() {
        d(1);
    }

    public final void p() {
        this.q = true;
        d();
        d(0);
        this.m = null;
        this.n = null;
        this.o = null;
    }

    public final void q() {
        if (this.b != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.b.size()) {
                    Fragment fragment = (Fragment) this.b.get(i3);
                    if (fragment != null) {
                        fragment.d_();
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder((int) NativeMapEngine.MAX_ICON_SIZE);
        sb.append("FragmentManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        if (this.o != null) {
            android.support.v4.b.a.a(this.o, sb);
        } else {
            android.support.v4.b.a.a(this.m, sb);
        }
        sb.append("}}");
        return sb.toString();
    }
}
