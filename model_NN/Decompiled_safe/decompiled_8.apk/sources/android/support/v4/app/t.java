package android.support.v4.app;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.view.u;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

public abstract class t extends u {

    /* renamed from: a  reason: collision with root package name */
    private final m f346a;
    private w b = null;
    private ArrayList c = new ArrayList();
    private ArrayList d = new ArrayList();
    private Fragment e = null;

    public t(m mVar) {
        this.f346a = mVar;
    }

    public final Parcelable a() {
        Bundle bundle = null;
        if (this.c.size() > 0) {
            bundle = new Bundle();
            Fragment.SavedState[] savedStateArr = new Fragment.SavedState[this.c.size()];
            this.c.toArray(savedStateArr);
            bundle.putParcelableArray("states", savedStateArr);
        }
        Bundle bundle2 = bundle;
        for (int i = 0; i < this.d.size(); i++) {
            Fragment fragment = (Fragment) this.d.get(i);
            if (fragment != null) {
                if (bundle2 == null) {
                    bundle2 = new Bundle();
                }
                this.f346a.a(bundle2, "f" + i, fragment);
            }
        }
        return bundle2;
    }

    public abstract Fragment a(int i);

    public Object a(ViewGroup viewGroup, int i) {
        Fragment.SavedState savedState;
        Fragment fragment;
        if (this.d.size() > i && (fragment = (Fragment) this.d.get(i)) != null) {
            return fragment;
        }
        if (this.b == null) {
            this.b = this.f346a.a();
        }
        Fragment a2 = a(i);
        if (this.c.size() > i && (savedState = (Fragment.SavedState) this.c.get(i)) != null) {
            a2.a(savedState);
        }
        while (this.d.size() <= i) {
            this.d.add(null);
        }
        a2.b(false);
        a2.c(false);
        this.d.set(i, a2);
        this.b.a(viewGroup.getId(), a2);
        return a2;
    }

    public final void a(Parcelable parcelable, ClassLoader classLoader) {
        if (parcelable != null) {
            Bundle bundle = (Bundle) parcelable;
            bundle.setClassLoader(classLoader);
            Parcelable[] parcelableArray = bundle.getParcelableArray("states");
            this.c.clear();
            this.d.clear();
            if (parcelableArray != null) {
                for (Parcelable parcelable2 : parcelableArray) {
                    this.c.add((Fragment.SavedState) parcelable2);
                }
            }
            for (String str : bundle.keySet()) {
                if (str.startsWith("f")) {
                    int parseInt = Integer.parseInt(str.substring(1));
                    Fragment a2 = this.f346a.a(bundle, str);
                    if (a2 != null) {
                        while (this.d.size() <= parseInt) {
                            this.d.add(null);
                        }
                        a2.b(false);
                        this.d.set(parseInt, a2);
                    } else {
                        Log.w("FragmentStatePagerAdapter", "Bad fragment at key " + str);
                    }
                }
            }
        }
    }

    public void a(ViewGroup viewGroup) {
        if (this.b != null) {
            this.b.c();
            this.b = null;
            this.f346a.b();
        }
    }

    public void a(ViewGroup viewGroup, int i, Object obj) {
        Fragment fragment = (Fragment) obj;
        if (this.b == null) {
            this.b = this.f346a.a();
        }
        while (this.c.size() <= i) {
            this.c.add(null);
        }
        this.c.set(i, this.f346a.a(fragment));
        this.d.set(i, null);
        this.b.b(fragment);
    }

    public final void a(Object obj) {
        Fragment fragment = (Fragment) obj;
        if (fragment != this.e) {
            if (this.e != null) {
                this.e.b(false);
                this.e.c(false);
            }
            if (fragment != null) {
                fragment.b(true);
                fragment.c(true);
            }
            this.e = fragment;
        }
    }

    public final boolean a(View view, Object obj) {
        return ((Fragment) obj).l() == view;
    }
}
