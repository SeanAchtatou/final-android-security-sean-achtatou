package android.support.v4.app;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TabWidget;
import java.util.ArrayList;

public final class FragmentTabHost extends TabHost implements TabHost.OnTabChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private final ArrayList f332a = new ArrayList();
    private FrameLayout b;
    private int c;
    private TabHost.OnTabChangeListener d;
    private v e;
    private boolean f;

    class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new u();

        /* renamed from: a  reason: collision with root package name */
        String f333a;

        private SavedState(Parcel parcel) {
            super(parcel);
            this.f333a = parcel.readString();
        }

        /* synthetic */ SavedState(Parcel parcel, byte b) {
            this(parcel);
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public String toString() {
            return "FragmentTabHost.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " curTab=" + this.f333a + "}";
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeString(this.f333a);
        }
    }

    public FragmentTabHost(Context context) {
        super(context, null);
        a(context, (AttributeSet) null);
    }

    public FragmentTabHost(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, attributeSet);
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [android.content.Context, android.support.v4.app.m] */
    private w a(String str, w wVar) {
        ? r3 = 0;
        int i = 0;
        v vVar = r3;
        while (i < this.f332a.size()) {
            v vVar2 = (v) this.f332a.get(i);
            if (!vVar2.f347a.equals(str)) {
                vVar2 = vVar;
            }
            i++;
            vVar = vVar2;
        }
        if (vVar == null) {
            throw new IllegalStateException("No tab known for tag " + str);
        }
        if (this.e != vVar) {
            if (wVar == null) {
                wVar = r3.a();
            }
            if (!(this.e == null || this.e.d == null)) {
                wVar.e(this.e.d);
            }
            if (vVar != null) {
                if (vVar.d == null) {
                    Fragment unused = vVar.d = Fragment.a((Context) r3, vVar.b.getName(), vVar.c);
                    wVar.a(this.c, vVar.d, vVar.f347a);
                } else {
                    wVar.f(vVar.d);
                }
            }
            this.e = vVar;
        }
        return wVar;
    }

    private void a(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, new int[]{16842995}, 0, 0);
        this.c = obtainStyledAttributes.getResourceId(0, 0);
        obtainStyledAttributes.recycle();
        super.setOnTabChangedListener(this);
        if (findViewById(16908307) == null) {
            LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setOrientation(1);
            addView(linearLayout, new FrameLayout.LayoutParams(-1, -1));
            TabWidget tabWidget = new TabWidget(context);
            tabWidget.setId(16908307);
            tabWidget.setOrientation(0);
            linearLayout.addView(tabWidget, new LinearLayout.LayoutParams(-1, -2, 0.0f));
            FrameLayout frameLayout = new FrameLayout(context);
            frameLayout.setId(16908305);
            linearLayout.addView(frameLayout, new LinearLayout.LayoutParams(0, 0, 0.0f));
            FrameLayout frameLayout2 = new FrameLayout(context);
            this.b = frameLayout2;
            this.b.setId(this.c);
            linearLayout.addView(frameLayout2, new LinearLayout.LayoutParams(-1, 0, 1.0f));
        }
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:19:0x0042 */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:21:0x0042 */
    /* JADX WARN: Type inference failed for: r1v0 */
    /* JADX WARN: Type inference failed for: r1v1, types: [android.support.v4.app.w] */
    /* JADX WARN: Type inference failed for: r1v2 */
    /* JADX WARN: Type inference failed for: r1v3, types: [android.support.v4.app.w] */
    /* JADX WARN: Type inference failed for: r1v5 */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Multi-variable type inference failed */
    protected final void onAttachedToWindow() {
        /*
            r6 = this;
            r3 = 0
            super.onAttachedToWindow()
            java.lang.String r4 = r6.getCurrentTabTag()
            r0 = 0
            r2 = r0
            r1 = r3
        L_0x000b:
            java.util.ArrayList r0 = r6.f332a
            int r0 = r0.size()
            if (r2 >= r0) goto L_0x0054
            java.util.ArrayList r0 = r6.f332a
            java.lang.Object r0 = r0.get(r2)
            android.support.v4.app.v r0 = (android.support.v4.app.v) r0
            java.lang.String r5 = r0.f347a
            android.support.v4.app.Fragment r5 = r3.a(r5)
            android.support.v4.app.Fragment unused = r0.d = r5
            android.support.v4.app.Fragment r5 = r0.d
            if (r5 == 0) goto L_0x0042
            android.support.v4.app.Fragment r5 = r0.d
            boolean r5 = r5.f()
            if (r5 != 0) goto L_0x0042
            java.lang.String r5 = r0.f347a
            boolean r5 = r5.equals(r4)
            if (r5 == 0) goto L_0x0046
            r6.e = r0
        L_0x0042:
            int r0 = r2 + 1
            r2 = r0
            goto L_0x000b
        L_0x0046:
            if (r1 != 0) goto L_0x004c
            android.support.v4.app.w r1 = r3.a()
        L_0x004c:
            android.support.v4.app.Fragment r0 = r0.d
            r1.e(r0)
            goto L_0x0042
        L_0x0054:
            r0 = 1
            r6.f = r0
            android.support.v4.app.w r0 = r6.a(r4, r1)
            if (r0 == 0) goto L_0x0063
            r0.b()
            r3.b()
        L_0x0063:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.app.FragmentTabHost.onAttachedToWindow():void");
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.f = false;
    }

    /* access modifiers changed from: protected */
    public final void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        setCurrentTabByTag(savedState.f333a);
    }

    /* access modifiers changed from: protected */
    public final Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f333a = getCurrentTabTag();
        return savedState;
    }

    public final void onTabChanged(String str) {
        w a2;
        if (this.f && (a2 = a(str, (w) null)) != null) {
            a2.b();
        }
        if (this.d != null) {
            this.d.onTabChanged(str);
        }
    }

    public final void setOnTabChangedListener(TabHost.OnTabChangeListener onTabChangeListener) {
        this.d = onTabChangeListener;
    }

    @Deprecated
    public final void setup() {
        throw new IllegalStateException("Must call setup() that takes a Context and FragmentManager");
    }
}
