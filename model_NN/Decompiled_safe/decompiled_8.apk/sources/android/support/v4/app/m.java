package android.support.v4.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;

public abstract class m {
    public abstract Fragment.SavedState a(Fragment fragment);

    public abstract Fragment a(Bundle bundle, String str);

    public abstract Fragment a(String str);

    public abstract w a();

    public abstract void a(Bundle bundle, String str, Fragment fragment);

    public abstract boolean b();
}
