package android.support.v4.app;

import android.os.Parcel;
import android.os.Parcelable;

final class s implements Parcelable.Creator {
    s() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new FragmentState(parcel);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new FragmentState[i];
    }
}
