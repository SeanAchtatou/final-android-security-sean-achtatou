package android.support.v4.app;

public abstract class w {
    public abstract w a();

    public abstract w a(int i, Fragment fragment);

    public abstract w a(int i, Fragment fragment, String str);

    public abstract w a(Fragment fragment);

    public abstract int b();

    public abstract w b(Fragment fragment);

    public abstract int c();

    public abstract w c(Fragment fragment);

    public abstract w d(Fragment fragment);

    public abstract w e(Fragment fragment);

    public abstract w f(Fragment fragment);
}
