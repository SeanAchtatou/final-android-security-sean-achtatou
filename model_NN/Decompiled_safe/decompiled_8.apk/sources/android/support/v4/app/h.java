package android.support.v4.app;

import android.os.Handler;
import android.os.Message;

final class h extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ g f338a;

    h(g gVar) {
        this.f338a = gVar;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                if (this.f338a.c) {
                    this.f338a.a(false);
                    return;
                }
                return;
            case 2:
                this.f338a.e_();
                this.f338a.b.d();
                return;
            default:
                super.handleMessage(message);
                return;
        }
    }
}
