package android.support.v4.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.c.c;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import com.mapabc.minimap.map.vmap.NativeMapEngine;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

public class g extends Activity {

    /* renamed from: a  reason: collision with root package name */
    final Handler f337a = new h(this);
    final n b = new n();
    boolean c;
    boolean d;
    private l e = new i(this);
    private boolean f;
    private boolean g;
    private boolean h;
    private boolean i;
    private boolean j;
    private HashMap k;
    private y l;

    private static String a(View view) {
        String str;
        char c2 = 'F';
        char c3 = '.';
        StringBuilder sb = new StringBuilder((int) NativeMapEngine.MAX_ICON_SIZE);
        sb.append(view.getClass().getName());
        sb.append('{');
        sb.append(Integer.toHexString(System.identityHashCode(view)));
        sb.append(' ');
        switch (view.getVisibility()) {
            case 0:
                sb.append('V');
                break;
            case 4:
                sb.append('I');
                break;
            case 8:
                sb.append('G');
                break;
            default:
                sb.append('.');
                break;
        }
        sb.append(view.isFocusable() ? 'F' : '.');
        sb.append(view.isEnabled() ? 'E' : '.');
        sb.append(view.willNotDraw() ? '.' : 'D');
        sb.append(view.isHorizontalScrollBarEnabled() ? 'H' : '.');
        sb.append(view.isVerticalScrollBarEnabled() ? 'V' : '.');
        sb.append(view.isClickable() ? 'C' : '.');
        sb.append(view.isLongClickable() ? 'L' : '.');
        sb.append(' ');
        if (!view.isFocused()) {
            c2 = '.';
        }
        sb.append(c2);
        sb.append(view.isSelected() ? 'S' : '.');
        if (view.isPressed()) {
            c3 = 'P';
        }
        sb.append(c3);
        sb.append(' ');
        sb.append(view.getLeft());
        sb.append(',');
        sb.append(view.getTop());
        sb.append('-');
        sb.append(view.getRight());
        sb.append(',');
        sb.append(view.getBottom());
        int id = view.getId();
        if (id != -1) {
            sb.append(" #");
            sb.append(Integer.toHexString(id));
            Resources resources = view.getResources();
            if (!(id == 0 || resources == null)) {
                switch (-16777216 & id) {
                    case 16777216:
                        str = "android";
                        String resourceTypeName = resources.getResourceTypeName(id);
                        String resourceEntryName = resources.getResourceEntryName(id);
                        sb.append(" ");
                        sb.append(str);
                        sb.append(":");
                        sb.append(resourceTypeName);
                        sb.append("/");
                        sb.append(resourceEntryName);
                        break;
                    case 2130706432:
                        str = "app";
                        String resourceTypeName2 = resources.getResourceTypeName(id);
                        String resourceEntryName2 = resources.getResourceEntryName(id);
                        sb.append(" ");
                        sb.append(str);
                        sb.append(":");
                        sb.append(resourceTypeName2);
                        sb.append("/");
                        sb.append(resourceEntryName2);
                        break;
                    default:
                        try {
                            str = resources.getResourcePackageName(id);
                            String resourceTypeName22 = resources.getResourceTypeName(id);
                            String resourceEntryName22 = resources.getResourceEntryName(id);
                            sb.append(" ");
                            sb.append(str);
                            sb.append(":");
                            sb.append(resourceTypeName22);
                            sb.append("/");
                            sb.append(resourceEntryName22);
                            break;
                        } catch (Resources.NotFoundException e2) {
                            break;
                        }
                }
            }
        }
        sb.append("}");
        return sb.toString();
    }

    private void a(String str, PrintWriter printWriter, View view) {
        ViewGroup viewGroup;
        int childCount;
        printWriter.print(str);
        if (view == null) {
            printWriter.println("null");
            return;
        }
        printWriter.println(a(view));
        if ((view instanceof ViewGroup) && (childCount = (viewGroup = (ViewGroup) view).getChildCount()) > 0) {
            String str2 = str + "  ";
            for (int i2 = 0; i2 < childCount; i2++) {
                a(str2, printWriter, viewGroup.getChildAt(i2));
            }
        }
    }

    public static void f_() {
    }

    public void a(Fragment fragment, Intent intent, int i2) {
        if (i2 == -1) {
            super.startActivityForResult(intent, -1);
        } else if ((-65536 & i2) != 0) {
            throw new IllegalArgumentException("Can only use lower 16 bits for requestCode");
        } else {
            super.startActivityForResult(intent, ((fragment.f + 1) << 16) + (65535 & i2));
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        y yVar;
        if (this.k != null && (yVar = (y) this.k.get(str)) != null && !yVar.c) {
            yVar.d();
            this.k.remove(str);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z) {
        if (!this.h) {
            this.h = true;
            this.d = z;
            this.f337a.removeMessages(1);
            if (this.j) {
                this.j = false;
                if (this.l != null) {
                    if (!this.d) {
                        this.l.b();
                    } else {
                        this.l.c();
                    }
                }
            }
            this.b.n();
        }
    }

    /* access modifiers changed from: package-private */
    public final y b(String str) {
        if (this.k == null) {
            this.k = new HashMap();
        }
        y yVar = (y) this.k.get(str);
        if (yVar != null) {
            yVar.a(this);
        }
        return yVar;
    }

    public final m c() {
        return this.b;
    }

    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        int i2 = Build.VERSION.SDK_INT;
        printWriter.print(str);
        printWriter.print("Local FragmentActivity ");
        printWriter.print(Integer.toHexString(System.identityHashCode(this)));
        printWriter.println(" State:");
        String str2 = str + "  ";
        printWriter.print(str2);
        printWriter.print("mCreated=");
        printWriter.print(this.f);
        printWriter.print("mResumed=");
        printWriter.print(this.g);
        printWriter.print(" mStopped=");
        printWriter.print(this.c);
        printWriter.print(" mReallyStopped=");
        printWriter.println(this.h);
        printWriter.print(str2);
        printWriter.print("mLoadersStarted=");
        printWriter.println(this.j);
        if (this.l != null) {
            printWriter.print(str);
            printWriter.print("Loader Manager ");
            printWriter.print(Integer.toHexString(System.identityHashCode(this.l)));
            printWriter.println(":");
            y yVar = this.l;
            str + "  ";
            y.e();
        }
        this.b.a(str, fileDescriptor, printWriter, strArr);
        printWriter.print(str);
        printWriter.println("View Hierarchy:");
        a(str + "  ", printWriter, getWindow().getDecorView());
    }

    /* access modifiers changed from: protected */
    public final void e_() {
        this.b.k();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        this.b.g();
        int i4 = i2 >> 16;
        if (i4 != 0) {
            int i5 = i4 - 1;
            if (this.b.f342a == null || i5 < 0 || i5 >= this.b.f342a.size()) {
                Log.w("FragmentActivity", "Activity result fragment index out of range: 0x" + Integer.toHexString(i2));
                return;
            }
            Fragment fragment = (Fragment) this.b.f342a.get(i5);
            if (fragment == null) {
                Log.w("FragmentActivity", "Activity result no fragment exists for index: 0x" + Integer.toHexString(i2));
            } else {
                fragment.a(65535 & i2, i3, intent);
            }
        } else {
            super.onActivityResult(i2, i3, intent);
        }
    }

    public void onBackPressed() {
        if (!this.b.c()) {
            finish();
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.b.a(configuration);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        this.b.a(this, this.e, (Fragment) null);
        if (getLayoutInflater().getFactory() == null) {
            getLayoutInflater().setFactory(this);
        }
        super.onCreate(bundle);
        k kVar = (k) getLastNonConfigurationInstance();
        if (kVar != null) {
            this.k = kVar.b;
        }
        if (bundle != null) {
            this.b.a(bundle.getParcelable("android:support:fragments"), kVar != null ? kVar.f341a : null);
        }
        this.b.h();
    }

    public boolean onCreatePanelMenu(int i2, Menu menu) {
        if (i2 != 0) {
            return super.onCreatePanelMenu(i2, menu);
        }
        boolean onCreatePanelMenu = super.onCreatePanelMenu(i2, menu) | this.b.a(menu, getMenuInflater());
        if (Build.VERSION.SDK_INT >= 11) {
            return onCreatePanelMenu;
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.n.a(android.support.v4.app.Fragment, boolean):void
     arg types: [android.support.v4.app.Fragment, int]
     candidates:
      android.support.v4.app.n.a(float, float):android.view.animation.Animation
      android.support.v4.app.n.a(int, android.support.v4.app.b):void
      android.support.v4.app.n.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.n.a(android.os.Parcelable, java.util.ArrayList):void
      android.support.v4.app.n.a(java.lang.Runnable, boolean):void
      android.support.v4.app.n.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.m.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.n.a(android.support.v4.app.Fragment, boolean):void */
    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        Fragment fragment = null;
        if (!"fragment".equals(str)) {
            return super.onCreateView(str, context, attributeSet);
        }
        String attributeValue = attributeSet.getAttributeValue(null, "class");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, j.f340a);
        if (attributeValue == null) {
            attributeValue = obtainStyledAttributes.getString(0);
        }
        int resourceId = obtainStyledAttributes.getResourceId(1, -1);
        String string = obtainStyledAttributes.getString(2);
        obtainStyledAttributes.recycle();
        if (resourceId != -1) {
            fragment = this.b.a(resourceId);
        }
        if (fragment == null && string != null) {
            fragment = this.b.a(string);
        }
        if (fragment == null) {
            fragment = this.b.a(0);
        }
        if (fragment == null) {
            Fragment a2 = Fragment.a(this, attributeValue);
            a2.o = true;
            a2.w = resourceId != 0 ? resourceId : 0;
            a2.x = 0;
            a2.y = string;
            a2.p = true;
            a2.s = this.b;
            Bundle bundle = a2.d;
            a2.i();
            this.b.a(a2, true);
            fragment = a2;
        } else if (fragment.p) {
            throw new IllegalArgumentException(attributeSet.getPositionDescription() + ": Duplicate id 0x" + Integer.toHexString(resourceId) + ", tag " + string + ", or parent id 0x" + Integer.toHexString(0) + " with another fragment for " + attributeValue);
        } else {
            fragment.p = true;
            if (!fragment.C) {
                Bundle bundle2 = fragment.d;
                fragment.i();
            }
            this.b.c(fragment);
        }
        if (fragment.G == null) {
            throw new IllegalStateException("Fragment " + attributeValue + " did not create a view.");
        }
        if (resourceId != 0) {
            fragment.G.setId(resourceId);
        }
        if (fragment.G.getTag() == null) {
            fragment.G.setTag(string);
        }
        return fragment.G;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        a(false);
        this.b.p();
        if (this.l != null) {
            this.l.d();
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (Build.VERSION.SDK_INT >= 5 || i2 != 4 || keyEvent.getRepeatCount() != 0) {
            return super.onKeyDown(i2, keyEvent);
        }
        onBackPressed();
        return true;
    }

    public void onLowMemory() {
        super.onLowMemory();
        this.b.q();
    }

    public boolean onMenuItemSelected(int i2, MenuItem menuItem) {
        if (super.onMenuItemSelected(i2, menuItem)) {
            return true;
        }
        switch (i2) {
            case 0:
                return this.b.a(menuItem);
            case 6:
                return this.b.b(menuItem);
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.b.g();
    }

    public void onPanelClosed(int i2, Menu menu) {
        switch (i2) {
            case 0:
                this.b.b(menu);
                break;
        }
        super.onPanelClosed(i2, menu);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.g = false;
        if (this.f337a.hasMessages(2)) {
            this.f337a.removeMessages(2);
            this.b.k();
        }
        this.b.l();
    }

    /* access modifiers changed from: protected */
    public void onPostResume() {
        super.onPostResume();
        this.f337a.removeMessages(2);
        this.b.k();
        this.b.d();
    }

    public boolean onPreparePanel(int i2, View view, Menu menu) {
        return (i2 != 0 || menu == null) ? super.onPreparePanel(i2, view, menu) : super.onPreparePanel(i2, view, menu) | this.b.a(menu);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.f337a.sendEmptyMessage(2);
        this.g = true;
        this.b.d();
    }

    public final Object onRetainNonConfigurationInstance() {
        boolean z;
        if (this.c) {
            a(true);
        }
        ArrayList e2 = this.b.e();
        if (this.k != null) {
            y[] yVarArr = new y[this.k.size()];
            this.k.values().toArray(yVarArr);
            z = false;
            for (y yVar : yVarArr) {
                if (yVar.c) {
                    z = true;
                } else {
                    yVar.d();
                    this.k.remove(yVar.f348a);
                }
            }
        } else {
            z = false;
        }
        if (e2 == null && !z) {
            return null;
        }
        k kVar = new k();
        kVar.f341a = e2;
        kVar.b = this.k;
        return kVar;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        Parcelable f2 = this.b.f();
        if (f2 != null) {
            bundle.putParcelable("android:support:fragments", f2);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.c = false;
        this.h = false;
        this.f337a.removeMessages(1);
        if (!this.f) {
            this.f = true;
            this.b.i();
        }
        this.b.g();
        this.b.d();
        if (!this.j) {
            this.j = true;
            if (this.l != null) {
                this.l.a();
            } else if (!this.i) {
                boolean z = this.j;
                this.l = b(null);
                if (this.l != null && !this.l.b) {
                    this.l.a();
                }
            }
            this.i = true;
        }
        this.b.j();
        if (this.k != null) {
            y[] yVarArr = new y[this.k.size()];
            this.k.values().toArray(yVarArr);
            for (y yVar : yVarArr) {
                if (yVar.c) {
                    yVar.c = false;
                    c.a();
                }
                c.a();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.c = true;
        this.f337a.sendEmptyMessage(1);
        this.b.m();
    }

    public void startActivityForResult(Intent intent, int i2) {
        if (i2 == -1 || (-65536 & i2) == 0) {
            super.startActivityForResult(intent, i2);
            return;
        }
        throw new IllegalArgumentException("Can only use lower 16 bits for requestCode");
    }
}
