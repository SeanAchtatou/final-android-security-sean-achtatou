package android.support.v4.app;

import com.immomo.momo.R;
import com.mapabc.minimap.map.vmap.NativeMapEngine;
import java.io.PrintWriter;
import java.util.ArrayList;

final class b extends w implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    a f335a;
    int b;
    int c;
    int d;
    boolean e;
    String f;
    int g = -1;
    int h;
    CharSequence i;
    int j;
    CharSequence k;
    private n l;
    private a m;
    private int n;
    private int o;
    private boolean p;

    public b(n nVar) {
        this.l = nVar;
    }

    private int a(boolean z) {
        if (this.p) {
            throw new IllegalStateException("commit already called");
        }
        this.p = true;
        if (this.e) {
            this.g = this.l.a(this);
        } else {
            this.g = -1;
        }
        this.l.a(this, z);
        return this.g;
    }

    private void a(int i2, Fragment fragment, String str, int i3) {
        fragment.s = this.l;
        if (str != null) {
            if (fragment.y == null || str.equals(fragment.y)) {
                fragment.y = str;
            } else {
                throw new IllegalStateException("Can't change tag of fragment " + fragment + ": was " + fragment.y + " now " + str);
            }
        }
        if (i2 != 0) {
            if (fragment.w == 0 || fragment.w == i2) {
                fragment.w = i2;
                fragment.x = i2;
            } else {
                throw new IllegalStateException("Can't change container ID of fragment " + fragment + ": was " + fragment.w + " now " + i2);
            }
        }
        a aVar = new a();
        aVar.c = i3;
        aVar.d = fragment;
        a(aVar);
    }

    public final w a() {
        this.n = R.anim.fragment_fadein;
        this.o = R.anim.fragment_fadeout;
        return this;
    }

    public final w a(int i2, Fragment fragment) {
        a(i2, fragment, null, 1);
        return this;
    }

    public final w a(int i2, Fragment fragment, String str) {
        a(i2, fragment, str, 1);
        return this;
    }

    public final w a(Fragment fragment) {
        a(R.id.layout_content, fragment, null, 2);
        return this;
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2) {
        if (this.e) {
            for (a aVar = this.f335a; aVar != null; aVar = aVar.f334a) {
                if (aVar.d != null) {
                    aVar.d.r += i2;
                }
                if (aVar.i != null) {
                    for (int size = aVar.i.size() - 1; size >= 0; size--) {
                        ((Fragment) aVar.i.get(size)).r += i2;
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(a aVar) {
        if (this.f335a == null) {
            this.m = aVar;
            this.f335a = aVar;
        } else {
            aVar.b = this.m;
            this.m.f334a = aVar;
            this.m = aVar;
        }
        aVar.e = this.n;
        aVar.f = this.o;
        aVar.g = 0;
        aVar.h = 0;
        this.b++;
    }

    public final void a(String str, PrintWriter printWriter) {
        String str2;
        printWriter.print(str);
        printWriter.print("mName=");
        printWriter.print(this.f);
        printWriter.print(" mIndex=");
        printWriter.print(this.g);
        printWriter.print(" mCommitted=");
        printWriter.println(this.p);
        if (this.c != 0) {
            printWriter.print(str);
            printWriter.print("mTransition=#");
            printWriter.print(Integer.toHexString(this.c));
            printWriter.print(" mTransitionStyle=#");
            printWriter.println(Integer.toHexString(this.d));
        }
        if (!(this.n == 0 && this.o == 0)) {
            printWriter.print(str);
            printWriter.print("mEnterAnim=#");
            printWriter.print(Integer.toHexString(this.n));
            printWriter.print(" mExitAnim=#");
            printWriter.println(Integer.toHexString(this.o));
        }
        if (!(this.h == 0 && this.i == null)) {
            printWriter.print(str);
            printWriter.print("mBreadCrumbTitleRes=#");
            printWriter.print(Integer.toHexString(this.h));
            printWriter.print(" mBreadCrumbTitleText=");
            printWriter.println(this.i);
        }
        if (!(this.j == 0 && this.k == null)) {
            printWriter.print(str);
            printWriter.print("mBreadCrumbShortTitleRes=#");
            printWriter.print(Integer.toHexString(this.j));
            printWriter.print(" mBreadCrumbShortTitleText=");
            printWriter.println(this.k);
        }
        if (this.f335a != null) {
            printWriter.print(str);
            printWriter.println("Operations:");
            String str3 = str + "    ";
            int i2 = 0;
            a aVar = this.f335a;
            while (aVar != null) {
                switch (aVar.c) {
                    case 0:
                        str2 = "NULL";
                        break;
                    case 1:
                        str2 = "ADD";
                        break;
                    case 2:
                        str2 = "REPLACE";
                        break;
                    case 3:
                        str2 = "REMOVE";
                        break;
                    case 4:
                        str2 = "HIDE";
                        break;
                    case 5:
                        str2 = "SHOW";
                        break;
                    case 6:
                        str2 = "DETACH";
                        break;
                    case 7:
                        str2 = "ATTACH";
                        break;
                    default:
                        str2 = "cmd=" + aVar.c;
                        break;
                }
                printWriter.print(str);
                printWriter.print("  Op #");
                printWriter.print(i2);
                printWriter.print(": ");
                printWriter.print(str2);
                printWriter.print(" ");
                printWriter.println(aVar.d);
                if (!(aVar.e == 0 && aVar.f == 0)) {
                    printWriter.print(str);
                    printWriter.print("enterAnim=#");
                    printWriter.print(Integer.toHexString(aVar.e));
                    printWriter.print(" exitAnim=#");
                    printWriter.println(Integer.toHexString(aVar.f));
                }
                if (!(aVar.g == 0 && aVar.h == 0)) {
                    printWriter.print(str);
                    printWriter.print("popEnterAnim=#");
                    printWriter.print(Integer.toHexString(aVar.g));
                    printWriter.print(" popExitAnim=#");
                    printWriter.println(Integer.toHexString(aVar.h));
                }
                if (aVar.i != null && aVar.i.size() > 0) {
                    for (int i3 = 0; i3 < aVar.i.size(); i3++) {
                        printWriter.print(str3);
                        if (aVar.i.size() == 1) {
                            printWriter.print("Removed: ");
                        } else {
                            if (i3 == 0) {
                                printWriter.println("Removed:");
                            }
                            printWriter.print(str3);
                            printWriter.print("  #");
                            printWriter.print(i3);
                            printWriter.print(": ");
                        }
                        printWriter.println(aVar.i.get(i3));
                    }
                }
                aVar = aVar.f334a;
                i2++;
            }
        }
    }

    public final int b() {
        return a(false);
    }

    public final w b(Fragment fragment) {
        a aVar = new a();
        aVar.c = 3;
        aVar.d = fragment;
        a(aVar);
        return this;
    }

    public final int c() {
        return a(true);
    }

    public final w c(Fragment fragment) {
        a aVar = new a();
        aVar.c = 4;
        aVar.d = fragment;
        a(aVar);
        return this;
    }

    public final w d(Fragment fragment) {
        a aVar = new a();
        aVar.c = 5;
        aVar.d = fragment;
        a(aVar);
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.n.a(android.support.v4.app.Fragment, boolean):void
     arg types: [android.support.v4.app.Fragment, int]
     candidates:
      android.support.v4.app.n.a(float, float):android.view.animation.Animation
      android.support.v4.app.n.a(int, android.support.v4.app.b):void
      android.support.v4.app.n.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.n.a(android.os.Parcelable, java.util.ArrayList):void
      android.support.v4.app.n.a(java.lang.Runnable, boolean):void
      android.support.v4.app.n.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.m.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.n.a(android.support.v4.app.Fragment, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.n.a(int, int, int, boolean):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.app.n.a(float, float, float, float):android.view.animation.Animation
      android.support.v4.app.n.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation
      android.support.v4.app.n.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.n.a(int, int, int, boolean):void */
    public final void d() {
        a(-1);
        for (a aVar = this.m; aVar != null; aVar = aVar.b) {
            switch (aVar.c) {
                case 1:
                    Fragment fragment = aVar.d;
                    fragment.E = aVar.h;
                    this.l.a(fragment, n.c(this.c), this.d);
                    break;
                case 2:
                    Fragment fragment2 = aVar.d;
                    if (fragment2 != null) {
                        fragment2.E = aVar.h;
                        this.l.a(fragment2, n.c(this.c), this.d);
                    }
                    if (aVar.i == null) {
                        break;
                    } else {
                        for (int i2 = 0; i2 < aVar.i.size(); i2++) {
                            Fragment fragment3 = (Fragment) aVar.i.get(i2);
                            fragment3.E = aVar.g;
                            this.l.a(fragment3, false);
                        }
                        break;
                    }
                case 3:
                    Fragment fragment4 = aVar.d;
                    fragment4.E = aVar.g;
                    this.l.a(fragment4, false);
                    break;
                case 4:
                    Fragment fragment5 = aVar.d;
                    fragment5.E = aVar.g;
                    this.l.c(fragment5, n.c(this.c), this.d);
                    break;
                case 5:
                    Fragment fragment6 = aVar.d;
                    fragment6.E = aVar.h;
                    this.l.b(fragment6, n.c(this.c), this.d);
                    break;
                case 6:
                    Fragment fragment7 = aVar.d;
                    fragment7.E = aVar.g;
                    this.l.e(fragment7, n.c(this.c), this.d);
                    break;
                case 7:
                    Fragment fragment8 = aVar.d;
                    fragment8.E = aVar.g;
                    this.l.d(fragment8, n.c(this.c), this.d);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown cmd: " + aVar.c);
            }
        }
        this.l.a(this.l.c, n.c(this.c), this.d, true);
        if (this.g >= 0) {
            this.l.b(this.g);
            this.g = -1;
        }
    }

    public final w e(Fragment fragment) {
        a aVar = new a();
        aVar.c = 6;
        aVar.d = fragment;
        a(aVar);
        return this;
    }

    public final w f(Fragment fragment) {
        a aVar = new a();
        aVar.c = 7;
        aVar.d = fragment;
        a(aVar);
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.n.a(android.support.v4.app.Fragment, boolean):void
     arg types: [android.support.v4.app.Fragment, int]
     candidates:
      android.support.v4.app.n.a(float, float):android.view.animation.Animation
      android.support.v4.app.n.a(int, android.support.v4.app.b):void
      android.support.v4.app.n.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.n.a(android.os.Parcelable, java.util.ArrayList):void
      android.support.v4.app.n.a(java.lang.Runnable, boolean):void
      android.support.v4.app.n.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.m.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.n.a(android.support.v4.app.Fragment, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.n.a(int, int, int, boolean):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.app.n.a(float, float, float, float):android.view.animation.Animation
      android.support.v4.app.n.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation
      android.support.v4.app.n.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.n.a(int, int, int, boolean):void */
    public final void run() {
        Fragment fragment;
        if (!this.e || this.g >= 0) {
            a(1);
            for (a aVar = this.f335a; aVar != null; aVar = aVar.f334a) {
                switch (aVar.c) {
                    case 1:
                        Fragment fragment2 = aVar.d;
                        fragment2.E = aVar.e;
                        this.l.a(fragment2, false);
                        break;
                    case 2:
                        Fragment fragment3 = aVar.d;
                        if (this.l.b != null) {
                            fragment = fragment3;
                            for (int i2 = 0; i2 < this.l.b.size(); i2++) {
                                Fragment fragment4 = (Fragment) this.l.b.get(i2);
                                if (fragment == null || fragment4.x == fragment.x) {
                                    if (fragment4 == fragment) {
                                        fragment = null;
                                        aVar.d = null;
                                    } else {
                                        if (aVar.i == null) {
                                            aVar.i = new ArrayList();
                                        }
                                        aVar.i.add(fragment4);
                                        fragment4.E = aVar.f;
                                        if (this.e) {
                                            fragment4.r++;
                                        }
                                        this.l.a(fragment4, this.c, this.d);
                                    }
                                }
                            }
                        } else {
                            fragment = fragment3;
                        }
                        if (fragment == null) {
                            break;
                        } else {
                            fragment.E = aVar.e;
                            this.l.a(fragment, false);
                            break;
                        }
                    case 3:
                        Fragment fragment5 = aVar.d;
                        fragment5.E = aVar.f;
                        this.l.a(fragment5, this.c, this.d);
                        break;
                    case 4:
                        Fragment fragment6 = aVar.d;
                        fragment6.E = aVar.f;
                        this.l.b(fragment6, this.c, this.d);
                        break;
                    case 5:
                        Fragment fragment7 = aVar.d;
                        fragment7.E = aVar.e;
                        this.l.c(fragment7, this.c, this.d);
                        break;
                    case 6:
                        Fragment fragment8 = aVar.d;
                        fragment8.E = aVar.f;
                        this.l.d(fragment8, this.c, this.d);
                        break;
                    case 7:
                        Fragment fragment9 = aVar.d;
                        fragment9.E = aVar.e;
                        this.l.e(fragment9, this.c, this.d);
                        break;
                    default:
                        throw new IllegalArgumentException("Unknown cmd: " + aVar.c);
                }
            }
            this.l.a(this.l.c, this.c, this.d, true);
            if (this.e) {
                this.l.b(this);
                return;
            }
            return;
        }
        throw new IllegalStateException("addToBackStack() called after commit()");
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder((int) NativeMapEngine.MAX_ICON_SIZE);
        sb.append("BackStackEntry{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        if (this.g >= 0) {
            sb.append(" #");
            sb.append(this.g);
        }
        if (this.f != null) {
            sb.append(" ");
            sb.append(this.f);
        }
        sb.append("}");
        return sb.toString();
    }
}
