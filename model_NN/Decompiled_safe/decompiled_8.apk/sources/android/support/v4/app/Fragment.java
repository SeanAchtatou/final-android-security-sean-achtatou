package android.support.v4.app;

import android.app.Activity;
import android.content.ComponentCallbacks;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.b.a;
import android.support.v4.c.c;
import android.util.SparseArray;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import com.mapabc.minimap.map.vmap.NativeMapEngine;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

public class Fragment implements ComponentCallbacks, View.OnCreateContextMenuListener {
    private static final HashMap L = new HashMap();
    boolean A;
    boolean B;
    boolean C;
    boolean D;
    int E;
    ViewGroup F;
    View G;
    View H;
    boolean I;
    boolean J = true;
    y K;
    private boolean M = true;
    private boolean N;
    private boolean O;

    /* renamed from: a  reason: collision with root package name */
    int f328a = 0;
    View b;
    int c;
    Bundle d;
    SparseArray e;
    int f = -1;
    String g;
    Bundle h;
    Fragment i;
    int j = -1;
    int k;
    boolean l;
    boolean m;
    boolean n;
    boolean o;
    boolean p;
    boolean q;
    int r;
    n s;
    g t;
    n u;
    Fragment v;
    int w;
    int x;
    String y;
    boolean z;

    public class SavedState implements Parcelable {
        public static final Parcelable.Creator CREATOR = new f();

        /* renamed from: a  reason: collision with root package name */
        final Bundle f329a;

        SavedState(Bundle bundle) {
            this.f329a = bundle;
        }

        SavedState(Parcel parcel) {
            this.f329a = parcel.readBundle();
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeBundle(this.f329a);
        }
    }

    private void B() {
        this.u = new n();
        this.u.a(this.t, new d(this), this);
    }

    public static Fragment a(Context context, String str) {
        return a(context, str, (Bundle) null);
    }

    public static Fragment a(Context context, String str, Bundle bundle) {
        try {
            Class<?> cls = (Class) L.get(str);
            if (cls == null) {
                cls = context.getClassLoader().loadClass(str);
                L.put(str, cls);
            }
            Fragment fragment = (Fragment) cls.newInstance();
            if (bundle != null) {
                bundle.setClassLoader(fragment.getClass().getClassLoader());
                fragment.h = bundle;
            }
            return fragment;
        } catch (ClassNotFoundException e2) {
            throw new e("Unable to instantiate fragment " + str + ": make sure class name exists, is public, and has an empty constructor that is public", e2);
        } catch (InstantiationException e3) {
            throw new e("Unable to instantiate fragment " + str + ": make sure class name exists, is public, and has an empty constructor that is public", e3);
        } catch (IllegalAccessException e4) {
            throw new e("Unable to instantiate fragment " + str + ": make sure class name exists, is public, and has an empty constructor that is public", e4);
        }
    }

    public static void g() {
    }

    public static Animation j() {
        return null;
    }

    public static void k() {
    }

    public static void s() {
    }

    /* access modifiers changed from: package-private */
    public final void A() {
        if (this.u != null) {
            this.u.p();
        }
        this.D = false;
        p();
        if (!this.D) {
            throw new aa("Fragment " + this + " did not call through to super.onDestroy()");
        }
    }

    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return null;
    }

    public final String a(int i2) {
        return d().getString(i2);
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        if (this.e != null) {
            this.H.restoreHierarchyState(this.e);
            this.e = null;
        }
        this.D = false;
        this.D = true;
        if (!this.D) {
            throw new aa("Fragment " + this + " did not call through to super.onViewStateRestored()");
        }
    }

    public void a(int i2, int i3, Intent intent) {
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2, Fragment fragment) {
        this.f = i2;
        if (fragment != null) {
            this.g = fragment.g + ":" + this.f;
        } else {
            this.g = "android:fragment:" + this.f;
        }
    }

    public void a(Activity activity) {
        this.D = true;
    }

    public final void a(Intent intent) {
        if (this.t == null) {
            throw new IllegalStateException("Fragment " + this + " not attached to Activity");
        }
        this.t.a(this, intent, -1);
    }

    public final void a(Intent intent, int i2) {
        if (this.t == null) {
            throw new IllegalStateException("Fragment " + this + " not attached to Activity");
        }
        this.t.a(this, intent, i2);
    }

    /* access modifiers changed from: package-private */
    public final void a(Configuration configuration) {
        onConfigurationChanged(configuration);
        if (this.u != null) {
            this.u.a(configuration);
        }
    }

    public void a(Bundle bundle) {
        this.D = true;
    }

    public final void a(SavedState savedState) {
        if (this.f >= 0) {
            throw new IllegalStateException("Fragment already active");
        }
        this.d = (savedState == null || savedState.f329a == null) ? null : savedState.f329a;
    }

    public final void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.print(str);
        printWriter.print("mFragmentId=#");
        printWriter.print(Integer.toHexString(this.w));
        printWriter.print(" mContainerId=#");
        printWriter.print(Integer.toHexString(this.x));
        printWriter.print(" mTag=");
        printWriter.println(this.y);
        printWriter.print(str);
        printWriter.print("mState=");
        printWriter.print(this.f328a);
        printWriter.print(" mIndex=");
        printWriter.print(this.f);
        printWriter.print(" mWho=");
        printWriter.print(this.g);
        printWriter.print(" mBackStackNesting=");
        printWriter.println(this.r);
        printWriter.print(str);
        printWriter.print("mAdded=");
        printWriter.print(this.l);
        printWriter.print(" mRemoving=");
        printWriter.print(this.m);
        printWriter.print(" mResumed=");
        printWriter.print(this.n);
        printWriter.print(" mFromLayout=");
        printWriter.print(this.o);
        printWriter.print(" mInLayout=");
        printWriter.println(this.p);
        printWriter.print(str);
        printWriter.print("mHidden=");
        printWriter.print(this.z);
        printWriter.print(" mDetached=");
        printWriter.print(this.A);
        printWriter.print(" mMenuVisible=");
        printWriter.print(this.M);
        printWriter.print(" mHasMenu=");
        printWriter.println(false);
        printWriter.print(str);
        printWriter.print("mRetainInstance=");
        printWriter.print(this.B);
        printWriter.print(" mRetaining=");
        printWriter.print(this.C);
        printWriter.print(" mUserVisibleHint=");
        printWriter.println(this.J);
        if (this.s != null) {
            printWriter.print(str);
            printWriter.print("mFragmentManager=");
            printWriter.println(this.s);
        }
        if (this.t != null) {
            printWriter.print(str);
            printWriter.print("mActivity=");
            printWriter.println(this.t);
        }
        if (this.v != null) {
            printWriter.print(str);
            printWriter.print("mParentFragment=");
            printWriter.println(this.v);
        }
        if (this.h != null) {
            printWriter.print(str);
            printWriter.print("mArguments=");
            printWriter.println(this.h);
        }
        if (this.d != null) {
            printWriter.print(str);
            printWriter.print("mSavedFragmentState=");
            printWriter.println(this.d);
        }
        if (this.e != null) {
            printWriter.print(str);
            printWriter.print("mSavedViewState=");
            printWriter.println(this.e);
        }
        if (this.i != null) {
            printWriter.print(str);
            printWriter.print("mTarget=");
            printWriter.print(this.i);
            printWriter.print(" mTargetRequestCode=");
            printWriter.println(this.k);
        }
        if (this.E != 0) {
            printWriter.print(str);
            printWriter.print("mNextAnim=");
            printWriter.println(this.E);
        }
        if (this.F != null) {
            printWriter.print(str);
            printWriter.print("mContainer=");
            printWriter.println(this.F);
        }
        if (this.G != null) {
            printWriter.print(str);
            printWriter.print("mView=");
            printWriter.println(this.G);
        }
        if (this.H != null) {
            printWriter.print(str);
            printWriter.print("mInnerView=");
            printWriter.println(this.G);
        }
        if (this.b != null) {
            printWriter.print(str);
            printWriter.print("mAnimatingAway=");
            printWriter.println(this.b);
            printWriter.print(str);
            printWriter.print("mStateAfterAnimating=");
            printWriter.println(this.c);
        }
        if (this.K != null) {
            printWriter.print(str);
            printWriter.println("Loader Manager:");
            y yVar = this.K;
            str + "  ";
            y.e();
        }
        if (this.u != null) {
            printWriter.print(str);
            printWriter.println("Child " + this.u + ":");
            this.u.a(str + "  ", fileDescriptor, printWriter, strArr);
        }
    }

    public final void a(boolean z2) {
        if (!z2 || this.v == null) {
            this.B = z2;
            return;
        }
        throw new IllegalStateException("Can't retain fragements that are nested in other fragments");
    }

    /* access modifiers changed from: package-private */
    public final boolean a(Menu menu) {
        if (this.z || this.u == null) {
            return false;
        }
        return this.u.a(menu) | false;
    }

    /* access modifiers changed from: package-private */
    public final boolean a(Menu menu, MenuInflater menuInflater) {
        if (this.z || this.u == null) {
            return false;
        }
        return this.u.a(menu, menuInflater) | false;
    }

    /* access modifiers changed from: package-private */
    public final boolean a(MenuItem menuItem) {
        return !this.z && this.u != null && this.u.a(menuItem);
    }

    /* access modifiers changed from: package-private */
    public final View b(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        if (this.u != null) {
            this.u.g();
        }
        return a(layoutInflater, viewGroup);
    }

    public void b(Bundle bundle) {
        this.D = true;
    }

    /* access modifiers changed from: package-private */
    public final void b(Menu menu) {
        if (!this.z && this.u != null) {
            this.u.b(menu);
        }
    }

    public final void b(boolean z2) {
        if (this.M != z2) {
            this.M = z2;
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean b() {
        return this.r > 0;
    }

    /* access modifiers changed from: package-private */
    public final boolean b(MenuItem menuItem) {
        return !this.z && this.u != null && this.u.b(menuItem);
    }

    public final g c() {
        return this.t;
    }

    public void c(Bundle bundle) {
    }

    public final void c(boolean z2) {
        if (!this.J && z2 && this.f328a < 4) {
            this.s.b(this);
        }
        this.J = z2;
        this.I = !z2;
    }

    /* access modifiers changed from: package-private */
    public final void c_() {
        if (this.u != null) {
            this.u.g();
            this.u.d();
        }
        this.D = false;
        m();
        if (!this.D) {
            throw new aa("Fragment " + this + " did not call through to super.onResume()");
        } else if (this.u != null) {
            this.u.k();
            this.u.d();
        }
    }

    public final Resources d() {
        if (this.t != null) {
            return this.t.getResources();
        }
        throw new IllegalStateException("Fragment " + this + " not attached to Activity");
    }

    /* access modifiers changed from: package-private */
    public final void d(Bundle bundle) {
        Parcelable parcelable;
        if (this.u != null) {
            this.u.g();
        }
        this.D = false;
        a(bundle);
        if (!this.D) {
            throw new aa("Fragment " + this + " did not call through to super.onCreate()");
        } else if (bundle != null && (parcelable = bundle.getParcelable("android:support:fragments")) != null) {
            if (this.u == null) {
                B();
            }
            this.u.a(parcelable, (ArrayList) null);
            this.u.h();
        }
    }

    /* access modifiers changed from: package-private */
    public final void d_() {
        onLowMemory();
        if (this.u != null) {
            this.u.q();
        }
    }

    public final m e() {
        if (this.u == null) {
            B();
            if (this.f328a >= 5) {
                this.u.k();
            } else if (this.f328a >= 4) {
                this.u.j();
            } else if (this.f328a >= 2) {
                this.u.i();
            } else if (this.f328a > 0) {
                this.u.h();
            }
        }
        return this.u;
    }

    /* access modifiers changed from: package-private */
    public final void e(Bundle bundle) {
        if (this.u != null) {
            this.u.g();
        }
        this.D = false;
        b(bundle);
        if (!this.D) {
            throw new aa("Fragment " + this + " did not call through to super.onActivityCreated()");
        } else if (this.u != null) {
            this.u.i();
        }
    }

    public final boolean equals(Object obj) {
        return super.equals(obj);
    }

    /* access modifiers changed from: package-private */
    public final void f(Bundle bundle) {
        Parcelable f2;
        c(bundle);
        if (this.u != null && (f2 = this.u.f()) != null) {
            bundle.putParcelable("android:support:fragments", f2);
        }
    }

    public final boolean f() {
        return this.A;
    }

    public final LayoutInflater h() {
        return this.t.getLayoutInflater();
    }

    public final int hashCode() {
        return super.hashCode();
    }

    public final void i() {
        this.D = true;
    }

    public final View l() {
        return this.G;
    }

    public void m() {
        this.D = true;
    }

    public void n() {
        this.D = true;
    }

    public void o() {
        this.D = true;
    }

    public void onConfigurationChanged(Configuration configuration) {
        this.D = true;
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        this.t.onCreateContextMenu(contextMenu, view, contextMenuInfo);
    }

    public void onLowMemory() {
        this.D = true;
    }

    public void p() {
        this.D = true;
        if (!this.O) {
            this.O = true;
            g gVar = this.t;
            String str = this.g;
            boolean z2 = this.N;
            this.K = gVar.b(str);
        }
        if (this.K != null) {
            this.K.d();
        }
    }

    /* access modifiers changed from: package-private */
    public final void q() {
        this.f = -1;
        this.g = null;
        this.l = false;
        this.m = false;
        this.n = false;
        this.o = false;
        this.p = false;
        this.q = false;
        this.r = 0;
        this.s = null;
        this.t = null;
        this.w = 0;
        this.x = 0;
        this.y = null;
        this.z = false;
        this.A = false;
        this.C = false;
        this.K = null;
        this.N = false;
        this.O = false;
    }

    public void r() {
        this.D = true;
    }

    /* access modifiers changed from: package-private */
    public final void t() {
        if (this.u != null) {
            this.u.g();
            this.u.d();
        }
        this.D = false;
        this.D = true;
        if (!this.N) {
            this.N = true;
            if (!this.O) {
                this.O = true;
                g gVar = this.t;
                String str = this.g;
                boolean z2 = this.N;
                this.K = gVar.b(str);
            }
            if (this.K != null) {
                this.K.a();
            }
        }
        if (!this.D) {
            throw new aa("Fragment " + this + " did not call through to super.onStart()");
        }
        if (this.u != null) {
            this.u.j();
        }
        if (this.K != null) {
            y yVar = this.K;
            c.a();
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder((int) NativeMapEngine.MAX_ICON_SIZE);
        a.a(this, sb);
        if (this.f >= 0) {
            sb.append(" #");
            sb.append(this.f);
        }
        if (this.w != 0) {
            sb.append(" id=0x");
            sb.append(Integer.toHexString(this.w));
        }
        if (this.y != null) {
            sb.append(" ");
            sb.append(this.y);
        }
        sb.append('}');
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public final void w() {
        if (this.u != null) {
            this.u.l();
        }
        this.D = false;
        n();
        if (!this.D) {
            throw new aa("Fragment " + this + " did not call through to super.onPause()");
        }
    }

    /* access modifiers changed from: package-private */
    public final void x() {
        if (this.u != null) {
            this.u.m();
        }
        this.D = false;
        o();
        if (!this.D) {
            throw new aa("Fragment " + this + " did not call through to super.onStop()");
        }
    }

    /* access modifiers changed from: package-private */
    public final void y() {
        if (this.u != null) {
            this.u.n();
        }
        if (this.N) {
            this.N = false;
            if (!this.O) {
                this.O = true;
                g gVar = this.t;
                String str = this.g;
                boolean z2 = this.N;
                this.K = gVar.b(str);
            }
            if (this.K == null) {
                return;
            }
            if (!this.t.d) {
                this.K.b();
            } else {
                this.K.c();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void z() {
        if (this.u != null) {
            this.u.o();
        }
        this.D = false;
        this.D = true;
        if (!this.D) {
            throw new aa("Fragment " + this + " did not call through to super.onDestroyView()");
        } else if (this.K != null) {
            y yVar = this.K;
            c.a();
        }
    }
}
