package android.support.v4.app;

import android.view.animation.Animation;

final class p implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ Fragment f344a;
    private /* synthetic */ n b;

    p(n nVar, Fragment fragment) {
        this.b = nVar;
        this.f344a = fragment;
    }

    public final void onAnimationEnd(Animation animation) {
        if (this.f344a.b != null) {
            this.f344a.b = null;
            this.b.a(this.f344a, this.f344a.c, 0, 0, false);
        }
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
