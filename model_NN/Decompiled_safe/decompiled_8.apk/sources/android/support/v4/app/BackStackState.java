package android.support.v4.app;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.util.ArrayList;

final class BackStackState implements Parcelable {
    public static final Parcelable.Creator CREATOR = new c();

    /* renamed from: a  reason: collision with root package name */
    private int[] f327a;
    private int b;
    private int c;
    private String d;
    private int e;
    private int f;
    private CharSequence g;
    private int h;
    private CharSequence i;

    public BackStackState(Parcel parcel) {
        this.f327a = parcel.createIntArray();
        this.b = parcel.readInt();
        this.c = parcel.readInt();
        this.d = parcel.readString();
        this.e = parcel.readInt();
        this.f = parcel.readInt();
        this.g = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.h = parcel.readInt();
        this.i = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
    }

    public BackStackState(b bVar) {
        int i2 = 0;
        for (a aVar = bVar.f335a; aVar != null; aVar = aVar.f334a) {
            if (aVar.i != null) {
                i2 += aVar.i.size();
            }
        }
        this.f327a = new int[(i2 + (bVar.b * 7))];
        if (!bVar.e) {
            throw new IllegalStateException("Not on back stack");
        }
        int i3 = 0;
        for (a aVar2 = bVar.f335a; aVar2 != null; aVar2 = aVar2.f334a) {
            int i4 = i3 + 1;
            this.f327a[i3] = aVar2.c;
            int i5 = i4 + 1;
            this.f327a[i4] = aVar2.d != null ? aVar2.d.f : -1;
            int i6 = i5 + 1;
            this.f327a[i5] = aVar2.e;
            int i7 = i6 + 1;
            this.f327a[i6] = aVar2.f;
            int i8 = i7 + 1;
            this.f327a[i7] = aVar2.g;
            int i9 = i8 + 1;
            this.f327a[i8] = aVar2.h;
            if (aVar2.i != null) {
                int size = aVar2.i.size();
                int i10 = i9 + 1;
                this.f327a[i9] = size;
                int i11 = 0;
                while (i11 < size) {
                    this.f327a[i10] = ((Fragment) aVar2.i.get(i11)).f;
                    i11++;
                    i10++;
                }
                i3 = i10;
            } else {
                i3 = i9 + 1;
                this.f327a[i9] = 0;
            }
        }
        this.b = bVar.c;
        this.c = bVar.d;
        this.d = bVar.f;
        this.e = bVar.g;
        this.f = bVar.h;
        this.g = bVar.i;
        this.h = bVar.j;
        this.i = bVar.k;
    }

    public final b a(n nVar) {
        b bVar = new b(nVar);
        int i2 = 0;
        while (i2 < this.f327a.length) {
            a aVar = new a();
            int i3 = i2 + 1;
            aVar.c = this.f327a[i2];
            int i4 = i3 + 1;
            int i5 = this.f327a[i3];
            if (i5 >= 0) {
                aVar.d = (Fragment) nVar.f342a.get(i5);
            } else {
                aVar.d = null;
            }
            int i6 = i4 + 1;
            aVar.e = this.f327a[i4];
            int i7 = i6 + 1;
            aVar.f = this.f327a[i6];
            int i8 = i7 + 1;
            aVar.g = this.f327a[i7];
            int i9 = i8 + 1;
            aVar.h = this.f327a[i8];
            i2 = i9 + 1;
            int i10 = this.f327a[i9];
            if (i10 > 0) {
                aVar.i = new ArrayList(i10);
                int i11 = 0;
                while (i11 < i10) {
                    aVar.i.add((Fragment) nVar.f342a.get(this.f327a[i2]));
                    i11++;
                    i2++;
                }
            }
            bVar.a(aVar);
        }
        bVar.c = this.b;
        bVar.d = this.c;
        bVar.f = this.d;
        bVar.g = this.e;
        bVar.e = true;
        bVar.h = this.f;
        bVar.i = this.g;
        bVar.j = this.h;
        bVar.k = this.i;
        bVar.a(1);
        return bVar;
    }

    public final int describeContents() {
        return 0;
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        parcel.writeIntArray(this.f327a);
        parcel.writeInt(this.b);
        parcel.writeInt(this.c);
        parcel.writeString(this.d);
        parcel.writeInt(this.e);
        parcel.writeInt(this.f);
        TextUtils.writeToParcel(this.g, parcel, 0);
        parcel.writeInt(this.h);
        TextUtils.writeToParcel(this.i, parcel, 0);
    }
}
