package android.support.v4.app;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

final class FragmentState implements Parcelable {
    public static final Parcelable.Creator CREATOR = new s();

    /* renamed from: a  reason: collision with root package name */
    Bundle f331a;
    Fragment b;
    private String c;
    private int d;
    private boolean e;
    private int f;
    private int g;
    private String h;
    private boolean i;
    private boolean j;
    private Bundle k;

    public FragmentState(Parcel parcel) {
        boolean z = true;
        this.c = parcel.readString();
        this.d = parcel.readInt();
        this.e = parcel.readInt() != 0;
        this.f = parcel.readInt();
        this.g = parcel.readInt();
        this.h = parcel.readString();
        this.i = parcel.readInt() != 0;
        this.j = parcel.readInt() == 0 ? false : z;
        this.k = parcel.readBundle();
        this.f331a = parcel.readBundle();
    }

    public FragmentState(Fragment fragment) {
        this.c = fragment.getClass().getName();
        this.d = fragment.f;
        this.e = fragment.o;
        this.f = fragment.w;
        this.g = fragment.x;
        this.h = fragment.y;
        this.i = fragment.B;
        this.j = fragment.A;
        this.k = fragment.h;
    }

    public final Fragment a(g gVar, Fragment fragment) {
        if (this.b != null) {
            return this.b;
        }
        if (this.k != null) {
            this.k.setClassLoader(gVar.getClassLoader());
        }
        this.b = Fragment.a(gVar, this.c, this.k);
        if (this.f331a != null) {
            this.f331a.setClassLoader(gVar.getClassLoader());
            this.b.d = this.f331a;
        }
        this.b.a(this.d, fragment);
        this.b.o = this.e;
        this.b.q = true;
        this.b.w = this.f;
        this.b.x = this.g;
        this.b.y = this.h;
        this.b.B = this.i;
        this.b.A = this.j;
        this.b.s = gVar.b;
        return this.b;
    }

    public final int describeContents() {
        return 0;
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        int i3 = 1;
        parcel.writeString(this.c);
        parcel.writeInt(this.d);
        parcel.writeInt(this.e ? 1 : 0);
        parcel.writeInt(this.f);
        parcel.writeInt(this.g);
        parcel.writeString(this.h);
        parcel.writeInt(this.i ? 1 : 0);
        if (!this.j) {
            i3 = 0;
        }
        parcel.writeInt(i3);
        parcel.writeBundle(this.k);
        parcel.writeBundle(this.f331a);
    }
}
