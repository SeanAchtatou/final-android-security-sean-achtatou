package android.support.v4.app;

import android.support.v4.b.a;
import android.support.v4.c.c;
import android.util.Log;
import com.mapabc.minimap.map.vmap.NativeMapEngine;

final class y extends x {

    /* renamed from: a  reason: collision with root package name */
    final String f348a;
    boolean b;
    boolean c;
    private g d;

    public static void e() {
        c.a();
        c.a();
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        if (this.b) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doStart when already started: " + this, runtimeException);
            return;
        }
        this.b = true;
        c.a();
    }

    /* access modifiers changed from: package-private */
    public final void a(g gVar) {
        this.d = gVar;
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        if (!this.b) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doStop when not started: " + this, runtimeException);
            return;
        }
        c.a();
        this.b = false;
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        if (!this.b) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doRetain when not started: " + this, runtimeException);
            return;
        }
        this.c = true;
        this.b = false;
        c.a();
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        if (!this.c) {
            c.a();
            c.b();
        }
        c.a();
        c.b();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder((int) NativeMapEngine.MAX_ICON_SIZE);
        sb.append("LoaderManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        a.a(this.d, sb);
        sb.append("}}");
        return sb.toString();
    }
}
