package android.support.v4.app;

import android.os.Parcelable;
import android.support.v4.view.u;
import android.view.View;
import android.view.ViewGroup;

public abstract class r extends u {

    /* renamed from: a  reason: collision with root package name */
    private final m f345a;
    private w b = null;
    private Fragment c = null;

    public r(m mVar) {
        this.f345a = mVar;
    }

    private static String a(int i, long j) {
        return "android:switcher:" + i + ":" + j;
    }

    public final Parcelable a() {
        return null;
    }

    public abstract Fragment a(int i);

    public Object a(ViewGroup viewGroup, int i) {
        if (this.b == null) {
            this.b = this.f345a.a();
        }
        long j = (long) i;
        Fragment a2 = this.f345a.a(a(viewGroup.getId(), j));
        if (a2 != null) {
            this.b.f(a2);
        } else {
            a2 = a(i);
            this.b.a(viewGroup.getId(), a2, a(viewGroup.getId(), j));
        }
        if (a2 != this.c) {
            a2.b(false);
            a2.c(false);
        }
        return a2;
    }

    public final void a(Parcelable parcelable, ClassLoader classLoader) {
    }

    public void a(ViewGroup viewGroup) {
        if (this.b != null) {
            this.b.c();
            this.b = null;
            this.f345a.b();
        }
    }

    public void a(ViewGroup viewGroup, int i, Object obj) {
        if (this.b == null) {
            this.b = this.f345a.a();
        }
        this.b.e((Fragment) obj);
    }

    public final void a(Object obj) {
        Fragment fragment = (Fragment) obj;
        if (fragment != this.c) {
            if (this.c != null) {
                this.c.b(false);
                this.c.c(false);
            }
            if (fragment != null) {
                fragment.b(true);
                fragment.c(true);
            }
            this.c = fragment;
        }
    }

    public final boolean a(View view, Object obj) {
        return ((Fragment) obj).l() == view;
    }
}
