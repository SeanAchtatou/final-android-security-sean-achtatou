package android.support.v4.app;

import android.view.View;

final class d implements l {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ Fragment f336a;

    d(Fragment fragment) {
        this.f336a = fragment;
    }

    public final View a(int i) {
        if (this.f336a.G != null) {
            return this.f336a.G.findViewById(i);
        }
        throw new IllegalStateException("Fragment does not have a view");
    }
}
