package android.support.v4.widget;

import android.view.View;

final class r extends z {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SlidingPaneLayout f396a;

    private r(SlidingPaneLayout slidingPaneLayout) {
        this.f396a = slidingPaneLayout;
    }

    /* synthetic */ r(SlidingPaneLayout slidingPaneLayout, byte b) {
        this(slidingPaneLayout);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.SlidingPaneLayout.a(android.support.v4.widget.SlidingPaneLayout, boolean):boolean
     arg types: [android.support.v4.widget.SlidingPaneLayout, int]
     candidates:
      android.support.v4.widget.SlidingPaneLayout.a(android.support.v4.widget.SlidingPaneLayout, int):void
      android.support.v4.widget.SlidingPaneLayout.a(android.support.v4.widget.SlidingPaneLayout, android.view.View):void
      android.support.v4.widget.SlidingPaneLayout.a(android.support.v4.widget.SlidingPaneLayout, boolean):boolean */
    public final void a(int i) {
        if (this.f396a.o.a() != 0) {
            return;
        }
        if (this.f396a.g == 0.0f) {
            this.f396a.a(this.f396a.f);
            SlidingPaneLayout slidingPaneLayout = this.f396a;
            View unused = this.f396a.f;
            slidingPaneLayout.b();
            boolean unused2 = this.f396a.p = false;
            return;
        }
        SlidingPaneLayout slidingPaneLayout2 = this.f396a;
        View unused3 = this.f396a.f;
        slidingPaneLayout2.a();
        boolean unused4 = this.f396a.p = true;
    }

    public final void a(int i, int i2) {
        this.f396a.o.a(this.f396a.f, i2);
    }

    public final void a(View view, float f) {
        int paddingLeft = ((s) view.getLayoutParams()).leftMargin + this.f396a.getPaddingLeft();
        if (f > 0.0f || (f == 0.0f && this.f396a.g > 0.5f)) {
            paddingLeft += this.f396a.i;
        }
        this.f396a.o.a(paddingLeft, view.getTop());
        this.f396a.invalidate();
    }

    public final void a(View view, int i) {
        SlidingPaneLayout.a(this.f396a, i);
        this.f396a.invalidate();
    }

    public final boolean a(View view) {
        if (this.f396a.j) {
            return false;
        }
        return ((s) view.getLayoutParams()).b;
    }

    public final int b(View view, int i) {
        int paddingLeft = ((s) this.f396a.f.getLayoutParams()).leftMargin + this.f396a.getPaddingLeft();
        return Math.min(Math.max(i, paddingLeft), this.f396a.i + paddingLeft);
    }

    public final void b(View view) {
        this.f396a.c();
    }

    public final int c(View view) {
        return this.f396a.i;
    }
}
