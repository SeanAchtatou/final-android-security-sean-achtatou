package android.support.v4.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.ViewGroup;

public final class b extends ViewGroup.MarginLayoutParams {

    /* renamed from: a  reason: collision with root package name */
    public int f389a = 0;
    float b;
    boolean c;
    boolean d;

    public b() {
        super(-1, -1);
    }

    public b(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, DrawerLayout.f384a);
        this.f389a = obtainStyledAttributes.getInt(0, 0);
        obtainStyledAttributes.recycle();
    }

    public b(b bVar) {
        super((ViewGroup.MarginLayoutParams) bVar);
        this.f389a = bVar.f389a;
    }

    public b(ViewGroup.LayoutParams layoutParams) {
        super(layoutParams);
    }

    public b(ViewGroup.MarginLayoutParams marginLayoutParams) {
        super(marginLayoutParams);
    }
}
