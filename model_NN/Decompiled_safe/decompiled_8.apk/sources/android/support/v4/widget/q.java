package android.support.v4.widget;

import android.support.v4.view.ah;
import android.view.View;

final class q implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private View f395a;
    private /* synthetic */ SlidingPaneLayout b;

    q(SlidingPaneLayout slidingPaneLayout, View view) {
        this.b = slidingPaneLayout;
        this.f395a = view;
    }

    public final void run() {
        if (this.f395a.getParent() == this.b) {
            ah.a(this.f395a, 0, null);
            this.b.c(this.f395a);
        }
        this.b.s.remove(this);
    }
}
