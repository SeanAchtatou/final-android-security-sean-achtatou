package android.support.v4.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.ah;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import com.immomo.momo.android.plugin.cropimage.q;
import java.util.ArrayList;
import mm.purchasesdk.PurchaseCode;

public class SlidingPaneLayout extends ViewGroup {
    private static j t;

    /* renamed from: a  reason: collision with root package name */
    private int f386a;
    private int b;
    private Drawable c;
    private final int d;
    private boolean e;
    /* access modifiers changed from: private */
    public View f;
    /* access modifiers changed from: private */
    public float g;
    private float h;
    /* access modifiers changed from: private */
    public int i;
    /* access modifiers changed from: private */
    public boolean j;
    private int k;
    private float l;
    private float m;
    private q n;
    /* access modifiers changed from: private */
    public final w o;
    /* access modifiers changed from: private */
    public boolean p;
    private boolean q;
    private final Rect r;
    /* access modifiers changed from: private */
    public final ArrayList s;

    class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new t();

        /* renamed from: a  reason: collision with root package name */
        boolean f387a;

        private SavedState(Parcel parcel) {
            super(parcel);
            this.f387a = parcel.readInt() != 0;
        }

        /* synthetic */ SavedState(Parcel parcel, byte b) {
            this(parcel);
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f387a ? 1 : 0);
        }
    }

    static {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 17) {
            t = new v();
        } else if (i2 >= 16) {
            t = new u();
        } else {
            t = new j();
        }
    }

    public SlidingPaneLayout(Context context) {
        this(context, null);
    }

    public SlidingPaneLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public SlidingPaneLayout(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f386a = -858993460;
        this.q = true;
        this.r = new Rect();
        this.s = new ArrayList();
        float f2 = context.getResources().getDisplayMetrics().density;
        this.d = (int) ((32.0f * f2) + 0.5f);
        ViewConfiguration.get(context);
        setWillNotDraw(false);
        ah.a(this, new p(this));
        ah.d(this);
        this.o = w.a(this, 0.5f, new r(this, (byte) 0));
        this.o.a(1);
        this.o.a(f2 * 400.0f);
    }

    static /* synthetic */ void a(SlidingPaneLayout slidingPaneLayout, int i2) {
        s sVar = (s) slidingPaneLayout.f.getLayoutParams();
        slidingPaneLayout.g = ((float) (i2 - (slidingPaneLayout.getPaddingLeft() + sVar.leftMargin))) / ((float) slidingPaneLayout.i);
        if (slidingPaneLayout.k != 0) {
            slidingPaneLayout.b(slidingPaneLayout.g);
        }
        if (sVar.c) {
            slidingPaneLayout.a(slidingPaneLayout.f, slidingPaneLayout.g, slidingPaneLayout.f386a);
        }
        View view = slidingPaneLayout.f;
        if (slidingPaneLayout.n != null) {
            q qVar = slidingPaneLayout.n;
            float f2 = slidingPaneLayout.g;
        }
    }

    private void a(View view, float f2, int i2) {
        s sVar = (s) view.getLayoutParams();
        if (f2 > 0.0f && i2 != 0) {
            int i3 = (((int) (((float) ((-16777216 & i2) >>> 24)) * f2)) << 24) | (16777215 & i2);
            if (sVar.d == null) {
                sVar.d = new Paint();
            }
            sVar.d.setColorFilter(new PorterDuffColorFilter(i3, PorterDuff.Mode.SRC_OVER));
            if (ah.e(view) != 2) {
                ah.a(view, 2, sVar.d);
            }
            c(view);
        } else if (ah.e(view) != 0) {
            if (sVar.d != null) {
                sVar.d.setColorFilter(null);
            }
            q qVar = new q(this, view);
            this.s.add(qVar);
            ah.a(this, qVar);
        }
    }

    private boolean a(float f2) {
        if (!this.e) {
            return false;
        }
        int paddingLeft = getPaddingLeft();
        if (!this.o.a(this.f, (int) (((float) (((s) this.f.getLayoutParams()).leftMargin + paddingLeft)) + (((float) this.i) * f2)), this.f.getTop())) {
            return false;
        }
        c();
        ah.b(this);
        return true;
    }

    private void b(float f2) {
        s sVar = (s) this.f.getLayoutParams();
        boolean z = sVar.c && sVar.leftMargin <= 0;
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (childAt != this.f) {
                this.h = f2;
                childAt.offsetLeftAndRight(((int) ((1.0f - this.h) * ((float) this.k))) - ((int) ((1.0f - f2) * ((float) this.k))));
                if (z) {
                    a(childAt, 1.0f - this.h, this.b);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void c(View view) {
        t.a(this, view);
    }

    private boolean d() {
        if (!this.q && !a(0.0f)) {
            return false;
        }
        this.p = false;
        return true;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        if (this.n != null) {
            q qVar = this.n;
        }
        sendAccessibilityEvent(32);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x003f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.view.View r17) {
        /*
            r16 = this;
            int r7 = r16.getPaddingLeft()
            int r1 = r16.getWidth()
            int r2 = r16.getPaddingRight()
            int r8 = r1 - r2
            int r9 = r16.getPaddingTop()
            int r1 = r16.getHeight()
            int r2 = r16.getPaddingBottom()
            int r10 = r1 - r2
            if (r17 == 0) goto L_0x0092
            boolean r1 = android.support.v4.view.ah.h(r17)
            if (r1 == 0) goto L_0x0079
            r1 = 1
        L_0x0025:
            if (r1 == 0) goto L_0x0092
            int r4 = r17.getLeft()
            int r3 = r17.getRight()
            int r2 = r17.getTop()
            int r1 = r17.getBottom()
        L_0x0037:
            r5 = 0
            int r11 = r16.getChildCount()
            r6 = r5
        L_0x003d:
            if (r6 >= r11) goto L_0x0099
            r0 = r16
            android.view.View r12 = r0.getChildAt(r6)
            r0 = r17
            if (r12 == r0) goto L_0x0099
            int r5 = r12.getLeft()
            int r5 = java.lang.Math.max(r7, r5)
            int r13 = r12.getTop()
            int r13 = java.lang.Math.max(r9, r13)
            int r14 = r12.getRight()
            int r14 = java.lang.Math.min(r8, r14)
            int r15 = r12.getBottom()
            int r15 = java.lang.Math.min(r10, r15)
            if (r5 < r4) goto L_0x0097
            if (r13 < r2) goto L_0x0097
            if (r14 > r3) goto L_0x0097
            if (r15 > r1) goto L_0x0097
            r5 = 4
        L_0x0072:
            r12.setVisibility(r5)
            int r5 = r6 + 1
            r6 = r5
            goto L_0x003d
        L_0x0079:
            int r1 = android.os.Build.VERSION.SDK_INT
            r2 = 18
            if (r1 >= r2) goto L_0x0090
            android.graphics.drawable.Drawable r1 = r17.getBackground()
            if (r1 == 0) goto L_0x0090
            int r1 = r1.getOpacity()
            r2 = -1
            if (r1 != r2) goto L_0x008e
            r1 = 1
            goto L_0x0025
        L_0x008e:
            r1 = 0
            goto L_0x0025
        L_0x0090:
            r1 = 0
            goto L_0x0025
        L_0x0092:
            r1 = 0
            r2 = r1
            r3 = r1
            r4 = r1
            goto L_0x0037
        L_0x0097:
            r5 = 0
            goto L_0x0072
        L_0x0099:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.SlidingPaneLayout.a(android.view.View):void");
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        if (this.n != null) {
            q qVar = this.n;
        }
        sendAccessibilityEvent(32);
    }

    /* access modifiers changed from: package-private */
    public final boolean b(View view) {
        if (view == null) {
            return false;
        }
        return this.e && ((s) view.getLayoutParams()).c && this.g > 0.0f;
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 4) {
                childAt.setVisibility(0);
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof s) && super.checkLayoutParams(layoutParams);
    }

    public void computeScroll() {
        if (!this.o.g()) {
            return;
        }
        if (!this.e) {
            this.o.f();
        } else {
            ah.b(this);
        }
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        View childAt = getChildCount() > 1 ? getChildAt(1) : null;
        if (childAt != null && this.c != null) {
            int intrinsicWidth = this.c.getIntrinsicWidth();
            int left = childAt.getLeft();
            this.c.setBounds(left - intrinsicWidth, childAt.getTop(), left, childAt.getBottom());
            this.c.draw(canvas);
        }
    }

    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j2) {
        boolean drawChild;
        s sVar = (s) view.getLayoutParams();
        int save = canvas.save(2);
        if (this.e && !sVar.b && this.f != null) {
            canvas.getClipBounds(this.r);
            this.r.right = Math.min(this.r.right, this.f.getLeft());
            canvas.clipRect(this.r);
        }
        if (Build.VERSION.SDK_INT < 11) {
            if (sVar.c && this.g > 0.0f) {
                if (!view.isDrawingCacheEnabled()) {
                    view.setDrawingCacheEnabled(true);
                }
                Bitmap drawingCache = view.getDrawingCache();
                if (drawingCache != null) {
                    canvas.drawBitmap(drawingCache, (float) view.getLeft(), (float) view.getTop(), sVar.d);
                    drawChild = false;
                    canvas.restoreToCount(save);
                    return drawChild;
                }
                Log.e("SlidingPaneLayout", "drawChild: child view " + view + " returned null drawing cache");
            } else if (view.isDrawingCacheEnabled()) {
                view.setDrawingCacheEnabled(false);
            }
        }
        drawChild = super.drawChild(canvas, view, j2);
        canvas.restoreToCount(save);
        return drawChild;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new s();
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new s(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof ViewGroup.MarginLayoutParams ? new s((ViewGroup.MarginLayoutParams) layoutParams) : new s(layoutParams);
    }

    public int getCoveredFadeColor() {
        return this.b;
    }

    public int getParallaxDistance() {
        return this.k;
    }

    public int getSliderFadeColor() {
        return this.f386a;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.q = true;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.q = true;
        int size = this.s.size();
        for (int i2 = 0; i2 < size; i2++) {
            ((q) this.s.get(i2)).run();
        }
        this.s.clear();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z;
        View childAt;
        int a2 = android.support.v4.view.q.a(motionEvent);
        if (!this.e && a2 == 0 && getChildCount() > 1 && (childAt = getChildAt(1)) != null) {
            w wVar = this.o;
            this.p = !w.b(childAt, (int) motionEvent.getX(), (int) motionEvent.getY());
        }
        if (!this.e || (this.j && a2 != 0)) {
            this.o.e();
            return super.onInterceptTouchEvent(motionEvent);
        } else if (a2 == 3 || a2 == 1) {
            this.o.e();
            return false;
        } else {
            switch (a2) {
                case 0:
                    this.j = false;
                    float x = motionEvent.getX();
                    float y = motionEvent.getY();
                    this.l = x;
                    this.m = y;
                    w wVar2 = this.o;
                    if (w.b(this.f, (int) x, (int) y) && b(this.f)) {
                        z = true;
                        break;
                    }
                    z = false;
                    break;
                case 1:
                default:
                    z = false;
                    break;
                case 2:
                    float x2 = motionEvent.getX();
                    float y2 = motionEvent.getY();
                    float abs = Math.abs(x2 - this.l);
                    float abs2 = Math.abs(y2 - this.m);
                    if (abs > ((float) this.o.d()) && abs2 > abs) {
                        this.o.e();
                        this.j = true;
                        return false;
                    }
                    z = false;
                    break;
            }
            return this.o.a(motionEvent) || z;
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int i6;
        int i7 = i4 - i2;
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int paddingTop = getPaddingTop();
        int childCount = getChildCount();
        if (this.q) {
            this.g = (!this.e || !this.p) ? 0.0f : 1.0f;
        }
        int i8 = 0;
        int i9 = paddingLeft;
        while (i8 < childCount) {
            View childAt = getChildAt(i8);
            if (childAt.getVisibility() != 8) {
                s sVar = (s) childAt.getLayoutParams();
                int measuredWidth = childAt.getMeasuredWidth();
                int i10 = 0;
                if (sVar.b) {
                    int min = (Math.min(i9, (i7 - paddingRight) - this.d) - paddingLeft) - (sVar.leftMargin + sVar.rightMargin);
                    this.i = min;
                    sVar.c = ((sVar.leftMargin + paddingLeft) + min) + (measuredWidth / 2) > i7 - paddingRight;
                    i6 = sVar.leftMargin + ((int) (((float) min) * this.g)) + paddingLeft;
                } else {
                    i10 = (!this.e || this.k == 0) ? 0 : (int) ((1.0f - this.g) * ((float) this.k));
                    i6 = i9;
                }
                int i11 = i6 - i10;
                childAt.layout(i11, paddingTop, i11 + measuredWidth, childAt.getMeasuredHeight() + paddingTop);
                i9 += childAt.getWidth();
            } else {
                i6 = paddingLeft;
            }
            i8++;
            paddingLeft = i6;
        }
        if (this.q) {
            if (this.e) {
                if (this.k != 0) {
                    b(this.g);
                }
                if (((s) this.f.getLayoutParams()).c) {
                    a(this.f, this.g, this.f386a);
                }
            } else {
                for (int i12 = 0; i12 < childCount; i12++) {
                    a(getChildAt(i12), 0.0f, this.f386a);
                }
            }
            a(this.f);
        }
        this.q = false;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int i4;
        int i5;
        int i6;
        int i7;
        int paddingTop;
        int i8;
        float f2;
        boolean z;
        int i9;
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        int size2 = View.MeasureSpec.getSize(i3);
        if (mode == 1073741824) {
            if (mode2 == 0) {
                if (!isInEditMode()) {
                    throw new IllegalStateException("Height must not be UNSPECIFIED");
                } else if (mode2 == 0) {
                    i4 = Integer.MIN_VALUE;
                    i5 = size;
                    i6 = 300;
                }
            }
            i4 = mode2;
            i5 = size;
            i6 = size2;
        } else if (isInEditMode()) {
            if (mode != Integer.MIN_VALUE && mode == 0) {
                i4 = mode2;
                i5 = 300;
                i6 = size2;
            }
            i4 = mode2;
            i5 = size;
            i6 = size2;
        } else {
            throw new IllegalStateException("Width must have an exact value or MATCH_PARENT");
        }
        switch (i4) {
            case Integer.MIN_VALUE:
                i7 = 0;
                paddingTop = (i6 - getPaddingTop()) - getPaddingBottom();
                break;
            case 1073741824:
                i7 = (i6 - getPaddingTop()) - getPaddingBottom();
                paddingTop = i7;
                break;
            default:
                i7 = 0;
                paddingTop = -1;
                break;
        }
        boolean z2 = false;
        int paddingLeft = (i5 - getPaddingLeft()) - getPaddingRight();
        int childCount = getChildCount();
        if (childCount > 2) {
            Log.e("SlidingPaneLayout", "onMeasure: More than two child views are not supported.");
        }
        this.f = null;
        int i10 = 0;
        int i11 = i7;
        float f3 = 0.0f;
        while (i10 < childCount) {
            View childAt = getChildAt(i10);
            s sVar = (s) childAt.getLayoutParams();
            if (childAt.getVisibility() == 8) {
                sVar.c = false;
                i8 = paddingLeft;
                i9 = i11;
                f2 = f3;
                z = z2;
            } else {
                if (sVar.f397a > 0.0f) {
                    f3 += sVar.f397a;
                    if (sVar.width == 0) {
                        i8 = paddingLeft;
                        i9 = i11;
                        f2 = f3;
                        z = z2;
                    }
                }
                int i12 = sVar.leftMargin + sVar.rightMargin;
                childAt.measure(sVar.width == -2 ? View.MeasureSpec.makeMeasureSpec(i5 - i12, Integer.MIN_VALUE) : sVar.width == -1 ? View.MeasureSpec.makeMeasureSpec(i5 - i12, 1073741824) : View.MeasureSpec.makeMeasureSpec(sVar.width, 1073741824), sVar.height == -2 ? View.MeasureSpec.makeMeasureSpec(paddingTop, Integer.MIN_VALUE) : sVar.height == -1 ? View.MeasureSpec.makeMeasureSpec(paddingTop, 1073741824) : View.MeasureSpec.makeMeasureSpec(sVar.height, 1073741824));
                int measuredWidth = childAt.getMeasuredWidth();
                int measuredHeight = childAt.getMeasuredHeight();
                if (i4 == Integer.MIN_VALUE && measuredHeight > i11) {
                    i11 = Math.min(measuredHeight, paddingTop);
                }
                int i13 = paddingLeft - measuredWidth;
                boolean z3 = i13 < 0;
                sVar.b = z3;
                boolean z4 = z3 | z2;
                if (sVar.b) {
                    this.f = childAt;
                }
                i8 = i13;
                f2 = f3;
                z = z4;
                i9 = i11;
            }
            i10++;
            z2 = z;
            i11 = i9;
            paddingLeft = i8;
            f3 = f2;
        }
        if (z2 || f3 > 0.0f) {
            int i14 = i5 - this.d;
            for (int i15 = 0; i15 < childCount; i15++) {
                View childAt2 = getChildAt(i15);
                if (childAt2.getVisibility() != 8) {
                    s sVar2 = (s) childAt2.getLayoutParams();
                    if (childAt2.getVisibility() != 8) {
                        boolean z5 = sVar2.width == 0 && sVar2.f397a > 0.0f;
                        int measuredWidth2 = z5 ? 0 : childAt2.getMeasuredWidth();
                        if (!z2 || childAt2 == this.f) {
                            if (sVar2.f397a > 0.0f) {
                                int makeMeasureSpec = sVar2.width == 0 ? sVar2.height == -2 ? View.MeasureSpec.makeMeasureSpec(paddingTop, Integer.MIN_VALUE) : sVar2.height == -1 ? View.MeasureSpec.makeMeasureSpec(paddingTop, 1073741824) : View.MeasureSpec.makeMeasureSpec(sVar2.height, 1073741824) : View.MeasureSpec.makeMeasureSpec(childAt2.getMeasuredHeight(), 1073741824);
                                if (z2) {
                                    int i16 = i5 - (sVar2.rightMargin + sVar2.leftMargin);
                                    int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(i16, 1073741824);
                                    if (measuredWidth2 != i16) {
                                        childAt2.measure(makeMeasureSpec2, makeMeasureSpec);
                                    }
                                } else {
                                    childAt2.measure(View.MeasureSpec.makeMeasureSpec(((int) ((sVar2.f397a * ((float) Math.max(0, paddingLeft))) / f3)) + measuredWidth2, 1073741824), makeMeasureSpec);
                                }
                            }
                        } else if (sVar2.width < 0 && (measuredWidth2 > i14 || sVar2.f397a > 0.0f)) {
                            childAt2.measure(View.MeasureSpec.makeMeasureSpec(i14, 1073741824), z5 ? sVar2.height == -2 ? View.MeasureSpec.makeMeasureSpec(paddingTop, Integer.MIN_VALUE) : sVar2.height == -1 ? View.MeasureSpec.makeMeasureSpec(paddingTop, 1073741824) : View.MeasureSpec.makeMeasureSpec(sVar2.height, 1073741824) : View.MeasureSpec.makeMeasureSpec(childAt2.getMeasuredHeight(), 1073741824));
                        }
                    }
                }
            }
        }
        setMeasuredDimension(i5, i11);
        this.e = z2;
        if (this.o.a() != 0 && !z2) {
            this.o.f();
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (savedState.f387a) {
            View view = this.f;
            if (this.q || a(1.0f)) {
                this.p = true;
            }
        } else {
            View view2 = this.f;
            d();
        }
        this.p = savedState.f387a;
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f387a = this.e ? !this.e || this.g == 1.0f : this.p;
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (i2 != i4) {
            this.q = true;
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.e) {
            return super.onTouchEvent(motionEvent);
        }
        this.o.b(motionEvent);
        switch (motionEvent.getAction() & PurchaseCode.AUTH_INVALID_APP) {
            case 0:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                this.l = x;
                this.m = y;
                break;
            case 1:
                if (b(this.f)) {
                    float x2 = motionEvent.getX();
                    float y2 = motionEvent.getY();
                    float f2 = x2 - this.l;
                    float f3 = y2 - this.m;
                    int d2 = this.o.d();
                    if ((f2 * f2) + (f3 * f3) < ((float) (d2 * d2))) {
                        w wVar = this.o;
                        if (w.b(this.f, (int) x2, (int) y2)) {
                            View view = this.f;
                            d();
                            break;
                        }
                    }
                }
                break;
        }
        return true;
    }

    public void requestChildFocus(View view, View view2) {
        super.requestChildFocus(view, view2);
        if (!isInTouchMode() && !this.e) {
            this.p = view == this.f;
        }
    }

    public void setCoveredFadeColor(int i2) {
        this.b = i2;
    }

    public void setPanelSlideListener$14b5545(q qVar) {
        this.n = qVar;
    }

    public void setParallaxDistance(int i2) {
        this.k = i2;
        requestLayout();
    }

    public void setShadowDrawable(Drawable drawable) {
        this.c = drawable;
    }

    public void setShadowResource(int i2) {
        setShadowDrawable(getResources().getDrawable(i2));
    }

    public void setSliderFadeColor(int i2) {
        this.f386a = i2;
    }
}
