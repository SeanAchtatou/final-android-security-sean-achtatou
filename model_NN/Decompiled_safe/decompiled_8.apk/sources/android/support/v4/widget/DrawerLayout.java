package android.support.v4.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.v4.view.ah;
import android.support.v4.view.at;
import android.support.v4.view.i;
import android.support.v4.view.m;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import com.amap.mapapi.map.MapView;
import com.immomo.momo.android.plugin.cropimage.q;
import mm.purchasesdk.PurchaseCode;

public final class DrawerLayout extends ViewGroup {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final int[] f384a = {16842931};
    private int b;
    private int c;
    private float d;
    private Paint e;
    private final w f;
    private final w g;
    private final d h;
    private final d i;
    private int j;
    private boolean k;
    private boolean l;
    private int m;
    private int n;
    private boolean o;
    private q p;
    private float q;
    private float r;

    public class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new c();

        /* renamed from: a  reason: collision with root package name */
        int f385a = 0;
        int b = 0;
        int c = 0;

        public SavedState(Parcel parcel) {
            super(parcel);
            this.f385a = parcel.readInt();
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f385a);
        }
    }

    public DrawerLayout(Context context) {
        this(context, null);
    }

    public DrawerLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public DrawerLayout(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.c = -1728053248;
        this.e = new Paint();
        this.l = true;
        float f2 = getResources().getDisplayMetrics().density;
        this.b = (int) ((64.0f * f2) + 0.5f);
        float f3 = f2 * 400.0f;
        this.h = new d(this, 3);
        this.i = new d(this, 5);
        this.f = w.a(this, 1.0f, this.h);
        this.f.a(1);
        this.f.a(f3);
        this.h.a(this.f);
        this.g = w.a(this, 1.0f, this.i);
        this.g.a(2);
        this.g.a(f3);
        this.i.a(this.g);
        setFocusableInTouchMode(true);
        ah.a(this, new a(this));
        at.a(this);
    }

    private void a(int i2, int i3) {
        int a2 = i.a(i3, ah.f(this));
        if (a2 == 3) {
            this.m = i2;
        } else if (a2 == 5) {
            this.n = i2;
        }
        if (i2 != 0) {
            (a2 == 3 ? this.f : this.g).e();
        }
        switch (i2) {
            case 1:
                View a3 = a(a2);
                if (a3 != null) {
                    d(a3);
                    return;
                }
                return;
            case 2:
                View a4 = a(a2);
                if (a4 != null) {
                    g(a4);
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void a(boolean z) {
        int childCount = getChildCount();
        boolean z2 = false;
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            b bVar = (b) childAt.getLayoutParams();
            if (c(childAt) && (!z || bVar.c)) {
                z2 = a(childAt, 3) ? z2 | this.f.a(childAt, -childAt.getWidth(), childAt.getTop()) : z2 | this.g.a(childAt, getWidth(), childAt.getTop());
                bVar.c = false;
            }
        }
        this.h.a();
        this.i.a();
        if (z2) {
            invalidate();
        }
    }

    static boolean a(View view, int i2) {
        return (e(view) & i2) == i2;
    }

    static float b(View view) {
        return ((b) view.getLayoutParams()).b;
    }

    static boolean c(View view) {
        return (i.a(((b) view.getLayoutParams()).f389a, ah.f(view)) & 7) != 0;
    }

    private View d() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (c(childAt)) {
                if (!c(childAt)) {
                    throw new IllegalArgumentException("View " + childAt + " is not a drawer");
                }
                if (((b) childAt.getLayoutParams()).b > 0.0f) {
                    return childAt;
                }
            }
        }
        return null;
    }

    private static int e(View view) {
        return i.a(((b) view.getLayoutParams()).f389a, ah.f(view));
    }

    private static boolean f(View view) {
        return ((b) view.getLayoutParams()).f389a == 0;
    }

    private void g(View view) {
        if (!c(view)) {
            throw new IllegalArgumentException("View " + view + " is not a sliding drawer");
        }
        if (this.l) {
            b bVar = (b) view.getLayoutParams();
            bVar.b = 1.0f;
            bVar.d = true;
        } else if (a(view, 3)) {
            this.f.a(view, 0, view.getTop());
        } else {
            this.g.a(view, getWidth() - view.getWidth(), view.getTop());
        }
        invalidate();
    }

    public final int a(View view) {
        int e2 = e(view);
        if (e2 == 3) {
            return this.m;
        }
        if (e2 == 5) {
            return this.n;
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public final View a() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (((b) childAt.getLayoutParams()).d) {
                return childAt;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final View a(int i2) {
        int childCount = getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = getChildAt(i3);
            if ((e(childAt) & 7) == (i2 & 7)) {
                return childAt;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2, View view) {
        int a2 = this.f.a();
        int a3 = this.g.a();
        int i3 = (a2 == 1 || a3 == 1) ? 1 : (a2 == 2 || a3 == 2) ? 2 : 0;
        if (view != null && i2 == 0) {
            b bVar = (b) view.getLayoutParams();
            if (bVar.b == 0.0f) {
                b bVar2 = (b) view.getLayoutParams();
                if (bVar2.d) {
                    bVar2.d = false;
                    if (this.p != null) {
                        q qVar = this.p;
                    }
                    sendAccessibilityEvent(32);
                }
            } else if (bVar.b == 1.0f) {
                b bVar3 = (b) view.getLayoutParams();
                if (!bVar3.d) {
                    bVar3.d = true;
                    if (this.p != null) {
                        q qVar2 = this.p;
                    }
                    view.sendAccessibilityEvent(32);
                }
            }
        }
        if (i3 != this.j) {
            this.j = i3;
            if (this.p != null) {
                q qVar3 = this.p;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(View view, float f2) {
        b bVar = (b) view.getLayoutParams();
        if (f2 != bVar.b) {
            bVar.b = f2;
            if (this.p != null) {
                q qVar = this.p;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        if (!this.o) {
            long uptimeMillis = SystemClock.uptimeMillis();
            MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
            int childCount = getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                getChildAt(i2).dispatchTouchEvent(obtain);
            }
            obtain.recycle();
            this.o = true;
        }
    }

    /* access modifiers changed from: protected */
    public final boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof b) && super.checkLayoutParams(layoutParams);
    }

    public final void computeScroll() {
        int childCount = getChildCount();
        float f2 = 0.0f;
        for (int i2 = 0; i2 < childCount; i2++) {
            f2 = Math.max(f2, ((b) getChildAt(i2).getLayoutParams()).b);
        }
        this.d = f2;
        if (this.f.g() || this.g.g()) {
            ah.b(this);
        }
    }

    public final void d(View view) {
        if (!c(view)) {
            throw new IllegalArgumentException("View " + view + " is not a sliding drawer");
        }
        if (this.l) {
            b bVar = (b) view.getLayoutParams();
            bVar.b = 0.0f;
            bVar.d = false;
        } else if (a(view, 3)) {
            this.f.a(view, -view.getWidth(), view.getTop());
        } else {
            this.g.a(view, getWidth(), view.getTop());
        }
        invalidate();
    }

    /* access modifiers changed from: protected */
    public final boolean drawChild(Canvas canvas, View view, long j2) {
        int i2;
        int height = getHeight();
        boolean f2 = f(view);
        int i3 = 0;
        int width = getWidth();
        int save = canvas.save();
        if (f2) {
            int childCount = getChildCount();
            int i4 = 0;
            while (i4 < childCount) {
                View childAt = getChildAt(i4);
                if (childAt != view && childAt.getVisibility() == 0) {
                    Drawable background = childAt.getBackground();
                    if ((background != null ? background.getOpacity() == -1 : false) && c(childAt) && childAt.getHeight() >= height) {
                        if (a(childAt, 3)) {
                            int right = childAt.getRight();
                            if (right <= i3) {
                                right = i3;
                            }
                            i3 = right;
                            i2 = width;
                        } else {
                            i2 = childAt.getLeft();
                            if (i2 < width) {
                            }
                        }
                        i4++;
                        width = i2;
                    }
                }
                i2 = width;
                i4++;
                width = i2;
            }
            canvas.clipRect(i3, 0, width, getHeight());
        }
        int i5 = width;
        boolean drawChild = super.drawChild(canvas, view, j2);
        canvas.restoreToCount(save);
        if (this.d > 0.0f && f2) {
            this.e.setColor((((int) (((float) ((this.c & -16777216) >>> 24)) * this.d)) << 24) | (this.c & 16777215));
            canvas.drawRect((float) i3, 0.0f, (float) i5, (float) getHeight(), this.e);
        }
        return drawChild;
    }

    /* access modifiers changed from: protected */
    public final ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new b();
    }

    public final ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new b(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public final ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof b ? new b((b) layoutParams) : layoutParams instanceof ViewGroup.MarginLayoutParams ? new b((ViewGroup.MarginLayoutParams) layoutParams) : new b(layoutParams);
    }

    /* access modifiers changed from: protected */
    public final void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.l = true;
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.l = true;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z;
        boolean z2;
        int a2 = android.support.v4.view.q.a(motionEvent);
        boolean a3 = this.f.a(motionEvent) | this.g.a(motionEvent);
        switch (a2) {
            case 0:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                this.q = x;
                this.r = y;
                z = this.d > 0.0f && f(this.f.b((int) x, (int) y));
                this.o = false;
                break;
            case 1:
            case 3:
                a(true);
                this.o = false;
                z = false;
                break;
            case 2:
                if (this.f.h()) {
                    this.h.a();
                    this.i.a();
                    z = false;
                    break;
                }
                z = false;
                break;
            default:
                z = false;
                break;
        }
        if (!a3 && !z) {
            int childCount = getChildCount();
            int i2 = 0;
            while (true) {
                if (i2 >= childCount) {
                    z2 = false;
                } else if (((b) getChildAt(i2).getLayoutParams()).c) {
                    z2 = true;
                } else {
                    i2++;
                }
            }
            if (!z2 && !this.o) {
                return false;
            }
        }
        return true;
    }

    public final boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            if (d() != null) {
                m.c(keyEvent);
                return true;
            }
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public final boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyUp(i2, keyEvent);
        }
        View d2 = d();
        if (d2 != null && a(d2) == 0) {
            a(false);
        }
        return d2 != null;
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int i6;
        float f2;
        this.k = true;
        int i7 = i4 - i2;
        int childCount = getChildCount();
        for (int i8 = 0; i8 < childCount; i8++) {
            View childAt = getChildAt(i8);
            if (childAt.getVisibility() != 8) {
                b bVar = (b) childAt.getLayoutParams();
                if (f(childAt)) {
                    childAt.layout(bVar.leftMargin, bVar.topMargin, bVar.leftMargin + childAt.getMeasuredWidth(), bVar.topMargin + childAt.getMeasuredHeight());
                } else {
                    int measuredWidth = childAt.getMeasuredWidth();
                    int measuredHeight = childAt.getMeasuredHeight();
                    if (a(childAt, 3)) {
                        i6 = ((int) (((float) measuredWidth) * bVar.b)) + (-measuredWidth);
                        f2 = ((float) (measuredWidth + i6)) / ((float) measuredWidth);
                    } else {
                        i6 = i7 - ((int) (((float) measuredWidth) * bVar.b));
                        f2 = ((float) (i7 - i6)) / ((float) measuredWidth);
                    }
                    boolean z2 = f2 != bVar.b;
                    switch (bVar.f389a & PurchaseCode.PARAMETER_ERR) {
                        case 16:
                            int i9 = i5 - i3;
                            int i10 = (i9 - measuredHeight) / 2;
                            if (i10 < bVar.topMargin) {
                                i10 = bVar.topMargin;
                            } else if (i10 + measuredHeight > i9 - bVar.bottomMargin) {
                                i10 = (i9 - bVar.bottomMargin) - measuredHeight;
                            }
                            childAt.layout(i6, i10, measuredWidth + i6, measuredHeight + i10);
                            break;
                        case MapView.LayoutParams.BOTTOM /*80*/:
                            int i11 = i5 - i3;
                            childAt.layout(i6, (i11 - bVar.bottomMargin) - childAt.getMeasuredHeight(), measuredWidth + i6, i11 - bVar.bottomMargin);
                            break;
                        default:
                            childAt.layout(i6, bVar.topMargin, measuredWidth + i6, measuredHeight);
                            break;
                    }
                    if (z2) {
                        a(childAt, f2);
                    }
                    int i12 = bVar.b > 0.0f ? 0 : 4;
                    if (childAt.getVisibility() != i12) {
                        childAt.setVisibility(i12);
                    }
                }
            }
        }
        this.k = false;
        this.l = false;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0027, code lost:
        if (r4 == 0) goto L_0x0029;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onMeasure(int r11, int r12) {
        /*
            r10 = this;
            r1 = 300(0x12c, float:4.2E-43)
            r6 = -2147483648(0xffffffff80000000, float:-0.0)
            r9 = 1073741824(0x40000000, float:2.0)
            int r3 = android.view.View.MeasureSpec.getMode(r11)
            int r4 = android.view.View.MeasureSpec.getMode(r12)
            int r2 = android.view.View.MeasureSpec.getSize(r11)
            int r0 = android.view.View.MeasureSpec.getSize(r12)
            if (r3 != r9) goto L_0x001a
            if (r4 == r9) goto L_0x00f4
        L_0x001a:
            boolean r5 = r10.isInEditMode()
            if (r5 == 0) goto L_0x006a
            if (r3 == r6) goto L_0x0025
            if (r3 != 0) goto L_0x0025
            r2 = r1
        L_0x0025:
            if (r4 == r6) goto L_0x00f4
            if (r4 != 0) goto L_0x00f4
        L_0x0029:
            r10.setMeasuredDimension(r2, r1)
            int r4 = r10.getChildCount()
            r0 = 0
            r3 = r0
        L_0x0032:
            if (r3 >= r4) goto L_0x00f3
            android.view.View r5 = r10.getChildAt(r3)
            int r0 = r5.getVisibility()
            r6 = 8
            if (r0 == r6) goto L_0x0066
            android.view.ViewGroup$LayoutParams r0 = r5.getLayoutParams()
            android.support.v4.widget.b r0 = (android.support.v4.widget.b) r0
            boolean r6 = f(r5)
            if (r6 == 0) goto L_0x0072
            int r6 = r0.leftMargin
            int r6 = r2 - r6
            int r7 = r0.rightMargin
            int r6 = r6 - r7
            int r6 = android.view.View.MeasureSpec.makeMeasureSpec(r6, r9)
            int r7 = r0.topMargin
            int r7 = r1 - r7
            int r0 = r0.bottomMargin
            int r0 = r7 - r0
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r9)
            r5.measure(r6, r0)
        L_0x0066:
            int r0 = r3 + 1
            r3 = r0
            goto L_0x0032
        L_0x006a:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "DrawerLayout must be measured with MeasureSpec.EXACTLY."
            r0.<init>(r1)
            throw r0
        L_0x0072:
            boolean r6 = c(r5)
            if (r6 == 0) goto L_0x00ce
            int r6 = e(r5)
            r6 = r6 & 7
            r7 = r6 & 0
            if (r7 == 0) goto L_0x00b1
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r0 = "Child drawer has absolute gravity "
            r2.<init>(r0)
            r0 = r6 & 3
            r3 = 3
            if (r0 != r3) goto L_0x00a4
            java.lang.String r0 = "LEFT"
        L_0x0092:
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r2 = " but this DrawerLayout already has a drawer view along that edge"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        L_0x00a4:
            r0 = r6 & 5
            r3 = 5
            if (r0 != r3) goto L_0x00ac
            java.lang.String r0 = "RIGHT"
            goto L_0x0092
        L_0x00ac:
            java.lang.String r0 = java.lang.Integer.toHexString(r6)
            goto L_0x0092
        L_0x00b1:
            int r6 = r10.b
            int r7 = r0.leftMargin
            int r6 = r6 + r7
            int r7 = r0.rightMargin
            int r6 = r6 + r7
            int r7 = r0.width
            int r6 = getChildMeasureSpec(r11, r6, r7)
            int r7 = r0.topMargin
            int r8 = r0.bottomMargin
            int r7 = r7 + r8
            int r0 = r0.height
            int r0 = getChildMeasureSpec(r12, r7, r0)
            r5.measure(r6, r0)
            goto L_0x0066
        L_0x00ce:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "Child "
            r1.<init>(r2)
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r2 = " at index "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r2 = " does not have a valid layout_gravity - must be Gravity.LEFT, Gravity.RIGHT or Gravity.NO_GRAVITY"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x00f3:
            return
        L_0x00f4:
            r1 = r0
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.DrawerLayout.onMeasure(int, int):void");
    }

    /* access modifiers changed from: protected */
    public final void onRestoreInstanceState(Parcelable parcelable) {
        View a2;
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (!(savedState.f385a == 0 || (a2 = a(savedState.f385a)) == null)) {
            g(a2);
        }
        a(savedState.b, 3);
        a(savedState.c, 5);
    }

    /* access modifiers changed from: protected */
    public final Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        int childCount = getChildCount();
        int i2 = 0;
        while (true) {
            if (i2 >= childCount) {
                break;
            }
            View childAt = getChildAt(i2);
            if (c(childAt)) {
                b bVar = (b) childAt.getLayoutParams();
                if (bVar.d) {
                    savedState.f385a = bVar.f389a;
                    break;
                }
            }
            i2++;
        }
        savedState.b = this.m;
        savedState.c = this.n;
        return savedState;
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        View a2;
        boolean z = false;
        this.f.b(motionEvent);
        this.g.b(motionEvent);
        switch (motionEvent.getAction() & PurchaseCode.AUTH_INVALID_APP) {
            case 0:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                this.q = x;
                this.r = y;
                this.o = false;
                break;
            case 1:
                float x2 = motionEvent.getX();
                float y2 = motionEvent.getY();
                View b2 = this.f.b((int) x2, (int) y2);
                if (b2 != null && f(b2)) {
                    float f2 = x2 - this.q;
                    float f3 = y2 - this.r;
                    int d2 = this.f.d();
                    if ((f2 * f2) + (f3 * f3) < ((float) (d2 * d2)) && (a2 = a()) != null) {
                        if (a(a2) == 2) {
                            z = true;
                        }
                        a(z);
                        break;
                    }
                }
                z = true;
                a(z);
            case 3:
                a(true);
                this.o = false;
                break;
        }
        return true;
    }

    public final void requestDisallowInterceptTouchEvent(boolean z) {
        super.requestDisallowInterceptTouchEvent(z);
        if (z) {
            a(true);
        }
    }

    public final void requestLayout() {
        if (!this.k) {
            super.requestLayout();
        }
    }

    public final void setDrawerListener$1b20c458(q qVar) {
        this.p = qVar;
    }

    public final void setDrawerLockMode(int i2) {
        a(i2, 3);
        a(i2, 5);
    }

    public final void setScrimColor(int i2) {
        this.c = i2;
        invalidate();
    }
}
