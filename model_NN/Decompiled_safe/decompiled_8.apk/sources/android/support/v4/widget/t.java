package android.support.v4.widget;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.widget.SlidingPaneLayout;

final class t implements Parcelable.Creator {
    t() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new SlidingPaneLayout.SavedState(parcel, (byte) 0);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new SlidingPaneLayout.SavedState[i];
    }
}
