package android.support.v4.widget;

import android.content.Context;
import android.os.Build;
import android.view.animation.Interpolator;

public final class k {
    private static l b;

    /* renamed from: a  reason: collision with root package name */
    private Object f393a;

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 14) {
            b = new o();
        } else if (i >= 9) {
            b = new n();
        } else {
            b = new m();
        }
    }

    private k(Context context, Interpolator interpolator) {
        this.f393a = b.a(context, interpolator);
    }

    public static k a(Context context, Interpolator interpolator) {
        return new k(context, interpolator);
    }

    public final void a(int i, int i2, int i3, int i4, int i5) {
        b.a(this.f393a, i, i2, i3, i4, i5);
    }

    public final boolean a() {
        return b.a(this.f393a);
    }

    public final int b() {
        return b.b(this.f393a);
    }

    public final int c() {
        return b.c(this.f393a);
    }

    public final int d() {
        return b.f(this.f393a);
    }

    public final int e() {
        return b.g(this.f393a);
    }

    public final boolean f() {
        return b.d(this.f393a);
    }

    public final void g() {
        b.e(this.f393a);
    }
}
