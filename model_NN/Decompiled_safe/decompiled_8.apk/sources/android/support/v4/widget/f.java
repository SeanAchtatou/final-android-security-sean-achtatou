package android.support.v4.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Build;

public final class f {
    private static final i b;

    /* renamed from: a  reason: collision with root package name */
    private Object f392a;

    static {
        if (Build.VERSION.SDK_INT >= 14) {
            b = new h();
        } else {
            b = new g();
        }
    }

    public f(Context context) {
        this.f392a = b.a(context);
    }

    public final void a(int i, int i2) {
        b.a(this.f392a, i, i2);
    }

    public final boolean a() {
        return b.a(this.f392a);
    }

    public final boolean a(float f) {
        return b.a(this.f392a, f);
    }

    public final boolean a(Canvas canvas) {
        return b.a(this.f392a, canvas);
    }

    public final void b() {
        b.b(this.f392a);
    }

    public final boolean c() {
        return b.c(this.f392a);
    }
}
