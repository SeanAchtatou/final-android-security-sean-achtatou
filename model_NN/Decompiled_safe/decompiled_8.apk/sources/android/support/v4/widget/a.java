package android.support.v4.widget;

import android.graphics.Rect;
import android.support.v4.view.ah;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;

final class a extends android.support.v4.view.a {

    /* renamed from: a  reason: collision with root package name */
    private final Rect f388a = new Rect();
    private /* synthetic */ DrawerLayout b;

    a(DrawerLayout drawerLayout) {
        this.b = drawerLayout;
    }

    private boolean b(View view) {
        View a2 = this.b.a();
        return (a2 == null || a2 == view) ? false : true;
    }

    public final void a(View view, android.support.v4.view.a.a aVar) {
        android.support.v4.view.a.a a2 = android.support.v4.view.a.a.a(aVar);
        super.a(view, a2);
        aVar.a(view);
        ViewParent g = ah.g(view);
        if (g instanceof View) {
            aVar.c((View) g);
        }
        Rect rect = this.f388a;
        a2.a(rect);
        aVar.b(rect);
        a2.c(rect);
        aVar.d(rect);
        aVar.c(a2.f());
        aVar.a(a2.l());
        aVar.b(a2.m());
        aVar.c(a2.n());
        aVar.h(a2.k());
        aVar.f(a2.i());
        aVar.a(a2.d());
        aVar.b(a2.e());
        aVar.d(a2.g());
        aVar.e(a2.h());
        aVar.g(a2.j());
        aVar.a(a2.b());
        a2.o();
        int childCount = this.b.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = this.b.getChildAt(i);
            if (!b(childAt)) {
                aVar.b(childAt);
            }
        }
    }

    public final boolean a(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        if (!b(view)) {
            return super.a(viewGroup, view, accessibilityEvent);
        }
        return false;
    }
}
