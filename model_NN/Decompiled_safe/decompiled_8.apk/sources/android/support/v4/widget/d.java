package android.support.v4.widget;

import android.view.View;

final class d extends z {

    /* renamed from: a  reason: collision with root package name */
    private final int f390a;
    private w b;
    private final Runnable c = new e(this);
    private /* synthetic */ DrawerLayout d;

    public d(DrawerLayout drawerLayout, int i) {
        this.d = drawerLayout;
        this.f390a = i;
    }

    static /* synthetic */ void a(d dVar) {
        View view;
        int i;
        int i2 = 0;
        int b2 = dVar.b.b();
        boolean z = dVar.f390a == 3;
        if (z) {
            View a2 = dVar.d.a(3);
            if (a2 != null) {
                i2 = -a2.getWidth();
            }
            int i3 = i2 + b2;
            view = a2;
            i = i3;
        } else {
            View a3 = dVar.d.a(5);
            int width = dVar.d.getWidth() - b2;
            view = a3;
            i = width;
        }
        if (view == null) {
            return;
        }
        if (((z && view.getLeft() < i) || (!z && view.getLeft() > i)) && dVar.d.a(view) == 0) {
            dVar.b.a(view, i, view.getTop());
            ((b) view.getLayoutParams()).c = true;
            dVar.d.invalidate();
            dVar.c();
            dVar.d.b();
        }
    }

    private void c() {
        int i = 3;
        if (this.f390a == 3) {
            i = 5;
        }
        View a2 = this.d.a(i);
        if (a2 != null) {
            this.d.d(a2);
        }
    }

    public final void a() {
        this.d.removeCallbacks(this.c);
    }

    public final void a(int i) {
        DrawerLayout drawerLayout = this.d;
        int i2 = this.f390a;
        drawerLayout.a(i, this.b.c());
    }

    public final void a(int i, int i2) {
        View a2 = (i & 1) == 1 ? this.d.a(3) : this.d.a(5);
        if (a2 != null && this.d.a(a2) == 0) {
            this.b.a(a2, i2);
        }
    }

    public final void a(w wVar) {
        this.b = wVar;
    }

    public final void a(View view, float f) {
        int width;
        DrawerLayout drawerLayout = this.d;
        float b2 = DrawerLayout.b(view);
        int width2 = view.getWidth();
        DrawerLayout drawerLayout2 = this.d;
        if (DrawerLayout.a(view, 3)) {
            width = (f > 0.0f || (f == 0.0f && b2 > 0.5f)) ? 0 : -width2;
        } else {
            width = this.d.getWidth();
            if (f < 0.0f || (f == 0.0f && b2 < 0.5f)) {
                width -= width2;
            }
        }
        this.b.a(width, view.getTop());
        this.d.invalidate();
    }

    public final void a(View view, int i) {
        int width = view.getWidth();
        DrawerLayout drawerLayout = this.d;
        float width2 = DrawerLayout.a(view, 3) ? ((float) (width + i)) / ((float) width) : ((float) (this.d.getWidth() - i)) / ((float) width);
        this.d.a(view, width2);
        view.setVisibility(width2 == 0.0f ? 4 : 0);
        this.d.invalidate();
    }

    public final boolean a(View view) {
        DrawerLayout drawerLayout = this.d;
        if (DrawerLayout.c(view)) {
            DrawerLayout drawerLayout2 = this.d;
            return DrawerLayout.a(view, this.f390a) && this.d.a(view) == 0;
        }
    }

    public final int b(View view, int i) {
        DrawerLayout drawerLayout = this.d;
        if (DrawerLayout.a(view, 3)) {
            return Math.max(-view.getWidth(), Math.min(i, 0));
        }
        int width = this.d.getWidth();
        return Math.max(width - view.getWidth(), Math.min(i, width));
    }

    public final void b() {
        this.d.postDelayed(this.c, 160);
    }

    public final void b(View view) {
        ((b) view.getLayoutParams()).c = false;
        c();
    }

    public final int c(View view) {
        return view.getWidth();
    }

    public final int d(View view) {
        return view.getTop();
    }
}
