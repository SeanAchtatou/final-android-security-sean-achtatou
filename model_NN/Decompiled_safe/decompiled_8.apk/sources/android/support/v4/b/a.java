package android.support.v4.b;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Parcelable;
import android.support.v4.view.bf;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import com.amap.mapapi.map.MapView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.baidu.location.BDLocation;
import com.immomo.a.a.b.e;
import com.immomo.a.a.c;
import com.immomo.a.a.d.b;
import com.immomo.a.a.d.d;
import com.immomo.a.a.f;
import com.immomo.a.a.h;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.contacts.CommunityPeopleActivity;
import com.immomo.momo.android.view.bp;
import com.immomo.momo.g;
import com.immomo.momo.protocol.imjson.l;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.q;
import com.jcraft.jzlib.GZIPHeader;
import com.mapabc.minimap.map.vmap.NativeMapEngine;
import com.mapabc.minimap.map.vmap.VMapProjection;
import com.sina.weibo.sdk.constant.Constants;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import mm.purchasesdk.PurchaseCode;
import org.json.JSONArray;
import org.json.JSONObject;

public class a implements bf, d, f, h {

    /* renamed from: a  reason: collision with root package name */
    private com.immomo.a.a.a f349a;
    private Lock b;
    private Condition c;
    private b d;
    private boolean e;
    private Exception f;
    private boolean g;

    public a() {
    }

    public a(com.immomo.a.a.a aVar) {
        this.f349a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = false;
        this.g = false;
        com.immomo.a.a.a.a().a("Authentication");
        this.f349a = aVar;
        this.b = new ReentrantLock();
        this.c = this.b.newCondition();
        this.f349a.b("conn", this);
        this.f349a.b("disconn", this);
    }

    private static Bitmap a(ContentResolver contentResolver, Uri uri, int i, int i2) {
        InputStream inputStream;
        Throwable th;
        double d2;
        double d3;
        int i3 = 1;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            inputStream = contentResolver.openInputStream(uri);
            try {
                BitmapFactory.decodeStream(inputStream, null, options);
                int i4 = options.outWidth;
                int i5 = options.outHeight;
                inputStream.close();
                if (i4 > i || i5 > i2) {
                    while (true) {
                        if (i4 / i3 <= i && i5 / i3 <= i2) {
                            break;
                        }
                        i3++;
                    }
                    options.inSampleSize = i3 - 1;
                    options.inJustDecodeBounds = false;
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    options.inPurgeable = true;
                    InputStream openInputStream = contentResolver.openInputStream(uri);
                    try {
                        Bitmap decodeStream = BitmapFactory.decodeStream(openInputStream, null, options);
                        if (decodeStream == null) {
                            a((Closeable) openInputStream);
                            return null;
                        }
                        int width = decodeStream.getWidth();
                        int height = decodeStream.getHeight();
                        if (((double) width) / ((double) i) < ((double) height) / ((double) i2)) {
                            d3 = (double) i2;
                            d2 = (d3 / ((double) height)) * ((double) width);
                        } else {
                            d2 = (double) i;
                            d3 = (d2 / ((double) width)) * ((double) height);
                        }
                        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(decodeStream, Math.round((float) d2), Math.round((float) d3), true);
                        decodeStream.recycle();
                        a((Closeable) openInputStream);
                        return createScaledBitmap;
                    } catch (Throwable th2) {
                        th = th2;
                        inputStream = openInputStream;
                        a((Closeable) inputStream);
                        throw th;
                    }
                } else {
                    options.inJustDecodeBounds = false;
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    InputStream openInputStream2 = contentResolver.openInputStream(uri);
                    Bitmap decodeStream2 = BitmapFactory.decodeStream(openInputStream2, null, options);
                    openInputStream2.close();
                    a((Closeable) openInputStream2);
                    return decodeStream2;
                }
            } catch (Throwable th3) {
                th = th3;
                a((Closeable) inputStream);
                throw th;
            }
        } catch (Throwable th4) {
            Throwable th5 = th4;
            inputStream = null;
            th = th5;
            a((Closeable) inputStream);
            throw th;
        }
    }

    public static Bitmap a(Bitmap bitmap, float f2) {
        if (bitmap == null) {
            return null;
        }
        Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        Paint paint = new Paint();
        Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        RectF rectF = new RectF(rect);
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(-12434878);
        canvas.drawRoundRect(rectF, f2, f2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return createBitmap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap a(Bitmap bitmap, float f2, boolean z) {
        Bitmap bitmap2 = null;
        if (bitmap != null) {
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            int i = width > height ? height : width;
            Matrix matrix = new Matrix();
            matrix.setScale(f2 / ((float) i), f2 / ((float) i));
            bitmap2 = Bitmap.createBitmap(bitmap, (width - i) / 2, (height - i) / 2, i, i, matrix, true);
            if (bitmap2.hashCode() != bitmap.hashCode() && z) {
                bitmap.recycle();
            }
        }
        return bitmap2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap a(Bitmap bitmap, int i, int i2) {
        if (bitmap == null) {
            return null;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Matrix matrix = new Matrix();
        matrix.postScale(((float) i) / ((float) width), ((float) i2) / ((float) height));
        return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private static Bitmap a(Bitmap bitmap, File file) {
        int i = 0;
        if (bitmap == null) {
            return null;
        }
        try {
            switch (new ExifInterface(file.getAbsolutePath()).getAttributeInt("Orientation", 1)) {
                case 3:
                    i = 180;
                    break;
                case 6:
                    i = 90;
                    break;
                case 8:
                    i = PurchaseCode.AUTH_OVER_COMSUMPTION;
                    break;
            }
            if (i == 0) {
                return bitmap;
            }
            Matrix matrix = new Matrix();
            matrix.postRotate((float) i);
            Bitmap createBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            try {
                bitmap.recycle();
                return createBitmap;
            } catch (IOException e2) {
                bitmap = createBitmap;
                e = e2;
                e.printStackTrace();
                return bitmap;
            }
        } catch (IOException e3) {
            e = e3;
            e.printStackTrace();
            return bitmap;
        }
    }

    public static Bitmap a(Uri uri, Context context, int i, int i2) {
        while (true) {
            if (uri.getPath().startsWith("file://") || new File(uri.getPath()).exists()) {
                Bitmap a2 = a(new File(uri.getPath()), i, i2);
                if (a2 != null) {
                    return a2;
                }
            } else {
                Bitmap a3 = a(context.getContentResolver(), uri, i, i2);
                if (a3 != null) {
                    return a3;
                }
            }
            if (i * i2 <= 230400) {
                return null;
            }
            i -= 200;
            i2 -= 200;
            if (i < 480) {
                i = 480;
            }
            if (i2 < 480) {
                i2 = 480;
            }
            System.gc();
        }
    }

    public static Bitmap a(File file, int i, int i2) {
        Bitmap decodeFile;
        double d2;
        double d3;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file.getAbsolutePath(), options);
        int i3 = 1;
        while (true) {
            if (options.outWidth / i3 <= i && options.outHeight / i3 <= i2) {
                break;
            }
            i3++;
        }
        if (i3 > 1) {
            BitmapFactory.Options options2 = new BitmapFactory.Options();
            options2.inSampleSize = i3 - 1;
            options2.inPreferredConfig = Bitmap.Config.ARGB_8888;
            options2.inPurgeable = true;
            Bitmap decodeFile2 = BitmapFactory.decodeFile(file.getAbsolutePath(), options2);
            if (decodeFile2 == null) {
                return null;
            }
            int width = decodeFile2.getWidth();
            int height = decodeFile2.getHeight();
            if (((double) width) / ((double) i) < ((double) height) / ((double) i2)) {
                d3 = (double) i2;
                d2 = (d3 / ((double) height)) * ((double) width);
            } else {
                d2 = (double) i;
                d3 = (d2 / ((double) width)) * ((double) height);
            }
            decodeFile = Bitmap.createScaledBitmap(decodeFile2, Math.round((float) d2), Math.round((float) d3), true);
            decodeFile2.recycle();
            System.gc();
        } else {
            decodeFile = BitmapFactory.decodeFile(file.getAbsolutePath());
        }
        return a(decodeFile, file);
    }

    public static Parcelable.Creator a(c cVar) {
        if (Build.VERSION.SDK_INT >= 13) {
            new d(cVar);
        }
        return new b(cVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap
     arg types: [android.graphics.Bitmap, int, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.io.File, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap */
    public static File a(File file) {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(file.getPath(), options);
            Bitmap a2 = a(BitmapFactory.decodeFile(file.getPath()), 150.0f, true);
            File file2 = new File(file.getParent(), String.valueOf(file.getName()) + "_");
            FileOutputStream fileOutputStream = new FileOutputStream(file2);
            a2.compress(Bitmap.CompressFormat.JPEG, 85, fileOutputStream);
            a2.recycle();
            a(fileOutputStream);
            return file2;
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public static String a() {
        return b("android " + Build.VERSION.RELEASE);
    }

    public static String a(double d2) {
        return (d2 >= 100.0d ? new DecimalFormat("0") : new DecimalFormat("0.00")).format(Math.abs(d2));
    }

    public static String a(float f2) {
        return (f2 >= 100.0f ? new DecimalFormat("0") : new DecimalFormat("0.00")).format((double) Math.abs(f2));
    }

    public static String a(int i, int i2) {
        return ((i != 1 || i2 < 20) && (i != 2 || i2 > 18)) ? ((i != 2 || i2 < 19) && (i != 3 || i2 > 20)) ? ((i != 3 || i2 < 21) && (i != 4 || i2 > 19)) ? ((i != 4 || i2 < 20) && (i != 5 || i2 > 20)) ? ((i != 5 || i2 < 21) && (i != 6 || i2 > 21)) ? ((i != 6 || i2 < 22) && (i != 7 || i2 > 22)) ? ((i != 7 || i2 < 23) && (i != 8 || i2 > 22)) ? ((i != 8 || i2 < 23) && (i != 9 || i2 > 22)) ? ((i != 9 || i2 < 23) && (i != 10 || i2 > 23)) ? ((i != 10 || i2 < 24) && (i != 11 || i2 > 22)) ? ((i != 11 || i2 < 23) && (i != 12 || i2 > 21)) ? ((i != 12 || i2 < 22) && (i != 1 || i2 > 19)) ? PoiTypeDef.All : "摩羯座" : "射手座" : "天蝎座" : "天秤座" : "处女座" : "狮子座" : "巨蟹座" : "双子座" : "金牛座" : "白羊座" : "双鱼座" : "水瓶座";
    }

    public static String a(Context context) {
        return b(((TelephonyManager) context.getSystemService("phone")).getDeviceId());
    }

    public static String a(Message message) {
        return !f(message.fileName) ? message.msgId : message.fileName.indexOf("://") > 0 ? message.msgId : message.fileName;
    }

    public static String a(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            char[] charArray = str.toCharArray();
            byte[] bArr = new byte[charArray.length];
            for (int i = 0; i < charArray.length; i++) {
                bArr[i] = (byte) charArray[i];
            }
            byte[] digest = instance.digest(bArr);
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b2 : digest) {
                byte b3 = b2 & GZIPHeader.OS_UNKNOWN;
                if (b3 < 16) {
                    stringBuffer.append("0");
                }
                stringBuffer.append(Integer.toHexString(b3));
            }
            return stringBuffer.toString();
        } catch (Exception e2) {
            e2.printStackTrace();
            return PoiTypeDef.All;
        }
    }

    public static String a(String str, String str2, String str3) {
        char c2 = '?';
        if (str2 == null || str3 == null) {
            return str;
        }
        String trim = str3.trim();
        if (trim.length() == 0) {
            return str;
        }
        try {
            trim = URLEncoder.encode(trim, "utf-8");
        } catch (UnsupportedEncodingException e2) {
        }
        int indexOf = str.indexOf(63, str.lastIndexOf(47) + 1);
        if (indexOf != -1) {
            c2 = '&';
        }
        while (indexOf != -1) {
            int i = indexOf + 1;
            int indexOf2 = str.indexOf(61, i);
            indexOf = str.indexOf(38, i);
            if (indexOf2 != -1 && str.substring(i, indexOf2).equals(str2)) {
                int length = indexOf != -1 ? indexOf : str.length();
                return !str.substring(indexOf2 + 1, length).equals(trim) ? new StringBuilder(str).replace(indexOf2 + 1, length, trim).toString() : str;
            }
        }
        return str + c2 + str2 + '=' + trim;
    }

    public static String a(Collection collection, String str) {
        if (collection == null) {
            return PoiTypeDef.All;
        }
        Iterator it = collection.iterator();
        if (it == null) {
            return null;
        }
        if (!it.hasNext()) {
            return PoiTypeDef.All;
        }
        Object next = it.next();
        if (!it.hasNext()) {
            return next != null ? next.toString() : PoiTypeDef.All;
        }
        StringBuffer stringBuffer = new StringBuffer(256);
        if (next != null) {
            stringBuffer.append(next);
        }
        while (it.hasNext()) {
            if (str != null) {
                stringBuffer.append(str);
            }
            Object next2 = it.next();
            if (next2 != null) {
                stringBuffer.append(next2);
            }
        }
        return stringBuffer.toString();
    }

    public static String a(Date date) {
        long j = 1;
        Date date2 = new Date();
        if (date == null) {
            return "未知";
        }
        if (date2.before(date)) {
            return "1分钟前";
        }
        long abs = (Math.abs(date2.getTime() - date.getTime()) / 1000) / 60;
        long j2 = abs / 60;
        long j3 = abs % 60;
        if (j2 >= 720) {
            return "30天前";
        }
        if (j2 >= 24) {
            return String.valueOf(((int) j2) / 24) + "天前";
        }
        if (j2 > 0) {
            return String.valueOf(j2) + "小时前";
        }
        if (j3 >= 1) {
            j = j3;
        }
        return String.valueOf(j) + "分钟前";
    }

    public static String a(Date date, boolean z) {
        long j = 1;
        if (date == null) {
            return "未知";
        }
        Date date2 = new Date();
        if (date2.before(date)) {
            return "1分钟前";
        }
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        int i = instance.get(1);
        int i2 = instance.get(6);
        Calendar instance2 = Calendar.getInstance();
        instance2.setTime(date2);
        int i3 = instance2.get(1);
        int i4 = instance2.get(6);
        if (i3 > i) {
            return z ? f(date) : i(date);
        }
        if (i4 - i2 > 1) {
            return z ? f(date) : i(date);
        }
        if (i4 - i2 == 1) {
            return "昨天 " + j(date);
        }
        long abs = (Math.abs(date2.getTime() - date.getTime()) / 1000) / 60;
        long j2 = abs / 60;
        long j3 = abs % 60;
        if (j2 >= 1) {
            return j(date);
        }
        if (j3 >= 1) {
            j = j3;
        }
        return String.valueOf(j) + "分钟前";
    }

    public static String a(byte[] bArr) {
        char[] cArr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(bArr);
            char[] cArr2 = new char[(r4 * 2)];
            int i = 0;
            for (byte b2 : instance.digest()) {
                int i2 = i + 1;
                cArr2[i] = cArr[(b2 >>> 4) & 15];
                i = i2 + 1;
                cArr2[i2] = cArr[b2 & 15];
            }
            return new String(cArr2);
        } catch (Exception e2) {
            return null;
        }
    }

    public static String a(Object[] objArr, String str) {
        if (objArr == null) {
            return null;
        }
        return a(objArr, str, PoiTypeDef.All, objArr.length);
    }

    public static String a(Object[] objArr, String str, String str2) {
        if (objArr == null) {
            return null;
        }
        return a(objArr, str2, str, objArr.length);
    }

    private static String a(Object[] objArr, String str, String str2, int i) {
        if (objArr == null) {
            return null;
        }
        if (str == null) {
            str = PoiTypeDef.All;
        }
        int i2 = i + 0;
        if (i2 <= 0) {
            return PoiTypeDef.All;
        }
        StringBuffer stringBuffer = new StringBuffer(((objArr[0] == null ? 16 : objArr[0].toString().length()) + str.length()) * i2);
        for (int i3 = 0; i3 < i; i3++) {
            if (i3 > 0) {
                stringBuffer.append(str);
            }
            if (objArr[i3] != null) {
                stringBuffer.append(String.valueOf(str2) + objArr[i3] + str2);
            }
        }
        return stringBuffer.toString();
    }

    public static Date a(long j) {
        if (j <= 0) {
            return null;
        }
        Date date = new Date();
        date.setTime(1000 * j);
        return date;
    }

    public static JSONObject a(String str, String str2) {
        JSONObject jSONObject = new JSONObject();
        try {
            String[] split = str.split(str2);
            for (int i = 0; i < split.length; i++) {
                String[] split2 = split[i].split("=");
                jSONObject.put(split2[0], split[i].substring(split2[0].length() + 1));
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return jSONObject;
    }

    public static void a(float f2, int[] iArr) {
        float f3 = ((-((float) Math.sqrt(2.0d))) * f2) / 64.0f;
        float f4 = (-f3) / 128.0f;
        for (int i = 0; i <= 128; i++) {
            iArr[i] = e((int) (0.5f + (((float) i) * (1.0f + (((float) i) * f4) + f3))));
        }
        for (int i2 = 129; i2 < 256; i2++) {
            int i3 = i2 - 128;
            iArr[i2] = e((int) ((((float) i2) - ((((float) i3) * f4) * ((float) i3))) - (((float) i3) * f3)));
        }
    }

    public static void a(Activity activity, int i) {
        Intent intent = new Intent();
        intent.setClass(activity, CommunityPeopleActivity.class);
        intent.putExtra("type", i);
        intent.putExtra("from", 11);
        activity.startActivity(intent);
    }

    public static void a(View view, Runnable runnable) {
        if (Build.VERSION.SDK_INT >= 16) {
            view.postOnAnimation(runnable);
        } else {
            view.postDelayed(runnable, 16);
        }
    }

    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e2) {
            }
        }
    }

    public static void a(File file, File file2) {
        ZipInputStream zipInputStream = new ZipInputStream(new CheckedInputStream(new FileInputStream(file), new CRC32()));
        a(file2, zipInputStream);
        zipInputStream.close();
    }

    private static void a(File file, ZipInputStream zipInputStream) {
        while (true) {
            ZipEntry nextEntry = zipInputStream.getNextEntry();
            if (nextEntry != null) {
                if (nextEntry.isDirectory()) {
                    new File(String.valueOf(file.getPath()) + File.separator + nextEntry.getName()).mkdirs();
                } else {
                    File file2 = new File(String.valueOf(file.getPath()) + File.separator + nextEntry.getName() + "_");
                    b(file2);
                    b(file2, zipInputStream);
                    if (file2.exists() && file2.getParentFile().getName().equals("middle")) {
                        q.a(file2, file2);
                    }
                }
                zipInputStream.closeEntry();
            } else {
                return;
            }
        }
    }

    public static void a(Object obj, StringBuilder sb) {
        int lastIndexOf;
        if (obj == null) {
            sb.append("null");
            return;
        }
        String simpleName = obj.getClass().getSimpleName();
        if ((simpleName == null || simpleName.length() <= 0) && (lastIndexOf = (simpleName = obj.getClass().getName()).lastIndexOf(46)) > 0) {
            simpleName = simpleName.substring(lastIndexOf + 1);
        }
        sb.append(simpleName);
        sb.append('{');
        sb.append(Integer.toHexString(System.identityHashCode(obj)));
    }

    private static boolean a(Context context, String str) {
        return context.getPackageManager().checkPermission(str, context.getPackageName()) == 0;
    }

    public static boolean a(b bVar, Message message) {
        int optInt = bVar.optInt("type");
        if (optInt == 1) {
            switch (bVar.optInt("style")) {
                case 1:
                    message.contentType = 3;
                    message.setContent(bVar.e());
                    break;
                case 2:
                    message.contentType = 5;
                    message.status = 4;
                    message.setContent(bVar.e());
                    break;
                default:
                    message.setContent(bVar.e());
                    message.contentType = 0;
                    break;
            }
        } else if (optInt == 3) {
            message.contentType = 4;
            Uri parse = Uri.parse(bVar.e());
            message.fileName = parse.getQueryParameter("file");
            message.setFileSize(parse.getQueryParameter("filesize"));
            message.setAudiotime(parse.getQueryParameter("audiotime"));
            message.expandedName = parse.getQueryParameter("ext");
        } else if (optInt == 2) {
            message.contentType = 1;
            Uri parse2 = Uri.parse(bVar.e());
            message.fileName = parse2.getQueryParameter("file");
            message.setFileSize(parse2.getQueryParameter("filesize"));
        } else if (optInt == 4) {
            message.contentType = 2;
            Uri parse3 = Uri.parse(bVar.e());
            message.setConvertLat(parse3.getQueryParameter("lat"));
            message.setConvertLng(parse3.getQueryParameter("lng"));
            message.setConvertAcc(parse3.getQueryParameter("acc"));
            message.setLat(parse3.getQueryParameter("orilat"));
            message.setLng(parse3.getQueryParameter("orilng"));
            message.setAcc(parse3.getQueryParameter("oriacc"));
        } else if (optInt == 5) {
            message.setContent(bVar.e());
            message.contentType = 6;
        } else if (optInt != 6) {
            return false;
        } else {
            message.contentType = 7;
            String e2 = bVar.e();
            String[] split = e2.substring(1, e2.length() - 1).split("\\|");
            message.setContent(split[0]);
            HashMap hashMap = new HashMap();
            for (int i = 1; i < split.length; i++) {
                String[] split2 = split[i].split("=");
                if (split2.length > 1) {
                    hashMap.put(split2[0], split2[1]);
                } else {
                    hashMap.put(split2[0], PoiTypeDef.All);
                }
            }
            message.fileName = (String) hashMap.get("n");
            message.layout = Integer.parseInt((String) hashMap.get("lt"));
            String[] split3 = ((String) hashMap.get("s")).split("x");
            if (split3.length == 2) {
                message.imageWidth = Integer.parseInt(split3[0]);
                message.imageHeight = Integer.parseInt(split3[1]);
            }
            JSONArray jSONArray = new JSONArray(bVar.getString("actions"));
            if (jSONArray.length() > 0) {
                message.setAction(jSONArray.getString(0));
            }
        }
        message.tailTitle = bVar.optString("re_title");
        message.tailIcon = bVar.optString("re_photo");
        message.tailAction = bVar.optString("re_action");
        return true;
    }

    public static boolean a(CharSequence charSequence) {
        return charSequence == null || charSequence.length() == 0;
    }

    public static boolean a(Double d2) {
        return (d2 == null || d2.doubleValue() == 0.0d) ? false : true;
    }

    public static boolean a(String[] strArr) {
        return (strArr == null || strArr.length == 0) ? false : true;
    }

    public static byte[] a(InputStream inputStream) {
        byte[] bArr = new byte[1024];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                byteArrayOutputStream.close();
                inputStream.close();
                return byteArray;
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(byte[], boolean):int[]
     arg types: [byte[], int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, float):android.graphics.Bitmap
      android.support.v4.b.a.a(android.graphics.Bitmap, java.io.File):android.graphics.Bitmap
      android.support.v4.b.a.a(int, int):java.lang.String
      android.support.v4.b.a.a(java.util.Collection, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.String, java.lang.String):org.json.JSONObject
      android.support.v4.b.a.a(float, int[]):void
      android.support.v4.b.a.a(android.app.Activity, int):void
      android.support.v4.b.a.a(android.view.View, java.lang.Runnable):void
      android.support.v4.b.a.a(java.io.File, java.io.File):void
      android.support.v4.b.a.a(java.io.File, java.util.zip.ZipInputStream):void
      android.support.v4.b.a.a(java.lang.Object, java.lang.StringBuilder):void
      android.support.v4.b.a.a(android.content.Context, java.lang.String):boolean
      android.support.v4.b.a.a(com.immomo.a.a.d.b, com.immomo.momo.service.bean.Message):boolean
      android.support.v4.b.a.a(byte[], java.lang.String):byte[]
      android.support.v4.b.a.a(byte[], byte[]):byte[]
      android.support.v4.b.a.a(int[], boolean):byte[]
      android.support.v4.b.a.a(int, float):void
      android.support.v4.b.a.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.view.bf.a(int, float):void
      com.immomo.a.a.f.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.b.a.a(byte[], boolean):int[] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(int[], boolean):byte[]
     arg types: [int[], int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, float):android.graphics.Bitmap
      android.support.v4.b.a.a(android.graphics.Bitmap, java.io.File):android.graphics.Bitmap
      android.support.v4.b.a.a(int, int):java.lang.String
      android.support.v4.b.a.a(java.util.Collection, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.String, java.lang.String):org.json.JSONObject
      android.support.v4.b.a.a(float, int[]):void
      android.support.v4.b.a.a(android.app.Activity, int):void
      android.support.v4.b.a.a(android.view.View, java.lang.Runnable):void
      android.support.v4.b.a.a(java.io.File, java.io.File):void
      android.support.v4.b.a.a(java.io.File, java.util.zip.ZipInputStream):void
      android.support.v4.b.a.a(java.lang.Object, java.lang.StringBuilder):void
      android.support.v4.b.a.a(android.content.Context, java.lang.String):boolean
      android.support.v4.b.a.a(com.immomo.a.a.d.b, com.immomo.momo.service.bean.Message):boolean
      android.support.v4.b.a.a(byte[], java.lang.String):byte[]
      android.support.v4.b.a.a(byte[], byte[]):byte[]
      android.support.v4.b.a.a(byte[], boolean):int[]
      android.support.v4.b.a.a(int, float):void
      android.support.v4.b.a.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.view.bf.a(int, float):void
      com.immomo.a.a.f.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.b.a.a(int[], boolean):byte[] */
    public static byte[] a(byte[] bArr, String str) {
        int[] iArr;
        int[] iArr2;
        byte[] bytes = str.getBytes();
        if (bArr.length != 0) {
            int[] a2 = a(bArr, true);
            int[] a3 = a(bytes, false);
            int length = a2.length - 1;
            if (length <= 0) {
                iArr2 = a2;
            } else {
                if (a3.length < 4) {
                    iArr = new int[4];
                    System.arraycopy(a3, 0, iArr, 0, a3.length);
                } else {
                    iArr = a3;
                }
                int i = (52 / (length + 1)) + 6;
                int i2 = 0;
                int i3 = a2[length];
                while (true) {
                    int i4 = i - 1;
                    if (i <= 0) {
                        break;
                    }
                    i2 -= 1640531527;
                    int i5 = (i2 >>> 2) & 3;
                    int i6 = 0;
                    while (i6 < length) {
                        int i7 = a2[i6 + 1];
                        i3 = (((i3 ^ iArr[(i6 & 3) ^ i5]) + (i7 ^ i2)) ^ (((i3 >>> 5) ^ (i7 << 2)) + ((i7 >>> 3) ^ (i3 << 4)))) + a2[i6];
                        a2[i6] = i3;
                        i6++;
                    }
                    int i8 = a2[0];
                    i3 = a2[length] + (((iArr[(i6 & 3) ^ i5] ^ i3) + (i8 ^ i2)) ^ (((i3 >>> 5) ^ (i8 << 2)) + ((i8 >>> 3) ^ (i3 << 4))));
                    a2[length] = i3;
                    i = i4;
                }
                iArr2 = a2;
            }
            bArr = a(iArr2, false);
        }
        return b(bArr).getBytes();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(byte[], boolean):int[]
     arg types: [byte[], int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, float):android.graphics.Bitmap
      android.support.v4.b.a.a(android.graphics.Bitmap, java.io.File):android.graphics.Bitmap
      android.support.v4.b.a.a(int, int):java.lang.String
      android.support.v4.b.a.a(java.util.Collection, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.String, java.lang.String):org.json.JSONObject
      android.support.v4.b.a.a(float, int[]):void
      android.support.v4.b.a.a(android.app.Activity, int):void
      android.support.v4.b.a.a(android.view.View, java.lang.Runnable):void
      android.support.v4.b.a.a(java.io.File, java.io.File):void
      android.support.v4.b.a.a(java.io.File, java.util.zip.ZipInputStream):void
      android.support.v4.b.a.a(java.lang.Object, java.lang.StringBuilder):void
      android.support.v4.b.a.a(android.content.Context, java.lang.String):boolean
      android.support.v4.b.a.a(com.immomo.a.a.d.b, com.immomo.momo.service.bean.Message):boolean
      android.support.v4.b.a.a(byte[], java.lang.String):byte[]
      android.support.v4.b.a.a(byte[], byte[]):byte[]
      android.support.v4.b.a.a(int[], boolean):byte[]
      android.support.v4.b.a.a(int, float):void
      android.support.v4.b.a.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.view.bf.a(int, float):void
      com.immomo.a.a.f.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.b.a.a(byte[], boolean):int[] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(int[], boolean):byte[]
     arg types: [int[], int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, float):android.graphics.Bitmap
      android.support.v4.b.a.a(android.graphics.Bitmap, java.io.File):android.graphics.Bitmap
      android.support.v4.b.a.a(int, int):java.lang.String
      android.support.v4.b.a.a(java.util.Collection, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.util.Date, boolean):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.String, java.lang.String):org.json.JSONObject
      android.support.v4.b.a.a(float, int[]):void
      android.support.v4.b.a.a(android.app.Activity, int):void
      android.support.v4.b.a.a(android.view.View, java.lang.Runnable):void
      android.support.v4.b.a.a(java.io.File, java.io.File):void
      android.support.v4.b.a.a(java.io.File, java.util.zip.ZipInputStream):void
      android.support.v4.b.a.a(java.lang.Object, java.lang.StringBuilder):void
      android.support.v4.b.a.a(android.content.Context, java.lang.String):boolean
      android.support.v4.b.a.a(com.immomo.a.a.d.b, com.immomo.momo.service.bean.Message):boolean
      android.support.v4.b.a.a(byte[], java.lang.String):byte[]
      android.support.v4.b.a.a(byte[], byte[]):byte[]
      android.support.v4.b.a.a(byte[], boolean):int[]
      android.support.v4.b.a.a(int, float):void
      android.support.v4.b.a.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.view.bf.a(int, float):void
      com.immomo.a.a.f.a(java.lang.Object, com.immomo.a.a.f):void
      android.support.v4.b.a.a(int[], boolean):byte[] */
    public static byte[] a(byte[] bArr, byte[] bArr2) {
        int[] iArr;
        int[] iArr2;
        if (bArr.length == 0) {
            return bArr;
        }
        int[] a2 = a(bArr, false);
        int[] a3 = a(bArr2, false);
        int length = a2.length - 1;
        if (length <= 0) {
            iArr2 = a2;
        } else {
            if (a3.length < 4) {
                iArr = new int[4];
                System.arraycopy(a3, 0, iArr, 0, a3.length);
            } else {
                iArr = a3;
            }
            int i = ((52 / (length + 1)) + 6) * -1640531527;
            int i2 = a2[0];
            while (i != 0) {
                int i3 = (i >>> 2) & 3;
                int i4 = i2;
                int i5 = length;
                while (i5 > 0) {
                    int i6 = a2[i5 - 1];
                    i4 = a2[i5] - (((i4 ^ i) + (i6 ^ iArr[(i5 & 3) ^ i3])) ^ (((i6 >>> 5) ^ (i4 << 2)) + ((i4 >>> 3) ^ (i6 << 4))));
                    a2[i5] = i4;
                    i5--;
                }
                int i7 = a2[length];
                int i8 = a2[0] - (((iArr[(i5 & 3) ^ i3] ^ i7) + (i4 ^ i)) ^ (((i7 >>> 5) ^ (i4 << 2)) + ((i4 >>> 3) ^ (i7 << 4))));
                a2[0] = i8;
                i -= -1640531527;
                i2 = i8;
            }
            iArr2 = a2;
        }
        return a(iArr2, true);
    }

    private static byte[] a(int[] iArr, boolean z) {
        int length = z ? iArr[iArr.length - 1] : iArr.length << 2;
        if (length > 4096 || length <= 0) {
            return "!@#$()_+1602".getBytes();
        }
        byte[] bArr = new byte[length];
        for (int i = 0; i < length; i++) {
            bArr[i] = (byte) (iArr[i >>> 2] >>> ((i & 3) << 3));
        }
        return bArr;
    }

    private static int[] a(byte[] bArr, boolean z) {
        int[] iArr;
        int length = (bArr.length & 3) == 0 ? bArr.length >>> 2 : (bArr.length >>> 2) + 1;
        if (z) {
            iArr = new int[(length + 1)];
            iArr[length] = bArr.length;
        } else {
            iArr = new int[length];
        }
        int length2 = bArr.length;
        for (int i = 0; i < length2; i++) {
            int i2 = i >>> 2;
            iArr[i2] = iArr[i2] | ((bArr[i] & GZIPHeader.OS_UNKNOWN) << ((i & 3) << 3));
        }
        return iArr;
    }

    public static int b(Date date, boolean z) {
        return (!z && date != null && new Date().getTime() - date.getTime() < 900000) ? R.drawable.bg_chat_dis_active : R.drawable.bg_chat_dis_normal;
    }

    public static long b(Date date) {
        return date.getTime() / 1000;
    }

    public static String b(double d2) {
        return new DecimalFormat("#.#").format(Math.abs(d2));
    }

    public static String b(Context context) {
        String a2 = a(context);
        if (a2 != null && a2.length() > 0) {
            return String.valueOf(a(a2).toCharArray(), 7, 18);
        }
        String f2 = f(context);
        if (f2 != null && f2.length() != 0) {
            return String.valueOf(a(f2).toCharArray(), 7, 18);
        }
        String c2 = c(context);
        return (c2 == null || c2.length() == 0) ? String.valueOf(a(b(Build.MODEL)).toCharArray(), 7, 18) : String.valueOf(a(c2).toCharArray(), 7, 18);
    }

    public static String b(String str) {
        if (str == null) {
            return PoiTypeDef.All;
        }
        if (str.length() > 30) {
            str = str.substring(0, 29);
        }
        return str.replace("\\", PoiTypeDef.All).replace("|", PoiTypeDef.All);
    }

    public static String b(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (byte b2 : bArr) {
            String hexString = Integer.toHexString(b2 & GZIPHeader.OS_UNKNOWN);
            if (hexString.length() == 1) {
                stringBuffer.append("0").append(hexString);
            } else {
                stringBuffer.append(hexString);
            }
        }
        return stringBuffer.toString().toUpperCase();
    }

    private static void b(File file) {
        File parentFile = file.getParentFile();
        if (!parentFile.exists()) {
            b(parentFile);
            parentFile.mkdir();
        }
    }

    public static void b(File file, File file2) {
        FileOutputStream fileOutputStream;
        FileInputStream fileInputStream = null;
        try {
            FileInputStream fileInputStream2 = new FileInputStream(file);
            try {
                fileOutputStream = new FileOutputStream(file2);
                try {
                    byte[] bArr = new byte[2048];
                    while (true) {
                        int read = fileInputStream2.read(bArr);
                        if (read == -1) {
                            file.delete();
                            a((Closeable) fileInputStream2);
                            a(fileOutputStream);
                            return;
                        }
                        fileOutputStream.write(bArr, 0, read);
                    }
                } catch (Throwable th) {
                    th = th;
                    fileInputStream = fileInputStream2;
                }
            } catch (Throwable th2) {
                th = th2;
                fileOutputStream = null;
                fileInputStream = fileInputStream2;
                a((Closeable) fileInputStream);
                a(fileOutputStream);
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            fileOutputStream = null;
            a((Closeable) fileInputStream);
            a(fileOutputStream);
            throw th;
        }
    }

    private static void b(File file, ZipInputStream zipInputStream) {
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file));
        byte[] bArr = new byte[1024];
        while (true) {
            int read = zipInputStream.read(bArr, 0, 1024);
            if (read == -1) {
                bufferedOutputStream.close();
                return;
            }
            bufferedOutputStream.write(bArr, 0, read);
        }
    }

    public static byte[] b(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[1024];
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                byteArrayOutputStream.close();
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    public static String[] b(String str, String str2) {
        if (!a((CharSequence) str)) {
            return str.split(str2);
        }
        return null;
    }

    public static String c(int i) {
        switch (i) {
            case 11:
                return "北京市";
            case 12:
                return "天津市";
            case 13:
                return "河北省";
            case com.immomo.momo.h.DragSortListView_drag_handle_id /*14*/:
                return "山西省";
            case 15:
                return "内蒙古自治区";
            case 16:
            case 17:
            case 18:
            case 19:
            case VMapProjection.MAXZOOMLEVEL /*20*/:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 38:
            case 39:
            case 40:
            case 47:
            case MapView.LayoutParams.TOP /*48*/:
            case 49:
            case 55:
            case 56:
            case 57:
            case 58:
            case 59:
            case 60:
            case BDLocation.TypeOffLineLocation /*66*/:
            case BDLocation.TypeOffLineLocationFail /*67*/:
            case BDLocation.TypeOffLineLocationNetworkFail /*68*/:
            case 69:
            case 70:
            case 72:
            case 73:
            case 74:
            case 75:
            case 76:
            case 77:
            case 78:
            case 79:
            case MapView.LayoutParams.BOTTOM /*80*/:
            default:
                return PoiTypeDef.All;
            case 21:
                return "辽宁省";
            case Constants.WEIBO_SDK_VERSION /*22*/:
                return "吉林省";
            case 23:
                return "黑龙江省";
            case 31:
                return "上海市";
            case 32:
                return "江苏省";
            case 33:
                return "浙江省";
            case 34:
                return "安徽省";
            case 35:
                return "福建省";
            case 36:
                return "江西省";
            case 37:
                return "山东省";
            case 41:
                return "河南省";
            case 42:
                return "湖北省";
            case 43:
                return "湖南省";
            case 44:
                return "广东省";
            case 45:
                return "广西壮族自治区";
            case 46:
                return "海南省";
            case 50:
                return "重庆市";
            case MapView.LayoutParams.TOP_LEFT /*51*/:
                return "四川省";
            case 52:
                return "贵州省";
            case 53:
                return "云南省";
            case 54:
                return "西藏自治区";
            case BDLocation.TypeGpsLocation /*61*/:
                return "陕西省";
            case BDLocation.TypeCriteriaException /*62*/:
                return "甘肃省";
            case BDLocation.TypeNetWorkException /*63*/:
                return "青海省";
            case 64:
                return "宁夏回族自治区";
            case BDLocation.TypeCacheLocation /*65*/:
                return "新疆维吾尔自治区";
            case 71:
                return "台湾省";
            case MapView.LayoutParams.BOTTOM_CENTER /*81*/:
                return "香港特别行政区";
            case 82:
                return "澳门特别行政区";
        }
    }

    public static String c(Context context) {
        String str;
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager == null) {
            return PoiTypeDef.All;
        }
        try {
            String subscriberId = telephonyManager.getSubscriberId();
            str = subscriberId != null ? subscriberId.trim() : PoiTypeDef.All;
            try {
                Log.i("MobileUtils", str);
                return str;
            } catch (Exception e2) {
            }
        } catch (Exception e3) {
            str = PoiTypeDef.All;
            Log.i("MobileUtils", "can't not read imsi");
            return str;
        }
    }

    public static String c(String str) {
        return str.length() > 7 ? String.valueOf(str.substring(0, 3)) + "****" + str.substring(str.length() - 4, str.length()) : str;
    }

    public static String c(Date date) {
        return new SimpleDateFormat("yyyyMMddHHmmss").format(date);
    }

    public static boolean c(String str, String str2) {
        if (a((CharSequence) str) && a((CharSequence) str2)) {
            return true;
        }
        if (a((CharSequence) str)) {
            return false;
        }
        if (a((CharSequence) str2)) {
            return false;
        }
        return str.contains(str2) || str2.contains(str);
    }

    public static int d(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    public static Bitmap d(int i) {
        int a2 = g.a(12.0f);
        int a3 = g.a(4.0f);
        int a4 = g.a(2.0f);
        Bitmap createBitmap = Bitmap.createBitmap(a2, a3, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(i);
        canvas.drawRoundRect(new RectF(0.0f, 0.0f, (float) a2, (float) a3), (float) a4, (float) a4, paint);
        return createBitmap;
    }

    public static String d(String str) {
        return "+" + str.substring(str.indexOf("(") + 1, str.length() - 1);
    }

    public static String d(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }

    public static boolean d(String str, String str2) {
        if (a((CharSequence) str) && a((CharSequence) str2)) {
            return true;
        }
        if (a((CharSequence) str)) {
            return false;
        }
        if (a((CharSequence) str2)) {
            return false;
        }
        return str.startsWith(str2) || str2.startsWith(str);
    }

    public static String[] d() {
        return g.l().getStringArray(R.array.country_codes);
    }

    private static int e(int i) {
        if (i < 0) {
            return 0;
        }
        return i > 255 ? PurchaseCode.AUTH_INVALID_APP : i;
    }

    public static int e(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    public static String e(String str) {
        for (String d2 : d()) {
            String d3 = d(d2);
            if (str.indexOf(d3) == 0) {
                return d3;
            }
        }
        return PoiTypeDef.All;
    }

    public static String e(Date date) {
        return (date == null || date.getTime() == 0) ? "未知时间" : new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
    }

    private void e() {
        try {
            this.b.lock();
            this.e = false;
            this.f349a.a(this);
            long nanos = TimeUnit.SECONDS.toNanos(15);
            while (!this.g && !this.e && nanos > 0) {
                nanos = this.c.awaitNanos(nanos);
            }
            if (this.g) {
                throw new InterruptedException(this.d.toString());
            } else if (!this.e) {
                throw new e(this.d.toString());
            } else if (this.f != null) {
                throw this.f;
            } else {
                this.b.unlock();
            }
        } catch (Exception e2) {
            throw e2;
        } catch (Throwable th) {
            this.b.unlock();
            throw th;
        }
    }

    public static String f(Context context) {
        try {
            if (a(context, "android.permission.ACCESS_WIFI_STATE")) {
                String macAddress = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress();
                return (macAddress == null || macAddress.equals(PoiTypeDef.All)) ? "unknown" : macAddress;
            }
            Log.w("MobileUtils", "Could not read MAC, forget to include ACCESS_WIFI_STATE permission?");
            return "unknown";
        } catch (Exception e2) {
            Log.w("MobileUtils", "Could not read MAC, forget to include ACCESS_WIFI_STATE permission?", e2);
            return "unknown";
        }
    }

    public static String f(Date date) {
        return (date == null || date.getTime() == 0) ? "未知时间" : new SimpleDateFormat("MM-dd HH:mm").format(date);
    }

    public static boolean f(String str) {
        return str != null && !PoiTypeDef.All.equals(str);
    }

    public static String g(Context context) {
        String string = context.getSharedPreferences("MoblieAgent_sys_config", 0).getString("MOBILE_APPKEY", PoiTypeDef.All);
        if (!string.equals(PoiTypeDef.All)) {
            return string;
        }
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), NativeMapEngine.MAX_ICON_SIZE);
            return applicationInfo != null ? applicationInfo.metaData.getString("MOBILE_APPKEY") : string;
        } catch (Exception e2) {
            e2.printStackTrace();
            return string;
        }
    }

    public static String g(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(date);
    }

    public static boolean g(String str) {
        if (!f(str) || str.length() < 6 || str.replaceAll(new StringBuilder(String.valueOf(str.charAt(0))).toString(), PoiTypeDef.All).length() == 0) {
            return true;
        }
        return Arrays.asList(new String[]{"123456", "1234567", "12345678", "123456789", "1234567890", "654321", "7654321", "87654321", "987654321", "0987654321", "012345", "123465", "234567", "5201314", "1314520", "qwerty", "qweasd", "asdfgh", "zxcvbn", "qazwsx", "qazxsw", "abcdef", "abcabc", "abc123", "a1b2c3", "1a2b3c", "aaa111", "q1w2e3", "1q2w3e", "password", "passw0rd", "iloveyou", "monkey", "master ", "letmein"}).contains(str);
    }

    public static int h(Date date) {
        Calendar instance = Calendar.getInstance();
        if (instance.before(date)) {
            return -1;
        }
        int i = instance.get(1);
        int i2 = instance.get(2);
        int i3 = instance.get(5);
        instance.setTime(date);
        int i4 = instance.get(1);
        int i5 = instance.get(2);
        int i6 = instance.get(5);
        int i7 = i - i4;
        return i2 <= i5 ? (i2 != i5 || i3 < i6) ? i7 - 1 : i7 : i7;
    }

    public static String h(Context context) {
        String string = context.getSharedPreferences("MoblieAgent_sys_config", 0).getString("MOBILE_CHANNEL", PoiTypeDef.All);
        if (!string.equals(PoiTypeDef.All)) {
            return b(string);
        }
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), NativeMapEngine.MAX_ICON_SIZE);
            if (applicationInfo != null && (string = applicationInfo.metaData.getString("MOBILE_CHANNEL")) == null) {
                Log.w("MobileUtils", "Could not read MOBILE_CHANNEL meta-data from AndroidManifest.xml.");
                string = PoiTypeDef.All;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return b(string);
    }

    public static Date h(String str) {
        if (a((CharSequence) str)) {
            return null;
        }
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(str);
        } catch (ParseException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static String i(Context context) {
        if (!a(context, "android.permission.ACCESS_NETWORK_STATE")) {
            return "unknown";
        }
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo == null) {
                return "unknown";
            }
            if (activeNetworkInfo.getType() == 1) {
                return "wifi";
            }
            String extraInfo = activeNetworkInfo.getExtraInfo();
            if (extraInfo == null) {
                return "unknown";
            }
            Log.i("MobileUtils", "net type:" + extraInfo);
            return extraInfo.trim();
        } catch (Exception e2) {
            Log.w("MobileUtils", "Could not read ACCESSPOINT, forget to include ACCESS_NETSTATE_STATE permission?", e2);
            return "unknown";
        }
    }

    private static String i(Date date) {
        return new SimpleDateFormat("MM-dd").format(date);
    }

    public static Date i(String str) {
        try {
            return new SimpleDateFormat("yyyyMMdd").parse(str);
        } catch (ParseException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static int j(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (Exception e2) {
            e2.printStackTrace();
            return -1;
        }
    }

    private static String j(Date date) {
        return new SimpleDateFormat("HH:mm").format(date);
    }

    public static Date j(String str) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(str);
        } catch (ParseException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static String k(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Exception e2) {
            e2.printStackTrace();
            return PoiTypeDef.All;
        }
    }

    public static Date k(String str) {
        try {
            return new SimpleDateFormat("yyyyMMddHHmmss").parse(str);
        } catch (ParseException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static Bitmap l(String str) {
        try {
            return BitmapFactory.decodeFile(str);
        } catch (Throwable th) {
            return null;
        }
    }

    public static String m(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.ensureCapacity(str.length() * 6);
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (Character.isDigit(charAt) || Character.isLowerCase(charAt) || Character.isUpperCase(charAt)) {
                stringBuffer.append(charAt);
            } else if (charAt < 256) {
                stringBuffer.append("%");
                if (charAt < 16) {
                    stringBuffer.append("0");
                }
                stringBuffer.append(Integer.toString(charAt, 16));
            } else {
                stringBuffer.append("%u");
                stringBuffer.append(Integer.toString(charAt, 16));
            }
        }
        return stringBuffer.toString();
    }

    public static String n(String str) {
        if (a((CharSequence) str)) {
            return PoiTypeDef.All;
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b2 : digest) {
                String hexString = Integer.toHexString(b2 & GZIPHeader.OS_UNKNOWN);
                while (hexString.length() < 2) {
                    hexString = "0" + hexString;
                }
                stringBuffer.append(hexString);
            }
            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e2) {
            return str;
        }
    }

    public static byte[] o(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-256");
            instance.update(str.getBytes());
            return instance.digest();
        } catch (Exception e2) {
            return null;
        }
    }

    public static String p(String str) {
        return a(str) ? PoiTypeDef.All : str.trim().replaceAll("([-])", PoiTypeDef.All).replaceAll(" ", PoiTypeDef.All).replaceAll(bp.b(), PoiTypeDef.All).toLowerCase();
    }

    public static String q(String str) {
        return a(str) ? PoiTypeDef.All : str.trim().replaceAll("([-])", PoiTypeDef.All);
    }

    public static byte[] r(String str) {
        byte[] bArr = new byte[(str.length() / 2)];
        int i = 0;
        while (i < bArr.length) {
            try {
                bArr[i] = Integer.decode("#" + str.substring(i * 2, (i * 2) + 2)).byteValue();
                i++;
            } catch (NumberFormatException e2) {
                return null;
            }
        }
        return bArr;
    }

    public final void a(int i, float f2) {
    }

    public final void a(Object obj, f fVar) {
    }

    public final void a(String str, String str2, String str3, int i) {
        if (!com.immomo.a.a.f.a.a(str3)) {
            str = String.valueOf(str) + "@" + str3;
        }
        this.g = false;
        this.e = false;
        this.f = null;
        long currentTimeMillis = System.currentTimeMillis();
        this.d = new b();
        this.d.a("conn");
        this.d.put("v", i);
        this.d.put("u", str);
        e();
        this.f349a.d("conn");
        l.j = System.currentTimeMillis() - currentTimeMillis;
        long currentTimeMillis2 = System.currentTimeMillis();
        this.f349a.b("auth", this);
        this.d = new b();
        this.d.a("auth");
        if (this.f349a.c() != null) {
            str2 = this.f349a.c().a(str2);
        }
        this.d.put("sessionid", str2);
        int k = this.f349a.b().k();
        if (k > 0 && this.f349a.b().m()) {
            this.d.put("cp", k);
        }
        if (this.f349a.c() != null && this.f349a.c().a() > 0) {
            this.d.put("etype", this.f349a.c().a());
        }
        if (!com.immomo.a.a.f.a.a(this.f349a.b().e())) {
            this.d.put("cflag", this.f349a.b().e());
        }
        if (!com.immomo.a.a.f.a.a(this.f349a.b().g())) {
            this.d.put("uid", this.f349a.b().g());
        }
        e();
        this.f349a.d("auth");
        l.k = System.currentTimeMillis() - currentTimeMillis2;
    }

    public final boolean a(b bVar) {
        try {
            String a2 = bVar.a();
            if ("conn".equals(a2)) {
                if (bVar.optInt("ec", 0) > 0) {
                    this.f = new com.immomo.a.a.b.a(bVar.optInt("ec"), bVar.optString("em", PoiTypeDef.All));
                } else if (bVar.has("cp")) {
                    JSONArray jSONArray = bVar.getJSONArray("cp");
                    int[] iArr = new int[jSONArray.length()];
                    for (int i = 0; i < jSONArray.length(); i++) {
                        iArr[i] = jSONArray.getInt(i);
                    }
                    this.f349a.b().a(iArr);
                    for (c b2 : this.f349a.e()) {
                        b2.b(bVar);
                    }
                }
                if (this.f349a.c() != null) {
                    this.f349a.c().a(bVar);
                }
            } else if ("auth".equals(a2)) {
                if (bVar.has("ec")) {
                    this.f = new com.immomo.a.a.b.a(bVar.optInt("ec"), bVar.optString("em", PoiTypeDef.All));
                } else {
                    if (bVar.has("uver")) {
                        this.f349a.b();
                        bVar.optInt("uver");
                        com.immomo.a.a.b.i();
                    }
                    if (bVar.has("cflag")) {
                        this.f349a.b().e(bVar.optString("cflag"));
                    }
                    if (this.f349a.c() != null) {
                        this.f349a.c().b();
                    }
                }
            } else if ("disconn".equals(a2)) {
                int optInt = bVar.optInt("ec", -1);
                String optString = bVar.optString("em", PoiTypeDef.All);
                bVar.toString();
                this.f = new com.immomo.a.a.b.c(optString, (byte) 0);
                this.f349a.k();
                for (c a3 : this.f349a.e()) {
                    a3.a(optInt, bVar);
                }
            }
            this.b.lock();
            try {
                this.e = true;
                this.c.signal();
            } catch (Exception e2) {
                this.f = e2;
            } finally {
                this.b.unlock();
            }
        } catch (Exception e3) {
            this.f = e3;
            this.b.lock();
            try {
                this.e = true;
                this.c.signal();
            } catch (Exception e4) {
                this.f = e4;
            } finally {
                this.b.unlock();
            }
        } catch (Throwable th) {
            this.b.lock();
            try {
                this.e = true;
                this.c.signal();
            } catch (Exception e5) {
                this.f = e5;
            } finally {
                this.b.unlock();
            }
            throw th;
        }
        return true;
    }

    public void a_(int i) {
    }

    public final void b() {
        this.b.lock();
        try {
            this.g = true;
            this.c.signal();
        } catch (Exception e2) {
        } finally {
            this.b.unlock();
        }
    }

    public final void b(int i) {
    }

    public final String c() {
        return this.d.toString();
    }
}
