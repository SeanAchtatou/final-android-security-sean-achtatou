package android.support.v4.b;

import android.os.Parcel;
import android.os.Parcelable;

final class b implements Parcelable.Creator {

    /* renamed from: a  reason: collision with root package name */
    private c f350a;

    public b(c cVar) {
        this.f350a = cVar;
    }

    public final Object createFromParcel(Parcel parcel) {
        return this.f350a.a(parcel, null);
    }

    public final Object[] newArray(int i) {
        return this.f350a.a(i);
    }
}
