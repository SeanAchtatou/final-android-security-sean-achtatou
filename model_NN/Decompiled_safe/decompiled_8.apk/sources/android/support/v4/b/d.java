package android.support.v4.b;

import android.os.Parcel;
import android.os.Parcelable;

final class d implements Parcelable.ClassLoaderCreator {

    /* renamed from: a  reason: collision with root package name */
    private final c f351a;

    public d(c cVar) {
        this.f351a = cVar;
    }

    public final Object createFromParcel(Parcel parcel) {
        return this.f351a.a(parcel, null);
    }

    public final Object createFromParcel(Parcel parcel, ClassLoader classLoader) {
        return this.f351a.a(parcel, classLoader);
    }

    public final Object[] newArray(int i) {
        return this.f351a.a(i);
    }
}
