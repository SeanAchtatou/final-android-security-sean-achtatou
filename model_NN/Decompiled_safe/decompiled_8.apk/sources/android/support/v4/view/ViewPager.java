package android.support.v4.view;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.b.a;
import android.support.v4.widget.f;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.Interpolator;
import android.widget.Scroller;
import com.amap.mapapi.map.MapView;
import com.baidu.location.LocationClientOption;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import mm.purchasesdk.PurchaseCode;

public class ViewPager extends ViewGroup {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final int[] f354a = {16842931};
    private static final Comparator c = new ax();
    private static final Interpolator d = new ay();
    private boolean A;
    private int B;
    private int C;
    private int D;
    private float E;
    private float F;
    private float G;
    private float H;
    private int I = -1;
    private VelocityTracker J;
    private int K;
    private int L;
    private int M;
    private int N;
    private f O;
    private f P;
    private boolean Q = true;
    private boolean R;
    private int S;
    private bf T;
    private bf U;
    private be V;
    private Method W;
    private final Runnable Z = new az(this);
    private int aa = 0;
    private int b;
    private final ArrayList e = new ArrayList();
    private final bb f = new bb();
    private final Rect g = new Rect();
    /* access modifiers changed from: private */
    public u h;
    /* access modifiers changed from: private */
    public int i;
    private int j = -1;
    private Parcelable k = null;
    private ClassLoader l = null;
    private Scroller m;
    private bg n;
    private int o;
    private Drawable p;
    private int q;
    private int r;
    private float s = -3.4028235E38f;
    private float t = Float.MAX_VALUE;
    private int u;
    private boolean v;
    private boolean w;
    private boolean x;
    private int y = 1;
    private boolean z;

    public class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = a.a(new bh());

        /* renamed from: a  reason: collision with root package name */
        int f355a;
        Parcelable b;
        ClassLoader c;

        SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel);
            classLoader = classLoader == null ? getClass().getClassLoader() : classLoader;
            this.f355a = parcel.readInt();
            this.b = parcel.readParcelable(classLoader);
            this.c = classLoader;
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public String toString() {
            return "FragmentPager.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " position=" + this.f355a + "}";
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f355a);
            parcel.writeParcelable(this.b, i);
        }
    }

    static {
        new bi();
    }

    public ViewPager(Context context) {
        super(context);
        d();
    }

    public ViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        d();
    }

    private Rect a(Rect rect, View view) {
        Rect rect2 = rect == null ? new Rect() : rect;
        if (view == null) {
            rect2.set(0, 0, 0, 0);
            return rect2;
        }
        rect2.left = view.getLeft();
        rect2.right = view.getRight();
        rect2.top = view.getTop();
        rect2.bottom = view.getBottom();
        ViewParent parent = view.getParent();
        while ((parent instanceof ViewGroup) && parent != this) {
            ViewGroup viewGroup = (ViewGroup) parent;
            rect2.left += viewGroup.getLeft();
            rect2.right += viewGroup.getRight();
            rect2.top += viewGroup.getTop();
            rect2.bottom += viewGroup.getBottom();
            parent = viewGroup.getParent();
        }
        return rect2;
    }

    private bb a(int i2, int i3) {
        bb bbVar = new bb();
        bbVar.b = i2;
        bbVar.f368a = this.h.a(this, i2);
        u uVar = this.h;
        bbVar.d = 1.0f;
        if (i3 < 0 || i3 >= this.e.size()) {
            this.e.add(bbVar);
        } else {
            this.e.add(i3, bbVar);
        }
        return bbVar;
    }

    private bb a(View view) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.e.size()) {
                return null;
            }
            bb bbVar = (bb) this.e.get(i3);
            if (this.h.a(view, bbVar.f368a)) {
                return bbVar;
            }
            i2 = i3 + 1;
        }
    }

    private void a(int i2, float f2) {
        int i3;
        int i4;
        int measuredWidth;
        if (this.S > 0) {
            int scrollX = getScrollX();
            int paddingLeft = getPaddingLeft();
            int paddingRight = getPaddingRight();
            int width = getWidth();
            int childCount = getChildCount();
            int i5 = 0;
            while (i5 < childCount) {
                View childAt = getChildAt(i5);
                bc bcVar = (bc) childAt.getLayoutParams();
                if (bcVar.f369a) {
                    switch (bcVar.b & 7) {
                        case 1:
                            measuredWidth = Math.max((width - childAt.getMeasuredWidth()) / 2, paddingLeft);
                            int i6 = paddingRight;
                            i3 = paddingLeft;
                            i4 = i6;
                            break;
                        case 2:
                        case 4:
                        default:
                            measuredWidth = paddingLeft;
                            int i7 = paddingRight;
                            i3 = paddingLeft;
                            i4 = i7;
                            break;
                        case 3:
                            int width2 = childAt.getWidth() + paddingLeft;
                            int i8 = paddingLeft;
                            i4 = paddingRight;
                            i3 = width2;
                            measuredWidth = i8;
                            break;
                        case 5:
                            measuredWidth = (width - paddingRight) - childAt.getMeasuredWidth();
                            int measuredWidth2 = paddingRight + childAt.getMeasuredWidth();
                            i3 = paddingLeft;
                            i4 = measuredWidth2;
                            break;
                    }
                    int left = (measuredWidth + scrollX) - childAt.getLeft();
                    if (left != 0) {
                        childAt.offsetLeftAndRight(left);
                    }
                } else {
                    int i9 = paddingRight;
                    i3 = paddingLeft;
                    i4 = i9;
                }
                i5++;
                int i10 = i4;
                paddingLeft = i3;
                paddingRight = i10;
            }
        }
        if (this.T != null) {
            this.T.a(i2, f2);
        }
        if (this.U != null) {
            this.U.a(i2, f2);
        }
        this.R = true;
    }

    private void a(int i2, int i3, int i4, int i5) {
        if (i3 <= 0 || this.e.isEmpty()) {
            bb c2 = c(this.i);
            int min = (int) ((c2 != null ? Math.min(c2.e, this.t) : 0.0f) * ((float) ((i2 - getPaddingLeft()) - getPaddingRight())));
            if (min != getScrollX()) {
                a(false);
                scrollTo(min, getScrollY());
                return;
            }
            return;
        }
        int paddingLeft = (int) (((float) (((i2 - getPaddingLeft()) - getPaddingRight()) + i4)) * (((float) getScrollX()) / ((float) (((i3 - getPaddingLeft()) - getPaddingRight()) + i5))));
        scrollTo(paddingLeft, getScrollY());
        if (!this.m.isFinished()) {
            this.m.startScroll(paddingLeft, 0, (int) (c(this.i).e * ((float) i2)), 0, this.m.getDuration() - this.m.timePassed());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    private void a(int i2, boolean z2, int i3, boolean z3) {
        int abs;
        bb c2 = c(i2);
        int i4 = 0;
        if (c2 != null) {
            i4 = (int) (((float) getClientWidth()) * Math.max(this.s, Math.min(c2.e, this.t)));
        }
        if (z2) {
            if (getChildCount() == 0) {
                setScrollingCacheEnabled(false);
            } else {
                int scrollX = getScrollX();
                int scrollY = getScrollY();
                int i5 = i4 - scrollX;
                int i6 = 0 - scrollY;
                if (i5 == 0 && i6 == 0) {
                    a(false);
                    b();
                    setScrollState(0);
                } else {
                    setScrollingCacheEnabled(true);
                    setScrollState(2);
                    int clientWidth = getClientWidth();
                    int i7 = clientWidth / 2;
                    float f2 = (float) i7;
                    float sin = (((float) i7) * ((float) Math.sin((double) ((float) (((double) (Math.min(1.0f, (1.0f * ((float) Math.abs(i5))) / ((float) clientWidth)) - 0.5f)) * 0.4712389167638204d))))) + f2;
                    int abs2 = Math.abs(i3);
                    if (abs2 > 0) {
                        abs = Math.round(1000.0f * Math.abs(sin / ((float) abs2))) * 4;
                    } else {
                        u uVar = this.h;
                        int i8 = this.i;
                        abs = (int) (((((float) Math.abs(i5)) / ((((float) clientWidth) * 1.0f) + ((float) this.o))) + 1.0f) * 100.0f);
                    }
                    this.m.startScroll(scrollX, scrollY, i5, i6, Math.min(abs, 600));
                    ah.b(this);
                }
            }
            if (z3 && this.T != null) {
                this.T.a_(i2);
            }
            if (z3 && this.U != null) {
                this.U.a_(i2);
                return;
            }
            return;
        }
        if (z3 && this.T != null) {
            this.T.a_(i2);
        }
        if (z3 && this.U != null) {
            this.U.a_(i2);
        }
        a(false);
        scrollTo(i4, 0);
        d(i4);
    }

    private void a(int i2, boolean z2, boolean z3) {
        a(i2, z2, z3, 0);
    }

    private void a(int i2, boolean z2, boolean z3, int i3) {
        boolean z4 = false;
        if (this.h == null || this.h.b() <= 0) {
            setScrollingCacheEnabled(false);
        } else if (z3 || this.i != i2 || this.e.size() == 0) {
            if (i2 < 0) {
                i2 = 0;
            } else if (i2 >= this.h.b()) {
                i2 = this.h.b() - 1;
            }
            int i4 = this.y;
            if (i2 > this.i + i4 || i2 < this.i - i4) {
                for (int i5 = 0; i5 < this.e.size(); i5++) {
                    ((bb) this.e.get(i5)).c = true;
                }
            }
            if (this.i != i2) {
                z4 = true;
            }
            if (this.Q) {
                this.i = i2;
                if (z4 && this.T != null) {
                    this.T.a_(i2);
                }
                if (z4 && this.U != null) {
                    this.U.a_(i2);
                }
                requestLayout();
                return;
            }
            b(i2);
            a(i2, z2, i3, z4);
        } else {
            setScrollingCacheEnabled(false);
        }
    }

    private void a(bb bbVar, int i2, bb bbVar2) {
        bb bbVar3;
        bb bbVar4;
        int b2 = this.h.b();
        int clientWidth = getClientWidth();
        float f2 = clientWidth > 0 ? ((float) this.o) / ((float) clientWidth) : 0.0f;
        if (bbVar2 != null) {
            int i3 = bbVar2.b;
            if (i3 < bbVar.b) {
                int i4 = 0;
                float f3 = bbVar2.e + bbVar2.d + f2;
                int i5 = i3 + 1;
                while (i5 <= bbVar.b && i4 < this.e.size()) {
                    Object obj = this.e.get(i4);
                    while (true) {
                        bbVar4 = (bb) obj;
                        if (i5 > bbVar4.b && i4 < this.e.size() - 1) {
                            i4++;
                            obj = this.e.get(i4);
                        }
                    }
                    while (i5 < bbVar4.b) {
                        u uVar = this.h;
                        f3 += 1.0f + f2;
                        i5++;
                    }
                    bbVar4.e = f3;
                    f3 += bbVar4.d + f2;
                    i5++;
                }
            } else if (i3 > bbVar.b) {
                int size = this.e.size() - 1;
                float f4 = bbVar2.e;
                int i6 = i3 - 1;
                while (i6 >= bbVar.b && size >= 0) {
                    Object obj2 = this.e.get(size);
                    while (true) {
                        bbVar3 = (bb) obj2;
                        if (i6 < bbVar3.b && size > 0) {
                            size--;
                            obj2 = this.e.get(size);
                        }
                    }
                    while (i6 > bbVar3.b) {
                        u uVar2 = this.h;
                        f4 -= 1.0f + f2;
                        i6--;
                    }
                    f4 -= bbVar3.d + f2;
                    bbVar3.e = f4;
                    i6--;
                }
            }
        }
        int size2 = this.e.size();
        float f5 = bbVar.e;
        int i7 = bbVar.b - 1;
        this.s = bbVar.b == 0 ? bbVar.e : -3.4028235E38f;
        this.t = bbVar.b == b2 + -1 ? (bbVar.e + bbVar.d) - 1.0f : Float.MAX_VALUE;
        for (int i8 = i2 - 1; i8 >= 0; i8--) {
            bb bbVar5 = (bb) this.e.get(i8);
            while (i7 > bbVar5.b) {
                u uVar3 = this.h;
                i7--;
                f5 -= 1.0f + f2;
            }
            f5 -= bbVar5.d + f2;
            bbVar5.e = f5;
            if (bbVar5.b == 0) {
                this.s = f5;
            }
            i7--;
        }
        float f6 = bbVar.e + bbVar.d + f2;
        int i9 = bbVar.b + 1;
        for (int i10 = i2 + 1; i10 < size2; i10++) {
            bb bbVar6 = (bb) this.e.get(i10);
            while (i9 < bbVar6.b) {
                u uVar4 = this.h;
                i9++;
                f6 += 1.0f + f2;
            }
            if (bbVar6.b == b2 - 1) {
                this.t = (bbVar6.d + f6) - 1.0f;
            }
            bbVar6.e = f6;
            f6 += bbVar6.d + f2;
            i9++;
        }
    }

    private void a(MotionEvent motionEvent) {
        int b2 = q.b(motionEvent);
        if (q.b(motionEvent, b2) == this.I) {
            int i2 = b2 == 0 ? 1 : 0;
            this.E = q.c(motionEvent, i2);
            this.I = q.b(motionEvent, i2);
            if (this.J != null) {
                this.J.clear();
            }
        }
    }

    private void a(boolean z2) {
        boolean z3 = this.aa == 2;
        if (z3) {
            setScrollingCacheEnabled(false);
            this.m.abortAnimation();
            int scrollX = getScrollX();
            int scrollY = getScrollY();
            int currX = this.m.getCurrX();
            int currY = this.m.getCurrY();
            if (!(scrollX == currX && scrollY == currY)) {
                scrollTo(currX, currY);
            }
        }
        this.x = false;
        boolean z4 = z3;
        for (int i2 = 0; i2 < this.e.size(); i2++) {
            bb bbVar = (bb) this.e.get(i2);
            if (bbVar.c) {
                bbVar.c = false;
                z4 = true;
            }
        }
        if (!z4) {
            return;
        }
        if (z2) {
            ah.a(this, this.Z);
        } else {
            this.Z.run();
        }
    }

    private boolean a(float f2) {
        boolean z2;
        float f3;
        boolean z3 = true;
        boolean z4 = false;
        this.E = f2;
        float scrollX = ((float) getScrollX()) + (this.E - f2);
        int clientWidth = getClientWidth();
        float f4 = ((float) clientWidth) * this.s;
        float f5 = ((float) clientWidth) * this.t;
        bb bbVar = (bb) this.e.get(0);
        bb bbVar2 = (bb) this.e.get(this.e.size() - 1);
        if (bbVar.b != 0) {
            f4 = bbVar.e * ((float) clientWidth);
            z2 = false;
        } else {
            z2 = true;
        }
        if (bbVar2.b != this.h.b() - 1) {
            f3 = bbVar2.e * ((float) clientWidth);
            z3 = false;
        } else {
            f3 = f5;
        }
        if (scrollX < f4) {
            if (z2) {
                z4 = this.O.a(Math.abs(f4 - scrollX) / ((float) clientWidth));
            }
        } else if (scrollX > f3) {
            if (z3) {
                z4 = this.P.a(Math.abs(scrollX - f3) / ((float) clientWidth));
            }
            f4 = f3;
        } else {
            f4 = scrollX;
        }
        this.E += f4 - ((float) ((int) f4));
        scrollTo((int) f4, getScrollY());
        d((int) f4);
        return z4;
    }

    private boolean a(View view, boolean z2, int i2, int i3, int i4) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int scrollX = view.getScrollX();
            int scrollY = view.getScrollY();
            for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = viewGroup.getChildAt(childCount);
                if (i3 + scrollX >= childAt.getLeft() && i3 + scrollX < childAt.getRight() && i4 + scrollY >= childAt.getTop() && i4 + scrollY < childAt.getBottom()) {
                    if (a(childAt, true, i2, (i3 + scrollX) - childAt.getLeft(), (i4 + scrollY) - childAt.getTop())) {
                        return true;
                    }
                }
            }
        }
        return z2 && ah.a(view, -i2);
    }

    private bb b(View view) {
        while (true) {
            ViewParent parent = view.getParent();
            if (parent == this) {
                return a(view);
            }
            if (parent == null || !(parent instanceof View)) {
                return null;
            }
            view = (View) parent;
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00d7, code lost:
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x01c1, code lost:
        if (r2.b == r0.i) goto L_0x00d8;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(int r19) {
        /*
            r18 = this;
            r3 = 0
            r2 = 2
            r0 = r18
            int r4 = r0.i
            r0 = r19
            if (r4 == r0) goto L_0x032c
            r0 = r18
            int r2 = r0.i
            r0 = r19
            if (r2 >= r0) goto L_0x002d
            r2 = 66
        L_0x0014:
            r0 = r18
            int r3 = r0.i
            r0 = r18
            android.support.v4.view.bb r3 = r0.c(r3)
            r0 = r19
            r1 = r18
            r1.i = r0
            r4 = r3
            r3 = r2
        L_0x0026:
            r0 = r18
            android.support.v4.view.u r2 = r0.h
            if (r2 != 0) goto L_0x0030
        L_0x002c:
            return
        L_0x002d:
            r2 = 17
            goto L_0x0014
        L_0x0030:
            r0 = r18
            boolean r2 = r0.x
            if (r2 != 0) goto L_0x002c
            android.os.IBinder r2 = r18.getWindowToken()
            if (r2 == 0) goto L_0x002c
            r0 = r18
            android.support.v4.view.u r2 = r0.h
            r0 = r18
            int r2 = r0.y
            r5 = 0
            r0 = r18
            int r6 = r0.i
            int r6 = r6 - r2
            int r11 = java.lang.Math.max(r5, r6)
            r0 = r18
            android.support.v4.view.u r5 = r0.h
            int r12 = r5.b()
            int r5 = r12 + -1
            r0 = r18
            int r6 = r0.i
            int r2 = r2 + r6
            int r13 = java.lang.Math.min(r5, r2)
            r0 = r18
            int r2 = r0.b
            if (r12 == r2) goto L_0x00ca
            android.content.res.Resources r2 = r18.getResources()     // Catch:{ NotFoundException -> 0x00c0 }
            int r3 = r18.getId()     // Catch:{ NotFoundException -> 0x00c0 }
            java.lang.String r2 = r2.getResourceName(r3)     // Catch:{ NotFoundException -> 0x00c0 }
        L_0x0073:
            java.lang.IllegalStateException r3 = new java.lang.IllegalStateException
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "The application's PagerAdapter changed the adapter's contents without calling PagerAdapter#notifyDataSetChanged! Expected adapter item count: "
            r4.<init>(r5)
            r0 = r18
            int r5 = r0.b
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = ", found: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r12)
            java.lang.String r5 = " Pager id: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r4 = " Pager class: "
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.Class r4 = r18.getClass()
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = " Problematic adapter: "
            java.lang.StringBuilder r2 = r2.append(r4)
            r0 = r18
            android.support.v4.view.u r4 = r0.h
            java.lang.Class r4 = r4.getClass()
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            r3.<init>(r2)
            throw r3
        L_0x00c0:
            r2 = move-exception
            int r2 = r18.getId()
            java.lang.String r2 = java.lang.Integer.toHexString(r2)
            goto L_0x0073
        L_0x00ca:
            r6 = 0
            r2 = 0
            r5 = r2
        L_0x00cd:
            r0 = r18
            java.util.ArrayList r2 = r0.e
            int r2 = r2.size()
            if (r5 < r2) goto L_0x01a9
        L_0x00d7:
            r2 = r6
        L_0x00d8:
            if (r2 != 0) goto L_0x0329
            if (r12 <= 0) goto L_0x0329
            r0 = r18
            int r2 = r0.i
            r0 = r18
            android.support.v4.view.bb r2 = r0.a(r2, r5)
            r10 = r2
        L_0x00e7:
            if (r10 == 0) goto L_0x0147
            r9 = 0
            int r8 = r5 + -1
            if (r8 < 0) goto L_0x01ca
            r0 = r18
            java.util.ArrayList r2 = r0.e
            java.lang.Object r2 = r2.get(r8)
            android.support.v4.view.bb r2 = (android.support.v4.view.bb) r2
        L_0x00f8:
            int r14 = r18.getClientWidth()
            if (r14 > 0) goto L_0x01cd
            r6 = 0
        L_0x00ff:
            r0 = r18
            int r7 = r0.i
            int r7 = r7 + -1
            r16 = r7
            r7 = r9
            r9 = r16
            r17 = r8
            r8 = r5
            r5 = r17
        L_0x010f:
            if (r9 >= 0) goto L_0x01dc
        L_0x0111:
            float r6 = r10.d
            int r9 = r8 + 1
            r2 = 1073741824(0x40000000, float:2.0)
            int r2 = (r6 > r2 ? 1 : (r6 == r2 ? 0 : -1))
            if (r2 >= 0) goto L_0x0142
            r0 = r18
            java.util.ArrayList r2 = r0.e
            int r2 = r2.size()
            if (r9 >= r2) goto L_0x024a
            r0 = r18
            java.util.ArrayList r2 = r0.e
            java.lang.Object r2 = r2.get(r9)
            android.support.v4.view.bb r2 = (android.support.v4.view.bb) r2
            r7 = r2
        L_0x0130:
            if (r14 > 0) goto L_0x024d
            r2 = 0
            r5 = r2
        L_0x0134:
            r0 = r18
            int r2 = r0.i
            int r2 = r2 + 1
            r16 = r7
            r7 = r9
            r9 = r2
            r2 = r16
        L_0x0140:
            if (r9 < r12) goto L_0x025a
        L_0x0142:
            r0 = r18
            r0.a(r10, r8, r4)
        L_0x0147:
            r0 = r18
            android.support.v4.view.u r4 = r0.h
            r0 = r18
            int r2 = r0.i
            if (r10 == 0) goto L_0x02ee
            java.lang.Object r2 = r10.f368a
        L_0x0153:
            r4.a(r2)
            r0 = r18
            android.support.v4.view.u r2 = r0.h
            r0 = r18
            r2.a(r0)
            int r5 = r18.getChildCount()
            r2 = 0
            r4 = r2
        L_0x0165:
            if (r4 < r5) goto L_0x02f1
            boolean r2 = r18.hasFocus()
            if (r2 == 0) goto L_0x002c
            android.view.View r2 = r18.findFocus()
            if (r2 == 0) goto L_0x031f
            r0 = r18
            android.support.v4.view.bb r2 = r0.b(r2)
        L_0x0179:
            if (r2 == 0) goto L_0x0183
            int r2 = r2.b
            r0 = r18
            int r4 = r0.i
            if (r2 == r4) goto L_0x002c
        L_0x0183:
            r2 = 0
        L_0x0184:
            int r4 = r18.getChildCount()
            if (r2 >= r4) goto L_0x002c
            r0 = r18
            android.view.View r4 = r0.getChildAt(r2)
            r0 = r18
            android.support.v4.view.bb r5 = r0.a(r4)
            if (r5 == 0) goto L_0x01a6
            int r5 = r5.b
            r0 = r18
            int r6 = r0.i
            if (r5 != r6) goto L_0x01a6
            boolean r4 = r4.requestFocus(r3)
            if (r4 != 0) goto L_0x002c
        L_0x01a6:
            int r2 = r2 + 1
            goto L_0x0184
        L_0x01a9:
            r0 = r18
            java.util.ArrayList r2 = r0.e
            java.lang.Object r2 = r2.get(r5)
            android.support.v4.view.bb r2 = (android.support.v4.view.bb) r2
            int r7 = r2.b
            r0 = r18
            int r8 = r0.i
            if (r7 < r8) goto L_0x01c5
            int r7 = r2.b
            r0 = r18
            int r8 = r0.i
            if (r7 != r8) goto L_0x00d7
            goto L_0x00d8
        L_0x01c5:
            int r2 = r5 + 1
            r5 = r2
            goto L_0x00cd
        L_0x01ca:
            r2 = 0
            goto L_0x00f8
        L_0x01cd:
            r6 = 1073741824(0x40000000, float:2.0)
            float r7 = r10.d
            float r6 = r6 - r7
            int r7 = r18.getPaddingLeft()
            float r7 = (float) r7
            float r15 = (float) r14
            float r7 = r7 / r15
            float r6 = r6 + r7
            goto L_0x00ff
        L_0x01dc:
            int r15 = (r7 > r6 ? 1 : (r7 == r6 ? 0 : -1))
            if (r15 < 0) goto L_0x0214
            if (r9 >= r11) goto L_0x0214
            if (r2 == 0) goto L_0x0111
            int r15 = r2.b
            if (r9 != r15) goto L_0x020e
            boolean r15 = r2.c
            if (r15 != 0) goto L_0x020e
            r0 = r18
            java.util.ArrayList r15 = r0.e
            r15.remove(r5)
            r0 = r18
            android.support.v4.view.u r15 = r0.h
            java.lang.Object r2 = r2.f368a
            r0 = r18
            r15.a(r0, r9, r2)
            int r5 = r5 + -1
            int r8 = r8 + -1
            if (r5 < 0) goto L_0x0212
            r0 = r18
            java.util.ArrayList r2 = r0.e
            java.lang.Object r2 = r2.get(r5)
            android.support.v4.view.bb r2 = (android.support.v4.view.bb) r2
        L_0x020e:
            int r9 = r9 + -1
            goto L_0x010f
        L_0x0212:
            r2 = 0
            goto L_0x020e
        L_0x0214:
            if (r2 == 0) goto L_0x022e
            int r15 = r2.b
            if (r9 != r15) goto L_0x022e
            float r2 = r2.d
            float r7 = r7 + r2
            int r5 = r5 + -1
            if (r5 < 0) goto L_0x022c
            r0 = r18
            java.util.ArrayList r2 = r0.e
            java.lang.Object r2 = r2.get(r5)
            android.support.v4.view.bb r2 = (android.support.v4.view.bb) r2
            goto L_0x020e
        L_0x022c:
            r2 = 0
            goto L_0x020e
        L_0x022e:
            int r2 = r5 + 1
            r0 = r18
            android.support.v4.view.bb r2 = r0.a(r9, r2)
            float r2 = r2.d
            float r7 = r7 + r2
            int r8 = r8 + 1
            if (r5 < 0) goto L_0x0248
            r0 = r18
            java.util.ArrayList r2 = r0.e
            java.lang.Object r2 = r2.get(r5)
            android.support.v4.view.bb r2 = (android.support.v4.view.bb) r2
            goto L_0x020e
        L_0x0248:
            r2 = 0
            goto L_0x020e
        L_0x024a:
            r7 = 0
            goto L_0x0130
        L_0x024d:
            int r2 = r18.getPaddingRight()
            float r2 = (float) r2
            float r5 = (float) r14
            float r2 = r2 / r5
            r5 = 1073741824(0x40000000, float:2.0)
            float r2 = r2 + r5
            r5 = r2
            goto L_0x0134
        L_0x025a:
            int r11 = (r6 > r5 ? 1 : (r6 == r5 ? 0 : -1))
            if (r11 < 0) goto L_0x02a0
            if (r9 <= r13) goto L_0x02a0
            if (r2 == 0) goto L_0x0142
            int r11 = r2.b
            if (r9 != r11) goto L_0x0322
            boolean r11 = r2.c
            if (r11 != 0) goto L_0x0322
            r0 = r18
            java.util.ArrayList r11 = r0.e
            r11.remove(r7)
            r0 = r18
            android.support.v4.view.u r11 = r0.h
            java.lang.Object r2 = r2.f368a
            r0 = r18
            r11.a(r0, r9, r2)
            r0 = r18
            java.util.ArrayList r2 = r0.e
            int r2 = r2.size()
            if (r7 >= r2) goto L_0x029e
            r0 = r18
            java.util.ArrayList r2 = r0.e
            java.lang.Object r2 = r2.get(r7)
            android.support.v4.view.bb r2 = (android.support.v4.view.bb) r2
        L_0x0290:
            r16 = r6
            r6 = r2
            r2 = r16
        L_0x0295:
            int r9 = r9 + 1
            r16 = r2
            r2 = r6
            r6 = r16
            goto L_0x0140
        L_0x029e:
            r2 = 0
            goto L_0x0290
        L_0x02a0:
            if (r2 == 0) goto L_0x02c7
            int r11 = r2.b
            if (r9 != r11) goto L_0x02c7
            float r2 = r2.d
            float r6 = r6 + r2
            int r7 = r7 + 1
            r0 = r18
            java.util.ArrayList r2 = r0.e
            int r2 = r2.size()
            if (r7 >= r2) goto L_0x02c5
            r0 = r18
            java.util.ArrayList r2 = r0.e
            java.lang.Object r2 = r2.get(r7)
            android.support.v4.view.bb r2 = (android.support.v4.view.bb) r2
        L_0x02bf:
            r16 = r6
            r6 = r2
            r2 = r16
            goto L_0x0295
        L_0x02c5:
            r2 = 0
            goto L_0x02bf
        L_0x02c7:
            r0 = r18
            android.support.v4.view.bb r2 = r0.a(r9, r7)
            int r7 = r7 + 1
            float r2 = r2.d
            float r6 = r6 + r2
            r0 = r18
            java.util.ArrayList r2 = r0.e
            int r2 = r2.size()
            if (r7 >= r2) goto L_0x02ec
            r0 = r18
            java.util.ArrayList r2 = r0.e
            java.lang.Object r2 = r2.get(r7)
            android.support.v4.view.bb r2 = (android.support.v4.view.bb) r2
        L_0x02e6:
            r16 = r6
            r6 = r2
            r2 = r16
            goto L_0x0295
        L_0x02ec:
            r2 = 0
            goto L_0x02e6
        L_0x02ee:
            r2 = 0
            goto L_0x0153
        L_0x02f1:
            r0 = r18
            android.view.View r6 = r0.getChildAt(r4)
            android.view.ViewGroup$LayoutParams r2 = r6.getLayoutParams()
            android.support.v4.view.bc r2 = (android.support.v4.view.bc) r2
            r2.f = r4
            boolean r7 = r2.f369a
            if (r7 != 0) goto L_0x031a
            float r7 = r2.c
            r8 = 0
            int r7 = (r7 > r8 ? 1 : (r7 == r8 ? 0 : -1))
            if (r7 != 0) goto L_0x031a
            r0 = r18
            android.support.v4.view.bb r6 = r0.a(r6)
            if (r6 == 0) goto L_0x031a
            float r7 = r6.d
            r2.c = r7
            int r6 = r6.b
            r2.e = r6
        L_0x031a:
            int r2 = r4 + 1
            r4 = r2
            goto L_0x0165
        L_0x031f:
            r2 = 0
            goto L_0x0179
        L_0x0322:
            r16 = r6
            r6 = r2
            r2 = r16
            goto L_0x0295
        L_0x0329:
            r10 = r2
            goto L_0x00e7
        L_0x032c:
            r4 = r3
            r3 = r2
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.view.ViewPager.b(int):void");
    }

    private bb c(int i2) {
        int i3 = 0;
        while (true) {
            int i4 = i3;
            if (i4 >= this.e.size()) {
                return null;
            }
            bb bbVar = (bb) this.e.get(i4);
            if (bbVar.b == i2) {
                return bbVar;
            }
            i3 = i4 + 1;
        }
    }

    private void d() {
        setWillNotDraw(false);
        setDescendantFocusability(262144);
        setFocusable(true);
        Context context = getContext();
        this.m = new Scroller(context, d);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        float f2 = context.getResources().getDisplayMetrics().density;
        this.D = ap.a(viewConfiguration);
        this.K = (int) (400.0f * f2);
        this.L = viewConfiguration.getScaledMaximumFlingVelocity();
        this.O = new f(context);
        this.P = new f(context);
        this.M = (int) (25.0f * f2);
        this.N = (int) (2.0f * f2);
        this.B = (int) (16.0f * f2);
        ah.a(this, new bd(this));
        if (ah.c(this) == 0) {
            ah.d(this);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, float):void
     arg types: [int, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.graphics.Rect, android.view.View):android.graphics.Rect
      android.support.v4.view.ViewPager.a(int, int):android.support.v4.view.bb
      android.support.v4.view.ViewPager.a(int, boolean):void
      android.support.v4.view.ViewPager.a(int, float):void */
    private boolean d(int i2) {
        if (this.e.size() == 0) {
            this.R = false;
            a(0, 0.0f);
            if (this.R) {
                return false;
            }
            throw new IllegalStateException("onPageScrolled did not call superclass implementation");
        }
        bb e2 = e();
        int clientWidth = getClientWidth();
        int i3 = this.o;
        float f2 = ((float) this.o) / ((float) clientWidth);
        this.R = false;
        a(e2.b, ((((float) i2) / ((float) clientWidth)) - e2.e) / (e2.d + f2));
        if (this.R) {
            return true;
        }
        throw new IllegalStateException("onPageScrolled did not call superclass implementation");
    }

    private bb e() {
        int i2;
        bb bbVar;
        int clientWidth = getClientWidth();
        float scrollX = clientWidth > 0 ? ((float) getScrollX()) / ((float) clientWidth) : 0.0f;
        float f2 = clientWidth > 0 ? ((float) this.o) / ((float) clientWidth) : 0.0f;
        float f3 = 0.0f;
        float f4 = 0.0f;
        int i3 = -1;
        int i4 = 0;
        boolean z2 = true;
        bb bbVar2 = null;
        while (i4 < this.e.size()) {
            bb bbVar3 = (bb) this.e.get(i4);
            if (z2 || bbVar3.b == i3 + 1) {
                bb bbVar4 = bbVar3;
                i2 = i4;
                bbVar = bbVar4;
            } else {
                bb bbVar5 = this.f;
                bbVar5.e = f3 + f4 + f2;
                bbVar5.b = i3 + 1;
                u uVar = this.h;
                int i5 = bbVar5.b;
                bbVar5.d = 1.0f;
                bb bbVar6 = bbVar5;
                i2 = i4 - 1;
                bbVar = bbVar6;
            }
            float f5 = bbVar.e;
            float f6 = bbVar.d + f5 + f2;
            if (!z2 && scrollX < f5) {
                return bbVar2;
            }
            if (scrollX < f6 || i2 == this.e.size() - 1) {
                return bbVar;
            }
            f4 = f5;
            i3 = bbVar.b;
            z2 = false;
            f3 = bbVar.d;
            bbVar2 = bbVar;
            i4 = i2 + 1;
        }
        return bbVar2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.graphics.Rect, android.view.View):android.graphics.Rect
      android.support.v4.view.ViewPager.a(int, int):android.support.v4.view.bb
      android.support.v4.view.ViewPager.a(int, float):void
      android.support.v4.view.ViewPager.a(int, boolean):void */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00cb, code lost:
        if (r10 != 2) goto L_0x0035;
     */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0037  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean e(int r10) {
        /*
            r9 = this;
            r1 = 0
            r8 = 66
            r7 = 17
            r4 = 0
            r3 = 1
            android.view.View r2 = r9.findFocus()
            if (r2 != r9) goto L_0x003f
            r0 = r1
        L_0x000e:
            android.view.FocusFinder r1 = android.view.FocusFinder.getInstance()
            android.view.View r1 = r1.findNextFocus(r9, r0, r10)
            if (r1 == 0) goto L_0x00be
            if (r1 == r0) goto L_0x00be
            if (r10 != r7) goto L_0x00a2
            android.graphics.Rect r2 = r9.g
            android.graphics.Rect r2 = r9.a(r2, r1)
            int r2 = r2.left
            android.graphics.Rect r3 = r9.g
            android.graphics.Rect r3 = r9.a(r3, r0)
            int r3 = r3.left
            if (r0 == 0) goto L_0x009d
            if (r2 < r3) goto L_0x009d
            boolean r0 = r9.g()
        L_0x0034:
            r4 = r0
        L_0x0035:
            if (r4 == 0) goto L_0x003e
            int r0 = android.view.SoundEffectConstants.getContantForFocusDirection(r10)
            r9.playSoundEffect(r0)
        L_0x003e:
            return r4
        L_0x003f:
            if (r2 == 0) goto L_0x00ea
            android.view.ViewParent r0 = r2.getParent()
        L_0x0045:
            boolean r5 = r0 instanceof android.view.ViewGroup
            if (r5 != 0) goto L_0x007e
            r0 = r4
        L_0x004a:
            if (r0 != 0) goto L_0x00ea
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.Class r0 = r2.getClass()
            java.lang.String r0 = r0.getSimpleName()
            r5.append(r0)
            android.view.ViewParent r0 = r2.getParent()
        L_0x0060:
            boolean r2 = r0 instanceof android.view.ViewGroup
            if (r2 != 0) goto L_0x0087
            java.lang.String r0 = "ViewPager"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r6 = "arrowScroll tried to find focus based on non-child current focused view "
            r2.<init>(r6)
            java.lang.String r5 = r5.toString()
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.String r2 = r2.toString()
            android.util.Log.e(r0, r2)
            r0 = r1
            goto L_0x000e
        L_0x007e:
            if (r0 != r9) goto L_0x0082
            r0 = r3
            goto L_0x004a
        L_0x0082:
            android.view.ViewParent r0 = r0.getParent()
            goto L_0x0045
        L_0x0087:
            java.lang.String r2 = " => "
            java.lang.StringBuilder r2 = r5.append(r2)
            java.lang.Class r6 = r0.getClass()
            java.lang.String r6 = r6.getSimpleName()
            r2.append(r6)
            android.view.ViewParent r0 = r0.getParent()
            goto L_0x0060
        L_0x009d:
            boolean r0 = r1.requestFocus()
            goto L_0x0034
        L_0x00a2:
            if (r10 != r8) goto L_0x0035
            android.graphics.Rect r2 = r9.g
            android.graphics.Rect r2 = r9.a(r2, r1)
            int r2 = r2.left
            android.graphics.Rect r5 = r9.g
            android.graphics.Rect r5 = r9.a(r5, r0)
            int r5 = r5.left
            if (r0 == 0) goto L_0x00b8
            if (r2 <= r5) goto L_0x00cd
        L_0x00b8:
            boolean r0 = r1.requestFocus()
            goto L_0x0034
        L_0x00be:
            if (r10 == r7) goto L_0x00c2
            if (r10 != r3) goto L_0x00c8
        L_0x00c2:
            boolean r0 = r9.g()
            goto L_0x0034
        L_0x00c8:
            if (r10 == r8) goto L_0x00cd
            r0 = 2
            if (r10 != r0) goto L_0x0035
        L_0x00cd:
            android.support.v4.view.u r0 = r9.h
            if (r0 == 0) goto L_0x00e7
            int r0 = r9.i
            android.support.v4.view.u r1 = r9.h
            int r1 = r1.b()
            int r1 = r1 + -1
            if (r0 >= r1) goto L_0x00e7
            int r0 = r9.i
            int r0 = r0 + 1
            r9.a(r0, r3)
            r0 = r3
            goto L_0x0034
        L_0x00e7:
            r0 = r4
            goto L_0x0034
        L_0x00ea:
            r0 = r2
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.view.ViewPager.e(int):boolean");
    }

    private void f() {
        this.z = false;
        this.A = false;
        if (this.J != null) {
            this.J.recycle();
            this.J = null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.graphics.Rect, android.view.View):android.graphics.Rect
      android.support.v4.view.ViewPager.a(int, int):android.support.v4.view.bb
      android.support.v4.view.ViewPager.a(int, float):void
      android.support.v4.view.ViewPager.a(int, boolean):void */
    private boolean g() {
        if (this.i <= 0) {
            return false;
        }
        a(this.i - 1, true);
        return true;
    }

    private int getClientWidth() {
        return (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight();
    }

    /* access modifiers changed from: private */
    public void setScrollState(int i2) {
        if (this.aa != i2) {
            this.aa = i2;
            if (this.T != null) {
                this.T.b(i2);
            }
        }
    }

    private void setScrollingCacheEnabled(boolean z2) {
        if (this.w != z2) {
            this.w = z2;
        }
    }

    /* access modifiers changed from: package-private */
    public final bf a(bf bfVar) {
        bf bfVar2 = this.U;
        this.U = bfVar;
        return bfVar2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, boolean):void
     arg types: [int, int, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.support.v4.view.bb, int, android.support.v4.view.bb):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean):void */
    /* access modifiers changed from: package-private */
    public final void a() {
        int b2 = this.h.b();
        this.b = b2;
        boolean z2 = this.e.size() < (this.y * 2) + 1 && this.e.size() < b2;
        int i2 = this.i;
        for (int i3 = 0; i3 < this.e.size(); i3++) {
            u uVar = this.h;
            Object obj = ((bb) this.e.get(i3)).f368a;
        }
        Collections.sort(this.e, c);
        if (z2) {
            int childCount = getChildCount();
            for (int i4 = 0; i4 < childCount; i4++) {
                bc bcVar = (bc) getChildAt(i4).getLayoutParams();
                if (!bcVar.f369a) {
                    bcVar.c = 0.0f;
                }
            }
            a(i2, false, true);
            requestLayout();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, boolean, int):void
     arg types: [int, boolean, int, int]
     candidates:
      android.support.v4.view.ViewPager.a(int, int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, int, boolean):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean, int):void */
    public final void a(int i2) {
        this.x = false;
        a(i2, !this.Q, false, 800);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, boolean):void
     arg types: [int, boolean, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.support.v4.view.bb, int, android.support.v4.view.bb):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean):void */
    public final void a(int i2, boolean z2) {
        this.x = false;
        a(i2, z2, false);
    }

    public void addFocusables(ArrayList arrayList, int i2, int i3) {
        bb a2;
        int size = arrayList.size();
        int descendantFocusability = getDescendantFocusability();
        if (descendantFocusability != 393216) {
            for (int i4 = 0; i4 < getChildCount(); i4++) {
                View childAt = getChildAt(i4);
                if (childAt.getVisibility() == 0 && (a2 = a(childAt)) != null && a2.b == this.i) {
                    childAt.addFocusables(arrayList, i2, i3);
                }
            }
        }
        if ((descendantFocusability == 262144 && size != arrayList.size()) || !isFocusable()) {
            return;
        }
        if (((i3 & 1) != 1 || !isInTouchMode() || isFocusableInTouchMode()) && arrayList != null) {
            arrayList.add(this);
        }
    }

    public void addTouchables(ArrayList arrayList) {
        bb a2;
        for (int i2 = 0; i2 < getChildCount(); i2++) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 0 && (a2 = a(childAt)) != null && a2.b == this.i) {
                childAt.addTouchables(arrayList);
            }
        }
    }

    public void addView(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        ViewGroup.LayoutParams generateLayoutParams = !checkLayoutParams(layoutParams) ? generateLayoutParams(layoutParams) : layoutParams;
        bc bcVar = (bc) generateLayoutParams;
        bcVar.f369a |= view instanceof ba;
        if (!this.v) {
            super.addView(view, i2, generateLayoutParams);
        } else if (bcVar == null || !bcVar.f369a) {
            bcVar.d = true;
            addViewInLayout(view, i2, generateLayoutParams);
        } else {
            throw new IllegalStateException("Cannot add pager decor view during layout");
        }
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        b(this.i);
    }

    public boolean canScrollHorizontally(int i2) {
        if (this.h == null) {
            return false;
        }
        int clientWidth = getClientWidth();
        int scrollX = getScrollX();
        return i2 < 0 ? scrollX > ((int) (((float) clientWidth) * this.s)) : i2 > 0 && scrollX < ((int) (((float) clientWidth) * this.t));
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof bc) && super.checkLayoutParams(layoutParams);
    }

    public void computeScroll() {
        if (this.m.isFinished() || !this.m.computeScrollOffset()) {
            a(true);
            return;
        }
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        int currX = this.m.getCurrX();
        int currY = this.m.getCurrY();
        if (!(scrollX == currX && scrollY == currY)) {
            scrollTo(currX, currY);
            if (!d(currX)) {
                this.m.abortAnimation();
                scrollTo(0, currY);
            }
        }
        ah.b(this);
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean dispatchKeyEvent(android.view.KeyEvent r5) {
        /*
            r4 = this;
            r1 = 1
            r0 = 0
            boolean r2 = super.dispatchKeyEvent(r5)
            if (r2 != 0) goto L_0x0044
            int r2 = r5.getAction()
            if (r2 != 0) goto L_0x0015
            int r2 = r5.getKeyCode()
            switch(r2) {
                case 21: goto L_0x0019;
                case 22: goto L_0x0020;
                case 61: goto L_0x0027;
                default: goto L_0x0015;
            }
        L_0x0015:
            r2 = r0
        L_0x0016:
            if (r2 != 0) goto L_0x0044
        L_0x0018:
            return r0
        L_0x0019:
            r2 = 17
            boolean r2 = r4.e(r2)
            goto L_0x0016
        L_0x0020:
            r2 = 66
            boolean r2 = r4.e(r2)
            goto L_0x0016
        L_0x0027:
            int r2 = android.os.Build.VERSION.SDK_INT
            r3 = 11
            if (r2 < r3) goto L_0x0015
            boolean r2 = android.support.v4.view.m.b(r5)
            if (r2 == 0) goto L_0x0039
            r2 = 2
            boolean r2 = r4.e(r2)
            goto L_0x0016
        L_0x0039:
            boolean r2 = android.support.v4.view.m.a(r5)
            if (r2 == 0) goto L_0x0015
            boolean r2 = r4.e(r1)
            goto L_0x0016
        L_0x0044:
            r0 = r1
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.view.ViewPager.dispatchKeyEvent(android.view.KeyEvent):boolean");
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        bb a2;
        if (accessibilityEvent.getEventType() == 4096) {
            return super.dispatchPopulateAccessibilityEvent(accessibilityEvent);
        }
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 0 && (a2 = a(childAt)) != null && a2.b == this.i && childAt.dispatchPopulateAccessibilityEvent(accessibilityEvent)) {
                return true;
            }
        }
        return false;
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        boolean z2 = false;
        int a2 = ah.a(this);
        if (a2 == 0 || (a2 == 1 && this.h != null && this.h.b() > 1)) {
            if (!this.O.a()) {
                int save = canvas.save();
                int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
                int width = getWidth();
                canvas.rotate(270.0f);
                canvas.translate((float) ((-height) + getPaddingTop()), this.s * ((float) width));
                this.O.a(height, width);
                z2 = this.O.a(canvas) | false;
                canvas.restoreToCount(save);
            }
            if (!this.P.a()) {
                int save2 = canvas.save();
                int width2 = getWidth();
                int height2 = (getHeight() - getPaddingTop()) - getPaddingBottom();
                canvas.rotate(90.0f);
                canvas.translate((float) (-getPaddingTop()), (-(this.t + 1.0f)) * ((float) width2));
                this.P.a(height2, width2);
                z2 |= this.P.a(canvas);
                canvas.restoreToCount(save2);
            }
        } else {
            this.O.b();
            this.P.b();
        }
        if (z2) {
            ah.b(this);
        }
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        Drawable drawable = this.p;
        if (drawable != null && drawable.isStateful()) {
            drawable.setState(getDrawableState());
        }
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new bc();
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new bc(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return generateDefaultLayoutParams();
    }

    public u getAdapter() {
        return this.h;
    }

    /* access modifiers changed from: protected */
    public int getChildDrawingOrder(int i2, int i3) {
        ArrayList arrayList = null;
        return ((bc) ((View) arrayList.get(i3)).getLayoutParams()).f;
    }

    public int getCurrentItem() {
        return this.i;
    }

    public int getOffscreenPageLimit() {
        return this.y;
    }

    public int getPageMargin() {
        return this.o;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.Q = true;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        removeCallbacks(this.Z);
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        float f2;
        super.onDraw(canvas);
        if (this.o > 0 && this.p != null && this.e.size() > 0 && this.h != null) {
            int scrollX = getScrollX();
            int width = getWidth();
            float f3 = ((float) this.o) / ((float) width);
            bb bbVar = (bb) this.e.get(0);
            float f4 = bbVar.e;
            int size = this.e.size();
            int i2 = bbVar.b;
            int i3 = ((bb) this.e.get(size - 1)).b;
            int i4 = 0;
            int i5 = i2;
            while (i5 < i3) {
                while (i5 > bbVar.b && i4 < size) {
                    i4++;
                    bbVar = (bb) this.e.get(i4);
                }
                if (i5 == bbVar.b) {
                    f2 = (bbVar.e + bbVar.d) * ((float) width);
                    f4 = bbVar.e + bbVar.d + f3;
                } else {
                    u uVar = this.h;
                    f2 = (1.0f + f4) * ((float) width);
                    f4 += 1.0f + f3;
                }
                if (((float) this.o) + f2 > ((float) scrollX)) {
                    this.p.setBounds((int) f2, this.q, (int) (((float) this.o) + f2 + 0.5f), this.r);
                    this.p.draw(canvas);
                }
                if (f2 <= ((float) (scrollX + width))) {
                    i5++;
                } else {
                    return;
                }
            }
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction() & PurchaseCode.AUTH_INVALID_APP;
        if (action == 3 || action == 1) {
            this.z = false;
            this.A = false;
            this.I = -1;
            if (this.J == null) {
                return false;
            }
            this.J.recycle();
            this.J = null;
            return false;
        }
        if (action != 0) {
            if (this.z) {
                return true;
            }
            if (this.A) {
                return false;
            }
        }
        switch (action) {
            case 0:
                float x2 = motionEvent.getX();
                this.G = x2;
                this.E = x2;
                float y2 = motionEvent.getY();
                this.H = y2;
                this.F = y2;
                this.I = q.b(motionEvent, 0);
                this.A = false;
                this.m.computeScrollOffset();
                if (this.aa == 2 && Math.abs(this.m.getFinalX() - this.m.getCurrX()) > this.N) {
                    this.m.abortAnimation();
                    this.x = false;
                    b();
                    this.z = true;
                    setScrollState(1);
                    break;
                } else {
                    a(false);
                    this.z = false;
                    break;
                }
            case 2:
                int i2 = this.I;
                if (i2 != -1) {
                    int a2 = q.a(motionEvent, i2);
                    float c2 = q.c(motionEvent, a2);
                    float f2 = c2 - this.E;
                    float abs = Math.abs(f2);
                    float d2 = q.d(motionEvent, a2);
                    float abs2 = Math.abs(d2 - this.H);
                    if (f2 != 0.0f) {
                        float f3 = this.E;
                        if (!((f3 < ((float) this.C) && f2 > 0.0f) || (f3 > ((float) (getWidth() - this.C)) && f2 < 0.0f)) && a(this, false, (int) f2, (int) c2, (int) d2)) {
                            this.E = c2;
                            this.F = d2;
                            this.A = true;
                            return false;
                        }
                    }
                    if (abs > ((float) this.D) && 0.5f * abs > abs2) {
                        this.z = true;
                        setScrollState(1);
                        this.E = f2 > 0.0f ? this.G + ((float) this.D) : this.G - ((float) this.D);
                        this.F = d2;
                        setScrollingCacheEnabled(true);
                    } else if (abs2 > ((float) this.D)) {
                        this.A = true;
                    }
                    if (this.z && a(c2)) {
                        ah.b(this);
                        break;
                    }
                }
                break;
            case 6:
                a(motionEvent);
                break;
        }
        if (this.J == null) {
            this.J = VelocityTracker.obtain();
        }
        this.J.addMovement(motionEvent);
        return this.z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, int, boolean):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.view.ViewPager.a(int, int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean, int):void
      android.support.v4.view.ViewPager.a(int, boolean, int, boolean):void */
    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        bb a2;
        int i6;
        int i7;
        int i8;
        int measuredHeight;
        int i9;
        int i10;
        int childCount = getChildCount();
        int i11 = i4 - i2;
        int i12 = i5 - i3;
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();
        int scrollX = getScrollX();
        int i13 = 0;
        int i14 = 0;
        while (i14 < childCount) {
            View childAt = getChildAt(i14);
            if (childAt.getVisibility() != 8) {
                bc bcVar = (bc) childAt.getLayoutParams();
                if (bcVar.f369a) {
                    int i15 = bcVar.b & 7;
                    int i16 = bcVar.b & PurchaseCode.PARAMETER_ERR;
                    switch (i15) {
                        case 1:
                            i8 = Math.max((i11 - childAt.getMeasuredWidth()) / 2, paddingLeft);
                            break;
                        case 2:
                        case 4:
                        default:
                            i8 = paddingLeft;
                            break;
                        case 3:
                            i8 = paddingLeft;
                            paddingLeft = childAt.getMeasuredWidth() + paddingLeft;
                            break;
                        case 5:
                            int measuredWidth = (i11 - paddingRight) - childAt.getMeasuredWidth();
                            paddingRight += childAt.getMeasuredWidth();
                            i8 = measuredWidth;
                            break;
                    }
                    switch (i16) {
                        case 16:
                            measuredHeight = Math.max((i12 - childAt.getMeasuredHeight()) / 2, paddingTop);
                            int i17 = paddingBottom;
                            i9 = paddingTop;
                            i10 = i17;
                            break;
                        case MapView.LayoutParams.TOP /*48*/:
                            int measuredHeight2 = childAt.getMeasuredHeight() + paddingTop;
                            int i18 = paddingTop;
                            i10 = paddingBottom;
                            i9 = measuredHeight2;
                            measuredHeight = i18;
                            break;
                        case MapView.LayoutParams.BOTTOM /*80*/:
                            measuredHeight = (i12 - paddingBottom) - childAt.getMeasuredHeight();
                            int measuredHeight3 = paddingBottom + childAt.getMeasuredHeight();
                            i9 = paddingTop;
                            i10 = measuredHeight3;
                            break;
                        default:
                            measuredHeight = paddingTop;
                            int i19 = paddingBottom;
                            i9 = paddingTop;
                            i10 = i19;
                            break;
                    }
                    int i20 = i8 + scrollX;
                    childAt.layout(i20, measuredHeight, childAt.getMeasuredWidth() + i20, childAt.getMeasuredHeight() + measuredHeight);
                    i6 = i13 + 1;
                    i7 = i9;
                    paddingBottom = i10;
                    i14++;
                    paddingLeft = paddingLeft;
                    paddingRight = paddingRight;
                    paddingTop = i7;
                    i13 = i6;
                }
            }
            i6 = i13;
            i7 = paddingTop;
            i14++;
            paddingLeft = paddingLeft;
            paddingRight = paddingRight;
            paddingTop = i7;
            i13 = i6;
        }
        int i21 = (i11 - paddingLeft) - paddingRight;
        for (int i22 = 0; i22 < childCount; i22++) {
            View childAt2 = getChildAt(i22);
            if (childAt2.getVisibility() != 8) {
                bc bcVar2 = (bc) childAt2.getLayoutParams();
                if (!bcVar2.f369a && (a2 = a(childAt2)) != null) {
                    int i23 = ((int) (a2.e * ((float) i21))) + paddingLeft;
                    if (bcVar2.d) {
                        bcVar2.d = false;
                        childAt2.measure(View.MeasureSpec.makeMeasureSpec((int) (bcVar2.c * ((float) i21)), 1073741824), View.MeasureSpec.makeMeasureSpec((i12 - paddingTop) - paddingBottom, 1073741824));
                    }
                    childAt2.layout(i23, paddingTop, childAt2.getMeasuredWidth() + i23, childAt2.getMeasuredHeight() + paddingTop);
                }
            }
        }
        this.q = paddingTop;
        this.r = i12 - paddingBottom;
        this.S = i13;
        if (this.Q) {
            a(this.i, false, 0, false);
        }
        this.Q = false;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00d4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r14, int r15) {
        /*
            r13 = this;
            r0 = 0
            int r0 = getDefaultSize(r0, r14)
            r1 = 0
            int r1 = getDefaultSize(r1, r15)
            r13.setMeasuredDimension(r0, r1)
            int r0 = r13.getMeasuredWidth()
            int r1 = r0 / 10
            int r2 = r13.B
            int r1 = java.lang.Math.min(r1, r2)
            r13.C = r1
            int r1 = r13.getPaddingLeft()
            int r0 = r0 - r1
            int r1 = r13.getPaddingRight()
            int r3 = r0 - r1
            int r0 = r13.getMeasuredHeight()
            int r1 = r13.getPaddingTop()
            int r0 = r0 - r1
            int r1 = r13.getPaddingBottom()
            int r5 = r0 - r1
            int r9 = r13.getChildCount()
            r0 = 0
            r8 = r0
        L_0x003b:
            if (r8 < r9) goto L_0x005c
            r0 = 1073741824(0x40000000, float:2.0)
            android.view.View.MeasureSpec.makeMeasureSpec(r3, r0)
            r0 = 1073741824(0x40000000, float:2.0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r5, r0)
            r13.u = r0
            r0 = 1
            r13.v = r0
            r13.b()
            r0 = 0
            r13.v = r0
            int r2 = r13.getChildCount()
            r0 = 0
            r1 = r0
        L_0x0059:
            if (r1 < r2) goto L_0x00dc
            return
        L_0x005c:
            android.view.View r10 = r13.getChildAt(r8)
            int r0 = r10.getVisibility()
            r1 = 8
            if (r0 == r1) goto L_0x00c4
            android.view.ViewGroup$LayoutParams r0 = r10.getLayoutParams()
            android.support.v4.view.bc r0 = (android.support.v4.view.bc) r0
            if (r0 == 0) goto L_0x00c4
            boolean r1 = r0.f369a
            if (r1 == 0) goto L_0x00c4
            int r1 = r0.b
            r6 = r1 & 7
            int r1 = r0.b
            r4 = r1 & 112(0x70, float:1.57E-43)
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            r7 = 48
            if (r4 == r7) goto L_0x00c9
            r7 = 80
            if (r4 == r7) goto L_0x00c9
            r4 = 0
            r7 = r4
        L_0x008a:
            r4 = 3
            if (r6 == r4) goto L_0x00cc
            r4 = 5
            if (r6 == r4) goto L_0x00cc
            r4 = 0
            r6 = r4
        L_0x0092:
            if (r7 == 0) goto L_0x00cf
            r2 = 1073741824(0x40000000, float:2.0)
        L_0x0096:
            int r4 = r0.width
            r11 = -2
            if (r4 == r11) goto L_0x010d
            r4 = 1073741824(0x40000000, float:2.0)
            int r2 = r0.width
            r11 = -1
            if (r2 == r11) goto L_0x010b
            int r2 = r0.width
        L_0x00a4:
            int r11 = r0.height
            r12 = -2
            if (r11 == r12) goto L_0x0109
            r1 = 1073741824(0x40000000, float:2.0)
            int r11 = r0.height
            r12 = -1
            if (r11 == r12) goto L_0x0109
            int r0 = r0.height
        L_0x00b2:
            int r2 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r4)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r1)
            r10.measure(r2, r0)
            if (r7 == 0) goto L_0x00d4
            int r0 = r10.getMeasuredHeight()
            int r5 = r5 - r0
        L_0x00c4:
            int r0 = r8 + 1
            r8 = r0
            goto L_0x003b
        L_0x00c9:
            r4 = 1
            r7 = r4
            goto L_0x008a
        L_0x00cc:
            r4 = 1
            r6 = r4
            goto L_0x0092
        L_0x00cf:
            if (r6 == 0) goto L_0x0096
            r1 = 1073741824(0x40000000, float:2.0)
            goto L_0x0096
        L_0x00d4:
            if (r6 == 0) goto L_0x00c4
            int r0 = r10.getMeasuredWidth()
            int r3 = r3 - r0
            goto L_0x00c4
        L_0x00dc:
            android.view.View r4 = r13.getChildAt(r1)
            int r0 = r4.getVisibility()
            r5 = 8
            if (r0 == r5) goto L_0x0104
            android.view.ViewGroup$LayoutParams r0 = r4.getLayoutParams()
            android.support.v4.view.bc r0 = (android.support.v4.view.bc) r0
            if (r0 == 0) goto L_0x00f4
            boolean r5 = r0.f369a
            if (r5 != 0) goto L_0x0104
        L_0x00f4:
            float r5 = (float) r3
            float r0 = r0.c
            float r0 = r0 * r5
            int r0 = (int) r0
            r5 = 1073741824(0x40000000, float:2.0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r5)
            int r5 = r13.u
            r4.measure(r0, r5)
        L_0x0104:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x0059
        L_0x0109:
            r0 = r5
            goto L_0x00b2
        L_0x010b:
            r2 = r3
            goto L_0x00a4
        L_0x010d:
            r4 = r2
            r2 = r3
            goto L_0x00a4
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.view.ViewPager.onMeasure(int, int):void");
    }

    /* access modifiers changed from: protected */
    public boolean onRequestFocusInDescendants(int i2, Rect rect) {
        int i3;
        bb a2;
        int i4 = -1;
        int childCount = getChildCount();
        if ((i2 & 2) != 0) {
            i4 = 1;
            i3 = 0;
        } else {
            i3 = childCount - 1;
            childCount = -1;
        }
        while (i3 != childCount) {
            View childAt = getChildAt(i3);
            if (childAt.getVisibility() == 0 && (a2 = a(childAt)) != null && a2.b == this.i && childAt.requestFocus(i2, rect)) {
                return true;
            }
            i3 += i4;
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, boolean):void
     arg types: [int, int, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.support.v4.view.bb, int, android.support.v4.view.bb):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean):void */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (this.h != null) {
            this.h.a(savedState.b, savedState.c);
            a(savedState.f355a, false, true);
            return;
        }
        this.j = savedState.f355a;
        this.k = savedState.b;
        this.l = savedState.c;
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f355a = this.i;
        if (this.h != null) {
            savedState.b = this.h.a();
        }
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (i2 != i4) {
            a(i2, i4, this.o, this.o);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, boolean, int):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.view.ViewPager.a(int, int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, int, boolean):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, int, boolean):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.view.ViewPager.a(int, int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean, int):void
      android.support.v4.view.ViewPager.a(int, boolean, int, boolean):void */
    public boolean onTouchEvent(MotionEvent motionEvent) {
        int i2;
        boolean z2 = false;
        if (motionEvent.getAction() == 0 && motionEvent.getEdgeFlags() != 0) {
            return false;
        }
        if (this.h == null || this.h.b() == 0) {
            return false;
        }
        if (this.J == null) {
            this.J = VelocityTracker.obtain();
        }
        this.J.addMovement(motionEvent);
        switch (motionEvent.getAction() & PurchaseCode.AUTH_INVALID_APP) {
            case 0:
                this.m.abortAnimation();
                this.x = false;
                b();
                this.z = true;
                setScrollState(1);
                float x2 = motionEvent.getX();
                this.G = x2;
                this.E = x2;
                float y2 = motionEvent.getY();
                this.H = y2;
                this.F = y2;
                this.I = q.b(motionEvent, 0);
                break;
            case 1:
                if (this.z) {
                    VelocityTracker velocityTracker = this.J;
                    velocityTracker.computeCurrentVelocity(LocationClientOption.MIN_SCAN_SPAN, (float) this.L);
                    int a2 = (int) ad.a(velocityTracker, this.I);
                    this.x = true;
                    int clientWidth = getClientWidth();
                    int scrollX = getScrollX();
                    bb e2 = e();
                    int i3 = e2.b;
                    float f2 = ((((float) scrollX) / ((float) clientWidth)) - e2.e) / e2.d;
                    if (Math.abs((int) (q.c(motionEvent, q.a(motionEvent, this.I)) - this.G)) <= this.M || Math.abs(a2) <= this.K) {
                        i2 = (int) (((float) i3) + f2 + (i3 >= this.i ? 0.4f : 0.6f));
                    } else {
                        if (a2 <= 0) {
                            i3++;
                        }
                        i2 = i3;
                    }
                    if (this.e.size() > 0) {
                        i2 = Math.max(((bb) this.e.get(0)).b, Math.min(i2, ((bb) this.e.get(this.e.size() - 1)).b));
                    }
                    a(i2, true, true, a2);
                    this.I = -1;
                    f();
                    z2 = this.O.c() | this.P.c();
                    break;
                }
                break;
            case 2:
                if (!this.z) {
                    int a3 = q.a(motionEvent, this.I);
                    float c2 = q.c(motionEvent, a3);
                    float abs = Math.abs(c2 - this.E);
                    float d2 = q.d(motionEvent, a3);
                    float abs2 = Math.abs(d2 - this.F);
                    if (abs > ((float) this.D) && abs > abs2) {
                        this.z = true;
                        this.E = c2 - this.G > 0.0f ? this.G + ((float) this.D) : this.G - ((float) this.D);
                        this.F = d2;
                        setScrollState(1);
                        setScrollingCacheEnabled(true);
                    }
                }
                if (this.z) {
                    z2 = a(q.c(motionEvent, q.a(motionEvent, this.I))) | false;
                    break;
                }
                break;
            case 3:
                if (this.z) {
                    a(this.i, true, 0, false);
                    this.I = -1;
                    f();
                    z2 = this.O.c() | this.P.c();
                    break;
                }
                break;
            case 5:
                int b2 = q.b(motionEvent);
                this.E = q.c(motionEvent, b2);
                this.I = q.b(motionEvent, b2);
                break;
            case 6:
                a(motionEvent);
                this.E = q.c(motionEvent, q.a(motionEvent, this.I));
                break;
        }
        if (z2) {
            ah.b(this);
        }
        return true;
    }

    public void removeView(View view) {
        if (this.v) {
            removeViewInLayout(view);
        } else {
            super.removeView(view);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, boolean):void
     arg types: [int, int, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.support.v4.view.bb, int, android.support.v4.view.bb):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean):void */
    public void setAdapter(u uVar) {
        if (this.h != null) {
            this.h.b(this.n);
            u uVar2 = this.h;
            for (int i2 = 0; i2 < this.e.size(); i2++) {
                bb bbVar = (bb) this.e.get(i2);
                this.h.a(this, bbVar.b, bbVar.f368a);
            }
            this.h.a((ViewGroup) this);
            this.e.clear();
            int i3 = 0;
            while (i3 < getChildCount()) {
                if (!((bc) getChildAt(i3).getLayoutParams()).f369a) {
                    removeViewAt(i3);
                    i3--;
                }
                i3++;
            }
            this.i = 0;
            scrollTo(0, 0);
        }
        u uVar3 = this.h;
        this.h = uVar;
        this.b = 0;
        if (this.h != null) {
            if (this.n == null) {
                this.n = new bg(this, (byte) 0);
            }
            this.h.a((DataSetObserver) this.n);
            this.x = false;
            boolean z2 = this.Q;
            this.Q = true;
            this.b = this.h.b();
            if (this.j >= 0) {
                this.h.a(this.k, this.l);
                a(this.j, false, true);
                this.j = -1;
                this.k = null;
                this.l = null;
            } else if (!z2) {
                b();
            } else {
                requestLayout();
            }
        }
        if (this.V != null && uVar3 != uVar) {
            this.V.a(uVar3, uVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void setChildrenDrawingOrderEnabledCompat(boolean z2) {
        if (Build.VERSION.SDK_INT >= 7) {
            if (this.W == null) {
                Class<ViewGroup> cls = ViewGroup.class;
                try {
                    this.W = cls.getDeclaredMethod("setChildrenDrawingOrderEnabled", Boolean.TYPE);
                } catch (NoSuchMethodException e2) {
                    Log.e("ViewPager", "Can't find setChildrenDrawingOrderEnabled", e2);
                }
            }
            try {
                this.W.invoke(this, Boolean.valueOf(z2));
            } catch (Exception e3) {
                Log.e("ViewPager", "Error changing children drawing order", e3);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, boolean):void
     arg types: [int, boolean, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.support.v4.view.bb, int, android.support.v4.view.bb):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean):void */
    public void setCurrentItem(int i2) {
        this.x = false;
        a(i2, !this.Q, false);
    }

    public void setOffscreenPageLimit(int i2) {
        if (i2 <= 0) {
            Log.w("ViewPager", "Requested offscreen page limit " + i2 + " too small; defaulting to 1");
            i2 = 1;
        }
        if (i2 != this.y) {
            this.y = i2;
            b();
        }
    }

    /* access modifiers changed from: package-private */
    public void setOnAdapterChangeListener(be beVar) {
        this.V = beVar;
    }

    public void setOnPageChangeListener(bf bfVar) {
        this.T = bfVar;
    }

    public void setPageMargin(int i2) {
        int i3 = this.o;
        this.o = i2;
        int width = getWidth();
        a(width, width, i2, i3);
        requestLayout();
    }

    public void setPageMarginDrawable(int i2) {
        setPageMarginDrawable(getContext().getResources().getDrawable(i2));
    }

    public void setPageMarginDrawable(Drawable drawable) {
        this.p = drawable;
        if (drawable != null) {
            refreshDrawableState();
        }
        setWillNotDraw(drawable == null);
        invalidate();
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.p;
    }
}
