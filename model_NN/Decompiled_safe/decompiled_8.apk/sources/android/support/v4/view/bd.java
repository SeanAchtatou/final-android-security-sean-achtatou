package android.support.v4.view;

import android.os.Bundle;
import android.support.v4.view.a.a;
import android.support.v4.view.a.k;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;

final class bd extends a {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ViewPager f370a;

    bd(ViewPager viewPager) {
        this.f370a = viewPager;
    }

    private boolean b() {
        return this.f370a.h != null && this.f370a.h.b() > 1;
    }

    public final void a(View view, a aVar) {
        super.a(view, aVar);
        aVar.b(ViewPager.class.getName());
        aVar.i(b());
        if (this.f370a.canScrollHorizontally(1)) {
            aVar.a(4096);
        }
        if (this.f370a.canScrollHorizontally(-1)) {
            aVar.a(8192);
        }
    }

    public final boolean a(View view, int i, Bundle bundle) {
        if (super.a(view, i, bundle)) {
            return true;
        }
        switch (i) {
            case 4096:
                if (!this.f370a.canScrollHorizontally(1)) {
                    return false;
                }
                this.f370a.setCurrentItem(this.f370a.i + 1);
                return true;
            case 8192:
                if (!this.f370a.canScrollHorizontally(-1)) {
                    return false;
                }
                this.f370a.setCurrentItem(this.f370a.i - 1);
                return true;
            default:
                return false;
        }
    }

    public final void d(View view, AccessibilityEvent accessibilityEvent) {
        super.d(view, accessibilityEvent);
        accessibilityEvent.setClassName(ViewPager.class.getName());
        k a2 = k.a();
        a2.a(b());
        if (accessibilityEvent.getEventType() == 4096 && this.f370a.h != null) {
            a2.a(this.f370a.h.b());
            a2.b(this.f370a.i);
            a2.c(this.f370a.i);
        }
    }
}
