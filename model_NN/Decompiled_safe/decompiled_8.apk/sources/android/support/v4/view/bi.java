package android.support.v4.view;

import android.view.View;
import java.util.Comparator;

final class bi implements Comparator {
    bi() {
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        bc bcVar = (bc) ((View) obj).getLayoutParams();
        bc bcVar2 = (bc) ((View) obj2).getLayoutParams();
        return bcVar.f369a != bcVar2.f369a ? bcVar.f369a ? 1 : -1 : bcVar.e - bcVar2.e;
    }
}
