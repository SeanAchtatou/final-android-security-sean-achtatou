package android.support.v4.view;

import android.os.Bundle;
import android.support.v4.view.a.g;
import android.view.View;
import android.view.accessibility.AccessibilityNodeProvider;

final class d extends b {
    d() {
    }

    public final g a(Object obj, View view) {
        AccessibilityNodeProvider accessibilityNodeProvider = ((View.AccessibilityDelegate) obj).getAccessibilityNodeProvider(view);
        if (accessibilityNodeProvider != null) {
            return new g(accessibilityNodeProvider);
        }
        return null;
    }

    public final Object a(a aVar) {
        return new g(new h(aVar));
    }

    public final boolean a(Object obj, View view, int i, Bundle bundle) {
        return ((View.AccessibilityDelegate) obj).performAccessibilityAction(view, i, bundle);
    }
}
