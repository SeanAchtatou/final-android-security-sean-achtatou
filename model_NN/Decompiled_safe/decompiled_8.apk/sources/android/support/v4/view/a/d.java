package android.support.v4.view.a;

import android.view.accessibility.AccessibilityNodeInfo;

class d extends b {
    d() {
    }

    public final void b(Object obj, int i) {
        ((AccessibilityNodeInfo) obj).setMovementGranularities(i);
    }

    public final void h(Object obj, boolean z) {
        ((AccessibilityNodeInfo) obj).setVisibleToUser(z);
    }

    public final void i(Object obj, boolean z) {
        ((AccessibilityNodeInfo) obj).setAccessibilityFocused(z);
    }

    public final int r(Object obj) {
        return ((AccessibilityNodeInfo) obj).getMovementGranularities();
    }

    public final boolean s(Object obj) {
        return ((AccessibilityNodeInfo) obj).isVisibleToUser();
    }

    public final boolean t(Object obj) {
        return ((AccessibilityNodeInfo) obj).isAccessibilityFocused();
    }
}
