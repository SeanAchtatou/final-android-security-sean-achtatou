package android.support.v4.view;

import android.database.DataSetObserver;

final class bg extends DataSetObserver {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ViewPager f371a;

    private bg(ViewPager viewPager) {
        this.f371a = viewPager;
    }

    /* synthetic */ bg(ViewPager viewPager, byte b) {
        this(viewPager);
    }

    public final void onChanged() {
        this.f371a.a();
    }

    public final void onInvalidated() {
        this.f371a.a();
    }
}
