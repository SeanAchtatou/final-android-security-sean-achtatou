package android.support.v4.view;

import android.view.View;
import android.view.ViewParent;

class am extends al {
    am() {
    }

    public final void a(View view, int i, int i2, int i3, int i4) {
        view.postInvalidate(i, i2, i3, i4);
    }

    public final void a(View view, Runnable runnable) {
        view.postOnAnimation(runnable);
    }

    public final void d(View view) {
        view.postInvalidateOnAnimation();
    }

    public final int e(View view) {
        return view.getImportantForAccessibility();
    }

    public final void f(View view) {
        view.setImportantForAccessibility(1);
    }

    public final ViewParent g(View view) {
        return view.getParentForAccessibility();
    }
}
