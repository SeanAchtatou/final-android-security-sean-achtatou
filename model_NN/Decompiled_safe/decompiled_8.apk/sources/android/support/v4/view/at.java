package android.support.v4.view;

import android.os.Build;
import android.view.ViewGroup;

public final class at {

    /* renamed from: a  reason: collision with root package name */
    private static aw f366a;

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 14) {
            f366a = new av();
        } else if (i >= 11) {
            f366a = new au();
        } else {
            f366a = new aw();
        }
    }

    public static void a(ViewGroup viewGroup) {
        f366a.a(viewGroup);
    }
}
