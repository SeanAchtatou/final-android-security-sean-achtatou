package android.support.v4.view;

import android.os.Build;
import android.view.KeyEvent;

public final class m {

    /* renamed from: a  reason: collision with root package name */
    private static p f377a;

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            f377a = new o();
        } else {
            f377a = new p();
        }
    }

    public static boolean a(KeyEvent keyEvent) {
        return f377a.b(keyEvent.getMetaState());
    }

    public static boolean b(KeyEvent keyEvent) {
        return f377a.c(keyEvent.getMetaState());
    }

    public static void c(KeyEvent keyEvent) {
        f377a.a(keyEvent);
    }
}
