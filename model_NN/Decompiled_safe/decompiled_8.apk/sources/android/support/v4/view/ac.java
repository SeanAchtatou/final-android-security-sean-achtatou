package android.support.v4.view;

import android.content.Context;
import android.text.method.SingleLineTransformationMethod;
import android.view.View;
import java.util.Locale;

final class ac extends SingleLineTransformationMethod {

    /* renamed from: a  reason: collision with root package name */
    private Locale f362a;

    public ac(Context context) {
        this.f362a = context.getResources().getConfiguration().locale;
    }

    public final CharSequence getTransformation(CharSequence charSequence, View view) {
        CharSequence transformation = super.getTransformation(charSequence, view);
        if (transformation != null) {
            return transformation.toString().toUpperCase(this.f362a);
        }
        return null;
    }
}
