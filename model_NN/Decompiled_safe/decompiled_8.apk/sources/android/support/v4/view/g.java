package android.support.v4.view;

import android.os.Bundle;
import android.support.v4.view.a.a;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;

final class g extends View.AccessibilityDelegate {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ h f374a;

    g(h hVar) {
        this.f374a = hVar;
    }

    public final boolean dispatchPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
        a aVar = this.f374a.f375a;
        return a.b(view, accessibilityEvent);
    }

    public final AccessibilityNodeProvider getAccessibilityNodeProvider(View view) {
        a aVar = this.f374a.f375a;
        android.support.v4.view.a.g a2 = a.a(view);
        return (AccessibilityNodeProvider) (a2 != null ? a2.a() : null);
    }

    public final void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
        this.f374a.f375a.d(view, accessibilityEvent);
    }

    public final void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfo accessibilityNodeInfo) {
        this.f374a.f375a.a(view, new a(accessibilityNodeInfo));
    }

    public final void onPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
        a aVar = this.f374a.f375a;
        a.c(view, accessibilityEvent);
    }

    public final boolean onRequestSendAccessibilityEvent(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        return this.f374a.f375a.a(viewGroup, view, accessibilityEvent);
    }

    public final boolean performAccessibilityAction(View view, int i, Bundle bundle) {
        return this.f374a.f375a.a(view, i, bundle);
    }

    public final void sendAccessibilityEvent(View view, int i) {
        a aVar = this.f374a.f375a;
        a.a(view, i);
    }

    public final void sendAccessibilityEventUnchecked(View view, AccessibilityEvent accessibilityEvent) {
        a aVar = this.f374a.f375a;
        a.a(view, accessibilityEvent);
    }
}
