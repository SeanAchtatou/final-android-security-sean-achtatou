package android.support.v4.view;

import android.os.Build;

public final class i {

    /* renamed from: a  reason: collision with root package name */
    private static j f376a;

    static {
        if (Build.VERSION.SDK_INT >= 17) {
            f376a = new l();
        } else {
            f376a = new k();
        }
    }

    public static int a(int i, int i2) {
        return f376a.a(i, i2);
    }
}
