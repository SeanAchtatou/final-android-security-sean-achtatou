package android.support.v4.view.a;

import android.os.Build;

public final class k {

    /* renamed from: a  reason: collision with root package name */
    private static final n f361a;
    private final Object b;

    static {
        if (Build.VERSION.SDK_INT >= 16) {
            f361a = new o();
        } else if (Build.VERSION.SDK_INT >= 15) {
            f361a = new m();
        } else if (Build.VERSION.SDK_INT >= 14) {
            f361a = new l();
        } else {
            f361a = new n();
        }
    }

    private k(Object obj) {
        this.b = obj;
    }

    public static k a() {
        return new k(f361a.a());
    }

    public final void a(int i) {
        f361a.b(this.b, i);
    }

    public final void a(boolean z) {
        f361a.a(this.b, z);
    }

    public final void b(int i) {
        f361a.a(this.b, i);
    }

    public final void c(int i) {
        f361a.c(this.b, i);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        k kVar = (k) obj;
        return this.b == null ? kVar.b == null : this.b.equals(kVar.b);
    }

    public final int hashCode() {
        if (this.b == null) {
            return 0;
        }
        return this.b.hashCode();
    }
}
