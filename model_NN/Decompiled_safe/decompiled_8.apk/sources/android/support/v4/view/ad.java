package android.support.v4.view;

import android.os.Build;
import android.view.VelocityTracker;

public final class ad {

    /* renamed from: a  reason: collision with root package name */
    private static ag f363a;

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            f363a = new af();
        } else {
            f363a = new ae();
        }
    }

    public static float a(VelocityTracker velocityTracker, int i) {
        return f363a.a(velocityTracker, i);
    }

    public static float b(VelocityTracker velocityTracker, int i) {
        return f363a.b(velocityTracker, i);
    }
}
