package android.support.v4.view.a;

import android.graphics.Rect;
import android.os.Build;
import android.view.View;
import com.mapabc.minimap.map.vmap.NativeMapEngine;
import mm.purchasesdk.PurchaseCode;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static final c f357a;
    private final Object b;

    static {
        if ("JellyBeanMR2".equals(Build.VERSION.CODENAME)) {
            f357a = new e();
        } else if (Build.VERSION.SDK_INT >= 16) {
            f357a = new d();
        } else if (Build.VERSION.SDK_INT >= 14) {
            f357a = new b();
        } else {
            f357a = new c();
        }
    }

    public a(Object obj) {
        this.b = obj;
    }

    public static a a(a aVar) {
        Object a2 = f357a.a(aVar.b);
        if (a2 != null) {
            return new a(a2);
        }
        return null;
    }

    public final Object a() {
        return this.b;
    }

    public final void a(int i) {
        f357a.a(this.b, i);
    }

    public final void a(Rect rect) {
        f357a.a(this.b, rect);
    }

    public final void a(View view) {
        f357a.c(this.b, view);
    }

    public final void a(CharSequence charSequence) {
        f357a.c(this.b, charSequence);
    }

    public final void a(boolean z) {
        f357a.c(this.b, z);
    }

    public final int b() {
        return f357a.b(this.b);
    }

    public final void b(int i) {
        f357a.b(this.b, i);
    }

    public final void b(Rect rect) {
        f357a.c(this.b, rect);
    }

    public final void b(View view) {
        f357a.a(this.b, view);
    }

    public final void b(CharSequence charSequence) {
        f357a.a(this.b, charSequence);
    }

    public final void b(boolean z) {
        f357a.d(this.b, z);
    }

    public final int c() {
        return f357a.r(this.b);
    }

    public final void c(Rect rect) {
        f357a.b(this.b, rect);
    }

    public final void c(View view) {
        f357a.b(this.b, view);
    }

    public final void c(CharSequence charSequence) {
        f357a.b(this.b, charSequence);
    }

    public final void c(boolean z) {
        f357a.h(this.b, z);
    }

    public final void d(Rect rect) {
        f357a.d(this.b, rect);
    }

    public final void d(boolean z) {
        f357a.i(this.b, z);
    }

    public final boolean d() {
        return f357a.k(this.b);
    }

    public final void e(boolean z) {
        f357a.g(this.b, z);
    }

    public final boolean e() {
        return f357a.l(this.b);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        a aVar = (a) obj;
        return this.b == null ? aVar.b == null : this.b.equals(aVar.b);
    }

    public final void f(boolean z) {
        f357a.a(this.b, z);
    }

    public final boolean f() {
        return f357a.s(this.b);
    }

    public final void g(boolean z) {
        f357a.e(this.b, z);
    }

    public final boolean g() {
        return f357a.t(this.b);
    }

    public final void h(boolean z) {
        f357a.b(this.b, z);
    }

    public final boolean h() {
        return f357a.p(this.b);
    }

    public final int hashCode() {
        if (this.b == null) {
            return 0;
        }
        return this.b.hashCode();
    }

    public final void i(boolean z) {
        f357a.f(this.b, z);
    }

    public final boolean i() {
        return f357a.i(this.b);
    }

    public final boolean j() {
        return f357a.m(this.b);
    }

    public final boolean k() {
        return f357a.j(this.b);
    }

    public final CharSequence l() {
        return f357a.e(this.b);
    }

    public final CharSequence m() {
        return f357a.c(this.b);
    }

    public final CharSequence n() {
        return f357a.d(this.b);
    }

    public final void o() {
        f357a.q(this.b);
    }

    public final String toString() {
        String str;
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        Rect rect = new Rect();
        a(rect);
        sb.append("; boundsInParent: " + rect);
        c(rect);
        sb.append("; boundsInScreen: " + rect);
        sb.append("; packageName: ").append(l());
        sb.append("; className: ").append(m());
        sb.append("; text: ").append(f357a.f(this.b));
        sb.append("; contentDescription: ").append(n());
        sb.append("; viewId: ").append(f357a.u(this.b));
        sb.append("; checkable: ").append(f357a.g(this.b));
        sb.append("; checked: ").append(f357a.h(this.b));
        sb.append("; focusable: ").append(d());
        sb.append("; focused: ").append(e());
        sb.append("; selected: ").append(h());
        sb.append("; clickable: ").append(i());
        sb.append("; longClickable: ").append(j());
        sb.append("; enabled: ").append(k());
        sb.append("; password: ").append(f357a.n(this.b));
        sb.append("; scrollable: " + f357a.o(this.b));
        sb.append("; [");
        int b2 = b();
        while (b2 != 0) {
            int numberOfTrailingZeros = 1 << Integer.numberOfTrailingZeros(b2);
            int i = (numberOfTrailingZeros ^ -1) & b2;
            switch (numberOfTrailingZeros) {
                case 1:
                    str = "ACTION_FOCUS";
                    break;
                case 2:
                    str = "ACTION_CLEAR_FOCUS";
                    break;
                case 4:
                    str = "ACTION_SELECT";
                    break;
                case 8:
                    str = "ACTION_CLEAR_SELECTION";
                    break;
                case 16:
                    str = "ACTION_CLICK";
                    break;
                case 32:
                    str = "ACTION_LONG_CLICK";
                    break;
                case 64:
                    str = "ACTION_ACCESSIBILITY_FOCUS";
                    break;
                case NativeMapEngine.MAX_ICON_SIZE /*128*/:
                    str = "ACTION_CLEAR_ACCESSIBILITY_FOCUS";
                    break;
                case 256:
                    str = "ACTION_NEXT_AT_MOVEMENT_GRANULARITY";
                    break;
                case PurchaseCode.QUERY_NO_APP /*512*/:
                    str = "ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY";
                    break;
                case 1024:
                    str = "ACTION_NEXT_HTML_ELEMENT";
                    break;
                case 2048:
                    str = "ACTION_PREVIOUS_HTML_ELEMENT";
                    break;
                case 4096:
                    str = "ACTION_SCROLL_FORWARD";
                    break;
                case 8192:
                    str = "ACTION_SCROLL_BACKWARD";
                    break;
                case 16384:
                    str = "ACTION_COPY";
                    break;
                case 32768:
                    str = "ACTION_PASTE";
                    break;
                case 65536:
                    str = "ACTION_CUT";
                    break;
                case 131072:
                    str = "ACTION_SET_SELECTION";
                    break;
                default:
                    str = "ACTION_UNKNOWN";
                    break;
            }
            sb.append(str);
            if (i != 0) {
                sb.append(", ");
            }
            b2 = i;
        }
        sb.append("]");
        return sb.toString();
    }
}
