package android.support.v4.view;

import android.os.Build;
import android.view.ViewConfiguration;

public final class ap {

    /* renamed from: a  reason: collision with root package name */
    private static as f365a;

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            f365a = new ar();
        } else {
            f365a = new aq();
        }
    }

    public static int a(ViewConfiguration viewConfiguration) {
        return f365a.a(viewConfiguration);
    }
}
