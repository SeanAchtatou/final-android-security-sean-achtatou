package android.support.v4.view;

import android.view.KeyEvent;
import com.mapabc.minimap.map.vmap.NativeMapEngine;
import mm.purchasesdk.PurchaseCode;

class p {
    p() {
    }

    private static int a(int i, int i2, int i3, int i4) {
        boolean z = true;
        boolean z2 = (i2 & 1) != 0;
        int i5 = i3 | i4;
        if ((i5 & 1) == 0) {
            z = false;
        }
        if (!z2) {
            return z ? i & (i2 ^ -1) : i;
        }
        if (!z) {
            return i & (i5 ^ -1);
        }
        throw new IllegalArgumentException("bad arguments");
    }

    public int a(int i) {
        int i2 = (i & 192) != 0 ? i | 1 : i;
        if ((i2 & 48) != 0) {
            i2 |= 2;
        }
        return i2 & PurchaseCode.AUTH_PAYCODE_ERROR;
    }

    public void a(KeyEvent keyEvent) {
    }

    public boolean b(int i) {
        return a(a(a(i) & PurchaseCode.AUTH_PAYCODE_ERROR, 1, 64, NativeMapEngine.MAX_ICON_SIZE), 2, 16, 32) == 1;
    }

    public boolean c(int i) {
        return (a(i) & PurchaseCode.AUTH_PAYCODE_ERROR) == 0;
    }
}
