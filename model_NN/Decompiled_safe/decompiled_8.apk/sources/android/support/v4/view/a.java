package android.support.v4.view;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.a.g;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final c f356a;
    private static final Object b = f356a.a();
    private Object c = f356a.a(this);

    static {
        if (Build.VERSION.SDK_INT >= 16) {
            f356a = new d();
        } else if (Build.VERSION.SDK_INT >= 14) {
            f356a = new b();
        } else {
            f356a = new c();
        }
    }

    public static g a(View view) {
        return f356a.a(b, view);
    }

    public static void a(View view, int i) {
        f356a.a(b, view, i);
    }

    public static void a(View view, AccessibilityEvent accessibilityEvent) {
        f356a.d(b, view, accessibilityEvent);
    }

    public static boolean b(View view, AccessibilityEvent accessibilityEvent) {
        return f356a.a(b, view, accessibilityEvent);
    }

    public static void c(View view, AccessibilityEvent accessibilityEvent) {
        f356a.c(b, view, accessibilityEvent);
    }

    /* access modifiers changed from: package-private */
    public final Object a() {
        return this.c;
    }

    public void a(View view, android.support.v4.view.a.a aVar) {
        f356a.a(b, view, aVar);
    }

    public boolean a(View view, int i, Bundle bundle) {
        return f356a.a(b, view, i, bundle);
    }

    public boolean a(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        return f356a.a(b, viewGroup, view, accessibilityEvent);
    }

    public void d(View view, AccessibilityEvent accessibilityEvent) {
        f356a.b(b, view, accessibilityEvent);
    }
}
