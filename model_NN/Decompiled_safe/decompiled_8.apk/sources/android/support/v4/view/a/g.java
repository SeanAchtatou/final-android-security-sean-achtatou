package android.support.v4.view.a;

import android.os.Build;
import java.util.List;

public final class g {

    /* renamed from: a  reason: collision with root package name */
    private static final f f358a;
    private final Object b;

    static {
        if (Build.VERSION.SDK_INT >= 16) {
            f358a = new h();
        } else {
            f358a = new f();
        }
    }

    public g() {
        this.b = f358a.a(this);
    }

    public g(Object obj) {
        this.b = obj;
    }

    public static a b() {
        return null;
    }

    public static boolean c() {
        return false;
    }

    public static List d() {
        return null;
    }

    public final Object a() {
        return this.b;
    }
}
