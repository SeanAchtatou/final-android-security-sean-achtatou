package android.support.v4.view;

import android.graphics.Paint;
import android.view.View;

final class an extends am {
    an() {
    }

    public final void a(View view, Paint paint) {
        view.setLayerPaint(paint);
    }

    public final int h(View view) {
        return view.getLayoutDirection();
    }
}
