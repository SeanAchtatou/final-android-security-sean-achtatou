package android.support.v4.view;

import android.database.DataSetObserver;

final class y extends DataSetObserver implements be, bf {

    /* renamed from: a  reason: collision with root package name */
    private int f383a;
    private /* synthetic */ x b;

    private y(x xVar) {
        this.b = xVar;
    }

    /* synthetic */ y(x xVar, byte b2) {
        this(xVar);
    }

    public final void a(int i, float f) {
        if (f > 0.5f) {
            i++;
        }
        this.b.a(i, f, false);
    }

    public final void a(u uVar, u uVar2) {
        this.b.a(uVar, uVar2);
    }

    public final void a_(int i) {
        float f = 0.0f;
        if (this.f383a == 0) {
            this.b.a(this.b.f382a.getCurrentItem(), this.b.f382a.getAdapter());
            if (this.b.g >= 0.0f) {
                f = this.b.g;
            }
            this.b.a(this.b.f382a.getCurrentItem(), f, true);
        }
    }

    public final void b(int i) {
        this.f383a = i;
    }

    public final void onChanged() {
        float f = 0.0f;
        this.b.a(this.b.f382a.getCurrentItem(), this.b.f382a.getAdapter());
        if (this.b.g >= 0.0f) {
            f = this.b.g;
        }
        this.b.a(this.b.f382a.getCurrentItem(), f, true);
    }
}
