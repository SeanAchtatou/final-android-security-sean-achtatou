package android.support.v4.view;

import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;

public abstract class u {

    /* renamed from: a  reason: collision with root package name */
    private DataSetObservable f379a = new DataSetObservable();

    public Parcelable a() {
        return null;
    }

    public Object a(ViewGroup viewGroup, int i) {
        throw new UnsupportedOperationException("Required method instantiateItem was not overridden");
    }

    public final void a(DataSetObserver dataSetObserver) {
        this.f379a.registerObserver(dataSetObserver);
    }

    public void a(Parcelable parcelable, ClassLoader classLoader) {
    }

    public void a(ViewGroup viewGroup) {
    }

    public void a(ViewGroup viewGroup, int i, Object obj) {
        throw new UnsupportedOperationException("Required method destroyItem was not overridden");
    }

    public void a(Object obj) {
    }

    public abstract boolean a(View view, Object obj);

    public abstract int b();

    public final void b(DataSetObserver dataSetObserver) {
        this.f379a.unregisterObserver(dataSetObserver);
    }

    public final void c() {
        this.f379a.notifyChanged();
    }
}
