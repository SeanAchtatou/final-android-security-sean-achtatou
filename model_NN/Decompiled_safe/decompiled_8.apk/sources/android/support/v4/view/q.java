package android.support.v4.view;

import android.os.Build;
import android.view.MotionEvent;
import mm.purchasesdk.PurchaseCode;

public final class q {

    /* renamed from: a  reason: collision with root package name */
    private static t f378a;

    static {
        if (Build.VERSION.SDK_INT >= 5) {
            f378a = new s();
        } else {
            f378a = new r();
        }
    }

    public static int a(MotionEvent motionEvent) {
        return motionEvent.getAction() & PurchaseCode.AUTH_INVALID_APP;
    }

    public static int a(MotionEvent motionEvent, int i) {
        return f378a.a(motionEvent, i);
    }

    public static int b(MotionEvent motionEvent) {
        return (motionEvent.getAction() & 65280) >> 8;
    }

    public static int b(MotionEvent motionEvent, int i) {
        return f378a.b(motionEvent, i);
    }

    public static float c(MotionEvent motionEvent, int i) {
        return f378a.c(motionEvent, i);
    }

    public static int c(MotionEvent motionEvent) {
        return f378a.a(motionEvent);
    }

    public static float d(MotionEvent motionEvent, int i) {
        return f378a.d(motionEvent, i);
    }
}
