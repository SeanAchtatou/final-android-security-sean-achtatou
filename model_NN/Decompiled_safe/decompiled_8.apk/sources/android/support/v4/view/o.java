package android.support.v4.view;

import android.view.KeyEvent;

final class o extends n {
    o() {
    }

    public final int a(int i) {
        return KeyEvent.normalizeMetaState(i);
    }

    public final boolean b(int i) {
        return KeyEvent.metaStateHasModifiers(i, 1);
    }

    public final boolean c(int i) {
        return KeyEvent.metaStateHasNoModifiers(i);
    }
}
