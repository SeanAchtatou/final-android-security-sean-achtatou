package android.support.v4.view;

import android.graphics.Paint;
import android.os.Build;
import android.view.View;
import android.view.ViewParent;

public final class ah {

    /* renamed from: a  reason: collision with root package name */
    private static ao f364a;

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 17) {
            f364a = new an();
        } else if (i >= 16) {
            f364a = new am();
        } else if (i >= 14) {
            f364a = new al();
        } else if (i >= 11) {
            f364a = new ak();
        } else if (i >= 9) {
            f364a = new aj();
        } else {
            f364a = new ao();
        }
    }

    public static int a(View view) {
        return f364a.b(view);
    }

    public static void a(View view, int i, int i2, int i3, int i4) {
        f364a.a(view, i, i2, i3, i4);
    }

    public static void a(View view, int i, Paint paint) {
        f364a.a(view, i, paint);
    }

    public static void a(View view, Paint paint) {
        f364a.a(view, paint);
    }

    public static void a(View view, a aVar) {
        f364a.a(view, aVar);
    }

    public static void a(View view, Runnable runnable) {
        f364a.a(view, runnable);
    }

    public static boolean a(View view, int i) {
        return f364a.a(view, i);
    }

    public static void b(View view) {
        f364a.d(view);
    }

    public static int c(View view) {
        return f364a.e(view);
    }

    public static void d(View view) {
        f364a.f(view);
    }

    public static int e(View view) {
        return f364a.c(view);
    }

    public static int f(View view) {
        return f364a.h(view);
    }

    public static ViewParent g(View view) {
        return f364a.g(view);
    }

    public static boolean h(View view) {
        return f364a.a(view);
    }
}
