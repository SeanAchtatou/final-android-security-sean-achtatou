package android.support.v4.c;

import android.util.Log;
import com.mapabc.minimap.map.vmap.NativeMapEngine;
import java.io.Writer;

public final class a extends Writer {

    /* renamed from: a  reason: collision with root package name */
    private final String f352a;
    private StringBuilder b = new StringBuilder((int) NativeMapEngine.MAX_ICON_SIZE);

    public a(String str) {
        this.f352a = str;
    }

    private void a() {
        if (this.b.length() > 0) {
            Log.d(this.f352a, this.b.toString());
            this.b.delete(0, this.b.length());
        }
    }

    public final void close() {
        a();
    }

    public final void flush() {
        a();
    }

    public final void write(char[] cArr, int i, int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            char c = cArr[i + i3];
            if (c == 10) {
                a();
            } else {
                this.b.append(c);
            }
        }
    }
}
