package com.baidu.location;

import com.baidu.location.c;
import java.io.File;
import java.io.RandomAccessFile;
import java.util.ArrayList;

class h {

    /* renamed from: a  reason: collision with root package name */
    private static int f582a = 100;

    /* renamed from: do  reason: not valid java name */
    private static String f171do = (f.ac + "/juz.dat");

    /* renamed from: for  reason: not valid java name */
    private static float f172for = 299.0f;

    /* renamed from: if  reason: not valid java name */
    private static String f173if = f.v;

    /* renamed from: int  reason: not valid java name */
    private static ArrayList f174int = null;

    /* renamed from: new  reason: not valid java name */
    private static int f175new = 64;

    /* renamed from: try  reason: not valid java name */
    private static long f176try = 64;

    class a {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public int f583a = 0;
        /* access modifiers changed from: private */

        /* renamed from: do  reason: not valid java name */
        public int f177do = 0;
        /* access modifiers changed from: private */

        /* renamed from: for  reason: not valid java name */
        public float f178for = 0.0f;
        /* access modifiers changed from: private */

        /* renamed from: if  reason: not valid java name */
        public int f179if = 0;
        /* access modifiers changed from: private */

        /* renamed from: int  reason: not valid java name */
        public double f180int = 0.0d;
        /* access modifiers changed from: private */

        /* renamed from: new  reason: not valid java name */
        public double f181new = 0.0d;
        /* access modifiers changed from: private */

        /* renamed from: try  reason: not valid java name */
        public int f182try = 0;

        public a(int i, int i2, int i3, int i4, double d, double d2, float f) {
            this.f177do = i;
            this.f182try = i2;
            this.f179if = i3;
            this.f583a = i4;
            this.f181new = d;
            this.f180int = d2;
            this.f178for = f;
        }

        public boolean a(int i, int i2, int i3) {
            return this.f583a == i && this.f177do == i2 && this.f182try == i3;
        }
    }

    h() {
    }

    public static String a(int i, int i2, int i3) {
        a aVar = m227if(i, i2, i3);
        if (aVar == null) {
            return null;
        }
        return String.format("{\"result\":{\"time\":\"" + j.m245for() + "\",\"error\":\"65\"},\"content\":{\"point\":{\"x\":\"%f\",\"y\":\"%f\"},\"radius\":\"%d\"}}", Double.valueOf(aVar.f181new), Double.valueOf(aVar.f180int), Integer.valueOf((int) aVar.f178for));
    }

    private static void a() {
        File file = new File(f171do);
        try {
            if (!file.exists()) {
                j.m250if(f173if, "locCache file does not exists...");
                return;
            }
            if (f174int != null) {
                f174int.clear();
                f174int = null;
            }
            f174int = new ArrayList();
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
            randomAccessFile.seek(0);
            int readInt = randomAccessFile.readInt();
            j.m250if(f173if, "size of loc cache is " + readInt);
            for (int i = 0; i < readInt; i++) {
                randomAccessFile.seek(f176try + ((long) (f175new * i)));
                float readFloat = randomAccessFile.readFloat();
                f174int.add(new a(randomAccessFile.readInt(), randomAccessFile.readInt(), randomAccessFile.readInt(), randomAccessFile.readInt(), randomAccessFile.readDouble(), randomAccessFile.readDouble(), readFloat));
            }
            randomAccessFile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(c.a aVar, double d, double d2, float f) {
        if (aVar != null) {
            float f2 = f < f172for ? f172for : f;
            a aVar2 = m227if(aVar.f109if, aVar.f108for, aVar.f112try);
            if (aVar2 == null) {
                if (f174int == null) {
                    f174int = new ArrayList();
                }
                f174int.add(new a(aVar.f108for, aVar.f112try, aVar.f107do, aVar.f109if, d, d2, f2));
                if (f174int.size() > f582a) {
                    f174int.remove(0);
                }
                j.m250if(f173if, "locCache add new cell info into loc cache ...");
                return;
            }
            double unused = aVar2.f181new = d;
            double unused2 = aVar2.f180int = d2;
            float unused3 = aVar2.f178for = f2;
            j.m250if(f173if, "locCache update loc cache ...");
        }
    }

    /* renamed from: do  reason: not valid java name */
    private static void m226do() {
        if (f174int != null) {
            File file = new File(f171do);
            try {
                if (!file.exists()) {
                    File file2 = new File(f.ac);
                    if (!file2.exists()) {
                        j.m250if(f173if, "locCache make dirs " + file2.mkdirs());
                    }
                    if (file.createNewFile()) {
                        j.m250if(f173if, "locCache create loc cache file success ...");
                    } else {
                        j.m250if(f173if, "locCache create loc cache file failure ...");
                        return;
                    }
                }
                RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
                if (randomAccessFile.length() < 1) {
                    randomAccessFile.writeInt(0);
                }
                int i = 0;
                for (int size = f174int.size() - 1; size >= 0; size--) {
                    a aVar = (a) f174int.get(size);
                    if (aVar != null) {
                        randomAccessFile.seek(f176try + ((long) (f175new * (size % f582a))));
                        randomAccessFile.writeFloat(aVar.f178for);
                        randomAccessFile.writeInt(aVar.f179if);
                        randomAccessFile.writeDouble(aVar.f181new);
                        randomAccessFile.writeInt(aVar.f583a);
                        randomAccessFile.writeDouble(aVar.f180int);
                        randomAccessFile.writeInt(aVar.f177do);
                        randomAccessFile.writeInt(aVar.f182try);
                        j.m250if(f173if, "add a new cell loc into file ...");
                    }
                    i++;
                }
                randomAccessFile.seek(0);
                randomAccessFile.writeInt(i);
                randomAccessFile.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: if  reason: not valid java name */
    private static a m227if(int i, int i2, int i3) {
        try {
            if (f174int == null || f174int.size() <= 0) {
                a();
            }
            if (f174int == null || f174int.size() <= 0) {
                return null;
            }
            for (int size = f174int.size() - 1; size >= 0; size--) {
                a aVar = (a) f174int.get(size);
                if (aVar != null && aVar.a(i, i2, i3)) {
                    return aVar;
                }
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* renamed from: if  reason: not valid java name */
    public static void m228if() {
        m226do();
    }
}
