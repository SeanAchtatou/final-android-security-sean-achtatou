package com.baidu.location;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import java.util.ArrayList;
import java.util.Iterator;

public class i {

    /* renamed from: new  reason: not valid java name */
    public static final String f183new = "android.com.baidu.location.TIMER.NOTIFY";

    /* renamed from: a  reason: collision with root package name */
    private int f584a = 0;
    private long b = 0;
    /* access modifiers changed from: private */

    /* renamed from: byte  reason: not valid java name */
    public ArrayList f184byte = null;
    private boolean c = false;

    /* renamed from: case  reason: not valid java name */
    private BDLocation f185case = null;

    /* renamed from: char  reason: not valid java name */
    private long f186char = 0;
    /* access modifiers changed from: private */
    public LocationClient d = null;
    /* access modifiers changed from: private */

    /* renamed from: do  reason: not valid java name */
    public String f187do = f.v;

    /* renamed from: else  reason: not valid java name */
    private b f188else = null;

    /* renamed from: for  reason: not valid java name */
    private AlarmManager f189for = null;

    /* renamed from: goto  reason: not valid java name */
    private float f190goto = Float.MAX_VALUE;

    /* renamed from: if  reason: not valid java name */
    private Context f191if = null;

    /* renamed from: int  reason: not valid java name */
    private a f192int = new a();

    /* renamed from: long  reason: not valid java name */
    private boolean f193long = false;

    /* renamed from: try  reason: not valid java name */
    private PendingIntent f194try = null;

    /* renamed from: void  reason: not valid java name */
    private boolean f195void = false;

    public class a implements BDLocationListener {
        public a() {
        }

        public void onReceiveLocation(BDLocation bDLocation) {
            i.this.a(bDLocation);
        }

        public void onReceivePoi(BDLocation bDLocation) {
        }
    }

    public class b extends BroadcastReceiver {
        public b() {
        }

        public void onReceive(Context context, Intent intent) {
            j.m250if(i.this.f187do, "timer expire,request location...");
            if (i.this.f184byte != null && !i.this.f184byte.isEmpty()) {
                i.this.d.requestNotifyLocation();
            }
        }
    }

    public i(Context context, LocationClient locationClient) {
        this.f191if = context;
        this.d = locationClient;
        this.d.registerNotifyLocationListener(this.f192int);
        this.f189for = (AlarmManager) this.f191if.getSystemService("alarm");
        this.f188else = new b();
        this.c = false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a() {
        /*
            r8 = this;
            r1 = 10000(0x2710, float:1.4013E-41)
            r2 = 0
            r3 = 1
            boolean r0 = r8.m237do()
            if (r0 != 0) goto L_0x000b
        L_0x000a:
            return
        L_0x000b:
            float r0 = r8.f190goto
            r4 = 1167867904(0x459c4000, float:5000.0)
            int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x0043
            r0 = 600000(0x927c0, float:8.40779E-40)
        L_0x0017:
            boolean r4 = r8.f193long
            if (r4 == 0) goto L_0x005f
            r8.f193long = r2
        L_0x001d:
            int r0 = r8.f584a
            if (r0 == 0) goto L_0x005d
            long r4 = r8.f186char
            int r0 = r8.f584a
            long r6 = (long) r0
            long r4 = r4 + r6
            long r6 = java.lang.System.currentTimeMillis()
            long r4 = r4 - r6
            long r6 = (long) r1
            int r0 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x005d
            r0 = r2
        L_0x0032:
            if (r0 == 0) goto L_0x000a
            r8.f584a = r1
            long r0 = java.lang.System.currentTimeMillis()
            r8.f186char = r0
            int r0 = r8.f584a
            long r0 = (long) r0
            r8.a(r0)
            goto L_0x000a
        L_0x0043:
            float r0 = r8.f190goto
            r4 = 1148846080(0x447a0000, float:1000.0)
            int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x004f
            r0 = 120000(0x1d4c0, float:1.68156E-40)
            goto L_0x0017
        L_0x004f:
            float r0 = r8.f190goto
            r4 = 1140457472(0x43fa0000, float:500.0)
            int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x005b
            r0 = 60000(0xea60, float:8.4078E-41)
            goto L_0x0017
        L_0x005b:
            r0 = r1
            goto L_0x0017
        L_0x005d:
            r0 = r3
            goto L_0x0032
        L_0x005f:
            r1 = r0
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.i.a():void");
    }

    private void a(long j) {
        if (this.f195void) {
            this.f189for.cancel(this.f194try);
        }
        this.f194try = PendingIntent.getBroadcast(this.f191if, 0, new Intent(f183new), 134217728);
        this.f189for.set(0, System.currentTimeMillis() + j, this.f194try);
        j.m250if(this.f187do, "timer start:" + j);
    }

    /* access modifiers changed from: private */
    public void a(BDLocation bDLocation) {
        j.m250if(this.f187do, "notify new loation");
        this.f195void = false;
        if (bDLocation.getLocType() != 61 && bDLocation.getLocType() != 161 && bDLocation.getLocType() != 65) {
            a(120000);
        } else if (System.currentTimeMillis() - this.b >= 5000 && this.f184byte != null) {
            this.f185case = bDLocation;
            this.b = System.currentTimeMillis();
            float[] fArr = new float[1];
            Iterator it = this.f184byte.iterator();
            float f = Float.MAX_VALUE;
            while (it.hasNext()) {
                BDNotifyListener bDNotifyListener = (BDNotifyListener) it.next();
                Location.distanceBetween(bDLocation.getLatitude(), bDLocation.getLongitude(), bDNotifyListener.mLatitudeC, bDNotifyListener.mLongitudeC, fArr);
                float radius = (fArr[0] - bDNotifyListener.mRadius) - bDLocation.getRadius();
                j.m250if(this.f187do, "distance:" + radius);
                if (radius > 0.0f) {
                    if (radius < f) {
                        f = radius;
                    }
                } else if (bDNotifyListener.Notified < 3) {
                    bDNotifyListener.Notified++;
                    bDNotifyListener.onNotify(bDLocation, fArr[0]);
                    if (bDNotifyListener.Notified < 3) {
                        this.f193long = true;
                    }
                }
            }
            if (f < this.f190goto) {
                this.f190goto = f;
            }
            this.f584a = 0;
            a();
        }
    }

    /* renamed from: do  reason: not valid java name */
    private boolean m237do() {
        boolean z = false;
        if (this.f184byte == null || this.f184byte.isEmpty()) {
            return false;
        }
        Iterator it = this.f184byte.iterator();
        while (true) {
            boolean z2 = z;
            if (!it.hasNext()) {
                return z2;
            }
            z = ((BDNotifyListener) it.next()).Notified < 3 ? true : z2;
        }
    }

    public void a(BDNotifyListener bDNotifyListener) {
        j.m250if(this.f187do, bDNotifyListener.mCoorType + "2gcj");
        if (bDNotifyListener.mCoorType != null) {
            if (!bDNotifyListener.mCoorType.equals("gcj02")) {
                double[] dArr = Jni.m1if(bDNotifyListener.mLongitude, bDNotifyListener.mLatitude, bDNotifyListener.mCoorType + "2gcj");
                bDNotifyListener.mLongitudeC = dArr[0];
                bDNotifyListener.mLatitudeC = dArr[1];
                j.m250if(this.f187do, bDNotifyListener.mCoorType + "2gcj");
                j.m250if(this.f187do, "coor:" + bDNotifyListener.mLongitude + "," + bDNotifyListener.mLatitude + ":" + bDNotifyListener.mLongitudeC + "," + bDNotifyListener.mLatitudeC);
            }
            if (this.f185case == null || System.currentTimeMillis() - this.b > 300000) {
                this.d.requestNotifyLocation();
            } else {
                float[] fArr = new float[1];
                Location.distanceBetween(this.f185case.getLatitude(), this.f185case.getLongitude(), bDNotifyListener.mLatitudeC, bDNotifyListener.mLongitudeC, fArr);
                float radius = (fArr[0] - bDNotifyListener.mRadius) - this.f185case.getRadius();
                if (radius > 0.0f) {
                    if (radius < this.f190goto) {
                        this.f190goto = radius;
                    }
                } else if (bDNotifyListener.Notified < 3) {
                    bDNotifyListener.Notified++;
                    bDNotifyListener.onNotify(this.f185case, fArr[0]);
                    if (bDNotifyListener.Notified < 3) {
                        this.f193long = true;
                    }
                }
            }
            a();
        }
    }

    /* renamed from: do  reason: not valid java name */
    public int m239do(BDNotifyListener bDNotifyListener) {
        if (this.f184byte == null) {
            return 0;
        }
        if (this.f184byte.contains(bDNotifyListener)) {
            this.f184byte.remove(bDNotifyListener);
        }
        if (this.f184byte.size() == 0 && this.f195void) {
            this.f189for.cancel(this.f194try);
        }
        return 1;
    }

    /* renamed from: if  reason: not valid java name */
    public int m240if(BDNotifyListener bDNotifyListener) {
        if (this.f184byte == null) {
            this.f184byte = new ArrayList();
        }
        this.f184byte.add(bDNotifyListener);
        bDNotifyListener.isAdded = true;
        bDNotifyListener.mNotifyCache = this;
        if (!this.c) {
            this.f191if.registerReceiver(this.f188else, new IntentFilter(f183new));
            this.c = true;
        }
        if (bDNotifyListener.mCoorType != null) {
            if (!bDNotifyListener.mCoorType.equals("gcj02")) {
                double[] dArr = Jni.m1if(bDNotifyListener.mLongitude, bDNotifyListener.mLatitude, bDNotifyListener.mCoorType + "2gcj");
                bDNotifyListener.mLongitudeC = dArr[0];
                bDNotifyListener.mLatitudeC = dArr[1];
                j.m250if(this.f187do, bDNotifyListener.mCoorType + "2gcj");
                j.m250if(this.f187do, "coor:" + bDNotifyListener.mLongitude + "," + bDNotifyListener.mLatitude + ":" + bDNotifyListener.mLongitudeC + "," + bDNotifyListener.mLatitudeC);
            }
            if (this.f185case == null || System.currentTimeMillis() - this.b > 30000) {
                this.d.requestNotifyLocation();
            } else {
                float[] fArr = new float[1];
                Location.distanceBetween(this.f185case.getLatitude(), this.f185case.getLongitude(), bDNotifyListener.mLatitudeC, bDNotifyListener.mLongitudeC, fArr);
                float radius = (fArr[0] - bDNotifyListener.mRadius) - this.f185case.getRadius();
                if (radius > 0.0f) {
                    if (radius < this.f190goto) {
                        this.f190goto = radius;
                    }
                } else if (bDNotifyListener.Notified < 3) {
                    bDNotifyListener.Notified++;
                    bDNotifyListener.onNotify(this.f185case, fArr[0]);
                    if (bDNotifyListener.Notified < 3) {
                        this.f193long = true;
                    }
                }
            }
            a();
        }
        return 1;
    }

    /* renamed from: if  reason: not valid java name */
    public void m241if() {
        if (this.f195void) {
            this.f189for.cancel(this.f194try);
        }
        this.f185case = null;
        this.b = 0;
        if (this.c) {
            j.m250if(this.f187do, "unregister...");
            this.f191if.unregisterReceiver(this.f188else);
        }
        this.c = false;
    }
}
