package com.baidu.location;

import android.app.AlarmManager;
import android.app.KeyguardManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.net.wifi.ScanResult;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Process;
import android.util.Log;
import com.amap.mapapi.location.LocationManagerProxy;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.baidu.location.c;
import com.baidu.location.e;
import com.jcraft.jzlib.GZIPHeader;
import java.io.File;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.io.StringWriter;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.Calendar;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

public final class f extends Service {
    static final int B = 92;
    static final int D = 57;
    static final int G = 52;
    static final int I = 26;
    static final int K = 64;
    static final int L = 27;
    static final int M = 62;
    static final int S = 1000;
    static final int U = 54;
    static final int V = 81;
    static final int W = 25;

    /* renamed from: a  reason: collision with root package name */
    private static String f575a = (ac + "/glb.dat");
    static final int aa = 21;
    static String ac = (Environment.getExternalStorageDirectory().getPath() + "/baidu/tempdata");
    private static final int af = 200;
    static final int ag = 43;
    static final int ah = 14;
    static final int ai = 3000;
    static final int ak = 56;
    static final int ao = 101;
    static final float ap = 3.3f;
    static final int aq = 61;
    static final int ar = 53;
    private static final int at = 800;
    static final int b = 63;

    /* renamed from: byte  reason: not valid java name */
    private static final int f131byte = 24;
    static final int c = 12;

    /* renamed from: case  reason: not valid java name */
    static final int f132case = 42;

    /* renamed from: do  reason: not valid java name */
    static final int f133do = 28;
    static final int e = 65;

    /* renamed from: else  reason: not valid java name */
    static final int f134else = 2000;

    /* renamed from: for  reason: not valid java name */
    static final int f135for = 22;
    static final int g = 15;
    static final int i = 55;

    /* renamed from: int  reason: not valid java name */
    static final int f136int = 31;
    /* access modifiers changed from: private */
    public static File j = null;
    /* access modifiers changed from: private */
    public static File k = null;
    static final int l = 11;

    /* renamed from: long  reason: not valid java name */
    static final int f137long = 13;
    static final int p = 41;
    static final int s = 23;
    static final int t = 91;
    public static final String v = "baidu_location_service";

    /* renamed from: void  reason: not valid java name */
    static final int f138void = 71;
    static final int w = 24;
    static final int x = 3000;
    static final int z = 51;
    private String A = null;
    private e.c C = null;
    private long E = 0;
    /* access modifiers changed from: private */
    public e F = null;
    private String H = null;
    private boolean J = true;
    private boolean N = true;
    private boolean O = false;
    private long P = 0;
    private boolean Q = false;
    final Handler R = new d();
    private SQLiteDatabase T = null;
    private String X = null;
    private boolean Y = true;
    private int Z = 0;
    private b ab = null;
    /* access modifiers changed from: private */
    public boolean ad = false;
    private e.c ae = null;
    /* access modifiers changed from: private */
    public boolean aj = false;
    private c.a al = null;
    private boolean am = false;
    final Messenger an = new Messenger(this.R);
    private String as = null;
    /* access modifiers changed from: private */
    public a au = null;

    /* renamed from: char  reason: not valid java name */
    private e.c f139char = null;
    private long d = 0;
    private Location f = null;

    /* renamed from: goto  reason: not valid java name */
    private boolean f140goto = false;
    private String h = null;

    /* renamed from: if  reason: not valid java name */
    private String f141if = "bdcltb09";
    /* access modifiers changed from: private */
    public String m = (ac + "/vm.dat");
    private double n = 0.0d;

    /* renamed from: new  reason: not valid java name */
    private String f142new = null;
    private double o = 0.0d;
    private double q = 0.0d;
    /* access modifiers changed from: private */
    public c r = null;

    /* renamed from: try  reason: not valid java name */
    private c.a f143try = null;
    private c.a u = null;
    /* access modifiers changed from: private */
    public c y = null;

    public class a implements Thread.UncaughtExceptionHandler {

        /* renamed from: if  reason: not valid java name */
        private Context f144if;

        a(Context context) {
            this.f144if = context;
            a();
        }

        private String a(Throwable th) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            th.printStackTrace(printWriter);
            printWriter.close();
            return stringWriter.toString();
        }

        public void a() {
            String str;
            String str2 = null;
            try {
                File file = new File((Environment.getExternalStorageDirectory().getPath() + "/traces") + "/error_fs.dat");
                if (file.exists()) {
                    RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
                    randomAccessFile.seek(280);
                    if (1326 == randomAccessFile.readInt()) {
                        randomAccessFile.seek(308);
                        int readInt = randomAccessFile.readInt();
                        if (readInt <= 0 || readInt >= 2048) {
                            str = null;
                        } else {
                            j.m250if(f.v, "A" + readInt);
                            byte[] bArr = new byte[readInt];
                            randomAccessFile.read(bArr, 0, readInt);
                            str = new String(bArr, 0, readInt);
                        }
                        randomAccessFile.seek(600);
                        int readInt2 = randomAccessFile.readInt();
                        if (readInt2 > 0 && readInt2 < 2048) {
                            j.m250if(f.v, "A" + readInt2);
                            byte[] bArr2 = new byte[readInt2];
                            randomAccessFile.read(bArr2, 0, readInt2);
                            str2 = new String(bArr2, 0, readInt2);
                        }
                        j.m250if(f.v, str + str2);
                        if (a(str, str2)) {
                            randomAccessFile.seek(280);
                            randomAccessFile.writeInt(12346);
                        }
                    }
                    randomAccessFile.close();
                }
            } catch (Exception e) {
            }
        }

        public void a(File file, String str, String str2) {
            try {
                RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
                randomAccessFile.seek(280);
                randomAccessFile.writeInt(12346);
                randomAccessFile.seek(300);
                randomAccessFile.writeLong(System.currentTimeMillis());
                byte[] bytes = str.getBytes();
                randomAccessFile.writeInt(bytes.length);
                randomAccessFile.write(bytes, 0, bytes.length);
                randomAccessFile.seek(600);
                byte[] bytes2 = str2.getBytes();
                randomAccessFile.writeInt(bytes2.length);
                randomAccessFile.write(bytes2, 0, bytes2.length);
                if (!a(str, str2)) {
                    randomAccessFile.seek(280);
                    randomAccessFile.writeInt(1326);
                }
                randomAccessFile.close();
            } catch (Exception e) {
            }
        }

        /* access modifiers changed from: package-private */
        public boolean a(String str, String str2) {
            if (!g.a(this.f144if)) {
                return false;
            }
            try {
                HttpPost httpPost = new HttpPost(j.N);
                ArrayList arrayList = new ArrayList();
                arrayList.add(new BasicNameValuePair("e0", str));
                arrayList.add(new BasicNameValuePair("e1", str2));
                httpPost.setEntity(new UrlEncodedFormEntity(arrayList, "utf-8"));
                DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
                defaultHttpClient.getParams().setParameter("http.connection.timeout", 12000);
                defaultHttpClient.getParams().setParameter("http.socket.timeout", 12000);
                j.m250if(f.v, "send begin ...");
                if (defaultHttpClient.execute(httpPost).getStatusLine().getStatusCode() != 200) {
                    return false;
                }
                j.m250if(f.v, "send ok....");
                return true;
            } catch (Exception e) {
                return false;
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:16:0x0083 A[Catch:{ Exception -> 0x00c9 }] */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x00a8 A[SYNTHETIC, Splitter:B:26:0x00a8] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void uncaughtException(java.lang.Thread r9, java.lang.Throwable r10) {
            /*
                r8 = this;
                r1 = 0
                boolean r0 = com.baidu.location.j.x
                if (r0 != 0) goto L_0x000d
                int r0 = android.os.Process.myPid()
                android.os.Process.killProcess(r0)
            L_0x000c:
                return
            L_0x000d:
                java.lang.String r2 = r8.a(r10)     // Catch:{ Exception -> 0x00a3 }
                java.lang.String r0 = "baidu_location_service"
                com.baidu.location.j.m250if(r0, r2)     // Catch:{ Exception -> 0x00cb }
                com.baidu.location.f r0 = com.baidu.location.f.this     // Catch:{ Exception -> 0x00cb }
                com.baidu.location.c unused = r0.r     // Catch:{ Exception -> 0x00cb }
                r0 = 0
                java.lang.String r0 = com.baidu.location.c.a(r0)     // Catch:{ Exception -> 0x00cb }
                com.baidu.location.f r3 = com.baidu.location.f.this     // Catch:{ Exception -> 0x00cb }
                com.baidu.location.a r3 = r3.au     // Catch:{ Exception -> 0x00cb }
                if (r3 == 0) goto L_0x0043
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00cb }
                r3.<init>()     // Catch:{ Exception -> 0x00cb }
                java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ Exception -> 0x00cb }
                com.baidu.location.f r3 = com.baidu.location.f.this     // Catch:{ Exception -> 0x00cb }
                com.baidu.location.a r3 = r3.au     // Catch:{ Exception -> 0x00cb }
                java.lang.String r3 = r3.m39byte()     // Catch:{ Exception -> 0x00cb }
                java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x00cb }
                java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00cb }
            L_0x0043:
                if (r0 == 0) goto L_0x00d0
                java.lang.String r0 = com.baidu.location.Jni.m0if(r0)     // Catch:{ Exception -> 0x00cb }
            L_0x0049:
                r3 = r0
            L_0x004a:
                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00c9 }
                r0.<init>()     // Catch:{ Exception -> 0x00c9 }
                java.io.File r4 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Exception -> 0x00c9 }
                java.lang.String r4 = r4.getPath()     // Catch:{ Exception -> 0x00c9 }
                java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ Exception -> 0x00c9 }
                java.lang.String r4 = "/traces"
                java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ Exception -> 0x00c9 }
                java.lang.String r4 = r0.toString()     // Catch:{ Exception -> 0x00c9 }
                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00c9 }
                r0.<init>()     // Catch:{ Exception -> 0x00c9 }
                java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ Exception -> 0x00c9 }
                java.lang.String r5 = "/error_fs.dat"
                java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ Exception -> 0x00c9 }
                java.lang.String r5 = r0.toString()     // Catch:{ Exception -> 0x00c9 }
                java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x00c9 }
                r0.<init>(r5)     // Catch:{ Exception -> 0x00c9 }
                boolean r5 = r0.exists()     // Catch:{ Exception -> 0x00c9 }
                if (r5 != 0) goto L_0x00a8
                java.io.File r5 = new java.io.File     // Catch:{ Exception -> 0x00c9 }
                r5.<init>(r4)     // Catch:{ Exception -> 0x00c9 }
                boolean r4 = r5.exists()     // Catch:{ Exception -> 0x00c9 }
                if (r4 != 0) goto L_0x0091
                r5.mkdirs()     // Catch:{ Exception -> 0x00c9 }
            L_0x0091:
                boolean r4 = r0.createNewFile()     // Catch:{ Exception -> 0x00c9 }
                if (r4 != 0) goto L_0x00ce
            L_0x0097:
                r8.a(r1, r3, r2)     // Catch:{ Exception -> 0x00c9 }
            L_0x009a:
                int r0 = android.os.Process.myPid()
                android.os.Process.killProcess(r0)
                goto L_0x000c
            L_0x00a3:
                r0 = move-exception
                r0 = r1
            L_0x00a5:
                r2 = r0
                r3 = r1
                goto L_0x004a
            L_0x00a8:
                java.io.RandomAccessFile r1 = new java.io.RandomAccessFile     // Catch:{ Exception -> 0x00c9 }
                java.lang.String r4 = "rw"
                r1.<init>(r0, r4)     // Catch:{ Exception -> 0x00c9 }
                r4 = 300(0x12c, double:1.48E-321)
                r1.seek(r4)     // Catch:{ Exception -> 0x00c9 }
                long r4 = r1.readLong()     // Catch:{ Exception -> 0x00c9 }
                long r6 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x00c9 }
                long r4 = r6 - r4
                r6 = 604800000(0x240c8400, double:2.988109026E-315)
                int r1 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                if (r1 <= 0) goto L_0x009a
                r8.a(r0, r3, r2)     // Catch:{ Exception -> 0x00c9 }
                goto L_0x009a
            L_0x00c9:
                r0 = move-exception
                goto L_0x009a
            L_0x00cb:
                r0 = move-exception
                r0 = r2
                goto L_0x00a5
            L_0x00ce:
                r1 = r0
                goto L_0x0097
            L_0x00d0:
                r0 = r1
                goto L_0x0049
            */
            throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.f.a.uncaughtException(java.lang.Thread, java.lang.Throwable):void");
        }
    }

    class b implements Runnable {
        private b() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.f.a(com.baidu.location.f, boolean):boolean
         arg types: [com.baidu.location.f, int]
         candidates:
          com.baidu.location.f.a(android.os.Message, int):void
          com.baidu.location.f.a(com.baidu.location.f, int):void
          com.baidu.location.f.a(com.baidu.location.f, android.os.Message):void
          com.baidu.location.f.a(com.baidu.location.f, boolean):boolean */
        public void run() {
            if (f.this.aj) {
                boolean unused = f.this.aj = false;
                f.this.m153byte();
            }
        }
    }

    public class c {

        /* renamed from: for  reason: not valid java name */
        public static final String f145for = "com.baidu.locTest.LocationServer";

        /* renamed from: a  reason: collision with root package name */
        private long[] f578a = new long[20];
        private c.a b = null;

        /* renamed from: byte  reason: not valid java name */
        private int f146byte = 1;
        /* access modifiers changed from: private */
        public String c = null;

        /* renamed from: case  reason: not valid java name */
        private a f147case = null;

        /* renamed from: char  reason: not valid java name */
        private final int f148char = 200;
        private PendingIntent d = null;

        /* renamed from: do  reason: not valid java name */
        private boolean f149do = false;

        /* renamed from: else  reason: not valid java name */
        private boolean f150else = false;

        /* renamed from: goto  reason: not valid java name */
        private Context f151goto = null;

        /* renamed from: if  reason: not valid java name */
        private boolean f152if = false;

        /* renamed from: int  reason: not valid java name */
        private int f153int = 0;

        /* renamed from: long  reason: not valid java name */
        private String f154long = null;

        /* renamed from: new  reason: not valid java name */
        private final long f155new = 86100000;

        /* renamed from: try  reason: not valid java name */
        private AlarmManager f156try = null;

        /* renamed from: void  reason: not valid java name */
        private long f157void = 0;

        public class a extends BroadcastReceiver {
            public a() {
            }

            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (action.equals(c.f145for)) {
                    f.this.R.obtainMessage(101).sendToTarget();
                    return;
                }
                try {
                    if (action.equals("android.intent.action.BATTERY_CHANGED")) {
                        int intExtra = intent.getIntExtra(LocationManagerProxy.KEY_STATUS_CHANGED, 0);
                        int intExtra2 = intent.getIntExtra("plugged", 0);
                        switch (intExtra) {
                            case 2:
                                String unused = c.this.c = "4";
                                break;
                            case 3:
                            case 4:
                                String unused2 = c.this.c = "3";
                                break;
                            default:
                                String unused3 = c.this.c = null;
                                break;
                        }
                        switch (intExtra2) {
                            case 1:
                                String unused4 = c.this.c = "6";
                                return;
                            case 2:
                                String unused5 = c.this.c = "5";
                                return;
                            default:
                                return;
                        }
                    }
                } catch (Exception e) {
                    String unused6 = c.this.c = null;
                }
            }
        }

        public c(Context context) {
            this.f151goto = context;
            this.f157void = System.currentTimeMillis();
            this.f156try = (AlarmManager) context.getSystemService("alarm");
            this.f147case = new a();
            context.registerReceiver(this.f147case, new IntentFilter(f145for));
            this.d = PendingIntent.getBroadcast(context, 0, new Intent(f145for), 134217728);
            this.f156try.setRepeating(2, j.M, j.M, this.d);
            f.this.registerReceiver(this.f147case, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        }

        public void a() {
            m197if();
            if (f.j != null) {
                try {
                    RandomAccessFile randomAccessFile = new RandomAccessFile(f.j, "rw");
                    if (randomAccessFile.length() < 1) {
                        randomAccessFile.close();
                        return;
                    }
                    randomAccessFile.seek(0);
                    int readInt = randomAccessFile.readInt();
                    randomAccessFile.seek(0);
                    randomAccessFile.writeInt(readInt + 1);
                    randomAccessFile.seek((long) ((readInt * 200) + 4));
                    randomAccessFile.writeLong(System.currentTimeMillis());
                    randomAccessFile.writeInt(this.f146byte);
                    randomAccessFile.writeInt(0);
                    randomAccessFile.writeInt(this.f153int);
                    randomAccessFile.writeInt(this.b.f107do);
                    randomAccessFile.writeInt(this.b.f109if);
                    randomAccessFile.writeInt(this.b.f108for);
                    randomAccessFile.writeInt(this.b.f112try);
                    byte[] bArr = new byte[160];
                    for (int i = 0; i < this.f153int; i++) {
                        bArr[(i * 8) + 7] = (byte) ((int) this.f578a[i]);
                        bArr[(i * 8) + 6] = (byte) ((int) (this.f578a[i] >> 8));
                        bArr[(i * 8) + 5] = (byte) ((int) (this.f578a[i] >> 16));
                        bArr[(i * 8) + 4] = (byte) ((int) (this.f578a[i] >> 24));
                        bArr[(i * 8) + 3] = (byte) ((int) (this.f578a[i] >> 32));
                        bArr[(i * 8) + 2] = (byte) ((int) (this.f578a[i] >> 40));
                        bArr[(i * 8) + 1] = (byte) ((int) (this.f578a[i] >> 48));
                        bArr[(i * 8) + 0] = (byte) ((int) (this.f578a[i] >> 56));
                    }
                    if (this.f153int > 0) {
                        randomAccessFile.write(bArr, 0, this.f153int * 8);
                    }
                    randomAccessFile.writeInt(this.f153int);
                    randomAccessFile.close();
                } catch (Exception e2) {
                }
            }
        }

        /* renamed from: byte  reason: not valid java name */
        public void m193byte() {
            int i = 0;
            if (this.f149do) {
                this.f146byte = 1;
                long j = j.ac * 1000 * 60;
                j.M = j;
                j.al = j >> 2;
                Calendar instance = Calendar.getInstance();
                int i2 = instance.get(5);
                int i3 = instance.get(1);
                if (i3 > f.f134else) {
                    i = i3 - 2000;
                }
                String str = i + "," + (instance.get(2) + 1) + "," + i2 + "," + instance.get(11) + "," + instance.get(12) + "," + j.ac;
                if (this.f152if) {
                    this.f154long = "&tr=" + j.f199do + "," + str;
                } else {
                    this.f154long += "|T" + str;
                }
                j.m250if(f.v, "trace begin:" + this.f154long);
                try {
                    RandomAccessFile randomAccessFile = new RandomAccessFile(f.k, "rw");
                    randomAccessFile.seek(12);
                    randomAccessFile.writeLong(System.currentTimeMillis());
                    randomAccessFile.writeInt(this.f146byte);
                    randomAccessFile.close();
                    RandomAccessFile randomAccessFile2 = new RandomAccessFile(f.j, "rw");
                    randomAccessFile2.seek(0);
                    randomAccessFile2.writeInt(0);
                    randomAccessFile2.close();
                } catch (Exception e2) {
                }
            }
        }

        /* renamed from: case  reason: not valid java name */
        public void m194case() {
            int i;
            f.m181long();
            if (f.k != null) {
                try {
                    RandomAccessFile randomAccessFile = new RandomAccessFile(f.k, "rw");
                    if (randomAccessFile.length() < 1) {
                        randomAccessFile.close();
                        return;
                    }
                    randomAccessFile.seek(0);
                    int readInt = randomAccessFile.readInt();
                    int readInt2 = randomAccessFile.readInt();
                    int readInt3 = randomAccessFile.readInt();
                    if (!this.f149do || !this.f152if) {
                        i = (readInt2 * f.at) + 24;
                    } else {
                        j.m250if(f.v, "trace new info:" + readInt + ":" + readInt2 + ":" + readInt3);
                        int i2 = (readInt2 + 1) % 200;
                        randomAccessFile.seek(4);
                        randomAccessFile.writeInt(i2);
                        readInt++;
                        if (readInt >= 200) {
                            readInt = 199;
                        }
                        if (i2 == readInt3 && readInt > 0) {
                            readInt3 = (readInt3 + 1) % 200;
                            randomAccessFile.writeInt(readInt3);
                        }
                        j.m250if(f.v, "trace new info:" + readInt + ":" + readInt2 + ":" + readInt3);
                        i = (i2 * f.at) + 24;
                    }
                    randomAccessFile.seek((long) (i + 4));
                    byte[] bytes = this.f154long.getBytes();
                    for (int i3 = 0; i3 < bytes.length; i3++) {
                        bytes[i3] = (byte) (bytes[i3] ^ 90);
                    }
                    randomAccessFile.write(bytes, 0, bytes.length);
                    randomAccessFile.writeInt(bytes.length);
                    randomAccessFile.seek((long) i);
                    randomAccessFile.writeInt(bytes.length);
                    if (this.f149do && this.f152if) {
                        randomAccessFile.seek(0);
                        randomAccessFile.writeInt(readInt);
                    }
                    randomAccessFile.close();
                } catch (Exception e2) {
                }
            }
        }

        /* renamed from: do  reason: not valid java name */
        public void m195do() {
            e.c cVar;
            int i;
            int i2 = 0;
            try {
                j.m250if(f.v, "regular expire...");
                m199new();
                if (this.f150else) {
                    this.f150else = false;
                    return;
                }
                m193byte();
                this.f153int = 0;
                this.b = null;
                if (f.this.F != null) {
                    f.this.F.m137new();
                }
                if (!(f.this.F == null || (cVar = f.this.F.m131byte()) == null || cVar.f128for == null)) {
                    int size = cVar.f128for.size();
                    if (size > 20) {
                        size = 20;
                    }
                    int i3 = 0;
                    while (i3 < size) {
                        try {
                            i = i2 + 1;
                            try {
                                this.f578a[i2] = Long.parseLong(((ScanResult) cVar.f128for.get(i3)).BSSID.replace(":", PoiTypeDef.All), 16);
                            } catch (Exception e2) {
                            }
                        } catch (Exception e3) {
                            i = i2;
                        }
                        i3++;
                        i2 = i;
                    }
                    this.f153int = i2;
                }
                if (f.this.r != null) {
                    this.b = f.this.r.a();
                }
                if (this.b != null) {
                    m196for();
                }
            } catch (Exception e4) {
            }
        }

        /* renamed from: for  reason: not valid java name */
        public void m196for() {
            String str;
            boolean z;
            String str2;
            String str3;
            int readInt;
            int readInt2;
            m197if();
            j.m250if(f.v, "trace1:" + this.f154long);
            try {
                str = f.this.m192else() ? "y2" : "y1";
            } catch (Exception e2) {
                str = "y";
            }
            if (!this.f149do) {
                try {
                    RandomAccessFile randomAccessFile = new RandomAccessFile(f.j, "rw");
                    if (randomAccessFile.length() < 1) {
                        randomAccessFile.close();
                        return;
                    }
                    int readInt3 = randomAccessFile.readInt();
                    int i = 0;
                    while (true) {
                        if (i >= readInt3) {
                            z = false;
                            break;
                        }
                        randomAccessFile.seek((long) ((i * 200) + 4));
                        randomAccessFile.readLong();
                        readInt = randomAccessFile.readInt();
                        readInt2 = randomAccessFile.readInt();
                        int readInt4 = randomAccessFile.readInt();
                        byte[] bArr = new byte[200];
                        randomAccessFile.read(bArr, 0, (readInt4 * 8) + 16);
                        byte b2 = (bArr[3] & GZIPHeader.OS_UNKNOWN) | ((bArr[2] << 8) & 65280) | ((bArr[1] << 16) & 16711680) | ((bArr[0] << 24) & -16777216);
                        byte b3 = (bArr[7] & GZIPHeader.OS_UNKNOWN) | ((bArr[6] << 8) & 65280) | ((bArr[5] << 16) & 16711680) | ((bArr[4] << 24) & -16777216);
                        byte b4 = (bArr[11] & GZIPHeader.OS_UNKNOWN) | ((bArr[10] << 8) & 65280) | ((bArr[9] << 16) & 16711680) | ((bArr[8] << 24) & -16777216);
                        byte b5 = (bArr[15] & GZIPHeader.OS_UNKNOWN) | ((bArr[14] << 8) & 65280) | ((bArr[13] << 16) & 16711680) | ((bArr[12] << 24) & -16777216);
                        if (this.b.f107do == b2 && this.b.f109if == b3 && this.b.f108for == b4 && this.b.f112try == b5) {
                            long[] jArr = new long[readInt4];
                            for (int i2 = 0; i2 < readInt4; i2++) {
                                jArr[i2] = ((((long) bArr[(i2 * 8) + 16]) & 255) << 56) | ((((long) bArr[((i2 * 8) + 16) + 1]) & 255) << 48) | ((((long) bArr[((i2 * 8) + 16) + 2]) & 255) << 40) | ((((long) bArr[((i2 * 8) + 16) + 3]) & 255) << 32) | ((((long) bArr[((i2 * 8) + 16) + 4]) & 255) << 24) | ((((long) bArr[((i2 * 8) + 16) + 5]) & 255) << 16) | ((((long) bArr[((i2 * 8) + 16) + 6]) & 255) << 8) | (((long) bArr[(i2 * 8) + 16 + 7]) & 255);
                            }
                            int i3 = 0;
                            int i4 = 0;
                            while (i4 < this.f153int) {
                                int i5 = i3;
                                for (int i6 = 0; i6 < readInt4; i6++) {
                                    if (this.f578a[i4] == jArr[i6]) {
                                        i5++;
                                    }
                                }
                                i4++;
                                i3 = i5;
                            }
                            if (i3 > 5 || i3 * 8 > this.f153int + readInt4 || ((readInt4 == 0 && this.f153int == 0) || ((readInt4 == 1 && this.f153int == 1 && this.f578a[0] == jArr[0]) || (readInt4 > 1 && this.f153int > 1 && this.f578a[0] == jArr[0] && this.f578a[1] == jArr[1])))) {
                                z = true;
                                randomAccessFile.seek((long) ((i * 200) + 16));
                                randomAccessFile.writeInt(readInt2 + 1);
                            }
                        }
                        i++;
                    }
                    z = true;
                    randomAccessFile.seek((long) ((i * 200) + 16));
                    randomAccessFile.writeInt(readInt2 + 1);
                    if (this.f154long != null) {
                        this.f154long = this.f154long + "|" + readInt + str;
                        if (this.c != null) {
                            this.f154long = this.f154long + this.c;
                        }
                    }
                    j.m250if(f.v, "daily info:is same");
                } catch (Exception e3) {
                    return;
                }
            } else {
                z = false;
            }
            if (!z) {
                String str4 = (this.b.f107do == 460 ? "|x," : "|x460,") + this.b.f109if + "," + this.b.f108for + "," + this.b.f112try;
                long j = 0;
                if (!(f.this.F == null || (str3 = f.this.F.m133char()) == null)) {
                    try {
                        j = Long.parseLong(str3, 16);
                    } catch (Exception e4) {
                    }
                }
                if (this.f153int == 1) {
                    str4 = str4 + "w" + Long.toHexString(this.f578a[0]) + "k";
                    if (this.f578a[0] == j) {
                        str2 = str4 + "k";
                    }
                    str2 = str4;
                } else {
                    if (this.f153int > 1) {
                        String str5 = str4 + "w" + Long.toHexString(this.f578a[0]);
                        if (this.f578a[0] == j) {
                            str5 = str5 + "k";
                            j = 0;
                        }
                        str2 = j > 0 ? str5 + "," + Long.toHexString(j) + "k" : str5 + "," + Long.toHexString(this.f578a[1]);
                    }
                    str2 = str4;
                }
                this.f154long = this.f154long + str2 + str;
                if (this.c != null) {
                    this.f154long = this.f154long + this.c;
                }
                a();
            }
            j.m250if(f.v, "trace2:" + this.f154long);
            m194case();
            this.f154long = null;
        }

        /* renamed from: if  reason: not valid java name */
        public void m197if() {
            try {
                if (f.this.m != null) {
                    File unused = f.j = new File(f.this.m);
                    if (!f.j.exists()) {
                        File file = new File(f.ac);
                        if (!file.exists()) {
                            file.mkdirs();
                        }
                        f.j.createNewFile();
                        RandomAccessFile randomAccessFile = new RandomAccessFile(f.j, "rw");
                        randomAccessFile.seek(0);
                        randomAccessFile.writeInt(0);
                        randomAccessFile.close();
                        return;
                    }
                    return;
                }
                File unused2 = f.j = (File) null;
            } catch (Exception e2) {
                File unused3 = f.j = (File) null;
            }
        }

        /* renamed from: int  reason: not valid java name */
        public void m198int() {
        }

        /* renamed from: new  reason: not valid java name */
        public void m199new() {
            this.f149do = false;
            this.f152if = false;
            m197if();
            f.m181long();
            try {
                RandomAccessFile randomAccessFile = new RandomAccessFile(f.k, "rw");
                randomAccessFile.seek(0);
                int readInt = randomAccessFile.readInt();
                int readInt2 = randomAccessFile.readInt();
                randomAccessFile.readInt();
                long readLong = randomAccessFile.readLong();
                int readInt3 = randomAccessFile.readInt();
                if (readInt < 0) {
                    this.f149do = true;
                    this.f152if = true;
                    randomAccessFile.close();
                    return;
                }
                randomAccessFile.seek((long) ((readInt2 * f.at) + 24));
                int readInt4 = randomAccessFile.readInt();
                if (readInt4 > 680) {
                    this.f149do = true;
                    this.f152if = true;
                    randomAccessFile.close();
                    return;
                }
                byte[] bArr = new byte[f.at];
                randomAccessFile.read(bArr, 0, readInt4);
                if (readInt4 != randomAccessFile.readInt()) {
                    j.m250if(f.v, "trace true check fail");
                    this.f149do = true;
                    this.f152if = true;
                    randomAccessFile.close();
                    return;
                }
                for (int i = 0; i < bArr.length; i++) {
                    bArr[i] = (byte) (bArr[i] ^ 90);
                }
                this.f154long = new String(bArr, 0, readInt4);
                if (!this.f154long.contains("&tr=")) {
                    this.f149do = true;
                    this.f152if = true;
                    randomAccessFile.close();
                    return;
                }
                long currentTimeMillis = System.currentTimeMillis();
                long j = currentTimeMillis - readLong;
                if (j > (j.M * 3) - j.al) {
                    this.f149do = true;
                } else if (j > (j.M * 2) - j.al) {
                    this.f154long += "|" + readInt3;
                    this.f146byte = readInt3 + 2;
                } else if (j > j.M - j.al) {
                    this.f146byte = readInt3 + 1;
                } else {
                    this.f150else = true;
                    randomAccessFile.close();
                    return;
                }
                randomAccessFile.seek(12);
                randomAccessFile.writeLong(currentTimeMillis);
                randomAccessFile.writeInt(this.f146byte);
                randomAccessFile.close();
                RandomAccessFile randomAccessFile2 = new RandomAccessFile(f.j, "rw");
                randomAccessFile2.seek(0);
                if (randomAccessFile2.readInt() == 0) {
                    this.f149do = true;
                    randomAccessFile2.close();
                    j.m250if(f.v, "Day file number 0");
                    return;
                }
                randomAccessFile2.close();
            } catch (Exception e2) {
                j.m250if(f.v, "exception!!!");
                this.f149do = true;
                this.f152if = true;
            }
        }

        /* renamed from: try  reason: not valid java name */
        public void m200try() {
            this.f151goto.unregisterReceiver(this.f147case);
            this.f156try.cancel(this.d);
            File unused = f.j = (File) null;
        }
    }

    public class d extends Handler {
        public d() {
        }

        public void handleMessage(Message message) {
            if (f.this.ad) {
                switch (message.what) {
                    case 11:
                        f.this.m172if(message);
                        break;
                    case 12:
                        f.this.m184new(message);
                        break;
                    case 15:
                        f.this.m187try(message);
                        break;
                    case f.aa /*21*/:
                        f.this.a(message, f.aa);
                        break;
                    case 22:
                        f.this.m177int(message);
                        break;
                    case f.W /*25*/:
                        f.this.m160do(message);
                        break;
                    case f.I /*26*/:
                        f.this.a(message, f.I);
                        break;
                    case f.f133do /*28*/:
                        f.this.m165for(message);
                        break;
                    case f.f136int /*31*/:
                        j.m250if(f.v, "on new cell ...");
                        break;
                    case f.p /*41*/:
                        f.this.m159do();
                        break;
                    case 51:
                        f.this.m171if();
                        break;
                    case f.G /*52*/:
                        f.this.b();
                        break;
                    case f.ar /*53*/:
                        f.this.c();
                        break;
                    case f.D /*57*/:
                        f.this.a(message);
                        break;
                    case 62:
                    case 63:
                        f.this.a((int) f.aa);
                        break;
                    case f.K /*64*/:
                    case 65:
                        f.this.a((int) f.I);
                        break;
                    case 81:
                        f.a(f.this);
                        break;
                    case f.t /*91*/:
                        f.this.m176int();
                        break;
                    case f.B /*92*/:
                        f.this.m156char();
                        break;
                    case 101:
                        if (f.this.y != null) {
                            f.this.y.m195do();
                            break;
                        }
                        break;
                }
            }
            super.handleMessage(message);
        }
    }

    private String a(String str) {
        String str2;
        String str3 = null;
        j.m250if(v, "generate locdata ...");
        if ((this.f143try == null || !this.f143try.m120do()) && this.r != null) {
            this.f143try = this.r.a();
        }
        this.A = this.f143try.a();
        if (this.f143try != null) {
            j.a(v, this.f143try.m122if());
        } else {
            j.a(v, "cellInfo null...");
        }
        if ((this.C == null || !this.C.m145for()) && this.F != null) {
            this.C = this.F.m131byte();
        }
        if (this.C != null) {
            j.a(v, this.C.m144else());
        } else {
            j.a(v, "wifi list null");
        }
        if (this.ab == null || !this.ab.m88new()) {
            this.f = null;
        } else {
            this.f = this.ab.m89try();
        }
        if (this.au != null) {
            str3 = this.au.m39byte();
        }
        String format = 3 == g.m204do(this) ? "&cn=32" : String.format("&cn=%d", Integer.valueOf(this.r.m119new()));
        if (this.Y && (str2 = k.m261if()) != null) {
            format = format + str2;
        }
        String str4 = format + str3;
        if (str != null) {
            str4 = str + str4;
        }
        return j.a(this.f143try, this.C, this.f, str4, 0);
    }

    private String a(boolean z2) {
        if ((this.f143try == null || !this.f143try.m120do()) && this.r != null) {
            this.f143try = this.r.a();
        }
        m162do(this.f143try.a());
        return m170if(z2);
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        j.m250if(v, "on network exception");
        j.a(v, "on network exception");
        this.f142new = null;
        this.f139char = null;
        if (this.au != null) {
            this.au.a(a(false), i2);
        }
        if (i2 == aa) {
            m154case();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.location.k.a(android.location.Location, boolean):boolean
     arg types: [android.location.Location, int]
     candidates:
      com.baidu.location.k.a(java.util.List, int):int
      com.baidu.location.k.a(int, int):void
      com.baidu.location.k.a(int, boolean):void
      com.baidu.location.k.a(java.lang.String, int):void
      com.baidu.location.k.a(android.location.Location, com.baidu.location.e$c):boolean
      com.baidu.location.k.a(java.lang.String, java.util.List):boolean
      com.baidu.location.k.a(android.location.Location, boolean):boolean */
    /* access modifiers changed from: private */
    public void a(Message message) {
        if (message == null || message.obj == null) {
            j.m250if(v, "Gps updateloation is null");
            return;
        }
        Location location = (Location) message.obj;
        if (location != null) {
            j.m250if(v, "on update gps...");
            if (k.a(location, true) && this.r != null && this.F != null) {
                a aVar = this.au;
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(Message message, int i2) {
        j.m250if(v, "on network success");
        j.a(v, "on network success");
        String str = (String) message.obj;
        j.m250if(v, "network:" + str);
        if (this.au != null) {
            this.au.a(str, i2);
        }
        if (j.m244do(str)) {
            if (i2 == aa) {
                this.f142new = str;
            } else {
                this.H = str;
            }
        } else if (i2 == aa) {
            this.f142new = null;
        } else {
            this.H = null;
        }
        int i3 = j.m246if(str, "ssid\":\"", "\"");
        if (i3 == Integer.MIN_VALUE || this.f139char == null) {
            this.h = null;
        } else {
            this.h = this.f139char.m146if(i3);
        }
        m174if(str);
        double d2 = j.m242do(str, "a\":\"", "\"");
        if (d2 != Double.MIN_VALUE) {
            k.a(d2, j.m242do(str, "b\":\"", "\""), j.m242do(str, "c\":\"", "\""), j.m242do(str, "b\":\"", "\""));
        }
        int i4 = j.m246if(str, "rWifiN\":\"", "\"");
        if (i4 > 15) {
            j.Y = i4;
        }
        int i5 = j.m246if(str, "rWifiT\":\"", "\"");
        if (i5 > 500) {
            j.S = i5;
        }
        float a2 = j.a(str, "hSpeedDis\":\"", "\"");
        if (a2 > 5.0f) {
            j.Q = a2;
        }
        float a3 = j.a(str, "mSpeedDis\":\"", "\"");
        if (a3 > 5.0f) {
            j.ai = a3;
        }
        float a4 = j.a(str, "mWifiR\":\"", "\"");
        if (a4 < 1.0f && ((double) a4) > 0.2d) {
            j.f196byte = a4;
        }
        if (i2 == aa) {
            m154case();
        }
    }

    static /* synthetic */ void a(f fVar) {
    }

    private boolean a(c.a aVar) {
        if (this.r == null) {
            return false;
        }
        this.f143try = this.r.a();
        if (this.f143try == aVar) {
            return false;
        }
        if (this.f143try == null || aVar == null) {
            return true;
        }
        return !aVar.a(this.f143try);
    }

    private boolean a(e.c cVar) {
        if (this.F == null) {
            return false;
        }
        this.C = this.F.m131byte();
        if (cVar == this.C) {
            return false;
        }
        if (this.C == null || cVar == null) {
            return true;
        }
        return !cVar.a(this.C);
    }

    /* access modifiers changed from: private */
    public void b() {
        j.m250if(v, "on switch gps ...");
        if (this.au != null) {
            if (this.au.m41for()) {
                if (this.ab == null) {
                    this.ab = new b(this, this.R);
                }
                this.ab.k();
            } else if (this.ab != null) {
                this.ab.l();
                this.ab = null;
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: byte  reason: not valid java name */
    public void m153byte() {
        if (!this.O) {
            if (System.currentTimeMillis() - this.P < 1000) {
                j.m250if(v, "request too frequency ...");
                if (this.f142new != null) {
                    this.au.a(this.f142new);
                    m154case();
                    return;
                }
            }
            j.m250if(v, "start network locating ...");
            j.a(v, "start network locating ...");
            this.O = true;
            this.J = a(this.al);
            if (a(this.f139char) || this.J || this.f142new == null) {
                String a2 = a((String) null);
                if (a2 == null) {
                    this.au.a("{\"result\":{\"time\":\"" + j.m245for() + "\",\"error\":\"62\"}}");
                    m154case();
                    return;
                }
                if (this.h != null) {
                    a2 = a2 + this.h;
                    this.h = null;
                }
                if (g.a(a2, this.R)) {
                    this.al = this.f143try;
                    this.f139char = this.C;
                } else {
                    j.m250if(v, "request error ..");
                }
                if (this.Y) {
                    this.Y = false;
                }
                this.P = System.currentTimeMillis();
                return;
            }
            this.au.a(this.f142new);
            m154case();
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        if (this.au != null) {
            this.au.m47new();
        }
    }

    /* renamed from: case  reason: not valid java name */
    private void m154case() {
        this.O = false;
        m190void();
    }

    /* access modifiers changed from: private */
    /* renamed from: char  reason: not valid java name */
    public void m156char() {
        try {
            if (j.n && j.G) {
                this.y = new c(this);
            }
        } catch (Exception e2) {
        }
    }

    private void d() {
        File file = new File(ac);
        File file2 = new File(ac + "/ls.db");
        if (!file.exists()) {
            file.mkdirs();
        }
        if (!file2.exists()) {
            try {
                file2.createNewFile();
            } catch (Exception e2) {
            }
        }
        if (file2.exists()) {
            this.T = SQLiteDatabase.openOrCreateDatabase(file2, (SQLiteDatabase.CursorFactory) null);
            this.T.execSQL("CREATE TABLE IF NOT EXISTS " + this.f141if + "(id CHAR(40) PRIMARY KEY,time DOUBLE,tag DOUBLE, type DOUBLE , ac INT);");
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: do  reason: not valid java name */
    public void m159do() {
        j.m250if(v, "on new wifi ...");
        if (this.aj) {
            m153byte();
            this.aj = false;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: do  reason: not valid java name */
    public void m160do(Message message) {
        if (System.currentTimeMillis() - this.d < 3000) {
            j.m250if(v, "request too frequency ...");
            if (this.H != null) {
                this.au.a(this.H, (int) I);
                return;
            }
        }
        if (this.au != null) {
            String a2 = a(this.au.a(message));
            if (this.h != null) {
                a2 = a2 + this.h;
                this.h = null;
            }
            g.m204do(this);
            if (g.m219if(a2, this.R)) {
                this.u = this.f143try;
                this.ae = this.C;
            } else {
                j.m250if(v, "request poi error ..");
            }
            this.d = System.currentTimeMillis();
        }
    }

    /* renamed from: do  reason: not valid java name */
    private void m162do(String str) {
        if (this.T == null || str == null) {
            j.m250if(v, "db is null...");
            this.Q = false;
            return;
        }
        j.m250if(v, "LOCATING...");
        if (System.currentTimeMillis() - this.E >= 1500 && !str.equals(this.as)) {
            this.Q = false;
            try {
                Cursor rawQuery = this.T.rawQuery("select * from " + this.f141if + " where id = \"" + str + "\";", null);
                this.as = str;
                this.E = System.currentTimeMillis();
                if (rawQuery != null) {
                    if (rawQuery.moveToFirst()) {
                        j.m250if(v, "lookup DB success:" + this.as);
                        this.o = rawQuery.getDouble(1) - 1235.4323d;
                        this.q = rawQuery.getDouble(2) - 4326.0d;
                        this.n = rawQuery.getDouble(3) - 2367.3217d;
                        this.Q = true;
                        j.m250if(v, "lookup DB success:x" + this.o + "y" + this.n + "r" + this.q);
                    }
                    rawQuery.close();
                }
            } catch (Exception e2) {
                this.E = System.currentTimeMillis();
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: for  reason: not valid java name */
    public void m165for(Message message) {
        if (this.au != null) {
            this.au.a(a(true), message);
        }
    }

    /* renamed from: goto  reason: not valid java name */
    private void m169goto() {
        j.m250if(v, "on new cell ...");
    }

    /* renamed from: if  reason: not valid java name */
    private String m170if(boolean z2) {
        if (!this.Q) {
            return z2 ? "{\"result\":{\"time\":\"" + j.m245for() + "\",\"error\":\"67\"}}" : "{\"result\":{\"time\":\"" + j.m245for() + "\",\"error\":\"63\"}}";
        }
        if (z2) {
            return String.format("{\"result\":{\"time\":\"" + j.m245for() + "\",\"error\":\"66\"},\"content\":{\"point\":{\"x\":\"%f\",\"y\":\"%f\"},\"radius\":\"%f\",\"isCellChanged\":\"%b\"}}", Double.valueOf(this.o), Double.valueOf(this.n), Double.valueOf(this.q), true);
        }
        return String.format("{\"result\":{\"time\":\"" + j.m245for() + "\",\"error\":\"68\"},\"content\":{\"point\":{\"x\":\"%f\",\"y\":\"%f\"},\"radius\":\"%f\",\"isCellChanged\":\"%b\"}}", Double.valueOf(this.o), Double.valueOf(this.n), Double.valueOf(this.q), Boolean.valueOf(this.J));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.location.k.a(android.location.Location, boolean):boolean
     arg types: [android.location.Location, int]
     candidates:
      com.baidu.location.k.a(java.util.List, int):int
      com.baidu.location.k.a(int, int):void
      com.baidu.location.k.a(int, boolean):void
      com.baidu.location.k.a(java.lang.String, int):void
      com.baidu.location.k.a(android.location.Location, com.baidu.location.e$c):boolean
      com.baidu.location.k.a(java.lang.String, java.util.List):boolean
      com.baidu.location.k.a(android.location.Location, boolean):boolean */
    /* access modifiers changed from: private */
    /* renamed from: if  reason: not valid java name */
    public void m171if() {
        if (this.ab != null) {
            j.m250if(v, "on new gps...");
            Location location = this.ab.m89try();
            if (!(!this.ab.m88new() || !k.a(location, true) || this.r == null || this.F == null || this.au == null)) {
                if (this.F != null) {
                    this.F.a();
                }
                k.a(this.r.a(), this.F.m136int(), location, this.au.m39byte());
            }
            if (this.au != null && this.ab.m88new()) {
                this.au.m45if(this.ab.m87int());
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: if  reason: not valid java name */
    public void m172if(Message message) {
        if (this.au != null) {
            this.au.m46int(message);
        }
        if (this.F != null) {
            this.F.m132case();
        }
        if (this.N) {
            g.m211for(this.R);
            this.N = false;
        }
    }

    /* renamed from: if  reason: not valid java name */
    private void m174if(String str) {
        float f2;
        double d2;
        double d3;
        boolean z2 = false;
        if (this.T != null && this.J) {
            try {
                j.m250if(v, "DB:" + str);
                JSONObject jSONObject = new JSONObject(str);
                int parseInt = Integer.parseInt(jSONObject.getJSONObject("result").getString("error"));
                if (parseInt == 161) {
                    JSONObject jSONObject2 = jSONObject.getJSONObject("content");
                    if (jSONObject2.has("clf")) {
                        String string = jSONObject2.getString("clf");
                        if (string.equals("0")) {
                            JSONObject jSONObject3 = jSONObject2.getJSONObject("point");
                            d2 = Double.parseDouble(jSONObject3.getString("x"));
                            d3 = Double.parseDouble(jSONObject3.getString("y"));
                            f2 = Float.parseFloat(jSONObject2.getString("radius"));
                        } else {
                            String[] split = string.split("\\|");
                            d2 = Double.parseDouble(split[0]);
                            d3 = Double.parseDouble(split[1]);
                            f2 = Float.parseFloat(split[2]);
                        }
                        j.m250if(v, "DB PARSE:x" + d2 + "y" + d3 + "R" + f2);
                    } else {
                        z2 = true;
                        f2 = 0.0f;
                        d2 = 0.0d;
                        d3 = 0.0d;
                    }
                } else if (parseInt == 167) {
                    this.T.delete(this.f141if, "id = \"" + this.A + "\"", null);
                    return;
                } else {
                    z2 = true;
                    f2 = 0.0f;
                    d2 = 0.0d;
                    d3 = 0.0d;
                }
                if (!z2) {
                    float f3 = 4326.0f + f2;
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("time", Double.valueOf(d2 + 1235.4323d));
                    contentValues.put("tag", Float.valueOf(f3));
                    contentValues.put("type", Double.valueOf(d3 + 2367.3217d));
                    try {
                        if (this.T.update(this.f141if, contentValues, "id = \"" + this.A + "\"", null) <= 0) {
                            contentValues.put("id", this.A);
                            this.T.insert(this.f141if, null, contentValues);
                            j.m250if(v, "insert DB success!");
                        }
                    } catch (Exception e2) {
                    }
                }
            } catch (Exception e3) {
                j.m250if(v, "DB PARSE:exp!");
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: int  reason: not valid java name */
    public void m176int() {
        if (g.a(this)) {
            g.f();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: int  reason: not valid java name */
    public void m177int(Message message) {
        j.m250if(v, "on request location ...");
        j.a(v, "on request location ...");
        if (this.au != null) {
            if (this.au.m40do(message) != 1 || this.ab == null || !this.ab.m88new()) {
                if (!this.Y) {
                    if (this.O) {
                        return;
                    }
                    if (this.F != null && this.F.m137new()) {
                        this.aj = true;
                        this.R.postDelayed(new b(), 2000);
                        return;
                    }
                }
                m153byte();
                return;
            }
            j.m250if(v, "send gps location to client ...");
            this.au.a(this.ab.m87int(), message);
        }
    }

    /* renamed from: long  reason: not valid java name */
    public static void m181long() {
        try {
            if (f575a != null) {
                File file = new File(f575a);
                k = file;
                if (!file.exists()) {
                    File file2 = new File(ac);
                    if (!file2.exists()) {
                        file2.mkdirs();
                    }
                    k.createNewFile();
                    RandomAccessFile randomAccessFile = new RandomAccessFile(k, "rw");
                    randomAccessFile.seek(0);
                    randomAccessFile.writeInt(-1);
                    randomAccessFile.writeInt(-1);
                    randomAccessFile.writeInt(0);
                    randomAccessFile.writeLong(0);
                    randomAccessFile.writeInt(0);
                    randomAccessFile.writeInt(0);
                    randomAccessFile.close();
                    return;
                }
                return;
            }
            k = null;
        } catch (Exception e2) {
            k = null;
        }
    }

    /* renamed from: new  reason: not valid java name */
    public static String m183new() {
        j.m250if(v, "read trace log1..");
        return null;
    }

    /* access modifiers changed from: private */
    /* renamed from: new  reason: not valid java name */
    public void m184new(Message message) {
        if (this.au != null) {
            this.au.m44if(message);
        }
    }

    /* renamed from: try  reason: not valid java name */
    private void m186try() {
    }

    /* access modifiers changed from: private */
    /* renamed from: try  reason: not valid java name */
    public void m187try(Message message) {
        if (!(this.au == null || !this.au.m42for(message) || this.F == null)) {
            this.F.m135for();
        }
        this.f142new = null;
    }

    /* renamed from: void  reason: not valid java name */
    private void m190void() {
        if (this.f142new != null && g.a(this)) {
            g.f();
        }
    }

    /* renamed from: else  reason: not valid java name */
    public final boolean m192else() {
        return ((KeyguardManager) getSystemService("keyguard")).inKeyguardRestrictedInputMode();
    }

    public final IBinder onBind(Intent intent) {
        return this.an.getBinder();
    }

    public final void onCreate() {
        Thread.setDefaultUncaughtExceptionHandler(new a(this));
        this.r = new c(this, this.R);
        this.F = new e(this, this.R);
        this.au = new a(this.R);
        this.r.m116do();
        this.F.m138try();
        this.ad = true;
        this.O = false;
        this.aj = false;
        try {
            d();
        } catch (Exception e2) {
        }
        j.m250if(v, "OnCreate");
        Log.d(v, "baidu location service start1 ..." + Process.myPid());
    }

    public final void onDestroy() {
        if (this.r != null) {
            this.r.m115byte();
        }
        if (this.F != null) {
            this.F.m134else();
        }
        if (this.ab != null) {
            this.ab.l();
        }
        k.a();
        this.O = false;
        this.aj = false;
        this.ad = false;
        if (this.y != null) {
            this.y.m200try();
        }
        if (this.T != null) {
            this.T.close();
        }
        j.m250if(v, "onDestroy");
        Log.d(v, "baidu location service stop ...");
        Process.killProcess(Process.myPid());
    }

    public final int onStartCommand(Intent intent, int i2, int i3) {
        j.m250if(v, "onStratCommandNotSticky");
        return 2;
    }
}
