package com.jcraft.jzlib;

import java.io.IOException;

public class GZIPException extends IOException {
    public GZIPException() {
    }

    public GZIPException(String str) {
        super(str);
    }
}
