package com.jcraft.jzlib;

public final class CRC32 implements a {
    private static final int GF2_DIM = 32;
    private static int[] crc_table;
    private int v = 0;

    static {
        crc_table = null;
        crc_table = new int[256];
        for (int i = 0; i < 256; i++) {
            int i2 = 8;
            int i3 = i;
            while (true) {
                i2--;
                if (i2 < 0) {
                    break;
                }
                i3 = (i3 & 1) != 0 ? (i3 >>> 1) ^ -306674912 : i3 >>> 1;
            }
            crc_table[i] = i3;
        }
    }

    static long combine(long j, long j2, long j3) {
        long[] jArr = new long[GF2_DIM];
        long[] jArr2 = new long[GF2_DIM];
        if (j3 <= 0) {
            return j;
        }
        jArr2[0] = 3988292384L;
        long j4 = 1;
        for (int i = 1; i < GF2_DIM; i++) {
            jArr2[i] = j4;
            j4 <<= 1;
        }
        gf2_matrix_square(jArr, jArr2);
        gf2_matrix_square(jArr2, jArr);
        do {
            gf2_matrix_square(jArr, jArr2);
            if ((1 & j3) != 0) {
                j = gf2_matrix_times(jArr, j);
            }
            long j5 = j3 >> 1;
            if (j5 == 0) {
                break;
            }
            gf2_matrix_square(jArr2, jArr);
            if ((1 & j5) != 0) {
                j = gf2_matrix_times(jArr2, j);
            }
            j3 = j5 >> 1;
        } while (j3 != 0);
        return j ^ j2;
    }

    public static int[] getCRC32Table() {
        int[] iArr = new int[crc_table.length];
        System.arraycopy(crc_table, 0, iArr, 0, iArr.length);
        return iArr;
    }

    static final void gf2_matrix_square(long[] jArr, long[] jArr2) {
        for (int i = 0; i < GF2_DIM; i++) {
            jArr[i] = gf2_matrix_times(jArr2, jArr2[i]);
        }
    }

    private static long gf2_matrix_times(long[] jArr, long j) {
        int i = 0;
        long j2 = 0;
        while (j != 0) {
            if ((1 & j) != 0) {
                j2 ^= jArr[i];
            }
            j >>= 1;
            i++;
        }
        return j2;
    }

    public final CRC32 copy() {
        CRC32 crc32 = new CRC32();
        crc32.v = this.v;
        return crc32;
    }

    public final long getValue() {
        return ((long) this.v) & 4294967295L;
    }

    public final void reset() {
        this.v = 0;
    }

    public final void reset(long j) {
        this.v = (int) (4294967295L & j);
    }

    public final void update(byte[] bArr, int i, int i2) {
        byte b = this.v ^ -1;
        while (true) {
            i2--;
            if (i2 < 0) {
                this.v = b ^ -1;
                return;
            }
            b = (b >>> 8) ^ crc_table[(bArr[i] ^ b) & GZIPHeader.OS_UNKNOWN];
            i++;
        }
    }
}
