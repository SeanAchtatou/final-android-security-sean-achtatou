package com.jcraft.jzlib;

import java.io.FilterInputStream;
import java.io.InputStream;
import mm.purchasesdk.PurchaseCode;

@Deprecated
public class ZInputStream extends FilterInputStream {
    private byte[] buf;
    private byte[] buf1;
    protected boolean compress;
    protected Deflater deflater;
    protected int flush;
    protected InflaterInputStream iis;
    protected InputStream in;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.jcraft.jzlib.ZInputStream.<init>(java.io.InputStream, boolean):void
     arg types: [java.io.InputStream, int]
     candidates:
      com.jcraft.jzlib.ZInputStream.<init>(java.io.InputStream, int):void
      com.jcraft.jzlib.ZInputStream.<init>(java.io.InputStream, boolean):void */
    public ZInputStream(InputStream inputStream) {
        this(inputStream, false);
    }

    public ZInputStream(InputStream inputStream, int i) {
        super(inputStream);
        this.flush = 0;
        this.in = null;
        this.buf1 = new byte[1];
        this.buf = new byte[PurchaseCode.QUERY_NO_APP];
        this.in = inputStream;
        this.deflater = new Deflater();
        this.deflater.init(i);
        this.compress = true;
    }

    public ZInputStream(InputStream inputStream, boolean z) {
        super(inputStream);
        this.flush = 0;
        this.in = null;
        this.buf1 = new byte[1];
        this.buf = new byte[PurchaseCode.QUERY_NO_APP];
        this.iis = new InflaterInputStream(inputStream);
        this.compress = false;
    }

    public void close() {
        if (this.compress) {
            this.deflater.end();
        } else {
            this.iis.close();
        }
    }

    public int getFlushMode() {
        return this.flush;
    }

    public long getTotalIn() {
        return this.compress ? this.deflater.total_in : this.iis.getTotalIn();
    }

    public long getTotalOut() {
        return this.compress ? this.deflater.total_out : this.iis.getTotalOut();
    }

    public int read() {
        if (read(this.buf1, 0, 1) == -1) {
            return -1;
        }
        return this.buf1[0] & GZIPHeader.OS_UNKNOWN;
    }

    public int read(byte[] bArr, int i, int i2) {
        int deflate;
        if (!this.compress) {
            return this.iis.read(bArr, i, i2);
        }
        this.deflater.setOutput(bArr, i, i2);
        do {
            int read = this.in.read(this.buf, 0, this.buf.length);
            if (read != -1) {
                this.deflater.setInput(this.buf, 0, read, true);
                deflate = this.deflater.deflate(this.flush);
                if (this.deflater.next_out_index <= 0) {
                    if (deflate != 1) {
                        if (deflate == -2) {
                            break;
                        }
                    } else {
                        return 0;
                    }
                } else {
                    return this.deflater.next_out_index;
                }
            } else {
                return -1;
            }
        } while (deflate != -3);
        throw new ZStreamException("deflating: " + this.deflater.msg);
    }

    public void setFlushMode(int i) {
        this.flush = i;
    }

    public long skip(long j) {
        int i = PurchaseCode.QUERY_NO_APP;
        if (j < 512) {
            i = (int) j;
        }
        return (long) read(new byte[i]);
    }
}
