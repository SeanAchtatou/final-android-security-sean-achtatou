package com.jcraft.jzlib;

import com.amap.mapapi.poisearch.PoiTypeDef;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import mm.purchasesdk.PurchaseCode;

@Deprecated
public class ZOutputStream extends FilterOutputStream {
    protected byte[] buf;
    private byte[] buf1;
    protected int bufsize;
    protected boolean compress;
    private DeflaterOutputStream dos;
    private boolean end;
    protected int flush;
    private Inflater inflater;
    protected OutputStream out;

    public ZOutputStream(OutputStream outputStream) {
        super(outputStream);
        this.bufsize = PurchaseCode.QUERY_NO_APP;
        this.flush = 0;
        this.buf = new byte[this.bufsize];
        this.end = false;
        this.buf1 = new byte[1];
        this.out = outputStream;
        this.inflater = new Inflater();
        this.inflater.init();
        this.compress = false;
    }

    public ZOutputStream(OutputStream outputStream, int i) {
        this(outputStream, i, false);
    }

    public ZOutputStream(OutputStream outputStream, int i, boolean z) {
        super(outputStream);
        this.bufsize = PurchaseCode.QUERY_NO_APP;
        this.flush = 0;
        this.buf = new byte[this.bufsize];
        this.end = false;
        this.buf1 = new byte[1];
        this.out = outputStream;
        this.dos = new DeflaterOutputStream(outputStream, new Deflater(i, z));
        this.compress = true;
    }

    public void close() {
        try {
            finish();
        } catch (IOException e) {
        } catch (Throwable th) {
            end();
            this.out.close();
            this.out = null;
            throw th;
        }
        end();
        this.out.close();
        this.out = null;
    }

    public synchronized void end() {
        if (!this.end) {
            if (this.compress) {
                try {
                    this.dos.finish();
                } catch (Exception e) {
                }
            } else {
                this.inflater.end();
            }
            this.end = true;
        }
    }

    public void finish() {
        if (this.compress) {
            int i = this.flush;
            write(PoiTypeDef.All.getBytes(), 0, 0);
        } else {
            this.dos.finish();
        }
        flush();
    }

    public void flush() {
        this.out.flush();
    }

    public int getFlushMode() {
        return this.flush;
    }

    public long getTotalIn() {
        return this.compress ? this.dos.getTotalIn() : this.inflater.total_in;
    }

    public long getTotalOut() {
        return this.compress ? this.dos.getTotalOut() : this.inflater.total_out;
    }

    public void setFlushMode(int i) {
        this.flush = i;
    }

    public void write(int i) {
        this.buf1[0] = (byte) i;
        write(this.buf1, 0, 1);
    }

    public void write(byte[] bArr, int i, int i2) {
        if (i2 != 0) {
            if (this.compress) {
                this.dos.write(bArr, i, i2);
                return;
            }
            this.inflater.setInput(bArr, i, i2, true);
            int i3 = 0;
            while (this.inflater.avail_in > 0) {
                this.inflater.setOutput(this.buf, 0, this.buf.length);
                i3 = this.inflater.inflate(this.flush);
                if (this.inflater.next_out_index > 0) {
                    this.out.write(this.buf, 0, this.inflater.next_out_index);
                    continue;
                }
                if (i3 != 0) {
                    break;
                }
            }
            if (i3 != 0) {
                throw new ZStreamException("inflating: " + this.inflater.msg);
            }
        }
    }
}
