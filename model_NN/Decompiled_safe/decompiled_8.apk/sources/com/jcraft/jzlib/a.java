package com.jcraft.jzlib;

interface a {
    a copy();

    long getValue();

    void reset();

    void reset(long j);

    void update(byte[] bArr, int i, int i2);
}
