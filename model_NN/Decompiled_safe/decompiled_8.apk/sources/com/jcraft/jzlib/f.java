package com.jcraft.jzlib;

import com.immomo.momo.h;
import com.mapabc.minimap.map.vmap.VMapProjection;
import com.sina.weibo.sdk.constant.Constants;
import java.io.ByteArrayOutputStream;

final class f {
    private static byte[] n;

    /* renamed from: a  reason: collision with root package name */
    int f3108a;
    long b = -1;
    int c;
    private int d;
    private long e;
    private int f;
    private int g;
    private c h;
    private final ZStream i;
    private int j;
    private int k = -1;
    private byte[] l = new byte[4];
    private GZIPHeader m = null;
    private ByteArrayOutputStream o = null;

    static {
        byte[] bArr = new byte[4];
        bArr[2] = -1;
        bArr[3] = -1;
        n = bArr;
    }

    f(ZStream zStream) {
        this.i = zStream;
    }

    private int a(int i2, int i3) {
        if (this.o == null) {
            this.o = new ByteArrayOutputStream();
        }
        while (this.i.avail_in != 0) {
            ZStream zStream = this.i;
            zStream.avail_in--;
            this.i.total_in++;
            byte b2 = this.i.next_in[this.i.next_in_index];
            if (b2 != 0) {
                this.o.write(this.i.next_in, this.i.next_in_index, 1);
            }
            this.i.adler.update(this.i.next_in, this.i.next_in_index, 1);
            this.i.next_in_index++;
            if (b2 == 0) {
                return i3;
            }
            i2 = i3;
        }
        throw new g(i2);
    }

    private int a(int i2, int i3, int i4) {
        if (this.k == -1) {
            this.k = i2;
            this.e = 0;
        }
        while (this.k > 0) {
            if (this.i.avail_in == 0) {
                throw new g(i3);
            }
            ZStream zStream = this.i;
            zStream.avail_in--;
            this.i.total_in++;
            long j2 = this.e;
            byte[] bArr = this.i.next_in;
            ZStream zStream2 = this.i;
            int i5 = zStream2.next_in_index;
            zStream2.next_in_index = i5 + 1;
            this.e = j2 | ((long) ((bArr[i5] & GZIPHeader.OS_UNKNOWN) << ((i2 - this.k) * 8)));
            this.k--;
            i3 = i4;
        }
        if (i2 == 2) {
            this.e &= 65535;
        } else if (i2 == 4) {
            this.e &= 4294967295L;
        }
        this.k = -1;
        return i3;
    }

    private void a(int i2, long j2) {
        for (int i3 = 0; i3 < i2; i3++) {
            this.l[i3] = (byte) ((int) (255 & j2));
            j2 >>= 8;
        }
        this.i.adler.update(this.l, 0, i2);
    }

    private int f() {
        if (this.i == null) {
            return -2;
        }
        ZStream zStream = this.i;
        this.i.total_out = 0;
        zStream.total_in = 0;
        this.i.msg = null;
        this.f3108a = 14;
        this.k = -1;
        this.h.a();
        return 0;
    }

    /* access modifiers changed from: package-private */
    public final int a() {
        if (this.h == null) {
            return 0;
        }
        this.h.b();
        return 0;
    }

    /* access modifiers changed from: package-private */
    public final int a(int i2) {
        this.i.msg = null;
        this.h = null;
        this.c = 0;
        if (i2 < 0) {
            i2 = -i2;
        } else {
            this.c = (i2 >> 4) + 1;
            if (i2 < 48) {
                i2 &= 15;
            }
        }
        if (i2 < 8 || i2 > 15) {
            a();
            return -2;
        }
        if (!(this.h == null || this.g == i2)) {
            this.h.b();
            this.h = null;
        }
        this.g = i2;
        this.h = new c(this.i, 1 << i2);
        f();
        return 0;
    }

    /* access modifiers changed from: package-private */
    public final int a(byte[] bArr, int i2) {
        int i3;
        if (this.i == null || (this.f3108a != 6 && this.c != 0)) {
            return -2;
        }
        if (this.f3108a == 6) {
            long value = this.i.adler.getValue();
            this.i.adler.reset();
            this.i.adler.update(bArr, 0, i2);
            if (this.i.adler.getValue() != value) {
                return -3;
            }
        }
        this.i.adler.reset();
        if (i2 >= (1 << this.g)) {
            int i4 = (1 << this.g) - 1;
            int i5 = i2 - i4;
            i2 = i4;
            i3 = i5;
        } else {
            i3 = 0;
        }
        this.h.a(bArr, i3, i2);
        this.f3108a = 7;
        return 0;
    }

    /* access modifiers changed from: package-private */
    public final int b() {
        if (this.i == null) {
            return -2;
        }
        if (this.f3108a != 13) {
            this.f3108a = 13;
            this.f = 0;
        }
        int i2 = this.i.avail_in;
        if (i2 == 0) {
            return -5;
        }
        int i3 = this.i.next_in_index;
        int i4 = this.f;
        while (i2 != 0 && i4 < 4) {
            i4 = this.i.next_in[i3] == n[i4] ? i4 + 1 : this.i.next_in[i3] != 0 ? 0 : 4 - i4;
            i3++;
            i2--;
        }
        this.i.total_in += (long) (i3 - this.i.next_in_index);
        this.i.next_in_index = i3;
        this.i.avail_in = i2;
        this.f = i4;
        if (i4 != 4) {
            return -3;
        }
        long j2 = this.i.total_in;
        long j3 = this.i.total_out;
        f();
        this.i.total_in = j2;
        this.i.total_out = j3;
        this.f3108a = 7;
        return 0;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:286:?, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:287:?, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:288:?, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:289:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:290:?, code lost:
        return 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00d0, code lost:
        if (r14.i.avail_in == 0) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00d2, code lost:
        r1 = r14.i;
        r1.avail_in--;
        r14.i.total_in++;
        r1 = r14.i.next_in;
        r2 = r14.i;
        r4 = r2.next_in_index;
        r2.next_in_index = r4 + 1;
        r14.e = ((long) ((r1[r4] & com.jcraft.jzlib.GZIPHeader.OS_UNKNOWN) << 24)) & 4278190080L;
        r14.f3108a = 3;
        r2 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0104, code lost:
        if (r14.i.avail_in == 0) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0106, code lost:
        r1 = r14.i;
        r1.avail_in--;
        r14.i.total_in++;
        r1 = r14.e;
        r4 = r14.i.next_in;
        r5 = r14.i;
        r6 = r5.next_in_index;
        r5.next_in_index = r6 + 1;
        r14.e = r1 + (((long) ((r4[r6] & com.jcraft.jzlib.GZIPHeader.OS_UNKNOWN) << 16)) & 16711680);
        r14.f3108a = 4;
        r2 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0139, code lost:
        if (r14.i.avail_in == 0) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x013b, code lost:
        r1 = r14.i;
        r1.avail_in--;
        r14.i.total_in++;
        r1 = r14.e;
        r4 = r14.i.next_in;
        r5 = r14.i;
        r6 = r5.next_in_index;
        r5.next_in_index = r6 + 1;
        r14.e = r1 + (((long) ((r4[r6] & com.jcraft.jzlib.GZIPHeader.OS_UNKNOWN) << 8)) & 65280);
        r14.f3108a = 5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x016d, code lost:
        if (r14.i.avail_in != 0) goto L_0x0172;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0172, code lost:
        r0 = r14.i;
        r0.avail_in--;
        r14.i.total_in++;
        r0 = r14.e;
        r2 = r14.i.next_in;
        r4 = r14.i;
        r5 = r4.next_in_index;
        r4.next_in_index = r5 + 1;
        r14.e = r0 + (((long) r2[r5]) & 255);
        r14.i.adler.reset(r14.e);
        r14.f3108a = 6;
     */
    /* JADX WARNING: Removed duplicated region for block: B:143:0x03c5  */
    /* JADX WARNING: Removed duplicated region for block: B:146:0x03d1  */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x03e4  */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x03fe  */
    /* JADX WARNING: Removed duplicated region for block: B:160:0x040d  */
    /* JADX WARNING: Removed duplicated region for block: B:172:0x0438 A[SYNTHETIC, Splitter:B:172:0x0438] */
    /* JADX WARNING: Removed duplicated region for block: B:186:0x0474 A[SYNTHETIC, Splitter:B:186:0x0474] */
    /* JADX WARNING: Removed duplicated region for block: B:194:0x0493 A[SYNTHETIC, Splitter:B:194:0x0493] */
    /* JADX WARNING: Removed duplicated region for block: B:202:0x04b2  */
    /* JADX WARNING: Removed duplicated region for block: B:217:0x04f4  */
    /* JADX WARNING: Removed duplicated region for block: B:230:0x0560  */
    /* JADX WARNING: Removed duplicated region for block: B:235:0x056f  */
    /* JADX WARNING: Removed duplicated region for block: B:240:0x057e  */
    /* JADX WARNING: Removed duplicated region for block: B:249:0x001b A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:259:0x001b A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:262:0x001b A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:265:0x001b A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x01ee  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0223  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0259  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x028f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int b(int r15) {
        /*
            r14 = this;
            r1 = 0
            r13 = 0
            r11 = 1
            r3 = 2
            r10 = 13
            com.jcraft.jzlib.ZStream r0 = r14.i
            if (r0 == 0) goto L_0x0011
            com.jcraft.jzlib.ZStream r0 = r14.i
            byte[] r0 = r0.next_in
            if (r0 != 0) goto L_0x001e
        L_0x0011:
            r0 = 4
            if (r15 != r0) goto L_0x001c
            int r0 = r14.f3108a
            r2 = 14
            if (r0 != r2) goto L_0x001c
            r2 = r1
        L_0x001b:
            return r2
        L_0x001c:
            r2 = -2
            goto L_0x001b
        L_0x001e:
            r0 = 4
            if (r15 != r0) goto L_0x002a
            r0 = -5
        L_0x0022:
            r2 = -5
        L_0x0023:
            int r4 = r14.f3108a
            switch(r4) {
                case 2: goto L_0x00cc;
                case 3: goto L_0x0100;
                case 4: goto L_0x0135;
                case 5: goto L_0x059b;
                case 6: goto L_0x01a7;
                case 7: goto L_0x01b4;
                case 8: goto L_0x01e8;
                case 9: goto L_0x021d;
                case 10: goto L_0x0253;
                case 11: goto L_0x0289;
                case 12: goto L_0x0357;
                case 13: goto L_0x0373;
                case 14: goto L_0x002c;
                case 15: goto L_0x02fb;
                case 16: goto L_0x03bc;
                case 17: goto L_0x03db;
                case 18: goto L_0x0407;
                case 19: goto L_0x0432;
                case 20: goto L_0x046e;
                case 21: goto L_0x048d;
                case 22: goto L_0x04ac;
                case 23: goto L_0x0376;
                default: goto L_0x0028;
            }
        L_0x0028:
            r2 = -2
            goto L_0x001b
        L_0x002a:
            r0 = r1
            goto L_0x0022
        L_0x002c:
            int r4 = r14.c
            if (r4 != 0) goto L_0x0034
            r4 = 7
            r14.f3108a = r4
            goto L_0x0023
        L_0x0034:
            r4 = 2
            int r2 = r14.a(r4, r2, r0)     // Catch:{ g -> 0x0066 }
            int r4 = r14.c
            r4 = r4 & 2
            if (r4 == 0) goto L_0x006a
            long r4 = r14.e
            r6 = 35615(0x8b1f, double:1.7596E-319)
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 != 0) goto L_0x006a
            com.jcraft.jzlib.ZStream r4 = r14.i
            com.jcraft.jzlib.CRC32 r5 = new com.jcraft.jzlib.CRC32
            r5.<init>()
            r4.adler = r5
            long r4 = r14.e
            r14.a(r3, r4)
            com.jcraft.jzlib.GZIPHeader r4 = r14.m
            if (r4 != 0) goto L_0x0061
            com.jcraft.jzlib.GZIPHeader r4 = new com.jcraft.jzlib.GZIPHeader
            r4.<init>()
            r14.m = r4
        L_0x0061:
            r4 = 23
            r14.f3108a = r4
            goto L_0x0023
        L_0x0066:
            r0 = move-exception
            int r2 = r0.f3109a
            goto L_0x001b
        L_0x006a:
            r14.j = r1
            long r4 = r14.e
            int r4 = (int) r4
            r4 = r4 & 255(0xff, float:3.57E-43)
            r14.d = r4
            long r4 = r14.e
            r6 = 8
            long r4 = r4 >> r6
            int r4 = (int) r4
            r4 = r4 & 255(0xff, float:3.57E-43)
            int r5 = r14.c
            r5 = r5 & 1
            if (r5 == 0) goto L_0x008a
            int r5 = r14.d
            int r5 = r5 << 8
            int r5 = r5 + r4
            int r5 = r5 % 31
            if (r5 == 0) goto L_0x0093
        L_0x008a:
            r14.f3108a = r10
            com.jcraft.jzlib.ZStream r4 = r14.i
            java.lang.String r5 = "incorrect header check"
            r4.msg = r5
            goto L_0x0023
        L_0x0093:
            int r5 = r14.d
            r5 = r5 & 15
            r6 = 8
            if (r5 == r6) goto L_0x00a4
            r14.f3108a = r10
            com.jcraft.jzlib.ZStream r4 = r14.i
            java.lang.String r5 = "unknown compression method"
            r4.msg = r5
            goto L_0x0023
        L_0x00a4:
            int r5 = r14.d
            int r5 = r5 >> 4
            int r5 = r5 + 8
            int r6 = r14.g
            if (r5 <= r6) goto L_0x00b8
            r14.f3108a = r10
            com.jcraft.jzlib.ZStream r4 = r14.i
            java.lang.String r5 = "invalid window size"
            r4.msg = r5
            goto L_0x0023
        L_0x00b8:
            com.jcraft.jzlib.ZStream r5 = r14.i
            com.jcraft.jzlib.Adler32 r6 = new com.jcraft.jzlib.Adler32
            r6.<init>()
            r5.adler = r6
            r4 = r4 & 32
            if (r4 != 0) goto L_0x00ca
            r4 = 7
            r14.f3108a = r4
            goto L_0x0023
        L_0x00ca:
            r14.f3108a = r3
        L_0x00cc:
            com.jcraft.jzlib.ZStream r1 = r14.i
            int r1 = r1.avail_in
            if (r1 == 0) goto L_0x001b
            com.jcraft.jzlib.ZStream r1 = r14.i
            int r2 = r1.avail_in
            int r2 = r2 + -1
            r1.avail_in = r2
            com.jcraft.jzlib.ZStream r1 = r14.i
            long r4 = r1.total_in
            long r4 = r4 + r11
            r1.total_in = r4
            com.jcraft.jzlib.ZStream r1 = r14.i
            byte[] r1 = r1.next_in
            com.jcraft.jzlib.ZStream r2 = r14.i
            int r4 = r2.next_in_index
            int r5 = r4 + 1
            r2.next_in_index = r5
            byte r1 = r1[r4]
            r1 = r1 & 255(0xff, float:3.57E-43)
            int r1 = r1 << 24
            long r1 = (long) r1
            r4 = 4278190080(0xff000000, double:2.113706745E-314)
            long r1 = r1 & r4
            r14.e = r1
            r1 = 3
            r14.f3108a = r1
            r2 = r0
        L_0x0100:
            com.jcraft.jzlib.ZStream r1 = r14.i
            int r1 = r1.avail_in
            if (r1 == 0) goto L_0x001b
            com.jcraft.jzlib.ZStream r1 = r14.i
            int r2 = r1.avail_in
            int r2 = r2 + -1
            r1.avail_in = r2
            com.jcraft.jzlib.ZStream r1 = r14.i
            long r4 = r1.total_in
            long r4 = r4 + r11
            r1.total_in = r4
            long r1 = r14.e
            com.jcraft.jzlib.ZStream r4 = r14.i
            byte[] r4 = r4.next_in
            com.jcraft.jzlib.ZStream r5 = r14.i
            int r6 = r5.next_in_index
            int r7 = r6 + 1
            r5.next_in_index = r7
            byte r4 = r4[r6]
            r4 = r4 & 255(0xff, float:3.57E-43)
            int r4 = r4 << 16
            long r4 = (long) r4
            r6 = 16711680(0xff0000, double:8.256667E-317)
            long r4 = r4 & r6
            long r1 = r1 + r4
            r14.e = r1
            r1 = 4
            r14.f3108a = r1
            r2 = r0
        L_0x0135:
            com.jcraft.jzlib.ZStream r1 = r14.i
            int r1 = r1.avail_in
            if (r1 == 0) goto L_0x001b
            com.jcraft.jzlib.ZStream r1 = r14.i
            int r2 = r1.avail_in
            int r2 = r2 + -1
            r1.avail_in = r2
            com.jcraft.jzlib.ZStream r1 = r14.i
            long r4 = r1.total_in
            long r4 = r4 + r11
            r1.total_in = r4
            long r1 = r14.e
            com.jcraft.jzlib.ZStream r4 = r14.i
            byte[] r4 = r4.next_in
            com.jcraft.jzlib.ZStream r5 = r14.i
            int r6 = r5.next_in_index
            int r7 = r6 + 1
            r5.next_in_index = r7
            byte r4 = r4[r6]
            r4 = r4 & 255(0xff, float:3.57E-43)
            int r4 = r4 << 8
            long r4 = (long) r4
            r6 = 65280(0xff00, double:3.22526E-319)
            long r4 = r4 & r6
            long r1 = r1 + r4
            r14.e = r1
            r1 = 5
            r14.f3108a = r1
        L_0x0169:
            com.jcraft.jzlib.ZStream r1 = r14.i
            int r1 = r1.avail_in
            if (r1 != 0) goto L_0x0172
            r2 = r0
            goto L_0x001b
        L_0x0172:
            com.jcraft.jzlib.ZStream r0 = r14.i
            int r1 = r0.avail_in
            int r1 = r1 + -1
            r0.avail_in = r1
            com.jcraft.jzlib.ZStream r0 = r14.i
            long r1 = r0.total_in
            long r1 = r1 + r11
            r0.total_in = r1
            long r0 = r14.e
            com.jcraft.jzlib.ZStream r2 = r14.i
            byte[] r2 = r2.next_in
            com.jcraft.jzlib.ZStream r4 = r14.i
            int r5 = r4.next_in_index
            int r6 = r5 + 1
            r4.next_in_index = r6
            byte r2 = r2[r5]
            long r4 = (long) r2
            r6 = 255(0xff, double:1.26E-321)
            long r4 = r4 & r6
            long r0 = r0 + r4
            r14.e = r0
            com.jcraft.jzlib.ZStream r0 = r14.i
            com.jcraft.jzlib.a r0 = r0.adler
            long r1 = r14.e
            r0.reset(r1)
            r0 = 6
            r14.f3108a = r0
            r2 = r3
            goto L_0x001b
        L_0x01a7:
            r14.f3108a = r10
            com.jcraft.jzlib.ZStream r0 = r14.i
            java.lang.String r2 = "need dictionary"
            r0.msg = r2
            r14.f = r1
            r2 = -2
            goto L_0x001b
        L_0x01b4:
            com.jcraft.jzlib.c r4 = r14.h
            int r2 = r4.a(r2)
            r4 = -3
            if (r2 != r4) goto L_0x01c3
            r14.f3108a = r10
            r14.f = r1
            goto L_0x0023
        L_0x01c3:
            if (r2 != 0) goto L_0x01c6
            r2 = r0
        L_0x01c6:
            r4 = 1
            if (r2 != r4) goto L_0x001b
            com.jcraft.jzlib.ZStream r2 = r14.i
            com.jcraft.jzlib.a r2 = r2.adler
            long r4 = r2.getValue()
            r14.b = r4
            com.jcraft.jzlib.c r2 = r14.h
            r2.a()
            int r2 = r14.c
            if (r2 != 0) goto L_0x01e3
            r2 = 12
            r14.f3108a = r2
            r2 = r0
            goto L_0x0023
        L_0x01e3:
            r2 = 8
            r14.f3108a = r2
            r2 = r0
        L_0x01e8:
            com.jcraft.jzlib.ZStream r4 = r14.i
            int r4 = r4.avail_in
            if (r4 == 0) goto L_0x001b
            com.jcraft.jzlib.ZStream r2 = r14.i
            int r4 = r2.avail_in
            int r4 = r4 + -1
            r2.avail_in = r4
            com.jcraft.jzlib.ZStream r2 = r14.i
            long r4 = r2.total_in
            long r4 = r4 + r11
            r2.total_in = r4
            com.jcraft.jzlib.ZStream r2 = r14.i
            byte[] r2 = r2.next_in
            com.jcraft.jzlib.ZStream r4 = r14.i
            int r5 = r4.next_in_index
            int r6 = r5 + 1
            r4.next_in_index = r6
            byte r2 = r2[r5]
            r2 = r2 & 255(0xff, float:3.57E-43)
            int r2 = r2 << 24
            long r4 = (long) r2
            r6 = 4278190080(0xff000000, double:2.113706745E-314)
            long r4 = r4 & r6
            r14.e = r4
            r2 = 9
            r14.f3108a = r2
            r2 = r0
        L_0x021d:
            com.jcraft.jzlib.ZStream r4 = r14.i
            int r4 = r4.avail_in
            if (r4 == 0) goto L_0x001b
            com.jcraft.jzlib.ZStream r2 = r14.i
            int r4 = r2.avail_in
            int r4 = r4 + -1
            r2.avail_in = r4
            com.jcraft.jzlib.ZStream r2 = r14.i
            long r4 = r2.total_in
            long r4 = r4 + r11
            r2.total_in = r4
            long r4 = r14.e
            com.jcraft.jzlib.ZStream r2 = r14.i
            byte[] r2 = r2.next_in
            com.jcraft.jzlib.ZStream r6 = r14.i
            int r7 = r6.next_in_index
            int r8 = r7 + 1
            r6.next_in_index = r8
            byte r2 = r2[r7]
            r2 = r2 & 255(0xff, float:3.57E-43)
            int r2 = r2 << 16
            long r6 = (long) r2
            r8 = 16711680(0xff0000, double:8.256667E-317)
            long r6 = r6 & r8
            long r4 = r4 + r6
            r14.e = r4
            r2 = 10
            r14.f3108a = r2
            r2 = r0
        L_0x0253:
            com.jcraft.jzlib.ZStream r4 = r14.i
            int r4 = r4.avail_in
            if (r4 == 0) goto L_0x001b
            com.jcraft.jzlib.ZStream r2 = r14.i
            int r4 = r2.avail_in
            int r4 = r4 + -1
            r2.avail_in = r4
            com.jcraft.jzlib.ZStream r2 = r14.i
            long r4 = r2.total_in
            long r4 = r4 + r11
            r2.total_in = r4
            long r4 = r14.e
            com.jcraft.jzlib.ZStream r2 = r14.i
            byte[] r2 = r2.next_in
            com.jcraft.jzlib.ZStream r6 = r14.i
            int r7 = r6.next_in_index
            int r8 = r7 + 1
            r6.next_in_index = r8
            byte r2 = r2[r7]
            r2 = r2 & 255(0xff, float:3.57E-43)
            int r2 = r2 << 8
            long r6 = (long) r2
            r8 = 65280(0xff00, double:3.22526E-319)
            long r6 = r6 & r8
            long r4 = r4 + r6
            r14.e = r4
            r2 = 11
            r14.f3108a = r2
            r2 = r0
        L_0x0289:
            com.jcraft.jzlib.ZStream r4 = r14.i
            int r4 = r4.avail_in
            if (r4 == 0) goto L_0x001b
            com.jcraft.jzlib.ZStream r2 = r14.i
            int r4 = r2.avail_in
            int r4 = r4 + -1
            r2.avail_in = r4
            com.jcraft.jzlib.ZStream r2 = r14.i
            long r4 = r2.total_in
            long r4 = r4 + r11
            r2.total_in = r4
            long r4 = r14.e
            com.jcraft.jzlib.ZStream r2 = r14.i
            byte[] r2 = r2.next_in
            com.jcraft.jzlib.ZStream r6 = r14.i
            int r7 = r6.next_in_index
            int r8 = r7 + 1
            r6.next_in_index = r8
            byte r2 = r2[r7]
            long r6 = (long) r2
            r8 = 255(0xff, double:1.26E-321)
            long r6 = r6 & r8
            long r4 = r4 + r6
            r14.e = r4
            int r2 = r14.j
            if (r2 == 0) goto L_0x02e8
            long r4 = r14.e
            r6 = -16777216(0xffffffffff000000, double:NaN)
            long r4 = r4 & r6
            r2 = 24
            long r4 = r4 >> r2
            long r6 = r14.e
            r8 = 16711680(0xff0000, double:8.256667E-317)
            long r6 = r6 & r8
            r2 = 8
            long r6 = r6 >> r2
            long r4 = r4 | r6
            long r6 = r14.e
            r8 = 65280(0xff00, double:3.22526E-319)
            long r6 = r6 & r8
            r2 = 8
            long r6 = r6 << r2
            long r4 = r4 | r6
            long r6 = r14.e
            r8 = 65535(0xffff, double:3.23786E-319)
            long r6 = r6 & r8
            r2 = 24
            long r6 = r6 << r2
            long r4 = r4 | r6
            r6 = 4294967295(0xffffffff, double:2.1219957905E-314)
            long r4 = r4 & r6
            r14.e = r4
        L_0x02e8:
            long r4 = r14.b
            int r2 = (int) r4
            long r4 = r14.e
            int r4 = (int) r4
            if (r2 == r4) goto L_0x0321
            com.jcraft.jzlib.ZStream r2 = r14.i
            java.lang.String r4 = "incorrect data check"
            r2.msg = r4
        L_0x02f6:
            r2 = 15
            r14.f3108a = r2
            r2 = r0
        L_0x02fb:
            int r4 = r14.c
            if (r4 == 0) goto L_0x035a
            int r4 = r14.j
            if (r4 == 0) goto L_0x035a
            r4 = 4
            int r2 = r14.a(r4, r2, r0)     // Catch:{ g -> 0x0330 }
            com.jcraft.jzlib.ZStream r4 = r14.i
            java.lang.String r4 = r4.msg
            if (r4 == 0) goto L_0x0335
            com.jcraft.jzlib.ZStream r4 = r14.i
            java.lang.String r4 = r4.msg
            java.lang.String r5 = "incorrect data check"
            boolean r4 = r4.equals(r5)
            if (r4 == 0) goto L_0x0335
            r14.f3108a = r10
            r4 = 5
            r14.f = r4
            goto L_0x0023
        L_0x0321:
            int r2 = r14.j
            if (r2 == 0) goto L_0x02f6
            com.jcraft.jzlib.GZIPHeader r2 = r14.m
            if (r2 == 0) goto L_0x02f6
            com.jcraft.jzlib.GZIPHeader r2 = r14.m
            long r4 = r14.e
            r2.crc = r4
            goto L_0x02f6
        L_0x0330:
            r0 = move-exception
            int r2 = r0.f3109a
            goto L_0x001b
        L_0x0335:
            long r4 = r14.e
            com.jcraft.jzlib.ZStream r6 = r14.i
            long r6 = r6.total_out
            r8 = 4294967295(0xffffffff, double:2.1219957905E-314)
            long r6 = r6 & r8
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 == 0) goto L_0x034f
            com.jcraft.jzlib.ZStream r4 = r14.i
            java.lang.String r5 = "incorrect length check"
            r4.msg = r5
            r14.f3108a = r10
            goto L_0x0023
        L_0x034f:
            com.jcraft.jzlib.ZStream r0 = r14.i
            r0.msg = r13
        L_0x0353:
            r0 = 12
            r14.f3108a = r0
        L_0x0357:
            r2 = 1
            goto L_0x001b
        L_0x035a:
            com.jcraft.jzlib.ZStream r4 = r14.i
            java.lang.String r4 = r4.msg
            if (r4 == 0) goto L_0x0353
            com.jcraft.jzlib.ZStream r4 = r14.i
            java.lang.String r4 = r4.msg
            java.lang.String r5 = "incorrect data check"
            boolean r4 = r4.equals(r5)
            if (r4 == 0) goto L_0x0353
            r14.f3108a = r10
            r4 = 5
            r14.f = r4
            goto L_0x0023
        L_0x0373:
            r2 = -3
            goto L_0x001b
        L_0x0376:
            r4 = 2
            int r2 = r14.a(r4, r2, r0)     // Catch:{ g -> 0x0396 }
            long r4 = r14.e
            int r4 = (int) r4
            r5 = 65535(0xffff, float:9.1834E-41)
            r4 = r4 & r5
            r14.j = r4
            int r4 = r14.j
            r4 = r4 & 255(0xff, float:3.57E-43)
            r5 = 8
            if (r4 == r5) goto L_0x039b
            com.jcraft.jzlib.ZStream r4 = r14.i
            java.lang.String r5 = "unknown compression method"
            r4.msg = r5
            r14.f3108a = r10
            goto L_0x0023
        L_0x0396:
            r0 = move-exception
            int r2 = r0.f3109a
            goto L_0x001b
        L_0x039b:
            int r4 = r14.j
            r5 = 57344(0xe000, float:8.0356E-41)
            r4 = r4 & r5
            if (r4 == 0) goto L_0x03ad
            com.jcraft.jzlib.ZStream r4 = r14.i
            java.lang.String r5 = "unknown header flags set"
            r4.msg = r5
            r14.f3108a = r10
            goto L_0x0023
        L_0x03ad:
            int r4 = r14.j
            r4 = r4 & 512(0x200, float:7.175E-43)
            if (r4 == 0) goto L_0x03b8
            long r4 = r14.e
            r14.a(r3, r4)
        L_0x03b8:
            r4 = 16
            r14.f3108a = r4
        L_0x03bc:
            r4 = 4
            int r2 = r14.a(r4, r2, r0)     // Catch:{ g -> 0x04e5 }
            com.jcraft.jzlib.GZIPHeader r4 = r14.m
            if (r4 == 0) goto L_0x03cb
            com.jcraft.jzlib.GZIPHeader r4 = r14.m
            long r5 = r14.e
            r4.time = r5
        L_0x03cb:
            int r4 = r14.j
            r4 = r4 & 512(0x200, float:7.175E-43)
            if (r4 == 0) goto L_0x03d7
            r4 = 4
            long r5 = r14.e
            r14.a(r4, r5)
        L_0x03d7:
            r4 = 17
            r14.f3108a = r4
        L_0x03db:
            r4 = 2
            int r2 = r14.a(r4, r2, r0)     // Catch:{ g -> 0x04ea }
            com.jcraft.jzlib.GZIPHeader r4 = r14.m
            if (r4 == 0) goto L_0x03f8
            com.jcraft.jzlib.GZIPHeader r4 = r14.m
            long r5 = r14.e
            int r5 = (int) r5
            r5 = r5 & 255(0xff, float:3.57E-43)
            r4.xflags = r5
            com.jcraft.jzlib.GZIPHeader r4 = r14.m
            long r5 = r14.e
            int r5 = (int) r5
            int r5 = r5 >> 8
            r5 = r5 & 255(0xff, float:3.57E-43)
            r4.os = r5
        L_0x03f8:
            int r4 = r14.j
            r4 = r4 & 512(0x200, float:7.175E-43)
            if (r4 == 0) goto L_0x0403
            long r4 = r14.e
            r14.a(r3, r4)
        L_0x0403:
            r4 = 18
            r14.f3108a = r4
        L_0x0407:
            int r4 = r14.j
            r4 = r4 & 1024(0x400, float:1.435E-42)
            if (r4 == 0) goto L_0x04f4
            r4 = 2
            int r2 = r14.a(r4, r2, r0)     // Catch:{ g -> 0x04ef }
            com.jcraft.jzlib.GZIPHeader r4 = r14.m
            if (r4 == 0) goto L_0x0423
            com.jcraft.jzlib.GZIPHeader r4 = r14.m
            long r5 = r14.e
            int r5 = (int) r5
            r6 = 65535(0xffff, float:9.1834E-41)
            r5 = r5 & r6
            byte[] r5 = new byte[r5]
            r4.extra = r5
        L_0x0423:
            int r4 = r14.j
            r4 = r4 & 512(0x200, float:7.175E-43)
            if (r4 == 0) goto L_0x042e
            long r4 = r14.e
            r14.a(r3, r4)
        L_0x042e:
            r4 = 19
            r14.f3108a = r4
        L_0x0432:
            int r4 = r14.j
            r4 = r4 & 1024(0x400, float:1.435E-42)
            if (r4 == 0) goto L_0x0560
            java.io.ByteArrayOutputStream r4 = r14.o     // Catch:{ g -> 0x050a }
            if (r4 != 0) goto L_0x0443
            java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream     // Catch:{ g -> 0x050a }
            r4.<init>()     // Catch:{ g -> 0x050a }
            r14.o = r4     // Catch:{ g -> 0x050a }
        L_0x0443:
            long r4 = r14.e     // Catch:{ g -> 0x050a }
            r6 = 0
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 > 0) goto L_0x04fe
            com.jcraft.jzlib.GZIPHeader r4 = r14.m     // Catch:{ g -> 0x050a }
            if (r4 == 0) goto L_0x046a
            java.io.ByteArrayOutputStream r4 = r14.o     // Catch:{ g -> 0x050a }
            byte[] r4 = r4.toByteArray()     // Catch:{ g -> 0x050a }
            r5 = 0
            r14.o = r5     // Catch:{ g -> 0x050a }
            int r5 = r4.length     // Catch:{ g -> 0x050a }
            com.jcraft.jzlib.GZIPHeader r6 = r14.m     // Catch:{ g -> 0x050a }
            byte[] r6 = r6.extra     // Catch:{ g -> 0x050a }
            int r6 = r6.length     // Catch:{ g -> 0x050a }
            if (r5 != r6) goto L_0x0554
            r5 = 0
            com.jcraft.jzlib.GZIPHeader r6 = r14.m     // Catch:{ g -> 0x050a }
            byte[] r6 = r6.extra     // Catch:{ g -> 0x050a }
            r7 = 0
            int r8 = r4.length     // Catch:{ g -> 0x050a }
            java.lang.System.arraycopy(r4, r5, r6, r7, r8)     // Catch:{ g -> 0x050a }
        L_0x046a:
            r4 = 20
            r14.f3108a = r4
        L_0x046e:
            int r4 = r14.j
            r4 = r4 & 2048(0x800, float:2.87E-42)
            if (r4 == 0) goto L_0x056f
            int r2 = r14.a(r2, r0)     // Catch:{ g -> 0x056a }
            com.jcraft.jzlib.GZIPHeader r4 = r14.m     // Catch:{ g -> 0x056a }
            if (r4 == 0) goto L_0x0486
            com.jcraft.jzlib.GZIPHeader r4 = r14.m     // Catch:{ g -> 0x056a }
            java.io.ByteArrayOutputStream r5 = r14.o     // Catch:{ g -> 0x056a }
            byte[] r5 = r5.toByteArray()     // Catch:{ g -> 0x056a }
            r4.name = r5     // Catch:{ g -> 0x056a }
        L_0x0486:
            r4 = 0
            r14.o = r4     // Catch:{ g -> 0x056a }
        L_0x0489:
            r4 = 21
            r14.f3108a = r4
        L_0x048d:
            int r4 = r14.j
            r4 = r4 & 4096(0x1000, float:5.74E-42)
            if (r4 == 0) goto L_0x057e
            int r2 = r14.a(r2, r0)     // Catch:{ g -> 0x0579 }
            com.jcraft.jzlib.GZIPHeader r4 = r14.m     // Catch:{ g -> 0x0579 }
            if (r4 == 0) goto L_0x04a5
            com.jcraft.jzlib.GZIPHeader r4 = r14.m     // Catch:{ g -> 0x0579 }
            java.io.ByteArrayOutputStream r5 = r14.o     // Catch:{ g -> 0x0579 }
            byte[] r5 = r5.toByteArray()     // Catch:{ g -> 0x0579 }
            r4.comment = r5     // Catch:{ g -> 0x0579 }
        L_0x04a5:
            r4 = 0
            r14.o = r4     // Catch:{ g -> 0x0579 }
        L_0x04a8:
            r4 = 22
            r14.f3108a = r4
        L_0x04ac:
            int r4 = r14.j
            r4 = r4 & 512(0x200, float:7.175E-43)
            if (r4 == 0) goto L_0x058d
            r4 = 2
            int r2 = r14.a(r4, r2, r0)     // Catch:{ g -> 0x0588 }
            com.jcraft.jzlib.GZIPHeader r4 = r14.m
            if (r4 == 0) goto L_0x04c6
            com.jcraft.jzlib.GZIPHeader r4 = r14.m
            long r5 = r14.e
            r7 = 65535(0xffff, double:3.23786E-319)
            long r5 = r5 & r7
            int r5 = (int) r5
            r4.hcrc = r5
        L_0x04c6:
            long r4 = r14.e
            com.jcraft.jzlib.ZStream r6 = r14.i
            com.jcraft.jzlib.a r6 = r6.adler
            long r6 = r6.getValue()
            r8 = 65535(0xffff, double:3.23786E-319)
            long r6 = r6 & r8
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 == 0) goto L_0x058d
            r14.f3108a = r10
            com.jcraft.jzlib.ZStream r4 = r14.i
            java.lang.String r5 = "header crc mismatch"
            r4.msg = r5
            r4 = 5
            r14.f = r4
            goto L_0x0023
        L_0x04e5:
            r0 = move-exception
            int r2 = r0.f3109a
            goto L_0x001b
        L_0x04ea:
            r0 = move-exception
            int r2 = r0.f3109a
            goto L_0x001b
        L_0x04ef:
            r0 = move-exception
            int r2 = r0.f3109a
            goto L_0x001b
        L_0x04f4:
            com.jcraft.jzlib.GZIPHeader r4 = r14.m
            if (r4 == 0) goto L_0x042e
            com.jcraft.jzlib.GZIPHeader r4 = r14.m
            r4.extra = r13
            goto L_0x042e
        L_0x04fe:
            com.jcraft.jzlib.ZStream r4 = r14.i     // Catch:{ g -> 0x050a }
            int r4 = r4.avail_in     // Catch:{ g -> 0x050a }
            if (r4 != 0) goto L_0x050f
            com.jcraft.jzlib.g r0 = new com.jcraft.jzlib.g     // Catch:{ g -> 0x050a }
            r0.<init>(r2)     // Catch:{ g -> 0x050a }
            throw r0     // Catch:{ g -> 0x050a }
        L_0x050a:
            r0 = move-exception
            int r2 = r0.f3109a
            goto L_0x001b
        L_0x050f:
            com.jcraft.jzlib.ZStream r2 = r14.i     // Catch:{ g -> 0x050a }
            int r4 = r2.avail_in     // Catch:{ g -> 0x050a }
            int r4 = r4 + -1
            r2.avail_in = r4     // Catch:{ g -> 0x050a }
            com.jcraft.jzlib.ZStream r2 = r14.i     // Catch:{ g -> 0x050a }
            long r4 = r2.total_in     // Catch:{ g -> 0x050a }
            long r4 = r4 + r11
            r2.total_in = r4     // Catch:{ g -> 0x050a }
            com.jcraft.jzlib.ZStream r2 = r14.i     // Catch:{ g -> 0x050a }
            byte[] r2 = r2.next_in     // Catch:{ g -> 0x050a }
            com.jcraft.jzlib.ZStream r2 = r14.i     // Catch:{ g -> 0x050a }
            int r2 = r2.next_in_index     // Catch:{ g -> 0x050a }
            java.io.ByteArrayOutputStream r2 = r14.o     // Catch:{ g -> 0x050a }
            com.jcraft.jzlib.ZStream r4 = r14.i     // Catch:{ g -> 0x050a }
            byte[] r4 = r4.next_in     // Catch:{ g -> 0x050a }
            com.jcraft.jzlib.ZStream r5 = r14.i     // Catch:{ g -> 0x050a }
            int r5 = r5.next_in_index     // Catch:{ g -> 0x050a }
            r6 = 1
            r2.write(r4, r5, r6)     // Catch:{ g -> 0x050a }
            com.jcraft.jzlib.ZStream r2 = r14.i     // Catch:{ g -> 0x050a }
            com.jcraft.jzlib.a r2 = r2.adler     // Catch:{ g -> 0x050a }
            com.jcraft.jzlib.ZStream r4 = r14.i     // Catch:{ g -> 0x050a }
            byte[] r4 = r4.next_in     // Catch:{ g -> 0x050a }
            com.jcraft.jzlib.ZStream r5 = r14.i     // Catch:{ g -> 0x050a }
            int r5 = r5.next_in_index     // Catch:{ g -> 0x050a }
            r6 = 1
            r2.update(r4, r5, r6)     // Catch:{ g -> 0x050a }
            com.jcraft.jzlib.ZStream r2 = r14.i     // Catch:{ g -> 0x050a }
            int r4 = r2.next_in_index     // Catch:{ g -> 0x050a }
            int r4 = r4 + 1
            r2.next_in_index = r4     // Catch:{ g -> 0x050a }
            long r4 = r14.e     // Catch:{ g -> 0x050a }
            long r4 = r4 - r11
            r14.e = r4     // Catch:{ g -> 0x050a }
            r2 = r0
            goto L_0x0443
        L_0x0554:
            com.jcraft.jzlib.ZStream r4 = r14.i     // Catch:{ g -> 0x050a }
            java.lang.String r5 = "bad extra field length"
            r4.msg = r5     // Catch:{ g -> 0x050a }
            r4 = 13
            r14.f3108a = r4     // Catch:{ g -> 0x050a }
            goto L_0x0023
        L_0x0560:
            com.jcraft.jzlib.GZIPHeader r4 = r14.m
            if (r4 == 0) goto L_0x046a
            com.jcraft.jzlib.GZIPHeader r4 = r14.m
            r4.extra = r13
            goto L_0x046a
        L_0x056a:
            r0 = move-exception
            int r2 = r0.f3109a
            goto L_0x001b
        L_0x056f:
            com.jcraft.jzlib.GZIPHeader r4 = r14.m
            if (r4 == 0) goto L_0x0489
            com.jcraft.jzlib.GZIPHeader r4 = r14.m
            r4.name = r13
            goto L_0x0489
        L_0x0579:
            r0 = move-exception
            int r2 = r0.f3109a
            goto L_0x001b
        L_0x057e:
            com.jcraft.jzlib.GZIPHeader r4 = r14.m
            if (r4 == 0) goto L_0x04a8
            com.jcraft.jzlib.GZIPHeader r4 = r14.m
            r4.comment = r13
            goto L_0x04a8
        L_0x0588:
            r0 = move-exception
            int r2 = r0.f3109a
            goto L_0x001b
        L_0x058d:
            com.jcraft.jzlib.ZStream r4 = r14.i
            com.jcraft.jzlib.CRC32 r5 = new com.jcraft.jzlib.CRC32
            r5.<init>()
            r4.adler = r5
            r4 = 7
            r14.f3108a = r4
            goto L_0x0023
        L_0x059b:
            r0 = r2
            goto L_0x0169
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jcraft.jzlib.f.b(int):int");
    }

    /* access modifiers changed from: package-private */
    public final int c() {
        if (this.i == null || this.h == null) {
            return -2;
        }
        return this.h.c();
    }

    public final GZIPHeader d() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public final boolean e() {
        switch (this.f3108a) {
            case 2:
            case 3:
            case 4:
            case 5:
            case h.DragSortListView_drag_handle_id:
            case 16:
            case 17:
            case 18:
            case 19:
            case VMapProjection.MAXZOOMLEVEL:
            case 21:
            case Constants.WEIBO_SDK_VERSION /*22*/:
            case 23:
                return true;
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 15:
            default:
                return false;
        }
    }
}
