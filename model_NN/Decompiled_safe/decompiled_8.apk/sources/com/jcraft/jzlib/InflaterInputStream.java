package com.jcraft.jzlib;

import com.amap.mapapi.poisearch.PoiTypeDef;
import java.io.EOFException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class InflaterInputStream extends FilterInputStream {
    protected static final int DEFAULT_BUFSIZE = 512;
    private byte[] b;
    protected byte[] buf;
    private byte[] byte1;
    private boolean close_in;
    private boolean closed;
    private boolean eof;
    protected final Inflater inflater;
    protected boolean myinflater;

    public InflaterInputStream(InputStream inputStream) {
        this(inputStream, new Inflater());
        this.myinflater = true;
    }

    public InflaterInputStream(InputStream inputStream, Inflater inflater2) {
        this(inputStream, inflater2, 512);
    }

    public InflaterInputStream(InputStream inputStream, Inflater inflater2, int i) {
        this(inputStream, inflater2, i, true);
    }

    public InflaterInputStream(InputStream inputStream, Inflater inflater2, int i, boolean z) {
        super(inputStream);
        this.closed = false;
        this.eof = false;
        this.close_in = true;
        this.myinflater = false;
        this.byte1 = new byte[1];
        this.b = new byte[512];
        if (inputStream == null || inflater2 == null) {
            throw new NullPointerException();
        } else if (i <= 0) {
            throw new IllegalArgumentException("buffer size must be greater than 0");
        } else {
            this.inflater = inflater2;
            this.buf = new byte[i];
            this.close_in = z;
        }
    }

    public int available() {
        if (!this.closed) {
            return this.eof ? 0 : 1;
        }
        throw new IOException("Stream closed");
    }

    public void close() {
        if (!this.closed) {
            if (this.myinflater) {
                this.inflater.end();
            }
            if (this.close_in) {
                this.in.close();
            }
            this.closed = true;
        }
    }

    /* access modifiers changed from: protected */
    public void fill() {
        if (this.closed) {
            throw new IOException("Stream closed");
        }
        int read = this.in.read(this.buf, 0, this.buf.length);
        if (read == -1) {
            if (this.inflater.istate.c == 0 && !this.inflater.finished()) {
                this.buf[0] = 0;
                read = 1;
            } else if (this.inflater.istate.b != -1) {
                throw new IOException("footer is not found");
            } else {
                throw new EOFException("Unexpected end of ZLIB input stream");
            }
        }
        this.inflater.setInput(this.buf, 0, read, true);
    }

    public byte[] getAvailIn() {
        if (this.inflater.avail_in <= 0) {
            return null;
        }
        byte[] bArr = new byte[this.inflater.avail_in];
        System.arraycopy(this.inflater.next_in, this.inflater.next_in_index, bArr, 0, this.inflater.avail_in);
        return bArr;
    }

    public Inflater getInflater() {
        return this.inflater;
    }

    public long getTotalIn() {
        return this.inflater.getTotalIn();
    }

    public long getTotalOut() {
        return this.inflater.getTotalOut();
    }

    public synchronized void mark(int i) {
    }

    public boolean markSupported() {
        return false;
    }

    public int read() {
        if (this.closed) {
            throw new IOException("Stream closed");
        } else if (read(this.byte1, 0, 1) == -1) {
            return -1;
        } else {
            return this.byte1[0] & GZIPHeader.OS_UNKNOWN;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public int read(byte[] bArr, int i, int i2) {
        if (this.closed) {
            throw new IOException("Stream closed");
        } else if (bArr == null) {
            throw new NullPointerException();
        } else if (i < 0 || i2 < 0 || i2 > bArr.length - i) {
            throw new IndexOutOfBoundsException();
        } else if (i2 == 0) {
            return 0;
        } else {
            if (this.eof) {
                return -1;
            }
            this.inflater.setOutput(bArr, i, i2);
            int i3 = 0;
            while (!this.eof) {
                if (this.inflater.avail_in == 0) {
                    fill();
                }
                int inflate = this.inflater.inflate(0);
                i3 += this.inflater.next_out_index - i;
                i = this.inflater.next_out_index;
                switch (inflate) {
                    case JZlib.Z_DATA_ERROR /*-3*/:
                        throw new IOException(this.inflater.msg);
                    case 1:
                    case 2:
                        this.eof = true;
                        if (inflate == 2) {
                            return -1;
                        }
                        break;
                }
                if (this.inflater.avail_out == 0) {
                    return i3;
                }
            }
            return i3;
        }
    }

    public void readHeader() {
        byte[] bytes = PoiTypeDef.All.getBytes();
        this.inflater.setInput(bytes, 0, 0, false);
        this.inflater.setOutput(bytes, 0, 0);
        this.inflater.inflate(0);
        if (this.inflater.istate.e()) {
            byte[] bArr = new byte[1];
            while (this.in.read(bArr) > 0) {
                this.inflater.setInput(bArr);
                if (this.inflater.inflate(0) != 0) {
                    throw new IOException(this.inflater.msg);
                } else if (!this.inflater.istate.e()) {
                    return;
                }
            }
            throw new IOException("no input");
        }
    }

    public synchronized void reset() {
        throw new IOException("mark/reset not supported");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public long skip(long j) {
        if (j < 0) {
            throw new IllegalArgumentException("negative skip length");
        } else if (this.closed) {
            throw new IOException("Stream closed");
        } else {
            int min = (int) Math.min(j, 2147483647L);
            int i = 0;
            while (true) {
                if (i >= min) {
                    break;
                }
                int i2 = min - i;
                if (i2 > this.b.length) {
                    i2 = this.b.length;
                }
                int read = read(this.b, 0, i2);
                if (read == -1) {
                    this.eof = true;
                    break;
                }
                i = read + i;
            }
            return (long) i;
        }
    }
}
