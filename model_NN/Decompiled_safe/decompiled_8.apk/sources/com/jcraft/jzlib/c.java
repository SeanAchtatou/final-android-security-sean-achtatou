package com.jcraft.jzlib;

final class c {
    private static final int[] g;
    private static int[] h;
    private final ZStream A;

    /* renamed from: a  reason: collision with root package name */
    int f3105a;
    int b;
    byte[] c;
    int d;
    int e;
    int f;
    private int i;
    private int j;
    private int k;
    private int l;
    private int[] m;
    private int[] n = new int[1];
    private int[] o = new int[1];
    private int[] p = new int[1];
    private int[] q = new int[1];
    private int[][] r = new int[1][];
    private int[][] s = new int[1][];
    private int[] t = new int[1];
    private int[] u = new int[1];
    private final d v;
    private int w;
    private int[] x;
    private boolean y;
    private final e z = new e();

    static {
        int[] iArr = new int[17];
        iArr[1] = 1;
        iArr[2] = 3;
        iArr[3] = 7;
        iArr[4] = 15;
        iArr[5] = 31;
        iArr[6] = 63;
        iArr[7] = 127;
        iArr[8] = 255;
        iArr[9] = 511;
        iArr[10] = 1023;
        iArr[11] = 2047;
        iArr[12] = 4095;
        iArr[13] = 8191;
        iArr[14] = 16383;
        iArr[15] = 32767;
        iArr[16] = 65535;
        g = iArr;
        int[] iArr2 = new int[19];
        iArr2[0] = 16;
        iArr2[1] = 17;
        iArr2[2] = 18;
        iArr2[4] = 8;
        iArr2[5] = 7;
        iArr2[6] = 9;
        iArr2[7] = 6;
        iArr2[8] = 10;
        iArr2[9] = 5;
        iArr2[10] = 11;
        iArr2[11] = 4;
        iArr2[12] = 12;
        iArr2[13] = 3;
        iArr2[14] = 13;
        iArr2[15] = 2;
        iArr2[16] = 14;
        iArr2[17] = 1;
        iArr2[18] = 15;
        h = iArr2;
    }

    c(ZStream zStream, int i2) {
        boolean z2 = true;
        this.A = zStream;
        this.v = new d(this.A, this);
        this.x = new int[4320];
        this.c = new byte[i2];
        this.d = i2;
        this.y = zStream.istate.c == 0 ? false : z2;
        this.i = 0;
        a();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:169:0x0820, code lost:
        r0.f = r2;
        r1 = b(r17);
        r11 = r0.f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x0830, code lost:
        if (r11 >= r0.e) goto L_0x0872;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:171:0x0832, code lost:
        r2 = r0.e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:173:0x083e, code lost:
        if (r0.e == r0.f) goto L_0x0877;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:174:0x0840, code lost:
        r0.b = r6;
        r0.f3105a = r5;
        r0.A.avail_in = r3;
        r0.A.total_in += (long) (r4 - r0.A.next_in_index);
        r0.A.next_in_index = r4;
        r0.f = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:175:0x0872, code lost:
        r2 = r0.d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:176:0x0877, code lost:
        r0.i = 8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:177:0x087d, code lost:
        r0.b = r6;
        r0.f3105a = r5;
        r0.A.avail_in = r3;
        r0.A.total_in += (long) (r4 - r0.A.next_in_index);
        r0.A.next_in_index = r4;
        r0.f = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:248:?, code lost:
        return b(-3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:252:?, code lost:
        return b(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:253:?, code lost:
        return b(1);
     */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x0540  */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x07d2  */
    /* JADX WARNING: Removed duplicated region for block: B:180:0x08e6  */
    /* JADX WARNING: Removed duplicated region for block: B:210:0x07ca A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:222:0x0423 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:228:0x06d6 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int a(int r17) {
        /*
            r16 = this;
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            int r4 = r1.next_in_index
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            int r3 = r1.avail_in
            r0 = r16
            int r6 = r0.b
            r0 = r16
            int r5 = r0.f3105a
            r0 = r16
            int r2 = r0.f
            r0 = r16
            int r1 = r0.e
            if (r2 >= r1) goto L_0x0062
            r0 = r16
            int r1 = r0.e
            int r1 = r1 - r2
            int r1 = r1 + -1
        L_0x0025:
            r8 = r1
            r11 = r2
            r1 = r3
            r2 = r4
        L_0x0029:
            r0 = r16
            int r3 = r0.i
            switch(r3) {
                case 0: goto L_0x0068;
                case 1: goto L_0x0193;
                case 2: goto L_0x0238;
                case 3: goto L_0x035e;
                case 4: goto L_0x0411;
                case 5: goto L_0x08f6;
                case 6: goto L_0x08fc;
                case 7: goto L_0x0902;
                case 8: goto L_0x0907;
                case 9: goto L_0x08b0;
                default: goto L_0x0030;
            }
        L_0x0030:
            r0 = r16
            r0.b = r6
            r0 = r16
            r0.f3105a = r5
            r0 = r16
            com.jcraft.jzlib.ZStream r3 = r0.A
            r3.avail_in = r1
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            long r3 = r1.total_in
            r0 = r16
            com.jcraft.jzlib.ZStream r5 = r0.A
            int r5 = r5.next_in_index
            int r5 = r2 - r5
            long r5 = (long) r5
            long r3 = r3 + r5
            r1.total_in = r3
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            r1.next_in_index = r2
            r0 = r16
            r0.f = r11
            r1 = -2
            r0 = r16
            int r1 = r0.b(r1)
        L_0x0061:
            return r1
        L_0x0062:
            r0 = r16
            int r1 = r0.d
            int r1 = r1 - r2
            goto L_0x0025
        L_0x0068:
            r9 = r1
            r10 = r2
            r12 = r5
            r13 = r6
        L_0x006c:
            r1 = 3
            if (r12 < r1) goto L_0x0081
            r1 = r13 & 7
            r2 = r1 & 1
            r0 = r16
            r0.w = r2
            int r1 = r1 >>> 1
            switch(r1) {
                case 0: goto L_0x00c8;
                case 1: goto L_0x00dd;
                case 2: goto L_0x0128;
                case 3: goto L_0x0137;
                default: goto L_0x007c;
            }
        L_0x007c:
            r1 = r9
            r2 = r10
            r5 = r12
            r6 = r13
            goto L_0x0029
        L_0x0081:
            if (r9 == 0) goto L_0x0099
            r17 = 0
            int r9 = r9 + -1
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            byte[] r1 = r1.next_in
            int r2 = r10 + 1
            byte r1 = r1[r10]
            r1 = r1 & 255(0xff, float:3.57E-43)
            int r1 = r1 << r12
            r13 = r13 | r1
            int r12 = r12 + 8
            r10 = r2
            goto L_0x006c
        L_0x0099:
            r0 = r16
            r0.b = r13
            r0 = r16
            r0.f3105a = r12
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            r1.avail_in = r9
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            long r2 = r1.total_in
            r0 = r16
            com.jcraft.jzlib.ZStream r4 = r0.A
            int r4 = r4.next_in_index
            int r4 = r10 - r4
            long r4 = (long) r4
            long r2 = r2 + r4
            r1.total_in = r2
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            r1.next_in_index = r10
            r0 = r16
            r0.f = r11
            int r1 = r16.b(r17)
            goto L_0x0061
        L_0x00c8:
            int r1 = r13 >>> 3
            int r2 = r12 + -3
            r4 = r2 & 7
            int r3 = r1 >>> r4
            int r1 = r2 - r4
            r2 = 1
            r0 = r16
            r0.i = r2
            r2 = r10
            r5 = r1
            r6 = r3
            r1 = r9
            goto L_0x0029
        L_0x00dd:
            r0 = r16
            int[] r1 = r0.p
            r0 = r16
            int[] r2 = r0.q
            r0 = r16
            int[][] r3 = r0.r
            r0 = r16
            int[][] r4 = r0.s
            r0 = r16
            com.jcraft.jzlib.ZStream r5 = r0.A
            com.jcraft.jzlib.e.a(r1, r2, r3, r4)
            r0 = r16
            com.jcraft.jzlib.d r1 = r0.v
            r0 = r16
            int[] r2 = r0.p
            r3 = 0
            r2 = r2[r3]
            r0 = r16
            int[] r3 = r0.q
            r4 = 0
            r3 = r3[r4]
            r0 = r16
            int[][] r4 = r0.r
            r5 = 0
            r4 = r4[r5]
            r5 = 0
            r0 = r16
            int[][] r6 = r0.s
            r7 = 0
            r6 = r6[r7]
            r7 = 0
            r1.a(r2, r3, r4, r5, r6, r7)
            int r3 = r13 >>> 3
            int r1 = r12 + -3
            r2 = 6
            r0 = r16
            r0.i = r2
            r2 = r10
            r5 = r1
            r6 = r3
            r1 = r9
            goto L_0x0029
        L_0x0128:
            int r3 = r13 >>> 3
            int r1 = r12 + -3
            r2 = 3
            r0 = r16
            r0.i = r2
            r2 = r10
            r5 = r1
            r6 = r3
            r1 = r9
            goto L_0x0029
        L_0x0137:
            int r1 = r13 >>> 3
            int r2 = r12 + -3
            r3 = 9
            r0 = r16
            r0.i = r3
            r0 = r16
            com.jcraft.jzlib.ZStream r3 = r0.A
            java.lang.String r4 = "invalid block type"
            r3.msg = r4
            r0 = r16
            r0.b = r1
            r0 = r16
            r0.f3105a = r2
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            r1.avail_in = r9
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            long r2 = r1.total_in
            r0 = r16
            com.jcraft.jzlib.ZStream r4 = r0.A
            int r4 = r4.next_in_index
            int r4 = r10 - r4
            long r4 = (long) r4
            long r2 = r2 + r4
            r1.total_in = r2
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            r1.next_in_index = r10
            r0 = r16
            r0.f = r11
            r1 = -3
            r0 = r16
            int r1 = r0.b(r1)
            goto L_0x0061
        L_0x017c:
            if (r1 == 0) goto L_0x01e6
            r17 = 0
            int r1 = r1 + -1
            r0 = r16
            com.jcraft.jzlib.ZStream r3 = r0.A
            byte[] r4 = r3.next_in
            int r3 = r2 + 1
            byte r2 = r4[r2]
            r2 = r2 & 255(0xff, float:3.57E-43)
            int r2 = r2 << r5
            r6 = r6 | r2
            int r5 = r5 + 8
            r2 = r3
        L_0x0193:
            r3 = 32
            if (r5 < r3) goto L_0x017c
            r3 = r6 ^ -1
            int r3 = r3 >>> 16
            r4 = 65535(0xffff, float:9.1834E-41)
            r3 = r3 & r4
            r4 = 65535(0xffff, float:9.1834E-41)
            r4 = r4 & r6
            if (r3 == r4) goto L_0x0216
            r3 = 9
            r0 = r16
            r0.i = r3
            r0 = r16
            com.jcraft.jzlib.ZStream r3 = r0.A
            java.lang.String r4 = "invalid stored block lengths"
            r3.msg = r4
            r0 = r16
            r0.b = r6
            r0 = r16
            r0.f3105a = r5
            r0 = r16
            com.jcraft.jzlib.ZStream r3 = r0.A
            r3.avail_in = r1
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            long r3 = r1.total_in
            r0 = r16
            com.jcraft.jzlib.ZStream r5 = r0.A
            int r5 = r5.next_in_index
            int r5 = r2 - r5
            long r5 = (long) r5
            long r3 = r3 + r5
            r1.total_in = r3
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            r1.next_in_index = r2
            r0 = r16
            r0.f = r11
            r1 = -3
            r0 = r16
            int r1 = r0.b(r1)
            goto L_0x0061
        L_0x01e6:
            r0 = r16
            r0.b = r6
            r0 = r16
            r0.f3105a = r5
            r0 = r16
            com.jcraft.jzlib.ZStream r3 = r0.A
            r3.avail_in = r1
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            long r3 = r1.total_in
            r0 = r16
            com.jcraft.jzlib.ZStream r5 = r0.A
            int r5 = r5.next_in_index
            int r5 = r2 - r5
            long r5 = (long) r5
            long r3 = r3 + r5
            r1.total_in = r3
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            r1.next_in_index = r2
            r0 = r16
            r0.f = r11
            int r1 = r16.b(r17)
            goto L_0x0061
        L_0x0216:
            r3 = 65535(0xffff, float:9.1834E-41)
            r3 = r3 & r6
            r0 = r16
            r0.j = r3
            r3 = 0
            r0 = r16
            int r4 = r0.j
            if (r4 == 0) goto L_0x022e
            r4 = 2
        L_0x0226:
            r0 = r16
            r0.i = r4
            r5 = r3
            r6 = r3
            goto L_0x0029
        L_0x022e:
            r0 = r16
            int r4 = r0.w
            if (r4 == 0) goto L_0x0236
            r4 = 7
            goto L_0x0226
        L_0x0236:
            r4 = 0
            goto L_0x0226
        L_0x0238:
            if (r1 != 0) goto L_0x026a
            r0 = r16
            r0.b = r6
            r0 = r16
            r0.f3105a = r5
            r0 = r16
            com.jcraft.jzlib.ZStream r3 = r0.A
            r3.avail_in = r1
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            long r3 = r1.total_in
            r0 = r16
            com.jcraft.jzlib.ZStream r5 = r0.A
            int r5 = r5.next_in_index
            int r5 = r2 - r5
            long r5 = (long) r5
            long r3 = r3 + r5
            r1.total_in = r3
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            r1.next_in_index = r2
            r0 = r16
            r0.f = r11
            int r1 = r16.b(r17)
            goto L_0x0061
        L_0x026a:
            if (r8 != 0) goto L_0x0307
            r0 = r16
            int r3 = r0.d
            if (r11 != r3) goto L_0x0288
            r0 = r16
            int r3 = r0.e
            if (r3 == 0) goto L_0x0288
            r11 = 0
            r0 = r16
            int r3 = r0.e
            if (r3 <= 0) goto L_0x02f3
            r0 = r16
            int r3 = r0.e
            int r3 = r3 + 0
            int r3 = r3 + -1
        L_0x0287:
            r8 = r3
        L_0x0288:
            if (r8 != 0) goto L_0x0307
            r0 = r16
            r0.f = r11
            int r4 = r16.b(r17)
            r0 = r16
            int r11 = r0.f
            r0 = r16
            int r3 = r0.e
            if (r11 >= r3) goto L_0x02fa
            r0 = r16
            int r3 = r0.e
            int r3 = r3 - r11
            int r3 = r3 + -1
        L_0x02a3:
            r0 = r16
            int r7 = r0.d
            if (r11 != r7) goto L_0x08f3
            r0 = r16
            int r7 = r0.e
            if (r7 == 0) goto L_0x08f3
            r11 = 0
            r0 = r16
            int r3 = r0.e
            if (r3 <= 0) goto L_0x0300
            r0 = r16
            int r3 = r0.e
            int r3 = r3 + 0
            int r3 = r3 + -1
        L_0x02be:
            r8 = r3
        L_0x02bf:
            if (r8 != 0) goto L_0x0307
            r0 = r16
            r0.b = r6
            r0 = r16
            r0.f3105a = r5
            r0 = r16
            com.jcraft.jzlib.ZStream r3 = r0.A
            r3.avail_in = r1
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            long r5 = r1.total_in
            r0 = r16
            com.jcraft.jzlib.ZStream r3 = r0.A
            int r3 = r3.next_in_index
            int r3 = r2 - r3
            long r7 = (long) r3
            long r5 = r5 + r7
            r1.total_in = r5
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            r1.next_in_index = r2
            r0 = r16
            r0.f = r11
            r0 = r16
            int r1 = r0.b(r4)
            goto L_0x0061
        L_0x02f3:
            r0 = r16
            int r3 = r0.d
            int r3 = r3 + 0
            goto L_0x0287
        L_0x02fa:
            r0 = r16
            int r3 = r0.d
            int r3 = r3 - r11
            goto L_0x02a3
        L_0x0300:
            r0 = r16
            int r3 = r0.d
            int r3 = r3 + 0
            goto L_0x02be
        L_0x0307:
            r17 = 0
            r0 = r16
            int r3 = r0.j
            if (r3 <= r1) goto L_0x0310
            r3 = r1
        L_0x0310:
            if (r3 <= r8) goto L_0x08f0
            r7 = r8
        L_0x0313:
            r0 = r16
            com.jcraft.jzlib.ZStream r3 = r0.A
            byte[] r3 = r3.next_in
            r0 = r16
            byte[] r4 = r0.c
            java.lang.System.arraycopy(r3, r2, r4, r11, r7)
            int r4 = r2 + r7
            int r3 = r1 - r7
            int r2 = r11 + r7
            int r1 = r8 - r7
            r0 = r16
            int r8 = r0.j
            int r7 = r8 - r7
            r0 = r16
            r0.j = r7
            if (r7 != 0) goto L_0x08ea
            r0 = r16
            int r7 = r0.w
            if (r7 == 0) goto L_0x0345
            r7 = 7
        L_0x033b:
            r0 = r16
            r0.i = r7
            r8 = r1
            r11 = r2
            r1 = r3
            r2 = r4
            goto L_0x0029
        L_0x0345:
            r7 = 0
            goto L_0x033b
        L_0x0347:
            if (r1 == 0) goto L_0x03b7
            r17 = 0
            int r1 = r1 + -1
            r0 = r16
            com.jcraft.jzlib.ZStream r3 = r0.A
            byte[] r4 = r3.next_in
            int r3 = r2 + 1
            byte r2 = r4[r2]
            r2 = r2 & 255(0xff, float:3.57E-43)
            int r2 = r2 << r5
            r6 = r6 | r2
            int r5 = r5 + 8
            r2 = r3
        L_0x035e:
            r3 = 14
            if (r5 < r3) goto L_0x0347
            r3 = r6 & 16383(0x3fff, float:2.2957E-41)
            r0 = r16
            r0.k = r3
            r4 = r3 & 31
            r7 = 29
            if (r4 > r7) goto L_0x0376
            int r4 = r3 >> 5
            r4 = r4 & 31
            r7 = 29
            if (r4 <= r7) goto L_0x03e7
        L_0x0376:
            r3 = 9
            r0 = r16
            r0.i = r3
            r0 = r16
            com.jcraft.jzlib.ZStream r3 = r0.A
            java.lang.String r4 = "too many length or distance symbols"
            r3.msg = r4
            r0 = r16
            r0.b = r6
            r0 = r16
            r0.f3105a = r5
            r0 = r16
            com.jcraft.jzlib.ZStream r3 = r0.A
            r3.avail_in = r1
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            long r3 = r1.total_in
            r0 = r16
            com.jcraft.jzlib.ZStream r5 = r0.A
            int r5 = r5.next_in_index
            int r5 = r2 - r5
            long r5 = (long) r5
            long r3 = r3 + r5
            r1.total_in = r3
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            r1.next_in_index = r2
            r0 = r16
            r0.f = r11
            r1 = -3
            r0 = r16
            int r1 = r0.b(r1)
            goto L_0x0061
        L_0x03b7:
            r0 = r16
            r0.b = r6
            r0 = r16
            r0.f3105a = r5
            r0 = r16
            com.jcraft.jzlib.ZStream r3 = r0.A
            r3.avail_in = r1
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            long r3 = r1.total_in
            r0 = r16
            com.jcraft.jzlib.ZStream r5 = r0.A
            int r5 = r5.next_in_index
            int r5 = r2 - r5
            long r5 = (long) r5
            long r3 = r3 + r5
            r1.total_in = r3
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            r1.next_in_index = r2
            r0 = r16
            r0.f = r11
            int r1 = r16.b(r17)
            goto L_0x0061
        L_0x03e7:
            r4 = r3 & 31
            int r4 = r4 + 258
            int r3 = r3 >> 5
            r3 = r3 & 31
            int r4 = r4 + r3
            r0 = r16
            int[] r3 = r0.m
            if (r3 == 0) goto L_0x03fd
            r0 = r16
            int[] r3 = r0.m
            int r3 = r3.length
            if (r3 >= r4) goto L_0x0491
        L_0x03fd:
            int[] r3 = new int[r4]
            r0 = r16
            r0.m = r3
        L_0x0403:
            int r6 = r6 >>> 14
            int r5 = r5 + -14
            r3 = 0
            r0 = r16
            r0.l = r3
            r3 = 4
            r0 = r16
            r0.i = r3
        L_0x0411:
            r7 = r1
            r8 = r2
            r9 = r5
            r10 = r6
        L_0x0415:
            r0 = r16
            int r1 = r0.l
            r0 = r16
            int r2 = r0.k
            int r2 = r2 >>> 10
            int r2 = r2 + 4
            if (r1 < r2) goto L_0x08e6
        L_0x0423:
            r0 = r16
            int r1 = r0.l
            r2 = 19
            if (r1 < r2) goto L_0x0508
            r0 = r16
            int[] r1 = r0.n
            r2 = 0
            r3 = 7
            r1[r2] = r3
            r0 = r16
            com.jcraft.jzlib.e r1 = r0.z
            r0 = r16
            int[] r2 = r0.m
            r0 = r16
            int[] r3 = r0.n
            r0 = r16
            int[] r4 = r0.o
            r0 = r16
            int[] r5 = r0.x
            r0 = r16
            com.jcraft.jzlib.ZStream r6 = r0.A
            int r1 = r1.a(r2, r3, r4, r5, r6)
            if (r1 == 0) goto L_0x051f
            r2 = -3
            if (r1 != r2) goto L_0x045f
            r2 = 0
            r0 = r16
            r0.m = r2
            r2 = 9
            r0 = r16
            r0.i = r2
        L_0x045f:
            r0 = r16
            r0.b = r10
            r0 = r16
            r0.f3105a = r9
            r0 = r16
            com.jcraft.jzlib.ZStream r2 = r0.A
            r2.avail_in = r7
            r0 = r16
            com.jcraft.jzlib.ZStream r2 = r0.A
            long r3 = r2.total_in
            r0 = r16
            com.jcraft.jzlib.ZStream r5 = r0.A
            int r5 = r5.next_in_index
            int r5 = r8 - r5
            long r5 = (long) r5
            long r3 = r3 + r5
            r2.total_in = r3
            r0 = r16
            com.jcraft.jzlib.ZStream r2 = r0.A
            r2.next_in_index = r8
            r0 = r16
            r0.f = r11
            r0 = r16
            int r1 = r0.b(r1)
            goto L_0x0061
        L_0x0491:
            r3 = 0
        L_0x0492:
            if (r3 >= r4) goto L_0x0403
            r0 = r16
            int[] r7 = r0.m
            r8 = 0
            r7[r3] = r8
            int r3 = r3 + 1
            goto L_0x0492
        L_0x049e:
            if (r1 == 0) goto L_0x04d8
            r17 = 0
            int r1 = r1 + -1
            r0 = r16
            com.jcraft.jzlib.ZStream r3 = r0.A
            byte[] r3 = r3.next_in
            int r8 = r2 + 1
            byte r2 = r3[r2]
            r2 = r2 & 255(0xff, float:3.57E-43)
            int r2 = r2 << r9
            r10 = r10 | r2
            int r9 = r9 + 8
            r2 = r8
        L_0x04b5:
            r3 = 3
            if (r9 < r3) goto L_0x049e
            r0 = r16
            int[] r3 = r0.m
            int[] r4 = com.jcraft.jzlib.c.h
            r0 = r16
            int r5 = r0.l
            int r6 = r5 + 1
            r0 = r16
            r0.l = r6
            r4 = r4[r5]
            r5 = r10 & 7
            r3[r4] = r5
            int r6 = r10 >>> 3
            int r5 = r9 + -3
            r7 = r1
            r8 = r2
            r9 = r5
            r10 = r6
            goto L_0x0415
        L_0x04d8:
            r0 = r16
            r0.b = r10
            r0 = r16
            r0.f3105a = r9
            r0 = r16
            com.jcraft.jzlib.ZStream r3 = r0.A
            r3.avail_in = r1
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            long r3 = r1.total_in
            r0 = r16
            com.jcraft.jzlib.ZStream r5 = r0.A
            int r5 = r5.next_in_index
            int r5 = r2 - r5
            long r5 = (long) r5
            long r3 = r3 + r5
            r1.total_in = r3
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            r1.next_in_index = r2
            r0 = r16
            r0.f = r11
            int r1 = r16.b(r17)
            goto L_0x0061
        L_0x0508:
            r0 = r16
            int[] r1 = r0.m
            int[] r2 = com.jcraft.jzlib.c.h
            r0 = r16
            int r3 = r0.l
            int r4 = r3 + 1
            r0 = r16
            r0.l = r4
            r2 = r2[r3]
            r3 = 0
            r1[r2] = r3
            goto L_0x0423
        L_0x051f:
            r1 = 0
            r0 = r16
            r0.l = r1
            r1 = 5
            r0 = r16
            r0.i = r1
            r12 = r7
            r13 = r8
            r14 = r9
            r15 = r10
        L_0x052d:
            r0 = r16
            int r1 = r0.k
            r0 = r16
            int r2 = r0.l
            r3 = r1 & 31
            int r3 = r3 + 258
            int r1 = r1 >> 5
            r1 = r1 & 31
            int r1 = r1 + r3
            if (r2 >= r1) goto L_0x06d6
            r0 = r16
            int[] r1 = r0.n
            r2 = 0
            r1 = r1[r2]
            r7 = r12
            r8 = r13
            r2 = r14
            r3 = r15
        L_0x054b:
            if (r2 < r1) goto L_0x059c
            r0 = r16
            int[] r4 = r0.o
            r0 = r16
            int[] r4 = r0.x
            r0 = r16
            int[] r5 = r0.o
            r6 = 0
            r5 = r5[r6]
            int[] r6 = com.jcraft.jzlib.c.g
            r1 = r6[r1]
            r1 = r1 & r3
            int r1 = r1 + r5
            int r1 = r1 * 3
            int r1 = r1 + 1
            r6 = r4[r1]
            r0 = r16
            int[] r1 = r0.x
            r0 = r16
            int[] r4 = r0.o
            r5 = 0
            r4 = r4[r5]
            int[] r5 = com.jcraft.jzlib.c.g
            r5 = r5[r6]
            r5 = r5 & r3
            int r4 = r4 + r5
            int r4 = r4 * 3
            int r4 = r4 + 2
            r12 = r1[r4]
            r1 = 16
            if (r12 >= r1) goto L_0x05e4
            int r10 = r3 >>> r6
            int r9 = r2 - r6
            r0 = r16
            int[] r1 = r0.m
            r0 = r16
            int r2 = r0.l
            int r3 = r2 + 1
            r0 = r16
            r0.l = r3
            r1[r2] = r12
            r12 = r7
            r13 = r8
            r14 = r9
            r15 = r10
            goto L_0x052d
        L_0x059c:
            if (r7 == 0) goto L_0x05b4
            r17 = 0
            int r7 = r7 + -1
            r0 = r16
            com.jcraft.jzlib.ZStream r4 = r0.A
            byte[] r4 = r4.next_in
            int r13 = r8 + 1
            byte r4 = r4[r8]
            r4 = r4 & 255(0xff, float:3.57E-43)
            int r4 = r4 << r2
            r3 = r3 | r4
            int r2 = r2 + 8
            r8 = r13
            goto L_0x054b
        L_0x05b4:
            r0 = r16
            r0.b = r3
            r0 = r16
            r0.f3105a = r2
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            r1.avail_in = r7
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            long r2 = r1.total_in
            r0 = r16
            com.jcraft.jzlib.ZStream r4 = r0.A
            int r4 = r4.next_in_index
            int r4 = r8 - r4
            long r4 = (long) r4
            long r2 = r2 + r4
            r1.total_in = r2
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            r1.next_in_index = r8
            r0 = r16
            r0.f = r11
            int r1 = r16.b(r17)
            goto L_0x0061
        L_0x05e4:
            r1 = 18
            if (r12 != r1) goto L_0x0663
            r1 = 7
            r5 = r1
        L_0x05ea:
            r1 = 18
            if (r12 != r1) goto L_0x0667
            r1 = 11
        L_0x05f0:
            r4 = r3
            r3 = r2
        L_0x05f2:
            int r2 = r6 + r5
            if (r3 < r2) goto L_0x0669
            int r4 = r4 >>> r6
            int r3 = r3 - r6
            int[] r2 = com.jcraft.jzlib.c.g
            r2 = r2[r5]
            r2 = r2 & r4
            int r2 = r2 + r1
            int r10 = r4 >>> r5
            int r9 = r3 - r5
            r0 = r16
            int r3 = r0.l
            r0 = r16
            int r1 = r0.k
            int r4 = r3 + r2
            r5 = r1 & 31
            int r5 = r5 + 258
            int r1 = r1 >> 5
            r1 = r1 & 31
            int r1 = r1 + r5
            if (r4 > r1) goto L_0x061d
            r1 = 16
            if (r12 != r1) goto L_0x06b2
            if (r3 > 0) goto L_0x06b2
        L_0x061d:
            r1 = 0
            r0 = r16
            r0.m = r1
            r1 = 9
            r0 = r16
            r0.i = r1
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            java.lang.String r2 = "invalid bit length repeat"
            r1.msg = r2
            r0 = r16
            r0.b = r10
            r0 = r16
            r0.f3105a = r9
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            r1.avail_in = r7
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            long r2 = r1.total_in
            r0 = r16
            com.jcraft.jzlib.ZStream r4 = r0.A
            int r4 = r4.next_in_index
            int r4 = r8 - r4
            long r4 = (long) r4
            long r2 = r2 + r4
            r1.total_in = r2
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            r1.next_in_index = r8
            r0 = r16
            r0.f = r11
            r1 = -3
            r0 = r16
            int r1 = r0.b(r1)
            goto L_0x0061
        L_0x0663:
            int r1 = r12 + -14
            r5 = r1
            goto L_0x05ea
        L_0x0667:
            r1 = 3
            goto L_0x05f0
        L_0x0669:
            if (r7 == 0) goto L_0x0682
            r17 = 0
            int r7 = r7 + -1
            r0 = r16
            com.jcraft.jzlib.ZStream r2 = r0.A
            byte[] r9 = r2.next_in
            int r2 = r8 + 1
            byte r8 = r9[r8]
            r8 = r8 & 255(0xff, float:3.57E-43)
            int r8 = r8 << r3
            r4 = r4 | r8
            int r3 = r3 + 8
            r8 = r2
            goto L_0x05f2
        L_0x0682:
            r0 = r16
            r0.b = r4
            r0 = r16
            r0.f3105a = r3
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            r1.avail_in = r7
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            long r2 = r1.total_in
            r0 = r16
            com.jcraft.jzlib.ZStream r4 = r0.A
            int r4 = r4.next_in_index
            int r4 = r8 - r4
            long r4 = (long) r4
            long r2 = r2 + r4
            r1.total_in = r2
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            r1.next_in_index = r8
            r0 = r16
            r0.f = r11
            int r1 = r16.b(r17)
            goto L_0x0061
        L_0x06b2:
            r1 = 16
            if (r12 != r1) goto L_0x06d4
            r0 = r16
            int[] r1 = r0.m
            int r4 = r3 + -1
            r1 = r1[r4]
        L_0x06be:
            r0 = r16
            int[] r5 = r0.m
            int r4 = r3 + 1
            r5[r3] = r1
            int r2 = r2 + -1
            if (r2 != 0) goto L_0x08e3
            r0 = r16
            r0.l = r4
            r12 = r7
            r13 = r8
            r14 = r9
            r15 = r10
            goto L_0x052d
        L_0x06d4:
            r1 = 0
            goto L_0x06be
        L_0x06d6:
            r0 = r16
            int[] r1 = r0.o
            r2 = 0
            r3 = -1
            r1[r2] = r3
            r0 = r16
            int[] r1 = r0.p
            r2 = 0
            r3 = 9
            r1[r2] = r3
            r0 = r16
            int[] r1 = r0.q
            r2 = 0
            r3 = 6
            r1[r2] = r3
            r0 = r16
            int r3 = r0.k
            r0 = r16
            com.jcraft.jzlib.e r1 = r0.z
            r2 = r3 & 31
            int r2 = r2 + 257
            int r3 = r3 >> 5
            r3 = r3 & 31
            int r3 = r3 + 1
            r0 = r16
            int[] r4 = r0.m
            r0 = r16
            int[] r5 = r0.p
            r0 = r16
            int[] r6 = r0.q
            r0 = r16
            int[] r7 = r0.t
            r0 = r16
            int[] r8 = r0.u
            r0 = r16
            int[] r9 = r0.x
            r0 = r16
            com.jcraft.jzlib.ZStream r10 = r0.A
            int r1 = r1.a(r2, r3, r4, r5, r6, r7, r8, r9, r10)
            if (r1 == 0) goto L_0x0763
            r2 = -3
            if (r1 != r2) goto L_0x0731
            r2 = 0
            r0 = r16
            r0.m = r2
            r2 = 9
            r0 = r16
            r0.i = r2
        L_0x0731:
            r0 = r16
            r0.b = r15
            r0 = r16
            r0.f3105a = r14
            r0 = r16
            com.jcraft.jzlib.ZStream r2 = r0.A
            r2.avail_in = r12
            r0 = r16
            com.jcraft.jzlib.ZStream r2 = r0.A
            long r3 = r2.total_in
            r0 = r16
            com.jcraft.jzlib.ZStream r5 = r0.A
            int r5 = r5.next_in_index
            int r5 = r13 - r5
            long r5 = (long) r5
            long r3 = r3 + r5
            r2.total_in = r3
            r0 = r16
            com.jcraft.jzlib.ZStream r2 = r0.A
            r2.next_in_index = r13
            r0 = r16
            r0.f = r11
            r0 = r16
            int r1 = r0.b(r1)
            goto L_0x0061
        L_0x0763:
            r0 = r16
            com.jcraft.jzlib.d r1 = r0.v
            r0 = r16
            int[] r2 = r0.p
            r3 = 0
            r2 = r2[r3]
            r0 = r16
            int[] r3 = r0.q
            r4 = 0
            r3 = r3[r4]
            r0 = r16
            int[] r4 = r0.x
            r0 = r16
            int[] r5 = r0.t
            r6 = 0
            r5 = r5[r6]
            r0 = r16
            int[] r6 = r0.x
            r0 = r16
            int[] r7 = r0.u
            r8 = 0
            r7 = r7[r8]
            r1.a(r2, r3, r4, r5, r6, r7)
            r1 = 6
            r0 = r16
            r0.i = r1
        L_0x0793:
            r0 = r16
            r0.b = r15
            r0 = r16
            r0.f3105a = r14
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            r1.avail_in = r12
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            long r2 = r1.total_in
            r0 = r16
            com.jcraft.jzlib.ZStream r4 = r0.A
            int r4 = r4.next_in_index
            int r4 = r13 - r4
            long r4 = (long) r4
            long r2 = r2 + r4
            r1.total_in = r2
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            r1.next_in_index = r13
            r0 = r16
            r0.f = r11
            r0 = r16
            com.jcraft.jzlib.d r1 = r0.v
            r0 = r17
            int r1 = r1.a(r0)
            r2 = 1
            if (r1 == r2) goto L_0x07d2
            r0 = r16
            int r1 = r0.b(r1)
            goto L_0x0061
        L_0x07d2:
            r17 = 0
            r0 = r16
            com.jcraft.jzlib.d r1 = r0.v
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            com.jcraft.jzlib.d.a()
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            int r4 = r1.next_in_index
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            int r3 = r1.avail_in
            r0 = r16
            int r6 = r0.b
            r0 = r16
            int r5 = r0.f3105a
            r0 = r16
            int r2 = r0.f
            r0 = r16
            int r1 = r0.e
            if (r2 >= r1) goto L_0x0815
            r0 = r16
            int r1 = r0.e
            int r1 = r1 - r2
            int r1 = r1 + -1
        L_0x0804:
            r0 = r16
            int r7 = r0.w
            if (r7 != 0) goto L_0x081b
            r7 = 0
            r0 = r16
            r0.i = r7
            r8 = r1
            r11 = r2
            r1 = r3
            r2 = r4
            goto L_0x0029
        L_0x0815:
            r0 = r16
            int r1 = r0.d
            int r1 = r1 - r2
            goto L_0x0804
        L_0x081b:
            r1 = 7
            r0 = r16
            r0.i = r1
        L_0x0820:
            r0 = r16
            r0.f = r2
            int r1 = r16.b(r17)
            r0 = r16
            int r11 = r0.f
            r0 = r16
            int r2 = r0.e
            if (r11 >= r2) goto L_0x0872
            r0 = r16
            int r2 = r0.e
        L_0x0836:
            r0 = r16
            int r2 = r0.e
            r0 = r16
            int r7 = r0.f
            if (r2 == r7) goto L_0x0877
            r0 = r16
            r0.b = r6
            r0 = r16
            r0.f3105a = r5
            r0 = r16
            com.jcraft.jzlib.ZStream r2 = r0.A
            r2.avail_in = r3
            r0 = r16
            com.jcraft.jzlib.ZStream r2 = r0.A
            long r5 = r2.total_in
            r0 = r16
            com.jcraft.jzlib.ZStream r3 = r0.A
            int r3 = r3.next_in_index
            int r3 = r4 - r3
            long r7 = (long) r3
            long r5 = r5 + r7
            r2.total_in = r5
            r0 = r16
            com.jcraft.jzlib.ZStream r2 = r0.A
            r2.next_in_index = r4
            r0 = r16
            r0.f = r11
            r0 = r16
            int r1 = r0.b(r1)
            goto L_0x0061
        L_0x0872:
            r0 = r16
            int r2 = r0.d
            goto L_0x0836
        L_0x0877:
            r1 = 8
            r0 = r16
            r0.i = r1
        L_0x087d:
            r0 = r16
            r0.b = r6
            r0 = r16
            r0.f3105a = r5
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            r1.avail_in = r3
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            long r2 = r1.total_in
            r0 = r16
            com.jcraft.jzlib.ZStream r5 = r0.A
            int r5 = r5.next_in_index
            int r5 = r4 - r5
            long r5 = (long) r5
            long r2 = r2 + r5
            r1.total_in = r2
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            r1.next_in_index = r4
            r0 = r16
            r0.f = r11
            r1 = 1
            r0 = r16
            int r1 = r0.b(r1)
            goto L_0x0061
        L_0x08b0:
            r0 = r16
            r0.b = r6
            r0 = r16
            r0.f3105a = r5
            r0 = r16
            com.jcraft.jzlib.ZStream r3 = r0.A
            r3.avail_in = r1
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            long r3 = r1.total_in
            r0 = r16
            com.jcraft.jzlib.ZStream r5 = r0.A
            int r5 = r5.next_in_index
            int r5 = r2 - r5
            long r5 = (long) r5
            long r3 = r3 + r5
            r1.total_in = r3
            r0 = r16
            com.jcraft.jzlib.ZStream r1 = r0.A
            r1.next_in_index = r2
            r0 = r16
            r0.f = r11
            r1 = -3
            r0 = r16
            int r1 = r0.b(r1)
            goto L_0x0061
        L_0x08e3:
            r3 = r4
            goto L_0x06be
        L_0x08e6:
            r1 = r7
            r2 = r8
            goto L_0x04b5
        L_0x08ea:
            r8 = r1
            r11 = r2
            r1 = r3
            r2 = r4
            goto L_0x0029
        L_0x08f0:
            r7 = r3
            goto L_0x0313
        L_0x08f3:
            r8 = r3
            goto L_0x02bf
        L_0x08f6:
            r12 = r1
            r13 = r2
            r14 = r5
            r15 = r6
            goto L_0x052d
        L_0x08fc:
            r12 = r1
            r13 = r2
            r14 = r5
            r15 = r6
            goto L_0x0793
        L_0x0902:
            r3 = r1
            r4 = r2
            r2 = r11
            goto L_0x0820
        L_0x0907:
            r3 = r1
            r4 = r2
            goto L_0x087d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jcraft.jzlib.c.a(int):int");
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        int i2 = this.i;
        if (this.i == 6) {
            d dVar = this.v;
            ZStream zStream = this.A;
            d.a();
        }
        this.i = 0;
        this.f3105a = 0;
        this.b = 0;
        this.f = 0;
        this.e = 0;
        if (this.y) {
            this.A.adler.reset();
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(byte[] bArr, int i2, int i3) {
        System.arraycopy(bArr, i2, this.c, 0, i3);
        this.f = i3;
        this.e = i3;
    }

    /* access modifiers changed from: package-private */
    public final int b(int i2) {
        int i3;
        int i4 = this.A.next_out_index;
        int i5 = this.e;
        int i6 = (i5 <= this.f ? this.f : this.d) - i5;
        if (i6 > this.A.avail_out) {
            i6 = this.A.avail_out;
        }
        if (i6 != 0 && i2 == -5) {
            i2 = 0;
        }
        this.A.avail_out -= i6;
        this.A.total_out += (long) i6;
        if (this.y && i6 > 0) {
            this.A.adler.update(this.c, i5, i6);
        }
        System.arraycopy(this.c, i5, this.A.next_out, i4, i6);
        int i7 = i4 + i6;
        int i8 = i6 + i5;
        if (i8 == this.d) {
            if (this.f == this.d) {
                this.f = 0;
            }
            int i9 = this.f + 0;
            if (i9 > this.A.avail_out) {
                i9 = this.A.avail_out;
            }
            if (i9 != 0 && i2 == -5) {
                i2 = 0;
            }
            this.A.avail_out -= i9;
            this.A.total_out += (long) i9;
            if (this.y && i9 > 0) {
                this.A.adler.update(this.c, 0, i9);
            }
            System.arraycopy(this.c, 0, this.A.next_out, i7, i9);
            i3 = i7 + i9;
            i8 = i9 + 0;
        } else {
            i3 = i7;
        }
        this.A.next_out_index = i3;
        this.e = i8;
        return i2;
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        a();
        this.c = null;
        this.x = null;
    }

    /* access modifiers changed from: package-private */
    public final int c() {
        return this.i == 1 ? 1 : 0;
    }
}
