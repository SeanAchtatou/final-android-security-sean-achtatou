package com.jcraft.jzlib;

import java.io.FilterOutputStream;
import java.io.OutputStream;
import mm.purchasesdk.PurchaseCode;

public class DeflaterOutputStream extends FilterOutputStream {
    protected static final int DEFAULT_BUFSIZE = 512;
    private final byte[] buf1;
    protected byte[] buffer;
    private boolean close_out;
    private boolean closed;
    protected final Deflater deflater;
    protected boolean mydeflater;
    private boolean syncFlush;

    public DeflaterOutputStream(OutputStream outputStream) {
        this(outputStream, new Deflater(-1), 512, true);
        this.mydeflater = true;
    }

    public DeflaterOutputStream(OutputStream outputStream, Deflater deflater2) {
        this(outputStream, deflater2, 512, true);
    }

    public DeflaterOutputStream(OutputStream outputStream, Deflater deflater2, int i) {
        this(outputStream, deflater2, i, true);
    }

    public DeflaterOutputStream(OutputStream outputStream, Deflater deflater2, int i, boolean z) {
        super(outputStream);
        this.closed = false;
        this.syncFlush = false;
        this.buf1 = new byte[1];
        this.mydeflater = false;
        this.close_out = true;
        if (outputStream == null || deflater2 == null) {
            throw new NullPointerException();
        } else if (i <= 0) {
            throw new IllegalArgumentException("buffer size must be greater than 0");
        } else {
            this.deflater = deflater2;
            this.buffer = new byte[i];
            this.close_out = z;
        }
    }

    public void close() {
        if (!this.closed) {
            finish();
            if (this.mydeflater) {
                this.deflater.end();
            }
            if (this.close_out) {
                this.out.close();
            }
            this.closed = true;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0023, code lost:
        if (r6 != 4) goto L_0x0025;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int deflate(int r6) {
        /*
            r5 = this;
            r4 = 0
            com.jcraft.jzlib.Deflater r0 = r5.deflater
            byte[] r1 = r5.buffer
            byte[] r2 = r5.buffer
            int r2 = r2.length
            r0.setOutput(r1, r4, r2)
            com.jcraft.jzlib.Deflater r0 = r5.deflater
            int r0 = r0.deflate(r6)
            switch(r0) {
                case -5: goto L_0x001c;
                case 0: goto L_0x0025;
                case 1: goto L_0x0025;
                default: goto L_0x0014;
            }
        L_0x0014:
            java.io.IOException r0 = new java.io.IOException
            java.lang.String r1 = "failed to deflate"
            r0.<init>(r1)
            throw r0
        L_0x001c:
            com.jcraft.jzlib.Deflater r1 = r5.deflater
            int r1 = r1.avail_in
            if (r1 > 0) goto L_0x0014
            r1 = 4
            if (r6 == r1) goto L_0x0014
        L_0x0025:
            com.jcraft.jzlib.Deflater r1 = r5.deflater
            int r1 = r1.next_out_index
            if (r1 <= 0) goto L_0x0032
            java.io.OutputStream r2 = r5.out
            byte[] r3 = r5.buffer
            r2.write(r3, r4, r1)
        L_0x0032:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jcraft.jzlib.DeflaterOutputStream.deflate(int):int");
    }

    public void finish() {
        while (!this.deflater.finished()) {
            deflate(4);
        }
    }

    public void flush() {
        int deflate;
        if (this.syncFlush && !this.deflater.finished()) {
            do {
                deflate = deflate(2);
                if (this.deflater.next_out_index < this.buffer.length) {
                    break;
                }
            } while (deflate != 1);
        }
        this.out.flush();
    }

    public Deflater getDeflater() {
        return this.deflater;
    }

    public boolean getSyncFlush() {
        return this.syncFlush;
    }

    public long getTotalIn() {
        return this.deflater.getTotalIn();
    }

    public long getTotalOut() {
        return this.deflater.getTotalOut();
    }

    public void setSyncFlush(boolean z) {
        this.syncFlush = z;
    }

    public void write(int i) {
        this.buf1[0] = (byte) (i & PurchaseCode.AUTH_INVALID_APP);
        write(this.buf1, 0, 1);
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0041  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void write(byte[] r6, int r7, int r8) {
        /*
            r5 = this;
            r2 = 0
            r1 = 1
            com.jcraft.jzlib.Deflater r0 = r5.deflater
            boolean r0 = r0.finished()
            if (r0 == 0) goto L_0x0012
            java.io.IOException r0 = new java.io.IOException
            java.lang.String r1 = "finished"
            r0.<init>(r1)
            throw r0
        L_0x0012:
            if (r7 >= 0) goto L_0x0028
            r3 = r1
        L_0x0015:
            if (r8 >= 0) goto L_0x002a
            r0 = r1
        L_0x0018:
            r3 = r3 | r0
            int r0 = r7 + r8
            int r4 = r6.length
            if (r0 <= r4) goto L_0x002c
            r0 = r1
        L_0x001f:
            r0 = r0 | r3
            if (r0 == 0) goto L_0x002e
            java.lang.IndexOutOfBoundsException r0 = new java.lang.IndexOutOfBoundsException
            r0.<init>()
            throw r0
        L_0x0028:
            r3 = r2
            goto L_0x0015
        L_0x002a:
            r0 = r2
            goto L_0x0018
        L_0x002c:
            r0 = r2
            goto L_0x001f
        L_0x002e:
            if (r8 != 0) goto L_0x0031
        L_0x0030:
            return
        L_0x0031:
            boolean r0 = r5.syncFlush
            if (r0 == 0) goto L_0x0036
            r2 = 2
        L_0x0036:
            com.jcraft.jzlib.Deflater r0 = r5.deflater
            r0.setInput(r6, r7, r8, r1)
        L_0x003b:
            com.jcraft.jzlib.Deflater r0 = r5.deflater
            int r0 = r0.avail_in
            if (r0 <= 0) goto L_0x0030
            int r0 = r5.deflate(r2)
            if (r0 == r1) goto L_0x0030
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jcraft.jzlib.DeflaterOutputStream.write(byte[], int, int):void");
    }
}
