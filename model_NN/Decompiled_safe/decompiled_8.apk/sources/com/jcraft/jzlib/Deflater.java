package com.jcraft.jzlib;

public final class Deflater extends ZStream {
    private static final int DEF_WBITS = 15;
    private static final int MAX_MEM_LEVEL = 9;
    private static final int MAX_WBITS = 15;
    private static final int Z_BUF_ERROR = -5;
    private static final int Z_DATA_ERROR = -3;
    private static final int Z_ERRNO = -1;
    private static final int Z_FINISH = 4;
    private static final int Z_FULL_FLUSH = 3;
    private static final int Z_MEM_ERROR = -4;
    private static final int Z_NEED_DICT = 2;
    private static final int Z_NO_FLUSH = 0;
    private static final int Z_OK = 0;
    private static final int Z_PARTIAL_FLUSH = 1;
    private static final int Z_STREAM_END = 1;
    private static final int Z_STREAM_ERROR = -2;
    private static final int Z_SYNC_FLUSH = 2;
    private static final int Z_VERSION_ERROR = -6;
    private boolean finished;

    public Deflater() {
        this.finished = false;
    }

    public Deflater(int i) {
        this(i, 15);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.jcraft.jzlib.Deflater.<init>(int, int, boolean):void
     arg types: [int, int, int]
     candidates:
      com.jcraft.jzlib.Deflater.<init>(int, int, int):void
      com.jcraft.jzlib.Deflater.<init>(int, int, boolean):void */
    public Deflater(int i, int i2) {
        this(i, i2, false);
    }

    public Deflater(int i, int i2, int i3) {
        this.finished = false;
        int init = init(i, i2, i3);
        if (init != 0) {
            throw new GZIPException(String.valueOf(init) + ": " + this.msg);
        }
    }

    public Deflater(int i, int i2, boolean z) {
        this.finished = false;
        int init = init(i, i2, z);
        if (init != 0) {
            throw new GZIPException(String.valueOf(init) + ": " + this.msg);
        }
    }

    public Deflater(int i, boolean z) {
        this(i, 15, z);
    }

    public final int copy(Deflater deflater) {
        this.finished = deflater.finished;
        return Deflate.deflateCopy(this, deflater);
    }

    public final int deflate(int i) {
        if (this.dstate == null) {
            return -2;
        }
        int deflate = this.dstate.deflate(i);
        if (deflate != 1) {
            return deflate;
        }
        this.finished = true;
        return deflate;
    }

    public final int end() {
        this.finished = true;
        if (this.dstate == null) {
            return -2;
        }
        int deflateEnd = this.dstate.deflateEnd();
        this.dstate = null;
        free();
        return deflateEnd;
    }

    public final boolean finished() {
        return this.finished;
    }

    public final int init(int i) {
        return init(i, 15);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.jcraft.jzlib.Deflater.init(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.jcraft.jzlib.Deflater.init(int, int, int):int
      com.jcraft.jzlib.Deflater.init(int, int, boolean):int */
    public final int init(int i, int i2) {
        return init(i, i2, false);
    }

    public final int init(int i, int i2, int i3) {
        this.finished = false;
        this.dstate = new Deflate(this);
        return this.dstate.deflateInit(i, i2, i3);
    }

    public final int init(int i, int i2, boolean z) {
        this.finished = false;
        this.dstate = new Deflate(this);
        Deflate deflate = this.dstate;
        if (z) {
            i2 = -i2;
        }
        return deflate.deflateInit(i, i2);
    }

    public final int init(int i, boolean z) {
        return init(i, 15, z);
    }

    public final int params(int i, int i2) {
        if (this.dstate == null) {
            return -2;
        }
        return this.dstate.deflateParams(i, i2);
    }

    public final int setDictionary(byte[] bArr, int i) {
        if (this.dstate == null) {
            return -2;
        }
        return this.dstate.deflateSetDictionary(bArr, i);
    }
}
