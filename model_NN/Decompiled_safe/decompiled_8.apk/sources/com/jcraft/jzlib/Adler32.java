package com.jcraft.jzlib;

public final class Adler32 implements a {
    private static final int BASE = 65521;
    private static final int NMAX = 5552;
    private long s1 = 1;
    private long s2 = 0;

    static long combine(long j, long j2, long j3) {
        long j4 = j3 % 65521;
        long j5 = 65535 & j;
        long j6 = j5 + (((65535 & j2) + 65521) - 1);
        long j7 = (((((j >> 16) & 65535) + ((j2 >> 16) & 65535)) + 65521) - j4) + ((j4 * j5) % 65521);
        if (j6 >= 65521) {
            j6 -= 65521;
        }
        if (j6 >= 65521) {
            j6 -= 65521;
        }
        if (j7 >= 131042) {
            j7 -= 131042;
        }
        if (j7 >= 65521) {
            j7 -= 65521;
        }
        return (j7 << 16) | j6;
    }

    public final Adler32 copy() {
        Adler32 adler32 = new Adler32();
        adler32.s1 = this.s1;
        adler32.s2 = this.s2;
        return adler32;
    }

    public final long getValue() {
        return (this.s2 << 16) | this.s1;
    }

    public final void reset() {
        this.s1 = 1;
        this.s2 = 0;
    }

    public final void reset(long j) {
        this.s1 = j & 65535;
        this.s2 = (j >> 16) & 65535;
    }

    public final void update(byte[] bArr, int i, int i2) {
        int i3;
        if (i2 == 1) {
            this.s1 += (long) (bArr[i] & GZIPHeader.OS_UNKNOWN);
            this.s2 += this.s1;
            this.s1 %= 65521;
            this.s2 %= 65521;
            return;
        }
        int i4 = i2 / NMAX;
        int i5 = i2 % NMAX;
        int i6 = i4;
        int i7 = i;
        while (true) {
            int i8 = i6 - 1;
            if (i6 <= 0) {
                break;
            }
            int i9 = NMAX;
            while (true) {
                i3 = i7;
                int i10 = i9;
                i9 = i10 - 1;
                if (i10 <= 0) {
                    break;
                }
                i7 = i3 + 1;
                this.s1 += (long) (bArr[i3] & GZIPHeader.OS_UNKNOWN);
                this.s2 += this.s1;
            }
            this.s1 %= 65521;
            this.s2 %= 65521;
            i6 = i8;
            i7 = i3;
        }
        int i11 = i7;
        while (true) {
            int i12 = i5 - 1;
            if (i5 <= 0) {
                this.s1 %= 65521;
                this.s2 %= 65521;
                return;
            }
            this.s1 += (long) (bArr[i11] & GZIPHeader.OS_UNKNOWN);
            this.s2 += this.s1;
            i11++;
            i5 = i12;
        }
    }
}
