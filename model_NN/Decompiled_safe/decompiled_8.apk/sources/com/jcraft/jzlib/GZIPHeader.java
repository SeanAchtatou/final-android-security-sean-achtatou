package com.jcraft.jzlib;

import com.amap.mapapi.poisearch.PoiTypeDef;
import java.io.UnsupportedEncodingException;
import mm.purchasesdk.PurchaseCode;

public class GZIPHeader implements Cloneable {
    public static final byte OS_AMIGA = 1;
    public static final byte OS_ATARI = 5;
    public static final byte OS_CPM = 9;
    public static final byte OS_MACOS = 7;
    public static final byte OS_MSDOS = 0;
    public static final byte OS_OS2 = 6;
    public static final byte OS_QDOS = 12;
    public static final byte OS_RISCOS = 13;
    public static final byte OS_TOPS20 = 10;
    public static final byte OS_UNIX = 3;
    public static final byte OS_UNKNOWN = -1;
    public static final byte OS_VMCMS = 4;
    public static final byte OS_VMS = 2;
    public static final byte OS_WIN32 = 11;
    public static final byte OS_ZSYSTEM = 8;
    byte[] comment;
    long crc;
    boolean done = false;
    byte[] extra;
    private boolean fhcrc = false;
    int hcrc;
    long mtime = 0;
    byte[] name;
    int os = PurchaseCode.AUTH_INVALID_APP;
    boolean text = false;
    long time;
    int xflags;

    public Object clone() {
        GZIPHeader gZIPHeader = (GZIPHeader) super.clone();
        if (gZIPHeader.extra != null) {
            byte[] bArr = new byte[gZIPHeader.extra.length];
            System.arraycopy(gZIPHeader.extra, 0, bArr, 0, bArr.length);
            gZIPHeader.extra = bArr;
        }
        if (gZIPHeader.name != null) {
            byte[] bArr2 = new byte[gZIPHeader.name.length];
            System.arraycopy(gZIPHeader.name, 0, bArr2, 0, bArr2.length);
            gZIPHeader.name = bArr2;
        }
        if (gZIPHeader.comment != null) {
            byte[] bArr3 = new byte[gZIPHeader.comment.length];
            System.arraycopy(gZIPHeader.comment, 0, bArr3, 0, bArr3.length);
            gZIPHeader.comment = bArr3;
        }
        return gZIPHeader;
    }

    public long getCRC() {
        return this.crc;
    }

    public String getComment() {
        if (this.comment == null) {
            return PoiTypeDef.All;
        }
        try {
            return new String(this.comment, "ISO-8859-1");
        } catch (UnsupportedEncodingException e) {
            throw new InternalError(e.toString());
        }
    }

    public long getModifiedTime() {
        return this.mtime;
    }

    public String getName() {
        if (this.name == null) {
            return PoiTypeDef.All;
        }
        try {
            return new String(this.name, "ISO-8859-1");
        } catch (UnsupportedEncodingException e) {
            throw new InternalError(e.toString());
        }
    }

    public int getOS() {
        return this.os;
    }

    /* access modifiers changed from: package-private */
    public void put(Deflate deflate) {
        int i = this.text ? 1 : 0;
        if (this.fhcrc) {
            i |= 2;
        }
        if (this.extra != null) {
            i |= 4;
        }
        if (this.name != null) {
            i |= 8;
        }
        if (this.comment != null) {
            i |= 16;
        }
        int i2 = deflate.level == 1 ? 4 : deflate.level == 9 ? 2 : 0;
        deflate.put_short(-29921);
        deflate.put_byte((byte) 8);
        deflate.put_byte((byte) i);
        deflate.put_byte((byte) ((int) this.mtime));
        deflate.put_byte((byte) ((int) (this.mtime >> 8)));
        deflate.put_byte((byte) ((int) (this.mtime >> 16)));
        deflate.put_byte((byte) ((int) (this.mtime >> 24)));
        deflate.put_byte((byte) i2);
        deflate.put_byte((byte) this.os);
        if (this.extra != null) {
            deflate.put_byte((byte) this.extra.length);
            deflate.put_byte((byte) (this.extra.length >> 8));
            deflate.put_byte(this.extra, 0, this.extra.length);
        }
        if (this.name != null) {
            deflate.put_byte(this.name, 0, this.name.length);
            deflate.put_byte((byte) 0);
        }
        if (this.comment != null) {
            deflate.put_byte(this.comment, 0, this.comment.length);
            deflate.put_byte((byte) 0);
        }
    }

    public void setCRC(long j) {
        this.crc = j;
    }

    public void setComment(String str) {
        try {
            this.comment = str.getBytes("ISO-8859-1");
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException("comment must be in ISO-8859-1 " + this.name);
        }
    }

    public void setModifiedTime(long j) {
        this.mtime = j;
    }

    public void setName(String str) {
        try {
            this.name = str.getBytes("ISO-8859-1");
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException("name must be in ISO-8859-1 " + str);
        }
    }

    public void setOS(int i) {
        if ((i < 0 || i > 13) && i != 255) {
            throw new IllegalArgumentException("os: " + i);
        }
        this.os = i;
    }
}
