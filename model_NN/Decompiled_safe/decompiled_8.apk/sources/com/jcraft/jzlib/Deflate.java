package com.jcraft.jzlib;

import com.amap.mapapi.poisearch.PoiTypeDef;
import com.mapabc.minimap.map.vmap.NativeMapEngine;
import mm.purchasesdk.PurchaseCode;

public final class Deflate implements Cloneable {
    private static final int BL_CODES = 19;
    private static final int BUSY_STATE = 113;
    private static final int BlockDone = 1;
    private static final int Buf_size = 16;
    private static final int DEF_MEM_LEVEL = 8;
    private static final int DYN_TREES = 2;
    private static final int D_CODES = 30;
    private static final int END_BLOCK = 256;
    private static final int FAST = 1;
    private static final int FINISH_STATE = 666;
    private static final int FinishDone = 3;
    private static final int FinishStarted = 2;
    private static final int HEAP_SIZE = 573;
    private static final int INIT_STATE = 42;
    private static final int LENGTH_CODES = 29;
    private static final int LITERALS = 256;
    private static final int L_CODES = 286;
    private static final int MAX_BITS = 15;
    private static final int MAX_MATCH = 258;
    private static final int MAX_MEM_LEVEL = 9;
    private static final int MAX_WBITS = 15;
    private static final int MIN_LOOKAHEAD = 262;
    private static final int MIN_MATCH = 3;
    private static final int NeedMore = 0;
    private static final int PRESET_DICT = 32;
    private static final int REPZ_11_138 = 18;
    private static final int REPZ_3_10 = 17;
    private static final int REP_3_6 = 16;
    private static final int SLOW = 2;
    private static final int STATIC_TREES = 1;
    private static final int STORED = 0;
    private static final int STORED_BLOCK = 0;
    private static final int Z_ASCII = 1;
    private static final int Z_BINARY = 0;
    private static final int Z_BUF_ERROR = -5;
    private static final int Z_DATA_ERROR = -3;
    private static final int Z_DEFAULT_COMPRESSION = -1;
    private static final int Z_DEFAULT_STRATEGY = 0;
    private static final int Z_DEFLATED = 8;
    private static final int Z_ERRNO = -1;
    private static final int Z_FILTERED = 1;
    private static final int Z_FINISH = 4;
    private static final int Z_FULL_FLUSH = 3;
    private static final int Z_HUFFMAN_ONLY = 2;
    private static final int Z_MEM_ERROR = -4;
    private static final int Z_NEED_DICT = 2;
    private static final int Z_NO_FLUSH = 0;
    private static final int Z_OK = 0;
    private static final int Z_PARTIAL_FLUSH = 1;
    private static final int Z_STREAM_END = 1;
    private static final int Z_STREAM_ERROR = -2;
    private static final int Z_SYNC_FLUSH = 2;
    private static final int Z_UNKNOWN = 2;
    private static final int Z_VERSION_ERROR = -6;
    private static final b[] config_table;
    private static final String[] z_errmsg = {"need dictionary", "stream end", PoiTypeDef.All, "file error", "stream error", "data error", "insufficient memory", "buffer error", "incompatible version", PoiTypeDef.All};
    short bi_buf;
    int bi_valid;
    short[] bl_count = new short[16];
    i bl_desc = new i();
    short[] bl_tree;
    int block_start;
    int d_buf;
    i d_desc = new i();
    byte data_type;
    byte[] depth = new byte[HEAP_SIZE];
    short[] dyn_dtree;
    short[] dyn_ltree;
    GZIPHeader gheader = null;
    int good_match;
    int hash_bits;
    int hash_mask;
    int hash_shift;
    int hash_size;
    short[] head;
    int[] heap = new int[HEAP_SIZE];
    int heap_len;
    int heap_max;
    int ins_h;
    int l_buf;
    i l_desc = new i();
    int last_eob_len;
    int last_flush;
    int last_lit;
    int level;
    int lit_bufsize;
    int lookahead;
    int match_available;
    int match_length;
    int match_start;
    int matches;
    int max_chain_length;
    int max_lazy_match;
    byte method;
    int nice_match;
    int opt_len;
    int pending;
    byte[] pending_buf;
    int pending_buf_size;
    int pending_out;
    short[] prev;
    int prev_length;
    int prev_match;
    int static_len;
    int status;
    int strategy;
    ZStream strm;
    int strstart;
    int w_bits;
    int w_mask;
    int w_size;
    byte[] window;
    int window_size;
    int wrap = 1;

    static {
        b[] bVarArr = new b[10];
        config_table = bVarArr;
        bVarArr[0] = new b(0, 0, 0, 0, 0);
        config_table[1] = new b(4, 4, 8, 4, 1);
        config_table[2] = new b(4, 5, 16, 8, 1);
        config_table[3] = new b(4, 6, PRESET_DICT, PRESET_DICT, 1);
        config_table[4] = new b(4, 4, 16, 16, 2);
        config_table[5] = new b(8, 16, PRESET_DICT, PRESET_DICT, 2);
        config_table[6] = new b(8, 16, NativeMapEngine.MAX_ICON_SIZE, NativeMapEngine.MAX_ICON_SIZE, 2);
        config_table[7] = new b(8, PRESET_DICT, NativeMapEngine.MAX_ICON_SIZE, 256, 2);
        config_table[8] = new b(PRESET_DICT, NativeMapEngine.MAX_ICON_SIZE, 258, 1024, 2);
        config_table[9] = new b(PRESET_DICT, 258, 258, 4096, 2);
    }

    Deflate(ZStream zStream) {
        this.strm = zStream;
        this.dyn_ltree = new short[1146];
        this.dyn_dtree = new short[122];
        this.bl_tree = new short[78];
    }

    static int deflateCopy(ZStream zStream, ZStream zStream2) {
        if (zStream2.dstate == null) {
            return -2;
        }
        if (zStream2.next_in != null) {
            zStream.next_in = new byte[zStream2.next_in.length];
            System.arraycopy(zStream2.next_in, 0, zStream.next_in, 0, zStream2.next_in.length);
        }
        zStream.next_in_index = zStream2.next_in_index;
        zStream.avail_in = zStream2.avail_in;
        zStream.total_in = zStream2.total_in;
        if (zStream2.next_out != null) {
            zStream.next_out = new byte[zStream2.next_out.length];
            System.arraycopy(zStream2.next_out, 0, zStream.next_out, 0, zStream2.next_out.length);
        }
        zStream.next_out_index = zStream2.next_out_index;
        zStream.avail_out = zStream2.avail_out;
        zStream.total_out = zStream2.total_out;
        zStream.msg = zStream2.msg;
        zStream.data_type = zStream2.data_type;
        zStream.adler = zStream2.adler.copy();
        try {
            zStream.dstate = (Deflate) zStream2.dstate.clone();
            zStream.dstate.strm = zStream;
        } catch (CloneNotSupportedException e) {
        }
        return 0;
    }

    private int deflateInit(int i, int i2, int i3, int i4, int i5) {
        int i6;
        this.strm.msg = null;
        if (i == -1) {
            i = 6;
        }
        if (i3 < 0) {
            i6 = 0;
            i3 = -i3;
        } else if (i3 > 15) {
            i3 -= 16;
            this.strm.adler = new CRC32();
            i6 = 2;
        } else {
            i6 = 1;
        }
        if (i4 <= 0 || i4 > 9 || i2 != 8 || i3 < 9 || i3 > 15 || i < 0 || i > 9 || i5 < 0 || i5 > 2) {
            return -2;
        }
        this.strm.dstate = this;
        this.wrap = i6;
        this.w_bits = i3;
        this.w_size = 1 << this.w_bits;
        this.w_mask = this.w_size - 1;
        this.hash_bits = i4 + 7;
        this.hash_size = 1 << this.hash_bits;
        this.hash_mask = this.hash_size - 1;
        this.hash_shift = ((this.hash_bits + 3) - 1) / 3;
        this.window = new byte[(this.w_size * 2)];
        this.prev = new short[this.w_size];
        this.head = new short[this.hash_size];
        this.lit_bufsize = 1 << (i4 + 6);
        this.pending_buf = new byte[(this.lit_bufsize * 4)];
        this.pending_buf_size = this.lit_bufsize * 4;
        this.d_buf = this.lit_bufsize / 2;
        this.l_buf = this.lit_bufsize * 3;
        this.level = i;
        this.strategy = i5;
        this.method = (byte) i2;
        return deflateReset();
    }

    private byte[] dup(byte[] bArr) {
        byte[] bArr2 = new byte[bArr.length];
        System.arraycopy(bArr, 0, bArr2, 0, bArr2.length);
        return bArr2;
    }

    private int[] dup(int[] iArr) {
        int[] iArr2 = new int[iArr.length];
        System.arraycopy(iArr, 0, iArr2, 0, iArr2.length);
        return iArr2;
    }

    private short[] dup(short[] sArr) {
        short[] sArr2 = new short[sArr.length];
        System.arraycopy(sArr, 0, sArr2, 0, sArr2.length);
        return sArr2;
    }

    static boolean smaller(short[] sArr, int i, int i2, byte[] bArr) {
        short s = sArr[i * 2];
        short s2 = sArr[i2 * 2];
        return s < s2 || (s == s2 && bArr[i] <= bArr[i2]);
    }

    /* access modifiers changed from: package-private */
    public final void _tr_align() {
        send_bits(2, 3);
        send_code(256, h.f3110a);
        bi_flush();
        if (((this.last_eob_len + 1) + 10) - this.bi_valid < 9) {
            send_bits(2, 3);
            send_code(256, h.f3110a);
            bi_flush();
        }
        this.last_eob_len = 7;
    }

    /* access modifiers changed from: package-private */
    public final void _tr_flush_block(int i, int i2, boolean z) {
        int i3;
        int i4;
        int i5;
        int i6 = 1;
        if (this.level > 0) {
            if (this.data_type == 2) {
                set_data_type();
            }
            this.l_desc.a(this);
            this.d_desc.a(this);
            i5 = build_bl_tree();
            i4 = ((this.opt_len + 3) + 7) >>> 3;
            i3 = ((this.static_len + 3) + 7) >>> 3;
            if (i3 <= i4) {
                i4 = i3;
            }
        } else {
            int i7 = i2 + 5;
            i3 = i7;
            i4 = i7;
            i5 = 0;
        }
        if (i2 + 4 <= i4 && i != -1) {
            _tr_stored_block(i, i2, z);
        } else if (i3 == i4) {
            if (!z) {
                i6 = 0;
            }
            send_bits(i6 + 2, 3);
            compress_block(h.f3110a, h.b);
        } else {
            if (!z) {
                i6 = 0;
            }
            send_bits(i6 + 4, 3);
            send_all_trees(this.l_desc.i + 1, this.d_desc.i + 1, i5 + 1);
            compress_block(this.dyn_ltree, this.dyn_dtree);
        }
        init_block();
        if (z) {
            bi_windup();
        }
    }

    /* access modifiers changed from: package-private */
    public final void _tr_stored_block(int i, int i2, boolean z) {
        send_bits((z ? 1 : 0) + 0, 3);
        copy_block(i, i2, true);
    }

    /* access modifiers changed from: package-private */
    public final boolean _tr_tally(int i, int i2) {
        this.pending_buf[this.d_buf + (this.last_lit * 2)] = (byte) (i >>> 8);
        this.pending_buf[this.d_buf + (this.last_lit * 2) + 1] = (byte) i;
        this.pending_buf[this.l_buf + this.last_lit] = (byte) i2;
        this.last_lit++;
        if (i == 0) {
            short[] sArr = this.dyn_ltree;
            int i3 = i2 * 2;
            sArr[i3] = (short) (sArr[i3] + 1);
        } else {
            this.matches++;
            short[] sArr2 = this.dyn_ltree;
            int i4 = (i.e[i2] + 256 + 1) * 2;
            sArr2[i4] = (short) (sArr2[i4] + 1);
            short[] sArr3 = this.dyn_dtree;
            int a2 = i.a(i - 1) * 2;
            sArr3[a2] = (short) (sArr3[a2] + 1);
        }
        if ((this.last_lit & 8191) == 0 && this.level > 2) {
            int i5 = this.strstart - this.block_start;
            int i6 = this.last_lit * 8;
            for (int i7 = 0; i7 < D_CODES; i7++) {
                i6 = (int) (((long) i6) + (((long) this.dyn_dtree[i7 * 2]) * (5 + ((long) i.b[i7]))));
            }
            int i8 = i6 >>> 3;
            if (this.matches < this.last_lit / 2 && i8 < i5 / 2) {
                return true;
            }
        }
        return this.last_lit == this.lit_bufsize + -1;
    }

    /* access modifiers changed from: package-private */
    public final void bi_flush() {
        if (this.bi_valid == 16) {
            put_short(this.bi_buf);
            this.bi_buf = 0;
            this.bi_valid = 0;
        } else if (this.bi_valid >= 8) {
            put_byte((byte) this.bi_buf);
            this.bi_buf = (short) (this.bi_buf >>> 8);
            this.bi_valid -= 8;
        }
    }

    /* access modifiers changed from: package-private */
    public final void bi_windup() {
        if (this.bi_valid > 8) {
            put_short(this.bi_buf);
        } else if (this.bi_valid > 0) {
            put_byte((byte) this.bi_buf);
        }
        this.bi_buf = 0;
        this.bi_valid = 0;
    }

    /* access modifiers changed from: package-private */
    public final int build_bl_tree() {
        scan_tree(this.dyn_ltree, this.l_desc.i);
        scan_tree(this.dyn_dtree, this.d_desc.i);
        this.bl_desc.a(this);
        int i = REPZ_11_138;
        while (i >= 3 && this.bl_tree[(i.d[i] * 2) + 1] == 0) {
            i--;
        }
        this.opt_len += ((i + 1) * 3) + 5 + 5 + 4;
        return i;
    }

    public final Object clone() {
        Deflate deflate = (Deflate) super.clone();
        deflate.pending_buf = dup(deflate.pending_buf);
        deflate.window = dup(deflate.window);
        deflate.prev = dup(deflate.prev);
        deflate.head = dup(deflate.head);
        deflate.dyn_ltree = dup(deflate.dyn_ltree);
        deflate.dyn_dtree = dup(deflate.dyn_dtree);
        deflate.bl_tree = dup(deflate.bl_tree);
        deflate.bl_count = dup(deflate.bl_count);
        deflate.heap = dup(deflate.heap);
        deflate.depth = dup(deflate.depth);
        deflate.l_desc.h = deflate.dyn_ltree;
        deflate.d_desc.h = deflate.dyn_dtree;
        deflate.bl_desc.h = deflate.bl_tree;
        if (deflate.gheader != null) {
            deflate.gheader = (GZIPHeader) deflate.gheader.clone();
        }
        return deflate;
    }

    /* access modifiers changed from: package-private */
    public final void compress_block(short[] sArr, short[] sArr2) {
        int i = 0;
        if (this.last_lit != 0) {
            do {
                byte b = ((this.pending_buf[this.d_buf + (i * 2)] << 8) & 65280) | (this.pending_buf[this.d_buf + (i * 2) + 1] & GZIPHeader.OS_UNKNOWN);
                byte b2 = this.pending_buf[this.l_buf + i] & GZIPHeader.OS_UNKNOWN;
                i++;
                if (b == 0) {
                    send_code(b2, sArr);
                } else {
                    byte b3 = i.e[b2];
                    send_code(b3 + 256 + 1, sArr);
                    int i2 = i.f3111a[b3];
                    if (i2 != 0) {
                        send_bits(b2 - i.f[b3], i2);
                    }
                    int i3 = b - 1;
                    int a2 = i.a(i3);
                    send_code(a2, sArr2);
                    int i4 = i.b[a2];
                    if (i4 != 0) {
                        send_bits(i3 - i.g[a2], i4);
                    }
                }
            } while (i < this.last_lit);
        }
        send_code(256, sArr);
        this.last_eob_len = sArr[513];
    }

    /* access modifiers changed from: package-private */
    public final void copy_block(int i, int i2, boolean z) {
        bi_windup();
        this.last_eob_len = 8;
        if (z) {
            put_short((short) i2);
            put_short((short) (i2 ^ -1));
        }
        put_byte(this.window, i, i2);
    }

    /* access modifiers changed from: package-private */
    public final int deflate(int i) {
        int deflate_slow;
        if (i > 4 || i < 0) {
            return -2;
        }
        if (this.strm.next_out == null || ((this.strm.next_in == null && this.strm.avail_in != 0) || (this.status == FINISH_STATE && i != 4))) {
            this.strm.msg = z_errmsg[4];
            return -2;
        } else if (this.strm.avail_out == 0) {
            this.strm.msg = z_errmsg[7];
            return -5;
        } else {
            int i2 = this.last_flush;
            this.last_flush = i;
            if (this.status == INIT_STATE) {
                if (this.wrap == 2) {
                    getGZIPHeader().put(this);
                    this.status = 113;
                } else {
                    int i3 = (((this.w_bits - 8) << 4) + 8) << 8;
                    int i4 = ((this.level - 1) & PurchaseCode.AUTH_INVALID_APP) >> 1;
                    if (i4 > 3) {
                        i4 = 3;
                    }
                    int i5 = (i4 << 6) | i3;
                    if (this.strstart != 0) {
                        i5 |= PRESET_DICT;
                    }
                    this.status = 113;
                    putShortMSB(i5 + (31 - (i5 % 31)));
                    if (this.strstart != 0) {
                        long value = this.strm.adler.getValue();
                        putShortMSB((int) (value >>> 16));
                        putShortMSB((int) (value & 65535));
                    }
                }
                this.strm.adler.reset();
            }
            if (this.pending != 0) {
                this.strm.flush_pending();
                if (this.strm.avail_out == 0) {
                    this.last_flush = -1;
                    return 0;
                }
            } else if (this.strm.avail_in == 0 && i <= i2 && i != 4) {
                this.strm.msg = z_errmsg[7];
                return -5;
            }
            if (this.status != FINISH_STATE || this.strm.avail_in == 0) {
                if (!(this.strm.avail_in == 0 && this.lookahead == 0 && (i == 0 || this.status == FINISH_STATE))) {
                    switch (config_table[this.level].e) {
                        case 0:
                            deflate_slow = deflate_stored(i);
                            break;
                        case 1:
                            deflate_slow = deflate_fast(i);
                            break;
                        case 2:
                            deflate_slow = deflate_slow(i);
                            break;
                        default:
                            deflate_slow = -1;
                            break;
                    }
                    if (deflate_slow == 2 || deflate_slow == 3) {
                        this.status = FINISH_STATE;
                    }
                    if (deflate_slow == 0 || deflate_slow == 2) {
                        if (this.strm.avail_out != 0) {
                            return 0;
                        }
                        this.last_flush = -1;
                        return 0;
                    } else if (deflate_slow == 1) {
                        if (i == 1) {
                            _tr_align();
                        } else {
                            _tr_stored_block(0, 0, false);
                            if (i == 3) {
                                for (int i6 = 0; i6 < this.hash_size; i6++) {
                                    this.head[i6] = 0;
                                }
                            }
                        }
                        this.strm.flush_pending();
                        if (this.strm.avail_out == 0) {
                            this.last_flush = -1;
                            return 0;
                        }
                    }
                }
                if (i != 4) {
                    return 0;
                }
                if (this.wrap <= 0) {
                    return 1;
                }
                if (this.wrap == 2) {
                    long value2 = this.strm.adler.getValue();
                    put_byte((byte) ((int) (value2 & 255)));
                    put_byte((byte) ((int) ((value2 >> 8) & 255)));
                    put_byte((byte) ((int) ((value2 >> 16) & 255)));
                    put_byte((byte) ((int) ((value2 >> 24) & 255)));
                    put_byte((byte) ((int) (this.strm.total_in & 255)));
                    put_byte((byte) ((int) ((this.strm.total_in >> 8) & 255)));
                    put_byte((byte) ((int) ((this.strm.total_in >> 16) & 255)));
                    put_byte((byte) ((int) ((this.strm.total_in >> 24) & 255)));
                    getGZIPHeader().setCRC(value2);
                } else {
                    long value3 = this.strm.adler.getValue();
                    putShortMSB((int) (value3 >>> 16));
                    putShortMSB((int) (value3 & 65535));
                }
                this.strm.flush_pending();
                if (this.wrap > 0) {
                    this.wrap = -this.wrap;
                }
                return this.pending == 0 ? 1 : 0;
            }
            this.strm.msg = z_errmsg[7];
            return -5;
        }
    }

    /* access modifiers changed from: package-private */
    public final int deflateEnd() {
        if (this.status != INIT_STATE && this.status != 113 && this.status != FINISH_STATE) {
            return -2;
        }
        this.pending_buf = null;
        this.head = null;
        this.prev = null;
        this.window = null;
        return this.status == 113 ? -3 : 0;
    }

    /* access modifiers changed from: package-private */
    public final int deflateInit(int i) {
        return deflateInit(i, 15);
    }

    /* access modifiers changed from: package-private */
    public final int deflateInit(int i, int i2) {
        return deflateInit(i, 8, i2, 8, 0);
    }

    /* access modifiers changed from: package-private */
    public final int deflateInit(int i, int i2, int i3) {
        return deflateInit(i, 8, i2, i3, 0);
    }

    /* access modifiers changed from: package-private */
    public final int deflateParams(int i, int i2) {
        int i3 = 0;
        if (i == -1) {
            i = 6;
        }
        if (i < 0 || i > 9 || i2 < 0 || i2 > 2) {
            return -2;
        }
        if (!(config_table[this.level].e == config_table[i].e || this.strm.total_in == 0)) {
            i3 = this.strm.deflate(1);
        }
        if (this.level != i) {
            this.level = i;
            this.max_lazy_match = config_table[this.level].b;
            this.good_match = config_table[this.level].f3104a;
            this.nice_match = config_table[this.level].c;
            this.max_chain_length = config_table[this.level].d;
        }
        this.strategy = i2;
        return i3;
    }

    /* access modifiers changed from: package-private */
    public final int deflateReset() {
        ZStream zStream = this.strm;
        this.strm.total_out = 0;
        zStream.total_in = 0;
        this.strm.msg = null;
        this.strm.data_type = 2;
        this.pending = 0;
        this.pending_out = 0;
        if (this.wrap < 0) {
            this.wrap = -this.wrap;
        }
        this.status = this.wrap == 0 ? 113 : INIT_STATE;
        this.strm.adler.reset();
        this.last_flush = 0;
        tr_init();
        lm_init();
        return 0;
    }

    /* access modifiers changed from: package-private */
    public final int deflateSetDictionary(byte[] bArr, int i) {
        int i2;
        if (bArr == null || this.status != INIT_STATE) {
            return -2;
        }
        this.strm.adler.update(bArr, 0, i);
        if (i < 3) {
            return 0;
        }
        if (i > this.w_size - 262) {
            int i3 = this.w_size - 262;
            i2 = i - i3;
            i = i3;
        } else {
            i2 = 0;
        }
        System.arraycopy(bArr, i2, this.window, 0, i);
        this.strstart = i;
        this.block_start = i;
        this.ins_h = this.window[0] & GZIPHeader.OS_UNKNOWN;
        this.ins_h = ((this.ins_h << this.hash_shift) ^ (this.window[1] & GZIPHeader.OS_UNKNOWN)) & this.hash_mask;
        for (int i4 = 0; i4 <= i - 3; i4++) {
            this.ins_h = ((this.ins_h << this.hash_shift) ^ (this.window[i4 + 2] & GZIPHeader.OS_UNKNOWN)) & this.hash_mask;
            this.prev[this.w_mask & i4] = this.head[this.ins_h];
            this.head[this.ins_h] = (short) i4;
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public final int deflate_fast(int i) {
        short s;
        boolean z;
        short s2;
        int i2;
        short s3 = 0;
        while (true) {
            if (this.lookahead < 262) {
                fill_window();
                if (this.lookahead < 262 && i == 0) {
                    return 0;
                }
                if (this.lookahead == 0) {
                    flush_block_only(i == 4);
                    return this.strm.avail_out == 0 ? i == 4 ? 2 : 0 : i == 4 ? 3 : 1;
                }
            }
            if (this.lookahead >= 3) {
                this.ins_h = ((this.ins_h << this.hash_shift) ^ (this.window[this.strstart + 2] & GZIPHeader.OS_UNKNOWN)) & this.hash_mask;
                s = this.head[this.ins_h] & 65535;
                this.prev[this.strstart & this.w_mask] = this.head[this.ins_h];
                this.head[this.ins_h] = (short) this.strstart;
            } else {
                s = s3;
            }
            if (!(((long) s) == 0 || ((this.strstart - s) & 65535) > this.w_size - 262 || this.strategy == 2)) {
                this.match_length = longest_match(s);
            }
            if (this.match_length >= 3) {
                boolean _tr_tally = _tr_tally(this.strstart - this.match_start, this.match_length - 3);
                this.lookahead -= this.match_length;
                if (this.match_length > this.max_lazy_match || this.lookahead < 3) {
                    this.strstart += this.match_length;
                    this.match_length = 0;
                    this.ins_h = this.window[this.strstart] & GZIPHeader.OS_UNKNOWN;
                    this.ins_h = ((this.ins_h << this.hash_shift) ^ (this.window[this.strstart + 1] & GZIPHeader.OS_UNKNOWN)) & this.hash_mask;
                    boolean z2 = _tr_tally;
                    s3 = s;
                    z = z2;
                } else {
                    this.match_length--;
                    do {
                        this.strstart++;
                        this.ins_h = ((this.ins_h << this.hash_shift) ^ (this.window[this.strstart + 2] & GZIPHeader.OS_UNKNOWN)) & this.hash_mask;
                        s2 = this.head[this.ins_h] & 65535;
                        this.prev[this.strstart & this.w_mask] = this.head[this.ins_h];
                        this.head[this.ins_h] = (short) this.strstart;
                        i2 = this.match_length - 1;
                        this.match_length = i2;
                    } while (i2 != 0);
                    this.strstart++;
                    boolean z3 = _tr_tally;
                    s3 = s2;
                    z = z3;
                }
            } else {
                boolean _tr_tally2 = _tr_tally(0, this.window[this.strstart] & GZIPHeader.OS_UNKNOWN);
                this.lookahead--;
                this.strstart++;
                boolean z4 = _tr_tally2;
                s3 = s;
                z = z4;
            }
            if (z) {
                flush_block_only(false);
                if (this.strm.avail_out == 0) {
                    return 0;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final int deflate_slow(int i) {
        int i2;
        short s = 0;
        while (true) {
            if (this.lookahead < 262) {
                fill_window();
                if (this.lookahead < 262 && i == 0) {
                    return 0;
                }
                if (this.lookahead == 0) {
                    if (this.match_available != 0) {
                        _tr_tally(0, this.window[this.strstart - 1] & GZIPHeader.OS_UNKNOWN);
                        this.match_available = 0;
                    }
                    flush_block_only(i == 4);
                    return this.strm.avail_out == 0 ? i == 4 ? 2 : 0 : i == 4 ? 3 : 1;
                }
            }
            if (this.lookahead >= 3) {
                this.ins_h = ((this.ins_h << this.hash_shift) ^ (this.window[this.strstart + 2] & GZIPHeader.OS_UNKNOWN)) & this.hash_mask;
                s = this.head[this.ins_h] & 65535;
                this.prev[this.strstart & this.w_mask] = this.head[this.ins_h];
                this.head[this.ins_h] = (short) this.strstart;
            }
            this.prev_length = this.match_length;
            this.prev_match = this.match_start;
            this.match_length = 2;
            if (s != 0 && this.prev_length < this.max_lazy_match && ((this.strstart - s) & 65535) <= this.w_size - 262) {
                if (this.strategy != 2) {
                    this.match_length = longest_match(s);
                }
                if (this.match_length <= 5 && (this.strategy == 1 || (this.match_length == 3 && this.strstart - this.match_start > 4096))) {
                    this.match_length = 2;
                }
            }
            if (this.prev_length >= 3 && this.match_length <= this.prev_length) {
                int i3 = (this.strstart + this.lookahead) - 3;
                boolean _tr_tally = _tr_tally((this.strstart - 1) - this.prev_match, this.prev_length - 3);
                this.lookahead -= this.prev_length - 1;
                this.prev_length -= 2;
                do {
                    int i4 = this.strstart + 1;
                    this.strstart = i4;
                    if (i4 <= i3) {
                        this.ins_h = ((this.ins_h << this.hash_shift) ^ (this.window[this.strstart + 2] & GZIPHeader.OS_UNKNOWN)) & this.hash_mask;
                        s = this.head[this.ins_h] & 65535;
                        this.prev[this.strstart & this.w_mask] = this.head[this.ins_h];
                        this.head[this.ins_h] = (short) this.strstart;
                    }
                    i2 = this.prev_length - 1;
                    this.prev_length = i2;
                } while (i2 != 0);
                this.match_available = 0;
                this.match_length = 2;
                this.strstart++;
                if (_tr_tally) {
                    flush_block_only(false);
                    if (this.strm.avail_out == 0) {
                        return 0;
                    }
                } else {
                    continue;
                }
            } else if (this.match_available != 0) {
                if (_tr_tally(0, this.window[this.strstart - 1] & GZIPHeader.OS_UNKNOWN)) {
                    flush_block_only(false);
                }
                this.strstart++;
                this.lookahead--;
                if (this.strm.avail_out == 0) {
                    return 0;
                }
            } else {
                this.match_available = 1;
                this.strstart++;
                this.lookahead--;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final int deflate_stored(int i) {
        int i2 = 65535;
        if (65535 > this.pending_buf_size - 5) {
            i2 = this.pending_buf_size - 5;
        }
        while (true) {
            if (this.lookahead <= 1) {
                fill_window();
                if (this.lookahead == 0 && i == 0) {
                    return 0;
                }
                if (this.lookahead == 0) {
                    flush_block_only(i == 4);
                    return this.strm.avail_out == 0 ? i == 4 ? 2 : 0 : i == 4 ? 3 : 1;
                }
            }
            this.strstart += this.lookahead;
            this.lookahead = 0;
            int i3 = this.block_start + i2;
            if (this.strstart == 0 || this.strstart >= i3) {
                this.lookahead = this.strstart - i3;
                this.strstart = i3;
                flush_block_only(false);
                if (this.strm.avail_out == 0) {
                    return 0;
                }
            }
            if (this.strstart - this.block_start >= this.w_size - 262) {
                flush_block_only(false);
                if (this.strm.avail_out == 0) {
                    return 0;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void fill_window() {
        int i;
        do {
            int i2 = (this.window_size - this.lookahead) - this.strstart;
            if (i2 == 0 && this.strstart == 0 && this.lookahead == 0) {
                i = this.w_size;
            } else if (i2 == -1) {
                i = i2 - 1;
            } else if (this.strstart >= (this.w_size + this.w_size) - 262) {
                System.arraycopy(this.window, this.w_size, this.window, 0, this.w_size);
                this.match_start -= this.w_size;
                this.strstart -= this.w_size;
                this.block_start -= this.w_size;
                int i3 = this.hash_size;
                int i4 = i3;
                do {
                    i3--;
                    short s = this.head[i3] & 65535;
                    this.head[i3] = s >= this.w_size ? (short) (s - this.w_size) : 0;
                    i4--;
                } while (i4 != 0);
                int i5 = this.w_size;
                int i6 = i5;
                do {
                    i5--;
                    short s2 = this.prev[i5] & 65535;
                    this.prev[i5] = s2 >= this.w_size ? (short) (s2 - this.w_size) : 0;
                    i6--;
                } while (i6 != 0);
                i = this.w_size + i2;
            } else {
                i = i2;
            }
            if (this.strm.avail_in != 0) {
                this.lookahead = this.strm.read_buf(this.window, this.strstart + this.lookahead, i) + this.lookahead;
                if (this.lookahead >= 3) {
                    this.ins_h = this.window[this.strstart] & GZIPHeader.OS_UNKNOWN;
                    this.ins_h = ((this.ins_h << this.hash_shift) ^ (this.window[this.strstart + 1] & GZIPHeader.OS_UNKNOWN)) & this.hash_mask;
                }
                if (this.lookahead >= 262) {
                    return;
                }
            } else {
                return;
            }
        } while (this.strm.avail_in != 0);
    }

    /* access modifiers changed from: package-private */
    public final void flush_block_only(boolean z) {
        _tr_flush_block(this.block_start >= 0 ? this.block_start : -1, this.strstart - this.block_start, z);
        this.block_start = this.strstart;
        this.strm.flush_pending();
    }

    /* access modifiers changed from: package-private */
    public final synchronized GZIPHeader getGZIPHeader() {
        if (this.gheader == null) {
            this.gheader = new GZIPHeader();
        }
        return this.gheader;
    }

    /* access modifiers changed from: package-private */
    public final void init_block() {
        for (int i = 0; i < L_CODES; i++) {
            this.dyn_ltree[i * 2] = 0;
        }
        for (int i2 = 0; i2 < D_CODES; i2++) {
            this.dyn_dtree[i2 * 2] = 0;
        }
        for (int i3 = 0; i3 < BL_CODES; i3++) {
            this.bl_tree[i3 * 2] = 0;
        }
        this.dyn_ltree[512] = 1;
        this.static_len = 0;
        this.opt_len = 0;
        this.matches = 0;
        this.last_lit = 0;
    }

    /* access modifiers changed from: package-private */
    public final void lm_init() {
        this.window_size = this.w_size * 2;
        this.head[this.hash_size - 1] = 0;
        for (int i = 0; i < this.hash_size - 1; i++) {
            this.head[i] = 0;
        }
        this.max_lazy_match = config_table[this.level].b;
        this.good_match = config_table[this.level].f3104a;
        this.nice_match = config_table[this.level].c;
        this.max_chain_length = config_table[this.level].d;
        this.strstart = 0;
        this.block_start = 0;
        this.lookahead = 0;
        this.prev_length = 2;
        this.match_length = 2;
        this.match_available = 0;
        this.ins_h = 0;
    }

    /* access modifiers changed from: package-private */
    public final int longest_match(int i) {
        int i2;
        int i3;
        int i4;
        int i5;
        byte b;
        byte b2;
        int i6 = this.max_chain_length;
        int i7 = this.strstart;
        int i8 = this.prev_length;
        int i9 = this.strstart > this.w_size + -262 ? this.strstart - (this.w_size - 262) : 0;
        int i10 = this.nice_match;
        int i11 = this.w_mask;
        int i12 = this.strstart + 258;
        byte b3 = this.window[(i7 + i8) - 1];
        byte b4 = this.window[i7 + i8];
        if (this.prev_length >= this.good_match) {
            i6 >>= 2;
        }
        if (i10 > this.lookahead) {
            byte b5 = b4;
            b = b3;
            i5 = this.lookahead;
            i4 = i8;
            i3 = i7;
            i2 = i6;
            b2 = b5;
        } else {
            byte b6 = b4;
            b = b3;
            i5 = i10;
            i4 = i8;
            i3 = i7;
            i2 = i6;
            b2 = b6;
        }
        while (true) {
            if (this.window[i + i4] == b2 && this.window[(i + i4) - 1] == b && this.window[i] == this.window[i3]) {
                int i13 = i + 1;
                if (this.window[i13] == this.window[i3 + 1]) {
                    int i14 = i3 + 2;
                    int i15 = i13 + 1;
                    do {
                        i14++;
                        int i16 = i15 + 1;
                        if (this.window[i14] != this.window[i16]) {
                            break;
                        }
                        i14++;
                        int i17 = i16 + 1;
                        if (this.window[i14] != this.window[i17]) {
                            break;
                        }
                        i14++;
                        int i18 = i17 + 1;
                        if (this.window[i14] != this.window[i18]) {
                            break;
                        }
                        i14++;
                        int i19 = i18 + 1;
                        if (this.window[i14] != this.window[i19]) {
                            break;
                        }
                        i14++;
                        int i20 = i19 + 1;
                        if (this.window[i14] != this.window[i20]) {
                            break;
                        }
                        i14++;
                        int i21 = i20 + 1;
                        if (this.window[i14] != this.window[i21]) {
                            break;
                        }
                        i14++;
                        int i22 = i21 + 1;
                        if (this.window[i14] != this.window[i22]) {
                            break;
                        }
                        i14++;
                        i15 = i22 + 1;
                        if (this.window[i14] != this.window[i15]) {
                            break;
                        }
                    } while (i14 < i12);
                    int i23 = 258 - (i12 - i14);
                    int i24 = i12 - 258;
                    if (i23 > i4) {
                        this.match_start = i;
                        if (i23 >= i5) {
                            i4 = i23;
                            break;
                        }
                        b = this.window[(i24 + i23) - 1];
                        b2 = this.window[i24 + i23];
                        i4 = i23;
                        i3 = i24;
                    } else {
                        i3 = i24;
                    }
                }
            }
            i = this.prev[i & i11] & 65535;
            if (i > i9) {
                i2--;
                if (i2 == 0) {
                    break;
                }
            } else {
                break;
            }
        }
        return i4 <= this.lookahead ? i4 : this.lookahead;
    }

    /* access modifiers changed from: package-private */
    public final void pqdownheap(short[] sArr, int i) {
        int i2 = this.heap[i];
        int i3 = i << 1;
        while (i3 <= this.heap_len) {
            int i4 = (i3 >= this.heap_len || !smaller(sArr, this.heap[i3 + 1], this.heap[i3], this.depth)) ? i3 : i3 + 1;
            if (smaller(sArr, i2, this.heap[i4], this.depth)) {
                break;
            }
            this.heap[i] = this.heap[i4];
            i3 = i4 << 1;
            i = i4;
        }
        this.heap[i] = i2;
    }

    /* access modifiers changed from: package-private */
    public final void putShortMSB(int i) {
        put_byte((byte) (i >> 8));
        put_byte((byte) i);
    }

    /* access modifiers changed from: package-private */
    public final void put_byte(byte b) {
        byte[] bArr = this.pending_buf;
        int i = this.pending;
        this.pending = i + 1;
        bArr[i] = b;
    }

    /* access modifiers changed from: package-private */
    public final void put_byte(byte[] bArr, int i, int i2) {
        System.arraycopy(bArr, i, this.pending_buf, this.pending, i2);
        this.pending += i2;
    }

    /* access modifiers changed from: package-private */
    public final void put_short(int i) {
        put_byte((byte) i);
        put_byte((byte) (i >>> 8));
    }

    /* access modifiers changed from: package-private */
    public final void scan_tree(short[] sArr, int i) {
        int i2;
        int i3;
        short s = -1;
        short s2 = sArr[1];
        if (s2 == 0) {
            i3 = 138;
            i2 = 3;
        } else {
            i2 = 4;
            i3 = 7;
        }
        sArr[((i + 1) * 2) + 1] = -1;
        int i4 = 0;
        int i5 = 0;
        while (i5 <= i) {
            short s3 = sArr[((i5 + 1) * 2) + 1];
            i4++;
            if (i4 >= i3 || s2 != s3) {
                if (i4 < i2) {
                    short[] sArr2 = this.bl_tree;
                    int i6 = s2 * 2;
                    sArr2[i6] = (short) (i4 + sArr2[i6]);
                } else if (s2 != 0) {
                    if (s2 != s) {
                        short[] sArr3 = this.bl_tree;
                        int i7 = s2 * 2;
                        sArr3[i7] = (short) (sArr3[i7] + 1);
                    }
                    short[] sArr4 = this.bl_tree;
                    sArr4[PRESET_DICT] = (short) (sArr4[PRESET_DICT] + 1);
                } else if (i4 <= 10) {
                    short[] sArr5 = this.bl_tree;
                    sArr5[34] = (short) (sArr5[34] + 1);
                } else {
                    short[] sArr6 = this.bl_tree;
                    sArr6[36] = (short) (sArr6[36] + 1);
                }
                if (s3 == 0) {
                    i3 = 138;
                    i4 = 0;
                    s = s2;
                    i2 = 3;
                } else if (s2 == s3) {
                    i3 = 6;
                    i4 = 0;
                    s = s2;
                    i2 = 3;
                } else {
                    i2 = 4;
                    i3 = 7;
                    i4 = 0;
                    s = s2;
                }
            }
            i5++;
            s2 = s3;
        }
    }

    /* access modifiers changed from: package-private */
    public final void send_all_trees(int i, int i2, int i3) {
        send_bits(i - 257, 5);
        send_bits(i2 - 1, 5);
        send_bits(i3 - 4, 4);
        for (int i4 = 0; i4 < i3; i4++) {
            send_bits(this.bl_tree[(i.d[i4] * 2) + 1], 3);
        }
        send_tree(this.dyn_ltree, i - 1);
        send_tree(this.dyn_dtree, i2 - 1);
    }

    /* access modifiers changed from: package-private */
    public final void send_bits(int i, int i2) {
        if (this.bi_valid > 16 - i2) {
            this.bi_buf = (short) (this.bi_buf | ((i << this.bi_valid) & 65535));
            put_short(this.bi_buf);
            this.bi_buf = (short) (i >>> (16 - this.bi_valid));
            this.bi_valid += i2 - 16;
            return;
        }
        this.bi_buf = (short) (this.bi_buf | ((i << this.bi_valid) & 65535));
        this.bi_valid += i2;
    }

    /* access modifiers changed from: package-private */
    public final void send_code(int i, short[] sArr) {
        int i2 = i * 2;
        send_bits(sArr[i2] & 65535, sArr[i2 + 1] & 65535);
    }

    /* access modifiers changed from: package-private */
    public final void send_tree(short[] sArr, int i) {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        short s = -1;
        short s2 = sArr[1];
        if (s2 == 0) {
            i2 = 3;
            i3 = 138;
        } else {
            i2 = 4;
            i3 = 7;
        }
        int i7 = i3;
        int i8 = 0;
        int i9 = i2;
        int i10 = 0;
        while (i8 <= i) {
            short s3 = sArr[((i8 + 1) * 2) + 1];
            int i11 = i10 + 1;
            if (i11 >= i7 || s2 != s3) {
                if (i11 < i9) {
                    do {
                        send_code(s2, this.bl_tree);
                        i11--;
                    } while (i11 != 0);
                } else if (s2 != 0) {
                    if (s2 != s) {
                        send_code(s2, this.bl_tree);
                        i11--;
                    }
                    send_code(16, this.bl_tree);
                    send_bits(i11 - 3, 2);
                } else if (i11 <= 10) {
                    send_code(17, this.bl_tree);
                    send_bits(i11 - 3, 3);
                } else {
                    send_code(REPZ_11_138, this.bl_tree);
                    send_bits(i11 - 11, 7);
                }
                if (s3 == 0) {
                    i6 = 3;
                    i5 = 138;
                    i4 = 0;
                    s = s2;
                } else if (s2 == s3) {
                    i5 = 6;
                    i4 = 0;
                    s = s2;
                    i6 = 3;
                } else {
                    i6 = 4;
                    i5 = 7;
                    i4 = 0;
                    s = s2;
                }
            } else {
                int i12 = i9;
                i5 = i7;
                i4 = i11;
                i6 = i12;
            }
            i8++;
            s2 = s3;
            int i13 = i4;
            i7 = i5;
            i9 = i6;
            i10 = i13;
        }
    }

    /* access modifiers changed from: package-private */
    public final void set_data_type() {
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (i3 < 7) {
            i2 += this.dyn_ltree[i3 * 2];
            i3++;
        }
        int i4 = 0;
        while (i3 < 128) {
            i4 += this.dyn_ltree[i3 * 2];
            i3++;
        }
        while (i3 < 256) {
            i2 += this.dyn_ltree[i3 * 2];
            i3++;
        }
        if (i2 <= (i4 >>> 2)) {
            i = 1;
        }
        this.data_type = (byte) i;
    }

    /* access modifiers changed from: package-private */
    public final void tr_init() {
        this.l_desc.h = this.dyn_ltree;
        this.l_desc.j = h.c;
        this.d_desc.h = this.dyn_dtree;
        this.d_desc.j = h.d;
        this.bl_desc.h = this.bl_tree;
        this.bl_desc.j = h.e;
        this.bi_buf = 0;
        this.bi_valid = 0;
        this.last_eob_len = 8;
        init_block();
    }
}
