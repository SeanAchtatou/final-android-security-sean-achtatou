package com.jcraft.jzlib;

final class d {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f3106a;
    private int b;
    private int c;
    private int[] d;
    private int e = 0;
    private int f;
    private int g;
    private int h;
    private int i;
    private byte j;
    private byte k;
    private int[] l;
    private int m;
    private int[] n;
    private int o;
    private final ZStream p;
    private final c q;

    static {
        int[] iArr = new int[17];
        iArr[1] = 1;
        iArr[2] = 3;
        iArr[3] = 7;
        iArr[4] = 15;
        iArr[5] = 31;
        iArr[6] = 63;
        iArr[7] = 127;
        iArr[8] = 255;
        iArr[9] = 511;
        iArr[10] = 1023;
        iArr[11] = 2047;
        iArr[12] = 4095;
        iArr[13] = 8191;
        iArr[14] = 16383;
        iArr[15] = 32767;
        iArr[16] = 65535;
        f3106a = iArr;
    }

    d(ZStream zStream, c cVar) {
        this.p = zStream;
        this.q = cVar;
    }

    static void a() {
    }

    /* JADX WARN: Type inference failed for: r4v11, types: [int] */
    /* JADX WARN: Type inference failed for: r5v27 */
    /* JADX WARN: Type inference failed for: r4v18, types: [int] */
    /* JADX WARN: Type inference failed for: r4v24, types: [int] */
    /* JADX WARN: Type inference failed for: r4v31, types: [int] */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x013f, code lost:
        if ((r4 >> 3) >= r1) goto L_0x0143;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0141, code lost:
        r1 = r4 >> 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0143, code lost:
        r3 = r3 - r1;
        r17.b = r5;
        r17.f3105a = r4 - (r1 << 3);
        r18.avail_in = r2 + r1;
        r18.total_in += (long) (r3 - r18.next_in_index);
        r18.next_in_index = r3;
        r17.f = r6;
        r23 = 0;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int a(int r23) {
        /*
            r22 = this;
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            int r4 = r1.next_in_index
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            int r3 = r1.avail_in
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r6 = r1.b
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r5 = r1.f3105a
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r2 = r1.f
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r1 = r1.e
            if (r2 >= r1) goto L_0x0070
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r1 = r1.e
            int r1 = r1 - r2
            int r1 = r1 + -1
        L_0x002f:
            r0 = r22
            int r7 = r0.b
            switch(r7) {
                case 0: goto L_0x0078;
                case 1: goto L_0x0432;
                case 2: goto L_0x0552;
                case 3: goto L_0x0587;
                case 4: goto L_0x06de;
                case 5: goto L_0x06fb;
                case 6: goto L_0x0863;
                case 7: goto L_0x0946;
                case 8: goto L_0x09c8;
                case 9: goto L_0x0a03;
                default: goto L_0x0036;
            }
        L_0x0036:
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.b = r6
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.f3105a = r5
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            r1.avail_in = r3
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            long r5 = r1.total_in
            r0 = r22
            com.jcraft.jzlib.ZStream r3 = r0.p
            int r3 = r3.next_in_index
            int r3 = r4 - r3
            long r7 = (long) r3
            long r5 = r5 + r7
            r1.total_in = r5
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            r1.next_in_index = r4
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.f = r2
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r2 = -2
            int r1 = r1.b(r2)
        L_0x006f:
            return r1
        L_0x0070:
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r1 = r1.d
            int r1 = r1 - r2
            goto L_0x002f
        L_0x0078:
            r7 = 258(0x102, float:3.62E-43)
            if (r1 < r7) goto L_0x0415
            r7 = 10
            if (r3 < r7) goto L_0x0415
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.b = r6
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.f3105a = r5
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            r1.avail_in = r3
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            long r5 = r1.total_in
            r0 = r22
            com.jcraft.jzlib.ZStream r3 = r0.p
            int r3 = r3.next_in_index
            int r3 = r4 - r3
            long r7 = (long) r3
            long r5 = r5 + r7
            r1.total_in = r5
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            r1.next_in_index = r4
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.f = r2
            r0 = r22
            byte r7 = r0.j
            r0 = r22
            byte r8 = r0.k
            r0 = r22
            int[] r13 = r0.l
            r0 = r22
            int r14 = r0.m
            r0 = r22
            int[] r15 = r0.n
            r0 = r22
            int r0 = r0.o
            r16 = r0
            r0 = r22
            com.jcraft.jzlib.c r0 = r0.q
            r17 = r0
            r0 = r22
            com.jcraft.jzlib.ZStream r0 = r0.p
            r18 = r0
            r0 = r18
            int r3 = r0.next_in_index
            r0 = r18
            int r2 = r0.avail_in
            r0 = r17
            int r5 = r0.b
            r0 = r17
            int r4 = r0.f3105a
            r0 = r17
            int r6 = r0.f
            r0 = r17
            int r1 = r0.e
            if (r6 >= r1) goto L_0x01ac
            r0 = r17
            int r1 = r0.e
            int r1 = r1 - r6
            int r1 = r1 + -1
        L_0x00f7:
            int[] r9 = com.jcraft.jzlib.d.f3106a
            r19 = r9[r7]
            int[] r7 = com.jcraft.jzlib.d.f3106a
            r20 = r7[r8]
            r7 = r1
            r8 = r2
            r9 = r3
            r2 = r4
            r3 = r5
        L_0x0104:
            r1 = 20
            if (r2 < r1) goto L_0x01b3
            r5 = r3 & r19
            int r1 = r14 + r5
            int r1 = r1 * 3
            r4 = r13[r1]
            if (r4 != 0) goto L_0x01c7
            int r4 = r1 + 1
            r4 = r13[r4]
            int r5 = r3 >> r4
            int r3 = r1 + 1
            r3 = r13[r3]
            int r4 = r2 - r3
            r0 = r17
            byte[] r2 = r0.c
            int r3 = r6 + 1
            int r1 = r1 + 2
            r1 = r13[r1]
            byte r1 = (byte) r1
            r2[r6] = r1
            int r1 = r7 + -1
            r6 = r3
            r2 = r8
            r3 = r9
        L_0x0130:
            r7 = 258(0x102, float:3.62E-43)
            if (r1 < r7) goto L_0x0138
            r7 = 10
            if (r2 >= r7) goto L_0x0a41
        L_0x0138:
            r0 = r18
            int r1 = r0.avail_in
            int r1 = r1 - r2
            int r7 = r4 >> 3
            if (r7 >= r1) goto L_0x0143
            int r1 = r4 >> 3
        L_0x0143:
            int r2 = r2 + r1
            int r3 = r3 - r1
            int r1 = r1 << 3
            int r1 = r4 - r1
            r0 = r17
            r0.b = r5
            r0 = r17
            r0.f3105a = r1
            r0 = r18
            r0.avail_in = r2
            r0 = r18
            long r1 = r0.total_in
            r0 = r18
            int r4 = r0.next_in_index
            int r4 = r3 - r4
            long r4 = (long) r4
            long r1 = r1 + r4
            r0 = r18
            r0.total_in = r1
            r0 = r18
            r0.next_in_index = r3
            r0 = r17
            r0.f = r6
            r23 = 0
        L_0x016f:
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            int r4 = r1.next_in_index
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            int r3 = r1.avail_in
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r6 = r1.b
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r5 = r1.f3105a
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r2 = r1.f
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r1 = r1.e
            if (r2 >= r1) goto L_0x0408
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r1 = r1.e
            int r1 = r1 - r2
            int r1 = r1 + -1
        L_0x019e:
            if (r23 == 0) goto L_0x0415
            r7 = 1
            r0 = r23
            if (r0 != r7) goto L_0x0411
            r7 = 7
        L_0x01a6:
            r0 = r22
            r0.b = r7
            goto L_0x002f
        L_0x01ac:
            r0 = r17
            int r1 = r0.d
            int r1 = r1 - r6
            goto L_0x00f7
        L_0x01b3:
            int r1 = r8 + -1
            r0 = r18
            byte[] r5 = r0.next_in
            int r4 = r9 + 1
            byte r5 = r5[r9]
            r5 = r5 & 255(0xff, float:3.57E-43)
            int r5 = r5 << r2
            r3 = r3 | r5
            int r2 = r2 + 8
            r8 = r1
            r9 = r4
            goto L_0x0104
        L_0x01c7:
            int r10 = r1 + 1
            r10 = r13[r10]
            int r3 = r3 >> r10
            int r10 = r1 + 1
            r10 = r13[r10]
            int r2 = r2 - r10
            r10 = r4 & 16
            if (r10 == 0) goto L_0x0350
            r4 = r4 & 15
            int r1 = r1 + 2
            r1 = r13[r1]
            int[] r5 = com.jcraft.jzlib.d.f3106a
            r5 = r5[r4]
            r5 = r5 & r3
            int r12 = r1 + r5
            int r3 = r3 >> r4
            int r2 = r2 - r4
        L_0x01e4:
            r1 = 15
            if (r2 < r1) goto L_0x0267
            r5 = r3 & r20
            int r1 = r16 + r5
            int r1 = r1 * 3
            r4 = r15[r1]
        L_0x01f0:
            int r10 = r1 + 1
            r10 = r15[r10]
            int r3 = r3 >> r10
            int r10 = r1 + 1
            r10 = r15[r10]
            int r2 = r2 - r10
            r10 = r4 & 16
            if (r10 == 0) goto L_0x02f7
            r5 = r4 & 15
            r4 = r3
            r3 = r2
        L_0x0202:
            if (r3 < r5) goto L_0x027a
            int r1 = r1 + 2
            r1 = r15[r1]
            int[] r2 = com.jcraft.jzlib.d.f3106a
            r2 = r2[r5]
            r2 = r2 & r4
            int r1 = r1 + r2
            int r11 = r4 >> r5
            int r10 = r3 - r5
            int r7 = r7 - r12
            if (r6 < r1) goto L_0x02a0
            int r1 = r6 - r1
            int r2 = r6 - r1
            if (r2 <= 0) goto L_0x028d
            r2 = 2
            int r3 = r6 - r1
            if (r2 <= r3) goto L_0x028d
            r0 = r17
            byte[] r2 = r0.c
            int r4 = r6 + 1
            r0 = r17
            byte[] r3 = r0.c
            int r5 = r1 + 1
            byte r1 = r3[r1]
            r2[r6] = r1
            r0 = r17
            byte[] r1 = r0.c
            int r2 = r4 + 1
            r0 = r17
            byte[] r6 = r0.c
            int r3 = r5 + 1
            byte r5 = r6[r5]
            r1[r4] = r5
            int r1 = r12 + -2
        L_0x0242:
            int r4 = r2 - r3
            if (r4 <= 0) goto L_0x02e3
            int r4 = r2 - r3
            if (r1 <= r4) goto L_0x02e3
            r4 = r3
        L_0x024b:
            r0 = r17
            byte[] r6 = r0.c
            int r3 = r2 + 1
            r0 = r17
            byte[] r12 = r0.c
            int r5 = r4 + 1
            byte r4 = r12[r4]
            r6[r2] = r4
            int r1 = r1 + -1
            if (r1 != 0) goto L_0x0a48
            r1 = r7
            r6 = r3
            r2 = r8
            r4 = r10
            r5 = r11
            r3 = r9
            goto L_0x0130
        L_0x0267:
            int r8 = r8 + -1
            r0 = r18
            byte[] r4 = r0.next_in
            int r1 = r9 + 1
            byte r4 = r4[r9]
            r4 = r4 & 255(0xff, float:3.57E-43)
            int r4 = r4 << r2
            r3 = r3 | r4
            int r2 = r2 + 8
            r9 = r1
            goto L_0x01e4
        L_0x027a:
            int r8 = r8 + -1
            r0 = r18
            byte[] r10 = r0.next_in
            int r2 = r9 + 1
            byte r9 = r10[r9]
            r9 = r9 & 255(0xff, float:3.57E-43)
            int r9 = r9 << r3
            r4 = r4 | r9
            int r3 = r3 + 8
            r9 = r2
            goto L_0x0202
        L_0x028d:
            r0 = r17
            byte[] r2 = r0.c
            r0 = r17
            byte[] r3 = r0.c
            r4 = 2
            java.lang.System.arraycopy(r2, r1, r3, r6, r4)
            int r2 = r6 + 2
            int r3 = r1 + 2
            int r1 = r12 + -2
            goto L_0x0242
        L_0x02a0:
            int r1 = r6 - r1
        L_0x02a2:
            r0 = r17
            int r2 = r0.d
            int r1 = r1 + r2
            if (r1 < 0) goto L_0x02a2
            r0 = r17
            int r2 = r0.d
            int r2 = r2 - r1
            if (r12 <= r2) goto L_0x0a51
            int r12 = r12 - r2
            int r3 = r6 - r1
            if (r3 <= 0) goto L_0x02d6
            int r3 = r6 - r1
            if (r2 <= r3) goto L_0x02d6
            r3 = r2
            r4 = r1
            r1 = r6
        L_0x02bc:
            r0 = r17
            byte[] r6 = r0.c
            int r2 = r1 + 1
            r0 = r17
            byte[] r0 = r0.c
            r21 = r0
            int r5 = r4 + 1
            byte r4 = r21[r4]
            r6[r1] = r4
            int r1 = r3 + -1
            if (r1 != 0) goto L_0x0a4c
        L_0x02d2:
            r3 = 0
            r1 = r12
            goto L_0x0242
        L_0x02d6:
            r0 = r17
            byte[] r3 = r0.c
            r0 = r17
            byte[] r4 = r0.c
            java.lang.System.arraycopy(r3, r1, r4, r6, r2)
            int r2 = r2 + r6
            goto L_0x02d2
        L_0x02e3:
            r0 = r17
            byte[] r4 = r0.c
            r0 = r17
            byte[] r5 = r0.c
            java.lang.System.arraycopy(r4, r3, r5, r2, r1)
            int r6 = r2 + r1
            r1 = r7
            r2 = r8
            r3 = r9
            r4 = r10
            r5 = r11
            goto L_0x0130
        L_0x02f7:
            r10 = r4 & 64
            if (r10 != 0) goto L_0x030f
            int r1 = r1 + 2
            r1 = r15[r1]
            int r1 = r1 + r5
            int[] r5 = com.jcraft.jzlib.d.f3106a
            r4 = r5[r4]
            r4 = r4 & r3
            int r5 = r1 + r4
            int r1 = r16 + r5
            int r1 = r1 * 3
            r4 = r15[r1]
            goto L_0x01f0
        L_0x030f:
            java.lang.String r1 = "invalid distance code"
            r0 = r18
            r0.msg = r1
            r0 = r18
            int r1 = r0.avail_in
            int r1 = r1 - r8
            int r4 = r2 >> 3
            if (r4 >= r1) goto L_0x0320
            int r1 = r2 >> 3
        L_0x0320:
            int r4 = r8 + r1
            int r5 = r9 - r1
            int r1 = r1 << 3
            int r1 = r2 - r1
            r0 = r17
            r0.b = r3
            r0 = r17
            r0.f3105a = r1
            r0 = r18
            r0.avail_in = r4
            r0 = r18
            long r1 = r0.total_in
            r0 = r18
            int r3 = r0.next_in_index
            int r3 = r5 - r3
            long r3 = (long) r3
            long r1 = r1 + r3
            r0 = r18
            r0.total_in = r1
            r0 = r18
            r0.next_in_index = r5
            r0 = r17
            r0.f = r6
            r23 = -3
            goto L_0x016f
        L_0x0350:
            r10 = r4 & 64
            if (r10 != 0) goto L_0x0388
            int r1 = r1 + 2
            r1 = r13[r1]
            int r1 = r1 + r5
            int[] r5 = com.jcraft.jzlib.d.f3106a
            r4 = r5[r4]
            r4 = r4 & r3
            int r5 = r1 + r4
            int r1 = r14 + r5
            int r1 = r1 * 3
            r4 = r13[r1]
            if (r4 != 0) goto L_0x01c7
            int r4 = r1 + 1
            r4 = r13[r4]
            int r5 = r3 >> r4
            int r3 = r1 + 1
            r3 = r13[r3]
            int r4 = r2 - r3
            r0 = r17
            byte[] r2 = r0.c
            int r3 = r6 + 1
            int r1 = r1 + 2
            r1 = r13[r1]
            byte r1 = (byte) r1
            r2[r6] = r1
            int r1 = r7 + -1
            r6 = r3
            r2 = r8
            r3 = r9
            goto L_0x0130
        L_0x0388:
            r1 = r4 & 32
            if (r1 == 0) goto L_0x03c7
            r0 = r18
            int r1 = r0.avail_in
            int r1 = r1 - r8
            int r4 = r2 >> 3
            if (r4 >= r1) goto L_0x0397
            int r1 = r2 >> 3
        L_0x0397:
            int r4 = r8 + r1
            int r5 = r9 - r1
            int r1 = r1 << 3
            int r1 = r2 - r1
            r0 = r17
            r0.b = r3
            r0 = r17
            r0.f3105a = r1
            r0 = r18
            r0.avail_in = r4
            r0 = r18
            long r1 = r0.total_in
            r0 = r18
            int r3 = r0.next_in_index
            int r3 = r5 - r3
            long r3 = (long) r3
            long r1 = r1 + r3
            r0 = r18
            r0.total_in = r1
            r0 = r18
            r0.next_in_index = r5
            r0 = r17
            r0.f = r6
            r23 = 1
            goto L_0x016f
        L_0x03c7:
            java.lang.String r1 = "invalid literal/length code"
            r0 = r18
            r0.msg = r1
            r0 = r18
            int r1 = r0.avail_in
            int r1 = r1 - r8
            int r4 = r2 >> 3
            if (r4 >= r1) goto L_0x03d8
            int r1 = r2 >> 3
        L_0x03d8:
            int r4 = r8 + r1
            int r5 = r9 - r1
            int r1 = r1 << 3
            int r1 = r2 - r1
            r0 = r17
            r0.b = r3
            r0 = r17
            r0.f3105a = r1
            r0 = r18
            r0.avail_in = r4
            r0 = r18
            long r1 = r0.total_in
            r0 = r18
            int r3 = r0.next_in_index
            int r3 = r5 - r3
            long r3 = (long) r3
            long r1 = r1 + r3
            r0 = r18
            r0.total_in = r1
            r0 = r18
            r0.next_in_index = r5
            r0 = r17
            r0.f = r6
            r23 = -3
            goto L_0x016f
        L_0x0408:
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r1 = r1.d
            int r1 = r1 - r2
            goto L_0x019e
        L_0x0411:
            r7 = 9
            goto L_0x01a6
        L_0x0415:
            r0 = r22
            byte r7 = r0.j
            r0 = r22
            r0.f = r7
            r0 = r22
            int[] r7 = r0.l
            r0 = r22
            r0.d = r7
            r0 = r22
            int r7 = r0.m
            r0 = r22
            r0.e = r7
            r7 = 1
            r0 = r22
            r0.b = r7
        L_0x0432:
            r0 = r22
            int r8 = r0.f
            r7 = r5
        L_0x0437:
            if (r7 < r8) goto L_0x0473
            r0 = r22
            int r5 = r0.e
            int[] r9 = com.jcraft.jzlib.d.f3106a
            r8 = r9[r8]
            r8 = r8 & r6
            int r5 = r5 + r8
            int r8 = r5 * 3
            r0 = r22
            int[] r5 = r0.d
            int r9 = r8 + 1
            r5 = r5[r9]
            int r6 = r6 >>> r5
            r0 = r22
            int[] r5 = r0.d
            int r9 = r8 + 1
            r5 = r5[r9]
            int r5 = r7 - r5
            r0 = r22
            int[] r7 = r0.d
            r7 = r7[r8]
            if (r7 != 0) goto L_0x04c8
            r0 = r22
            int[] r7 = r0.d
            int r8 = r8 + 2
            r7 = r7[r8]
            r0 = r22
            r0.g = r7
            r7 = 6
            r0 = r22
            r0.b = r7
            goto L_0x002f
        L_0x0473:
            if (r3 == 0) goto L_0x048c
            r23 = 0
            int r3 = r3 + -1
            r0 = r22
            com.jcraft.jzlib.ZStream r5 = r0.p
            byte[] r9 = r5.next_in
            int r5 = r4 + 1
            byte r4 = r9[r4]
            r4 = r4 & 255(0xff, float:3.57E-43)
            int r4 = r4 << r7
            r6 = r6 | r4
            int r4 = r7 + 8
            r7 = r4
            r4 = r5
            goto L_0x0437
        L_0x048c:
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.b = r6
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.f3105a = r7
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            r1.avail_in = r3
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            long r5 = r1.total_in
            r0 = r22
            com.jcraft.jzlib.ZStream r3 = r0.p
            int r3 = r3.next_in_index
            int r3 = r4 - r3
            long r7 = (long) r3
            long r5 = r5 + r7
            r1.total_in = r5
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            r1.next_in_index = r4
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.f = r2
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r0 = r23
            int r1 = r1.b(r0)
            goto L_0x006f
        L_0x04c8:
            r9 = r7 & 16
            if (r9 == 0) goto L_0x04e5
            r7 = r7 & 15
            r0 = r22
            r0.h = r7
            r0 = r22
            int[] r7 = r0.d
            int r8 = r8 + 2
            r7 = r7[r8]
            r0 = r22
            r0.c = r7
            r7 = 2
            r0 = r22
            r0.b = r7
            goto L_0x002f
        L_0x04e5:
            r9 = r7 & 64
            if (r9 != 0) goto L_0x04fe
            r0 = r22
            r0.f = r7
            int r7 = r8 / 3
            r0 = r22
            int[] r9 = r0.d
            int r8 = r8 + 2
            r8 = r9[r8]
            int r7 = r7 + r8
            r0 = r22
            r0.e = r7
            goto L_0x002f
        L_0x04fe:
            r7 = r7 & 32
            if (r7 == 0) goto L_0x0509
            r7 = 7
            r0 = r22
            r0.b = r7
            goto L_0x002f
        L_0x0509:
            r1 = 9
            r0 = r22
            r0.b = r1
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            java.lang.String r7 = "invalid literal/length code"
            r1.msg = r7
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.b = r6
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.f3105a = r5
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            r1.avail_in = r3
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            long r5 = r1.total_in
            r0 = r22
            com.jcraft.jzlib.ZStream r3 = r0.p
            int r3 = r3.next_in_index
            int r3 = r4 - r3
            long r7 = (long) r3
            long r5 = r5 + r7
            r1.total_in = r5
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            r1.next_in_index = r4
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.f = r2
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r2 = -3
            int r1 = r1.b(r2)
            goto L_0x006f
        L_0x0552:
            r0 = r22
            int r8 = r0.h
            r7 = r5
        L_0x0557:
            if (r7 < r8) goto L_0x05d0
            r0 = r22
            int r5 = r0.c
            int[] r9 = com.jcraft.jzlib.d.f3106a
            r9 = r9[r8]
            r9 = r9 & r6
            int r5 = r5 + r9
            r0 = r22
            r0.c = r5
            int r6 = r6 >> r8
            int r5 = r7 - r8
            r0 = r22
            byte r7 = r0.k
            r0 = r22
            r0.f = r7
            r0 = r22
            int[] r7 = r0.n
            r0 = r22
            r0.d = r7
            r0 = r22
            int r7 = r0.o
            r0 = r22
            r0.e = r7
            r7 = 3
            r0 = r22
            r0.b = r7
        L_0x0587:
            r0 = r22
            int r8 = r0.f
            r7 = r5
        L_0x058c:
            if (r7 < r8) goto L_0x0626
            r0 = r22
            int r5 = r0.e
            int[] r9 = com.jcraft.jzlib.d.f3106a
            r8 = r9[r8]
            r8 = r8 & r6
            int r5 = r5 + r8
            int r8 = r5 * 3
            r0 = r22
            int[] r5 = r0.d
            int r9 = r8 + 1
            r5 = r5[r9]
            int r6 = r6 >> r5
            r0 = r22
            int[] r5 = r0.d
            int r9 = r8 + 1
            r5 = r5[r9]
            int r5 = r7 - r5
            r0 = r22
            int[] r7 = r0.d
            r7 = r7[r8]
            r9 = r7 & 16
            if (r9 == 0) goto L_0x067c
            r7 = r7 & 15
            r0 = r22
            r0.h = r7
            r0 = r22
            int[] r7 = r0.d
            int r8 = r8 + 2
            r7 = r7[r8]
            r0 = r22
            r0.i = r7
            r7 = 4
            r0 = r22
            r0.b = r7
            goto L_0x002f
        L_0x05d0:
            if (r3 == 0) goto L_0x05ea
            r23 = 0
            int r3 = r3 + -1
            r0 = r22
            com.jcraft.jzlib.ZStream r5 = r0.p
            byte[] r9 = r5.next_in
            int r5 = r4 + 1
            byte r4 = r9[r4]
            r4 = r4 & 255(0xff, float:3.57E-43)
            int r4 = r4 << r7
            r6 = r6 | r4
            int r4 = r7 + 8
            r7 = r4
            r4 = r5
            goto L_0x0557
        L_0x05ea:
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.b = r6
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.f3105a = r7
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            r1.avail_in = r3
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            long r5 = r1.total_in
            r0 = r22
            com.jcraft.jzlib.ZStream r3 = r0.p
            int r3 = r3.next_in_index
            int r3 = r4 - r3
            long r7 = (long) r3
            long r5 = r5 + r7
            r1.total_in = r5
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            r1.next_in_index = r4
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.f = r2
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r0 = r23
            int r1 = r1.b(r0)
            goto L_0x006f
        L_0x0626:
            if (r3 == 0) goto L_0x0640
            r23 = 0
            int r3 = r3 + -1
            r0 = r22
            com.jcraft.jzlib.ZStream r5 = r0.p
            byte[] r9 = r5.next_in
            int r5 = r4 + 1
            byte r4 = r9[r4]
            r4 = r4 & 255(0xff, float:3.57E-43)
            int r4 = r4 << r7
            r6 = r6 | r4
            int r4 = r7 + 8
            r7 = r4
            r4 = r5
            goto L_0x058c
        L_0x0640:
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.b = r6
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.f3105a = r7
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            r1.avail_in = r3
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            long r5 = r1.total_in
            r0 = r22
            com.jcraft.jzlib.ZStream r3 = r0.p
            int r3 = r3.next_in_index
            int r3 = r4 - r3
            long r7 = (long) r3
            long r5 = r5 + r7
            r1.total_in = r5
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            r1.next_in_index = r4
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.f = r2
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r0 = r23
            int r1 = r1.b(r0)
            goto L_0x006f
        L_0x067c:
            r9 = r7 & 64
            if (r9 != 0) goto L_0x0695
            r0 = r22
            r0.f = r7
            int r7 = r8 / 3
            r0 = r22
            int[] r9 = r0.d
            int r8 = r8 + 2
            r8 = r9[r8]
            int r7 = r7 + r8
            r0 = r22
            r0.e = r7
            goto L_0x002f
        L_0x0695:
            r1 = 9
            r0 = r22
            r0.b = r1
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            java.lang.String r7 = "invalid distance code"
            r1.msg = r7
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.b = r6
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.f3105a = r5
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            r1.avail_in = r3
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            long r5 = r1.total_in
            r0 = r22
            com.jcraft.jzlib.ZStream r3 = r0.p
            int r3 = r3.next_in_index
            int r3 = r4 - r3
            long r7 = (long) r3
            long r5 = r5 + r7
            r1.total_in = r5
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            r1.next_in_index = r4
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.f = r2
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r2 = -3
            int r1 = r1.b(r2)
            goto L_0x006f
        L_0x06de:
            r0 = r22
            int r8 = r0.h
            r7 = r5
        L_0x06e3:
            if (r7 < r8) goto L_0x0710
            r0 = r22
            int r5 = r0.i
            int[] r9 = com.jcraft.jzlib.d.f3106a
            r9 = r9[r8]
            r9 = r9 & r6
            int r5 = r5 + r9
            r0 = r22
            r0.i = r5
            int r6 = r6 >> r8
            int r5 = r7 - r8
            r7 = 5
            r0 = r22
            r0.b = r7
        L_0x06fb:
            r0 = r22
            int r7 = r0.i
            int r7 = r2 - r7
        L_0x0701:
            if (r7 < 0) goto L_0x0765
        L_0x0703:
            r0 = r22
            int r8 = r0.c
            if (r8 != 0) goto L_0x076d
            r7 = 0
            r0 = r22
            r0.b = r7
            goto L_0x002f
        L_0x0710:
            if (r3 == 0) goto L_0x0729
            r23 = 0
            int r3 = r3 + -1
            r0 = r22
            com.jcraft.jzlib.ZStream r5 = r0.p
            byte[] r9 = r5.next_in
            int r5 = r4 + 1
            byte r4 = r9[r4]
            r4 = r4 & 255(0xff, float:3.57E-43)
            int r4 = r4 << r7
            r6 = r6 | r4
            int r4 = r7 + 8
            r7 = r4
            r4 = r5
            goto L_0x06e3
        L_0x0729:
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.b = r6
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.f3105a = r7
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            r1.avail_in = r3
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            long r5 = r1.total_in
            r0 = r22
            com.jcraft.jzlib.ZStream r3 = r0.p
            int r3 = r3.next_in_index
            int r3 = r4 - r3
            long r7 = (long) r3
            long r5 = r5 + r7
            r1.total_in = r5
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            r1.next_in_index = r4
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.f = r2
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r0 = r23
            int r1 = r1.b(r0)
            goto L_0x006f
        L_0x0765:
            r0 = r22
            com.jcraft.jzlib.c r8 = r0.q
            int r8 = r8.d
            int r7 = r7 + r8
            goto L_0x0701
        L_0x076d:
            if (r1 != 0) goto L_0x0837
            r0 = r22
            com.jcraft.jzlib.c r8 = r0.q
            int r8 = r8.d
            if (r2 != r8) goto L_0x0792
            r0 = r22
            com.jcraft.jzlib.c r8 = r0.q
            int r8 = r8.e
            if (r8 == 0) goto L_0x0792
            r2 = 0
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r1 = r1.e
            if (r1 <= 0) goto L_0x081c
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r1 = r1.e
            int r1 = r1 + 0
            int r1 = r1 + -1
        L_0x0792:
            if (r1 != 0) goto L_0x0837
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.f = r2
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r0 = r23
            int r23 = r1.b(r0)
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r2 = r1.f
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r1 = r1.e
            if (r2 >= r1) goto L_0x0826
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r1 = r1.e
            int r1 = r1 - r2
            int r1 = r1 + -1
        L_0x07bb:
            r0 = r22
            com.jcraft.jzlib.c r8 = r0.q
            int r8 = r8.d
            if (r2 != r8) goto L_0x07de
            r0 = r22
            com.jcraft.jzlib.c r8 = r0.q
            int r8 = r8.e
            if (r8 == 0) goto L_0x07de
            r2 = 0
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r1 = r1.e
            if (r1 <= 0) goto L_0x082e
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r1 = r1.e
            int r1 = r1 + 0
            int r1 = r1 + -1
        L_0x07de:
            if (r1 != 0) goto L_0x0837
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.b = r6
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.f3105a = r5
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            r1.avail_in = r3
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            long r5 = r1.total_in
            r0 = r22
            com.jcraft.jzlib.ZStream r3 = r0.p
            int r3 = r3.next_in_index
            int r3 = r4 - r3
            long r7 = (long) r3
            long r5 = r5 + r7
            r1.total_in = r5
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            r1.next_in_index = r4
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.f = r2
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r0 = r23
            int r1 = r1.b(r0)
            goto L_0x006f
        L_0x081c:
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r1 = r1.d
            int r1 = r1 + 0
            goto L_0x0792
        L_0x0826:
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r1 = r1.d
            int r1 = r1 - r2
            goto L_0x07bb
        L_0x082e:
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r1 = r1.d
            int r1 = r1 + 0
            goto L_0x07de
        L_0x0837:
            r8 = r2
            r0 = r22
            com.jcraft.jzlib.c r2 = r0.q
            byte[] r10 = r2.c
            int r2 = r8 + 1
            r0 = r22
            com.jcraft.jzlib.c r9 = r0.q
            byte[] r11 = r9.c
            int r9 = r7 + 1
            byte r7 = r11[r7]
            r10[r8] = r7
            int r1 = r1 + -1
            r0 = r22
            com.jcraft.jzlib.c r7 = r0.q
            int r7 = r7.d
            if (r9 != r7) goto L_0x0a3e
            r7 = 0
        L_0x0857:
            r0 = r22
            int r8 = r0.c
            int r8 = r8 + -1
            r0 = r22
            r0.c = r8
            goto L_0x0703
        L_0x0863:
            if (r1 != 0) goto L_0x092b
            r0 = r22
            com.jcraft.jzlib.c r7 = r0.q
            int r7 = r7.d
            if (r2 != r7) goto L_0x0888
            r0 = r22
            com.jcraft.jzlib.c r7 = r0.q
            int r7 = r7.e
            if (r7 == 0) goto L_0x0888
            r2 = 0
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r1 = r1.e
            if (r1 <= 0) goto L_0x0910
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r1 = r1.e
            int r1 = r1 + 0
            int r1 = r1 + -1
        L_0x0888:
            if (r1 != 0) goto L_0x092b
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.f = r2
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r0 = r23
            int r7 = r1.b(r0)
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r2 = r1.f
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r1 = r1.e
            if (r2 >= r1) goto L_0x091a
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r1 = r1.e
            int r1 = r1 - r2
            int r1 = r1 + -1
        L_0x08b1:
            r0 = r22
            com.jcraft.jzlib.c r8 = r0.q
            int r8 = r8.d
            if (r2 != r8) goto L_0x08d4
            r0 = r22
            com.jcraft.jzlib.c r8 = r0.q
            int r8 = r8.e
            if (r8 == 0) goto L_0x08d4
            r2 = 0
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r1 = r1.e
            if (r1 <= 0) goto L_0x0922
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r1 = r1.e
            int r1 = r1 + 0
            int r1 = r1 + -1
        L_0x08d4:
            if (r1 != 0) goto L_0x092b
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.b = r6
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.f3105a = r5
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            r1.avail_in = r3
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            long r5 = r1.total_in
            r0 = r22
            com.jcraft.jzlib.ZStream r3 = r0.p
            int r3 = r3.next_in_index
            int r3 = r4 - r3
            long r8 = (long) r3
            long r5 = r5 + r8
            r1.total_in = r5
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            r1.next_in_index = r4
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.f = r2
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r1 = r1.b(r7)
            goto L_0x006f
        L_0x0910:
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r1 = r1.d
            int r1 = r1 + 0
            goto L_0x0888
        L_0x091a:
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r1 = r1.d
            int r1 = r1 - r2
            goto L_0x08b1
        L_0x0922:
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            int r1 = r1.d
            int r1 = r1 + 0
            goto L_0x08d4
        L_0x092b:
            r7 = r2
            r23 = 0
            r0 = r22
            com.jcraft.jzlib.c r2 = r0.q
            byte[] r8 = r2.c
            int r2 = r7 + 1
            r0 = r22
            int r9 = r0.g
            byte r9 = (byte) r9
            r8[r7] = r9
            int r1 = r1 + -1
            r7 = 0
            r0 = r22
            r0.b = r7
            goto L_0x002f
        L_0x0946:
            r1 = 7
            if (r5 <= r1) goto L_0x094f
            int r5 = r5 + -8
            int r3 = r3 + 1
            int r4 = r4 + -1
        L_0x094f:
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.f = r2
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r0 = r23
            int r1 = r1.b(r0)
            r0 = r22
            com.jcraft.jzlib.c r2 = r0.q
            int r2 = r2.f
            r0 = r22
            com.jcraft.jzlib.c r7 = r0.q
            int r7 = r7.e
            if (r2 >= r7) goto L_0x09bb
            r0 = r22
            com.jcraft.jzlib.c r7 = r0.q
            int r7 = r7.e
        L_0x0973:
            r0 = r22
            com.jcraft.jzlib.c r7 = r0.q
            int r7 = r7.e
            r0 = r22
            com.jcraft.jzlib.c r8 = r0.q
            int r8 = r8.f
            if (r7 == r8) goto L_0x09c2
            r0 = r22
            com.jcraft.jzlib.c r7 = r0.q
            r7.b = r6
            r0 = r22
            com.jcraft.jzlib.c r6 = r0.q
            r6.f3105a = r5
            r0 = r22
            com.jcraft.jzlib.ZStream r5 = r0.p
            r5.avail_in = r3
            r0 = r22
            com.jcraft.jzlib.ZStream r3 = r0.p
            long r5 = r3.total_in
            r0 = r22
            com.jcraft.jzlib.ZStream r7 = r0.p
            int r7 = r7.next_in_index
            int r7 = r4 - r7
            long r7 = (long) r7
            long r5 = r5 + r7
            r3.total_in = r5
            r0 = r22
            com.jcraft.jzlib.ZStream r3 = r0.p
            r3.next_in_index = r4
            r0 = r22
            com.jcraft.jzlib.c r3 = r0.q
            r3.f = r2
            r0 = r22
            com.jcraft.jzlib.c r2 = r0.q
            int r1 = r2.b(r1)
            goto L_0x006f
        L_0x09bb:
            r0 = r22
            com.jcraft.jzlib.c r7 = r0.q
            int r7 = r7.d
            goto L_0x0973
        L_0x09c2:
            r1 = 8
            r0 = r22
            r0.b = r1
        L_0x09c8:
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.b = r6
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.f3105a = r5
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            r1.avail_in = r3
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            long r5 = r1.total_in
            r0 = r22
            com.jcraft.jzlib.ZStream r3 = r0.p
            int r3 = r3.next_in_index
            int r3 = r4 - r3
            long r7 = (long) r3
            long r5 = r5 + r7
            r1.total_in = r5
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            r1.next_in_index = r4
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.f = r2
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r2 = 1
            int r1 = r1.b(r2)
            goto L_0x006f
        L_0x0a03:
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.b = r6
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.f3105a = r5
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            r1.avail_in = r3
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            long r5 = r1.total_in
            r0 = r22
            com.jcraft.jzlib.ZStream r3 = r0.p
            int r3 = r3.next_in_index
            int r3 = r4 - r3
            long r7 = (long) r3
            long r5 = r5 + r7
            r1.total_in = r5
            r0 = r22
            com.jcraft.jzlib.ZStream r1 = r0.p
            r1.next_in_index = r4
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r1.f = r2
            r0 = r22
            com.jcraft.jzlib.c r1 = r0.q
            r2 = -3
            int r1 = r1.b(r2)
            goto L_0x006f
        L_0x0a3e:
            r7 = r9
            goto L_0x0857
        L_0x0a41:
            r7 = r1
            r8 = r2
            r9 = r3
            r2 = r4
            r3 = r5
            goto L_0x0104
        L_0x0a48:
            r2 = r3
            r4 = r5
            goto L_0x024b
        L_0x0a4c:
            r3 = r1
            r4 = r5
            r1 = r2
            goto L_0x02bc
        L_0x0a51:
            r2 = r6
            r3 = r1
            r1 = r12
            goto L_0x0242
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jcraft.jzlib.d.a(int):int");
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2, int i3, int[] iArr, int i4, int[] iArr2, int i5) {
        this.b = 0;
        this.j = (byte) i2;
        this.k = (byte) i3;
        this.l = iArr;
        this.m = i4;
        this.n = iArr2;
        this.o = i5;
        this.d = null;
    }
}
