package com.jcraft.jzlib;

public final class Inflater extends ZStream {
    private static final int DEF_WBITS = 15;
    private static final int MAX_MEM_LEVEL = 9;
    private static final int MAX_WBITS = 15;
    private static final int Z_BUF_ERROR = -5;
    private static final int Z_DATA_ERROR = -3;
    private static final int Z_ERRNO = -1;
    private static final int Z_FINISH = 4;
    private static final int Z_FULL_FLUSH = 3;
    private static final int Z_MEM_ERROR = -4;
    private static final int Z_NEED_DICT = 2;
    private static final int Z_NO_FLUSH = 0;
    private static final int Z_OK = 0;
    private static final int Z_PARTIAL_FLUSH = 1;
    private static final int Z_STREAM_END = 1;
    private static final int Z_STREAM_ERROR = -2;
    private static final int Z_SYNC_FLUSH = 2;
    private static final int Z_VERSION_ERROR = -6;
    private boolean finished;

    public Inflater() {
        this.finished = false;
        init();
    }

    public Inflater(int i) {
        this(i, false);
    }

    public Inflater(int i, boolean z) {
        this.finished = false;
        int init = init(i, z);
        if (init != 0) {
            throw new GZIPException(String.valueOf(init) + ": " + this.msg);
        }
    }

    public final int end() {
        this.finished = true;
        if (this.istate == null) {
            return -2;
        }
        this.istate.a();
        return 0;
    }

    public final boolean finished() {
        return this.istate.f3108a == 12;
    }

    public final int inflate(int i) {
        if (this.istate == null) {
            return -2;
        }
        int b = this.istate.b(i);
        if (b != 1) {
            return b;
        }
        this.finished = true;
        return b;
    }

    public final int init() {
        return init(15);
    }

    public final int init(int i) {
        return init(i, false);
    }

    public final int init(int i, boolean z) {
        this.finished = false;
        this.istate = new f(this);
        f fVar = this.istate;
        if (z) {
            i = -i;
        }
        return fVar.a(i);
    }

    public final int init(boolean z) {
        return init(15, z);
    }

    public final int setDictionary(byte[] bArr, int i) {
        if (this.istate == null) {
            return -2;
        }
        return this.istate.a(bArr, i);
    }

    public final int sync() {
        if (this.istate == null) {
            return -2;
        }
        return this.istate.b();
    }

    public final int syncPoint() {
        if (this.istate == null) {
            return -2;
        }
        return this.istate.c();
    }
}
