package com.jcraft.jzlib;

import com.amap.mapapi.poisearch.PoiTypeDef;
import java.io.IOException;
import java.io.InputStream;
import mm.purchasesdk.PurchaseCode;

public class GZIPInputStream extends InflaterInputStream {
    public GZIPInputStream(InputStream inputStream) {
        this(inputStream, PurchaseCode.QUERY_NO_APP, true);
    }

    public GZIPInputStream(InputStream inputStream, int i, boolean z) {
        this(inputStream, new Inflater(31), i, z);
        this.myinflater = true;
    }

    public GZIPInputStream(InputStream inputStream, Inflater inflater, int i, boolean z) {
        super(inputStream, inflater, i, z);
    }

    private int fill(byte[] bArr) {
        int i;
        int length = bArr.length;
        int i2 = 0;
        do {
            try {
                i = this.in.read(bArr, i2, bArr.length - i2);
            } catch (IOException e) {
                i = -1;
            }
            if (i == -1) {
                break;
            }
            i2 += i;
        } while (i2 < length);
        return i2;
    }

    public long getCRC() {
        if (this.inflater.istate.f3108a == 12) {
            return this.inflater.istate.d().getCRC();
        }
        throw new GZIPException("checksum is not calculated yet.");
    }

    public String getComment() {
        return this.inflater.istate.d().getComment();
    }

    public long getModifiedtime() {
        return this.inflater.istate.d().getModifiedTime();
    }

    public String getName() {
        return this.inflater.istate.d().getName();
    }

    public int getOS() {
        return this.inflater.istate.d().getOS();
    }

    public void readHeader() {
        byte[] bArr;
        int fill;
        byte[] bytes = PoiTypeDef.All.getBytes();
        this.inflater.setOutput(bytes, 0, 0);
        this.inflater.setInput(bytes, 0, 0, false);
        byte[] bArr2 = new byte[10];
        int fill2 = fill(bArr2);
        if (fill2 != 10) {
            if (fill2 > 0) {
                this.inflater.setInput(bArr2, 0, fill2, false);
                this.inflater.next_in_index = 0;
                this.inflater.avail_in = fill2;
            }
            throw new IOException("no input");
        }
        this.inflater.setInput(bArr2, 0, fill2, false);
        byte[] bArr3 = new byte[1];
        do {
            if (this.inflater.avail_in <= 0) {
                if (this.in.read(bArr3) <= 0) {
                    throw new IOException("no input");
                }
                this.inflater.setInput(bArr3, 0, 1, true);
            }
            if (this.inflater.inflate(0) != 0) {
                int length = 2048 - this.inflater.next_in.length;
                if (length > 0 && (fill = fill((bArr = new byte[length]))) > 0) {
                    this.inflater.avail_in += this.inflater.next_in_index;
                    this.inflater.next_in_index = 0;
                    this.inflater.setInput(bArr, 0, fill, true);
                }
                this.inflater.avail_in += this.inflater.next_in_index;
                this.inflater.next_in_index = 0;
                throw new IOException(this.inflater.msg);
            }
        } while (this.inflater.istate.e());
    }
}
