package com.jcraft.jzlib;

@Deprecated
public class ZStream {
    private static final int DEF_WBITS = 15;
    private static final int MAX_MEM_LEVEL = 9;
    private static final int MAX_WBITS = 15;
    private static final int Z_BUF_ERROR = -5;
    private static final int Z_DATA_ERROR = -3;
    private static final int Z_ERRNO = -1;
    private static final int Z_FINISH = 4;
    private static final int Z_FULL_FLUSH = 3;
    private static final int Z_MEM_ERROR = -4;
    private static final int Z_NEED_DICT = 2;
    private static final int Z_NO_FLUSH = 0;
    private static final int Z_OK = 0;
    private static final int Z_PARTIAL_FLUSH = 1;
    private static final int Z_STREAM_END = 1;
    private static final int Z_STREAM_ERROR = -2;
    private static final int Z_SYNC_FLUSH = 2;
    private static final int Z_VERSION_ERROR = -6;
    a adler;
    public int avail_in;
    public int avail_out;
    int data_type;
    Deflate dstate;
    f istate;
    public String msg;
    public byte[] next_in;
    public int next_in_index;
    public byte[] next_out;
    public int next_out_index;
    public long total_in;
    public long total_out;

    public ZStream() {
        this(new Adler32());
    }

    public ZStream(a aVar) {
        this.adler = aVar;
    }

    public int deflate(int i) {
        if (this.dstate == null) {
            return -2;
        }
        return this.dstate.deflate(i);
    }

    public int deflateEnd() {
        if (this.dstate == null) {
            return -2;
        }
        int deflateEnd = this.dstate.deflateEnd();
        this.dstate = null;
        return deflateEnd;
    }

    public int deflateInit(int i) {
        return deflateInit(i, 15);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.jcraft.jzlib.ZStream.deflateInit(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.jcraft.jzlib.ZStream.deflateInit(int, int, int):int
      com.jcraft.jzlib.ZStream.deflateInit(int, int, boolean):int */
    public int deflateInit(int i, int i2) {
        return deflateInit(i, i2, false);
    }

    public int deflateInit(int i, int i2, int i3) {
        this.dstate = new Deflate(this);
        return this.dstate.deflateInit(i, i2, i3);
    }

    public int deflateInit(int i, int i2, boolean z) {
        this.dstate = new Deflate(this);
        Deflate deflate = this.dstate;
        if (z) {
            i2 = -i2;
        }
        return deflate.deflateInit(i, i2);
    }

    public int deflateInit(int i, boolean z) {
        return deflateInit(i, 15, z);
    }

    public int deflateParams(int i, int i2) {
        if (this.dstate == null) {
            return -2;
        }
        return this.dstate.deflateParams(i, i2);
    }

    public int deflateSetDictionary(byte[] bArr, int i) {
        if (this.dstate == null) {
            return -2;
        }
        return this.dstate.deflateSetDictionary(bArr, i);
    }

    public int end() {
        return 0;
    }

    public boolean finished() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public void flush_pending() {
        int i = this.dstate.pending;
        if (i > this.avail_out) {
            i = this.avail_out;
        }
        if (i != 0) {
            if (this.dstate.pending_buf.length > this.dstate.pending_out && this.next_out.length > this.next_out_index && this.dstate.pending_buf.length >= this.dstate.pending_out + i) {
                byte[] bArr = this.next_out;
            }
            System.arraycopy(this.dstate.pending_buf, this.dstate.pending_out, this.next_out, this.next_out_index, i);
            this.next_out_index += i;
            this.dstate.pending_out += i;
            this.total_out += (long) i;
            this.avail_out -= i;
            this.dstate.pending -= i;
            if (this.dstate.pending == 0) {
                this.dstate.pending_out = 0;
            }
        }
    }

    public void free() {
        this.next_in = null;
        this.next_out = null;
        this.msg = null;
    }

    public long getAdler() {
        return this.adler.getValue();
    }

    public int getAvailIn() {
        return this.avail_in;
    }

    public int getAvailOut() {
        return this.avail_out;
    }

    public String getMessage() {
        return this.msg;
    }

    public byte[] getNextIn() {
        return this.next_in;
    }

    public int getNextInIndex() {
        return this.next_in_index;
    }

    public byte[] getNextOut() {
        return this.next_out;
    }

    public int getNextOutIndex() {
        return this.next_out_index;
    }

    public long getTotalIn() {
        return this.total_in;
    }

    public long getTotalOut() {
        return this.total_out;
    }

    public int inflate(int i) {
        if (this.istate == null) {
            return -2;
        }
        return this.istate.b(i);
    }

    public int inflateEnd() {
        if (this.istate == null) {
            return -2;
        }
        this.istate.a();
        return 0;
    }

    public boolean inflateFinished() {
        return this.istate.f3108a == 12;
    }

    public int inflateInit() {
        return inflateInit(15);
    }

    public int inflateInit(int i) {
        return inflateInit(i, false);
    }

    public int inflateInit(int i, boolean z) {
        this.istate = new f(this);
        f fVar = this.istate;
        if (z) {
            i = -i;
        }
        return fVar.a(i);
    }

    public int inflateInit(boolean z) {
        return inflateInit(15, z);
    }

    public int inflateSetDictionary(byte[] bArr, int i) {
        if (this.istate == null) {
            return -2;
        }
        return this.istate.a(bArr, i);
    }

    public int inflateSync() {
        if (this.istate == null) {
            return -2;
        }
        return this.istate.b();
    }

    public int inflateSyncPoint() {
        if (this.istate == null) {
            return -2;
        }
        return this.istate.c();
    }

    /* access modifiers changed from: package-private */
    public int read_buf(byte[] bArr, int i, int i2) {
        int i3 = this.avail_in;
        if (i3 > i2) {
            i3 = i2;
        }
        if (i3 == 0) {
            return 0;
        }
        this.avail_in -= i3;
        if (this.dstate.wrap != 0) {
            this.adler.update(this.next_in, this.next_in_index, i3);
        }
        System.arraycopy(this.next_in, this.next_in_index, bArr, i, i3);
        this.next_in_index += i3;
        this.total_in += (long) i3;
        return i3;
    }

    public void setAvailIn(int i) {
        this.avail_in = i;
    }

    public void setAvailOut(int i) {
        this.avail_out = i;
    }

    public void setInput(byte[] bArr) {
        setInput(bArr, 0, bArr.length, false);
    }

    public void setInput(byte[] bArr, int i, int i2, boolean z) {
        if (i2 <= 0 && z && this.next_in != null) {
            return;
        }
        if (this.avail_in <= 0 || !z) {
            this.next_in = bArr;
            this.next_in_index = i;
            this.avail_in = i2;
            return;
        }
        byte[] bArr2 = new byte[(this.avail_in + i2)];
        System.arraycopy(this.next_in, this.next_in_index, bArr2, 0, this.avail_in);
        System.arraycopy(bArr, i, bArr2, this.avail_in, i2);
        this.next_in = bArr2;
        this.next_in_index = 0;
        this.avail_in += i2;
    }

    public void setInput(byte[] bArr, boolean z) {
        setInput(bArr, 0, bArr.length, z);
    }

    public void setNextIn(byte[] bArr) {
        this.next_in = bArr;
    }

    public void setNextInIndex(int i) {
        this.next_in_index = i;
    }

    public void setNextOut(byte[] bArr) {
        this.next_out = bArr;
    }

    public void setNextOutIndex(int i) {
        this.next_out_index = i;
    }

    public void setOutput(byte[] bArr) {
        setOutput(bArr, 0, bArr.length);
    }

    public void setOutput(byte[] bArr, int i, int i2) {
        this.next_out = bArr;
        this.next_out_index = i;
        this.avail_out = i2;
    }
}
