package com.jcraft.jzlib;

import java.io.OutputStream;
import mm.purchasesdk.PurchaseCode;

public class GZIPOutputStream extends DeflaterOutputStream {
    public GZIPOutputStream(OutputStream outputStream) {
        this(outputStream, PurchaseCode.QUERY_NO_APP);
    }

    public GZIPOutputStream(OutputStream outputStream, int i) {
        this(outputStream, i, true);
    }

    public GZIPOutputStream(OutputStream outputStream, int i, boolean z) {
        this(outputStream, new Deflater(-1, 31), i, z);
        this.mydeflater = true;
    }

    public GZIPOutputStream(OutputStream outputStream, Deflater deflater, int i, boolean z) {
        super(outputStream, deflater, i, z);
    }

    private void check() {
        if (this.deflater.dstate.status != 42) {
            throw new GZIPException("header is already written.");
        }
    }

    public long getCRC() {
        if (this.deflater.dstate.status == 666) {
            return this.deflater.dstate.getGZIPHeader().getCRC();
        }
        throw new GZIPException("checksum is not calculated yet.");
    }

    public void setComment(String str) {
        check();
        this.deflater.dstate.getGZIPHeader().setComment(str);
    }

    public void setModifiedTime(long j) {
        check();
        this.deflater.dstate.getGZIPHeader().setModifiedTime(j);
    }

    public void setName(String str) {
        check();
        this.deflater.dstate.getGZIPHeader().setName(str);
    }

    public void setOS(int i) {
        check();
        this.deflater.dstate.getGZIPHeader().setOS(i);
    }
}
