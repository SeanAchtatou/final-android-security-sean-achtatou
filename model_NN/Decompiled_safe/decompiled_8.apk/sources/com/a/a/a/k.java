package com.a.a.a;

import java.io.ByteArrayInputStream;
import javax.xml.parsers.SAXParserFactory;

public final class k {
    public static String a(String str) {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(str.getBytes());
        SAXParserFactory newInstance = SAXParserFactory.newInstance();
        m mVar = new m();
        try {
            newInstance.newSAXParser().parse(byteArrayInputStream, mVar);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mVar.f406a;
    }

    public static i b(String str) {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(str.getBytes());
        SAXParserFactory newInstance = SAXParserFactory.newInstance();
        l lVar = new l();
        try {
            newInstance.newSAXParser().parse(byteArrayInputStream, lVar);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lVar.f405a;
    }
}
