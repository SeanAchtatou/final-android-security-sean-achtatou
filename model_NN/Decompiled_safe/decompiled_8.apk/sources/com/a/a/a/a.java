package com.a.a.a;

import android.content.Context;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Looper;
import android.telephony.CellLocation;
import android.telephony.NeighboringCellInfo;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import com.amap.mapapi.location.LocationManagerProxy;
import com.b.g;
import com.e.a.a.a.m;
import java.util.ArrayList;
import java.util.List;

public final class a implements h {

    /* renamed from: a  reason: collision with root package name */
    private static String f401a = null;
    private static String b = null;
    private static a c = null;
    private static Context d = null;
    private static n e = null;
    /* access modifiers changed from: private */
    public static int f = 0;
    private static ConnectivityManager g = null;
    /* access modifiers changed from: private */
    public static WifiManager h = null;
    /* access modifiers changed from: private */
    public static TelephonyManager i = null;
    private static LocationManager j = null;
    private static LocationListener k = null;
    private static Location l = null;
    /* access modifiers changed from: private */
    public static Location m = null;
    /* access modifiers changed from: private */
    public static ArrayList n = new ArrayList();
    /* access modifiers changed from: private */
    public static ArrayList o = new ArrayList();
    /* access modifiers changed from: private */
    public static List p = new ArrayList();
    private static g q = new g("autonavi00spas$#@!666666");
    private static PhoneStateListener r = null;
    /* access modifiers changed from: private */
    public static int s = 10;
    private static f t = null;
    private static WifiInfo u = null;
    private static String v = null;
    private static i w = null;
    /* access modifiers changed from: private */
    public static long x = 0;
    private static boolean y = false;

    public a(Context context) {
        d = context;
        c = this;
        h = (WifiManager) d.getSystemService("wifi");
        c.getClass();
        t = new f((byte) 0);
        if (h.getWifiState() == 3) {
            u = h.getConnectionInfo();
            new d().start();
        }
        d.registerReceiver(t, new IntentFilter("android.net.wifi.SCAN_RESULTS"));
        g = (ConnectivityManager) d.getSystemService("connectivity");
        TelephonyManager telephonyManager = (TelephonyManager) d.getSystemService("phone");
        i = telephonyManager;
        e = n.a(telephonyManager);
        r = new e();
        i.listen(r, 256);
        i.listen(r, 16);
        j = (LocationManager) d.getSystemService(LocationManagerProxy.KEY_LOCATION_CHANGED);
        k = new b();
    }

    /* access modifiers changed from: private */
    public static synchronized void b(CellLocation cellLocation, List list) {
        synchronized (a.class) {
            if (cellLocation != null) {
                String networkOperator = i.getNetworkOperator();
                if (cellLocation instanceof GsmCellLocation) {
                    f = 1;
                    n.clear();
                    if (networkOperator == null || networkOperator.length() <= 4) {
                        Log.d("aps", "network operator: " + networkOperator);
                    } else {
                        m mVar = new m();
                        mVar.a(((GsmCellLocation) cellLocation).getLac());
                        mVar.b(((GsmCellLocation) cellLocation).getCid());
                        mVar.c(s);
                        mVar.a(networkOperator.substring(0, 3));
                        mVar.b(networkOperator.substring(3, 5));
                        n.add(mVar);
                        if (list != null && list.size() > 0) {
                            for (int i2 = 0; i2 < list.size(); i2++) {
                                NeighboringCellInfo neighboringCellInfo = (NeighboringCellInfo) list.get(i2);
                                m mVar2 = new m();
                                mVar2.c((neighboringCellInfo.getRssi() * 2) - 133);
                                mVar2.a(neighboringCellInfo.getLac());
                                mVar2.b(neighboringCellInfo.getCid());
                                mVar2.a(networkOperator.substring(0, 3));
                                mVar2.b(networkOperator.substring(3, 5));
                                n.add(mVar2);
                            }
                        }
                    }
                } else {
                    try {
                        Class.forName("android.telephony.cdma.CdmaCellLocation");
                        f = 2;
                        o.clear();
                        if (networkOperator == null || networkOperator.length() <= 4) {
                            Log.d("aps", "network operator: " + networkOperator);
                        } else {
                            g gVar = new g();
                            CdmaCellLocation cdmaCellLocation = (CdmaCellLocation) cellLocation;
                            gVar.a(cdmaCellLocation.getBaseStationLatitude());
                            gVar.b(cdmaCellLocation.getBaseStationLongitude());
                            gVar.c(cdmaCellLocation.getBaseStationId());
                            gVar.d(cdmaCellLocation.getNetworkId());
                            gVar.e(cdmaCellLocation.getSystemId());
                            gVar.f(s);
                            gVar.a(networkOperator.substring(0, 3));
                            networkOperator.substring(3, 5);
                            o.add(gVar);
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }
        return;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0059, code lost:
        if (r0.c() > 0.0d) goto L_0x005b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized com.a.a.a.i c(java.lang.String r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            android.content.Context r0 = com.a.a.a.a.d     // Catch:{ all -> 0x005f }
            com.a.a.a.j r0 = com.a.a.a.j.a(r0)     // Catch:{ all -> 0x005f }
            java.lang.String r1 = "http://aps.amap.com/APS/r"
            java.lang.String r0 = r0.a(r1, r6)     // Catch:{ all -> 0x005f }
            if (r0 == 0) goto L_0x005d
            int r1 = r0.length()     // Catch:{ all -> 0x005f }
            if (r1 <= 0) goto L_0x005d
            boolean r1 = com.a.a.a.a.y     // Catch:{ all -> 0x005f }
            if (r1 != 0) goto L_0x002a
            com.a.a.a.g r1 = com.a.a.a.a.q     // Catch:{ all -> 0x005f }
            com.a.a.a.k r2 = new com.a.a.a.k     // Catch:{ all -> 0x005f }
            r2.<init>()     // Catch:{ all -> 0x005f }
            java.lang.String r0 = com.a.a.a.k.a(r0)     // Catch:{ all -> 0x005f }
            java.lang.String r2 = "GBK"
            java.lang.String r0 = r1.a(r0, r2)     // Catch:{ all -> 0x005f }
        L_0x002a:
            java.lang.String r1 = "aps"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x005f }
            java.lang.String r3 = "response:"
            r2.<init>(r3)     // Catch:{ all -> 0x005f }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ all -> 0x005f }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x005f }
            android.util.Log.d(r1, r2)     // Catch:{ all -> 0x005f }
            if (r0 == 0) goto L_0x005d
            int r1 = r0.length()     // Catch:{ all -> 0x005f }
            if (r1 <= 0) goto L_0x005d
            com.a.a.a.k r1 = new com.a.a.a.k     // Catch:{ all -> 0x005f }
            r1.<init>()     // Catch:{ all -> 0x005f }
            com.a.a.a.i r0 = com.a.a.a.k.b(r0)     // Catch:{ all -> 0x005f }
            if (r0 == 0) goto L_0x005d
            double r1 = r0.c()     // Catch:{ all -> 0x005f }
            r3 = 0
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 <= 0) goto L_0x005d
        L_0x005b:
            monitor-exit(r5)
            return r0
        L_0x005d:
            r0 = 0
            goto L_0x005b
        L_0x005f:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.a.a.a.c(java.lang.String):com.a.a.a.i");
    }

    private synchronized String k() {
        StringBuilder sb;
        if (f401a == null || b == null) {
            Log.d("aps", "licence or productname is null");
        }
        if (h.getWifiState() == 3) {
            new c().start();
        }
        r.onCellLocationChanged(i.getCellLocation());
        StringBuilder sb2 = new StringBuilder();
        sb2.append("<?xml version=\"1.0\" encoding=\"GBK\" ?>");
        sb2.append("<location>");
        sb2.append("<license>").append(f401a).append("</license>");
        sb2.append("<src>").append(b).append("</src>");
        StringBuilder append = sb2.append("<imei>");
        n nVar = e;
        append.append(n.a()).append("#").append(i.getNetworkOperatorName()).append("#").append(i.getLine1Number()).append("</imei>");
        NetworkInfo activeNetworkInfo = g.getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isAvailable() && activeNetworkInfo.isConnected()) {
            sb2.append("<network>").append(activeNetworkInfo.toString()).append("</network>");
        }
        if (f == 1) {
            sb2.append("<cdma>0</cdma>");
            if (n.size() > 0) {
                m mVar = (m) n.get(0);
                sb2.append("<mcc>").append(mVar.a()).append("</mcc>");
                sb2.append("<mnc>").append(mVar.b()).append("</mnc>");
                sb2.append("<lac>").append(mVar.c()).append("</lac>");
                sb2.append("<cellid>").append(mVar.d()).append("</cellid>");
                sb2.append("<signal>").append(mVar.e()).append("</signal>");
                if (n.size() > 1) {
                    StringBuffer stringBuffer = new StringBuffer();
                    for (int i2 = 1; i2 < n.size(); i2++) {
                        if (i2 > 1) {
                            stringBuffer.append("*");
                        }
                        m mVar2 = (m) n.get(i2);
                        stringBuffer.append(mVar2.b()).append(",").append(mVar2.c()).append(",").append(mVar2.d()).append(",").append(mVar2.e());
                    }
                    sb2.append("<nb>").append(stringBuffer.toString()).append("</nb>");
                }
            }
        } else if (f == 2) {
            sb2.append("<cdma>1</cdma>");
            if (o.size() > 0) {
                g gVar = (g) o.get(0);
                sb2.append("<mcc>").append(gVar.a()).append("</mcc>");
                sb2.append("<sid>").append(gVar.d()).append("</sid>");
                sb2.append("<nid>").append(gVar.e()).append("</nid>");
                sb2.append("<bid>").append(gVar.f()).append("</bid>");
                sb2.append("<lon>").append(gVar.c()).append("</lon>");
                sb2.append("<lat>").append(gVar.b()).append("</lat>");
                sb2.append("<signal>").append(gVar.g()).append("</signal>");
                if (o.size() > 1) {
                    StringBuffer stringBuffer2 = new StringBuffer();
                    for (int i3 = 1; i3 < o.size(); i3++) {
                        if (i3 > 1) {
                            stringBuffer2.append("*");
                        }
                        g gVar2 = (g) o.get(i3);
                        stringBuffer2.append(gVar2.d()).append(",").append(gVar2.e()).append(",").append(gVar2.f()).append(",").append(gVar2.g());
                    }
                    sb2.append("<nb>" + stringBuffer2.toString() + "</nb>");
                }
            }
        }
        if (j.isProviderEnabled(LocationManagerProxy.GPS_PROVIDER)) {
            if (l != null) {
                double latitude = l.getLatitude();
                double longitude = l.getLongitude();
                if (latitude > 3.0d && longitude > 73.0d) {
                    sb2.append("<gps>1</gps>");
                    sb2.append("<glong>").append(longitude).append("</glong>");
                    sb2.append("<glat>").append(latitude).append("</glat>");
                }
                l = null;
            }
        } else if (j.isProviderEnabled(LocationManagerProxy.NETWORK_PROVIDER)) {
            Log.d("aps", "google network provider is enabled");
            j.requestLocationUpdates(LocationManagerProxy.NETWORK_PROVIDER, 20000, 0.0f, k, Looper.getMainLooper());
            try {
                Log.d("aps", "waiting google network provider for 3s");
                Thread.sleep(3000);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
            if (m != null) {
                double latitude2 = m.getLatitude();
                double longitude2 = m.getLongitude();
                if (latitude2 > 3.0d && longitude2 > 73.0d) {
                    sb2.append("<gps>2</gps>");
                    sb2.append("<glong>").append(longitude2).append("</glong>");
                    sb2.append("<glat>").append(latitude2).append("</glat>");
                }
            }
            Log.d("aps", "remove google network provider listener");
            m = null;
            j.removeUpdates(k);
        }
        if (p.size() > 0) {
            StringBuilder sb3 = new StringBuilder();
            int i4 = 0;
            while (i4 < p.size() && i4 <= 3) {
                ScanResult scanResult = (ScanResult) p.get(i4);
                sb3.append(scanResult.BSSID).append(",").append(scanResult.level).append("*");
                i4++;
            }
            sb2.append("<macs>").append(sb3.toString()).append("</macs>");
        } else if (h.getWifiState() == 3) {
            WifiInfo connectionInfo = h.getConnectionInfo();
            u = connectionInfo;
            if (connectionInfo != null && u.getBSSID() != null) {
                sb2.append("<macs>").append(u.getBSSID()).append(",").append(u.getRssi()).append("*</macs>");
            } else if (u != null && u.getBSSID() == null) {
                Log.d("aps", "wifi fake connection");
            }
        }
        sb2.append("</location>");
        Log.d("aps", "request:" + sb2.toString());
        sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"GBK\" ?>");
        sb.append("<saps>");
        sb.append("<src>").append(b).append("</src>");
        try {
            sb.append("<sreq>").append(q.a(sb2.toString())).append("</sreq>");
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        sb.append("</saps>");
        return sb.toString();
    }

    public final i a(Location location) {
        long currentTimeMillis = System.currentTimeMillis() - x;
        if (currentTimeMillis < 2000) {
            Log.d("aps", "block frequent request, return last location, duration is " + String.valueOf(currentTimeMillis) + " milliseconds");
            return w;
        }
        l = location;
        if (location != null) {
            y = true;
        } else {
            y = false;
        }
        String k2 = k();
        if (!k2.equalsIgnoreCase(v) || w == null) {
            v = k2;
            w = c(v);
            x = System.currentTimeMillis();
            return w;
        }
        Log.d("aps", "same request, direct return");
        return w;
    }

    public final void a() {
        Log.d("aps", "ondestroy");
        try {
            d.unregisterReceiver(t);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        try {
            j.removeUpdates(k);
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        try {
            i.listen(r, 0);
        } catch (Exception e4) {
            e4.printStackTrace();
        }
        n.clear();
        o.clear();
        p.clear();
        i = null;
        k = null;
        r = null;
        u = null;
        c = null;
    }

    public final void a(String str) {
        f401a = str;
    }

    public final void b(String str) {
        b = str;
    }
}
