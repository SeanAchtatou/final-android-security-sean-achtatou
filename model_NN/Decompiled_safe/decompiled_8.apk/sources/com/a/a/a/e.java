package com.a.a.a;

import android.telephony.CellLocation;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.util.Log;
import com.b.g;
import com.e.a.a.a.m;

final class e extends PhoneStateListener {
    e() {
    }

    public final void onCellLocationChanged(CellLocation cellLocation) {
        Log.d("aps", "cell location change");
        if (a.i != null) {
            a.b(cellLocation, a.i.getNeighboringCellInfo());
            super.onCellLocationChanged(cellLocation);
        }
    }

    public final void onServiceStateChanged(ServiceState serviceState) {
        super.onServiceStateChanged(serviceState);
    }

    public final void onSignalStrengthsChanged(SignalStrength signalStrength) {
        Log.d("aps", "cell signal strength change");
        int unused = a.s = (signalStrength.getGsmSignalStrength() * 2) - 113;
        if (a.f == 1 && a.n.size() > 0) {
            ((m) a.n.get(0)).c(a.s);
        } else if (a.f != 2 || a.o.size() <= 0) {
            Log.d("aps", "unknown phone type");
        } else {
            ((g) a.o.get(0)).f(a.s);
        }
        super.onSignalStrengthsChanged(signalStrength);
    }
}
