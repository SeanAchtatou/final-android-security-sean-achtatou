package com.a.a.a;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;
import com.amap.mapapi.location.LocationManagerProxy;

final class b implements LocationListener {
    b() {
    }

    public final void onLocationChanged(Location location) {
        if (location != null && location.getProvider().equalsIgnoreCase(LocationManagerProxy.NETWORK_PROVIDER)) {
            Log.d("aps", "receive google network provider location");
            Location unused = a.m = location;
        }
    }

    public final void onProviderDisabled(String str) {
        if (str.equals(LocationManagerProxy.GPS_PROVIDER)) {
            Log.d("aps", "google network provider disabled");
            a.m.reset();
        }
    }

    public final void onProviderEnabled(String str) {
        if (str.equalsIgnoreCase(LocationManagerProxy.NETWORK_PROVIDER)) {
            Log.d("aps", "google network provider is enabled");
        }
    }

    public final void onStatusChanged(String str, int i, Bundle bundle) {
        if (str.equalsIgnoreCase(LocationManagerProxy.NETWORK_PROVIDER)) {
            Log.d("aps", "google network provider status changed");
        }
    }
}
