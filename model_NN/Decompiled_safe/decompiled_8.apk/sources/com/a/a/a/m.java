package com.a.a.a;

import com.amap.mapapi.poisearch.PoiTypeDef;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

final class m extends DefaultHandler {

    /* renamed from: a  reason: collision with root package name */
    public String f406a = PoiTypeDef.All;
    private boolean b = false;

    m() {
    }

    public final void characters(char[] cArr, int i, int i2) {
        if (this.b) {
            this.f406a = new String(cArr, i, i2);
        }
        super.characters(cArr, i, i2);
    }

    public final void endElement(String str, String str2, String str3) {
        if (str2.equals("sres")) {
            this.b = false;
        }
        super.endElement(str, str2, str3);
    }

    public final void startElement(String str, String str2, String str3, Attributes attributes) {
        if (str2.equals("sres")) {
            this.b = true;
        }
        super.startElement(str, str2, str3, attributes);
    }
}
