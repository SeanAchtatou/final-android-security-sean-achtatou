package com.a.a.a;

import android.telephony.TelephonyManager;

public final class n {

    /* renamed from: a  reason: collision with root package name */
    private static String f407a = null;
    private static n b = null;

    protected n() {
    }

    public static n a(TelephonyManager telephonyManager) {
        if (b == null) {
            b = new n();
            f407a = telephonyManager.getDeviceId();
        }
        return b;
    }

    public static String a() {
        return f407a;
    }
}
