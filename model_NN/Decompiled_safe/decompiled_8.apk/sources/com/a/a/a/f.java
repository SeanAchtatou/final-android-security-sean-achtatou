package com.a.a.a;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import java.util.List;

final class f extends BroadcastReceiver {
    private f() {
    }

    /* synthetic */ f(byte b) {
        this();
    }

    public final void onReceive(Context context, Intent intent) {
        Log.d("aps", "receive " + intent.getAction());
        if (intent.getAction().equalsIgnoreCase("android.net.wifi.SCAN_RESULTS")) {
            List unused = a.p = a.h.getScanResults();
            Log.d("aps", "wifi scan " + String.valueOf(a.p.size()) + " results");
        } else if (intent.getAction().equalsIgnoreCase("android.intent.action.TIME_SET") || intent.getAction().equalsIgnoreCase("android.intent.action.TIMEZONE_CHANGED")) {
            long unused2 = a.x = 0;
        }
    }
}
