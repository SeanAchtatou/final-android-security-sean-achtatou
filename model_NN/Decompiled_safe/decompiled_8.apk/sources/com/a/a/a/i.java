package com.a.a.a;

import com.amap.mapapi.poisearch.PoiTypeDef;

public final class i {

    /* renamed from: a  reason: collision with root package name */
    private String f403a = PoiTypeDef.All;
    private String b = PoiTypeDef.All;
    private double c = 0.0d;
    private double d = 0.0d;
    private double e = 0.0d;
    private String f = PoiTypeDef.All;
    private String g = PoiTypeDef.All;
    private String h = PoiTypeDef.All;

    public final String a() {
        return this.f;
    }

    public final void a(double d2) {
        this.c = d2;
    }

    public final void a(String str) {
        this.f = str;
    }

    public final String b() {
        return this.g;
    }

    public final void b(double d2) {
        this.d = d2;
    }

    public final void b(String str) {
        this.g = str;
    }

    public final double c() {
        return this.c;
    }

    public final void c(double d2) {
        this.e = d2;
    }

    public final void c(String str) {
        this.h = str;
    }

    public final double d() {
        return this.d;
    }

    public final void d(String str) {
        this.f403a = str;
    }

    public final double e() {
        return this.e;
    }

    public final void e(String str) {
        this.b = str;
    }

    public final String toString() {
        return "result:" + this.f403a + "\nrdesc:" + this.b + "\ncenx:" + this.c + "\nceny:" + this.d + "\nradius:" + this.e + "\ncitycode:" + this.f + "\nadcode:" + this.h + "\ndesc:" + this.g + "\n";
    }
}
