package com.a.a.a;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

final class l extends DefaultHandler {

    /* renamed from: a  reason: collision with root package name */
    public i f405a = new i();
    private StringBuffer b = new StringBuffer();

    l() {
    }

    public final void characters(char[] cArr, int i, int i2) {
        this.b.append(new String(cArr, i, i2).toString());
        super.characters(cArr, i, i2);
    }

    public final void endElement(String str, String str2, String str3) {
        if (str2.equals("result")) {
            this.f405a.d(this.b.toString());
        } else if (str2.equals("rdesc")) {
            this.f405a.e(this.b.toString());
        } else if (str2.equals("cenx")) {
            try {
                this.f405a.a(Double.valueOf(this.b.toString()).doubleValue());
            } catch (Exception e) {
                this.f405a.a(0.0d);
            }
        } else if (str2.equals("ceny")) {
            try {
                this.f405a.b(Double.valueOf(this.b.toString()).doubleValue());
            } catch (Exception e2) {
                this.f405a.b(0.0d);
            }
        } else if (str2.equals("radius")) {
            try {
                this.f405a.c(Double.valueOf(this.b.toString()).doubleValue());
            } catch (Exception e3) {
                this.f405a.c(0.0d);
            }
        } else if (str2.equals("citycode")) {
            this.f405a.a(this.b.toString());
        } else if (str2.equals("desc")) {
            this.f405a.b(this.b.toString());
        } else if (str2.equals("adcode")) {
            this.f405a.c(this.b.toString());
        }
        super.endElement(str, str2, str3);
    }

    public final void startElement(String str, String str2, String str3, Attributes attributes) {
        this.b.delete(0, this.b.toString().length());
        super.startElement(str, str2, str3, attributes);
    }
}
