package com.a.a.a;

import com.jcraft.jzlib.GZIPHeader;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

public final class g {

    /* renamed from: a  reason: collision with root package name */
    private static String f402a = "DESede";
    private Cipher b = null;
    private Cipher c = null;

    public g(String str) {
        try {
            SecureRandom secureRandom = new SecureRandom();
            SecretKey generateSecret = SecretKeyFactory.getInstance(f402a).generateSecret(new DESedeKeySpec(str.getBytes("utf-8")));
            this.b = Cipher.getInstance(f402a);
            this.b.init(1, generateSecret, secureRandom);
            this.c = Cipher.getInstance(f402a);
            this.c.init(2, generateSecret, secureRandom);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static byte[] b(String str) {
        String trim;
        int length;
        if (str == null || (length = (trim = str.trim()).length()) == 0 || length % 2 == 1) {
            return null;
        }
        byte[] bArr = new byte[(length / 2)];
        int i = 0;
        while (i < trim.length()) {
            try {
                bArr[i / 2] = (byte) Integer.decode("0X" + trim.substring(i, i + 2)).intValue();
                i += 2;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return bArr;
    }

    public final String a(String str) {
        byte[] doFinal = this.b.doFinal(str.getBytes("utf-8"));
        StringBuilder sb = new StringBuilder();
        for (byte b2 : doFinal) {
            String hexString = Integer.toHexString(b2 & GZIPHeader.OS_UNKNOWN);
            if (hexString.length() == 1) {
                sb.append("0").append(hexString);
            } else {
                sb.append(hexString);
            }
        }
        return sb.toString();
    }

    public final String a(String str, String str2) {
        if (str == null || str.length() <= 0) {
            return null;
        }
        return new String(this.c.doFinal(b(str)), str2);
    }
}
