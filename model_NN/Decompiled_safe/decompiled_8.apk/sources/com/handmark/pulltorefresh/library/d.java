package com.handmark.pulltorefresh.library;

import android.view.animation.Interpolator;

final class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final Interpolator f646a;
    private final int b;
    private final int c;
    private final long d;
    private boolean e = true;
    private long f = -1;
    private int g = -1;
    private /* synthetic */ a h;

    public d(a aVar, int i, int i2, long j) {
        this.h = aVar;
        this.c = i;
        this.b = i2;
        this.f646a = aVar.p;
        this.d = j;
    }

    public final void a() {
        this.e = false;
        this.h.removeCallbacks(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public final void run() {
        if (this.f == -1) {
            this.f = System.currentTimeMillis();
        } else {
            this.g = this.c - Math.round(this.f646a.getInterpolation(((float) Math.max(Math.min(((System.currentTimeMillis() - this.f) * 1000) / this.d, 1000L), 0L)) / 1000.0f) * ((float) (this.c - this.b)));
            this.h.setHeaderScroll((float) this.g);
        }
        if (this.e && this.b != this.g) {
            this.h.postDelayed(this, 10);
        }
    }
}
