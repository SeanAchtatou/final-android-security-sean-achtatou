package com.handmark.pulltorefresh.library;

public enum e {
    RESET(0),
    PULL_TO_REFRESH(1),
    RELEASE_TO_REFRESH(2),
    REFRESHING(8),
    MANUAL_REFRESHING(9);
    
    private int f;

    private e(int i) {
        this.f = i;
    }

    public static e a(int i) {
        switch (i) {
            case 1:
                return PULL_TO_REFRESH;
            case 2:
                return RELEASE_TO_REFRESH;
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            default:
                return RESET;
            case 8:
                return REFRESHING;
            case 9:
                return MANUAL_REFRESHING;
        }
    }

    /* access modifiers changed from: package-private */
    public final int a() {
        return this.f;
    }
}
