package com.handmark.pulltorefresh.library.a;

import android.content.Context;
import android.view.LayoutInflater;
import com.immomo.momo.R;

public final class d extends b {
    public d(Context context) {
        super(context);
        setGravity(16);
        LayoutInflater.from(context).inflate((int) R.layout.userguiforthree_footer, this);
    }

    /* access modifiers changed from: protected */
    public final int getDefaultBottomDrawableResId() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public final int getDefaultTopDrawableResId() {
        return 0;
    }
}
