package com.handmark.pulltorefresh.library.a;

import android.content.Context;
import android.graphics.Matrix;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;

public final class c extends b {
    private final Animation b = new RotateAnimation(0.0f, 720.0f, 1, 0.5f, 1, 0.5f);

    public c(Context context, com.handmark.pulltorefresh.library.c cVar) {
        super(context, cVar);
        new Matrix();
        this.b.setInterpolator(f643a);
        this.b.setDuration(1200);
        this.b.setRepeatCount(-1);
        this.b.setRepeatMode(1);
    }

    /* access modifiers changed from: protected */
    public final int getDefaultBottomDrawableResId() {
        return -1;
    }

    /* access modifiers changed from: protected */
    public final int getDefaultTopDrawableResId() {
        return -1;
    }
}
