package com.handmark.pulltorefresh.library.a;

import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import com.handmark.pulltorefresh.library.c;

public final class a extends b {
    private final Animation b;
    private final Animation c;

    public a(Context context, c cVar) {
        super(context, cVar);
        int i = cVar == c.PULL_DOWN_TO_REFRESH ? 180 : -180;
        this.b = new RotateAnimation(0.0f, (float) i, 1, 0.5f, 1, 0.5f);
        this.b.setInterpolator(f643a);
        this.b.setDuration(150);
        this.b.setFillAfter(true);
        this.c = new RotateAnimation((float) i, 0.0f, 1, 0.5f, 1, 0.5f);
        this.c.setInterpolator(f643a);
        this.c.setDuration(150);
        this.c.setFillAfter(true);
    }

    /* access modifiers changed from: protected */
    public final int getDefaultBottomDrawableResId() {
        return -1;
    }

    /* access modifiers changed from: protected */
    public final int getDefaultTopDrawableResId() {
        return -1;
    }
}
