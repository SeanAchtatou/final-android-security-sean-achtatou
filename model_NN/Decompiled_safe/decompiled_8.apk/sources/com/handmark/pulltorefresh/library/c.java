package com.handmark.pulltorefresh.library;

public enum c {
    DISABLED(0),
    PULL_DOWN_TO_REFRESH(1),
    PULL_UP_TO_REFRESH(2),
    BOTH(3);
    
    private int e;

    private c(int i) {
        this.e = i;
    }

    public static c a(int i) {
        switch (i) {
            case 0:
                return DISABLED;
            case 1:
            default:
                return PULL_DOWN_TO_REFRESH;
            case 2:
                return PULL_UP_TO_REFRESH;
            case 3:
                return BOTH;
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean a() {
        return this == PULL_DOWN_TO_REFRESH || this == BOTH;
    }

    /* access modifiers changed from: package-private */
    public final boolean b() {
        return this == PULL_UP_TO_REFRESH || this == BOTH;
    }

    /* access modifiers changed from: package-private */
    public final int c() {
        return this.e;
    }
}
