package com.handmark.pulltorefresh.library;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import com.handmark.pulltorefresh.library.a.b;
import com.handmark.pulltorefresh.library.a.d;
import com.immomo.momo.android.plugin.cropimage.q;
import com.immomo.momo.g;
import com.immomo.momo.h;
import com.immomo.momo.util.m;
import mm.purchasesdk.PurchaseCode;

public abstract class a extends LinearLayout {
    private static c d = c.PULL_DOWN_TO_REFRESH;
    private static /* synthetic */ int[] y;
    private static /* synthetic */ int[] z;

    /* renamed from: a  reason: collision with root package name */
    protected boolean f642a = false;
    protected View b;
    protected q c;
    private int e;
    private float f;
    private float g;
    private float h;
    private e i = e.RESET;
    private c j = d;
    private c k;
    private FrameLayout l;
    private boolean m = true;
    private boolean n = true;
    private boolean o = true;
    /* access modifiers changed from: private */
    public Interpolator p;
    private b q;
    private b r;
    private b s;
    private int t;
    private int u;
    private q v;
    private q w;
    private d x;

    public a(Context context) {
        super(context);
        new m(getClass().getSimpleName());
        b(context, null);
    }

    public a(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        new m(getClass().getSimpleName());
        b(context, attributeSet);
    }

    public a(Context context, c cVar) {
        super(context);
        new m(getClass().getSimpleName());
        this.j = cVar;
        b(context, null);
    }

    private void a(int i2) {
        a(i2, (long) getPullToRefreshScrollDuration());
    }

    private static void a(View view) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams == null) {
            layoutParams = new ViewGroup.LayoutParams(-1, -2);
        }
        int childMeasureSpec = ViewGroup.getChildMeasureSpec(0, 0, layoutParams.width);
        int i2 = layoutParams.height;
        view.measure(childMeasureSpec, i2 > 0 ? View.MeasureSpec.makeMeasureSpec(i2, 1073741824) : View.MeasureSpec.makeMeasureSpec(0, 0));
    }

    private void b(Context context, AttributeSet attributeSet) {
        Drawable drawable;
        Drawable drawable2;
        setOrientation(1);
        this.e = ViewConfiguration.get(context).getScaledTouchSlop();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, h.PullToRefresh);
        if (obtainStyledAttributes.hasValue(4)) {
            this.j = c.a(obtainStyledAttributes.getInteger(4, 0));
        }
        this.q = b.a(obtainStyledAttributes.getInteger(12, 0));
        this.b = a(context, attributeSet);
        View view = this.b;
        this.l = new FrameLayout(context);
        this.l.addView(view, -1, -1);
        super.addView(this.l, -1, new LinearLayout.LayoutParams(-1, 0, 1.0f));
        this.r = a(context, c.PULL_DOWN_TO_REFRESH, obtainStyledAttributes);
        this.s = new d(context);
        if (obtainStyledAttributes.hasValue(1) && (drawable2 = obtainStyledAttributes.getDrawable(1)) != null) {
            setBackgroundDrawable(drawable2);
        }
        if (obtainStyledAttributes.hasValue(0) && (drawable = obtainStyledAttributes.getDrawable(0)) != null) {
            this.b.setBackgroundDrawable(drawable);
        }
        if (obtainStyledAttributes.hasValue(9)) {
            obtainStyledAttributes.getBoolean(9, true);
        }
        obtainStyledAttributes.recycle();
        f();
    }

    private boolean d() {
        return this.j != c.DISABLED;
    }

    private boolean e() {
        return this.i == e.REFRESHING || this.i == e.MANUAL_REFRESHING;
    }

    private void f() {
        if (this == this.r.getParent()) {
            removeView(this.r);
        }
        if (this.j.a()) {
            super.addView(this.r, 0, new LinearLayout.LayoutParams(-1, -2));
        }
        if (this == this.s.getParent()) {
            removeView(this.s);
        }
        if (this.j.b()) {
            super.addView(this.s, -1, new LinearLayout.LayoutParams(-1, -2));
        }
        h();
        this.k = this.j != c.BOTH ? this.j : c.PULL_DOWN_TO_REFRESH;
    }

    private boolean g() {
        switch (i()[this.j.ordinal()]) {
            case 2:
                return b();
            case 3:
                return c();
            case 4:
                return c() || b();
            default:
                return false;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private void h() {
        this.u = 0;
        this.t = 0;
        if (this.j.a()) {
            a(this.r);
            this.t = this.r.getMeasuredHeight();
        }
        if (this.j.b()) {
            a(this.s);
            this.u = this.s.getMeasuredHeight();
        }
        switch (i()[this.j.ordinal()]) {
            case 1:
                setPadding(0, 0, 0, 0);
                break;
            case 2:
            default:
                setPadding(0, -this.t, 0, 0);
                return;
            case 3:
                setPadding(0, 0, 0, -this.u);
                return;
            case 4:
                break;
        }
        setPadding(0, -this.t, 0, -this.u);
    }

    private static /* synthetic */ int[] i() {
        int[] iArr = y;
        if (iArr == null) {
            iArr = new int[c.values().length];
            try {
                iArr[c.BOTH.ordinal()] = 4;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[c.DISABLED.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[c.PULL_DOWN_TO_REFRESH.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[c.PULL_UP_TO_REFRESH.ordinal()] = 3;
            } catch (NoSuchFieldError e5) {
            }
            y = iArr;
        }
        return iArr;
    }

    private static /* synthetic */ int[] j() {
        int[] iArr = z;
        if (iArr == null) {
            iArr = new int[e.values().length];
            try {
                iArr[e.MANUAL_REFRESHING.ordinal()] = 5;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[e.PULL_TO_REFRESH.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[e.REFRESHING.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[e.RELEASE_TO_REFRESH.ordinal()] = 3;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[e.RESET.ordinal()] = 1;
            } catch (NoSuchFieldError e6) {
            }
            z = iArr;
        }
        return iArr;
    }

    /* access modifiers changed from: protected */
    public abstract View a(Context context, AttributeSet attributeSet);

    /* access modifiers changed from: protected */
    public b a(Context context, c cVar, TypedArray typedArray) {
        return this.q.a(context, cVar);
    }

    public final void a() {
        if (e()) {
            a(e.RESET, new boolean[0]);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(int i2, long j2) {
        if (this.x != null) {
            this.x.a();
        }
        if (getScrollY() != i2) {
            if (this.p == null) {
                this.p = new DecelerateInterpolator();
            }
            this.x = new d(this, getScrollY(), i2, j2);
            post(this.x);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(e eVar, boolean... zArr) {
        this.i = eVar;
        switch (j()[this.i.ordinal()]) {
            case 1:
                this.f642a = false;
                if (this.j.a()) {
                    b bVar = this.r;
                    b.e();
                }
                if (this.j.b()) {
                    b bVar2 = this.s;
                    b.e();
                }
                a(0);
                break;
            case 2:
                switch (i()[this.k.ordinal()]) {
                    case 2:
                        b bVar3 = this.r;
                        b.b();
                        break;
                    case 3:
                        b bVar4 = this.s;
                        b.b();
                        break;
                }
            case 3:
                switch (i()[this.k.ordinal()]) {
                    case 2:
                        b bVar5 = this.r;
                        b.d();
                        break;
                    case 3:
                        b bVar6 = this.s;
                        b.d();
                        break;
                }
            case 4:
            case 5:
                boolean z2 = zArr[0];
                if (this.j.a()) {
                    this.r.c();
                }
                if (this.j.b()) {
                    this.s.c();
                }
                if (z2) {
                    if (this.m) {
                        a(this.k == c.PULL_DOWN_TO_REFRESH ? -this.t : this.u);
                    } else {
                        a(0);
                    }
                }
                setRefreshingInternal(z2);
                break;
        }
        if (this.w != null) {
            q qVar = this.w;
            e eVar2 = this.i;
            c cVar = this.k;
        }
    }

    public void addView(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        View refreshableView = getRefreshableView();
        if (refreshableView instanceof ViewGroup) {
            ((ViewGroup) refreshableView).addView(view, i2, layoutParams);
            return;
        }
        throw new UnsupportedOperationException("Refreshable View is not a ViewGroup so can't addView");
    }

    /* access modifiers changed from: protected */
    public abstract boolean b();

    /* access modifiers changed from: protected */
    public abstract boolean c();

    public final c getCurrentMode() {
        return this.k;
    }

    public final boolean getFilterTouchEvents() {
        return this.o;
    }

    /* access modifiers changed from: protected */
    public int getFooterHeight() {
        return this.u;
    }

    /* access modifiers changed from: protected */
    public b getFooterLayout() {
        return this.s;
    }

    /* access modifiers changed from: protected */
    public int getHeaderHeight() {
        return this.t;
    }

    /* access modifiers changed from: protected */
    public b getHeaderLayout() {
        return this.r;
    }

    public final c getMode() {
        return this.j;
    }

    /* access modifiers changed from: protected */
    public int getPullToRefreshScrollDuration() {
        return PurchaseCode.LOADCHANNEL_ERR;
    }

    /* access modifiers changed from: protected */
    public int getPullToRefreshScrollDurationLonger() {
        return 325;
    }

    public final View getRefreshableView() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public FrameLayout getRefreshableViewWrapper() {
        return this.l;
    }

    public final boolean getShowViewWhileRefreshing() {
        return this.m;
    }

    public final e getState() {
        return this.i;
    }

    public final boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z2 = false;
        if (!d()) {
            return false;
        }
        int action = motionEvent.getAction();
        if (action == 3 || action == 1) {
            this.f642a = false;
            return false;
        } else if (action != 0 && this.f642a) {
            return true;
        } else {
            switch (action) {
                case 0:
                    if (g()) {
                        float y2 = motionEvent.getY();
                        this.h = y2;
                        this.g = y2;
                        this.f = motionEvent.getX();
                        this.f642a = false;
                        break;
                    }
                    break;
                case 2:
                    if (!this.n || !e()) {
                        if (g()) {
                            float y3 = motionEvent.getY();
                            float f2 = y3 - this.g;
                            float abs = Math.abs(f2);
                            float abs2 = Math.abs(motionEvent.getX() - this.f);
                            if (g.G() != null && g.G().toLowerCase().indexOf("meizu") >= 0) {
                                z2 = true;
                            }
                            if (!z2 ? abs > ((float) this.e) : 10.0f * abs > 4.0f) {
                                if (!this.o || abs > abs2) {
                                    if (!this.j.a() || f2 < 1.0f || !b()) {
                                        if (this.j.b() && f2 <= -1.0f && c()) {
                                            this.g = y3;
                                            this.f642a = true;
                                            if (this.j == c.BOTH) {
                                                this.k = c.PULL_UP_TO_REFRESH;
                                                break;
                                            }
                                        }
                                    } else {
                                        this.g = y3;
                                        this.f642a = true;
                                        if (this.j == c.BOTH) {
                                            this.k = c.PULL_DOWN_TO_REFRESH;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        return true;
                    }
                    break;
            }
            return this.f642a;
        }
    }

    /* access modifiers changed from: protected */
    public final void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            this.j = c.a(bundle.getInt("ptr_mode", 0));
            this.k = c.a(bundle.getInt("ptr_current_mode", 0));
            this.n = bundle.getBoolean("ptr_disable_scrolling", true);
            this.m = bundle.getBoolean("ptr_show_refreshing_view", true);
            super.onRestoreInstanceState(bundle.getParcelable("ptr_super"));
            e a2 = e.a(bundle.getInt("ptr_state", 0));
            if (a2 == e.REFRESHING || a2 == e.MANUAL_REFRESHING) {
                a(a2, true);
                return;
            }
            return;
        }
        super.onRestoreInstanceState(parcelable);
    }

    /* access modifiers changed from: protected */
    public final Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putInt("ptr_state", this.i.a());
        bundle.putInt("ptr_mode", this.j.c());
        bundle.putInt("ptr_current_mode", this.k.c());
        bundle.putBoolean("ptr_disable_scrolling", this.n);
        bundle.putBoolean("ptr_show_refreshing_view", this.m);
        bundle.putParcelable("ptr_super", super.onSaveInstanceState());
        return bundle;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    public boolean onTouchEvent(MotionEvent motionEvent) {
        float max;
        int i2;
        if (!d()) {
            return false;
        }
        if (this.n && e()) {
            return true;
        }
        if (motionEvent.getAction() == 0 && motionEvent.getEdgeFlags() != 0) {
            return false;
        }
        switch (motionEvent.getAction()) {
            case 0:
                if (g()) {
                    float y2 = motionEvent.getY();
                    this.h = y2;
                    this.g = y2;
                    return true;
                }
                break;
            case 1:
            case 3:
                if (this.f642a) {
                    this.f642a = false;
                    if (this.i == e.RELEASE_TO_REFRESH) {
                        if (this.c != null) {
                            a(e.REFRESHING, true);
                            q qVar = this.c;
                            return true;
                        } else if (this.v != null) {
                            a(e.REFRESHING, true);
                            if (this.k == c.PULL_DOWN_TO_REFRESH) {
                                q qVar2 = this.v;
                            } else if (this.k == c.PULL_UP_TO_REFRESH) {
                                q qVar3 = this.v;
                            }
                            return true;
                        }
                    }
                    a(e.RESET, new boolean[0]);
                    return true;
                }
                break;
            case 2:
                if (this.f642a) {
                    this.g = motionEvent.getY();
                    switch (i()[this.k.ordinal()]) {
                        case 3:
                            max = Math.max(this.h - this.g, 0.0f) / 3.0f;
                            i2 = this.u;
                            break;
                        default:
                            max = Math.min(this.h - this.g, 0.0f) / 3.0f;
                            i2 = this.t;
                            break;
                    }
                    setHeaderScroll(max);
                    if (max != 0.0f) {
                        Math.abs(max);
                        switch (i()[this.k.ordinal()]) {
                            case 2:
                                this.r.a();
                                break;
                            case 3:
                                this.s.a();
                                break;
                        }
                        if (this.i != e.PULL_TO_REFRESH && ((float) i2) >= Math.abs(max)) {
                            a(e.PULL_TO_REFRESH, new boolean[0]);
                        } else if (this.i == e.PULL_TO_REFRESH && ((float) i2) < Math.abs(max)) {
                            a(e.RELEASE_TO_REFRESH, new boolean[0]);
                        }
                    }
                    return true;
                }
                break;
        }
        return false;
    }

    public final void setDisableScrollingWhileRefreshing(boolean z2) {
        this.n = z2;
    }

    public final void setFilterTouchEvents(boolean z2) {
        this.o = z2;
    }

    /* access modifiers changed from: protected */
    public void setHeaderScroll(float f2) {
        scrollTo(0, Math.round(f2));
    }

    public void setLastUpdatedLabel(CharSequence charSequence) {
        if (this.r != null) {
            this.r.setSubHeaderText(charSequence);
        }
        if (this.s != null) {
            this.s.setSubHeaderText(charSequence);
        }
        h();
    }

    public void setLoadingDrawable(Drawable drawable) {
        c cVar = c.BOTH;
        if (this.r != null && cVar.a()) {
            this.r.setLoadingDrawable(drawable);
        }
        if (this.s != null && cVar.b()) {
            this.s.setLoadingDrawable(drawable);
        }
        h();
    }

    public void setLongClickable(boolean z2) {
        getRefreshableView().setLongClickable(z2);
    }

    public final void setMode(c cVar) {
        if (cVar != this.j) {
            this.j = cVar;
            f();
        }
    }

    public void setOnPullEventListener$3c7bf85f(q qVar) {
        this.w = qVar;
    }

    public final void setOnRefreshListener$324c1adb(q qVar) {
        this.v = qVar;
        this.c = null;
    }

    public final void setOnRefreshListener$ee4e519(q qVar) {
        this.c = qVar;
        this.v = null;
    }

    public void setPullLabel(CharSequence charSequence) {
        c cVar = c.BOTH;
        if (this.r != null && cVar.a()) {
            this.r.setPullLabel(charSequence);
        }
        if (this.s != null && cVar.b()) {
            this.s.setPullLabel(charSequence);
        }
    }

    public final void setPullToRefreshEnabled(boolean z2) {
        setMode(z2 ? d : c.DISABLED);
    }

    public final void setPullToRefreshOverScrollEnabled(boolean z2) {
    }

    public final void setRefreshing(boolean z2) {
        if (!e()) {
            a(e.MANUAL_REFRESHING, z2);
        }
    }

    /* access modifiers changed from: protected */
    public void setRefreshingInternal(boolean z2) {
    }

    public void setRefreshingLabel(CharSequence charSequence) {
        c cVar = c.BOTH;
        if (this.r != null && cVar.a()) {
            this.r.setRefreshingLabel(charSequence);
        }
        if (this.s != null && cVar.b()) {
            this.s.setRefreshingLabel(charSequence);
        }
    }

    public void setReleaseLabel(CharSequence charSequence) {
        c cVar = c.BOTH;
        if (this.r != null && cVar.a()) {
            this.r.setReleaseLabel(charSequence);
        }
        if (this.s != null && cVar.b()) {
            this.s.setReleaseLabel(charSequence);
        }
    }

    public void setScrollAnimationInterpolator(Interpolator interpolator) {
        this.p = interpolator;
    }

    public final void setShowViewWhileRefreshing(boolean z2) {
        this.m = z2;
    }
}
