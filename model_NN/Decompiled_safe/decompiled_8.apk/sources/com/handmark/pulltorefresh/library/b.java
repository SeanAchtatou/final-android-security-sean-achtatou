package com.handmark.pulltorefresh.library;

import android.content.Context;
import com.handmark.pulltorefresh.library.a.a;
import com.handmark.pulltorefresh.library.a.c;

public enum b {
    ROTATE,
    FLIP;
    
    private static /* synthetic */ int[] c;

    public static b a(int i) {
        switch (i) {
            case 1:
                return FLIP;
            default:
                return ROTATE;
        }
    }

    public final com.handmark.pulltorefresh.library.a.b a(Context context, c cVar) {
        switch (a()[ordinal()]) {
            case 2:
                return new a(context, cVar);
            default:
                return new c(context, cVar);
        }
    }
}
