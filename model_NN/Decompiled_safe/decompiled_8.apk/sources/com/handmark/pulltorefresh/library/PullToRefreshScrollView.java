package com.handmark.pulltorefresh.library;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ScrollView;
import com.handmark.pulltorefresh.library.a.b;
import com.immomo.momo.R;
import com.immomo.momo.android.plugin.cropimage.q;
import com.immomo.momo.util.m;

public class PullToRefreshScrollView extends a {
    private static int d = 700;

    public PullToRefreshScrollView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        new m(this);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ View a(Context context, AttributeSet attributeSet) {
        f fVar = new f(context, attributeSet);
        fVar.setId(R.id.scrollview);
        fVar.setVerticalScrollBarEnabled(false);
        return fVar;
    }

    /* access modifiers changed from: protected */
    public final b a(Context context, c cVar, TypedArray typedArray) {
        return super.a(context, cVar, typedArray);
    }

    /* access modifiers changed from: protected */
    public final boolean b() {
        return ((ScrollView) this.b).getScrollY() == 0;
    }

    /* access modifiers changed from: protected */
    public final boolean c() {
        View childAt = ((ScrollView) this.b).getChildAt(0);
        if (childAt != null) {
            return ((ScrollView) this.b).getScrollY() >= childAt.getHeight() - getHeight();
        }
        return false;
    }

    public final void d() {
        ((ScrollView) getRefreshableView()).smoothScrollTo(0, 0);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
    }

    /* access modifiers changed from: protected */
    public void onScrollChanged(int i, int i2, int i3, int i4) {
        super.onScrollChanged(i, i2, i3, i4);
        if (i2 <= (-getHeight()) && this.c != null) {
            q qVar = this.c;
            a(e.RESET, new boolean[0]);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 1:
            case 3:
                if (this.f642a) {
                    this.f642a = false;
                    if (getState() == e.RELEASE_TO_REFRESH) {
                        a(-getHeight(), (long) d);
                        return true;
                    }
                    a(e.RESET, new boolean[0]);
                    return true;
                }
                break;
        }
        return super.onTouchEvent(motionEvent);
    }
}
