package com.handmark.pulltorefresh.library.a;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.LinearLayout;
import com.handmark.pulltorefresh.library.c;
import com.immomo.momo.R;

public abstract class b extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    static final Interpolator f643a = new LinearInterpolator();
    private static /* synthetic */ int[] c;
    private boolean b;

    public b(Context context) {
        super(context);
    }

    public b(Context context, c cVar) {
        super(context);
        setGravity(16);
        setPadding(0, 0, 0, 0);
        LayoutInflater.from(context).inflate((int) R.layout.userguiforthree_header, this);
        f();
        cVar.ordinal();
    }

    public static void b() {
    }

    public static void d() {
    }

    public static void e() {
    }

    private static /* synthetic */ int[] f() {
        int[] iArr = c;
        if (iArr == null) {
            iArr = new int[c.values().length];
            try {
                iArr[c.BOTH.ordinal()] = 4;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[c.DISABLED.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[c.PULL_DOWN_TO_REFRESH.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[c.PULL_UP_TO_REFRESH.ordinal()] = 3;
            } catch (NoSuchFieldError e4) {
            }
            c = iArr;
        }
        return iArr;
    }

    public final void a() {
        boolean z = this.b;
    }

    public final void c() {
        boolean z = this.b;
    }

    /* access modifiers changed from: protected */
    public abstract int getDefaultBottomDrawableResId();

    /* access modifiers changed from: protected */
    public abstract int getDefaultTopDrawableResId();

    public final void setLoadingDrawable(Drawable drawable) {
        this.b = drawable instanceof AnimationDrawable;
    }

    public void setPullLabel(CharSequence charSequence) {
    }

    public void setRefreshingLabel(CharSequence charSequence) {
    }

    public void setReleaseLabel(CharSequence charSequence) {
    }

    public void setSubHeaderText(CharSequence charSequence) {
    }

    public void setSubTextAppearance(int i) {
    }

    public void setSubTextColor(int i) {
        setSubTextColor(ColorStateList.valueOf(i));
    }

    public void setSubTextColor(ColorStateList colorStateList) {
    }

    public void setTextAppearance(int i) {
    }

    public void setTextColor(int i) {
        setTextColor(ColorStateList.valueOf(i));
    }

    public void setTextColor(ColorStateList colorStateList) {
    }
}
