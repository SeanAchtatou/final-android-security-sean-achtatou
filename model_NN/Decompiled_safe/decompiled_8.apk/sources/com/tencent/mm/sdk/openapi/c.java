package com.tencent.mm.sdk.openapi;

import android.os.Bundle;
import com.tencent.mm.sdk.b.a;

public final class c extends a {
    public WXMediaMessage b;
    public int c;

    public final void a(Bundle bundle) {
        super.a(bundle);
        WXMediaMessage wXMediaMessage = this.b;
        Bundle bundle2 = new Bundle();
        bundle2.putInt("_wxobject_sdkVer", wXMediaMessage.sdkVer);
        bundle2.putString("_wxobject_title", wXMediaMessage.title);
        bundle2.putString("_wxobject_description", wXMediaMessage.description);
        bundle2.putByteArray("_wxobject_thumbdata", wXMediaMessage.thumbData);
        if (wXMediaMessage.mediaObject != null) {
            bundle2.putString("_wxobject_identifier_", wXMediaMessage.mediaObject.getClass().getName());
            wXMediaMessage.mediaObject.serialize(bundle2);
        }
        bundle.putAll(bundle2);
        bundle.putInt("_wxapi_sendmessagetowx_req_scene", this.c);
    }

    /* access modifiers changed from: package-private */
    public final boolean a() {
        if (this.b != null) {
            return this.b.checkArgs();
        }
        a.a("MicroMsg.SDK.SendMessageToWX.Req", "checkArgs fail ,message is null");
        return false;
    }
}
