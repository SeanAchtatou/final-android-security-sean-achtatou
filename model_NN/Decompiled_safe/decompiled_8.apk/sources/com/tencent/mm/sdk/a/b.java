package com.tencent.mm.sdk.a;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.immomo.a.a.a.c;
import com.immomo.a.a.a.d;
import com.tencent.mm.sdk.b.a;

public final class b implements c {
    public static boolean a(Context context, String str, String str2, String str3, Bundle bundle) {
        if (context == null || str == null || str.length() == 0 || str2 == null || str2.length() == 0) {
            a.a("MicroMsg.SDK.MMessageAct", "send fail, invalid arguments");
            return false;
        }
        Intent intent = new Intent();
        intent.setClassName(str, str2);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        String packageName = context.getPackageName();
        intent.putExtra("_mmessage_sdkVersion", 553910273);
        intent.putExtra("_mmessage_appPackage", packageName);
        intent.putExtra("_mmessage_content", str3);
        intent.putExtra("_mmessage_checksum", a.a(str3, packageName));
        intent.addFlags(268435456).addFlags(134217728);
        try {
            context.startActivity(intent);
            a.b("MicroMsg.SDK.MMessageAct", "send mm message, intent=" + intent);
            return true;
        } catch (ActivityNotFoundException e) {
            a.a("MicroMsg.SDK.MMessageAct", "send fail, target ActivityNotFound");
            return false;
        }
    }

    public final com.immomo.a.a.a.a a(String str) {
        return new d(str);
    }
}
