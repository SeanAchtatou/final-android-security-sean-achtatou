package com.tencent.mm.sdk.a;

import android.content.Context;
import android.content.Intent;

public final class a {
    public static void a(Context context, String str, String str2, String str3) {
        String str4 = str + ".permission.MM_MESSAGE";
        Intent intent = new Intent(str2);
        String packageName = context.getPackageName();
        intent.putExtra("_mmessage_sdkVersion", 553910273);
        intent.putExtra("_mmessage_appPackage", packageName);
        intent.putExtra("_mmessage_content", str3);
        intent.putExtra("_mmessage_checksum", a(str3, packageName));
        context.sendBroadcast(intent, str4);
        com.tencent.mm.sdk.b.a.b("MicroMsg.SDK.MMessage", "send mm message, intent=" + intent + ", perm=" + str4);
    }

    static byte[] a(String str, String str2) {
        StringBuffer stringBuffer = new StringBuffer();
        if (str != null) {
            stringBuffer.append(str);
        }
        stringBuffer.append(553910273);
        stringBuffer.append(str2);
        stringBuffer.append("mMcShCsTr");
        return android.support.v4.b.a.a(stringBuffer.toString().substring(1, 9).getBytes()).getBytes();
    }
}
