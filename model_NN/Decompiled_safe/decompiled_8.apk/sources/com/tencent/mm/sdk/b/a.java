package com.tencent.mm.sdk.b;

import android.os.Build;
import android.util.Log;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static int f3116a = 6;

    static {
        StringBuilder sb = new StringBuilder();
        sb.append("VERSION.RELEASE:[" + Build.VERSION.RELEASE);
        sb.append("] VERSION.CODENAME:[" + Build.VERSION.CODENAME);
        sb.append("] VERSION.INCREMENTAL:[" + Build.VERSION.INCREMENTAL);
        sb.append("] BOARD:[" + Build.BOARD);
        sb.append("] DEVICE:[" + Build.DEVICE);
        sb.append("] DISPLAY:[" + Build.DISPLAY);
        sb.append("] FINGERPRINT:[" + Build.FINGERPRINT);
        sb.append("] HOST:[" + Build.HOST);
        sb.append("] MANUFACTURER:[" + Build.MANUFACTURER);
        sb.append("] MODEL:[" + Build.MODEL);
        sb.append("] PRODUCT:[" + Build.PRODUCT);
        sb.append("] TAGS:[" + Build.TAGS);
        sb.append("] TYPE:[" + Build.TYPE);
        sb.append("] USER:[" + Build.USER + "]");
        sb.toString();
    }

    public static void a(String str, String str2) {
        if (f3116a <= 4) {
            Log.e(str, str2);
            "E/" + str;
            b.a();
        }
    }

    public static void b(String str, String str2) {
        if (f3116a <= 1) {
            Log.d(str, str2);
            "D/" + str;
            b.a();
        }
    }
}
