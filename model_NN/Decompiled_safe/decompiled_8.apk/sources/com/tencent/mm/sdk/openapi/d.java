package com.tencent.mm.sdk.openapi;

import android.content.Context;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private static d f3119a = null;

    private d() {
    }

    public static b a(Context context, String str) {
        if (f3119a == null) {
            f3119a = new d();
        }
        return new b(context, str);
    }
}
