package com.tencent.mm.sdk.openapi;

import android.content.Context;
import android.os.Bundle;
import com.tencent.mm.sdk.b.a;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private Context f3118a;
    private String b;

    protected b(Context context, String str) {
        this(context, str, (byte) 0);
    }

    private b(Context context, String str, byte b2) {
        this.f3118a = context;
        this.b = str;
    }

    private static boolean a() {
        a.b("MicroMsg.SDK.WXApiImplV10", "ignore wechat app signature validation");
        return true;
    }

    public final boolean a(a aVar) {
        a();
        if (!aVar.a()) {
            a.a("MicroMsg.SDK.WXApiImplV10", "sendReq checkArgs fail");
            return false;
        }
        Bundle bundle = new Bundle();
        aVar.a(bundle);
        return com.tencent.mm.sdk.a.b.a(this.f3118a, "com.tencent.mm", "com.tencent.mm.plugin.base.stub.WXEntryActivity", "weixin://sendreq?appid=" + this.b, bundle);
    }

    public final boolean a(String str) {
        a();
        if (str != null) {
            this.b = str;
        }
        a.b("MicroMsg.SDK.WXApiImplV10", "register app " + this.f3118a.getPackageName());
        com.tencent.mm.sdk.a.a.a(this.f3118a, "com.tencent.mm", "com.tencent.mm.plugin.openapi.Intent.ACTION_HANDLE_APP_REGISTER", "weixin://registerapp?appid=" + this.b);
        return true;
    }
}
