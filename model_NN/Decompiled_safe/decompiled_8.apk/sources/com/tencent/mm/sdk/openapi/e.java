package com.tencent.mm.sdk.openapi;

import android.os.Bundle;

public interface e {
    boolean checkArgs();

    void serialize(Bundle bundle);

    int type();
}
