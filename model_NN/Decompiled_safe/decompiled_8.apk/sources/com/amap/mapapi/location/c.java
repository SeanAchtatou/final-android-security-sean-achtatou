package com.amap.mapapi.location;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

public class c implements LocationListener {

    /* renamed from: a  reason: collision with root package name */
    private LocationManagerProxy f444a;
    private LocationListener b = null;

    public c(LocationManagerProxy locationManagerProxy) {
        this.f444a = locationManagerProxy;
    }

    public void a() {
        if (this.f444a != null) {
            this.f444a.removeUpdates(this);
        }
        this.b = null;
    }

    public boolean a(LocationListener locationListener, long j, float f) {
        boolean z = false;
        this.b = locationListener;
        for (String str : this.f444a.getProviders(true)) {
            if (LocationManagerProxy.GPS_PROVIDER.equals(str) || LocationManagerProxy.NETWORK_PROVIDER.equals(str)) {
                this.f444a.requestLocationUpdates(str, j, f, this);
                z = true;
            }
        }
        return z;
    }

    public boolean a(LocationListener locationListener, long j, float f, String str) {
        this.b = locationListener;
        if (!LocationProviderProxy.MapABCNetwork.equals(str)) {
            return false;
        }
        this.f444a.requestLocationUpdates(str, j, f, this);
        return true;
    }

    public void onLocationChanged(Location location) {
        if (this.b != null) {
            this.b.onLocationChanged(location);
        }
    }

    public void onProviderDisabled(String str) {
        if (this.b != null) {
            this.b.onProviderDisabled(str);
        }
    }

    public void onProviderEnabled(String str) {
        if (this.b != null) {
            this.b.onProviderEnabled(str);
        }
    }

    public void onStatusChanged(String str, int i, Bundle bundle) {
        if (this.b != null) {
            this.b.onStatusChanged(str, i, bundle);
        }
    }
}
