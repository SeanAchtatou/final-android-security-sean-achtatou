package com.amap.mapapi.location;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.a.a.a.h;
import com.a.a.a.i;
import com.amap.mapapi.core.d;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.util.ArrayList;
import java.util.Iterator;
import mm.purchasesdk.PurchaseCode;

public class a implements Runnable {
    private static a b = null;
    /* access modifiers changed from: private */
    public static int f = 100;
    /* access modifiers changed from: private */
    public static int g = PurchaseCode.ORDER_OK;
    /* access modifiers changed from: private */
    public static int h = PurchaseCode.UNSUB_OK;
    /* access modifiers changed from: private */
    public static long r;

    /* renamed from: a  reason: collision with root package name */
    private h f440a = null;
    private C0001a c = null;
    private volatile boolean d = true;
    private Thread e = null;
    /* access modifiers changed from: private */
    public ArrayList i = null;
    private Location j = null;
    private Context k;
    /* access modifiers changed from: private */
    public LocationManager l;
    private boolean m = false;
    /* access modifiers changed from: private */
    public Location n = null;
    private float o = 20.0f;
    private long p = 2000;
    private long q = 5000;
    /* access modifiers changed from: private */
    public LocationListener s = new b(this);

    /* renamed from: com.amap.mapapi.location.a$a  reason: collision with other inner class name */
    class C0001a extends Handler {
        C0001a() {
        }

        public void handleMessage(Message message) {
            if (message.what == a.f) {
                Iterator it = a.this.i.iterator();
                while (it.hasNext()) {
                    ((b) it.next()).c.onLocationChanged(((b) message.obj).f);
                }
            } else if (message.what != a.g && message.what == a.h) {
                LocationListener locationListener = (LocationListener) message.obj;
                int size = a.this.i.size();
                for (int i = 0; i < size; i++) {
                    b bVar = (b) a.this.i.get(i);
                    if (locationListener.equals(bVar.c)) {
                        a.this.i.remove(bVar);
                    }
                }
                if (a.this.l != null && a.this.i.size() == 0) {
                    a.this.l.removeUpdates(a.this.s);
                }
            }
        }
    }

    public class b {

        /* renamed from: a  reason: collision with root package name */
        public long f442a;
        public float b;
        public LocationListener c;
        public long d;
        public boolean e = true;
        public Location f = null;

        public b(long j, float f2, LocationListener locationListener) {
            this.f442a = j;
            this.b = f2;
            this.c = locationListener;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            return this.c == null ? bVar.c == null : this.c.equals(bVar.c);
        }

        public int hashCode() {
            return (this.c == null ? 0 : this.c.hashCode()) + 31;
        }
    }

    private a(Context context, LocationManager locationManager) {
        this.k = context;
        this.l = locationManager;
        this.c = new C0001a();
        this.f440a = new com.a.a.a.a(context.getApplicationContext());
        this.f440a.b("autonavi");
        this.f440a.a("401FFB6E52385325E41206A6AFF7A316");
        this.i = new ArrayList();
    }

    private Location a(i iVar) {
        Location location = new Location(PoiTypeDef.All);
        location.setProvider(LocationProviderProxy.MapABCNetwork);
        location.setLatitude(iVar.d());
        location.setLongitude(iVar.c());
        location.setAccuracy((float) iVar.e());
        Bundle bundle = new Bundle();
        bundle.putString("citycode", iVar.a());
        bundle.putString("desc", iVar.b());
        location.setExtras(bundle);
        return location;
    }

    public static synchronized a a(Context context, LocationManager locationManager) {
        a aVar;
        synchronized (a.class) {
            if (b == null) {
                b = new a(context, locationManager);
            }
            aVar = b;
        }
        return aVar;
    }

    public void a() {
        this.d = false;
        if (this.e != null) {
            this.e.interrupt();
        }
        if (this.l != null) {
            this.l.removeUpdates(this.s);
        }
        if (this.c != null) {
            this.c.removeMessages(f);
        }
        if (this.f440a != null) {
            this.f440a.a();
        }
        this.i.clear();
        this.f440a = null;
        b = null;
        this.j = null;
        this.c = null;
    }

    public void a(long j2, float f2, LocationListener locationListener) {
        this.i.add(new b(j2, f2, locationListener));
        if (this.l != null && this.i.size() == 1) {
            this.l.requestLocationUpdates(LocationManagerProxy.GPS_PROVIDER, this.p, this.o, this.s);
            this.m = true;
        }
    }

    public void a(LocationListener locationListener) {
        if (this.c != null) {
            Message obtainMessage = this.c.obtainMessage();
            obtainMessage.what = h;
            obtainMessage.obj = locationListener;
            this.c.sendMessage(obtainMessage);
        }
    }

    public Location b() {
        return this.j != null ? this.j : d.d(this.k);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:74:0x00fc, code lost:
        java.lang.Thread.currentThread().interrupt();
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r14 = this;
            r3 = 5000(0x1388, double:2.4703E-320)
        L_0x0002:
            boolean r0 = r14.d
            if (r0 == 0) goto L_0x0020
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            r14.e = r0
            java.util.ArrayList r0 = r14.i
            int r0 = r0.size()
            if (r0 == 0) goto L_0x0002
            java.util.ArrayList r5 = r14.i
            monitor-enter(r5)
            java.util.ArrayList r0 = r14.i     // Catch:{ all -> 0x006f }
            int r0 = r0.size()     // Catch:{ all -> 0x006f }
            if (r0 != 0) goto L_0x0021
            monitor-exit(r5)     // Catch:{ all -> 0x006f }
        L_0x0020:
            return
        L_0x0021:
            java.util.ArrayList r0 = r14.i     // Catch:{ all -> 0x006f }
            java.util.ArrayList r1 = r14.i     // Catch:{ all -> 0x006f }
            int r1 = r1.size()     // Catch:{ all -> 0x006f }
            int r1 = r1 + -1
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x006f }
            com.amap.mapapi.location.a$b r0 = (com.amap.mapapi.location.a.b) r0     // Catch:{ all -> 0x006f }
            long r6 = com.amap.mapapi.core.d.a()     // Catch:{ all -> 0x006f }
            float r1 = r14.o     // Catch:{ all -> 0x006f }
            float r2 = r0.b     // Catch:{ all -> 0x006f }
            int r1 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r1 <= 0) goto L_0x0072
            float r1 = r0.b     // Catch:{ all -> 0x006f }
        L_0x003f:
            r14.o = r1     // Catch:{ all -> 0x006f }
            long r1 = r14.p     // Catch:{ all -> 0x006f }
            long r8 = r0.f442a     // Catch:{ all -> 0x006f }
            int r1 = (r1 > r8 ? 1 : (r1 == r8 ? 0 : -1))
            if (r1 <= 0) goto L_0x0075
            long r1 = r0.f442a     // Catch:{ all -> 0x006f }
        L_0x004b:
            r14.p = r1     // Catch:{ all -> 0x006f }
            if (r0 == 0) goto L_0x00f3
            long r1 = r0.f442a     // Catch:{ all -> 0x006f }
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 >= 0) goto L_0x0078
            r1 = r3
        L_0x0056:
            r14.q = r1     // Catch:{ all -> 0x006f }
            long r1 = r0.d     // Catch:{ all -> 0x006f }
            long r1 = r6 - r1
            long r8 = r0.f442a     // Catch:{ all -> 0x006f }
            int r1 = (r1 > r8 ? 1 : (r1 == r8 ? 0 : -1))
            if (r1 < 0) goto L_0x0065
            r1 = 1
            r0.e = r1     // Catch:{ all -> 0x006f }
        L_0x0065:
            boolean r1 = r0.e     // Catch:{ all -> 0x006f }
            if (r1 == 0) goto L_0x00f3
            com.amap.mapapi.location.a$a r1 = r14.c     // Catch:{ all -> 0x006f }
            if (r1 != 0) goto L_0x007b
            monitor-exit(r5)     // Catch:{ all -> 0x006f }
            goto L_0x0002
        L_0x006f:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x0072:
            float r1 = r14.o     // Catch:{ all -> 0x006f }
            goto L_0x003f
        L_0x0075:
            long r1 = r14.p     // Catch:{ all -> 0x006f }
            goto L_0x004b
        L_0x0078:
            long r1 = r0.f442a     // Catch:{ all -> 0x006f }
            goto L_0x0056
        L_0x007b:
            com.amap.mapapi.location.a$a r1 = r14.c     // Catch:{ all -> 0x006f }
            android.os.Message r2 = r1.obtainMessage()     // Catch:{ all -> 0x006f }
            long r8 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x006f }
            long r10 = com.amap.mapapi.location.a.r     // Catch:{ all -> 0x006f }
            long r8 = r8 - r10
            long r10 = r14.p     // Catch:{ all -> 0x006f }
            r12 = 10
            long r10 = r10 * r12
            int r1 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r1 <= 0) goto L_0x00d4
            r1 = 0
            r14.m = r1     // Catch:{ all -> 0x006f }
        L_0x0094:
            android.location.Location r1 = r14.n     // Catch:{ Exception -> 0x00e0 }
            if (r1 == 0) goto L_0x00d8
            boolean r1 = r14.m     // Catch:{ Exception -> 0x00e0 }
            if (r1 == 0) goto L_0x00d8
            com.a.a.a.h r1 = r14.f440a     // Catch:{ Exception -> 0x00e0 }
            android.location.Location r8 = r14.n     // Catch:{ Exception -> 0x00e0 }
            com.a.a.a.i r1 = r1.a(r8)     // Catch:{ Exception -> 0x00e0 }
            if (r1 == 0) goto L_0x00b0
            android.location.Location r8 = r14.n     // Catch:{ Exception -> 0x00e0 }
            float r8 = r8.getAccuracy()     // Catch:{ Exception -> 0x00e0 }
            double r8 = (double) r8     // Catch:{ Exception -> 0x00e0 }
            r1.c(r8)     // Catch:{ Exception -> 0x00e0 }
        L_0x00b0:
            if (r1 == 0) goto L_0x00f3
            r8 = 0
            r0.e = r8     // Catch:{ all -> 0x006f }
            r0.d = r6     // Catch:{ all -> 0x006f }
            android.location.Location r1 = r14.a(r1)     // Catch:{ all -> 0x006f }
            android.location.Location r6 = r14.j     // Catch:{ all -> 0x006f }
            boolean r6 = r1.equals(r6)     // Catch:{ all -> 0x006f }
            if (r6 != 0) goto L_0x00f3
            r14.j = r1     // Catch:{ all -> 0x006f }
            r0.f = r1     // Catch:{ all -> 0x006f }
            int r6 = com.amap.mapapi.location.a.f     // Catch:{ all -> 0x006f }
            r2.what = r6     // Catch:{ all -> 0x006f }
            r2.obj = r0     // Catch:{ all -> 0x006f }
            com.amap.mapapi.location.a$a r0 = r14.c     // Catch:{ all -> 0x006f }
            if (r0 != 0) goto L_0x00e9
            monitor-exit(r5)     // Catch:{ all -> 0x006f }
            goto L_0x0002
        L_0x00d4:
            r1 = 1
            r14.m = r1     // Catch:{ all -> 0x006f }
            goto L_0x0094
        L_0x00d8:
            com.a.a.a.h r1 = r14.f440a     // Catch:{ Exception -> 0x00e0 }
            r8 = 0
            com.a.a.a.i r1 = r1.a(r8)     // Catch:{ Exception -> 0x00e0 }
            goto L_0x00b0
        L_0x00e0:
            r1 = move-exception
            if (r0 == 0) goto L_0x00e6
            r1 = 1
            r0.e = r1     // Catch:{ all -> 0x006f }
        L_0x00e6:
            monitor-exit(r5)     // Catch:{ all -> 0x006f }
            goto L_0x0002
        L_0x00e9:
            com.amap.mapapi.location.a$a r0 = r14.c     // Catch:{ all -> 0x006f }
            r0.sendMessage(r2)     // Catch:{ all -> 0x006f }
            android.content.Context r0 = r14.k     // Catch:{ all -> 0x006f }
            com.amap.mapapi.core.d.a(r0, r1)     // Catch:{ all -> 0x006f }
        L_0x00f3:
            long r0 = r14.q     // Catch:{ Exception -> 0x00fb }
            java.lang.Thread.sleep(r0)     // Catch:{ Exception -> 0x00fb }
        L_0x00f8:
            monitor-exit(r5)     // Catch:{ all -> 0x006f }
            goto L_0x0002
        L_0x00fb:
            r0 = move-exception
            java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x006f }
            r0.interrupt()     // Catch:{ all -> 0x006f }
            goto L_0x00f8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.mapapi.location.a.run():void");
    }
}
