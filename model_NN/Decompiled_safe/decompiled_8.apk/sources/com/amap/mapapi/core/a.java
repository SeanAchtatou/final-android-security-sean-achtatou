package com.amap.mapapi.core;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;
import com.amap.mapapi.map.MapActivity;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.util.Random;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static a f421a = null;
    private static String b = null;
    private static Context c = null;
    private static TelephonyManager d;
    private static ConnectivityManager e;
    private static String f;

    private a() {
    }

    public static a a(Context context) {
        if (f421a == null) {
            f421a = new a();
            c = context;
            d = (TelephonyManager) context.getSystemService("phone");
            e = (ConnectivityManager) c.getSystemService("connectivity");
            f = c.getApplicationContext().getPackageName();
        }
        return f421a;
    }

    public String a() {
        StringBuilder sb = new StringBuilder();
        String mapApiKey = MapActivity.getMapApiKey();
        if (mapApiKey != null && mapApiKey.length() >= 15) {
            sb.append("api_key=");
            sb.append(mapApiKey);
            sb.append("&");
        }
        String deviceId = d.getDeviceId();
        String subscriberId = d.getSubscriberId();
        sb.append("imei=" + deviceId);
        sb.append("&imsi=" + subscriberId);
        sb.append("&pkg=" + f);
        sb.append("&model=");
        sb.append(c());
        sb.append("&sys=");
        sb.append(b());
        sb.append("&nettype=");
        sb.append(e());
        String simOperatorName = d.getSimOperatorName();
        sb.append("&netprovider=");
        sb.append(simOperatorName);
        sb.append("&uid=");
        sb.append("'" + f() + "'");
        sb.append("&sys_ver=");
        sb.append(b());
        sb.append("&bid=");
        sb.append(d());
        return sb.toString();
    }

    public String b() {
        return Build.VERSION.RELEASE;
    }

    public String c() {
        return Build.MODEL;
    }

    public String d() {
        return Build.ID;
    }

    public String e() {
        NetworkInfo activeNetworkInfo;
        return (c.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != 0 || e == null || (activeNetworkInfo = e.getActiveNetworkInfo()) == null) ? PoiTypeDef.All : activeNetworkInfo.getTypeName();
    }

    public String f() {
        int nextInt = new Random().nextInt();
        b = d.getDeviceId();
        return nextInt + b;
    }
}
