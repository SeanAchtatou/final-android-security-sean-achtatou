package com.amap.mapapi.core;

import com.jcraft.jzlib.GZIPHeader;

public class c {
    public static int a(byte[] bArr, int i) {
        return ((bArr[i + 3] & GZIPHeader.OS_UNKNOWN) << 24) + ((bArr[i + 2] & GZIPHeader.OS_UNKNOWN) << 16) + ((bArr[i + 1] & GZIPHeader.OS_UNKNOWN) << 8) + ((bArr[i + 0] & GZIPHeader.OS_UNKNOWN) << 0);
    }

    public static void a(byte[] bArr, int i, byte[] bArr2, int i2, int i3) {
        byte[] bArr3 = new byte[i3];
        System.arraycopy(bArr, i, bArr3, 0, i3);
        System.arraycopy(bArr3, 0, bArr2, i2, i3);
    }

    public static short b(byte[] bArr, int i) {
        return (short) (((bArr[i + 1] & GZIPHeader.OS_UNKNOWN) << 8) + ((bArr[i + 0] & GZIPHeader.OS_UNKNOWN) << 0));
    }
}
