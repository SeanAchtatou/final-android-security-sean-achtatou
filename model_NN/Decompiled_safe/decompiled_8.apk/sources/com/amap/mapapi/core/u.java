package com.amap.mapapi.core;

import com.amap.mapapi.poisearch.PoiTypeDef;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class u {

    /* renamed from: a  reason: collision with root package name */
    private static String f432a = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
    private String b;
    private Object c;
    private List d;
    private List e;

    class a {

        /* renamed from: a  reason: collision with root package name */
        private String f433a;
        private Object b;

        public a(String str, Object obj) {
            this.f433a = str;
            this.b = obj;
        }

        public String a() {
            return this.f433a;
        }

        public void a(Object obj) {
            this.b = obj;
        }

        public Object b() {
            return this.b;
        }
    }

    public u(String str) {
        this.b = str;
    }

    private final String a(String str, String str2, int i) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i2 = 0; i2 < i; i2++) {
            stringBuffer.append(str);
        }
        StringBuffer stringBuffer2 = new StringBuffer();
        stringBuffer2.append(((Object) stringBuffer) + "<" + this.b);
        if (this.d != null) {
            for (a aVar : this.d) {
                stringBuffer2.append(" " + aVar.a() + "=\"" + aVar.b() + "\"");
            }
        }
        if (this.e != null) {
            stringBuffer2.append(">" + str2);
            int i3 = i + 1;
            for (u a2 : this.e) {
                stringBuffer2.append(a2.a(str, str2, i3));
            }
            stringBuffer2.append(((Object) stringBuffer) + "</" + this.b + ">" + str2);
        } else if (this.c == null) {
            stringBuffer2.append("/>" + str2);
        } else {
            stringBuffer2.append(">");
            stringBuffer2.append(this.c);
            stringBuffer2.append("</" + this.b + ">" + str2);
        }
        return stringBuffer2.toString();
    }

    public final String a() {
        return f432a + a(PoiTypeDef.All, PoiTypeDef.All);
    }

    /* access modifiers changed from: protected */
    public final String a(String str, String str2) {
        return a(str, str2, 0);
    }

    public final void a(u uVar) {
        if (this.e == null) {
            this.e = new ArrayList();
        }
        this.e.add(uVar);
    }

    public final void a(Object obj) {
        this.c = obj;
    }

    public final void a(String str, Object obj) {
        a aVar;
        if (this.d == null) {
            this.d = new ArrayList();
        }
        Iterator it = this.d.iterator();
        while (true) {
            if (!it.hasNext()) {
                aVar = null;
                break;
            }
            aVar = (a) it.next();
            if (str.equalsIgnoreCase(aVar.a())) {
                break;
            }
        }
        if (aVar == null) {
            this.d.add(new a(str, obj));
            return;
        }
        aVar.a(obj);
    }
}
