package com.amap.mapapi.core;

import android.os.Parcel;
import android.os.Parcelable;
import com.amap.mapapi.poisearch.PoiTypeDef;

public class PoiItem extends OverlayItem {
    public static final Parcelable.Creator CREATOR = new k();
    public static final String DesSplit = " - ";

    /* renamed from: a  reason: collision with root package name */
    private String f420a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;

    private PoiItem(Parcel parcel) {
        super(parcel);
        this.e = PoiTypeDef.All;
        this.f420a = parcel.readString();
        this.d = parcel.readString();
        this.c = parcel.readString();
        this.b = parcel.readString();
        this.e = parcel.readString();
    }

    /* synthetic */ PoiItem(Parcel parcel, k kVar) {
        this(parcel);
    }

    public PoiItem(String str, GeoPoint geoPoint, String str2, String str3) {
        super(geoPoint, str2, str3);
        this.e = PoiTypeDef.All;
        this.f420a = str;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        return obj != null && obj.getClass() == getClass() && this.f420a == ((PoiItem) obj).f420a;
    }

    public String getAdCode() {
        return this.d;
    }

    public String getPoiId() {
        return this.f420a;
    }

    public String getTel() {
        return this.c;
    }

    public String getTypeCode() {
        return this.b;
    }

    public String getTypeDes() {
        return this.e;
    }

    public String getXmlNode() {
        return this.f;
    }

    public int hashCode() {
        return this.f420a.hashCode();
    }

    public void setAdCode(String str) {
        this.d = str;
    }

    public void setTel(String str) {
        this.c = str;
    }

    public void setTypeCode(String str) {
        this.b = str;
    }

    public void setTypeDes(String str) {
        this.e = str;
    }

    public void setXmlNode(String str) {
        this.f = str;
    }

    public String toString() {
        return this.mTitle;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.f420a);
        parcel.writeString(this.d);
        parcel.writeString(this.c);
        parcel.writeString(this.b);
        parcel.writeString(this.e);
    }
}
