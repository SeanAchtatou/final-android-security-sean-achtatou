package com.amap.mapapi.core;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class s implements List {

    /* renamed from: a  reason: collision with root package name */
    protected LinkedList f431a = new LinkedList();

    public synchronized void add(int i, Object obj) {
        this.f431a.add(i, obj);
    }

    public synchronized boolean add(Object obj) {
        return this.f431a.add(obj);
    }

    public synchronized boolean addAll(int i, Collection collection) {
        return this.f431a.addAll(i, collection);
    }

    public synchronized boolean addAll(Collection collection) {
        return this.f431a.addAll(collection);
    }

    public synchronized void c(Object obj) {
        add(obj);
    }

    public synchronized void clear() {
        this.f431a.clear();
    }

    public synchronized boolean contains(Object obj) {
        return this.f431a.contains(obj);
    }

    public synchronized boolean containsAll(Collection collection) {
        return this.f431a.containsAll(collection);
    }

    public synchronized Object get(int i) {
        Object obj;
        obj = null;
        try {
            obj = this.f431a.get(i);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }

    public synchronized int indexOf(Object obj) {
        return this.f431a.indexOf(obj);
    }

    public synchronized boolean isEmpty() {
        return this.f431a.isEmpty();
    }

    public synchronized Iterator iterator() {
        return this.f431a.listIterator();
    }

    public synchronized int lastIndexOf(Object obj) {
        return this.f431a.lastIndexOf(obj);
    }

    public synchronized ListIterator listIterator() {
        return this.f431a.listIterator();
    }

    public synchronized ListIterator listIterator(int i) {
        return this.f431a.listIterator(i);
    }

    public synchronized Object remove(int i) {
        return this.f431a.remove(i);
    }

    public synchronized boolean remove(Object obj) {
        return this.f431a.remove(obj);
    }

    public synchronized boolean removeAll(Collection collection) {
        return this.f431a.removeAll(collection);
    }

    public synchronized boolean retainAll(Collection collection) {
        return this.f431a.retainAll(collection);
    }

    public synchronized Object set(int i, Object obj) {
        return this.f431a.set(i, obj);
    }

    public synchronized int size() {
        return this.f431a.size();
    }

    public synchronized List subList(int i, int i2) {
        return this.f431a.subList(i, i2);
    }

    public synchronized Object[] toArray() {
        return this.f431a.toArray();
    }

    public synchronized Object[] toArray(Object[] objArr) {
        return this.f431a.toArray(objArr);
    }
}
