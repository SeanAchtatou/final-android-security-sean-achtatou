package com.amap.mapapi.core;

import java.io.IOException;
import java.io.InputStream;
import java.net.Proxy;
import java.util.ArrayList;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public abstract class v extends l {
    private ArrayList i = new ArrayList();

    public v(Object obj, Proxy proxy, String str, String str2) {
        super(obj, proxy, str, str2);
    }

    /* access modifiers changed from: protected */
    public String a(Node node) {
        Node firstChild;
        if (node == null || (firstChild = node.getFirstChild()) == null || firstChild.getNodeType() != 3) {
            return null;
        }
        String nodeValue = firstChild.getNodeValue();
        int indexOf = nodeValue.indexOf("ppppppppShitJava");
        return indexOf >= 0 ? (String) this.i.get(Integer.parseInt(nodeValue.substring(indexOf + 16))) : nodeValue;
    }

    /* access modifiers changed from: protected */
    public abstract Object b(NodeList nodeList);

    /* access modifiers changed from: protected */
    public NodeList b(InputStream inputStream) {
        return d.b(d(inputStream)).getDocumentElement().getChildNodes();
    }

    /* access modifiers changed from: protected */
    public Object c(InputStream inputStream) {
        Object b = b(b(inputStream));
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e) {
                throw new AMapException(AMapException.ERROR_IO);
            }
        }
        return b;
    }
}
