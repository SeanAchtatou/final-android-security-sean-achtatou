package com.amap.mapapi.core;

import java.io.IOException;
import java.io.InputStream;
import java.net.Proxy;
import java.util.ArrayList;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public abstract class t extends v {
    public t(Object obj, Proxy proxy, String str, String str2) {
        super(obj, proxy, str, str2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ArrayList c(InputStream inputStream) {
        NodeList b = b(inputStream);
        ArrayList arrayList = new ArrayList();
        int length = b.getLength();
        for (int i = 0; i < length; i++) {
            a(b.item(i), arrayList);
        }
        a(arrayList);
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e) {
                throw new AMapException(AMapException.ERROR_IO);
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ArrayList b(NodeList nodeList) {
        return null;
    }

    /* access modifiers changed from: protected */
    public abstract void a(ArrayList arrayList);

    /* access modifiers changed from: protected */
    public abstract void a(Node node, ArrayList arrayList);
}
