package com.amap.mapapi.core;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.util.DisplayMetrics;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.io.InputStream;
import java.lang.reflect.Field;

public class m {

    /* renamed from: a  reason: collision with root package name */
    private Context f427a = null;
    private String[] b = {"nomap.png", "beta.png", "poi_1.png", "compass_bg.png", "compass_pointer.png", "loc1.png", "loc2.png", "zoom_in_true_HVGA.9.png", "zoom_out_true_HVGA.9.png", "zoom_in_disabled_HVGA.9.png", "zoom_out_disabled_HVGA.9.png", "zoom_in_selected_HVGA.9.png", "zoom_out_selected_HVGA.9.png"};
    private String[] c = {"nomap.png", "beta.png", "poi_1_WVGA.png", "compass_bg.png", "compass_pointer.png", "loc1.png", "loc2.png", "zoom_in_true_WVGA.9.png", "zoom_out_true_WVGA.9.png", "zoom_in_disabled_WVGA.9.png", "zoom_out_disabled_WVGA.9.png", "zoom_in_selected_WVGA.9.png", "zoom_out_selected_WVGA.9.png"};
    private String[] d = {"nomap.png", "beta.png", "poi_1_QVGA.png", "compass_bg__QVGA.png", "compass_pointer_QVGA.png", "loc1_QVGA.png", "loc2_QVGA.png", "zoom_in_true_QVGA.9.png", "zoom_out_true_QVGA.9.png", "zoom_in_disabled_QVGA.9.png", "zoom_out_disabled_QVGA.9.png", "zoom_in_selected_QVGA.9.png", "zoom_out_selected_QVGA.9.png"};
    private Bitmap[] e = null;

    public m(Context context) {
        this.f427a = context;
    }

    public final Bitmap a(int i) {
        if (this.e == null) {
            this.e = new Bitmap[this.b.length];
        }
        if (this.e[i] != null && !this.e[i].isRecycled()) {
            return this.e[i];
        }
        String str = PoiTypeDef.All;
        if (b.e == 2) {
            str = this.c[i];
        } else if (b.e == 1) {
            str = this.d[i];
        } else if (b.e == 3) {
            str = this.b[i];
        }
        Bitmap a2 = a(this.f427a, str);
        if (a2 != null) {
            this.e[i] = a2;
        }
        return this.e[i];
    }

    public final Bitmap a(Context context, String str) {
        Exception e2;
        Bitmap bitmap;
        try {
            InputStream open = context.getAssets().open(str);
            bitmap = BitmapFactory.decodeStream(open);
            try {
                open.close();
            } catch (Exception e3) {
                e2 = e3;
                e2.printStackTrace();
                return bitmap;
            }
        } catch (Exception e4) {
            Exception exc = e4;
            bitmap = null;
            e2 = exc;
        }
        return bitmap;
    }

    public final NinePatchDrawable a(Context context, String str, byte[] bArr, Rect rect) {
        return new NinePatchDrawable(a(context, str), bArr, rect, null);
    }

    public void a() {
        if (this.e != null) {
            int length = this.e.length;
            for (int i = 0; i < length; i++) {
                if (this.e[i] != null) {
                    this.e[i].recycle();
                    this.e[i] = null;
                }
            }
            this.e = null;
        }
    }

    public final Drawable b(Context context, String str) {
        BitmapDrawable bitmapDrawable = new BitmapDrawable(a(context, str));
        bitmapDrawable.setBounds(0, 0, bitmapDrawable.getIntrinsicWidth(), bitmapDrawable.getIntrinsicHeight());
        return bitmapDrawable;
    }

    public void b() {
        int i;
        new DisplayMetrics();
        DisplayMetrics displayMetrics = this.f427a.getApplicationContext().getResources().getDisplayMetrics();
        Field field = null;
        try {
            field = displayMetrics.getClass().getField("densityDpi");
        } catch (NoSuchFieldException | SecurityException e2) {
        }
        if (field != null) {
            long j = (long) (displayMetrics.widthPixels * displayMetrics.heightPixels);
            try {
                i = field.getInt(displayMetrics);
            } catch (IllegalArgumentException e3) {
                e3.printStackTrace();
                i = 160;
            } catch (IllegalAccessException e4) {
                e4.printStackTrace();
                i = 160;
            }
            if (i <= 120) {
                b.e = 1;
                return;
            }
            if (i > 160) {
                if (i <= 240) {
                    b.e = 2;
                    return;
                } else if (j > 153600) {
                    b.e = 2;
                    return;
                } else if (j < 153600) {
                    b.e = 1;
                    return;
                }
            }
            b.e = 3;
            return;
        }
        long j2 = (long) (displayMetrics.widthPixels * displayMetrics.heightPixels);
        if (j2 > 153600) {
            b.e = 2;
        } else if (j2 < 153600) {
            b.e = 1;
        } else {
            b.e = 3;
        }
    }
}
