package com.amap.mapapi.core;

import com.amap.mapapi.poisearch.PoiTypeDef;
import java.util.Random;

public class i {
    private static i g;

    /* renamed from: a  reason: collision with root package name */
    private String f425a = "http://webrd01.is.autonavi.com";
    private String b = "http://tm.mapabc.com";
    private String c = "http://api.amap.com:9090/sisserver";
    private String d = "http://ds.mapabc.com:8888";
    private String e = "http://mst01.is.autonavi.com";
    private String f = "http://tmds.mapabc.com";

    private i() {
    }

    public static synchronized i a() {
        i iVar;
        synchronized (i.class) {
            if (g == null) {
                g = new i();
            }
            iVar = g;
        }
        return iVar;
    }

    public void a(String str) {
        this.e = str;
    }

    public String b() {
        String str = PoiTypeDef.All;
        switch (new Random(System.currentTimeMillis()).nextInt(100000) % 4) {
            case 0:
                if (b.e != 2) {
                    str = "http://webrd01.is.autonavi.com";
                    break;
                } else {
                    str = "http://wprd01.is.autonavi.com";
                    break;
                }
            case 1:
                if (b.e != 2) {
                    str = "http://webrd02.is.autonavi.com";
                    break;
                } else {
                    str = "http://wprd02.is.autonavi.com";
                    break;
                }
            case 2:
                if (b.e != 2) {
                    str = "http://webrd03.is.autonavi.com";
                    break;
                } else {
                    str = "http://wprd03.is.autonavi.com";
                    break;
                }
            case 3:
                if (b.e != 2) {
                    str = "http://webrd04.is.autonavi.com";
                    break;
                } else {
                    str = "http://wprd04.is.autonavi.com";
                    break;
                }
        }
        this.f425a = str;
        return this.f425a;
    }

    public void b(String str) {
        this.f = str;
    }

    public String c() {
        return this.b;
    }

    public void c(String str) {
        this.f425a = str;
    }

    public String d() {
        return this.c;
    }

    public void d(String str) {
        this.b = str;
    }

    public String e() {
        String str = PoiTypeDef.All;
        switch (new Random(System.currentTimeMillis()).nextInt(100000) % 4) {
            case 0:
                str = "http://mst01.is.autonavi.com";
                break;
            case 1:
                str = "http://mst02.is.autonavi.com";
                break;
            case 2:
                str = "http://mst03.is.autonavi.com";
                break;
            case 3:
                str = "http://mst04.is.autonavi.com";
                break;
        }
        this.e = str;
        return this.e;
    }

    public void e(String str) {
        this.c = str;
    }

    public String f() {
        return this.d;
    }

    public void f(String str) {
        this.d = str;
    }
}
