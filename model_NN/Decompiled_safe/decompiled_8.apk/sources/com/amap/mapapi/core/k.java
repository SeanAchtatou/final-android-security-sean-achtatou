package com.amap.mapapi.core;

import android.os.Parcel;
import android.os.Parcelable;

final class k implements Parcelable.Creator {
    k() {
    }

    /* renamed from: a */
    public final PoiItem createFromParcel(Parcel parcel) {
        return new PoiItem(parcel, null);
    }

    /* renamed from: a */
    public final PoiItem[] newArray(int i) {
        return new PoiItem[i];
    }
}
