package com.amap.mapapi.core;

import android.os.Parcel;
import android.os.Parcelable;

final class e implements Parcelable.Creator {
    e() {
    }

    /* renamed from: a */
    public final GeoPoint createFromParcel(Parcel parcel) {
        return new GeoPoint(parcel, (e) null);
    }

    /* renamed from: a */
    public final GeoPoint[] newArray(int i) {
        return new GeoPoint[i];
    }
}
