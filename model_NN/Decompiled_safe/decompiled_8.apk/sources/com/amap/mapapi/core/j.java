package com.amap.mapapi.core;

import android.os.Parcel;
import android.os.Parcelable;

final class j implements Parcelable.Creator {
    j() {
    }

    /* renamed from: a */
    public final OverlayItem createFromParcel(Parcel parcel) {
        return new OverlayItem(parcel);
    }

    /* renamed from: a */
    public final OverlayItem[] newArray(int i) {
        return new OverlayItem[i];
    }
}
