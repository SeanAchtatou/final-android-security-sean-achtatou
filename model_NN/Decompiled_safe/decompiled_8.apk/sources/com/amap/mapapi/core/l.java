package com.amap.mapapi.core;

import com.amap.mapapi.poisearch.PoiTypeDef;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.UnknownHostException;
import java.net.UnknownServiceException;
import java.util.zip.GZIPInputStream;

public abstract class l {

    /* renamed from: a  reason: collision with root package name */
    protected Proxy f426a;
    protected Object b;
    protected int c = 1;
    protected int d = 20;
    protected int e = 0;
    protected int f = 0;
    protected String g;
    protected String h = PoiTypeDef.All;

    public l(Object obj, Proxy proxy, String str, String str2) {
        this.f426a = proxy;
        this.b = obj;
        this.c = 1;
        this.d = 5;
        this.e = 2;
        this.g = str2;
    }

    private Object a(InputStream inputStream) {
        return c(inputStream);
    }

    private String a() {
        String[] f2 = f();
        if (f2 == null) {
            return PoiTypeDef.All;
        }
        StringBuilder sb = new StringBuilder();
        if (f2 != null) {
            for (String append : f2) {
                sb.append(append);
            }
        }
        return sb.toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:42:0x0093 A[SYNTHETIC, Splitter:B:42:0x0093] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0098  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.Object b() {
        /*
            r9 = this;
            r4 = 0
            r0 = 0
            r1 = r4
            r2 = r4
            r3 = r4
        L_0x0005:
            int r5 = r9.c
            if (r0 >= r5) goto L_0x00bb
            boolean r5 = r9.e()     // Catch:{ AMapException -> 0x0065, all -> 0x00bc }
            if (r5 != 0) goto L_0x0038
            java.lang.String r5 = r9.h()     // Catch:{ AMapException -> 0x0065, all -> 0x00bc }
            r9.h = r5     // Catch:{ AMapException -> 0x0065, all -> 0x00bc }
            byte[] r5 = r9.i()     // Catch:{ AMapException -> 0x0065, all -> 0x00bc }
            java.lang.String r6 = r9.h     // Catch:{ AMapException -> 0x0065, all -> 0x00bc }
            java.net.Proxy r7 = r9.f426a     // Catch:{ AMapException -> 0x0065, all -> 0x00bc }
            java.net.HttpURLConnection r3 = com.amap.mapapi.core.f.a(r6, r5, r7)     // Catch:{ AMapException -> 0x0065, all -> 0x00bc }
        L_0x0021:
            java.io.InputStream r5 = r9.a(r3)     // Catch:{ AMapException -> 0x0065, all -> 0x00bc }
            java.lang.Object r1 = r9.a(r5)     // Catch:{ AMapException -> 0x00be }
            int r0 = r9.c     // Catch:{ AMapException -> 0x00be }
            if (r5 == 0) goto L_0x00c2
            r5.close()     // Catch:{ IOException -> 0x005c }
            r2 = r4
        L_0x0031:
            if (r3 == 0) goto L_0x0005
            r3.disconnect()
            r3 = r4
            goto L_0x0005
        L_0x0038:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ AMapException -> 0x0065, all -> 0x00bc }
            r5.<init>()     // Catch:{ AMapException -> 0x0065, all -> 0x00bc }
            java.lang.String r6 = r9.h()     // Catch:{ AMapException -> 0x0065, all -> 0x00bc }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ AMapException -> 0x0065, all -> 0x00bc }
            java.lang.String r6 = r9.a()     // Catch:{ AMapException -> 0x0065, all -> 0x00bc }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ AMapException -> 0x0065, all -> 0x00bc }
            java.lang.String r5 = r5.toString()     // Catch:{ AMapException -> 0x0065, all -> 0x00bc }
            r9.h = r5     // Catch:{ AMapException -> 0x0065, all -> 0x00bc }
            java.lang.String r5 = r9.h     // Catch:{ AMapException -> 0x0065, all -> 0x00bc }
            java.net.Proxy r6 = r9.f426a     // Catch:{ AMapException -> 0x0065, all -> 0x00bc }
            java.net.HttpURLConnection r3 = com.amap.mapapi.core.f.a(r5, r6)     // Catch:{ AMapException -> 0x0065, all -> 0x00bc }
            goto L_0x0021
        L_0x005c:
            r0 = move-exception
            com.amap.mapapi.core.AMapException r0 = new com.amap.mapapi.core.AMapException
            java.lang.String r1 = "IO 操作异常 - IOException"
            r0.<init>(r1)
            throw r0
        L_0x0065:
            r5 = move-exception
            r8 = r5
            r5 = r2
            r2 = r8
        L_0x0069:
            int r0 = r0 + 1
            int r6 = r9.c     // Catch:{ all -> 0x008f }
            if (r0 >= r6) goto L_0x009c
            int r2 = r9.e     // Catch:{ InterruptedException -> 0x0084 }
            int r2 = r2 * 1000
            long r6 = (long) r2     // Catch:{ InterruptedException -> 0x0084 }
            java.lang.Thread.sleep(r6)     // Catch:{ InterruptedException -> 0x0084 }
            if (r5 == 0) goto L_0x00c0
            r5.close()     // Catch:{ IOException -> 0x00a9 }
            r2 = r4
        L_0x007d:
            if (r3 == 0) goto L_0x0005
            r3.disconnect()
            r3 = r4
            goto L_0x0005
        L_0x0084:
            r0 = move-exception
            com.amap.mapapi.core.AMapException r1 = new com.amap.mapapi.core.AMapException     // Catch:{ all -> 0x008f }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x008f }
            r1.<init>(r0)     // Catch:{ all -> 0x008f }
            throw r1     // Catch:{ all -> 0x008f }
        L_0x008f:
            r0 = move-exception
            r2 = r5
        L_0x0091:
            if (r2 == 0) goto L_0x0096
            r2.close()     // Catch:{ IOException -> 0x00b2 }
        L_0x0096:
            if (r3 == 0) goto L_0x009b
            r3.disconnect()
        L_0x009b:
            throw r0
        L_0x009c:
            r9.k()     // Catch:{ all -> 0x008f }
            com.amap.mapapi.core.AMapException r0 = new com.amap.mapapi.core.AMapException     // Catch:{ all -> 0x008f }
            java.lang.String r1 = r2.getErrorMessage()     // Catch:{ all -> 0x008f }
            r0.<init>(r1)     // Catch:{ all -> 0x008f }
            throw r0     // Catch:{ all -> 0x008f }
        L_0x00a9:
            r0 = move-exception
            com.amap.mapapi.core.AMapException r0 = new com.amap.mapapi.core.AMapException
            java.lang.String r1 = "IO 操作异常 - IOException"
            r0.<init>(r1)
            throw r0
        L_0x00b2:
            r0 = move-exception
            com.amap.mapapi.core.AMapException r0 = new com.amap.mapapi.core.AMapException
            java.lang.String r1 = "IO 操作异常 - IOException"
            r0.<init>(r1)
            throw r0
        L_0x00bb:
            return r1
        L_0x00bc:
            r0 = move-exception
            goto L_0x0091
        L_0x00be:
            r2 = move-exception
            goto L_0x0069
        L_0x00c0:
            r2 = r5
            goto L_0x007d
        L_0x00c2:
            r2 = r5
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.mapapi.core.l.b():java.lang.Object");
    }

    /* access modifiers changed from: protected */
    public InputStream a(HttpURLConnection httpURLConnection) {
        try {
            PushbackInputStream pushbackInputStream = new PushbackInputStream(httpURLConnection.getInputStream(), 2);
            byte[] bArr = new byte[2];
            pushbackInputStream.read(bArr);
            pushbackInputStream.unread(bArr);
            return (bArr[0] == 31 && bArr[1] == -117) ? new GZIPInputStream(pushbackInputStream) : pushbackInputStream;
        } catch (ProtocolException e2) {
            throw new AMapException(AMapException.ERROR_PROTOCOL);
        } catch (UnknownHostException e3) {
            throw new AMapException(AMapException.ERROR_UNKNOW_HOST);
        } catch (UnknownServiceException e4) {
            throw new AMapException(AMapException.ERROR_UNKNOW_SERVICE);
        } catch (IOException e5) {
            throw new AMapException(AMapException.ERROR_IO);
        }
    }

    /* access modifiers changed from: protected */
    public abstract Object c(InputStream inputStream);

    /* access modifiers changed from: protected */
    public String d(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            byte[] bArr = new byte[1024];
            while (true) {
                int read = inputStream.read(bArr);
                if (read != -1) {
                    byteArrayOutputStream.write(bArr, 0, read);
                    byteArrayOutputStream.flush();
                } else {
                    try {
                        break;
                    } catch (IOException e2) {
                        throw new AMapException(AMapException.ERROR_IO);
                    }
                }
            }
            byteArrayOutputStream.close();
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e3) {
                    throw new AMapException(AMapException.ERROR_IO);
                }
            }
            try {
                return new String(byteArrayOutputStream.toByteArray(), "UTF-8");
            } catch (UnsupportedEncodingException e4) {
                e4.printStackTrace();
                return new String();
            }
        } catch (IOException e5) {
            throw new AMapException(AMapException.ERROR_IO);
        } catch (Throwable th) {
            try {
                byteArrayOutputStream.close();
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e6) {
                        throw new AMapException(AMapException.ERROR_IO);
                    }
                }
                throw th;
            } catch (IOException e7) {
                throw new AMapException(AMapException.ERROR_IO);
            }
        }
    }

    /* access modifiers changed from: protected */
    public abstract boolean e();

    /* access modifiers changed from: protected */
    public abstract String[] f();

    /* access modifiers changed from: protected */
    public abstract byte[] g();

    /* access modifiers changed from: protected */
    public abstract String h();

    /* access modifiers changed from: protected */
    public byte[] i() {
        return g();
    }

    public Object j() {
        if (this.b != null) {
            return b();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public Object k() {
        return null;
    }
}
