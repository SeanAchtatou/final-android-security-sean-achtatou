package com.amap.mapapi.core;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import com.jcraft.jzlib.GZIPHeader;
import mm.purchasesdk.PurchaseCode;

public class q {

    /* renamed from: a  reason: collision with root package name */
    public static Drawable f429a;
    public static Drawable b;
    public static Drawable c;
    public static Drawable d;
    public static Drawable e;
    public static Drawable f;
    public static Drawable g;
    public static Drawable h;
    public static Drawable i;
    public static Drawable j;
    public static Paint k;
    public static Paint l;
    public static Paint m;
    public static boolean n = false;

    public static void a() {
        Bitmap bitmap;
        Bitmap bitmap2;
        Bitmap bitmap3;
        Bitmap bitmap4;
        Bitmap bitmap5;
        Bitmap bitmap6;
        Bitmap bitmap7;
        Bitmap bitmap8;
        n = false;
        if (!(f429a == null || (bitmap8 = ((BitmapDrawable) f429a).getBitmap()) == null || bitmap8.isRecycled())) {
            bitmap8.recycle();
            f429a = null;
        }
        if (!(b == null || (bitmap7 = ((BitmapDrawable) b).getBitmap()) == null || bitmap7.isRecycled())) {
            bitmap7.recycle();
            b = null;
        }
        if (!(c == null || (bitmap6 = ((BitmapDrawable) c).getBitmap()) == null || bitmap6.isRecycled())) {
            bitmap6.recycle();
            c = null;
        }
        if (!(d == null || (bitmap5 = ((BitmapDrawable) d).getBitmap()) == null || bitmap5.isRecycled())) {
            bitmap5.recycle();
            d = null;
        }
        if (!(e == null || (bitmap4 = ((BitmapDrawable) e).getBitmap()) == null || bitmap4.isRecycled())) {
            bitmap4.recycle();
            e = null;
        }
        if (!(f == null || (bitmap3 = ((BitmapDrawable) f).getBitmap()) == null || bitmap3.isRecycled())) {
            bitmap3.recycle();
            f = null;
        }
        if (!(g == null || (bitmap2 = ((BitmapDrawable) g).getBitmap()) == null || bitmap2.isRecycled())) {
            bitmap2.recycle();
            g = null;
        }
        if (h != null) {
            h = null;
        }
        if (i != null) {
            i = null;
        }
        if (j != null && (bitmap = ((BitmapDrawable) j).getBitmap()) != null && !bitmap.isRecycled()) {
            bitmap.recycle();
            j = null;
        }
    }

    public static void a(Context context) {
        if (!n) {
            Paint paint = new Paint();
            l = paint;
            paint.setStyle(Paint.Style.STROKE);
            l.setColor(Color.rgb(54, (int) PurchaseCode.NOGSM_ERR, 227));
            l.setAlpha(180);
            l.setStrokeWidth(5.5f);
            l.setStrokeJoin(Paint.Join.ROUND);
            l.setStrokeCap(Paint.Cap.ROUND);
            l.setAntiAlias(true);
            Paint paint2 = new Paint();
            k = paint2;
            paint2.setStyle(Paint.Style.STROKE);
            k.setColor(Color.rgb(54, (int) PurchaseCode.NOGSM_ERR, 227));
            k.setAlpha(150);
            k.setStrokeWidth(5.5f);
            k.setStrokeJoin(Paint.Join.ROUND);
            k.setStrokeCap(Paint.Cap.ROUND);
            k.setAntiAlias(true);
            Paint paint3 = new Paint();
            m = paint3;
            paint3.setStyle(Paint.Style.STROKE);
            m.setColor(Color.rgb(54, (int) PurchaseCode.NOGSM_ERR, 227));
            m.setAlpha(180);
            m.setStrokeWidth(5.5f);
            m.setStrokeJoin(Paint.Join.ROUND);
            m.setStrokeCap(Paint.Cap.ROUND);
            m.setAntiAlias(true);
            new DisplayMetrics();
            DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            long j2 = (long) (displayMetrics.heightPixels * displayMetrics.widthPixels);
            if (j2 > 153600) {
                a(context, 1);
            } else if (j2 < 153600) {
                a(context, 3);
            } else {
                a(context, 2);
            }
            Rect rect = new Rect(8, 4, 16, 14);
            Rect rect2 = new Rect(17, 5, 8, 12);
            byte[] bArr = {1, 2, 2, 9, 0, 0, 0, 0, 0, 0, 0, 0, 17, 0, 0, 0, 8, 0, 0, 0, 5, 0, 0, 0, GZIPHeader.OS_QDOS, 0, 0, 0, 0, 0, 0, 0, 14, 0, 0, 0, 26, 0, 0, 0, 5, 0, 0, 0, 19, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0};
            h = b.g.a(context, "left_back.png", new byte[]{1, 2, 2, 9, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 16, 0, 0, 0, 4, 0, 0, 0, 14, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 18, 0, 0, 0, 4, 0, 0, 0, 17, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0}, rect);
            i = b.g.a(context, "right_back.png", bArr, rect2);
            n = true;
        }
    }

    static void a(Context context, int i2) {
        switch (i2) {
            case 1:
                try {
                    f429a = b.g.b(context, "start_w.png");
                    b = b.g.b(context, "end_w.png");
                    c = b.g.b(context, "foot_w.png");
                    e = b.g.b(context, "bus_w.png");
                    d = b.g.b(context, "car_w.png");
                    f = b.g.b(context, "starticon_w.png");
                    g = b.g.b(context, "endicon_w.png");
                    j = b.g.b(context, "route_coner_w.png");
                    return;
                } catch (Exception e2) {
                    return;
                }
            case 2:
                try {
                    f429a = b.g.b(context, "start.png");
                    b = b.g.b(context, "end.png");
                    c = b.g.b(context, "foot.png");
                    e = b.g.b(context, "bus.png");
                    d = b.g.b(context, "car.png");
                    f = b.g.b(context, "starticon.png");
                    g = b.g.b(context, "endicon.png");
                    j = b.g.b(context, "route_coner.png");
                    return;
                } catch (Exception e3) {
                    return;
                }
            case 3:
                try {
                    f429a = b.g.b(context, "start.png");
                    b = b.g.b(context, "end.png");
                    c = b.g.b(context, "foot.png");
                    e = b.g.b(context, "bus.png");
                    d = b.g.b(context, "car.png");
                    f = b.g.b(context, "starticon.png");
                    g = b.g.b(context, "endicon.png");
                    j = b.g.b(context, "route_coner_q.png");
                    return;
                } catch (Exception e4) {
                    return;
                }
            default:
                try {
                    f429a = b.g.b(context, "start.png");
                    b = b.g.b(context, "end.png");
                    c = b.g.b(context, "foot.png");
                    e = b.g.b(context, "bus_w.png");
                    d = b.g.b(context, "car.png");
                    f = b.g.b(context, "starticon.png");
                    g = b.g.b(context, "endicon.png");
                    j = b.g.b(context, "route_coner.png");
                    return;
                } catch (Exception e5) {
                    return;
                }
        }
    }

    public static Drawable b(Context context) {
        Rect rect = new Rect(8, 4, 16, 14);
        return b.g.a(context, "left_back.png", new byte[]{1, 2, 2, 9, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 16, 0, 0, 0, 4, 0, 0, 0, 14, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 18, 0, 0, 0, 4, 0, 0, 0, 17, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0}, rect);
    }
}
