package com.amap.mapapi.map;

import android.graphics.Bitmap;
import com.amap.mapapi.core.AMapException;
import com.amap.mapapi.core.l;
import com.amap.mapapi.core.s;
import com.amap.mapapi.map.au;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.Iterator;

class ay extends l {
    private w i = null;

    public ay(ArrayList arrayList, Proxy proxy, String str, String str2) {
        super(arrayList, proxy, str, str2);
    }

    private void a(au.a aVar, int i2) {
        if (aVar != null && i2 >= 0 && this.i != null && this.i.o != null) {
            s sVar = this.i.o;
            int size = sVar.size();
            int i3 = 0;
            while (i3 < size) {
                au.a aVar2 = (au.a) sVar.get(i3);
                if (aVar2 == null || !aVar2.equals(aVar)) {
                    i3++;
                } else {
                    aVar2.g = i2;
                    return;
                }
            }
        }
    }

    private byte[] a(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public int a(InputStream inputStream, au.a aVar) {
        if (aVar == null || inputStream == null) {
            return -1;
        }
        if (this.i == null || this.i.m == null) {
            return -1;
        }
        int a2 = this.i.m.a(null, inputStream, false, null, aVar.b + "-" + aVar.c + "-" + aVar.d);
        if (a2 < 0) {
            return -1;
        }
        a(aVar, a2);
        if (this.i == null || !this.i.g) {
            return a2;
        }
        byte[] a3 = a(this.i.m.a(a2));
        if (this.i == null || this.i.n == null) {
            return a2;
        }
        this.i.n.a(a3, aVar.b, aVar.c, aVar.d);
        return a2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ArrayList k() {
        ArrayList arrayList = new ArrayList();
        Iterator it = ((ArrayList) this.b).iterator();
        while (it.hasNext()) {
            arrayList.add(new au.a((au.a) it.next()));
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ArrayList c(InputStream inputStream) {
        ArrayList arrayList = null;
        if (this.b != null) {
            int size = ((ArrayList) this.b).size();
            for (int i2 = 0; i2 < size; i2++) {
                au.a aVar = (au.a) ((ArrayList) this.b).get(i2);
                if (a(inputStream, aVar) < 0) {
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.add(new au.a(aVar));
                }
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new AMapException(AMapException.ERROR_IO);
                }
            }
        }
        return arrayList;
    }

    public void a(w wVar) {
        this.i = wVar;
    }

    /* access modifiers changed from: protected */
    public boolean e() {
        return true;
    }

    /* access modifiers changed from: protected */
    public String[] f() {
        return null;
    }

    /* access modifiers changed from: protected */
    public byte[] g() {
        return null;
    }

    /* access modifiers changed from: protected */
    public String h() {
        return this.i.j.a(((au.a) ((ArrayList) this.b).get(0)).b, ((au.a) ((ArrayList) this.b).get(0)).c, ((au.a) ((ArrayList) this.b).get(0)).d);
    }
}
