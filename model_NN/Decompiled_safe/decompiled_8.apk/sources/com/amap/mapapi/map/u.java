package com.amap.mapapi.map;

import com.amap.mapapi.core.s;
import com.amap.mapapi.map.au;

class u extends s {
    u() {
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(au.a aVar) {
        remove(aVar);
    }

    /* access modifiers changed from: package-private */
    public synchronized boolean b(au.a aVar) {
        boolean z = true;
        synchronized (this) {
            if (contains(aVar)) {
                z = false;
            } else {
                c(aVar);
            }
        }
        return z;
    }
}
