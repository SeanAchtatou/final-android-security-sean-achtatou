package com.amap.mapapi.map;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

class m extends ae {

    /* renamed from: a  reason: collision with root package name */
    private SensorManager f508a;
    private Sensor b;
    private SensorEventListener c = null;

    public m(ah ahVar, Context context) {
        super(ahVar, context);
        this.f508a = (SensorManager) context.getSystemService("sensor");
        this.b = this.f508a.getDefaultSensor(3);
    }

    private void g() {
        if (this.c != null) {
            try {
                this.f508a.unregisterListener(this.c);
            } catch (Exception e) {
            }
        }
    }

    private boolean i() {
        if (this.c == null) {
            return false;
        }
        try {
            return this.f508a.registerListener(this.c, this.b, 1);
        } catch (Exception e) {
            return false;
        }
    }

    public boolean a(SensorEventListener sensorEventListener) {
        g();
        this.c = sensorEventListener;
        return i();
    }

    public void a_() {
        i();
    }

    public void c() {
        g();
    }

    public void e() {
        g();
        this.c = null;
    }
}
