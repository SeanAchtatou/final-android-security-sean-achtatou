package com.amap.mapapi.map;

import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.view.View;
import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.map.MapView;

public class ap extends t implements MapView.b {
    private RouteOverlay i;
    private int j;

    public ap(MapView mapView, View view, GeoPoint geoPoint, Drawable drawable, MapView.LayoutParams layoutParams, RouteOverlay routeOverlay, int i2) {
        super(mapView, view, geoPoint, drawable, layoutParams);
        this.i = routeOverlay;
        this.j = i2;
    }

    public ap(MapView mapView, View view, GeoPoint geoPoint, RouteOverlay routeOverlay, int i2) {
        this(mapView, view, geoPoint, null, null, routeOverlay, i2);
    }

    public /* bridge */ /* synthetic */ void a() {
        super.a();
    }

    public void a(int i2) {
        this.i.b.onRouteEvent(this.d, this.i, this.j, i2);
    }

    public void a(boolean z) {
        boolean z2 = true;
        super.b();
        this.d.mRouteCtrl.a(z, this);
        this.d.mRouteCtrl.b(this.j < this.i.getRoute().getStepCount());
        this.d.mRouteCtrl.a(this.j != 0);
        this.d.mRouteCtrl.c(this.d.getMaxZoomLevel() != this.d.getZoomLevel());
        MapView.e eVar = this.d.mRouteCtrl;
        if (this.d.getMinZoomLevel() == this.d.getZoomLevel()) {
            z2 = false;
        }
        eVar.d(z2);
    }

    public /* bridge */ /* synthetic */ void b() {
        super.b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amap.mapapi.map.MapView.e.a(boolean, com.amap.mapapi.map.MapView$b):void
     arg types: [int, com.amap.mapapi.map.ap]
     candidates:
      com.amap.mapapi.map.MapView.e.a(int, boolean):void
      com.amap.mapapi.map.MapView.e.a(int, int):void
      com.amap.mapapi.map.MapView.e.a(boolean, com.amap.mapapi.map.MapView$b):void */
    public void c() {
        super.c();
        this.d.mRouteCtrl.a(false, (MapView.b) this);
    }

    public /* bridge */ /* synthetic */ boolean onDown(MotionEvent motionEvent) {
        return super.onDown(motionEvent);
    }

    public /* bridge */ /* synthetic */ boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        return super.onFling(motionEvent, motionEvent2, f, f2);
    }

    public /* bridge */ /* synthetic */ void onLongPress(MotionEvent motionEvent) {
        super.onLongPress(motionEvent);
    }

    public /* bridge */ /* synthetic */ boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        return super.onScroll(motionEvent, motionEvent2, f, f2);
    }

    public /* bridge */ /* synthetic */ void onShowPress(MotionEvent motionEvent) {
        super.onShowPress(motionEvent);
    }

    public /* bridge */ /* synthetic */ boolean onSingleTapUp(MotionEvent motionEvent) {
        return super.onSingleTapUp(motionEvent);
    }
}
