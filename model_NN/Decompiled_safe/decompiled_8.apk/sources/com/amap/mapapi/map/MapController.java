package com.amap.mapapi.map;

import android.graphics.Point;
import android.graphics.PointF;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.core.d;
import com.amap.mapapi.map.ZoomButtonsController;
import com.mapabc.minimap.map.vmap.VMapProjection;
import com.sina.weibo.sdk.constant.Constants;
import java.util.LinkedList;
import java.util.List;
import mm.purchasesdk.PurchaseCode;

public final class MapController implements View.OnKeyListener {

    /* renamed from: a  reason: collision with root package name */
    private int f449a = 0;
    private int b = 0;
    /* access modifiers changed from: private */
    public ah c;
    private boolean d;
    private b e;
    private a f;

    class a implements bc {
        private bb b = null;
        private Message c = null;
        private Runnable d = null;

        a() {
        }

        private bb b(GeoPoint geoPoint) {
            return new bb(PurchaseCode.QUERY_FROZEN, 10, MapController.this.c.f.j, geoPoint, MapController.this.c.b.e(), this);
        }

        private void c() {
            this.b = null;
            this.c = null;
            this.d = null;
        }

        public void a() {
            if (this.b != null) {
                this.b.d();
            }
        }

        public void a(GeoPoint geoPoint) {
            if (geoPoint != null) {
                if (geoPoint.b() == Long.MIN_VALUE || geoPoint.a() == Long.MIN_VALUE) {
                    MapController.this.setCenter(MapController.this.c.f.b(geoPoint));
                    return;
                }
                MapController.this.setCenter(geoPoint);
            }
        }

        public void a(GeoPoint geoPoint, Message message, Runnable runnable) {
            MapController.this.c.c.f477a = true;
            MapController.this.c.f.k = geoPoint.e();
            a();
            this.b = b(geoPoint);
            this.c = message;
            this.d = runnable;
            this.b.c();
        }

        public void b() {
            if (this.c != null) {
                this.c.getTarget().sendMessage(this.c);
            }
            if (this.d != null) {
                this.d.run();
            }
            c();
            MapController.this.c.c.f477a = false;
        }
    }

    class b implements Animation.AnimationListener {
        private LinkedList b = new LinkedList();
        private boolean c = false;
        /* access modifiers changed from: private */
        public bj d = null;

        b() {
        }

        private void a(int i, int i2, int i3, boolean z) {
            if (this.d == null) {
                this.d = new bj(MapController.this.c.b.g(), this);
            }
            this.d.j = z;
            this.d.i = i;
            this.d.a(i, false, (float) i2, (float) i3);
            this.c = true;
        }

        private void b(int i, int i2, int i3, boolean z) {
            if (this.d == null) {
                this.d = new bj(MapController.this.c.b.g(), this);
            }
            this.d.i = i;
            this.d.j = z;
            if (this.d.j) {
                Point point = new Point(i2, i3);
                GeoPoint fromPixels = MapController.this.c.b.g().getProjection().fromPixels(i2, i3);
                MapController.this.c.f.j = MapController.this.c.f.a(fromPixels);
                MapController.this.c.f.a(point);
            }
            this.d.a(i, true, (float) i2, (float) i3);
            this.c = true;
        }

        public void a() {
            this.b.clear();
        }

        public void a(int i, int i2, int i3, boolean z, boolean z2) {
            if (!z) {
                a(i3, i, i2, z2);
            } else {
                b(i3, i, i2, z2);
            }
        }

        public void onAnimationEnd(Animation animation) {
            MapView g = MapController.this.c.b.g();
            if (this.b.size() == 0) {
                this.c = false;
                g.getZoomMgr().a(true);
                MapController.this.c.d.d();
                return;
            }
            g.b().startAnimation((Animation) this.b.remove());
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationStart(Animation animation) {
        }
    }

    MapController(ah ahVar) {
        this.c = ahVar;
        this.d = false;
        this.e = new b();
        this.f = new a();
    }

    private int a(float f2) {
        int i = 1;
        int i2 = 0;
        int i3 = 1;
        while (((float) i3) <= f2) {
            i3 *= 2;
            i2 = i;
            i++;
        }
        return i2;
    }

    private boolean a(int i, int i2, boolean z, boolean z2) {
        return a(i, i2, z, z2, 1);
    }

    private boolean a(int i, int i2, boolean z, boolean z2, int i3) {
        int b2 = this.c.b.g().b(z ? this.c.b.e() + i3 : this.c.b.e() - i3);
        if (b2 == this.c.b.e()) {
            return false;
        }
        zoomAnimationAtLevel(i, i2, b2, z, z2);
        return true;
    }

    public static float calculateDistance(GeoPoint geoPoint, GeoPoint geoPoint2) {
        double a2 = d.a(geoPoint.a());
        double a3 = d.a(geoPoint.b());
        double d2 = a2 * 0.01745329251994329d;
        double d3 = a3 * 0.01745329251994329d;
        double a4 = d.a(geoPoint2.a()) * 0.01745329251994329d;
        double a5 = d.a(geoPoint2.b()) * 0.01745329251994329d;
        double sin = Math.sin(d2);
        double sin2 = Math.sin(d3);
        double cos = Math.cos(d2);
        double cos2 = Math.cos(d3);
        double sin3 = Math.sin(a4);
        double sin4 = Math.sin(a5);
        double cos3 = Math.cos(a4);
        double cos4 = Math.cos(a5);
        double[] dArr = {cos * cos2, cos2 * sin, sin2};
        double[] dArr2 = {cos4 * cos3, cos4 * sin3, sin4};
        return (float) (Math.asin(Math.sqrt((((dArr[0] - dArr2[0]) * (dArr[0] - dArr2[0])) + ((dArr[1] - dArr2[1]) * (dArr[1] - dArr2[1]))) + ((dArr[2] - dArr2[2]) * (dArr[2] - dArr2[2]))) / 2.0d) * 1.27420015798544E7d);
    }

    /* access modifiers changed from: package-private */
    public final boolean a() {
        return this.e.d.f();
    }

    /* access modifiers changed from: package-private */
    public final boolean a(int i) {
        return a(this.c.b.c() / 2, this.c.b.d() / 2, true, false, i);
    }

    public final void animateTo(GeoPoint geoPoint) {
        this.f.a(geoPoint, null, null);
    }

    public final void animateTo(GeoPoint geoPoint, Message message) {
        this.f.a(geoPoint, message, null);
    }

    public final void animateTo(GeoPoint geoPoint, Runnable runnable) {
        this.f.a(geoPoint, null, runnable);
    }

    /* access modifiers changed from: package-private */
    public final boolean b(int i) {
        return a(this.c.b.c() / 2, this.c.b.d() / 2, false, false, i);
    }

    public final int getReqLatSpan() {
        return this.f449a;
    }

    public final int getReqLngSpan() {
        return this.b;
    }

    public final boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 0) {
            return false;
        }
        boolean z = true;
        switch (i) {
            case 19:
                scrollBy(0, -10);
                break;
            case VMapProjection.MAXZOOMLEVEL /*20*/:
                scrollBy(0, 10);
                break;
            case 21:
                scrollBy(-10, 0);
                break;
            case Constants.WEIBO_SDK_VERSION /*22*/:
                scrollBy(10, 0);
                break;
            default:
                z = false;
                break;
        }
        return z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amap.mapapi.map.ah.d.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.amap.mapapi.map.ah.d.a(android.view.View, com.amap.mapapi.map.MapView$LayoutParams):void
      com.amap.mapapi.map.ah.d.a(int, int):void
      com.amap.mapapi.map.ah.d.a(boolean, boolean):void */
    public final void scrollBy(int i, int i2) {
        if (this.d) {
            this.d = false;
        } else if (i != 0 || i2 != 0) {
            if (com.amap.mapapi.core.b.n) {
                this.c.f.a(new PointF(0.0f, 0.0f), new PointF((float) i, (float) i2), this.c.b.e());
            }
            this.c.b.a(false, false);
        }
    }

    public final void setCenter(GeoPoint geoPoint) {
        this.c.b.a(geoPoint);
        if (this.c.b.g().VMapMode) {
            be LatLongToPixels = VMapProjection.LatLongToPixels(((double) geoPoint.getLatitudeE6()) / 1000000.0d, ((double) geoPoint.getLongitudeE6()) / 1000000.0d, 20);
            this.c.b.g().centerX = LatLongToPixels.f497a;
            this.c.b.g().centerY = LatLongToPixels.b;
        }
    }

    public final void setFitView(List list) {
        if (list != null && list.size() >= 2) {
            int i = 0;
            int i2 = Integer.MAX_VALUE;
            int i3 = Integer.MIN_VALUE;
            int i4 = Integer.MAX_VALUE;
            int i5 = Integer.MIN_VALUE;
            while (true) {
                int i6 = i;
                if (i6 < list.size()) {
                    GeoPoint geoPoint = (GeoPoint) list.get(i6);
                    int longitudeE6 = geoPoint.getLongitudeE6();
                    int latitudeE6 = geoPoint.getLatitudeE6();
                    if (longitudeE6 < i4) {
                        i4 = longitudeE6;
                    }
                    if (latitudeE6 < i2) {
                        i2 = latitudeE6;
                    }
                    if (longitudeE6 > i3) {
                        i3 = longitudeE6;
                    }
                    if (latitudeE6 > i5) {
                        i5 = latitudeE6;
                    }
                    i = i6 + 1;
                } else {
                    setCenter(new GeoPoint((i2 + i5) / 2, (i4 + i3) / 2));
                    zoomToSpan(i5 - i2, i3 - i4);
                    return;
                }
            }
        }
    }

    public final void setReqLatSpan(int i) {
        this.f449a = i;
    }

    public final void setReqLngSpan(int i) {
        this.b = i;
    }

    public final int setZoom(int i) {
        int i2;
        MapView g = this.c.b.g();
        int b2 = g.b(i);
        if (!this.c.b.g().VMapMode) {
            i2 = this.c.f.g;
            this.c.b.a(b2);
        } else {
            i2 = g.mapLevel;
            this.c.b.a(b2);
        }
        if (g.d != null) {
            ZoomButtonsController.OnZoomListener onZoomListener = g.getZoomButtonsController().getOnZoomListener();
            if (i2 < b2 && onZoomListener != null) {
                onZoomListener.onZoom(true);
            }
            if (i2 > b2 && onZoomListener != null) {
                onZoomListener.onZoom(false);
            }
        }
        return b2;
    }

    public final void stopAnimation(boolean z) {
        this.e.a();
        this.f.a();
    }

    public final void stopPanning() {
        this.d = true;
    }

    public final void zoomAnimationAtLevel(int i, int i2, int i3, boolean z, boolean z2) {
        this.e.a(i, i2, i3, z, z2);
    }

    public final boolean zoomIn() {
        return a(1);
    }

    public final boolean zoomInFixing(int i, int i2) {
        return a(i, i2, true, true);
    }

    public final boolean zoomOut() {
        return b(1);
    }

    public final boolean zoomOutFixing(int i, int i2) {
        return a(i, i2, false, true);
    }

    public final void zoomToSpan(int i, int i2) {
        if (i > 0 && i2 > 0) {
            int b2 = this.c.b.b();
            int a2 = this.c.b.a();
            int e2 = this.c.b.e();
            int b3 = this.c.f474a.b();
            int a3 = this.c.f474a.a();
            if (b3 == 0 && a3 == 0) {
                this.f449a = i;
                this.b = i2;
                return;
            }
            float max = Math.max(((float) i) / ((float) a3), ((float) i2) / ((float) b3));
            if (max > 1.0f) {
                int a4 = e2 - a(max);
                if (a4 > b2) {
                    b2 = a4;
                }
            } else if (((double) max) < 0.5d) {
                b2 = (a(1.0f / max) + e2) - 1;
                if (b2 >= a2) {
                    b2 = a2;
                }
            } else {
                b2 = e2;
            }
            setZoom(b2);
        }
    }
}
