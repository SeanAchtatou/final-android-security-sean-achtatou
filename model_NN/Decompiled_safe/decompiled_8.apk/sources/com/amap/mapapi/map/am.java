package com.amap.mapapi.map;

import java.util.Vector;

class am extends Vector {

    /* renamed from: a  reason: collision with root package name */
    protected int f485a = -1;

    am() {
    }

    public synchronized Object a() {
        Object elementAt;
        if (c()) {
            elementAt = null;
        } else {
            elementAt = super.elementAt(0);
            super.removeElementAt(0);
        }
        return elementAt;
    }

    public synchronized Object b() {
        return c() ? null : super.elementAt(0);
    }

    public boolean c() {
        return super.isEmpty();
    }

    public synchronized void clear() {
        super.removeAllElements();
    }
}
