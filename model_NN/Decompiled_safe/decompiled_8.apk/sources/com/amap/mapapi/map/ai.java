package com.amap.mapapi.map;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.util.FloatMath;
import android.view.MotionEvent;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import mm.purchasesdk.PurchaseCode;

abstract class ai {
    static float j = 1.0f;
    /* access modifiers changed from: private */
    public static Method o;
    /* access modifiers changed from: private */
    public static Method p;
    /* access modifiers changed from: private */
    public static boolean q = false;
    private static boolean r = false;

    /* renamed from: a  reason: collision with root package name */
    b f481a;
    int b = 0;
    Matrix c = new Matrix();
    Matrix d = new Matrix();
    PointF e = new PointF();
    PointF f = new PointF();
    PointF g = new PointF();
    float h = 1.0f;
    float i = 1.0f;
    boolean k = false;
    boolean l = false;
    boolean m = false;
    public int n = 0;

    class a extends ai {
        float o;
        float p;
        float q;
        float r;

        private a() {
        }

        private void a(PointF pointF, MotionEvent motionEvent) {
            float f;
            float f2 = 0.0f;
            try {
                f = ((Float) ai.o.invoke(motionEvent, 1)).floatValue() + ((Float) ai.o.invoke(motionEvent, 0)).floatValue();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                f = 0.0f;
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
                f = 0.0f;
            } catch (InvocationTargetException e3) {
                e3.printStackTrace();
                f = 0.0f;
            }
            try {
                f2 = ((Float) ai.p.invoke(motionEvent, 0)).floatValue() + ((Float) ai.p.invoke(motionEvent, 1)).floatValue();
            } catch (IllegalArgumentException e4) {
                e4.printStackTrace();
            } catch (IllegalAccessException e5) {
                e5.printStackTrace();
            } catch (InvocationTargetException e6) {
                e6.printStackTrace();
            }
            pointF.set(f / 2.0f, f2 / 2.0f);
        }

        private float c(MotionEvent motionEvent) {
            float f;
            float f2 = 0.0f;
            try {
                f = ((Float) ai.o.invoke(motionEvent, 0)).floatValue() - ((Float) ai.o.invoke(motionEvent, 1)).floatValue();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                f = 0.0f;
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
                f = 0.0f;
            } catch (InvocationTargetException e3) {
                e3.printStackTrace();
                f = 0.0f;
            }
            try {
                f2 = ((Float) ai.p.invoke(motionEvent, 0)).floatValue() - ((Float) ai.p.invoke(motionEvent, 1)).floatValue();
            } catch (IllegalArgumentException e4) {
                e4.printStackTrace();
            } catch (IllegalAccessException e5) {
                e5.printStackTrace();
            } catch (InvocationTargetException e6) {
                e6.printStackTrace();
            }
            return FloatMath.sqrt((f * f) + (f2 * f2));
        }

        public boolean a(MotionEvent motionEvent) {
            ai.c(motionEvent);
            if (!ai.q) {
                return false;
            }
            switch (motionEvent.getAction() & PurchaseCode.AUTH_INVALID_APP) {
                case 0:
                    this.o = motionEvent.getX();
                    this.p = motionEvent.getY();
                    this.d.set(this.c);
                    this.e.set(this.o, this.p);
                    this.b = 1;
                    return false;
                case 1:
                    this.k = false;
                    this.b = 0;
                    return false;
                case 2:
                    if (this.b == 1) {
                        float x = motionEvent.getX();
                        float y = motionEvent.getY();
                        this.c.set(this.d);
                        this.c.postTranslate(motionEvent.getX() - this.e.x, motionEvent.getY() - this.e.y);
                        this.o = x;
                        this.p = y;
                        return this.f481a.a(this.c) | this.f481a.a(x - this.o, y - this.p) | false;
                    } else if (this.b != 2) {
                        return false;
                    } else {
                        float c = c(motionEvent);
                        this.i = 0.0f;
                        if (c <= 10.0f || Math.abs(c - this.h) <= 5.0f) {
                            return false;
                        }
                        this.c.set(this.d);
                        this.i = c > this.h ? c / this.h : this.h / c;
                        j = c / this.h;
                        if (c < this.h) {
                            this.i = -this.i;
                        }
                        a(this.g, motionEvent);
                        this.q = this.g.x;
                        this.r = this.g.y;
                        this.c.postScale(c / this.h, c / this.h, this.f.x, this.f.y);
                        boolean a2 = this.f481a.a(this.g.x - this.q, this.g.y - this.r) | false | this.f481a.b(this.i) | this.f481a.b(this.c);
                        this.l = true;
                        return a2;
                    }
                case 3:
                case 4:
                default:
                    return false;
                case 5:
                    this.n++;
                    if (this.n != 1) {
                        return false;
                    }
                    this.m = true;
                    j = 1.0f;
                    this.h = c(motionEvent);
                    if (this.h <= 10.0f) {
                        return false;
                    }
                    this.c.reset();
                    this.d.reset();
                    this.d.set(this.c);
                    a(this.f, motionEvent);
                    this.b = 2;
                    this.k = true;
                    boolean a3 = this.f481a.a(this.e) | false;
                    this.q = this.f.x;
                    this.r = this.f.y;
                    return a3;
                case 6:
                    this.n--;
                    if (this.n == 1) {
                        this.m = true;
                        this.b = 2;
                    }
                    if (this.n != 0) {
                        return false;
                    }
                    a(this.f, motionEvent);
                    this.l = false;
                    this.m = false;
                    if (!this.k) {
                        return false;
                    }
                    this.k = false;
                    this.b = 0;
                    return this.f481a.a(this.i, this.f) | false;
            }
        }
    }

    public interface b {
        boolean a(float f, float f2);

        boolean a(float f, PointF pointF);

        boolean a(Matrix matrix);

        boolean a(PointF pointF);

        boolean b(float f);

        boolean b(Matrix matrix);
    }

    ai() {
    }

    public static ai a(Context context, b bVar) {
        a aVar = new a();
        aVar.f481a = bVar;
        return aVar;
    }

    /* access modifiers changed from: private */
    public static void c(MotionEvent motionEvent) {
        if (!r) {
            r = true;
            try {
                o = motionEvent.getClass().getMethod("getX", Integer.TYPE);
                p = motionEvent.getClass().getMethod("getY", Integer.TYPE);
                if (o != null && p != null) {
                    q = true;
                }
            } catch (Exception e2) {
            }
        }
    }

    public abstract boolean a(MotionEvent motionEvent);
}
