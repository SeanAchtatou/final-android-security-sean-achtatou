package com.amap.mapapi.map;

import android.content.Context;
import java.net.Proxy;
import java.util.ArrayList;

abstract class c extends ae {

    /* renamed from: a  reason: collision with root package name */
    protected volatile boolean f498a = true;
    protected ArrayList b = null;
    protected as c;
    protected at d;
    private Runnable g = new d(this);
    private Runnable h = new e(this);

    public c(ah ahVar, Context context) {
        super(ahVar, context);
        if (this.b == null) {
            this.b = new ArrayList();
        }
        this.d = new at(f(), this.h, this.g);
        this.d.a();
    }

    /* access modifiers changed from: protected */
    public abstract ArrayList a(ArrayList arrayList);

    /* access modifiers changed from: protected */
    public abstract ArrayList a(ArrayList arrayList, Proxy proxy);

    public void a() {
        this.c.a();
        e();
        this.c.c();
        this.c = null;
        this.e = null;
        this.f = null;
    }

    public void b() {
        super.b();
        e();
    }

    public void c() {
        super.c();
        e();
    }

    public void d() {
        this.f498a = true;
        if (this.b == null) {
            this.b = new ArrayList();
        }
        if (this.d == null) {
            this.d = new at(f(), this.h, this.g);
            this.d.a();
        }
    }

    public void e() {
        this.f498a = false;
        if (this.b != null) {
            int size = this.b.size();
            for (int i = 0; i < size; i++) {
                Thread thread = (Thread) this.b.get(0);
                if (thread != null) {
                    thread.interrupt();
                    this.b.remove(0);
                }
            }
            this.b = null;
        }
        if (this.d != null) {
            this.d.b();
            this.d.c();
            this.d = null;
        }
    }

    /* access modifiers changed from: protected */
    public abstract int f();

    /* access modifiers changed from: protected */
    public abstract int g();
}
