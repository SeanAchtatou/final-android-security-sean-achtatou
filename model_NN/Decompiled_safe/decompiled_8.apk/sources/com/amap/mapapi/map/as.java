package com.amap.mapapi.map;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;

class as {

    /* renamed from: a  reason: collision with root package name */
    protected LinkedList f489a = new LinkedList();
    protected final Semaphore b = new Semaphore(0, false);
    protected boolean c = true;

    as() {
    }

    public ArrayList a(int i, boolean z) {
        if (this.f489a == null) {
            return null;
        }
        try {
            this.b.acquire();
        } catch (InterruptedException e) {
        }
        if (this.c) {
            return b(i, z);
        }
        return null;
    }

    public void a() {
        this.c = false;
        this.b.release(100);
    }

    public synchronized void a(List list, boolean z) {
        if (this.f489a != null) {
            if (z) {
                this.f489a.clear();
            }
            if (list != null) {
                this.f489a.addAll(list);
            }
            b();
        }
    }

    /* access modifiers changed from: protected */
    public synchronized ArrayList b(int i, boolean z) {
        ArrayList arrayList;
        synchronized (this) {
            if (this.f489a == null) {
                arrayList = null;
            } else {
                int size = this.f489a.size();
                if (i > size) {
                    i = size;
                }
                arrayList = new ArrayList(i);
                for (int i2 = 0; i2 < i; i2++) {
                    arrayList.add(this.f489a.get(0));
                    this.f489a.removeFirst();
                }
                b();
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public void b() {
        if (this.f489a != null && this.c && this.f489a.size() != 0) {
            this.b.release();
        }
    }

    public void c() {
        if (this.f489a != null) {
            this.f489a.clear();
        }
    }
}
