package com.amap.mapapi.map;

import android.graphics.Bitmap;
import android.graphics.Point;

class bi extends s {
    public bi(ah ahVar, Bitmap bitmap) {
        super(ahVar, bitmap);
    }

    /* access modifiers changed from: protected */
    public Point a() {
        return new Point(0, (this.f511a.b.d() - this.b.getHeight()) - 10);
    }

    public void b() {
        if (this.b != null && !this.b.isRecycled()) {
            this.b.recycle();
            this.b = null;
        }
    }
}
