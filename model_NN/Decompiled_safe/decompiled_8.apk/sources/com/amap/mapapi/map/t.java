package com.amap.mapapi.map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.core.b;
import com.amap.mapapi.core.d;
import com.amap.mapapi.map.MapView;
import mm.purchasesdk.PurchaseCode;

class t implements GestureDetector.OnGestureListener {

    /* renamed from: a  reason: collision with root package name */
    public static t f512a = null;
    public static Drawable b = null;
    public static Bitmap c = null;
    protected MapView d;
    protected View e;
    protected GeoPoint f;
    protected long g = -1;
    protected MapView.LayoutParams h;

    public t(MapView mapView, View view, GeoPoint geoPoint, Drawable drawable, MapView.LayoutParams layoutParams) {
        this.d = mapView;
        this.e = view;
        this.f = geoPoint;
        this.h = layoutParams;
        a(drawable);
    }

    private static void a(Context context) {
        Rect rect = new Rect(20, 15, 19, 36);
        c = b.g.a(context, "popup_bg.9.png");
        b = new NinePatchDrawable(c, new byte[]{1, 2, 2, 9, 0, 0, 0, 0, 0, 0, 0, 0, 20, 0, 0, 0, 19, 0, 0, 0, 15, 0, 0, 0, 36, 0, 0, 0, 0, 0, 0, 0, 20, 0, 0, 0, -117, 0, 0, 0, 15, 0, 0, 0, 29, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, -1, -1, -1, -14, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0}, rect, null);
    }

    private void a(Drawable drawable) {
        if (drawable == null) {
            if (b == null) {
                a(this.d.getContext());
            }
            drawable = b;
            drawable.setAlpha(PurchaseCode.AUTH_INVALID_APP);
        }
        this.e.setBackgroundDrawable(drawable);
    }

    private boolean d() {
        return f512a == this;
    }

    public void a() {
        if (c != null && !c.isRecycled()) {
            c.recycle();
            c = null;
        }
    }

    public void b() {
        if (!d()) {
            if (f512a != null) {
                f512a.c();
            }
            f512a = this;
            this.d.b().a(this);
            if (this.h == null) {
                this.h = new MapView.LayoutParams(-2, -2, this.f, 25, 5, 85);
            }
            this.d.addView(this.e, this.h);
            this.g = d.a();
            this.d.b().a(this.g);
        }
    }

    public void c() {
        if (d() && this.d != null) {
            f512a = null;
            this.d.removeView(this.e);
            this.d.b().b(this);
        }
    }

    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        return false;
    }

    public void onLongPress(MotionEvent motionEvent) {
    }

    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        return false;
    }

    public void onShowPress(MotionEvent motionEvent) {
    }

    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }
}
