package com.amap.mapapi.map;

import com.amap.mapapi.poisearch.PoiTypeDef;
import java.util.Hashtable;

class aw extends Thread {

    /* renamed from: a  reason: collision with root package name */
    int f493a = 0;
    MapView b;
    long c;
    volatile boolean d = true;
    Thread e;
    private Hashtable f = new Hashtable();

    public aw(MapView mapView) {
        this.b = mapView;
        b();
    }

    public synchronized void a() {
        this.f.clear();
    }

    public synchronized void a(String str) {
        this.f.remove(str);
    }

    public void b() {
        this.c = System.currentTimeMillis();
    }

    public synchronized boolean b(String str) {
        return this.f.get(str) != null;
    }

    public void c() {
        this.d = false;
        if (this.e != null) {
            this.e.interrupt();
        }
    }

    public synchronized void c(String str) {
        this.f.put(str, PoiTypeDef.All);
    }

    public void run() {
        this.e = Thread.currentThread();
        while (this.d) {
            if (this.f493a > 0 && System.currentTimeMillis() - this.c > 300) {
                this.b.loadBMtilesData2(this.b.e(), true);
            }
            try {
                sleep(50);
            } catch (Exception e2) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
