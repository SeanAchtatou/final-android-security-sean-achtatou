package com.amap.mapapi.map;

import com.amap.mapapi.core.AMapException;
import java.util.ArrayList;
import java.util.List;

class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f499a;

    d(c cVar) {
        this.f499a = cVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amap.mapapi.map.as.a(int, boolean):java.util.ArrayList
     arg types: [int, int]
     candidates:
      com.amap.mapapi.map.as.a(java.util.List, boolean):void
      com.amap.mapapi.map.as.a(int, boolean):java.util.ArrayList */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amap.mapapi.map.as.a(java.util.List, boolean):void
     arg types: [java.util.ArrayList, int]
     candidates:
      com.amap.mapapi.map.as.a(int, boolean):java.util.ArrayList
      com.amap.mapapi.map.as.a(java.util.List, boolean):void */
    public void run() {
        ArrayList arrayList = null;
        this.f499a.b.add(Thread.currentThread());
        ArrayList arrayList2 = null;
        while (this.f499a.f498a) {
            if (this.f499a.e == null) {
                this.f499a.f498a = false;
            } else {
                if (this.f499a.c != null) {
                    arrayList2 = this.f499a.c.a(this.f499a.g(), false);
                }
                if (arrayList2 == null || arrayList2.size() != 0) {
                    if (this.f499a.f498a) {
                        if (arrayList2 != null) {
                            if (!this.f499a.f498a) {
                                return;
                            }
                            if (!(this.f499a.e == null || this.f499a.e.e == null)) {
                                try {
                                    arrayList = this.f499a.a(arrayList2, this.f499a.e.e.c());
                                } catch (AMapException e) {
                                    e.printStackTrace();
                                }
                                if (!(arrayList == null || this.f499a.c == null)) {
                                    this.f499a.c.a((List) arrayList, false);
                                }
                            }
                        }
                        if (this.f499a.f498a) {
                            try {
                                Thread.sleep(50);
                            } catch (Exception e2) {
                                Thread.currentThread().interrupt();
                            }
                        }
                    } else {
                        return;
                    }
                }
            }
        }
    }
}
