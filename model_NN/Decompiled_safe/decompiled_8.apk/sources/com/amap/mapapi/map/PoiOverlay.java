package com.amap.mapapi.map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.Spanned;
import android.text.util.Linkify;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.amap.mapapi.core.PoiItem;
import com.amap.mapapi.core.d;
import com.amap.mapapi.map.MapView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PoiOverlay extends ItemizedOverlay {

    /* renamed from: a  reason: collision with root package name */
    private t f462a;
    private ArrayList b;
    private boolean c;
    private MapView d;
    private boolean e;

    public PoiOverlay(Drawable drawable, List list) {
        this(drawable, list, PoiTypeDef.All);
    }

    public PoiOverlay(Drawable drawable, List list, String str) {
        super(drawable);
        this.f462a = null;
        this.b = new ArrayList();
        this.c = true;
        this.e = false;
        String trim = str.trim();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            PoiItem poiItem = (PoiItem) it.next();
            if (trim.equals(PoiTypeDef.All)) {
                this.b.add(poiItem);
            } else if (poiItem.getTypeCode() != null) {
                if (trim.length() <= 2 && poiItem.getTypeCode().startsWith(trim)) {
                    this.b.add(poiItem);
                } else if (trim.equals(poiItem.getTypeCode())) {
                    this.b.add(poiItem);
                }
            }
        }
        populate();
    }

    static Bitmap a(Drawable drawable) {
        int intrinsicWidth = drawable.getIntrinsicWidth();
        int intrinsicHeight = drawable.getIntrinsicHeight();
        Bitmap createBitmap = Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, drawable.getOpacity() != -1 ? Bitmap.Config.ARGB_4444 : Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(createBitmap);
        drawable.setBounds(0, 0, intrinsicWidth, intrinsicHeight);
        drawable.draw(canvas);
        return createBitmap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    static Drawable a(Drawable drawable, int i, int i2) {
        int intrinsicWidth = drawable.getIntrinsicWidth();
        int intrinsicHeight = drawable.getIntrinsicHeight();
        Bitmap a2 = a(drawable);
        Matrix matrix = new Matrix();
        matrix.postScale(((float) i) / ((float) intrinsicWidth), ((float) i2) / ((float) intrinsicHeight));
        Bitmap createBitmap = Bitmap.createBitmap(a2, 0, 0, intrinsicWidth, intrinsicHeight, matrix, true);
        if (a2 != null && !a2.isRecycled()) {
            a2.recycle();
        }
        return new BitmapDrawable(createBitmap);
    }

    private Spanned a(PoiItem poiItem) {
        String str = PoiTypeDef.All;
        try {
            str = d.a("类别: " + poiItem.getTypeDes().split(PoiItem.DesSplit)[1], "#000000");
        } catch (Exception e2) {
        }
        return d.c(str);
    }

    private void a(LinearLayout linearLayout, PoiItem poiItem, Context context) {
        String tel = poiItem.getTel();
        if (!d.a(tel)) {
            TextView textView = new TextView(context);
            String a2 = d.a("Tel:  " + tel, "#000000");
            textView.setBackgroundColor(-1);
            textView.setText(d.c(a2));
            textView.setLinksClickable(true);
            Linkify.addLinks(textView, 4);
            linearLayout.addView(textView, new LinearLayout.LayoutParams(-1, -2));
        }
    }

    private Spanned b(PoiItem poiItem) {
        String snippet = poiItem.getSnippet();
        if (d.a(snippet)) {
            return null;
        }
        return d.c(d.a("地址: " + snippet, "#000000"));
    }

    public void addToMap(MapView mapView) {
        this.d = mapView;
        mapView.getOverlays().add(this);
        this.e = true;
    }

    public void closePopupWindow() {
        if (this.f462a != null) {
            this.f462a.c();
        }
        this.f462a = null;
    }

    /* access modifiers changed from: protected */
    public PoiItem createItem(int i) {
        return (PoiItem) this.b.get(i);
    }

    public void enablePopup(boolean z) {
        this.c = z;
        if (!this.c) {
            closePopupWindow();
        }
    }

    /* access modifiers changed from: protected */
    public MapView.LayoutParams getLayoutParam() {
        return null;
    }

    /* access modifiers changed from: protected */
    public MapView.LayoutParams getLayoutParam(int i) {
        return null;
    }

    /* access modifiers changed from: protected */
    public Drawable getPopupBackground() {
        return null;
    }

    /* access modifiers changed from: protected */
    public Drawable getPopupMarker(PoiItem poiItem) {
        Drawable marker = poiItem.getMarker(0);
        if (marker == null) {
            marker = getDefaultMarker();
        }
        return a(marker, 24, 18);
    }

    /* access modifiers changed from: protected */
    public View getPopupView(PoiItem poiItem) {
        Context context = this.d.getContext();
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(1);
        linearLayout.setPadding(5, 10, 5, 20);
        LinearLayout linearLayout2 = new LinearLayout(context);
        linearLayout2.setOrientation(0);
        linearLayout2.setGravity(51);
        ImageView imageView = new ImageView(context);
        imageView.setBackgroundColor(-1);
        imageView.setImageDrawable(getPopupMarker(poiItem));
        TextView textView = new TextView(context);
        textView.setBackgroundColor(-1);
        textView.setText(d.c(d.a(poiItem.getTitle(), "#000000")));
        linearLayout2.addView(imageView, new LinearLayout.LayoutParams(-2, -2));
        linearLayout2.addView(textView, new LinearLayout.LayoutParams(-2, -2));
        linearLayout.addView(linearLayout2);
        if (b(poiItem) != null) {
            TextView textView2 = new TextView(context);
            textView2.setBackgroundColor(-1);
            textView2.setText(b(poiItem));
            linearLayout.addView(textView2, new LinearLayout.LayoutParams(-1, -2));
        }
        TextView textView3 = new TextView(context);
        textView3.setBackgroundColor(-1);
        textView3.setText(a(poiItem));
        linearLayout.addView(textView3, new LinearLayout.LayoutParams(-1, -2));
        a(linearLayout, poiItem, context);
        TextView textView4 = new TextView(context);
        textView4.setText(PoiTypeDef.All);
        textView4.setHeight(5);
        textView4.setWidth(1);
        linearLayout.addView(textView4);
        return linearLayout;
    }

    /* access modifiers changed from: protected */
    public boolean onTap(int i) {
        super.onTap(i);
        return showPopupWindow(i);
    }

    public boolean removeFromMap() {
        if (this.d == null) {
            throw new UnsupportedOperationException("poioverlay must be added to map frist!");
        } else if (!this.e) {
            return false;
        } else {
            boolean remove = this.d.getOverlays().remove(this);
            if (remove) {
                if (this.f462a != null) {
                    this.f462a.a();
                    closePopupWindow();
                }
                this.e = false;
            }
            return remove;
        }
    }

    public boolean showPopupWindow(int i) {
        if (!this.c) {
            return false;
        }
        if (this.d == null) {
            throw new UnsupportedOperationException("poioverlay must be added to map first!");
        }
        PoiItem poiItem = (PoiItem) this.b.get(i);
        this.f462a = new t(this.d, getPopupView((PoiItem) this.b.get(i)), poiItem.getPoint(), getPopupBackground(), getLayoutParam(i));
        this.d.a().b.a(poiItem.getPoint());
        this.f462a.b();
        return true;
    }

    public int size() {
        return this.b.size();
    }
}
