package com.amap.mapapi.map;

import android.graphics.Canvas;
import android.graphics.Point;
import android.view.MotionEvent;
import com.amap.mapapi.core.GeoPoint;

class aq {
    protected RouteOverlay c;

    public aq(RouteOverlay routeOverlay) {
        this.c = routeOverlay;
    }

    /* access modifiers changed from: protected */
    public Point a(MapView mapView, GeoPoint geoPoint) {
        return mapView.getProjection().toPixels(geoPoint, null);
    }

    /* access modifiers changed from: protected */
    public GeoPoint a(MapView mapView, Point point) {
        return mapView.getProjection().fromPixels(point.x, point.y);
    }

    public void a(Canvas canvas, MapView mapView, boolean z) {
    }

    public void a(MapView mapView) {
    }

    public boolean a(MotionEvent motionEvent, MapView mapView) {
        return false;
    }

    public void b(MapView mapView) {
    }
}
