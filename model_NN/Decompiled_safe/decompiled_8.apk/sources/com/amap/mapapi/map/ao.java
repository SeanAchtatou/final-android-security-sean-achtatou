package com.amap.mapapi.map;

import android.graphics.Point;
import com.mapabc.minimap.map.vmap.NativeMap;
import com.mapabc.minimap.map.vmap.NativeMapEngine;
import com.mapabc.minimap.map.vmap.VMapProjection;
import java.nio.ByteBuffer;

class ao {

    /* renamed from: a  reason: collision with root package name */
    public String f487a;
    boolean b = false;

    ao() {
    }

    public boolean a(NativeMapEngine nativeMapEngine) {
        try {
            ByteBuffer allocate = ByteBuffer.allocate(131072);
            if (nativeMapEngine.hasBitMapData(this.f487a)) {
                nativeMapEngine.fillBitmapBufferData(this.f487a, allocate.array());
            } else {
                NativeMap nativeMap = new NativeMap();
                nativeMap.initMap(allocate.array(), 256, 256);
                Point QuadKeyToTile = VMapProjection.QuadKeyToTile(this.f487a);
                int length = this.f487a.length();
                nativeMap.setMapParameter(((QuadKeyToTile.x * 256) + NativeMapEngine.MAX_ICON_SIZE) << (20 - length), ((QuadKeyToTile.y * 256) + NativeMapEngine.MAX_ICON_SIZE) << (20 - length), length, 0);
                nativeMap.paintMap(nativeMapEngine, 1);
                nativeMapEngine.putBitmapData(this.f487a, allocate.array(), 256, 256);
            }
            this.b = true;
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
