package com.amap.mapapi.map;

import android.graphics.Point;
import android.graphics.PointF;
import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.core.b;
import com.amap.mapapi.core.d;
import com.amap.mapapi.map.ah;
import com.amap.mapapi.map.au;
import java.util.ArrayList;

class ac {

    /* renamed from: a  reason: collision with root package name */
    double f469a = 156543.0339d;
    int b = 0;
    double c = -2.003750834E7d;
    double d = 2.003750834E7d;
    public int e = 3;
    public int f = 18;
    public int g = 10;
    public double[] h = null;
    public ad i = null;
    public GeoPoint j = null;
    public GeoPoint k = null;
    public Point l = null;
    public a m = null;
    ah.d n = null;
    private double o = 116.39716d;
    private double p = 39.91669d;
    private double q = 0.01745329251994329d;

    class a {

        /* renamed from: a  reason: collision with root package name */
        float f470a;
        float b;
        float c;
        float d;

        a() {
        }
    }

    public ac(ah.d dVar) {
        this.n = dVar;
    }

    private int a(int i2, int i3) {
        int i4 = 1;
        for (int i5 = 0; i5 < i3; i5++) {
            i4 *= i2;
        }
        return i4;
    }

    public float a(GeoPoint geoPoint, GeoPoint geoPoint2) {
        double a2 = d.a(geoPoint.a());
        double a3 = d.a(geoPoint.b());
        double a4 = d.a(geoPoint2.a());
        double a5 = d.a(geoPoint2.b());
        double d2 = a2 * this.q;
        double d3 = a3 * this.q;
        double d4 = a4 * this.q;
        double d5 = a5 * this.q;
        double sin = Math.sin(d2);
        double sin2 = Math.sin(d3);
        double cos = Math.cos(d2);
        double cos2 = Math.cos(d3);
        double sin3 = Math.sin(d4);
        double sin4 = Math.sin(d5);
        double cos3 = Math.cos(d4);
        double cos4 = Math.cos(d5);
        double[] dArr = {cos * cos2, cos2 * sin, sin2};
        double[] dArr2 = {cos4 * cos3, cos4 * sin3, sin4};
        return (float) (Math.asin(Math.sqrt((((dArr[0] - dArr2[0]) * (dArr[0] - dArr2[0])) + ((dArr[1] - dArr2[1]) * (dArr[1] - dArr2[1]))) + ((dArr[2] - dArr2[2]) * (dArr[2] - dArr2[2]))) / 2.0d) * 1.27420015798544E7d);
    }

    /* access modifiers changed from: package-private */
    public PointF a(int i2, int i3, int i4, int i5, PointF pointF, int i6, int i7) {
        PointF pointF2 = new PointF();
        pointF2.x = ((float) ((i2 - i4) * 256)) + pointF.x;
        if (this.b == 0) {
            pointF2.y = ((float) ((i3 - i5) * 256)) + pointF.y;
        } else if (this.b == 1) {
            pointF2.y = pointF.y - ((float) ((i3 - i5) * 256));
        }
        if (pointF2.x + 256.0f <= 0.0f || pointF2.x >= ((float) i6) || pointF2.y + 256.0f <= 0.0f || pointF2.y >= ((float) i7)) {
            return null;
        }
        return pointF2;
    }

    /* access modifiers changed from: package-private */
    public PointF a(GeoPoint geoPoint, GeoPoint geoPoint2, Point point, double d2) {
        PointF pointF = new PointF();
        pointF.x = (float) (((geoPoint.c() - geoPoint2.c()) / d2) + ((double) point.x));
        pointF.y = (float) (((double) point.y) - ((geoPoint.d() - geoPoint2.d()) / d2));
        return pointF;
    }

    public GeoPoint a(PointF pointF, GeoPoint geoPoint, Point point, double d2, a aVar) {
        return b(b(pointF, geoPoint, point, d2, aVar));
    }

    public GeoPoint a(GeoPoint geoPoint) {
        if (geoPoint == null) {
            return null;
        }
        if (b.h == GeoPoint.EnumMapProjection.projection_custBeijing54) {
            return geoPoint.e();
        }
        if (b.h != GeoPoint.EnumMapProjection.projection_900913) {
            return null;
        }
        return new GeoPoint(((Math.log(Math.tan((((((double) geoPoint.getLatitudeE6()) / 1000000.0d) + 90.0d) * 3.141592653589793d) / 360.0d)) / 0.017453292519943295d) * 2.003750834E7d) / 180.0d, ((((double) geoPoint.getLongitudeE6()) / 1000000.0d) * 2.003750834E7d) / 180.0d, false);
    }

    public ArrayList a(GeoPoint geoPoint, int i2, int i3, int i4) {
        int i5;
        double d2 = this.h[this.g];
        int c2 = (int) ((geoPoint.c() - this.c) / (256.0d * d2));
        double d3 = this.c + (((double) (c2 * 256)) * d2);
        double d4 = 0.0d;
        if (this.b == 0) {
            int d5 = (int) ((this.d - geoPoint.d()) / (256.0d * d2));
            d4 = this.d - (((double) (d5 * 256)) * d2);
            i5 = d5;
        } else if (this.b == 1) {
            int d6 = (int) ((geoPoint.d() - this.d) / (256.0d * d2));
            d4 = ((double) ((d6 + 1) * 256)) * d2;
            i5 = d6;
        } else {
            i5 = 0;
        }
        PointF a2 = a(new GeoPoint(d4, d3, false), geoPoint, this.l, d2);
        au.a aVar = new au.a(c2, i5, this.g, -1);
        aVar.f = a2;
        ArrayList arrayList = new ArrayList();
        arrayList.add(aVar);
        int i6 = 1;
        while (true) {
            int i7 = i6;
            boolean z = false;
            for (int i8 = c2 - i7; i8 <= c2 + i7; i8++) {
                int i9 = i5 + i7;
                PointF a3 = a(i8, i9, c2, i5, a2, i3, i4);
                if (a3 != null) {
                    boolean z2 = !z ? true : z;
                    au.a aVar2 = new au.a(i8, i9, this.g, -1);
                    aVar2.f = a3;
                    arrayList.add(aVar2);
                    z = z2;
                }
                int i10 = i5 - i7;
                PointF a4 = a(i8, i10, c2, i5, a2, i3, i4);
                if (a4 != null) {
                    if (!z) {
                        z = true;
                    }
                    au.a aVar3 = new au.a(i8, i10, this.g, -1);
                    aVar3.f = a4;
                    arrayList.add(aVar3);
                }
            }
            for (int i11 = (i5 + i7) - 1; i11 > i5 - i7; i11--) {
                int i12 = c2 + i7;
                PointF a5 = a(i12, i11, c2, i5, a2, i3, i4);
                if (a5 != null) {
                    boolean z3 = !z ? true : z;
                    au.a aVar4 = new au.a(i12, i11, this.g, -1);
                    aVar4.f = a5;
                    arrayList.add(aVar4);
                    z = z3;
                }
                int i13 = c2 - i7;
                PointF a6 = a(i13, i11, c2, i5, a2, i3, i4);
                if (a6 != null) {
                    if (!z) {
                        z = true;
                    }
                    au.a aVar5 = new au.a(i13, i11, this.g, -1);
                    aVar5.f = a6;
                    arrayList.add(aVar5);
                }
            }
            if (!z) {
                return arrayList;
            }
            i6 = i7 + 1;
        }
    }

    public void a() {
        GeoPoint geoPoint;
        if (this.i != null) {
            if (this.i.f471a > 0.0d) {
                this.o = this.i.f471a;
            }
            if (this.i.b > 0.0d) {
                this.p = this.i.b;
            }
            b.h = this.i.c;
            if (this.i.d > 0.0d) {
                this.f469a = this.i.d;
            }
            this.b = this.i.e;
            this.c = this.i.f;
            this.d = this.i.g;
            if (this.i.h >= 0) {
                this.e = this.i.h;
            }
            if (this.i.i >= 0) {
                this.f = this.i.i;
            }
            if (this.i.j >= 0) {
                this.g = this.i.j;
            }
        }
        this.h = new double[(this.f + 1)];
        for (int i2 = 0; i2 <= this.f; i2++) {
            this.h[i2] = this.f469a / ((double) a(2, i2));
        }
        if (b.h == GeoPoint.EnumMapProjection.projection_900913) {
            geoPoint = new GeoPoint(this.p, this.o, true);
            d.f424a = true;
        } else {
            geoPoint = new GeoPoint(this.p, this.o, false);
            d.f424a = false;
        }
        this.j = a(geoPoint);
        this.k = this.j.e();
        this.l = new Point(this.n.c() / 2, this.n.d() / 2);
        this.m = new a();
        if (b.h == GeoPoint.EnumMapProjection.projection_900913) {
            this.m.f470a = -2.0037508E7f;
            this.m.b = 2.0037508E7f;
            this.m.c = 2.0037508E7f;
            this.m.d = -2.0037508E7f;
        }
    }

    public void a(Point point) {
        this.l = point;
    }

    public void a(PointF pointF, PointF pointF2, int i2) {
        double d2 = this.h[i2];
        GeoPoint b2 = b(pointF, this.j, this.l, d2, this.m);
        GeoPoint b3 = b(pointF2, this.j, this.l, d2, this.m);
        double c2 = b3.c() - b2.c();
        double d3 = b3.d() - b2.d();
        double c3 = this.j.c() + c2;
        double d4 = this.j.d() + d3;
        if (b.h == GeoPoint.EnumMapProjection.projection_900913) {
            while (c3 < ((double) this.m.f470a)) {
                c3 += (double) (this.m.b - this.m.f470a);
            }
            while (c3 > ((double) this.m.b)) {
                c3 -= (double) (this.m.b - this.m.f470a);
            }
            while (d4 < ((double) this.m.d)) {
                d4 += (double) (this.m.c - this.m.d);
            }
            while (d4 > ((double) this.m.c)) {
                d4 -= (double) (this.m.c - this.m.d);
            }
        }
        this.j.b(d4);
        this.j.a(c3);
    }

    public void a(ad adVar) {
        this.i = adVar;
        a();
    }

    public PointF b(GeoPoint geoPoint, GeoPoint geoPoint2, Point point, double d2) {
        return this.n.g().toScreenPoint(a(a(geoPoint), geoPoint2, point, d2));
    }

    /* access modifiers changed from: package-private */
    public GeoPoint b(PointF pointF, GeoPoint geoPoint, Point point, double d2, a aVar) {
        PointF fromScreenPoint = this.n.g().fromScreenPoint(pointF);
        float f2 = fromScreenPoint.x - ((float) point.x);
        float f3 = fromScreenPoint.y - ((float) point.y);
        double c2 = (((double) f2) * d2) + geoPoint.c();
        double d3 = geoPoint.d() - (((double) f3) * d2);
        if (b.h == GeoPoint.EnumMapProjection.projection_900913) {
            while (c2 < ((double) aVar.f470a)) {
                c2 += (double) (aVar.b - aVar.f470a);
            }
            while (c2 > ((double) aVar.b)) {
                c2 -= (double) (aVar.b - aVar.f470a);
            }
            while (d3 < ((double) aVar.d)) {
                d3 += (double) (aVar.c - aVar.d);
            }
            while (d3 > ((double) aVar.c)) {
                d3 -= (double) (aVar.c - aVar.d);
            }
        }
        return new GeoPoint(d3, c2, false);
    }

    public GeoPoint b(GeoPoint geoPoint) {
        if (b.h == GeoPoint.EnumMapProjection.projection_custBeijing54) {
            return new GeoPoint(geoPoint.d(), geoPoint.c(), (long) geoPoint.d(), (long) geoPoint.c());
        }
        if (b.h != GeoPoint.EnumMapProjection.projection_900913) {
            return null;
        }
        return new GeoPoint((int) (((double) ((float) (57.29577951308232d * ((2.0d * Math.atan(Math.exp((((double) ((float) ((geoPoint.d() * 180.0d) / 2.003750834E7d))) * 3.141592653589793d) / 180.0d))) - 1.5707963267948966d)))) * 1000000.0d), (int) (((double) ((float) ((geoPoint.c() * 180.0d) / 2.003750834E7d))) * 1000000.0d));
    }

    public au.a b() {
        double d2 = this.h[this.g];
        int i2 = 0;
        int c2 = (int) ((this.j.c() - this.c) / (d2 * 256.0d));
        if (this.b == 0) {
            i2 = (int) ((this.d - this.j.d()) / (d2 * 256.0d));
        } else if (this.b == 1) {
            i2 = (int) ((this.j.d() - this.d) / (d2 * 256.0d));
        }
        return new au.a(c2, i2, this.g, -1);
    }
}
