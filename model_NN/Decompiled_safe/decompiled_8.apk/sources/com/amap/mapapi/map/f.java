package com.amap.mapapi.map;

import android.graphics.Bitmap;
import android.graphics.Canvas;

class f {

    /* renamed from: a  reason: collision with root package name */
    protected Bitmap f501a = null;
    protected Canvas b = null;
    protected Bitmap.Config c;

    public f(Bitmap.Config config) {
        this.c = config;
    }

    public void a() {
        if (this.f501a != null) {
            this.f501a.recycle();
        }
        this.f501a = null;
        this.b = null;
    }

    public void a(int i, int i2) {
        a();
        this.f501a = Bitmap.createBitmap(i, i2, this.c);
        this.b = new Canvas(this.f501a);
    }

    public void a(Bitmap bitmap) {
        this.f501a = bitmap;
        this.b = new Canvas(this.f501a);
    }

    public void a(g gVar) {
        this.b.save(1);
        gVar.a(this.b);
        this.b.restore();
    }

    public Bitmap b() {
        return this.f501a;
    }
}
