package com.amap.mapapi.map;

import android.graphics.drawable.Drawable;
import android.view.View;
import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.map.MapView;

class bg extends al {
    private MapView.LayoutParams d;
    private View e;

    public bg(RouteOverlay routeOverlay, int i, GeoPoint geoPoint, View view, Drawable drawable, MapView.LayoutParams layoutParams) {
        super(routeOverlay, i, geoPoint);
        this.e = view;
        this.e.setBackgroundDrawable(drawable);
        this.d = layoutParams;
    }

    public void a(MapView mapView) {
        mapView.addView(this.e, this.d);
    }

    public void b(MapView mapView) {
        mapView.removeView(this.e);
    }
}
