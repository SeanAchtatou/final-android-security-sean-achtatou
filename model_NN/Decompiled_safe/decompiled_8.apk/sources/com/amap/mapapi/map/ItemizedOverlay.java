package com.amap.mapapi.map;

import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.KeyEvent;
import android.view.MotionEvent;
import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.core.OverlayItem;
import com.amap.mapapi.core.b;
import com.amap.mapapi.map.Overlay;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

public abstract class ItemizedOverlay extends Overlay implements Overlay.Snappable {
    private static int d = -1;

    /* renamed from: a  reason: collision with root package name */
    private boolean f445a = true;
    /* access modifiers changed from: private */
    public Drawable b;
    private Drawable c;
    private b e = null;
    private OnFocusChangeListener f = null;
    private int g = -1;
    private int h = -1;

    public interface OnFocusChangeListener {
        void onFocusChanged(ItemizedOverlay itemizedOverlay, OverlayItem overlayItem);
    }

    enum a {
        Normal,
        Center,
        CenterBottom
    }

    class b implements Comparator {
        private ArrayList b;
        private ArrayList c;

        public b() {
            int size = ItemizedOverlay.this.size();
            this.b = new ArrayList(size);
            this.c = new ArrayList(size);
            for (int i = 0; i < size; i++) {
                this.c.add(Integer.valueOf(i));
                this.b.add(ItemizedOverlay.this.createItem(i));
            }
            Collections.sort(this.c, this);
        }

        private double a(OverlayItem overlayItem, Projection projection, Point point, int i) {
            if (!b(overlayItem, projection, point, i)) {
                return -1.0d;
            }
            GeoPoint.a a2 = a(overlayItem, projection, point);
            return (double) ((a2.b * a2.b) + (a2.f418a * a2.f418a));
        }

        private GeoPoint.a a(OverlayItem overlayItem, Projection projection, Point point) {
            Point pixels = projection.toPixels(overlayItem.getPoint(), null);
            return new GeoPoint.a(point.x - pixels.x, point.y - pixels.y);
        }

        private boolean b(OverlayItem overlayItem, Projection projection, Point point, int i) {
            GeoPoint.a a2 = a(overlayItem, projection, point);
            Drawable drawable = overlayItem.getmMarker();
            if (drawable == null) {
                drawable = ItemizedOverlay.this.b;
            }
            return ItemizedOverlay.this.hitTest(overlayItem, drawable, a2.f418a, a2.b);
        }

        public int a() {
            return this.b.size();
        }

        public int a(OverlayItem overlayItem) {
            if (overlayItem != null) {
                for (int i = 0; i < a(); i++) {
                    if (overlayItem.equals(this.b.get(i))) {
                        return i;
                    }
                }
            }
            return -1;
        }

        /* renamed from: a */
        public int compare(Integer num, Integer num2) {
            GeoPoint point = ((OverlayItem) this.b.get(num.intValue())).getPoint();
            GeoPoint point2 = ((OverlayItem) this.b.get(num2.intValue())).getPoint();
            if (point.getLatitudeE6() > point2.getLatitudeE6()) {
                return -1;
            }
            if (point.getLatitudeE6() < point2.getLatitudeE6()) {
                return 1;
            }
            if (point.getLongitudeE6() < point2.getLongitudeE6()) {
                return -1;
            }
            return point.getLongitudeE6() > point2.getLongitudeE6() ? 1 : 0;
        }

        public int a(boolean z) {
            if (this.b.size() == 0) {
                return 0;
            }
            int i = Integer.MAX_VALUE;
            Iterator it = this.b.iterator();
            int i2 = Integer.MIN_VALUE;
            while (true) {
                int i3 = i;
                if (!it.hasNext()) {
                    return i2 - i3;
                }
                GeoPoint point = ((OverlayItem) it.next()).getPoint();
                i = z ? point.getLatitudeE6() : point.getLongitudeE6();
                if (i > i2) {
                    i2 = i;
                }
                if (i >= i3) {
                    i = i3;
                }
            }
        }

        public OverlayItem a(int i) {
            return (OverlayItem) this.b.get(i);
        }

        public boolean a(GeoPoint geoPoint, MapView mapView) {
            boolean z;
            Projection projection = mapView.getProjection();
            Point pixels = projection.toPixels(geoPoint, null);
            int i = 0;
            int i2 = -1;
            int i3 = Integer.MAX_VALUE;
            double d = Double.MAX_VALUE;
            while (true) {
                int i4 = i;
                if (i4 >= a()) {
                    break;
                }
                double a2 = a((OverlayItem) this.b.get(i4), projection, pixels, i4);
                if (a2 >= 0.0d && a2 < d) {
                    i3 = b(i4);
                    d = a2;
                    i2 = i4;
                } else if (a2 == d && b(i4) > i3) {
                    i2 = i4;
                }
                i = i4 + 1;
            }
            if (-1 != i2) {
                z = ItemizedOverlay.this.onTap(i2);
            } else {
                ItemizedOverlay.this.setFocus(null);
                z = false;
            }
            mapView.a().d.d();
            return z;
        }

        public int b(int i) {
            return ((Integer) this.c.get(i)).intValue();
        }
    }

    public ItemizedOverlay(Drawable drawable) {
        this.b = drawable;
        if (this.b == null) {
            this.b = new BitmapDrawable(com.amap.mapapi.core.b.g.a(b.a.emarker.ordinal()));
        }
        this.b.setBounds(0, 0, this.b.getIntrinsicWidth(), this.b.getIntrinsicHeight());
        this.c = new ar().a(this.b);
        if (1 == d) {
            boundCenterBottom(this.b);
        } else if (2 == d) {
            boundCenter(this.b);
        } else {
            boundCenterBottom(this.b);
        }
    }

    private static Drawable a(Drawable drawable, a aVar) {
        int i = 0;
        if (drawable == null || a.Normal == aVar) {
            return null;
        }
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        Rect bounds = drawable.getBounds();
        int width = bounds.width() / 2;
        int i2 = -bounds.height();
        if (aVar == a.Center) {
            i2 /= 2;
            i = -i2;
        }
        drawable.setBounds(-width, i2, width, i);
        return drawable;
    }

    private OverlayItem a(int i) {
        if (i == 0) {
            return null;
        }
        return this.e.a(i - 1);
    }

    private void a(Canvas canvas, MapView mapView, boolean z, OverlayItem overlayItem, int i) {
        Drawable marker = overlayItem.getMarker(i);
        boolean z2 = marker == null;
        if (marker != null) {
            z2 = marker.equals(this.b);
        }
        if (z2) {
            if (z) {
                marker = this.c;
                this.c.setBounds(this.b.copyBounds());
                ar.a(this.c, this.b);
            } else {
                marker = this.b;
            }
        }
        Point pixels = mapView.getProjection().toPixels(overlayItem.getPoint(), null);
        if (z2) {
            Overlay.a(canvas, marker, pixels.x, pixels.y);
        } else {
            Overlay.drawAt(canvas, marker, pixels.x, pixels.y, z);
        }
    }

    private OverlayItem b(int i) {
        if (i == this.e.a() - 1) {
            return null;
        }
        return this.e.a(i + 1);
    }

    public static Drawable boundCenter(Drawable drawable) {
        d = 2;
        return a(drawable, a.Center);
    }

    public static Drawable boundCenterBottom(Drawable drawable) {
        d = 1;
        return a(drawable, a.CenterBottom);
    }

    /* access modifiers changed from: protected */
    public abstract OverlayItem createItem(int i);

    public void draw(Canvas canvas, MapView mapView, boolean z) {
        for (int i = 0; i < this.e.a(); i++) {
            int indexToDraw = getIndexToDraw(i);
            if (indexToDraw != this.h) {
                a(canvas, mapView, z, getItem(indexToDraw), 0);
            }
        }
        OverlayItem focus = getFocus();
        if (this.f445a && focus != null) {
            a(canvas, mapView, true, focus, 4);
            a(canvas, mapView, false, focus, 4);
        }
    }

    public GeoPoint getCenter() {
        return getItem(getIndexToDraw(0)).getPoint();
    }

    /* access modifiers changed from: protected */
    public Drawable getDefaultMarker() {
        BitmapDrawable bitmapDrawable = new BitmapDrawable(com.amap.mapapi.core.b.g.a(b.a.emarker.ordinal()));
        bitmapDrawable.setBounds(0, 0, bitmapDrawable.getIntrinsicWidth(), bitmapDrawable.getIntrinsicHeight());
        return bitmapDrawable;
    }

    public OverlayItem getFocus() {
        if (this.h != -1) {
            return this.e.a(this.h);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public int getIndexToDraw(int i) {
        return this.e.b(i);
    }

    public final OverlayItem getItem(int i) {
        return this.e.a(i);
    }

    public final int getLastFocusedIndex() {
        return this.g;
    }

    public int getLatSpanE6() {
        return this.e.a(true);
    }

    public int getLonSpanE6() {
        return this.e.a(false);
    }

    /* access modifiers changed from: protected */
    public boolean hitTest(OverlayItem overlayItem, Drawable drawable, int i, int i2) {
        return drawable.getBounds().contains(i, i2);
    }

    public OverlayItem nextFocus(boolean z) {
        if (this.e.a() == 0) {
            return null;
        }
        if (this.g != -1) {
            int i = this.h == -1 ? this.g : this.h;
            return z ? b(i) : a(i);
        } else if (z) {
            return this.e.a(0);
        } else {
            return null;
        }
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent, MapView mapView) {
        return false;
    }

    public boolean onSnapToItem(int i, int i2, Point point, MapView mapView) {
        for (int i3 = 0; i3 < this.e.a(); i3++) {
            Point pixels = mapView.getProjection().toPixels(this.e.a(i3).getPoint(), null);
            point.x = pixels.x;
            point.y = pixels.y;
            double d2 = (double) (i - pixels.x);
            double d3 = (double) (i2 - pixels.y);
            boolean z = (d2 * d2) + (d3 * d3) < 64.0d;
            if (z) {
                return z;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean onTap(int i) {
        if (i == this.h) {
            return false;
        }
        setFocus(getItem(i));
        return false;
    }

    public boolean onTap(GeoPoint geoPoint, MapView mapView) {
        return this.e.a(geoPoint, mapView);
    }

    public boolean onTouchEvent(MotionEvent motionEvent, MapView mapView) {
        return false;
    }

    public boolean onTrackballEvent(MotionEvent motionEvent, MapView mapView) {
        return false;
    }

    /* access modifiers changed from: protected */
    public final void populate() {
        this.e = new b();
        this.g = -1;
        this.h = -1;
    }

    public void setDrawFocusedItem(boolean z) {
        this.f445a = z;
    }

    public void setFocus(OverlayItem overlayItem) {
        if (overlayItem != null && this.h == this.e.a(overlayItem)) {
            return;
        }
        if (overlayItem != null || this.h == -1) {
            this.h = this.e.a(overlayItem);
            if (this.h != -1) {
                setLastFocusedIndex(this.h);
                if (this.f != null) {
                    this.f.onFocusChanged(this, overlayItem);
                    return;
                }
                return;
            }
            return;
        }
        if (this.f != null) {
            this.f.onFocusChanged(this, overlayItem);
        }
        this.h = -1;
    }

    /* access modifiers changed from: protected */
    public void setLastFocusedIndex(int i) {
        this.g = i;
    }

    public void setOnFocusChangeListener(OnFocusChangeListener onFocusChangeListener) {
        this.f = onFocusChangeListener;
    }

    public abstract int size();
}
