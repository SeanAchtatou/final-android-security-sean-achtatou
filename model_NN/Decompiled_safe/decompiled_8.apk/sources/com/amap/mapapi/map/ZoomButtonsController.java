package com.amap.mapapi.map;

import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import com.amap.mapapi.map.MapView;

public class ZoomButtonsController implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private MapView.g f465a;
    private ViewGroup b;
    private OnZoomListener c;

    public interface OnZoomListener {
        void onVisibilityChanged(boolean z);

        void onZoom(boolean z);
    }

    public ZoomButtonsController(View view) {
        this.b = (MapView) view;
        this.f465a = ((MapView) view).getZoomMgr();
    }

    public ViewGroup getContainer() {
        return this.b;
    }

    public OnZoomListener getOnZoomListener() {
        return this.c;
    }

    public View getZoomControls() {
        return null;
    }

    public boolean isAutoDismissed() {
        return false;
    }

    public boolean isVisible() {
        return this.f465a.b();
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        return false;
    }

    public void setAutoDismissed(boolean z) {
    }

    public void setFocusable(boolean z) {
    }

    public void setOnZoomListener(OnZoomListener onZoomListener) {
        this.c = onZoomListener;
    }

    public void setVisible(boolean z) {
        this.f465a.a(z);
    }

    public void setZoomInEnabled(boolean z) {
        this.f465a.f().setEnabled(z);
    }

    public void setZoomOutEnabled(boolean z) {
        this.f465a.g().setEnabled(z);
    }

    public void setZoomSpeed(long j) {
    }
}
