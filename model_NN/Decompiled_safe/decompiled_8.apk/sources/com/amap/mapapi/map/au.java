package com.amap.mapapi.map;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.PointF;
import mm.purchasesdk.PurchaseCode;

class au {

    /* renamed from: a  reason: collision with root package name */
    private static Paint f491a = null;
    private static Bitmap b = null;
    private static int c = Color.rgb((int) PurchaseCode.CERT_REQUEST_CANCEL, (int) PurchaseCode.CETRT_SID_ERR, (int) PurchaseCode.APPLYCERT_OTHER_ERR);

    class a {

        /* renamed from: a  reason: collision with root package name */
        public int f492a = 0;
        public final int b;
        public final int c;
        public final int d;
        public final int e;
        public PointF f;
        public int g = -1;

        public a(int i, int i2, int i3, int i4) {
            this.b = i;
            this.c = i2;
            this.d = i3;
            this.e = i4;
        }

        public a(a aVar) {
            this.b = aVar.b;
            this.c = aVar.c;
            this.d = aVar.d;
            this.e = aVar.e;
            this.f = aVar.f;
            this.f492a = aVar.f492a;
        }

        /* renamed from: a */
        public a clone() {
            return new a(this);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return this.b == aVar.b && this.c == aVar.c && this.d == aVar.d && this.e == aVar.e;
        }

        public int hashCode() {
            return (this.b * 7) + (this.c * 11) + (this.d * 13) + this.e;
        }

        public String toString() {
            return this.b + "-" + this.c + "-" + this.d + "-" + this.e;
        }
    }

    au() {
    }

    public static int a() {
        return c;
    }

    public static Paint b() {
        if (f491a == null) {
            Paint paint = new Paint();
            f491a = paint;
            paint.setColor(-7829368);
            f491a.setAlpha(90);
            f491a.setPathEffect(new DashPathEffect(new float[]{2.0f, 2.5f}, 1.0f));
        }
        return f491a;
    }

    public static Bitmap c() {
        if (b == null) {
            AnonymousClass1 r0 = new g() {
                public final void a(Canvas canvas) {
                    Paint b = au.b();
                    canvas.drawColor(au.a());
                    int i = 0;
                    while (true) {
                        int i2 = i;
                        if (i2 < 235) {
                            canvas.drawLine((float) i2, 0.0f, (float) i2, 256.0f, b);
                            canvas.drawLine(0.0f, (float) i2, 256.0f, (float) i2, b);
                            i = i2 + 21;
                        } else {
                            return;
                        }
                    }
                }
            };
            f fVar = new f(Bitmap.Config.ARGB_4444);
            fVar.a(256, 256);
            fVar.a(r0);
            b = fVar.b();
        }
        return b;
    }
}
