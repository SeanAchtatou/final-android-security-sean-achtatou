package com.amap.mapapi.map;

import android.content.Context;
import com.amap.mapapi.core.a;

public class aj {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f482a;
    private Runnable b = new ak(this);

    public aj(Context context) {
        this.f482a = context;
    }

    /* access modifiers changed from: private */
    public byte[] b() {
        a a2 = a.a(this.f482a);
        if (a2 != null) {
            return a2.a().getBytes();
        }
        return null;
    }

    public void a() {
        new Thread(this.b).start();
    }
}
