package com.amap.mapapi.map;

import java.util.ArrayList;

class an extends Thread {

    /* renamed from: a  reason: collision with root package name */
    volatile boolean f486a = true;
    Thread b;
    MapView c;
    ArrayList d = new ArrayList();

    public an(MapView mapView) {
        this.c = mapView;
    }

    public void a() {
        this.f486a = false;
        if (this.b != null) {
            this.b.interrupt();
        }
    }

    public void a(ao aoVar) {
        this.d.add(aoVar);
    }

    public boolean a(String str) {
        int i = 0;
        while (i < this.d.size()) {
            try {
                if (((ao) this.d.get(i)).f487a.equals(str)) {
                    return true;
                }
                i++;
            } catch (Exception e) {
                return false;
            }
        }
        return false;
    }

    public void run() {
        while (this.f486a) {
            this.b = Thread.currentThread();
            if (this.d.size() > 0) {
                ao aoVar = (ao) this.d.get(0);
                this.d.remove(0);
                if (this.c.a(aoVar.f487a)) {
                    aoVar.a(this.c.f);
                    this.c.postInvalidate();
                }
            }
            try {
                sleep(50);
            } catch (Exception e) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
