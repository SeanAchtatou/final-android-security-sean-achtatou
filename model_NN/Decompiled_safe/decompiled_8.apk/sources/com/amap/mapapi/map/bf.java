package com.amap.mapapi.map;

import android.content.Context;
import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.core.s;
import com.amap.mapapi.map.au;
import com.mapabc.minimap.map.vmap.VMapProjection;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.List;

class bf extends c implements bh {
    private u g = new u();

    public bf(ah ahVar, Context context) {
        super(ahVar, context);
        this.c = new av();
        ahVar.b.a(this);
    }

    private ArrayList a(ArrayList arrayList, w wVar, int i, boolean z) {
        int size;
        if (arrayList == null || wVar == null || !wVar.f || wVar.o == null) {
            return null;
        }
        wVar.o.clear();
        if (i > wVar.b || i < wVar.c || (size = arrayList.size()) <= 0) {
            return null;
        }
        ArrayList arrayList2 = new ArrayList();
        for (int i2 = 0; i2 < size; i2++) {
            au.a aVar = (au.a) arrayList.get(i2);
            if (aVar != null) {
                int a2 = wVar.m.a(aVar.b + "-" + aVar.c + "-" + aVar.d);
                au.a aVar2 = new au.a(aVar.b, aVar.c, aVar.d, wVar.k);
                aVar2.g = a2;
                aVar2.f = aVar.f;
                wVar.o.add(aVar2);
                if (a(aVar2) && !z && !this.g.contains(aVar2)) {
                    if (!wVar.g) {
                        aVar2.f492a = -1;
                    }
                    arrayList2.add(aVar2);
                }
            }
        }
        return arrayList2;
    }

    private void a(ArrayList arrayList, boolean z) {
        if (this.c != null && arrayList != null && arrayList.size() != 0) {
            this.c.a(arrayList, z);
        }
    }

    private boolean a(au.a aVar) {
        return aVar == null || aVar.g < 0;
    }

    private void b(ArrayList arrayList) {
        int size;
        if (arrayList != null && this.g != null && (size = arrayList.size()) != 0) {
            for (int i = 0; i < size; i++) {
                this.g.a((au.a) arrayList.get(i));
            }
        }
    }

    /* access modifiers changed from: protected */
    public ArrayList a(ArrayList arrayList) {
        int i;
        if (arrayList == null) {
            return null;
        }
        int size = arrayList.size();
        if (size == 0) {
            return null;
        }
        int i2 = 0;
        ArrayList arrayList2 = null;
        while (i2 < size) {
            au.a aVar = (au.a) arrayList.get(i2);
            if (aVar != null) {
                if (this.e == null || this.e.d == null || this.e.d.f475a == null) {
                    return null;
                }
                if (aVar.e < this.e.d.f475a.size() && ((w) this.e.d.f475a.get(aVar.e)).g) {
                    int a2 = ((w) this.e.d.f475a.get(aVar.e)).n.a(aVar);
                    if (a2 >= 0) {
                        arrayList.remove(i2);
                        size--;
                        i2--;
                        s sVar = ((w) this.e.d.f475a.get(aVar.e)).o;
                        if (sVar != null) {
                            int size2 = sVar.size();
                            int i3 = 0;
                            while (true) {
                                if (i3 < size2) {
                                    au.a aVar2 = (au.a) sVar.get(i3);
                                    if (aVar2 != null && aVar2.equals(aVar)) {
                                        aVar2.g = a2;
                                        this.e.d.d();
                                        i = i2;
                                        break;
                                    }
                                    i3++;
                                } else {
                                    i = i2;
                                    break;
                                }
                            }
                        }
                    } else {
                        ArrayList arrayList3 = arrayList2 == null ? new ArrayList() : arrayList2;
                        au.a aVar3 = new au.a(aVar);
                        aVar3.f492a = -1;
                        arrayList3.add(aVar3);
                        i = i2;
                        arrayList2 = arrayList3;
                    }
                    i2 = i + 1;
                    size = size;
                }
            }
            i = i2;
            i2 = i + 1;
            size = size;
        }
        return arrayList2;
    }

    /* access modifiers changed from: protected */
    public ArrayList a(ArrayList arrayList, Proxy proxy) {
        ArrayList arrayList2;
        if (arrayList == null || arrayList.size() == 0 || this.e == null || this.e.d == null || this.e.d.f475a == null || ((au.a) arrayList.get(0)).e >= this.e.d.f475a.size()) {
            return null;
        }
        a((List) arrayList);
        if (arrayList.size() == 0 || this.e.d.f475a.size() == 0) {
            return null;
        }
        if (((w) this.e.d.f475a.get(((au.a) arrayList.get(0)).e)).j != null) {
            ay ayVar = new ay(arrayList, proxy, this.e.e.a(), this.e.e.b());
            ayVar.a((w) this.e.d.f475a.get(((au.a) arrayList.get(0)).e));
            arrayList2 = (ArrayList) ayVar.j();
            ayVar.a((w) null);
        } else {
            arrayList2 = null;
        }
        b(arrayList);
        if (this.e == null || this.e.d == null) {
            return arrayList2;
        }
        this.e.d.d();
        return arrayList2;
    }

    public void a() {
        super.a();
        this.g.clear();
    }

    public void a(List list) {
        int size;
        int i;
        int i2;
        if (list != null && (size = list.size()) != 0) {
            int i3 = 0;
            while (i3 < size) {
                if (!this.g.b((au.a) list.get(i3))) {
                    list.remove(i3);
                    i = i3 - 1;
                    i2 = size - 1;
                } else {
                    i = i3;
                    i2 = size;
                }
                size = i2;
                i3 = i + 1;
            }
        }
    }

    public void a(boolean z, boolean z2) {
        ArrayList a2;
        if (this.e != null) {
            GeoPoint f = this.e.b.f();
            be LatLongToPixels = VMapProjection.LatLongToPixels(((double) f.getLatitudeE6()) / 1000000.0d, ((double) f.getLongitudeE6()) / 1000000.0d, 20);
            this.e.b.g().centerX = LatLongToPixels.f497a;
            this.e.b.g().centerY = LatLongToPixels.b;
            this.e.b.g().mapLevel = this.e.b.e();
            this.e.f.g = this.e.b.e();
            ArrayList a3 = this.e.f.a(this.e.f.j, this.e.f.g, this.e.b.c(), this.e.b.d());
            if (a3 != null && a3.size() > 0) {
                int size = (this.e.d == null || this.e.d.f475a == null) ? 0 : this.e.d.f475a.size();
                boolean z3 = true;
                for (int i = 0; i < size; i++) {
                    w wVar = (w) this.e.d.f475a.get(i);
                    if (wVar.f514a.equalsIgnoreCase("GridTmc") && wVar.f && (a2 = a(a3, wVar, this.e.b.e(), z2)) != null) {
                        a(a2, z3);
                        boolean z4 = z3 ? false : z3;
                        a2.clear();
                        z3 = z4;
                    }
                }
                a3.clear();
                this.e.b.g().invalidate();
            }
        }
    }

    public void a_() {
    }

    public void c() {
    }

    /* access modifiers changed from: protected */
    public int f() {
        return 4;
    }

    /* access modifiers changed from: protected */
    public int g() {
        return 1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amap.mapapi.map.bf.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.amap.mapapi.map.bf.a(java.util.ArrayList, boolean):void
      com.amap.mapapi.map.bf.a(java.util.ArrayList, java.net.Proxy):java.util.ArrayList
      com.amap.mapapi.map.c.a(java.util.ArrayList, java.net.Proxy):java.util.ArrayList
      com.amap.mapapi.map.bf.a(boolean, boolean):void */
    public void h() {
        a(false, false);
    }
}
