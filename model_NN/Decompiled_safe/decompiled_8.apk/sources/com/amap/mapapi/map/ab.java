package com.amap.mapapi.map;

import com.amap.mapapi.core.c;
import com.mapabc.minimap.map.vmap.NativeMapEngine;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.zip.GZIPInputStream;

class ab {

    /* renamed from: a  reason: collision with root package name */
    MapView f468a;
    public ArrayList b = new ArrayList();
    boolean c = false;
    long d;
    int e;
    byte[] f;
    int g = 0;
    int h = 0;
    boolean i = false;
    boolean j = false;

    public ab(MapView mapView) {
        this.f468a = mapView;
        this.d = System.currentTimeMillis();
    }

    private void d() {
        while (true) {
            if (this.h == 0) {
                if (this.g >= 8) {
                    this.h = c.a(this.f, 0) + 8;
                } else {
                    return;
                }
            } else if (this.g >= this.h) {
                int a2 = c.a(this.f, 0);
                int a3 = c.a(this.f, 4);
                if (a3 == 0) {
                    a(this.f, 8, a2);
                } else {
                    try {
                        GZIPInputStream gZIPInputStream = new GZIPInputStream(new ByteArrayInputStream(this.f, 8, a2));
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        byte[] bArr = new byte[NativeMapEngine.MAX_ICON_SIZE];
                        while (true) {
                            int read = gZIPInputStream.read(bArr);
                            if (read < 0) {
                                break;
                            }
                            byteArrayOutputStream.write(bArr, 0, read);
                        }
                        a(byteArrayOutputStream.toByteArray(), 0, a3);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
                c.a(this.f, this.h, this.f, 0, this.g - this.h);
                this.g -= this.h;
                this.h = 0;
            } else {
                return;
            }
        }
    }

    public String a() {
        StringBuffer stringBuffer = new StringBuffer();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.b.size()) {
                break;
            }
            stringBuffer.append(((String) this.b.get(i3)) + ";");
            i2 = i3 + 1;
        }
        if (stringBuffer.length() <= 0) {
            return null;
        }
        stringBuffer.deleteCharAt(stringBuffer.length() - 1);
        return "&cp=1&mesh=" + stringBuffer.toString();
    }

    public void a(ab abVar) {
        this.f = new byte[262144];
        this.h = 0;
        this.g = 0;
        this.i = false;
        a("连接打开成功...");
    }

    public void a(ab abVar, int i2, byte[] bArr, int i3) {
        System.arraycopy(bArr, 0, this.f, this.g, i3);
        this.g += i3;
        if (!this.i) {
            if (this.g <= 7) {
                return;
            }
            if (c.a(this.f, 0) != 0) {
                abVar.c = true;
                return;
            }
            c.a(this.f, 4);
            c.a(this.f, 8, this.f, 0, i3 - 8);
            this.g -= 8;
            this.h = 0;
            this.i = true;
            d();
        }
        d();
    }

    public void a(String str) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.<init>(byte[], int, int):void}
     arg types: [byte[], int, byte]
     candidates:
      ClspMth{java.lang.String.<init>(int[], int, int):void}
      ClspMth{java.lang.String.<init>(char[], int, int):void}
      ClspMth{java.lang.String.<init>(byte[], int, int):void} */
    /* access modifiers changed from: package-private */
    public void a(byte[] bArr, int i2, int i3) {
        c.b(this.f, i2);
        int i4 = i2 + 2;
        c.b(this.f, i4);
        int i5 = i4 + 2;
        c.a(this.f, i5);
        int i6 = i5 + 4;
        String str = new String(bArr, i6 + 1, (int) bArr[i6]);
        if (this.f468a.f != null) {
            this.f468a.f.putGridData(bArr, i2, i3 - i2);
            this.f468a.f.removeBitmapData(str, this.f468a.getGridLevelOff(str.length()));
            if (this.f468a.isGridInScreen(str)) {
                this.f468a.postInvalidate();
            }
        }
    }

    /* JADX WARN: Type inference failed for: r4v0 */
    /* JADX WARN: Type inference failed for: r4v1, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r4v2, types: [java.net.HttpURLConnection] */
    /* JADX WARN: Type inference failed for: r4v3 */
    /* JADX WARN: Type inference failed for: r4v4 */
    /* JADX WARN: Type inference failed for: r4v5 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00d5  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00e1 A[SYNTHETIC, Splitter:B:59:0x00e1] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00e6  */
    /* JADX WARNING: Removed duplicated region for block: B:86:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b() {
        /*
            r7 = this;
            r4 = 0
            r6 = 1
            r2 = 0
            r7.j = r6
            boolean r0 = r7.c()
            if (r0 != 0) goto L_0x0016
            com.amap.mapapi.map.MapView r0 = r7.f468a
            com.amap.mapapi.map.n r0 = r0.i
            r0.a()
            r7.b(r7)
        L_0x0015:
            return
        L_0x0016:
            r1 = r2
            r3 = r2
        L_0x0018:
            java.util.ArrayList r0 = r7.b
            int r0 = r0.size()
            if (r1 >= r0) goto L_0x0054
            java.util.ArrayList r0 = r7.b
            java.lang.Object r0 = r0.get(r1)
            java.lang.String r0 = (java.lang.String) r0
            com.amap.mapapi.map.MapView r5 = r7.f468a
            com.mapabc.minimap.map.vmap.NativeMapEngine r5 = r5.f
            if (r5 == 0) goto L_0x0052
            com.amap.mapapi.map.MapView r5 = r7.f468a
            com.mapabc.minimap.map.vmap.NativeMapEngine r5 = r5.f
            boolean r5 = r5.hasGridData(r0)
            if (r5 == 0) goto L_0x0052
            r5 = r6
        L_0x0039:
            if (r5 == 0) goto L_0x004b
            java.util.ArrayList r5 = r7.b
            r5.remove(r1)
            int r1 = r1 + -1
            int r3 = r3 + 1
            com.amap.mapapi.map.MapView r5 = r7.f468a
            com.amap.mapapi.map.aw r5 = r5.tileDownloadCtrl
            r5.a(r0)
        L_0x004b:
            r0 = r1
            r1 = r3
            int r0 = r0 + 1
            r3 = r1
            r1 = r0
            goto L_0x0018
        L_0x0052:
            r5 = r2
            goto L_0x0039
        L_0x0054:
            java.util.ArrayList r0 = r7.b
            int r0 = r0.size()
            if (r0 != 0) goto L_0x0060
            r7.b(r7)
            goto L_0x0015
        L_0x0060:
            if (r3 <= 0) goto L_0x0067
            com.amap.mapapi.map.MapView r0 = r7.f468a
            r0.postInvalidate()
        L_0x0067:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00f6, all -> 0x00da }
            r0.<init>()     // Catch:{ IOException -> 0x00f6, all -> 0x00da }
            java.lang.String r1 = r7.a()     // Catch:{ IOException -> 0x00f6, all -> 0x00da }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ IOException -> 0x00f6, all -> 0x00da }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x00f6, all -> 0x00da }
            com.amap.mapapi.map.MapView r1 = r7.f468a     // Catch:{ IOException -> 0x00f6, all -> 0x00da }
            java.net.HttpURLConnection r1 = r1.getConnection(r0)     // Catch:{ IOException -> 0x00f6, all -> 0x00da }
            if (r1 != 0) goto L_0x0089
            r7.b(r7)
            if (r1 == 0) goto L_0x0015
            r1.disconnect()
            goto L_0x0015
        L_0x0089:
            r0 = 15000(0x3a98, float:2.102E-41)
            r1.setConnectTimeout(r0)     // Catch:{ IOException -> 0x00f9, all -> 0x00f0 }
            java.lang.String r0 = "GET"
            r1.setRequestMethod(r0)     // Catch:{ IOException -> 0x00f9, all -> 0x00f0 }
            java.io.InputStream r0 = r1.getInputStream()     // Catch:{ IOException -> 0x00f9, all -> 0x00f0 }
            r7.a(r7)     // Catch:{ IOException -> 0x00c9, all -> 0x00f2 }
            r2 = 1024(0x400, float:1.435E-42)
            byte[] r2 = new byte[r2]     // Catch:{ IOException -> 0x00c9, all -> 0x00f2 }
        L_0x009e:
            int r3 = r0.read(r2)     // Catch:{ IOException -> 0x00c9, all -> 0x00f2 }
            if (r3 < 0) goto L_0x00b5
            boolean r4 = r7.c()     // Catch:{ IOException -> 0x00c9, all -> 0x00f2 }
            if (r4 == 0) goto L_0x00ae
            boolean r4 = r7.c     // Catch:{ IOException -> 0x00c9, all -> 0x00f2 }
            if (r4 == 0) goto L_0x00c4
        L_0x00ae:
            com.amap.mapapi.map.MapView r2 = r7.f468a     // Catch:{ IOException -> 0x00c9, all -> 0x00f2 }
            com.amap.mapapi.map.n r2 = r2.i     // Catch:{ IOException -> 0x00c9, all -> 0x00f2 }
            r2.a()     // Catch:{ IOException -> 0x00c9, all -> 0x00f2 }
        L_0x00b5:
            r7.b(r7)
            if (r0 == 0) goto L_0x00bd
            r0.close()     // Catch:{ IOException -> 0x00ea }
        L_0x00bd:
            if (r1 == 0) goto L_0x0015
            r1.disconnect()
            goto L_0x0015
        L_0x00c4:
            r4 = 0
            r7.a(r7, r4, r2, r3)     // Catch:{ IOException -> 0x00c9, all -> 0x00f2 }
            goto L_0x009e
        L_0x00c9:
            r2 = move-exception
            r4 = r1
        L_0x00cb:
            r7.b(r7)
            if (r0 == 0) goto L_0x00d3
            r0.close()     // Catch:{ IOException -> 0x00ec }
        L_0x00d3:
            if (r4 == 0) goto L_0x0015
            r4.disconnect()
            goto L_0x0015
        L_0x00da:
            r0 = move-exception
            r1 = r4
        L_0x00dc:
            r7.b(r7)
            if (r4 == 0) goto L_0x00e4
            r4.close()     // Catch:{ IOException -> 0x00ee }
        L_0x00e4:
            if (r1 == 0) goto L_0x00e9
            r1.disconnect()
        L_0x00e9:
            throw r0
        L_0x00ea:
            r0 = move-exception
            goto L_0x00bd
        L_0x00ec:
            r0 = move-exception
            goto L_0x00d3
        L_0x00ee:
            r2 = move-exception
            goto L_0x00e4
        L_0x00f0:
            r0 = move-exception
            goto L_0x00dc
        L_0x00f2:
            r2 = move-exception
            r4 = r0
            r0 = r2
            goto L_0x00dc
        L_0x00f6:
            r0 = move-exception
            r0 = r4
            goto L_0x00cb
        L_0x00f9:
            r0 = move-exception
            r0 = r4
            r4 = r1
            goto L_0x00cb
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.mapapi.map.ab.b():void");
    }

    public void b(ab abVar) {
        int i2 = 0;
        this.f = null;
        this.h = 0;
        this.g = 0;
        a((String) null);
        while (true) {
            int i3 = i2;
            if (i3 >= abVar.b.size()) {
                break;
            }
            this.f468a.tileDownloadCtrl.a((String) abVar.b.get(i3));
            i2 = i3 + 1;
        }
        if (this.f468a.i.f509a.b() == this) {
            this.f468a.i.f509a.a();
        }
        this.f468a.postInvalidate();
    }

    public void b(String str) {
        this.b.add(str);
    }

    public boolean c() {
        if (this.e != this.f468a.mapLevel) {
            return false;
        }
        return this.f468a.isAGridsInScreen(this.b);
    }
}
