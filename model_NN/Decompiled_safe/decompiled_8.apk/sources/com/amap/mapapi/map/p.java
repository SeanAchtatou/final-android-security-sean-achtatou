package com.amap.mapapi.map;

import android.graphics.Canvas;

abstract class p extends Overlay {
    p() {
    }

    public abstract void b();

    public abstract boolean draw(Canvas canvas, MapView mapView, boolean z, long j);
}
