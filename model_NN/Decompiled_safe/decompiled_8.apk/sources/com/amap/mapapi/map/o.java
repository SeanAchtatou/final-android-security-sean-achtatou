package com.amap.mapapi.map;

import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import com.amap.mapapi.core.GeoPoint;

class o extends al {
    private Drawable d;
    private RouteMessageHandler e;
    private boolean f;
    private boolean g;
    private int h;
    private int i;
    private boolean j = false;

    public o(RouteOverlay routeOverlay, int i2, GeoPoint geoPoint, Drawable drawable, RouteMessageHandler routeMessageHandler, boolean z) {
        super(routeOverlay, i2, geoPoint);
        this.d = drawable;
        this.e = routeMessageHandler;
        this.f = false;
        this.g = false;
        Rect bounds = this.d.getBounds();
        this.h = (int) (((double) bounds.width()) * 1.5d);
        this.i = (int) (((double) bounds.height()) * 1.5d);
        this.j = z;
    }

    private boolean a(MapView mapView, int i2, int i3) {
        Point a2 = a(mapView, this.b);
        Rect bounds = this.d.getBounds();
        if (!this.j) {
            return bounds.contains(i2 - a2.x, i3 - a2.y);
        }
        return Math.abs(i2 - a2.x) * 2 < bounds.width() / 2 && a2.y - i3 > 0 && a2.y - i3 < bounds.height();
    }

    private boolean b(MapView mapView, Point point) {
        Point a2 = a(mapView, this.b);
        return ((a2.y - point.y) * (a2.y - point.y)) + ((a2.x - point.x) * (a2.x - point.x)) > this.h * this.i;
    }

    public void a(Canvas canvas, MapView mapView, boolean z) {
        if (this.d != null && !z) {
            Point a2 = a(mapView, this.b);
            Overlay.a(canvas, this.d, a2.x, a2.y);
        }
    }

    public void a(GeoPoint geoPoint) {
        this.b = geoPoint;
    }

    public boolean a(MotionEvent motionEvent, MapView mapView) {
        Point point = new Point((int) motionEvent.getX(), (int) motionEvent.getY());
        if (motionEvent.getAction() == 0 && a(mapView, point.x, point.y)) {
            this.f = true;
            return true;
        } else if (motionEvent.getAction() == 2) {
            if (this.g) {
                if (!b(mapView, point)) {
                    return true;
                }
                this.e.onDrag(mapView, this.c, this.f484a, a(mapView, point));
                return true;
            } else if (!this.f) {
                return false;
            } else {
                if (!b(mapView, point)) {
                    return true;
                }
                this.g = true;
                this.e.onDragBegin(mapView, this.c, this.f484a, a(mapView, point));
                return true;
            }
        } else if (motionEvent.getAction() != 1 || !this.f) {
            return false;
        } else {
            this.f = false;
            if (this.g) {
                this.g = false;
                this.e.onDragEnd(mapView, this.c, this.f484a, a(mapView, point));
                return true;
            }
            this.e.onRouteEvent(mapView, this.c, this.f484a, 4);
            return true;
        }
    }
}
