package com.amap.mapapi.map;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

class r extends a {
    private Bitmap[] e;
    private Rect f = new Rect(0, 0, this.e[0].getWidth(), this.e[0].getHeight());
    private int g = 0;
    private ah h;

    public r(int i, int i2, ah ahVar, Bitmap[] bitmapArr) {
        super(i, i2);
        this.e = bitmapArr;
        this.h = ahVar;
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.g++;
        if (this.g >= this.e.length) {
            this.g = 0;
        }
        this.h.d.b(this.f.left, this.f.top, this.f.right, this.f.bottom);
    }

    public void a(Canvas canvas, int i, int i2) {
        int width = this.f.width() / 2;
        int height = this.f.height() / 2;
        this.f.set(i - width, i2 - height, width + i, height + i2);
        this.g++;
        if (this.g >= this.e.length) {
            this.g = 0;
        }
        canvas.drawBitmap(this.e[this.g], (float) this.f.left, (float) this.f.top, (Paint) null);
    }

    /* access modifiers changed from: protected */
    public void b() {
    }

    public int h() {
        return this.e[0].getWidth();
    }

    public int i() {
        return this.e[0].getHeight();
    }
}
