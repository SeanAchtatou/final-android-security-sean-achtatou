package com.amap.mapapi.map;

import com.amap.mapapi.core.GeoPoint;

class bb extends a {
    private GeoPoint e;
    private GeoPoint f;
    private int g = ((int) this.e.c());
    private int h = ((int) this.e.d());
    private int i;
    private int j;
    private int k;
    private int l;
    private int m;
    private int n;
    private bc o;

    public bb(int i2, int i3, GeoPoint geoPoint, GeoPoint geoPoint2, int i4, bc bcVar) {
        super(i2, i3);
        this.e = geoPoint;
        this.f = geoPoint2;
        this.o = bcVar;
        this.k = (int) Math.abs(geoPoint2.c() - this.e.c());
        this.l = (int) Math.abs(geoPoint2.d() - this.e.d());
        this.m = 7;
        a(i4);
    }

    private int a(int i2, int i3, int i4) {
        int i5;
        if (i3 > i2) {
            i5 = i2 + i4;
            if (i5 >= i3) {
                this.n = 0;
                return i3;
            }
        } else {
            i5 = i2 - i4;
            if (i5 <= i3) {
                this.n = 0;
                return i3;
            }
        }
        return i5;
    }

    private void a(int i2) {
        this.i = this.k / this.m;
        this.j = this.l / this.m;
    }

    /* access modifiers changed from: protected */
    public void a() {
        int c = (int) this.f.c();
        int d = (int) this.f.d();
        if (!f()) {
            this.g = c;
            this.h = d;
            this.o.a(new GeoPoint((double) this.h, (double) this.g, false));
            return;
        }
        this.n++;
        this.i += this.n * (this.n + 1);
        this.j += this.n * (this.n + 1);
        this.g = a(this.g, c, this.i);
        this.h = a(this.h, d, this.j);
        this.o.a(new GeoPoint((double) this.h, (double) this.g, false));
    }

    /* access modifiers changed from: protected */
    public void b() {
        this.o.b();
    }
}
