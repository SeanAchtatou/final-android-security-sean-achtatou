package com.amap.mapapi.map;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class i {

    /* renamed from: a  reason: collision with root package name */
    public static int f505a = 2048;

    public static byte[] a(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[f505a];
        while (true) {
            int read = inputStream.read(bArr, 0, f505a);
            if (read == -1) {
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }
}
