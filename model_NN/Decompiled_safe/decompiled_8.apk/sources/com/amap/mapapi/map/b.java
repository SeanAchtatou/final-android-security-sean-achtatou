package com.amap.mapapi.map;

import android.os.Handler;

class b implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f495a;

    b(a aVar) {
        this.f495a = aVar;
    }

    public void run() {
        this.f495a.e();
        if (!this.f495a.f()) {
            this.f495a.e.removeCallbacks(this);
            Handler unused = this.f495a.e = null;
            this.f495a.b();
            return;
        }
        long currentTimeMillis = System.currentTimeMillis();
        this.f495a.a();
        this.f495a.g();
        long currentTimeMillis2 = System.currentTimeMillis();
        if (currentTimeMillis2 - currentTimeMillis < ((long) this.f495a.d)) {
            try {
                Thread.sleep(((long) this.f495a.d) - (currentTimeMillis2 - currentTimeMillis));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
