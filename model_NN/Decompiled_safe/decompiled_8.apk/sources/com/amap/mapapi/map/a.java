package com.amap.mapapi.map;

import android.os.Handler;
import android.os.Looper;

abstract class a {

    /* renamed from: a  reason: collision with root package name */
    protected int f466a = 0;
    public boolean b = false;
    protected int c;
    protected int d;
    /* access modifiers changed from: private */
    public Handler e = null;
    private Runnable f = new b(this);

    public a(int i, int i2) {
        this.c = i;
        this.d = i2;
    }

    private void h() {
        this.b = false;
    }

    /* access modifiers changed from: protected */
    public abstract void a();

    /* access modifiers changed from: protected */
    public abstract void b();

    public void c() {
        if (!f()) {
            this.e = new Handler(Looper.getMainLooper());
            this.b = true;
            this.f466a = 0;
        }
        g();
    }

    public void d() {
        this.b = false;
        this.f.run();
    }

    /* access modifiers changed from: protected */
    public void e() {
        this.f466a += this.d;
        if (this.c != -1 && this.f466a > this.c) {
            this.b = false;
        }
    }

    public boolean f() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public void g() {
        this.e.post(this.f);
    }
}
