package com.amap.mapapi.map;

class at {

    /* renamed from: a  reason: collision with root package name */
    private Thread[] f490a;

    public at(int i, Runnable runnable, Runnable runnable2) {
        this.f490a = new Thread[i];
        for (int i2 = 0; i2 < i; i2++) {
            if (i2 != 0 || i <= 1) {
                this.f490a[i2] = new Thread(runnable2);
            } else {
                this.f490a[i2] = new Thread(runnable);
            }
        }
    }

    public void a() {
        for (Thread thread : this.f490a) {
            thread.setDaemon(true);
            thread.start();
        }
    }

    public void b() {
        int length = this.f490a.length;
        if (length > 1) {
            try {
                this.f490a[0].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        for (int i = 1; i < length; i++) {
            try {
                this.f490a[i].join(300);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        }
    }

    public void c() {
        if (this.f490a != null) {
            int length = this.f490a.length;
            for (int i = 0; i < length; i++) {
                this.f490a[i].interrupt();
                this.f490a[i] = null;
            }
            this.f490a = null;
        }
    }
}
