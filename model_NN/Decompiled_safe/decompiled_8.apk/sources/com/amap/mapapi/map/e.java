package com.amap.mapapi.map;

import com.amap.mapapi.core.AMapException;
import java.util.ArrayList;
import java.util.List;

class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f500a;

    e(c cVar) {
        this.f500a = cVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amap.mapapi.map.as.a(int, boolean):java.util.ArrayList
     arg types: [int, int]
     candidates:
      com.amap.mapapi.map.as.a(java.util.List, boolean):void
      com.amap.mapapi.map.as.a(int, boolean):java.util.ArrayList */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amap.mapapi.map.as.a(java.util.List, boolean):void
     arg types: [java.util.ArrayList, int]
     candidates:
      com.amap.mapapi.map.as.a(int, boolean):java.util.ArrayList
      com.amap.mapapi.map.as.a(java.util.List, boolean):void */
    public void run() {
        ArrayList arrayList = null;
        this.f500a.b.add(Thread.currentThread());
        ArrayList arrayList2 = null;
        while (this.f500a.f498a) {
            if (this.f500a.e == null) {
                this.f500a.f498a = false;
            } else {
                if (this.f500a.c != null) {
                    arrayList2 = this.f500a.c.a(this.f500a.g(), true);
                }
                if (arrayList2 == null || arrayList2.size() != 0) {
                    if (this.f500a.f498a) {
                        try {
                            arrayList = this.f500a.a(arrayList2);
                        } catch (AMapException e) {
                            e.printStackTrace();
                        }
                        if (!(arrayList == null || this.f500a.c == null)) {
                            this.f500a.c.a((List) arrayList, false);
                        }
                        if (this.f500a.f498a) {
                            try {
                                Thread.sleep(50);
                            } catch (Exception e2) {
                                Thread.currentThread().interrupt();
                            }
                        }
                    } else {
                        return;
                    }
                }
            }
        }
    }
}
