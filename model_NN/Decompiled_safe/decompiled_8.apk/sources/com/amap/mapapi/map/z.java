package com.amap.mapapi.map;

import android.os.Handler;
import android.os.Message;

class z extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MapActivity f516a;

    z(MapActivity mapActivity) {
        this.f516a = mapActivity;
    }

    public void handleMessage(Message message) {
        ah a2;
        switch (message.what) {
            case 1:
                int size = this.f516a.f.size();
                for (int i = 0; i < size; i++) {
                    MapView mapView = (MapView) this.f516a.f.get(i);
                    if (!(mapView == null || (a2 = mapView.a()) == null)) {
                        a2.c.b();
                    }
                }
                return;
            default:
                return;
        }
    }
}
