package com.amap.mapapi.map;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import com.amap.mapapi.core.GeoPoint;
import java.util.ArrayList;

class y extends aq {

    /* renamed from: a  reason: collision with root package name */
    private Paint f515a;
    private GeoPoint[] b;
    private Path d;

    y(RouteOverlay routeOverlay, GeoPoint[] geoPointArr, Paint paint) {
        super(routeOverlay);
        this.f515a = paint;
        this.b = geoPointArr;
    }

    private int a(MapView mapView, ArrayList arrayList) {
        Point a2 = RouteOverlay.a(mapView, this.b[0]);
        int i = 0;
        while (i < this.b.length - 1) {
            i++;
            Point a3 = RouteOverlay.a(mapView, this.b[i]);
            if (arrayList.size() == 0) {
                arrayList.add(a2);
                arrayList.add(a3);
            } else if (a(a2, a3)) {
                arrayList.set(arrayList.size() - 1, a3);
            } else {
                arrayList.add(a3);
            }
            a2 = a3;
        }
        if (arrayList.size() > 2 && a((Point) arrayList.get(0), (Point) arrayList.get(1))) {
            arrayList.remove(1);
        }
        return i;
    }

    private void a(Canvas canvas, MapView mapView, ArrayList arrayList) {
        boolean z;
        if (this.d == null) {
            this.d = new Path();
        }
        boolean z2 = true;
        int size = arrayList.size();
        int i = 0;
        while (i < size) {
            Point point = (Point) arrayList.get(i);
            if (z2) {
                this.d.moveTo((float) point.x, (float) point.y);
                z = false;
            } else {
                this.d.lineTo((float) point.x, (float) point.y);
                z = z2;
            }
            i++;
            z2 = z;
        }
        canvas.drawPath(this.d, this.f515a);
        this.d.reset();
    }

    private boolean a(Point point, Point point2) {
        return Math.abs(point.x - point2.x) <= 2 && Math.abs(point.y - point2.y) <= 2;
    }

    public void a(Canvas canvas, MapView mapView, boolean z) {
        if (!z) {
            ArrayList arrayList = new ArrayList();
            a(mapView, arrayList);
            if (arrayList.size() > 0) {
                a(canvas, mapView, arrayList);
                arrayList.clear();
            }
        }
    }
}
