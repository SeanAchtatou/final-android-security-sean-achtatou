package com.amap.mapapi.offlinemap;

import com.amap.mapapi.poisearch.PoiTypeDef;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

public class a {

    /* renamed from: a  reason: collision with root package name */
    RandomAccessFile f519a;
    long b;

    public a() {
        this(PoiTypeDef.All, 0);
    }

    public a(String str, long j) {
        File file = new File(str);
        if (!file.exists()) {
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            try {
                if (!file.exists()) {
                    file.createNewFile();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.f519a = new RandomAccessFile(str, "rw");
        this.b = j;
        this.f519a.seek(j);
    }

    public synchronized int a(byte[] bArr, int i, int i2) {
        try {
            this.f519a.write(bArr, i, i2);
        } catch (IOException e) {
            e.printStackTrace();
            i2 = -1;
        }
        return i2;
    }
}
