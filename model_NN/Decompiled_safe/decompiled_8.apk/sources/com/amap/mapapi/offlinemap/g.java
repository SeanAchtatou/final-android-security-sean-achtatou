package com.amap.mapapi.offlinemap;

import com.amap.mapapi.poisearch.PoiTypeDef;
import com.sina.sdk.api.message.InviteApi;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;

public class g {

    /* renamed from: a  reason: collision with root package name */
    public int f525a = 3;
    public String b = null;
    public String c = PoiTypeDef.All;
    public String d = PoiTypeDef.All;
    long e = 0;
    long f = 0;
    long g = 0;
    private String h = null;
    private String i = null;
    private String j = null;
    private String k = null;
    private String l = null;
    private int m;

    public g() {
    }

    public g(i iVar) {
        this.k = iVar.getPinyin();
        this.b = iVar.e;
        this.h = iVar.getCity();
        this.j = iVar.getCode();
        this.i = iVar.f526a;
        this.f = iVar.b;
        String a2 = c.a();
        this.l = a2 + this.j + ".zip.tmp";
        try {
            if (!new File(a2 + this.j).exists() && !new File(a2 + this.j + ".zip.tmp").exists()) {
                new File(this.l).createNewFile();
            }
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        this.c = iVar.d;
    }

    public String a() {
        return this.l;
    }

    public void a(int i2) {
        this.m = i2;
    }

    public String b() {
        return this.j;
    }

    public String c() {
        return this.i;
    }

    public int d() {
        return this.m;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
    public void e() {
        JSONObject jSONObject = new JSONObject();
        try {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("title", this.h);
            jSONObject2.put("code", this.j);
            jSONObject2.put(InviteApi.KEY_URL, this.i);
            jSONObject2.put("pinyin", this.k);
            jSONObject2.put("jianpin", this.b);
            jSONObject2.put("fileName", this.l);
            jSONObject2.put("lLocalLength", this.e);
            jSONObject2.put("lRemoteLength", this.f);
            jSONObject2.put("mState", this.f525a);
            jSONObject2.put("Schedule", this.g);
            jSONObject2.put("version", this.c);
            jSONObject.put("file", jSONObject2);
            File file = new File(this.l + ".dt");
            file.delete();
            try {
                FileWriter fileWriter = new FileWriter(file, true);
                fileWriter.write(jSONObject.toString());
                fileWriter.close();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        } catch (JSONException e3) {
            e3.printStackTrace();
        }
    }
}
