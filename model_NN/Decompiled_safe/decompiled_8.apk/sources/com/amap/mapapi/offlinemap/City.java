package com.amap.mapapi.offlinemap;

public class City implements Comparable {

    /* renamed from: a  reason: collision with root package name */
    private String f517a;
    private String b;
    private String c;
    private String d;
    private String e;

    public City(String str, String str2, String str3, String str4, String str5) {
        setProvince(str);
        setCity(str2);
        setInitial(str4);
        this.c = str3;
        setPinyin(str5);
    }

    public int compareTo(Object obj) {
        String str = ((City) obj).d;
        if (str.charAt(0) > this.d.charAt(0)) {
            return -1;
        }
        return str.charAt(0) < this.d.charAt(0) ? 1 : 0;
    }

    public String getCity() {
        return this.b;
    }

    public String getCode() {
        return this.c;
    }

    public String getInitial() {
        return this.d;
    }

    public String getPinyin() {
        return this.e;
    }

    public String getProvince() {
        return this.f517a;
    }

    public void setCity(String str) {
        this.b = str;
    }

    public void setCode(String str) {
        this.c = str;
    }

    public void setInitial(String str) {
        this.d = str;
    }

    public void setPinyin(String str) {
        this.e = str;
    }

    public void setProvince(String str) {
        this.f517a = str;
    }
}
