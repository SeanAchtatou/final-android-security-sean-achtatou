package com.amap.mapapi.offlinemap;

import android.content.Context;
import android.os.Handler;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONObject;

public final class OfflineMapManager {

    /* renamed from: a  reason: collision with root package name */
    c f518a;
    OfflineMapDownloadListener b;
    ArrayList c = new ArrayList();
    Handler d = new d(this);

    public interface OfflineMapDownloadListener {
        void onDownload(int i, int i2);
    }

    public OfflineMapManager(Context context, OfflineMapDownloadListener offlineMapDownloadListener) {
        this.f518a = new c(context, this.d);
        this.b = offlineMapDownloadListener;
        a(context);
    }

    private void a(Context context) {
        try {
            InputStreamReader inputStreamReader = new InputStreamReader(context.getAssets().open("1010.png"));
            StringBuffer stringBuffer = new StringBuffer();
            char[] cArr = new char[1024];
            while (inputStreamReader.read(cArr) > 0) {
                stringBuffer.append(cArr);
            }
            inputStreamReader.close();
            this.f518a.a(new JSONObject(stringBuffer.toString()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void a(i iVar) {
        this.c.add(iVar);
        g gVar = new g(iVar);
        gVar.a(this.f518a.b.size());
        gVar.f525a = 2;
        this.f518a.b.add(gVar);
        this.f518a.a(this.f518a.b.size() - 1);
    }

    public final boolean downloadByCityCode(String str) {
        i itemByCityCode = getItemByCityCode(str);
        if (itemByCityCode == null) {
            return false;
        }
        a(itemByCityCode);
        return true;
    }

    public final boolean downloadByCityName(String str) {
        i itemByCityName = getItemByCityName(str);
        if (itemByCityName == null) {
            return false;
        }
        a(itemByCityName);
        return true;
    }

    public final ArrayList getDownloadingCityList() {
        return this.c;
    }

    public final i getItemByCityCode(String str) {
        Iterator it = this.f518a.c.iterator();
        while (it.hasNext()) {
            i iVar = (i) it.next();
            if (iVar.getCode().equals(str)) {
                return iVar;
            }
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:3:0x000e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.amap.mapapi.offlinemap.i getItemByCityName(java.lang.String r5) {
        /*
            r4 = this;
            com.amap.mapapi.offlinemap.c r0 = r4.f518a
            java.util.ArrayList r0 = r0.c
            java.util.Iterator r1 = r0.iterator()
        L_0x0008:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0025
            java.lang.Object r0 = r1.next()
            com.amap.mapapi.offlinemap.i r0 = (com.amap.mapapi.offlinemap.i) r0
            java.lang.String r2 = r0.getCity()
            boolean r3 = r2.contains(r5)
            if (r3 != 0) goto L_0x0024
            boolean r2 = r5.contains(r2)
            if (r2 == 0) goto L_0x0008
        L_0x0024:
            return r0
        L_0x0025:
            r0 = 0
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.mapapi.offlinemap.OfflineMapManager.getItemByCityName(java.lang.String):com.amap.mapapi.offlinemap.i");
    }

    public final ArrayList getOfflineCityList() {
        return this.f518a.d;
    }

    public final void getUpdateInfo(String str) {
    }

    public final void pause() {
        this.f518a.b(0);
    }

    public final void remove(String str) {
        i itemByCityName = getItemByCityName(str);
        if (itemByCityName != null) {
            this.f518a.a(new g(itemByCityName));
        }
    }

    public final void restart() {
        this.f518a.a(this.f518a.b.size() - 1);
    }

    public final void stop() {
        this.f518a.b();
    }
}
