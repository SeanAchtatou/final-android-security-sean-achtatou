package com.amap.mapapi.offlinemap;

import com.amap.mapapi.poisearch.PoiTypeDef;

public class f {

    /* renamed from: a  reason: collision with root package name */
    private String f524a;
    private String b;
    private String c;
    private int d;

    public f() {
        this(PoiTypeDef.All, PoiTypeDef.All, PoiTypeDef.All, 5);
    }

    public f(String str, String str2, String str3, int i) {
        this.f524a = str;
        this.b = str2;
        this.c = str3;
        this.d = i;
    }

    public String a() {
        return this.f524a;
    }

    public String b() {
        return this.b;
    }

    public String c() {
        return this.c;
    }

    public int d() {
        return this.d;
    }
}
