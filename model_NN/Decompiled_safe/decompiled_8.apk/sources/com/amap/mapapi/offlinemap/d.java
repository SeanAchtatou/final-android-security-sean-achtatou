package com.amap.mapapi.offlinemap;

import android.os.Handler;
import android.os.Message;
import com.amap.mapapi.location.LocationManagerProxy;

class d extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ OfflineMapManager f522a;

    d(OfflineMapManager offlineMapManager) {
        this.f522a = offlineMapManager;
    }

    public void handleMessage(Message message) {
        this.f522a.b.onDownload(message.getData().getInt(LocationManagerProxy.KEY_STATUS_CHANGED), message.getData().getInt("completepercent"));
    }
}
