package com.amap.mapapi.geocoder;

import android.content.Context;
import android.location.Address;
import com.amap.mapapi.core.AMapException;
import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.core.a;
import com.amap.mapapi.core.d;
import com.amap.mapapi.core.h;
import com.amap.mapapi.core.o;
import com.amap.mapapi.core.p;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class Geocoder {
    public static final String Cross = "Cross";
    public static final String POI = "POI";
    public static final String Street_Road = "StreetAndRoad";

    /* renamed from: a  reason: collision with root package name */
    private String f434a;
    private Context b;

    public Geocoder(Context context) {
        a.a(context);
        a(context, d.a(context));
    }

    public Geocoder(Context context, String str) {
        a.a(context);
        a(context, d.a(context));
    }

    private List a(double d, double d2, int i, boolean z) {
        if (d.f424a) {
            if (d < d.a(1000000L) || d > d.a(65000000L)) {
                throw new AMapException("无效的参数 - IllegalArgumentException latitude == " + d);
            } else if (d2 < d.a(50000000L) || d2 > d.a(145000000L)) {
                throw new AMapException("无效的参数 - IllegalArgumentException longitude == " + d2);
            }
        }
        return i <= 0 ? new ArrayList() : (List) new o(new p(d2, d, i, z), d.b(this.b), this.f434a, null).j();
    }

    private List a(List list, double d, double d2, double d3, double d4, int i) {
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            Address address = (Address) it.next();
            double longitude = address.getLongitude();
            double latitude = address.getLatitude();
            if (longitude <= d4 && longitude >= d2 && latitude <= d3 && latitude >= d && arrayList.size() < i) {
                arrayList.add(address);
            }
        }
        return arrayList;
    }

    private void a(Context context, String str) {
        this.b = context;
        this.f434a = str;
    }

    public final List getFromLocation(double d, double d2, int i) {
        return a(d, d2, i, false);
    }

    public final List getFromLocationName(String str, int i) {
        return getFromLocationName(str, i, d.a(1000000L), d.a(50000000L), d.a(65000000L), d.a(145000000L));
    }

    public final List getFromLocationName(String str, int i, double d, double d2, double d3, double d4) {
        if (str == null) {
            throw new IllegalArgumentException("locationName == null");
        }
        if (d.f424a) {
            if (d < d.a(1000000L) || d > d.a(65000000L)) {
                throw new AMapException("无效的参数 - IllegalArgumentException lowerLeftLatitude == " + d);
            } else if (d2 < d.a(50000000L) || d2 > d.a(145000000L)) {
                throw new AMapException("无效的参数 - IllegalArgumentException lowerLeftLongitude == " + d2);
            } else if (d3 < d.a(1000000L) || d3 > d.a(65000000L)) {
                throw new AMapException("无效的参数 - IllegalArgumentException upperRightLatitude == " + d3);
            } else if (d4 < d.a(50000000L) || d4 > d.a(145000000L)) {
                throw new AMapException("无效的参数 - IllegalArgumentException upperRightLongitude == " + d4);
            }
        }
        if (i <= 0) {
            return new ArrayList();
        }
        List list = (List) new a(new b(str, 15), d.b(this.b), this.f434a, null).j();
        return d.f424a ? a(list, d, d2, d3, d4, i) : list;
    }

    public final List getFromRawGpsLocation(double d, double d2, int i) {
        double d3;
        double d4;
        try {
            GeoPoint.b bVar = (GeoPoint.b) new h(new GeoPoint.b(d2, d), d.b(this.b), this.f434a, null).j();
            if (bVar != null) {
                d3 = bVar.f419a;
                d4 = bVar.b;
            } else {
                d3 = d2;
                d4 = d;
            }
            if (Double.valueOf(0.0d).doubleValue() == d3 && Double.valueOf(0.0d).doubleValue() == d4) {
                return null;
            }
            return a(d4, d3, i, false);
        } catch (Exception e) {
            return new ArrayList();
        }
    }
}
