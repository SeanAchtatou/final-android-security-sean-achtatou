package com.amap.mapapi.geocoder;

import android.location.Address;
import android.text.TextUtils;
import com.amap.mapapi.core.d;
import com.amap.mapapi.core.t;
import com.amap.mapapi.map.i;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.Proxy;
import java.net.URLEncoder;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Node;

class a extends t {
    public int i = 0;

    public a(b bVar, Proxy proxy, String str, String str2) {
        super(bVar, proxy, str, str2);
        this.i = bVar.b;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ArrayList c(InputStream inputStream) {
        String str;
        if (e()) {
            return super.c(inputStream);
        }
        ArrayList arrayList = new ArrayList();
        try {
            str = new String(i.a(inputStream));
        } catch (Exception e) {
            e.printStackTrace();
            str = null;
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.getInt("count") <= 0) {
                return arrayList;
            }
            JSONArray jSONArray = jSONObject.getJSONArray("list");
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
                Address b = d.b();
                String string = jSONObject2.getString("name");
                if (!TextUtils.isEmpty(string)) {
                    b.setFeatureName(string);
                }
                String string2 = jSONObject2.getString("address");
                if (!TextUtils.isEmpty(string2)) {
                    b.setAddressLine(2, string2);
                }
                String string3 = jSONObject2.getString("province");
                if (!TextUtils.isEmpty(string3)) {
                    b.setAdminArea(string3);
                }
                String string4 = jSONObject2.getString("city");
                if (TextUtils.isEmpty(string4)) {
                    string4 = string3;
                }
                b.setLocality(string4);
                String string5 = jSONObject2.getString("district");
                if (!TextUtils.isEmpty(string5)) {
                    try {
                        Method method = b.getClass().getMethod("setSubLocality", String.class);
                        if (method != null) {
                            method.invoke(b, string5);
                        }
                    } catch (Exception e2) {
                    }
                }
                b.setAddressLine(0, "中国");
                if (!string3.equals(string4)) {
                    b.setAddressLine(1, string3 + string4 + string5);
                } else {
                    b.setAddressLine(1, string4 + string5);
                }
                String string6 = jSONObject2.getString("x");
                if (!TextUtils.isEmpty(string6)) {
                    b.setLongitude(Double.parseDouble(string6));
                }
                String string7 = jSONObject2.getString("y");
                if (!TextUtils.isEmpty(string7)) {
                    b.setLatitude(Double.parseDouble(string7));
                }
                arrayList.add(b);
            }
            return arrayList;
        } catch (JSONException e3) {
            e3.printStackTrace();
            return arrayList;
        }
    }

    /* access modifiers changed from: protected */
    public void a(ArrayList arrayList) {
    }

    /* access modifiers changed from: protected */
    public void a(Node node, ArrayList arrayList) {
    }

    /* access modifiers changed from: protected */
    public boolean e() {
        return false;
    }

    /* access modifiers changed from: protected */
    public String[] f() {
        String[] strArr = new String[3];
        strArr[0] = "&enc=utf-8&ver=2.0";
        String str = ((b) this.b).f435a;
        try {
            str = URLEncoder.encode(str, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        strArr[1] = "&address=" + str;
        strArr[2] = "&poiNumber=" + ((b) this.b).b;
        return strArr;
    }

    /* access modifiers changed from: protected */
    public byte[] g() {
        StringBuilder sb = new StringBuilder();
        sb.append("config=GOC&resType=json&enc=utf-8&ver=2.0&address=");
        String str = ((b) this.b).f435a;
        try {
            str = URLEncoder.encode(str, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        sb.append(str);
        sb.append("&poiNumber=");
        sb.append(((b) this.b).b);
        com.amap.mapapi.core.a a2 = com.amap.mapapi.core.a.a(null);
        sb.append("&a_k=");
        sb.append(a2.a());
        return sb.toString().getBytes();
    }

    /* access modifiers changed from: protected */
    public String h() {
        return e() ? com.amap.mapapi.core.i.a().d() + "?&config=GOC&resType=xml" : com.amap.mapapi.core.i.a().d();
    }
}
