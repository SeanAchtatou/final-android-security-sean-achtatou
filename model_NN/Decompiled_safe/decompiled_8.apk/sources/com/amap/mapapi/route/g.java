package com.amap.mapapi.route;

import java.net.Proxy;
import java.util.ArrayList;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

class g extends d {
    public g(f fVar, Proxy proxy, String str, String str2) {
        super(fVar, proxy, str, str2);
    }

    private String a(String str) {
        if (str == null || !str.endsWith("行驶")) {
            return str;
        }
        char[] charArray = str.toCharArray();
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < charArray.length; i++) {
            if (!"向".equals(new StringBuilder().append(charArray[i]).toString()) && !"行".equals(new StringBuilder().append(charArray[i]).toString()) && !"驶".equals(new StringBuilder().append(charArray[i]).toString())) {
                stringBuffer.append(charArray[i]);
            }
        }
        return stringBuffer.toString();
    }

    /* access modifiers changed from: protected */
    public void a(ArrayList arrayList) {
    }

    /* access modifiers changed from: protected */
    public void a(Node node, ArrayList arrayList) {
    }

    /* access modifiers changed from: protected */
    public Segment b(Node node) {
        NodeList childNodes = node.getChildNodes();
        DriveWalkSegment driveWalkSegment = new DriveWalkSegment();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node item = childNodes.item(i);
            String nodeName = item.getNodeName();
            if (nodeName.equals("roadName")) {
                driveWalkSegment.setRoadName(a(item));
            } else if (nodeName.equals("roadLength")) {
                driveWalkSegment.setLength(Integer.parseInt(a(item)));
            } else if (nodeName.equals("action")) {
                driveWalkSegment.setActionDescription(a(a(item)));
            } else if (nodeName.equals("coor")) {
                driveWalkSegment.setShapes(a(a(item).split(",")));
            }
        }
        return driveWalkSegment;
    }

    /* access modifiers changed from: protected */
    public boolean e() {
        return false;
    }

    /* access modifiers changed from: protected */
    public byte[] g() {
        return null;
    }
}
