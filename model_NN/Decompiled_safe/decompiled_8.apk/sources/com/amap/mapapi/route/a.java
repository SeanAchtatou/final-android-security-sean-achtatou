package com.amap.mapapi.route;

import com.amap.mapapi.core.AMapException;
import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.core.d;
import com.amap.mapapi.map.i;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.Proxy;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

class a extends e {
    public a(f fVar, Proxy proxy, String str, String str2) {
        super(fVar, proxy, str, str2);
    }

    private Segment a(GeoPoint geoPoint, GeoPoint geoPoint2) {
        Segment segment = new Segment();
        GeoPoint[] geoPointArr = new GeoPoint[2];
        segment.setShapes(geoPointArr);
        geoPointArr[0] = geoPoint;
        geoPointArr[1] = geoPoint2;
        int latitudeE6 = geoPoint2.getLatitudeE6() - geoPoint.getLatitudeE6();
        int longitudeE6 = geoPoint2.getLongitudeE6() - geoPoint.getLongitudeE6();
        segment.setLength(d.a((int) Math.sqrt((double) ((latitudeE6 * latitudeE6) + (longitudeE6 * longitudeE6)))));
        if (segment.getLength() == 0) {
            segment.setLength(10);
        }
        return segment;
    }

    private List a(LinkedList linkedList) {
        int size = linkedList.size();
        Segment[] segmentArr = new Segment[(size - 1)];
        for (int i = 0; i <= size - 2; i++) {
            segmentArr[i] = a(((Segment) linkedList.get(i)).getLastPoint(), ((Segment) linkedList.get(i + 1)).getFirstPoint());
        }
        for (int i2 = 0; i2 <= size - 2; i2++) {
            linkedList.add((i2 * 2) + 1, segmentArr[i2]);
        }
        linkedList.addFirst(a(this.k, ((Segment) linkedList.getFirst()).getFirstPoint()));
        linkedList.addLast(a(((Segment) linkedList.getLast()).getLastPoint(), this.l));
        return linkedList;
    }

    private void a(BusSegment busSegment, String str) {
        busSegment.setLineName(str);
        busSegment.setFirstStationName(PoiTypeDef.All);
        busSegment.setLastStationName(PoiTypeDef.All);
        int indexOf = str.indexOf("(");
        int lastIndexOf = str.lastIndexOf(")");
        if (indexOf > 0 && lastIndexOf > 0 && lastIndexOf > indexOf) {
            busSegment.setLineName(str.substring(0, indexOf));
            try {
                String[] split = str.substring(indexOf + 1, lastIndexOf).split("--");
                busSegment.setFirstStationName(split[0]);
                busSegment.setLastStationName(split[1]);
            } catch (Exception e) {
            }
        }
    }

    private void a(GeoPoint[] geoPointArr, String[] strArr) {
        int i = 0;
        int i2 = 1;
        while (i < strArr.length - 1) {
            geoPointArr[i2] = new GeoPoint((int) (Double.parseDouble(strArr[i + 1]) * 1000000.0d), (int) (Double.parseDouble(strArr[i]) * 1000000.0d));
            i += 2;
            i2++;
        }
    }

    private void a(String[] strArr, String[] strArr2) {
        for (int i = 0; i < strArr2.length; i++) {
            strArr[i + 1] = strArr2[i];
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ArrayList c(InputStream inputStream) {
        String str;
        if (e()) {
            return super.c(inputStream);
        }
        ArrayList arrayList = new ArrayList();
        try {
            str = new String(i.a(inputStream));
        } catch (Exception e) {
            e.printStackTrace();
            str = null;
        }
        try {
            JSONArray jSONArray = new JSONObject(str).getJSONArray("busList");
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= jSONArray.length()) {
                    break;
                }
                JSONArray jSONArray2 = jSONArray.getJSONObject(i2).getJSONArray("segmentList");
                Route route = new Route(((f) this.b).b);
                LinkedList linkedList = new LinkedList();
                int i3 = 0;
                while (true) {
                    int i4 = i3;
                    if (i4 >= jSONArray2.length()) {
                        break;
                    }
                    JSONObject jSONObject = jSONArray2.getJSONObject(i4);
                    BusSegment busSegment = new BusSegment();
                    String string = jSONObject.getString("startName");
                    String string2 = jSONObject.getString("endName");
                    a(busSegment, jSONObject.getString("busName"));
                    busSegment.setLength(jSONObject.getInt("driverLength"));
                    int i5 = jSONObject.getInt("passDepotCount");
                    String[] strArr = new String[(i5 + 2)];
                    GeoPoint[] geoPointArr = new GeoPoint[(i5 + 2)];
                    String[] split = jSONObject.getString("passDepotName").split(" ");
                    String[] split2 = jSONObject.getString("passDepotCoordinate").split(",");
                    String[] split3 = jSONObject.getString("coordinateList").split(",");
                    a(strArr, split);
                    try {
                        a(geoPointArr, split2);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    try {
                        busSegment.setShapes(a(split3));
                    } catch (Exception e3) {
                        e3.printStackTrace();
                    }
                    geoPointArr[0] = busSegment.getFirstPoint().e();
                    geoPointArr[i5 + 1] = busSegment.getLastPoint().e();
                    strArr[0] = string;
                    strArr[i5 + 1] = string2;
                    busSegment.setPassStopPos(geoPointArr);
                    busSegment.setPassStopName(strArr);
                    linkedList.add(busSegment);
                    i3 = i4 + 1;
                }
                route.a(linkedList);
                a(route);
                for (Segment route2 : route.a()) {
                    route2.setRoute(route);
                }
                route.setStartPlace(this.i);
                route.setTargetPlace(this.j);
                arrayList.add(route);
                i = i2 + 1;
            }
        } catch (JSONException e4) {
            e4.printStackTrace();
        }
        if (arrayList.size() != 0) {
            return arrayList;
        }
        throw new AMapException(AMapException.ERROR_IO);
    }

    /* access modifiers changed from: protected */
    public void a(Route route) {
        route.a(a((LinkedList) route.a()));
    }

    /* access modifiers changed from: protected */
    public void a(ArrayList arrayList) {
    }

    /* access modifiers changed from: protected */
    public void a(Node node, ArrayList arrayList) {
    }

    /* access modifiers changed from: protected */
    public Segment b(Node node) {
        BusSegment busSegment = new BusSegment();
        NodeList childNodes = node.getChildNodes();
        String[] strArr = null;
        String[] strArr2 = null;
        String[] strArr3 = null;
        GeoPoint[] geoPointArr = null;
        String[] strArr4 = null;
        int i = 0;
        String str = PoiTypeDef.All;
        String str2 = PoiTypeDef.All;
        for (int i2 = 0; i2 < childNodes.getLength(); i2++) {
            Node item = childNodes.item(i2);
            String nodeName = item.getNodeName();
            if (nodeName.equals("startName")) {
                str2 = a(item);
            } else if (nodeName.equals("endName")) {
                str = a(item);
            } else if (nodeName.equals("busName")) {
                a(busSegment, a(item));
            } else if (nodeName.equals("driverLength")) {
                busSegment.setLength(Integer.parseInt(a(item)));
            } else if (nodeName.equals("passDepotCount")) {
                i = Integer.parseInt(a(item));
                strArr4 = new String[(i + 2)];
                geoPointArr = new GeoPoint[(i + 2)];
            } else if (nodeName.equals("passDepotName")) {
                strArr3 = a(item).split(" ");
            } else if (nodeName.equals("passDepotCoordinate")) {
                strArr2 = a(item).split(",");
            } else if (nodeName.equals("coordinateList")) {
                strArr = a(item).split(",");
            }
        }
        a(strArr4, strArr3);
        a(geoPointArr, strArr2);
        busSegment.setShapes(a(strArr));
        geoPointArr[0] = busSegment.getFirstPoint().e();
        geoPointArr[i + 1] = busSegment.getLastPoint().e();
        strArr4[0] = str2;
        strArr4[i + 1] = str;
        busSegment.setPassStopPos(geoPointArr);
        busSegment.setPassStopName(strArr4);
        return busSegment;
    }

    /* access modifiers changed from: protected */
    public Route c(Node node) {
        LinkedList linkedList = new LinkedList();
        NodeList childNodes = node.getChildNodes();
        Route route = null;
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node item = childNodes.item(i);
            if (item.getNodeName().equals("segmentList")) {
                NodeList childNodes2 = item.getChildNodes();
                childNodes2.getLength();
                Route route2 = new Route(((f) this.b).b);
                for (int i2 = 0; i2 < childNodes2.getLength(); i2++) {
                    Segment b = b(childNodes2.item(i2));
                    if (b.getShapes().length != 0) {
                        linkedList.add(b);
                    }
                }
                if (linkedList.size() == 0) {
                    return null;
                }
                route2.a(linkedList);
                a(route2);
                for (Segment route3 : route2.a()) {
                    route3.setRoute(route2);
                }
                route = route2;
            }
        }
        return route;
    }

    /* access modifiers changed from: protected */
    public boolean e() {
        return false;
    }

    /* access modifiers changed from: protected */
    public String[] f() {
        String[] strArr = new String[8];
        strArr[0] = "&enc=utf-8";
        try {
            strArr[1] = "&cityCode=" + URLEncoder.encode(((f) this.b).c, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        strArr[2] = "&x1=" + ((f) this.b).a();
        strArr[3] = "&y1=" + ((f) this.b).c();
        strArr[4] = "&x2=" + ((f) this.b).b();
        strArr[5] = "&y2=" + ((f) this.b).d();
        strArr[6] = "&routeType=" + ((f) this.b).b;
        strArr[7] = "&ver=2.0";
        return strArr;
    }

    /* access modifiers changed from: protected */
    public byte[] g() {
        StringBuilder sb = new StringBuilder();
        sb.append("config=BR&resType=json&enc=utf-8&ver=2.0&cityCode=");
        String str = ((f) this.b).c;
        if (str == null || str.equals(PoiTypeDef.All)) {
            sb.append("total");
        } else {
            try {
                str = URLEncoder.encode(str, "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            sb.append(str);
        }
        sb.append("&x1=");
        sb.append(((f) this.b).a());
        sb.append("&y1=");
        sb.append(((f) this.b).c());
        sb.append("&x2=");
        sb.append(((f) this.b).b());
        sb.append("&y2=");
        sb.append(((f) this.b).d());
        sb.append("&routeType=");
        sb.append(((f) this.b).b);
        sb.append("&per=");
        sb.append(50);
        com.amap.mapapi.core.a a2 = com.amap.mapapi.core.a.a(null);
        sb.append("&a_k=");
        sb.append(a2.a());
        return sb.toString().getBytes();
    }

    /* access modifiers changed from: protected */
    public String h() {
        return e() ? com.amap.mapapi.core.i.a().d() + "?&config=BR" : com.amap.mapapi.core.i.a().d();
    }
}
