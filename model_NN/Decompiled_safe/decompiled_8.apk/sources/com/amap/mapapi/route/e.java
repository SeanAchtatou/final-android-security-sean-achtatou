package com.amap.mapapi.route;

import com.amap.mapapi.core.AMapException;
import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.core.t;
import java.io.IOException;
import java.io.InputStream;
import java.net.Proxy;
import java.util.ArrayList;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public abstract class e extends t {
    protected String i = "起点";
    protected String j = "终点";
    protected GeoPoint k = ((f) this.b).f540a.mFrom;
    protected GeoPoint l = ((f) this.b).f540a.mTo;

    public e(f fVar, Proxy proxy, String str, String str2) {
        super(fVar, proxy, str, str2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ArrayList c(InputStream inputStream) {
        NodeList b = b(inputStream);
        ArrayList arrayList = new ArrayList();
        int length = b.getLength();
        for (int i2 = 0; i2 < length; i2++) {
            Node item = b.item(i2);
            String nodeName = item.getNodeName();
            if (nodeName.equals("count")) {
                Integer.parseInt(a(item));
            } else if (nodeName.equals("busList")) {
                NodeList childNodes = item.getChildNodes();
                for (int i3 = 0; i3 < childNodes.getLength(); i3++) {
                    try {
                        Route c = c(childNodes.item(i3));
                        if (c != null) {
                            c.setStartPlace(this.i);
                            c.setTargetPlace(this.j);
                            arrayList.add(c);
                        }
                    } catch (Exception e) {
                    }
                }
            }
        }
        if (arrayList.size() == 0) {
            throw new AMapException(AMapException.ERROR_IO);
        }
        a(arrayList);
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e2) {
                throw new AMapException(AMapException.ERROR_IO);
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public GeoPoint[] a(String[] strArr) {
        int i2 = 0;
        GeoPoint[] geoPointArr = new GeoPoint[(strArr.length / 2)];
        int length = strArr.length;
        int i3 = 0;
        while (i3 < length - 1) {
            geoPointArr[i2] = new GeoPoint((long) (Double.parseDouble(strArr[i3 + 1]) * 1000000.0d), (long) (Double.parseDouble(strArr[i3]) * 1000000.0d));
            i3 += 2;
            i2++;
        }
        return geoPointArr;
    }

    /* access modifiers changed from: protected */
    public abstract Route c(Node node);
}
