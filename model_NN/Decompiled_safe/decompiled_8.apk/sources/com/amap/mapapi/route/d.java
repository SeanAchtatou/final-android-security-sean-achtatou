package com.amap.mapapi.route;

import com.amap.mapapi.core.AMapException;
import com.amap.mapapi.core.i;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.io.IOException;
import java.io.InputStream;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.LinkedList;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

abstract class d extends e {
    public d(f fVar, Proxy proxy, String str, String str2) {
        super(fVar, proxy, str, str2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ArrayList c(InputStream inputStream) {
        NodeList b = b(inputStream);
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < b.getLength(); i++) {
            Node item = b.item(i);
            String nodeName = item.getNodeName();
            if (nodeName.equals("count")) {
                Integer.parseInt(a(item));
            } else if (nodeName.equals("segmengList")) {
                try {
                    Route c = c(item);
                    if (c != null) {
                        c.setStartPlace(this.i);
                        c.setTargetPlace(this.j);
                        arrayList.add(c);
                    }
                } catch (Exception e) {
                }
            }
        }
        if (arrayList.size() == 0) {
            throw new AMapException(AMapException.ERROR_IO);
        }
        a(arrayList);
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e2) {
                throw new AMapException(AMapException.ERROR_IO);
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public void a(Route route) {
        for (int stepCount = route.getStepCount() - 1; stepCount > 0; stepCount--) {
            DriveWalkSegment driveWalkSegment = (DriveWalkSegment) route.getStep(stepCount);
            DriveWalkSegment driveWalkSegment2 = (DriveWalkSegment) route.getStep(stepCount - 1);
            driveWalkSegment.setActionCode(driveWalkSegment2.getActionCode());
            driveWalkSegment.setActionDescription(driveWalkSegment2.getActionDescription());
        }
        DriveWalkSegment driveWalkSegment3 = (DriveWalkSegment) route.getStep(0);
        driveWalkSegment3.setActionCode(-1);
        driveWalkSegment3.setActionDescription(PoiTypeDef.All);
    }

    /* access modifiers changed from: protected */
    public Segment b(Node node) {
        NodeList childNodes = node.getChildNodes();
        DriveWalkSegment driveWalkSegment = new DriveWalkSegment();
        int length = childNodes.getLength();
        for (int i = 0; i < length; i++) {
            Node item = childNodes.item(i);
            String nodeName = item.getNodeName();
            if (nodeName.equals("roadName")) {
                driveWalkSegment.setRoadName(a(item));
            } else if (nodeName.equals("roadLength")) {
                try {
                    driveWalkSegment.setLength(Integer.parseInt(a(item)));
                } catch (NumberFormatException e) {
                    e.getStackTrace();
                }
            } else if (nodeName.equals("action")) {
                driveWalkSegment.setActionDescription(a(item));
            } else if (nodeName.equals("coor")) {
                driveWalkSegment.setShapes(a(a(item).split(",")));
            } else if (nodeName.equals("driveTime")) {
                driveWalkSegment.setConsumeTime(a(item));
            }
        }
        return driveWalkSegment;
    }

    /* access modifiers changed from: protected */
    public Route c(Node node) {
        Route route;
        LinkedList linkedList = new LinkedList();
        if (node.getNodeName().equals("segmengList")) {
            NodeList childNodes = node.getChildNodes();
            childNodes.getLength();
            Route route2 = new Route(((f) this.b).b);
            for (int i = 0; i < childNodes.getLength(); i++) {
                Segment b = b(childNodes.item(i));
                if (b.getShapes().length != 0) {
                    linkedList.add(b);
                }
            }
            if (linkedList.size() == 0) {
                return null;
            }
            route2.a(linkedList);
            a(route2);
            for (Segment route3 : route2.a()) {
                route3.setRoute(route2);
            }
            route = route2;
        } else {
            route = null;
        }
        return route;
    }

    /* access modifiers changed from: protected */
    public String[] f() {
        return new String[]{"&x1=" + ((f) this.b).a(), "&y1=" + ((f) this.b).c(), "&x2=" + ((f) this.b).b(), "&y2=" + ((f) this.b).d(), "&routeType=" + (((f) this.b).b % 10)};
    }

    /* access modifiers changed from: protected */
    public String h() {
        return e() ? i.a().d() + "?highLight=false&enc=utf-8&ver=2.0&config=R&resType=xml" : i.a().d();
    }
}
