package com.amap.mapapi.route;

import android.view.View;
import com.amap.mapapi.map.MapView;
import com.amap.mapapi.map.RouteMessageHandler;
import com.amap.mapapi.map.RouteOverlay;

class b {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public RouteMessageHandler f538a;
    /* access modifiers changed from: private */
    public int b;
    /* access modifiers changed from: private */
    public int c;
    /* access modifiers changed from: private */
    public MapView d;
    /* access modifiers changed from: private */
    public RouteOverlay e;

    public b(MapView mapView, RouteMessageHandler routeMessageHandler, RouteOverlay routeOverlay, int i, int i2) {
        this.d = mapView;
        this.f538a = routeMessageHandler;
        this.b = i2;
        this.c = i;
        this.e = routeOverlay;
    }

    public void a(View view) {
        view.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                b.this.f538a.onRouteEvent(b.this.d, b.this.e, b.this.c, b.this.b);
            }
        });
    }
}
