package com.amap.mapapi.route;

import com.amap.mapapi.core.d;
import com.amap.mapapi.route.Route;

public class f {

    /* renamed from: a  reason: collision with root package name */
    public Route.FromAndTo f540a;
    public int b;
    public String c;

    public f(Route.FromAndTo fromAndTo, int i) {
        this.f540a = fromAndTo;
        this.b = i;
    }

    public double a() {
        return d.a(this.f540a.mFrom.a());
    }

    public void a(String str) {
        this.c = str;
    }

    public double b() {
        return d.a(this.f540a.mTo.a());
    }

    public double c() {
        return d.a(this.f540a.mFrom.b());
    }

    public double d() {
        return d.a(this.f540a.mTo.b());
    }
}
