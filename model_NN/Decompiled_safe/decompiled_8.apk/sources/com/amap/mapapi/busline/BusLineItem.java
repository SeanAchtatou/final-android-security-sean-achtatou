package com.amap.mapapi.busline;

import com.amap.mapapi.core.GeoPoint;
import java.util.ArrayList;

public class BusLineItem {
    private ArrayList A;
    private GeoPoint B = null;
    private GeoPoint C = null;

    /* renamed from: a  reason: collision with root package name */
    private float f409a;
    private String b;
    private int c;
    private String d;
    private int e;
    private float f;
    private ArrayList g;
    private String h;
    private String i;
    private String j;
    private String k;
    private String l;
    private String m;
    private String n;
    private float o;
    private float p;
    private boolean q;
    private boolean r;
    private boolean s;
    private boolean t;
    private boolean u;
    private int v;
    private boolean w;
    private String x;
    private String y;
    private boolean z;

    private void a() {
        if (this.g != null && this.g.size() != 0) {
            int i2 = 0;
            int i3 = Integer.MAX_VALUE;
            int i4 = Integer.MIN_VALUE;
            int i5 = Integer.MAX_VALUE;
            int i6 = Integer.MIN_VALUE;
            while (true) {
                int i7 = i2;
                if (i7 < this.g.size()) {
                    GeoPoint geoPoint = (GeoPoint) this.g.get(i7);
                    int longitudeE6 = geoPoint.getLongitudeE6();
                    int latitudeE6 = geoPoint.getLatitudeE6();
                    if (longitudeE6 < i5) {
                        i5 = longitudeE6;
                    }
                    if (latitudeE6 < i3) {
                        i3 = latitudeE6;
                    }
                    if (longitudeE6 > i4) {
                        i4 = longitudeE6;
                    }
                    if (latitudeE6 > i6) {
                        i6 = latitudeE6;
                    }
                    i2 = i7 + 1;
                } else {
                    this.B = new GeoPoint(i3, i5);
                    this.C = new GeoPoint(i6, i4);
                    return;
                }
            }
        }
    }

    public GeoPoint getLowerLeftPoint() {
        if (this.B == null) {
            a();
        }
        return this.B;
    }

    public GeoPoint getUpperRightPoint() {
        if (this.C == null) {
            a();
        }
        return this.C;
    }

    public boolean getmAir() {
        return this.w;
    }

    public boolean getmAuto() {
        return this.r;
    }

    public float getmBasicPrice() {
        return this.o;
    }

    public boolean getmCommunicationTicket() {
        return this.q;
    }

    public String getmCompany() {
        return this.n;
    }

    public int getmDataSource() {
        return this.v;
    }

    public String getmDescription() {
        return this.d;
    }

    public String getmEndTime() {
        return this.m;
    }

    public String getmFrontName() {
        return this.j;
    }

    public String getmFrontSpell() {
        return this.x;
    }

    public String getmKeyName() {
        return this.i;
    }

    public float getmLength() {
        return this.f409a;
    }

    public String getmLineId() {
        return this.h;
    }

    public String getmName() {
        return this.b;
    }

    public float getmSpeed() {
        return this.f;
    }

    public String getmStartTime() {
        return this.l;
    }

    public ArrayList getmStations() {
        return this.A;
    }

    public int getmStatus() {
        return this.e;
    }

    public String getmTerminalName() {
        return this.k;
    }

    public String getmTerminalSpell() {
        return this.y;
    }

    public float getmTotalPrice() {
        return this.p;
    }

    public int getmType() {
        return this.c;
    }

    public ArrayList getmXys() {
        return this.g;
    }

    public boolean ismDoubleDeck() {
        return this.u;
    }

    public boolean ismExpressWay() {
        return this.z;
    }

    public boolean ismIcCard() {
        return this.s;
    }

    public boolean ismLoop() {
        return this.t;
    }

    public void setmAir(boolean z2) {
        this.w = z2;
    }

    public void setmAuto(boolean z2) {
        this.r = z2;
    }

    public void setmBasicPrice(float f2) {
        this.o = f2;
    }

    public void setmCommunicationTicket(boolean z2) {
        this.q = z2;
    }

    public void setmCompany(String str) {
        this.n = str;
    }

    public void setmDataSource(int i2) {
        this.v = i2;
    }

    public void setmDescription(String str) {
        this.d = str;
    }

    public void setmDoubleDeck(boolean z2) {
        this.u = z2;
    }

    public void setmEndTime(String str) {
        this.m = str;
    }

    public void setmExpressWay(boolean z2) {
        this.z = z2;
    }

    public void setmFrontName(String str) {
        this.j = str;
    }

    public void setmFrontSpell(String str) {
        this.x = str;
    }

    public void setmIcCard(boolean z2) {
        this.s = z2;
    }

    public void setmKeyName(String str) {
        this.i = str;
    }

    public void setmLength(float f2) {
        this.f409a = f2;
    }

    public void setmLineId(String str) {
        this.h = str;
    }

    public void setmLoop(boolean z2) {
        this.t = z2;
    }

    public void setmName(String str) {
        this.b = str;
    }

    public void setmSpeed(float f2) {
        this.f = f2;
    }

    public void setmStartTime(String str) {
        this.l = str;
    }

    public void setmStations(ArrayList arrayList) {
        this.A = arrayList;
    }

    public void setmStatus(int i2) {
        this.e = i2;
    }

    public void setmTerminalName(String str) {
        this.k = str;
    }

    public void setmTerminalSpell(String str) {
        this.y = str;
    }

    public void setmTotalPrice(float f2) {
        this.p = f2;
    }

    public void setmType(int i2) {
        this.c = i2;
    }

    public void setmXys(ArrayList arrayList) {
        this.g = arrayList;
    }

    public String toString() {
        return this.b + " " + this.l + "-" + this.m;
    }
}
