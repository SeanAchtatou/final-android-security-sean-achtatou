package com.amap.mapapi.busline;

import android.content.Context;
import com.amap.mapapi.core.a;
import com.amap.mapapi.core.d;
import java.util.ArrayList;

public class BusSearch {

    /* renamed from: a  reason: collision with root package name */
    private Context f413a;
    private BusQuery b;
    private int c = 10;

    public BusSearch(Context context, BusQuery busQuery) {
        a.a(context);
        this.f413a = context;
        this.b = busQuery;
    }

    public BusSearch(Context context, String str, BusQuery busQuery) {
        a.a(context);
        this.f413a = context;
        this.b = busQuery;
    }

    public BusQuery getQuery() {
        return this.b;
    }

    public BusPagedResult searchBusLine() {
        a aVar = new a(this.b, d.b(this.f413a), d.a(this.f413a), null);
        aVar.a(1);
        aVar.b(this.c);
        return BusPagedResult.a(aVar, (ArrayList) aVar.j());
    }

    public void setPageSize(int i) {
        this.c = i;
    }

    public void setQuery(BusQuery busQuery) {
        this.b = busQuery;
    }
}
