package com.amap.mapapi.busline;

import com.amap.mapapi.core.d;

public class BusQuery {

    /* renamed from: a  reason: collision with root package name */
    private String f411a;
    private SearchType b;
    private String c;

    public enum SearchType {
        BY_ID,
        BY_LINE_NAME,
        BY_STATION_NAME
    }

    public BusQuery(String str, SearchType searchType) {
        this(str, searchType, null);
    }

    public BusQuery(String str, SearchType searchType, String str2) {
        this.f411a = str;
        this.b = searchType;
        this.c = str2;
        if (!a()) {
            throw new IllegalArgumentException("Empty query");
        }
    }

    private boolean a() {
        return !d.a(this.f411a);
    }

    public SearchType getCategory() {
        return this.b;
    }

    public String getCity() {
        return this.c;
    }

    public String getQueryString() {
        return this.f411a;
    }
}
