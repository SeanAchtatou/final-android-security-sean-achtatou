package com.amap.mapapi.busline;

import com.amap.mapapi.core.GeoPoint;

public class BusStationItem {

    /* renamed from: a  reason: collision with root package name */
    private String f414a;
    private GeoPoint b;
    private String c;
    private String d;
    private int e;

    public String getmCode() {
        return this.d;
    }

    public GeoPoint getmCoord() {
        return this.b;
    }

    public String getmName() {
        return this.f414a;
    }

    public String getmSpell() {
        return this.c;
    }

    public int getmStationNum() {
        return this.e;
    }

    public void setmCode(String str) {
        this.d = str;
    }

    public void setmCoord(GeoPoint geoPoint) {
        this.b = geoPoint;
    }

    public void setmName(String str) {
        this.f414a = str;
    }

    public void setmSpell(String str) {
        this.c = str;
    }

    public void setmStationNum(int i) {
        this.e = i;
    }

    public String toString() {
        return "Name: " + this.f414a + " Coord: " + this.b.toString() + " Spell: " + this.c + " Code: " + this.d + " StationNum: " + this.e;
    }
}
