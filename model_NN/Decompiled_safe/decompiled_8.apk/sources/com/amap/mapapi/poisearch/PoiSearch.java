package com.amap.mapapi.poisearch;

import android.content.Context;
import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.core.a;
import com.amap.mapapi.core.b;
import com.amap.mapapi.core.d;
import com.amap.mapapi.map.MapView;
import java.util.ArrayList;

public class PoiSearch {

    /* renamed from: a  reason: collision with root package name */
    private SearchBound f528a;
    private Query b;
    private Context c;
    private int d = 20;

    public class Query {

        /* renamed from: a  reason: collision with root package name */
        private String f529a;
        private String b;
        private String c;

        public Query(String str, String str2) {
            this(str, str2, null);
        }

        public Query(String str, String str2, String str3) {
            this.f529a = str;
            this.b = str2;
            this.c = str3;
            if (!b()) {
                throw new IllegalArgumentException("Empty  query and catagory");
            }
        }

        private boolean b() {
            return !d.a(this.f529a) || !d.a(this.b);
        }

        /* access modifiers changed from: package-private */
        public String a() {
            return PoiTypeDef.All;
        }

        public String getCategory() {
            return (this.b == null || this.b.equals("00") || this.b.equals("00|")) ? a() : this.b;
        }

        public String getCity() {
            return this.c;
        }

        public String getQueryString() {
            return this.f529a;
        }
    }

    public class SearchBound {
        public static final String BOUND_SHAPE = "bound";
        public static final String ELLIPSE_SHAPE = "Ellipse";
        public static final String POLYGON_SHAPE = "Polygon";
        public static final String RECTANGLE_SHAPE = "Rectangle";

        /* renamed from: a  reason: collision with root package name */
        private GeoPoint f530a;
        private GeoPoint b;
        private int c;
        private GeoPoint d;
        private String e;

        public SearchBound(GeoPoint geoPoint, int i) {
            this.e = BOUND_SHAPE;
            this.c = i;
            this.d = geoPoint;
            a(geoPoint, d.b(i), d.b(i));
        }

        public SearchBound(GeoPoint geoPoint, GeoPoint geoPoint2) {
            this.e = RECTANGLE_SHAPE;
            a(geoPoint, geoPoint2);
        }

        public SearchBound(MapView mapView) {
            this.e = RECTANGLE_SHAPE;
            a(mapView.getProjection().fromPixels(0, b.j), mapView.getProjection().fromPixels(b.i, 0));
        }

        private void a(GeoPoint geoPoint, int i, int i2) {
            int i3 = i / 2;
            int i4 = i2 / 2;
            long b2 = geoPoint.b();
            long a2 = geoPoint.a();
            a(new GeoPoint(b2 - ((long) i3), a2 - ((long) i4)), new GeoPoint(b2 + ((long) i3), ((long) i4) + a2));
        }

        private void a(GeoPoint geoPoint, GeoPoint geoPoint2) {
            this.f530a = geoPoint;
            this.b = geoPoint2;
            if (this.f530a.b() >= this.b.b() || this.f530a.a() >= this.b.a()) {
                throw new IllegalArgumentException("invalid rect ");
            }
            this.d = new GeoPoint((this.f530a.b() + this.b.b()) / 2, (this.f530a.a() + this.b.a()) / 2);
        }

        public GeoPoint getCenter() {
            return this.d;
        }

        public int getLatSpanInMeter() {
            return d.a(this.b.getLatitudeE6() - this.f530a.getLatitudeE6());
        }

        public int getLonSpanInMeter() {
            return d.a(this.b.getLongitudeE6() - this.f530a.getLongitudeE6());
        }

        public GeoPoint getLowerLeft() {
            return this.f530a;
        }

        public int getRange() {
            return this.c;
        }

        public String getShape() {
            return this.e;
        }

        public GeoPoint getUpperRight() {
            return this.b;
        }
    }

    public PoiSearch(Context context, Query query) {
        a.a(context);
        this.c = context;
        setQuery(query);
    }

    public PoiSearch(Context context, String str, Query query) {
        a.a(context);
        this.c = context;
        setQuery(query);
    }

    public SearchBound getBound() {
        return this.f528a;
    }

    public Query getQuery() {
        return this.b;
    }

    public PoiPagedResult searchPOI() {
        a aVar = new a(new b(this.b, this.f528a), d.b(this.c), d.a(this.c), null);
        aVar.a(1);
        aVar.b(this.d);
        return PoiPagedResult.a(aVar, (ArrayList) aVar.j());
    }

    public void setBound(SearchBound searchBound) {
        this.f528a = searchBound;
    }

    public void setPageSize(int i) {
        this.d = i;
    }

    @Deprecated
    public void setPoiNumber(int i) {
        setPageSize(i);
    }

    public void setQuery(Query query) {
        this.b = query;
    }
}
