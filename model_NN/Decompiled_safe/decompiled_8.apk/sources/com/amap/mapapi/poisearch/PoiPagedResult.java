package com.amap.mapapi.poisearch;

import com.amap.mapapi.poisearch.PoiSearch;
import java.util.ArrayList;
import java.util.List;

public final class PoiPagedResult {

    /* renamed from: a  reason: collision with root package name */
    private int f527a;
    private ArrayList b;
    private a c;

    private PoiPagedResult(a aVar, ArrayList arrayList) {
        this.c = aVar;
        this.f527a = a(aVar.c());
        a(arrayList);
    }

    private int a(int i) {
        int a2 = this.c.a();
        int i2 = ((i + a2) - 1) / a2;
        if (i2 > 30) {
            return 30;
        }
        return i2;
    }

    static PoiPagedResult a(a aVar, ArrayList arrayList) {
        return new PoiPagedResult(aVar, arrayList);
    }

    private void a(ArrayList arrayList) {
        this.b = new ArrayList();
        for (int i = 0; i <= this.f527a; i++) {
            this.b.add(null);
        }
        if (this.f527a > 0) {
            this.b.set(1, arrayList);
        }
    }

    private boolean b(int i) {
        return i <= this.f527a && i > 0;
    }

    public final PoiSearch.SearchBound getBound() {
        return this.c.l();
    }

    public final List getPage(int i) {
        if (this.f527a == 0) {
            return null;
        }
        if (!b(i)) {
            throw new IllegalArgumentException("page out of range");
        }
        ArrayList arrayList = (ArrayList) getPageLocal(i);
        if (arrayList != null) {
            return arrayList;
        }
        this.c.a(i);
        ArrayList arrayList2 = (ArrayList) this.c.j();
        this.b.set(i, arrayList2);
        return arrayList2;
    }

    public final int getPageCount() {
        return this.f527a;
    }

    public final List getPageLocal(int i) {
        if (b(i)) {
            return (List) this.b.get(i);
        }
        throw new IllegalArgumentException("page out of range");
    }

    public final PoiSearch.Query getQuery() {
        return this.c.d();
    }

    public final List getSearchSuggestions() {
        return this.c.m();
    }
}
