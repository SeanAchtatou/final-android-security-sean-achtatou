package com.amap.api.location.core;

import android.os.Parcel;
import android.os.Parcelable;

public class GeoPoint implements Parcelable {
    public static final Parcelable.Creator CREATOR = new a();

    /* renamed from: a  reason: collision with root package name */
    private long f408a;
    private long b;
    private double c;
    private double d;

    public GeoPoint() {
        this.f408a = Long.MIN_VALUE;
        this.b = Long.MIN_VALUE;
        this.c = Double.MIN_VALUE;
        this.d = Double.MIN_VALUE;
        this.f408a = 0;
        this.b = 0;
    }

    private GeoPoint(Parcel parcel) {
        this.f408a = Long.MIN_VALUE;
        this.b = Long.MIN_VALUE;
        this.c = Double.MIN_VALUE;
        this.d = Double.MIN_VALUE;
        this.f408a = parcel.readLong();
        this.b = parcel.readLong();
    }

    /* synthetic */ GeoPoint(Parcel parcel, byte b2) {
        this(parcel);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        GeoPoint geoPoint = (GeoPoint) obj;
        return this.c == geoPoint.c && this.d == geoPoint.d && this.f408a == geoPoint.f408a && this.b == geoPoint.b;
    }

    public int hashCode() {
        return (int) ((this.d * 7.0d) + (this.c * 11.0d));
    }

    public String toString() {
        return this.f408a + "," + this.b;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.f408a);
        parcel.writeLong(this.b);
    }
}
