package com.amap.api.location.core;

import android.os.Parcel;
import android.os.Parcelable;

final class a implements Parcelable.Creator {
    a() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new GeoPoint(parcel, (byte) 0);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new GeoPoint[i];
    }
}
