package com.c.a;

import android.os.Bundle;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private static String f593a = "https://api.facebook.com/restserver.php";
    private String b = null;

    public d(String str) {
        if (str == null) {
            throw new IllegalArgumentException("You must specify your application ID when instantiating a Facebook object. See README for details.");
        }
    }

    public final String a(Bundle bundle, String str) {
        bundle.putString("format", "json");
        if (this.b != null && (0 == 0 || System.currentTimeMillis() < 0)) {
            bundle.putString("access_token", this.b);
        }
        return e.a(f593a, str, bundle);
    }

    public final void a(String str) {
        this.b = str;
        System.currentTimeMillis();
    }
}
