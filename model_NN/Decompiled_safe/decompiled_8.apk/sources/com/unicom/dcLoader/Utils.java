package com.unicom.dcLoader;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import com.amap.mapapi.poisearch.PoiTypeDef;
import dalvik.system.DexClassLoader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.Constructor;

public class Utils {
    /* access modifiers changed from: private */
    public static b d;
    private static DexClassLoader g = null;
    private static Utils h;

    /* renamed from: a  reason: collision with root package name */
    private String f3120a = "src_init";
    private String b = "sdk_version";
    private String c = "so_init";
    private Context e;
    private boolean f = false;
    private Handler i = new a();

    private Utils() {
    }

    public static Utils a() {
        if (h == null) {
            h = new Utils();
        }
        return h;
    }

    private void b(Context context) {
        g = new DexClassLoader(new File(getNativeFile(context.getApplicationInfo().packageName)).getAbsolutePath(), new File(getNativeDir(context.getApplicationInfo().packageName)).getAbsolutePath(), null, context.getClassLoader());
    }

    private int c() {
        SharedPreferences sharedPreferences = this.e.getSharedPreferences(this.f3120a, 0);
        try {
            String str = "/data/data/" + this.e.getApplicationInfo().packageName + "/.ulibs/";
            File file = new File(str);
            if (!file.exists()) {
                file.mkdirs();
            }
            File file2 = new File(str + "libunicomsdk.so");
            if (!file2.exists()) {
                file2.createNewFile();
            } else {
                file2.delete();
                file2.createNewFile();
            }
            String str2 = Build.CPU_ABI;
            Log.v("xyf", "ABI:" + str2);
            InputStream open = str2.indexOf("armeabi-v7a") >= 0 ? this.e.getAssets().open("armeabi-v7a/libunicomsdk.jar") : str2.indexOf("mips") >= 0 ? this.e.getAssets().open("mips/libunicomsdk.jar") : str2.indexOf("x86") >= 0 ? this.e.getAssets().open("x86/libunicomsdk.jar") : this.e.getAssets().open("armeabi/libunicomsdk.jar");
            if (open == null) {
                return 0;
            }
            FileOutputStream fileOutputStream = new FileOutputStream(file2);
            byte[] bArr = new byte[1024];
            while (true) {
                int read = open.read(bArr);
                if (read != -1) {
                    fileOutputStream.write(bArr, 0, read);
                } else {
                    fileOutputStream.flush();
                    open.close();
                    fileOutputStream.close();
                    Runtime.getRuntime().gc();
                    System.load("/data/data/" + this.e.getApplicationInfo().packageName + "/.ulibs/libunicomsdk.so");
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putBoolean(this.c, true);
                    edit.commit();
                    return 1;
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    private boolean c(Context context) {
        try {
            File file = new File(getNativeDir(context.getApplicationInfo().packageName));
            File file2 = new File(getNativeFile(context.getApplicationInfo().packageName));
            if (!file.exists()) {
                file.mkdirs();
            }
            if (file2.exists()) {
                return true;
            }
            file2.createNewFile();
            if (context.getSharedPreferences(this.f3120a, 0).getString(this.b, "1.2.0").equals("1.2.0")) {
                InputStream open = context.getAssets().open("classes.jar");
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = open.read(bArr);
                    if (read != -1) {
                        byteArrayOutputStream.write(bArr, 0, read);
                    } else {
                        byteArrayOutputStream.flush();
                        byte[] byteArray = byteArrayOutputStream.toByteArray();
                        open.close();
                        byteArrayOutputStream.close();
                        loadclass(context, byteArray, file.getAbsolutePath(), file2.getAbsolutePath(), context.getApplicationInfo().packageName, context.getClassLoader());
                        return true;
                    }
                }
            } else {
                FileInputStream fileInputStream = new FileInputStream(new File(Environment.getExternalStorageDirectory() + "/.ucache/classes.jar"));
                ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
                byte[] bArr2 = new byte[1024];
                while (true) {
                    int read2 = fileInputStream.read(bArr2);
                    if (read2 != -1) {
                        byteArrayOutputStream2.write(bArr2, 0, read2);
                    } else {
                        byteArrayOutputStream2.flush();
                        byte[] byteArray2 = byteArrayOutputStream2.toByteArray();
                        fileInputStream.close();
                        byteArrayOutputStream2.close();
                        loadclass(context, byteArray2, file.getAbsolutePath(), file2.getAbsolutePath(), context.getApplicationInfo().packageName, context.getClassLoader());
                        return true;
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            return true;
        }
    }

    private native String getNativeDir(String str);

    private native String getNativeFile(String str);

    private native int loadclass(Context context, byte[] bArr, String str, String str2, String str3, ClassLoader classLoader);

    public final int a(Context context, String str) {
        try {
            if (g == null) {
                b(context);
            }
            Class loadClass = g.loadClass("com.unipay.tools.MultimodeConfig");
            loadClass.getMethod("setCallbackUrl", String.class).invoke(loadClass, str);
            Class loadClass2 = g.loadClass("com.unipay.unipay_sdk.UniPay");
            Constructor<?>[] declaredConstructors = loadClass2.getDeclaredConstructors();
            declaredConstructors[0].setAccessible(true);
            Object newInstance = declaredConstructors[0].newInstance(new Object[0]);
            loadClass2.getDeclaredMethod("setPaySelected", Boolean.TYPE, Boolean.TYPE).invoke(newInstance, true, false);
            return 1;
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public final void a(Context context) {
        if (this.f) {
            try {
                if (g == null) {
                    b(context);
                }
                Class loadClass = g.loadClass("com.unipay.unipay_sdk.UniPay");
                Constructor<?>[] declaredConstructors = loadClass.getDeclaredConstructors();
                declaredConstructors[0].setAccessible(true);
                Object newInstance = declaredConstructors[0].newInstance(new Object[0]);
                loadClass.getDeclaredMethod("exit", Context.class).invoke(newInstance, context);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public final void a(Context context, String str, String str2, String str3, String str4, String str5, String str6, b bVar) {
        d = bVar;
        this.e = context;
        context.getSharedPreferences(this.f3120a, 0).getString(this.b, "1.2.0");
        try {
            if (c() == 0) {
                bVar.a(PoiTypeDef.All, 10000, -1, "SO文件加载失败");
                return;
            }
            c(context);
            try {
                if (g == null) {
                    b(context);
                }
                Class loadClass = g.loadClass("com.unipay.beans.GameBaseBean");
                Object newInstance = loadClass.getConstructor(String.class, String.class, String.class, String.class, String.class, String.class).newInstance(str, str2, str3, str4, str5, str6);
                Class loadClass2 = g.loadClass("com.unipay.unipay_sdk.UniPay");
                Constructor<?>[] declaredConstructors = loadClass2.getDeclaredConstructors();
                declaredConstructors[0].setAccessible(true);
                Object newInstance2 = declaredConstructors[0].newInstance(new Object[0]);
                loadClass2.getDeclaredMethod("init", Context.class, loadClass).invoke(newInstance2, context, newInstance);
                this.f = true;
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            Log.v("xyf", "init success");
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    public final void a(Context context, String str, String str2, String str3, String str4, String str5, String str6, c cVar, b bVar) {
        d = bVar;
        if (this.f) {
            try {
                if (g == null) {
                    b(context);
                }
                Class loadClass = g.loadClass("com.unipay.beans.PayValueBean");
                Object newInstance = loadClass.getConstructor(String.class, String.class, String.class, String.class, String.class, Integer.TYPE, String.class).newInstance(str, str2, str3, str4, str5, Integer.valueOf(cVar.ordinal()), str6);
                Class loadClass2 = g.loadClass("com.unipay.unipay_sdk.UniPay");
                Constructor<?>[] declaredConstructors = loadClass2.getDeclaredConstructors();
                declaredConstructors[0].setAccessible(true);
                Object newInstance2 = declaredConstructors[0].newInstance(new Object[0]);
                loadClass2.getDeclaredMethod("pay", Context.class, loadClass, Handler.class).invoke(newInstance2, context, newInstance, this.i);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}
