package com.e.a.a;

import mm.purchasesdk.PurchaseCode;

final class i implements n {
    private j b;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.e.a.a.i.a(byte[], short):boolean
     arg types: [byte[], int]
     candidates:
      com.e.a.a.i.a(byte[], int):boolean
      com.e.a.a.i.a(byte[], short):boolean */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0034  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public i(java.lang.String r13, java.io.InputStream r14, com.e.a.a.l r15) {
        /*
            r12 = this;
            r12.<init>()
            com.e.a.a.m r3 = com.e.a.a.n.f635a
            boolean r0 = r14.markSupported()
            if (r0 != 0) goto L_0x0013
            java.lang.Error r0 = new java.lang.Error
            java.lang.String r1 = "Precondition violation: the InputStream passed to ParseByteStream must support mark"
            r0.<init>(r1)
            throw r0
        L_0x0013:
            r0 = 71
            r14.mark(r0)
            r0 = 4
            byte[] r1 = new byte[r0]
            int r0 = r14.read(r1)
            r2 = 4
            if (r0 == r2) goto L_0x00bd
            if (r0 > 0) goto L_0x00a0
            java.lang.String r0 = "no characters in input"
        L_0x0026:
            r2 = 1
            com.e.a.a.m.a(r0, r13, r2)
        L_0x002a:
            java.lang.String r4 = "UTF-8"
        L_0x002c:
            java.lang.String r0 = "UTF-8"
            boolean r0 = r4.equals(r0)
            if (r0 != 0) goto L_0x008a
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            java.lang.String r2 = "From start "
            r0.<init>(r2)
            r2 = 0
            byte r2 = r1[r2]
            java.lang.String r2 = a(r2)
            java.lang.StringBuffer r0 = r0.append(r2)
            java.lang.String r2 = " "
            java.lang.StringBuffer r0 = r0.append(r2)
            r2 = 1
            byte r2 = r1[r2]
            java.lang.String r2 = a(r2)
            java.lang.StringBuffer r0 = r0.append(r2)
            java.lang.String r2 = " "
            java.lang.StringBuffer r0 = r0.append(r2)
            r2 = 2
            byte r2 = r1[r2]
            java.lang.String r2 = a(r2)
            java.lang.StringBuffer r0 = r0.append(r2)
            java.lang.String r2 = " "
            java.lang.StringBuffer r0 = r0.append(r2)
            r2 = 3
            byte r1 = r1[r2]
            java.lang.String r1 = a(r1)
            java.lang.StringBuffer r0 = r0.append(r1)
            java.lang.String r1 = " deduced encoding = "
            java.lang.StringBuffer r0 = r0.append(r1)
            java.lang.StringBuffer r0 = r0.append(r4)
            java.lang.String r0 = r0.toString()
            com.e.a.a.m.a(r0, r13)
        L_0x008a:
            r14.reset()     // Catch:{ f -> 0x017e }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ f -> 0x017e }
            java.lang.String r0 = a(r4)     // Catch:{ f -> 0x017e }
            r2.<init>(r14, r0)     // Catch:{ f -> 0x017e }
            com.e.a.a.j r0 = new com.e.a.a.j     // Catch:{ IOException -> 0x0146 }
            r1 = r13
            r5 = r15
            r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ IOException -> 0x0146 }
            r12.b = r0     // Catch:{ IOException -> 0x0146 }
        L_0x009f:
            return
        L_0x00a0:
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            java.lang.String r4 = "less than 4 characters in input: \""
            r2.<init>(r4)
            java.lang.String r4 = new java.lang.String
            r5 = 0
            r4.<init>(r1, r5, r0)
            java.lang.StringBuffer r0 = r2.append(r4)
            java.lang.String r2 = "\""
            java.lang.StringBuffer r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            goto L_0x0026
        L_0x00bd:
            r0 = 65279(0xfeff, float:9.1475E-41)
            boolean r0 = a(r1, r0)
            if (r0 != 0) goto L_0x00ff
            r0 = -131072(0xfffffffffffe0000, float:NaN)
            boolean r0 = a(r1, r0)
            if (r0 != 0) goto L_0x00ff
            r0 = 65534(0xfffe, float:9.1833E-41)
            boolean r0 = a(r1, r0)
            if (r0 != 0) goto L_0x00ff
            r0 = -16842752(0xfffffffffeff0000, float:-1.6947657E38)
            boolean r0 = a(r1, r0)
            if (r0 != 0) goto L_0x00ff
            r0 = 60
            boolean r0 = a(r1, r0)
            if (r0 != 0) goto L_0x00ff
            r0 = 1006632960(0x3c000000, float:0.0078125)
            boolean r0 = a(r1, r0)
            if (r0 != 0) goto L_0x00ff
            r0 = 15360(0x3c00, float:2.1524E-41)
            boolean r0 = a(r1, r0)
            if (r0 != 0) goto L_0x00ff
            r0 = 3932160(0x3c0000, float:5.51013E-39)
            boolean r0 = a(r1, r0)
            if (r0 == 0) goto L_0x0103
        L_0x00ff:
            java.lang.String r4 = "UCS-4"
            goto L_0x002c
        L_0x0103:
            r0 = 3932223(0x3c003f, float:5.510218E-39)
            boolean r0 = a(r1, r0)
            if (r0 == 0) goto L_0x0110
            java.lang.String r4 = "UTF-16BE"
            goto L_0x002c
        L_0x0110:
            r0 = 1006649088(0x3c003f00, float:0.00782752)
            boolean r0 = a(r1, r0)
            if (r0 == 0) goto L_0x011d
            java.lang.String r4 = "UTF-16LE"
            goto L_0x002c
        L_0x011d:
            r0 = 1010792557(0x3c3f786d, float:0.011686427)
            boolean r0 = a(r1, r0)
            if (r0 != 0) goto L_0x002a
            r0 = 1282385812(0x4c6fa794, float:6.2824016E7)
            boolean r0 = a(r1, r0)
            if (r0 == 0) goto L_0x0133
            java.lang.String r4 = "EBCDIC"
            goto L_0x002c
        L_0x0133:
            r0 = -2
            boolean r0 = a(r1, r0)
            if (r0 != 0) goto L_0x0142
            r0 = -257(0xfffffffffffffeff, float:NaN)
            boolean r0 = a(r1, r0)
            if (r0 == 0) goto L_0x002a
        L_0x0142:
            java.lang.String r4 = "UTF-16"
            goto L_0x002c
        L_0x0146:
            r0 = move-exception
            java.lang.String r9 = "euc-jp"
            java.lang.StringBuffer r0 = new java.lang.StringBuffer     // Catch:{ f -> 0x017e }
            java.lang.String r1 = "Problem reading with assumed encoding of "
            r0.<init>(r1)     // Catch:{ f -> 0x017e }
            java.lang.StringBuffer r0 = r0.append(r4)     // Catch:{ f -> 0x017e }
            java.lang.String r1 = " so restarting with "
            java.lang.StringBuffer r0 = r0.append(r1)     // Catch:{ f -> 0x017e }
            java.lang.StringBuffer r0 = r0.append(r9)     // Catch:{ f -> 0x017e }
            java.lang.String r0 = r0.toString()     // Catch:{ f -> 0x017e }
            com.e.a.a.m.a(r0, r13)     // Catch:{ f -> 0x017e }
            r14.reset()     // Catch:{ f -> 0x017e }
            java.io.InputStreamReader r7 = new java.io.InputStreamReader     // Catch:{ UnsupportedEncodingException -> 0x01bd }
            java.lang.String r0 = a(r9)     // Catch:{ UnsupportedEncodingException -> 0x01bd }
            r7.<init>(r14, r0)     // Catch:{ UnsupportedEncodingException -> 0x01bd }
            com.e.a.a.j r5 = new com.e.a.a.j     // Catch:{ f -> 0x017e }
            r9 = 0
            r6 = r13
            r8 = r3
            r10 = r15
            r5.<init>(r6, r7, r8, r9, r10)     // Catch:{ f -> 0x017e }
            r12.b = r5     // Catch:{ f -> 0x017e }
            goto L_0x009f
        L_0x017e:
            r0 = move-exception
            java.lang.String r7 = r0.a()
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            java.lang.String r1 = "Encoding declaration of "
            r0.<init>(r1)
            java.lang.StringBuffer r0 = r0.append(r7)
            java.lang.String r1 = " is different that assumed "
            java.lang.StringBuffer r0 = r0.append(r1)
            java.lang.StringBuffer r0 = r0.append(r4)
            java.lang.String r1 = " so restarting the parsing with the new encoding"
            java.lang.StringBuffer r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.e.a.a.m.a(r0, r13)
            r14.reset()
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ UnsupportedEncodingException -> 0x01dd }
            java.lang.String r0 = a(r7)     // Catch:{ UnsupportedEncodingException -> 0x01dd }
            r2.<init>(r14, r0)     // Catch:{ UnsupportedEncodingException -> 0x01dd }
            com.e.a.a.j r0 = new com.e.a.a.j
            r4 = 0
            r1 = r13
            r5 = r15
            r0.<init>(r1, r2, r3, r4, r5)
            r12.b = r0
            goto L_0x009f
        L_0x01bd:
            r0 = move-exception
            com.e.a.a.k r5 = new com.e.a.a.k     // Catch:{ f -> 0x017e }
            r7 = 1
            r8 = 0
            java.lang.StringBuffer r0 = new java.lang.StringBuffer     // Catch:{ f -> 0x017e }
            java.lang.String r1 = "\""
            r0.<init>(r1)     // Catch:{ f -> 0x017e }
            java.lang.StringBuffer r0 = r0.append(r9)     // Catch:{ f -> 0x017e }
            java.lang.String r1 = "\" is not a supported encoding"
            java.lang.StringBuffer r0 = r0.append(r1)     // Catch:{ f -> 0x017e }
            java.lang.String r10 = r0.toString()     // Catch:{ f -> 0x017e }
            r11 = 0
            r6 = r13
            r5.<init>(r6, r7, r8, r9, r10, r11)     // Catch:{ f -> 0x017e }
            throw r5     // Catch:{ f -> 0x017e }
        L_0x01dd:
            r0 = move-exception
            com.e.a.a.k r0 = new com.e.a.a.k
            r2 = 1
            r3 = 0
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            java.lang.String r4 = "\""
            r1.<init>(r4)
            java.lang.StringBuffer r1 = r1.append(r7)
            java.lang.String r4 = "\" is not a supported encoding"
            java.lang.StringBuffer r1 = r1.append(r4)
            java.lang.String r5 = r1.toString()
            r6 = 0
            r1 = r13
            r4 = r7
            r0.<init>(r1, r2, r3, r4, r5, r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.e.a.a.i.<init>(java.lang.String, java.io.InputStream, com.e.a.a.l):void");
    }

    private static String a(byte b2) {
        String hexString = Integer.toHexString(b2);
        switch (hexString.length()) {
            case 1:
                return new StringBuffer("0").append(hexString).toString();
            case 2:
                return hexString;
            default:
                return hexString.substring(hexString.length() - 2);
        }
    }

    private static String a(String str) {
        return str.toLowerCase().equals("utf8") ? "UTF-8" : str;
    }

    private static boolean a(byte[] bArr, int i) {
        return bArr[0] == ((byte) (i >>> 24)) && bArr[1] == ((byte) ((i >>> 16) & PurchaseCode.AUTH_INVALID_APP)) && bArr[2] == ((byte) ((i >>> 8) & PurchaseCode.AUTH_INVALID_APP)) && bArr[3] == ((byte) (i & PurchaseCode.AUTH_INVALID_APP));
    }

    private static boolean a(byte[] bArr, short s) {
        return bArr[0] == ((byte) (s >>> 8)) && bArr[1] == ((byte) (s & 255));
    }

    public final String toString() {
        return this.b.toString();
    }
}
