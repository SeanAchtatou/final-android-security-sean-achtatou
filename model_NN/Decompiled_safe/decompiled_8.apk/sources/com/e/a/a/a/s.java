package com.e.a.a.a;

import com.baidu.location.BDLocation;
import com.jcraft.jzlib.JZlib;
import com.sina.sdk.api.message.InviteApi;

public final class s {

    /* renamed from: a  reason: collision with root package name */
    private final n f623a;
    private final k b;
    private final boolean c;

    static {
        new s(y.f627a, z.f628a);
    }

    s(aa aaVar, boolean z, r rVar) {
        k wVar;
        int i;
        int i2;
        this.c = z;
        switch (rVar.f622a) {
            case JZlib.Z_DATA_ERROR /*-3*/:
                if (rVar.c.equals(InviteApi.KEY_TEXT)) {
                    if (rVar.a() == 40 && rVar.a() == 41) {
                        this.f623a = x.f626a;
                        break;
                    } else {
                        throw new ab(aaVar, "after text", rVar, "()");
                    }
                } else {
                    this.f623a = new l(rVar.c);
                    break;
                }
                break;
            case 42:
                this.f623a = a.f611a;
                break;
            case 46:
                if (rVar.a() != 46) {
                    rVar.b();
                    this.f623a = y.f627a;
                    break;
                } else {
                    this.f623a = p.f620a;
                    break;
                }
            case 64:
                if (rVar.a() == -3) {
                    this.f623a = new j(rVar.c);
                    break;
                } else {
                    throw new ab(aaVar, "after @ in node test", rVar, "name");
                }
            default:
                throw new ab(aaVar, "at begininning of step", rVar, "'.' or '*' or name");
        }
        if (rVar.a() == 91) {
            rVar.a();
            switch (rVar.f622a) {
                case JZlib.Z_DATA_ERROR /*-3*/:
                    if (rVar.c.equals(InviteApi.KEY_TEXT)) {
                        if (rVar.a() == 40) {
                            if (rVar.a() == 41) {
                                switch (rVar.a()) {
                                    case 33:
                                        rVar.a();
                                        if (rVar.f622a != 61) {
                                            throw new ab(aaVar, "after !", rVar, "=");
                                        }
                                        rVar.a();
                                        if (rVar.f622a == 34 || rVar.f622a == 39) {
                                            String str = rVar.c;
                                            rVar.a();
                                            wVar = new w(str);
                                            break;
                                        } else {
                                            throw new ab(aaVar, "right hand side of !=", rVar, "quoted string");
                                        }
                                        break;
                                    case BDLocation.TypeGpsLocation:
                                        rVar.a();
                                        if (rVar.f622a == 34 || rVar.f622a == 39) {
                                            String str2 = rVar.c;
                                            rVar.a();
                                            wVar = new u(str2);
                                            break;
                                        } else {
                                            throw new ab(aaVar, "right hand side of equals", rVar, "quoted string");
                                        }
                                        break;
                                    default:
                                        wVar = v.f625a;
                                        break;
                                }
                            } else {
                                throw new ab(aaVar, "after text(", rVar, ")");
                            }
                        } else {
                            throw new ab(aaVar, "after text", rVar, "(");
                        }
                    } else {
                        throw new ab(aaVar, "at beginning of expression", rVar, "text()");
                    }
                case JZlib.Z_STREAM_ERROR /*-2*/:
                    int i3 = rVar.b;
                    rVar.a();
                    wVar = new q(i3);
                    break;
                case 64:
                    if (rVar.a() == -3) {
                        String str3 = rVar.c;
                        switch (rVar.a()) {
                            case 33:
                                rVar.a();
                                if (rVar.f622a != 61) {
                                    throw new ab(aaVar, "after !", rVar, "=");
                                }
                                rVar.a();
                                if (rVar.f622a == 34 || rVar.f622a == 39) {
                                    String str4 = rVar.c;
                                    rVar.a();
                                    wVar = new h(str3, str4);
                                    break;
                                } else {
                                    throw new ab(aaVar, "right hand side of !=", rVar, "quoted string");
                                }
                                break;
                            case 60:
                                rVar.a();
                                if (rVar.f622a == 34 || rVar.f622a == 39) {
                                    i2 = Integer.parseInt(rVar.c);
                                } else if (rVar.f622a == -2) {
                                    i2 = rVar.b;
                                } else {
                                    throw new ab(aaVar, "right hand side of less-than", rVar, "quoted string or number");
                                }
                                rVar.a();
                                wVar = new g(str3, i2);
                                break;
                            case BDLocation.TypeGpsLocation:
                                rVar.a();
                                if (rVar.f622a == 34 || rVar.f622a == 39) {
                                    String str5 = rVar.c;
                                    rVar.a();
                                    wVar = new c(str3, str5);
                                    break;
                                } else {
                                    throw new ab(aaVar, "right hand side of equals", rVar, "quoted string");
                                }
                                break;
                            case BDLocation.TypeCriteriaException:
                                rVar.a();
                                if (rVar.f622a == 34 || rVar.f622a == 39) {
                                    i = Integer.parseInt(rVar.c);
                                } else if (rVar.f622a == -2) {
                                    i = rVar.b;
                                } else {
                                    throw new ab(aaVar, "right hand side of greater-than", rVar, "quoted string or number");
                                }
                                rVar.a();
                                wVar = new f(str3, i);
                                break;
                            default:
                                wVar = new d(str3);
                                break;
                        }
                    } else {
                        throw new ab(aaVar, "after @", rVar, "name");
                    }
                default:
                    throw new ab(aaVar, "at beginning of expression", rVar, "@, number, or text()");
            }
            this.b = wVar;
            if (rVar.f622a != 93) {
                throw new ab(aaVar, "after predicate expression", rVar, "]");
            }
            rVar.a();
            return;
        }
        this.b = z.f628a;
    }

    private s(n nVar, k kVar) {
        this.f623a = nVar;
        this.b = kVar;
        this.c = false;
    }

    public final boolean a() {
        return this.c;
    }

    public final boolean b() {
        return this.f623a.a();
    }

    public final n c() {
        return this.f623a;
    }

    public final k d() {
        return this.b;
    }

    public final String toString() {
        return new StringBuffer().append(this.f623a.toString()).append(this.b.toString()).toString();
    }
}
