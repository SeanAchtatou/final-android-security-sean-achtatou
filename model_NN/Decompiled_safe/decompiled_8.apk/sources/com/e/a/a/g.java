package com.e.a.a;

import com.baidu.location.BDLocation;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

public abstract class g {

    /* renamed from: a  reason: collision with root package name */
    private b f632a = null;
    private d b = null;
    private g c = null;
    private g d = null;
    private int e = 0;

    protected static void a(Writer writer, String str) {
        String str2;
        int i = 0;
        int length = str.length();
        for (int i2 = 0; i2 < length; i2++) {
            char charAt = str.charAt(i2);
            if (charAt < 128) {
                switch (charAt) {
                    case '\"':
                        str2 = "&quot;";
                        break;
                    case '&':
                        str2 = "&amp;";
                        break;
                    case '\'':
                        str2 = "&#39;";
                        break;
                    case '<':
                        str2 = "&lt;";
                        break;
                    case BDLocation.TypeCriteriaException:
                        str2 = "&gt;";
                        break;
                    default:
                        str2 = null;
                        break;
                }
            } else {
                str2 = new StringBuffer("&#").append((int) charAt).append(";").toString();
            }
            if (str2 != null) {
                writer.write(str, i, i2 - i);
                writer.write(str2);
                i = i2 + 1;
            }
        }
        if (i < length) {
            writer.write(str, i, length - i);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(b bVar) {
        this.f632a = bVar;
    }

    /* access modifiers changed from: package-private */
    public abstract void a(Writer writer);

    /* access modifiers changed from: package-private */
    public void b() {
        this.e = 0;
        if (this.f632a != null) {
            this.f632a.b();
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(d dVar) {
        this.b = dVar;
    }

    /* access modifiers changed from: package-private */
    public abstract void b(Writer writer);

    /* access modifiers changed from: protected */
    public abstract int c();

    /* access modifiers changed from: package-private */
    public final void c(g gVar) {
        this.c = gVar;
        if (gVar != null) {
            gVar.d = this;
        }
    }

    public abstract Object clone();

    public final b f() {
        return this.f632a;
    }

    public final d g() {
        return this.b;
    }

    public final g h() {
        return this.c;
    }

    public int hashCode() {
        if (this.e == 0) {
            this.e = c();
        }
        return this.e;
    }

    public final g i() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public final void j() {
        if (this.c != null) {
            this.c.d = this.d;
        }
        if (this.d != null) {
            this.d.c = this.c;
        }
        this.d = null;
        this.c = null;
    }

    public String toString() {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(byteArrayOutputStream);
            a(outputStreamWriter);
            outputStreamWriter.flush();
            return new String(byteArrayOutputStream.toByteArray());
        } catch (IOException e2) {
            return super.toString();
        }
    }
}
