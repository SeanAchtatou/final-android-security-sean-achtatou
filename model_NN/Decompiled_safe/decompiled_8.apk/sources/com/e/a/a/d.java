package com.e.a.a;

import com.e.a.a.a.aa;
import com.e.a.a.a.ab;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public final class d extends g {

    /* renamed from: a  reason: collision with root package name */
    private g f630a = null;
    private g b = null;
    private Hashtable c = null;
    private Vector d = null;
    private String e = null;

    d() {
    }

    private d(String str) {
        this.e = o.a(str);
    }

    public final String a() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public final void a(g gVar) {
        d g = gVar.g();
        if (g != null) {
            g gVar2 = g.f630a;
            while (true) {
                if (gVar2 == null) {
                    break;
                } else if (gVar2.equals(gVar)) {
                    if (g.f630a == gVar2) {
                        g.f630a = gVar2.i();
                    }
                    if (g.b == gVar2) {
                        g.b = gVar2.h();
                    }
                    gVar2.j();
                    gVar2.b((d) null);
                    gVar2.a((b) null);
                } else {
                    gVar2 = gVar2.i();
                }
            }
        }
        gVar.c(this.b);
        if (this.f630a == null) {
            this.f630a = gVar;
        }
        gVar.b(this);
        this.b = gVar;
        gVar.a(f());
    }

    /* access modifiers changed from: package-private */
    public final void a(Writer writer) {
        for (g gVar = this.f630a; gVar != null; gVar = gVar.i()) {
            gVar.a(writer);
        }
    }

    public final void a(String str) {
        this.e = o.a(str);
        b();
    }

    public final void a(String str, String str2) {
        if (this.c == null) {
            this.c = new Hashtable();
            this.d = new Vector();
        }
        if (this.c.get(str) == null) {
            this.d.addElement(str);
        }
        this.c.put(str, str2);
        b();
    }

    public final String b(String str) {
        if (this.c == null) {
            return null;
        }
        return (String) this.c.get(str);
    }

    public final void b(g gVar) {
        boolean z;
        d dVar = this;
        while (true) {
            if (gVar == dVar) {
                z = false;
                break;
            }
            dVar = dVar.g();
            if (dVar == null) {
                z = true;
                break;
            }
        }
        a(!z ? (d) gVar.clone() : gVar);
        b();
    }

    public final void b(Writer writer) {
        writer.write(new StringBuffer("<").append(this.e).toString());
        if (this.d != null) {
            Enumeration elements = this.d.elements();
            while (elements.hasMoreElements()) {
                String str = (String) elements.nextElement();
                writer.write(new StringBuffer(" ").append(str).append("=\"").toString());
                g.a(writer, (String) this.c.get(str));
                writer.write("\"");
            }
        }
        if (this.f630a == null) {
            writer.write("/>");
            return;
        }
        writer.write(">");
        for (g gVar = this.f630a; gVar != null; gVar = gVar.i()) {
            gVar.b(writer);
        }
        writer.write(new StringBuffer("</").append(this.e).append(">").toString());
    }

    /* access modifiers changed from: protected */
    public final int c() {
        int i;
        int hashCode = this.e.hashCode();
        if (this.c != null) {
            Enumeration keys = this.c.keys();
            while (true) {
                i = hashCode;
                if (!keys.hasMoreElements()) {
                    break;
                }
                String str = (String) keys.nextElement();
                int hashCode2 = (i * 31) + str.hashCode();
                hashCode = ((String) this.c.get(str)).hashCode() + (hashCode2 * 31);
            }
        } else {
            i = hashCode;
        }
        for (g gVar = this.f630a; gVar != null; gVar = gVar.i()) {
            i = (i * 31) + gVar.hashCode();
        }
        return i;
    }

    public final String c(String str) {
        try {
            aa a2 = aa.a(str);
            if (a2.b()) {
                return new t(this, a2).h();
            }
            throw new ab(a2, new StringBuffer("\"").append(a2).append("\" evaluates to ").append("evaluates to element not string").toString());
        } catch (ab e2) {
            throw new k("XPath problem", e2);
        }
    }

    public final Object clone() {
        d dVar = new d(this.e);
        if (this.d != null) {
            Enumeration elements = this.d.elements();
            while (elements.hasMoreElements()) {
                String str = (String) elements.nextElement();
                dVar.a(str, (String) this.c.get(str));
            }
        }
        for (g gVar = this.f630a; gVar != null; gVar = gVar.i()) {
            dVar.b((g) gVar.clone());
        }
        return dVar;
    }

    public final g d() {
        return this.f630a;
    }

    public final g e() {
        return this.b;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof d)) {
            return false;
        }
        d dVar = (d) obj;
        if (!this.e.equals(dVar.e)) {
            return false;
        }
        if ((this.c == null ? 0 : this.c.size()) != (dVar.c == null ? 0 : dVar.c.size())) {
            return false;
        }
        if (this.c != null) {
            Enumeration keys = this.c.keys();
            while (keys.hasMoreElements()) {
                String str = (String) keys.nextElement();
                if (!((String) this.c.get(str)).equals((String) dVar.c.get(str))) {
                    return false;
                }
            }
        }
        g gVar = this.f630a;
        g gVar2 = dVar.f630a;
        while (gVar != null) {
            if (!gVar.equals(gVar2)) {
                return false;
            }
            gVar = gVar.i();
            gVar2 = gVar2.i();
        }
        return true;
    }
}
