package com.e.a.a.a;

public abstract class i extends e {

    /* renamed from: a  reason: collision with root package name */
    private final int f616a;

    i(String str, int i) {
        super(str);
        this.f616a = i;
    }

    public final double a() {
        return (double) this.f616a;
    }

    /* access modifiers changed from: protected */
    public final String a(String str) {
        return new StringBuffer("[").append(super.toString()).append(str).append("'").append(this.f616a).append("']").toString();
    }
}
