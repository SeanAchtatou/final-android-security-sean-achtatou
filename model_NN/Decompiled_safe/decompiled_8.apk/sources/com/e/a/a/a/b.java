package com.e.a.a.a;

import com.e.a.a.o;

public abstract class b extends e {

    /* renamed from: a  reason: collision with root package name */
    private final String f614a;

    b(String str, String str2) {
        super(str);
        this.f614a = o.a(str2);
    }

    public final String a() {
        return this.f614a;
    }

    /* access modifiers changed from: protected */
    public final String a(String str) {
        return new StringBuffer("[").append(super.toString()).append(str).append("'").append(this.f614a).append("']").toString();
    }
}
