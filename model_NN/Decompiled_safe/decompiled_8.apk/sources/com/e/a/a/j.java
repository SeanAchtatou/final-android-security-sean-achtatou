package com.e.a.a;

import com.amap.mapapi.poisearch.PoiTypeDef;
import com.mapabc.minimap.map.vmap.NativeMapEngine;
import java.io.Reader;
import java.util.Hashtable;
import mm.purchasesdk.PurchaseCode;

final class j implements n {
    private static final char[] b = {'.', '-', '_', ':'};
    private static final boolean[] c = new boolean[NativeMapEngine.MAX_ICON_SIZE];
    private static final char[] d = "<!--".toCharArray();
    private static final char[] e = "-->".toCharArray();
    private static final char[] f = "<?".toCharArray();
    private static final char[] g = "?>".toCharArray();
    private static final char[] h = "<!DOCTYPE".toCharArray();
    private static final char[] i = "<?xml".toCharArray();
    private static final char[] j = "encoding".toCharArray();
    private static final char[] k = "version".toCharArray();
    private static final char[] l = {'_', '.', ':', '-'};
    private static final char[] m = "<!".toCharArray();
    private static final char[] n = "&#".toCharArray();
    private static final char[] o = "<!ENTITY".toCharArray();
    private static final char[] p = "NDATA".toCharArray();
    private static final char[] q = "SYSTEM".toCharArray();
    private static final char[] r = "PUBLIC".toCharArray();
    private static final char[] s = "<![CDATA[".toCharArray();
    private static final char[] t = "]]>".toCharArray();
    private static final char[] u = "/>".toCharArray();
    private static final char[] v = "</".toCharArray();
    private final Hashtable A;
    private final m B;
    private final String C;
    private int D;
    private boolean E;
    private final char[] F;
    private int G;
    private int H;
    private boolean I;
    private final char[] J;
    private int K;
    private final l L;
    private String w;
    private String x;
    private final Reader y;
    private final Hashtable z;

    static {
        for (char c2 = 0; c2 < 128; c2 = (char) (c2 + 1)) {
            c[c2] = d(c2);
        }
    }

    public j(String str, Reader reader, m mVar, String str2, l lVar) {
        this(str, reader, mVar, str2, lVar, (byte) 0);
    }

    private j(String str, Reader reader, m mVar, String str2, l lVar, byte b2) {
        String str3 = null;
        this.x = null;
        this.z = new Hashtable();
        this.A = new Hashtable();
        this.D = -2;
        this.E = false;
        this.G = 0;
        this.H = 0;
        this.I = false;
        this.J = new char[PurchaseCode.AUTH_INVALID_APP];
        this.K = -1;
        this.K = 1;
        this.B = mVar == null ? n.f635a : mVar;
        this.C = str2 != null ? str2.toLowerCase() : str3;
        this.z.put("lt", "<");
        this.z.put("gt", ">");
        this.z.put("amp", "&");
        this.z.put("apos", "'");
        this.z.put("quot", "\"");
        this.y = reader;
        this.F = new char[1024];
        f();
        this.w = str;
        this.L = lVar;
        this.L.a(this);
        v();
        l lVar2 = this.L;
        d D2 = D();
        if (this.x != null && !this.x.equals(D2.a())) {
            m mVar2 = this.B;
            m.b(new StringBuffer("DOCTYPE name \"").append(this.x).append("\" not same as tag name, \"").append(D2.a()).append("\" of root element").toString(), this.w, this.K);
        }
        while (p()) {
            q();
        }
        if (this.y != null) {
            this.y.close();
        }
        l lVar3 = this.L;
    }

    private String A() {
        a('%');
        String l2 = l();
        String str = (String) this.A.get(l2);
        if (str == null) {
            str = PoiTypeDef.All;
            m mVar = this.B;
            m.b(new StringBuffer("No declaration of %").append(l2).append(";").toString(), this.w, this.K);
        }
        a(';');
        return str;
    }

    private String B() {
        if (b(q)) {
            a(q);
        } else if (b(r)) {
            a(r);
            k();
            o();
        } else {
            throw new k(this, "expecting \"SYSTEM\" or \"PUBLIC\" while reading external ID");
        }
        k();
        o();
        return "(WARNING: external ID not read)";
    }

    private void C() {
        int i2 = 0;
        while (!b('<') && !b('&') && !b(t)) {
            this.J[i2] = g();
            if (this.J[i2] == 13 && h() == 10) {
                this.J[i2] = g();
            }
            i2++;
            if (i2 == 255) {
                this.L.a(this.J, PurchaseCode.AUTH_INVALID_APP);
                i2 = 0;
            }
        }
        if (i2 > 0) {
            this.L.a(this.J, i2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.e.a.a.j.a(char, char):boolean
     arg types: [int, int]
     candidates:
      com.e.a.a.j.a(char, char[]):boolean
      com.e.a.a.j.a(char, char):boolean */
    private final d D() {
        d dVar = new d();
        a('<');
        dVar.a(l());
        while (j()) {
            k();
            if (!a('/', '>')) {
                String l2 = l();
                w();
                char i2 = i();
                StringBuffer stringBuffer = new StringBuffer();
                while (!b(i2)) {
                    if (b('&')) {
                        stringBuffer.append(z());
                    } else {
                        stringBuffer.append(g());
                    }
                }
                a(i2);
                String stringBuffer2 = stringBuffer.toString();
                if (dVar.b(l2) != null) {
                    m mVar = this.B;
                    m.b(new StringBuffer("Element ").append(this).append(" contains attribute ").append(l2).append("more than once").toString(), this.w, this.K);
                }
                dVar.a(l2, stringBuffer2);
            }
        }
        if (j()) {
            k();
        }
        boolean b2 = b('>');
        if (b2) {
            a('>');
        } else {
            a(u);
        }
        this.L.a(dVar);
        if (b2) {
            E();
            a(v);
            String l3 = l();
            if (!l3.equals(dVar.a())) {
                m mVar2 = this.B;
                m.b(new StringBuffer("end tag (").append(l3).append(") does not match begin tag (").append(dVar.a()).append(")").toString(), this.w, this.K);
            }
            if (j()) {
                k();
            }
            a('>');
        }
        this.L.b();
        return dVar;
    }

    private void E() {
        boolean z2;
        StringBuffer stringBuffer;
        int i2;
        C();
        boolean z3 = true;
        while (z3) {
            if (!b(v)) {
                if (b('&')) {
                    char[] z4 = z();
                    this.L.a(z4, z4.length);
                    z2 = z3;
                } else if (b(s)) {
                    StringBuffer stringBuffer2 = null;
                    a(s);
                    int i3 = 0;
                    while (!b(t)) {
                        if (i3 >= 255) {
                            if (stringBuffer2 == null) {
                                stringBuffer2 = new StringBuffer(i3);
                                stringBuffer2.append(this.J, 0, i3);
                            } else {
                                stringBuffer2.append(this.J, 0, i3);
                            }
                            stringBuffer = stringBuffer2;
                            i2 = 0;
                        } else {
                            int i4 = i3;
                            stringBuffer = stringBuffer2;
                            i2 = i4;
                        }
                        int i5 = i2 + 1;
                        this.J[i2] = g();
                        stringBuffer2 = stringBuffer;
                        i3 = i5;
                    }
                    a(t);
                    if (stringBuffer2 != null) {
                        stringBuffer2.append(this.J, 0, i3);
                        char[] charArray = stringBuffer2.toString().toCharArray();
                        this.L.a(charArray, charArray.length);
                        z2 = z3;
                    } else {
                        this.L.a(this.J, i3);
                        z2 = z3;
                    }
                } else if (u()) {
                    t();
                    z2 = z3;
                } else if (s()) {
                    r();
                    z2 = z3;
                } else if (b('<')) {
                    D();
                    z2 = z3;
                }
                C();
                z3 = z2;
            }
            z2 = false;
            C();
            z3 = z2;
        }
    }

    private int a(int i2) {
        int i3 = 0;
        if (this.I) {
            return -1;
        }
        if (this.F.length - this.G < i2) {
            for (int i4 = 0; this.G + i4 < this.H; i4++) {
                this.F[i4] = this.F[this.G + i4];
            }
            int i5 = this.H - this.G;
            this.H = i5;
            this.G = 0;
            i3 = i5;
        }
        int f2 = f();
        if (f2 != -1) {
            return i3 + f2;
        }
        if (i3 == 0) {
            return -1;
        }
        return i3;
    }

    private final void a(char c2) {
        char g2 = g();
        if (g2 != c2) {
            throw new k(this, g2, c2);
        }
    }

    private final void a(char[] cArr) {
        int length = cArr.length;
        if (this.H - this.G >= length || a(length) > 0) {
            this.D = this.F[this.H - 1];
            if (this.H - this.G < length) {
                throw new k(this, "end of XML file", cArr);
            }
            for (int i2 = 0; i2 < length; i2++) {
                if (this.F[this.G + i2] != cArr[i2]) {
                    throw new k(this, new String(this.F, this.G, length), cArr);
                }
            }
            this.G += length;
            return;
        }
        this.D = -1;
        throw new k(this, "end of XML file", cArr);
    }

    private final boolean a(char c2, char c3) {
        if (this.G >= this.H && f() == -1) {
            return false;
        }
        char c4 = this.F[this.G];
        return c4 == c2 || c4 == c3;
    }

    private static final boolean a(char c2, char[] cArr) {
        for (char c3 : cArr) {
            if (c2 == c3) {
                return true;
            }
        }
        return false;
    }

    private final boolean b(char c2) {
        if (this.G < this.H || f() != -1) {
            return this.F[this.G] == c2;
        }
        throw new k(this, "unexpected end of expression.");
    }

    private final boolean b(char[] cArr) {
        int length = cArr.length;
        if (this.H - this.G >= length || a(length) > 0) {
            this.D = this.F[this.H - 1];
            if (this.H - this.G < length) {
                return false;
            }
            for (int i2 = 0; i2 < length; i2++) {
                if (this.F[this.G + i2] != cArr[i2]) {
                    return false;
                }
            }
            return true;
        }
        this.D = -1;
        return false;
    }

    private static boolean c(char c2) {
        return "abcdefghijklmnopqrstuvwxyz".indexOf(Character.toLowerCase(c2)) != -1;
    }

    static String d() {
        return PoiTypeDef.All;
    }

    private static boolean d(char c2) {
        boolean z2;
        if (!Character.isDigit(c2) && !c(c2) && !a(c2, b)) {
            switch (c2) {
                case 183:
                case 720:
                case 721:
                case 903:
                case 1600:
                case 3654:
                case 3782:
                case 12293:
                case 12337:
                case 12338:
                case 12339:
                case 12340:
                case 12341:
                case 12445:
                case 12446:
                case 12540:
                case 12541:
                case 12542:
                    z2 = true;
                    break;
                default:
                    z2 = false;
                    break;
            }
            if (!z2) {
                return false;
            }
        }
        return true;
    }

    private int f() {
        if (this.I) {
            return -1;
        }
        if (this.H == this.F.length) {
            this.H = 0;
            this.G = 0;
        }
        int read = this.y.read(this.F, this.H, this.F.length - this.H);
        if (read <= 0) {
            this.I = true;
            return -1;
        }
        this.H += read;
        return read;
    }

    private final char g() {
        if (this.G < this.H || f() != -1) {
            if (this.F[this.G] == 10) {
                this.K++;
            }
            char[] cArr = this.F;
            int i2 = this.G;
            this.G = i2 + 1;
            return cArr[i2];
        }
        throw new k(this, "unexpected end of expression.");
    }

    private final char h() {
        if (this.G < this.H || f() != -1) {
            return this.F[this.G];
        }
        throw new k(this, "unexpected end of expression.");
    }

    private final char i() {
        char g2 = g();
        if (g2 == '\'' || g2 == '\"') {
            return g2;
        }
        throw new k(this, g2, new char[]{'\'', '\"'});
    }

    private final boolean j() {
        if (this.G >= this.H && f() == -1) {
            return false;
        }
        char c2 = this.F[this.G];
        return c2 == ' ' || c2 == 9 || c2 == 13 || c2 == 10;
    }

    private final void k() {
        char g2 = g();
        if (g2 == ' ' || g2 == 9 || g2 == 13 || g2 == 10) {
            while (j()) {
                g();
            }
            return;
        }
        throw new k(this, g2, new char[]{' ', 9, 13, 10});
    }

    private final String l() {
        StringBuffer stringBuffer;
        int i2;
        StringBuffer stringBuffer2 = null;
        char[] cArr = this.J;
        int i3 = 1;
        char g2 = g();
        if (c(g2) || g2 == '_' || g2 == ':') {
            cArr[0] = g2;
            while (true) {
                char h2 = h();
                if (!(h2 < 128 ? c[h2] : d(h2))) {
                    break;
                }
                if (i3 >= 255) {
                    if (stringBuffer2 == null) {
                        stringBuffer2 = new StringBuffer(i3);
                        stringBuffer2.append(this.J, 0, i3);
                    } else {
                        stringBuffer2.append(this.J, 0, i3);
                    }
                    stringBuffer = stringBuffer2;
                    i2 = 0;
                } else {
                    int i4 = i3;
                    stringBuffer = stringBuffer2;
                    i2 = i4;
                }
                int i5 = i2 + 1;
                this.J[i2] = g();
                stringBuffer2 = stringBuffer;
                i3 = i5;
            }
            if (stringBuffer2 == null) {
                return o.a(new String(this.J, 0, i3));
            }
            stringBuffer2.append(this.J, 0, i3);
            return stringBuffer2.toString();
        }
        throw new k(this, g2, "letter, underscore, colon");
    }

    private final String m() {
        char i2 = i();
        StringBuffer stringBuffer = new StringBuffer();
        while (!b(i2)) {
            if (b('%')) {
                stringBuffer.append(A());
            } else if (b('&')) {
                stringBuffer.append(z());
            } else {
                stringBuffer.append(g());
            }
        }
        a(i2);
        return stringBuffer.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.e.a.a.j.a(char, char):boolean
     arg types: [int, int]
     candidates:
      com.e.a.a.j.a(char, char[]):boolean
      com.e.a.a.j.a(char, char):boolean */
    private final boolean n() {
        return a('\'', '\"');
    }

    private final void o() {
        char g2 = g();
        while (h() != g2) {
            g();
        }
        a(g2);
    }

    private boolean p() {
        return b(d) || b(f) || j();
    }

    private void q() {
        if (b(d)) {
            r();
        } else if (b(f)) {
            t();
        } else if (j()) {
            k();
        } else {
            throw new k(this, "expecting comment or processing instruction or space");
        }
    }

    private final void r() {
        a(d);
        while (!b(e)) {
            g();
        }
        a(e);
    }

    private final boolean s() {
        return b(d);
    }

    private final void t() {
        a(f);
        while (!b(g)) {
            g();
        }
        a(g);
    }

    private final boolean u() {
        return b(f);
    }

    private void v() {
        if (b(i)) {
            a(i);
            k();
            a(k);
            w();
            char i2 = i();
            g();
            while (true) {
                char h2 = h();
                if (!(Character.isDigit(h2) || ('a' <= h2 && h2 <= 'z') || (('Z' <= h2 && h2 <= 'Z') || a(h2, l)))) {
                    break;
                }
                g();
            }
            a(i2);
            if (j()) {
                k();
            }
            if (b(j)) {
                a(j);
                w();
                char i3 = i();
                StringBuffer stringBuffer = new StringBuffer();
                while (!b(i3)) {
                    stringBuffer.append(g());
                }
                a(i3);
                String stringBuffer2 = stringBuffer.toString();
                if (this.C != null && !stringBuffer2.toLowerCase().equals(this.C)) {
                    throw new f(this.w, stringBuffer2, this.C);
                }
            }
            while (!b(g)) {
                g();
            }
            a(g);
        }
        while (p()) {
            q();
        }
        if (b(h)) {
            x();
            while (p()) {
                q();
            }
        }
    }

    private final void w() {
        if (j()) {
            k();
        }
        a('=');
        if (j()) {
            k();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.e.a.a.j.a(char, char):boolean
     arg types: [int, int]
     candidates:
      com.e.a.a.j.a(char, char[]):boolean
      com.e.a.a.j.a(char, char):boolean */
    private void x() {
        String B2;
        a(h);
        k();
        this.x = l();
        if (j()) {
            k();
            if (!b('>') && !b('[')) {
                this.E = true;
                B();
                if (j()) {
                    k();
                }
            }
        }
        if (b('[')) {
            g();
            while (!b(']')) {
                if (b('%') || j()) {
                    if (b('%')) {
                        A();
                    } else {
                        k();
                    }
                } else if (b(f)) {
                    t();
                } else if (b(d)) {
                    r();
                } else if (b(o)) {
                    a(o);
                    k();
                    if (b('%')) {
                        a('%');
                        k();
                        String l2 = l();
                        k();
                        this.A.put(l2, n() ? m() : B());
                    } else {
                        String l3 = l();
                        k();
                        if (n()) {
                            B2 = m();
                        } else {
                            if (b(q) || b(r)) {
                                B2 = B();
                                if (j()) {
                                    k();
                                }
                                if (b(p)) {
                                    a(p);
                                    k();
                                    l();
                                }
                            } else {
                                throw new k(this, "expecting double-quote, \"PUBLIC\" or \"SYSTEM\" while reading entity declaration");
                            }
                        }
                        this.z.put(l3, B2);
                    }
                    if (j()) {
                        k();
                    }
                    a('>');
                } else if (b(m)) {
                    while (!b('>')) {
                        if (a('\'', '\"')) {
                            char g2 = g();
                            while (!b(g2)) {
                                g();
                            }
                            a(g2);
                        } else {
                            g();
                        }
                    }
                    a('>');
                } else {
                    throw new k(this, "expecting processing instruction, comment, or \"<!\"");
                }
            }
            a(']');
            if (j()) {
                k();
            }
        }
        a('>');
    }

    private char y() {
        a(n);
        int i2 = 10;
        if (b('x')) {
            g();
            i2 = 16;
        }
        int i3 = 0;
        while (!b(';')) {
            int i4 = i3 + 1;
            this.J[i3] = g();
            if (i4 >= 255) {
                m mVar = this.B;
                m.b("Tmp buffer overflow on readCharRef", this.w, this.K);
                return ' ';
            }
            i3 = i4;
        }
        a(';');
        String str = new String(this.J, 0, i3);
        try {
            return (char) Integer.parseInt(str, i2);
        } catch (NumberFormatException e2) {
            m mVar2 = this.B;
            m.b(new StringBuffer("\"").append(str).append("\" is not a valid ").append(i2 == 16 ? "hexadecimal" : "decimal").append(" number").toString(), this.w, this.K);
            return ' ';
        }
    }

    private final char[] z() {
        if (b(n)) {
            return new char[]{y()};
        }
        a('&');
        String l2 = l();
        String str = (String) this.z.get(l2);
        if (str == null) {
            str = PoiTypeDef.All;
            if (this.E) {
                m mVar = this.B;
                m.b(new StringBuffer("&").append(l2).append("; not found -- possibly defined in external DTD)").toString(), this.w, this.K);
            } else {
                m mVar2 = this.B;
                m.b(new StringBuffer("No declaration of &").append(l2).append(";").toString(), this.w, this.K);
            }
        }
        a(';');
        return str.toCharArray();
    }

    public final String a() {
        return this.w;
    }

    public final int b() {
        return this.K;
    }

    /* access modifiers changed from: package-private */
    public final int c() {
        return this.D;
    }

    /* access modifiers changed from: package-private */
    public final m e() {
        return this.B;
    }

    public final String toString() {
        return this.w;
    }
}
