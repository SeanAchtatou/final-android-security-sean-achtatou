package com.e.a.a;

public class k extends Exception {

    /* renamed from: a  reason: collision with root package name */
    private Throwable f634a;

    public k(j jVar, char c, char c2) {
        this(jVar, new StringBuffer("got '").append(c).append("' instead of expected '").append(c2).append("'").toString());
    }

    public k(j jVar, char c, String str) {
        this(jVar, new StringBuffer("got '").append(c).append("' instead of ").append(str).append(" as expected").toString());
    }

    public k(j jVar, char c, char[] cArr) {
        this(jVar, new StringBuffer("got '").append(c).append("' instead of ").append(a(cArr)).toString());
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public k(j jVar, String str) {
        this(jVar.a(), jVar.b(), jVar.c(), j.d(), str, (byte) 0);
        jVar.e();
    }

    private k(j jVar, String str, String str2) {
        this(jVar, new StringBuffer("got \"").append(str).append("\" instead of \"").append(str2).append("\" as expected").toString());
    }

    public k(j jVar, String str, char[] cArr) {
        this(jVar, str, new String(cArr));
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public k(String str, int i, int i2, String str2, String str3) {
        super(new StringBuffer().append(str).append("(").append(i).append("): \n").append(str2).append("\nLast character read was '").append(i2 == -1 ? "EOF" : new StringBuffer().append((char) i2).toString()).append("'\n").append(str3).toString());
        this.f634a = null;
    }

    public k(String str, int i, int i2, String str2, String str3, byte b) {
        this(str, i, i2, str2, str3);
        m.a(str3, str, i);
    }

    public k(String str, Throwable th) {
        super(new StringBuffer().append(str).append(" ").append(th).toString());
        this.f634a = null;
        this.f634a = th;
    }

    private static String a(char[] cArr) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(cArr[0]);
        for (int i = 1; i < cArr.length; i++) {
            stringBuffer.append(new StringBuffer("or ").append(cArr[i]).toString());
        }
        return stringBuffer.toString();
    }

    public Throwable getCause() {
        return this.f634a;
    }
}
