package com.e.a.a;

import java.util.Enumeration;
import java.util.NoSuchElementException;

final class e implements Enumeration {
    e() {
    }

    public final boolean hasMoreElements() {
        return false;
    }

    public final Object nextElement() {
        throw new NoSuchElementException();
    }
}
