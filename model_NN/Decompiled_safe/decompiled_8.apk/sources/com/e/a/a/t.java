package com.e.a.a;

import com.e.a.a.a.aa;
import com.e.a.a.a.ab;
import com.e.a.a.a.c;
import com.e.a.a.a.d;
import com.e.a.a.a.f;
import com.e.a.a.a.g;
import com.e.a.a.a.h;
import com.e.a.a.a.j;
import com.e.a.a.a.k;
import com.e.a.a.a.l;
import com.e.a.a.a.o;
import com.e.a.a.a.q;
import com.e.a.a.a.s;
import com.e.a.a.a.u;
import com.e.a.a.a.w;
import java.util.Enumeration;
import java.util.Vector;

final class t implements o {

    /* renamed from: a  reason: collision with root package name */
    private static final Boolean f639a = new Boolean(true);
    private static final Boolean b = new Boolean(false);
    private final h c;
    private Vector d;
    private Enumeration e;
    private Object f;
    private final u g;
    private g h;
    private boolean i;
    private aa j;

    private t(aa aaVar, g gVar) {
        this.c = new h();
        this.d = new Vector();
        this.e = null;
        this.f = null;
        this.g = new u((byte) 0);
        this.j = aaVar;
        this.h = gVar;
        this.d = new Vector(1);
        this.d.addElement(this.h);
        Enumeration c2 = aaVar.c();
        while (c2.hasMoreElements()) {
            s sVar = (s) c2.nextElement();
            this.i = sVar.a();
            this.e = null;
            sVar.c().a(this);
            this.e = this.c.a();
            this.d.removeAllElements();
            k d2 = sVar.d();
            while (this.e.hasMoreElements()) {
                this.f = this.e.nextElement();
                d2.a(this);
                if (this.g.a().booleanValue()) {
                    this.d.addElement(this.f);
                }
            }
        }
    }

    public t(b bVar, aa aaVar) {
        this(aaVar, bVar);
    }

    public t(d dVar, aa aaVar) {
        this(aaVar, dVar);
        if (aaVar.a()) {
            throw new ab(aaVar, "Cannot use element as context node for absolute xpath");
        }
    }

    private void a(d dVar) {
        int i2 = 0;
        for (g d2 = dVar.d(); d2 != null; d2 = d2.i()) {
            if (d2 instanceof d) {
                int i3 = i2 + 1;
                this.c.a(d2, i3);
                if (this.i) {
                    a((d) d2);
                }
                i2 = i3;
            }
        }
    }

    private void a(d dVar, String str) {
        int i2 = 0;
        for (g d2 = dVar.d(); d2 != null; d2 = d2.i()) {
            if (d2 instanceof d) {
                d dVar2 = (d) d2;
                if (dVar2.a() == str) {
                    i2++;
                    this.c.a(dVar2, i2);
                }
                if (this.i) {
                    a(dVar2, str);
                }
            }
        }
    }

    public final void a() {
        this.c.b();
        this.c.a(this.h, 1);
    }

    public final void a(c cVar) {
        if (!(this.f instanceof d)) {
            throw new ab(this.j, "Cannot test attribute of document");
        }
        this.g.a(cVar.a().equals(((d) this.f).b(cVar.b())) ? f639a : b);
    }

    public final void a(d dVar) {
        if (!(this.f instanceof d)) {
            throw new ab(this.j, "Cannot test attribute of document");
        }
        String b2 = ((d) this.f).b(dVar.b());
        this.g.a(b2 != null && b2.length() > 0 ? f639a : b);
    }

    public final void a(f fVar) {
        if (!(this.f instanceof d)) {
            throw new ab(this.j, "Cannot test attribute of document");
        }
        this.g.a((((double) Long.parseLong(((d) this.f).b(fVar.b()))) > fVar.a() ? 1 : (((double) Long.parseLong(((d) this.f).b(fVar.b()))) == fVar.a() ? 0 : -1)) > 0 ? f639a : b);
    }

    public final void a(g gVar) {
        if (!(this.f instanceof d)) {
            throw new ab(this.j, "Cannot test attribute of document");
        }
        this.g.a((((double) Long.parseLong(((d) this.f).b(gVar.b()))) > gVar.a() ? 1 : (((double) Long.parseLong(((d) this.f).b(gVar.b()))) == gVar.a() ? 0 : -1)) < 0 ? f639a : b);
    }

    public final void a(h hVar) {
        if (!(this.f instanceof d)) {
            throw new ab(this.j, "Cannot test attribute of document");
        }
        this.g.a(!hVar.a().equals(((d) this.f).b(hVar.b())) ? f639a : b);
    }

    public final void a(j jVar) {
        String b2;
        Vector vector = this.d;
        this.c.b();
        Enumeration elements = vector.elements();
        while (elements.hasMoreElements()) {
            g gVar = (g) elements.nextElement();
            if ((gVar instanceof d) && (b2 = ((d) gVar).b(jVar.b())) != null) {
                this.c.a(b2);
            }
        }
    }

    public final void a(l lVar) {
        d a2;
        String b2 = lVar.b();
        Vector vector = this.d;
        int size = vector.size();
        this.c.b();
        for (int i2 = 0; i2 < size; i2++) {
            Object elementAt = vector.elementAt(i2);
            if (elementAt instanceof d) {
                a((d) elementAt, b2);
            } else if ((elementAt instanceof b) && (a2 = ((b) elementAt).a()) != null) {
                if (a2.a() == b2) {
                    this.c.a(a2, 1);
                }
                if (this.i) {
                    a(a2, b2);
                }
            }
        }
    }

    public final void a(q qVar) {
        if (!(this.f instanceof d)) {
            throw new ab(this.j, "Cannot test position of document");
        }
        this.g.a(this.c.a((d) this.f) == qVar.a() ? f639a : b);
    }

    public final void a(u uVar) {
        if (!(this.f instanceof d)) {
            throw new ab(this.j, "Cannot test attribute of document");
        }
        g d2 = ((d) this.f).d();
        while (d2 != null) {
            if (!(d2 instanceof s) || !((s) d2).a().equals(uVar.a())) {
                d2 = d2.i();
            } else {
                this.g.a(f639a);
                return;
            }
        }
        this.g.a(b);
    }

    public final void a(w wVar) {
        if (!(this.f instanceof d)) {
            throw new ab(this.j, "Cannot test attribute of document");
        }
        g d2 = ((d) this.f).d();
        while (d2 != null) {
            if (!(d2 instanceof s) || ((s) d2).a().equals(wVar.a())) {
                d2 = d2.i();
            } else {
                this.g.a(f639a);
                return;
            }
        }
        this.g.a(b);
    }

    public final void b() {
        this.c.b();
        d g2 = this.h.g();
        if (g2 == null) {
            throw new ab(this.j, "Illegal attempt to apply \"..\" to node with no parent.");
        }
        this.c.a(g2, 1);
    }

    public final void c() {
        Vector vector = this.d;
        this.c.b();
        Enumeration elements = vector.elements();
        while (elements.hasMoreElements()) {
            Object nextElement = elements.nextElement();
            if (nextElement instanceof d) {
                a((d) nextElement);
            } else if (nextElement instanceof b) {
                d a2 = ((b) nextElement).a();
                this.c.a(a2, 1);
                if (this.i) {
                    a(a2);
                }
            }
        }
    }

    public final void d() {
        Vector vector = this.d;
        this.c.b();
        Enumeration elements = vector.elements();
        while (elements.hasMoreElements()) {
            Object nextElement = elements.nextElement();
            if (nextElement instanceof d) {
                for (g d2 = ((d) nextElement).d(); d2 != null; d2 = d2.i()) {
                    if (d2 instanceof s) {
                        this.c.a(((s) d2).a());
                    }
                }
            }
        }
    }

    public final void e() {
        this.g.a(f639a);
    }

    public final void f() {
        if (!(this.f instanceof d)) {
            throw new ab(this.j, "Cannot test attribute of document");
        }
        for (g d2 = ((d) this.f).d(); d2 != null; d2 = d2.i()) {
            if (d2 instanceof s) {
                this.g.a(f639a);
                return;
            }
        }
        this.g.a(b);
    }

    public final d g() {
        if (this.d.size() == 0) {
            return null;
        }
        return (d) this.d.elementAt(0);
    }

    public final String h() {
        if (this.d.size() == 0) {
            return null;
        }
        return this.d.elementAt(0).toString();
    }
}
