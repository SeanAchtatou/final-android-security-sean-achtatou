package com.e.a.a;

import java.io.Writer;

public final class s extends g {

    /* renamed from: a  reason: collision with root package name */
    private StringBuffer f638a;

    public s(String str) {
        this.f638a = new StringBuffer(str);
    }

    public final String a() {
        return this.f638a.toString();
    }

    /* access modifiers changed from: package-private */
    public final void a(Writer writer) {
        writer.write(this.f638a.toString());
    }

    public final void a(char[] cArr, int i) {
        this.f638a.append(cArr, 0, i);
        b();
    }

    /* access modifiers changed from: package-private */
    public final void b(Writer writer) {
        String stringBuffer = this.f638a.toString();
        if (stringBuffer.length() < 50) {
            g.a(writer, stringBuffer);
            return;
        }
        writer.write("<![CDATA[");
        writer.write(stringBuffer);
        writer.write("]]>");
    }

    /* access modifiers changed from: protected */
    public final int c() {
        return this.f638a.toString().hashCode();
    }

    public final Object clone() {
        return new s(this.f638a.toString());
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof s)) {
            return false;
        }
        return this.f638a.toString().equals(((s) obj).f638a.toString());
    }
}
