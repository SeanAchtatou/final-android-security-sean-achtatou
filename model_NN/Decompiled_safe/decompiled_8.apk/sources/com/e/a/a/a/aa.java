package com.e.a.a.a;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Stack;

public final class aa {
    private static Hashtable d = new Hashtable();

    /* renamed from: a  reason: collision with root package name */
    private Stack f612a;
    private boolean b;
    private String c;

    private aa(String str) {
        this(str, new InputStreamReader(new ByteArrayInputStream(str.getBytes())));
    }

    private aa(String str, Reader reader) {
        boolean z;
        boolean z2;
        this.f612a = new Stack();
        try {
            this.c = str;
            r rVar = new r(reader);
            rVar.a('/');
            rVar.a('.');
            rVar.a(':', ':');
            rVar.a('_', '_');
            if (rVar.a() == 47) {
                this.b = true;
                if (rVar.a() == 47) {
                    rVar.a();
                    z = true;
                } else {
                    z = false;
                }
            } else {
                this.b = false;
                z = false;
            }
            this.f612a.push(new s(this, z, rVar));
            while (rVar.f622a == 47) {
                if (rVar.a() == 47) {
                    rVar.a();
                    z2 = true;
                } else {
                    z2 = false;
                }
                this.f612a.push(new s(this, z2, rVar));
            }
            if (rVar.f622a != -1) {
                throw new ab(this, "at end of XPATH expression", rVar, "end of expression");
            }
        } catch (IOException e) {
            throw new ab(this, e);
        }
    }

    private aa(boolean z, s[] sVarArr) {
        this.f612a = new Stack();
        for (s addElement : sVarArr) {
            this.f612a.addElement(addElement);
        }
        this.b = z;
        this.c = null;
    }

    public static aa a(String str) {
        aa aaVar;
        synchronized (d) {
            aaVar = (aa) d.get(str);
            if (aaVar == null) {
                aaVar = new aa(str);
                d.put(str, aaVar);
            }
        }
        return aaVar;
    }

    public final boolean a() {
        return this.b;
    }

    public final boolean b() {
        return ((s) this.f612a.peek()).b();
    }

    public final Enumeration c() {
        return this.f612a.elements();
    }

    public final Object clone() {
        s[] sVarArr = new s[this.f612a.size()];
        Enumeration elements = this.f612a.elements();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= sVarArr.length) {
                return new aa(this.b, sVarArr);
            }
            sVarArr[i2] = (s) elements.nextElement();
            i = i2 + 1;
        }
    }

    public final String toString() {
        if (this.c == null) {
            StringBuffer stringBuffer = new StringBuffer();
            boolean z = true;
            Enumeration elements = this.f612a.elements();
            while (true) {
                boolean z2 = z;
                if (!elements.hasMoreElements()) {
                    break;
                }
                s sVar = (s) elements.nextElement();
                if (!z2 || this.b) {
                    stringBuffer.append('/');
                    if (sVar.a()) {
                        stringBuffer.append('/');
                    }
                }
                stringBuffer.append(sVar.toString());
                z = false;
            }
            this.c = stringBuffer.toString();
        }
        return this.c;
    }
}
