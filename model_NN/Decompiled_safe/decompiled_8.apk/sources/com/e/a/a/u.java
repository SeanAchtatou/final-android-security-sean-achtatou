package com.e.a.a;

final class u {

    /* renamed from: a  reason: collision with root package name */
    private v f640a;

    private u() {
        this.f640a = null;
    }

    u(byte b) {
        this();
    }

    /* access modifiers changed from: package-private */
    public final Boolean a() {
        Boolean bool = this.f640a.f641a;
        this.f640a = this.f640a.b;
        return bool;
    }

    /* access modifiers changed from: package-private */
    public final void a(Boolean bool) {
        this.f640a = new v(bool, this.f640a);
    }
}
