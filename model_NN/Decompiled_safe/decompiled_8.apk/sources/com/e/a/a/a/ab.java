package com.e.a.a.a;

import com.jcraft.jzlib.JZlib;
import java.io.IOException;

public final class ab extends Exception {

    /* renamed from: a  reason: collision with root package name */
    private Throwable f613a;

    ab(aa aaVar, Exception exc) {
        super(new StringBuffer().append(aaVar).append(" ").append(exc).toString());
        this.f613a = null;
        this.f613a = exc;
    }

    public ab(aa aaVar, String str) {
        super(new StringBuffer().append(aaVar).append(" ").append(str).toString());
        this.f613a = null;
    }

    ab(aa aaVar, String str, r rVar, String str2) {
        this(aaVar, new StringBuffer().append(str).append(" got \"").append(a(rVar)).append("\" instead of expected ").append(str2).toString());
    }

    private static String a(r rVar) {
        try {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(b(rVar));
            if (rVar.f622a != -1) {
                rVar.a();
                stringBuffer.append(b(rVar));
                rVar.b();
            }
            return stringBuffer.toString();
        } catch (IOException e) {
            return new StringBuffer("(cannot get  info: ").append(e).append(")").toString();
        }
    }

    private static String b(r rVar) {
        switch (rVar.f622a) {
            case JZlib.Z_DATA_ERROR /*-3*/:
                return rVar.c;
            case JZlib.Z_STREAM_ERROR /*-2*/:
                return new StringBuffer().append(rVar.b).toString();
            case -1:
                return "<end of expression>";
            default:
                return new StringBuffer().append((char) rVar.f622a).toString();
        }
    }

    public final Throwable getCause() {
        return this.f613a;
    }
}
