package com.e.a.a;

final class a implements c, l {
    private d b;
    private final b c;
    private n d;

    public a() {
        this((byte) 0);
    }

    private a(byte b2) {
        this.b = null;
        this.c = new b();
        this.d = null;
        m mVar = n.f635a;
    }

    public final b a() {
        return this.c;
    }

    public final void a(d dVar) {
        if (this.b == null) {
            this.c.a(dVar);
        } else {
            this.b.b((g) dVar);
        }
        this.b = dVar;
    }

    public final void a(n nVar) {
        this.d = nVar;
        this.c.a(nVar.toString());
    }

    public final void a(char[] cArr, int i) {
        d dVar = this.b;
        if (dVar.e() instanceof s) {
            ((s) dVar.e()).a(cArr, i);
        } else {
            dVar.a(new s(new String(cArr, 0, i)));
        }
    }

    public final void b() {
        this.b = this.b.g();
    }

    public final String toString() {
        if (this.d != null) {
            return new StringBuffer("BuildDoc: ").append(this.d.toString()).toString();
        }
        return null;
    }
}
