package com.e.a.a;

import java.util.Hashtable;

public final class r {

    /* renamed from: a  reason: collision with root package name */
    private final Hashtable f637a = new Hashtable();

    r() {
    }

    public final String a(String str) {
        String str2 = (String) this.f637a.get(str);
        if (str2 != null) {
            return str2;
        }
        this.f637a.put(str, str);
        return str;
    }
}
