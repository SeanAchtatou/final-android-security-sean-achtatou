package com.e.a.a;

import com.e.a.a.a.aa;
import com.e.a.a.a.ab;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;

public final class b extends g {

    /* renamed from: a  reason: collision with root package name */
    private d f629a = null;
    private String b;
    private Vector c;

    static {
        new Integer(1);
        new e();
    }

    public b() {
        o.a();
        this.c = new Vector();
        this.b = "MEMORY";
    }

    private b(String str) {
        o.a();
        this.c = new Vector();
        this.b = str;
    }

    public final d a() {
        return this.f629a;
    }

    public final void a(d dVar) {
        this.f629a = dVar;
        this.f629a.a(this);
        b();
    }

    public final void a(Writer writer) {
        this.f629a.a(writer);
    }

    public final void a(String str) {
        this.b = str;
        b();
    }

    public final d b(String str) {
        try {
            if (str.charAt(0) != '/') {
                str = new StringBuffer("/").append(str).toString();
            }
            aa a2 = aa.a(str);
            if (!a2.b()) {
                return new t(this, a2).g();
            }
            throw new ab(a2, new StringBuffer("\"").append(a2).append("\" evaluates to ").append("evaluates to string not element").toString());
        } catch (ab e) {
            throw new k("XPath problem", e);
        }
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        Enumeration elements = this.c.elements();
        while (elements.hasMoreElements()) {
            elements.nextElement();
        }
    }

    public final void b(Writer writer) {
        writer.write("<?xml version=\"1.0\" ?>\n");
        this.f629a.b(writer);
    }

    /* access modifiers changed from: protected */
    public final int c() {
        return this.f629a.hashCode();
    }

    public final Object clone() {
        b bVar = new b(this.b);
        bVar.f629a = (d) this.f629a.clone();
        return bVar;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof b)) {
            return false;
        }
        return this.f629a.equals(((b) obj).f629a);
    }

    public final String toString() {
        return this.b;
    }
}
