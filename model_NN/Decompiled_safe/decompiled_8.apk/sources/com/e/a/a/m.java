package com.e.a.a;

import java.io.InputStream;

public final class m {
    m() {
    }

    public static b a(String str, InputStream inputStream) {
        a aVar = new a();
        new i(str, inputStream, aVar);
        return aVar.a();
    }

    public static void a(String str, String str2) {
        System.out.println(new StringBuffer().append(str2).append("(1): ").append(str).append(" (NOTE)").toString());
    }

    public static void a(String str, String str2, int i) {
        System.err.println(new StringBuffer().append(str2).append("(").append(i).append("): ").append(str).append(" (ERROR)").toString());
    }

    public static void b(String str, String str2, int i) {
        System.out.println(new StringBuffer().append(str2).append("(").append(i).append("): ").append(str).append(" (WARNING)").toString());
    }
}
