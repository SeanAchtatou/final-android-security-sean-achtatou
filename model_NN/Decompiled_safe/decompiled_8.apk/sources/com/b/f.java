package com.b;

import android.content.Context;
import com.amap.mapapi.poisearch.PoiTypeDef;

final class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ Context f546a;

    f(Context context) {
        this.f546a = context;
    }

    public final void run() {
        if (a.f541a == 2) {
            try {
                Thread.sleep((long) a.b);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        boolean z = true;
        while (true) {
            String d = a.l(this.f546a);
            if (!d.equals(PoiTypeDef.All) && z) {
                if (d.substring(0, 3).equals("act")) {
                    z = a.a(this.f546a, d);
                } else if (d.substring(0, 3).equals("evn")) {
                    z = a.c(this.f546a, d, null);
                } else if (d.substring(0, 3).equals("err")) {
                    z = a.b(this.f546a, d);
                }
                if (z) {
                    try {
                        Thread.sleep(0);
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }
        if (a.a(this.f546a, 1)) {
            a.a(this.f546a);
        }
    }
}
