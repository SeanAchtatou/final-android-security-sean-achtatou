package com.b;

import android.content.Context;
import android.util.Log;
import com.amap.mapapi.poisearch.PoiTypeDef;

public final class h extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private Context f548a;
    private String b = PoiTypeDef.All;
    private int c;
    private String d = PoiTypeDef.All;

    h(Context context) {
        this.f548a = context;
        this.c = 6;
    }

    h(Context context, String str) {
        this.f548a = context;
        this.c = 2;
        this.d = str;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final void run() {
        try {
            switch (this.c) {
                case 1:
                    a.a(this.f548a, this.b);
                    return;
                case 2:
                    a.c(this.f548a, this.b, this.d);
                    return;
                case 3:
                    a.a(this.f548a);
                    return;
                case 4:
                    a.b(this.f548a, (String) null);
                    return;
                case 5:
                case 7:
                default:
                    return;
                case 6:
                    a.c(this.f548a);
                    return;
                case 8:
                    a.b(this.f548a);
                    return;
            }
        } catch (Exception e) {
            Log.e("MobileAgent", "Exception occurred when recording usage.");
            e.printStackTrace();
        }
        Log.e("MobileAgent", "Exception occurred when recording usage.");
        e.printStackTrace();
    }
}
