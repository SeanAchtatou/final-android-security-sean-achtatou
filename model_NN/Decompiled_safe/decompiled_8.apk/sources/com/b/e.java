package com.b;

import java.io.OutputStream;

public final class e extends d {

    /* renamed from: a  reason: collision with root package name */
    private static final char[] f545a = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

    /* access modifiers changed from: protected */
    public final void a(OutputStream outputStream, byte[] bArr, int i, int i2) {
        if (i2 == 1) {
            byte b = bArr[i];
            outputStream.write(f545a[(b >>> 2) & 63]);
            outputStream.write(f545a[(b << 4) & 48]);
            outputStream.write(61);
            outputStream.write(61);
        } else if (i2 == 2) {
            byte b2 = bArr[i];
            byte b3 = bArr[i + 1];
            outputStream.write(f545a[(b2 >>> 2) & 63]);
            outputStream.write(f545a[((b2 << 4) & 48) + ((b3 >>> 4) & 15)]);
            outputStream.write(f545a[(b3 << 2) & 60]);
            outputStream.write(61);
        } else {
            byte b4 = bArr[i];
            byte b5 = bArr[i + 1];
            byte b6 = bArr[i + 2];
            outputStream.write(f545a[(b4 >>> 2) & 63]);
            outputStream.write(f545a[((b4 << 4) & 48) + ((b5 >>> 4) & 15)]);
            outputStream.write(f545a[((b5 << 2) & 60) + ((b6 >>> 6) & 3)]);
            outputStream.write(f545a[b6 & 63]);
        }
    }
}
