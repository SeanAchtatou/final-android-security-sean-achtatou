package com.b;

import android.content.Context;
import android.util.Log;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.lang.Thread;

public final class b implements Thread.UncaughtExceptionHandler {

    /* renamed from: a  reason: collision with root package name */
    private static b f542a = new b();
    private Context b;

    private b() {
    }

    public static b a() {
        return f542a;
    }

    public final void a(Context context) {
        this.b = context;
        Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    public final void uncaughtException(Thread thread, Throwable th) {
        StackTraceElement[] stackTrace = th.getStackTrace();
        String str = PoiTypeDef.All;
        for (int i = 0; i < stackTrace.length; i++) {
            str = str + stackTrace[i].toString() + "\n";
        }
        String str2 = th.toString() + "\n" + str;
        Log.i("CrashHandler", str2);
        a.c(this.b, str2);
        System.exit(10);
    }
}
