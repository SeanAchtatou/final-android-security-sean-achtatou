package com.b;

import android.content.Context;

public final class g {

    /* renamed from: a  reason: collision with root package name */
    private int f547a;
    private int b;
    private String c;
    private int d;
    private int e;
    private int f;
    private int g = 10;

    static boolean a(Context context, String str) {
        try {
            return context.checkCallingOrSelfPermission(str) != -1;
        } catch (Exception e2) {
        }
    }

    public final String a() {
        return this.c;
    }

    public final void a(int i) {
        if (i < Integer.MAX_VALUE) {
            this.f547a = i;
        }
    }

    public final void a(String str) {
        this.c = str;
    }

    public final int b() {
        return this.f547a;
    }

    public final void b(int i) {
        if (this.f547a < Integer.MAX_VALUE) {
            this.b = i;
        }
    }

    public final int c() {
        return this.b;
    }

    public final void c(int i) {
        this.d = i;
    }

    public final int d() {
        return this.d;
    }

    public final void d(int i) {
        this.e = i;
    }

    public final int e() {
        return this.e;
    }

    public final void e(int i) {
        this.f = i;
    }

    public final int f() {
        return this.f;
    }

    public final void f(int i) {
        this.g = i;
    }

    public final int g() {
        return this.g;
    }
}
