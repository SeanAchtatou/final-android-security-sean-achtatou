package com.b;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Looper;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.weibo.sdk.android.api.WeiboAPI;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Random;
import org.apache.http.HttpHost;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    static int f541a = -1;
    static int b = 0;
    private static JSONObject c;
    private static boolean d = true;
    private static boolean e = false;
    private static boolean f = true;
    private static String g = PoiTypeDef.All;
    private static String h = PoiTypeDef.All;
    private static long i = 0;
    private static String j = PoiTypeDef.All;
    private static Object k = new Object();
    private static boolean l = false;

    private static int a(Context context, String str, JSONObject jSONObject) {
        try {
            String g2 = android.support.v4.b.a.g(context);
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str + "&appkey=" + g2 + "&channel=" + URLEncoder.encode(android.support.v4.b.a.h(context), "UTF-8") + "&code=104").openConnection();
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setRequestMethod(WeiboAPI.HTTPMETHOD_POST);
            byte[] a2 = c.a().a(jSONObject.toString());
            httpURLConnection.setRequestProperty("Content-length", new StringBuilder().append(a2.length).toString());
            httpURLConnection.setRequestProperty("Content-Type", "application/octet-stream");
            httpURLConnection.setRequestProperty("Charset", "UTF-8");
            OutputStream outputStream = httpURLConnection.getOutputStream();
            outputStream.write(a2);
            outputStream.close();
            if (200 != httpURLConnection.getResponseCode()) {
                return 2;
            }
            StringBuffer stringBuffer = new StringBuffer();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream(), "utf-8"));
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    stringBuffer.append(readLine).append("\n");
                } else {
                    bufferedReader.close();
                    new JSONObject(stringBuffer.toString()).getInt("resultcode");
                    return 1;
                }
            }
        } catch (ClientProtocolException e2) {
            return 2;
        } catch (IOException e3) {
            return 2;
        } catch (JSONException e4) {
            return 3;
        } catch (Exception e5) {
            e5.printStackTrace();
            return 2;
        }
    }

    protected static void a(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("pid", 1);
            jSONObject.put("protocolVersion", "1.0.4");
            jSONObject.put("sdkVersion", "1.0.4");
            jSONObject.put("cid", android.support.v4.b.a.b(context));
            jSONObject.put("deviceId", android.support.v4.b.a.a(context));
            jSONObject.put("appKey", android.support.v4.b.a.g(context));
            jSONObject.put("packageName", context.getPackageName());
            jSONObject.put("versionCode", android.support.v4.b.a.j(context));
            jSONObject.put("versionName", android.support.v4.b.a.k(context));
            jSONObject.put("sendTime", System.currentTimeMillis());
            jSONObject.put("imsi", android.support.v4.b.a.c(context));
            jSONObject.put("mac", android.support.v4.b.a.f(context));
            jSONObject.put("deviceDetail", URLEncoder.encode(android.support.v4.b.a.b(Build.MODEL), "UTF-8"));
            jSONObject.put("manufacturer", URLEncoder.encode(android.support.v4.b.a.b(Build.MANUFACTURER), "UTF-8"));
            jSONObject.put("phoneOS", URLEncoder.encode(android.support.v4.b.a.a(), "UTF-8"));
            jSONObject.put("screenWidth", android.support.v4.b.a.d(context));
            jSONObject.put("screenHeight", android.support.v4.b.a.e(context));
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
            jSONObject.put("screenDensity", displayMetrics.densityDpi);
            jSONObject.put("carrierName", URLEncoder.encode(((TelephonyManager) context.getSystemService("phone")).getNetworkOperatorName(), "UTF-8"));
            jSONObject.put("accessPoint", android.support.v4.b.a.i(context));
            jSONObject.put("countryCode", Locale.getDefault().getCountry());
            jSONObject.put("languageCode", Locale.getDefault().getLanguage());
            jSONObject.put("channel", URLEncoder.encode(android.support.v4.b.a.h(context), "UTF-8"));
        } catch (JSONException e2) {
            e2.printStackTrace();
            return;
        } catch (UnsupportedEncodingException e3) {
            e3.printStackTrace();
        }
        if (a(context, "http://da.mmarket.com/mmsdk/mmsdk?func=mmsdk:postsyslog", jSONObject) == 1) {
            b(context, 1);
            Log.i("MobileAgent", "send syslog success");
        }
    }

    public static void a(Context context, String str, String str2) {
        StringBuffer stringBuffer = new StringBuffer();
        try {
            stringBuffer.append(URLEncoder.encode(android.support.v4.b.a.h(context), "UTF-8"));
        } catch (UnsupportedEncodingException e2) {
        }
        stringBuffer.append("@@");
        stringBuffer.append(android.support.v4.b.a.c(context));
        stringBuffer.append("@@");
        stringBuffer.append(android.support.v4.b.a.b(Build.MANUFACTURER));
        stringBuffer.append("@@");
        stringBuffer.append(android.support.v4.b.a.b(Build.MODEL));
        stringBuffer.append("@@");
        try {
            stringBuffer.append(URLEncoder.encode(android.support.v4.b.a.a(), "UTF-8"));
        } catch (UnsupportedEncodingException e3) {
        }
        stringBuffer.append("@@");
        stringBuffer.append(android.support.v4.b.a.f(context));
        stringBuffer.append("@@");
        stringBuffer.append(android.support.v4.b.a.i(context));
        stringBuffer.append("@@");
        stringBuffer.append(android.support.v4.b.a.d(context) + "*" + android.support.v4.b.a.e(context));
        stringBuffer.append("@@");
        try {
            stringBuffer.append(URLEncoder.encode(str2, "UTF-8"));
        } catch (UnsupportedEncodingException e4) {
        }
        StringBuffer stringBuffer2 = new StringBuffer();
        try {
            stringBuffer2.append(URLEncoder.encode(str, "UTF-8"));
            stringBuffer2.append("|");
            stringBuffer2.append(URLEncoder.encode(stringBuffer.toString(), "UTF-8"));
            stringBuffer2.append("|");
            stringBuffer2.append(0);
            stringBuffer2.append("|");
            stringBuffer2.append(System.currentTimeMillis());
            stringBuffer2.append("\n");
            JSONObject jSONObject = new JSONObject();
            c = jSONObject;
            jSONObject.put("sid", j);
            c.put("logJsonAry", stringBuffer2.toString());
            Log.i("MobileAgent", "send event_rt .....");
            new h(context, c.toString()).start();
        } catch (UnsupportedEncodingException e5) {
            e5.printStackTrace();
        } catch (JSONException e6) {
            e6.printStackTrace();
        }
    }

    protected static boolean a(Context context, int i2) {
        int i3;
        int i4;
        SharedPreferences f2 = f(context);
        if (i2 == 3) {
            i3 = f2.getInt("actionmonth", 0);
            i4 = f2.getInt("actionday", 0);
        } else if (i2 == 2) {
            i3 = f2.getInt("eventmonth", 0);
            i4 = f2.getInt("eventday", 0);
        } else {
            i3 = f2.getInt("sysmonth", 0);
            i4 = f2.getInt("sysday", 0);
        }
        Date date = new Date();
        return (Integer.valueOf(new SimpleDateFormat("M").format(date)).intValue() == i3 && Integer.valueOf(new SimpleDateFormat("dd").format(date)).intValue() == i4) ? false : true;
    }

    private static boolean a(Context context, SharedPreferences sharedPreferences) {
        String string = sharedPreferences.getString("sessionId", null);
        String string2 = sharedPreferences.getString("activities", null);
        c = new JSONObject();
        try {
            c.put("sid", string);
            c.put("logs", string2);
            if (!a(context, c.toString(), 3)) {
                return true;
            }
            sharedPreferences.edit().putString("activities", PoiTypeDef.All).commit();
            return true;
        } catch (JSONException e2) {
            e2.printStackTrace();
            return true;
        }
    }

    protected static boolean a(Context context, String str) {
        int i2;
        String e2 = e(context, str);
        if (!e2.equals(PoiTypeDef.All)) {
            JSONObject i3 = i(context);
            try {
                i3.put("sid", new JSONObject(e2).get("sid"));
                i3.put("logJsonAry", new JSONArray("[" + e2 + "]"));
                i2 = a(context, "http://da.mmarket.com/mmsdk/mmsdk?func=mmsdk:postactlog", i3);
            } catch (JSONException e3) {
                e3.printStackTrace();
                i2 = 3;
            }
            if (i2 == 1 || i2 == 3) {
                b(context, 3);
                f(context, str);
                Log.i("MobileAgent", "send actlog success:" + str + " size:" + i3.toString().getBytes().length);
                return true;
            }
            if (i2 == 2) {
            }
            return false;
        }
        b(context, 3);
        f(context, str);
        return true;
    }

    private static synchronized boolean a(Context context, String str, int i2) {
        String str2;
        boolean z = false;
        synchronized (a.class) {
            if (i2 == 3) {
                str2 = "act";
            } else if (i2 == 2) {
                str2 = "evn";
            } else if (i2 == 4) {
                str2 = "err";
            }
            if (!str.equals(PoiTypeDef.All)) {
                long k2 = k(context);
                long j2 = 1 + k2;
                String str3 = str2 + k2;
                try {
                    FileOutputStream openFileOutput = context.openFileOutput(str3, 1);
                    synchronized (k) {
                        SharedPreferences h2 = h(context);
                        int i3 = h2.getInt("uploadcount", 0);
                        String str4 = h2.getString("uploadList", PoiTypeDef.All) + str3 + "|";
                        h2.edit().putString("uploadList", str4).commit();
                        h2.edit().putLong("uploadpopindex", j2).commit();
                        if (str4.split("\\|").length > 200) {
                            String l2 = l(context);
                            context.deleteFile(l2);
                            g(context, l2);
                        } else {
                            h2.edit().putInt("uploadcount", i3 + 1).commit();
                        }
                    }
                    openFileOutput.write(str.getBytes());
                    openFileOutput.close();
                } catch (FileNotFoundException e2) {
                } catch (IOException e3) {
                }
                z = true;
            }
        }
        return z;
    }

    protected static void b(Context context) {
        HttpPost httpPost = new HttpPost("http://da.mmarket.com/mmsdk/mmsdk?func=mmsdk:getappparameter&appkey=" + android.support.v4.b.a.g(context));
        try {
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            HttpClientParams.setRedirecting(basicHttpParams, true);
            HttpProtocolParams.setUserAgent(basicHttpParams, j.a());
            HttpProtocolParams.setContentCharset(basicHttpParams, "utf-8");
            HttpProtocolParams.setHttpElementCharset(basicHttpParams, "utf-8");
            if (j.a(context).equals("cmwap")) {
                basicHttpParams.setParameter("http.route.default-proxy", new HttpHost("10.0.0.172", 80, (String) null));
            }
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
            defaultHttpClient.setRedirectHandler(new i());
            JSONObject jSONObject = new JSONObject(EntityUtils.toString(defaultHttpClient.execute(httpPost).getEntity()));
            SharedPreferences.Editor edit = f(context).edit();
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                edit.putString(next, jSONObject.getString(next));
            }
            edit.commit();
            Looper.prepare();
            jSONObject.length();
            Looper.loop();
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        } catch (JSONException e3) {
            e3.printStackTrace();
        } catch (ParseException e4) {
            e4.printStackTrace();
        } catch (IOException e5) {
            e5.printStackTrace();
        }
    }

    private static void b(Context context, int i2) {
        Date date = new Date();
        int parseInt = Integer.parseInt(new SimpleDateFormat("dd").format(date));
        int parseInt2 = Integer.parseInt(new SimpleDateFormat("M").format(date));
        SharedPreferences.Editor edit = f(context).edit();
        if (i2 == 3) {
            edit.putInt("actionmonth", parseInt2);
            edit.putInt("actionday", parseInt);
        } else if (i2 == 2) {
            edit.putInt("eventmonth", parseInt2);
            edit.putInt("eventday", parseInt);
        } else {
            edit.putInt("sysmonth", parseInt2);
            edit.putInt("sysday", parseInt);
        }
        edit.commit();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00d8, code lost:
        if ((r0 != null && r0.getType() == 1) != false) goto L_0x00da;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void b(android.content.Context r5, java.lang.String r6, java.lang.String r7) {
        /*
            r1 = 1
            r2 = 0
            boolean r0 = com.b.a.l
            if (r0 != 0) goto L_0x0049
            java.lang.String r0 = "MoblieAgent_sys_config"
            android.content.SharedPreferences r0 = r5.getSharedPreferences(r0, r2)
            android.content.SharedPreferences$Editor r3 = r0.edit()
            java.lang.String r4 = "MOBILE_APPKEY"
            android.content.SharedPreferences$Editor r3 = r3.putString(r4, r6)
            r3.commit()
            android.content.SharedPreferences$Editor r0 = r0.edit()
            java.lang.String r3 = "MOBILE_CHANNEL"
            android.content.SharedPreferences$Editor r0 = r0.putString(r3, r7)
            r0.commit()
            java.lang.String r0 = android.support.v4.b.a.g(r5)
            android.support.v4.b.a.h(r5)
            java.lang.String r3 = "onResume"
            java.lang.String r4 = com.b.a.h     // Catch:{ Exception -> 0x005f }
            boolean r3 = r3.equals(r4)     // Catch:{ Exception -> 0x005f }
            if (r3 == 0) goto L_0x004a
            java.lang.Class r3 = r5.getClass()     // Catch:{ Exception -> 0x005f }
            java.lang.String r3 = r3.getName()     // Catch:{ Exception -> 0x005f }
            java.lang.String r4 = com.b.a.g     // Catch:{ Exception -> 0x005f }
            boolean r3 = r3.equals(r4)     // Catch:{ Exception -> 0x005f }
            if (r3 == 0) goto L_0x004a
        L_0x0047:
            com.b.a.l = r1
        L_0x0049:
            return
        L_0x004a:
            boolean r3 = com.b.a.d     // Catch:{ Exception -> 0x005f }
            if (r3 == 0) goto L_0x0055
            com.b.b r3 = com.b.b.a()     // Catch:{ Exception -> 0x005f }
            r3.a(r5)     // Catch:{ Exception -> 0x005f }
        L_0x0055:
            if (r5 != 0) goto L_0x006b
            java.lang.String r0 = "MobileAgent"
            java.lang.String r2 = "unexpected null context"
            android.util.Log.e(r0, r2)     // Catch:{ Exception -> 0x005f }
            goto L_0x0047
        L_0x005f:
            r0 = move-exception
            java.lang.String r2 = "MobileAgent"
            java.lang.String r3 = "Exception occurred in Mobclick.onResume(). "
            android.util.Log.e(r2, r3)
            r0.printStackTrace()
            goto L_0x0047
        L_0x006b:
            if (r0 == 0) goto L_0x0073
            int r0 = r0.length()     // Catch:{ Exception -> 0x005f }
            if (r0 != 0) goto L_0x007b
        L_0x0073:
            java.lang.String r0 = "MobileAgent"
            java.lang.String r2 = "unexpected empty appkey"
            android.util.Log.e(r0, r2)     // Catch:{ Exception -> 0x005f }
            goto L_0x0047
        L_0x007b:
            boolean r3 = e(r5)     // Catch:{ Exception -> 0x005f }
            boolean r0 = com.b.a.f     // Catch:{ Exception -> 0x005f }
            if (r0 == 0) goto L_0x00bb
            java.lang.String r0 = "updateonlyonwifi"
            java.lang.String r0 = d(r5, r0)     // Catch:{ Exception -> 0x005f }
            java.lang.String r4 = "1"
            boolean r0 = r0.equals(r4)     // Catch:{ Exception -> 0x005f }
            if (r0 == 0) goto L_0x00e4
            r0 = 1
            com.b.a.e = r0     // Catch:{ Exception -> 0x005f }
        L_0x0094:
            java.lang.String r0 = "updatedelay"
            java.lang.String r0 = d(r5, r0)     // Catch:{ Exception -> 0x005f }
            java.lang.String r4 = "0"
            boolean r4 = r0.equals(r4)     // Catch:{ Exception -> 0x005f }
            if (r4 != 0) goto L_0x00aa
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ Exception -> 0x005f }
            int r0 = r0 * 1000
            com.b.a.b = r0     // Catch:{ Exception -> 0x005f }
        L_0x00aa:
            java.lang.String r0 = "send_policy"
            java.lang.String r0 = d(r5, r0)     // Catch:{ Exception -> 0x005f }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ Exception -> 0x005f }
            com.b.a.f541a = r0     // Catch:{ Exception -> 0x005f }
            if (r0 != 0) goto L_0x00bb
            r0 = 1
            com.b.a.f541a = r0     // Catch:{ Exception -> 0x005f }
        L_0x00bb:
            boolean r0 = com.b.a.e     // Catch:{ Exception -> 0x005f }
            if (r0 == 0) goto L_0x00da
            boolean r0 = com.b.a.e     // Catch:{ Exception -> 0x005f }
            if (r0 == 0) goto L_0x00df
            java.lang.String r0 = "connectivity"
            java.lang.Object r0 = r5.getSystemService(r0)     // Catch:{ Exception -> 0x005f }
            android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0     // Catch:{ Exception -> 0x005f }
            android.net.NetworkInfo r0 = r0.getActiveNetworkInfo()     // Catch:{ Exception -> 0x005f }
            if (r0 == 0) goto L_0x00e8
            int r0 = r0.getType()     // Catch:{ Exception -> 0x005f }
            if (r0 != r1) goto L_0x00e8
            r0 = r1
        L_0x00d8:
            if (r0 == 0) goto L_0x00df
        L_0x00da:
            int r0 = com.b.a.f541a     // Catch:{ Exception -> 0x005f }
            switch(r0) {
                case 1: goto L_0x00ea;
                case 2: goto L_0x00f5;
                case 3: goto L_0x0105;
                default: goto L_0x00df;
            }     // Catch:{ Exception -> 0x005f }
        L_0x00df:
            r0 = 0
            com.b.a.f = r0     // Catch:{ Exception -> 0x005f }
            goto L_0x0047
        L_0x00e4:
            r0 = 0
            com.b.a.e = r0     // Catch:{ Exception -> 0x005f }
            goto L_0x0094
        L_0x00e8:
            r0 = r2
            goto L_0x00d8
        L_0x00ea:
            if (r3 == 0) goto L_0x00df
            com.b.h r0 = new com.b.h     // Catch:{ Exception -> 0x005f }
            r0.<init>(r5)     // Catch:{ Exception -> 0x005f }
            r0.start()     // Catch:{ Exception -> 0x005f }
            goto L_0x00df
        L_0x00f5:
            boolean r0 = com.b.a.f     // Catch:{ Exception -> 0x005f }
            if (r0 == 0) goto L_0x00df
            com.b.h r0 = new com.b.h     // Catch:{ Exception -> 0x005f }
            r0.<init>(r5)     // Catch:{ Exception -> 0x005f }
            r0.start()     // Catch:{ Exception -> 0x005f }
            r0 = 0
            com.b.a.f = r0     // Catch:{ Exception -> 0x005f }
            goto L_0x00df
        L_0x0105:
            r0 = 3
            boolean r0 = a(r5, r0)     // Catch:{ Exception -> 0x005f }
            if (r0 == 0) goto L_0x00df
            com.b.h r0 = new com.b.h     // Catch:{ Exception -> 0x005f }
            r0.<init>(r5)     // Catch:{ Exception -> 0x005f }
            r0.start()     // Catch:{ Exception -> 0x005f }
            goto L_0x00df
        */
        throw new UnsupportedOperationException("Method not decompiled: com.b.a.b(android.content.Context, java.lang.String, java.lang.String):void");
    }

    protected static boolean b(Context context, String str) {
        try {
            JSONObject jSONObject = new JSONObject(e(context, str));
            jSONObject.put("pid", 1);
            jSONObject.put("protocolVersion", "1.0.4");
            jSONObject.put("sdkVersion", "1.0.4");
            jSONObject.put("cid", android.support.v4.b.a.b(context));
            jSONObject.put("deviceId", android.support.v4.b.a.a(context));
            jSONObject.put("appKey", android.support.v4.b.a.g(context));
            jSONObject.put("packageName", context.getPackageName());
            jSONObject.put("versionCode", android.support.v4.b.a.j(context));
            jSONObject.put("versionName", android.support.v4.b.a.k(context));
            jSONObject.put("sendTime", System.currentTimeMillis());
            int a2 = a(context, "http://da.mmarket.com/mmsdk/mmsdk?func=mmsdk:posterrlog", jSONObject);
            if (a2 == 1 || a2 == 3) {
                b(context, 3);
                f(context, str);
                Log.i("MobileAgent", "send errlog success" + str);
                return true;
            }
            if (a2 == 2) {
            }
            return false;
        } catch (JSONException e2) {
            e2.printStackTrace();
            return false;
        }
    }

    static void c(Context context) {
        new Thread(new f(context)).start();
    }

    protected static void c(Context context, String str) {
        try {
            JSONObject jSONObject = new JSONObject();
            JSONArray jSONArray = new JSONArray();
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("occurtime", System.currentTimeMillis());
            jSONObject2.put("errmsg", URLEncoder.encode(str, "UTF-8"));
            jSONArray.put(jSONObject2);
            jSONObject.put("sid", j);
            jSONObject.put("errjsonary", jSONArray);
            a(context, jSONObject.toString(), 4);
        } catch (UnsupportedEncodingException | JSONException e2) {
        }
    }

    protected static boolean c(Context context, String str, String str2) {
        String e2 = str2 == null ? e(context, str) : str2;
        if (!e2.equals(PoiTypeDef.All)) {
            try {
                JSONObject jSONObject = new JSONObject(e2);
                String b2 = android.support.v4.b.a.b(context);
                jSONObject.put("pid", 1);
                jSONObject.put("protocolVersion", "1.0.4");
                jSONObject.put("sdkVersion", "1.0.4");
                jSONObject.put("cid", b2);
                jSONObject.put("appKey", android.support.v4.b.a.g(context));
                jSONObject.put("packageName", context.getPackageName());
                jSONObject.put("versionCode", android.support.v4.b.a.j(context));
                jSONObject.put("versionName", android.support.v4.b.a.k(context));
                jSONObject.put("sendTime", System.currentTimeMillis());
                jSONObject.put("deviceId", android.support.v4.b.a.a(context));
                int a2 = a(context, "http://da.mmarket.com/mmsdk/mmsdk?func=mmsdk:posteventlog", jSONObject);
                if (str2 != null || (a2 != 1 && a2 != 3)) {
                    return a2 == 2 ? false : false;
                }
                b(context, 3);
                f(context, str);
                Log.i("MobileAgent", "send eventlog success" + str + " size:" + jSONObject.toString().getBytes().length);
                return true;
            } catch (JSONException e3) {
                e3.printStackTrace();
                return true;
            }
        } else if (str2 != null) {
            return false;
        } else {
            b(context, 3);
            f(context, str);
            return true;
        }
    }

    private static String d(Context context, String str) {
        return f(context).getString(str, "0");
    }

    private static String d(Context context, String str, String str2) {
        long j2 = 0;
        long currentTimeMillis = System.currentTimeMillis();
        if (str.equals("onResume")) {
            i = currentTimeMillis;
        } else if (str.equals("onPause") && g.equals(context.getClass().getName())) {
            j2 = currentTimeMillis - i;
        }
        if (str2 == null) {
            str2 = PoiTypeDef.All;
        }
        g = context.getClass().getName();
        h = str;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(str2);
        stringBuffer.append(str);
        stringBuffer.append("|");
        stringBuffer.append(context.getClass().getName());
        stringBuffer.append("|");
        stringBuffer.append(currentTimeMillis);
        stringBuffer.append("|");
        stringBuffer.append(j2);
        stringBuffer.append("\n");
        return stringBuffer.toString();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0032, code lost:
        r0 = com.amap.mapapi.poisearch.PoiTypeDef.All;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static synchronized java.lang.String e(android.content.Context r8, java.lang.String r9) {
        /*
            java.lang.Class<com.b.a> r1 = com.b.a.class
            monitor-enter(r1)
            java.lang.String r0 = ""
            java.io.FileInputStream r2 = r8.openFileInput(r9)     // Catch:{ FileNotFoundException -> 0x0031, IOException -> 0x003a, all -> 0x0035 }
            r3 = 10000(0x2710, float:1.4013E-41)
            byte[] r3 = new byte[r3]     // Catch:{ FileNotFoundException -> 0x0031, IOException -> 0x003a, all -> 0x0035 }
        L_0x000d:
            int r4 = r2.read(r3)     // Catch:{ FileNotFoundException -> 0x0031, IOException -> 0x003a, all -> 0x0035 }
            r5 = -1
            if (r4 == r5) goto L_0x002c
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0031, IOException -> 0x003a, all -> 0x0035 }
            r5.<init>()     // Catch:{ FileNotFoundException -> 0x0031, IOException -> 0x003a, all -> 0x0035 }
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ FileNotFoundException -> 0x0031, IOException -> 0x003a, all -> 0x0035 }
            java.lang.String r6 = new java.lang.String     // Catch:{ FileNotFoundException -> 0x0031, IOException -> 0x003a, all -> 0x0035 }
            r7 = 0
            r6.<init>(r3, r7, r4)     // Catch:{ FileNotFoundException -> 0x0031, IOException -> 0x003a, all -> 0x0035 }
            java.lang.StringBuilder r4 = r5.append(r6)     // Catch:{ FileNotFoundException -> 0x0031, IOException -> 0x003a, all -> 0x0035 }
            java.lang.String r0 = r4.toString()     // Catch:{ FileNotFoundException -> 0x0031, IOException -> 0x003a, all -> 0x0035 }
            goto L_0x000d
        L_0x002c:
            r2.close()     // Catch:{ FileNotFoundException -> 0x0031, IOException -> 0x003a, all -> 0x0035 }
        L_0x002f:
            monitor-exit(r1)
            return r0
        L_0x0031:
            r0 = move-exception
            java.lang.String r0 = ""
            goto L_0x002f
        L_0x0035:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0037 }
        L_0x0037:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x003a:
            r2 = move-exception
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.b.a.e(android.content.Context, java.lang.String):java.lang.String");
    }

    private static synchronized boolean e(Context context) {
        boolean z = true;
        synchronized (a.class) {
            String g2 = android.support.v4.b.a.g(context);
            SharedPreferences g3 = g(context);
            String string = g3.getString("sessionId", null);
            if (System.currentTimeMillis() - g3.getLong("endTime", -1) > 30000) {
                if (string != null) {
                    j(context);
                    a(context, g3);
                    SharedPreferences.Editor edit = g3.edit();
                    edit.clear();
                    edit.commit();
                }
                String valueOf = String.valueOf(android.support.v4.b.a.a(System.currentTimeMillis() + g2 + android.support.v4.b.a.a(context) + android.support.v4.b.a.f(context) + new Random().nextInt(10)).toCharArray(), 8, 16);
                SharedPreferences.Editor edit2 = g3.edit();
                edit2.putString("appKey", g2);
                edit2.putString("sessionId", valueOf);
                edit2.putLong("lastResumeTime", System.currentTimeMillis());
                edit2.putString("activities", d(context, "onResume", null));
                edit2.commit();
                j = valueOf;
                a(context, g3);
                SharedPreferences.Editor edit3 = g3.edit();
                edit3.putString("appKey", g2);
                edit3.putString("sessionId", j);
                edit3.putLong("lastResumeTime", System.currentTimeMillis());
                edit3.putString("activities", PoiTypeDef.All);
                edit3.commit();
                String str = j;
            } else {
                String string2 = g3.getString("activities", null);
                if (string2.getBytes().length > 10000) {
                    a(context, g3);
                    string2 = PoiTypeDef.All;
                }
                SharedPreferences.Editor edit4 = g3.edit();
                edit4.putString("activities", d(context, "onResume", string2));
                edit4.putLong("lastResumeTime", System.currentTimeMillis());
                edit4.commit();
                z = false;
            }
        }
        return z;
    }

    private static SharedPreferences f(Context context) {
        return context.getSharedPreferences("MoblieAgent_config_" + context.getPackageName(), 0);
    }

    private static void f(Context context, String str) {
        context.deleteFile(str);
        g(context, str);
    }

    private static SharedPreferences g(Context context) {
        return context.getSharedPreferences("MoblieAgent_state_" + context.getPackageName(), 0);
    }

    private static void g(Context context, String str) {
        synchronized (k) {
            SharedPreferences h2 = h(context);
            h2.edit().putString("uploadList", h2.getString("uploadList", PoiTypeDef.All).replace(str + "|", PoiTypeDef.All)).commit();
        }
    }

    private static SharedPreferences h(Context context) {
        return context.getSharedPreferences("MoblieAgent_upload_" + context.getPackageName(), 0);
    }

    private static JSONObject i(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("pid", 1);
            jSONObject.put("protocolVersion", "1.0.4");
            jSONObject.put("sdkVersion", "1.0.4");
            jSONObject.put("cid", android.support.v4.b.a.b(context));
            jSONObject.put("appKey", android.support.v4.b.a.g(context));
            jSONObject.put("packageName", context.getPackageName());
            jSONObject.put("versionCode", android.support.v4.b.a.j(context));
            jSONObject.put("versionName", android.support.v4.b.a.k(context));
            jSONObject.put("sendTime", System.currentTimeMillis());
            jSONObject.put("deviceId", android.support.v4.b.a.a(context));
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        return jSONObject;
    }

    private static boolean j(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("MoblieAgent_event_" + context.getPackageName(), 0);
        String string = sharedPreferences.getString("eventlogs", PoiTypeDef.All);
        if (string.equals(PoiTypeDef.All)) {
            return false;
        }
        String string2 = g(context).getString("sessionId", null);
        c = new JSONObject();
        try {
            c.put("sid", string2);
            c.put("logJsonAry", string);
            if (a(context, c.toString(), 2)) {
                sharedPreferences.edit().putString("eventlogs", PoiTypeDef.All).commit();
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        return true;
    }

    private static synchronized long k(Context context) {
        long j2;
        synchronized (a.class) {
            j2 = h(context).getLong("uploadpopindex", 0);
        }
        return j2;
    }

    /* access modifiers changed from: private */
    public static String l(Context context) {
        String str;
        synchronized (k) {
            str = PoiTypeDef.All;
            String string = h(context).getString("uploadList", PoiTypeDef.All);
            if (!string.equals(PoiTypeDef.All)) {
                str = string.substring(0, string.indexOf("|"));
            }
        }
        return str;
    }
}
