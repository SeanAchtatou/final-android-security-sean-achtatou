package com.ccit.mmwlan.a;

import android.util.Log;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.ccit.mmwlan.b.b;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import org.apache.http.HttpHost;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

public final class e {
    private static b b;
    private static String c;
    private static String d = b.a("readTimeout");
    private static final int e = Integer.parseInt(c);
    private static final int f = Integer.parseInt(d);

    /* renamed from: a  reason: collision with root package name */
    private StringBuilder f599a;

    static {
        b bVar = new b();
        b = bVar;
        c = bVar.a("connectTimeout");
    }

    public static byte[] a(String str, byte[] bArr, HttpHost httpHost) {
        Log.v("MmClientSdk", "doPostByHttpClient() url -> " + str);
        Log.v("MmClientSdk", "doPostByHttpClient() request -> " + new String(bArr, "UTF-8"));
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpParams params = defaultHttpClient.getParams();
        HttpConnectionParams.setConnectionTimeout(params, e);
        HttpConnectionParams.setSoTimeout(params, f);
        if (!(httpHost == null || httpHost.getHostName() == null || httpHost.getHostName().equals(PoiTypeDef.All))) {
            defaultHttpClient.getParams().setParameter("http.route.default-proxy", httpHost);
            Log.v("MmClientSdk", "doPostByHttpClient()  used wapHost -> " + httpHost.getHostName() + ":" + httpHost.getPort());
        }
        HttpPost httpPost = new HttpPost(str);
        httpPost.setHeader("Charset", "UTF-8");
        httpPost.setHeader("Content-Type", "text/xml");
        httpPost.setEntity(new ByteArrayEntity(bArr));
        DataInputStream dataInputStream = new DataInputStream(defaultHttpClient.execute(httpPost).getEntity().getContent());
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr2 = new byte[4096];
        while (true) {
            int read = dataInputStream.read(bArr2);
            if (read < 0) {
                defaultHttpClient.getConnectionManager().shutdown();
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                Log.v("MmClientSdk", "doPost() response -> " + new String(byteArray, "UTF-8"));
                return byteArray;
            }
            byteArrayOutputStream.write(bArr2, 0, read);
            byteArrayOutputStream.flush();
        }
    }

    public final String a(String str, String str2, String str3, String str4, String str5) {
        this.f599a = new StringBuilder();
        this.f599a.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        this.f599a.append("<request>");
        this.f599a.append("<sid>").append(str2).append("</sid>");
        this.f599a.append("<pubkey>").append(str3).append("</pubkey>");
        this.f599a.append("<imsi>").append(str4).append("</imsi>");
        this.f599a.append("<id_mode>").append(str5).append("</id_mode>");
        this.f599a.append("<appid>").append(str).append("</appid>");
        this.f599a.append("</request>");
        return this.f599a.toString();
    }

    public final String a(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        this.f599a = new StringBuilder();
        this.f599a.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        this.f599a.append("<request>");
        this.f599a.append("<sid>").append(str2).append("</sid>");
        this.f599a.append("<pubkey>").append(str3).append("</pubkey>");
        this.f599a.append("<deviceid>").append(str4).append("</deviceid>");
        this.f599a.append("<loginType>").append(str5).append("</loginType>");
        this.f599a.append("<userName>").append(str6).append("</userName>");
        this.f599a.append("<passCode>").append(str7).append("</passCode>");
        this.f599a.append("<appid>").append(str).append("</appid>");
        this.f599a.append("</request>");
        return this.f599a.toString();
    }
}
