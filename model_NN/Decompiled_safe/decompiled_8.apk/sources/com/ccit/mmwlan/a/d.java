package com.ccit.mmwlan.a;

import android.util.Log;
import com.weibo.sdk.android.api.WeiboAPI;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private StringBuilder f598a;

    public static byte[] a(String str, byte[] bArr) {
        int length = bArr.length;
        URL url = new URL(str);
        Log.v("HTTPConnectionTool", "doPost() url -> " + url.toString());
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setRequestMethod(WeiboAPI.HTTPMETHOD_POST);
        httpURLConnection.setRequestProperty("Connection", "close");
        httpURLConnection.setRequestProperty("Charset", "UTF-8");
        httpURLConnection.setRequestProperty("Content-Type", "text/xml");
        httpURLConnection.setRequestProperty("Content-length", String.valueOf(length));
        httpURLConnection.setConnectTimeout(8000);
        httpURLConnection.setReadTimeout(8000);
        httpURLConnection.setDoInput(true);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setUseCaches(false);
        OutputStream outputStream = httpURLConnection.getOutputStream();
        outputStream.write(bArr);
        outputStream.flush();
        outputStream.close();
        DataInputStream dataInputStream = new DataInputStream(httpURLConnection.getInputStream());
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr2 = new byte[6000];
        while (true) {
            int read = dataInputStream.read(bArr2);
            if (read < 0) {
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr2, 0, read);
            byteArrayOutputStream.flush();
        }
    }

    public final String a(String str) {
        this.f598a = null;
        this.f598a = new StringBuilder();
        this.f598a.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        this.f598a.append("<request>");
        this.f598a.append("<deviceID>").append(str).append("</deviceID>");
        this.f598a.append("</request>");
        return this.f598a.toString();
    }

    public final String a(String str, String str2, String str3, String str4, String str5) {
        this.f598a = null;
        this.f598a = new StringBuilder();
        this.f598a.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        this.f598a.append("<request>");
        this.f598a.append("<appid>").append(str).append("</appid>");
        this.f598a.append("<DeviceId>").append(str2).append("</DeviceId>");
        this.f598a.append("<sid>").append(str3).append("</sid>");
        this.f598a.append("<pubkey>").append(str4).append("</pubkey>");
        if (str5 == null) {
            this.f598a.append("<deviceAuthorizationCode></deviceAuthorizationCode>");
        } else {
            this.f598a.append("<deviceAuthorizationCode>").append(str5).append("</deviceAuthorizationCode>");
        }
        this.f598a.append("</request>");
        return this.f598a.toString();
    }

    public final String a(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        this.f598a = null;
        this.f598a = new StringBuilder();
        this.f598a.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        this.f598a.append("<request>");
        this.f598a.append("<appid>").append(str).append("</appid>");
        this.f598a.append("<mobilePhone>").append(str2).append("</mobilePhone>");
        this.f598a.append("<deviceID>").append(str3).append("</deviceID>");
        this.f598a.append("<mac></mac>");
        this.f598a.append("<deviceName>").append(str5).append("</deviceName>");
        this.f598a.append("<MODEL></MODEL>");
        this.f598a.append("<VERSION></VERSION>");
        this.f598a.append("</request>");
        return this.f598a.toString();
    }
}
