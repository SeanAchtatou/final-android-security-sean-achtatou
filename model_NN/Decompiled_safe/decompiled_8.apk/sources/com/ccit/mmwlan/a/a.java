package com.ccit.mmwlan.a;

import java.util.ArrayList;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public final class a extends DefaultHandler {

    /* renamed from: a  reason: collision with root package name */
    private com.ccit.mmwlan.vo.a f595a;
    private ArrayList b = null;
    private StringBuilder c = null;
    private String d = null;
    private boolean e = false;

    public final ArrayList a() {
        return this.b;
    }

    public final void characters(char[] cArr, int i, int i2) {
        if (this.e) {
            this.c.setLength(0);
            this.c.append(cArr, i, i2);
        }
        super.characters(cArr, i, i2);
    }

    public final void endDocument() {
        super.endDocument();
    }

    public final void endElement(String str, String str2, String str3) {
        super.endElement(str, str2, str3);
        if ("resp".equals(str2) || "response".equals(str2)) {
            this.e = false;
            this.b.add(this.f595a);
        } else if ("result".equals(str2)) {
            this.f595a.a(this.c.toString().trim());
            this.d = this.c.toString().trim();
            this.c.setLength(0);
        } else if ("errormsg".equals(str2)) {
            this.f595a.b(this.c.toString().trim());
            this.c.setLength(0);
        } else if ("cert".equals(str2)) {
            if (!"1".equals(this.d.toString().trim())) {
                this.f595a.c(this.c.toString().trim());
                this.c.setLength(0);
            }
        } else if ("randNum".equals(str2)) {
            if (!"1".equals(this.d.toString().trim())) {
                this.f595a.d(this.c.toString().trim());
                this.c.setLength(0);
            }
        } else if ("encData".equals(str2) && !"1".equals(this.d.toString().trim())) {
            this.f595a.e(this.c.toString().trim());
            this.c.setLength(0);
        }
    }

    public final void startDocument() {
        super.startDocument();
        this.b = new ArrayList();
        this.c = new StringBuilder();
    }

    public final void startElement(String str, String str2, String str3, Attributes attributes) {
        if ("resp".equals(str2) || "response".equals(str2)) {
            this.e = true;
            this.f595a = new com.ccit.mmwlan.vo.a();
        }
        super.startElement(str, str2, str3, attributes);
    }
}
