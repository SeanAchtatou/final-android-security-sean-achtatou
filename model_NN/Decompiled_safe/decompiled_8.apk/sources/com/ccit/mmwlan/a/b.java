package com.ccit.mmwlan.a;

import java.util.ArrayList;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public final class b extends DefaultHandler {

    /* renamed from: a  reason: collision with root package name */
    private com.ccit.mmwlan.vo.b f596a;
    private ArrayList b = null;
    private StringBuilder c = null;
    private boolean d = false;

    public final ArrayList a() {
        return this.b;
    }

    public final void characters(char[] cArr, int i, int i2) {
        if (this.d) {
            this.c.setLength(0);
            this.c.append(cArr, i, i2);
        }
        super.characters(cArr, i, i2);
    }

    public final void endDocument() {
        super.endDocument();
    }

    public final void endElement(String str, String str2, String str3) {
        super.endElement(str, str2, str3);
        if ("response".equals(str2)) {
            this.d = false;
            this.b.add(this.f596a);
        } else if ("result".equals(str2)) {
            this.f596a.a(this.c.toString().trim());
            this.c.setLength(0);
        } else if ("errormsg".equals(str2)) {
            this.f596a.b(this.c.toString().trim());
            this.c.setLength(0);
        }
    }

    public final void startDocument() {
        super.startDocument();
        this.b = new ArrayList();
        this.c = new StringBuilder();
    }

    public final void startElement(String str, String str2, String str3, Attributes attributes) {
        if ("response".equals(str2)) {
            this.d = true;
            this.f596a = new com.ccit.mmwlan.vo.b();
        }
        super.startElement(str, str2, str3, attributes);
    }
}
