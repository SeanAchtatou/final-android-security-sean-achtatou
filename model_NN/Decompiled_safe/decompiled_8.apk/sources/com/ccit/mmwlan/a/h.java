package com.ccit.mmwlan.a;

import com.jcraft.jzlib.GZIPHeader;
import mm.purchasesdk.PurchaseCode;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    private static int f602a = -16909061;
    private static int b = PurchaseCode.NOT_CMCC_ERR;
    private static int c = 0;

    /* JADX WARN: Type inference failed for: r1v2, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(byte[] r10) {
        /*
            r9 = 4
            r8 = 2
            r3 = 0
            r1 = 0
            byte[] r0 = new byte[r9]
            java.io.ByteArrayInputStream r5 = new java.io.ByteArrayInputStream
            r5.<init>(r10)
            r5.mark(r3)
            r5.reset()
            int r2 = r0.length
            r5.read(r0, r3, r2)
            r2 = 1
            r5.mark(r2)
            int r2 = b(r0, r3)
            int r4 = com.ccit.mmwlan.a.h.f602a
            if (r2 == r4) goto L_0x0036
            java.lang.Exception r0 = new java.lang.Exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r3 = "Protocol Version error! protocolVersion="
            r1.<init>(r3)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0036:
            int r2 = r0.length
            r5.read(r0, r3, r2)
            r5.mark(r8)
            int r2 = r0.length
            r5.read(r0, r3, r2)
            r2 = 3
            r5.mark(r2)
            int r2 = b(r0, r3)
            java.lang.String r4 = "MmClientSdk"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r7 = "MoServerHttpPostResponse() messageType -> "
            r6.<init>(r7)
            java.lang.StringBuilder r2 = r6.append(r2)
            java.lang.String r2 = r2.toString()
            android.util.Log.v(r4, r2)
            int r2 = r0.length
            r5.read(r0, r3, r2)
            r5.mark(r9)
            int r2 = b(r0, r3)
            int r4 = com.ccit.mmwlan.a.h.b
            if (r2 == r4) goto L_0x0081
            java.lang.Exception r0 = new java.lang.Exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r3 = "TransactionID error! TransactionID="
            r1.<init>(r3)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0081:
            byte[] r6 = new byte[r8]
            int r2 = r6.length
            r5.read(r6, r3, r2)
            r2 = 5
            r5.mark(r2)
            short r2 = a(r6, r3)
            if (r2 == 0) goto L_0x00a6
            java.lang.Exception r0 = new java.lang.Exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r3 = "ReturnCode error! returnCode="
            r1.<init>(r3)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x00a6:
            int r2 = r6.length
            r5.read(r6, r3, r2)
            r2 = 6
            r5.mark(r2)
            short r7 = a(r6, r3)
            r2 = r3
        L_0x00b3:
            if (r2 < r7) goto L_0x00b6
            return r1
        L_0x00b6:
            int r4 = r6.length
            r5.read(r6, r3, r4)
            r4 = 7
            r5.mark(r4)
            int r4 = r6.length
            r5.read(r6, r3, r4)
            r4 = 8
            r5.mark(r4)
            int r4 = r0.length
            r5.read(r0, r3, r4)
            r4 = 9
            r5.mark(r4)
            int r0 = b(r0, r3)
            byte[] r4 = new byte[r0]
            int r0 = r4.length
            r5.read(r4, r3, r0)
            r0 = 10
            r5.mark(r0)
            java.lang.String r0 = new java.lang.String     // Catch:{ UnsupportedEncodingException -> 0x00ec }
            java.lang.String r8 = "utf-8"
            r0.<init>(r4, r8)     // Catch:{ UnsupportedEncodingException -> 0x00ec }
        L_0x00e6:
            int r1 = r2 + 1
            r2 = r1
            r1 = r0
            r0 = r4
            goto L_0x00b3
        L_0x00ec:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r1
            goto L_0x00e6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ccit.mmwlan.a.h.a(byte[]):java.lang.String");
    }

    private static short a(byte[] bArr, int i) {
        return (short) ((((short) (bArr[0] & GZIPHeader.OS_UNKNOWN)) << 8) | ((short) (bArr[1] & GZIPHeader.OS_UNKNOWN)));
    }

    private static int b(byte[] bArr, int i) {
        return ((bArr[0] & GZIPHeader.OS_UNKNOWN) << 24) | ((bArr[1] & GZIPHeader.OS_UNKNOWN) << 16) | ((bArr[2] & GZIPHeader.OS_UNKNOWN) << 8) | (bArr[3] & GZIPHeader.OS_UNKNOWN);
    }
}
