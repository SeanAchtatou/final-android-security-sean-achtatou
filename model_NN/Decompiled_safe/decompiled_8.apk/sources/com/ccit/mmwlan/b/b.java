package com.ccit.mmwlan.b;

import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class b {
    public final String a(String str) {
        try {
            InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream("config.properties");
            Properties properties = new Properties();
            properties.load(resourceAsStream);
            return properties.getProperty(str);
        } catch (IOException e) {
            e.printStackTrace();
            Log.v("ReadConfigFile", e.toString());
            return null;
        }
    }
}
