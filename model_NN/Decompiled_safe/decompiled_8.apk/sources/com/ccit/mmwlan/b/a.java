package com.ccit.mmwlan.b;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.ccit.mmwlan.exception.ClientSDKException;
import com.ccit.mmwlan.vo.DeviceInfo;

public final class a {
    public static DeviceInfo a(Context context, int i) {
        DeviceInfo deviceInfo = new DeviceInfo();
        if (context == null || !(i == 0 || i == 1 || i == 2)) {
            throw new ClientSDKException("参数错误!");
        }
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (i == 0) {
            String a2 = a(false);
            String deviceId = telephonyManager.getDeviceId();
            if (deviceId == null || deviceId.equals(PoiTypeDef.All)) {
                deviceId = "000000000000000";
            }
            if (a2 == null || a2.equals(PoiTypeDef.All)) {
                throw new ClientSDKException("获取设备信息imsi或imei失败!");
            }
            deviceInfo.setStrImsi(a2);
            deviceInfo.setStrImei(deviceId);
        }
        if (i == 1) {
            String a3 = a(true);
            String deviceId2 = telephonyManager.getDeviceId();
            if (deviceId2 == null || deviceId2.equals(PoiTypeDef.All)) {
                deviceId2 = "000000000000000";
            }
            if (a3 == null || a3.equals(PoiTypeDef.All)) {
                throw new ClientSDKException("获取设备信息imsi或imei失败!");
            }
            deviceInfo.setStrImsi(a3);
            deviceInfo.setStrImei(deviceId2);
        }
        if (i == 2) {
            String macAddress = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress();
            if (macAddress == null || macAddress.equals(PoiTypeDef.All)) {
                throw new ClientSDKException("获取设备信息mac失败!");
            }
            String[] split = macAddress.split(":");
            String str = PoiTypeDef.All;
            for (int i2 = 0; i2 < split.length; i2++) {
                str = String.valueOf(str) + split[i2].trim();
            }
            deviceInfo.setStrMac(str);
        }
        String str2 = context.getFilesDir().getPath().toString();
        if (str2 == null || str2.equals(PoiTypeDef.All)) {
            throw new ClientSDKException("获取应用私有路径失败!");
        }
        deviceInfo.setFilePath(str2);
        Log.v("MmClientSdk", deviceInfo.toString());
        return deviceInfo;
    }

    private static String a(boolean z) {
        Object a2;
        Object a3;
        String str;
        while (true) {
            Class[] clsArr = {String.class};
            Object[] objArr = new Object[1];
            objArr[0] = z ? "iphonesubinfo2" : "iphonesubinfo1";
            a2 = c.a("android.os.ServiceManager", "getService", clsArr, objArr);
            if (!z && a2 == null) {
                a2 = c.a("android.os.ServiceManager", "getService", new Class[]{String.class}, new Object[]{"iphonesubinfo"});
            }
            if (a2 == null && z) {
                z = false;
            } else if (a2 != null || (a3 = c.a("com.android.internal.telephony.IPhoneSubInfo$Stub", "asInterface", new Class[]{IBinder.class}, new Object[]{a2})) == null) {
                return PoiTypeDef.All;
            } else {
                str = (String) c.a(a3, "getSubscriberId", (Class[]) null, (Object[]) null);
                if (str == null || str.equals(PoiTypeDef.All)) {
                    str = (String) c.a(a3, "getSubscriberIdExt", new Class[]{Integer.TYPE}, new Object[]{5});
                }
                Log.w("MmClientSdk", "getSubscriberId=" + str);
                return str;
            }
        }
        if (a2 != null) {
            return PoiTypeDef.All;
        }
        str = (String) c.a(a3, "getSubscriberId", (Class[]) null, (Object[]) null);
        str = (String) c.a(a3, "getSubscriberIdExt", new Class[]{Integer.TYPE}, new Object[]{5});
        Log.w("MmClientSdk", "getSubscriberId=" + str);
        return str;
    }
}
