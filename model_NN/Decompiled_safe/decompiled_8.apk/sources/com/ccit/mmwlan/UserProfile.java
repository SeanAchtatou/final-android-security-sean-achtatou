package com.ccit.mmwlan;

import org.apache.http.HttpHost;

public class UserProfile {
    public String appID = null;
    public int cardSlot = -1;
    public int loginType = -1;
    public HttpHost moServerHost = null;
    public String password = null;
    public String userName = null;
}
