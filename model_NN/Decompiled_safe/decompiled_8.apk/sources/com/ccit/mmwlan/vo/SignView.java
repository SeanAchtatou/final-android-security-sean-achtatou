package com.ccit.mmwlan.vo;

import com.amap.mapapi.poisearch.PoiTypeDef;

public class SignView {
    private String m_strErrMsg = PoiTypeDef.All;
    private int result;
    private String userSignature;

    public String getErrMsg() {
        return this.m_strErrMsg;
    }

    public int getResult() {
        return this.result;
    }

    public String getUserSignature() {
        return this.userSignature;
    }

    public void setErrMsg(String str) {
        this.m_strErrMsg = str;
    }

    public void setResult(int i) {
        this.result = i;
    }

    public void setUserSignature(String str) {
        this.userSignature = str;
    }
}
