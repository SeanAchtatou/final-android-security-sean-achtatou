package com.ccit.mmwlan.vo;

public class IPDress_ForPad {
    private String strApplySecCertIP;
    private String strDeviceOuthIp;
    private String strGetDeviceName;

    public String getStrApplySecCertIP() {
        return this.strApplySecCertIP;
    }

    public String getStrDeviceOuthIp() {
        return this.strDeviceOuthIp;
    }

    public String getStrGetDeviceName() {
        return this.strGetDeviceName;
    }

    public void setStrApplySecCertIP(String str) {
        this.strApplySecCertIP = str;
    }

    public void setStrDeviceOuthIp(String str) {
        this.strDeviceOuthIp = str;
    }

    public void setStrGetDeviceName(String str) {
        this.strGetDeviceName = str;
    }
}
