package com.ccit.mmwlan;

import com.ccit.mmwlan.vo.DeviceInfo;

public final class ClientSDK {
    static {
        System.loadLibrary("casdkjni");
    }

    public final native String AsymmetricDecryptionForBilling(String str, String str2);

    public final native String AsymmetricEncryptionForBilling(String str);

    public final native String AsymmetricEncryptionForMMLogin(int i, String str, DeviceInfo deviceInfo, String str2);

    public final native int DestorySecCertForBilling(String str);

    public final native String SIDSignNativeForBilling(String str, String str2, String str3);

    public final native String SignNativeForLogin(String str, String str2, int i, String str3, DeviceInfo deviceInfo);

    public final native String SymmetricDecryptionForBilling(String str, int i, String str2);

    public final native int UpdateRandNumForBilling(String str);

    public final native int UpdateRandNumForLogin(String str, int i, String str2, DeviceInfo deviceInfo, String str3);

    public final native boolean VerifyWithCertForBilling(String str, String str2, String str3, int i);

    public final native int checkSecCertNativeForBilling();

    public final native int checkSecCertNativeForLogin(int i, String str, DeviceInfo deviceInfo);

    public final native String encWithCertNativeForLogin(String str, String str2);

    public final native int genPKIKeyNativeForBilling();

    public final native int genPKIKeyNativeForLogin(int i, String str, DeviceInfo deviceInfo);

    public final native String genSIDNative();

    public final native String genTempPKIKeyNativeForBilling();

    public final native int getCertStateNativeForBilling();

    public final native String getDigestNative(String str, String str2);

    public final native String getPubKeyForBilling();

    public final native String getPubKeyForLogin(int i, String str, DeviceInfo deviceInfo);

    public final native String getSecCertNativeForBilling();

    public final native int saveSecCertNativeForBilling(String str, String str2);

    public final native int saveSecCertNativeForLogin(String str, String str2, int i, String str3, DeviceInfo deviceInfo, String str4);

    public final native int transmitInfoNative(DeviceInfo deviceInfo);
}
