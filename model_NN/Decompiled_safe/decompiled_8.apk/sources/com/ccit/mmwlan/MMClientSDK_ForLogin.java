package com.ccit.mmwlan;

import android.content.Context;
import android.util.Log;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.ccit.mmwlan.a.e;
import com.ccit.mmwlan.a.f;
import com.ccit.mmwlan.a.g;
import com.ccit.mmwlan.a.h;
import com.ccit.mmwlan.b.a;
import com.ccit.mmwlan.b.b;
import com.ccit.mmwlan.exception.ClientSDKException;
import com.ccit.mmwlan.vo.DeviceInfo;
import java.util.ArrayList;
import org.apache.http.HttpHost;

public final class MMClientSDK_ForLogin {
    private static final int INT_RESULT_0 = 0;
    private static final int INT_RESULT_1 = 1;
    private static ClientSDK clientSDK;
    private static Context context = null;
    private static a getDevInfo = new a();

    static {
        clientSDK = null;
        clientSDK = new ClientSDK();
    }

    public static String SIDSign(int i, int i2, String str, String str2, String str3) {
        if ((i != 0 && i != 1 && i != 2) || ((i2 != 1 && i2 != 2) || str2 == null || str2.equals(PoiTypeDef.All) || str3 == null || str3.equals(PoiTypeDef.All))) {
            Log.v("MmClientSdk", "SIDSign() result -> " + ((String) null));
            return null;
        } else if (i2 != 2 || (str != null && !str.equals(PoiTypeDef.All))) {
            try {
                DeviceInfo a2 = a.a(context, i);
                if (i == 2) {
                    a2.getStrMac();
                } else {
                    a2.getStrImsi();
                }
                try {
                    String SignNativeForLogin = clientSDK.SignNativeForLogin(str2, str3, i2, str, a2);
                    if (SignNativeForLogin == null || SignNativeForLogin.equals(PoiTypeDef.All)) {
                        Log.v("MmClientSdk", "SIDSign() result -> " + ((String) null) + "; 签名失败!");
                        return null;
                    }
                    String str4 = "#V1.0#" + SignNativeForLogin;
                    Log.v("MmClientSdk", "SIDSign() result -> " + str4);
                    return str4;
                } catch (ClientSDKException e) {
                    e.printStackTrace();
                    Log.v("MmClientSdk", "SIDSign() ClientSDKException -> " + e.toString());
                    return null;
                }
            } catch (ClientSDKException e2) {
                e2.printStackTrace();
                Log.v("MmClientSdk", "SIDSign() ClientSDKException -> " + e2.toString());
                return null;
            }
        } else {
            Log.v("MmClientSdk", "SIDSign() result -> " + ((String) null));
            return null;
        }
    }

    public static int applySecCert(int i, int i2, String str, String str2, String str3, String str4, String str5, HttpHost httpHost, HttpHost httpHost2, int i3) {
        if ((i != 0 && i != 1 && i != 2) || ((i2 != 1 && i2 != 2) || str5 == null || str5.equals(PoiTypeDef.All) || str4 == null || str4.equals(PoiTypeDef.All) || httpHost.getHostName() == null || httpHost.getHostName().equals(PoiTypeDef.All))) {
            Log.v("MmClientSdk", "applySecCert() result -> 5");
            return 5;
        } else if (i2 != 2 || (str != null && !str.equals(PoiTypeDef.All) && str2 != null && !str2.equals(PoiTypeDef.All))) {
            try {
                DeviceInfo a2 = a.a(context, i);
                try {
                    String digestNative = clientSDK.getDigestNative("md5", i == 2 ? a2.getStrMac() : a2.getStrImsi());
                    try {
                        String pubKeyForLogin = clientSDK.getPubKeyForLogin(i2, str, a2);
                        if (pubKeyForLogin == null || pubKeyForLogin.equals(PoiTypeDef.All)) {
                            if (clientSDK.genPKIKeyNativeForLogin(i2, str, a2) != 0) {
                                Log.v("MmClientSdk", "applySecCert() result -> 1");
                                return 1;
                            }
                            pubKeyForLogin = clientSDK.getPubKeyForLogin(i2, str, a2);
                        }
                        String[] applySecCertFromMoserver = applySecCertFromMoserver(str4, digestNative, pubKeyForLogin, i2, str, str2, str5, httpHost, httpHost2);
                        if (applySecCertFromMoserver[0] == null || applySecCertFromMoserver[0].equals(PoiTypeDef.All)) {
                            Log.v("MmClientSdk", "applySecCert() result -> 1");
                            return 1;
                        } else if (applySecCertFromMoserver[0].equals("105")) {
                            int parseInt = Integer.parseInt(applySecCertFromMoserver[0]);
                            Log.v("MmClientSdk", "applySecCert() result -> " + parseInt);
                            return parseInt;
                        } else if (applySecCertFromMoserver[0].equals("107")) {
                            int parseInt2 = Integer.parseInt(applySecCertFromMoserver[0]);
                            Log.v("MmClientSdk", "applySecCert() result -> " + parseInt2);
                            return parseInt2;
                        } else if (applySecCertFromMoserver[0].equals("108")) {
                            int parseInt3 = Integer.parseInt(applySecCertFromMoserver[0]);
                            Log.v("MmClientSdk", "applySecCert() result -> " + parseInt3);
                            return parseInt3;
                        } else if (applySecCertFromMoserver[0].equals("122")) {
                            int parseInt4 = Integer.parseInt(applySecCertFromMoserver[0]);
                            Log.v("MmClientSdk", "applySecCert() result -> " + parseInt4);
                            return parseInt4;
                        } else if (applySecCertFromMoserver[0].equals("500")) {
                            int parseInt5 = Integer.parseInt(applySecCertFromMoserver[0]);
                            Log.v("MmClientSdk", "applySecCert() result -> " + parseInt5);
                            return parseInt5;
                        } else {
                            try {
                                int i4 = clientSDK.saveSecCertNativeForLogin(applySecCertFromMoserver[0], applySecCertFromMoserver[1], i2, str, a2, str4) == 0 ? 0 : 1;
                                Log.v("MmClientSdk", "applySecCert() result -> " + i4);
                                return i4;
                            } catch (ClientSDKException e) {
                                e.printStackTrace();
                                Log.v("MmClientSdk", "saveSecCertNative() ClientSDKException -> " + e.toString());
                                return 1;
                            }
                        }
                    } catch (ClientSDKException e2) {
                        e2.printStackTrace();
                        Log.v("MmClientSdk", "genPKIKeyNative() ClientSDKException -> " + e2.toString());
                        return 1;
                    }
                } catch (ClientSDKException e3) {
                    e3.printStackTrace();
                    Log.v("MmClientSdk", "applySecCert() ClientSDKException -> " + e3.toString());
                    return 1;
                }
            } catch (ClientSDKException e4) {
                e4.printStackTrace();
                Log.v("MmClientSdk", "applySecCert() ClientSDKException -> " + e4.toString());
                return 2;
            }
        } else {
            Log.v("MmClientSdk", "applySecCert() result -> 5");
            return 5;
        }
    }

    private static String[] applySecCertFromMoserver(String str, String str2, String str3, int i, String str4, String str5, String str6, HttpHost httpHost, HttpHost httpHost2) {
        String a2;
        int i2;
        String[] strArr = new String[2];
        String str7 = "http://" + httpHost.getHostName() + ":" + httpHost.getPort() + "/" + new b().a("applySecCertUrl");
        Log.v("MmClientSdk", "applySecCertFromMoserver() applyCertUrl -> " + str7);
        g gVar = new g();
        new h();
        f fVar = new f();
        e eVar = new e();
        if (i == 1) {
            a2 = eVar.a(str, str6, str3, str2, new StringBuilder(String.valueOf(i)).toString());
            i2 = 65568;
        } else {
            a2 = eVar.a(str, str6, str3, str2, new StringBuilder(String.valueOf(i)).toString(), str4, str5);
            i2 = 65584;
        }
        Log.v("MmClientSdk", "applySecCertFromMoserver() requestXml -> " + a2);
        try {
            try {
                ArrayList a3 = fVar.a(h.a(e.a(str7, gVar.a(a2, i2), httpHost2)));
                if (a3.get(0) == null || ((com.ccit.mmwlan.vo.a) a3.get(0)).equals(PoiTypeDef.All)) {
                    Log.v("MmClientSdk", "applySecCertFromMoserver()  Errormsg -> Apply cert failed!");
                    return strArr;
                }
                String a4 = ((com.ccit.mmwlan.vo.a) a3.get(0)).a();
                if (a4 == null || !a4.equals("1")) {
                    String c = ((com.ccit.mmwlan.vo.a) a3.get(0)).c();
                    strArr[0] = c;
                    Log.v("MmClientSdk", "applySecCertFromMoserver() cert -> " + c);
                    String e = ((com.ccit.mmwlan.vo.a) a3.get(0)).e();
                    strArr[1] = e;
                    Log.v("MmClientSdk", "applySecCertFromMoserver() encData -> " + e);
                    return strArr;
                }
                Log.v("MmClientSdk", "applySecCertFromMoserver()  Errormsg -> " + ((com.ccit.mmwlan.vo.a) a3.get(0)).b());
                return strArr;
            } catch (Exception e2) {
                Log.v("MmClientSdk", "applySecCertFromMoserver() Exception -> " + e2.toString());
                e2.printStackTrace();
                String exc = e2.toString();
                if (exc == null || !exc.contains("returnCode")) {
                    return strArr;
                }
                strArr[0] = exc.split("=")[1].trim();
                return strArr;
            }
        } catch (Exception e3) {
            Log.v("MmClientSdk", "applySecCertFromMoserver() Exception -> 与mo.server通信异常\n" + e3.toString());
            e3.printStackTrace();
            return strArr;
        }
    }

    public static int checkSecCert(int i, int i2, String str) {
        int i3 = 1;
        if ((i != 0 && i != 1 && i != 2) || (i2 != 1 && i2 != 2)) {
            Log.v("MmClientSdk", "checkSecCert() result -> 5");
            return 5;
        } else if (i2 != 2 || (str != null && !str.equals(PoiTypeDef.All))) {
            try {
                try {
                    if (clientSDK.checkSecCertNativeForLogin(i2, str, a.a(context, i)) == 0) {
                        i3 = 0;
                    }
                    Log.v("MmClientSdk", "checkSecCert() result -> " + i3);
                    return i3;
                } catch (ClientSDKException e) {
                    e.printStackTrace();
                    Log.v("MmClientSdk", "checkSecCertNative() ClientSDKException -> " + e.toString());
                    return 6;
                }
            } catch (ClientSDKException e2) {
                e2.printStackTrace();
                Log.v("MmClientSdk", "checkSecCert() ClientSDKException -> " + e2.toString());
                return 2;
            }
        } else {
            Log.v("MmClientSdk", "checkSecCert() result -> 5");
            return 5;
        }
    }

    public static String encWithCert(String str) {
        String str2 = null;
        if (str == null || str.equals(PoiTypeDef.All)) {
            Log.v("MmClientSdk", "encWithCert() result -> " + ((String) null));
        } else {
            try {
                str2 = clientSDK.encWithCertNativeForLogin(str, new b().a("uicCert"));
                Log.v("MmClientSdk", "encWithCert() result -> " + str2);
            } catch (ClientSDKException e) {
                e.printStackTrace();
                Log.v("MmClientSdk", "encWithCert() ClientSDKException -> " + e.toString());
            }
        }
        return str2;
    }

    public static String encWithPubKey(int i, int i2, String str, String str2) {
        if ((i != 0 && i != 1 && i != 2) || (i2 != 1 && i2 != 2)) {
            Log.v("MmClientSdk", "encWithPubKey() error -> parameters error!");
            return null;
        } else if (i2 == 2 && (str == null || str.equals(PoiTypeDef.All))) {
            Log.v("MmClientSdk", "encWithPubKey() error -> parameters error!" + ((String) null));
            return null;
        } else if (str2 == null || str2.equals(PoiTypeDef.All)) {
            Log.v("MmClientSdk", "encWithPubKey error --> the fourth parameter error!" + ((String) null));
            return null;
        } else {
            try {
                try {
                    return clientSDK.AsymmetricEncryptionForMMLogin(i2, str, a.a(context, i), str2);
                } catch (ClientSDKException e) {
                    Log.v("MmClientSdk", "encWithPubKey call AsymmetricEncryptionForMMLogin error --> !" + ((String) null));
                    return null;
                }
            } catch (ClientSDKException e2) {
                e2.printStackTrace();
                Log.v("MmClientSdk", "checkSecCert() ClientSDKException -> " + e2.toString());
                return null;
            }
        }
    }

    public static String genPKIKey(int i, int i2, String str) {
        String str2 = null;
        if ((i != 0 && i != 1 && i != 2) || (i2 != 1 && i2 != 2)) {
            Log.v("MmClientSdk", "genPKIKey() error -> parameters error!");
        } else if (i2 != 2 || (str != null && !str.equals(PoiTypeDef.All))) {
            try {
                DeviceInfo a2 = a.a(context, i);
                try {
                    if (clientSDK.genPKIKeyNativeForLogin(i2, str, a2) != 0) {
                        Log.v("MmClientSdk", "genPKIKey() error -> generation pki key failed!");
                    } else {
                        try {
                            str2 = clientSDK.getPubKeyForLogin(i2, str, a2);
                            Log.v("MmClientSdk", "genPKIKey() pubKey -> " + str2);
                        } catch (ClientSDKException e) {
                            e.printStackTrace();
                            Log.v("MmClientSdk", "genPKIKeyNative() ClientSDKException -> " + e.toString());
                        }
                    }
                } catch (ClientSDKException e2) {
                    e2.printStackTrace();
                    Log.v("MmClientSdk", "genPKIKeyNative() ClientSDKException -> " + e2.toString());
                }
            } catch (ClientSDKException e3) {
                e3.printStackTrace();
                Log.v("MmClientSdk", "checkSecCert() ClientSDKException -> " + e3.toString());
            }
        } else {
            Log.v("MmClientSdk", "genPKIKey() error -> parameters error!" + ((String) null));
        }
        return str2;
    }

    public static String genSID() {
        ClientSDKException e;
        String str;
        try {
            str = clientSDK.genSIDNative();
            try {
                Log.v("MmClientSdk", "genSID() strResult -> " + str);
            } catch (ClientSDKException e2) {
                e = e2;
                Log.v("MmClientSdk", "genSID() 生成SID失败 -> " + e);
                return str;
            }
        } catch (ClientSDKException e3) {
            ClientSDKException clientSDKException = e3;
            str = null;
            e = clientSDKException;
            Log.v("MmClientSdk", "genSID() 生成SID失败 -> " + e);
            return str;
        }
        return str;
    }

    public static String getDigest(String str, String str2) {
        String str3 = null;
        if (str2 == null || str2.equals(PoiTypeDef.All)) {
            Log.v("MmClientSdk", "getMd5Digest() result -> " + ((String) null));
        } else {
            try {
                str3 = clientSDK.getDigestNative(str, str2);
                Log.v("MmClientSdk", "getMd5Digest() result -> " + str3);
            } catch (ClientSDKException e) {
                e.printStackTrace();
                Log.v("MmClientSdk", "getDigestNative() ClientSDKException -> " + e.toString());
            }
        }
        return str3;
    }

    public static String getVersion() {
        return "1.1.6";
    }

    public static int initMmClientSdk(Context context2, int i) {
        context = context2;
        try {
            try {
                return clientSDK.transmitInfoNative(a.a(context, i)) == 0 ? 0 : 1;
            } catch (ClientSDKException e) {
                e.printStackTrace();
                Log.v("MmClientSdk", "initMmClientSdk() ClientSDKException -> " + e.toString());
                return 1;
            }
        } catch (ClientSDKException e2) {
            e2.printStackTrace();
            Log.v("MmClientSdk", "initMmClientSdk() ClientSDKException -> " + e2.toString());
            return 2;
        }
    }

    public static int saveCert(int i, int i2, String str, String str2, String str3, String str4) {
        if ((i != 0 && i != 1 && i != 2) || ((i2 != 1 && i2 != 2) || str3 == null || str3.equals(PoiTypeDef.All) || str2 == null || str2.equals(PoiTypeDef.All) || str4 == null || str4.equals(PoiTypeDef.All))) {
            Log.v("MmClientSdk", "saveCert() parameter invalid -> 1");
            return 1;
        } else if (i2 != 2 || (str != null && !str.equals(PoiTypeDef.All))) {
            try {
                try {
                    int i3 = clientSDK.saveSecCertNativeForLogin(str3, str4, i2, str, a.a(context, i), str2) == 0 ? 0 : 1;
                    Log.v("MmClientSdk", "saveCert() result -> " + i3);
                    return i3;
                } catch (ClientSDKException e) {
                    e.printStackTrace();
                    Log.v("MmClientSdk", "saveCert(...) call saveSecCertNativeForLogin() throw ClientSDKException -> " + e.toString());
                    return 1;
                }
            } catch (ClientSDKException e2) {
                e2.printStackTrace();
                Log.v("MmClientSdk", "saveCert(...) call getDeviceInfo(...) ClientSDKException -> " + e2.toString());
                return 1;
            }
        } else {
            Log.v("MmClientSdk", "saveCert() the third parameter invalid -> 1");
            return 1;
        }
    }

    public static int updateRandNum(int i, int i2, String str, String str2, String str3) {
        int i3;
        if ((i != 0 && i != 1 && i != 2) || ((i2 != 1 && i2 != 2) || str2 == null || str2.equals(PoiTypeDef.All) || str3 == null || str3.equals(PoiTypeDef.All))) {
            Log.v("MmClientSdk", "updateRandNum() parameter invalid -> 1");
            return 1;
        } else if (i2 != 2 || (str != null && !str.equals(PoiTypeDef.All))) {
            try {
                try {
                    i3 = clientSDK.UpdateRandNumForLogin(str3, i2, str, a.a(context, i), str2);
                    Log.v("MmClientSdk", "updateRandNum() call UpdateRandNumForLogin(...) iResult -> " + i3);
                    if (i3 != 0) {
                        return 1;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    i3 = 1;
                }
                return i3;
            } catch (ClientSDKException e2) {
                e2.printStackTrace();
                Log.v("MmClientSdk", "updateRandNum() call getDeviceInfo(...) ClientSDKException -> " + e2.toString());
                return 1;
            }
        } else {
            Log.v("MmClientSdk", "updateRandNum() the third parameter error -> 1");
            return 1;
        }
    }
}
