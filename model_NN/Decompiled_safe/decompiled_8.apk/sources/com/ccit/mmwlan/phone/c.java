package com.ccit.mmwlan.phone;

import com.weibo.sdk.android.api.WeiboAPI;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import org.apache.http.HttpHost;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private StringBuilder f605a;

    public static byte[] a(String str, byte[] bArr, HttpHost httpHost, String str2) {
        HttpURLConnection httpURLConnection;
        int length = bArr.length;
        URL url = new URL(str);
        if (httpHost != null) {
            httpURLConnection = (HttpURLConnection) url.openConnection(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(httpHost.getHostName(), httpHost.getPort())));
        } else {
            httpURLConnection = (HttpURLConnection) url.openConnection();
        }
        if (str2 != null) {
            long parseLong = Long.parseLong(str2) * 1000;
            synchronized (httpURLConnection) {
                httpURLConnection.wait(parseLong);
            }
        } else {
            synchronized (httpURLConnection) {
                httpURLConnection.wait(7000);
            }
        }
        httpURLConnection.setRequestMethod(WeiboAPI.HTTPMETHOD_POST);
        httpURLConnection.setRequestProperty("Connection", "close");
        httpURLConnection.setRequestProperty("Charset", "UTF-8");
        httpURLConnection.setRequestProperty("Content-Type", "text/xml");
        httpURLConnection.setRequestProperty("Content-length", String.valueOf(length));
        httpURLConnection.setDoInput(true);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setUseCaches(false);
        OutputStream outputStream = httpURLConnection.getOutputStream();
        outputStream.write(bArr);
        outputStream.flush();
        outputStream.close();
        DataInputStream dataInputStream = new DataInputStream(httpURLConnection.getInputStream());
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr2 = new byte[6000];
        while (true) {
            int read = dataInputStream.read(bArr2);
            if (read < 0) {
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr2, 0, read);
            byteArrayOutputStream.flush();
        }
    }

    public final String a(String str, String str2, String str3, String str4) {
        this.f605a = null;
        this.f605a = new StringBuilder();
        this.f605a.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        this.f605a.append("<request>");
        this.f605a.append("<sid>").append(str).append("</sid>");
        this.f605a.append("<appuid>").append(str4).append("</appuid>");
        this.f605a.append("<pubkey>").append(str2).append("</pubkey>");
        this.f605a.append("<imsi>").append(str3).append("</imsi>");
        this.f605a.append("</request>");
        return this.f605a.toString();
    }
}
