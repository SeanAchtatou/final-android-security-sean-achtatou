package com.ccit.mmwlan.phone;

import java.util.ArrayList;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public final class a extends DefaultHandler {

    /* renamed from: a  reason: collision with root package name */
    private b f603a;
    private ArrayList b = null;
    private StringBuilder c = null;
    private String d = null;
    private boolean e = false;

    public final ArrayList a() {
        return this.b;
    }

    public final void characters(char[] cArr, int i, int i2) {
        if (this.e) {
            this.c.setLength(0);
            this.c.append(cArr, i, i2);
        }
        super.characters(cArr, i, i2);
    }

    public final void endDocument() {
        super.endDocument();
    }

    public final void endElement(String str, String str2, String str3) {
        super.endElement(str, str2, str3);
        if ("response".equals(str2)) {
            this.e = false;
            this.b.add(this.f603a);
        } else if ("result".equals(str2)) {
            this.f603a.a(this.c.toString().trim());
            this.d = this.c.toString().trim();
            this.c.setLength(0);
        } else if ("errormsg".equals(str2)) {
            this.c.toString().trim();
            this.c.setLength(0);
        } else if ("cert".equals(str2)) {
            if (!"1".equals(this.d.toString().trim())) {
                this.f603a.b(this.c.toString().trim());
                this.c.setLength(0);
            }
        } else if ("mobilePhone".equals(str2)) {
            this.c.toString().trim();
            this.c.setLength(0);
        } else if ("encKey".equals(str2)) {
            this.f603a.c(this.c.toString().trim());
            this.c.setLength(0);
        } else if ("randNum".equals(str2)) {
            this.f603a.d(this.c.toString().trim());
            this.c.setLength(0);
        }
    }

    public final void startDocument() {
        super.startDocument();
        this.b = new ArrayList();
        this.c = new StringBuilder();
    }

    public final void startElement(String str, String str2, String str3, Attributes attributes) {
        if ("response".equals(str2)) {
            this.e = true;
            this.f603a = new b();
        }
        super.startElement(str, str2, str3, attributes);
    }
}
