package com.ccit.mmwlan.phone;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.ccit.mmwlan.ClientSDK;
import com.ccit.mmwlan.vo.DeviceInfo;
import com.ccit.mmwlan.vo.SignView;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.http.HttpHost;

public final class MMClientSDK_ForPhone {
    private static final int INT_RESULT_0 = 0;
    private static final int INT_RESULT_1 = 1;
    private static final int INT_RESULT_2 = 2;
    private static final int INT_RESULT_3 = 3;
    private static final int INT_RESULT_4 = 4;
    private static final int INT_RESULT_5 = 5;
    private static final int INT_RESULT_6 = 6;
    private static final int INT_RESULT_7 = 7;
    private static final String MMCLIENT_SDK = "MMClientSDK_ForPhone";
    private static String SMSNumber = null;
    private static ClientSDK clientSDK;
    private static Context context = null;
    private static String strApplyCertForPhone = null;

    static {
        clientSDK = null;
        clientSDK = new ClientSDK();
    }

    public static int DestorySecCert(String str) {
        Exception e;
        int i;
        try {
            i = clientSDK.DestorySecCertForBilling(str);
            try {
                Log.v(MMCLIENT_SDK, "DestroySecCert() iRet -> " + i);
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                return i;
            }
        } catch (Exception e3) {
            Exception exc = e3;
            i = -1;
            e = exc;
            e.printStackTrace();
            return i;
        }
        return i;
    }

    public static String RSAEncryptWithPubKey(String str) {
        try {
            String AsymmetricEncryptionForBilling = clientSDK.AsymmetricEncryptionForBilling(str);
            if (AsymmetricEncryptionForBilling == null) {
                AsymmetricEncryptionForBilling = String.valueOf(1);
            }
            Log.v(MMCLIENT_SDK, "RSAEncryptWithPubKey() strRet -> " + AsymmetricEncryptionForBilling);
            return AsymmetricEncryptionForBilling;
        } catch (Exception e) {
            Exception exc = e;
            String valueOf = String.valueOf(1);
            exc.printStackTrace();
            return valueOf;
        }
    }

    public static SignView SIDSign(String str, HttpHost httpHost) {
        SignView signView = new SignView();
        if (str == null) {
            signView.setResult(6);
        } else {
            int checkSecCertNativeForBilling = clientSDK.checkSecCertNativeForBilling();
            Log.v(MMCLIENT_SDK, "SIDSign() iCertState -> " + checkSecCertNativeForBilling);
            if (checkSecCertNativeForBilling == 0) {
                String genSID = genSID();
                if ("2".equals(genSID)) {
                    signView.setResult(2);
                } else {
                    String sIDSignValue = getSIDSignValue(genSID, str);
                    if (sIDSignValue == null) {
                        signView.setResult(7);
                    } else {
                        signView.setResult(0);
                        signView.setUserSignature(sIDSignValue);
                    }
                }
            } else if (1 == checkSecCertNativeForBilling) {
                int sendMessageAndApplyCert = sendMessageAndApplyCert(str, httpHost);
                if (sendMessageAndApplyCert != 0) {
                    signView.setResult(sendMessageAndApplyCert);
                } else {
                    String genSID2 = genSID();
                    if ("2".equals(genSID2)) {
                        signView.setResult(2);
                    } else {
                        String sIDSignValue2 = getSIDSignValue(genSID2, str);
                        if (sIDSignValue2 == null) {
                            signView.setResult(7);
                        } else {
                            signView.setResult(0);
                            signView.setUserSignature(sIDSignValue2);
                        }
                    }
                }
            } else if (2 == checkSecCertNativeForBilling) {
                String genSID3 = genSID();
                if ("2".equals(genSID3)) {
                    signView.setResult(2);
                } else {
                    try {
                        int applySecCertMethod = applySecCertMethod(genSID3, str, httpHost);
                        Log.v(MMCLIENT_SDK, "SIDSign() iResult -> " + applySecCertMethod);
                        if (applySecCertMethod == 0) {
                            String genSID4 = genSID();
                            if ("2".equals(genSID4)) {
                                signView.setResult(2);
                            } else {
                                String sIDSignValue3 = getSIDSignValue(genSID4, str);
                                if (sIDSignValue3 == null) {
                                    signView.setResult(7);
                                } else {
                                    signView.setResult(applySecCertMethod);
                                    signView.setUserSignature(sIDSignValue3);
                                }
                            }
                        } else {
                            signView.setResult(applySecCertMethod);
                        }
                    } catch (Exception e) {
                        signView.setResult(6);
                        e.printStackTrace();
                    }
                }
            } else {
                int sendMessageAndApplyCert2 = sendMessageAndApplyCert(str, httpHost);
                if (sendMessageAndApplyCert2 != 0) {
                    signView.setResult(sendMessageAndApplyCert2);
                } else {
                    String genSID5 = genSID();
                    if ("2".equals(genSID5)) {
                        signView.setResult(2);
                    } else {
                        String sIDSignValue4 = getSIDSignValue(genSID5, str);
                        if (sIDSignValue4 == null) {
                            signView.setResult(7);
                        } else {
                            signView.setResult(0);
                            signView.setUserSignature(sIDSignValue4);
                        }
                    }
                }
            }
        }
        return signView;
    }

    private static String applyCertPrivate(c cVar, d dVar, String str, String str2, HttpHost httpHost, String str3) {
        int updateRandNum;
        byte[] a2 = c.a(str, str2.getBytes("utf-8"), httpHost, str3);
        Log.v(MMCLIENT_SDK, "applyCertPrivate() -> " + new String(a2));
        ArrayList a3 = dVar.a(new String(a2));
        String a4 = ((b) a3.get(0)).a();
        if (!"0".equals(a4)) {
            return a4;
        }
        String b = ((b) a3.get(0)).b();
        String d = ((b) a3.get(0)).d();
        if (d != null && (updateRandNum = updateRandNum(d)) != 0) {
            return String.valueOf(updateRandNum);
        }
        int saveSecCertNativeForBilling = clientSDK.saveSecCertNativeForBilling(b, null);
        Log.v(MMCLIENT_SDK, "applyCertPrivate() iResult -> " + saveSecCertNativeForBilling);
        return String.valueOf(saveSecCertNativeForBilling);
    }

    public static int applySecCert(String str, HttpHost httpHost) {
        int i;
        if (str == null) {
            return 6;
        }
        String genSID = genSID();
        if ("2".equals(genSID)) {
            return 2;
        }
        try {
            i = applySecCertMethod(genSID, str, httpHost);
            Log.v(MMCLIENT_SDK, "applySecCert() iResult -> " + i);
        } catch (Exception e) {
            e.printStackTrace();
            i = 6;
        }
        return i;
    }

    private static int applySecCertMethod(String str, String str2, HttpHost httpHost) {
        String str3;
        int updateRandNum;
        String str4 = "http://" + strApplyCertForPhone + "/mmwlan/applySecCertForAPPThird";
        Log.v(MMCLIENT_SDK, "applySecCertMethod() strUrl -> " + str4);
        int checkSecCertNativeForBilling = clientSDK.checkSecCertNativeForBilling();
        Log.v(MMCLIENT_SDK, "applySecCertMethod() iCertState -> " + checkSecCertNativeForBilling);
        if (checkSecCertNativeForBilling == 2) {
            str3 = getGenPubkey();
            if ("4".equals(str3)) {
                return 4;
            }
        } else if (3 == genPKIKey()) {
            return 3;
        } else {
            str3 = getGenPubkey();
            if ("4".equals(str3)) {
                return 4;
            }
        }
        String str5 = str3;
        String imsiOfMD5Value = imsiOfMD5Value();
        if ("5".equals(imsiOfMD5Value)) {
            return 5;
        }
        c cVar = new c();
        String a2 = cVar.a(str, str5, imsiOfMD5Value, str2);
        Log.v(MMCLIENT_SDK, "applySecCertMethod() requestXML -> " + a2);
        d dVar = new d();
        byte[] a3 = c.a(str4, a2.getBytes("utf-8"), httpHost, "3");
        Log.v(MMCLIENT_SDK, "applySecCertMethod() byResponse -> " + new String(a3));
        ArrayList a4 = dVar.a(new String(a3));
        String a5 = ((b) a4.get(0)).a();
        Log.v(MMCLIENT_SDK, "applySecCertMethod() strApplyCertResult -> " + a5);
        if ("0".equals(a5)) {
            String b = ((b) a4.get(0)).b();
            Log.v(MMCLIENT_SDK, "applySecCertMethod() strDynPdworld -> " + ((b) a4.get(0)).c());
            String d = ((b) a4.get(0)).d();
            if (d != null && (updateRandNum = updateRandNum(d)) != 0) {
                return updateRandNum;
            }
            int saveSecCertNativeForBilling = clientSDK.saveSecCertNativeForBilling(b, null);
            Log.v(MMCLIENT_SDK, "applySecCertMethod() iResult -> " + saveSecCertNativeForBilling);
            return saveSecCertNativeForBilling;
        } else if (!"7".equals(a5)) {
            return Integer.parseInt(a5);
        } else {
            for (int i = 0; i < 10; i++) {
                String applyCertPrivate = applyCertPrivate(cVar, dVar, str4, a2, httpHost, "2");
                if (!"7".equals(applyCertPrivate)) {
                    return Integer.parseInt(applyCertPrivate);
                }
            }
            return 1;
        }
    }

    public static int checkSecCert() {
        try {
            int checkSecCertNativeForBilling = clientSDK.checkSecCertNativeForBilling();
            Log.v(MMCLIENT_SDK, "checkSecCert()  iResult -> " + checkSecCertNativeForBilling);
            return checkSecCertNativeForBilling;
        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        }
    }

    private static int genPKIKey() {
        try {
            int genPKIKeyNativeForBilling = clientSDK.genPKIKeyNativeForBilling();
            Log.v(MMCLIENT_SDK, "genPKIKey() iResult -> " + genPKIKeyNativeForBilling);
            return genPKIKeyNativeForBilling;
        } catch (Exception e) {
            e.printStackTrace();
            return 3;
        }
    }

    private static String genSID() {
        try {
            String genSIDNative = clientSDK.genSIDNative();
            if (genSIDNative == null) {
                genSIDNative = String.valueOf(2);
            }
            Log.v(MMCLIENT_SDK, "genSID() strResult -> " + genSIDNative);
            return genSIDNative;
        } catch (Exception e) {
            Exception exc = e;
            String valueOf = String.valueOf(2);
            exc.printStackTrace();
            return valueOf;
        }
    }

    public static String getDeviceID() {
        StringBuilder sb = new StringBuilder();
        sb.setLength(0);
        String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        if (deviceId == null) {
            sb.append(((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress().toString());
            Log.v(MMCLIENT_SDK, "getDeviceID_PAD() MacAddress -> " + sb.toString());
            return sb.toString();
        }
        sb.append(deviceId);
        Log.v(MMCLIENT_SDK, "getDeviceID_PAD() strIMEI -> " + sb.toString());
        return sb.toString();
    }

    private static String getGenPubkey() {
        try {
            String pubKeyForBilling = clientSDK.getPubKeyForBilling();
            if (pubKeyForBilling == null) {
                return String.valueOf(4);
            }
            Log.v(MMCLIENT_SDK, "getGenPubkey()  strResult -> " + pubKeyForBilling);
            return pubKeyForBilling;
        } catch (Exception e) {
            Exception exc = e;
            String valueOf = String.valueOf(4);
            exc.printStackTrace();
            return valueOf;
        }
    }

    public static String getIMSI() {
        StringBuilder sb = new StringBuilder();
        sb.setLength(0);
        String subscriberId = ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
        if (subscriberId == null) {
            sb.append(String.valueOf(1));
            Log.v(MMCLIENT_SDK, "getIMSI() errorValue -> " + sb.toString());
            return sb.toString();
        }
        sb.append(subscriberId);
        Log.v(MMCLIENT_SDK, "getIMSI() IMSI -> " + sb.toString());
        return sb.toString();
    }

    private static String getSIDSignValue(String str, String str2) {
        Exception e;
        String str3;
        try {
            str3 = clientSDK.SIDSignNativeForBilling(str, str2, null);
            try {
                Log.v(MMCLIENT_SDK, "getSIDSignValue() -> " + str3);
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                return str3;
            }
        } catch (Exception e3) {
            Exception exc = e3;
            str3 = null;
            e = exc;
            e.printStackTrace();
            return str3;
        }
        return str3;
    }

    public static String getVersion() {
        return "1.1.6";
    }

    private static String imsiOfMD5Value() {
        StringBuilder sb = new StringBuilder();
        String subscriberId = ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
        if (subscriberId == null) {
            sb.setLength(0);
            sb.append(String.valueOf(5).toString());
            return sb.toString().trim();
        }
        try {
            String digestNative = clientSDK.getDigestNative("md5", subscriberId);
            if (digestNative == null) {
                sb.setLength(0);
                sb.append(String.valueOf(5).toString());
                return sb.toString().trim();
            }
            Log.v(MMCLIENT_SDK, "imsiOfMD5Value() strMD5Result -> " + digestNative);
            sb.setLength(0);
            sb.append(digestNative.toString());
            return sb.toString().trim();
        } catch (Exception e) {
            sb.setLength(0);
            sb.append(String.valueOf(5).toString());
            e.printStackTrace();
        }
    }

    private static int initialImsiAndImeiValue() {
        DeviceInfo deviceInfo = new DeviceInfo();
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        String subscriberId = telephonyManager.getSubscriberId();
        if (subscriberId == null) {
            return 1;
        }
        deviceInfo.setStrImsi(subscriberId);
        Log.v(MMCLIENT_SDK, "strIMSI -> " + subscriberId);
        String deviceId = telephonyManager.getDeviceId();
        if (deviceId == null) {
            deviceId = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress().toString();
            Log.v(MMCLIENT_SDK, "initialImsiAndImeiValue() strIMEI MacAddress -> " + deviceId);
        }
        deviceInfo.setStrImei(deviceId);
        Log.v(MMCLIENT_SDK, "strIMEI -> " + deviceId);
        String str = context.getFilesDir().getPath().toString();
        deviceInfo.setFilePath(str);
        Log.v(MMCLIENT_SDK, "FilePath -> " + str);
        try {
            int transmitInfoNative = clientSDK.transmitInfoNative(deviceInfo);
            Log.v(MMCLIENT_SDK, "initialImsiAndImeiValue() iResult -> " + transmitInfoNative);
            return transmitInfoNative != 0 ? 4 : 0;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int initialMMSDK(Context context2, IPDress_ForPhone iPDress_ForPhone) {
        context = context2;
        if (iPDress_ForPhone == null || iPDress_ForPhone.getStrApplySecCertIP() == null || iPDress_ForPhone.getStrSMSNumber() == null) {
            return 5;
        }
        strApplyCertForPhone = iPDress_ForPhone.getStrApplySecCertIP();
        SMSNumber = iPDress_ForPhone.getStrSMSNumber();
        Log.v(MMCLIENT_SDK, "initialMMSDK() strIPDress -> " + strApplyCertForPhone + "  :  " + SMSNumber);
        int initialImsiAndImeiValue = initialImsiAndImeiValue();
        Log.v(MMCLIENT_SDK, "initialMMSDK() iResult -> " + initialImsiAndImeiValue);
        return initialImsiAndImeiValue;
    }

    public static String md5Algorithm(String str) {
        if (str == null) {
            return String.valueOf(1);
        }
        try {
            String digestNative = clientSDK.getDigestNative("md5", str);
            return digestNative == null ? String.valueOf(2) : digestNative;
        } catch (Exception e) {
            Exception exc = e;
            String valueOf = String.valueOf(2);
            exc.printStackTrace();
            return valueOf;
        }
    }

    private static int sendMessageAndApplyCert(String str, HttpHost httpHost) {
        String genSID = genSID();
        "2".equals(genSID);
        String imsiOfMD5Value = imsiOfMD5Value();
        "5".equals(imsiOfMD5Value);
        StringBuilder sb = new StringBuilder();
        sb.setLength(0);
        sb.append("MM#WLAN#").append(imsiOfMD5Value.toString().trim()).append("#").append(genSID.toString().trim()).append("#").append(str.toString());
        Log.v(MMCLIENT_SDK, "sendMessageAndApplyCert() sendMessage -> " + sb.toString());
        SmsManager smsManager = SmsManager.getDefault();
        if (sb.toString().trim().length() > 70) {
            Iterator<String> it = smsManager.divideMessage(sb.toString().trim()).iterator();
            while (it.hasNext()) {
                smsManager.sendTextMessage(SMSNumber, null, it.next(), null, null);
            }
        } else {
            smsManager.sendTextMessage(SMSNumber, null, sb.toString().trim(), null, null);
        }
        try {
            int applySecCertMethod = applySecCertMethod(genSID, str, httpHost);
            Log.v(MMCLIENT_SDK, "sendMessageAndApplyCert() -> " + applySecCertMethod);
            return applySecCertMethod;
        } catch (Exception e) {
            e.printStackTrace();
            return 6;
        }
    }

    public static int updateRandNum(String str) {
        int i;
        if (str == null) {
            return 1;
        }
        try {
            i = clientSDK.UpdateRandNumForBilling(str);
            if (i != 0) {
                return 1;
            }
            Log.v(MMCLIENT_SDK, "updateRandNum()  iResult -> " + i);
            return i;
        } catch (Exception e) {
            e.printStackTrace();
            i = 1;
        }
    }
}
