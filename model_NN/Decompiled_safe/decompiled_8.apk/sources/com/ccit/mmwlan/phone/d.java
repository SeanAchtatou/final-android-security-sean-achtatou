package com.ccit.mmwlan.phone;

import java.io.StringReader;
import java.util.ArrayList;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private a f606a = null;
    private ArrayList b = null;

    public final ArrayList a(String str) {
        try {
            XMLReader xMLReader = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
            this.f606a = new a();
            xMLReader.setContentHandler(this.f606a);
            xMLReader.parse(new InputSource(new StringReader(str)));
            this.b = this.f606a.a();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.b;
    }
}
