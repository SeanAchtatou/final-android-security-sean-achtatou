package com.ccit.mmwlan;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.ccit.mmwlan.a.d;
import com.ccit.mmwlan.a.f;
import com.ccit.mmwlan.exception.ClientSDKException;
import com.ccit.mmwlan.vo.DeviceInfo;
import com.ccit.mmwlan.vo.DeviceName;
import com.ccit.mmwlan.vo.IPDress_ForPad;
import com.ccit.mmwlan.vo.SignView;
import com.ccit.mmwlan.vo.a;
import com.ccit.mmwlan.vo.b;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public final class MMClientSDK_ForPad {
    private static final int INT_RESULT_0 = 0;
    private static final int INT_RESULT_1 = 1;
    private static final int INT_RESULT_2 = 2;
    private static final int INT_RESULT_3 = 3;
    private static final int INT_RESULT_4 = 4;
    private static final int INT_RESULT_5 = 5;
    private static final int INT_RESULT_6 = 6;
    private static final int INT_RESULT_7 = 7;
    private static final String MMCLIENT_SDK = "MMClientSDK_ForPad";
    private static ClientSDK clientSDK;
    private static Context context = null;
    private static String strApplyCert = null;
    private static String strDeviceOuth = null;
    private static String strGetDeviceName = null;

    static {
        clientSDK = null;
        clientSDK = new ClientSDK();
    }

    public static int DestorySecCert(String str) {
        int i = 0;
        try {
            int DestorySecCertForBilling = clientSDK.DestorySecCertForBilling(str);
            try {
                Log.v(MMCLIENT_SDK, "DestroySecCert() iRet -> " + DestorySecCertForBilling);
            } catch (Exception e) {
                Exception exc = e;
                i = DestorySecCertForBilling;
                e = exc;
                e.printStackTrace();
                return i;
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            return i;
        }
        return i;
    }

    public static String RSAEncryptWithPubKey(String str) {
        try {
            String AsymmetricEncryptionForBilling = clientSDK.AsymmetricEncryptionForBilling(str);
            if (AsymmetricEncryptionForBilling == null) {
                AsymmetricEncryptionForBilling = String.valueOf(1);
            }
            Log.v(MMCLIENT_SDK, "RSAEncryptWithPubKey() strRet -> " + AsymmetricEncryptionForBilling);
            return AsymmetricEncryptionForBilling;
        } catch (Exception e) {
            Exception exc = e;
            String valueOf = String.valueOf(1);
            exc.printStackTrace();
            return valueOf;
        }
    }

    private static SignView applyCert(String str, String str2) {
        SignView signView = new SignView();
        String str3 = "http://" + strApplyCert + "/mmwlan/applyCert_PAD";
        String imeiOfMD5Value = imeiOfMD5Value();
        if ("5".equals(imeiOfMD5Value)) {
            signView.setResult(5);
            signView.setErrMsg("获取IMEIMD5值失败");
            return signView;
        }
        String genSid_PAD = genSid_PAD();
        if ("2".equals(genSid_PAD)) {
            signView.setResult(2);
            signView.setErrMsg("准备PKI密钥对失败");
            return signView;
        } else if (3 == genPKIKey_PAD()) {
            signView.setResult(3);
            signView.setErrMsg("申请安全凭证失败");
            return signView;
        } else {
            String genPubkey = getGenPubkey();
            if ("4".equals(genPubkey)) {
                signView.setResult(4);
                signView.setErrMsg("保存安全凭证失败");
                return signView;
            }
            d dVar = new d();
            String a2 = str2 == null ? dVar.a(str, imeiOfMD5Value, genSid_PAD, genPubkey, null) : dVar.a(str, imeiOfMD5Value, genSid_PAD, genPubkey, str2);
            Log.v(MMCLIENT_SDK, "applyCert() strRequestXML -> " + a2);
            f fVar = new f();
            try {
                byte[] a3 = d.a(str3, a2.getBytes("utf-8"));
                Log.v(MMCLIENT_SDK, "applyCert() byResponse -> " + new String(a3));
                ArrayList a4 = fVar.a(new String(a3));
                String a5 = ((a) a4.get(0)).a();
                signView.setErrMsg(((a) a4.get(0)).b());
                Log.v(MMCLIENT_SDK, "applyCert() strCertResult -> " + a5);
                if ("0".equals(a5)) {
                    String c = ((a) a4.get(0)).c();
                    String d = ((a) a4.get(0)).d();
                    if (d != null) {
                        int updateRandNum_PAD = updateRandNum_PAD(d);
                        signView.setResult(updateRandNum_PAD);
                        if (updateRandNum_PAD != 0) {
                            return signView;
                        }
                    }
                    try {
                        int saveSecCertNativeForBilling = clientSDK.saveSecCertNativeForBilling(c, null);
                        signView.setResult(saveSecCertNativeForBilling);
                        Log.v(MMCLIENT_SDK, "applyCert() saveSecCertNative -> " + saveSecCertNativeForBilling);
                    } catch (ClientSDKException e) {
                        signView.setResult(4);
                        signView.setErrMsg("保存安全凭证失败");
                        e.printStackTrace();
                    }
                    signView.setResult(0);
                    signView.setErrMsg("成功");
                    return signView;
                }
                signView.setResult(Integer.parseInt(a5));
                return signView;
            } catch (UnsupportedEncodingException e2) {
                signView.setResult(7);
                signView.setErrMsg("签名失败");
                e2.printStackTrace();
            } catch (Exception e3) {
                signView.setResult(7);
                signView.setErrMsg("签名失败");
                e3.printStackTrace();
            }
        }
    }

    public static SignView applyCert_PAD(String str) {
        SignView signView = new SignView();
        if (str != null) {
            return applyCert(str, null);
        }
        signView.setResult(6);
        signView.setErrMsg("获取应用appid错误");
        return signView;
    }

    private static SignView authProcess(SignView signView, String str, String str2, String str3, String str4) {
        String str5 = "http://" + strDeviceOuth + "/mmwlan/ApplyDeviceAuth_PAD";
        Log.v(MMCLIENT_SDK, "authProcess() strUrl -> " + str5);
        String a2 = new d().a(str, str4, str2, null, str3, null, null);
        Log.v(MMCLIENT_SDK, "authProcess() strRequestXML -> " + a2);
        f fVar = new f();
        try {
            byte[] a3 = d.a(str5, a2.getBytes("utf-8"));
            Log.v(MMCLIENT_SDK, "authProcess() byResponse -> " + new String(a3));
            ArrayList c = fVar.c(new String(a3));
            String a4 = ((b) c.get(0)).a();
            signView.setErrMsg(((b) c.get(0)).b());
            Log.v(MMCLIENT_SDK, "authProcess() strCertResult -> " + a4);
            signView.setResult(Integer.parseInt(a4.toString()));
        } catch (UnsupportedEncodingException e) {
            signView.setResult(7);
            e.printStackTrace();
        } catch (Exception e2) {
            signView.setResult(7);
            e2.printStackTrace();
        }
        return signView;
    }

    public static int checkSecCert_PAD() {
        try {
            int checkSecCertNativeForBilling = clientSDK.checkSecCertNativeForBilling();
            Log.v(MMCLIENT_SDK, "checkSecCert_PAD() -> " + checkSecCertNativeForBilling);
            return checkSecCertNativeForBilling;
        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        }
    }

    private static int genPKIKey_PAD() {
        try {
            int genPKIKeyNativeForBilling = clientSDK.genPKIKeyNativeForBilling();
            Log.v(MMCLIENT_SDK, "genPKIKey_PAD() -> " + genPKIKeyNativeForBilling);
            return genPKIKeyNativeForBilling;
        } catch (Exception e) {
            e.printStackTrace();
            return 3;
        }
    }

    private static String genSid_PAD() {
        try {
            String genSIDNative = clientSDK.genSIDNative();
            if (genSIDNative == null) {
                genSIDNative = String.valueOf(2);
            }
            Log.v(MMCLIENT_SDK, "genSid_PAD() -> " + genSIDNative);
            return genSIDNative;
        } catch (Exception e) {
            Exception exc = e;
            String valueOf = String.valueOf(2);
            exc.printStackTrace();
            return valueOf;
        }
    }

    public static String getDeviceID_PAD() {
        StringBuilder sb = new StringBuilder();
        sb.setLength(0);
        String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        if (deviceId == null) {
            sb.append(((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress().toString());
            Log.v(MMCLIENT_SDK, "getDeviceID_PAD() MacAddress -> " + sb.toString());
            return sb.toString();
        }
        sb.append(deviceId);
        Log.v(MMCLIENT_SDK, "getDeviceID_PAD() strIMEI -> " + sb.toString());
        return sb.toString();
    }

    public static DeviceName getDeviceNameNew_PAD() {
        DeviceName deviceName;
        String str = "http://" + strGetDeviceName + "/mmwlan/getDeviceName_PAD";
        DeviceName deviceName2 = new DeviceName();
        String imeiOfMD5Value = imeiOfMD5Value();
        if ("5".equals(imeiOfMD5Value)) {
            deviceName2.setResult("5");
            deviceName2.setErrormsg("imei/imsi/mac的MD5失败");
            deviceName2.setDeviceName(PoiTypeDef.All);
            return deviceName2;
        }
        String a2 = new d().a(imeiOfMD5Value);
        Log.v(MMCLIENT_SDK, "getDeviceName_PAD() strRequestXML -> " + a2);
        f fVar = new f();
        try {
            byte[] a3 = d.a(str, a2.getBytes("utf-8"));
            Log.v(MMCLIENT_SDK, "getDeviceName_PAD() byResponse -> " + new String(a3));
            deviceName = (DeviceName) fVar.b(new String(a3)).get(0);
        } catch (UnsupportedEncodingException e) {
            deviceName2.setResult("7");
            deviceName2.setErrormsg("请求设备名失败");
            deviceName2.setDeviceName(PoiTypeDef.All);
            e.printStackTrace();
            deviceName = deviceName2;
        } catch (Exception e2) {
            deviceName2.setResult("7");
            deviceName2.setErrormsg("请求设备名失败");
            deviceName2.setDeviceName(PoiTypeDef.All);
            e2.printStackTrace();
            deviceName = deviceName2;
        }
        return deviceName;
    }

    public static String getDeviceName_PAD() {
        String str = "http://" + strGetDeviceName + "/mmwlan/getDeviceName_PAD";
        StringBuilder sb = new StringBuilder();
        String imeiOfMD5Value = imeiOfMD5Value();
        if ("5".equals(imeiOfMD5Value)) {
            sb.setLength(0);
            sb.append(imeiOfMD5Value.toString());
            return sb.toString().trim();
        }
        String a2 = new d().a(imeiOfMD5Value);
        Log.v(MMCLIENT_SDK, "getDeviceName_PAD() strRequestXML -> " + a2);
        f fVar = new f();
        try {
            byte[] a3 = d.a(str, a2.getBytes("utf-8"));
            Log.v(MMCLIENT_SDK, "getDeviceName_PAD() byResponse -> " + new String(a3));
            ArrayList b = fVar.b(new String(a3));
            String deviceName = ((DeviceName) b.get(0)).getDeviceName();
            ((DeviceName) b.get(0)).getResult();
            ((DeviceName) b.get(0)).getErrormsg();
            Log.v(MMCLIENT_SDK, "getDeviceName_PAD() strXMLResult -> " + deviceName);
            if ("0".equals(deviceName)) {
                String deviceName2 = ((DeviceName) b.get(0)).getDeviceName();
                String result = ((DeviceName) b.get(0)).getResult();
                String errormsg = ((DeviceName) b.get(0)).getErrormsg();
                Log.d("strDeviceName --- >", "strDeviceName" + deviceName2);
                Log.v(MMCLIENT_SDK, "getDeviceName_PAD() strDeviceName -> " + deviceName2);
                sb.setLength(0);
                sb.append(deviceName2.toString());
                sb.append(result.toString());
                sb.append(errormsg.toString());
                return sb.toString().trim();
            }
            sb.setLength(0);
            sb.append(deviceName.toString());
            return sb.toString().trim();
        } catch (UnsupportedEncodingException e) {
            sb.setLength(0);
            sb.append(String.valueOf(7).toString());
            e.printStackTrace();
        } catch (Exception e2) {
            sb.setLength(0);
            sb.append(String.valueOf(7).toString());
            e2.printStackTrace();
        }
    }

    private static String getGenPubkey() {
        try {
            String pubKeyForBilling = clientSDK.getPubKeyForBilling();
            if (pubKeyForBilling == null) {
                return String.valueOf(4);
            }
            Log.v(MMCLIENT_SDK, "getGenPubkey() -> " + pubKeyForBilling);
            return pubKeyForBilling;
        } catch (Exception e) {
            Exception exc = e;
            String valueOf = String.valueOf(4);
            exc.printStackTrace();
            return valueOf;
        }
    }

    public static String getIMSI_PAD() {
        StringBuilder sb = new StringBuilder();
        sb.setLength(0);
        sb.append("10086");
        Log.v(MMCLIENT_SDK, "getIMSI_PAD() strIMEI -> " + sb.toString());
        return sb.toString();
    }

    public static String getVersion() {
        Log.v(MMCLIENT_SDK, "getVersion() -> " + "1.1.6");
        return "1.1.6";
    }

    private static String imeiOfMD5Value() {
        StringBuilder sb = new StringBuilder();
        try {
            String digestNative = clientSDK.getDigestNative("md5", getDeviceID_PAD());
            if (digestNative == null) {
                sb.setLength(0);
                sb.append(String.valueOf(5).toString());
                return sb.toString().trim();
            }
            Log.v(MMCLIENT_SDK, "imeiOfMD5Value() strMD5Result -> " + digestNative);
            sb.setLength(0);
            sb.append(digestNative.toString());
            return sb.toString().trim();
        } catch (Exception e) {
            sb.setLength(0);
            sb.append(String.valueOf(5).toString());
            e.printStackTrace();
        }
    }

    private static int initialImsiAndImeiValue() {
        DeviceInfo deviceInfo = new DeviceInfo();
        deviceInfo.setStrImei("10086");
        Log.v(MMCLIENT_SDK, "initialImsiAndImeiValue() strIMSI -> " + "10086");
        String deviceID_PAD = getDeviceID_PAD();
        Log.d(MMCLIENT_SDK, "initialImsiAndImeiValue() IMEI ->" + deviceID_PAD);
        if (deviceID_PAD == null) {
            return 1;
        }
        deviceInfo.setStrImsi(deviceID_PAD);
        Log.v(MMCLIENT_SDK, "initialImsiAndImeiValue() strIMEI -> " + deviceID_PAD);
        String str = context.getFilesDir().getPath().toString();
        deviceInfo.setFilePath(str);
        Log.v(MMCLIENT_SDK, "initialImsiAndImeiValue() FilePath -> " + str);
        try {
            int transmitInfoNative = clientSDK.transmitInfoNative(deviceInfo);
            Log.v(MMCLIENT_SDK, "initialImsiAndImeiValue() iResult -> " + transmitInfoNative);
            if (transmitInfoNative != 0) {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static int initialMMSDK(Context context2, IPDress_ForPad iPDress_ForPad) {
        context = context2;
        if (iPDress_ForPad == null || iPDress_ForPad.getStrDeviceOuthIp() == null || iPDress_ForPad.getStrApplySecCertIP() == null || iPDress_ForPad.getStrGetDeviceName() == null) {
            return 5;
        }
        strDeviceOuth = iPDress_ForPad.getStrDeviceOuthIp();
        strApplyCert = iPDress_ForPad.getStrApplySecCertIP();
        strGetDeviceName = iPDress_ForPad.getStrGetDeviceName();
        Log.v(MMCLIENT_SDK, "initialMMSDK() strIPDress -> " + strApplyCert + "  :  " + strApplyCert);
        int initialImsiAndImeiValue = initialImsiAndImeiValue();
        Log.v(MMCLIENT_SDK, "initialMMSDK() -> " + initialImsiAndImeiValue);
        return initialImsiAndImeiValue;
    }

    public static String md5Algorithm(String str) {
        if (str == null) {
            return String.valueOf(1);
        }
        try {
            String digestNative = clientSDK.getDigestNative("md5", str);
            return digestNative == null ? String.valueOf(2) : digestNative;
        } catch (Exception e) {
            Exception exc = e;
            String valueOf = String.valueOf(2);
            exc.printStackTrace();
            return valueOf;
        }
    }

    public static SignView sidSign_PAD(String str, String str2, String str3, String str4) {
        SignView signView = new SignView();
        if (str == null) {
            signView.setResult(6);
            signView.setErrMsg("应用appid为空");
            return signView;
        }
        String imeiOfMD5Value = imeiOfMD5Value();
        if ("5".equals(imeiOfMD5Value)) {
            signView.setResult(5);
            signView.setErrMsg("获取IMEI的MD5值错误");
            return signView;
        }
        int i = -1;
        try {
            i = clientSDK.checkSecCertNativeForBilling();
            Log.v(MMCLIENT_SDK, "sidSign_PAD() iCertState -> " + i);
        } catch (ClientSDKException e) {
            signView.setResult(7);
            signView.setErrMsg("检查凭证异常");
            e.printStackTrace();
        }
        if (i == 0) {
            return signProcess(signView, str);
        }
        if (1 == i) {
            if (str2 != null && str3 != null) {
                return authProcess(signView, str, imeiOfMD5Value, str2, str3);
            }
            if (str3 == null || str4 == null) {
                signView.setResult(6);
                signView.setErrMsg("应用appid为空");
                return signView;
            }
            SignView applyCert = applyCert(str, str4);
            return applyCert.getResult() == 0 ? signProcess(signView, str) : applyCert;
        } else if (2 == i) {
            SignView applyCert2 = applyCert(str, null);
            Log.v(MMCLIENT_SDK, "sidSign_PAD() iUpdateCert -> " + applyCert2);
            return applyCert2.getResult() == 0 ? signProcess(signView, str) : applyCert2;
        } else {
            signView.setResult(i);
            return signView;
        }
    }

    private static SignView signProcess(SignView signView, String str) {
        String genSid_PAD = genSid_PAD();
        if ("2".equals(genSid_PAD)) {
            signView.setResult(2);
            signView.setErrMsg("获取sid失败");
        } else {
            try {
                String SIDSignNativeForBilling = clientSDK.SIDSignNativeForBilling(genSid_PAD, str, null);
                if (SIDSignNativeForBilling == null) {
                    signView.setResult(7);
                    signView.setErrMsg("签名失败");
                } else {
                    Log.v(MMCLIENT_SDK, "signProcess() strSignResult -> " + SIDSignNativeForBilling);
                    signView.setResult(0);
                    signView.setErrMsg("签名成功");
                    signView.setUserSignature(SIDSignNativeForBilling);
                }
            } catch (Exception e) {
                signView.setResult(7);
                signView.setErrMsg("签名失败");
                e.printStackTrace();
            }
        }
        return signView;
    }

    public static int updateRandNum_PAD(String str) {
        int i;
        if (str == null) {
            return 1;
        }
        try {
            i = clientSDK.UpdateRandNumForBilling(str);
            if (i != 0) {
                return 1;
            }
            Log.v(MMCLIENT_SDK, "updateRandNum()  iResult -> " + i);
            return i;
        } catch (Exception e) {
            e.printStackTrace();
            i = 1;
        }
    }
}
