package com.ccit.mmwlan;

import android.content.Context;
import android.telephony.SmsManager;
import android.util.Log;
import com.ccit.mmwlan.a.e;
import com.ccit.mmwlan.b.a;
import com.ccit.mmwlan.b.b;
import com.ccit.mmwlan.exception.ClientSDKException;
import com.ccit.mmwlan.vo.DeviceInfo;

public final class MMClientSDK_ForIdentity {
    private String TAG = "IdentitySDK";
    private MMClientSDK_ForLogin clientsdk = null;
    private Context context = null;
    private DeviceInfo deviceInfo = null;
    private UserProfile userProfile = null;

    public final int applyCert() {
        if (MMClientSDK_ForLogin.checkSecCert(this.userProfile.cardSlot, this.userProfile.loginType, this.userProfile.userName) == 0) {
            return 0;
        }
        String sessionID = getSessionID();
        return MMClientSDK_ForLogin.applySecCert(this.userProfile.cardSlot, this.userProfile.loginType, this.userProfile.userName, this.userProfile.password, null, this.userProfile.appID, sessionID, this.userProfile.moServerHost, null, 0);
    }

    public final String getDeviceID() {
        if (this.deviceInfo == null) {
            new a();
            try {
                this.deviceInfo = a.a(this.context, this.userProfile.cardSlot);
            } catch (ClientSDKException e) {
                e.printStackTrace();
            }
        }
        if (this.deviceInfo != null) {
            return this.deviceInfo.getStrImei();
        }
        return null;
    }

    public final String getIdentity() {
        byte[] bArr;
        String format = String.format("<req><sidSignature>%s</sidSignature></req>", MMClientSDK_ForLogin.SIDSign(this.userProfile.cardSlot, this.userProfile.loginType, this.userProfile.userName, getSessionID(), this.userProfile.appID));
        new e();
        try {
            bArr = e.a("http://" + this.userProfile.moServerHost.getHostName() + ":" + this.userProfile.moServerHost.getPort() + "/" + new b().a("applyIdentUrl"), format.getBytes(), null);
        } catch (Exception e) {
            e.printStackTrace();
            bArr = null;
        }
        String str = new String(bArr);
        int indexOf = str.indexOf("result");
        int lastIndexOf = str.lastIndexOf("result");
        if (indexOf == -1 || lastIndexOf == -1 || indexOf + 7 > lastIndexOf - 3) {
            return null;
        }
        char[] cArr = new char[(((lastIndexOf - 3) - (indexOf + 7)) + 1)];
        str.getChars(indexOf + 7, (lastIndexOf - 3) + 1, cArr, 0);
        if (Integer.parseInt(String.valueOf(cArr)) != 0) {
            return null;
        }
        int indexOf2 = str.indexOf("mobilephone");
        int lastIndexOf2 = str.lastIndexOf("mobilephone");
        if (indexOf2 == -1 || lastIndexOf2 == -1 || indexOf2 + 12 > lastIndexOf2 - 3) {
            return null;
        }
        char[] cArr2 = new char[(((lastIndexOf2 - 3) - (indexOf2 + 12)) + 1)];
        str.getChars(indexOf2 + 12, (lastIndexOf2 - 3) + 1, cArr2, 0);
        if (!String.valueOf(cArr2).equals("13900000000")) {
            return new String(cArr2);
        }
        return null;
    }

    public final String getSessionID() {
        return MMClientSDK_ForLogin.genSID();
    }

    public final int initContext(Context context2, UserProfile userProfile2) {
        if (this.clientsdk == null) {
            this.clientsdk = new MMClientSDK_ForLogin();
        }
        int initMmClientSdk = MMClientSDK_ForLogin.initMmClientSdk(context2, userProfile2.cardSlot);
        this.context = context2;
        this.userProfile = userProfile2;
        return initMmClientSdk;
    }

    public final String registerIdentity() {
        String format = String.format("GDWLANIDP#%s#%s:%s", MMClientSDK_ForLogin.getDigest("md5", this.deviceInfo.getStrImsi()), getSessionID(), this.userProfile.appID);
        SmsManager.getDefault().sendTextMessage("10658682803", null, format, null, null);
        Log.v(this.TAG, "send sms:" + format + " to:" + "10658682803");
        return format;
    }
}
