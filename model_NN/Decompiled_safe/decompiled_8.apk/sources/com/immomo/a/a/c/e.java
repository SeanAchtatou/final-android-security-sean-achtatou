package com.immomo.a.a.c;

import com.baidu.location.LocationClientOption;
import com.immomo.a.a.d.b;
import com.immomo.a.a.d.d;
import com.immomo.a.a.f;
import com.immomo.momo.util.ar;
import java.util.concurrent.TimeoutException;

public final class e implements f, Runnable {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public boolean f657a = true;
    private int b = 0;
    private d c = new com.immomo.a.a.d.e();
    private d d = new com.immomo.a.a.d.f();
    private int e;
    private long f = 0;
    private long g = 0;
    private /* synthetic */ d h;

    public e(d dVar, int i, int i2) {
        this.h = dVar;
        this.b = i;
        this.e = i2;
        this.g = (long) (this.d.c().getBytes().length + 4);
        dVar.f656a.a(System.currentTimeMillis());
    }

    public final void a(Object obj, f fVar) {
    }

    public final boolean a(b bVar) {
        String a2 = bVar.a();
        if ("pi".equals(a2)) {
            this.h.a(this.d);
        } else {
            "po".equals(a2);
        }
        this.f += this.g;
        return true;
    }

    public final void run() {
        while (this.f657a && this.h.b()) {
            try {
                Thread.sleep((long) (this.b * LocationClientOption.MIN_SCAN_SPAN));
            } catch (InterruptedException e2) {
            }
            if (!this.f657a) {
                break;
            } else if (this.h.f656a.j() != 0 && System.currentTimeMillis() - this.h.f656a.j() >= ((long) (this.e * LocationClientOption.MIN_SCAN_SPAN))) {
                this.h.f656a.a("pi po timeout", new TimeoutException("pi po timeout"));
            } else if (this.h.f656a.j() == 0 || System.currentTimeMillis() - this.h.f656a.j() >= ((long) (this.b * LocationClientOption.MIN_SCAN_SPAN))) {
                this.h.a(this.c);
                this.f += this.g;
            }
        }
        if (this.f > 0) {
            ar.i(this.f);
        }
    }
}
