package com.immomo.a.a.c;

import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.a.a.a.a;
import com.immomo.momo.android.c.u;
import com.immomo.momo.d;
import com.immomo.momo.protocol.imjson.k;
import com.immomo.momo.util.ar;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import org.json.JSONException;

final class b extends Thread {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public boolean f654a = true;
    private ByteArrayOutputStream b = new ByteArrayOutputStream();
    private boolean c;
    private boolean d;
    private boolean e;
    private byte f;
    /* access modifiers changed from: private */
    public a g = com.immomo.a.a.a.a().a("PacketReader-" + getId());
    private byte[] h = new byte[2048];
    private InputStream i = null;
    private /* synthetic */ a j;

    public b(a aVar, InputStream inputStream) {
        this.j = aVar;
        this.i = inputStream;
    }

    public final void run() {
        while (this.f654a && this.j.f) {
            try {
                int read = this.i.read(this.h);
                ar.d((long) read);
                int i2 = 0;
                while (true) {
                    if (i2 >= read) {
                        continue;
                        break;
                    }
                    byte b2 = this.h[i2];
                    synchronized (this.j) {
                        if (this.f654a && this.j.f) {
                            if (b2 == 1) {
                                if (!this.c) {
                                    this.c = true;
                                }
                                if ((!this.c || this.d) && this.c && this.d && !this.e) {
                                    this.b.write(b2);
                                }
                            } else if (b2 == 2) {
                                if (!this.d) {
                                    this.d = true;
                                }
                                this.b.write(b2);
                            } else if (b2 != 3) {
                                if (b2 == 4 && this.c && this.d && this.f == 3) {
                                    this.e = true;
                                    byte[] byteArray = this.b.toByteArray();
                                    try {
                                        String str = byteArray.length > 0 ? new String((this.j.b.c() == null || byteArray.length <= 0) ? byteArray : this.j.b.c().b(byteArray), "UTF-8") : PoiTypeDef.All;
                                        this.j.b.a(System.currentTimeMillis());
                                        this.g.a("<--: " + str);
                                        this.j.c.a(this.j.b, str);
                                        a.f653a = String.valueOf(str) + "[" + getId() + "]";
                                    } catch (JSONException e2) {
                                        this.g.a("parse failed. ", e2);
                                        StringBuilder sb = new StringBuilder();
                                        sb.append("(" + e2.getClass().getName() + ": " + e2.getMessage() + ")");
                                        sb.append("(" + this.j.b.c().a() + ")");
                                        sb.append("(" + this.j.b.c().d() + ")");
                                        sb.append("(" + this.j.b.b().c() + ")");
                                        sb.append("(" + android.support.v4.b.a.b(byteArray) + ")");
                                        sb.append("(" + android.support.v4.b.a.b((byte[]) null) + ")");
                                        sb.append("(" + getId() + ")");
                                        if (a.f653a != null) {
                                            sb.append("(" + a.f653a + ")");
                                        }
                                        if (k.f2925a != null) {
                                            sb.append("(" + k.f2925a + ")");
                                        }
                                        u.d().execute(new c(this, sb));
                                        this.g.c("丢消息了:" + sb.toString());
                                    } catch (Exception e3) {
                                        d.a(new Exception("(IMJ Warning)" + e3.getMessage(), e3));
                                        this.g.a((Throwable) e3);
                                    }
                                    this.c = false;
                                    this.d = false;
                                    this.e = false;
                                    this.b.reset();
                                }
                                this.b.write(b2);
                            }
                            this.f = b2;
                        }
                    }
                    i2++;
                }
            } catch (Exception e4) {
                this.f654a = false;
                this.j.b.a("packetreader stoped. threadid=" + getId() + ". ", e4);
            }
        }
    }
}
