package com.immomo.a.a;

import com.immomo.a.a.a.c;
import com.immomo.a.a.e.d;
import com.immomo.a.a.e.e;
import com.tencent.mm.sdk.a.b;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class a {
    private static c b = new b();
    private static Map e = new ConcurrentHashMap();

    /* renamed from: a  reason: collision with root package name */
    protected final b f648a;
    private final Map c = new ConcurrentHashMap();
    private final Map d = new ConcurrentHashMap();
    private i f = null;
    private Set g = new HashSet();
    private com.tencent.mm.sdk.a.a h = new com.tencent.mm.sdk.a.a();
    private ExecutorService i = null;
    private d j = new d();
    private e k = null;
    private com.immomo.a.a.a.a l = b.a(getClass().getSimpleName());
    private long m = 0;

    public a(b bVar) {
        this.f648a = bVar;
        this.i = Executors.newSingleThreadExecutor();
    }

    public static c a() {
        return b;
    }

    public static h a(String str) {
        return (h) e.remove(str);
    }

    public static void a(c cVar) {
        b = cVar;
    }

    public static void a(String str, h hVar) {
        e.put(str, hVar);
    }

    public final void a(long j2) {
        this.m = j2;
    }

    public final void a(c cVar) {
        this.g.add(cVar);
    }

    public abstract void a(com.immomo.a.a.d.d dVar);

    public final void a(d dVar) {
        this.j = dVar;
    }

    public final void a(i iVar) {
        this.f = iVar;
    }

    public final void a(String str, f fVar) {
        this.c.put(str, fVar);
    }

    public abstract void a(String str, String str2, String str3);

    public abstract void a(String str, Throwable th);

    public final b b() {
        return this.f648a;
    }

    public final void b(c cVar) {
        this.g.remove(cVar);
    }

    public final void b(String str) {
        this.c.remove(str);
    }

    public final void b(String str, f fVar) {
        this.d.put(str, fVar);
    }

    public final f c(String str) {
        return (f) this.c.get(str);
    }

    public final i c() {
        return this.f;
    }

    public final com.tencent.mm.sdk.a.a d() {
        return this.h;
    }

    public final void d(String str) {
        this.d.remove(str);
    }

    public final f e(String str) {
        return (f) this.d.get(str);
    }

    public final Collection e() {
        return this.g;
    }

    public abstract void f();

    public final synchronized void g() {
        f();
        a(this.f648a.c(), this.f648a.d(), this.f648a.h());
    }

    public final void h() {
        String c2 = this.f648a.c();
        if (!l()) {
            throw new com.immomo.a.a.b.d("Server not connected");
        }
        com.immomo.a.a.e.b bVar = null;
        if (this.k != null) {
            bVar = this.k.b();
            if (this.k.c()) {
                this.l.c("~~~~~~~~~~~~~~~~~synchronizer is runing....");
                this.k.a();
            }
        }
        this.k = new e(this, this.j.a(c2));
        if (bVar != null) {
            this.k.a(bVar);
        } else {
            this.k.a(this.j.a());
        }
        this.i.execute(this.k);
    }

    public final e i() {
        return this.k;
    }

    public final long j() {
        return this.m;
    }

    public void k() {
        if (this.k != null) {
            this.k.a();
        }
        if (this.f != null) {
            this.f.c();
        }
        for (c a2 : this.g) {
            a2.a();
        }
        for (h hVar : e.values()) {
            if (hVar != null) {
                try {
                    hVar.b();
                } catch (Exception e2) {
                }
            }
        }
        this.m = 0;
    }

    public abstract boolean l();

    public abstract boolean m();
}
