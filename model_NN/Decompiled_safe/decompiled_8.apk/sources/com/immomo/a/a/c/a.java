package com.immomo.a.a.c;

import com.immomo.a.a.g;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    static String f653a = null;
    /* access modifiers changed from: private */
    public com.immomo.a.a.a b = null;
    /* access modifiers changed from: private */
    public g c = null;
    private b d = null;
    private InputStream e = null;
    /* access modifiers changed from: private */
    public boolean f;

    public a(com.immomo.a.a.a aVar) {
        this.b = aVar;
        this.c = g.a();
    }

    private void b() {
        this.f = false;
        if (this.d != null) {
            this.d.f654a = false;
            try {
                this.d.interrupt();
            } catch (Exception e2) {
            }
        }
        if (this.e != null) {
            try {
                this.e.close();
            } catch (IOException e3) {
            }
            this.e = null;
        }
    }

    public final synchronized void a() {
        b();
    }

    public final synchronized void a(InputStream inputStream) {
        if (this.f) {
            b();
        }
        this.f = true;
        this.e = new BufferedInputStream(inputStream, 1024);
        this.d = new b(this, this.e);
        this.d.start();
    }
}
