package com.immomo.a.a.d;

import com.immomo.a.a.a;
import org.json.JSONArray;
import org.json.JSONException;

public final class c extends i {
    public c(a aVar, String str) {
        super(aVar, str);
        a("msgst");
    }

    public final void a(String[] strArr) {
        JSONArray jSONArray = new JSONArray();
        for (String str : strArr) {
            if (str != null) {
                jSONArray.put(str);
            }
        }
        try {
            put("msgid", jSONArray);
        } catch (JSONException e) {
        }
    }

    public final void f(String str) {
        try {
            put("st", str);
        } catch (JSONException e) {
        }
    }
}
