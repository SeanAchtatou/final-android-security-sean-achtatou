package com.immomo.a.a.e;

import com.immomo.a.a.a;
import com.immomo.a.a.f.b;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.json.JSONObject;

public final class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private boolean f664a = false;
    /* access modifiers changed from: private */
    public final Lock b = new ReentrantLock();
    /* access modifiers changed from: private */
    public final Condition c = this.b.newCondition();
    private final Condition d = this.b.newCondition();
    private c e = null;
    private b f = null;
    /* access modifiers changed from: private */
    public a g = null;
    /* access modifiers changed from: private */
    public com.immomo.a.a.a.a h = a.a().a("Synchronizer-" + b.a(5).toLowerCase());
    private TimerTask i = null;
    private Timer j = new Timer();
    /* access modifiers changed from: private */
    public boolean k = false;
    /* access modifiers changed from: private */
    public boolean l = false;
    private boolean m = false;
    private boolean n = true;
    /* access modifiers changed from: private */
    public int o = 0;

    public e(a aVar, c cVar) {
        this.g = aVar;
        this.e = cVar;
    }

    private void a(long j2) {
        if (this.i != null) {
            this.i.cancel();
            this.i = null;
            this.j.purge();
        }
        if (j2 > 0) {
            this.i = new f(this);
            this.j.schedule(this.i, j2);
        }
    }

    private void d() {
        JSONObject b2 = this.e.b();
        if (b2 != null && b2.length() > 0) {
            a aVar = new a(this.g, this.e);
            aVar.b(b2);
            aVar.j();
        }
    }

    /* JADX INFO: finally extract failed */
    private void e() {
        do {
            this.k = true;
            com.immomo.a.a.d.b bVar = new com.immomo.a.a.d.b();
            bVar.a("msg-syn");
            int a2 = this.f.a();
            if (this.f != null && a2 > 0) {
                bVar.put("net", this.f.a());
            }
            this.g.a(bVar);
            this.b.lock();
            while (this.k) {
                try {
                    try {
                        this.c.awaitNanos(TimeUnit.SECONDS.toNanos(60));
                        if (this.k && this.o >= 4) {
                            throw new com.immomo.a.a.b.e("sync timeout");
                        }
                    } catch (InterruptedException e2) {
                    }
                } catch (Throwable th) {
                    this.b.unlock();
                    throw th;
                }
            }
            this.b.unlock();
            if (!this.m) {
                d();
            }
            if (!this.l) {
                break;
            }
        } while (!this.m);
        this.o = 0;
    }

    public final void a() {
        this.b.lock();
        try {
            this.o = 0;
            this.m = true;
            a(0);
            this.d.signalAll();
            if (this.k) {
                this.k = false;
                this.l = false;
                this.c.signalAll();
            }
            this.e.c();
        } catch (IOException e2) {
            this.h.a((Throwable) e2);
        } finally {
            this.b.unlock();
        }
        this.h.b("Synchronizer destoryed");
    }

    public final void a(b bVar) {
        this.f = bVar;
    }

    /* JADX INFO: finally extract failed */
    public final boolean a(com.immomo.a.a.d.b bVar) {
        boolean z = false;
        if (bVar.has("lv") && bVar.has("lt")) {
            a(15000);
            long j2 = bVar.getLong("lv");
            String string = bVar.getString("lt");
            if (j2 <= this.e.a(string)) {
                this.h.c("ListVersion error. Must be greater than " + this.e.a(string));
                return true;
            }
            JSONObject b2 = this.e.b();
            if (b2 == null) {
                b2 = new JSONObject();
                this.e.a(b2);
            }
            b2.put(string, j2);
            this.e.a(string, j2);
        }
        if ("msg-ack".equals(bVar.a())) {
            this.b.lock();
            try {
                JSONObject optJSONObject = bVar.optJSONObject("lvs");
                if (optJSONObject == null || optJSONObject.length() == 0) {
                    this.e.a((JSONObject) null);
                } else {
                    this.e.a(optJSONObject);
                }
                this.o = 0;
                this.k = false;
                if (bVar.optInt("remain") == 1) {
                    z = true;
                }
                this.l = z;
                a(0);
                this.c.signal();
                this.b.unlock();
                return true;
            } catch (Throwable th) {
                this.b.unlock();
                throw th;
            }
        } else if (!"msg-psh".equals(bVar.a())) {
            return false;
        } else {
            this.b.lock();
            try {
                this.f664a = true;
                this.d.signal();
                this.b.unlock();
                return true;
            } catch (Throwable th2) {
                this.b.unlock();
                throw th2;
            }
        }
    }

    public final b b() {
        return this.f;
    }

    public final boolean c() {
        return this.n;
    }

    public final void run() {
        this.h.b("Synchronizer launched");
        try {
            d();
            e();
            while (!this.m) {
                this.b.lock();
                while (!this.m && (this.k || !this.f664a)) {
                    try {
                        this.d.await();
                    } catch (Exception e2) {
                    }
                }
                this.f664a = false;
                this.b.unlock();
                if (!this.m) {
                    e();
                }
            }
            this.n = false;
        } catch (Exception e3) {
            this.g.a("Synchronizer error", e3);
            this.n = false;
        } catch (Throwable th) {
            this.n = false;
            throw th;
        }
    }
}
