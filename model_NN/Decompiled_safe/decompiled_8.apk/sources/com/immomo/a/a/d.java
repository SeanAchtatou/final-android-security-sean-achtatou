package com.immomo.a.a;

import com.immomo.a.a.a.a;
import com.immomo.a.a.b.b;
import com.immomo.momo.protocol.imjson.l;
import java.io.IOException;
import java.net.Socket;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;

public final class d extends a {
    private Socket b = null;
    private a c = a().a(getClass().getSimpleName());
    private com.immomo.a.a.c.a d = null;
    private com.immomo.a.a.c.d e = null;
    private boolean f = false;
    private boolean g = false;

    public d(b bVar) {
        super(bVar);
    }

    private static Socket a(String str, int i) {
        Socket[] socketArr = new Socket[1];
        Exception[] excArr = new Exception[1];
        Object obj = new Object();
        AtomicBoolean atomicBoolean = new AtomicBoolean(false);
        new Thread(new e(obj, socketArr, str, i, atomicBoolean, excArr)).start();
        synchronized (obj) {
            try {
                obj.wait(10000);
            } catch (InterruptedException e2) {
            }
        }
        if (excArr[0] != null) {
            throw excArr[0];
        } else if (socketArr[0] != null) {
            return socketArr[0];
        } else {
            atomicBoolean.set(true);
            throw new b("[" + str + ":" + i + "] connect timeout, total time=10000");
        }
    }

    public final void a(com.immomo.a.a.d.d dVar) {
        if (!this.g) {
            throw new IllegalStateException("Not connected to server.");
        } else if (dVar == null || this.e == null) {
            throw new NullPointerException("Packet or Writer is null.");
        } else {
            this.e.a(dVar);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.a.a.a.a(java.lang.String, com.immomo.a.a.h):void
     arg types: [java.lang.String, android.support.v4.b.a]
     candidates:
      com.immomo.a.a.d.a(java.lang.String, int):java.net.Socket
      com.immomo.a.a.d.a(java.lang.String, java.lang.Throwable):void
      com.immomo.a.a.a.a(java.lang.String, com.immomo.a.a.f):void
      com.immomo.a.a.a.a(java.lang.String, java.lang.Throwable):void
      com.immomo.a.a.a.a(java.lang.String, com.immomo.a.a.h):void */
    public final void a(String str, String str2, String str3) {
        d();
        android.support.v4.b.a aVar = new android.support.v4.b.a(this);
        a(com.immomo.a.a.f.b.a(5), (h) aVar);
        aVar.a(str, str2, str3, this.f648a.j());
        this.e.c();
        this.f = true;
        this.f648a.b(str);
        this.f648a.c(str2);
        this.f648a.f(str3);
    }

    public final void a(String str, Throwable th) {
        this.c.a(str, th);
        if (this.g) {
            k();
            Iterator it = e().iterator();
            while (it.hasNext()) {
                it.next();
            }
        }
    }

    public final void f() {
        if (this.g) {
            k();
        }
        String a2 = this.f648a.a();
        int b2 = this.f648a.b();
        l.i = 0;
        l.n = 0;
        l.k = 0;
        l.m = 0;
        l.j = 0;
        this.c.a("try connect to server , " + a2 + ":" + b2);
        long currentTimeMillis = System.currentTimeMillis();
        this.b = a(a2, b2);
        l.i = System.currentTimeMillis() - currentTimeMillis;
        this.c.a("connect success , " + a2 + ":" + b2);
        long currentTimeMillis2 = System.currentTimeMillis();
        this.g = true;
        if (this.e == null) {
            this.e = new com.immomo.a.a.c.d(this);
        }
        if (this.d == null) {
            this.d = new com.immomo.a.a.c.a(this);
        }
        this.d.a(this.b.getInputStream());
        this.e.a(this.b.getOutputStream());
        if (c() != null) {
            c().c();
        }
        this.f648a.l();
        l.n = System.currentTimeMillis() - currentTimeMillis2;
    }

    public final void k() {
        this.f = false;
        if (this.g) {
            this.g = false;
            if (this.e != null) {
                this.e.a();
            }
            if (this.d != null) {
                this.d.a();
            }
            if (this.b != null) {
                try {
                    this.b.close();
                } catch (IOException e2) {
                    this.c.a((Throwable) e2);
                }
                this.b = null;
            }
            super.k();
            this.c.a("Connection disconnected! ");
        }
    }

    public final boolean l() {
        return this.g;
    }

    public final boolean m() {
        return this.f;
    }
}
