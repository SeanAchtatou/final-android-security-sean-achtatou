package com.immomo.a.a.f;

import java.util.Random;

public abstract class b {

    /* renamed from: a  reason: collision with root package name */
    private static Random f666a = new Random();
    private static char[] b = "0123456789abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
    private static String c = (String.valueOf(a(7)) + "-");
    private static long d = 0;

    public static synchronized String a() {
        String sb;
        synchronized (b.class) {
            StringBuilder sb2 = new StringBuilder(String.valueOf(c));
            long j = d;
            d = 1 + j;
            sb = sb2.append(Long.toString(j)).toString();
        }
        return sb;
    }

    public static String a(int i) {
        if (i <= 0) {
            return null;
        }
        char[] cArr = new char[i];
        for (int i2 = 0; i2 < cArr.length; i2++) {
            cArr[i2] = b[f666a.nextInt(71)];
        }
        return new String(cArr);
    }
}
