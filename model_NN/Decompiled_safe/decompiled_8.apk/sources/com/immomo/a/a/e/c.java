package com.immomo.a.a.e;

import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.a.a.f.a;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;
import org.json.JSONObject;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private String f662a;
    private Properties b;
    private File c = null;
    private JSONObject d = null;

    public c(String str) {
        this.f662a = str;
        this.b = new Properties();
        File file = new File(this.f662a);
        if (!file.exists()) {
            file.createNewFile();
        }
        FileInputStream fileInputStream = new FileInputStream(file);
        this.b.load(fileInputStream);
        fileInputStream.close();
        this.c = file;
        if (!a.a(a("lvs", PoiTypeDef.All))) {
            this.d = new JSONObject(a("lvs", PoiTypeDef.All));
        }
    }

    private String a(String str, String str2) {
        return this.b.getProperty(str, str2);
    }

    public final long a(String str) {
        try {
            return Long.parseLong(a(str, new StringBuilder(String.valueOf(0L)).toString()));
        } catch (Exception e) {
            return 0;
        }
    }

    public final String a() {
        return this.f662a;
    }

    public final void a(String str, long j) {
        a(str, Long.valueOf(j));
    }

    public final void a(String str, Object obj) {
        this.b.setProperty(str, obj.toString());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.a.a.e.c.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.immomo.a.a.e.c.a(java.lang.String, java.lang.String):java.lang.String
      com.immomo.a.a.e.c.a(java.lang.String, long):void
      com.immomo.a.a.e.c.a(java.lang.String, java.lang.Object):void */
    public final void a(JSONObject jSONObject) {
        this.d = jSONObject;
        if (jSONObject == null) {
            this.b.remove("lvs");
        } else {
            a("lvs", (Object) jSONObject.toString());
        }
    }

    public final JSONObject b() {
        return this.d;
    }

    public final void c() {
        a(this.d);
        FileOutputStream fileOutputStream = new FileOutputStream(this.c);
        this.b.store(fileOutputStream, (String) null);
        fileOutputStream.close();
    }
}
