package com.immomo.a.a.d;

import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.a.a.a;
import com.immomo.a.a.b.c;
import org.json.JSONException;

public final class h extends i {
    public h(a aVar) {
        super(aVar);
        a("set");
        f(PoiTypeDef.All);
    }

    public final boolean a(b bVar) {
        if (bVar.has("ec")) {
            bVar.optInt("ec");
            new c(bVar.optString("em"));
        }
        return super.a(bVar);
    }

    public final void f(String str) {
        try {
            put("ns", str);
        } catch (JSONException e) {
        }
    }

    @Deprecated
    public final b j() {
        return super.j();
    }
}
