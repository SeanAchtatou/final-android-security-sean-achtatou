package com.immomo.a.a.c;

import com.immomo.a.a.a.a;
import com.immomo.a.a.d.d;
import com.immomo.a.a.d.e;
import com.immomo.a.a.d.g;
import com.immomo.momo.util.ar;

final class f extends Thread {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public boolean f658a;
    private a b;
    private /* synthetic */ d c;

    private f(d dVar) {
        this.c = dVar;
        this.f658a = true;
        this.b = com.immomo.a.a.a.a().a("PacketWriter-" + getId());
    }

    /* synthetic */ f(d dVar, byte b2) {
        this(dVar);
    }

    public final void run() {
        byte[] bArr;
        while (true) {
            try {
                if (!this.f658a || !this.c.b) {
                    break;
                }
                try {
                    d dVar = (d) this.c.f.take();
                    if (!(dVar instanceof e)) {
                        String c2 = dVar.c();
                        byte[] bytes = c2.getBytes("UTF-8");
                        synchronized (this.c) {
                            if (this.c.b && this.f658a && !(dVar instanceof g)) {
                                this.b.a("-->: " + c2);
                                bArr = this.c.f656a.c() != null ? this.c.f656a.c().a(bytes) : bytes;
                            }
                        }
                    } else {
                        this.b.a("-->: ");
                        bArr = null;
                    }
                    this.c.e.write(1);
                    this.c.e.write(2);
                    if (bArr != null) {
                        this.c.e.write(bArr);
                    }
                    this.c.e.write(3);
                    this.c.e.write(4);
                    this.c.e.flush();
                    if (bArr != null) {
                        ar.d((long) (bArr.length + 4));
                    } else {
                        ar.d(4);
                    }
                } catch (Exception e) {
                    this.f658a = false;
                    this.c.e.close();
                    this.c.f656a.a("packetwriter stoped. thread id=" + getId() + ". ", e);
                }
            } catch (Exception e2) {
                this.f658a = false;
                this.c.f656a.a("packetwriter stoped. thread id=" + getId() + ". ", e2);
                return;
            } finally {
                this.c.f.clear();
            }
        }
    }
}
