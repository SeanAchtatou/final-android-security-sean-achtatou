package com.immomo.a.a.c;

import com.immomo.a.a.a;
import com.immomo.a.a.d.g;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public final class d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public a f656a = null;
    /* access modifiers changed from: private */
    public boolean b;
    private f c = null;
    private e d = null;
    /* access modifiers changed from: private */
    public OutputStream e = null;
    /* access modifiers changed from: private */
    public final BlockingQueue f;
    private com.immomo.a.a.a.a g = a.a().a("PacketWriter");

    public d(a aVar) {
        this.f656a = aVar;
        this.f = new LinkedBlockingQueue();
    }

    private void d() {
        this.b = false;
        try {
            this.f.clear();
            this.f.put(new g());
        } catch (InterruptedException e2) {
        }
        if (this.c != null) {
            this.c.f658a = false;
            try {
                this.c.interrupt();
            } catch (Exception e3) {
            }
            this.c = null;
        }
        if (this.e != null) {
            try {
                this.e.close();
            } catch (IOException e4) {
            }
            this.e = null;
        }
        if (this.d != null) {
            this.d.f657a = false;
            this.d = null;
        }
    }

    public final synchronized void a() {
        d();
    }

    public final void a(com.immomo.a.a.d.d dVar) {
        try {
            this.f.put(dVar);
        } catch (InterruptedException e2) {
            this.g.a((Throwable) e2);
        }
    }

    public final synchronized void a(OutputStream outputStream) {
        if (this.b) {
            d();
        }
        this.b = true;
        this.f.clear();
        this.e = new BufferedOutputStream(outputStream);
        this.c = new f(this, (byte) 0);
        this.c.start();
    }

    public final synchronized boolean b() {
        return this.b;
    }

    public final synchronized void c() {
        if (this.d != null) {
            this.d.f657a = false;
        }
        if (this.f656a.b().n() > 0) {
            this.d = new e(this, this.f656a.b().n(), this.f656a.b().f());
            this.f656a.b("pi", this.d);
            this.f656a.b("po", this.d);
            new Thread(this.d).start();
        }
    }
}
