package com.immomo.a.a.e;

import java.io.File;

public class d {

    /* renamed from: a  reason: collision with root package name */
    private static c f663a = null;

    public b a() {
        return new b();
    }

    public final c a(String str) {
        if (f663a != null && f663a.a().equals(str)) {
            return f663a;
        }
        c b = b(str);
        f663a = b;
        return b;
    }

    /* access modifiers changed from: protected */
    public c b(String str) {
        try {
            return new c(str);
        } catch (Exception e) {
            new File(str).delete();
            return new c(str);
        }
    }
}
