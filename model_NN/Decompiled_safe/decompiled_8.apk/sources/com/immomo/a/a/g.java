package com.immomo.a.a;

import com.immomo.a.a.a.a;
import com.immomo.a.a.d.b;
import com.immomo.a.a.e.e;

public final class g {
    private static g b = null;

    /* renamed from: a  reason: collision with root package name */
    private a f667a = a.a().a("JsonParser");

    private g() {
    }

    public static g a() {
        if (b == null) {
            b = new g();
        }
        return b;
    }

    public final void a(a aVar, String str) {
        b bVar;
        if (!com.immomo.a.a.f.a.a(str)) {
            bVar = b.e(str);
        } else {
            bVar = new b();
            bVar.a("po");
        }
        e i = aVar.i();
        if (i == null || !i.a(bVar)) {
            f fVar = null;
            if (bVar.has("id") && (fVar = aVar.c(bVar.d())) != null && fVar.a(bVar)) {
                return;
            }
            if (fVar == null && (fVar = aVar.e(bVar.a())) != null && fVar.a(bVar)) {
                return;
            }
            if ((fVar != null || (fVar = aVar.e("*")) == null || !fVar.a(bVar)) && fVar == null) {
                this.f667a.c("adapter not found by '" + str + "'");
            }
        }
    }
}
