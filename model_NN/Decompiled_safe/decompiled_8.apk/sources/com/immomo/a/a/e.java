package com.immomo.a.a;

import java.net.Socket;
import java.util.concurrent.atomic.AtomicBoolean;

final class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Object f660a;
    private final /* synthetic */ Socket[] b;
    private final /* synthetic */ String c;
    private final /* synthetic */ int d;
    private final /* synthetic */ AtomicBoolean e;
    private final /* synthetic */ Exception[] f;

    e(Object obj, Socket[] socketArr, String str, int i, AtomicBoolean atomicBoolean, Exception[] excArr) {
        this.f660a = obj;
        this.b = socketArr;
        this.c = str;
        this.d = i;
        this.e = atomicBoolean;
        this.f = excArr;
    }

    public final void run() {
        try {
            this.b[0] = new Socket(this.c, this.d);
            if (this.e.get()) {
                this.b[0].close();
                this.b[0] = null;
            }
            synchronized (this.f660a) {
                this.f660a.notify();
            }
        } catch (Exception e2) {
            this.f[0] = e2;
            synchronized (this.f660a) {
                this.f660a.notify();
            }
        } catch (Throwable th) {
            synchronized (this.f660a) {
                this.f660a.notify();
                throw th;
            }
        }
    }
}
