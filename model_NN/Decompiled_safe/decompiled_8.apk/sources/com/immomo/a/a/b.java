package com.immomo.a.a;

import com.amap.mapapi.poisearch.PoiTypeDef;

public final class b implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private String f651a;
    private int b;
    private String c;
    private String d;
    private String e;
    private String f = PoiTypeDef.All;
    private int g;
    private int h;
    private boolean i = false;
    private int j = 30;
    private int[] k = new int[0];
    private int l = 60;
    private String m = PoiTypeDef.All;

    public static void i() {
    }

    public final String a() {
        return this.f651a;
    }

    public final void a(int i2) {
        this.b = i2;
    }

    public final void a(String str) {
        this.f651a = str;
    }

    public final void a(int[] iArr) {
        if (this.k != null && iArr != null) {
            for (int i2 : this.k) {
                for (int i3 : iArr) {
                    if (i2 == i3) {
                        this.h = i2;
                        this.i = true;
                    }
                }
            }
        }
    }

    public final int b() {
        return this.b;
    }

    public final void b(int i2) {
        this.l = i2;
        if (this.l <= 0) {
            this.l = 60;
        }
    }

    public final void b(String str) {
        this.c = str;
    }

    public final String c() {
        return this.c;
    }

    public final void c(int i2) {
        this.g = i2;
    }

    public final void c(String str) {
        this.d = str;
    }

    public final String d() {
        return this.d;
    }

    public final void d(int i2) {
        this.j = i2;
        if (i2 < 0) {
            this.j = 30;
        }
    }

    public final void d(String str) {
        this.m = str;
    }

    public final String e() {
        return this.f;
    }

    public final void e(String str) {
        this.f = str;
    }

    public final int f() {
        return this.l;
    }

    public final void f(String str) {
        this.e = str;
    }

    public final String g() {
        return this.m;
    }

    public final String h() {
        return this.e;
    }

    public final int j() {
        return this.g;
    }

    public final int k() {
        return this.h;
    }

    public final void l() {
        this.h = 0;
        this.i = false;
    }

    public final boolean m() {
        return this.i;
    }

    public final int n() {
        return this.j;
    }
}
