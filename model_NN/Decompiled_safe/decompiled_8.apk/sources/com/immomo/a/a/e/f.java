package com.immomo.a.a.e;

import java.util.TimerTask;

final class f extends TimerTask {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ e f665a;

    f(e eVar) {
        this.f665a = eVar;
    }

    public final void run() {
        boolean z = true;
        try {
            this.f665a.b.lock();
            if (this.f665a.k) {
                e eVar = this.f665a;
                eVar.o = eVar.o + 1;
                this.f665a.l = this.f665a.o < 4;
                e eVar2 = this.f665a;
                if (this.f665a.o < 4) {
                    z = false;
                }
                eVar2.k = z;
                this.f665a.c.signal();
            }
            this.f665a.h.c("sync timeout");
            this.f665a.b.unlock();
        } catch (Exception e) {
            this.f665a.g.a(" send list version failed ", e);
        }
    }
}
