package com.immomo.a.a.d;

import com.immomo.a.a.a;
import com.immomo.a.a.b.e;
import com.immomo.a.a.f;
import com.immomo.a.a.f.b;
import com.immomo.a.a.h;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.json.JSONObject;

public class i extends b implements f, h {

    /* renamed from: a  reason: collision with root package name */
    private a f659a;
    private int b;
    private int c;
    private boolean d;
    private boolean e;
    private b f;
    private Lock g;
    private Condition h;

    public i(a aVar) {
        this(aVar, b.a());
    }

    public i(a aVar, String str) {
        this.f659a = null;
        this.b = 1;
        this.c = 20;
        this.d = false;
        this.e = false;
        this.f = null;
        this.g = null;
        this.h = null;
        this.f659a = aVar;
        b(str);
        this.g = new ReentrantLock();
        this.h = this.g.newCondition();
    }

    public final void a(Object obj, f fVar) {
    }

    public final void a(JSONObject jSONObject) {
        super.a(jSONObject);
        b(optString("id"));
    }

    public boolean a(b bVar) {
        this.g.lock();
        try {
            this.f = bVar;
            this.d = true;
            this.h.signal();
        } catch (Exception e2) {
        } finally {
            this.g.unlock();
        }
        return true;
    }

    public final void b() {
        this.g.lock();
        try {
            this.e = true;
            this.h.signal();
        } catch (Exception e2) {
        } finally {
            this.g.unlock();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.a.a.a.a(java.lang.String, com.immomo.a.a.f):void
     arg types: [java.lang.String, com.immomo.a.a.d.i]
     candidates:
      com.immomo.a.a.a.a(java.lang.String, com.immomo.a.a.h):void
      com.immomo.a.a.a.a(java.lang.String, java.lang.Throwable):void
      com.immomo.a.a.a.a(java.lang.String, com.immomo.a.a.f):void */
    public final void b(String str) {
        super.b(str);
        this.f659a.a(str, (f) this);
    }

    /* JADX INFO: finally extract failed */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.a.a.a.a(java.lang.String, com.immomo.a.a.h):void
     arg types: [java.lang.String, com.immomo.a.a.d.i]
     candidates:
      com.immomo.a.a.a.a(java.lang.String, com.immomo.a.a.f):void
      com.immomo.a.a.a.a(java.lang.String, java.lang.Throwable):void
      com.immomo.a.a.a.a(java.lang.String, com.immomo.a.a.h):void */
    public b j() {
        try {
            a aVar = this.f659a;
            a.a(optString("id"), (h) this);
            this.f659a.a(this);
            if (!(this.b > 0 && this.c > 0)) {
                a aVar2 = this.f659a;
                a.a(optString("id"));
                this.f659a.b(optString("id"));
                return null;
            }
            while (true) {
                int i = this.b;
                this.b = i - 1;
                if (i <= 0) {
                    break;
                }
                try {
                    this.g.lock();
                    long nanos = TimeUnit.SECONDS.toNanos((long) this.c);
                    while (!this.e && !this.d && nanos > 0) {
                        nanos = this.h.awaitNanos(nanos);
                    }
                    if (!this.e) {
                        if (this.d) {
                            this.g.unlock();
                            break;
                        }
                        if (this.b > 0) {
                            this.f659a.a(this);
                        }
                        this.g.unlock();
                    } else {
                        throw new InterruptedException(c());
                    }
                } catch (Exception e2) {
                    throw e2;
                } catch (Throwable th) {
                    this.g.unlock();
                    throw th;
                }
            }
            if (!this.d) {
                throw new e("response timeout by [ " + c() + " ]");
            }
            b bVar = this.f;
            a aVar3 = this.f659a;
            a.a(optString("id"));
            this.f659a.b(optString("id"));
            return bVar;
        } catch (Throwable th2) {
            a aVar4 = this.f659a;
            a.a(optString("id"));
            this.f659a.b(optString("id"));
            throw th2;
        }
    }
}
