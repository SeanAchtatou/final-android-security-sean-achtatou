package com.immomo.a.a.e;

import com.immomo.a.a.d.b;
import com.immomo.a.a.d.i;
import java.io.IOException;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

public final class a extends i {

    /* renamed from: a  reason: collision with root package name */
    private c f661a = null;
    private JSONObject b = null;

    public a(com.immomo.a.a.a aVar, c cVar) {
        super(aVar);
        this.f661a = cVar;
        a("msg-fin");
    }

    public final void b(JSONObject jSONObject) {
        try {
            put("lvs", jSONObject);
            this.b = jSONObject;
        } catch (JSONException e) {
        }
    }

    public final b j() {
        b j = super.j();
        this.f661a.a((JSONObject) null);
        Iterator<String> keys = this.b.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            this.f661a.a(next, this.b.get(next));
        }
        try {
            this.f661a.c();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return j;
    }
}
