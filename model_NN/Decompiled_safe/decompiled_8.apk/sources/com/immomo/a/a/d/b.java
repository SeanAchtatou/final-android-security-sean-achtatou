package com.immomo.a.a.d;

import com.immomo.a.a.f.a;
import com.sina.sdk.api.message.InviteApi;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

public class b extends JSONObject implements d {
    public b() {
    }

    private b(String str) {
        super(str);
    }

    public static b e(String str) {
        if (!a.a(str)) {
            return new b(str);
        }
        throw new JSONException("json string is empty.");
    }

    public final String a() {
        return optString("_");
    }

    public final void a(Object obj) {
        try {
            put("_t", obj);
        } catch (JSONException e) {
        }
    }

    public final void a(String str) {
        try {
            put("_", str);
        } catch (JSONException e) {
        }
    }

    public void a(JSONObject jSONObject) {
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            put(next, jSONObject.get(next));
        }
    }

    public final void b(Object obj) {
        try {
            put("type", obj);
        } catch (JSONException e) {
        }
    }

    public void b(String str) {
        try {
            put("id", str);
        } catch (Exception e) {
        }
    }

    public final String c() {
        return toString();
    }

    public final void c(String str) {
        try {
            put(InviteApi.KEY_TEXT, str);
        } catch (JSONException e) {
        }
    }

    public final String d() {
        return optString("id");
    }

    public final void d(String str) {
        try {
            put("to", str);
        } catch (JSONException e) {
        }
    }

    public final String e() {
        return optString(InviteApi.KEY_TEXT);
    }

    public final String f() {
        return optString("to");
    }

    public final String g() {
        return optString("fr");
    }

    public final String h() {
        return optString("ns");
    }

    public final boolean i() {
        return has(InviteApi.KEY_TEXT);
    }
}
