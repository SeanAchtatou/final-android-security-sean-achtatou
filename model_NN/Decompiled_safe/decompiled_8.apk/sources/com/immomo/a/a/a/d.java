package com.immomo.a.a.a;

import com.amap.mapapi.poisearch.PoiTypeDef;
import java.text.SimpleDateFormat;
import java.util.Date;

public class d implements a {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f650a = true;
    private String b;
    private String c;
    private String d;

    public d() {
        this.b = PoiTypeDef.All;
        this.c = PoiTypeDef.All;
        this.d = PoiTypeDef.All;
        this.b = getClass().getSimpleName();
    }

    public d(String str) {
        this.b = PoiTypeDef.All;
        this.c = PoiTypeDef.All;
        this.d = PoiTypeDef.All;
        this.b = str;
    }

    private static void a(Appendable appendable, String str, Throwable th) {
        try {
            appendable.append(th.toString());
            appendable.append("\n");
            StackTraceElement[] stackTrace = th.getStackTrace();
            if (stackTrace != null) {
                for (int i = 0; i < stackTrace.length + 0; i++) {
                    appendable.append(str);
                    appendable.append("\tat ");
                    appendable.append(stackTrace[i].toString());
                    appendable.append("\n");
                }
            }
            try {
                Throwable[] thArr = (Throwable[]) th.getClass().getMethod("getSuppressed", new Class[0]).invoke(th, new Object[0]);
                if (thArr != null) {
                    for (Throwable a2 : thArr) {
                        appendable.append(str);
                        appendable.append("\tSuppressed: ");
                        a(appendable, String.valueOf(str) + "\t", a2);
                    }
                }
            } catch (Exception e) {
            }
            Throwable cause = th.getCause();
            if (cause != null) {
                appendable.append(str);
                appendable.append("Caused by: ");
                a(appendable, str, cause);
            }
        } catch (Exception e2) {
            throw new AssertionError();
        }
    }

    private void a(Object obj, Throwable th) {
        a(obj != null ? obj.toString() : "null", th, b.ERROR);
    }

    public final String a() {
        return this.c;
    }

    public final void a(Object obj) {
        a(String.valueOf(this.c) + (obj == null ? "null" : obj.toString()) + this.d, (Throwable) null, b.INFO);
    }

    public void a(String str) {
        this.b = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.a.a.a.d.a(java.lang.Object, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Throwable]
     candidates:
      com.immomo.a.a.a.d.a(java.lang.String, java.lang.Throwable):void
      com.immomo.a.a.a.a.a(java.lang.String, java.lang.Throwable):void
      com.immomo.a.a.a.d.a(java.lang.Object, java.lang.Throwable):void */
    public final void a(String str, Throwable th) {
        a((Object) str, th);
    }

    public void a(String str, Throwable th, b bVar) {
        if (f650a) {
            StringBuilder append = new StringBuilder().append(new SimpleDateFormat("HH:mm:ss").format(new Date())).append('/').append(String.valueOf(bVar.name()) + "/" + this.b + "\t").append(str).append(10);
            if (th != null) {
                a(append, PoiTypeDef.All, th);
            }
            System.out.println(str);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.a.a.a.d.a(java.lang.Object, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Throwable]
     candidates:
      com.immomo.a.a.a.d.a(java.lang.String, java.lang.Throwable):void
      com.immomo.a.a.a.a.a(java.lang.String, java.lang.Throwable):void
      com.immomo.a.a.a.d.a(java.lang.Object, java.lang.Throwable):void */
    public final void a(Throwable th) {
        a((Object) (th != null ? th.getMessage() : "null"), th);
    }

    public final void b(Object obj) {
        a(String.valueOf(this.c) + (obj != null ? obj.toString() : "null") + this.d, (Throwable) null, b.DEBUG);
    }

    public final void b(String str) {
        this.c = str;
    }

    public final void c(Object obj) {
        a(String.valueOf(this.c) + (obj != null ? obj.toString() : "null") + this.d, (Throwable) null, b.WARNING);
    }
}
