package com.immomo.momo;

import android.graphics.Bitmap;
import android.support.v4.b.a;
import com.immomo.momo.android.c.u;
import com.immomo.momo.service.ad;
import com.immomo.momo.service.bean.am;
import com.immomo.momo.util.c;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class b {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final Set f2835a = new HashSet();
    private static final Map b = new HashMap();
    private static final List c = new ArrayList();

    public static Bitmap a(String str) {
        Bitmap a2;
        Bitmap bitmap = (Bitmap) c.a().a(str);
        if (bitmap != null) {
            return bitmap;
        }
        File file = new File(a.z(), String.valueOf(str) + ".png_");
        return (!file.exists() || (a2 = g.a(file, R.drawable.ic_userinfo_renren)) == null) ? bitmap : a(str, a2);
    }

    public static Bitmap a(String str, Bitmap bitmap) {
        c.a().a(str, bitmap);
        return bitmap;
    }

    public static Bitmap a(String str, boolean z) {
        am c2 = c(str);
        if (c2 == null) {
            return null;
        }
        return a(String.valueOf(c2.d) + (z ? "_s2" : "_l"));
    }

    public static void a() {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(java.util.List r5) {
        /*
            java.util.Map r1 = com.immomo.momo.b.b
            monitor-enter(r1)
            if (r5 == 0) goto L_0x000b
            boolean r0 = r5.isEmpty()     // Catch:{ all -> 0x001e }
            if (r0 == 0) goto L_0x000d
        L_0x000b:
            monitor-exit(r1)     // Catch:{ all -> 0x001e }
        L_0x000c:
            return
        L_0x000d:
            java.util.List r0 = com.immomo.momo.b.c     // Catch:{ all -> 0x001e }
            r0.clear()     // Catch:{ all -> 0x001e }
            java.util.Iterator r2 = r5.iterator()     // Catch:{ all -> 0x001e }
        L_0x0016:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x001e }
            if (r0 != 0) goto L_0x0021
            monitor-exit(r1)     // Catch:{ all -> 0x001e }
            goto L_0x000c
        L_0x001e:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x0021:
            java.lang.Object r0 = r2.next()     // Catch:{ all -> 0x001e }
            com.immomo.momo.service.bean.am r0 = (com.immomo.momo.service.bean.am) r0     // Catch:{ all -> 0x001e }
            java.util.Map r3 = com.immomo.momo.b.b     // Catch:{ all -> 0x001e }
            java.lang.String r4 = r0.f2991a     // Catch:{ all -> 0x001e }
            r3.put(r4, r0)     // Catch:{ all -> 0x001e }
            java.util.List r3 = com.immomo.momo.b.c     // Catch:{ all -> 0x001e }
            r3.add(r0)     // Catch:{ all -> 0x001e }
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.b.a(java.util.List):void");
    }

    public static Bitmap b(String str) {
        if (a.a((CharSequence) str)) {
            return null;
        }
        Bitmap a2 = a(a.n(str));
        if (a2 != null || a.a((CharSequence) str) || f2835a.contains(str)) {
            return a2;
        }
        f2835a.add(str);
        u.b().execute(new c(str));
        return a2;
    }

    public static List b() {
        if (c.isEmpty()) {
            d();
        }
        return c;
    }

    public static am c(String str) {
        if (b.isEmpty()) {
            d();
        }
        return (am) b.get(str);
    }

    private static void d() {
        synchronized (b) {
            if (b.isEmpty()) {
                List<am> c2 = new ad().c();
                c.clear();
                c.addAll(c2);
                for (am amVar : c2) {
                    b.put(amVar.f2991a, amVar);
                }
            }
        }
    }
}
