package com.immomo.momo.util;

import com.immomo.momo.g;
import org.json.JSONException;
import org.json.JSONObject;

public final class ar {

    /* renamed from: a  reason: collision with root package name */
    private static long f3073a;
    private static long b = x.a("audiod", (Long) 0L);
    private static long c = x.a("imaged", (Long) 0L);
    private static long d = x.a("imageu", (Long) 0L);
    private static long e = x.a("audiou", (Long) 0L);
    private static long f = x.a("emotiond", (Long) 0L);
    private static long g = x.a("emotionpk", (Long) 0L);
    private static long h = x.a("im", (Long) 0L);
    private static long i = x.a("pipo", (Long) 0L);
    private static long j = x.a("maximageu", (Long) 0L);
    private static long k = x.a("maxaudiou", (Long) 0L);
    private static long l = x.a("maxaudiod", (Long) 0L);
    private static long m = x.a("maximaged", (Long) 0L);
    private static long n = x.a("logtotal", (Long) 0L);
    private static long o = x.a("logaudiod", (Long) 0L);
    private static long p = x.a("logimgd", (Long) 0L);
    private static long q = x.a("logimgu", (Long) 0L);
    private static long r = x.a("logaudiou", (Long) 0L);
    private static long s = x.a("logemd", (Long) 0L);
    private static long t = x.a("logempkd", (Long) 0L);
    private static long u = x.a("logimj", (Long) 0L);
    private static long v = x.a("logpipo", (Long) 0L);
    private static long w = x.a("logoth", (Long) 0L);
    private static ak x;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
     arg types: [java.lang.String, long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long */
    static {
        ak a2 = ak.a(g.c(), "trafficinfo");
        x = a2;
        f3073a = a2.a("total", (Long) 0L);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    private static void A(long j2) {
        i = j2;
        x.a("pipo", (Object) Long.valueOf(j2));
        if (j2 >= 1048576 && j2 - v >= 1048576) {
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("a", ((double) Math.round(((((double) j2) / 1024.0d) / 1024.0d) * 1000.0d)) / 1000.0d);
                jSONObject.put("t", ((double) Math.round(((((double) f3073a) / 1024.0d) / 1024.0d) * 1000.0d)) / 1000.0d);
                new k("T", "T03", jSONObject).e();
                s(j2);
            } catch (JSONException e2) {
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    private static void B(long j2) {
        j = j2;
        x.a("maximageu", (Object) Long.valueOf(j2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    private static void C(long j2) {
        k = j2;
        x.a("maxaudiou", (Object) Long.valueOf(j2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    private static void D(long j2) {
        l = j2;
        x.a("maxaudiod", (Object) Long.valueOf(j2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    private static void E(long j2) {
        m = j2;
        x.a("maximaged", (Object) Long.valueOf(j2));
    }

    public static long a() {
        return f3073a;
    }

    public static void a(long j2) {
        j(f3073a + j2);
    }

    public static long b() {
        return c;
    }

    public static void b(long j2) {
        u(c + j2);
        a(j2);
        if (j2 > m) {
            E(j2);
        }
    }

    public static long c() {
        return h;
    }

    public static void c(long j2) {
        w(b + j2);
        if (j2 > l) {
            D(j2);
        }
    }

    public static long d() {
        return b;
    }

    public static void d(long j2) {
        long j3 = h + j2;
        h = j3;
        v(j3);
        a(j2);
    }

    public static long e() {
        return d;
    }

    public static void e(long j2) {
        x(d + j2);
        if (j2 > j) {
            B(j2);
        }
    }

    public static long f() {
        return e;
    }

    public static void f(long j2) {
        y(e + j2);
        if (j2 > k) {
            C(j2);
        }
    }

    public static long g() {
        return f;
    }

    public static void g(long j2) {
        z(f + j2);
    }

    public static long h() {
        return g;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    public static void h(long j2) {
        long j3 = g + j2;
        g = j3;
        x.a("emotionpk", (Object) Long.valueOf(j3));
        if (j3 >= 1048576 && j3 - t >= 1048576) {
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("a", ((double) Math.round(((((double) j3) / 1024.0d) / 1024.0d) * 1000.0d)) / 1000.0d);
                jSONObject.put("t", ((double) Math.round(((((double) f3073a) / 1024.0d) / 1024.0d) * 1000.0d)) / 1000.0d);
                new k("T", "T042", jSONObject).e();
                q(j3);
            } catch (JSONException e2) {
            }
        }
    }

    public static long i() {
        return i;
    }

    public static void i(long j2) {
        A(i + j2);
    }

    public static long j() {
        return j;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    private static void j(long j2) {
        f3073a = j2;
        x.a("total", (Object) Long.valueOf(j2));
        if (j2 >= 1048576 && j2 - n >= 1048576) {
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("a", ((double) Math.round(((((double) j2) / 1024.0d) / 1024.0d) * 1000.0d)) / 1000.0d);
                new k("T", "T01", jSONObject).e();
                k(j2);
            } catch (JSONException e2) {
            }
        }
        long n2 = n();
        if (n2 >= 1048576 && n2 - w >= 1048576) {
            try {
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("a", ((double) Math.round(((((double) n2) / 1024.0d) / 1024.0d) * 1000.0d)) / 1000.0d);
                jSONObject2.put("t", ((double) Math.round(((((double) j2) / 1024.0d) / 1024.0d) * 1000.0d)) / 1000.0d);
                new k("T", "T99", jSONObject2).e();
                t(n2);
            } catch (JSONException e3) {
            }
        }
    }

    public static long k() {
        return k;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    private static void k(long j2) {
        new m("LoggerUtil").a((Object) ("logTotalBytes=" + j2 + ",totalBytes=" + f3073a + ", totalBytes-logTotalBytes=" + (f3073a - j2) + ", 1024*1024=1048576"));
        n = j2;
        x.a("logtotal", (Object) Long.valueOf(j2));
    }

    public static long l() {
        return l;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    private static void l(long j2) {
        o = j2;
        x.a("logaudiod", (Object) Long.valueOf(j2));
    }

    public static long m() {
        return m;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    private static void m(long j2) {
        p = j2;
        x.a("logimgd", (Object) Long.valueOf(j2));
    }

    public static long n() {
        return (((((f3073a - b) - c) - e) - d) - h) - f;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    private static void n(long j2) {
        q = j2;
        x.a("logimgu", (Object) Long.valueOf(j2));
    }

    public static void o() {
        w(0);
        y(0);
        v(0);
        D(0);
        E(0);
        u(0);
        x(0);
        C(0);
        B(0);
        A(0);
        j(0);
        z(0);
        l(0);
        o(0);
        p(0);
        q(0);
        m(0);
        n(0);
        r(0);
        t(0);
        s(0);
        k(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    private static void o(long j2) {
        r = j2;
        x.a("logaudiou", (Object) Long.valueOf(j2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    private static void p(long j2) {
        s = j2;
        x.a("logemd", (Object) Long.valueOf(j2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    private static void q(long j2) {
        t = j2;
        x.a("logempkd", (Object) Long.valueOf(j2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    private static void r(long j2) {
        u = j2;
        x.a("logimj", (Object) Long.valueOf(j2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    private static void s(long j2) {
        v = j2;
        x.a("logpipo", (Object) Long.valueOf(j2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    private static void t(long j2) {
        w = j2;
        x.a("logoth", (Object) Long.valueOf(j2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    private static void u(long j2) {
        c = j2;
        x.a("imaged", (Object) Long.valueOf(j2));
        if (c >= 1048576 && c - p >= 1048576) {
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("a", ((double) Math.round(((((double) c) / 1024.0d) / 1024.0d) * 1000.0d)) / 1000.0d);
                jSONObject.put("t", ((double) Math.round(((((double) f3073a) / 1024.0d) / 1024.0d) * 1000.0d)) / 1000.0d);
                new k("T", "T04", jSONObject).e();
                m(c);
            } catch (JSONException e2) {
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    private static void v(long j2) {
        h = j2;
        x.a("im", (Object) Long.valueOf(j2));
        if (j2 >= 1048576 && j2 - u >= 1048576) {
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("a", ((double) Math.round(((((double) j2) / 1024.0d) / 1024.0d) * 1000.0d)) / 1000.0d);
                jSONObject.put("t", ((double) Math.round(((((double) f3073a) / 1024.0d) / 1024.0d) * 1000.0d)) / 1000.0d);
                new k("T", "T02", jSONObject).e();
                r(j2);
            } catch (JSONException e2) {
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    private static void w(long j2) {
        b = j2;
        x.a("audiod", (Object) Long.valueOf(j2));
        if (b >= 1048576 && b - o >= 1048576) {
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("a", ((double) Math.round(((((double) b) / 1024.0d) / 1024.0d) * 1000.0d)) / 1000.0d);
                jSONObject.put("t", ((double) Math.round(((((double) f3073a) / 1024.0d) / 1024.0d) * 1000.0d)) / 1000.0d);
                new k("T", "T06", jSONObject).e();
                l(j2);
            } catch (JSONException e2) {
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    private static void x(long j2) {
        d = j2;
        x.a("imageu", (Object) Long.valueOf(e));
        if (j2 >= 1048576 && j2 - q >= 1048576) {
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("a", ((double) Math.round(((((double) j2) / 1024.0d) / 1024.0d) * 1000.0d)) / 1000.0d);
                jSONObject.put("t", ((double) Math.round(((((double) f3073a) / 1024.0d) / 1024.0d) * 1000.0d)) / 1000.0d);
                new k("T", "T05", jSONObject).e();
                n(j2);
            } catch (JSONException e2) {
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    private static void y(long j2) {
        e = j2;
        x.a("audiou", (Object) Long.valueOf(j2));
        if (j2 >= 1048576 && j2 - r >= 1048576) {
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("a", ((double) Math.round(((((double) j2) / 1024.0d) / 1024.0d) * 1000.0d)) / 1000.0d);
                jSONObject.put("t", ((double) Math.round(((((double) f3073a) / 1024.0d) / 1024.0d) * 1000.0d)) / 1000.0d);
                new k("T", "T07", jSONObject).e();
                o(j2);
            } catch (JSONException e2) {
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.immomo.momo.util.ak.a(android.content.Context, java.lang.String):com.immomo.momo.util.ak
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Integer):int
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Long):long
      com.immomo.momo.util.ak.a(java.lang.String, int):void
      com.immomo.momo.util.ak.a(java.lang.String, long):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.String):void
      com.immomo.momo.util.ak.a(java.lang.String, boolean):void
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Boolean):boolean
      com.immomo.momo.util.ak.a(java.lang.String, java.lang.Object):void */
    private static void z(long j2) {
        f = j2;
        x.a("emotiond", (Object) Long.valueOf(j2));
        if (j2 >= 1048576 && j2 - s >= 1048576) {
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("a", ((double) Math.round(((((double) j2) / 1024.0d) / 1024.0d) * 1000.0d)) / 1000.0d);
                jSONObject.put("t", ((double) Math.round(((((double) f3073a) / 1024.0d) / 1024.0d) * 1000.0d)) / 1000.0d);
                new k("T", "T041", jSONObject).e();
                p(j2);
            } catch (JSONException e2) {
            }
        }
    }
}
