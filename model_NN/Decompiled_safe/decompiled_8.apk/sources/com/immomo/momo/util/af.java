package com.immomo.momo.util;

import org.json.JSONException;
import org.json.JSONObject;

final class af implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ w f3065a;

    af(w wVar) {
        this.f3065a = wVar;
    }

    public final void run() {
        w.g(this.f3065a);
        JSONObject jSONObject = new JSONObject();
        int length = w.j.length;
        for (int i = 0; i < length && this.f3065a.c; i++) {
            try {
                this.f3065a.b.a((Object) ("get ip " + w.j[i] + "..."));
                jSONObject.put(w.j[i], this.f3065a.c(w.j[i]));
            } catch (JSONException e) {
                this.f3065a.b.a((Throwable) e);
            }
        }
        try {
            w.f.put("dns_st", jSONObject);
        } catch (JSONException e2) {
            this.f3065a.b.a((Throwable) e2);
        }
    }
}
