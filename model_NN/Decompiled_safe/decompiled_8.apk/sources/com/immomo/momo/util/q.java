package com.immomo.momo.util;

import com.immomo.momo.protocol.a.c;
import java.io.File;

final class q implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ o f3095a;
    private final /* synthetic */ File b;

    q(o oVar, File file) {
        this.f3095a = oVar;
        this.b = file;
    }

    public final void run() {
        try {
            String[] split = this.b.getName().split("_");
            String str = null;
            if (split.length > 1) {
                str = split[1];
            }
            c.a().b(this.b, str);
            this.b.delete();
        } catch (Exception e) {
            this.f3095a.i.a((Throwable) e);
        }
    }
}
