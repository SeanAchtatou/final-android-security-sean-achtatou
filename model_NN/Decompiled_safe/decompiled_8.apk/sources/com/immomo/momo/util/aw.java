package com.immomo.momo.util;

import android.support.v4.b.a;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.android.activity.fs;
import com.immomo.momo.android.c.g;
import com.immomo.momo.android.c.o;
import com.immomo.momo.android.c.u;
import com.immomo.momo.service.bean.bh;

public final class aw {
    private static aw b;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public m f3078a = new m("test_momo", "[ --- WallpaperHelper --- ] \n");
    private u c = new u(2, 2);

    private aw() {
    }

    public static aw a() {
        if (b == null) {
            b = new aw();
        }
        return b;
    }

    public static boolean a(bh bhVar) {
        if (!a.f(bhVar.f3013a)) {
            return false;
        }
        return h.a(bhVar.a(), 2).exists();
    }

    public final void a(bh bhVar, fs fsVar, g gVar) {
        if (!bhVar.isImageLoading()) {
            bhVar.setImageLoading(true);
            o oVar = new o(String.valueOf(bhVar.a()) + "_temp", new ax(this, gVar, bhVar), 2, fsVar);
            oVar.a(bhVar.getLoadImageId().replace("_preview", PoiTypeDef.All));
            this.c.execute(oVar);
        }
    }
}
