package com.immomo.momo.util;

import com.jcraft.jzlib.GZIPHeader;
import com.mapabc.minimap.map.vmap.NativeMapEngine;
import java.io.ByteArrayOutputStream;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private static char[] f3081a = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};
    private static byte[] b;

    static {
        byte[] bArr = new byte[NativeMapEngine.MAX_ICON_SIZE];
        bArr[0] = -1;
        bArr[1] = -1;
        bArr[2] = -1;
        bArr[3] = -1;
        bArr[4] = -1;
        bArr[5] = -1;
        bArr[6] = -1;
        bArr[7] = -1;
        bArr[8] = -1;
        bArr[9] = -1;
        bArr[10] = -1;
        bArr[11] = -1;
        bArr[12] = -1;
        bArr[13] = -1;
        bArr[14] = -1;
        bArr[15] = -1;
        bArr[16] = -1;
        bArr[17] = -1;
        bArr[18] = -1;
        bArr[19] = -1;
        bArr[20] = -1;
        bArr[21] = -1;
        bArr[22] = -1;
        bArr[23] = -1;
        bArr[24] = -1;
        bArr[25] = -1;
        bArr[26] = -1;
        bArr[27] = -1;
        bArr[28] = -1;
        bArr[29] = -1;
        bArr[30] = -1;
        bArr[31] = -1;
        bArr[32] = -1;
        bArr[33] = -1;
        bArr[34] = -1;
        bArr[35] = -1;
        bArr[36] = -1;
        bArr[37] = -1;
        bArr[38] = -1;
        bArr[39] = -1;
        bArr[40] = -1;
        bArr[41] = -1;
        bArr[42] = -1;
        bArr[43] = 62;
        bArr[44] = -1;
        bArr[45] = -1;
        bArr[46] = -1;
        bArr[47] = 63;
        bArr[48] = 52;
        bArr[49] = 53;
        bArr[50] = 54;
        bArr[51] = 55;
        bArr[52] = 56;
        bArr[53] = 57;
        bArr[54] = 58;
        bArr[55] = 59;
        bArr[56] = 60;
        bArr[57] = 61;
        bArr[58] = -1;
        bArr[59] = -1;
        bArr[60] = -1;
        bArr[61] = -1;
        bArr[62] = -1;
        bArr[63] = -1;
        bArr[64] = -1;
        bArr[66] = 1;
        bArr[67] = 2;
        bArr[68] = 3;
        bArr[69] = 4;
        bArr[70] = 5;
        bArr[71] = 6;
        bArr[72] = 7;
        bArr[73] = 8;
        bArr[74] = 9;
        bArr[75] = 10;
        bArr[76] = GZIPHeader.OS_WIN32;
        bArr[77] = GZIPHeader.OS_QDOS;
        bArr[78] = GZIPHeader.OS_RISCOS;
        bArr[79] = 14;
        bArr[80] = 15;
        bArr[81] = 16;
        bArr[82] = 17;
        bArr[83] = 18;
        bArr[84] = 19;
        bArr[85] = 20;
        bArr[86] = 21;
        bArr[87] = 22;
        bArr[88] = 23;
        bArr[89] = 24;
        bArr[90] = 25;
        bArr[91] = -1;
        bArr[92] = -1;
        bArr[93] = -1;
        bArr[94] = -1;
        bArr[95] = -1;
        bArr[96] = -1;
        bArr[97] = 26;
        bArr[98] = 27;
        bArr[99] = 28;
        bArr[100] = 29;
        bArr[101] = 30;
        bArr[102] = 31;
        bArr[103] = 32;
        bArr[104] = 33;
        bArr[105] = 34;
        bArr[106] = 35;
        bArr[107] = 36;
        bArr[108] = 37;
        bArr[109] = 38;
        bArr[110] = 39;
        bArr[111] = 40;
        bArr[112] = 41;
        bArr[113] = 42;
        bArr[114] = 43;
        bArr[115] = 44;
        bArr[116] = 45;
        bArr[117] = 46;
        bArr[118] = 47;
        bArr[119] = 48;
        bArr[120] = 49;
        bArr[121] = 50;
        bArr[122] = 51;
        bArr[123] = -1;
        bArr[124] = -1;
        bArr[125] = -1;
        bArr[126] = -1;
        bArr[127] = -1;
        b = bArr;
    }

    public static String a(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        int length = bArr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                break;
            }
            int i2 = i + 1;
            byte b2 = bArr[i] & GZIPHeader.OS_UNKNOWN;
            if (i2 == length) {
                stringBuffer.append(f3081a[b2 >>> 2]);
                stringBuffer.append(f3081a[(b2 & 3) << 4]);
                stringBuffer.append("==");
                break;
            }
            int i3 = i2 + 1;
            byte b3 = bArr[i2] & GZIPHeader.OS_UNKNOWN;
            if (i3 == length) {
                stringBuffer.append(f3081a[b2 >>> 2]);
                stringBuffer.append(f3081a[((b2 & 3) << 4) | ((b3 & 240) >>> 4)]);
                stringBuffer.append(f3081a[(b3 & 15) << 2]);
                stringBuffer.append("=");
                break;
            }
            i = i3 + 1;
            byte b4 = bArr[i3] & GZIPHeader.OS_UNKNOWN;
            stringBuffer.append(f3081a[b2 >>> 2]);
            stringBuffer.append(f3081a[((b2 & 3) << 4) | ((b3 & 240) >>> 4)]);
            stringBuffer.append(f3081a[((b3 & 15) << 2) | ((b4 & 192) >>> 6)]);
            stringBuffer.append(f3081a[b4 & 63]);
        }
        return stringBuffer.toString();
    }

    public static byte[] b(byte[] bArr) {
        byte b2;
        int i;
        byte b3;
        int i2;
        byte b4;
        int i3;
        int i4;
        int i5;
        byte b5 = 0;
        int length = bArr.length;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(length);
        byte b6 = 0;
        byte b7 = 0;
        byte b8 = 0;
        int i6 = 0;
        while (i6 < length) {
            while (true) {
                if (i6 >= bArr.length) {
                    b2 = b8;
                    i = i6;
                    break;
                }
                int i7 = i6 + 1;
                b8 = b[bArr[i6]];
                if (i7 >= length || b8 != -1) {
                    b2 = b8;
                    i = i7;
                } else {
                    i6 = i7;
                }
            }
            if (b2 == -1) {
                break;
            }
            int i8 = i;
            byte b9 = b7;
            while (true) {
                if (i8 >= bArr.length) {
                    b3 = b9;
                    i2 = i8;
                    break;
                }
                int i9 = i8 + 1;
                b9 = b[bArr[i8]];
                if (i9 >= length || b9 != -1) {
                    b3 = b9;
                    i2 = i9;
                } else {
                    i8 = i9;
                }
            }
            if (b3 == -1) {
                break;
            }
            byteArrayOutputStream.write((b2 << 2) | ((b3 & 48) >>> 4));
            int i10 = i2;
            byte b10 = b6;
            while (true) {
                if (i10 >= bArr.length) {
                    b4 = b10;
                    i3 = i10;
                    break;
                }
                i5 = i10 + 1;
                byte b11 = bArr[i10];
                if (b11 == 61) {
                    return byteArrayOutputStream.toByteArray();
                }
                b10 = b[b11];
                if (i5 >= length || b10 != -1) {
                    b4 = b10;
                    i3 = i5;
                } else {
                    i10 = i5;
                }
            }
            b4 = b10;
            i3 = i5;
            if (b4 == -1) {
                break;
            }
            byteArrayOutputStream.write(((b3 & 15) << 4) | ((b4 & 60) >>> 2));
            int i11 = i3;
            byte b12 = b5;
            while (true) {
                if (i11 >= bArr.length) {
                    i4 = i11;
                    break;
                }
                i4 = i11 + 1;
                byte b13 = bArr[i11];
                if (b13 != 61) {
                    b12 = b[b13];
                    if (i4 >= length || b12 != -1) {
                        break;
                    }
                    i11 = i4;
                } else {
                    return byteArrayOutputStream.toByteArray();
                }
            }
            if (b12 == -1) {
                break;
            }
            byteArrayOutputStream.write(((b4 & 3) << 6) | b12);
            b5 = b12;
            b6 = b4;
            i6 = i4;
            b8 = b2;
            b7 = b3;
        }
        return byteArrayOutputStream.toByteArray();
    }
}
