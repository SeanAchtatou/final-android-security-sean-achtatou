package com.immomo.momo.util.jni;

import android.app.Activity;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.util.m;
import java.io.UnsupportedEncodingException;

public class Codec {

    /* renamed from: a  reason: collision with root package name */
    private static String f3090a = null;
    private static String b = null;

    static {
        System.loadLibrary("momonative");
        new m("Codec");
    }

    public static native String Dse();

    public static String a() {
        if (b == null) {
            b = mccc();
        }
        return b;
    }

    public static String a(String str) {
        return dfgjk(str);
    }

    public static byte[] a(byte[] bArr, String str) {
        return aec(bArr, bArr.length, Integer.valueOf(str).intValue());
    }

    public static native String aaa();

    public static native String acct();

    private static native byte[] aec(byte[] bArr, int i, int i2);

    public static String b() {
        if (f3090a == null) {
            f3090a = acct();
        }
        return f3090a;
    }

    public static String b(String str) {
        if (str == null) {
            return str;
        }
        try {
            return str.length() == 0 ? str : new String(mmla(str).getBytes(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return PoiTypeDef.All;
        }
    }

    public static byte[] b(byte[] bArr, String str) {
        return mmacc(bArr, bArr.length, Integer.valueOf(str).intValue());
    }

    public static native String bbb();

    public static String c(String str) {
        if (str == null || str.length() == 0) {
            return str;
        }
        try {
            return cmas(new String(str.getBytes(), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            return PoiTypeDef.All;
        }
    }

    private static native String cmas(String str);

    public static native String coo();

    public static native String dbu();

    public static native String ddd();

    public static native String dde();

    public static native String dfgjk(String str);

    public static native String dm();

    public static native String dma();

    public static native String dmo();

    public static native String dr();

    public static native String ds();

    public static native String du();

    public static native String dwf4();

    public static native String eee();

    public static native String em();

    public static native String etr968ww();

    public static native String fff();

    public static native String ggg();

    public static native byte[] gpi(Activity activity, int i);

    public static native String gvk();

    public static native String hfdwefher();

    public static native int hhh();

    public static native String[] hhhArray();

    public static native String iii();

    public static native String iiou(int i);

    public static native boolean isEmulator();

    public static native String jjj();

    public static native String kkk();

    public static native int kkkxxx(int i, int i2);

    public static native String kwiwek(int i);

    public static native String lll();

    public static native String loiwq(int i);

    public static native String lsn();

    public static native String lwjey(int i);

    public static native String mccc();

    public static native String mm();

    private static native byte[] mmacc(byte[] bArr, int i, int i2);

    private static native String mmla(String str);

    public static native String mmm();

    public static native String njaei(int i);

    public static native String nnn();

    public static native String ooo();

    public static native String opq();

    public static native String oqhyn(int i);

    public static native String owqtwn(int i);

    public static native String ppp();

    public static native String qqq();

    public static native String rrr();

    public static native String rscccc();

    public static native String saa();

    public static native String sss();

    public static native String sssl();

    public static native String uuu();

    public static native String weeryt(String str, String str2);

    public static native String wfer68();

    public static native String wfergfe(int i);

    public static native String xxilss();

    public static native String[] xxxArray();
}
