package com.immomo.momo.util;

import com.immomo.momo.protocol.a.c;
import java.io.File;

final class r implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ o f3096a;
    private final /* synthetic */ File b;
    private final /* synthetic */ String c;

    r(o oVar, File file, String str) {
        this.f3096a = oVar;
        this.b = file;
        this.c = str;
    }

    public final void run() {
        try {
            if (this.b.getName().startsWith("error_")) {
                c.a().a(this.b, this.c);
            } else if (this.b.getName().startsWith("e_")) {
                String[] split = this.b.getName().split("_");
                long parseLong = Long.parseLong(split[1]);
                c.a().a(this.b, this.c, Integer.parseInt(split[2]), parseLong);
            }
            this.b.delete();
        } catch (Exception e) {
            this.f3096a.i.a((Throwable) e);
        }
    }
}
