package com.immomo.momo.util;

import android.content.SharedPreferences;
import android.support.v4.b.a;
import com.immomo.momo.android.c.u;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.c;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;

public final class o implements Runnable {
    private static o l = null;

    /* renamed from: a  reason: collision with root package name */
    private BlockingQueue f3094a;
    private Writer b;
    private File c;
    private File d;
    private File e;
    private boolean f;
    private boolean g;
    private String h;
    /* access modifiers changed from: private */
    public m i;
    private ThreadPoolExecutor j;
    private SharedPreferences k;

    private o() {
        this.f3094a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = false;
        this.g = false;
        this.i = new m(this);
        this.j = null;
        this.k = null;
        this.f3094a = new LinkedBlockingQueue();
        this.j = new u(2, 2);
        this.k = g.c().getSharedPreferences("mmlog", 0);
        new Thread(this).start();
    }

    public static o a() {
        if (l == null) {
            l = new o();
        }
        return l;
    }

    private void a(File file, boolean z) {
        File file2 = new File(f(), z ? String.valueOf(file.getName()) + "_" + this.h : file.getName());
        this.i.a((Object) (String.valueOf(file.getPath()) + " renameTo-> " + file2.getPath()));
        if (!file.renameTo(file2)) {
            a.b(file, file2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    private void c() {
        File e2;
        File file;
        boolean z = true;
        this.f = g.q() != null;
        if (this.f) {
            this.h = g.q().h;
            if (a.a((CharSequence) this.h)) {
                throw new IllegalStateException("userId is null");
            }
            File file2 = new File(com.immomo.momo.a.a(this.h), "mmlog");
            if (!file2.exists()) {
                file2.mkdirs();
            }
            e2 = file2;
        } else {
            e2 = e();
        }
        try {
            File[] listFiles = e2.listFiles(new p());
            if (listFiles == null || listFiles.length <= 0) {
                file = new File(e2, new StringBuilder(String.valueOf(System.currentTimeMillis())).toString());
                file.createNewFile();
                this.i.a((Object) ("file createNewFile," + file.getPath()));
            } else {
                File file3 = listFiles[0];
                try {
                    this.i.a((Object) ("file exist," + file3.getPath() + ", length=" + file3.length()));
                    if (this.f) {
                        if (file3.length() <= 5120) {
                            z = false;
                        }
                        File[] listFiles2 = e().listFiles();
                        if (listFiles2 != null && listFiles2.length > 0) {
                            for (File file4 : listFiles2) {
                                try {
                                    if (file4.isFile()) {
                                        a(file4, false);
                                    }
                                } catch (Exception e3) {
                                    this.i.a((Throwable) e3);
                                }
                            }
                        }
                    } else if (file3.length() <= 512) {
                        z = false;
                    }
                    if (z) {
                        a(file3, this.f);
                        file = new File(e2, new StringBuilder(String.valueOf(System.currentTimeMillis())).toString());
                        try {
                            file.createNewFile();
                            this.i.a((Object) ("file createNewFile," + file.getPath()));
                        } catch (IOException e4) {
                            e = e4;
                        }
                    } else {
                        file = file3;
                    }
                } catch (IOException e5) {
                    e = e5;
                    file = file3;
                }
            }
            this.b = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true)), 1024);
            this.c = file;
            if (Math.abs(System.currentTimeMillis() - this.k.getLong("uploadtime", 0)) > 18000000) {
                d();
            }
        } catch (IOException e6) {
            e = e6;
            file = null;
            this.i.a((Throwable) e);
            if (file != null && file.exists()) {
                file.delete();
            }
        }
    }

    private boolean d() {
        boolean z;
        File[] listFiles = f().listFiles();
        SharedPreferences.Editor edit = this.k.edit();
        edit.putLong("uploadtime", System.currentTimeMillis());
        edit.commit();
        if (listFiles == null || listFiles.length <= 0) {
            z = false;
        } else {
            z = false;
            for (File file : listFiles) {
                if (file.isFile()) {
                    this.j.execute(new q(this, file));
                    z = true;
                }
            }
        }
        File[] listFiles2 = com.immomo.momo.a.p().listFiles();
        this.i.a((Object) ("uploadLogfile.size=" + listFiles2.length));
        if (listFiles2 != null && listFiles2.length > 0) {
            for (File file2 : listFiles2) {
                if (file2.isFile() && (file2.getName().startsWith("e") || file2.getName().startsWith("error_"))) {
                    this.i.a((Object) ("doupload crash file -> " + file2));
                    this.j.execute(new r(this, file2, g.q() != null ? g.q().h : null));
                    z = true;
                }
            }
            c.a();
            c.b();
        }
        return z;
    }

    private File e() {
        if (this.d == null) {
            this.d = new File(g.c().getFilesDir(), "mmlog");
            if (!this.d.exists()) {
                this.d.mkdirs();
            }
        }
        return this.d;
    }

    private File f() {
        if (this.e == null) {
            this.e = new File(e(), "upload");
            if (!this.e.exists()) {
                this.e.mkdirs();
            }
        }
        return this.e;
    }

    private void g() {
        this.f3094a.clear();
        if (this.b != null) {
            try {
                this.b.flush();
            } catch (IOException e2) {
                this.i.a((Throwable) e2);
            }
            a.a(this.b);
            this.c = null;
        }
        c();
    }

    public final void a(k kVar) {
        boolean z = true;
        if (this.g) {
            boolean z2 = g.q() != null;
            if (this.f && z2) {
                if (g.q().h.equals(this.h)) {
                    z = false;
                }
                if (z) {
                    this.i.a((Object) ("change user, id=" + g.q().h + ", oldUserid=" + this.h));
                }
            } else if (z2 != this.f) {
                this.i.a((Object) ("change login status, login=" + z2));
            } else {
                z = false;
            }
            if (z) {
                g();
                this.i.a((Object) ("change end, login=" + this.f + ", userid=" + this.h + ", logfile=" + this.c.getPath()));
            }
        }
        try {
            this.f3094a.put(kVar);
        } catch (InterruptedException e2) {
            this.i.a((Throwable) e2);
        }
    }

    public final void b() {
        if (this.g) {
            try {
                this.b.flush();
                a.a(this.b);
                a(this.c, this.f);
                d();
                g();
            } catch (IOException e2) {
                this.i.a((Throwable) e2);
            }
        }
    }

    public final void run() {
        c();
        this.g = true;
        int i2 = 0;
        while (this.g) {
            try {
                k kVar = (k) this.f3094a.take();
                this.b.write(kVar.a());
                this.b.write(9);
                this.b.write(kVar.b());
                this.b.write(9);
                this.b.write(new StringBuilder(String.valueOf(kVar.c().getTime())).toString());
                if (kVar.d() != null) {
                    this.b.write(9);
                    this.b.write(kVar.d().toString());
                }
                this.b.write(10);
                int i3 = i2 + 1;
                if (i2 >= 40) {
                    this.b.flush();
                    i2 = 0;
                } else {
                    i2 = i3;
                }
                this.i.a(kVar);
            } catch (Exception e2) {
                this.i.a((Throwable) e2);
                return;
            }
        }
    }
}
