package com.immomo.momo.util;

import org.json.JSONException;
import org.json.JSONObject;

final class ah implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ w f3067a;

    ah(w wVar) {
        this.f3067a = wVar;
    }

    public final void run() {
        w.g(this.f3067a);
        this.f3067a.b.a((Object) "ping api.immomo.com...");
        String a2 = this.f3067a.c("api.immomo.com");
        JSONObject jSONObject = new JSONObject();
        if (a2 != null) {
            String str = "pinging " + a2;
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < 3 && this.f3067a.c) {
                    try {
                        jSONObject.put(String.valueOf(str) + " " + i2, String.valueOf(this.f3067a.b(a2)) + "ms");
                    } catch (JSONException e) {
                        this.f3067a.b.a((Throwable) e);
                    }
                    i = i2 + 1;
                }
            }
        }
        try {
            w.f.put("api_st", jSONObject);
        } catch (JSONException e2) {
            this.f3067a.b.a((Throwable) e2);
        }
    }
}
