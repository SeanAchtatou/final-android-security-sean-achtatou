package com.immomo.momo.util.a;

import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.m;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

public final class a {
    static {
        new m("JsonUtil");
    }

    public static String a(Object obj) {
        StringBuilder sb = new StringBuilder();
        if (obj == null) {
            sb.append("\"\"");
        } else if ((obj instanceof String) || (obj instanceof Integer) || (obj instanceof Float) || (obj instanceof Boolean) || (obj instanceof Short) || (obj instanceof Double) || (obj instanceof Long) || (obj instanceof BigDecimal) || (obj instanceof BigInteger) || (obj instanceof Byte)) {
            sb.append("\"").append(a(obj.toString())).append("\"");
        } else if (obj instanceof Object[]) {
            sb.append(a((Object[]) obj));
        } else if (obj instanceof List) {
            sb.append(a((List) obj));
        } else if (obj instanceof Map) {
            sb.append(a((Map) obj));
        } else if (obj instanceof Set) {
            sb.append(a((Set) obj));
        } else if (!(obj instanceof Date)) {
            sb.append(b(obj));
        }
        return sb.toString();
    }

    private static String a(String str) {
        if (str == null) {
            return PoiTypeDef.All;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            switch (charAt) {
                case 8:
                    sb.append("\\b");
                    break;
                case 9:
                    sb.append("\\t");
                    break;
                case 10:
                    sb.append("\\n");
                    break;
                case 12:
                    sb.append("\\f");
                    break;
                case 13:
                    sb.append("\\r");
                    break;
                case '\"':
                    sb.append("\\\"");
                    break;
                case '/':
                    sb.append("\\/");
                    break;
                case '\\':
                    sb.append("\\\\");
                    break;
                default:
                    if (charAt >= 0 && charAt <= 31) {
                        String hexString = Integer.toHexString(charAt);
                        sb.append("\\u");
                        for (int i2 = 0; i2 < 4 - hexString.length(); i2++) {
                            sb.append('0');
                        }
                        sb.append(hexString.toUpperCase());
                        break;
                    } else {
                        sb.append(charAt);
                        break;
                    }
            }
        }
        return sb.toString();
    }

    private static String a(List list) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        if (list == null || list.size() <= 0) {
            sb.append("]");
        } else {
            for (Object a2 : list) {
                sb.append(a(a2));
                sb.append(",");
            }
            sb.setCharAt(sb.length() - 1, ']');
        }
        return sb.toString();
    }

    private static String a(Map map) {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        if (map == null || map.size() <= 0) {
            sb.append("}");
        } else {
            for (Object next : map.keySet()) {
                sb.append(a(next));
                sb.append(":");
                sb.append(a(map.get(next)));
                sb.append(",");
            }
            sb.setCharAt(sb.length() - 1, '}');
        }
        return sb.toString();
    }

    private static String a(Set set) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        if (set == null || set.size() <= 0) {
            sb.append("]");
        } else {
            for (Object a2 : set) {
                sb.append(a(a2));
                sb.append(",");
            }
            sb.setCharAt(sb.length() - 1, ']');
        }
        return sb.toString();
    }

    public static String a(Object[] objArr) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        if (objArr == null || objArr.length <= 0) {
            sb.append("]");
        } else {
            for (Object a2 : objArr) {
                sb.append(a(a2));
                sb.append(",");
            }
            sb.setCharAt(sb.length() - 1, ']');
        }
        return sb.toString();
    }

    private static List a(Class cls) {
        String substring;
        ArrayList arrayList = new ArrayList();
        for (Method name : cls.getMethods()) {
            String name2 = name.getName();
            if (name2.startsWith("set") && (substring = name2.substring(3, name2.length())) != PoiTypeDef.All) {
                arrayList.add(String.valueOf(substring.substring(0, 1).toLowerCase()) + substring.substring(1));
            }
        }
        return arrayList;
    }

    public static void a(String str, bf bfVar) {
        boolean z = true;
        JSONObject jSONObject = null;
        try {
            jSONObject = new JSONObject(str);
        } catch (JSONException e) {
        }
        try {
            bfVar.h = jSONObject.getString("momoid");
        } catch (JSONException e2) {
        }
        try {
            bfVar.G = jSONObject.getString("email");
        } catch (JSONException e3) {
        }
        try {
            bfVar.i = jSONObject.getString("name");
        } catch (JSONException e4) {
        }
        try {
            bfVar.aD = android.support.v4.b.a.a(jSONObject.getLong("follow_time"));
        } catch (JSONException e5) {
        }
        try {
            bfVar.aA = android.support.v4.b.a.a(jSONObject.getLong("follower_chgtime"));
        } catch (JSONException e6) {
        }
        try {
            bfVar.aB = android.support.v4.b.a.a(jSONObject.getLong("following_chgtime"));
        } catch (JSONException e7) {
        }
        try {
            bfVar.aC = android.support.v4.b.a.a(jSONObject.getLong("lcftime"));
        } catch (JSONException e8) {
        }
        try {
            bfVar.a(jSONObject.getLong("loc_timesec"));
        } catch (Exception e9) {
        }
        try {
            bfVar.ax = android.support.v4.b.a.a(jSONObject.getLong("updateTime"));
        } catch (JSONException e10) {
        }
        try {
            bfVar.H = jSONObject.getString("sex");
        } catch (JSONException e11) {
        }
        try {
            bfVar.I = jSONObject.getInt("age");
        } catch (JSONException e12) {
        }
        try {
            bfVar.B = jSONObject.getInt("regtype");
        } catch (JSONException e13) {
        }
        try {
            bfVar.ay = android.support.v4.b.a.a(jSONObject.getLong("regtime"));
        } catch (JSONException e14) {
        }
        try {
            bfVar.J = jSONObject.getString("birthday");
        } catch (JSONException e15) {
        }
        try {
            bfVar.b(jSONObject.getString("sign"));
        } catch (JSONException e16) {
        }
        try {
            bfVar.az = android.support.v4.b.a.a(jSONObject.getLong("sign_time"));
        } catch (JSONException e17) {
        }
        try {
            bfVar.ae = android.support.v4.b.a.b(jSONObject.getString("photos"), ",");
        } catch (JSONException e18) {
        }
        try {
            bfVar.P = jSONObject.getString("relation");
        } catch (JSONException e19) {
        }
        try {
            bfVar.K = jSONObject.getString("constellation");
        } catch (JSONException e20) {
        }
        try {
            bfVar.L = jSONObject.getString("interest");
        } catch (JSONException e21) {
        }
        try {
            bfVar.N = jSONObject.getString("job");
        } catch (JSONException e22) {
        }
        try {
            bfVar.D = jSONObject.getString("website");
        } catch (JSONException e23) {
        }
        try {
            bfVar.X = (long) jSONObject.getInt("version");
        } catch (JSONException e24) {
        }
        try {
            bfVar.C = jSONObject.getString("company");
        } catch (JSONException e25) {
        }
        try {
            bfVar.Q = jSONObject.getString("school");
        } catch (JSONException e26) {
        }
        try {
            bfVar.E = jSONObject.getString("aboutme");
        } catch (JSONException e27) {
        }
        try {
            bfVar.F = jSONObject.getString("hangout");
        } catch (JSONException e28) {
        }
        try {
            bfVar.v = jSONObject.getInt("newfollowercount");
        } catch (JSONException e29) {
        }
        try {
            bfVar.w = jSONObject.getInt("followercount");
        } catch (JSONException e30) {
        }
        try {
            bfVar.x = jSONObject.getInt("followingcount");
        } catch (JSONException e31) {
        }
        try {
            bfVar.aw = jSONObject.getInt("isBindEmail") == 1;
        } catch (JSONException e32) {
        }
        try {
            bfVar.an = jSONObject.getInt("isBindSinaWeibo") == 1;
        } catch (JSONException e33) {
        }
        try {
            bfVar.ap = jSONObject.getInt("isSinaWeiboVip") == 1;
        } catch (JSONException e34) {
        }
        try {
            bfVar.ab = jSONObject.getString("client");
        } catch (JSONException e35) {
        }
        try {
            bfVar.ar = jSONObject.getInt("isBindRenren") == 1;
        } catch (Exception e36) {
        }
        try {
            jSONObject.getInt("isBindDouban");
        } catch (Exception e37) {
        }
        try {
            if (jSONObject.getInt("isBaned") != 1) {
                z = false;
            }
            bfVar.ac = z;
        } catch (Exception e38) {
        }
    }

    public static void a(String str, Object obj) {
        int i = 0;
        JSONObject jSONObject = new JSONObject(str);
        Class<?> cls = obj.getClass();
        List a2 = a((Class) cls);
        if (a2 != null) {
            while (true) {
                int i2 = i;
                if (i2 < a2.size()) {
                    try {
                        String str2 = (String) a2.get(i2);
                        cls.getMethod("set" + str2.substring(0, 1).toUpperCase() + str2.substring(1), Object.class).invoke(obj, jSONObject.get(str2));
                    } catch (Exception e) {
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public static String b(Object obj) {
        int i = 0;
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        List a2 = a((Class) obj.getClass());
        Class<?> cls = obj.getClass();
        if (a2 != null) {
            while (true) {
                int i2 = i;
                if (i2 >= a2.size()) {
                    break;
                }
                try {
                    String str = (String) a2.get(i2);
                    String a3 = a((Object) str);
                    String a4 = a(cls.getMethod("get" + str.substring(0, 1).toUpperCase() + str.substring(1), new Class[0]).invoke(obj, new Object[0]));
                    sb.append(a3);
                    sb.append(":");
                    sb.append(a4);
                    sb.append(",");
                } catch (Exception e) {
                }
                i = i2 + 1;
            }
            sb.setCharAt(sb.length() - 1, '}');
        } else {
            sb.append("}");
        }
        return sb.toString();
    }
}
