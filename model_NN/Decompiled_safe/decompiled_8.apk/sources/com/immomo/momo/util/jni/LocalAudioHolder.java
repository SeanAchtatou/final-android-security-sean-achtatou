package com.immomo.momo.util.jni;

public class LocalAudioHolder {
    static {
        System.loadLibrary("nativeaudio");
    }

    public static native int decodeAMR2WAV(String str, String str2);

    public static native int encodeWAV2AMR(String str, String str2, int i, int i2);
}
