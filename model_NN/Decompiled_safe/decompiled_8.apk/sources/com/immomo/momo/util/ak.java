package com.immomo.momo.util;

import android.content.Context;
import android.content.SharedPreferences;

public final class ak {
    private static ak b = null;

    /* renamed from: a  reason: collision with root package name */
    private SharedPreferences f3069a = null;
    private String c;

    private ak(Context context, String str) {
        this.f3069a = context.getSharedPreferences(str, 0);
        this.c = str;
    }

    public static ak a(Context context, String str) {
        if (b == null || !str.equals(b.c)) {
            b = new ak(context, str);
        }
        return b;
    }

    public final int a(String str, Integer num) {
        return this.f3069a.getInt(str, num.intValue());
    }

    public final long a(String str, Long l) {
        try {
            return this.f3069a.getLong(str, l.longValue());
        } catch (Exception e) {
            return l.longValue();
        }
    }

    public final SharedPreferences a() {
        return this.f3069a;
    }

    public final void a(String str, int i) {
        SharedPreferences.Editor edit = this.f3069a.edit();
        edit.putInt(str, i);
        edit.commit();
    }

    public final void a(String str, long j) {
        SharedPreferences.Editor edit = this.f3069a.edit();
        edit.putLong(str, j);
        edit.commit();
    }

    public final void a(String str, Object obj) {
        if (obj instanceof Long) {
            a(str, ((Long) obj).longValue());
        } else if (obj instanceof Float) {
            float floatValue = ((Float) obj).floatValue();
            SharedPreferences.Editor edit = this.f3069a.edit();
            edit.putFloat(str, floatValue);
            edit.commit();
        } else if (obj instanceof String) {
            a(str, (String) obj);
        } else if (obj instanceof Integer) {
            a(str, ((Integer) obj).intValue());
        } else if (obj instanceof Boolean) {
            a(str, ((Boolean) obj).booleanValue());
        } else {
            a(str, obj.toString());
        }
    }

    public final void a(String str, String str2) {
        SharedPreferences.Editor edit = this.f3069a.edit();
        edit.putString(str, str2);
        edit.commit();
    }

    public final void a(String str, boolean z) {
        SharedPreferences.Editor edit = this.f3069a.edit();
        edit.putBoolean(str, z);
        edit.commit();
    }

    public final boolean a(String str) {
        return this.f3069a.contains(str);
    }

    public final boolean a(String str, Boolean bool) {
        return this.f3069a.getBoolean(str, bool.booleanValue());
    }

    public final Object b(String str, Object obj) {
        try {
            if (obj instanceof Long) {
                return Long.valueOf(a(str, (Long) obj));
            }
            if (obj instanceof Float) {
                return Float.valueOf(this.f3069a.getFloat(str, ((Float) obj).floatValue()));
            }
            if (obj instanceof String) {
                return b(str, (String) obj);
            }
            if (obj instanceof Integer) {
                return Integer.valueOf(a(str, (Integer) obj));
            }
            if (obj instanceof Boolean) {
                return Boolean.valueOf(a(str, (Boolean) obj));
            }
            return null;
        } catch (Exception e) {
            return obj;
        }
    }

    public final String b(String str, String str2) {
        return this.f3069a.getString(str, str2);
    }

    public final void b() {
        SharedPreferences.Editor edit = this.f3069a.edit();
        edit.clear();
        edit.commit();
    }

    public final void b(String str) {
        SharedPreferences.Editor edit = this.f3069a.edit();
        edit.remove(str);
        edit.commit();
    }
}
