package com.immomo.momo.util;

import java.io.File;
import java.io.FileFilter;

final class p implements FileFilter {
    p() {
    }

    public final boolean accept(File file) {
        return file.isFile();
    }
}
