package com.immomo.momo.util.jni;

public class ImageFilter {
    static {
        System.loadLibrary("imagefilter");
    }

    public static native int[] native_ImgOldPhoto(int[] iArr, int i, int i2);

    public static native int[] native_ImgSoftBY(int[] iArr, int i, int i2);

    public static native int[] native_ImgToGray(int[] iArr, int i, int i2);
}
