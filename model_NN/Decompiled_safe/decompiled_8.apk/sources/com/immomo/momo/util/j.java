package com.immomo.momo.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.c.ag;
import com.immomo.momo.android.c.o;
import com.immomo.momo.android.c.p;
import com.immomo.momo.android.c.r;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.android.view.az;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.aj;
import java.io.File;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;

public final class j {

    /* renamed from: a  reason: collision with root package name */
    public static HashMap f3089a = new HashMap();
    public static final Map b = new HashMap();
    private static Map c = new HashMap();

    public static Bitmap a(File file) {
        if (file.exists()) {
            return BitmapFactory.decodeFile(file.getAbsolutePath());
        }
        return null;
    }

    public static Bitmap a(String str) {
        SoftReference softReference = (SoftReference) c.get(str);
        Bitmap bitmap = softReference == null ? null : (Bitmap) softReference.get();
        if (bitmap == null || !bitmap.isRecycled()) {
            return bitmap;
        }
        c.remove(bitmap);
        return null;
    }

    private static o a(aj ajVar, int i, View view, ViewGroup viewGroup, boolean z, int i2, boolean z2) {
        p pVar;
        if (viewGroup != null) {
            if (((viewGroup instanceof HandyListView) && ((HandyListView) viewGroup).g()) || ((viewGroup instanceof az) && ((az) viewGroup).c())) {
                return null;
            }
        }
        String loadImageId = ajVar.getLoadImageId();
        if (!ajVar.isImageLoading() || (pVar = (p) ajVar.getImageCallback()) == null || ajVar.isImageMultipleDiaplay()) {
            if (z && i2 <= 0) {
                i2 = g.a(4.0f);
            }
            p pVar2 = new p(ajVar, view, z, i2);
            pVar2.a(z2);
            ajVar.setImageLoading(true);
            ajVar.setImageCallback(pVar2);
            Bitmap a2 = a(loadImageId);
            if (!z || a2 == null) {
                o oVar = (o) f3089a.get(loadImageId);
                if (oVar == null || oVar.d()) {
                    r rVar = new r(ajVar.isImageUrl() ? a.n(loadImageId) : loadImageId, pVar2, i, null);
                    if (ajVar.isImageUrl()) {
                        rVar.a(ajVar.getLoadImageId());
                    }
                    f3089a.put(loadImageId, rVar);
                    ag.a(rVar);
                    return rVar;
                }
                oVar.a((com.immomo.momo.android.c.g) pVar2);
                return null;
            }
            pVar2.a(a2);
            return null;
        }
        pVar.a(view);
        return null;
    }

    public static void a(ImageView imageView, boolean z, boolean z2, int i) {
        Bitmap t;
        if (z) {
            imageView.setImageBitmap(null);
            return;
        }
        if (z2) {
            try {
                t = a("defaultphoto_r");
                if (t == null) {
                    t = a.a(g.t(), (float) i);
                    a("defaultphoto_r", t);
                }
            } catch (Throwable th) {
                th.printStackTrace();
                return;
            }
        } else {
            t = g.t();
        }
        imageView.setImageBitmap(t);
    }

    public static void a(aj ajVar, ImageView imageView, int i) {
        a(ajVar, imageView, null, i, false, true, 0, true);
    }

    public static void a(aj ajVar, ImageView imageView, int i, boolean z, boolean z2) {
        a(ajVar, imageView, null, i, z, false, -1, z2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    public static void a(aj ajVar, ImageView imageView, ViewGroup viewGroup) {
        a(ajVar, imageView, viewGroup, 3, false, true, -1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    public static void a(aj ajVar, ImageView imageView, ViewGroup viewGroup, int i) {
        a(ajVar, imageView, viewGroup, i, false, false, -1);
    }

    public static void a(aj ajVar, ImageView imageView, ViewGroup viewGroup, int i, boolean z) {
        a(ajVar, imageView, viewGroup, i, false, z, 0, true);
    }

    public static void a(aj ajVar, ImageView imageView, ViewGroup viewGroup, int i, boolean z, boolean z2, int i2) {
        a(ajVar, imageView, viewGroup, i, z, z2, i2, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
     arg types: [com.immomo.momo.service.bean.aj, int, android.widget.ImageView, android.view.ViewGroup, boolean, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o */
    public static void a(aj ajVar, ImageView imageView, ViewGroup viewGroup, int i, boolean z, boolean z2, int i2, boolean z3) {
        if (imageView != null) {
            if (ajVar == null || a.a((CharSequence) ajVar.getLoadImageId())) {
                imageView.setTag(R.id.tag_item_imageid, PoiTypeDef.All);
                if (z3) {
                    a(imageView, z, z2, i2);
                    return;
                }
                return;
            }
            int a2 = (!z2 || i2 > 0) ? i2 : g.a(4.0f);
            String loadImageId = ajVar.getLoadImageId();
            String str = z2 ? String.valueOf(loadImageId) + "_r" + a2 : loadImageId;
            imageView.setTag(R.id.tag_item_imageid, loadImageId);
            Bitmap a3 = a(str);
            if (a3 == null) {
                a(ajVar, i, (View) imageView, viewGroup, z2, a2, false);
            }
            if (a3 != null) {
                imageView.setImageBitmap(a3);
            } else if (z3) {
                a(imageView, z, z2, a2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(android.widget.ImageView, boolean, boolean, int):void
     arg types: [android.widget.ImageView, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int):void
      com.immomo.momo.util.j.a(android.widget.ImageView, boolean, boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
     arg types: [com.immomo.momo.service.bean.aj, int, android.widget.ImageView, com.immomo.momo.android.view.HandyListView, int, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o */
    public static void a(aj ajVar, ImageView imageView, HandyListView handyListView) {
        if (imageView != null) {
            int a2 = g.a(8.0f);
            if (ajVar == null || a.a((CharSequence) ajVar.getLoadImageId())) {
                imageView.setTag(R.id.tag_item_imageid, PoiTypeDef.All);
                a(imageView, false, true, a2);
                return;
            }
            String loadImageId = ajVar.getLoadImageId();
            imageView.setTag(R.id.tag_item_imageid, loadImageId);
            Bitmap bitmap = (Bitmap) b.get(loadImageId);
            if (bitmap == null) {
                a(ajVar, 3, (View) imageView, (ViewGroup) handyListView, true, a2, true);
            }
            if (bitmap != null && !bitmap.isRecycled()) {
                imageView.setImageBitmap(bitmap);
            } else if (b.get("defaultphoto") != null) {
                imageView.setImageBitmap((Bitmap) b.get("defaultphoto"));
            } else {
                try {
                    Bitmap a3 = a.a(g.t(), (float) a2);
                    Bitmap a4 = a.a(a3, g.a(36.0f), g.a(36.0f));
                    imageView.setImageBitmap(a4);
                    b.put("defaultphoto", a4);
                    a("defaultphoto_r", a3);
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }
        }
    }

    public static void a(String str, Bitmap bitmap) {
        if (bitmap != null) {
            Bitmap a2 = a(str);
            if (a2 == null || a2 != bitmap) {
                c.put(str, new SoftReference(bitmap));
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    public static void b(aj ajVar, ImageView imageView, ViewGroup viewGroup, int i, boolean z) {
        a(ajVar, imageView, viewGroup, i, z, false, -1);
    }
}
