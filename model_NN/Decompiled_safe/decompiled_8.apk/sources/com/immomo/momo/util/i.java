package com.immomo.momo.util;

import android.graphics.Bitmap;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;

public final class i {

    /* renamed from: a  reason: collision with root package name */
    private static Map f3088a = new HashMap();

    static {
        new m("LoadFilterUtil");
    }

    public static Bitmap a(String str) {
        Bitmap bitmap;
        synchronized (f3088a) {
            bitmap = f3088a.get(str) == null ? null : (Bitmap) ((SoftReference) f3088a.get(str)).get();
            if (bitmap != null && bitmap.isRecycled()) {
                f3088a.put(str, null);
                bitmap = null;
            }
        }
        return bitmap;
    }

    public static void a() {
        f3088a.clear();
    }

    public static void a(Bitmap bitmap) {
        if (bitmap != null && !bitmap.isRecycled()) {
            bitmap.recycle();
        }
    }

    public static void a(String str, Bitmap bitmap) {
        if (bitmap != null) {
            synchronized (f3088a) {
                f3088a.put(str, new SoftReference(bitmap));
            }
        }
    }
}
