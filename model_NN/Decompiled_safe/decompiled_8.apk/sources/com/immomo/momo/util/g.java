package com.immomo.momo.util;

import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.b.a;
import com.amap.mapapi.location.LocationManagerProxy;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

public final class g {

    /* renamed from: a  reason: collision with root package name */
    private static Map f3086a = new HashMap();

    @SuppressLint({"NewApi"})
    public static boolean a(Context context, String str, String str2, String str3) {
        Uri parse = Uri.parse(str);
        if (!"http".equalsIgnoreCase(parse.getScheme()) && !"ftp".equalsIgnoreCase(parse.getScheme())) {
            return false;
        }
        if (Build.VERSION.SDK_INT >= 9) {
            try {
                DownloadManager downloadManager = (DownloadManager) context.getSystemService("download");
                if (f3086a.containsKey(str)) {
                    DownloadManager.Query query = new DownloadManager.Query();
                    query.setFilterById(((Long) f3086a.get(str)).longValue());
                    query.setFilterByStatus(15);
                    Cursor query2 = downloadManager.query(query);
                    if (query2 != null) {
                        if (query2.moveToNext()) {
                            if (query2.getInt(query2.getColumnIndex(LocationManagerProxy.KEY_STATUS_CHANGED)) == 8) {
                                File file = new File(query2.getString(query2.getColumnIndex("local_filename")));
                                if (file.exists()) {
                                    Intent intent = new Intent();
                                    intent.addFlags(268435456);
                                    intent.setAction("android.intent.action.VIEW");
                                    intent.setDataAndType(Uri.fromFile(file), str3);
                                    context.startActivity(intent);
                                }
                            } else {
                                ao.b("已在下载中");
                            }
                            query2.close();
                            return true;
                        }
                        query2.close();
                    }
                }
                DownloadManager.Request request = new DownloadManager.Request(parse);
                if (a.a((CharSequence) str2)) {
                    str2 = parse.getLastPathSegment();
                }
                request.setTitle(str2);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, String.valueOf(str2) + System.currentTimeMillis());
                request.setMimeType(str3);
                f3086a.put(str, Long.valueOf(downloadManager.enqueue(request)));
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            Intent intent2 = new Intent("android.intent.action.VIEW", parse);
            intent2.addFlags(268435456);
            context.startActivity(intent2);
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }
}
