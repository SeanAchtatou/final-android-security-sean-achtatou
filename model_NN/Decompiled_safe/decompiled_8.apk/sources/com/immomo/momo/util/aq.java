package com.immomo.momo.util;

import android.text.Editable;
import android.text.TextWatcher;

public final class aq implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    private final int f3072a;
    private boolean b = true;
    private m c = new m(this);

    public aq(int i) {
        this.f3072a = i;
    }

    public final aq a() {
        return this;
    }

    public final void afterTextChanged(Editable editable) {
        if (this.b) {
            try {
                this.b = false;
                while (editable.toString().getBytes("GBK").length > this.f3072a) {
                    int length = editable.length() - 1;
                    editable.delete(length, length + 1);
                }
                this.b = true;
            } catch (Exception e) {
                this.c.a((Throwable) e);
            }
        }
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
