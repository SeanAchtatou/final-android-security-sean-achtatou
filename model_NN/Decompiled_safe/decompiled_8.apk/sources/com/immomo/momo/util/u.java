package com.immomo.momo.util;

import com.immomo.momo.service.bean.aj;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public final class u {

    /* renamed from: a  reason: collision with root package name */
    private static final Map f3098a = new HashMap();

    public static void a() {
        f3098a.clear();
    }

    public static void a(String str) {
        f3098a.remove(str);
    }

    public static void a(String str, Object obj) {
        if (obj instanceof Collection) {
            try {
                Collection collection = (Collection) obj.getClass().newInstance();
                collection.addAll((Collection) obj);
                obj = collection;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (obj instanceof Map) {
            try {
                Map map = (Map) obj.getClass().newInstance();
                map.putAll((Map) obj);
                obj = map;
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        } else if (obj instanceof Set) {
            try {
                Set set = (Set) obj.getClass().newInstance();
                set.addAll((Set) obj);
                obj = set;
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
        f3098a.put(str, obj);
    }

    public static Object b(String str) {
        Object obj = f3098a.get(str);
        if (obj == null) {
            return obj;
        }
        if (obj instanceof Collection) {
            try {
                Collection collection = (Collection) obj.getClass().newInstance();
                collection.addAll((Collection) obj);
                try {
                    for (Object next : collection) {
                        if (next instanceof aj) {
                            ((aj) next).setImageLoadFailed(false);
                            ((aj) next).setImageLoading(false);
                            ((aj) next).setImageCallback(null);
                        }
                    }
                    return collection;
                } catch (Exception e) {
                    obj = collection;
                    e = e;
                    e.printStackTrace();
                    return obj;
                }
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                return obj;
            }
        } else if (obj instanceof Map) {
            try {
                Map map = (Map) obj.getClass().newInstance();
                map.putAll((Map) obj);
                return map;
            } catch (Exception e3) {
                e3.printStackTrace();
                return obj;
            }
        } else if (!(obj instanceof Set)) {
            return obj;
        } else {
            try {
                Set set = (Set) obj.getClass().newInstance();
                set.addAll((Set) obj);
                return set;
            } catch (Exception e4) {
                e4.printStackTrace();
                return obj;
            }
        }
    }

    public static boolean c(String str) {
        return f3098a.containsKey(str);
    }
}
