package com.immomo.momo.util;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.Toast;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.g;

public class ao {
    private static Context b = null;
    /* access modifiers changed from: private */
    public static ao c = null;
    private static Handler d = new ap(Looper.getMainLooper());

    /* renamed from: a  reason: collision with root package name */
    protected Toast f3071a = null;

    protected ao() {
        if (b == null) {
            throw new RuntimeException("Showner not been activated. You must call 'doEnable(Context c)' method before");
        }
        a();
    }

    public static void a(Context context) {
        b = context;
    }

    public static void a(CharSequence charSequence) {
        Message message = new Message();
        message.what = 1366;
        message.obj = charSequence;
        d.sendMessage(message);
    }

    public static void a(Object obj, int i) {
        b(obj.toString(), i);
    }

    public static void b(CharSequence charSequence) {
        Message message = new Message();
        message.what = 1367;
        message.obj = charSequence;
        message.arg1 = 1;
        d.sendMessage(message);
    }

    public static void b(String str) {
        b(str, 0);
    }

    public static void b(String str, int i) {
        if (c == null) {
            c = t.b();
        }
        int duration = c.f3071a.getDuration();
        c.a(str, i);
        c.c(duration);
    }

    public static void d(int i) {
        b(b.getString(i), 1);
    }

    public static void e(int i) {
        b(b.getString(i), 0);
    }

    public static void f(int i) {
        b((CharSequence) b.getString(i));
    }

    public static void g(int i) {
        String string = b.getString(i);
        Message message = new Message();
        message.what = 1366;
        message.obj = string;
        d.sendMessage(message);
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.f3071a = Toast.makeText(b, PoiTypeDef.All, 0);
    }

    public final void a(String str) {
        a(str, 0);
    }

    public void a(String str, int i) {
        if (!g.a()) {
            this.f3071a.cancel();
        }
        this.f3071a.setText(str);
        this.f3071a.setDuration(i);
        this.f3071a.show();
    }

    public final void b(int i) {
        this.f3071a.setGravity(80, -1, i);
    }

    public final int c() {
        return this.f3071a.getDuration();
    }

    public final void c(int i) {
        this.f3071a.setDuration(i);
    }
}
