package com.immomo.momo.util;

import android.content.Context;
import android.net.Uri;
import android.text.style.ClickableSpan;
import android.view.View;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.o;

final class as extends ClickableSpan {

    /* renamed from: a  reason: collision with root package name */
    private String f3074a;
    /* access modifiers changed from: private */
    public Context b;

    as(String str, Context context) {
        this.f3074a = str;
        this.b = context;
    }

    public final void onClick(View view) {
        Uri parse = Uri.parse(this.f3074a);
        if ("tel".equals(parse.getScheme())) {
            o oVar = new o(this.b, new String[]{"复制号码", "拨号"});
            oVar.setTitle(parse.getEncodedSchemeSpecificPart());
            oVar.a(new at(this, parse));
            oVar.show();
        } else if ("mailto".equals(parse.getScheme())) {
            o oVar2 = new o(this.b, new String[]{"复制文本", "发送邮件"});
            oVar2.setTitle(parse.getEncodedSchemeSpecificPart());
            oVar2.a(new au(this, parse));
            oVar2.show();
        } else {
            n.a(this.b, "您访问的链接由用户发布，可能产生风险或额外费用。确定继续访问吗？", new av(this, parse)).show();
        }
    }
}
