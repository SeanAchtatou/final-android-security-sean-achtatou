package com.immomo.momo.util;

import android.content.Context;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.TextAppearanceSpan;
import android.text.style.URLSpan;
import android.text.style.UnderlineSpan;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.view.EmoteTextView;
import com.immomo.momo.android.view.ao;
import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    private Cipher f3084a = null;
    private Cipher b = null;

    public e(String str) {
        Key a2 = a(str.getBytes());
        this.f3084a = Cipher.getInstance("DES");
        this.f3084a.init(1, a2);
        this.b = Cipher.getInstance("DES");
        this.b.init(2, a2);
    }

    private static Key a(byte[] bArr) {
        byte[] bArr2 = new byte[8];
        int i = 0;
        while (i < bArr.length && i < bArr2.length) {
            bArr2[i] = bArr[i];
            i++;
        }
        return new SecretKeySpec(bArr2, "DES");
    }

    public static void a(TextView textView, int i, int i2) {
        textView.setFocusable(true);
        textView.setClickable(true);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(textView.getText());
        spannableStringBuilder.setSpan(new TextAppearanceSpan(textView.getContext(), R.style.Style_Text_Link), i, i2, 33);
        spannableStringBuilder.setSpan(new UnderlineSpan(), i, i2, 33);
        textView.setText(spannableStringBuilder);
    }

    public static void a(TextView textView, int i, int i2, int i3) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(textView.getText());
        spannableStringBuilder.setSpan(new TextAppearanceSpan(textView.getContext(), i3), i, i2, 33);
        textView.setText(spannableStringBuilder);
    }

    public static void a(TextView textView, String str, Context context) {
        textView.setText(str);
        CharSequence text = textView.getText();
        if (text instanceof Spannable) {
            int length = text.length();
            Spannable spannable = (Spannable) textView.getText();
            URLSpan[] uRLSpanArr = (URLSpan[]) spannable.getSpans(0, length, URLSpan.class);
            if (uRLSpanArr.length > 0) {
                textView.setMovementMethod(LinkMovementMethod.getInstance());
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(text);
                for (URLSpan uRLSpan : uRLSpanArr) {
                    spannableStringBuilder.removeSpan(uRLSpan);
                    spannableStringBuilder.setSpan(new as(uRLSpan.getURL(), context), spannable.getSpanStart(uRLSpan), spannable.getSpanEnd(uRLSpan), 33);
                }
                textView.setText(spannableStringBuilder);
            }
        }
    }

    public static void a(EmoteTextView emoteTextView, ao aoVar, Context context) {
        boolean a2 = aoVar.a();
        emoteTextView.setText(aoVar);
        if (!a2) {
            CharSequence text = emoteTextView.getText();
            if (text instanceof Spannable) {
                int length = text.length();
                Spannable spannable = (Spannable) emoteTextView.getText();
                URLSpan[] uRLSpanArr = (URLSpan[]) spannable.getSpans(0, length, URLSpan.class);
                if (uRLSpanArr.length > 0) {
                    emoteTextView.setMovementMethod(LinkMovementMethod.getInstance());
                    SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(text);
                    for (URLSpan uRLSpan : uRLSpanArr) {
                        spannableStringBuilder.removeSpan(uRLSpan);
                        spannableStringBuilder.setSpan(new as(uRLSpan.getURL(), context), spannable.getSpanStart(uRLSpan), spannable.getSpanEnd(uRLSpan), 33);
                    }
                    aoVar.b(spannableStringBuilder);
                    emoteTextView.setText(aoVar);
                }
            }
        }
    }

    public static void b(TextView textView, String str, Context context) {
        textView.setText(Html.fromHtml(str));
        CharSequence text = textView.getText();
        if (text instanceof Spannable) {
            int length = text.length();
            Spannable spannable = (Spannable) textView.getText();
            URLSpan[] uRLSpanArr = (URLSpan[]) spannable.getSpans(0, length, URLSpan.class);
            if (uRLSpanArr.length > 0) {
                textView.setMovementMethod(LinkMovementMethod.getInstance());
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(text);
                for (URLSpan uRLSpan : uRLSpanArr) {
                    spannableStringBuilder.removeSpan(uRLSpan);
                    spannableStringBuilder.setSpan(new as(uRLSpan.getURL(), context), spannable.getSpanStart(uRLSpan), spannable.getSpanEnd(uRLSpan), 33);
                }
                textView.setText(spannableStringBuilder);
            }
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v5, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v9, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v10, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String a(java.lang.String r8) {
        /*
            r7 = this;
            r6 = 16
            byte[] r0 = r8.getBytes()
            javax.crypto.Cipher r1 = r7.f3084a
            byte[] r2 = r1.doFinal(r0)
            int r3 = r2.length
            java.lang.StringBuffer r4 = new java.lang.StringBuffer
            int r0 = r3 * 2
            r4.<init>(r0)
            r0 = 0
            r1 = r0
        L_0x0016:
            if (r1 < r3) goto L_0x001d
            java.lang.String r0 = r4.toString()
            return r0
        L_0x001d:
            byte r0 = r2[r1]
        L_0x001f:
            if (r0 < 0) goto L_0x0033
            if (r0 >= r6) goto L_0x0028
            java.lang.String r5 = "0"
            r4.append(r5)
        L_0x0028:
            java.lang.String r0 = java.lang.Integer.toString(r0, r6)
            r4.append(r0)
            int r0 = r1 + 1
            r1 = r0
            goto L_0x0016
        L_0x0033:
            int r0 = r0 + 256
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.util.e.a(java.lang.String):java.lang.String");
    }
}
