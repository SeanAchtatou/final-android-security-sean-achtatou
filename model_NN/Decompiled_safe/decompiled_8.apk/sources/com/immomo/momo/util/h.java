package com.immomo.momo.util;

import android.graphics.Bitmap;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.a;
import com.immomo.momo.service.a.ad;
import com.immomo.momo.service.bean.ak;
import com.mapabc.minimap.map.vmap.VMapProjection;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import org.apache.http.util.EncodingUtils;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    private static m f3087a = new m("test_momo", "[ --- FileUtils --- ]");

    private static File a(int i) {
        switch (i) {
            case 0:
            case 13:
                return a.j();
            case 1:
            case com.immomo.momo.h.DragSortListView_drag_handle_id:
                return a.k();
            case 2:
            case 16:
                return a.g();
            case 3:
            case 15:
                return a.d();
            case 4:
                return a.l();
            case 5:
                return a.w();
            case 6:
                return a.x();
            case 7:
                return a.A();
            case 8:
                return a.n();
            case 9:
                return a.o();
            case 10:
            case 11:
            case 12:
            case 19:
            default:
                return null;
            case 17:
                return a.y();
            case 18:
                return a.c();
            case VMapProjection.MAXZOOMLEVEL /*20*/:
                return a.e();
            case 21:
                return a.h();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x001a A[SYNTHETIC, Splitter:B:13:0x001a] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x002e A[SYNTHETIC, Splitter:B:22:0x002e] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x003d A[SYNTHETIC, Splitter:B:29:0x003d] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:10:0x0013=Splitter:B:10:0x0013, B:19:0x0027=Splitter:B:19:0x0027} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.io.File a(android.graphics.Bitmap r3, java.io.File r4) {
        /*
            r2 = 0
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0011, Exception -> 0x0025, all -> 0x0039 }
            r1.<init>(r4)     // Catch:{ FileNotFoundException -> 0x0011, Exception -> 0x0025, all -> 0x0039 }
            android.graphics.Bitmap$CompressFormat r0 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ FileNotFoundException -> 0x0053, Exception -> 0x0051 }
            r2 = 85
            r3.compress(r0, r2, r1)     // Catch:{ FileNotFoundException -> 0x0053, Exception -> 0x0051 }
            r1.close()     // Catch:{ IOException -> 0x0048 }
        L_0x0010:
            return r4
        L_0x0011:
            r0 = move-exception
            r1 = r2
        L_0x0013:
            com.immomo.momo.util.m r2 = com.immomo.momo.util.h.f3087a     // Catch:{ all -> 0x004f }
            r2.a(r0)     // Catch:{ all -> 0x004f }
            if (r1 == 0) goto L_0x0010
            r1.close()     // Catch:{ IOException -> 0x001e }
            goto L_0x0010
        L_0x001e:
            r0 = move-exception
            com.immomo.momo.util.m r1 = com.immomo.momo.util.h.f3087a
            r1.a(r0)
            goto L_0x0010
        L_0x0025:
            r0 = move-exception
            r1 = r2
        L_0x0027:
            com.immomo.momo.util.m r2 = com.immomo.momo.util.h.f3087a     // Catch:{ all -> 0x004f }
            r2.a(r0)     // Catch:{ all -> 0x004f }
            if (r1 == 0) goto L_0x0010
            r1.close()     // Catch:{ IOException -> 0x0032 }
            goto L_0x0010
        L_0x0032:
            r0 = move-exception
            com.immomo.momo.util.m r1 = com.immomo.momo.util.h.f3087a
            r1.a(r0)
            goto L_0x0010
        L_0x0039:
            r0 = move-exception
            r1 = r2
        L_0x003b:
            if (r1 == 0) goto L_0x0040
            r1.close()     // Catch:{ IOException -> 0x0041 }
        L_0x0040:
            throw r0
        L_0x0041:
            r1 = move-exception
            com.immomo.momo.util.m r2 = com.immomo.momo.util.h.f3087a
            r2.a(r1)
            goto L_0x0040
        L_0x0048:
            r0 = move-exception
            com.immomo.momo.util.m r1 = com.immomo.momo.util.h.f3087a
            r1.a(r0)
            goto L_0x0010
        L_0x004f:
            r0 = move-exception
            goto L_0x003b
        L_0x0051:
            r0 = move-exception
            goto L_0x0027
        L_0x0053:
            r0 = move-exception
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.util.h.a(android.graphics.Bitmap, java.io.File):java.io.File");
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0018 A[SYNTHETIC, Splitter:B:14:0x0018] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x002b A[SYNTHETIC, Splitter:B:22:0x002b] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0039 A[SYNTHETIC, Splitter:B:28:0x0039] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.io.File a(android.graphics.Bitmap r3, java.io.File r4, android.graphics.Bitmap.CompressFormat r5) {
        /*
            r2 = 0
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x000f, Exception -> 0x0023 }
            r1.<init>(r4)     // Catch:{ FileNotFoundException -> 0x000f, Exception -> 0x0023 }
            r0 = 85
            r3.compress(r5, r0, r1)     // Catch:{ FileNotFoundException -> 0x0051, Exception -> 0x004e }
            r1.close()     // Catch:{ IOException -> 0x0044 }
        L_0x000e:
            return r4
        L_0x000f:
            r0 = move-exception
            r1 = r2
        L_0x0011:
            com.immomo.momo.util.m r2 = com.immomo.momo.util.h.f3087a     // Catch:{ all -> 0x004b }
            r2.a(r0)     // Catch:{ all -> 0x004b }
            if (r1 == 0) goto L_0x000e
            r1.close()     // Catch:{ IOException -> 0x001c }
            goto L_0x000e
        L_0x001c:
            r0 = move-exception
            com.immomo.momo.util.m r1 = com.immomo.momo.util.h.f3087a
            r1.a(r0)
            goto L_0x000e
        L_0x0023:
            r0 = move-exception
        L_0x0024:
            com.immomo.momo.util.m r1 = com.immomo.momo.util.h.f3087a     // Catch:{ all -> 0x0036 }
            r1.a(r0)     // Catch:{ all -> 0x0036 }
            if (r2 == 0) goto L_0x000e
            r2.close()     // Catch:{ IOException -> 0x002f }
            goto L_0x000e
        L_0x002f:
            r0 = move-exception
            com.immomo.momo.util.m r1 = com.immomo.momo.util.h.f3087a
            r1.a(r0)
            goto L_0x000e
        L_0x0036:
            r0 = move-exception
        L_0x0037:
            if (r2 == 0) goto L_0x003c
            r2.close()     // Catch:{ IOException -> 0x003d }
        L_0x003c:
            throw r0
        L_0x003d:
            r1 = move-exception
            com.immomo.momo.util.m r2 = com.immomo.momo.util.h.f3087a
            r2.a(r1)
            goto L_0x003c
        L_0x0044:
            r0 = move-exception
            com.immomo.momo.util.m r1 = com.immomo.momo.util.h.f3087a
            r1.a(r0)
            goto L_0x000e
        L_0x004b:
            r0 = move-exception
            r2 = r1
            goto L_0x0037
        L_0x004e:
            r0 = move-exception
            r2 = r1
            goto L_0x0024
        L_0x0051:
            r0 = move-exception
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.util.h.a(android.graphics.Bitmap, java.io.File, android.graphics.Bitmap$CompressFormat):java.io.File");
    }

    public static File a(String str) {
        File file = new File(a.m(), str.substring(0, 1));
        if (!file.exists()) {
            file.mkdirs();
        }
        File file2 = new File(file, str);
        file2.createNewFile();
        if (file2.canWrite()) {
            return file2;
        }
        throw new IOException("Cannot write to " + file2.getParent());
    }

    public static File a(String str, int i) {
        File file;
        String str2 = ".jpg_";
        switch (i) {
            case 2:
            case 3:
            case 15:
            case 16:
            case 18:
            case VMapProjection.MAXZOOMLEVEL /*20*/:
            case 21:
                file = new File(a(i), str.substring(0, 1));
                break;
            default:
                file = a(i);
                break;
        }
        if (file == null) {
            return null;
        }
        if (!file.exists()) {
            file.mkdirs();
        }
        StringBuilder sb = new StringBuilder(String.valueOf(str));
        if (!android.support.v4.b.a.f(str2)) {
            str2 = PoiTypeDef.All;
        }
        return new File(file, sb.append(str2).toString());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap
     arg types: [android.graphics.Bitmap, int, int]
     candidates:
      android.support.v4.b.a.a(android.graphics.Bitmap, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.io.File, int, int):android.graphics.Bitmap
      android.support.v4.b.a.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(java.lang.Object[], java.lang.String, java.lang.String):java.lang.String
      android.support.v4.b.a.a(android.graphics.Bitmap, float, boolean):android.graphics.Bitmap */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
     arg types: [java.lang.String, android.graphics.Bitmap, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File */
    public static File a(String str, Bitmap bitmap, int i, boolean z) {
        int i2 = 3;
        if (android.support.v4.b.a.a((CharSequence) str)) {
            return null;
        }
        File a2 = a(str, i);
        a(bitmap, a2);
        ak akVar = new ak();
        akVar.f2990a = str;
        akVar.b = a2.getAbsolutePath();
        akVar.e = new Date();
        akVar.d = i;
        if (i == 3 || i == 15 || i == 20 || i == 1 || i == 14) {
            akVar.f2990a = String.valueOf(akVar.f2990a) + "_s";
        } else if (i == 2 || i == 16 || i == 21 || i == 0 || i == 13) {
            akVar.f2990a = String.valueOf(akVar.f2990a) + "_l";
        }
        ad.g().b(akVar);
        if (z) {
            switch (i) {
                case 0:
                    i2 = 1;
                    break;
                case 2:
                    break;
                case 16:
                    i2 = 15;
                    break;
                case 21:
                    i2 = 20;
                    break;
                default:
                    i2 = -1;
                    break;
            }
            Bitmap a3 = android.support.v4.b.a.a(bitmap, 150.0f, false);
            a(str, a3, i2, false);
            a3.recycle();
        }
        return a2;
    }

    public static File a(String str, String str2) {
        File b = b(str);
        File b2 = b(str2);
        b.renameTo(b2);
        return b2;
    }

    public static String a(File file) {
        FileInputStream fileInputStream;
        Throwable th;
        String str = null;
        try {
            if (!file.exists()) {
                android.support.v4.b.a.a((Closeable) null);
            } else {
                fileInputStream = new FileInputStream(file);
                try {
                    byte[] bArr = new byte[fileInputStream.available()];
                    fileInputStream.read(bArr);
                    str = EncodingUtils.getString(bArr, "UTF-8");
                    android.support.v4.b.a.a((Closeable) fileInputStream);
                } catch (Throwable th2) {
                    th = th2;
                    android.support.v4.b.a.a((Closeable) fileInputStream);
                    throw th;
                }
            }
            return str;
        } catch (Throwable th3) {
            Throwable th4 = th3;
            fileInputStream = null;
            th = th4;
        }
    }

    public static void a(File file, String str) {
        FileOutputStream fileOutputStream;
        try {
            fileOutputStream = new FileOutputStream(file);
            try {
                fileOutputStream.write(str.getBytes());
                fileOutputStream.flush();
                android.support.v4.b.a.a(fileOutputStream);
            } catch (Throwable th) {
                th = th;
                android.support.v4.b.a.a(fileOutputStream);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fileOutputStream = null;
            android.support.v4.b.a.a(fileOutputStream);
            throw th;
        }
    }

    public static void a(String str, String str2, int i, boolean z) {
        while (true) {
            File a2 = a(str, i);
            File a3 = a(str2, i);
            if (a2 != null && a3 != null) {
                a2.renameTo(a3);
                ak akVar = new ak();
                akVar.f2990a = str2;
                akVar.b = a3.getAbsolutePath();
                akVar.e = new Date();
                akVar.d = i;
                if (i == 3 || i == 15 || i == 20) {
                    akVar.f2990a = String.valueOf(akVar.f2990a) + "_s";
                } else if (i == 2 || i == 16 || i == 21) {
                    akVar.f2990a = String.valueOf(akVar.f2990a) + "_l";
                }
                ad.g().b(akVar);
                if (z) {
                    switch (i) {
                        case 0:
                            i = 1;
                            break;
                        case 2:
                            i = 3;
                            break;
                        case 16:
                            i = 15;
                            break;
                        case 21:
                            i = 20;
                            break;
                        default:
                            return;
                    }
                    z = false;
                } else {
                    return;
                }
            } else {
                return;
            }
        }
    }

    public static File b(String str) {
        File file = new File(a.m(), str.substring(0, 1));
        if (!file.exists()) {
            file.mkdirs();
        }
        return new File(file, str);
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x0092 A[Catch:{ all -> 0x0096 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void b(java.lang.String r10, java.lang.String r11, int r12, boolean r13) {
        /*
            r2 = 20
            r1 = 15
            r3 = 3
            r0 = 0
        L_0x0006:
            java.io.File r7 = a(r10, r12)
            java.io.File r8 = a(r11, r12)
            if (r7 == 0) goto L_0x0012
            if (r8 != 0) goto L_0x0013
        L_0x0012:
            return
        L_0x0013:
            r4 = 0
            r5 = 0
            java.io.BufferedInputStream r6 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x00d1, all -> 0x00c9 }
            java.io.FileInputStream r9 = new java.io.FileInputStream     // Catch:{ IOException -> 0x00d1, all -> 0x00c9 }
            r9.<init>(r7)     // Catch:{ IOException -> 0x00d1, all -> 0x00c9 }
            r7 = 2048(0x800, float:2.87E-42)
            r6.<init>(r9, r7)     // Catch:{ IOException -> 0x00d1, all -> 0x00c9 }
            java.io.BufferedOutputStream r4 = new java.io.BufferedOutputStream     // Catch:{ IOException -> 0x00d5, all -> 0x00cc }
            java.io.FileOutputStream r7 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x00d5, all -> 0x00cc }
            r7.<init>(r8)     // Catch:{ IOException -> 0x00d5, all -> 0x00cc }
            r9 = 2048(0x800, float:2.87E-42)
            r4.<init>(r7, r9)     // Catch:{ IOException -> 0x00d5, all -> 0x00cc }
            r5 = 2048(0x800, float:2.87E-42)
            byte[] r5 = new byte[r5]     // Catch:{ IOException -> 0x0089, all -> 0x00ce }
        L_0x0031:
            int r7 = r6.read(r5)     // Catch:{ IOException -> 0x0089, all -> 0x00ce }
            r9 = -1
            if (r7 != r9) goto L_0x0084
            r4.flush()     // Catch:{ IOException -> 0x0089, all -> 0x00ce }
            android.support.v4.b.a.a(r6)
            android.support.v4.b.a.a(r4)
            com.immomo.momo.service.bean.ak r4 = new com.immomo.momo.service.bean.ak
            r4.<init>()
            r4.f2990a = r11
            java.lang.String r5 = r8.getAbsolutePath()
            r4.b = r5
            java.util.Date r5 = new java.util.Date
            r5.<init>()
            r4.e = r5
            r4.d = r12
            if (r12 == r3) goto L_0x005d
            if (r12 == r1) goto L_0x005d
            if (r12 != r2) goto L_0x00a0
        L_0x005d:
            java.lang.String r5 = r4.f2990a
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r5 = java.lang.String.valueOf(r5)
            r6.<init>(r5)
            java.lang.String r5 = "_s"
            java.lang.StringBuilder r5 = r6.append(r5)
            java.lang.String r5 = r5.toString()
            r4.f2990a = r5
        L_0x0074:
            com.immomo.momo.service.a.ad r5 = com.immomo.momo.service.a.ad.g()
            r5.b(r4)
            if (r13 == 0) goto L_0x0012
            switch(r12) {
                case 0: goto L_0x0081;
                case 2: goto L_0x00c7;
                case 16: goto L_0x00c3;
                case 21: goto L_0x00c5;
                default: goto L_0x0080;
            }
        L_0x0080:
            goto L_0x0012
        L_0x0081:
            r12 = 1
        L_0x0082:
            r13 = r0
            goto L_0x0006
        L_0x0084:
            r9 = 0
            r4.write(r5, r9, r7)     // Catch:{ IOException -> 0x0089, all -> 0x00ce }
            goto L_0x0031
        L_0x0089:
            r0 = move-exception
            r1 = r4
            r2 = r6
        L_0x008c:
            boolean r3 = r8.exists()     // Catch:{ all -> 0x0096 }
            if (r3 == 0) goto L_0x0095
            r8.delete()     // Catch:{ all -> 0x0096 }
        L_0x0095:
            throw r0     // Catch:{ all -> 0x0096 }
        L_0x0096:
            r0 = move-exception
            r5 = r1
            r6 = r2
        L_0x0099:
            android.support.v4.b.a.a(r6)
            android.support.v4.b.a.a(r5)
            throw r0
        L_0x00a0:
            r5 = 2
            if (r12 == r5) goto L_0x00ab
            r5 = 16
            if (r12 == r5) goto L_0x00ab
            r5 = 21
            if (r12 != r5) goto L_0x0074
        L_0x00ab:
            java.lang.String r5 = r4.f2990a
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r5 = java.lang.String.valueOf(r5)
            r6.<init>(r5)
            java.lang.String r5 = "_l"
            java.lang.StringBuilder r5 = r6.append(r5)
            java.lang.String r5 = r5.toString()
            r4.f2990a = r5
            goto L_0x0074
        L_0x00c3:
            r12 = r1
            goto L_0x0082
        L_0x00c5:
            r12 = r2
            goto L_0x0082
        L_0x00c7:
            r12 = r3
            goto L_0x0082
        L_0x00c9:
            r0 = move-exception
            r6 = r4
            goto L_0x0099
        L_0x00cc:
            r0 = move-exception
            goto L_0x0099
        L_0x00ce:
            r0 = move-exception
            r5 = r4
            goto L_0x0099
        L_0x00d1:
            r0 = move-exception
            r1 = r5
            r2 = r4
            goto L_0x008c
        L_0x00d5:
            r0 = move-exception
            r1 = r5
            r2 = r6
            goto L_0x008c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.util.h.b(java.lang.String, java.lang.String, int, boolean):void");
    }
}
