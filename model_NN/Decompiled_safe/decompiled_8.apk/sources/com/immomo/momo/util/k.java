package com.immomo.momo.util;

import com.amap.mapapi.poisearch.PoiTypeDef;
import java.util.Date;
import org.json.JSONObject;

public final class k {

    /* renamed from: a  reason: collision with root package name */
    private String f3091a;
    private String b;
    private Date c;
    private JSONObject d;

    public k(String str, String str2) {
        this.f3091a = str;
        this.b = str2;
        this.c = new Date();
    }

    public k(String str, String str2, JSONObject jSONObject) {
        this(str, str2);
        this.d = jSONObject;
    }

    public final String a() {
        return this.f3091a;
    }

    public final String b() {
        return this.b;
    }

    public final Date c() {
        return this.c;
    }

    public final JSONObject d() {
        return this.d;
    }

    public final void e() {
        try {
            o.a().a(this);
        } catch (Throwable th) {
            new m("LoggerUtil").a(th);
        }
    }

    public final String toString() {
        return String.valueOf(this.f3091a) + "  " + this.b + "  " + (this.c != null ? this.c.getTime() : 0) + "  " + (this.d != null ? this.d : PoiTypeDef.All);
    }
}
