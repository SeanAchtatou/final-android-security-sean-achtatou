package com.immomo.momo.util;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import com.immomo.momo.android.c.g;
import com.immomo.momo.service.bean.bh;
import java.io.File;

final class ax implements g {

    /* renamed from: a  reason: collision with root package name */
    private g f3079a;
    private bh b;
    private /* synthetic */ aw c;

    public ax(aw awVar, g gVar, bh bhVar) {
        this.c = awVar;
        this.f3079a = gVar;
        this.b = bhVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public final /* synthetic */ void a(Object obj) {
        float f;
        int i;
        float f2;
        Bitmap createBitmap;
        Bitmap bitmap = (Bitmap) obj;
        if (bitmap == null) {
            File a2 = h.a(String.valueOf(this.b.a()) + "_temp", 2);
            if (a2.exists()) {
                a2.delete();
            }
            if (this.f3079a != null) {
                this.f3079a.a(bitmap);
                return;
            }
            return;
        }
        File a3 = h.a(this.b.a(), 2);
        try {
            int J = com.immomo.momo.g.J();
            int L = com.immomo.momo.g.L();
            if (bitmap == null) {
                createBitmap = null;
            } else {
                Matrix matrix = new Matrix();
                int width = bitmap.getWidth();
                int height = bitmap.getHeight();
                boolean z = ((float) L) / ((float) J) < ((float) height) / ((float) width);
                if (z) {
                    f = ((float) J) / ((float) width);
                    i = (int) (((((float) height) * f) - ((float) L)) / 2.0f);
                    f2 = f;
                } else {
                    f = ((float) L) / ((float) height);
                    i = (int) (((((float) width) * f) - ((float) J)) / 2.0f);
                    f2 = f;
                }
                matrix.postScale(f2, f);
                createBitmap = z ? Bitmap.createBitmap(bitmap, 0, i, width, height - i, matrix, true) : Bitmap.createBitmap(bitmap, i, 0, width - i, height, matrix, true);
            }
            h.a(createBitmap, a3);
            if (!(bitmap == null || bitmap.hashCode() == createBitmap.hashCode())) {
                bitmap.recycle();
            }
            if (this.f3079a != null) {
                this.f3079a.a(createBitmap);
                return;
            }
            try {
                createBitmap.recycle();
            } catch (Throwable th) {
                this.c.f3078a.a(th);
            }
        } catch (Throwable th2) {
            this.c.f3078a.a(th2);
        }
    }
}
