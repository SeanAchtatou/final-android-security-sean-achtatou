package com.immomo.momo.util;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.b.a;
import android.webkit.WebView;
import java.io.File;
import java.io.FileOutputStream;

public final class ay {

    /* renamed from: a  reason: collision with root package name */
    private Activity f3080a;
    private WebView b;
    private m c = new m(this);
    private File d;

    public ay(Activity activity, WebView webView) {
        this.f3080a = activity;
        this.b = webView;
    }

    private void a(File file) {
        try {
            Bitmap a2 = a.a(file, 720, 720);
            if (a2 != null) {
                File file2 = new File(this.f3080a.getCacheDir(), String.valueOf(System.currentTimeMillis()) + ".jpg_");
                FileOutputStream fileOutputStream = new FileOutputStream(file2);
                a2.compress(Bitmap.CompressFormat.JPEG, 85, fileOutputStream);
                fileOutputStream.close();
                String absolutePath = file2.getAbsolutePath();
                this.c.a((Object) ("sendImage, path=" + absolutePath));
                this.b.loadUrl("javascript:showImage('file://" + absolutePath + "')");
            }
        } catch (Exception e) {
            this.c.a((Throwable) e);
        }
        if (this.d != null) {
            if (this.d.exists()) {
                this.d.delete();
            }
            this.d = null;
        }
    }

    public final void a(int i, int i2, Intent intent) {
        if (i2 != -1) {
            return;
        }
        if (i == 123) {
            Uri data = intent.getData();
            Cursor query = this.f3080a.getContentResolver().query(data, new String[]{"_data"}, null, null, null);
            if (query.moveToFirst()) {
                String string = query.getString(0);
                query.close();
                a(new File(string));
            }
        } else if (i == 124 && this.d != null) {
            a(this.d);
        }
    }

    public final void a(Bundle bundle) {
        if (this.d != null) {
            bundle.putString("cam", this.d.getPath());
        }
    }

    public final void b(Bundle bundle) {
        if (bundle.containsKey("cam")) {
            this.d = new File(bundle.getString("cam"));
        }
    }
}
