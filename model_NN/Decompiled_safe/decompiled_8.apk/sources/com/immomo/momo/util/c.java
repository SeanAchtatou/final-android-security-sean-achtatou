package com.immomo.momo.util;

import android.graphics.Bitmap;
import android.support.v4.c.b;
import android.util.Log;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;

public final class c extends b {

    /* renamed from: a  reason: collision with root package name */
    private static int f3082a = (((int) (Runtime.getRuntime().maxMemory() / 1024)) / 8);
    private static c c = null;
    private Map b;

    private c() {
        this(8192);
    }

    private c(int i) {
        super(i);
        new m(this);
        this.b = new HashMap();
    }

    public static c a() {
        int i = 8192;
        if (c == null) {
            Log.d("BitmapCache", "MOMO=====CacheSize=" + f3082a);
            if (f3082a <= 8192) {
                i = f3082a < 8192 ? 1024 : f3082a;
            }
            c = new c(i);
        }
        return c;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ int b(Object obj) {
        Bitmap bitmap = (Bitmap) obj;
        return (bitmap.getRowBytes() * bitmap.getHeight()) / 1024;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void b(Object obj, Object obj2) {
        String str = (String) obj;
        Bitmap bitmap = (Bitmap) obj2;
        if (bitmap != null) {
            Log.d("BitmapCache", "MOMO=====entryRemovedm, key=" + str);
            this.b.put(str, new SoftReference(bitmap));
        }
    }
}
