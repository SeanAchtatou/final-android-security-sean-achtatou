package com.immomo.momo.util;

import android.util.Log;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.protocol.imjson.util.c;
import java.io.File;

public class m {
    private static /* synthetic */ int[] e;

    /* renamed from: a  reason: collision with root package name */
    private String f3092a;
    private String b;
    private String c;
    private boolean d;

    public m() {
        boolean z = false;
        this.f3092a = "Log4Android";
        this.b = "MOMO==**  ";
        this.c = PoiTypeDef.All;
        this.d = false;
        this.d = c.c() ? true : z;
    }

    public m(Object obj) {
        this(obj.getClass().getSimpleName());
    }

    public m(String str) {
        this();
        this.f3092a = str;
        this.d = c.c();
    }

    public m(String str, String str2) {
        this(str);
        this.b = String.valueOf(str2) + "\n";
    }

    private static void a(Appendable appendable, String str, Throwable th) {
        try {
            appendable.append(th.toString());
            appendable.append("\n");
            StackTraceElement[] stackTrace = th.getStackTrace();
            if (stackTrace != null) {
                for (int i = 0; i < stackTrace.length + 0; i++) {
                    appendable.append(str);
                    appendable.append("\tat ");
                    appendable.append(stackTrace[i].toString());
                    appendable.append("\n");
                }
            }
            try {
                Throwable[] thArr = (Throwable[]) th.getClass().getMethod("getSuppressed", new Class[0]).invoke(th, new Object[0]);
                if (thArr != null) {
                    for (Throwable a2 : thArr) {
                        appendable.append(str);
                        appendable.append("\tSuppressed: ");
                        a(appendable, String.valueOf(str) + "\t", a2);
                    }
                }
            } catch (Exception e2) {
            }
            Throwable cause = th.getCause();
            if (cause != null) {
                appendable.append(str);
                appendable.append("Caused by: ");
                a(appendable, str, cause);
            }
        } catch (Exception e3) {
            throw new AssertionError();
        }
    }

    public static void a(Appendable appendable, Throwable th) {
        a(appendable, PoiTypeDef.All, th);
    }

    private void a(Object obj, Throwable th) {
        a(String.valueOf(this.b) + (obj != null ? obj.toString() : "null") + this.c, th, n.LOG_ERROR);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0015 A[SYNTHETIC, Splitter:B:10:0x0015] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x001e A[SYNTHETIC, Splitter:B:15:0x001e] */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(java.lang.String r4, java.io.File r5) {
        /*
            r1 = 0
            java.io.FileWriter r0 = new java.io.FileWriter     // Catch:{ Exception -> 0x0011, all -> 0x001b }
            r2 = 1
            r0.<init>(r5, r2)     // Catch:{ Exception -> 0x0011, all -> 0x001b }
            r0.append(r4)     // Catch:{ Exception -> 0x002b, all -> 0x0026 }
            r0.flush()     // Catch:{ Exception -> 0x002b, all -> 0x0026 }
            r0.close()     // Catch:{ IOException -> 0x0024 }
        L_0x0010:
            return
        L_0x0011:
            r0 = move-exception
            r0 = r1
        L_0x0013:
            if (r0 == 0) goto L_0x0010
            r0.close()     // Catch:{ IOException -> 0x0019 }
            goto L_0x0010
        L_0x0019:
            r0 = move-exception
            goto L_0x0010
        L_0x001b:
            r0 = move-exception
        L_0x001c:
            if (r1 == 0) goto L_0x0021
            r1.close()     // Catch:{ IOException -> 0x0022 }
        L_0x0021:
            throw r0
        L_0x0022:
            r1 = move-exception
            goto L_0x0021
        L_0x0024:
            r0 = move-exception
            goto L_0x0010
        L_0x0026:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
            goto L_0x001c
        L_0x002b:
            r1 = move-exception
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.util.m.a(java.lang.String, java.io.File):void");
    }

    public static void a(StringBuilder sb, Throwable th, File file) {
        if (th != null) {
            a(sb, PoiTypeDef.All, th);
        }
        String sb2 = sb.toString();
        n nVar = n.LOG_ERROR;
        a(sb2, file);
    }

    private static /* synthetic */ int[] e() {
        int[] iArr = e;
        if (iArr == null) {
            iArr = new int[n.values().length];
            try {
                iArr[n.LOG_DEBUG.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[n.LOG_ERROR.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[n.LOG_INFO.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[n.LOG_VERBOSE.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[n.LOG_WARNING.ordinal()] = 4;
            } catch (NoSuchFieldError e6) {
            }
            e = iArr;
        }
        return iArr;
    }

    public final m a() {
        this.d = false;
        return this;
    }

    public final void a(Object obj) {
        a(String.valueOf(this.b) + (obj == null ? "null" : obj.toString()) + this.c, (Throwable) null, n.LOG_INFO);
    }

    public final void a(String str) {
        this.f3092a = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.m.a(java.lang.Object, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Throwable]
     candidates:
      com.immomo.momo.util.m.a(java.lang.Appendable, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.String, java.io.File):void
      com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.Object, java.lang.Throwable):void */
    public final void a(String str, Throwable th) {
        a((Object) str, th);
    }

    public void a(String str, Throwable th, n nVar) {
        switch (e()[nVar.ordinal()]) {
            case 1:
                if (this.d) {
                    Log.i(this.f3092a, str);
                    return;
                }
                return;
            case 2:
                if (this.d) {
                    Log.d(this.f3092a, str);
                    return;
                }
                return;
            case 3:
                if (th == null) {
                    Log.e(this.f3092a, str);
                    return;
                } else if (this.d) {
                    Log.e(this.f3092a, str, th);
                    return;
                } else {
                    return;
                }
            case 4:
                if (this.d) {
                    Log.w(this.f3092a, str);
                    return;
                }
                return;
            case 5:
                if (this.d) {
                    Log.v(this.f3092a, str);
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.m.a(java.lang.Object, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Throwable]
     candidates:
      com.immomo.momo.util.m.a(java.lang.Appendable, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.String, java.io.File):void
      com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.Object, java.lang.Throwable):void */
    public final void a(Throwable th) {
        a((Object) (th != null ? th.getMessage() : "null"), th);
    }

    public final m b() {
        this.d = false;
        return this;
    }

    public final void b(Object obj) {
        a(String.valueOf(this.b) + (obj != null ? obj.toString() : "null") + this.c, (Throwable) null, n.LOG_DEBUG);
    }

    public final void b(String str) {
        this.b = str;
    }

    public final String c() {
        return this.b;
    }

    public final void c(Object obj) {
        a(String.valueOf(this.b) + (obj != null ? obj.toString() : "null") + this.c, (Throwable) null, n.LOG_WARNING);
    }

    public final String d() {
        return this.f3092a;
    }
}
