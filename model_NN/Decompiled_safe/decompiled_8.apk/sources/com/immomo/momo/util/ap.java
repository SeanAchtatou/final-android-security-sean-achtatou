package com.immomo.momo.util;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

final class ap extends Handler {
    ap(Looper looper) {
        super(looper);
    }

    public final void handleMessage(Message message) {
        if (ao.c == null) {
            ao.c = t.b();
        }
        if (message.what == 1366) {
            ao.c.a((String) message.obj);
        } else if (message.what == 1367) {
            int c = ao.c.c();
            ao.c.a((String) message.obj, message.arg1);
            ao.c.c(c);
        }
    }
}
