package com.immomo.momo.util;

import android.support.v4.b.a;
import com.amap.mapapi.poisearch.PoiTypeDef;
import java.util.HashSet;
import java.util.Set;
import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private static Set f3083a;

    static {
        new m("Cn2SpellUtil").a();
        HashSet hashSet = new HashSet();
        hashSet.add("b");
        hashSet.add("p");
        hashSet.add("m");
        hashSet.add("f");
        hashSet.add("d");
        hashSet.add("t");
        hashSet.add("n");
        hashSet.add("l");
        hashSet.add("g");
        hashSet.add("k");
        hashSet.add("h");
        hashSet.add("j");
        hashSet.add("q");
        hashSet.add("x");
        hashSet.add("z");
        hashSet.add("c");
        hashSet.add("s");
        hashSet.add("r");
        hashSet.add("z");
        hashSet.add("y");
        hashSet.add("w");
        hashSet.add(" ");
        f3083a = hashSet;
    }

    public static String a(String str) {
        String str2 = PoiTypeDef.All;
        try {
            char[] charArray = str.toCharArray();
            HanyuPinyinOutputFormat hanyuPinyinOutputFormat = new HanyuPinyinOutputFormat();
            hanyuPinyinOutputFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
            hanyuPinyinOutputFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
            for (int i = 0; i < charArray.length; i++) {
                if (charArray[i] > 128) {
                    try {
                        str2 = String.valueOf(str2) + PinyinHelper.toHanyuPinyinStringArray(charArray[i], hanyuPinyinOutputFormat)[0].charAt(0);
                    } catch (Exception e) {
                    }
                } else {
                    str2 = String.valueOf(str2) + charArray[i];
                }
            }
        } catch (Exception e2) {
        }
        return str2.toLowerCase();
    }

    public static String b(String str) {
        return a.a(str) ? PoiTypeDef.All : e(a.p(str));
    }

    public static String c(String str) {
        return a.a(str) ? PoiTypeDef.All : a(a.p(str));
    }

    public static boolean d(String str) {
        if (a.a((CharSequence) str.trim())) {
            return false;
        }
        return f3083a.contains(str.substring(0, 1));
    }

    private static String e(String str) {
        String str2 = PoiTypeDef.All;
        try {
            char[] charArray = str.toCharArray();
            HanyuPinyinOutputFormat hanyuPinyinOutputFormat = new HanyuPinyinOutputFormat();
            hanyuPinyinOutputFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
            hanyuPinyinOutputFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
            for (int i = 0; i < charArray.length; i++) {
                if (charArray[i] > 128) {
                    try {
                        str2 = String.valueOf(str2) + PinyinHelper.toHanyuPinyinStringArray(charArray[i], hanyuPinyinOutputFormat)[0];
                    } catch (Exception e) {
                    }
                } else {
                    str2 = String.valueOf(str2) + charArray[i];
                }
            }
        } catch (Exception e2) {
        }
        return str2.toLowerCase();
    }
}
