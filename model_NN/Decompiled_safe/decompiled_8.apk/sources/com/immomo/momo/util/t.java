package com.immomo.momo.util;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.g;

public final class t extends ao implements View.OnClickListener {
    private TextView b = null;
    private View c = null;

    static {
        new m("MToaster");
    }

    public static t a(int i) {
        t b2 = b();
        b2.h(R.layout.common_toast_arrow);
        b2.b(i);
        return b2;
    }

    public static t b() {
        t tVar = new t();
        tVar.h(R.layout.common_toast);
        return tVar;
    }

    private void h(int i) {
        super.a();
        this.c = g.o().inflate(i, (ViewGroup) null);
        this.b = (TextView) this.c.findViewById(R.id.textview);
        this.f3071a.setView(this.c);
        this.f3071a.setGravity(17, -1, 0);
        this.b.setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        h(R.layout.common_toast);
    }

    public final void a(String str, int i) {
        if (!g.a()) {
            this.f3071a.cancel();
        }
        this.b.setText(str);
        this.f3071a.setDuration(i);
        this.f3071a.show();
    }

    public final void onClick(View view) {
        this.f3071a.cancel();
    }
}
