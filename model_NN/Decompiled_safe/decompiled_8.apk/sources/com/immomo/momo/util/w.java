package com.immomo.momo.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.b.a;
import com.immomo.momo.g;
import com.immomo.momo.util.jni.Codec;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.Queue;
import org.json.JSONObject;

public final class w extends Thread {
    private static final String e = ("/data/data/" + g.h() + "/traceroute");
    /* access modifiers changed from: private */
    public static JSONObject f = null;
    /* access modifiers changed from: private */
    public static long h = 120000;
    /* access modifiers changed from: private */
    public static final String i = Codec.xxilss();
    /* access modifiers changed from: private */
    public static final String[] j = Codec.hhhArray();
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f3100a = null;
    /* access modifiers changed from: private */
    public m b = new m();
    /* access modifiers changed from: private */
    public boolean c = false;
    private final Queue d = new LinkedList();
    /* access modifiers changed from: private */
    public aj g = null;

    public w(Context context, aj ajVar) {
        this.f3100a = context;
        this.g = ajVar;
    }

    /* access modifiers changed from: private */
    public ai a(String str) {
        ai aiVar = new ai();
        aiVar.c = str;
        try {
            long currentTimeMillis = System.currentTimeMillis();
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            httpURLConnection.setDoInput(true);
            httpURLConnection.connect();
            InputStream inputStream = httpURLConnection.getInputStream();
            long currentTimeMillis2 = System.currentTimeMillis();
            aiVar.f3068a = (long) a.b(inputStream).length;
            aiVar.b = currentTimeMillis2 - currentTimeMillis;
        } catch (Exception e2) {
            this.b.a((Throwable) e2);
        }
        return aiVar;
    }

    static /* synthetic */ String a(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null) {
            return activeNetworkInfo.getTypeName();
        }
        return null;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0087  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public double b(java.lang.String r5) {
        /*
            r4 = this;
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "ping "
            r0.<init>(r1)
            java.lang.StringBuilder r0 = r0.append(r5)
            java.lang.String r0 = r0.toString()
            r1 = 0
            java.lang.Runtime r2 = java.lang.Runtime.getRuntime()     // Catch:{ Exception -> 0x0077, all -> 0x0083 }
            java.lang.Process r2 = r2.exec(r0)     // Catch:{ Exception -> 0x0077, all -> 0x0083 }
            java.io.InputStream r0 = r2.getInputStream()     // Catch:{ Exception -> 0x0090, all -> 0x008b }
            java.io.InputStreamReader r1 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0090, all -> 0x008b }
            r1.<init>(r0)     // Catch:{ Exception -> 0x0090, all -> 0x008b }
            java.io.BufferedReader r0 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0090, all -> 0x008b }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0090, all -> 0x008b }
        L_0x0026:
            boolean r1 = r4.c     // Catch:{ Exception -> 0x0090, all -> 0x008b }
            if (r1 == 0) goto L_0x0030
            java.lang.String r1 = r0.readLine()     // Catch:{ Exception -> 0x0090, all -> 0x008b }
            if (r1 != 0) goto L_0x0038
        L_0x0030:
            if (r2 == 0) goto L_0x0035
            r2.destroy()
        L_0x0035:
            r0 = 0
        L_0x0037:
            return r0
        L_0x0038:
            java.lang.String r3 = "("
            int r1 = r1.indexOf(r3)     // Catch:{ Exception -> 0x0090, all -> 0x008b }
            if (r1 <= 0) goto L_0x0026
        L_0x0040:
            boolean r1 = r4.c     // Catch:{ Exception -> 0x0090, all -> 0x008b }
            if (r1 == 0) goto L_0x0026
            java.lang.String r1 = r0.readLine()     // Catch:{ Exception -> 0x0090, all -> 0x008b }
            if (r1 == 0) goto L_0x0026
            java.lang.String r3 = "time="
            int r3 = r1.indexOf(r3)     // Catch:{ Exception -> 0x0090, all -> 0x008b }
            if (r3 <= 0) goto L_0x0040
            java.lang.String r0 = "time="
            int r0 = r1.indexOf(r0)     // Catch:{ Exception -> 0x0090, all -> 0x008b }
            int r0 = r0 + 5
            java.lang.String r0 = r1.substring(r0)     // Catch:{ Exception -> 0x0090, all -> 0x008b }
            r1 = 0
            java.lang.String r3 = "ms"
            int r3 = r0.indexOf(r3)     // Catch:{ Exception -> 0x0090, all -> 0x008b }
            java.lang.String r0 = r0.substring(r1, r3)     // Catch:{ Exception -> 0x0090, all -> 0x008b }
            java.lang.String r0 = r0.trim()     // Catch:{ Exception -> 0x0090, all -> 0x008b }
            double r0 = java.lang.Double.parseDouble(r0)     // Catch:{ Exception -> 0x0090, all -> 0x008b }
            if (r2 == 0) goto L_0x0037
            r2.destroy()
            goto L_0x0037
        L_0x0077:
            r0 = move-exception
        L_0x0078:
            com.immomo.momo.util.m r2 = r4.b     // Catch:{ all -> 0x008d }
            r2.a(r0)     // Catch:{ all -> 0x008d }
            if (r1 == 0) goto L_0x0035
            r1.destroy()
            goto L_0x0035
        L_0x0083:
            r0 = move-exception
            r2 = r1
        L_0x0085:
            if (r2 == 0) goto L_0x008a
            r2.destroy()
        L_0x008a:
            throw r0
        L_0x008b:
            r0 = move-exception
            goto L_0x0085
        L_0x008d:
            r0 = move-exception
            r2 = r1
            goto L_0x0085
        L_0x0090:
            r0 = move-exception
            r1 = r2
            goto L_0x0078
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.util.w.b(java.lang.String):double");
    }

    private static boolean b(Context context) {
        try {
            InputStream open = context.getAssets().open("traceroute");
            int available = open.available();
            byte[] bArr = new byte[available];
            open.read(bArr);
            FileOutputStream fileOutputStream = new FileOutputStream(new File(e));
            fileOutputStream.write(bArr, 0, available);
            open.close();
            fileOutputStream.close();
            Process exec = Runtime.getRuntime().exec("chmod 755 " + e);
            exec.waitFor();
            exec.destroy();
            return true;
        } catch (IOException e2) {
            e2.printStackTrace();
            return false;
        } catch (InterruptedException e3) {
            e3.printStackTrace();
            return false;
        }
    }

    /* access modifiers changed from: private */
    public String c(String str) {
        try {
            return InetAddress.getByName(str).getHostAddress();
        } catch (UnknownHostException e2) {
            this.b.a((Throwable) e2);
            return null;
        }
    }

    /* access modifiers changed from: private */
    public String d(String str) {
        String readLine;
        if (!i()) {
            b(this.f3100a);
        }
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec(String.valueOf(e) + " -n " + InetAddress.getByName(str).getHostAddress()).getInputStream()));
            StringBuilder sb = new StringBuilder();
            while (this.c && (readLine = bufferedReader.readLine()) != null) {
                sb.append(readLine);
                sb.append(System.getProperty("line.separator"));
            }
            return sb.toString();
        } catch (Exception e2) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0037  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String g() {
        /*
            r4 = this;
            r1 = 0
            java.lang.String r0 = "getprop net.dns1"
            java.lang.Runtime r2 = java.lang.Runtime.getRuntime()     // Catch:{ IOException -> 0x0025, all -> 0x0033 }
            java.lang.Process r2 = r2.exec(r0)     // Catch:{ IOException -> 0x0025, all -> 0x0033 }
            java.io.InputStream r0 = r2.getInputStream()     // Catch:{ IOException -> 0x0043 }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0043 }
            r3.<init>(r0)     // Catch:{ IOException -> 0x0043 }
            java.io.BufferedReader r0 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0043 }
            r0.<init>(r3)     // Catch:{ IOException -> 0x0043 }
            java.lang.String r0 = r0.readLine()     // Catch:{ IOException -> 0x0043 }
            if (r0 == 0) goto L_0x003b
            if (r2 == 0) goto L_0x0024
            r2.destroy()
        L_0x0024:
            return r0
        L_0x0025:
            r0 = move-exception
            r2 = r1
        L_0x0027:
            com.immomo.momo.util.m r3 = r4.b     // Catch:{ all -> 0x0041 }
            r3.a(r0)     // Catch:{ all -> 0x0041 }
            if (r2 == 0) goto L_0x0031
            r2.destroy()
        L_0x0031:
            r0 = r1
            goto L_0x0024
        L_0x0033:
            r0 = move-exception
            r2 = r1
        L_0x0035:
            if (r2 == 0) goto L_0x003a
            r2.destroy()
        L_0x003a:
            throw r0
        L_0x003b:
            if (r2 == 0) goto L_0x0031
            r2.destroy()
            goto L_0x0031
        L_0x0041:
            r0 = move-exception
            goto L_0x0035
        L_0x0043:
            r0 = move-exception
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.util.w.g():java.lang.String");
    }

    static /* synthetic */ void g(w wVar) {
        if (!g.y()) {
            if (wVar.c && wVar.g != null) {
                wVar.g.a(f);
            }
            wVar.b();
        }
    }

    /* access modifiers changed from: private */
    public String h() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                Enumeration<InetAddress> inetAddresses = networkInterfaces.nextElement().getInetAddresses();
                while (true) {
                    if (inetAddresses.hasMoreElements()) {
                        InetAddress nextElement = inetAddresses.nextElement();
                        if (!nextElement.isLoopbackAddress() && !nextElement.isLinkLocalAddress()) {
                            return nextElement.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (Exception e2) {
            this.b.a((Throwable) e2);
        }
        return null;
    }

    private boolean i() {
        try {
            Process exec = Runtime.getRuntime().exec(e);
            exec.waitFor();
            exec.destroy();
            return true;
        } catch (Exception e2) {
            this.b.a((Throwable) e2);
            return false;
        }
    }

    public final void a() {
        if (!this.c) {
            this.c = true;
            f = new JSONObject();
            if (g.q() != null) {
                try {
                    f.put("momoid", g.q().h);
                } catch (Exception e2) {
                }
            }
            this.d.offer(new aa(this));
            this.d.offer(new ab(this));
            this.d.offer(new ac(this));
            this.d.offer(new ad(this));
            this.d.offer(new ae(this));
            this.d.offer(new af(this));
            this.d.offer(new ag(this));
            this.d.offer(new ah(this));
            this.d.offer(new y(this));
            this.d.offer(new z(this));
            start();
            new x(this).start();
        }
    }

    public final synchronized void b() {
        if (this.c) {
            this.c = false;
            this.d.clear();
            h = 120000;
            interrupt();
        }
    }

    public final void run() {
        super.run();
        while (true) {
            Runnable runnable = (Runnable) this.d.poll();
            if (runnable != null && this.c) {
                runnable.run();
            }
        }
        if (this.c) {
            this.c = false;
            this.g.a(f);
        }
        b();
    }
}
