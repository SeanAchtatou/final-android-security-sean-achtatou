package com.immomo.momo.util;

import android.content.Intent;
import android.net.Uri;
import com.immomo.momo.android.view.a.al;
import com.immomo.momo.g;

final class au implements al {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ as f3076a;
    private final /* synthetic */ Uri b;

    au(as asVar, Uri uri) {
        this.f3076a = asVar;
        this.b = uri;
    }

    public final void a(int i) {
        if (i == 0) {
            g.a((CharSequence) this.b.getEncodedSchemeSpecificPart());
            ao.b("已复制消息文本");
        } else if (i == 1) {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            intent.setData(this.b);
            this.f3076a.b.startActivity(intent);
        }
    }
}
