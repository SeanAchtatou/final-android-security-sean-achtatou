package com.immomo.momo.util;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;

final class av implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ as f3077a;
    private final /* synthetic */ Uri b;

    av(as asVar, Uri uri) {
        this.f3077a = asVar;
        this.b = uri;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.setData(this.b);
        this.f3077a.b.startActivity(intent);
    }
}
