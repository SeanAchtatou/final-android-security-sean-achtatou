package com.immomo.momo.util;

import android.media.SoundPool;
import com.immomo.momo.R;
import com.immomo.momo.g;
import java.util.HashMap;
import java.util.Map;

public class am {

    /* renamed from: a  reason: collision with root package name */
    private static am f3070a = null;
    private SoundPool b = null;
    private Map c = null;

    private am() {
        new m(this);
        this.b = new SoundPool(3, 3, 0);
        this.c = new HashMap();
        this.c.put(Integer.valueOf((int) R.raw.ms_voice_played), Integer.valueOf(this.b.load(g.c(), R.raw.ms_voice_played, 1)));
        this.c.put(Integer.valueOf((int) R.raw.ms_voice_stoped), Integer.valueOf(this.b.load(g.c(), R.raw.ms_voice_stoped, 1)));
        this.c.put(Integer.valueOf((int) R.raw.ref_success), Integer.valueOf(this.b.load(g.c(), R.raw.ref_success, 1)));
        this.c.put(Integer.valueOf((int) R.raw.roma0), Integer.valueOf(this.b.load(g.c(), R.raw.roma0, 1)));
        this.c.put(Integer.valueOf((int) R.raw.romafinal), Integer.valueOf(this.b.load(g.c(), R.raw.romafinal, 1)));
    }

    public static am a() {
        synchronized (am.class) {
            if (f3070a == null) {
                f3070a = new am();
            }
        }
        return f3070a;
    }

    public static void b() {
        a();
    }

    public final void a(int i) {
        int intValue = this.c.get(Integer.valueOf(i)) == null ? 0 : ((Integer) this.c.get(Integer.valueOf(i))).intValue();
        if (intValue > 0) {
            this.b.play(intValue, 1.0f, 1.0f, 1, 0, 1.0f);
        }
    }
}
