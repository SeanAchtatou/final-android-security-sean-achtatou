package com.immomo.momo.util;

import com.immomo.momo.util.jni.Codec;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public final class a {
    private static a c = null;

    /* renamed from: a  reason: collision with root package name */
    private IvParameterSpec f3059a = new IvParameterSpec(Codec.hfdwefher().getBytes());
    private Cipher b = Cipher.getInstance("AES/CBC/PKCS7Padding");

    private a() {
    }

    public static a a() {
        if (c == null) {
            c = new a();
        }
        return c;
    }

    public final String a(String str, String str2) {
        try {
            this.b.init(1, new SecretKeySpec(android.support.v4.b.a.o(str2), "AES"), this.f3059a);
            return b.a(this.b.doFinal(str.getBytes()));
        } catch (Exception e) {
            return null;
        }
    }

    public final String b(String str, String str2) {
        if (str == null || str.length() == 0) {
            throw new Exception("Empty string");
        }
        try {
            this.b.init(2, new SecretKeySpec(android.support.v4.b.a.o(str2), "AES"), this.f3059a);
            return new String(this.b.doFinal(b.b(str.getBytes())));
        } catch (Exception e) {
            return null;
        }
    }
}
