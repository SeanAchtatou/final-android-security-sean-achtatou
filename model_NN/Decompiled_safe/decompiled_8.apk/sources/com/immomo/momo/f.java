package com.immomo.momo;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import mm.purchasesdk.PurchaseCode;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    private Context f2839a;
    private long b;
    private int c = -1;
    private CharSequence d;
    private CharSequence e;
    private PendingIntent f;
    private CharSequence g;
    private Bitmap h;
    private Uri i;
    private long[] j;
    private int k;
    private int l;
    private int m;
    private int n = -55;
    private int o;
    private int p;

    public f(Context context) {
        this.f2839a = context;
        this.b = System.currentTimeMillis();
    }

    private f c(int i2) {
        this.o |= i2;
        return this;
    }

    public final f a() {
        this.k = -16776961;
        this.l = PurchaseCode.QUERY_FROZEN;
        this.m = 1500;
        return this;
    }

    public final f a(int i2) {
        this.c = i2;
        return this;
    }

    public final f a(long j2) {
        this.b = j2;
        return this;
    }

    public final f a(Bitmap bitmap) {
        this.h = bitmap;
        return this;
    }

    public final f a(Uri uri) {
        this.i = uri;
        return this;
    }

    public final f a(CharSequence charSequence) {
        this.d = charSequence;
        return this;
    }

    public final f a(long[] jArr) {
        this.j = jArr;
        return this;
    }

    public final void a(PendingIntent pendingIntent) {
        this.f = pendingIntent;
    }

    public final f b() {
        c(16);
        return this;
    }

    public final f b(CharSequence charSequence) {
        this.e = charSequence;
        return this;
    }

    public final void b(int i2) {
        this.p = i2;
    }

    public final f c(CharSequence charSequence) {
        this.g = charSequence;
        return this;
    }

    public final void c() {
        c(2);
    }

    @SuppressLint({"NewApi"})
    public final Notification d() {
        Notification notification;
        if (g.a()) {
            Notification.Builder builder = new Notification.Builder(this.f2839a);
            builder.setWhen(this.b);
            builder.setSmallIcon(0);
            builder.setNumber(this.c);
            builder.setContentIntent(this.f);
            builder.setTicker(this.g);
            builder.setLargeIcon(this.h);
            builder.setSound(this.i);
            builder.setVibrate(this.j);
            builder.setLights(this.k, this.l, this.m);
            if (this.n != -55) {
                builder.setDefaults(this.n);
            }
            builder.setContentTitle(this.d);
            builder.setContentText(this.e);
            builder.setSmallIcon(this.p);
            if (g.b()) {
                Notification.BigTextStyle bigTextStyle = new Notification.BigTextStyle();
                bigTextStyle.bigText(this.e);
                builder.setStyle(bigTextStyle);
            }
            int i2 = Build.VERSION.SDK_INT;
            notification = builder.getNotification();
        } else {
            notification = new Notification();
            if (this.c != -1) {
                StringBuilder sb = new StringBuilder(this.d);
                sb.append(" (").append(this.c).append(')');
                this.d = sb;
            }
            notification.icon = this.p;
            notification.ledARGB = this.k;
            notification.ledOffMS = this.m;
            notification.ledOnMS = this.l;
            notification.vibrate = this.j;
            notification.sound = this.i;
            notification.when = this.b;
            notification.tickerText = this.g;
            if (this.n != -55) {
                notification.defaults = this.n;
            }
            notification.setLatestEventInfo(this.f2839a, this.d, this.e, this.f);
        }
        notification.flags = this.o;
        if (!(this.l == 0 || this.m == 0)) {
            notification.flags |= 1;
        }
        if ((this.n & 4) != 0) {
            notification.flags |= 1;
        }
        return notification;
    }
}
