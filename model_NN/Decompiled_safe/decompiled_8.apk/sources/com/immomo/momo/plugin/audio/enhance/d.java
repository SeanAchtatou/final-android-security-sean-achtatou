package com.immomo.momo.plugin.audio.enhance;

final class d {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2853a;
    private byte[] b;

    public d() {
    }

    public d(boolean z, byte[] bArr) {
        this.f2853a = z;
        this.b = bArr;
    }

    public final boolean a() {
        return this.f2853a;
    }

    public final byte[] b() {
        return this.b;
    }
}
