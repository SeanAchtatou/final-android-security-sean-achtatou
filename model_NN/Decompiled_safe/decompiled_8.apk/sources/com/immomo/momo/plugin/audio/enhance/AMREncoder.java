package com.immomo.momo.plugin.audio.enhance;

import java.io.File;

public class AMREncoder {
    private static final String AMR_MAGIC_NUMBER = "#!AMR\n";

    private native byte[] _amr_encode(byte[] bArr);

    private native void _amr_encode_deinit();

    private native void _amr_encode_init(String str);

    private native void _amr_encode_short(short[] sArr);

    public void deinit() {
        _amr_encode_deinit();
    }

    public void encode(short[] sArr) {
        _amr_encode_short(sArr);
    }

    public byte[] encode(byte[] bArr) {
        return _amr_encode(bArr);
    }

    public void finish(File file) {
    }

    public void init(String str) {
        _amr_encode_init(str);
    }
}
