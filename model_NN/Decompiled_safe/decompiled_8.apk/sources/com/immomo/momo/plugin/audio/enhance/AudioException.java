package com.immomo.momo.plugin.audio.enhance;

class AudioException extends Exception {
    AudioException(String str) {
        super(str);
    }
}
