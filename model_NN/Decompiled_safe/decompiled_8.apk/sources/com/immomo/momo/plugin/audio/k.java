package com.immomo.momo.plugin.audio;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import com.immomo.momo.g;
import com.immomo.momo.util.m;

public final class k implements SensorEventListener {
    private static float b = Float.MAX_VALUE;
    private static boolean c = false;

    /* renamed from: a  reason: collision with root package name */
    private m f2859a = new m("ProximitySensorEventListener");
    private boolean d = false;
    private boolean e = false;
    private AudioManager f = ((AudioManager) g.d().getSystemService("audio"));
    private SensorManager g;
    private Sensor h;
    private int i = 0;

    private void a(int i2) {
        if (this.i != i2) {
            this.i = i2;
            if (this.i == 2) {
                d.b(0);
            } else if (this.i == 1) {
                d.b(3);
            }
        }
    }

    public final void a() {
        if (!this.e) {
            d.a();
            this.i = d.c();
            if (this.i == 2) {
                d.a();
                d.c(0);
            } else if (this.i == 1) {
                d.a();
                d.c(3);
            } else {
                d.a();
                d.c(3);
                if (this.g == null) {
                    this.g = (SensorManager) g.d().getSystemService("sensor");
                }
                if (this.h == null && this.g != null) {
                    this.h = this.g.getDefaultSensor(8);
                }
                if (this.h != null) {
                    this.e = this.g.registerListener(this, this.h, 2);
                    this.f2859a.b((Object) "--------------------------------------------------registerListener a sensorEventListener");
                }
            }
        }
    }

    public final void b() {
        this.e = false;
        this.d = false;
        if (this.g == null) {
            this.g = (SensorManager) g.d().getSystemService("sensor");
        }
        if (this.g != null && this.h != null) {
            this.g.unregisterListener(this, this.h);
            this.h = null;
            this.g = null;
            this.f2859a.b((Object) "--------------------------------------------------unregisterListener a sensorEventListener");
        }
    }

    public final void c() {
        this.f2859a.b((Object) "/////////////////////  onSensorRelease");
        this.f.setMode(0);
        this.f.setSpeakerphoneOn(false);
    }

    public final void onAccuracyChanged(Sensor sensor, int i2) {
    }

    public final void onSensorChanged(SensorEvent sensorEvent) {
        this.f2859a.c("onSensorChanged --- isHeadset:" + g.ad() + " hasInitProximityValue:" + c + " bigValue:" + b);
        if (!g.ad()) {
            d.a();
            if (d.c() == 0) {
                if (!c) {
                    this.g = (SensorManager) g.b("sensor");
                    if (this.g != null) {
                        this.h = this.g.getDefaultSensor(8);
                        if (this.h != null) {
                            b = this.h.getMaximumRange();
                        }
                    }
                    c = true;
                }
                float f2 = sensorEvent.values[0];
                this.f2859a.c("----->eventValue:" + f2 + " bigValue:" + b);
                this.d = this.e && this.d;
                if (this.d || f2 < b) {
                    if (f2 >= b) {
                        a(1);
                    } else {
                        a(2);
                    }
                }
                this.d = true;
            }
        }
    }
}
