package com.immomo.momo.plugin.audio;

import android.media.AudioManager;
import com.immomo.momo.a;
import com.immomo.momo.android.c.u;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.at;
import com.immomo.momo.util.m;
import java.util.concurrent.ThreadPoolExecutor;

public final class d {
    private static d e = null;
    private static int f = 3;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public e f2847a;
    private g b = null;
    private h c = null;
    /* access modifiers changed from: private */
    public Message d = null;
    private ThreadPoolExecutor g = null;
    private AudioManager h = ((AudioManager) g.d().getSystemService("audio"));
    /* access modifiers changed from: private */
    public m i = new m("test_momo", "[ AudioMessagePlayer ]");

    private d() {
    }

    public static synchronized d a() {
        d dVar;
        synchronized (d.class) {
            if (e == null) {
                e = new d();
            }
            d dVar2 = e;
            if (dVar2.g == null || dVar2.g.isTerminating() || dVar2.g.isTerminated() || dVar2.g.isShutdown()) {
                dVar2.g = u.c();
            }
            dVar = e;
        }
        return dVar;
    }

    public static void b(int i2) {
        System.out.println("MOMO AudioMessagePlayer.replayByStreamType()");
        d a2 = a();
        a2.b();
        f = i2;
        a2.a(a2.b == null ? 0 : a2.b.c());
    }

    public static int c() {
        at b2 = g.d().b();
        if (b2 != null) {
            return b2.i;
        }
        return 0;
    }

    public static void c(int i2) {
        f = i2;
    }

    public final void a(int i2) {
        g oVar;
        Message message = this.d;
        System.out.println("MOMO AudioMessagePlayer.play()");
        this.g.getQueue().clear();
        b();
        if (this.f2847a != null) {
            this.f2847a.a();
            this.f2847a.a(f);
        }
        if (f == 0) {
            this.h.setMode(2);
            this.h.setSpeakerphoneOn(false);
        } else {
            this.h.setMode(0);
            this.h.setSpeakerphoneOn(true);
        }
        this.d = message;
        int i3 = f;
        if (g.f2840a) {
            oVar = new a();
        } else {
            a.C();
            oVar = new o();
        }
        this.b = oVar;
        this.b.a(this.d.tempFile);
        this.b.a(f);
        this.b.b(i2);
        g gVar = this.b;
        if (this.c == null) {
            a.C();
            this.c = new h(this);
        }
        gVar.a(this.c);
        this.b.e();
    }

    public final void a(e eVar) {
        this.f2847a = eVar;
    }

    public final void a(Message message, e eVar) {
        this.d = message;
        this.f2847a = eVar;
    }

    public final boolean a(Message message) {
        return (this.b != null && this.b.d()) && this.d != null && this.d.equals(message);
    }

    public final void b() {
        System.out.println("MOMO AudioMessagePlayer.stop()");
        if (this.b != null) {
            this.b.a();
        }
    }

    public final void d() {
        if (this.g != null) {
            this.g.shutdownNow();
        }
    }
}
