package com.immomo.momo.plugin.audio;

import com.immomo.momo.plugin.audio.enhance.a;
import java.io.File;

public abstract class g {

    /* renamed from: a  reason: collision with root package name */
    protected boolean f2856a = false;
    protected File b = null;
    protected h c = null;
    protected int d = 0;
    protected int e = 3;

    public g() {
        new a("test_momo", "[ AudioPlayer ]");
    }

    public abstract void a();

    public void a(int i) {
        this.e = i;
    }

    public final void a(h hVar) {
        this.c = hVar;
    }

    public void a(File file) {
        this.b = file;
    }

    public abstract void b();

    public void b(int i) {
        this.d = i;
    }

    public abstract int c();

    public boolean d() {
        return this.f2856a;
    }

    public void e() {
        if (!this.f2856a) {
            this.f2856a = true;
            f();
            b();
        }
    }

    /* access modifiers changed from: protected */
    public void f() {
        if (this.c != null) {
            this.c.a();
        }
    }

    /* access modifiers changed from: protected */
    public final void g() {
        if (this.c != null) {
            this.c.d();
        }
    }

    /* access modifiers changed from: protected */
    public final void h() {
        this.f2856a = false;
        if (this.c != null) {
            this.c.c();
        }
    }

    /* access modifiers changed from: protected */
    public final void i() {
        if (this.c != null) {
            h hVar = this.c;
        }
    }
}
