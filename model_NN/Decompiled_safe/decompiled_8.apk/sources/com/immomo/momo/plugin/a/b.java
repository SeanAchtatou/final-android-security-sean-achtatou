package com.immomo.momo.plugin.a;

import com.amap.mapapi.location.LocationManagerProxy;
import com.immomo.momo.util.m;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private static m f2842a = new m("DoubanJsonParser");

    public static d a(String str) {
        JSONObject jSONObject = new JSONObject(str);
        d dVar = new d();
        try {
            dVar.c = jSONObject.getString("douban_user_id");
        } catch (Exception e) {
            f2842a.b((Object) "parse 'douban_user_id' failed");
        }
        try {
            dVar.f2844a = jSONObject.getString(LocationManagerProxy.KEY_LOCATION_CHANGED);
        } catch (Exception e2) {
            f2842a.b((Object) "parse 'location' failed");
        }
        try {
            dVar.b = jSONObject.getString("title");
        } catch (Exception e3) {
            f2842a.b((Object) "parse 'title' failed");
        }
        try {
            dVar.e = jSONObject.getString("icon");
        } catch (Exception e4) {
            f2842a.b((Object) "parse 'icon' failed");
        }
        try {
            dVar.f = jSONObject.getInt("friend_count");
        } catch (Exception e5) {
            f2842a.b((Object) "parse 'friend_count' failed");
        }
        try {
            dVar.g = jSONObject.getInt("contacts_count");
        } catch (Exception e6) {
            f2842a.b((Object) "parse 'contacts_count' failed");
        }
        try {
            dVar.d = jSONObject.getString("content");
        } catch (Exception e7) {
            f2842a.b((Object) "parse 'content' failed");
        }
        return dVar;
    }

    public static List b(String str) {
        JSONArray jSONArray = new JSONObject(str).getJSONArray("entry");
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < jSONArray.length(); i++) {
            c cVar = new c();
            JSONObject jSONObject = jSONArray.getJSONObject(i);
            try {
                cVar.f2843a = jSONObject.getJSONObject("content").getString("$t");
            } catch (Exception e) {
                f2842a.b((Object) "parse 'content' failed");
            }
            try {
                cVar.b = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(jSONObject.getJSONObject("published").getString("$t"));
            } catch (Exception e2) {
                f2842a.b((Object) "parse 'published' failed");
            }
            arrayList.add(cVar);
        }
        return arrayList;
    }
}
