package com.immomo.momo.plugin.c;

import com.immomo.momo.util.m;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private static m f2867a = new m("test_momo");

    private static a a(JSONObject jSONObject) {
        int optInt = jSONObject.optInt("type");
        a aVar = new a();
        jSONObject.optString("post_id");
        aVar.a(jSONObject.optString("message"));
        jSONObject.optString("photo_id");
        if (optInt == 247) {
            try {
                JSONObject jSONObject2 = jSONObject.optJSONObject("attachment").optJSONArray("media").getJSONObject(0);
                jSONObject2.optString("src");
                aVar.b(jSONObject2.optString("src"));
            } catch (Exception e) {
                f2867a.a((Throwable) e);
            }
        }
        long optLong = jSONObject.optLong("created_time", -1);
        f2867a.a((Object) ("created_time = " + optLong));
        aVar.a(optLong != -1 ? new Date(optLong * 1000) : null);
        return aVar;
    }

    public static List a(JSONArray jSONArray) {
        if (jSONArray == null) {
            return null;
        }
        int length = jSONArray.length();
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < length; i++) {
            try {
                arrayList.add(a(jSONArray.getJSONObject(i)));
            } catch (JSONException e) {
                f2867a.a((Throwable) e);
            }
        }
        return arrayList;
    }
}
