package com.immomo.momo.plugin.audio;

import android.media.AudioTrack;
import android.support.v4.b.a;
import com.immomo.momo.util.m;
import java.io.BufferedInputStream;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import mm.purchasesdk.PurchaseCode;

public final class o extends g implements Runnable {
    private m f = new m("test_momo", "[ WAVPlayer ]");
    private AudioTrack g = null;
    private InputStream h = null;
    private DataInputStream i = null;
    private BufferedInputStream j = null;
    private int k = 0;

    private void j() {
        a.a((Closeable) this.i);
        a.a((Closeable) this.j);
        a.a((Closeable) this.h);
    }

    public final void a() {
        this.f2856a = false;
    }

    public final void b() {
        new Thread(this).start();
    }

    public final int c() {
        return this.k;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Exception]
     candidates:
      com.immomo.momo.util.m.a(java.lang.Appendable, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.Object, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.String, java.io.File):void
      com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void */
    public final void run() {
        int i2;
        Exception e;
        int read;
        int i3;
        if (this.b == null || !this.b.exists()) {
            this.f2856a = false;
            g();
            i();
            return;
        }
        int i4 = 0;
        int i5 = 0;
        while (true) {
            if (this.g != null) {
                this.g.release();
                this.g = null;
            }
            this.f.b((Object) ("+++++++++++++++++ while " + i4));
            try {
                int minBufferSize = AudioTrack.getMinBufferSize(f.f2855a[i4], 2, 2);
                try {
                    this.g = new AudioTrack(this.e, f.f2855a[i4], 2, 2, minBufferSize, 1);
                    i2 = minBufferSize;
                } catch (Exception e2) {
                    e = e2;
                    i2 = minBufferSize;
                    this.f.a("Exception has handled,just for debug", (Throwable) e);
                    if (this.g == null) {
                    }
                    i4 = i3;
                    i5 = i2;
                }
            } catch (Exception e3) {
                Exception exc = e3;
                i2 = i5;
                e = exc;
            }
            if (!(this.g == null && this.g.getState() == 1) && (i3 = i4 + 1) < f.f2855a.length) {
                i4 = i3;
                i5 = i2;
            }
        }
        if (this.g == null) {
            this.f2856a = false;
            g();
            i();
        } else if (this.g.getState() != 1) {
            this.f2856a = false;
            this.g.release();
            this.g = null;
            g();
            i();
        } else {
            try {
                this.g.play();
                byte[] bArr = new byte[100];
                this.h = new FileInputStream(this.b);
                this.j = new BufferedInputStream(this.h);
                this.i = new DataInputStream(this.j);
                if (this.d <= 0 || ((long) this.d) >= this.b.length()) {
                    this.k = PurchaseCode.LOADCHANNEL_ERR;
                    this.i.skip((long) this.k);
                } else {
                    this.i.skip((long) this.d);
                    this.k = this.d;
                }
                while (true) {
                    if (!this.f2856a || (read = this.i.read(bArr)) == -1) {
                        break;
                    } else if (this.g.write(bArr, 0, read) < 0) {
                        this.f.b((Object) "++++++++++ audio write error state");
                        break;
                    } else {
                        this.k = read + this.k;
                    }
                }
                this.f.b((Object) ("+++++++++++++++ truck stop. bufferSize:" + i2 + " track:" + this.g.hashCode()));
                try {
                    this.g.pause();
                    this.g.flush();
                } catch (Exception e4) {
                    this.f.b(e4);
                }
                try {
                    this.g.release();
                    this.g = null;
                } catch (Exception e5) {
                    this.f.a("[handled]", (Throwable) e5);
                }
                this.g = null;
                if (this.f2856a) {
                    h();
                }
                j();
                i();
            } catch (Throwable th) {
                try {
                    this.g.pause();
                    this.g.flush();
                } catch (Exception e6) {
                    this.f.b(e6);
                }
                try {
                    this.g.release();
                    this.g = null;
                } catch (Exception e7) {
                    this.f.a("[handled]", (Throwable) e7);
                }
                this.g = null;
                if (this.f2856a) {
                    h();
                }
                j();
                i();
                throw th;
            }
        }
    }
}
