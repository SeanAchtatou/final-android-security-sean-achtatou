package com.immomo.momo.plugin.audio.enhance;

interface AudioDataHandler {
    void handle(byte[] bArr);

    void handle(short[] sArr);
}
