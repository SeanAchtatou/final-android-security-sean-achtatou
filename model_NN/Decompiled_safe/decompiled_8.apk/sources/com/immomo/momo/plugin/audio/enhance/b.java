package com.immomo.momo.plugin.audio.enhance;

import com.immomo.momo.protocol.imjson.a;
import com.immomo.momo.service.af;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.util.h;
import java.io.File;

final class b extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ AudioMessageTaskEnhance f2851a;
    private final /* synthetic */ Message b;

    b(AudioMessageTaskEnhance audioMessageTaskEnhance, Message message) {
        this.f2851a = audioMessageTaskEnhance;
        this.b = message;
    }

    public final void run() {
        try {
            File file = this.f2851a.f2848a;
            this.f2851a.c.a((Object) ("amrFile : " + file.getAbsolutePath()));
            if (file.length() > 200 && file.length() < 256000) {
                String a2 = a.a(this.b.chatType == 1 ? this.b.remoteId : this.b.chatType == 2 ? this.b.groupId : this.b.chatType == 3 ? this.b.discussId : null, file, this.b.chatType);
                if (android.support.v4.b.a.f(a2)) {
                    this.b.fileName = a2;
                    this.b.fileSize = file.length();
                    af.c().b(this.b);
                    this.b.tempFile = h.a(this.f2851a.f2848a.getName(), a2);
                    this.f2851a.e = true;
                }
            }
        } catch (Exception e) {
            this.f2851a.c.a((Throwable) e);
        }
        if (!this.f2851a.e) {
            AudioMessageTaskEnhance audioMessageTaskEnhance = this.f2851a;
            int i = audioMessageTaskEnhance.b;
            audioMessageTaskEnhance.b = i + 1;
            if (i >= 3) {
                this.f2851a.c();
            }
        }
        if (this.f2851a.f != null) {
            this.f2851a.f.cancel();
            this.f2851a.f = null;
            AudioMessageTaskEnhance audioMessageTaskEnhance2 = this.f2851a;
            AudioMessageTaskEnhance.a().purge();
        }
        AudioMessageTaskEnhance audioMessageTaskEnhance3 = this.f2851a;
        synchronized (AudioMessageTaskEnhance.b()) {
            AudioMessageTaskEnhance audioMessageTaskEnhance4 = this.f2851a;
            AudioMessageTaskEnhance.b().notify();
            Object obj = null;
        }
    }
}
