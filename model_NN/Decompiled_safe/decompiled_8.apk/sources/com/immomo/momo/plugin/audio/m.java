package com.immomo.momo.plugin.audio;

final class m extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ l f2860a;

    private m(l lVar) {
        this.f2860a = lVar;
    }

    /* synthetic */ m(l lVar, byte b) {
        this(lVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Exception]
     candidates:
      com.immomo.momo.util.m.a(java.lang.Appendable, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.Object, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.String, java.io.File):void
      com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void */
    public final void run() {
        try {
            if (this.f2860a.f != n.RECORDING) {
                throw new Exception("prepare() method called on illegal state:" + this.f2860a.f);
            } else if (this.f2860a.c.getState() != 1 || this.f2860a.e == null) {
                throw new Exception("prepare() method called on uninitialized recorder");
            } else {
                this.f2860a.g();
                Thread.sleep(300);
                this.f2860a.c.startRecording();
                this.f2860a.c.read(this.f2860a.l, 0, this.f2860a.l.length);
                while (this.f2860a.f == n.RECORDING) {
                    if (this.f2860a.c.read(this.f2860a.l, 0, this.f2860a.l.length) < 0) {
                        break;
                    }
                    int length = this.f2860a.l.length;
                    try {
                        this.f2860a.n.write(this.f2860a.l, 0, length);
                        l lVar = this.f2860a;
                        lVar.m = lVar.m + length;
                        if (this.f2860a.i == 16) {
                            for (int i = 0; i < length / 2; i++) {
                                l lVar2 = this.f2860a;
                                short a2 = l.a(this.f2860a.l[i * 2], this.f2860a.l[(i * 2) + 1]);
                                if (a2 > this.f2860a.d) {
                                    this.f2860a.d = a2;
                                }
                            }
                        } else {
                            for (int i2 = 0; i2 < length; i2++) {
                                if (this.f2860a.l[i2] > this.f2860a.d) {
                                    this.f2860a.d = this.f2860a.l[i2];
                                }
                            }
                        }
                    } catch (Exception e) {
                        l.b.a("Error occured in updateListener, recording is aborted", (Throwable) e);
                    }
                }
                l.b.b((Object) ("----------------- RecoderTask state = " + this.f2860a.f));
                l.b.a((Object) "RecoderTask end");
            }
        } catch (Exception e2) {
            l.b.a("Error record Thread Exception", (Throwable) e2);
            this.f2860a.j();
        }
    }
}
