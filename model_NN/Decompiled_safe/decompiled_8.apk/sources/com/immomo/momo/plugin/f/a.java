package com.immomo.momo.plugin.f;

import com.amap.mapapi.location.LocationManagerProxy;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.util.m;
import com.sina.sdk.api.message.InviteApi;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static m f2876a = new m("WeiboJsonParser").a();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Exception]
     candidates:
      com.immomo.momo.util.m.a(java.lang.Appendable, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.Object, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.String, java.io.File):void
      com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void */
    public static b a(String str) {
        JSONObject jSONObject = new JSONObject(new JSONObject(str).getString("profiles"));
        b bVar = new b();
        try {
            bVar.f2877a = jSONObject.getString("id");
        } catch (Exception e) {
            f2876a.a("parse 'id' failed", (Throwable) e);
        }
        try {
            bVar.b = jSONObject.optString("screen_name", bVar.f2877a);
        } catch (Exception e2) {
            f2876a.a("parse 'id' failed", (Throwable) e2);
        }
        try {
            bVar.e = jSONObject.getString(InviteApi.KEY_URL);
        } catch (Exception e3) {
            f2876a.a("parse 'id' failed", (Throwable) e3);
        }
        try {
            bVar.d = jSONObject.optString("description", PoiTypeDef.All);
        } catch (Exception e4) {
            f2876a.a("parse 'id' failed", (Throwable) e4);
        }
        try {
            bVar.c = jSONObject.optString("openid", PoiTypeDef.All);
        } catch (Exception e5) {
            f2876a.a("parse 'id' failed", (Throwable) e5);
        }
        try {
            bVar.l = jSONObject.optString("domain", PoiTypeDef.All);
        } catch (Exception e6) {
            f2876a.a("parse 'id' failed", (Throwable) e6);
        }
        try {
            bVar.m = jSONObject.optString("tencent_vip_desc", PoiTypeDef.All);
        } catch (Exception e7) {
            f2876a.a("parse 'id' failed", (Throwable) e7);
        }
        try {
            bVar.n = jSONObject.optInt("isvip", bVar.n);
        } catch (Exception e8) {
            f2876a.a("parse 'id' failed", (Throwable) e8);
        }
        try {
            bVar.h = jSONObject.optInt("fansnum", 0);
        } catch (Exception e9) {
            f2876a.a("parse 'id' failed", (Throwable) e9);
        }
        try {
            bVar.i = jSONObject.optInt("idolnum", 0);
        } catch (Exception e10) {
            f2876a.a("parse 'id' failed", (Throwable) e10);
        }
        try {
            bVar.j = jSONObject.optInt("statuses_count", 0);
        } catch (Exception e11) {
            f2876a.a("parse 'id' failed", (Throwable) e11);
        }
        try {
            bVar.k = jSONObject.optString("gender", "f");
        } catch (Exception e12) {
            f2876a.a("parse 'id' failed", (Throwable) e12);
        }
        try {
            bVar.o = jSONObject.optInt("has_head_img", 1) == 1;
        } catch (Exception e13) {
            f2876a.a("parse 'id' failed", (Throwable) e13);
        }
        try {
            bVar.a(jSONObject.optString("profile_image_url", PoiTypeDef.All));
        } catch (Exception e14) {
            f2876a.a("parse 'id' failed", (Throwable) e14);
        }
        return bVar;
    }

    public static List b(String str) {
        ArrayList arrayList = new ArrayList();
        JSONObject jSONObject = new JSONObject(str);
        if (jSONObject.has(LocationManagerProxy.KEY_STATUS_CHANGED)) {
            JSONArray jSONArray = new JSONArray(jSONObject.getString(LocationManagerProxy.KEY_STATUS_CHANGED));
            for (int i = 0; i < jSONArray.length(); i++) {
                arrayList.add(c(jSONArray.getJSONObject(i).toString()));
            }
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Exception]
     candidates:
      com.immomo.momo.util.m.a(java.lang.Appendable, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.Object, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.String, java.io.File):void
      com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void */
    private static c c(String str) {
        c cVar = new c();
        JSONObject jSONObject = new JSONObject(str);
        try {
            cVar.e = jSONObject.getLong("timestamp");
        } catch (Exception e) {
            f2876a.a("parse 'created_at' failed", (Throwable) e);
        }
        try {
            cVar.f2878a = (long) jSONObject.getInt("id");
        } catch (Exception e2) {
            f2876a.a("parse 'id' failed", (Throwable) e2);
        }
        try {
            cVar.b = jSONObject.getString(InviteApi.KEY_TEXT);
        } catch (Exception e3) {
            f2876a.a("parse 'text' failed", (Throwable) e3);
        }
        try {
            cVar.d = jSONObject.getString("origtext");
        } catch (Exception e4) {
            f2876a.a("parse 'text' failed", (Throwable) e4);
        }
        try {
            cVar.c = jSONObject.getString("source_nick");
        } catch (Exception e5) {
            f2876a.a("parse 'text' failed", (Throwable) e5);
        }
        try {
            String string = jSONObject.getString("image");
            if (com.immomo.a.a.f.a.a(string)) {
                cVar.h = false;
            } else {
                cVar.f = String.valueOf(string) + "/120";
                String.valueOf(string) + "/160";
                cVar.g = String.valueOf(string) + "/460";
                cVar.h = true;
            }
            if (cVar.f != null) {
                cVar.h = true;
            }
        } catch (Exception e6) {
            f2876a.a("parse 'thumbnail_pic' failed", (Throwable) e6);
        }
        return cVar;
    }
}
