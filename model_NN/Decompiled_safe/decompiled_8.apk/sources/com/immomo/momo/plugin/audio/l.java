package com.immomo.momo.plugin.audio;

import android.media.AudioRecord;
import android.support.v4.b.a;
import com.baidu.location.LocationClientOption;
import com.immomo.momo.util.m;
import java.io.File;
import java.io.RandomAccessFile;
import mm.purchasesdk.PurchaseCode;

public final class l extends i {
    /* access modifiers changed from: private */
    public static m b = new m("WAVAudioRecorder");
    /* access modifiers changed from: private */
    public AudioRecord c = null;
    /* access modifiers changed from: private */
    public int d = 0;
    /* access modifiers changed from: private */
    public String e = null;
    /* access modifiers changed from: private */
    public n f;
    private short g;
    private int h;
    /* access modifiers changed from: private */
    public short i;
    private int j;
    private int k;
    /* access modifiers changed from: private */
    public byte[] l;
    /* access modifiers changed from: private */
    public int m;
    /* access modifiers changed from: private */
    public RandomAccessFile n;

    private l(int i2) {
        try {
            this.i = 16;
            this.g = 1;
            this.h = i2;
            this.k = (i2 * PurchaseCode.SDK_RUNNING) / LocationClientOption.MIN_SCAN_SPAN;
            this.j = (((this.k * 2) * this.i) * this.g) / 8;
            int minBufferSize = AudioRecord.getMinBufferSize(i2, 2, 2);
            if (this.j < minBufferSize) {
                this.j = minBufferSize;
                this.k = this.j / (((this.i * 2) * this.g) / 8);
                b.c("Increasing buffer size to " + Integer.toString(this.j));
            }
            this.c = new AudioRecord(1, i2, 2, 2, this.j);
            b.a((Object) ("---------------- create a new AudioRecord bufferSize=" + this.j + " hash:" + this.c.hashCode() + "  state:" + this.c.getState()));
            if (this.c.getState() != 1) {
                int state = this.c.getState();
                this.c.release();
                this.c = null;
                throw new Exception("AudioRecord initialization failed  state=" + state);
            }
            this.d = 0;
            this.e = null;
            this.f = n.INITIALIZING;
        } catch (Exception e2) {
            b.a((Throwable) e2);
            this.f = n.ERROR;
        }
    }

    static /* synthetic */ short a(byte b2, byte b3) {
        return (short) ((b3 << 8) | b2);
    }

    public static l f() {
        l lVar;
        int i2 = 0;
        do {
            lVar = new l(f.f2855a[i2]);
            b.b((Object) ("******************* getRecoderInstance " + lVar.hashCode() + "  state:" + lVar.f));
            if (lVar.f == n.INITIALIZING) {
                break;
            }
            i2++;
        } while (i2 < f.f2855a.length);
        return lVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Exception]
     candidates:
      com.immomo.momo.util.m.a(java.lang.Appendable, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.Object, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.String, java.io.File):void
      com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void */
    private void i() {
        b.a((Object) ("before stop recode state:" + this.f));
        if (this.f == n.RECORDING) {
            this.f = n.STOPPED;
            try {
                this.n.seek(4);
                this.n.writeInt(Integer.reverseBytes(this.m + 36));
                this.n.seek(40);
                this.n.writeInt(Integer.reverseBytes(this.m));
                b.a((Object) "randomAccessWriter closed");
            } catch (Exception e2) {
                b.a("Exception occured while closing output file", (Throwable) e2);
                this.f = n.ERROR;
            } finally {
                a.a(this.n);
            }
        } else if (this.f == n.STOPPED) {
            b.a("stop() called on recoder already stoped", (Throwable) null);
        } else {
            b.a("stop() called on illegal state", (Throwable) null);
            this.f = n.ERROR;
        }
        try {
            this.c.release();
        } catch (Exception e3) {
        }
        this.c = null;
        b.b((Object) "++++++++++ recoder has stoped");
    }

    /* access modifiers changed from: private */
    public void j() {
        this.f = n.ERROR;
        try {
            this.c.release();
        } catch (Exception e2) {
        }
        this.c = null;
        if (this.f2858a != null) {
            this.f2858a.d();
        }
    }

    public final void a() {
        if (this.f == n.INITIALIZING) {
            this.m = 0;
            this.f = n.RECORDING;
            if (this.m == 0 && this.f2858a != null) {
                this.f2858a.a();
            }
            new m(this, (byte) 0).start();
            return;
        }
        b.a("start() called on illegal state", (Throwable) null);
        this.f = n.ERROR;
        j();
    }

    public final void a(File file) {
        if (this.f == n.INITIALIZING) {
            this.e = file.getPath();
        }
    }

    public final synchronized void b() {
        i();
        if (this.f2858a != null) {
            this.f2858a.b();
        }
    }

    public final void c() {
        i();
        if (this.f2858a != null) {
            this.f2858a.c();
        }
    }

    public final boolean d() {
        return this.f == n.RECORDING;
    }

    public final int e() {
        if (this.f != n.RECORDING) {
            return 0;
        }
        int i2 = this.d;
        this.d = 0;
        return i2;
    }

    public final void g() {
        try {
            this.n = new RandomAccessFile(this.e, "rw");
            this.n.setLength(0);
            this.n.writeBytes("RIFF");
            this.n.writeInt(0);
            this.n.writeBytes("WAVE");
            this.n.writeBytes("fmt ");
            this.n.writeInt(Integer.reverseBytes(16));
            this.n.writeShort(Short.reverseBytes(1));
            this.n.writeShort(Short.reverseBytes(this.g));
            this.n.writeInt(Integer.reverseBytes(this.h));
            this.n.writeInt(Integer.reverseBytes(((this.h * this.i) * this.g) / 8));
            this.n.writeShort(Short.reverseBytes((short) ((this.g * this.i) / 8)));
            this.n.writeShort(Short.reverseBytes(this.i));
            this.n.writeBytes("data");
            this.n.writeInt(0);
            this.l = new byte[(((this.k * this.i) / 8) * this.g)];
            b.a((Object) ("----------------prepare, buffer.size=" + this.l.length));
        } catch (Exception e2) {
            b.a((Throwable) e2);
            this.f = n.ERROR;
            throw new Exception("Error on prepare randomAccessWriter");
        }
    }
}
