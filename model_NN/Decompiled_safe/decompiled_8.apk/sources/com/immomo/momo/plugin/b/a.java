package com.immomo.momo.plugin.b;

import android.graphics.drawable.Drawable;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.c.g;
import com.immomo.momo.android.view.de;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.util.m;
import java.util.HashMap;
import java.util.Map;
import mm.purchasesdk.PurchaseCode;

public class a extends de implements aj {

    /* renamed from: a  reason: collision with root package name */
    String f2862a;
    m b;
    private String c;
    private String d;
    private int e;
    private int f;
    private Drawable g;
    private Map h;
    private boolean i;
    private boolean j;
    private boolean k;
    private g l;

    private a() {
        this.f2862a = PoiTypeDef.All;
        this.c = PoiTypeDef.All;
        this.d = PoiTypeDef.All;
        this.g = null;
        this.h = null;
        this.b = new m(this);
        this.i = false;
        this.j = false;
        this.k = false;
        this.l = new b(this);
    }

    public a(String str) {
        this();
        if (!com.immomo.a.a.f.a.a(str)) {
            String[] split = str.substring(1, str.length() - 1).split("\\|");
            this.f2862a = split[0];
            HashMap hashMap = new HashMap();
            for (int i2 = 2; i2 < split.length; i2++) {
                String[] split2 = split[i2].split("=");
                if (split2.length > 1) {
                    hashMap.put(split2[0], split2[1]);
                } else {
                    hashMap.put(split2[0], PoiTypeDef.All);
                }
            }
            this.h = hashMap;
            this.c = a("l", PoiTypeDef.All);
            this.d = a("n", PoiTypeDef.All);
            this.i = this.d != null && this.d.endsWith(".gif");
            if (a("s") != null) {
                try {
                    String[] split3 = a("s").split("x");
                    this.e = Integer.parseInt(split3[0]);
                    this.f = Integer.parseInt(split3[1]);
                    if (this.e > 500) {
                        this.e = PurchaseCode.LOADCHANNEL_ERR;
                    }
                    if (this.f > 500) {
                        this.f = PurchaseCode.LOADCHANNEL_ERR;
                    }
                } catch (Exception e2) {
                }
            }
            d();
        }
    }

    private String a(String str) {
        return (String) this.h.get(str);
    }

    private String a(String str, String str2) {
        return a(str) == null ? str2 : (String) this.h.get(str);
    }

    public final void a(Drawable drawable) {
        this.g = drawable;
    }

    /* access modifiers changed from: protected */
    public final Drawable b() {
        if (super.b() != this.g) {
            c();
        }
        return super.b();
    }

    /* access modifiers changed from: protected */
    public final Drawable e() {
        if (this.g == null) {
            Drawable a2 = c.a(this.d, this.c, this);
            if (a2 == null) {
                a2 = com.immomo.momo.g.b((int) R.drawable.zemoji_error);
            }
            this.g = a2;
        }
        return this.g;
    }

    public final boolean f() {
        return this.i;
    }

    public final String g() {
        return this.d;
    }

    public g getImageCallback() {
        return this.l;
    }

    public String getLoadImageId() {
        return null;
    }

    public final String h() {
        return this.c;
    }

    public final String i() {
        return this.f2862a;
    }

    public boolean isImageLoading() {
        return this.j;
    }

    public boolean isImageLoadingFailed() {
        return this.k;
    }

    public boolean isImageMultipleDiaplay() {
        return false;
    }

    public boolean isImageUrl() {
        return false;
    }

    public final int j() {
        return this.e;
    }

    public final int k() {
        return this.f;
    }

    public void setImageCallback(g gVar) {
        this.l = gVar;
    }

    public void setImageLoadFailed(boolean z) {
        this.k = z;
    }

    public void setImageLoading(boolean z) {
        this.j = z;
    }

    public String toString() {
        return "[" + this.f2862a + "|et|l=" + this.c + "|n=" + this.d + "|s=" + this.e + "x" + this.f + "]";
    }
}
