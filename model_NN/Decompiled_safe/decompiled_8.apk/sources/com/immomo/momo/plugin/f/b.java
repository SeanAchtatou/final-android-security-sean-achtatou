package com.immomo.momo.plugin.f;

import com.immomo.a.a.f.a;
import com.immomo.momo.service.bean.al;
import java.io.Serializable;

public final class b extends al implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public String f2877a;
    public String b;
    public String c;
    public String d;
    public String e;
    public String f;
    public String g;
    public int h;
    public int i;
    public int j;
    public String k;
    public String l;
    public String m;
    public int n;
    public boolean o;
    private String p;

    public final void a(String str) {
        if (!a.a(str)) {
            if (this.o) {
                this.p = String.valueOf(str) + "/40";
                this.f = String.valueOf(str) + "/50";
                this.g = String.valueOf(str) + "/120";
                return;
            }
            this.p = str;
            this.f = str;
            this.g = str;
        }
    }

    public final boolean a() {
        return this.n != 0;
    }

    public final String getLoadImageId() {
        return this.p;
    }

    public final boolean isImageUrl() {
        return true;
    }
}
