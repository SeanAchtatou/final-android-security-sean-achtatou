package com.immomo.momo.plugin.audio;

import android.media.MediaPlayer;
import com.immomo.momo.util.m;

public final class a extends g {
    /* access modifiers changed from: private */
    public m f = new m("test_momo", "[ AmrPlayer ]");
    private MediaPlayer g = null;

    public final void a() {
        this.f2856a = false;
        try {
            this.g.stop();
        } catch (Exception e) {
            this.f.b(e);
        }
    }

    public final void b() {
        if (this.b == null || !this.b.exists()) {
            this.f2856a = false;
            g();
            return;
        }
        if (this.g != null) {
            if (this.g.isPlaying()) {
                this.g.stop();
            }
            this.g.reset();
        } else {
            this.g = new MediaPlayer();
        }
        this.g.setAudioStreamType(this.e);
        this.g.setOnCompletionListener(new b(this));
        this.g.setOnErrorListener(new c(this));
        try {
            this.g.setDataSource(this.b.getAbsolutePath());
            try {
                this.g.prepare();
                this.f.a("@@@@@@@@@@@@@@@@@@ mediaPlayer ready to play", (Throwable) null);
                if (this.f2856a) {
                    this.f.a("@@@@@@@@@@@@@@@@@@ mediaPlayer start to play", (Throwable) null);
                    this.g.seekTo(this.d);
                    this.g.start();
                }
                this.f.a("@@@@@@@@@@@@@@@@@@ mediaPlayer after to call start", (Throwable) null);
            } catch (Exception e) {
                this.f.a((Throwable) e);
                g();
                i();
            }
        } catch (Exception e2) {
            this.f.a((Throwable) e2);
            g();
            i();
        }
    }

    public final int c() {
        if (this.g == null) {
            return 0;
        }
        return this.g.getCurrentPosition();
    }
}
