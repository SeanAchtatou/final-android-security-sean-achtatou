package com.immomo.momo.plugin.d;

import com.amap.mapapi.location.LocationManagerProxy;
import com.immomo.momo.util.m;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static m f2868a = new m("RenrenJsobParser");

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Exception]
     candidates:
      com.immomo.momo.util.m.a(java.lang.Appendable, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.Object, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.String, java.io.File):void
      com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void */
    public static c a(String str) {
        JSONObject jSONObject = new JSONObject(str);
        c cVar = new c();
        try {
            JSONObject jSONObject2 = new JSONObject(jSONObject.getString("profiles"));
            try {
                cVar.f2870a = jSONObject2.optString("name", cVar.f2870a);
            } catch (Exception e) {
                f2868a.b((Object) "parse 'name' failed");
            }
            try {
                cVar.b = jSONObject2.getString("sex").equals("1") ? "M" : "F";
            } catch (Exception e2) {
                f2868a.b((Object) "parse 'sex' failed");
            }
            try {
                cVar.c = jSONObject2.getString("star");
            } catch (Exception e3) {
                f2868a.b((Object) "parse 'star' failed");
            }
            try {
                cVar.d = jSONObject2.getString("tinyurl");
            } catch (Exception e4) {
                f2868a.b((Object) "parse 'tinyurl' failed");
            }
            try {
                cVar.e = jSONObject2.getString("hometown_location");
            } catch (Exception e5) {
                f2868a.b((Object) "parse 'hometown_location' failed");
            }
            try {
                JSONArray jSONArray = jSONObject2.getJSONArray("university_history");
                String[] strArr = new String[jSONArray.length()];
                for (int i = 0; i < jSONArray.length(); i++) {
                    strArr[i] = jSONArray.getString(i);
                }
                cVar.f = strArr;
            } catch (Exception e6) {
                f2868a.a("parse 'university_history' failed", (Throwable) e6);
            }
            try {
                JSONArray jSONArray2 = jSONObject2.getJSONArray("work_history");
                String[] strArr2 = new String[jSONArray2.length()];
                for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                    strArr2[i2] = jSONArray2.getString(i2);
                }
                cVar.g = strArr2;
            } catch (Exception e7) {
                f2868a.b((Object) "parse 'work_history' failed");
            }
            try {
                JSONArray jSONArray3 = jSONObject2.getJSONArray("hs_history");
                String[] strArr3 = new String[jSONArray3.length()];
                for (int i3 = 0; i3 < jSONArray3.length(); i3++) {
                    strArr3[i3] = jSONArray3.getString(i3);
                }
                cVar.h = strArr3;
            } catch (Exception e8) {
                f2868a.b((Object) "parse 'hs_history' failed");
            }
            try {
                cVar.i = jSONObject2.getInt("friends_count");
            } catch (Exception e9) {
                f2868a.b((Object) "parse 'friends_count' failed");
            }
            try {
                cVar.j = jSONObject2.getString("user_id");
            } catch (Exception e10) {
                f2868a.b((Object) "parse 'user_id' failed");
            }
        } catch (Exception e11) {
            f2868a.b((Object) "parse 'profiles' failed");
        }
        try {
            JSONArray jSONArray4 = jSONObject.getJSONArray(LocationManagerProxy.KEY_STATUS_CHANGED);
            ArrayList arrayList = new ArrayList();
            cVar.k = arrayList;
            for (int i4 = 0; i4 < jSONArray4.length(); i4++) {
                try {
                    JSONObject optJSONObject = jSONArray4.optJSONObject(i4);
                    try {
                        b bVar = new b();
                        bVar.f2869a = optJSONObject.getString("message");
                        bVar.b = optJSONObject.getString("time");
                        arrayList.add(bVar);
                    } catch (Exception e12) {
                    }
                } catch (Exception e13) {
                    f2868a.b((Object) ("parse message index[" + i4 + "] failed"));
                }
            }
        } catch (Exception e14) {
            f2868a.b((Object) "parse 'status' failed");
        }
        return cVar;
    }
}
