package com.immomo.momo.plugin.b;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v4.b.a;

public final class e extends a {
    public e(String str) {
        super(str);
    }

    public final void draw(Canvas canvas, CharSequence charSequence, int i, int i2, float f, int i3, int i4, int i5, Paint paint) {
        if (a.a((CharSequence) this.f2862a)) {
            canvas.drawText("[表情]", f, (float) i4, paint);
        } else {
            canvas.drawText("[" + this.f2862a + "]", f, (float) i4, paint);
        }
    }

    public final int getSize(Paint paint, CharSequence charSequence, int i, int i2, Paint.FontMetricsInt fontMetricsInt) {
        return a.a(this.f2862a) ? (int) paint.measureText("[表情]") : (int) paint.measureText("[" + this.f2862a + "]");
    }
}
