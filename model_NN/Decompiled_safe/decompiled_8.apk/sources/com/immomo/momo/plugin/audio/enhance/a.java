package com.immomo.momo.plugin.audio.enhance;

import com.immomo.momo.util.m;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private m f2850a = null;

    public a(String str) {
        this.f2850a = new m(str);
    }

    public a(String str, String str2) {
        this.f2850a = new m(str, str2);
    }

    private static String a() {
        return "[ --- Thread : " + Thread.currentThread().getName() + ":" + Thread.currentThread().getId() + " ] ";
    }

    public static void c(String str) {
        null.f2850a.c(String.valueOf(a()) + str);
    }

    public final void a(String str) {
        this.f2850a.a((Object) (String.valueOf(a()) + str));
    }

    public final void a(Throwable th) {
        this.f2850a.a(a(), th);
    }

    public final void b(String str) {
        this.f2850a.b((Object) (String.valueOf(a()) + str));
    }
}
