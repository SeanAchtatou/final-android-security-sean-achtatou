package com.immomo.momo.plugin.b;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.immomo.momo.R;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.q;
import com.immomo.momo.util.m;
import java.io.File;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    static Map f2864a = new HashMap();
    private static Map b = new HashMap();

    static {
        new m("EmoteLoadUtil");
    }

    static /* synthetic */ Bitmap a(Bitmap bitmap) {
        return bitmap;
    }

    public static Drawable a(String str, String str2, aj ajVar) {
        Bitmap a2;
        Drawable drawable;
        if (b.get(String.valueOf(str2) + str) != null && (drawable = (Drawable) ((WeakReference) b.get(String.valueOf(str2) + str)).get()) != null) {
            return drawable;
        }
        File a3 = q.a(str, str2);
        if (!a3.exists() || (a2 = g.a(a3, (int) R.drawable.zem1)) == null) {
            if (!ajVar.isImageLoading()) {
                ajVar.setImageLoading(true);
                d dVar = (d) f2864a.get(String.valueOf(str2) + str);
                if (dVar == null || dVar.d()) {
                    d dVar2 = new d(str, str2, ajVar.getImageCallback());
                    f2864a.put(String.valueOf(str2) + str, dVar2);
                    dVar2.a();
                } else {
                    dVar.b(ajVar.getImageCallback());
                }
            }
            return null;
        }
        BitmapDrawable bitmapDrawable = new BitmapDrawable(g.l(), a2);
        b.put(String.valueOf(str2) + str, new WeakReference(bitmapDrawable));
        return bitmapDrawable;
    }
}
