package com.immomo.momo.plugin.a;

import com.immomo.momo.R;
import com.immomo.momo.a.w;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.q;
import com.immomo.momo.util.m;
import java.util.HashMap;
import java.util.List;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static a f2841a;
    private static m b = new m("DoubanApi");

    private a() {
    }

    public static a a() {
        if (f2841a == null) {
            f2841a = new a();
        }
        return f2841a;
    }

    public static List a(String str) {
        if (!g.y()) {
            g.a((int) R.string.errormsg_network_unfind);
            throw new w();
        }
        HashMap hashMap = new HashMap();
        hashMap.put("max-results", new StringBuilder(String.valueOf(10)).toString());
        String str2 = new String(q.a("http://api.douban.com/people/$uid/miniblog?alt=json".replace("$uid", str), hashMap, null));
        b.b((Object) str2);
        return b.b(str2);
    }
}
