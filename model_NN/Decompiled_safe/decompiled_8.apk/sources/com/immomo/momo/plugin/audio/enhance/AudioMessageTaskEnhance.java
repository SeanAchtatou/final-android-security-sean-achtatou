package com.immomo.momo.plugin.audio.enhance;

import android.support.v4.b.a;
import com.immomo.a.a.d.i;
import com.immomo.momo.protocol.imjson.task.MessageTask;
import com.immomo.momo.service.bean.Message;
import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

public class AudioMessageTaskEnhance extends MessageTask {

    /* renamed from: a  reason: collision with root package name */
    protected File f2848a;
    int b;
    /* access modifiers changed from: private */
    public boolean e;
    /* access modifiers changed from: private */
    public TimerTask f;

    static /* synthetic */ Timer a() {
        return null;
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [java.util.Timer, java.util.TimerTask] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private boolean a(com.immomo.momo.service.bean.Message r6) {
        /*
            r5 = this;
            r1 = 1
            r0 = 0
            r4 = 0
            java.io.File r2 = r5.f2848a
            if (r2 != 0) goto L_0x001e
            java.lang.String r2 = r6.fileName
            java.lang.String r3 = "file://"
            boolean r2 = r2.startsWith(r3)
            if (r2 == 0) goto L_0x001e
            java.io.File r2 = new java.io.File
            java.lang.String r3 = r6.fileName
            java.net.URI r3 = java.net.URI.create(r3)
            r2.<init>(r3)
            r5.f2848a = r2
        L_0x001e:
            java.io.File r2 = r5.f2848a
            if (r2 == 0) goto L_0x0059
            r2 = r1
        L_0x0023:
            if (r2 == 0) goto L_0x005b
        L_0x0025:
            r5.e = r0
            if (r2 == 0) goto L_0x0056
            java.util.TimerTask r0 = r5.f
            if (r0 == 0) goto L_0x0037
            java.util.TimerTask r0 = r5.f
            r0.cancel()
            r5.f = r4
            r4.purge()
        L_0x0037:
            com.immomo.momo.plugin.audio.enhance.c r0 = new com.immomo.momo.plugin.audio.enhance.c
            r0.<init>(r5)
            r5.f = r0
            java.util.TimerTask r0 = r5.f
            r1 = 120000(0x1d4c0, double:5.9288E-319)
            r4.schedule(r0, r1)
            com.immomo.momo.plugin.audio.enhance.b r0 = new com.immomo.momo.plugin.audio.enhance.b
            r0.<init>(r5, r6)
            r0.start()
            r0 = 0
            monitor-enter(r0)     // Catch:{ InterruptedException -> 0x0061 }
            r0 = 0
            r0.wait()     // Catch:{ all -> 0x005d }
            r0 = 0
            monitor-exit(r0)     // Catch:{ all -> 0x005d }
        L_0x0056:
            boolean r0 = r5.e
            return r0
        L_0x0059:
            r2 = r0
            goto L_0x0023
        L_0x005b:
            r0 = r1
            goto L_0x0025
        L_0x005d:
            r0 = move-exception
            r1 = 0
            monitor-exit(r1)     // Catch:{ InterruptedException -> 0x0061 }
            throw r0     // Catch:{ InterruptedException -> 0x0061 }
        L_0x0061:
            r0 = move-exception
            com.immomo.momo.util.m r1 = r5.c
            r1.a(r0)
            goto L_0x0056
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.plugin.audio.enhance.AudioMessageTaskEnhance.a(com.immomo.momo.service.bean.Message):boolean");
    }

    static /* synthetic */ Object b() {
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a(Message message, i iVar) {
        if (!a(message)) {
            throw new Exception("audio upload failed");
        }
        iVar.c(a.a(a.a(a.a(a.a(a.a(String.valueOf(com.immomo.momo.a.f) + "/chataudio", "mode", "GUID"), "filesize", new StringBuilder(String.valueOf(message.fileSize)).toString()), "file", message.fileName), "audiotime", new StringBuilder(String.valueOf(message.audiotime)).toString()), "ext", message.expandedName));
    }

    public final boolean a(com.immomo.a.a.a aVar) {
        e();
        return super.a(aVar);
    }
}
