package com.immomo.momo.plugin.d;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public final class c implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public String f2870a;
    public String b;
    public String c;
    public String d;
    public String e;
    public String[] f;
    public String[] g;
    public String[] h;
    public int i;
    public String j;
    public List k;

    public final String toString() {
        return "RenrenUser [name=" + this.f2870a + ", gender=" + this.b + ", star=" + this.c + ", tinyunrl=" + this.d + ", location=" + this.e + ", universityHistory=" + Arrays.toString(this.f) + ", workHistory=" + Arrays.toString(this.g) + ", hsHistory=" + Arrays.toString(this.h) + ", friendsCount=" + this.i + ", id=" + this.j + ", messages=" + this.k + "]";
    }
}
