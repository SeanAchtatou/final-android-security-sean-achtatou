package com.immomo.momo.plugin.audio;

import android.media.MediaPlayer;

final class c implements MediaPlayer.OnErrorListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f2846a;

    c(a aVar) {
        this.f2846a = aVar;
    }

    public final boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        this.f2846a.f.a("@@@@@@@@@@@@@@@@@@ mediaPlayer onError what:" + i + " extra:" + i2, (Throwable) null);
        this.f2846a.g();
        this.f2846a.i();
        return true;
    }
}
