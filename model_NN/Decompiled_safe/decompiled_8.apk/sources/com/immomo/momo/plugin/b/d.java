package com.immomo.momo.plugin.b;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import com.immomo.momo.R;
import com.immomo.momo.android.c.g;
import com.immomo.momo.android.c.i;
import java.io.File;
import java.util.HashSet;
import java.util.Set;

public final class d extends i {

    /* renamed from: a  reason: collision with root package name */
    private Set f2865a;

    public d(String str, String str2, g gVar) {
        super(str, str2, null, (byte) 0);
        this.f2865a = null;
        this.f2865a = new HashSet();
        b(gVar);
    }

    public final /* synthetic */ void a(Object obj) {
        BitmapDrawable bitmapDrawable = null;
        File file = (File) obj;
        c.f2864a.remove(this);
        if (file != null) {
            try {
                Bitmap a2 = com.immomo.momo.g.a(file, (int) R.drawable.zem1);
                if (a2 != null) {
                    bitmapDrawable = new BitmapDrawable(com.immomo.momo.g.l(), c.a(a2));
                }
            } catch (Throwable th) {
                Throwable th2 = th;
                for (g a3 : this.f2865a) {
                    a3.a(null);
                }
                throw th2;
            }
        }
        for (g a4 : this.f2865a) {
            a4.a(bitmapDrawable);
        }
    }

    public final void b(g gVar) {
        this.f2865a.add(gVar);
    }
}
