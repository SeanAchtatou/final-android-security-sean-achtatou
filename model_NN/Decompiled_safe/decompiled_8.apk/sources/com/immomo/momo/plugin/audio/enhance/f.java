package com.immomo.momo.plugin.audio.enhance;

import java.util.concurrent.TimeUnit;

public final class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private int f2854a;
    private /* synthetic */ MomoAudioPlayer b;

    public f(MomoAudioPlayer momoAudioPlayer) {
        this.b = momoAudioPlayer;
    }

    private void c() {
        MomoAudioPlayer momoAudioPlayer = this.b;
        MomoAudioPlayer.j().b("Player - onTaskFinish - 本次播放帧数:" + this.f2854a + " " + this);
        MomoAudioPlayer.g(this.b);
        MomoAudioPlayer momoAudioPlayer2 = this.b;
        MomoAudioPlayer.l().clear();
        MomoAudioPlayer momoAudioPlayer3 = this.b;
        MomoAudioPlayer.m().sendEmptyMessage(4);
    }

    public final void a() {
        this.f2854a = 0;
        new Thread(this).start();
    }

    public final int b() {
        return this.f2854a;
    }

    public final void run() {
        try {
            if (!this.b.n) {
                MomoAudioPlayer momoAudioPlayer = this.b;
                MomoAudioPlayer.j().b("Player - 我执行了");
                Thread.sleep(200);
                MomoAudioPlayer momoAudioPlayer2 = this.b;
                MomoAudioPlayer.j().b("Player - executeTask");
                while (true) {
                    if (!this.b.o) {
                        break;
                    }
                    MomoAudioPlayer momoAudioPlayer3 = this.b;
                    d dVar = (d) MomoAudioPlayer.l().poll(100, TimeUnit.MILLISECONDS);
                    if (this.b.j <= -100) {
                        MomoAudioPlayer momoAudioPlayer4 = this.b;
                        MomoAudioPlayer.j().b("Player - 解码出错，播放退出");
                        break;
                    } else if (dVar != null) {
                        if (dVar.a()) {
                            MomoAudioPlayer momoAudioPlayer5 = this.b;
                            MomoAudioPlayer.j().b("Player - 结束包，停止播放 " + this);
                            break;
                        }
                        byte[] b2 = dVar.b();
                        this.b.f.write(b2, 0, b2.length);
                        this.f2854a++;
                    }
                }
                c();
            } else {
                MomoAudioPlayer momoAudioPlayer6 = this.b;
                MomoAudioPlayer.j().b("Player - 我还没执行就停止了");
            }
        } catch (Exception e) {
            c();
            MomoAudioPlayer momoAudioPlayer7 = this.b;
            MomoAudioPlayer.j().b("Player - onTaskError - 播放异常");
            MomoAudioPlayer momoAudioPlayer8 = this.b;
            MomoAudioPlayer.j().a(e);
            if (e instanceof InterruptedException) {
                MomoAudioPlayer momoAudioPlayer9 = this.b;
                MomoAudioPlayer.j().b("Player - onTaskError - 从队列获取播放数据被打断");
                MomoAudioPlayer momoAudioPlayer10 = this.b;
                MomoAudioPlayer.m().sendEmptyMessage(-301);
            } else if (e instanceof RuntimeException) {
                MomoAudioPlayer momoAudioPlayer11 = this.b;
                MomoAudioPlayer.j().b("Player - onTaskError - 没有获取到PCM数据");
                MomoAudioPlayer momoAudioPlayer12 = this.b;
                MomoAudioPlayer.m().sendEmptyMessage(-302);
            } else {
                MomoAudioPlayer momoAudioPlayer13 = this.b;
                MomoAudioPlayer.j().b("Player - onTaskError - 其他错误");
                MomoAudioPlayer momoAudioPlayer14 = this.b;
                MomoAudioPlayer.m().sendEmptyMessage(-300);
            }
        } finally {
            MomoAudioPlayer momoAudioPlayer15 = this.b;
            MomoAudioPlayer.k().countDown();
        }
    }
}
