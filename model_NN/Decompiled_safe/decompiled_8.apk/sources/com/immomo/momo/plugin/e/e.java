package com.immomo.momo.plugin.e;

import com.immomo.momo.service.bean.al;
import java.io.Serializable;

public final class e extends al implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public String f2875a;
    public String b;
    public String c;
    public String d;
    public String e;
    public String f;
    public String g;
    public String h;
    public String i;
    public String j;
    public int k;
    public int l;
    public int m;
    public boolean n;
    public String o;

    public final String getLoadImageId() {
        return this.h;
    }

    public final boolean isImageUrl() {
        return true;
    }

    public final String toString() {
        return "WeiboUser [id=" + this.f2875a + ", idStr=" + ((String) null) + ", screenName=" + this.b + ", name=" + this.c + ", province=" + this.d + ", city=" + this.e + ", location=" + ((String) null) + ", description=" + this.f + ", url=" + this.g + ", profileImageUrl=" + this.h + ", avatarLargeUrl=" + ((String) null) + ", domain=" + this.i + ", gender=" + this.j + ", followers_count=" + this.k + ", friends_count=" + this.l + ", statuses_count=" + this.m + ", favourites_count=0, createdAt=" + ((Object) null) + ", following=false, allowAllActMsg=false, geoEnabled=false, verified=" + this.n + ", verifiedType=0, verifiedReason=" + this.o + ", remark=" + ((String) null) + ", allowAllComment=false, followMe=false, onlineStatus=0, biFollowersCount=0, lang=" + ((String) null) + ", momoid=" + ((String) null) + "]";
    }
}
