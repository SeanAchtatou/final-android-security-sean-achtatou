package com.immomo.momo.plugin.audio.enhance;

import java.io.File;

public class AMRDecoder implements e {
    private a mlog$29b0dd6d = new a("[MomoAMRDecoder]");

    private native int _amr_decode(String str);

    private native void _amr_decode_deinit();

    private native void _amr_decode_init();

    private native void _amr_decode_stop();

    private native void _amr_set_data_handler(AudioDataHandler audioDataHandler);

    private native void _amr_skip(int i);

    public int decode(File file) {
        this.mlog$29b0dd6d.a("decode : " + file.getAbsolutePath());
        return _amr_decode(file.getAbsolutePath());
    }

    public void deinit() {
        _amr_decode_deinit();
    }

    public void init() {
        _amr_decode_init();
    }

    public void seek(int i) {
        _amr_skip(i);
    }

    public void setDataHandler(AudioDataHandler audioDataHandler) {
        _amr_set_data_handler(audioDataHandler);
    }

    public void stop() {
        _amr_decode_stop();
    }
}
