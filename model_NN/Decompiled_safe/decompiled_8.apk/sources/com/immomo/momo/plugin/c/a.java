package com.immomo.momo.plugin.c;

import android.graphics.Bitmap;
import java.util.Date;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    public Bitmap f2866a;
    private String b;
    private String c;
    private Date d;

    public final Date a() {
        return this.d;
    }

    public final void a(String str) {
        this.b = str;
    }

    public final void a(Date date) {
        this.d = date;
    }

    public final String b() {
        return this.b;
    }

    public final void b(String str) {
        this.c = str;
    }

    public final String c() {
        return this.c;
    }
}
