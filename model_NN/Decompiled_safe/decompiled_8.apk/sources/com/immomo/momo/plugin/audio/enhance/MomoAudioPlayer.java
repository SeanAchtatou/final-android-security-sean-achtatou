package com.immomo.momo.plugin.audio.enhance;

import android.media.AudioTrack;
import android.os.Handler;
import com.immomo.momo.plugin.audio.g;
import java.io.File;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;

public final class MomoAudioPlayer extends g {
    /* access modifiers changed from: private */
    public AudioTrack f;
    /* access modifiers changed from: private */
    public File g;
    private DecodeTask h;
    private f i;
    /* access modifiers changed from: private */
    public int j;
    private int k;
    private int l;
    /* access modifiers changed from: private */
    public int m;
    /* access modifiers changed from: private */
    public volatile boolean n;
    /* access modifiers changed from: private */
    public volatile boolean o;
    /* access modifiers changed from: private */
    public volatile boolean p;

    class DecodeTask implements AudioDataHandler, Runnable {

        /* renamed from: a  reason: collision with root package name */
        private e f2849a = new AMRDecoder();
        private int b;

        public DecodeTask() {
        }

        private void c() {
            MomoAudioPlayer momoAudioPlayer = MomoAudioPlayer.this;
            MomoAudioPlayer.j().b("Decoder - onTaskFinish - 解码完毕 ，本次解码帧数：" + this.b + " " + this);
            this.f2849a.deinit();
            MomoAudioPlayer.this.p = false;
        }

        public final void a() {
            new Thread(this).start();
        }

        public final void b() {
            this.f2849a.stop();
        }

        public void handle(byte[] bArr) {
            d dVar;
            if (bArr == null) {
                MomoAudioPlayer momoAudioPlayer = MomoAudioPlayer.this;
                MomoAudioPlayer.j().b("Decoder - no more data from decoder");
                dVar = new d(true, new byte[0]);
            } else {
                dVar = new d(false, bArr);
            }
            try {
                MomoAudioPlayer momoAudioPlayer2 = MomoAudioPlayer.this;
                MomoAudioPlayer.l().put(dVar);
            } catch (InterruptedException e) {
                MomoAudioPlayer momoAudioPlayer3 = MomoAudioPlayer.this;
                MomoAudioPlayer.j().a(e);
            }
            this.b++;
        }

        public void handle(short[] sArr) {
        }

        public void run() {
            try {
                if (!MomoAudioPlayer.this.n) {
                    MomoAudioPlayer momoAudioPlayer = MomoAudioPlayer.this;
                    MomoAudioPlayer.j().b("Decoder - 我执行了");
                    this.b = 0;
                    MomoAudioPlayer.this.p = true;
                    this.f2849a.init();
                    this.f2849a.setDataHandler(this);
                    this.f2849a.seek(MomoAudioPlayer.this.m);
                    MomoAudioPlayer momoAudioPlayer2 = MomoAudioPlayer.this;
                    MomoAudioPlayer.j().b("Decoder - executeTask");
                    this.b = this.f2849a.decode(MomoAudioPlayer.this.g);
                    MomoAudioPlayer momoAudioPlayer3 = MomoAudioPlayer.this;
                    MomoAudioPlayer.l().put(new d(true, new byte[0]));
                    int i = this.b;
                    c();
                } else {
                    MomoAudioPlayer momoAudioPlayer4 = MomoAudioPlayer.this;
                    MomoAudioPlayer.j().b("Decoder - 我还没执行就停止了");
                }
            } catch (Exception e) {
                c();
                MomoAudioPlayer momoAudioPlayer5 = MomoAudioPlayer.this;
                MomoAudioPlayer.j().b("Decoder - onTaskError - 解码异常");
                MomoAudioPlayer momoAudioPlayer6 = MomoAudioPlayer.this;
                MomoAudioPlayer.j().a(e);
                if (e instanceof InterruptedException) {
                    MomoAudioPlayer momoAudioPlayer7 = MomoAudioPlayer.this;
                    MomoAudioPlayer.j().b("Decoder - onTaskError - 向队列存放解码数据被打断");
                    MomoAudioPlayer momoAudioPlayer8 = MomoAudioPlayer.this;
                    MomoAudioPlayer.m().sendEmptyMessage(-202);
                } else if (e instanceof AudioException) {
                    MomoAudioPlayer momoAudioPlayer9 = MomoAudioPlayer.this;
                    MomoAudioPlayer.j().b("Decoder - onTaskError - 解码错误");
                    MomoAudioPlayer momoAudioPlayer10 = MomoAudioPlayer.this;
                    MomoAudioPlayer.m().sendEmptyMessage(-201);
                } else {
                    MomoAudioPlayer momoAudioPlayer11 = MomoAudioPlayer.this;
                    MomoAudioPlayer.j().b("Decoder - onTaskError - 其他错误");
                    MomoAudioPlayer momoAudioPlayer12 = MomoAudioPlayer.this;
                    MomoAudioPlayer.m().sendEmptyMessage(-200);
                }
            } finally {
                MomoAudioPlayer momoAudioPlayer13 = MomoAudioPlayer.this;
                MomoAudioPlayer.k().countDown();
            }
        }
    }

    static {
        System.loadLibrary("nativeaudio");
    }

    private void c(int i2) {
        a aVar = null;
        aVar.a("onError : " + i2 + " " + this);
        this.j = 0;
        if (this.c != null) {
            this.c.d();
        }
    }

    static /* synthetic */ void g(MomoAudioPlayer momoAudioPlayer) {
        a aVar = null;
        aVar.a("stopTrack " + momoAudioPlayer);
        momoAudioPlayer.f.stop();
        momoAudioPlayer.f.flush();
        momoAudioPlayer.n();
    }

    static /* synthetic */ a j() {
        return null;
    }

    static /* synthetic */ CountDownLatch k() {
        return null;
    }

    static /* synthetic */ BlockingQueue l() {
        return null;
    }

    static /* synthetic */ Handler m() {
        return null;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [com.immomo.momo.plugin.audio.enhance.a, android.media.AudioTrack] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private void n() {
        /*
            r3 = this;
            r2 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "release "
            r0.<init>(r1)
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r0 = r0.toString()
            r2.a(r0)
            android.media.AudioTrack r0 = r3.f
            r0.release()
            r3.f = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.plugin.audio.enhance.MomoAudioPlayer.n():void");
    }

    public final void a() {
        a aVar = null;
        aVar.a("stop, cur state : " + this.j + " " + this);
        this.h.b();
        this.n = true;
        this.o = false;
        if (this.j == 2) {
            aVar.a("onStop " + this);
            this.j = 3;
            if (this.c != null) {
                this.c.b();
            }
            try {
                long currentTimeMillis = System.currentTimeMillis();
                CountDownLatch countDownLatch = null;
                countDownLatch.await();
                a aVar2 = null;
                aVar2.a("等待时间 : " + (System.currentTimeMillis() - currentTimeMillis) + "毫秒");
            } catch (InterruptedException e) {
                aVar.a(e);
            }
        }
    }

    public final void a(int i2) {
        this.k = i2;
    }

    public final void a(File file) {
        this.g = file;
    }

    public final void b() {
    }

    public final void b(int i2) {
        this.m = i2;
    }

    public final int c() {
        return this.i == null ? this.m : this.m + this.i.b();
    }

    public final boolean d() {
        return this.j == 2;
    }

    public final void e() {
        a aVar = null;
        if (this.j == 2) {
            a.c("already start");
        } else if (this.j <= 0) {
            throw new IllegalStateException("start() called on an uninitialized AudioPlayer.");
        } else if (this.g == null || !this.g.exists()) {
            n();
            c(-103);
        } else {
            this.l = AudioTrack.getMinBufferSize(0, 0, 0);
            this.f = new AudioTrack(this.k, 0, 0, 0, this.l, 1);
            if (this.f.getState() != 1) {
                n();
                c(-101);
                return;
            }
            this.f.play();
            int playState = this.f.getPlayState();
            if (playState == 3) {
                this.o = true;
                this.h = new DecodeTask();
                this.h.a();
                this.i = new f(this);
                this.i.a();
            }
            if (playState == 3) {
                aVar.a("start playing successed " + this);
                f();
                return;
            }
            aVar.a("start playing failed");
            n();
            c(-102);
        }
    }

    /* access modifiers changed from: protected */
    public final void f() {
        a aVar = null;
        aVar.a("onStart " + this);
        this.j = 2;
        if (this.c != null) {
            this.c.a();
        }
    }
}
