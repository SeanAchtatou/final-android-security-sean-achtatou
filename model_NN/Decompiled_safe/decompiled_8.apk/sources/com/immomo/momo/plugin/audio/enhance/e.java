package com.immomo.momo.plugin.audio.enhance;

import java.io.File;

public interface e {
    int decode(File file);

    void deinit();

    void init();

    void seek(int i);

    void setDataHandler(AudioDataHandler audioDataHandler);

    void stop();
}
