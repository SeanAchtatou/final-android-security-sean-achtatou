package com.immomo.momo.plugin.e;

import android.support.v4.b.a;
import com.amap.mapapi.location.LocationManagerProxy;
import com.immomo.momo.util.m;
import com.sina.sdk.api.message.InviteApi;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private static m f2874a = new m("WeiboJsonParser").a();

    public static List a(String str) {
        ArrayList arrayList = new ArrayList();
        JSONObject jSONObject = new JSONObject(str);
        if (jSONObject.has(LocationManagerProxy.KEY_STATUS_CHANGED)) {
            JSONArray jSONArray = new JSONArray(jSONObject.getString(LocationManagerProxy.KEY_STATUS_CHANGED));
            for (int i = 0; i < jSONArray.length(); i++) {
                arrayList.add(b(jSONArray.getJSONObject(i).toString()));
            }
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Exception]
     candidates:
      com.immomo.momo.util.m.a(java.lang.Appendable, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.Object, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.String, java.io.File):void
      com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void */
    public static void a(e eVar, String str) {
        JSONObject jSONObject = new JSONObject(str);
        try {
            eVar.b = new String(jSONObject.getString("screen_name").getBytes(), "UTF-8");
        } catch (Exception e) {
            f2874a.a("parse 'screen_name' failed", (Throwable) e);
        }
        try {
            eVar.f2875a = jSONObject.getString("id");
        } catch (Exception e2) {
            f2874a.a("parse 'id' failed", (Throwable) e2);
        }
        try {
            eVar.g = jSONObject.getString(InviteApi.KEY_URL);
        } catch (Exception e3) {
            f2874a.a("parse 'url' failed", (Throwable) e3);
        }
        try {
            eVar.f = new String(jSONObject.getString("description").getBytes(), "UTF-8");
        } catch (Exception e4) {
            f2874a.a("parse 'description' failed", (Throwable) e4);
        }
        try {
            eVar.d = a.c(jSONObject.getInt("province"));
        } catch (Exception e5) {
            f2874a.a("parse 'province' failed", (Throwable) e5);
        }
        try {
            eVar.e = a.c(jSONObject.getInt("city"));
        } catch (Exception e6) {
            f2874a.a("parse 'city' failed", (Throwable) e6);
        }
        try {
            eVar.k = jSONObject.getInt("followers_count");
        } catch (Exception e7) {
            f2874a.a("parse 'followers_count' failed", (Throwable) e7);
        }
        try {
            eVar.l = jSONObject.getInt("friends_count");
        } catch (Exception e8) {
            f2874a.a("parse 'friends_count' failed", (Throwable) e8);
        }
        try {
            eVar.m = jSONObject.getInt("statuses_count");
        } catch (Exception e9) {
            f2874a.a("parse 'statuses_count' failed", (Throwable) e9);
        }
        try {
            eVar.j = jSONObject.getString("gender");
        } catch (Exception e10) {
            f2874a.a("parse 'gender' failed", (Throwable) e10);
        }
        try {
            eVar.h = jSONObject.getString("profile_image_url");
        } catch (Exception e11) {
            f2874a.a("parse 'profile_image_url' failed", (Throwable) e11);
        }
        try {
            eVar.i = jSONObject.getString("domain");
        } catch (Exception e12) {
            f2874a.a("parse 'domain' failed", (Throwable) e12);
        }
        try {
            eVar.o = jSONObject.getString("sina_vip_desc");
            if (!a.a((CharSequence) eVar.o)) {
                eVar.n = true;
            }
        } catch (Exception e13) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Exception]
     candidates:
      com.immomo.momo.util.m.a(java.lang.Appendable, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.Object, java.lang.Throwable):void
      com.immomo.momo.util.m.a(java.lang.String, java.io.File):void
      com.immomo.momo.util.m.a(java.lang.String, java.lang.Throwable):void */
    private static c b(String str) {
        c cVar = new c();
        JSONObject jSONObject = new JSONObject(str);
        try {
            cVar.e = jSONObject.getLong("timestamp");
        } catch (Exception e) {
            f2874a.a("parse 'created_at' failed", (Throwable) e);
        }
        try {
            cVar.f2873a = jSONObject.getString("id");
        } catch (Exception e2) {
            f2874a.a("parse 'id' failed", (Throwable) e2);
        }
        try {
            cVar.b = jSONObject.getString(InviteApi.KEY_TEXT);
        } catch (Exception e3) {
            f2874a.a("parse 'text' failed", (Throwable) e3);
        }
        try {
            cVar.d = jSONObject.getString("origtext");
        } catch (Exception e4) {
            f2874a.a("parse 'text' failed", (Throwable) e4);
        }
        try {
            cVar.c = jSONObject.getString("source_nick");
        } catch (Exception e5) {
            f2874a.a("parse 'text' failed", (Throwable) e5);
        }
        try {
            cVar.f = jSONObject.getString("image");
            if (!a.a((CharSequence) cVar.f)) {
                cVar.h = true;
            }
        } catch (Exception e6) {
            f2874a.a("parse 'thumbnail_pic' failed", (Throwable) e6);
        }
        try {
            cVar.g = jSONObject.getString("image_large");
        } catch (Exception e7) {
            f2874a.a("parse 'thumbnail_pic' failed", (Throwable) e7);
        }
        return cVar;
    }
}
