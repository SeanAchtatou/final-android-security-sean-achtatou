package com.immomo.momo.plugin.g;

import android.content.Context;
import com.immomo.momo.g;
import com.tencent.mm.sdk.openapi.WXMediaMessage;
import com.tencent.mm.sdk.openapi.WXWebpageObject;
import com.tencent.mm.sdk.openapi.b;
import com.tencent.mm.sdk.openapi.c;
import com.tencent.mm.sdk.openapi.d;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static a f2879a;
    private b b;

    private a(Context context) {
        this.b = d.a(context, "wx53440afb924e0ace");
        this.b.a("wx53440afb924e0ace");
    }

    public static a a() {
        if (f2879a == null) {
            f2879a = new a(g.c());
        }
        return f2879a;
    }

    private static c b(String str, String str2, File file) {
        File a2;
        WXWebpageObject wXWebpageObject = new WXWebpageObject();
        wXWebpageObject.webpageUrl = str;
        WXMediaMessage wXMediaMessage = new WXMediaMessage();
        wXMediaMessage.title = str2;
        wXMediaMessage.description = str2;
        wXMediaMessage.mediaObject = wXWebpageObject;
        if (!(file == null || (a2 = android.support.v4.b.a.a(file)) == null)) {
            try {
                wXMediaMessage.thumbData = android.support.v4.b.a.b(new FileInputStream(a2));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        c cVar = new c();
        cVar.f3117a = new StringBuilder(String.valueOf(System.currentTimeMillis())).toString();
        cVar.b = wXMediaMessage;
        return cVar;
    }

    public final void a(String str, String str2, File file) {
        c b2 = b(str, str2, file);
        b2.c = 1;
        this.b.a(b2);
    }
}
