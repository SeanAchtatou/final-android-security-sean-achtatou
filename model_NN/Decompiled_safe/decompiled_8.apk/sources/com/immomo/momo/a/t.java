package com.immomo.momo.a;

import com.immomo.momo.R;
import com.immomo.momo.g;

public final class t extends a {
    public t(Throwable th) {
        super(g.a((int) R.string.errormsg_network_https_certificate), th);
    }

    public t(Throwable th, String str) {
        super(str, th);
    }
}
