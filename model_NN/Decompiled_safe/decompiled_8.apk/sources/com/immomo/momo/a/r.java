package com.immomo.momo.a;

import com.immomo.momo.R;
import com.immomo.momo.g;

public final class r extends a {
    private int b = -1;
    private String c;

    public r(String str, int i) {
        super(String.valueOf(g.a((int) R.string.errormsg_http_statuserror)) + "(" + i + ")");
        this.b = i;
        this.c = str;
    }

    public final String toString() {
        String localizedMessage = getLocalizedMessage();
        String name = getClass().getName();
        return localizedMessage == null ? name : String.valueOf(name) + ": " + this.c + "[" + this.b + "]";
    }
}
