package com.immomo.momo;

import android.annotation.SuppressLint;
import android.content.ClipboardManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.support.v4.b.a;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.service.bean.at;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.ak;
import com.immomo.momo.util.m;
import com.mapabc.minimap.map.vmap.NativeMapEngine;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.Locale;
import java.util.regex.Pattern;

public final class g {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f2840a;
    private static Context b = null;
    private static ContentResolver c = null;
    private static String d = null;
    private static Float e = null;
    private static LayoutInflater f = null;
    private static m g = new m("Momokit");
    private static Bitmap h = null;
    private static Bitmap i = null;
    private static Bitmap j = null;
    private static String k = null;
    private static Uri l = Uri.parse("content://telephony/carriers/preferapn");
    private static boolean m = false;

    static {
        f2840a = false;
        if ("GT-I9100".equals(Build.MODEL) || "GT-I9100G".equals(Build.MODEL)) {
            f2840a = true;
        }
    }

    public static String A() {
        try {
            return b.getPackageManager().getPackageInfo(b.getPackageName(), 16384).versionName;
        } catch (Exception e2) {
            g.a((Throwable) e2);
            return PoiTypeDef.All;
        }
    }

    public static int B() {
        return Build.VERSION.SDK_INT;
    }

    public static String C() {
        return Build.VERSION.RELEASE;
    }

    public static String D() {
        return ((TelephonyManager) b.getSystemService("phone")).getDeviceId();
    }

    public static String E() {
        return ((WifiManager) b.getSystemService("wifi")).getConnectionInfo().getMacAddress();
    }

    public static String F() {
        return Build.MODEL;
    }

    public static String G() {
        return Build.BRAND;
    }

    public static String H() {
        String str = Build.FINGERPRINT;
        int lastIndexOf = str.lastIndexOf(":");
        if (lastIndexOf <= 0) {
            return str;
        }
        String[] split = str.substring(0, lastIndexOf).split("/");
        return split.length > 2 ? String.valueOf(String.valueOf(PoiTypeDef.All) + split[split.length - 2] + "/") + split[split.length - 1] : str;
    }

    public static String I() {
        String D = D();
        return a.f(D) ? c(D) : PoiTypeDef.All;
    }

    public static int J() {
        return ae().widthPixels;
    }

    public static boolean K() {
        return "1".equals(Settings.Secure.getString(e(), "mock_location"));
    }

    public static int L() {
        return ae().heightPixels;
    }

    public static String M() {
        return Locale.getDefault().getCountry();
    }

    public static String N() {
        return Locale.getDefault().getLanguage();
    }

    public static String O() {
        if (a.a((CharSequence) k)) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("MomoChat/").append(A()).append(" Android/").append(z()).append(" (").append(String.valueOf(Build.MODEL) + ";").append(" ").append("Android " + Build.VERSION.RELEASE + ";").append(" ").append("Gapps " + (v() ? 1 : 0) + ";").append(" ").append(String.valueOf(Locale.getDefault().getLanguage()) + "_" + Locale.getDefault().getCountry() + ";").append(" ").append(g()).append(")");
            k = stringBuffer.toString();
        }
        return k;
    }

    public static boolean P() {
        ak akVar = ((MomoApplication) b).f668a;
        boolean z = akVar.a("guide_", 9) < Q();
        return Q() <= 10 ? z && !akVar.a("guide_10", false) : z;
    }

    public static int Q() {
        try {
            return b.getPackageManager().getApplicationInfo(h(), NativeMapEngine.MAX_ICON_SIZE).metaData.getInt("userguide_code", 10);
        } catch (Exception e2) {
            return 10;
        }
    }

    public static int R() {
        TelephonyManager telephonyManager = (TelephonyManager) b.getSystemService("phone");
        if (telephonyManager != null) {
            return telephonyManager.getPhoneType();
        }
        return -1;
    }

    public static boolean S() {
        return R() == 2;
    }

    public static String T() {
        int R = R();
        return R == 2 ? "CDMA" : R == 1 ? "GSM" : R == 3 ? "SIP" : R == 0 ? "UNKNOW" : "UNKNOW";
    }

    public static String U() {
        TelephonyManager telephonyManager = (TelephonyManager) b.getSystemService("phone");
        return telephonyManager != null ? telephonyManager.getNetworkOperatorName() : "UNKNOW";
    }

    public static String V() {
        return new StringBuilder(String.valueOf(((TelephonyManager) b.getSystemService("phone")).getNetworkType())).toString();
    }

    public static String W() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                Enumeration<InetAddress> inetAddresses = networkInterfaces.nextElement().getInetAddresses();
                while (true) {
                    if (inetAddresses.hasMoreElements()) {
                        InetAddress nextElement = inetAddresses.nextElement();
                        String hostAddress = nextElement.getHostAddress();
                        String[] split = hostAddress.split("\\.");
                        if (!nextElement.isLoopbackAddress() && !nextElement.isLinkLocalAddress()) {
                            try {
                                Integer.parseInt(split[0]);
                                return hostAddress;
                            } catch (Exception e2) {
                            }
                        }
                    }
                }
            }
        } catch (Exception e3) {
        }
        return PoiTypeDef.All;
    }

    public static String X() {
        try {
            String str = b.getApplicationInfo().publicSourceDir;
            Class<?> cls = Class.forName("android.content.pm.PackageParser");
            Method method = cls.getMethod("parsePackage", File.class, String.class, DisplayMetrics.class, Integer.TYPE);
            Object newInstance = cls.getConstructor(String.class).newInstance(PoiTypeDef.All);
            Object invoke = method.invoke(newInstance, new File(str), null, b.getResources().getDisplayMetrics(), 4);
            cls.getMethod("collectCertificates", Class.forName("android.content.pm.PackageParser$Package"), Integer.TYPE).invoke(newInstance, invoke, 64);
            Signature[] signatureArr = (Signature[]) invoke.getClass().getField("mSignatures").get(invoke);
            Signature signature = signatureArr.length > 0 ? signatureArr[0] : null;
            if (signature != null) {
                return a.n(signature.toCharsString());
            }
        } catch (Exception e2) {
        }
        return null;
    }

    public static String Y() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) b.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null) {
            if (activeNetworkInfo.getType() == 1) {
                return "wifi";
            }
            if (activeNetworkInfo.getType() == 0) {
                return "mobile";
            }
        }
        return null;
    }

    public static int Z() {
        try {
            return ((TelephonyManager) b.getSystemService("phone")).getNetworkType();
        } catch (Exception e2) {
            return -1;
        }
    }

    public static int a(float f2) {
        return Math.round(TypedValue.applyDimension(1, f2, ae()));
    }

    public static int a(String str) {
        return b.getResources().getIdentifier(str, null, b.getPackageName());
    }

    public static Bitmap a(File file, int i2) {
        if (!file.exists()) {
            return null;
        }
        try {
            return a(new FileInputStream(file), i2);
        } catch (FileNotFoundException e2) {
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
     arg types: [int, android.util.TypedValue, int]
     candidates:
      ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
      ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException} */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap a(java.io.InputStream r5, int r6) {
        /*
            if (r6 >= 0) goto L_0x0005
            r6 = 2130837505(0x7f020001, float:1.7279966E38)
        L_0x0005:
            android.graphics.Rect r1 = new android.graphics.Rect     // Catch:{ Throwable -> 0x005d }
            r1.<init>()     // Catch:{ Throwable -> 0x005d }
            android.graphics.BitmapFactory$Options r2 = new android.graphics.BitmapFactory$Options     // Catch:{ Throwable -> 0x005d }
            r2.<init>()     // Catch:{ Throwable -> 0x005d }
            java.lang.Class<android.util.DisplayMetrics> r0 = android.util.DisplayMetrics.class
            java.lang.String r3 = "getDeviceDensity"
            r4 = 0
            java.lang.Class[] r4 = new java.lang.Class[r4]     // Catch:{ Exception -> 0x0056 }
            java.lang.reflect.Method r0 = r0.getDeclaredMethod(r3, r4)     // Catch:{ Exception -> 0x0056 }
            r3 = 1
            r0.setAccessible(r3)     // Catch:{ Exception -> 0x0056 }
            java.lang.Class<android.util.DisplayMetrics> r3 = android.util.DisplayMetrics.class
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x0056 }
            java.lang.Object r0 = r0.invoke(r3, r4)     // Catch:{ Exception -> 0x0056 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x0056 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0056 }
            r2.inScreenDensity = r0     // Catch:{ Exception -> 0x0056 }
        L_0x002f:
            android.util.TypedValue r0 = new android.util.TypedValue     // Catch:{ Throwable -> 0x005d }
            r0.<init>()     // Catch:{ Throwable -> 0x005d }
            android.content.Context r3 = com.immomo.momo.g.b     // Catch:{ Throwable -> 0x005d }
            android.content.res.Resources r3 = r3.getResources()     // Catch:{ Throwable -> 0x005d }
            r4 = 0
            r3.getValue(r6, r0, r4)     // Catch:{ Throwable -> 0x005d }
            int r0 = r0.density     // Catch:{ Throwable -> 0x005d }
            if (r0 != 0) goto L_0x0060
            r0 = 160(0xa0, float:2.24E-43)
            r2.inDensity = r0     // Catch:{ Throwable -> 0x005d }
        L_0x0046:
            android.util.DisplayMetrics r0 = r3.getDisplayMetrics()     // Catch:{ Throwable -> 0x005d }
            int r0 = r0.densityDpi     // Catch:{ Throwable -> 0x005d }
            r2.inTargetDensity = r0     // Catch:{ Throwable -> 0x005d }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r5, r1, r2)     // Catch:{ Throwable -> 0x005d }
            r5.close()     // Catch:{ Throwable -> 0x005d }
        L_0x0055:
            return r0
        L_0x0056:
            r0 = move-exception
            com.immomo.momo.util.m r3 = com.immomo.momo.g.g     // Catch:{ Throwable -> 0x005d }
            r3.a(r0)     // Catch:{ Throwable -> 0x005d }
            goto L_0x002f
        L_0x005d:
            r0 = move-exception
            r0 = 0
            goto L_0x0055
        L_0x0060:
            r4 = 65535(0xffff, float:9.1834E-41)
            if (r0 == r4) goto L_0x0046
            r2.inDensity = r0     // Catch:{ Throwable -> 0x005d }
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.g.a(java.io.InputStream, int):android.graphics.Bitmap");
    }

    public static RectF a(View view) {
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        int i2 = iArr[0];
        int i3 = iArr[1];
        return new RectF((float) i2, (float) i3, (float) (i2 + view.getMeasuredWidth()), (float) (i3 + view.getMeasuredHeight()));
    }

    public static String a(int i2) {
        return b.getString(i2);
    }

    public static String a(String str, int i2) {
        if (str == null) {
            return PoiTypeDef.All;
        }
        String substring = str.substring(0, str.length() < i2 ? str.length() : i2);
        int i3 = i2;
        String str2 = substring;
        int length = substring.getBytes("GBK").length;
        String str3 = str2;
        while (length > i2) {
            int i4 = i3 - 1;
            String substring2 = str.substring(0, i4 > str.length() ? str.length() : i4);
            int length2 = substring2.getBytes("GBK").length;
            str3 = substring2;
            i3 = i4;
            length = length2;
        }
        return str3;
    }

    public static String a(Object... objArr) {
        return b.getString(R.string.contact_apply_smscontent, objArr);
    }

    public static void a(Context context) {
        if (b == null) {
            b = context;
        }
    }

    @SuppressLint({"NewApi"})
    public static void a(CharSequence charSequence) {
        if (Build.VERSION.SDK_INT >= 11) {
            ((ClipboardManager) b.getSystemService("clipboard")).setText(charSequence);
        } else {
            ((android.text.ClipboardManager) b.getSystemService("clipboard")).setText(charSequence);
        }
    }

    public static void a(boolean z) {
        m = z;
    }

    public static boolean a() {
        return Build.VERSION.SDK_INT >= 14;
    }

    public static boolean aa() {
        return "wifi".equals(Y());
    }

    public static boolean ab() {
        return "mobile".equals(Y());
    }

    public static boolean ac() {
        int af = af();
        return af == 2 || af == 1 || af == 5 || af == 3;
    }

    public static boolean ad() {
        return m;
    }

    private static DisplayMetrics ae() {
        return b.getResources().getDisplayMetrics();
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0096 A[Catch:{ Exception -> 0x00bb }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:? A[Catch:{ Exception -> 0x00bb }, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int af() {
        /*
            r6 = 6
            r7 = 1
            r8 = -1
            android.content.Context r0 = com.immomo.momo.g.b     // Catch:{ Exception -> 0x00bb }
            java.lang.String r1 = "connectivity"
            java.lang.Object r0 = r0.getSystemService(r1)     // Catch:{ Exception -> 0x00bb }
            android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0     // Catch:{ Exception -> 0x00bb }
            android.net.NetworkInfo r9 = r0.getActiveNetworkInfo()     // Catch:{ Exception -> 0x00bb }
            if (r9 == 0) goto L_0x0019
            boolean r0 = r9.isAvailable()     // Catch:{ Exception -> 0x00bb }
            if (r0 != 0) goto L_0x001b
        L_0x0019:
            r0 = 0
        L_0x001a:
            return r0
        L_0x001b:
            int r0 = r9.getType()     // Catch:{ Exception -> 0x00bb }
            if (r0 != r7) goto L_0x0023
            r0 = r6
            goto L_0x001a
        L_0x0023:
            if (r0 != 0) goto L_0x00c1
            android.content.Context r0 = com.immomo.momo.g.b     // Catch:{ Exception -> 0x00bb }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x00bb }
            android.net.Uri r1 = com.immomo.momo.g.l     // Catch:{ Exception -> 0x00bb }
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x00bb }
            if (r1 == 0) goto L_0x00bf
            r1.moveToFirst()     // Catch:{ Exception -> 0x00bb }
            java.lang.String r0 = "user"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x00bb }
            java.lang.String r0 = r1.getString(r0)     // Catch:{ Exception -> 0x00bb }
            boolean r2 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Exception -> 0x00bb }
            if (r2 != 0) goto L_0x00bf
            com.immomo.momo.util.m r2 = com.immomo.momo.g.g     // Catch:{ Exception -> 0x00bb }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00bb }
            java.lang.String r4 = "=====================>代理："
            r3.<init>(r4)     // Catch:{ Exception -> 0x00bb }
            java.lang.String r4 = "proxy"
            int r4 = r1.getColumnIndex(r4)     // Catch:{ Exception -> 0x00bb }
            java.lang.String r4 = r1.getString(r4)     // Catch:{ Exception -> 0x00bb }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x00bb }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00bb }
            r2.a(r3)     // Catch:{ Exception -> 0x00bb }
            java.lang.String r2 = "ctwap"
            boolean r0 = r0.startsWith(r2)     // Catch:{ Exception -> 0x00bb }
            if (r0 == 0) goto L_0x00bf
            com.immomo.momo.util.m r0 = com.immomo.momo.g.g     // Catch:{ Exception -> 0x00bb }
            java.lang.String r2 = "=====================>电信wap网络"
            r0.a(r2)     // Catch:{ Exception -> 0x00bb }
            r8 = 5
            r0 = r8
        L_0x0079:
            r1.close()     // Catch:{ Exception -> 0x00bb }
            java.lang.String r1 = r9.getExtraInfo()     // Catch:{ Exception -> 0x00bb }
            com.immomo.momo.util.m r2 = com.immomo.momo.g.g     // Catch:{ Exception -> 0x00bb }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00bb }
            java.lang.String r4 = "netMode ================== "
            r3.<init>(r4)     // Catch:{ Exception -> 0x00bb }
            java.lang.StringBuilder r3 = r3.append(r1)     // Catch:{ Exception -> 0x00bb }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00bb }
            r2.a(r3)     // Catch:{ Exception -> 0x00bb }
            if (r1 == 0) goto L_0x001a
            java.lang.String r1 = r1.toLowerCase()     // Catch:{ Exception -> 0x00bb }
            java.lang.String r2 = "cmwap"
            boolean r2 = r1.equals(r2)     // Catch:{ Exception -> 0x00bb }
            if (r2 == 0) goto L_0x00a5
            r0 = r7
            goto L_0x001a
        L_0x00a5:
            java.lang.String r2 = "3gwap"
            boolean r2 = r1.equals(r2)     // Catch:{ Exception -> 0x00bb }
            if (r2 == 0) goto L_0x00b0
            r0 = 2
            goto L_0x001a
        L_0x00b0:
            java.lang.String r2 = "uniwap"
            boolean r1 = r1.equals(r2)     // Catch:{ Exception -> 0x00bb }
            if (r1 == 0) goto L_0x001a
            r0 = 3
            goto L_0x001a
        L_0x00bb:
            r0 = move-exception
            r0 = r6
            goto L_0x001a
        L_0x00bf:
            r0 = r8
            goto L_0x0079
        L_0x00c1:
            r0 = r8
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.g.af():int");
    }

    public static int b(float f2) {
        return Math.round(TypedValue.applyDimension(2, f2, ae()));
    }

    public static Drawable b(int i2) {
        return b.getResources().getDrawable(i2);
    }

    public static Object b(String str) {
        return b.getSystemService(str);
    }

    public static boolean b() {
        return Build.VERSION.SDK_INT >= 16;
    }

    public static int c(int i2) {
        return b.getResources().getColor(i2);
    }

    public static Context c() {
        return b;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v3, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v6, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v7, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String c(java.lang.String r5) {
        /*
            java.lang.String r0 = "MD5"
            java.security.MessageDigest r0 = java.security.MessageDigest.getInstance(r0)     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            byte[] r1 = r5.getBytes()     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            r0.update(r1)     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            java.lang.StringBuffer r2 = new java.lang.StringBuffer     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            r2.<init>()     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            byte[] r3 = r0.digest()     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            r0 = 0
        L_0x0017:
            int r1 = r3.length     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            if (r0 < r1) goto L_0x001f
            java.lang.String r0 = r2.toString()     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
        L_0x001e:
            return r0
        L_0x001f:
            byte r1 = r3[r0]     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            if (r1 >= 0) goto L_0x0025
            int r1 = r1 + 256
        L_0x0025:
            r4 = 16
            if (r1 >= r4) goto L_0x002e
            java.lang.String r4 = "0"
            r2.append(r4)     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
        L_0x002e:
            java.lang.String r1 = java.lang.Integer.toHexString(r1)     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            r2.append(r1)     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            int r0 = r0 + 1
            goto L_0x0017
        L_0x0038:
            r0 = move-exception
            java.lang.String r0 = ""
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.g.c(java.lang.String):java.lang.String");
    }

    public static MomoApplication d() {
        return (MomoApplication) b;
    }

    public static boolean d(String str) {
        return Pattern.compile("\\w[\\w.-]*@[\\w.-]+\\.\\w+").matcher(str).matches();
    }

    public static ContentResolver e() {
        if (c == null) {
            c = b.getContentResolver();
        }
        return c;
    }

    public static boolean f() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    public static String g() {
        try {
            String string = b.getPackageManager().getApplicationInfo(h(), NativeMapEngine.MAX_ICON_SIZE).metaData.getString("source");
            return string == null ? PoiTypeDef.All : string;
        } catch (Exception e2) {
            return PoiTypeDef.All;
        }
    }

    public static String h() {
        if (d == null) {
            String packageName = b.getPackageName();
            d = packageName;
            if (packageName.indexOf(":") >= 0) {
                d = d.substring(0, d.lastIndexOf(":"));
            }
        }
        return d;
    }

    public static AudioManager i() {
        return (AudioManager) b.getSystemService("audio");
    }

    public static InputMethodManager j() {
        return (InputMethodManager) b.getSystemService("input_method");
    }

    public static float k() {
        if (e == null) {
            e = Float.valueOf(b.getResources().getDisplayMetrics().density);
        }
        return e.floatValue();
    }

    public static Resources l() {
        return b.getResources();
    }

    public static Bitmap m() {
        return BitmapFactory.decodeResource(b.getResources(), R.drawable.ic_header_contacts);
    }

    public static int n() {
        return b.getResources().getDimensionPixelSize(R.dimen.listitem_feed_image_hight);
    }

    public static LayoutInflater o() {
        if (f == null) {
            f = LayoutInflater.from(b);
        }
        return f;
    }

    public static boolean p() {
        return Build.VERSION.SDK_INT >= 8;
    }

    public static bf q() {
        if (b != null) {
            return ((MomoApplication) b).a();
        }
        return null;
    }

    public static at r() {
        if (b != null) {
            return ((MomoApplication) b).b();
        }
        return null;
    }

    public static String s() {
        return ((MomoApplication) b).a() != null ? ((MomoApplication) b).a().W : PoiTypeDef.All;
    }

    public static Bitmap t() {
        if (h == null || h.isRecycled()) {
            h = u();
        }
        return h;
    }

    public static Bitmap u() {
        return BitmapFactory.decodeResource(b.getResources(), R.drawable.ic_common_def_header);
    }

    public static boolean v() {
        try {
            Class.forName("com.google.android.maps.MapActivity");
            return true;
        } catch (Throwable th) {
            return false;
        }
    }

    public static Bitmap w() {
        if (i == null) {
            i = BitmapFactory.decodeResource(b.getResources(), R.drawable.ic_common_def_map);
        }
        return i;
    }

    public static Bitmap x() {
        if (j == null) {
            j = BitmapFactory.decodeResource(b.getResources(), R.drawable.ic_common_def_header);
        }
        return j;
    }

    public static boolean y() {
        NetworkInfo[] allNetworkInfo;
        ConnectivityManager connectivityManager = (ConnectivityManager) b.getSystemService("connectivity");
        if (connectivityManager == null || (allNetworkInfo = connectivityManager.getAllNetworkInfo()) == null) {
            return false;
        }
        for (NetworkInfo state : allNetworkInfo) {
            if (state.getState() == NetworkInfo.State.CONNECTED) {
                return true;
            }
        }
        return false;
    }

    public static int z() {
        try {
            return b.getPackageManager().getPackageInfo(b.getPackageName(), 16384).versionCode;
        } catch (Exception e2) {
            g.a((Throwable) e2);
            return 0;
        }
    }
}
