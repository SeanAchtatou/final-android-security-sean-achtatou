package com.immomo.momo.android.game;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.m;

final class af extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ac f2402a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public af(ac acVar, Context context) {
        super(context);
        this.f2402a = acVar;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        m.a().a(this.f2402a.Q, this.f2402a.R, this.f2402a.O);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f2402a.a(new v(this.f2402a.c(), "正在获取陌陌账号余额...", this));
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        this.f2402a.P.setText("陌陌账号余额: " + ((long) this.f2402a.O.f) + "陌陌币");
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f2402a.N();
    }
}
