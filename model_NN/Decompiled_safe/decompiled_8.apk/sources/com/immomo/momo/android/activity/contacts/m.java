package com.immomo.momo.android.activity.contacts;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.w;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

final class m extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f1193a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m(a aVar, Context context) {
        super(context);
        this.f1193a = aVar;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        int unused = w.a().a(arrayList, this.f1193a.P.getCount(), this.f1193a.N);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List list = (List) obj;
        super.a(list);
        if (list != null) {
            if (list.size() > 0) {
                this.f1193a.P.b((Collection) list);
                if (this.f1193a.P.getCount() < g.q().y) {
                    return;
                }
            }
            this.f1193a.O.k();
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.f1193a.W = null;
        this.f1193a.Q.e();
    }
}
