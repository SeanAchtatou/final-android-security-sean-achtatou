package com.immomo.momo.android.activity.common;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.a.fz;
import com.immomo.momo.android.a.ge;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.android.activity.plugin.TxWeiboActivity;
import com.immomo.momo.android.activity.setting.CommunityBindActivity;
import com.immomo.momo.android.view.MomoRefreshListView;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.android.view.cv;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.e;
import java.util.ArrayList;

public class ar extends lh implements View.OnClickListener, AdapterView.OnItemClickListener, ge, l, bu, cv {
    /* access modifiers changed from: private */
    public MomoRefreshListView O;
    /* access modifiers changed from: private */
    public as P;
    private TextView Q;
    private Button R;
    private View S;
    /* access modifiers changed from: private */
    public fz T;

    /* access modifiers changed from: protected */
    public final int B() {
        return R.layout.layout_invitesns;
    }

    /* access modifiers changed from: protected */
    public final void C() {
        this.O = (MomoRefreshListView) c((int) R.id.both_listview);
        this.O.setEnableLoadMoreFoolter(false);
        this.O.setTimeEnable(false);
        this.T = new fz(c(), new ArrayList(), this.O, this);
        this.O.setAdapter((ListAdapter) this.T);
        this.S = c((int) R.id.layout_bind);
        this.R = (Button) c((int) R.id.btn_bind);
        this.Q = (TextView) c((int) R.id.tv_bindinfo);
        this.Q.setText("还没有绑定腾讯微博");
    }

    /* access modifiers changed from: protected */
    public final void E() {
        super.E();
        this.O.o();
        this.T.a(true);
    }

    public final void O() {
    }

    public final void S() {
        super.S();
    }

    public final void ae() {
        super.ae();
    }

    public final void ag() {
        super.ag();
    }

    public final void ai() {
        this.O.i();
    }

    public final void aj() {
        int i = 8;
        this.O.setVisibility(this.M.at ? 0 : 8);
        View view = this.S;
        if (!this.M.at) {
            i = 0;
        }
        view.setVisibility(i);
    }

    /* access modifiers changed from: protected */
    public final void b(int i, int i2, Intent intent) {
        switch (i) {
            case 21:
                if (i2 == -1) {
                    this.S.setVisibility(8);
                    this.O.setVisibility(0);
                    this.O.m();
                    return;
                }
                return;
            default:
                return;
        }
    }

    public final void b_() {
        if (this.P != null && !this.P.isCancelled()) {
            this.P.cancel(true);
        }
        this.P = new as(this, c());
        this.P.execute(new Object[0]);
    }

    public final void f(int i) {
        new at(this, c(), i).execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public final void g(Bundle bundle) {
        this.O.setOnPullToRefreshListener$42b903f6(this);
        this.O.setOnCancelListener$135502(this);
        this.O.setOnItemClickListener(this);
        this.R.setOnClickListener(this);
        aj();
    }

    public final void m() {
        if (this.M.at && this.T.getCount() <= 0) {
            this.O.b();
        }
        super.m();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_bind /*2131165394*/:
                Intent intent = new Intent(g.c(), CommunityBindActivity.class);
                intent.putExtra("type", 2);
                a(intent, 21);
                return;
            default:
                return;
        }
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Intent intent = new Intent(c(), TxWeiboActivity.class);
        intent.putExtra("keyType", 2);
        intent.putExtra("uid", ((e) this.T.getItem(i)).c);
        a(intent);
    }

    public final void p() {
        super.p();
        if (this.P != null && !this.P.isCancelled()) {
            this.P.cancel(true);
        }
    }

    public final void v() {
    }
}
