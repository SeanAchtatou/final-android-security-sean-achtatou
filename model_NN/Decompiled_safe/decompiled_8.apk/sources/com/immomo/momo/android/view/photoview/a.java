package com.immomo.momo.android.view.photoview;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import com.immomo.momo.android.plugin.cropimage.q;
import com.immomo.momo.util.m;
import java.lang.ref.WeakReference;

public final class a implements GestureDetector.OnDoubleTapListener, View.OnTouchListener, ViewTreeObserver.OnGlobalLayoutListener, m {
    private static /* synthetic */ int[] B;

    /* renamed from: a  reason: collision with root package name */
    static final boolean f2817a = Log.isLoggable("PhotoViewAttacher", 3);
    private ImageView.ScaleType A = ImageView.ScaleType.FIT_CENTER;
    private m b = new m(this);
    private float c = 1.0f;
    private float d = 3.0f;
    private float e = 10.0f;
    private boolean f = true;
    /* access modifiers changed from: private */
    public WeakReference g;
    private ViewTreeObserver h;
    private GestureDetector i;
    private h j;
    private final Matrix k = new Matrix();
    private final Matrix l = new Matrix();
    /* access modifiers changed from: private */
    public final Matrix m = new Matrix();
    private final RectF n = new RectF();
    private final float[] o = new float[9];
    private q p;
    private q q;
    private q r;
    /* access modifiers changed from: private */
    public View.OnLongClickListener s;
    private int t;
    private int u;
    private int v;
    private int w;
    private d x;
    private int y = 2;
    private boolean z;

    public a(ImageView imageView) {
        this.g = new WeakReference(imageView);
        imageView.setOnTouchListener(this);
        this.h = imageView.getViewTreeObserver();
        this.h.addOnGlobalLayoutListener(this);
        b(imageView);
        if (!imageView.isInEditMode()) {
            Context context = imageView.getContext();
            int i2 = Build.VERSION.SDK_INT;
            h iVar = i2 < 5 ? new i(context) : i2 < 8 ? new j(context) : new k(context);
            iVar.f2823a = this;
            this.j = iVar;
            this.i = new GestureDetector(imageView.getContext(), new b(this));
            this.i.setOnDoubleTapListener(this);
            b(true);
        }
    }

    private RectF a(Matrix matrix) {
        Drawable drawable;
        ImageView c2 = c();
        if (c2 == null || (drawable = c2.getDrawable()) == null) {
            return null;
        }
        this.n.set(0.0f, 0.0f, (float) drawable.getIntrinsicWidth(), (float) drawable.getIntrinsicHeight());
        matrix.mapRect(this.n);
        return this.n;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    private void a(Drawable drawable) {
        ImageView c2 = c();
        if (c2 != null && drawable != null) {
            float width = (float) c2.getWidth();
            float height = (float) c2.getHeight();
            int intrinsicWidth = drawable.getIntrinsicWidth();
            int intrinsicHeight = drawable.getIntrinsicHeight();
            this.k.reset();
            float f2 = width / ((float) intrinsicWidth);
            float f3 = height / ((float) intrinsicHeight);
            if (this.A != ImageView.ScaleType.CENTER) {
                if (this.A != ImageView.ScaleType.CENTER_CROP) {
                    if (this.A != ImageView.ScaleType.CENTER_INSIDE) {
                        RectF rectF = new RectF(0.0f, 0.0f, (float) intrinsicWidth, (float) intrinsicHeight);
                        RectF rectF2 = new RectF(0.0f, 0.0f, width, height);
                        switch (n()[this.A.ordinal()]) {
                            case 4:
                                this.k.setRectToRect(rectF, rectF2, Matrix.ScaleToFit.CENTER);
                                break;
                            case 5:
                                this.k.setRectToRect(rectF, rectF2, Matrix.ScaleToFit.END);
                                break;
                            case 6:
                                this.k.setRectToRect(rectF, rectF2, Matrix.ScaleToFit.START);
                                break;
                            case 7:
                                this.k.setRectToRect(rectF, rectF2, Matrix.ScaleToFit.FILL);
                                break;
                        }
                    } else {
                        float min = Math.min(1.0f, Math.min(f2, f3));
                        this.k.postScale(min, min);
                        this.k.postTranslate((width - (((float) intrinsicWidth) * min)) / 2.0f, (height - (((float) intrinsicHeight) * min)) / 2.0f);
                    }
                } else {
                    float max = Math.max(f2, f3);
                    this.k.postScale(max, max);
                    this.k.postTranslate((width - (((float) intrinsicWidth) * max)) / 2.0f, (height - (((float) intrinsicHeight) * max)) / 2.0f);
                }
            } else {
                this.k.postTranslate((width - ((float) intrinsicWidth)) / 2.0f, (height - ((float) intrinsicHeight)) / 2.0f);
            }
            m();
        }
    }

    private static boolean a(ImageView imageView) {
        return (imageView == null || imageView.getDrawable() == null) ? false : true;
    }

    private static void b(float f2, float f3, float f4) {
        if (f2 >= f3) {
            throw new IllegalArgumentException("MinZoom should be less than MidZoom");
        } else if (f3 >= f4) {
            throw new IllegalArgumentException("MidZoom should be less than MaxZoom");
        }
    }

    /* access modifiers changed from: private */
    public void b(Matrix matrix) {
        ImageView c2 = c();
        if (c2 != null) {
            ImageView c3 = c();
            if (c3 == null || (c3 instanceof PhotoView) || c3.getScaleType() == ImageView.ScaleType.MATRIX) {
                c2.setImageMatrix(matrix);
                if (this.p != null && a(matrix) != null) {
                    q qVar = this.p;
                    return;
                }
                return;
            }
            throw new IllegalStateException("The ImageView's ScaleType has been changed since attaching a PhotoViewAttacher");
        }
    }

    private static void b(ImageView imageView) {
        if (imageView != null && !(imageView instanceof PhotoView)) {
            imageView.setScaleType(ImageView.ScaleType.MATRIX);
        }
    }

    private void c(float f2, float f3, float f4) {
        ImageView c2 = c();
        if (c2 != null) {
            c2.post(new c(this, g(), f2, f3, f4));
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        l();
        b(j());
    }

    private void l() {
        RectF a2;
        float f2;
        float f3 = 0.0f;
        ImageView c2 = c();
        if (c2 != null && (a2 = a(j())) != null) {
            float height = a2.height();
            float width = a2.width();
            int height2 = c2.getHeight();
            if (height <= ((float) height2)) {
                switch (n()[this.A.ordinal()]) {
                    case 5:
                        f2 = (((float) height2) - height) - a2.top;
                        break;
                    case 6:
                        f2 = -a2.top;
                        break;
                    default:
                        f2 = ((((float) height2) - height) / 2.0f) - a2.top;
                        break;
                }
            } else {
                f2 = a2.top > 0.0f ? -a2.top : a2.bottom < ((float) height2) ? ((float) height2) - a2.bottom : 0.0f;
            }
            int width2 = c2.getWidth();
            if (width <= ((float) width2)) {
                switch (n()[this.A.ordinal()]) {
                    case 5:
                        f3 = (((float) width2) - width) - a2.left;
                        break;
                    case 6:
                        f3 = -a2.left;
                        break;
                    default:
                        f3 = ((((float) width2) - width) / 2.0f) - a2.left;
                        break;
                }
                this.y = 2;
            } else if (a2.left > 0.0f) {
                this.y = 0;
                f3 = -a2.left;
            } else if (a2.right < ((float) width2)) {
                f3 = ((float) width2) - a2.right;
                this.y = 1;
            } else {
                this.y = -1;
            }
            this.m.postTranslate(f3, f2);
        }
    }

    private void m() {
        this.m.reset();
        b(j());
        l();
    }

    private static /* synthetic */ int[] n() {
        int[] iArr = B;
        if (iArr == null) {
            iArr = new int[ImageView.ScaleType.values().length];
            try {
                iArr[ImageView.ScaleType.CENTER.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[ImageView.ScaleType.CENTER_CROP.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[ImageView.ScaleType.CENTER_INSIDE.ordinal()] = 3;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[ImageView.ScaleType.FIT_CENTER.ordinal()] = 4;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[ImageView.ScaleType.FIT_END.ordinal()] = 5;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[ImageView.ScaleType.FIT_START.ordinal()] = 6;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[ImageView.ScaleType.FIT_XY.ordinal()] = 7;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[ImageView.ScaleType.MATRIX.ordinal()] = 8;
            } catch (NoSuchFieldError e9) {
            }
            B = iArr;
        }
        return iArr;
    }

    public final void a() {
        if (this.h != null && this.h.isAlive()) {
            this.h.removeGlobalOnLayoutListener(this);
        }
        this.h = null;
        this.p = null;
        this.q = null;
        this.r = null;
        this.g = null;
    }

    public final void a(float f2) {
        b(f2, this.d, this.e);
        this.c = f2;
    }

    public final void a(float f2, float f3) {
        if (f2817a) {
            Log.d("PhotoViewAttacher", String.format("onDrag: dx: %.2f. dy: %.2f", Float.valueOf(f2), Float.valueOf(f3)));
        }
        ImageView c2 = c();
        if (c2 == null) {
            return;
        }
        if (a(c2)) {
            this.m.postTranslate(f2, f3);
            k();
            if (this.f && !this.j.a()) {
                if (this.y == 2 || ((this.y == 0 && f2 >= 1.0f) || (this.y == 1 && f2 <= -1.0f))) {
                    c2.getParent().requestDisallowInterceptTouchEvent(false);
                    return;
                }
                return;
            }
            return;
        }
        c2.getParent().requestDisallowInterceptTouchEvent(false);
    }

    public final void a(float f2, float f3, float f4) {
        if (f2817a) {
            Log.d("PhotoViewAttacher", String.format("onScale: scale: %.2f. fX: %.2f. fY: %.2f", Float.valueOf(f2), Float.valueOf(f3), Float.valueOf(f4)));
        }
        if (!a(c())) {
            return;
        }
        if (g() < this.e || f2 < 1.0f) {
            this.b.b((Object) String.format("onScale: scale: %.2f. fX: %.2f. fY: %.2f", Float.valueOf(f2), Float.valueOf(f3), Float.valueOf(f4)));
            this.m.postScale(f2, f2, f3, f4);
            k();
        }
    }

    public final void a(float f2, float f3, float f4, float f5) {
        if (f2817a) {
            Log.d("PhotoViewAttacher", "onFling. sX: " + f2 + " sY: " + f3 + " Vx: " + f4 + " Vy: " + f5);
        }
        ImageView c2 = c();
        if (a(c2)) {
            this.x = new d(this, c2.getContext());
            this.x.a(c2.getWidth(), c2.getHeight(), (int) f4, (int) f5);
            c2.post(this.x);
        }
    }

    public final void a(View.OnLongClickListener onLongClickListener) {
        this.s = onLongClickListener;
    }

    public final void a(ImageView.ScaleType scaleType) {
        boolean z2;
        if (scaleType != null) {
            switch (n()[scaleType.ordinal()]) {
                case 8:
                    throw new IllegalArgumentException(String.valueOf(scaleType.name()) + " is not supported in PhotoView");
                default:
                    z2 = true;
                    break;
            }
        } else {
            z2 = false;
        }
        if (z2 && scaleType != this.A) {
            this.A = scaleType;
            i();
        }
    }

    public final void a(q qVar) {
        this.p = qVar;
    }

    public final void a(boolean z2) {
        this.f = z2;
    }

    public final RectF b() {
        l();
        return a(j());
    }

    public final void b(float f2) {
        b(this.c, f2, this.e);
        this.d = f2;
    }

    public final void b(q qVar) {
        this.q = qVar;
    }

    public final void b(boolean z2) {
        this.z = z2;
        i();
    }

    public final ImageView c() {
        ImageView imageView = null;
        if (this.g != null) {
            imageView = (ImageView) this.g.get();
        }
        if (imageView == null) {
            a();
        }
        return imageView;
    }

    public final void c(float f2) {
        b(this.c, this.d, f2);
        this.e = f2;
    }

    public final void c(q qVar) {
        this.r = qVar;
    }

    public final float d() {
        return this.c;
    }

    public final float e() {
        return this.d;
    }

    public final float f() {
        return this.e;
    }

    public final float g() {
        this.m.getValues(this.o);
        return this.o[0];
    }

    public final ImageView.ScaleType h() {
        return this.A;
    }

    public final void i() {
        ImageView c2 = c();
        if (c2 == null) {
            return;
        }
        if (this.z) {
            b(c2);
            a(c2.getDrawable());
            return;
        }
        m();
    }

    /* access modifiers changed from: protected */
    public final Matrix j() {
        this.l.set(this.k);
        this.l.postConcat(this.m);
        return this.l;
    }

    public final boolean onDoubleTap(MotionEvent motionEvent) {
        try {
            float g2 = g();
            float x2 = motionEvent.getX();
            float y2 = motionEvent.getY();
            if (g2 < this.d) {
                c(this.d, x2, y2);
                return true;
            }
            c(this.c, x2, y2);
            return true;
        } catch (ArrayIndexOutOfBoundsException e2) {
            return true;
        }
    }

    public final boolean onDoubleTapEvent(MotionEvent motionEvent) {
        return false;
    }

    public final void onGlobalLayout() {
        ImageView c2 = c();
        if (c2 != null && this.z) {
            int top = c2.getTop();
            int right = c2.getRight();
            int bottom = c2.getBottom();
            int left = c2.getLeft();
            if (top != this.t || bottom != this.v || left != this.w || right != this.u) {
                a(c2.getDrawable());
                this.t = top;
                this.u = right;
                this.v = bottom;
                this.w = left;
            }
        }
    }

    public final boolean onSingleTapConfirmed(MotionEvent motionEvent) {
        RectF b2;
        if (c() != null) {
            if (this.q != null && (b2 = b()) != null && b2.contains(motionEvent.getX(), motionEvent.getY())) {
                float f2 = b2.left;
                b2.width();
                float f3 = b2.top;
                b2.height();
                q qVar = this.q;
                return true;
            } else if (this.r != null) {
                q qVar2 = this.r;
                motionEvent.getX();
                motionEvent.getY();
            }
        }
        return false;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        RectF b2;
        boolean z2 = false;
        if (!this.z) {
            return false;
        }
        switch (motionEvent.getAction()) {
            case 0:
                view.getParent().requestDisallowInterceptTouchEvent(true);
                if (this.x != null) {
                    this.x.a();
                    this.x = null;
                    break;
                }
                break;
            case 1:
            case 3:
                if (g() < this.c && (b2 = b()) != null) {
                    view.post(new c(this, g(), this.c, b2.centerX(), b2.centerY()));
                    z2 = true;
                    break;
                }
        }
        if (this.i != null && this.i.onTouchEvent(motionEvent)) {
            z2 = true;
        }
        if (this.j == null || !this.j.a(motionEvent)) {
            return z2;
        }
        return true;
    }
}
