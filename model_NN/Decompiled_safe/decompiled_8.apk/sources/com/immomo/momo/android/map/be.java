package com.immomo.momo.android.map;

import android.content.DialogInterface;

final class be implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ bo f2484a;

    be(bo boVar) {
        this.f2484a = boVar;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        if (this.f2484a != null) {
            this.f2484a.a(true);
        }
    }
}
