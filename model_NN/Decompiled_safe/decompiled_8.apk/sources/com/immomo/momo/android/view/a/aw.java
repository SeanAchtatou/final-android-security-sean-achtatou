package com.immomo.momo.android.view.a;

import android.content.Context;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import com.immomo.momo.R;
import com.immomo.momo.g;

public abstract class aw implements View.OnKeyListener, View.OnTouchListener, PopupWindow.OnDismissListener {

    /* renamed from: a  reason: collision with root package name */
    private View f2679a = null;
    private ax b = null;
    protected PopupWindow c = null;
    protected Context d = null;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void}
     arg types: [android.view.View, int, int, int]
     candidates:
      ClspMth{android.widget.PopupWindow.<init>(android.content.Context, android.util.AttributeSet, int, int):void}
      ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void} */
    public aw(Context context, int i) {
        this.f2679a = g.o().inflate(i, (ViewGroup) null);
        this.d = context;
        this.c = new PopupWindow(this.f2679a, -1, -1, true);
        this.c.setInputMethodMode(1);
        this.c.setOnDismissListener(this);
        this.c.setAnimationStyle(R.style.Popup_Animation_DownUp);
        this.f2679a.setOnTouchListener(this);
        this.f2679a.setFocusableInTouchMode(true);
        this.f2679a.setOnKeyListener(this);
    }

    public void a(View view) {
        this.c.showAsDropDown(view, 0, 0);
    }

    public final void a(View view, int i) {
        this.c.showAtLocation(view, i, 0, 0);
    }

    public final void a(ax axVar) {
        this.b = axVar;
    }

    /* access modifiers changed from: protected */
    public boolean a() {
        return false;
    }

    public final void b(int i) {
        this.c.setAnimationStyle(i);
    }

    public final View c(int i) {
        return this.f2679a.findViewById(i);
    }

    public final boolean d() {
        return this.c.isShowing();
    }

    public final void e() {
        this.c.dismiss();
    }

    public void onDismiss() {
        if (this.b != null) {
            this.b.a();
        }
    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (i != 4 || keyEvent.getAction() != 1 || a()) {
            return false;
        }
        this.c.dismiss();
        return true;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() != 1) {
            return false;
        }
        this.c.dismiss();
        return true;
    }
}
