package com.immomo.momo.android.a;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.android.view.EmoteTextView;

final class iy {

    /* renamed from: a  reason: collision with root package name */
    public ImageView f891a;
    public TextView b;
    public TextView c;
    public TextView d;
    public TextView e;
    public EmoteTextView f;
    public ImageView g;
    public View h;
    public ImageView i;
    public ImageView j;
    public ImageView k;
    public ImageView l;
    public ImageView m;
    public ImageView n;
    public ImageView o;
    public ImageView p;
    public View q;

    private iy() {
    }

    /* synthetic */ iy(byte b2) {
        this();
    }
}
