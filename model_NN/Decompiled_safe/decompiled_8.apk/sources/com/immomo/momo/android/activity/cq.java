package com.immomo.momo.android.activity;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.aq;
import com.immomo.momo.util.h;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import org.json.JSONArray;
import org.json.JSONObject;

final class cq extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EditUserPhotoActivity f1205a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cq(EditUserPhotoActivity editUserPhotoActivity, Context context) {
        super(context);
        this.f1205a = editUserPhotoActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void
     arg types: [java.lang.String, java.lang.String, int, int]
     candidates:
      com.immomo.momo.util.h.a(java.lang.String, android.graphics.Bitmap, int, boolean):java.io.File
      com.immomo.momo.util.h.a(java.lang.String, java.lang.String, int, boolean):void */
    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(this.f1205a.i);
        HashSet hashSet = new HashSet();
        if (this.f1205a.f.ae != null) {
            for (String add : this.f1205a.f.ae) {
                hashSet.add(add);
            }
        }
        HashMap hashMap = new HashMap();
        JSONArray jSONArray = new JSONArray();
        for (int i = 0; i < arrayList.size(); i++) {
            JSONObject jSONObject = new JSONObject();
            String str = (String) arrayList.get(i);
            if (hashSet.contains(str)) {
                jSONObject.put("upload", "YES");
                jSONObject.put("guid", str);
            } else {
                jSONObject.put("upload", "NO");
                jSONObject.put("key", "photo_" + i);
                File a2 = h.a(str, 2);
                if (a2 != null && a2.exists()) {
                    hashMap.put("photo_" + i, a2);
                }
            }
            jSONArray.put(jSONObject);
        }
        HashMap hashMap2 = new HashMap();
        hashMap2.put("photos", jSONArray.toString());
        w.a().a(this.f1205a.f, hashMap2, hashMap);
        for (int i2 = 0; i2 < arrayList.size(); i2++) {
            String str2 = (String) arrayList.get(i2);
            if (!hashSet.contains(str2) && this.f1205a.f.ae != null && i2 < this.f1205a.f.ae.length) {
                h.a(str2, this.f1205a.f.ae[i2], 2, true);
            }
        }
        this.f1205a.f.X++;
        new aq().b(this.f1205a.f);
        Intent intent = new Intent(com.immomo.momo.android.broadcast.w.f2366a);
        intent.putExtra("momoid", this.f1205a.f.h);
        this.f1205a.sendBroadcast(intent);
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f1205a.a(new v(this.f1205a, this));
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        this.f1205a.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f1205a.p();
    }
}
