package com.immomo.momo.android.activity.plugin;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.immomo.momo.R;
import com.immomo.momo.util.ao;

final class am extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ BindTwitter2Activity f2040a;

    private am(BindTwitter2Activity bindTwitter2Activity) {
        this.f2040a = bindTwitter2Activity;
    }

    /* synthetic */ am(BindTwitter2Activity bindTwitter2Activity, byte b) {
        this(bindTwitter2Activity);
    }

    public final void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        if (this.f2040a.o != null && this.f2040a.o.isShowing()) {
            this.f2040a.o.dismiss();
        }
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
        if (this.f2040a.o != null) {
            this.f2040a.o.a("正在加载，请稍候...");
            this.f2040a.o.show();
        }
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        if (i == -2) {
            ao.e(R.string.errormsg_network_unfind);
        } else {
            ao.e(R.string.errormsg_server);
        }
        if (this.f2040a.o != null && this.f2040a.o.isShowing()) {
            this.f2040a.o.dismiss();
        }
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        this.f2040a.h.b((Object) ("shouldOverrideUrlLoading : " + str));
        if (str.indexOf(this.f2040a.l) >= 0) {
            this.f2040a.c(str);
        } else {
            webView.loadUrl(str);
        }
        return true;
    }
}
