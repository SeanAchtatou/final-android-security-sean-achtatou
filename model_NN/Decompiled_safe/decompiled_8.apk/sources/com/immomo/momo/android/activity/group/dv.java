package com.immomo.momo.android.activity.group;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.group.foundgroup.JoinGroupActivity;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.n;
import com.immomo.momo.service.bean.a.h;

final class dv extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1618a;
    /* access modifiers changed from: private */
    public /* synthetic */ GroupProfileActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public dv(GroupProfileActivity groupProfileActivity, Context context) {
        super(context);
        this.c = groupProfileActivity;
        this.f1618a = new v(groupProfileActivity, "正在获取加入群权限...");
        this.f1618a.setOnCancelListener(new dw(this));
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object a(Object... objArr) {
        return n.a().a((String) objArr[0]);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.c.a(this.f1618a);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        h hVar = (h) obj;
        if (hVar == null) {
            return;
        }
        if (hVar.c) {
            Intent intent = new Intent(this.c.getApplicationContext(), JoinGroupActivity.class);
            intent.putExtra("agree_tip", hVar.d);
            intent.putExtra("gid", this.c.l);
            this.c.startActivityForResult(intent, 13);
        } else if (!this.c.f.b()) {
            this.c.a(com.immomo.momo.android.view.a.n.a(this.c, (int) R.string.nonvip__followgroup_dialog_msg, (int) R.string.nonvip__dialog_enter, (int) R.string.nonvip__dialog_cancel, new dx(this), new dy(this)));
        } else {
            a(hVar.f);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f1618a.dismiss();
    }
}
