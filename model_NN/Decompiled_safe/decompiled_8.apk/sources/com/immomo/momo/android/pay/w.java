package com.immomo.momo.android.pay;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.a;
import java.util.Map;

final class w extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f2574a = null;
    private Map c;
    private /* synthetic */ BuyMemberActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public w(BuyMemberActivity buyMemberActivity, Context context, Map map) {
        super(context);
        this.d = buyMemberActivity;
        this.c = map;
        this.f2574a = new v(context);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object a(Object... objArr) {
        return a.a().a(this.c);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f2574a.setCancelable(true);
        this.f2574a.setOnCancelListener(new x(this));
        this.f2574a.a("请求提交中...");
        try {
            this.d.a(this.f2574a);
        } catch (Throwable th) {
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        if (new ap().a((String) obj, this.d.F, this.d)) {
            this.d.G = new v(this.d);
            this.d.G.setCancelable(false);
            this.d.G.a("正在启动支付宝....");
            try {
                this.d.a(this.d.G);
            } catch (Throwable th) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        try {
            this.d.p();
        } catch (Throwable th) {
        }
    }
}
