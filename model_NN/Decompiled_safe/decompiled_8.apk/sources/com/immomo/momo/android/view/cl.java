package com.immomo.momo.android.view;

import android.text.Spanned;
import android.text.method.NumberKeyListener;
import com.amap.mapapi.poisearch.PoiTypeDef;

final class cl extends NumberKeyListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ NumberPicker f2764a;

    private cl(NumberPicker numberPicker) {
        this.f2764a = numberPicker;
    }

    /* synthetic */ cl(NumberPicker numberPicker, byte b) {
        this(numberPicker);
    }

    public final CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
        CharSequence filter = super.filter(charSequence, i, i2, spanned, i3, i4);
        if (filter == null) {
            filter = charSequence.subSequence(i, i2);
        }
        String str = String.valueOf(String.valueOf(spanned.subSequence(0, i3))) + ((Object) filter) + ((Object) spanned.subSequence(i4, spanned.length()));
        if (PoiTypeDef.All.equals(str)) {
            return str;
        }
        NumberPicker numberPicker = this.f2764a;
        return Integer.parseInt(str) > this.f2764a.f2641a ? PoiTypeDef.All : filter;
    }

    /* access modifiers changed from: protected */
    public final char[] getAcceptedChars() {
        return NumberPicker.m;
    }

    public final int getInputType() {
        return 2;
    }
}
