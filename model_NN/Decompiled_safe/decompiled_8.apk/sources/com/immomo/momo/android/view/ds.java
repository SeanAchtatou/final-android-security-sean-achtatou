package com.immomo.momo.android.view;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ImageBrowserActivity;

final class ds implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ UserPhotosView f2801a;

    ds(UserPhotosView userPhotosView) {
        this.f2801a = userPhotosView;
    }

    public final void onClick(View view) {
        int intValue = ((Integer) view.getTag(R.id.tag_item_position)).intValue();
        if (this.f2801a.l == null || !this.f2801a.l.a()) {
            Intent intent = new Intent(this.f2801a.getContext(), ImageBrowserActivity.class);
            intent.putExtra("array", this.f2801a.i);
            intent.putExtra("imagetype", "avator");
            intent.putExtra("index", intValue);
            this.f2801a.getContext().startActivity(intent);
            ((Activity) this.f2801a.getContext()).overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
            return;
        }
        this.f2801a.l.b();
    }
}
