package com.immomo.momo.android.view;

import android.content.Context;
import android.graphics.Canvas;
import android.support.v4.b.a;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.a.b;
import com.immomo.momo.g;
import java.util.Date;

public class MomoRefreshExpandableListView extends cu implements cw {
    private boolean A = true;
    private int B = -1;
    private float C;
    private float D;
    private boolean d;
    private bu e;
    private LinearLayout f;
    private ImageView g = null;
    private boolean h;
    private int i;
    private TextView j;
    private TextView k;
    private TextView l;
    private TextView m;
    private LinearLayout n;
    private RotatingImageView o;
    private cw p = this;
    private int q;
    private LoadingButton r = null;
    private boolean s = false;
    private Animation t = null;
    /* access modifiers changed from: private */
    public cv u = null;
    private b v;
    private View w;
    private boolean x = false;
    private int y;
    private int z;

    public MomoRefreshExpandableListView(Context context) {
        super(context);
        n();
        o();
    }

    public MomoRefreshExpandableListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        n();
        o();
    }

    public MomoRefreshExpandableListView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        n();
        o();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, com.immomo.momo.android.view.MomoRefreshExpandableListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void n() {
        this.e = null;
        setScrollbarFadingEnabled(true);
        setFastScrollEnabled(true);
        View inflate = g.o().inflate((int) R.layout.common_list_loadmore, (ViewGroup) null);
        this.c = inflate;
        this.r = (LoadingButton) inflate.findViewById(R.id.btn_loadmore);
        this.t = AnimationUtils.loadAnimation(getContext(), R.anim.loading);
        setFooterView(inflate);
        if (this.s) {
            j();
        }
        this.n = (LinearLayout) g.o().inflate(getRefreshLayout(), (ViewGroup) this, false);
        this.m = (TextView) this.n.findViewById(R.id.refresh_tv_message);
        this.o = (RotatingImageView) this.n.findViewById(R.id.refresh_iv_image);
        this.j = (TextView) this.n.findViewById(R.id.refresh_tv_time);
        a(this.n, 60);
        setOverScrollListener(this.p);
        setAutoOverScrollMultiplier(0.4f);
        LinearLayout linearLayout = (LinearLayout) g.o().inflate(getRefreshingLayout(), (ViewGroup) this, false);
        this.f = (LinearLayout) linearLayout.findViewById(R.id.refresh_loading_container);
        this.f.setVisibility(8);
        this.g = (ImageView) linearLayout.findViewById(R.id.refresh_iv_processing);
        this.k = (TextView) linearLayout.findViewById(R.id.refresh_tv_time);
        this.l = (TextView) linearLayout.findViewById(R.id.refresh_tv_message);
        this.f.findViewById(R.id.refresh_iv_cancel).setOnClickListener(new bq(this));
        g.a(60.0f);
        addHeaderView(linearLayout);
        setFadingEdgeColor(0);
        setFadingEdgeLength(0);
    }

    private void o() {
        setOnScrollListener(new bs(this));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public final void a(int i2, int i3) {
        if (this.q == 0) {
            this.q = g.a(30.0f);
        }
        if (i2 > 0) {
            if (this.h) {
                setOverScrollState(1);
            } else if (i2 < i3) {
                setOverScrollState(4);
            } else if (k()) {
                setOverScrollState(2);
            } else {
                setOverScrollState(3);
            }
        }
        if (i2 <= this.q || this.h) {
            this.o.setDegress(0);
            return;
        }
        this.o.setDegress((int) (Math.min(1.0f, ((float) (i2 - this.q)) / ((float) (i3 - this.q))) * 180.0f));
    }

    public final void b() {
        m();
        if (this.e != null && !this.d) {
            setLoadingVisible(true);
            this.e.b_();
        }
    }

    public final void b(int i2, int i3) {
        if (g.p() && this.w != null && this.v != null && this.v.getGroupCount() != 0) {
            switch (this.v.b(i2, i3)) {
                case 0:
                    this.x = false;
                    return;
                case 1:
                    this.v.a(this.w, i2);
                    if (this.w.getTop() != 0) {
                        this.w.layout(0, 0, this.y, this.z);
                    }
                    this.x = true;
                    if (this.A) {
                        requestLayout();
                        this.A = false;
                        return;
                    }
                    return;
                case 2:
                    int bottom = getChildAt(0).getBottom();
                    int height = this.w.getHeight();
                    int i4 = bottom < height ? bottom - height : 0;
                    this.v.a(this.w, i2);
                    if (this.w.getTop() != i4) {
                        this.w.layout(0, i4, this.y, this.z + i4);
                    }
                    this.x = true;
                    if (this.A) {
                        requestLayout();
                        this.A = false;
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final boolean d() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        if (g.p() && this.x) {
            drawChild(canvas, this.w, getDrawingTime());
        }
    }

    public final void g() {
        removeFooterView(this.c);
    }

    public LoadingButton getFooterViewButton() {
        return this.r;
    }

    public LinearLayout getLoadingContainer() {
        return this.f;
    }

    public cv getOnCancelListener() {
        return this.u;
    }

    /* access modifiers changed from: protected */
    public int getRefreshLayout() {
        return R.layout.include_pull_to_refresh;
    }

    /* access modifiers changed from: protected */
    public int getRefreshingLayout() {
        return R.layout.include_pull_to_refreshing_header;
    }

    public final void h() {
        postDelayed(new br(this), 500);
    }

    public final void i() {
        setLoadingVisible(false);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        super.onLayout(z2, i2, i3, i4, i5);
        if (g.p()) {
            long expandableListPosition = getExpandableListPosition(getFirstVisiblePosition());
            int packedPositionGroup = ExpandableListView.getPackedPositionGroup(expandableListPosition);
            int packedPositionChild = ExpandableListView.getPackedPositionChild(expandableListPosition);
            if (this.v != null) {
                int b = this.v.b(packedPositionGroup, packedPositionChild);
                if (!(this.w == null || b == this.B)) {
                    this.B = b;
                    this.w.layout(0, 0, this.y, this.z);
                }
            }
            b(packedPositionGroup, packedPositionChild);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        if (this.w != null) {
            measureChild(this.w, i2, i3);
            this.y = this.w.getMeasuredWidth();
            this.z = this.w.getMeasuredHeight();
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.x) {
            switch (motionEvent.getAction()) {
                case 0:
                    this.C = motionEvent.getX();
                    this.D = motionEvent.getY();
                    if (this.C <= ((float) this.y) && this.D <= ((float) this.z)) {
                        return true;
                    }
                case 1:
                    float x2 = motionEvent.getX();
                    float y2 = motionEvent.getY();
                    float abs = Math.abs(x2 - this.C);
                    float abs2 = Math.abs(y2 - this.D);
                    if (x2 <= ((float) this.y) && y2 <= ((float) this.z) && abs <= ((float) this.y) && abs2 <= ((float) this.z)) {
                        View view = this.w;
                        return true;
                    }
            }
        }
        return super.onTouchEvent(motionEvent);
    }

    public void setAdapter(ExpandableListAdapter expandableListAdapter) {
        super.setAdapter(expandableListAdapter);
        removeFooterView(this.c);
        if (this.s) {
            addFooterView(this.c);
        }
        this.v = (b) expandableListAdapter;
        if (getListPaddingBottom() > 0) {
            f();
        }
    }

    public void setEnableLoadMoreFoolter(boolean z2) {
        this.s = z2;
    }

    public void setLastFlushTime(Date date) {
        String str = String.valueOf(getContext().getString(R.string.pull_to_refresh_lastTime)) + "：";
        if (date != null) {
            str = String.valueOf(str) + a.a(date);
        }
        this.j.setText(str);
        this.k.setText(str);
    }

    public void setLoadingViewText(int i2) {
        this.l.setText(i2);
    }

    public void setLoadingVisible(boolean z2) {
        boolean z3 = false;
        if (this.f != null) {
            this.d = z2;
            if (z2) {
                this.f.setAnimation(null);
                this.f.setVisibility(0);
                this.g.startAnimation(this.t);
                if (l() != z2) {
                    setPreventOverScroll(z2);
                }
                if (!z2) {
                    z3 = true;
                }
                setTipVisible(z3);
                return;
            }
            this.g.clearAnimation();
            this.f.setVisibility(8);
            if (l()) {
                setPreventOverScroll(false);
            }
            setTipVisible(true);
        }
    }

    public void setMMHeaderView(View view) {
        this.w = view;
        view.setLayoutParams(new AbsListView.LayoutParams(-1, -2));
        if (this.w != null) {
            setFadingEdgeLength(0);
        }
        requestLayout();
    }

    public void setNoMoreRefresh(boolean z2) {
        this.h = z2;
    }

    public void setOnCancelListener(cv cvVar) {
        this.u = cvVar;
    }

    public void setOnPullToRefreshListener(bu buVar) {
        this.e = buVar;
    }

    /* access modifiers changed from: protected */
    public void setOverScrollState(int i2) {
        if (i2 != this.i) {
            switch (i2) {
                case 1:
                    this.m.setText((int) R.string.pull_to_refresh_pull_label);
                    break;
                case 2:
                    this.m.setText((int) R.string.pull_to_refresh_release_label);
                    break;
                case 3:
                    this.m.setText((int) R.string.pull_to_refresh_refreshing_label);
                    break;
                case 4:
                    this.m.setText((int) R.string.pull_to_refresh_pull_label);
                    break;
            }
            this.i = i2;
        }
    }

    public void setTimeEnable(boolean z2) {
        if (z2) {
            this.j.setVisibility(0);
            this.k.setVisibility(0);
            return;
        }
        this.j.setVisibility(8);
        this.k.setVisibility(8);
    }

    public void setTipVisible(boolean z2) {
    }
}
