package com.immomo.momo.android.activity;

import android.os.Bundle;
import android.support.v4.a.b;
import android.view.View;
import android.widget.ListAdapter;
import com.immomo.momo.R;
import com.immomo.momo.android.a.g;
import com.immomo.momo.android.broadcast.e;
import com.immomo.momo.android.broadcast.w;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.MomoRefreshListView;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.android.view.cv;
import com.immomo.momo.service.aq;
import java.util.Date;
import java.util.List;

public class BlacklistActivity extends ah implements b, View.OnClickListener, bu, cv {
    private HeaderLayout h;
    /* access modifiers changed from: private */
    public MomoRefreshListView i;
    /* access modifiers changed from: private */
    public List j = null;
    /* access modifiers changed from: private */
    public aq k;
    /* access modifiers changed from: private */
    public g l;
    private e m;
    /* access modifiers changed from: private */
    public Date n = null;
    private w o = null;
    private bb p = null;

    static /* synthetic */ void a(BlacklistActivity blacklistActivity, List list) {
        if (list != null && list.size() > 0) {
            List list2 = blacklistActivity.j;
            blacklistActivity.j = list;
            blacklistActivity.l = new g(blacklistActivity.getApplicationContext(), blacklistActivity.j, blacklistActivity.i);
            blacklistActivity.i.setAdapter((ListAdapter) blacklistActivity.l);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_blacklist);
        this.i = (MomoRefreshListView) findViewById(R.id.listview);
        this.h = (HeaderLayout) findViewById(R.id.layout_header);
        this.h.setTitleText((int) R.string.blacklist_header_title);
        this.i.setOnCancelListener$135502(this);
        this.i.setOnPullToRefreshListener$42b903f6(this);
        this.i.setOnItemLongClickListener(new av(this));
        this.i.setOnItemClickListener(new ay(this));
        this.m = new e(this);
        this.m.a(new az(this));
        this.o = new w(this);
        this.o.a(new ba(this));
        d();
    }

    public final void b_() {
        this.p = new bb(this, (byte) 0);
        this.p.execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.k = new aq();
        this.j = this.k.i();
        this.l = new g(this, this.j, this.i);
        this.i.setAdapter((ListAdapter) this.l);
        if (this.j.size() <= 0) {
            this.i.l();
        }
        this.i.setLastFlushTime(this.g.e());
        this.n = this.g.d();
    }

    public void onClick(View view) {
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.m != null) {
            unregisterReceiver(this.m);
            this.m = null;
        }
        if (this.o != null) {
            unregisterReceiver(this.o);
            this.o = null;
        }
        super.onDestroy();
    }

    public final void v() {
        if (this.p != null && !this.p.isCancelled()) {
            this.p.cancel(true);
            this.p = null;
        }
    }
}
