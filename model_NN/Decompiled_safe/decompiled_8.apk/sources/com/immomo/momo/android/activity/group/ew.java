package com.immomo.momo.android.activity.group;

import android.text.Editable;
import android.text.TextWatcher;
import com.amap.mapapi.poisearch.PoiTypeDef;

final class ew implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SearchGroupActivity f1644a;

    ew(SearchGroupActivity searchGroupActivity) {
        this.f1644a = searchGroupActivity;
    }

    public final void afterTextChanged(Editable editable) {
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        this.f1644a.r.setVisibility(charSequence.toString().equals(PoiTypeDef.All) ? 8 : 0);
    }
}
