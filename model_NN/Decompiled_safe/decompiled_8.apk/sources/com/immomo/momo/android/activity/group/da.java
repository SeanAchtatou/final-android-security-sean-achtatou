package com.immomo.momo.android.activity.group;

import android.graphics.Bitmap;
import com.immomo.momo.R;
import com.immomo.momo.a;
import com.immomo.momo.android.view.a.ao;
import com.immomo.momo.util.h;
import java.io.File;

final class da implements Runnable {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ cz f1598a;
    private final /* synthetic */ Bitmap b;
    private final /* synthetic */ String c;
    private final /* synthetic */ String d;

    da(cz czVar, Bitmap bitmap, String str, String str2) {
        this.f1598a = czVar;
        this.b = bitmap;
        this.c = str;
        this.d = str2;
    }

    public final void run() {
        if (!this.f1598a.f1597a.isFinishing()) {
            ao aoVar = new ao(this.f1598a.f1597a, this.f1598a.f1597a.f);
            aoVar.setTitle((int) R.string.group_sharedialog_title);
            aoVar.a(this.c.trim(), (int) R.drawable.round_shareobox);
            aoVar.a(this.b);
            File file = new File(a.b(), "groupshare.png_");
            h.a(this.b, file);
            aoVar.a(new db(this, aoVar, this.d, file));
            aoVar.show();
            this.f1598a.f1597a.g.c(String.valueOf(this.f1598a.f1597a.l) + "_alertshared", false);
        } else if (this.b != null && !this.b.isRecycled()) {
            this.b.recycle();
        }
    }
}
