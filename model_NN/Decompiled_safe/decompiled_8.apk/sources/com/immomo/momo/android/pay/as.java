package com.immomo.momo.android.pay;

import android.os.Handler;

final class as implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ap f2537a;
    private final /* synthetic */ String b;
    private final /* synthetic */ Handler c;

    as(ap apVar, String str, Handler handler) {
        this.f2537a = apVar;
        this.b = str;
        this.c = handler;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0060, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0061, code lost:
        r3.f2537a.e.a((java.lang.Throwable) r0);
        r1 = new android.os.Message();
        r1.what = 2;
        r1.obj = r0.toString();
        r3.c.sendMessage(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r3 = this;
            com.immomo.momo.android.pay.ap r0 = r3.f2537a     // Catch:{ Exception -> 0x0060 }
            java.lang.Integer r1 = r0.f2534a     // Catch:{ Exception -> 0x0060 }
            monitor-enter(r1)     // Catch:{ Exception -> 0x0060 }
            com.immomo.momo.android.pay.ap r0 = r3.f2537a     // Catch:{ all -> 0x005d }
            com.alipay.android.app.IAlixPay r0 = r0.b     // Catch:{ all -> 0x005d }
            if (r0 != 0) goto L_0x0012
            com.immomo.momo.android.pay.ap r0 = r3.f2537a     // Catch:{ all -> 0x005d }
            java.lang.Integer r0 = r0.f2534a     // Catch:{ all -> 0x005d }
            r0.wait()     // Catch:{ all -> 0x005d }
        L_0x0012:
            monitor-exit(r1)     // Catch:{ all -> 0x005d }
            com.immomo.momo.android.pay.ap r0 = r3.f2537a     // Catch:{ Exception -> 0x0060 }
            com.alipay.android.app.IAlixPay r0 = r0.b     // Catch:{ Exception -> 0x0060 }
            com.immomo.momo.android.pay.ap r1 = r3.f2537a     // Catch:{ Exception -> 0x0060 }
            com.alipay.android.app.IRemoteServiceCallback r1 = r1.g     // Catch:{ Exception -> 0x0060 }
            r0.registerCallback(r1)     // Catch:{ Exception -> 0x0060 }
            com.immomo.momo.android.pay.ap r0 = r3.f2537a     // Catch:{ Exception -> 0x0060 }
            com.alipay.android.app.IAlixPay r0 = r0.b     // Catch:{ Exception -> 0x0060 }
            java.lang.String r1 = r3.b     // Catch:{ Exception -> 0x0060 }
            java.lang.String r0 = r0.Pay(r1)     // Catch:{ Exception -> 0x0060 }
            com.immomo.momo.android.pay.ap r1 = r3.f2537a     // Catch:{ Exception -> 0x0060 }
            r2 = 0
            r1.c = r2     // Catch:{ Exception -> 0x0060 }
            com.immomo.momo.android.pay.ap r1 = r3.f2537a     // Catch:{ Exception -> 0x0060 }
            com.alipay.android.app.IAlixPay r1 = r1.b     // Catch:{ Exception -> 0x0060 }
            com.immomo.momo.android.pay.ap r2 = r3.f2537a     // Catch:{ Exception -> 0x0060 }
            com.alipay.android.app.IRemoteServiceCallback r2 = r2.g     // Catch:{ Exception -> 0x0060 }
            r1.unregisterCallback(r2)     // Catch:{ Exception -> 0x0060 }
            com.immomo.momo.android.pay.ap r1 = r3.f2537a     // Catch:{ Exception -> 0x0060 }
            android.app.Activity r1 = r1.d     // Catch:{ Exception -> 0x0060 }
            android.content.Context r1 = r1.getApplicationContext()     // Catch:{ Exception -> 0x0060 }
            com.immomo.momo.android.pay.ap r2 = r3.f2537a     // Catch:{ Exception -> 0x0060 }
            android.content.ServiceConnection r2 = r2.f     // Catch:{ Exception -> 0x0060 }
            r1.unbindService(r2)     // Catch:{ Exception -> 0x0060 }
            android.os.Message r1 = new android.os.Message     // Catch:{ Exception -> 0x0060 }
            r1.<init>()     // Catch:{ Exception -> 0x0060 }
            r2 = 1
            r1.what = r2     // Catch:{ Exception -> 0x0060 }
            r1.obj = r0     // Catch:{ Exception -> 0x0060 }
            android.os.Handler r0 = r3.c     // Catch:{ Exception -> 0x0060 }
            r0.sendMessage(r1)     // Catch:{ Exception -> 0x0060 }
        L_0x005c:
            return
        L_0x005d:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ Exception -> 0x0060 }
            throw r0     // Catch:{ Exception -> 0x0060 }
        L_0x0060:
            r0 = move-exception
            com.immomo.momo.android.pay.ap r1 = r3.f2537a
            com.immomo.momo.util.m r1 = r1.e
            r1.a(r0)
            android.os.Message r1 = new android.os.Message
            r1.<init>()
            r2 = 2
            r1.what = r2
            java.lang.String r0 = r0.toString()
            r1.obj = r0
            android.os.Handler r0 = r3.c
            r0.sendMessage(r1)
            goto L_0x005c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.immomo.momo.android.pay.as.run():void");
    }
}
