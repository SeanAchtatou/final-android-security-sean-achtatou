package com.immomo.momo.android.activity;

import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.android.view.a.o;
import com.immomo.momo.service.bean.au;

final class cw implements View.OnLongClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ EditUserProfileActivity f1210a;

    cw(EditUserProfileActivity editUserProfileActivity) {
        this.f1210a = editUserProfileActivity;
    }

    public final boolean onLongClick(View view) {
        au auVar = (au) this.f1210a.h.get(((jt) view.getTag()).b);
        if (auVar.d) {
            return false;
        }
        o oVar = new o(this.f1210a, (int) R.array.profile_edit_avatar);
        oVar.setTitle((int) R.string.dialog_title_avatar_long_press);
        oVar.a(new cx(this, auVar));
        oVar.show();
        return true;
    }
}
