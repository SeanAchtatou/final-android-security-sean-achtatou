package com.immomo.momo.android.view.a;

import android.widget.AbsListView;

final class k implements AbsListView.OnScrollListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ e f2699a;

    private k(e eVar) {
        this.f2699a = eVar;
    }

    /* synthetic */ k(e eVar, byte b) {
        this(eVar);
    }

    public final void onScroll(AbsListView absListView, int i, int i2, int i3) {
    }

    public final void onScrollStateChanged(AbsListView absListView, int i) {
        if (i == 1 && !this.f2699a.h() && this.f2699a.c.getContentView() != null) {
            this.f2699a.p.removeCallbacks(this.f2699a.l);
            this.f2699a.l.run();
        }
    }
}
