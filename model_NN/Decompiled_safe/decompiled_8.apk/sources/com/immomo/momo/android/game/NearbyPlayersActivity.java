package com.immomo.momo.android.game;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListAdapter;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.a.dl;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.activity.d;
import com.immomo.momo.android.b.o;
import com.immomo.momo.android.b.z;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.MomoRefreshListView;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.bean.GameApp;
import com.immomo.momo.service.u;
import java.util.ArrayList;

public class NearbyPlayersActivity extends ah implements bu {
    /* access modifiers changed from: private */
    public MomoRefreshListView h = null;
    /* access modifiers changed from: private */
    public LoadingButton i;
    /* access modifiers changed from: private */
    public dl j = null;
    /* access modifiers changed from: private */
    public bg k = null;
    /* access modifiers changed from: private */
    public String l = null;
    /* access modifiers changed from: private */
    public aq m = null;
    /* access modifiers changed from: private */
    public o n = null;
    /* access modifiers changed from: private */
    public String o;
    private GameApp p = null;
    private String q;

    public final void a(Intent intent, int i2, Bundle bundle, String str) {
        if (intent.getComponent() != null && d.g(intent.getComponent().getClassName())) {
            intent.putExtra("afromname", this.p != null ? this.p.appname : PoiTypeDef.All);
        }
        super.a(intent, i2, bundle, str);
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_game_nearbyplayers);
        this.o = getIntent().getStringExtra("appid");
        this.q = getIntent().getStringExtra("title");
        this.m = new aq();
        this.h = (MomoRefreshListView) findViewById(R.id.listview);
        this.h.setEnableLoadMoreFoolter(true);
        this.i = this.h.getFooterViewButton();
        this.h.setTimeEnable(false);
        setTitle(this.q);
        this.i.setVisibility(8);
        this.i.setOnProcessListener(new bc(this));
        this.h.setOnPullToRefreshListener$42b903f6(this);
        this.h.setOnItemClickListener(new bd(this));
        this.h.setOnCancelListener$135502(new be(this));
        d();
    }

    public final void b_() {
        b(new bg(this, this));
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.j = new dl(this, new ArrayList(), this.h, false);
        this.h.setAdapter((ListAdapter) this.j);
        this.h.l();
        this.p = new u().a(this.o);
        if (this.p != null) {
            setTitle(this.p.appname);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.l != null) {
            z.a(this.l);
            this.l = null;
        }
    }
}
