package com.immomo.momo.android.a;

import android.content.Context;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.c.r;
import com.immomo.momo.android.c.u;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.e;
import java.util.List;

public final class fz extends a {
    /* access modifiers changed from: private */
    public HandyListView d;
    /* access modifiers changed from: private */
    public ge e;

    public fz(Context context, List list, HandyListView handyListView, ge geVar) {
        super(context, list);
        this.d = handyListView;
        this.e = geVar;
    }

    public final void d(int i) {
        e eVar = (e) getItem(i);
        if (eVar != null) {
            eVar.f = true;
        }
        notifyDataSetChanged();
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            gb gbVar = new gb((byte) 0);
            view = g.o().inflate((int) R.layout.listitem_sinacontactchild, (ViewGroup) null);
            gbVar.f835a = (TextView) view.findViewById(R.id.tv_name);
            gbVar.b = (TextView) view.findViewById(R.id.tv_operate);
            gbVar.c = (ImageView) view.findViewById(R.id.userlist_item_iv_face);
            gbVar.e = view.findViewById(R.id.iv_arrow);
            gbVar.f = (Button) view.findViewById(R.id.btn_operate);
            gbVar.d = (ImageView) view.findViewById(R.id.userlist_item_iv_face_cover_click);
            view.setTag(R.id.tag_userlist_item, gbVar);
        }
        e eVar = (e) getItem(i);
        gb gbVar2 = (gb) view.getTag(R.id.tag_userlist_item);
        gbVar2.f835a.setText(eVar.d);
        ImageView imageView = gbVar2.c;
        String str = eVar.g;
        String str2 = eVar.c;
        imageView.setTag(R.id.tag_item_imageid, str2);
        if (eVar.h == null && !a.a((CharSequence) str2) && !a.a((CharSequence) str) && !this.d.h()) {
            r rVar = new r(str2, new gc(this, eVar), 6, null);
            rVar.a(str);
            u.b().execute(rVar);
        }
        imageView.setImageBitmap(eVar.h == null ? g.t() : eVar.h);
        gbVar2.e.setVisibility(0);
        gbVar2.b.setVisibility(8);
        gbVar2.f.setVisibility(0);
        gbVar2.f.setFocusable(false);
        if (eVar.f) {
            gbVar2.f.setEnabled(false);
            gbVar2.f.setText("已邀请");
        } else {
            gbVar2.f.setEnabled(true);
            gbVar2.f.setText("邀请");
        }
        gbVar2.f.setOnClickListener(new ga(this, i));
        gbVar2.d.setClickable(false);
        return view;
    }
}
