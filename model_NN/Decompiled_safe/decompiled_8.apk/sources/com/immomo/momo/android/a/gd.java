package com.immomo.momo.android.a;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

final class gd extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ gc f837a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public gd(gc gcVar, Looper looper) {
        super(looper);
        this.f837a = gcVar;
    }

    public final void handleMessage(Message message) {
        if (message.what == 1 && this.f837a.c != null) {
            this.f837a.f836a.notifyDataSetChanged();
        }
    }
}
