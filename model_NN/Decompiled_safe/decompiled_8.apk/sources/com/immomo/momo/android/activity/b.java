package com.immomo.momo.android.activity;

import android.content.Intent;
import android.view.View;

final class b implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ AboutActivity f1037a;

    b(AboutActivity aboutActivity) {
        this.f1037a = aboutActivity;
    }

    public final void onClick(View view) {
        this.f1037a.startActivity(new Intent(this.f1037a, UserGuideActivity.class));
    }
}
