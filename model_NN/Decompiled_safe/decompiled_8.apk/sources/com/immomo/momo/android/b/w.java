package com.immomo.momo.android.b;

import android.location.Location;
import java.util.List;
import mm.purchasesdk.PurchaseCode;

final class w implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ q f2341a;
    private final /* synthetic */ Object b;
    private final /* synthetic */ String c;
    private final /* synthetic */ List d;
    private final /* synthetic */ p e;
    private final /* synthetic */ boolean f;

    w(q qVar, Object obj, String str, List list, p pVar, boolean z) {
        this.f2341a = qVar;
        this.b = obj;
        this.c = str;
        this.d = list;
        this.e = pVar;
        this.f = z;
    }

    public final void run() {
        synchronized (this.b) {
            try {
                this.b.wait(60000);
                this.f2341a.f2335a.a(this.c);
                if (this.d.size() <= 0) {
                    this.e.a(null, 0, PurchaseCode.QUERY_OK, PurchaseCode.LOADCHANNEL_ERR);
                } else {
                    Location location = (Location) this.d.get(this.d.size() - 1);
                    if (this.f) {
                        new l(this.e, PurchaseCode.LOADCHANNEL_ERR).execute(location);
                    } else {
                        this.e.a(location, 0, 100, PurchaseCode.LOADCHANNEL_ERR);
                    }
                }
            } catch (Exception e2) {
                this.f2341a.b.a((Throwable) e2);
                this.e.a(null, 0, PurchaseCode.QUERY_OK, PurchaseCode.LOADCHANNEL_ERR);
            }
        }
        return;
    }
}
