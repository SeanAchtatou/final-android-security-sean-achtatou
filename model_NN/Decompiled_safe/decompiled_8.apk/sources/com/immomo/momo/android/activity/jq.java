package com.immomo.momo.android.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v4.b.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;

final class jq extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1772a;
    private /* synthetic */ PhoneNumberBindEmailActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public jq(PhoneNumberBindEmailActivity phoneNumberBindEmailActivity, Context context) {
        super(context);
        this.c = phoneNumberBindEmailActivity;
        this.f1772a = new v(phoneNumberBindEmailActivity);
        this.f1772a.a("请求提交中");
        this.f1772a.setCancelable(true);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        com.immomo.momo.protocol.a.d.e(this.c.j.getText().toString().trim(), a.n(this.c.k.getText().toString().trim()));
        return "yes";
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (this.f1772a != null) {
            this.f1772a.setOnCancelListener(new jr(this));
            this.f1772a.show();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        if (str != null && str.equals("yes")) {
            a("验证邮件已发送到你的注册email账号");
            this.c.setResult(0, null);
            Intent intent = new Intent(this.c, ResendEmailActivity.class);
            intent.putExtra("email", this.c.j.getText().toString().trim());
            intent.putExtra("password", a.n(this.c.k.getText().toString().trim()));
            this.c.startActivity(intent);
            this.c.finish();
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.f1772a != null) {
            this.f1772a.dismiss();
            this.f1772a = null;
        }
    }
}
