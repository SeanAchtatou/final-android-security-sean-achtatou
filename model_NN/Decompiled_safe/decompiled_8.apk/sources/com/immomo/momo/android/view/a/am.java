package com.immomo.momo.android.view.a;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.immomo.momo.R;
import java.util.Arrays;

public final class am extends aw implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private ListView f2673a;
    private al b;

    private am(Context context) {
        super(context, R.layout.include_dialog_simplelist_drak);
        this.f2673a = null;
        this.b = null;
        b(R.style.Popup_Animation_PushDownUp);
        this.f2673a = (ListView) c(R.id.listview);
        this.f2673a.setOnItemClickListener(this);
    }

    private am(Context context, Object[] objArr) {
        this(context);
        this.f2673a.setAdapter((ListAdapter) new an(this.d, Arrays.asList(objArr)));
    }

    public am(Context context, Object[] objArr, byte b2) {
        this(context, objArr);
    }

    public final void a(al alVar) {
        this.b = alVar;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (this.b != null) {
            this.b.a(i);
        }
        e();
    }
}
