package com.immomo.momo.android.activity.retrieve;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.a.g;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.util.ao;

final class i extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f2115a = null;
    private String c = null;
    private /* synthetic */ InputPhoneNumberActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public i(InputPhoneNumberActivity inputPhoneNumberActivity, Context context, String str) {
        super(context);
        this.d = inputPhoneNumberActivity;
        this.c = str;
        if (inputPhoneNumberActivity.s != null) {
            inputPhoneNumberActivity.s.cancel(true);
        }
        inputPhoneNumberActivity.s = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        this.b.a((Object) ("校验：" + this.c));
        return w.a().c(this.c, this.d.u);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f2115a = new v(this.d, "请稍候，正在校验....");
        this.f2115a.setOnCancelListener(new j(this));
        this.f2115a.show();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (exc instanceof g) {
            this.d.b(new m(this.d, this.d));
            this.d.x.setImageBitmap(null);
            ao.d(R.string.reg_scode_timeout);
            return;
        }
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        if (this.d.z != null && this.d.z.isShowing()) {
            this.d.z.dismiss();
        }
        this.d.v = str;
        this.d.g();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f2115a.dismiss();
        this.f2115a = null;
    }
}
