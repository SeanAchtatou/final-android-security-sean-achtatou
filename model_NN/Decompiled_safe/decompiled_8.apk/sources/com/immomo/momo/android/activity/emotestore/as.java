package com.immomo.momo.android.activity.emotestore;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.i;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

final class as extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ am f1296a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public as(am amVar, Context context) {
        super(context);
        this.f1296a = amVar;
        if (amVar.T != null) {
            amVar.T.cancel(true);
        }
        amVar.T = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        i.a().a(arrayList, this.f1296a.R.getCount());
        this.f1296a.Q.b(arrayList);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List list = (List) obj;
        if (list.size() < 30) {
            this.f1296a.P.setVisibility(8);
        } else {
            this.f1296a.P.setVisibility(0);
        }
        this.f1296a.R.b((Collection) list);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f1296a.P.e();
    }
}
