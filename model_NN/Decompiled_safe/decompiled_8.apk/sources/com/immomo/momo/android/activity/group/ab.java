package com.immomo.momo.android.activity.group;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Message;
import com.immomo.momo.android.c.g;
import com.immomo.momo.service.bean.au;
import com.immomo.momo.util.j;

final class ab implements g {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ aa f1527a;
    private final /* synthetic */ au b;

    ab(aa aaVar, au auVar) {
        this.f1527a = aaVar;
        this.b = auVar;
    }

    public final /* synthetic */ void a(Object obj) {
        Bitmap bitmap = (Bitmap) obj;
        if (j.a(this.b.b) == null && bitmap != null) {
            j.a(this.b.b, bitmap);
        }
        Message message = new Message();
        Bundle bundle = new Bundle();
        bundle.putString("guid", this.b.b);
        message.setData(bundle);
        message.obj = bitmap;
        message.what = 11;
        this.f1527a.f1526a.H.sendMessage(message);
    }
}
