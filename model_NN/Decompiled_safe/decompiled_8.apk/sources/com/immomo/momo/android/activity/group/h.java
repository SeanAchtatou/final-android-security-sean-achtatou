package com.immomo.momo.android.activity.group;

import android.app.DatePickerDialog;
import android.support.v4.b.a;
import android.widget.DatePicker;
import java.util.Calendar;
import java.util.Date;

final class h implements DatePickerDialog.OnDateSetListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EditGroupPartyActivity f1683a;

    h(EditGroupPartyActivity editGroupPartyActivity) {
        this.f1683a = editGroupPartyActivity;
    }

    public final void onDateSet(DatePicker datePicker, int i, int i2, int i3) {
        this.f1683a.h = i;
        this.f1683a.i = i2;
        this.f1683a.j = i3;
        this.f1683a.k = this.f1683a.m.get(11);
        this.f1683a.l = this.f1683a.m.get(12);
        this.f1683a.m.clear();
        this.f1683a.m.set(this.f1683a.h, this.f1683a.i, this.f1683a.j, this.f1683a.k, this.f1683a.l);
        this.f1683a.x.f = this.f1683a.m.getTime();
        this.f1683a.q.setText(a.g(this.f1683a.x.f));
        this.f1683a.e.b((Object) ("setting clicked ClaTime:" + this.f1683a.m.getTime().getTime() + "/" + Calendar.getInstance().getTime().getTime() + "/" + Calendar.getInstance().getTimeInMillis() + " DateTine:" + this.f1683a.x.f.getTime() + "/" + new Date().getTime()));
        EditGroupPartyActivity.o(this.f1683a);
    }
}
