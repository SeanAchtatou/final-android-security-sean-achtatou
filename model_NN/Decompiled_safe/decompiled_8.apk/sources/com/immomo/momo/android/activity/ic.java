package com.immomo.momo.android.activity;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.android.a.gw;
import com.immomo.momo.android.activity.plugin.RenrenActivity;
import com.immomo.momo.android.activity.plugin.SinaWeiboActivity;
import com.immomo.momo.android.activity.plugin.TxWeiboActivity;
import com.immomo.momo.android.game.GameProfileActivity;
import com.immomo.momo.android.view.f;
import com.immomo.momo.util.k;
import org.json.JSONObject;

final class ic implements f {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ OtherProfileV2Activity f1734a;

    ic(OtherProfileV2Activity otherProfileV2Activity) {
        this.f1734a = otherProfileV2Activity;
    }

    public final void a(int i, View view) {
        gw a2 = this.f1734a.aF.getItem(i);
        if (a2.b == 1) {
            Intent intent = new Intent(this.f1734a, SinaWeiboActivity.class);
            intent.putExtra("uid", this.f1734a.t.am);
            intent.putExtra("momoid", this.f1734a.t.h);
            this.f1734a.startActivity(intent);
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("botton", "sina");
                new k("C", "C2108", jSONObject).e();
            } catch (Throwable th) {
            }
        } else if (a2.b == 2) {
            Intent intent2 = new Intent(this.f1734a, TxWeiboActivity.class);
            intent2.putExtra("uid", this.f1734a.t.h);
            this.f1734a.startActivity(intent2);
            try {
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("botton", "tencent");
                new k("C", "C2108", jSONObject2).e();
            } catch (Throwable th2) {
            }
        } else if (a2.b == 3) {
            Intent intent3 = new Intent(this.f1734a, RenrenActivity.class);
            intent3.putExtra("momoid", this.f1734a.t.h);
            this.f1734a.startActivity(intent3);
            try {
                JSONObject jSONObject3 = new JSONObject();
                jSONObject3.put("botton", "renren");
                new k("C", "C2108", jSONObject3).e();
            } catch (Throwable th3) {
            }
        } else if (a2.b == 4) {
            Intent intent4 = new Intent(this.f1734a, GameProfileActivity.class);
            intent4.putExtra("appid", a2.f851a.appid);
            this.f1734a.startActivity(intent4);
        }
    }
}
