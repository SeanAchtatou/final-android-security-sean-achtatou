package com.immomo.momo.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.a;
import com.immomo.momo.android.c.r;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.g;
import java.util.Calendar;

public class ShareGroupPageActivity extends ah implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private static String p = (String.valueOf(a.c) + "/sharecard").replace("https", "http");
    Handler h = new kg(this);
    private HeaderLayout i = null;
    /* access modifiers changed from: private */
    public CheckBox j;
    private Button k;
    private View l;
    private TextView m;
    /* access modifiers changed from: private */
    public ImageView n;
    private int o = -1;

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_setting_sharepage_group);
        d();
        this.i = (HeaderLayout) findViewById(R.id.layout_header);
        this.i.setTitleText("分享陌陌号");
        this.j = (CheckBox) findViewById(R.id.checkbox_focus);
        this.k = (Button) findViewById(R.id.share_button);
        this.l = findViewById(R.id.layout_focus_momo);
        this.m = (TextView) findViewById(R.id.share_title);
        this.n = (ImageView) findViewById(R.id.share_image);
        if (this.f != null) {
            r rVar = new r(g.c("momoshared_" + this.f.h), new kh(this), 17, null);
            rVar.a(String.valueOf(p) + "/" + this.f.h + ".jpg?day=" + Calendar.getInstance().get(5));
            new Thread(rVar).start();
        }
        if (this.o == 0) {
            this.l.setVisibility(0);
        } else {
            this.l.setVisibility(8);
        }
        String str = PoiTypeDef.All;
        switch (this.o) {
            case 0:
                str = "新浪微博";
                break;
            case 2:
                str = "腾讯微博";
                break;
            case 3:
                str = "人人网";
                break;
            case 4:
                str = "QQ空间";
                break;
        }
        this.m.setText(String.valueOf(g.a((int) R.string.share_momocard_title)) + str);
        this.j.setOnCheckedChangeListener(this);
        this.k.setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.o = getIntent().getIntExtra("share_type", -1);
    }

    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.share_button /*2131165926*/:
                switch (this.o) {
                    case 0:
                        if (this.f == null) {
                            return;
                        }
                        if (this.f.an) {
                            new kk(this).execute(new String[0]);
                            return;
                        }
                        Intent intent = new Intent(this, ShareWebviewActivity.class);
                        intent.putExtra("share_type", 0);
                        startActivity(intent);
                        return;
                    case 1:
                    default:
                        return;
                    case 2:
                        Intent intent2 = new Intent(this, ShareWebviewActivity.class);
                        intent2.putExtra("share_type", 2);
                        startActivity(intent2);
                        return;
                    case 3:
                        if (this.f == null) {
                            return;
                        }
                        if (this.f.ar) {
                            new ki(this).execute(new String[0]);
                            return;
                        }
                        Intent intent3 = new Intent(this, ShareWebviewActivity.class);
                        intent3.putExtra("share_type", 3);
                        startActivity(intent3);
                        return;
                    case 4:
                        Intent intent4 = new Intent(this, ShareWebviewActivity.class);
                        intent4.putExtra("share_type", 4);
                        startActivity(intent4);
                        return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }
}
