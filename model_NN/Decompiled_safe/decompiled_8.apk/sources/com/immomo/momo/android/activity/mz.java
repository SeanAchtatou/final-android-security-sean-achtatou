package com.immomo.momo.android.activity;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.android.activity.account.LoginActivity;

final class mz implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ WelcomeActivity f2016a;

    mz(WelcomeActivity welcomeActivity) {
        this.f2016a = welcomeActivity;
    }

    public final void onClick(View view) {
        this.f2016a.startActivity(new Intent(this.f2016a.getApplicationContext(), LoginActivity.class));
    }
}
