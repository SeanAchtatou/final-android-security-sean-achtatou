package com.immomo.momo.android.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import com.immomo.momo.R;
import com.immomo.momo.android.view.ScrollViewPager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public abstract class jx extends lh {
    private final ArrayList O = new ArrayList();
    private ScrollViewPager P;
    private boolean Q = true;
    /* access modifiers changed from: private */
    public Map R = new HashMap();
    /* access modifiers changed from: private */
    public int S = -1;
    private int T = 1;

    public final void O() {
        this.T = 4;
        if (this.P != null) {
            this.P.setOffscreenPageLimit(4);
        }
    }

    public final lh P() {
        return (lh) this.R.get(Integer.valueOf(this.P.getCurrentItem()));
    }

    public final int Q() {
        return this.P.getCurrentItem();
    }

    /* access modifiers changed from: protected */
    public void a(Fragment fragment, int i) {
    }

    public final void a(Class... clsArr) {
        for (Class jyVar : clsArr) {
            this.O.add(new jy(jyVar));
        }
    }

    public final boolean a(int i, KeyEvent keyEvent) {
        Fragment fragment = (Fragment) this.R.get(Integer.valueOf(this.S));
        if (fragment == null || !(fragment instanceof lh) || !((lh) fragment).a(i, keyEvent)) {
            return super.a(i, keyEvent);
        }
        return true;
    }

    public final boolean b(int i, KeyEvent keyEvent) {
        Fragment fragment = (Fragment) this.R.get(Integer.valueOf(this.S));
        if (fragment == null || !(fragment instanceof lh) || !((lh) fragment).b(i, keyEvent)) {
            return super.b(i, keyEvent);
        }
        return true;
    }

    public final void c(Bundle bundle) {
        if (this.P != null) {
            bundle.putInt("tab", this.P.getCurrentItem());
        }
        super.c(bundle);
    }

    public final void f(int i) {
        if (this.P != null) {
            this.P.setCurrentItem(i);
        }
        this.S = i;
    }

    /* access modifiers changed from: protected */
    public final void i(Bundle bundle) {
        super.i(bundle);
        this.P = (ScrollViewPager) c((int) R.id.pagertabcontent);
        this.P.setEnableTouchScroll(this.Q);
        this.P.setOffscreenPageLimit(this.T);
        new jz(this, this, this.P, this.O);
        if (this.S != -1) {
            this.P.setCurrentItem(this.S);
        } else {
            this.P.setCurrentItem(0);
        }
    }
}
