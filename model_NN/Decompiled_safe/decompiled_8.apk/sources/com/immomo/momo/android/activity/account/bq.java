package com.immomo.momo.android.activity.account;

import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import com.immomo.momo.android.activity.ProtocolActivity;
import com.immomo.momo.android.view.a.q;
import mm.purchasesdk.PurchaseCode;

final class bq extends ClickableSpan {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bn f970a;

    bq(bn bnVar) {
        this.f970a = bnVar;
    }

    public final void onClick(View view) {
        q a2 = q.a(this.f970a.q);
        a2.b(PurchaseCode.BILL_DYMARK_CREATE_ERROR);
        a2.setTitle("用户协议");
        a2.a(ProtocolActivity.i);
        a2.show();
    }

    public final void updateDrawState(TextPaint textPaint) {
    }
}
