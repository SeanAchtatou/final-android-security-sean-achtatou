package com.immomo.momo.android.pay;

import android.content.Context;
import com.immomo.momo.a.b;
import com.immomo.momo.a.c;
import com.immomo.momo.a.t;
import com.immomo.momo.a.w;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.a;
import com.immomo.momo.protocol.a.a.f;
import com.immomo.momo.service.aq;
import com.immomo.momo.util.ao;

final class j extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f2561a = null;
    private /* synthetic */ BuyMemberActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public j(BuyMemberActivity buyMemberActivity, Context context) {
        super(context);
        this.c = buyMemberActivity;
        this.f2561a = new v(context);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        f a2 = a.a().a(this.c.x, this.c.u, this.c.A ? this.c.z : this.c.y);
        this.c.f.b((long) a2.b);
        new aq().b(this.c.f);
        return a2.f2885a;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f2561a.setCancelable(false);
        this.f2561a.a("请求提交中....");
        this.c.a(this.f2561a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
        if (!(exc instanceof com.immomo.momo.a.d) && !(exc instanceof c) && !(exc instanceof b) && !(exc instanceof t) && !(exc instanceof w)) {
            BuyMemberActivity.p(this.c);
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        BuyMemberActivity.q(this.c);
        if (!com.immomo.a.a.f.a.a(str)) {
            ao.a((CharSequence) str);
        }
        this.c.x();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.p();
    }
}
