package com.immomo.momo.android.map;

import android.os.Handler;
import android.os.Message;
import com.amap.mapapi.map.RouteOverlay;
import com.amap.mapapi.route.Route;
import java.util.ArrayList;
import mm.purchasesdk.PurchaseCode;

final class a extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ AMapActivity f2454a;

    a(AMapActivity aMapActivity) {
        this.f2454a = aMapActivity;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case PurchaseCode.INIT_OK /*100*/:
                try {
                    if (this.f2454a.o != null) {
                        this.f2454a.o.removeFromMap(this.f2454a.i);
                    }
                    if (this.f2454a.b.size() > 0) {
                        Route route = (Route) this.f2454a.b.get(0);
                        this.f2454a.o = new RouteOverlay(this.f2454a, route);
                        this.f2454a.o.registerRouteMessage(this.f2454a);
                        this.f2454a.o.addToMap(this.f2454a.i);
                        ArrayList arrayList = new ArrayList();
                        arrayList.add(route.getLowerLeftPoint());
                        arrayList.add(route.getUpperRightPoint());
                        this.f2454a.i.getController().setFitView(arrayList);
                        this.f2454a.i.invalidate();
                        this.f2454a.b();
                        return;
                    }
                    return;
                } catch (Exception e) {
                    this.f2454a.k.a((Throwable) e);
                    this.f2454a.b();
                    return;
                }
            default:
                return;
        }
    }
}
