package com.immomo.momo.android.a;

import android.content.Context;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.a.o;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.a.a;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.bean.i;
import com.immomo.momo.service.bean.j;

final class ao extends d {

    /* renamed from: a  reason: collision with root package name */
    private String f722a = PoiTypeDef.All;
    private i c = null;
    private j d = null;
    private /* synthetic */ ae e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ao(ae aeVar, Context context, String str, i iVar, j jVar) {
        super(context);
        this.e = aeVar;
        this.c = iVar;
        this.d = jVar;
        this.f722a = str;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        switch (this.c.b()) {
            case 1:
                return w.a().e(this.f722a, this.c.k());
            case 2:
                return w.a().f(this.f722a, this.c.c().f2875a);
            case 3:
                return w.a().g(this.f722a, this.c.c().f2875a);
            case 4:
                return w.a().h(this.f722a, this.c.c().f2875a);
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.e.e.a(new v(f(), this));
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (exc == null || !(exc instanceof o)) {
            super.a(exc);
            return;
        }
        a(exc.getMessage());
        this.d.d();
        this.c.b(true);
        this.e.d.a(this.c);
        this.e.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        if (this.c.b() == 1) {
            a aVar = (a) obj;
            if (aVar.b) {
                this.e.a(this.c.k());
            } else {
                a(aVar.f2880a);
            }
        } else {
            a((String) obj);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.e.e.p();
    }
}
