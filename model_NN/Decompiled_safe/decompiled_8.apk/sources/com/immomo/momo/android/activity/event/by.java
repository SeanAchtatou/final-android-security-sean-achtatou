package com.immomo.momo.android.activity.event;

import android.view.View;

final class by implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PublishEventFeedActivity f1392a;

    by(PublishEventFeedActivity publishEventFeedActivity) {
        this.f1392a = publishEventFeedActivity;
    }

    public final void onClick(View view) {
        if (PublishEventFeedActivity.r(this.f1392a)) {
            this.f1392a.a(new ce(this.f1392a, this.f1392a)).execute(new Object[0]);
        }
    }
}
