package com.immomo.momo.android.activity.maintab;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.k;
import com.immomo.momo.service.bean.ab;
import java.util.ArrayList;
import java.util.List;

final class v extends d {

    /* renamed from: a  reason: collision with root package name */
    private List f1901a = new ArrayList();
    private /* synthetic */ l c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public v(l lVar, Context context) {
        super(context);
        this.c = lVar;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        boolean a2 = k.a().a(this.f1901a, this.c.W.getCount(), this.c.M.S, this.c.M.T, this.c.M.aH);
        this.c.X.a(this.f1901a);
        return Boolean.valueOf(a2);
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        Boolean bool = (Boolean) obj;
        if (this.f1901a.isEmpty() || !bool.booleanValue()) {
            this.c.P.setVisibility(8);
        } else {
            this.c.P.setVisibility(0);
        }
        for (ab abVar : this.f1901a) {
            if (!this.c.S.contains(abVar)) {
                this.c.W.b(abVar);
                this.c.S.add(abVar);
            }
        }
        this.c.W.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.P.e();
    }
}
