package com.immomo.momo.android.plugin.cropimage;

import android.graphics.Bitmap;
import java.util.concurrent.CountDownLatch;

final class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ f f2586a;
    private final /* synthetic */ Bitmap b;
    private final /* synthetic */ CountDownLatch c;

    g(f fVar, Bitmap bitmap, CountDownLatch countDownLatch) {
        this.f2586a = fVar;
        this.b = bitmap;
        this.c = countDownLatch;
    }

    public final void run() {
        if (!(this.b == this.f2586a.f2585a.l || this.b == null)) {
            this.f2586a.f2585a.j.a(this.b);
            if (this.f2586a.f2585a.l != null) {
                this.f2586a.f2585a.l.recycle();
            }
            this.f2586a.f2585a.l = this.b;
        }
        if (this.f2586a.f2585a.j.f() == 1.0f) {
            this.f2586a.f2585a.j.e();
        }
        this.c.countDown();
    }
}
