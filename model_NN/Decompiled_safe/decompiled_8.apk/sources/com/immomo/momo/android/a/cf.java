package com.immomo.momo.android.a;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ImageBrowserActivity;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.u;
import com.immomo.momo.service.bean.x;
import com.immomo.momo.util.j;
import com.immomo.momo.util.m;
import java.util.List;

public final class cf extends a implements View.OnClickListener {
    private Activity d = null;
    private HandyListView e = null;

    public cf(Activity activity, List list, HandyListView handyListView) {
        super(activity, list);
        new m(this);
        this.d = activity;
        this.e = handyListView;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        cg cgVar;
        if (view == null) {
            view = g.o().inflate((int) R.layout.listitem_event, (ViewGroup) null);
            cg cgVar2 = new cg((byte) 0);
            view.setTag(R.id.tag_userlist_item, cgVar2);
            cgVar2.f754a = (TextView) view.findViewById(R.id.eventprofile_tv_name);
            cgVar2.e = (TextView) view.findViewById(R.id.eventprofile_tv_adress);
            cgVar2.c = (ImageView) view.findViewById(R.id.eventprofile_iv_avatar);
            cgVar2.g = (TextView) view.findViewById(R.id.eventlist_tv_commentcount);
            cgVar2.d = (TextView) view.findViewById(R.id.eventprofile_tv_distance);
            cgVar2.b = (TextView) view.findViewById(R.id.eventprofile_tv_holdtime);
            cgVar2.f = (TextView) view.findViewById(R.id.eventlist_tv_joincount);
            cgVar2.h = (TextView) view.findViewById(R.id.eventprofile_tv_subject);
            cgVar2.i = (TextView) view.findViewById(R.id.eventprofile_tv_hot);
            cgVar2.j = (TextView) view.findViewById(R.id.eventprofile_tv_today);
            cgVar2.k = (TextView) view.findViewById(R.id.eventprofile_tv_free);
            cgVar = cgVar2;
        } else {
            cgVar = (cg) view.getTag(R.id.tag_userlist_item);
        }
        u uVar = (u) getItem(i);
        cgVar.e.setTag(R.id.tag_item_position, Integer.valueOf(i));
        cgVar.c.setTag(R.id.tag_item_position, Integer.valueOf(i));
        cgVar.g.setTag(R.id.tag_item_position, Integer.valueOf(i));
        cgVar.d.setTag(R.id.tag_item_position, Integer.valueOf(i));
        cgVar.b.setTag(R.id.tag_item_position, Integer.valueOf(i));
        cgVar.f.setTag(R.id.tag_item_position, Integer.valueOf(i));
        cgVar.f754a.setTag(R.id.tag_item_position, Integer.valueOf(i));
        cgVar.h.setTag(R.id.tag_item_position, Integer.valueOf(i));
        cgVar.f754a.setText(uVar.b);
        cgVar.b.setText(uVar.d);
        cgVar.e.setText(uVar.e);
        cgVar.d.setText(uVar.h);
        cgVar.h.setText(x.a(uVar.i).b());
        cgVar.f.setText(String.valueOf(uVar.f()) + "人参加");
        cgVar.g.setText(String.valueOf(uVar.d()) + "讨论");
        cgVar.c.setImageBitmap(null);
        j.a(uVar, cgVar.c, this.e, 20, false, false, 0, false);
        if (uVar.m) {
            cgVar.i.setVisibility(0);
        } else {
            cgVar.i.setVisibility(8);
        }
        if (uVar.n) {
            cgVar.j.setVisibility(0);
        } else {
            cgVar.j.setVisibility(8);
        }
        if (uVar.o) {
            cgVar.k.setVisibility(0);
        } else {
            cgVar.k.setVisibility(8);
        }
        return view;
    }

    public final void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_feed_photo /*2131166248*/:
                Intent intent = new Intent(this.d, ImageBrowserActivity.class);
                intent.putExtra("array", new String[]{((u) getItem(((Integer) view.getTag(R.id.tag_item_position)).intValue())).c});
                intent.putExtra("imagetype", "feed");
                intent.putExtra("index", 0);
                this.d.startActivity(intent);
                this.d.overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
                return;
            default:
                return;
        }
    }
}
