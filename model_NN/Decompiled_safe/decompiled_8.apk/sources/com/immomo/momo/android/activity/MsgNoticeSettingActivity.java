package com.immomo.momo.android.activity;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import com.immomo.momo.R;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.g;
import com.immomo.momo.service.ag;
import com.immomo.momo.service.bean.at;

public class MsgNoticeSettingActivity extends ah implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private CheckBox h = null;
    private CheckBox i = null;
    private CheckBox j = null;
    private CheckBox k = null;
    private CheckBox l = null;
    private CheckBox m = null;
    private CheckBox n = null;
    private CheckBox o = null;
    private CheckBox p = null;
    private Animation q;
    private AnimationSet r;
    /* access modifiers changed from: private */
    public View s = null;
    private HeaderLayout t = null;
    private boolean u = false;

    private void u() {
        boolean z = true;
        this.u = true;
        this.h.setChecked(this.g.f2996a);
        this.i.setChecked(this.g.b);
        this.j.setChecked(this.g.e);
        CheckBox checkBox = this.k;
        if (this.g.f) {
            z = false;
        }
        checkBox.setChecked(z);
        this.l.setChecked(this.g.t);
        this.m.setChecked(this.g.u);
        this.o.setChecked(this.g.v);
        this.p.setChecked(this.g.w);
        this.n.setChecked(this.g.y);
        this.s.setVisibility(this.g.f2996a ? 0 : 4);
        this.u = false;
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_setting_msgnotice);
        this.t = (HeaderLayout) findViewById(R.id.layout_header);
        this.t.setTitleText("消息提醒");
        this.h = (CheckBox) findViewById(R.id.setting_cb_alert);
        this.i = (CheckBox) findViewById(R.id.setting_cb_sound);
        this.j = (CheckBox) findViewById(R.id.setting_cb_shock);
        this.k = (CheckBox) findViewById(R.id.setting_cb_hide_msgcontent);
        this.o = (CheckBox) findViewById(R.id.setting_cb_eventnotice);
        this.p = (CheckBox) findViewById(R.id.setting_cb_tiebanotice);
        this.l = (CheckBox) findViewById(R.id.setting_cb_groupnotice);
        this.m = (CheckBox) findViewById(R.id.setting_cb_friendnotice);
        this.n = (CheckBox) findViewById(R.id.setting_cb_strangernotice);
        this.s = findViewById(R.id.setting_layout_other);
        this.h.setOnCheckedChangeListener(this);
        this.i.setOnCheckedChangeListener(this);
        this.j.setOnCheckedChangeListener(this);
        this.k.setOnCheckedChangeListener(this);
        this.m.setOnCheckedChangeListener(this);
        this.o.setOnCheckedChangeListener(this);
        this.p.setOnCheckedChangeListener(this);
        this.l.setOnCheckedChangeListener(this);
        this.n.setOnCheckedChangeListener(this);
        findViewById(R.id.setting_layout_alert).setOnClickListener(this);
        findViewById(R.id.setting_layout_hide_msgcontent).setOnClickListener(this);
        findViewById(R.id.setting_layout_clickable).setOnClickListener(this);
        findViewById(R.id.setting_layout_friendnotice).setOnClickListener(this);
        findViewById(R.id.setting_layout_eventnotice).setOnClickListener(this);
        findViewById(R.id.setting_layout_tiebanotice).setOnClickListener(this);
        findViewById(R.id.setting_layout_groupnotice).setOnClickListener(this);
        findViewById(R.id.setting_layout_strangernotice).setOnClickListener(this);
        findViewById(R.id.setting_layout_shock).setOnClickListener(this);
        u();
    }

    /* access modifiers changed from: protected */
    public final void d() {
    }

    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        if (!this.u) {
            switch (compoundButton.getId()) {
                case R.id.setting_cb_alert /*2131165884*/:
                    at atVar = this.g;
                    at atVar2 = this.g;
                    boolean isChecked = this.h.isChecked();
                    atVar2.f2996a = isChecked;
                    atVar.a("openAlarm", Boolean.valueOf(isChecked));
                    return;
                case R.id.setting_layout_other /*2131165885*/:
                case R.id.setting_layout_sound /*2131165886*/:
                case R.id.setting_layout_clickable /*2131165887*/:
                case R.id.setting_layout_shock /*2131165889*/:
                case R.id.setting_layout_hide_msgcontent /*2131165891*/:
                case R.id.setting_layout_strangernotice /*2131165893*/:
                case R.id.setting_layout_groupnotice /*2131165895*/:
                case R.id.setting_layout_friendnotice /*2131165897*/:
                case R.id.setting_layout_eventnotice /*2131165899*/:
                case R.id.setting_layout_tiebanotice /*2131165901*/:
                default:
                    return;
                case R.id.setting_cb_sound /*2131165888*/:
                    at atVar3 = this.g;
                    at atVar4 = this.g;
                    boolean isChecked2 = this.i.isChecked();
                    atVar4.b = isChecked2;
                    atVar3.a("sound", Boolean.valueOf(isChecked2));
                    return;
                case R.id.setting_cb_shock /*2131165890*/:
                    at atVar5 = this.g;
                    at atVar6 = this.g;
                    boolean isChecked3 = this.j.isChecked();
                    atVar6.e = isChecked3;
                    atVar5.a("vibrate", Boolean.valueOf(isChecked3));
                    return;
                case R.id.setting_cb_hide_msgcontent /*2131165892*/:
                    at atVar7 = this.g;
                    at atVar8 = this.g;
                    boolean z2 = !this.k.isChecked();
                    atVar8.f = z2;
                    atVar7.a("is_show_msg_content", Boolean.valueOf(z2));
                    return;
                case R.id.setting_cb_strangernotice /*2131165894*/:
                    at atVar9 = this.g;
                    at atVar10 = this.g;
                    boolean isChecked4 = this.n.isChecked();
                    atVar10.y = isChecked4;
                    atVar9.a("notify_stranger", Boolean.valueOf(isChecked4));
                    if (this.g.y) {
                        new ag().a(13, 5);
                    } else {
                        new ag().a(5, 13);
                    }
                    Bundle bundle = new Bundle();
                    bundle.putString("sessionid", "-2222");
                    g.d().a(bundle, "action.sessionchanged");
                    return;
                case R.id.setting_cb_groupnotice /*2131165896*/:
                    at atVar11 = this.g;
                    at atVar12 = this.g;
                    boolean isChecked5 = this.l.isChecked();
                    atVar12.t = isChecked5;
                    atVar11.a("notify_groupnotice", Boolean.valueOf(isChecked5));
                    g.d().a(new Bundle(), "actions.groupnoticechanged");
                    return;
                case R.id.setting_cb_friendnotice /*2131165898*/:
                    at atVar13 = this.g;
                    at atVar14 = this.g;
                    boolean isChecked6 = this.m.isChecked();
                    atVar14.u = isChecked6;
                    atVar13.a("notify_friend", Boolean.valueOf(isChecked6));
                    g.d().a(new Bundle(), "actions.feedchanged");
                    return;
                case R.id.setting_cb_eventnotice /*2131165900*/:
                    at atVar15 = this.g;
                    at atVar16 = this.g;
                    boolean isChecked7 = this.o.isChecked();
                    atVar16.v = isChecked7;
                    atVar15.a("notify_event", Boolean.valueOf(isChecked7));
                    g.d().a(new Bundle(), "actions.eventstatuschanged");
                    return;
                case R.id.setting_cb_tiebanotice /*2131165902*/:
                    at atVar17 = this.g;
                    at atVar18 = this.g;
                    boolean isChecked8 = this.p.isChecked();
                    atVar18.w = isChecked8;
                    atVar17.a("notify_tieba", Boolean.valueOf(isChecked8));
                    g.d().a(new Bundle(), "actions.tiebachanged");
                    return;
            }
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.setting_layout_alert /*2131165883*/:
                this.h.toggle();
                if (this.g.f2996a) {
                    if (Build.VERSION.SDK_INT > 10) {
                        this.s.clearAnimation();
                        View view2 = this.s;
                        if (this.q == null) {
                            this.q = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, -1.0f, 1, 0.0f);
                            this.q.setDuration(200);
                            this.q.setFillAfter(true);
                            this.q.setAnimationListener(new fv(this));
                        }
                        view2.startAnimation(this.q);
                        return;
                    }
                    this.s.setVisibility(0);
                    return;
                } else if (Build.VERSION.SDK_INT > 10) {
                    this.s.clearAnimation();
                    View view3 = this.s;
                    if (this.r == null) {
                        this.r = new AnimationSet(true);
                        TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, -1.0f);
                        translateAnimation.setDuration(200);
                        translateAnimation.setFillAfter(true);
                        this.r.setAnimationListener(new fw(this));
                        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
                        alphaAnimation.setDuration(100);
                        alphaAnimation.setFillAfter(true);
                        this.r.addAnimation(translateAnimation);
                        this.r.addAnimation(alphaAnimation);
                    }
                    view3.startAnimation(this.r);
                    return;
                } else {
                    this.s.setVisibility(8);
                    return;
                }
            case R.id.setting_cb_alert /*2131165884*/:
            case R.id.setting_layout_other /*2131165885*/:
            case R.id.setting_layout_sound /*2131165886*/:
            case R.id.setting_cb_sound /*2131165888*/:
            case R.id.setting_cb_shock /*2131165890*/:
            case R.id.setting_cb_hide_msgcontent /*2131165892*/:
            case R.id.setting_cb_strangernotice /*2131165894*/:
            case R.id.setting_cb_groupnotice /*2131165896*/:
            case R.id.setting_cb_friendnotice /*2131165898*/:
            case R.id.setting_cb_eventnotice /*2131165900*/:
            default:
                return;
            case R.id.setting_layout_clickable /*2131165887*/:
                this.i.toggle();
                return;
            case R.id.setting_layout_shock /*2131165889*/:
                this.j.toggle();
                return;
            case R.id.setting_layout_hide_msgcontent /*2131165891*/:
                this.k.toggle();
                return;
            case R.id.setting_layout_strangernotice /*2131165893*/:
                this.n.toggle();
                return;
            case R.id.setting_layout_groupnotice /*2131165895*/:
                this.l.toggle();
                return;
            case R.id.setting_layout_friendnotice /*2131165897*/:
                this.m.toggle();
                return;
            case R.id.setting_layout_eventnotice /*2131165899*/:
                this.o.toggle();
                return;
            case R.id.setting_layout_tiebanotice /*2131165901*/:
                this.p.toggle();
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        u();
    }
}
