package com.immomo.momo.android.activity;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.bean.bf;

final class y extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f2310a;
    /* access modifiers changed from: private */
    public /* synthetic */ AlipayUserWelcomeActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public y(AlipayUserWelcomeActivity alipayUserWelcomeActivity, Context context) {
        super(context);
        this.c = alipayUserWelcomeActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return w.a().a(this.c.l, this.c.h, this.c.k);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f2310a = new v(this.c);
        this.f2310a.a("请求提交中");
        this.f2310a.setCancelable(true);
        this.f2310a.setOnCancelListener(new z(this));
        this.c.a(this.f2310a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
        this.c.finish();
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj) {
        AlipayUserWelcomeActivity.a(this.c, (bf) obj);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.p();
    }
}
