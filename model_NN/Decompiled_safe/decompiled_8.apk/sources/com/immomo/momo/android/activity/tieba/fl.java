package com.immomo.momo.android.activity.tieba;

import android.content.Context;
import com.immomo.momo.android.a.ji;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.v;
import java.util.List;

final class fl extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ UsersTiebaActivity f2288a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fl(UsersTiebaActivity usersTiebaActivity, Context context) {
        super(context);
        this.f2288a = usersTiebaActivity;
        if (usersTiebaActivity.l != null) {
            usersTiebaActivity.l.cancel(true);
        }
        usersTiebaActivity.l = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return v.a().r(this.f2288a.m);
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        List list = (List) obj;
        this.f2288a.k.a();
        this.f2288a.k.a(list);
        this.f2288a.k = new ji(list, this.f2288a.j);
        this.f2288a.j.setAdapter(this.f2288a.k);
        this.f2288a.k.b();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f2288a.j.i();
    }
}
