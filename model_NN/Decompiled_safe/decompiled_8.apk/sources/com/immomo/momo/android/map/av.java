package com.immomo.momo.android.map;

import android.location.Location;

final class av implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ au f2474a;
    private final /* synthetic */ Location b;

    av(au auVar, Location location) {
        this.f2474a = auVar;
        this.b = location;
    }

    public final void run() {
        if (!this.f2474a.f2473a.isFinishing() && this.f2474a.f2473a.x != null) {
            this.f2474a.f2473a.x.dismiss();
            this.f2474a.f2473a.x = null;
        }
        this.f2474a.f2473a.o = this.b;
        this.f2474a.f2473a.b();
    }
}
