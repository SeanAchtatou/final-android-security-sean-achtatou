package com.immomo.momo.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.common.CreateDiscussTabsActivity;
import com.immomo.momo.android.activity.contacts.CommunityPeopleActivity;
import com.immomo.momo.android.activity.contacts.ContactPeopleActivity;
import com.immomo.momo.android.activity.contacts.OpenContactActivity;
import com.immomo.momo.android.activity.group.foundgroup.FoundGroupActivity;
import com.immomo.momo.android.activity.setting.CommunityBindActivity;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.imjson.util.c;
import com.immomo.momo.service.bean.a.a;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.k;
import com.sina.weibo.sdk.constant.Constants;

public class AddFriendActivity extends ah implements View.OnClickListener {
    /* access modifiers changed from: private */
    public bf h;
    /* access modifiers changed from: private */
    public a i;
    /* access modifiers changed from: private */
    public Button j;
    /* access modifiers changed from: private */
    public EditText k;
    private d l = null;
    private d m = null;
    private boolean n = true;
    private String o = PoiTypeDef.All;
    private String p = PoiTypeDef.All;

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_addfriend);
        m().setTitleText("添加");
        this.j = (Button) findViewById(R.id.btn_find);
        this.k = (EditText) findViewById(R.id.friend_edit_momoid);
        EditText editText = this.k;
        boolean z = this.n;
        editText.setHint((int) R.string.hint_searchuser);
        this.j.setEnabled(false);
        findViewById(R.id.addtabs_layout_addfriend).setOnClickListener(this);
        findViewById(R.id.addtabs_layout_addfriend).setSelected(true);
        findViewById(R.id.addtabs_layout_addgroup).setOnClickListener(this);
        this.j.setOnClickListener(this);
        findViewById(R.id.layout_addcontactpeople).setOnClickListener(this);
        findViewById(R.id.layout_addsinacontactpeople).setOnClickListener(this);
        findViewById(R.id.layout_addtxcontactpeople).setOnClickListener(this);
        findViewById(R.id.layout_addrenrencontactpeople).setOnClickListener(this);
        findViewById(R.id.layout_addgroupcontactpeople).setOnClickListener(this);
        findViewById(R.id.layout_adddiscusscontactpeople).setOnClickListener(this);
        this.k.addTextChangedListener(new g(this));
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        Intent intent2 = new Intent();
        switch (i2) {
            case 21:
                if (i3 == -1) {
                    intent2.setClass(this, CommunityPeopleActivity.class);
                    intent2.putExtra("type", 3);
                    intent2.putExtra("from", 11);
                    startActivity(intent2);
                    return;
                }
                return;
            case Constants.WEIBO_SDK_VERSION /*22*/:
                if (i3 == -1) {
                    intent2.setClass(this, CommunityPeopleActivity.class);
                    intent2.putExtra("type", 2);
                    intent2.putExtra("from", 11);
                    startActivity(intent2);
                    return;
                }
                return;
            case 23:
                if (i3 == -1) {
                    intent2.setClass(this, CommunityPeopleActivity.class);
                    intent2.putExtra("type", 1);
                    intent2.putExtra("from", 11);
                    startActivity(intent2);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.addtabs_layout_addfriend /*2131165373*/:
                if (!this.n) {
                    this.p = this.k.getText().toString().trim();
                }
                this.n = true;
                this.k.setHint((int) R.string.hint_searchuser);
                this.k.setText(this.o);
                this.k.setSelection(this.k.getText().toString().length());
                findViewById(R.id.addtabs_layout_addfriend).setSelected(true);
                findViewById(R.id.addtabs_layout_addgroup).setSelected(false);
                return;
            case R.id.tabitem_ligthblue_driver_right /*2131165374*/:
            case R.id.layout_addcontainer /*2131165376*/:
            case R.id.iv_addgroup /*2131165379*/:
            case R.id.iv_arrow /*2131165380*/:
            case R.id.tv_addcount /*2131165381*/:
            default:
                return;
            case R.id.addtabs_layout_addgroup /*2131165375*/:
                if (this.n) {
                    this.o = this.k.getText().toString().trim();
                }
                this.n = false;
                this.k.setHint((int) R.string.hint_searchgroup);
                this.k.setText(this.p);
                this.k.setSelection(this.k.getText().toString().length());
                findViewById(R.id.addtabs_layout_addgroup).setSelected(true);
                findViewById(R.id.addtabs_layout_addfriend).setSelected(false);
                return;
            case R.id.btn_find /*2131165377*/:
                if (this.n) {
                    String editable = this.k.getText().toString();
                    if (android.support.v4.b.a.a((CharSequence) editable)) {
                        a((int) R.string.find_empty_momoid);
                        return;
                    } else if (editable.equals(this.f.h)) {
                        a((int) R.string.find_your_momoid);
                        return;
                    } else if (!c.c() || !editable.equals("1602")) {
                        this.l = new j(this, this);
                        this.l.execute(editable);
                        new k("C", "C69101").e();
                        return;
                    } else {
                        c.a().d();
                        return;
                    }
                } else {
                    String editable2 = this.k.getText().toString();
                    if (android.support.v4.b.a.a((CharSequence) editable2)) {
                        a((int) R.string.find_empty_gid);
                        return;
                    }
                    this.m = new h(this, this);
                    this.m.execute(editable2);
                    new k("C", "C69102").e();
                    return;
                }
            case R.id.layout_addcontactpeople /*2131165378*/:
                new k("C", "C69201").e();
                if (!h().g || this.g == null || !this.g.c) {
                    startActivity(new Intent(this, OpenContactActivity.class));
                    return;
                } else {
                    startActivity(new Intent(this, ContactPeopleActivity.class));
                    return;
                }
            case R.id.layout_addsinacontactpeople /*2131165382*/:
                if (this.f.q()) {
                    Intent intent = new Intent();
                    intent.setClass(this, CommunityPeopleActivity.class);
                    intent.putExtra("type", 1);
                    intent.putExtra("from", 11);
                    startActivity(intent);
                    return;
                }
                Intent intent2 = new Intent(this, CommunityBindActivity.class);
                intent2.putExtra("type", 1);
                startActivityForResult(intent2, 23);
                return;
            case R.id.layout_addtxcontactpeople /*2131165383*/:
                if (this.f.at) {
                    Intent intent3 = new Intent();
                    intent3.setClass(this, CommunityPeopleActivity.class);
                    intent3.putExtra("type", 2);
                    intent3.putExtra("from", 11);
                    startActivity(intent3);
                    return;
                }
                Intent intent4 = new Intent(this, CommunityBindActivity.class);
                intent4.putExtra("type", 2);
                startActivityForResult(intent4, 22);
                return;
            case R.id.layout_addrenrencontactpeople /*2131165384*/:
                if (this.f.ar) {
                    Intent intent5 = new Intent();
                    intent5.setClass(this, CommunityPeopleActivity.class);
                    intent5.putExtra("type", 3);
                    intent5.putExtra("from", 11);
                    startActivity(intent5);
                    return;
                }
                Intent intent6 = new Intent(this, CommunityBindActivity.class);
                intent6.putExtra("type", 3);
                startActivityForResult(intent6, 21);
                return;
            case R.id.layout_addgroupcontactpeople /*2131165385*/:
                startActivity(new Intent(this, FoundGroupActivity.class));
                return;
            case R.id.layout_adddiscusscontactpeople /*2131165386*/:
                startActivity(new Intent(this, CreateDiscussTabsActivity.class));
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.l != null && !this.l.isCancelled()) {
            this.l.cancel(true);
        }
        if (this.m != null && !this.m.isCancelled()) {
            this.m.cancel(true);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        new k("PO", "P691").e();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        new k("PI", "P691").e();
    }
}
