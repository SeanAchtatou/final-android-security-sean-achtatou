package com.immomo.momo.android.activity;

import android.os.Bundle;
import android.support.v4.b.a;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.immomo.momo.MomoApplication;
import com.immomo.momo.R;

public class NewVersionActivity extends ao {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_newversion);
        String str = getIntent().getExtras() != null ? (String) getIntent().getExtras().get("url_download") : null;
        findViewById(R.id.dilaog_button1).setOnClickListener(new ga(this, str));
        findViewById(R.id.dilaog_button2).setOnClickListener(new gb(this, str));
        WebView webView = (WebView) findViewById(R.id.webview);
        View findViewById = findViewById(R.id.loading_indicator);
        WebSettings settings = webView.getSettings();
        settings.setCacheMode(2);
        settings.setJavaScriptEnabled(true);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webView.setWebChromeClient(new gc());
        webView.setDownloadListener(new gd(this));
        webView.setWebViewClient(new ge(this, findViewById));
        webView.loadUrl(a.a(com.immomo.momo.a.r, "type", "dialog"));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return super.onKeyDown(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public final MomoApplication t() {
        return (MomoApplication) getApplication();
    }
}
