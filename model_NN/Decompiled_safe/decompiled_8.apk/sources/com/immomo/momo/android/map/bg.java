package com.immomo.momo.android.map;

import android.support.v4.b.a;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.b.z;

final class bg implements TextWatcher {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ SelectSiteGoogleActivity f2486a;

    bg(SelectSiteGoogleActivity selectSiteGoogleActivity) {
        this.f2486a = selectSiteGoogleActivity;
    }

    public final void afterTextChanged(Editable editable) {
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [com.immomo.momo.android.map.SelectSiteGoogleActivity, android.app.Activity] */
    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        if (!this.f2486a.s) {
            if (!a.a((CharSequence) charSequence.toString().trim())) {
                this.f2486a.q.b();
                this.f2486a.f2453a.removeFooterView(this.f2486a.m);
                ((TextView) this.f2486a.m.findViewById(R.id.textview)).setText("添加地点'" + charSequence.toString().trim() + "' ");
                this.f2486a.m.findViewById(R.id.layout_footer).setOnClickListener(new bh(this, charSequence));
                if (z.a(this.f2486a.n)) {
                    this.f2486a.f2453a.addFooterView(this.f2486a.m);
                    this.f2486a.m.requestFocus();
                    return;
                }
                this.f2486a.f2453a.removeFooterView(this.f2486a.m);
            } else if (this.f2486a.q.getCount() > 0) {
                this.f2486a.f2453a.removeFooterView(this.f2486a.m);
                this.f2486a.q.b();
            }
        } else if (!charSequence.toString().trim().equals(this.f2486a.i)) {
            if (this.f2486a.r != null && !this.f2486a.r.isCancelled()) {
                this.f2486a.r.cancel(true);
                this.f2486a.r = (bp) null;
            }
            this.f2486a.i = charSequence.toString().trim();
            this.f2486a.r = new bp(this.f2486a, this.f2486a, this.f2486a.i, 0);
            this.f2486a.r.execute(new Object[0]);
        }
    }
}
