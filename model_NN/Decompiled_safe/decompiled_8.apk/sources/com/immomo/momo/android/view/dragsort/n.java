package com.immomo.momo.android.view.dragsort;

import android.view.View;

final class n extends t {
    private int b;
    private int c;
    private float d;
    private float e;
    private /* synthetic */ DragSortListView f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public n(DragSortListView dragSortListView, int i) {
        super(dragSortListView, i);
        this.f = dragSortListView;
    }

    private int e() {
        int firstVisiblePosition = this.f.getFirstVisiblePosition();
        int i = (this.f.B + this.f.getDividerHeight()) / 2;
        View childAt = this.f.getChildAt(this.b - firstVisiblePosition);
        if (childAt != null) {
            return this.b == this.c ? childAt.getTop() : this.b < this.c ? childAt.getTop() - i : (childAt.getBottom() + i) - this.f.C;
        }
        d();
        return -1;
    }

    public final void a() {
        this.b = this.f.o;
        this.c = this.f.s;
        this.f.A = 2;
        this.d = (float) (this.f.h.y - e());
        this.e = (float) (this.f.h.x - this.f.getPaddingLeft());
    }

    public final void a(float f2) {
        int e2 = e();
        float paddingLeft = (float) (this.f.h.x - this.f.getPaddingLeft());
        float f3 = 1.0f - f2;
        if (f3 < Math.abs(((float) (this.f.h.y - e2)) / this.d) || f3 < Math.abs(paddingLeft / this.e)) {
            this.f.h.y = e2 + ((int) (this.d * f3));
            this.f.h.x = this.f.getPaddingLeft() + ((int) (this.e * f3));
            this.f.B();
        }
    }

    public final void b() {
        this.f.u();
    }
}
