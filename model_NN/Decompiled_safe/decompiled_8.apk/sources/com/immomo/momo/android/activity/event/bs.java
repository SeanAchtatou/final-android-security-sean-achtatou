package com.immomo.momo.android.activity.event;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.j;
import com.immomo.momo.service.bean.u;
import java.util.ArrayList;
import java.util.List;

final class bs extends d {

    /* renamed from: a  reason: collision with root package name */
    private List f1387a = new ArrayList();
    private /* synthetic */ bd c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bs(bd bdVar, Context context) {
        super(context);
        this.c = bdVar;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        boolean a2 = j.a().a(this.f1387a, this.c.V.getCount(), this.c.M.S, this.c.M.T, this.c.M.aH, this.c.Y.a(), this.c.Z.a(), this.c.aa.a());
        this.c.W.e(this.f1387a);
        return Boolean.valueOf(a2);
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        Boolean bool = (Boolean) obj;
        if (this.f1387a.isEmpty() || !bool.booleanValue()) {
            this.c.P.setVisibility(8);
        } else {
            this.c.P.setVisibility(0);
        }
        for (u uVar : this.f1387a) {
            if (!this.c.S.contains(uVar)) {
                this.c.V.b(uVar);
                this.c.S.add(uVar);
            }
        }
        this.c.V.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.P.e();
    }
}
