package com.immomo.momo.android.activity;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.R;
import java.util.List;

final class ci implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EditUserPhotoActivity f1066a;

    ci(EditUserPhotoActivity editUserPhotoActivity) {
        this.f1066a = editUserPhotoActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (i < this.f1066a.i.size()) {
            List list = this.f1066a.i;
            Intent intent = new Intent(this.f1066a.getApplicationContext(), ImageBrowserActivity.class);
            intent.putExtra("array", (String[]) list.toArray(new String[this.f1066a.i.size()]));
            intent.putExtra("imagetype", "avator");
            intent.putExtra("index", i);
            this.f1066a.startActivity(intent);
            this.f1066a.overridePendingTransition(R.anim.zoom_enter, R.anim.normal);
        }
    }
}
