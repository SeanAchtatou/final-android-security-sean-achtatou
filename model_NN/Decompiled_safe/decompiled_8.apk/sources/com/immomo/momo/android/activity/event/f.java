package com.immomo.momo.android.activity.event;

import android.content.Context;
import android.os.Bundle;
import com.immomo.momo.android.c.d;
import com.immomo.momo.g;
import com.immomo.momo.protocol.a.j;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

final class f extends d {

    /* renamed from: a  reason: collision with root package name */
    private boolean f1408a = true;
    private boolean c = false;
    private List d = null;
    private /* synthetic */ e e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public f(e eVar, Context context, boolean z) {
        super(context);
        this.e = eVar;
        this.f1408a = z;
    }

    /* access modifiers changed from: protected */
    public final Object a(Object... objArr) {
        this.d = new ArrayList();
        if (this.f1408a) {
            this.c = j.a().b(this.d, 0);
            this.e.R.c(this.d);
            return null;
        }
        this.c = j.a().b(this.d, this.e.P.getCount());
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        if (!this.f1408a) {
            this.e.P.b((Collection) this.d);
        } else {
            this.e.Q = this.d;
            this.e.P.a(false);
            this.e.P.b((Collection) this.d);
            Date date = new Date();
            g.r().a("prf_time_event_feed_my_comments", date);
            this.e.O.setLastFlushTime(date);
            ((MainEventActivity) this.e.c()).z();
            g.d().a(new Bundle(), "actions.eventstatuschanged");
        }
        if (!this.c) {
            this.e.S.setVisibility(8);
        } else {
            this.e.S.setVisibility(0);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.e.O.n();
        this.e.S.e();
        if (this.f1408a) {
            this.e.T = null;
        } else {
            this.e.U = null;
        }
    }
}
