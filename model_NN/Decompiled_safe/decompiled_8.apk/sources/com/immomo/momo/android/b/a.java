package com.immomo.momo.android.b;

import android.content.Context;
import android.support.v4.app.x;
import com.immomo.a.a.f.b;
import com.immomo.momo.R;
import com.immomo.momo.a.w;
import com.immomo.momo.g;
import com.immomo.momo.util.m;
import java.util.ArrayList;

public final class a extends x {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public m f2312a = new m("BaiduClient");
    /* access modifiers changed from: private */
    public h b = null;

    public a(Context context) {
        new Object();
        new b(this);
        this.b = h.a(context);
    }

    /* access modifiers changed from: private */
    public void b(String str, p pVar) {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        String str2 = String.valueOf(str) + "_gps";
        String str3 = String.valueOf(str) + "_network";
        Object obj = new Object();
        c cVar = new c(this, arrayList, str3, str2, obj);
        try {
            this.b.a(str2, new d(this, arrayList2, str3, str2, obj));
        } catch (Exception e) {
            this.f2312a.a((Throwable) e);
        }
        try {
            this.b.b(str3, cVar);
        } catch (Exception e2) {
            this.f2312a.a((Throwable) e2);
        }
        new Thread(new e(this, obj, str3, str2, arrayList, arrayList2, pVar)).start();
    }

    public final void a() {
        this.f2312a.b((Object) "Baidu client cancel all listener");
        if (this.b != null) {
            this.b.a();
        }
    }

    public final void a(p pVar) {
        String str = String.valueOf(g.h()) + b.a();
        this.f2312a.a((Object) "baidu smart locating....");
        if (!g.y()) {
            g.a((int) R.string.errormsg_network_unfind);
            throw new w();
        } else {
            b(str, pVar);
        }
    }

    public final void a(String str) {
        this.f2312a.b((Object) ("Baidu client cancel " + str));
        if (this.b != null) {
            this.b.a(String.valueOf(str) + "_gps");
            this.b.a(String.valueOf(str) + "_network");
        }
    }

    public final void a(String str, p pVar) {
        this.f2312a.a((Object) "baidu smart locating....");
        if (!g.y()) {
            g.a((int) R.string.errormsg_network_unfind);
            throw new w();
        }
        ArrayList arrayList = new ArrayList();
        String str2 = String.valueOf(str) + "_gps";
        Object obj = new Object();
        try {
            this.b.a(str2, new f(this, arrayList, str2, obj));
        } catch (Exception e) {
            this.f2312a.a((Throwable) e);
        }
        new Thread(new g(this, obj, arrayList, str2, pVar)).start();
    }
}
