package com.immomo.momo.android.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v4.view.bf;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ag;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.aj;
import com.immomo.momo.service.bean.al;
import com.immomo.momo.util.j;
import com.immomo.momo.util.m;
import java.util.ArrayList;

public class UserPhotosView extends LinearLayout implements bf {

    /* renamed from: a  reason: collision with root package name */
    private View[] f2659a;
    private LinearLayout b;
    private LinearLayout c;
    private LinearLayout d;
    private LinearLayout e;
    private View f;
    private View g;
    private ViewPager h;
    /* access modifiers changed from: private */
    public String[] i;
    private View.OnClickListener j = new ds(this);
    private int k = 0;
    /* access modifiers changed from: private */
    public ag l;
    private du m = null;

    public UserPhotosView(Context context) {
        super(context);
        new m();
        new ArrayList();
    }

    public UserPhotosView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        new m();
        new ArrayList();
    }

    public final void a(int i2, float f2) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void
     arg types: [com.immomo.momo.service.bean.al, android.widget.ImageView, ?[OBJECT, ARRAY], int, boolean, int, int]
     candidates:
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, int, android.view.View, android.view.ViewGroup, boolean, int, boolean):com.immomo.momo.android.c.o
      com.immomo.momo.util.j.a(com.immomo.momo.service.bean.aj, android.widget.ImageView, android.view.ViewGroup, int, boolean, boolean, int):void */
    public final void a(String[] strArr, boolean z, boolean z2) {
        int length;
        UserPhotosView userPhotosView;
        removeAllViews();
        View inflate = inflate(getContext(), R.layout.common_userphoto, null);
        int round = Math.round(((((float) g.J()) - (g.l().getDimension(R.dimen.pic_item_margin) * 8.0f)) - (g.l().getDimension(R.dimen.pic_layout_margin) * 2.0f)) / 4.0f);
        this.h = (ViewPager) inflate.findViewById(R.id.viewpager);
        if (z2) {
            this.f = inflate(getContext(), R.layout.common_avatarvip, null);
            this.g = inflate(getContext(), R.layout.common_avatarvip, null);
        } else {
            this.f = inflate(getContext(), R.layout.common_avatarnotvip, null);
            this.g = inflate(getContext(), R.layout.common_avatarnotvip, null);
        }
        this.b = (LinearLayout) this.f.findViewById(R.id.avatar_page_line1);
        this.c = (LinearLayout) this.f.findViewById(R.id.avatar_page_line2);
        this.d = (LinearLayout) this.g.findViewById(R.id.avatar_page_line1);
        this.e = (LinearLayout) this.g.findViewById(R.id.avatar_page_line2);
        this.f2659a = new RelativeLayout[16];
        for (int i2 = 0; i2 < 8; i2++) {
            this.f2659a[i2] = this.f.findViewById(getResources().getIdentifier("avatar_block" + i2, "id", getContext().getPackageName()));
            ViewGroup.LayoutParams layoutParams = this.f2659a[i2].getLayoutParams();
            layoutParams.height = round;
            layoutParams.width = round;
            this.f2659a[i2].setLayoutParams(layoutParams);
            this.f2659a[i2].setVisibility(4);
        }
        for (int i3 = 8; i3 < 16; i3++) {
            this.f2659a[i3] = this.g.findViewById(getResources().getIdentifier("avatar_block" + (i3 - 8), "id", getContext().getPackageName()));
            ViewGroup.LayoutParams layoutParams2 = this.f2659a[i3].getLayoutParams();
            layoutParams2.height = round;
            layoutParams2.width = round;
            this.f2659a[i3].setLayoutParams(layoutParams2);
            this.f2659a[i3].setVisibility(4);
        }
        addView(inflate);
        if (strArr != null) {
            if (z2) {
                length = strArr.length;
                userPhotosView = this;
            } else if (strArr.length > 8) {
                length = 8;
                userPhotosView = this;
            } else {
                length = strArr.length;
                userPhotosView = this;
            }
            userPhotosView.i = new String[length];
            for (int i4 = 0; i4 < this.i.length; i4++) {
                this.i[i4] = strArr[i4];
            }
            int length2 = this.i.length;
            if (length2 <= 4) {
                this.b.setVisibility(0);
                this.c.setVisibility(8);
                this.d.setVisibility(8);
                this.e.setVisibility(8);
                this.f.setVisibility(0);
                this.g.setVisibility(8);
            } else if (length2 > 4 && length2 <= 8) {
                this.b.setVisibility(0);
                this.c.setVisibility(0);
                this.d.setVisibility(8);
                this.e.setVisibility(8);
                this.f.setVisibility(0);
                this.g.setVisibility(8);
            } else if (length2 > 8 && length2 <= 12) {
                this.b.setVisibility(0);
                this.c.setVisibility(0);
                this.d.setVisibility(0);
                this.e.setVisibility(8);
                this.f.setVisibility(0);
                this.g.setVisibility(0);
            } else if (length2 > 12) {
                this.b.setVisibility(0);
                this.c.setVisibility(0);
                this.d.setVisibility(0);
                this.e.setVisibility(0);
                this.f.setVisibility(0);
                this.g.setVisibility(0);
            }
            ArrayList arrayList = new ArrayList();
            arrayList.add(this.f);
            if (this.g.getVisibility() == 0) {
                arrayList.add(this.g);
            }
            this.h.setAdapter(new dt(arrayList));
            this.h.setOnPageChangeListener(this);
            for (int i5 = 0; i5 < this.f2659a.length; i5++) {
                View view = this.f2659a[i5];
                if (i5 >= length2) {
                    view.setVisibility(4);
                } else {
                    view.setVisibility(0);
                    ImageView imageView = (ImageView) this.f2659a[i5].findViewById(R.id.avatar_imageview);
                    if (z2) {
                        j.a((aj) new al(this.i[i5]), imageView, (ViewGroup) null, 3, z, true, 0);
                    } else {
                        j.a(new al(this.i[i5]), imageView, (ViewGroup) null, 3);
                    }
                    ImageView imageView2 = (ImageView) this.f2659a[i5].findViewById(R.id.avatar_cover);
                    imageView2.setTag(R.id.tag_item_position, Integer.valueOf(i5));
                    imageView2.setOnClickListener(this.j);
                }
            }
            ViewGroup.LayoutParams layoutParams3 = getLayoutParams();
            int round2 = ((length2 > 4 ? 2 : 1) * Math.round(((((float) g.J()) - (g.l().getDimension(R.dimen.pic_item_margin) * 8.0f)) - (g.l().getDimension(R.dimen.pic_layout_margin) * 2.0f)) / 4.0f)) + ((int) (g.l().getDimension(R.dimen.pic_item_margin) * 2.0f)) + ((int) (g.l().getDimension(R.dimen.pic_layout_margin) * 2.0f));
            layoutParams3.height = round2;
            setLayoutParams(layoutParams3);
            this.k = round2;
        }
    }

    public final void a_(int i2) {
        if (this.m != null) {
            this.m.u();
        }
    }

    public final void b(int i2) {
    }

    public int getPhotoHeight() {
        return this.k;
    }

    public void setAvatarClickListener(ag agVar) {
        this.l = agVar;
    }

    public void setPageSelectedListener(du duVar) {
        this.m = duVar;
    }
}
