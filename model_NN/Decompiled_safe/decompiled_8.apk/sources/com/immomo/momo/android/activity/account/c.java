package com.immomo.momo.android.activity.account;

import android.content.DialogInterface;
import android.support.v4.b.a;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.util.ao;

final class c implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ LoginActivity f980a;

    c(LoginActivity loginActivity) {
        this.f980a = loginActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        String trim = this.f980a.w.getText().toString().trim();
        if (a.a((CharSequence) trim)) {
            ao.b("验证码不可为空，请重试");
            this.f980a.w.requestFocus();
        } else if (this.f980a.s) {
            this.f980a.b(new f(this.f980a, this.f980a, trim));
        }
        ((n) dialogInterface).c();
    }
}
