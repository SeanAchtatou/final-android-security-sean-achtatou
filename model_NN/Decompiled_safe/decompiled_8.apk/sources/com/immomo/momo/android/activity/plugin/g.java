package com.immomo.momo.android.activity.plugin;

import android.os.AsyncTask;
import com.immomo.momo.R;
import com.immomo.momo.a.a;
import com.immomo.momo.a.o;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.aq;

final class g extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private v f2086a;
    private /* synthetic */ BindEmailActivity b;

    public g(BindEmailActivity bindEmailActivity) {
        this.b = bindEmailActivity;
        this.f2086a = new v(bindEmailActivity);
        this.f2086a.a("请求提交中");
        this.f2086a.setCancelable(true);
    }

    private String a() {
        try {
            w.a().n(this.b.f.G);
            return "yes";
        } catch (o e) {
            this.b.e.a((Throwable) e);
            this.b.b((CharSequence) "邮箱已经绑定成功");
            return "409";
        } catch (a e2) {
            this.b.e.a((Throwable) e2);
            this.b.b((CharSequence) e2.getMessage());
            return "no";
        } catch (Exception e3) {
            this.b.e.a((Throwable) e3);
            this.b.b((int) R.string.errormsg_server);
            return "no";
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object... objArr) {
        return a();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        String str = (String) obj;
        this.f2086a.cancel();
        if (str != null && str.equals("yes")) {
            this.b.a((CharSequence) "验证邮件已发送到你的注册email账号");
            this.b.setResult(0, null);
            this.b.finish();
        } else if (str != null && str.equals("409")) {
            aq aqVar = new aq();
            this.b.f.aw = true;
            aqVar.b(this.b.f);
            this.b.setResult(-1, null);
            this.b.finish();
        }
        super.onPostExecute(str);
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        if (this.f2086a != null) {
            this.f2086a.setOnCancelListener(new h(this));
            this.f2086a.show();
        }
        super.onPreExecute();
    }
}
