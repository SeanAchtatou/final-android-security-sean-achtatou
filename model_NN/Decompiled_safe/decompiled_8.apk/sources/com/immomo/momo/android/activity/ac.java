package com.immomo.momo.android.activity;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.a;
import com.immomo.momo.service.bean.b;

final class ac extends d {

    /* renamed from: a  reason: collision with root package name */
    private boolean f927a = false;
    private String c;
    private v d;
    /* access modifiers changed from: private */
    public /* synthetic */ AlipayUserWelcomeActivity e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ac(AlipayUserWelcomeActivity alipayUserWelcomeActivity, Context context, boolean z, String str) {
        super(context);
        this.e = alipayUserWelcomeActivity;
        this.f927a = z;
        this.c = str;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        b b = a.a().b(this.c, this.e.h, this.e.k);
        this.e.j = b.b;
        this.e.i = b.f3003a;
        this.e.w = !b.e;
        return b;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.d = new v(this.e);
        this.d.a("请求提交中");
        this.d.setCancelable(true);
        this.d.setOnCancelListener(new ad(this));
        this.e.a(this.d);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
        this.e.finish();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        b bVar = (b) obj;
        if (this.f927a) {
            if (bVar.d) {
                this.e.p.setVisibility(8);
                if (bVar.f) {
                    AlipayUserWelcomeActivity.d(this.e);
                } else {
                    AlipayUserWelcomeActivity.e(this.e);
                }
            } else {
                AlipayUserWelcomeActivity.a(this.e, bVar);
                AlipayUserWelcomeActivity.f(this.e);
            }
        } else if (bVar.d) {
            this.e.p.setVisibility(8);
            this.e.b(new y(this.e, this.e));
        } else {
            AlipayUserWelcomeActivity.a(this.e, bVar);
            AlipayUserWelcomeActivity.b(this.e, bVar.e);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.e.p();
    }
}
