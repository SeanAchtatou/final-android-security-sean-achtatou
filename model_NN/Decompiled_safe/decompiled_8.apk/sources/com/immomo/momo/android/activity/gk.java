package com.immomo.momo.android.activity;

import android.content.DialogInterface;

final class gk implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ OnlineSettingActivity f1518a;

    gk(OnlineSettingActivity onlineSettingActivity) {
        this.f1518a = onlineSettingActivity;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.f1518a.n = true;
        OnlineSettingActivity.b(this.f1518a);
    }
}
