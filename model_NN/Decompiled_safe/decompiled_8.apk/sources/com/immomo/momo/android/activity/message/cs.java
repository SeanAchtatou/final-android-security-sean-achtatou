package com.immomo.momo.android.activity.message;

import com.immomo.momo.protocol.a.w;
import com.immomo.momo.service.bean.Message;
import com.immomo.momo.service.bean.bf;

final class cs implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private Message f1978a;
    private /* synthetic */ MultiChatActivity b;

    public cs(MultiChatActivity multiChatActivity, Message message) {
        this.b = multiChatActivity;
        this.f1978a = message;
        if (message.remoteUser == null) {
            message.remoteUser = new bf(message.remoteId);
        }
    }

    public final void run() {
        try {
            w.a().c(this.f1978a.remoteUser, this.f1978a.remoteId);
            this.b.M.d(this.f1978a.remoteUser);
            this.b.Y();
        } catch (Exception e) {
        }
    }
}
