package com.immomo.momo.android.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.GridView;
import android.widget.ListAdapter;
import com.immomo.momo.android.a.i;
import com.immomo.momo.util.ao;

final class fz extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MulImagePickerActivity f1507a;

    private fz(MulImagePickerActivity mulImagePickerActivity) {
        this.f1507a = mulImagePickerActivity;
    }

    /* synthetic */ fz(MulImagePickerActivity mulImagePickerActivity, byte b) {
        this(mulImagePickerActivity);
    }

    public final void onReceive(Context context, Intent intent) {
        this.f1507a.m.setVisibility(8);
        this.f1507a.l.setVisibility(0);
        MulImagePickerActivity mulImagePickerActivity = this.f1507a;
        MulImagePickerActivity mulImagePickerActivity2 = this.f1507a;
        GridView unused = this.f1507a.l;
        mulImagePickerActivity.p = new i(mulImagePickerActivity2);
        this.f1507a.l.setAdapter((ListAdapter) this.f1507a.p);
        this.f1507a.h = MulImagePickerActivity.i;
        if (intent.getAction().equals("android.intent.action.MEDIA_MOUNTED")) {
            ao.b("SD卡插入");
        } else {
            ao.b("SD卡移除");
        }
    }
}
