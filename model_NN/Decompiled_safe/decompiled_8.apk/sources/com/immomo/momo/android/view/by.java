package com.immomo.momo.android.view;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.LinearLayout;

final class by extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MomoUnrefreshExpandableListView f2755a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    by(MomoUnrefreshExpandableListView momoUnrefreshExpandableListView, Looper looper) {
        super(looper);
        this.f2755a = momoUnrefreshExpandableListView;
    }

    public final void handleMessage(Message message) {
        if (message.what == 156) {
            MomoUnrefreshExpandableListView momoUnrefreshExpandableListView = this.f2755a;
            LinearLayout linearLayout = null;
            linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, message.arg1));
        } else if (message.what == 268) {
            this.f2755a.b();
        }
    }
}
