package com.immomo.momo.android.activity.event;

import android.text.Editable;
import android.text.TextWatcher;

final class bx implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PublishEventFeedActivity f1391a;

    bx(PublishEventFeedActivity publishEventFeedActivity) {
        this.f1391a = publishEventFeedActivity;
    }

    public final void afterTextChanged(Editable editable) {
        this.f1391a.t.setText(String.valueOf(editable.length()) + "/120字");
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
