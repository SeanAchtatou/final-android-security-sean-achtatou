package com.immomo.momo.android.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.widget.Button;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.bi;

public final class fr {

    /* renamed from: a  reason: collision with root package name */
    public HeaderLayout f1500a = null;
    public bi b = null;
    public Button c = null;
    public Button d = null;
    public int e;
    public int f;
    public int g;
    public int h;
    public boolean i;
    public boolean j;
    public int k;
    public int l;
    public int m;
    public int n;
    public Uri o;
    public Uri p;
    public Bitmap q;
    public String r;
    public String s;
    final /* synthetic */ ImageFactoryActivity t;

    public fr(ImageFactoryActivity imageFactoryActivity) {
        this.t = imageFactoryActivity;
    }

    public final void a() {
        this.t.finish();
    }

    public final void a(int i2) {
        this.t.setResult(i2);
    }

    public final void a(int i2, Intent intent) {
        this.t.setResult(i2, intent);
    }

    public final void b() {
        this.t.a(1);
    }
}
