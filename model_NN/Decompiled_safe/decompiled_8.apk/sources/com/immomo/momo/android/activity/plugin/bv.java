package com.immomo.momo.android.activity.plugin;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import com.immomo.momo.a;

final class bv implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ TxWeiboActivity f2076a;

    bv(TxWeiboActivity txWeiboActivity) {
        this.f2076a = txWeiboActivity;
    }

    public final void onClick(View view) {
        this.f2076a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(String.valueOf(a.f) + "/api/tencentweibo_profile_page/" + this.f2076a.m.f2877a)));
    }
}
