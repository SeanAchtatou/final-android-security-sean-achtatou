package com.immomo.momo.android.map;

import android.text.InputFilter;
import android.text.Spanned;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.util.ao;
import java.io.UnsupportedEncodingException;

final class ap implements InputFilter {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SelectSiteAMapActivity f2468a;

    ap(SelectSiteAMapActivity selectSiteAMapActivity) {
        this.f2468a = selectSiteAMapActivity;
    }

    public final CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
        try {
            if ((String.valueOf(this.f2468a.c.getText().toString().trim()) + charSequence.toString().trim()).getBytes("GBK").length > 20) {
                ao.a((CharSequence) "地点名称过长");
                return PoiTypeDef.All;
            }
        } catch (UnsupportedEncodingException e) {
            this.f2468a.j.a((Throwable) e);
        }
        return null;
    }
}
