package com.immomo.momo.android.activity.group;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.b.a;
import android.view.KeyEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.map.GeoAMapActivity;
import com.immomo.momo.android.map.GeoGoogleMapActivity;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.h;
import com.immomo.momo.service.bean.a.g;
import com.immomo.momo.service.x;
import java.util.Calendar;
import java.util.Date;

public class EditGroupPartyActivity extends ah implements View.OnClickListener {
    /* access modifiers changed from: private */
    public x A = null;
    /* access modifiers changed from: private */
    public int B = 0;
    private p C = null;
    private n D = null;
    private DatePickerDialog E = null;
    private TimePickerDialog F = null;
    /* access modifiers changed from: private */
    public int h = 0;
    /* access modifiers changed from: private */
    public int i = 0;
    /* access modifiers changed from: private */
    public int j = 0;
    /* access modifiers changed from: private */
    public int k = 0;
    /* access modifiers changed from: private */
    public int l = 0;
    /* access modifiers changed from: private */
    public Calendar m = Calendar.getInstance();
    private String n = PoiTypeDef.All;
    private HeaderLayout o = null;
    private bi p = null;
    /* access modifiers changed from: private */
    public TextView q;
    private TextView r;
    private TextView s;
    private View t;
    private View u;
    private ImageView v;
    private CheckBox w;
    /* access modifiers changed from: private */
    public g x = null;
    /* access modifiers changed from: private */
    public String y = PoiTypeDef.All;
    private String z = PoiTypeDef.All;

    static /* synthetic */ void f(EditGroupPartyActivity editGroupPartyActivity) {
        boolean z2 = true;
        String charSequence = editGroupPartyActivity.s.getText().toString();
        if (charSequence.length() < 10) {
            editGroupPartyActivity.a((CharSequence) "活动描述至少10个字");
            editGroupPartyActivity.s.requestFocus();
            z2 = false;
        } else if (charSequence.length() > 500) {
            editGroupPartyActivity.a((CharSequence) "活动描述不能超过500个字");
            editGroupPartyActivity.s.requestFocus();
            z2 = false;
        } else {
            editGroupPartyActivity.x.d = editGroupPartyActivity.s.getText().toString();
            if (editGroupPartyActivity.x.f == null) {
                editGroupPartyActivity.a((CharSequence) "还未设置活动开始时间");
                z2 = false;
            } else {
                editGroupPartyActivity.x.h = editGroupPartyActivity.w.isChecked() ? 1 : 0;
            }
        }
        if (!z2) {
            return;
        }
        if (editGroupPartyActivity.B == 2) {
            editGroupPartyActivity.C = new p(editGroupPartyActivity, editGroupPartyActivity);
            editGroupPartyActivity.C.execute(new Object[0]);
            return;
        }
        editGroupPartyActivity.D = new n(editGroupPartyActivity, editGroupPartyActivity);
        editGroupPartyActivity.D.execute(new Object[0]);
    }

    static /* synthetic */ void o(EditGroupPartyActivity editGroupPartyActivity) {
        if (editGroupPartyActivity.F == null) {
            editGroupPartyActivity.F = new TimePickerDialog(editGroupPartyActivity, new i(editGroupPartyActivity), editGroupPartyActivity.B == 1 ? editGroupPartyActivity.m.get(11) + 1 : editGroupPartyActivity.m.get(11), editGroupPartyActivity.B == 1 ? editGroupPartyActivity.m.get(12) + 1 : editGroupPartyActivity.m.get(12), true);
        }
        if (!editGroupPartyActivity.F.isShowing()) {
            editGroupPartyActivity.F.show();
        }
    }

    /* access modifiers changed from: private */
    public boolean u() {
        return !this.n.equals(new StringBuilder(String.valueOf(this.s.getText().toString().trim())).append(this.w.isChecked()).append(this.x.toString()).toString());
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        boolean z2 = true;
        super.a(bundle);
        setContentView((int) R.layout.activity_edit_groupparty);
        this.A = new x();
        this.o = (HeaderLayout) findViewById(R.id.layout_header);
        this.p = new bi(this);
        this.p.a("提交").a((int) R.drawable.ic_topbar_confirm);
        this.o.a(this.p, new g(this));
        this.q = (TextView) findViewById(R.id.tv_party_starttime);
        this.r = (TextView) findViewById(R.id.tv_party_address);
        this.s = (TextView) findViewById(R.id.tv_desc);
        this.t = findViewById(R.id.layout_time);
        this.t.setOnClickListener(this);
        this.u = findViewById(R.id.layout_place);
        this.u.setOnClickListener(this);
        this.w = (CheckBox) findViewById(R.id.checkbox_needphone);
        this.v = (ImageView) findViewById(R.id.iv_distance);
        if (bundle == null || !bundle.getBoolean("from_saveinstance", false)) {
            Intent intent = getIntent();
            this.y = intent.getStringExtra("pid");
            this.z = intent.getStringExtra("gid");
            this.B = intent.getIntExtra("modify_type", 0);
            this.e.b((Object) ("--------------get intent pid:" + this.y));
        } else {
            this.y = bundle.getString("pid");
            this.z = bundle.getString("gid");
            this.B = bundle.getInt("modify_type", 0);
            if (this.x == null) {
                this.x = new g(this.y);
            }
            this.x.f = new Date(bundle.getLong("time"));
        }
        switch (this.B) {
            case 1:
                this.o.setTitleText("创建群活动");
                break;
            case 2:
                this.o.setTitleText("编辑群活动");
                this.x = this.A.b(this.y);
                break;
            case 3:
                this.o.setTitleText("创建群活动");
                this.x = this.A.b(this.y);
                break;
        }
        this.e.b((Object) ("initPartyProfile:" + this.x));
        if (this.x == null) {
            this.x = new g(this.y);
        }
        this.x.c = getIntent().getStringExtra("gid");
        this.n = String.valueOf(this.x.d) + (this.x.h == 1) + this.x.toString();
        this.r.setText(a.a(this.x.e) ? "可选" : this.x.e);
        this.v.setVisibility(a.a(this.x.e) ? 8 : 0);
        this.s.setText(a.a(this.x.d) ? PoiTypeDef.All : this.x.d);
        if (this.x.f != null) {
            this.q.setText(a.g(this.x.f));
        } else {
            this.q.setText("未选择");
        }
        CheckBox checkBox = this.w;
        if (this.x.h != 1) {
            z2 = false;
        }
        checkBox.setChecked(z2);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        switch (i2) {
            case h.DragSortListView_drag_handle_id /*14*/:
                if (i3 == -1) {
                    this.x.j = intent.getDoubleExtra("latitude", 0.0d);
                    this.x.k = intent.getDoubleExtra("longitude", 0.0d);
                    this.x.e = intent.getStringExtra("geo_adress");
                } else {
                    this.x.j = 0.0d;
                    this.x.k = 0.0d;
                    this.x.e = PoiTypeDef.All;
                }
                this.r.setText(a.a(this.x.e) ? "可选" : this.x.e);
                this.v.setVisibility(a.a(this.x.e) ? 8 : 0);
                break;
        }
        super.onActivityResult(i2, i3, intent);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layout_time /*2131165512*/:
                if (this.x.f == null) {
                    this.x.f = new Date();
                }
                this.m.setTime(this.x.f);
                this.E = new DatePickerDialog(this, new h(this), this.m.get(1), this.m.get(2), this.m.get(5));
                if (!this.E.isShowing()) {
                    this.E.show();
                    return;
                }
                return;
            case R.id.tv_party_starttime /*2131165513*/:
            default:
                return;
            case R.id.layout_place /*2131165514*/:
                try {
                    Class.forName("com.google.android.maps.MapActivity");
                    Intent intent = new Intent(this, GeoGoogleMapActivity.class);
                    intent.putExtra("latitude", this.x.j);
                    intent.putExtra("longitude", this.x.k);
                    startActivityForResult(intent, 14);
                    return;
                } catch (Throwable th) {
                    Intent intent2 = new Intent(this, GeoAMapActivity.class);
                    intent2.putExtra("latitude", this.x.j);
                    intent2.putExtra("longitude", this.x.k);
                    startActivityForResult(intent2, 14);
                    return;
                }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.C != null && !this.C.isCancelled()) {
            this.C.cancel(true);
        }
        if (this.D != null && !this.D.isCancelled()) {
            this.D.cancel(true);
        }
        super.onDestroy();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4 || !u()) {
            return super.onKeyDown(i2, keyEvent);
        }
        if (this.B == 1) {
            n.a(this, "确认要放弃创建活动?", new j(this)).show();
            return true;
        }
        n nVar = new n(this);
        nVar.a();
        nVar.setTitle("退出编辑群活动");
        nVar.a("群活动已经修改，退出前要提交吗？");
        nVar.a(0, "提交", new k(this));
        nVar.a(1, "不提交", new l(this));
        nVar.a(2, "取消", new m());
        nVar.show();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putBoolean("from_saveinstance", true);
        bundle.putInt("modify_type", this.B);
        bundle.putString("gid", this.z);
        bundle.putString("pid", this.y);
        if (!(this.x == null || this.x.f == null)) {
            bundle.putLong("time", this.x.f.getTime());
        }
        super.onSaveInstanceState(bundle);
    }
}
