package com.immomo.momo.android.view;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.HeaderViewListAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import com.immomo.momo.R;
import com.immomo.momo.android.plugin.cropimage.q;
import com.immomo.momo.g;
import com.immomo.momo.util.m;

public class az extends ExpandableListView {

    /* renamed from: a  reason: collision with root package name */
    private View f2733a = null;
    private View b = null;
    private View c = null;
    private q d = null;
    private int e = -1;
    private int f = 8;
    /* access modifiers changed from: private */
    public boolean g = false;
    private boolean h = true;
    /* access modifiers changed from: private */
    public AdapterView.OnItemLongClickListener i;
    /* access modifiers changed from: private */
    public ExpandableListAdapter j;
    /* access modifiers changed from: private */
    public AbsListView.OnScrollListener k;
    private GestureDetector l;
    /* access modifiers changed from: private */
    public float m;
    private int n;
    private View o;
    private q p;

    public az(Context context) {
        super(context);
        new m("HandyListView");
        this.i = null;
        this.j = null;
        this.k = null;
        this.l = null;
        this.m = 0.0f;
        this.n = 0;
        this.o = null;
        this.p = null;
        b();
    }

    public az(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        new m("HandyListView");
        this.i = null;
        this.j = null;
        this.k = null;
        this.l = null;
        this.m = 0.0f;
        this.n = 0;
        this.o = null;
        this.p = null;
        b();
    }

    public az(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        new m("HandyListView");
        this.i = null;
        this.j = null;
        this.k = null;
        this.l = null;
        this.m = 0.0f;
        this.n = 0;
        this.o = null;
        this.p = null;
        b();
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        if (this.f2733a == null) {
            return;
        }
        if (z) {
            this.f2733a.setVisibility(0);
        } else {
            this.f2733a.setVisibility(8);
        }
    }

    private void b() {
        this.l = new GestureDetector(getContext(), new bc(this, (byte) 0));
        super.setOnScrollListener(new ba(this));
    }

    public final void a(View view) {
        this.f2733a = view;
        if (this.f2733a != null) {
            this.f2733a.setVisibility(8);
            LinearLayout linearLayout = new LinearLayout(getContext());
            linearLayout.setLayoutParams(new AbsListView.LayoutParams(-1, -2));
            linearLayout.setGravity(17);
            linearLayout.addView(this.f2733a);
            linearLayout.setBackgroundDrawable(view.getBackground());
            addHeaderView(linearLayout);
        }
    }

    public final boolean a() {
        return this.h;
    }

    public final boolean c() {
        return this.g;
    }

    /* access modifiers changed from: protected */
    public boolean d() {
        return true;
    }

    public final void e() {
        if (getAdapter() != null && getAdapter().getCount() > 0) {
            if (Build.VERSION.SDK_INT < 8) {
                setSelection(0);
                return;
            }
            if (getFirstVisiblePosition() > 5) {
                setSelection(5);
            }
            if (g.a()) {
                smoothScrollToPositionFromTop(0, 0);
            } else {
                smoothScrollToPosition(0);
            }
        }
    }

    public final void f() {
        if (this.o == null) {
            this.o = inflate(getContext(), R.layout.listitem_blank, null);
            this.o.setLayoutParams(new AbsListView.LayoutParams(-1, this.n));
            addFooterView(this.o);
            return;
        }
        removeFooterView(this.o);
        addFooterView(this.o);
    }

    public ListAdapter getBaseAdapter() {
        ListAdapter adapter = super.getAdapter();
        return adapter instanceof HeaderViewListAdapter ? ((HeaderViewListAdapter) adapter).getWrappedAdapter() : adapter;
    }

    public View getEmptyView() {
        return this.b != null ? this.b : super.getEmptyView();
    }

    public int getListPaddingBottom() {
        return this.n;
    }

    public float getScrollVelocity() {
        return this.m;
    }

    public int getSolidColor() {
        return this.e == -1 ? super.getSolidColor() : this.e;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return this.p != null ? this.p.a() : super.onInterceptTouchEvent(motionEvent);
    }

    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (this.d != null) {
            q qVar = this.d;
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        this.l.onTouchEvent(motionEvent);
        return super.onTouchEvent(motionEvent);
    }

    public void setAdapter(ExpandableListAdapter expandableListAdapter) {
        boolean z = false;
        if (this.c != null) {
            this.c.setVisibility(8);
        }
        setVisibility(0);
        super.setEmptyView(this.b);
        this.b = null;
        super.setAdapter(expandableListAdapter);
        if (expandableListAdapter == null || expandableListAdapter.isEmpty()) {
            z = true;
        }
        a(z);
        if (expandableListAdapter != null) {
            expandableListAdapter.registerDataSetObserver(new bb(this));
        }
        this.j = expandableListAdapter;
        if (this.n > 0 && d()) {
            f();
        }
    }

    public void setEmptyView(View view) {
        this.b = view;
        if (this.b != null) {
            this.b.setVisibility(8);
        }
    }

    public void setFadingEdgeColor(int i2) {
        this.e = i2;
    }

    public void setInterceptItemClick(boolean z) {
    }

    public void setListPaddingBottom(int i2) {
        if (i2 == -3) {
            this.n = (int) getContext().getResources().getDimension(R.dimen.bottomtabbar);
        } else {
            this.n = i2;
        }
    }

    public void setListViewHiddenValue(int i2) {
        this.f = i2;
    }

    public void setLoadingListView(View view) {
        this.c = view;
        if (this.c != null) {
            setVisibility(this.f);
            this.c.setVisibility(0);
        }
    }

    public void setOnInterceptTouchListener$156a8461(q qVar) {
        this.p = qVar;
    }

    public void setOnItemLongClickListener(AdapterView.OnItemLongClickListener onItemLongClickListener) {
        this.i = onItemLongClickListener;
        super.setOnItemLongClickListener(new bd(this, (byte) 0));
    }

    public void setOnScrollListener(AbsListView.OnScrollListener onScrollListener) {
        this.k = onScrollListener;
    }

    public void setOnSizeChangedListener$6af10a49(q qVar) {
        this.d = qVar;
    }

    public void setScorllEndReflush(boolean z) {
        this.h = z;
    }

    public void setScrolling(boolean z) {
        this.g = z;
    }
}
