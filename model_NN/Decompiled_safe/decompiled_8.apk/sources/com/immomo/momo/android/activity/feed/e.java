package com.immomo.momo.android.activity.feed;

import android.content.Context;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.u;

final class e extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1470a;
    private /* synthetic */ CorrectSiteActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public e(CorrectSiteActivity correctSiteActivity, Context context) {
        super(context);
        this.c = correctSiteActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return u.a().a(String.valueOf(this.c.k), this.c.h.getText().toString().trim());
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f1470a = new v(this.c);
        this.f1470a.a("请求提交中");
        this.f1470a.setCancelable(true);
        this.f1470a.setOnCancelListener(new f(this));
        this.f1470a.show();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        this.f1470a.dismiss();
        this.c.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.f1470a != null && !this.c.isFinishing()) {
            this.f1470a.dismiss();
        }
    }
}
