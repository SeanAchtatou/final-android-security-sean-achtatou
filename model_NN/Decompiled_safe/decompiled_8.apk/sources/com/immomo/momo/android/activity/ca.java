package com.immomo.momo.android.activity;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.immomo.momo.android.activity.feed.PublishFeedActivity;
import com.immomo.momo.android.activity.group.PublishGroupFeedActivity;
import com.immomo.momo.android.activity.tieba.EditTieActivity;
import com.immomo.momo.android.activity.tieba.PublishTieActivity;
import com.immomo.momo.service.m;

final class ca implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ DraftPublishActivity f1060a;

    ca(DraftPublishActivity draftPublishActivity) {
        this.f1060a = draftPublishActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        m mVar = (m) this.f1060a.h.getItem(i);
        if (mVar.f == 1) {
            Intent intent = new Intent(this.f1060a.getApplicationContext(), PublishTieActivity.class);
            intent.putExtra("ddraft", mVar.f3050a);
            intent.putExtra("ddraftid", mVar.b);
            this.f1060a.startActivity(intent);
        } else if (mVar.f == 2) {
            Intent intent2 = new Intent(this.f1060a.getApplicationContext(), PublishFeedActivity.class);
            intent2.putExtra("ddraft", mVar.f3050a);
            intent2.putExtra("ddraftid", mVar.b);
            this.f1060a.startActivity(intent2);
        } else if (mVar.f == 3) {
            Intent intent3 = new Intent(this.f1060a.getApplicationContext(), PublishGroupFeedActivity.class);
            intent3.putExtra("ddraft", mVar.f3050a);
            intent3.putExtra("ddraftid", mVar.b);
            this.f1060a.startActivity(intent3);
        } else if (mVar.f == 4) {
            Intent intent4 = new Intent(this.f1060a.getApplicationContext(), EditTieActivity.class);
            intent4.putExtra("ddraft", mVar.f3050a);
            intent4.putExtra("ddraftid", mVar.b);
            this.f1060a.startActivity(intent4);
        }
    }
}
