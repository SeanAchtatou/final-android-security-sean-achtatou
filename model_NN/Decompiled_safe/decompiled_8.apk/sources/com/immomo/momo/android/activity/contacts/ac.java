package com.immomo.momo.android.activity.contacts;

import android.content.Intent;
import android.view.View;
import android.widget.ExpandableListView;
import com.immomo.momo.android.activity.OtherProfileActivity;
import com.immomo.momo.service.bean.g;
import com.immomo.momo.service.bean.h;

final class ac implements ExpandableListView.OnChildClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ContactPeopleActivity f1138a;

    ac(ContactPeopleActivity contactPeopleActivity) {
        this.f1138a = contactPeopleActivity;
    }

    public final boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long j) {
        switch (((g) ((h) this.f1138a.k.get(i)).b.get(i2)).b) {
            case 1:
                ContactPeopleActivity.a(this.f1138a, i, i2);
                break;
            case 2:
                ContactPeopleActivity.a(this.f1138a, ((g) ((h) this.f1138a.k.get(i)).b.get(i2)).d, com.immomo.momo.g.a(this.f1138a.f.h));
                break;
            case 3:
                Intent intent = new Intent(this.f1138a, OtherProfileActivity.class);
                intent.putExtra("momoid", ((g) ((h) this.f1138a.k.get(i)).b.get(i2)).f3025a);
                intent.putExtra("tag", "local");
                this.f1138a.startActivity(intent);
                break;
        }
        return true;
    }
}
