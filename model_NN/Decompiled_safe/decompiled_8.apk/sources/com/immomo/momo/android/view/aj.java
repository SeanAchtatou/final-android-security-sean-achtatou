package com.immomo.momo.android.view;

import android.content.Intent;
import android.view.View;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.emotestore.MainEmotionActivity;
import com.immomo.momo.service.bean.at;

final class aj implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EmoteInputView f2719a;

    aj(EmoteInputView emoteInputView) {
        this.f2719a = emoteInputView;
    }

    public final void onClick(View view) {
        this.f2719a.getContext().startActivity(new Intent(this.f2719a.getContext(), MainEmotionActivity.class));
        if (this.f2719a.v.ak > this.f2719a.w.j) {
            at j = this.f2719a.w;
            at j2 = this.f2719a.w;
            long j3 = this.f2719a.v.ak;
            j2.j = j3;
            j.c("shop_update_time", Long.valueOf(j3));
            this.f2719a.findViewById(R.id.emotionbar_iv_newmark).setVisibility(8);
        }
    }
}
