package com.immomo.momo.android.activity.common;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.b.a;
import android.widget.ListAdapter;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.a.hl;
import com.immomo.momo.android.activity.lh;
import com.immomo.momo.android.broadcast.d;
import com.immomo.momo.android.broadcast.t;
import com.immomo.momo.android.broadcast.u;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.MomoRefreshListView;
import com.immomo.momo.android.view.bl;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.g;
import com.immomo.momo.service.aq;
import com.immomo.momo.service.h;
import com.immomo.momo.service.y;
import com.immomo.momo.util.k;
import java.util.Date;
import java.util.List;

public class ba extends lh implements l, bl, bu {
    /* access modifiers changed from: private */
    public MomoRefreshListView O;
    /* access modifiers changed from: private */
    public hl P;
    /* access modifiers changed from: private */
    public List Q;
    /* access modifiers changed from: private */
    public List R;
    /* access modifiers changed from: private */
    public Date S = null;
    /* access modifiers changed from: private */
    public y T;
    /* access modifiers changed from: private */
    public h U;
    private aq V = null;
    /* access modifiers changed from: private */
    public bq W;
    /* access modifiers changed from: private */
    public bp X;
    private u Y = null;
    private t Z = null;
    /* access modifiers changed from: private */
    public Handler aa = new Handler();
    private d ab = new bb(this);
    private d ac = new bk(this);
    private j ad;

    /* access modifiers changed from: private */
    public void P() {
        this.Q = this.T.c();
        this.R = this.U.c();
        this.P = new hl(g.c(), this.Q, this.R, this.M, this.O);
        this.O.setAdapter((ListAdapter) this.P);
    }

    static /* synthetic */ void a(ba baVar, int i) {
        g.q().A = i;
        baVar.V.c(i, g.q().h);
    }

    static /* synthetic */ void b(ba baVar, int i) {
        g.q().z = i;
        baVar.V.b(i, g.q().h);
    }

    static /* synthetic */ j m(ba baVar) {
        if (baVar.ad == null) {
            baVar.ad = (j) baVar.c();
        }
        return baVar.ad;
    }

    /* access modifiers changed from: protected */
    public final int B() {
        return R.layout.layout_relation_group;
    }

    /* access modifiers changed from: protected */
    public final void C() {
        this.O = (MomoRefreshListView) c((int) R.id.listview);
    }

    public final void O() {
    }

    public final void S() {
        super.S();
    }

    public final void a(Context context, HeaderLayout headerLayout) {
    }

    public final void ae() {
        super.ae();
        new k("PI", "P64").e();
    }

    public final void ag() {
        super.ag();
        new k("PO", "P64").e();
    }

    public final void ai() {
        this.O.i();
    }

    public final void aj() {
        Date date;
        P();
        try {
            String str = (String) this.N.b("lasttime_mygroups_success", PoiTypeDef.All);
            date = !a.a(str) ? a.k(str) : null;
        } catch (Exception e) {
            date = null;
        }
        this.O.setLastFlushTime(date);
        try {
            String str2 = (String) this.N.b("lasttime_my_grouplist", PoiTypeDef.All);
            if (!a.a((CharSequence) str2)) {
                this.S = a.k(str2);
            }
        } catch (Exception e2) {
        }
    }

    public final void b(int i, int i2, Intent intent) {
        switch (i) {
            case 15:
                if (i2 == -1) {
                    this.P.a(new y().e(intent.getStringExtra("gid")));
                    this.P.notifyDataSetChanged();
                    return;
                }
                return;
            default:
                return;
        }
    }

    public final void b_() {
        if (this.W != null && !this.W.isCancelled()) {
            this.W.cancel(true);
        }
        this.W = new bq(this, c());
        this.W.execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public final void g(Bundle bundle) {
        this.T = new y();
        this.U = new h();
        this.Y = new u(c());
        this.Y.a(this.ab);
        this.Z = new t(c());
        this.Z.a(this.ac);
        this.V = new aq();
        this.O.setOnPullToRefreshListener$42b903f6(this);
        this.O.setOnCancelListener$135502(new br(this, (byte) 0));
        this.O.setOnItemClickListener(new bo(this));
        aj();
    }

    public final void m() {
        super.m();
        if ((this.P != null && this.P.isEmpty()) || this.S == null || System.currentTimeMillis() - this.S.getTime() > 900000) {
            this.O.l();
        }
    }

    public final void p() {
        super.p();
        if (this.W != null && !this.W.isCancelled()) {
            this.W.cancel(true);
            this.W = null;
        }
        if (this.X != null && this.X.isCancelled()) {
            this.X.cancel(true);
            this.X = null;
        }
        if (this.Y != null) {
            a(this.Y);
            this.Y = null;
        }
        if (this.Z != null) {
            a(this.Z);
            this.Z = null;
        }
    }

    public final void u() {
    }
}
