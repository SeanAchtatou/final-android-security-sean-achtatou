package com.immomo.momo.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import com.immomo.momo.R;
import com.immomo.momo.service.bean.c.f;
import com.immomo.momo.util.j;
import java.util.ArrayList;
import java.util.List;

public class TiebaCategoryGuidView extends d implements f {
    /* access modifiers changed from: private */
    public List b = new ArrayList();

    public TiebaCategoryGuidView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setItemViewWeith(55);
        setMinPading(5);
    }

    static /* synthetic */ void a(View view, View view2) {
        AnimationSet animationSet = new AnimationSet(true);
        ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 0.4f, 1.0f, 0.4f, (float) (view2.getMeasuredWidth() / 4), (float) (view2.getMeasuredHeight() / 4));
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(alphaAnimation);
        animationSet.setDuration(600);
        animationSet.setAnimationListener(new dh(view2, view));
        view.clearAnimation();
        view.startAnimation(animationSet);
    }

    static /* synthetic */ void b(View view, View view2) {
        AnimationSet animationSet = new AnimationSet(true);
        ScaleAnimation scaleAnimation = new ScaleAnimation(0.3f, 1.0f, 0.3f, 1.0f, (float) (view2.getMeasuredWidth() / 4), (float) (view2.getMeasuredHeight() / 4));
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.9f, 1.0f);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(alphaAnimation);
        animationSet.setDuration(600);
        animationSet.setInterpolator(new OvershootInterpolator(4.0f));
        animationSet.setAnimationListener(new dg(view, view2));
        view.clearAnimation();
        view.startAnimation(animationSet);
    }

    public final /* synthetic */ View a(Object obj) {
        f fVar = (f) obj;
        View inflate = inflate(getContext(), R.layout.listitem_tiebaguidcategory, null);
        j.a(fVar, (ImageView) inflate.findViewById(R.id.tiebacategory_item_img_category), (ViewGroup) null, 3);
        ((EmoteTextView) inflate.findViewById(R.id.tiebacategory_item_text_name)).setText(fVar.b);
        View findViewById = inflate.findViewById(R.id.tiebacategory_item_img_click);
        View findViewById2 = inflate.findViewById(R.id.tiebacategory_item_img_layout);
        if (fVar.g) {
            findViewById.setVisibility(0);
            findViewById2.setBackgroundResource(R.drawable.bg_cover_tiebaguide_outerpress);
            if (!this.b.contains(fVar.f3021a)) {
                this.b.add(fVar.f3021a);
            }
        } else {
            findViewById.setVisibility(8);
            findViewById2.setBackgroundResource(R.drawable.bg_cover_userheader);
            if (this.b.contains(fVar.f3021a)) {
                this.b.remove(fVar.f3021a);
            }
        }
        return inflate;
    }

    public final void a(int i, View view) {
        if (this.b.contains(((f) a(i)).f3021a)) {
            this.b.remove(((f) a(i)).f3021a);
            view.findViewById(R.id.tiebacategory_item_img_click).setVisibility(8);
            view.findViewById(R.id.tiebacategory_item_img_layout).setBackgroundResource(R.drawable.bg_cover_userheader);
            return;
        }
        view.findViewById(R.id.tiebacategory_item_img_click).setVisibility(0);
        view.findViewById(R.id.tiebacategory_item_img_layout).setBackgroundResource(R.drawable.bg_cover_tiebaguide_outerpress);
        this.b.add(((f) a(i)).f3021a);
    }

    /* access modifiers changed from: protected */
    public final void b(int i, View view) {
        view.findViewById(R.id.tiebacategory_item_img_layout).setOnClickListener(new df(this, view, i));
    }

    public int getSelectCount() {
        return this.b.size();
    }

    public String getSelectIds() {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.b.size()) {
                return sb.toString();
            }
            sb.append((String) this.b.get(i2));
            if (i2 != this.b.size() - 1) {
                sb.append(",");
            }
            i = i2 + 1;
        }
    }
}
