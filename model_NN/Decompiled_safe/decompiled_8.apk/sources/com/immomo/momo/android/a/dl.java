package com.immomo.momo.android.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.b.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.android.view.EmoteTextView;
import com.immomo.momo.android.view.HandyListView;
import com.immomo.momo.b;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.j;
import com.immomo.momo.util.m;
import java.util.List;

public final class dl extends a {
    private HandyListView d = null;
    private boolean e;

    public dl(Context context, List list, HandyListView handyListView, boolean z) {
        super(context, list);
        new m("FriendsListViewAdapter");
        this.e = false;
        this.e = z;
        this.b = context;
        this.d = handyListView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap
     arg types: [java.lang.String, int]
     candidates:
      com.immomo.momo.b.a(java.lang.String, android.graphics.Bitmap):android.graphics.Bitmap
      com.immomo.momo.b.a(java.lang.String, boolean):android.graphics.Bitmap */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            dm dmVar = new dm((byte) 0);
            view = a((int) R.layout.listitem_user);
            dmVar.f777a = (ImageView) view.findViewById(R.id.userlist_item_iv_face);
            dmVar.b = (TextView) view.findViewById(R.id.userlist_item_tv_name);
            dmVar.c = (TextView) view.findViewById(R.id.userlist_item_tv_age);
            dmVar.d = (TextView) view.findViewById(R.id.userlist_item_tv_distance);
            dmVar.e = (TextView) view.findViewById(R.id.userlist_tv_time);
            dmVar.f = (EmoteTextView) view.findViewById(R.id.userlist_item_tv_sign);
            dmVar.i = (ImageView) view.findViewById(R.id.userlist_item_iv_gender);
            dmVar.h = view.findViewById(R.id.userlist_item_layout_genderbackgroud);
            dmVar.q = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_group_role);
            dmVar.j = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_weibo);
            dmVar.k = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_txweibo);
            dmVar.l = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_renren);
            dmVar.m = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_vip);
            dmVar.n = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_relation);
            dmVar.o = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_multipic);
            dmVar.p = (ImageView) view.findViewById(R.id.userlist_item_pic_iv_industry);
            dmVar.g = (ImageView) view.findViewById(R.id.userlist_item_pic_sign);
            dmVar.r = view.findViewById(R.id.userlist_tv_timedriver);
            view.setTag(R.id.tag_userlist_item, dmVar);
        }
        bf bfVar = (bf) getItem(i);
        dm dmVar2 = (dm) view.getTag(R.id.tag_userlist_item);
        dmVar2.d.setText(bfVar.Z);
        if (bfVar.d() < 0.0f) {
            dmVar2.e.setVisibility(8);
            dmVar2.r.setVisibility(8);
        } else {
            dmVar2.e.setVisibility(0);
            dmVar2.r.setVisibility(0);
            dmVar2.e.setText(bfVar.aa);
        }
        dmVar2.c.setText(new StringBuilder(String.valueOf(bfVar.I)).toString());
        dmVar2.b.setText(bfVar.h());
        if (bfVar.b()) {
            dmVar2.b.setTextColor(g.c((int) R.color.font_vip_name));
        } else {
            dmVar2.b.setTextColor(g.c((int) R.color.text_color));
        }
        dmVar2.f.setText(bfVar.n());
        if (!a.a((CharSequence) bfVar.R)) {
            Bitmap b = b.b(bfVar.R);
            if (b != null) {
                dmVar2.g.setVisibility(0);
                dmVar2.g.setImageBitmap(b);
            } else {
                dmVar2.g.setVisibility(8);
            }
        } else {
            dmVar2.g.setVisibility(8);
        }
        if ("F".equals(bfVar.H)) {
            dmVar2.h.setBackgroundResource(R.drawable.bg_gender_famal);
            dmVar2.i.setImageResource(R.drawable.ic_user_famale);
        } else {
            dmVar2.h.setBackgroundResource(R.drawable.bg_gender_male);
            dmVar2.i.setImageResource(R.drawable.ic_user_male);
        }
        if (bfVar.j()) {
            dmVar2.o.setVisibility(0);
        } else {
            dmVar2.o.setVisibility(8);
        }
        if (this.e || !"both".equals(bfVar.P)) {
            dmVar2.n.setVisibility(8);
        } else {
            dmVar2.n.setVisibility(0);
        }
        if (bfVar.aJ == 1 || bfVar.aJ == 3) {
            dmVar2.q.setVisibility(0);
            dmVar2.q.setImageResource(bfVar.aJ == 1 ? R.drawable.ic_userinfo_groupowner : R.drawable.ic_userinfo_group);
        } else {
            dmVar2.q.setVisibility(8);
        }
        if (bfVar.an) {
            dmVar2.j.setVisibility(0);
            dmVar2.j.setImageResource(bfVar.ap ? R.drawable.ic_userinfo_weibov : R.drawable.ic_userinfo_weibo);
        } else {
            dmVar2.j.setVisibility(8);
        }
        if (bfVar.at) {
            dmVar2.k.setVisibility(0);
            dmVar2.k.setImageResource(bfVar.au ? R.drawable.ic_userinfo_tweibov : R.drawable.ic_userinfo_tweibo);
        } else {
            dmVar2.k.setVisibility(8);
        }
        if (bfVar.ar) {
            dmVar2.l.setVisibility(0);
        } else {
            dmVar2.l.setVisibility(8);
        }
        if (bfVar.b()) {
            dmVar2.m.setVisibility(0);
            if (bfVar.c()) {
                dmVar2.m.setImageResource(R.drawable.ic_userinfo_vip_year);
            } else {
                dmVar2.m.setImageResource(R.drawable.ic_userinfo_vip);
            }
        } else {
            dmVar2.m.setVisibility(8);
        }
        if (!a.a((CharSequence) bfVar.M)) {
            dmVar2.p.setVisibility(0);
            dmVar2.p.setImageBitmap(b.a(bfVar.M, true));
        } else {
            dmVar2.p.setVisibility(8);
        }
        j.a(bfVar, dmVar2.f777a, this.d, 3);
        return view;
    }
}
