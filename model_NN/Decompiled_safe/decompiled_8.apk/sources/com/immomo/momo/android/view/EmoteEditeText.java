package com.immomo.momo.android.view;

import android.content.Context;
import android.support.v4.b.a;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.plugin.b.e;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmoteEditeText extends EditText {
    public EmoteEditeText(Context context) {
        this(context, null);
        a();
    }

    public EmoteEditeText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    public EmoteEditeText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a();
    }

    private void a() {
        setEditableFactory(new ab(this));
    }

    private CharSequence b(CharSequence charSequence) {
        boolean z = false;
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(charSequence);
        Matcher matcher = Pattern.compile("(\\[[.[^\\[\\]]]*?\\|et\\|([.[^\\[\\]]]*?\\|)*[.[^\\[\\]]]*?\\])").matcher(charSequence);
        while (matcher.find()) {
            z = true;
            getContext();
            spannableStringBuilder.setSpan(new e(matcher.group()), matcher.start(), matcher.end(), 33);
        }
        return z ? spannableStringBuilder : charSequence;
    }

    public CharSequence a(CharSequence charSequence) {
        if (charSequence == null) {
            return PoiTypeDef.All;
        }
        if (charSequence.length() == 0) {
            return charSequence;
        }
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(charSequence);
        Matcher matcher = Pattern.compile("([-])").matcher(charSequence);
        boolean z = false;
        while (matcher.find()) {
            z = true;
            spannableStringBuilder.setSpan(new ad(getContext(), getResources().getIdentifier("zemoji_" + a.m(matcher.group()).substring(2), "drawable", getContext().getPackageName()), (byte) 0), matcher.start(), matcher.end(), 33);
        }
        if (z) {
            charSequence = spannableStringBuilder;
        }
        return b(charSequence);
    }

    public final void a(CharSequence charSequence, int i) {
        if (getText() instanceof Editable) {
            getText().insert(i, charSequence);
        } else {
            append(charSequence);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.text.Editable.append(java.lang.CharSequence, int, int):android.text.Editable}
     arg types: [java.lang.CharSequence, int, int]
     candidates:
      ClspMth{android.text.Editable.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{java.lang.Appendable.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{android.text.Editable.append(java.lang.CharSequence, int, int):android.text.Editable} */
    public void append(CharSequence charSequence, int i, int i2) {
        if (getText() instanceof Editable) {
            getText().append(charSequence, i, i2);
        } else {
            super.append(a(charSequence), i, i2);
        }
    }

    public void setText(CharSequence charSequence, TextView.BufferType bufferType) {
        super.setText(a(charSequence), bufferType);
    }
}
