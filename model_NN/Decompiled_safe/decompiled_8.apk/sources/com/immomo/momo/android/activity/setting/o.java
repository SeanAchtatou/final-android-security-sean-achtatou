package com.immomo.momo.android.activity.setting;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.w;
import com.immomo.momo.util.ao;

final class o extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f2138a;
    private String c;
    private int d;
    private /* synthetic */ FeedBackActivity e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public o(FeedBackActivity feedBackActivity, Context context, String str, int i) {
        super(context);
        this.e = feedBackActivity;
        if (feedBackActivity.q != null) {
            feedBackActivity.q.cancel(true);
        }
        feedBackActivity.q = this;
        this.c = str;
        this.d = i;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return w.a().a(this.c, this.d, this.e.u);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f2138a = new v(this.e);
        this.f2138a.a("请求提交中");
        this.f2138a.setCancelable(true);
        this.f2138a.setOnCancelListener(new p(this));
        this.e.a(this.f2138a);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        ao.e(R.string.feedback_success);
        this.e.setResult(-1);
        this.e.finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.e.p();
    }
}
