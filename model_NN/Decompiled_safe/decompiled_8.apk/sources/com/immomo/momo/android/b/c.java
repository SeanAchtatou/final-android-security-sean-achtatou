package com.immomo.momo.android.b;

import android.location.Location;
import java.util.List;

final class c extends p {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f2322a;
    private final /* synthetic */ List c;
    private final /* synthetic */ String d;
    private final /* synthetic */ String e;
    private final /* synthetic */ Object f;

    c(a aVar, List list, String str, String str2, Object obj) {
        this.f2322a = aVar;
        this.c = list;
        this.d = str;
        this.e = str2;
        this.f = obj;
    }

    public final void a(Location location, int i, int i2, int i3) {
        this.c.add(location);
        if (this.c.size() > 0) {
            try {
                this.f2322a.b.a(this.d);
                this.f2322a.b.a(this.e);
            } catch (Throwable th) {
                this.f2322a.f2312a.a(th);
            }
            synchronized (this.f) {
                this.f.notify();
            }
        }
    }
}
