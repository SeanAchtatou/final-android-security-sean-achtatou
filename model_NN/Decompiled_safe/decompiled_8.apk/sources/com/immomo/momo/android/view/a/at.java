package com.immomo.momo.android.view.a;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.PopupWindow;
import com.immomo.momo.R;
import com.immomo.momo.g;
import com.immomo.momo.util.m;
import java.util.Arrays;
import java.util.List;

public final class at extends e implements PopupWindow.OnDismissListener {
    public static final int b = g.a(150.0f);
    private List c = null;
    private PopupWindow.OnDismissListener d;
    private AdapterView.OnItemClickListener e;
    /* access modifiers changed from: private */
    public int f;

    static {
        g.a(230.0f);
    }

    public at(Context context, View view, String[] strArr) {
        super(context);
        new m("SimpleMenuSmartBox");
        this.d = null;
        this.e = null;
        this.f = -1;
        this.c = Arrays.asList(strArr);
        a();
        a(view);
        a(new au(this, context, this.c));
        a(new Drawable[]{g.b((int) R.drawable.bg_dropmenu), g.b((int) R.drawable.bg_dropmenu_up)});
        super.a((PopupWindow.OnDismissListener) this);
        a(b);
        c();
    }

    /* access modifiers changed from: protected */
    public final void a(int i, View view) {
        e();
        if (this.f != i) {
            this.f = i;
            if (this.e != null) {
                this.e.onItemClick(i(), view, i, -1);
            }
        }
    }

    public final void a(AdapterView.OnItemClickListener onItemClickListener) {
        this.e = onItemClickListener;
    }

    public final void a(PopupWindow.OnDismissListener onDismissListener) {
        this.d = onDismissListener;
    }

    public final void d() {
        super.d();
        b().setSelected(true);
    }

    public final void onDismiss() {
        if (this.d != null) {
            this.d.onDismiss();
        }
        b().setSelected(false);
    }
}
