package com.immomo.momo.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.ImageButton;
import com.immomo.momo.R;

public class NumberPickerButton extends ImageButton {

    /* renamed from: a  reason: collision with root package name */
    private NumberPicker f2642a;

    public NumberPickerButton(Context context) {
        super(context);
    }

    public NumberPickerButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NumberPickerButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    private void a() {
        if (R.id.increment == getId()) {
            this.f2642a.b();
        } else if (R.id.decrement == getId()) {
            this.f2642a.c();
        }
    }

    private void a(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 3 || motionEvent.getAction() == 1) {
            a();
        }
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (i == 23 || i == 66) {
            a();
        }
        return super.onKeyUp(i, keyEvent);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        a(motionEvent);
        return super.onTouchEvent(motionEvent);
    }

    public boolean onTrackballEvent(MotionEvent motionEvent) {
        a(motionEvent);
        return super.onTrackballEvent(motionEvent);
    }

    public void setNumberPicker(NumberPicker numberPicker) {
        this.f2642a = numberPicker;
    }
}
