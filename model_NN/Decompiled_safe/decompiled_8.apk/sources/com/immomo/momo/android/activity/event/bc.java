package com.immomo.momo.android.activity.event;

import android.content.Context;
import android.widget.ListAdapter;
import com.immomo.momo.android.a.cf;
import com.immomo.momo.android.c.d;
import com.immomo.momo.protocol.a.j;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

final class bc extends d {

    /* renamed from: a  reason: collision with root package name */
    private List f1373a = new ArrayList();
    private /* synthetic */ HistoryEventListActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bc(HistoryEventListActivity historyEventListActivity, Context context) {
        super(context);
        this.c = historyEventListActivity;
        if (historyEventListActivity.k != null) {
            historyEventListActivity.k.cancel(true);
        }
        historyEventListActivity.k = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        boolean a2 = j.a().a(this.f1373a, 0);
        this.c.o.b(this.f1373a);
        this.c.n.clear();
        this.c.n.addAll(this.f1373a);
        return Boolean.valueOf(a2);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        Boolean bool = (Boolean) obj;
        this.c.h.setLastFlushTime(this.c.l);
        this.c.g.a("nevents_lasttime_success", this.c.l);
        this.c.g.c("nefilter_remain", bool);
        this.c.m = this.f1373a;
        this.c.i = new cf(this.c, this.c.m, this.c.h);
        this.c.h.setAdapter((ListAdapter) this.c.i);
        if (!bool.booleanValue()) {
            this.c.h.k();
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.c.h.n();
        this.c.l = new Date();
        this.c.g.a("nevents_latttime_reflush", this.c.l);
        this.c.k = (bc) null;
    }
}
