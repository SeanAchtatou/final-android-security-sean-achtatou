package com.immomo.momo.android.view;

import android.content.Context;
import android.view.ViewConfiguration;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import mm.purchasesdk.PurchaseCode;

public final class dc {
    private static float n = 0.4f;
    private static float o = (1.0f - n);
    private static final float[] p = new float[PurchaseCode.QUERY_OK];
    private static float r = 8.0f;
    private static float s;

    /* renamed from: a  reason: collision with root package name */
    private int f2772a;
    private int b;
    private int c;
    private int d;
    private int e;
    private int f;
    private long g;
    private int h;
    private float i;
    private float j;
    private float k;
    private boolean l;
    private Interpolator m;
    private final float q;

    static {
        float f2;
        float f3;
        Math.log(0.75d);
        Math.log(0.9d);
        float f4 = 0.0f;
        int i2 = 0;
        while (i2 <= 100) {
            float f5 = ((float) i2) / 100.0f;
            float f6 = 1.0f;
            float f7 = f4;
            while (true) {
                f2 = ((f6 - f7) / 2.0f) + f7;
                f3 = 3.0f * f2 * (1.0f - f2);
                float f8 = ((((1.0f - f2) * n) + (o * f2)) * f3) + (f2 * f2 * f2);
                if (((double) Math.abs(f8 - f5)) < 1.0E-5d) {
                    break;
                } else if (f8 > f5) {
                    f6 = f2;
                } else {
                    f7 = f2;
                }
            }
            p[i2] = (f2 * f2 * f2) + f3;
            i2++;
            f4 = f7;
        }
        p[100] = 1.0f;
        s = 1.0f;
        s = 1.0f / a(1.0f);
    }

    public dc(Context context) {
        this(context, null);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public dc(Context context, Interpolator interpolator) {
        this(context, interpolator, (byte) 0);
        int i2 = context.getApplicationInfo().targetSdkVersion;
    }

    public dc(Context context, Interpolator interpolator, byte b2) {
        this.l = true;
        this.m = interpolator;
        this.q = context.getResources().getDisplayMetrics().density * 160.0f;
        ViewConfiguration.getScrollFriction();
        float f2 = this.q;
    }

    private static float a(float f2) {
        float f3 = r * f2;
        return (f3 < 1.0f ? f3 - (1.0f - ((float) Math.exp((double) (-f3)))) : ((1.0f - ((float) Math.exp((double) (1.0f - f3)))) * 0.63212055f) + 0.36787945f) * s;
    }

    public final void a(int i2, int i3, int i4, int i5) {
        a(i2, i3, i4, i5, PurchaseCode.AUTH_OTHER_ERROR);
    }

    public final void a(int i2, int i3, int i4, int i5, int i6) {
        this.l = false;
        this.h = i6;
        this.g = AnimationUtils.currentAnimationTimeMillis();
        this.f2772a = i2;
        this.b = i3;
        this.c = i2 + i4;
        this.d = i3 + i5;
        this.j = (float) i4;
        this.k = (float) i5;
        this.i = 1.0f / ((float) this.h);
    }

    public final boolean a() {
        return this.l;
    }

    public final int b() {
        return this.e;
    }

    public final int c() {
        return this.f;
    }

    public final boolean d() {
        if (this.l) {
            return false;
        }
        int currentAnimationTimeMillis = (int) (AnimationUtils.currentAnimationTimeMillis() - this.g);
        if (currentAnimationTimeMillis < this.h) {
            float f2 = ((float) currentAnimationTimeMillis) * this.i;
            float a2 = this.m == null ? a(f2) : this.m.getInterpolation(f2);
            this.e = this.f2772a + Math.round(this.j * a2);
            this.f = Math.round(a2 * this.k) + this.b;
        } else {
            this.e = this.c;
            this.f = this.d;
            this.l = true;
        }
        return true;
    }

    public final void e() {
        this.e = this.c;
        this.f = this.d;
        this.l = true;
    }
}
