package com.immomo.momo.android.activity.discuss;

import com.immomo.momo.android.view.a.al;
import java.util.List;

final class c implements al {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ DiscussMemberListActivity f1229a;

    c(DiscussMemberListActivity discussMemberListActivity) {
        this.f1229a = discussMemberListActivity;
    }

    public final void a(int i) {
        this.f1229a.q = i + 1;
        List c = this.f1229a.l.c(this.f1229a.o, this.f1229a.q);
        this.f1229a.e.a((Object) ("list: " + c));
        if (c != null) {
            this.f1229a.a(c);
        }
    }
}
