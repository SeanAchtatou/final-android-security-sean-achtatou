package com.immomo.momo.android.view;

import android.content.Context;
import android.support.v4.b.a;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.immomo.momo.R;
import com.immomo.momo.g;
import java.util.Date;

public class MomoRefreshListView extends RefreshOnOverScrollListView implements cw {

    /* renamed from: a  reason: collision with root package name */
    protected boolean f2639a;
    protected int b;
    protected TextView c;
    private int g;
    /* access modifiers changed from: private */
    public bu h;
    private LinearLayout i;
    private ImageView j = null;
    private boolean k;
    private TextView l;
    private TextView m;
    private TextView n;
    private LinearLayout o;
    private RotatingImageView p;
    private cw q = this;
    private int r;
    private LoadingButton s = null;
    private boolean t = false;
    /* access modifiers changed from: private */
    public boolean u = true;
    private Animation v = null;
    private View w = null;
    /* access modifiers changed from: private */
    public cv x = null;

    public MomoRefreshListView(Context context) {
        super(context);
        a();
    }

    public MomoRefreshListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    public MomoRefreshListView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, com.immomo.momo.android.view.MomoRefreshListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void a() {
        this.h = null;
        setScrollbarFadingEnabled(true);
        setFastScrollEnabled(true);
        View inflate = g.o().inflate((int) R.layout.common_list_loadmore, (ViewGroup) null);
        this.w = inflate;
        this.s = (LoadingButton) inflate.findViewById(R.id.btn_loadmore);
        this.v = AnimationUtils.loadAnimation(getContext(), R.anim.loading);
        this.o = (LinearLayout) g.o().inflate(getRefreshLayout(), (ViewGroup) this, false);
        this.c = (TextView) this.o.findViewById(R.id.refresh_tv_message);
        this.p = (RotatingImageView) this.o.findViewById(R.id.refresh_iv_image);
        this.l = (TextView) this.o.findViewById(R.id.refresh_tv_time);
        a(this.o, 60);
        setOverScrollListener$2880bfd9(this.q);
        setAutoOverScrollMultiplier(0.4f);
        LinearLayout linearLayout = (LinearLayout) g.o().inflate(getRefreshingLayout(), (ViewGroup) this, false);
        this.i = (LinearLayout) linearLayout.findViewById(R.id.refresh_loading_container);
        this.i.setVisibility(8);
        this.j = (ImageView) linearLayout.findViewById(R.id.refresh_iv_processing);
        this.m = (TextView) linearLayout.findViewById(R.id.refresh_tv_time);
        this.n = (TextView) linearLayout.findViewById(R.id.refresh_tv_message);
        this.i.findViewById(R.id.refresh_iv_cancel).setOnClickListener(new bv(this));
        this.g = g.a(60.0f);
        addHeaderView(linearLayout);
        setFadingEdgeColor(0);
        setFadingEdgeLength(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public final void a(int i2, int i3) {
        if (this.r == 0) {
            this.r = g.a(30.0f);
        }
        if (i2 > 0) {
            if (this.k) {
                setOverScrollState(1);
            } else if (i2 < i3) {
                setOverScrollState(4);
            } else if (p()) {
                setOverScrollState(2);
            } else {
                setOverScrollState(3);
            }
        }
        if (i2 <= this.r || this.k) {
            this.p.setDegress(0);
            return;
        }
        this.p.setDegress((int) (Math.min(1.0f, ((float) (i2 - this.r)) / ((float) (i3 - this.r))) * 180.0f));
    }

    public void b() {
        r();
        if (this.h != null && !this.f2639a) {
            setLoadingVisible(true);
            this.h.b_();
        }
    }

    /* access modifiers changed from: protected */
    public final boolean e() {
        return false;
    }

    public LoadingButton getFooterViewButton() {
        return this.s;
    }

    public LinearLayout getLoadingContainer() {
        return this.i;
    }

    public int getLoadingHeigth() {
        return this.g;
    }

    public cv getOnCancelListener$67ffae72() {
        return this.x;
    }

    /* access modifiers changed from: protected */
    public int getRefreshLayout() {
        return R.layout.include_pull_to_refresh;
    }

    /* access modifiers changed from: protected */
    public int getRefreshingLayout() {
        return R.layout.include_pull_to_refreshing_header;
    }

    public final void k() {
        removeFooterView(this.w);
    }

    public final void l() {
        postDelayed(new bx(this), 200);
    }

    public final void m() {
        b();
    }

    public final void n() {
        setLoadingVisible(false);
    }

    public final void o() {
        if (this.f2639a && this.i != null) {
            this.f2639a = false;
            if (this.i instanceof ReflushingHeaderView) {
                ((ReflushingHeaderView) this.i).a();
            } else {
                this.i.setVisibility(8);
            }
            if (q()) {
                setPreventOverScroll(false);
            }
            setTipVisible(true);
        }
    }

    public void setAdapter(ListAdapter listAdapter) {
        super.setAdapter(listAdapter);
        removeFooterView(this.w);
        if (listAdapter != null && this.t) {
            addFooterView(this.w);
        }
        if (getListPaddingBottom() > 0) {
            j();
        }
    }

    public void setCompleteScrollTop(boolean z) {
        this.u = z;
    }

    public void setEnableLoadMoreFoolter(boolean z) {
        this.t = z;
    }

    public void setLastFlushTime(Date date) {
        String str = String.valueOf(getContext().getString(R.string.pull_to_refresh_lastTime)) + "：";
        if (date != null) {
            str = String.valueOf(str) + a.a(date);
        }
        this.l.setText(str);
        this.m.setText(str);
    }

    public void setLoadingViewText(int i2) {
        this.n.setText(i2);
    }

    public void setLoadingVisible(boolean z) {
        boolean z2 = false;
        if (this.i != null && this.f2639a != z) {
            this.f2639a = z;
            if (!this.e.a()) {
                this.e.e();
            }
            if (z) {
                this.i.setAnimation(null);
                this.i.setVisibility(0);
                this.j.startAnimation(this.v);
                if (q() != z) {
                    setPreventOverScroll(z);
                }
                if (!z) {
                    z2 = true;
                }
                setTipVisible(z2);
                return;
            }
            if (getCustomScrollY() != 0) {
                setPreventInvalidate(true);
                scrollTo(0, 0);
                setPreventInvalidate(false);
            }
            this.j.clearAnimation();
            this.i.setVisibility(8);
            if (q()) {
                setPreventOverScroll(false);
            }
            setTipVisible(true);
            if (this.u) {
                postDelayed(new bw(this), 500);
            }
        }
    }

    public void setNoMoreRefresh(boolean z) {
        this.k = z;
    }

    public void setOnCancelListener$135502(cv cvVar) {
        this.x = cvVar;
    }

    public void setOnPullToRefreshListener$42b903f6(bu buVar) {
        this.h = buVar;
    }

    /* access modifiers changed from: protected */
    public void setOverScrollState(int i2) {
        if (i2 != this.b) {
            switch (i2) {
                case 1:
                    this.c.setText((int) R.string.pull_to_refresh_pull_label);
                    break;
                case 2:
                    this.c.setText((int) R.string.pull_to_refresh_release_label);
                    break;
                case 3:
                    this.c.setText((int) R.string.pull_to_refresh_refreshing_label);
                    break;
                case 4:
                    this.c.setText((int) R.string.pull_to_refresh_pull_label);
                    break;
            }
            this.b = i2;
        }
    }

    public void setTimeEnable(boolean z) {
        if (z) {
            this.l.setVisibility(0);
            this.m.setVisibility(0);
            return;
        }
        this.l.setVisibility(8);
        this.m.setVisibility(8);
    }

    public void setTipVisible(boolean z) {
    }
}
