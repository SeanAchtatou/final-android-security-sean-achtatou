package com.immomo.momo.android.activity.tieba;

import android.content.Intent;
import android.net.Uri;
import android.webkit.DownloadListener;

final class av implements DownloadListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PublishTieActivity f2171a;

    av(PublishTieActivity publishTieActivity) {
        this.f2171a = publishTieActivity;
    }

    public final void onDownloadStart(String str, String str2, String str3, String str4, long j) {
        this.f2171a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
    }
}
