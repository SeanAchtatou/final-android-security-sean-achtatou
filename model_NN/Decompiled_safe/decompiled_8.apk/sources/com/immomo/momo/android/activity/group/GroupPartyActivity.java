package com.immomo.momo.android.activity.group;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.b.a;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.amap.mapapi.location.LocationManagerProxy;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.b.z;
import com.immomo.momo.android.map.AMapActivity;
import com.immomo.momo.android.map.GoogleMapActivity;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.a.at;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.service.bean.a.g;
import com.immomo.momo.service.bean.al;
import com.immomo.momo.service.bean.as;
import com.immomo.momo.service.x;
import com.immomo.momo.service.y;
import com.immomo.momo.util.ao;
import com.immomo.momo.util.j;
import java.util.ArrayList;
import java.util.List;

public class GroupPartyActivity extends ah implements View.OnClickListener {
    private View A = null;
    private View B = null;
    private View C = null;
    private View D = null;
    private View.OnClickListener E = new cf(this);
    private View.OnClickListener F = new cg(this);
    List h;
    private HeaderLayout i = null;
    private bi j = null;
    private LinearLayout k = null;
    private View l = null;
    private TextView m;
    private TextView n;
    private TextView o;
    private TextView p;
    private TextView q;
    private ImageView r;
    private at s;
    /* access modifiers changed from: private */
    public g t = null;
    /* access modifiers changed from: private */
    public String u = PoiTypeDef.All;
    private String v = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public x w = null;
    private y x;
    /* access modifiers changed from: private */
    public cs y;
    private View z = null;

    /* access modifiers changed from: private */
    public void A() {
        this.k.removeAllViews();
        this.k.setClickable(true);
        this.k.setFocusable(true);
        if (this.t.p != null && this.t.p.size() > 0) {
            for (int i2 = 0; i2 < this.t.p.size(); i2++) {
                as asVar = (as) this.t.p.get(i2);
                View inflate = LayoutInflater.from(this).inflate((int) R.layout.groupparty_member_item, (ViewGroup) null);
                ((TextView) inflate.findViewById(R.id.tv_membername)).setText(asVar.b);
                TextView textView = (TextView) inflate.findViewById(R.id.tv_phone);
                if (this.t.h == 1) {
                    textView.setVisibility(0);
                    textView.setText(asVar.c);
                    textView.setOnClickListener(this.F);
                } else {
                    textView.setVisibility(8);
                }
                inflate.setOnClickListener(this.E);
                ImageView imageView = (ImageView) inflate.findViewById(R.id.avatar_imageview);
                if (a.f(asVar.d)) {
                    j.a(new al(asVar.d), imageView, (ViewGroup) null, 3);
                } else {
                    imageView.setImageBitmap(com.immomo.momo.g.x());
                }
                Bundle bundle = new Bundle();
                bundle.putString("momoid", asVar.f2995a);
                bundle.putString("phoneNum", asVar.c);
                inflate.setTag(bundle);
                textView.setTag(bundle);
                this.k.addView(inflate);
            }
        }
        this.q.setText(String.valueOf(com.immomo.momo.g.a((int) R.string.groupparty_title2)) + "(" + this.t.p.size() + ")");
    }

    public static void a(Activity activity, String str) {
        Intent intent = new Intent(activity, GroupPartyActivity.class);
        intent.putExtra("pid", str);
        activity.startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void c(int i2) {
        Intent intent = new Intent(this, ShareGroupPartyActivity.class);
        intent.putExtra("pid", this.u);
        intent.putExtra("share_type", i2);
        startActivity(intent);
    }

    private void c(Bundle bundle) {
        if (bundle == null || !bundle.getBoolean("from_saveinstance", false)) {
            Intent intent = getIntent();
            this.u = intent.getStringExtra("pid");
            this.v = intent.getStringExtra("intent_groupname");
        } else {
            this.u = (String) bundle.get("pid");
            this.v = (String) bundle.get("intent_groupname");
        }
        x();
    }

    static /* synthetic */ void g(GroupPartyActivity groupPartyActivity) {
        com.immomo.momo.service.bean.a.a e = groupPartyActivity.x.e(groupPartyActivity.t.c);
        if (e == null ? false : e.n == 2 || e.n == 1) {
            if (groupPartyActivity.j.getParent() == null) {
                groupPartyActivity.i.a(groupPartyActivity.j, null);
            }
            groupPartyActivity.j.b(R.string.groupparty_btntext_opration).a((int) R.drawable.ic_topbar_editor);
            groupPartyActivity.j.setOnClickListener(new ci(groupPartyActivity));
        }
        if (groupPartyActivity.t.i != 1) {
            return;
        }
        if (groupPartyActivity.t.o == 1) {
            if (groupPartyActivity.t.i == 1) {
                groupPartyActivity.z.setVisibility(0);
                groupPartyActivity.A.setVisibility(8);
                groupPartyActivity.B.setVisibility(0);
            }
        } else if (groupPartyActivity.t.o == 0 && groupPartyActivity.t.i == 1) {
            groupPartyActivity.z.setVisibility(0);
            groupPartyActivity.A.setVisibility(0);
            groupPartyActivity.B.setVisibility(8);
        }
    }

    static /* synthetic */ void w() {
    }

    private void x() {
        if (!a.a((CharSequence) this.u)) {
            this.t = this.w.b(this.u);
            this.e.b((Object) ("-----------------initdata partyid:" + this.u + this.t));
            if (this.t == null) {
                this.t = new g(this.u);
            } else if (!y()) {
                return;
            }
            A();
            z();
        }
    }

    /* access modifiers changed from: private */
    public boolean y() {
        int i2 = 8;
        if (this.t == null) {
            return false;
        }
        if (this.t.i == 2) {
            this.i.setTitleText("群活动(已结束)");
        }
        if (this.t.i == 3) {
            ao.b("活动已被删除");
            finish();
            return false;
        }
        if (z.a(this.t.j, this.t.k)) {
            this.D.setVisibility(0);
            this.C.setVisibility(0);
        } else {
            this.D.setVisibility(8);
            this.C.setVisibility(8);
        }
        if (this.t.m) {
            this.r.setVisibility(0);
        } else {
            this.r.setVisibility(8);
        }
        TextView textView = this.n;
        if (!a.a((CharSequence) this.t.e)) {
            i2 = 0;
        }
        textView.setVisibility(i2);
        this.n.setText(a.a(this.t.e) ? PoiTypeDef.All : this.t.e);
        this.o.setText(String.valueOf(a.a(this.t.l / 1000.0d)) + "km");
        this.p.setText(a.a(this.t.d) ? PoiTypeDef.All : this.t.d);
        if (this.t.f != null) {
            this.m.setText(a.g(this.t.f));
        } else {
            this.m.setText(PoiTypeDef.All);
        }
        return true;
    }

    private void z() {
        b(new cl(this, this));
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_groupparty);
        this.w = new x();
        this.x = new y();
        this.i = (HeaderLayout) findViewById(R.id.layout_header);
        this.i.setTitleText("群活动");
        this.j = new bi(this);
        this.z = findViewById(R.id.layout_bottom);
        this.A = this.z.findViewById(R.id.btn_join);
        this.B = this.z.findViewById(R.id.btn_quit);
        this.k = (LinearLayout) findViewById(R.id.layout_membercontainer);
        this.m = (TextView) findViewById(R.id.tv_party_starttime);
        this.n = (TextView) findViewById(R.id.tv_party_address);
        this.o = (TextView) findViewById(R.id.tv_party_distance);
        this.p = (TextView) findViewById(R.id.tv_desc);
        this.q = (TextView) findViewById(R.id.tv_member_count);
        this.r = (ImageView) findViewById(R.id.groupparty_iv_top);
        this.A.setOnClickListener(this);
        this.B.setOnClickListener(this);
        this.l = findViewById(R.id.layout_place);
        this.l.setOnClickListener(new ch(this));
        this.C = findViewById(R.id.layout_placecontainer);
        this.D = findViewById(R.id.layout_distance_container);
        c(bundle);
    }

    /* access modifiers changed from: protected */
    public final void e() {
        super.e();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        switch (i2) {
            case 33:
                if (i3 == -1 && intent != null) {
                    this.u = intent.getStringExtra("pid");
                    if (a.f(this.u)) {
                        c(2);
                        x();
                        break;
                    }
                }
                break;
            case 34:
                if (i3 == -1 && intent != null) {
                    this.u = intent.getStringExtra("pid");
                    if (a.f(this.u)) {
                        c(1);
                        x();
                        z();
                        break;
                    }
                }
                break;
        }
        super.onActivityResult(i2, i3, intent);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_join /*2131165685*/:
                b(new cm(this, this));
                return;
            case R.id.textView_bottom /*2131165686*/:
            default:
                return;
            case R.id.btn_quit /*2131165687*/:
                b(new co(this, this));
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        setResult(-1, new Intent().putExtra("pid", this.u));
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        String str = intent.getExtras() != null ? (String) intent.getExtras().get("pid") : null;
        String str2 = intent.getExtras() != null ? (String) intent.getExtras().get("intent_groupname") : null;
        if (!a.a((CharSequence) str) && !this.u.equals(str)) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("from_saveinstance", true);
            bundle.putString("pid", str);
            bundle.putString("intent_groupname", str2);
            c(bundle);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putBoolean("from_saveinstance", true);
        bundle.putString("pid", this.u);
        bundle.putString("intent_groupname", this.v);
        super.onSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public final void u() {
        if (z.a(this.t.j, this.t.k)) {
            try {
                Class.forName("com.google.android.maps.MapActivity");
                Intent intent = new Intent(this, GoogleMapActivity.class);
                intent.putExtra("latitude", this.t.j);
                intent.putExtra("longitude", this.t.k);
                startActivity(intent);
            } catch (Exception e) {
                Location location = new Location(LocationManagerProxy.GPS_PROVIDER);
                location.setLatitude(this.t.j);
                location.setLongitude(this.t.k);
                if (z.a(location)) {
                    Intent intent2 = new Intent(this, AMapActivity.class);
                    intent2.putExtra("latitude", this.t.j);
                    intent2.putExtra("longitude", this.t.k);
                    startActivity(intent2);
                    return;
                }
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://maps.google.com/maps?q=" + this.t.j + "," + this.t.k)));
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void v() {
        if (this.s == null || !this.s.g()) {
            this.h = new ArrayList();
            if (this.t.m) {
                this.h.add("取消置顶");
            } else {
                this.h.add("置顶");
            }
            if (this.t.o == 2) {
                this.h.add("编辑活动");
            }
            this.h.add("通知群成员");
            this.h.add("删除活动");
            this.s = new at(this, this.j, (String[]) this.h.toArray(new String[0]));
            this.s.a(new cj(this));
            this.s.d();
        }
    }
}
