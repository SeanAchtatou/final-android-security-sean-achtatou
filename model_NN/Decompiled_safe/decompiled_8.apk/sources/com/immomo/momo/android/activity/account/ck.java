package com.immomo.momo.android.activity.account;

import android.support.v4.b.a;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.mapapi.poisearch.PoiTypeDef;
import com.immomo.momo.R;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.bf;
import com.immomo.momo.util.e;
import com.immomo.momo.util.k;
import com.immomo.momo.util.m;
import java.util.Date;
import java.util.Timer;

public final class ck extends ab {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public m f990a = new m("test_momo", "[ -- StepSendMessageCode -- ]");
    /* access modifiers changed from: private */
    public Timer b = null;
    /* access modifiers changed from: private */
    public bf c;
    private TextView d;
    private Button e;
    /* access modifiers changed from: private */
    public TextView f;
    private View g;
    /* access modifiers changed from: private */
    public RegisterActivityWithP h;
    private Animation i;

    public ck(bf bfVar, View view, RegisterActivityWithP registerActivityWithP) {
        super(view);
        new Date();
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.c = bfVar;
        this.h = registerActivityWithP;
        registerActivityWithP.c(RegisterActivityWithP.j);
    }

    private void h() {
        if (this.b == null) {
            this.b = new Timer();
            this.b.schedule(new cm(this), 0, 5000);
        }
    }

    public final boolean a() {
        return this.c.f;
    }

    public final void b() {
        if (!a.f(this.h.q) || !a.f(this.h.p)) {
            this.h.b(new cp(this, this.h));
        } else {
            g();
        }
    }

    public final void c() {
        if (a()) {
            this.h.s = false;
            this.h.t = false;
            this.h.u();
        } else if (this.b == null) {
            this.h.a((CharSequence) "请先发送短信进行验证");
            h();
        } else {
            this.h.a((CharSequence) "正在验证，请稍候...");
            h();
        }
    }

    public final void d() {
        if (this.b != null) {
            this.b.cancel();
            this.b = null;
        }
        this.h.c(2);
    }

    public final void e() {
        new k("PI", "P1221").e();
    }

    public final void f() {
        new k("PO", "P1221").e();
    }

    /* access modifiers changed from: protected */
    public final void g() {
        this.d = (TextView) a((int) R.id.rg_tv_info);
        TextView textView = this.d;
        String str = this.h.q;
        String str2 = this.h.p;
        String str3 = "编辑短信内容 " + str + " 发送到 " + str2;
        int indexOf = str3.indexOf(str2);
        int indexOf2 = str3.indexOf(str);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(str3);
        spannableStringBuilder.setSpan(new ForegroundColorSpan(g.c((int) R.color.blue)), indexOf, str2.length() + indexOf, 33);
        spannableStringBuilder.setSpan(new ForegroundColorSpan(g.c((int) R.color.blue)), indexOf2, str.length() + indexOf2, 33);
        textView.setText(spannableStringBuilder);
        a((int) R.id.rg_tv_waittime);
        this.f = (TextView) a((int) R.id.rg_link_nocode);
        this.g = a((int) R.id.rg_layout_loading);
        ImageView imageView = (ImageView) this.g.findViewById(R.id.rg_iv_loading);
        if (imageView.getDrawable() != null && this.i == null) {
            this.i = AnimationUtils.loadAnimation(this.h, R.anim.loading);
            imageView.startAnimation(this.i);
        }
        this.e = (Button) a((int) R.id.rg_bt_sendmsg);
        this.e.setOnClickListener(new cl(this));
        this.f.setEnabled(true);
        this.f.setText("我已收到验证码");
        this.f.setVisibility(0);
        SpannableStringBuilder spannableStringBuilder2 = new SpannableStringBuilder(this.f.getText());
        spannableStringBuilder2.setSpan(new co(this), 0, 7, 33);
        this.f.setMovementMethod(LinkMovementMethod.getInstance());
        this.f.setText(spannableStringBuilder2);
        e.a(this.f, 0, 7);
        if (this.h.s) {
            this.f.setText(PoiTypeDef.All);
        }
        h();
    }
}
