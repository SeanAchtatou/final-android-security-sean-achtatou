package com.immomo.momo.android.activity.group;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.R;
import com.immomo.momo.android.broadcast.u;
import com.immomo.momo.android.broadcast.w;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.n;
import com.immomo.momo.service.y;
import com.immomo.momo.util.ao;

/* renamed from: com.immomo.momo.android.activity.group.do  reason: invalid class name */
final class Cdo extends d {

    /* renamed from: a  reason: collision with root package name */
    private v f1611a = null;
    private String c;
    private /* synthetic */ GroupProfileActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Cdo(GroupProfileActivity groupProfileActivity, Context context, String str) {
        super(context);
        this.d = groupProfileActivity;
        this.c = str;
        this.f1611a = new v(context);
        this.f1611a.setCancelable(true);
        this.f1611a.setOnCancelListener(new dp(this));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        n.a().e(this.d.l, this.c);
        y yVar = new y();
        yVar.b(this.d.f.h, this.d.l);
        yVar.c(this.d.l, 5);
        return true;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.d.a(this.f1611a);
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        Boolean bool = (Boolean) obj;
        super.a(bool);
        if (bool.booleanValue()) {
            ao.g(R.string.group_setting_dismiss_sucess);
            Intent intent = new Intent(u.b);
            intent.putExtra("gid", this.d.l);
            this.d.sendBroadcast(intent);
            this.d.sendBroadcast(new Intent(w.b));
            this.d.setResult(-1);
            this.d.finish();
            return;
        }
        ao.g(R.string.group_setting_quit_failed);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.d.p();
    }
}
