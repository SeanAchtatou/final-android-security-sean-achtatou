package com.immomo.momo.android.activity.contacts;

import com.immomo.momo.android.a.hf;

final class br implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bl f1172a;
    private final /* synthetic */ String b;

    br(bl blVar, String str) {
        this.f1172a = blVar;
        this.b = str;
    }

    public final void run() {
        int a2 = this.f1172a.f1166a.P.a(this.b, 0);
        if (a2 < 0 || a2 > this.f1172a.f1166a.P.getCount()) {
            this.f1172a.f1166a.Q = this.f1172a.f1166a.T.c();
            this.f1172a.f1166a.aa.post(new bs(this));
            return;
        }
        hf hfVar = (hf) this.f1172a.f1166a.P.getItem(this.f1172a.f1166a.P.a(this.b, 0));
        if (hfVar != null && hfVar.f != null) {
            this.f1172a.f1166a.T.a(hfVar.f, this.b);
            this.f1172a.f1166a.aa.post(new bt(this));
        }
    }
}
