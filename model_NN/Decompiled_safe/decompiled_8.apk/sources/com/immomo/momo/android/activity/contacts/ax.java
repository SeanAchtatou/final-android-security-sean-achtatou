package com.immomo.momo.android.activity.contacts;

import android.content.Intent;
import android.support.v4.b.a;
import com.immomo.momo.android.broadcast.d;

final class ax implements d {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ aw f1154a;

    ax(aw awVar) {
        this.f1154a = awVar;
    }

    public final void a(Intent intent) {
        String str = intent.getExtras() != null ? (String) intent.getExtras().get("momoid") : null;
        if (!a.a((CharSequence) str) && this.f1154a.S != null) {
            new Thread(new ay(this, str)).start();
        }
    }
}
