package com.immomo.momo.android.activity.setting;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import com.immomo.momo.R;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.NumberPicker;
import com.immomo.momo.service.bean.at;

public class SettingMutetimeActivity extends ah implements View.OnClickListener {
    private HeaderLayout h = null;
    private int i = -1;
    private int j = -1;
    private boolean k = true;
    private View l = null;
    /* access modifiers changed from: private */
    public NumberPicker m = null;
    /* access modifiers changed from: private */
    public NumberPicker n = null;
    private CheckBox o = null;
    /* access modifiers changed from: private */
    public at p = null;
    /* access modifiers changed from: private */
    public View q;

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_setting_mutetime);
        this.h = (HeaderLayout) findViewById(R.id.layout_header);
        this.h.setTitleText("静音时段提醒");
        this.q = findViewById(R.id.layout_numberpicker);
        this.l = findViewById(R.id.timepicker_layout_mutetime);
        this.m = (NumberPicker) findViewById(R.id.timepicker_np_start);
        this.n = (NumberPicker) findViewById(R.id.timepicker_np_end);
        this.o = (CheckBox) findViewById(R.id.timepicker_cb_mutetime);
        this.m.a();
        this.n.a();
        d();
        this.l.setOnClickListener(this);
        this.o.setOnCheckedChangeListener(new u(this));
        v vVar = new v(this);
        this.m.setOnChangeListener(vVar);
        this.n.setOnChangeListener(vVar);
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.p = this.g;
        this.i = this.p.b().intValue();
        this.j = this.p.c().intValue();
        this.k = this.p.h;
        this.o.setChecked(this.k);
        this.q.setVisibility(this.k ? 0 : 8);
        this.m.setCurrent(this.i);
        this.n.setCurrent(this.j);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.timepicker_layout_mutetime /*2131165903*/:
                this.o.setChecked(!this.o.isChecked());
                return;
            default:
                return;
        }
    }
}
