package com.immomo.momo.android.activity.setting;

import android.content.Context;
import com.immomo.a.a.f.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.plugin.e.b;
import com.immomo.momo.service.bean.c;
import com.immomo.momo.util.ao;
import com.sina.weibo.sdk.WeiboSDK;
import com.sina.weibo.sdk.api.IWeiboAPI;
import com.weibo.sdk.android.Weibo;
import com.weibo.sdk.android.sso.SsoHandler;

final class i extends d {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ CommunityBindActivity f2132a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public i(CommunityBindActivity communityBindActivity, Context context) {
        super(context);
        this.f2132a = communityBindActivity;
        if (communityBindActivity.s != null) {
            communityBindActivity.s.cancel(true);
        }
        communityBindActivity.s = this;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return com.immomo.momo.protocol.a.d.b();
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.f2132a.r = new v(this.f2132a, "正在请求数据，请稍候...");
        this.f2132a.r.setOnCancelListener(new j(this));
        this.f2132a.a(this.f2132a.r);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        c cVar = (c) obj;
        super.a(cVar);
        if (cVar == null || a.a(cVar.f3015a) || a.a(cVar.b)) {
            ao.a((CharSequence) "获取验证信息失败，请稍候尝试");
            return;
        }
        this.f2132a.h = cVar.f3015a;
        this.f2132a.i = cVar.b;
        IWeiboAPI createWeiboAPI = WeiboSDK.createWeiboAPI(this.f2132a, cVar.f3015a);
        WeiboSDK.createWeiboAPI(this.f2132a, cVar.f3015a, false);
        if (!createWeiboAPI.isWeiboAppInstalled() || !createWeiboAPI.isWeiboAppSupportAPI()) {
            this.f2132a.u();
            return;
        }
        this.f2132a.v = true;
        new SsoHandler(this.f2132a, Weibo.getInstance(cVar.f3015a, b.f2872a, "email,direct_messages_read,direct_messages_write,friendships_groups_read,friendships_groups_write,statuses_to_me_read,invitation_write")).authorize(new b(this.f2132a), null);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.f2132a.p();
    }
}
