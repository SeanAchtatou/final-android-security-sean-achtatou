package com.immomo.momo.android.activity.emotestore;

import android.content.Context;
import android.content.Intent;
import com.immomo.momo.a.a;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.a.f;
import com.immomo.momo.protocol.a.i;
import com.immomo.momo.service.aq;

final class z extends d {

    /* renamed from: a  reason: collision with root package name */
    private String f1337a = null;
    private String c;
    private /* synthetic */ EmotionProfileActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public z(EmotionProfileActivity emotionProfileActivity, Context context, String str, String str2) {
        super(context);
        this.d = emotionProfileActivity;
        this.c = str2;
        this.f1337a = str;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        f a2 = i.a().a(this.d.j, this.f1337a, this.c);
        this.d.f.b((long) a2.b);
        new aq().b(this.d.f);
        return a2.f2885a;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.d.a(new v(f(), this));
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        if (!(exc instanceof a) || ((a) exc).f670a != 20405) {
            super.a(exc);
        } else {
            EmotionProfileActivity.d(this.d);
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        String str = (String) obj;
        if (!android.support.v4.b.a.a((CharSequence) str)) {
            a(str);
        }
        this.d.sendBroadcast(new Intent(com.immomo.momo.android.broadcast.z.b));
        if (this.d.k) {
            this.d.setResult(-1);
            this.d.finish();
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.d.p();
    }
}
