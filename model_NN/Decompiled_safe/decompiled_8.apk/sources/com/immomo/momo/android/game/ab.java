package com.immomo.momo.android.game;

import java.util.HashMap;
import mm.purchasesdk.OnPurchaseListener;
import mm.purchasesdk.Purchase;

public final class ab implements OnPurchaseListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ y f2399a;

    public ab(y yVar) {
        this.f2399a = yVar;
    }

    public final void onAfterApply() {
        this.f2399a.L.a((Object) "onAfterApply");
    }

    public final void onAfterDownload() {
        this.f2399a.L.a((Object) "onAfterDownload");
    }

    public final void onBeforeApply() {
        this.f2399a.L.a((Object) "onBeforeApply");
    }

    public final void onBeforeDownload() {
        this.f2399a.L.a((Object) "onBeforeDownload");
    }

    public final void onBillingFinish(int i, HashMap hashMap) {
        String str;
        this.f2399a.L.a((Object) ("billing finish, status code = " + i));
        String str2 = "订购结果：订购成功";
        if (i == 102 || i == 104) {
            if (hashMap != null) {
                String str3 = (String) hashMap.get(OnPurchaseListener.LEFTDAY);
                if (!(str3 == null || str3.trim().length() == 0)) {
                    str2 = String.valueOf(str2) + ",剩余时间 ： " + str3;
                }
                String str4 = (String) hashMap.get(OnPurchaseListener.ORDERID);
                if (!(str4 == null || str4.trim().length() == 0)) {
                    str2 = String.valueOf(str2) + ",OrderID ： " + str4;
                }
                String str5 = (String) hashMap.get(OnPurchaseListener.PAYCODE);
                if (!(str5 == null || str5.trim().length() == 0)) {
                    str2 = String.valueOf(str2) + ",Paycode:" + str5;
                }
                String str6 = (String) hashMap.get(OnPurchaseListener.TRADEID);
                str = (str6 == null || str6.trim().length() == 0) ? str2 : String.valueOf(str2) + ",tradeID:" + str6;
                String str7 = (String) hashMap.get(OnPurchaseListener.ORDERTYPE);
                if (!(str6 == null || str6.trim().length() == 0)) {
                    str = String.valueOf(str) + ",ORDERTYPE:" + str7;
                }
            } else {
                str = str2;
            }
            y.c(this.f2399a);
        } else {
            str = "订购结果：" + Purchase.getReason(i);
        }
        this.f2399a.L.a((Object) str);
    }

    public final void onInitFinish(int i) {
        this.f2399a.N();
        String reason = Purchase.getReason(i);
        this.f2399a.L.a((Object) ("Init finish, status code = " + i + ", result=" + reason));
        if (i == 100) {
            this.f2399a.O();
            return;
        }
        this.f2399a.R.i = false;
        this.f2399a.b(reason);
    }

    public final void onQueryFinish(int i, HashMap hashMap) {
    }

    public final void onUnsubscribeFinish(int i) {
    }
}
