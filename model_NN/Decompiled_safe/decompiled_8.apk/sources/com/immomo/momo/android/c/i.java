package com.immomo.momo.android.c;

import com.immomo.momo.service.bean.q;
import com.immomo.momo.util.m;
import java.io.File;

public class i extends t {
    private static u b = new u(3, 3);

    /* renamed from: a  reason: collision with root package name */
    private m f2384a;
    private String c;
    private String d;

    public i(String str, String str2, g gVar) {
        this(str, str2, gVar, (byte) 0);
    }

    public i(String str, String str2, g gVar, byte b2) {
        super(gVar);
        this.f2384a = new m(this);
        this.c = str;
        this.d = str2;
    }

    public final void a() {
        b.execute(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.immomo.momo.protocol.a.i.a(java.lang.String, java.lang.String, java.io.File, boolean):void
     arg types: [java.lang.String, java.lang.String, java.io.File, int]
     candidates:
      com.immomo.momo.protocol.a.p.a(java.lang.String, java.util.Map, com.immomo.momo.protocol.a.l[], java.util.Map):java.lang.String
      com.immomo.momo.protocol.a.i.a(java.lang.String, java.lang.String, java.io.File, boolean):void */
    public void run() {
        File a2 = q.a(this.c, this.d);
        File file = new File(a2.getParentFile(), new StringBuilder(String.valueOf(System.currentTimeMillis())).toString());
        try {
            file.createNewFile();
            com.immomo.momo.protocol.a.i.a().a(this.d, this.c, file, false);
            a2.delete();
            file.renameTo(a2);
            a(a2);
        } catch (Throwable th) {
            this.f2384a.a(th);
            if (a2.exists()) {
                a2.delete();
            }
            if (file.exists()) {
                file.delete();
            }
            a((Object) null);
        }
    }
}
