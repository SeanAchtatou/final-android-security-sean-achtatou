package com.immomo.momo.android.a;

import android.view.View;

final class ck implements View.OnLongClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ch f756a;
    private final /* synthetic */ int b;

    ck(ch chVar, int i) {
        this.f756a = chVar;
        this.b = i;
    }

    public final boolean onLongClick(View view) {
        if (this.f756a.e.getOnItemLongClickListenerInWrapper() == null) {
            return false;
        }
        return this.f756a.e.getOnItemLongClickListenerInWrapper().onItemLongClick(this.f756a.e, view, this.b, (long) view.getId());
    }
}
