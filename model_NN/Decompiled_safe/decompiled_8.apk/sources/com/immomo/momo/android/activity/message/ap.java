package com.immomo.momo.android.activity.message;

import android.content.DialogInterface;
import android.view.View;
import com.immomo.momo.android.view.a.n;
import com.immomo.momo.android.view.a.o;

final class ap implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ ChatActivity f1923a;

    ap(ChatActivity chatActivity) {
        this.f1923a = chatActivity;
    }

    public final void onClick(View view) {
        if (!this.f1923a.E) {
            n.a(this.f1923a, "成为好友之后才能赠送", "确定", (DialogInterface.OnClickListener) null).show();
        } else {
            o oVar = new o(this.f1923a, new String[]{"赠送表情", "赠送会员"});
            oVar.a(new aq(this));
            oVar.show();
        }
        this.f1923a.C();
    }
}
