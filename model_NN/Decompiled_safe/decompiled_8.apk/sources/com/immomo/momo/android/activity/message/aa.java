package com.immomo.momo.android.activity.message;

import com.immomo.momo.MomoApplication;
import com.immomo.momo.g;
import com.immomo.momo.service.af;
import com.immomo.momo.service.bean.Message;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

final class aa extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private BlockingQueue f1909a = new LinkedBlockingQueue();
    private /* synthetic */ a b;

    aa(a aVar) {
        this.b = aVar;
    }

    private void b(Message message) {
        try {
            af.c().a().beginTransaction();
            af.c().a(message);
            if (message.contentType != 2) {
                g.d();
                MomoApplication.a(message);
            }
            af.c().a().setTransactionSuccessful();
        } catch (Exception e) {
            message.status = 3;
            this.b.h.a((Throwable) e);
        } finally {
            af.c().a().endTransaction();
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(Message message) {
        try {
            this.f1909a.put(message);
        } catch (InterruptedException e) {
        }
    }

    public final void run() {
        while (true) {
            try {
                Message message = (Message) this.f1909a.take();
                if (message instanceof af) {
                    break;
                }
                b(message);
            } catch (InterruptedException e) {
            }
        }
        if (!this.f1909a.isEmpty()) {
            while (true) {
                Message message2 = (Message) this.f1909a.poll();
                if (message2 != null) {
                    b(message2);
                } else {
                    return;
                }
            }
        }
    }
}
