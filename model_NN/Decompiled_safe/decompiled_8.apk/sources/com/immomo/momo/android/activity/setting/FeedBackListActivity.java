package com.immomo.momo.android.activity.setting;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListAdapter;
import com.immomo.momo.R;
import com.immomo.momo.android.a.cq;
import com.immomo.momo.android.activity.ao;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.MomoRefreshListView;
import com.immomo.momo.android.view.bi;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.g;
import java.util.ArrayList;
import java.util.List;

public class FeedBackListActivity extends ao implements View.OnClickListener, bu {
    private Button h = null;
    /* access modifiers changed from: private */
    public MomoRefreshListView i = null;
    /* access modifiers changed from: private */
    public List j = null;
    /* access modifiers changed from: private */
    public cq k = null;
    /* access modifiers changed from: private */
    public LoadingButton l = null;

    public final void b_() {
        this.i.setLoadingViewText(R.string.pull_to_refresh_refreshing_label);
        b(new t(this, this, true));
    }

    /* access modifiers changed from: protected */
    public final void d() {
        super.d();
        this.j = new ArrayList();
        this.k = new cq(this, this.j, this.i);
        this.i.setAdapter((ListAdapter) this.k);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i3 == -1) {
            this.i.l();
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_feedback /*2131165628*/:
                startActivityForResult(new Intent(this, FeedBackActivity.class), 0);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_feedbacklist);
        m().setTitleText("意见反馈");
        bi biVar = new bi(this);
        biVar.a((int) R.drawable.ic_topbar_about);
        m().a(biVar, new s(this));
        this.h = (Button) findViewById(R.id.btn_feedback);
        this.i = (MomoRefreshListView) findViewById(R.id.lv_feedback);
        this.i.setFastScrollEnabled(false);
        this.i.a(g.o().inflate((int) R.layout.include_feedback_listempty, (ViewGroup) null));
        this.i.setEnableLoadMoreFoolter(true);
        this.l = this.i.getFooterViewButton();
        this.l.setVisibility(8);
        d();
        this.h.setOnClickListener(this);
        this.i.setOnPullToRefreshListener$42b903f6(this);
        this.i.setOnCancelListener$135502(new q(this));
        this.l.setOnProcessListener(new r(this));
        this.i.l();
    }
}
