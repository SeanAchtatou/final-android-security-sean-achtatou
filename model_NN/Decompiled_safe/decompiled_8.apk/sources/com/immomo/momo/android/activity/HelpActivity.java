package com.immomo.momo.android.activity;

import android.os.Bundle;
import com.immomo.momo.R;

public class HelpActivity extends as {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.h.setHorizontalScrollBarEnabled(false);
        this.h.setVerticalScrollBarEnabled(false);
        this.h.post(new ew(this));
        this.h.getSettings().setCacheMode(2);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        AboutTabsActivity.f921a.setTitleText((int) R.string.abouts_userhelp);
    }
}
