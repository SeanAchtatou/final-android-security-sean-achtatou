package com.immomo.momo.android.a;

import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

final class fb implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ es f813a;
    private final /* synthetic */ EditText b;

    fb(es esVar, EditText editText) {
        this.f813a = esVar;
        this.b = editText;
    }

    public final void run() {
        ((InputMethodManager) this.f813a.d.getSystemService("input_method")).showSoftInput(this.b, 1);
        this.b.requestFocus();
    }
}
