package com.immomo.momo.android.activity.group.foundgroup;

import android.content.Context;
import com.immomo.momo.R;
import com.immomo.momo.android.view.a.v;
import com.immomo.momo.protocol.a.n;
import com.immomo.momo.service.bean.a.h;

final class d extends com.immomo.momo.android.c.d {

    /* renamed from: a  reason: collision with root package name */
    private v f1663a = null;
    /* access modifiers changed from: private */
    public /* synthetic */ b c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public d(b bVar, Context context) {
        super(context);
        this.c = bVar;
        this.f1663a = new v(bVar.f1662a, (int) R.string.str_permissions_verification);
        this.f1663a.setOnCancelListener(new e(this));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Object... objArr) {
        return n.a().b();
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f1663a.show();
    }

    /* access modifiers changed from: protected */
    public final void a(Exception exc) {
        super.a(exc);
        this.c.f1662a.finish();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        h hVar = (h) obj;
        "GetUserPermissions  onTaskSuccess method is called" + hVar;
        b.a(this.c, hVar);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.f1663a.dismiss();
    }
}
