package com.immomo.momo.android.view;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.immomo.momo.R;
import com.immomo.momo.android.a.a;
import com.immomo.momo.g;
import com.immomo.momo.service.bean.al;
import com.immomo.momo.util.j;

final class ca extends a {
    private int d = 0;
    private /* synthetic */ MultiImageView e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ca(MultiImageView multiImageView, Context context) {
        super(context);
        this.e = multiImageView;
        this.d = (multiImageView.getWidth() - ((multiImageView.getNumColumns() - 1) * multiImageView.getHorizontalSpacing())) / multiImageView.getNumColumns();
    }

    public final int getCount() {
        return this.e.e.length;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        cc ccVar;
        if (view == null) {
            cc ccVar2 = new cc((byte) 0);
            view = g.o().inflate((int) R.layout.listitem_view_multiimage, (ViewGroup) null);
            ccVar2.f2758a = (ImageView) view.findViewById(R.id.headerwall_iv_header);
            ccVar2.b = view.findViewById(R.id.headerwall_iv_cover);
            ccVar2.b.setOnClickListener(this.e);
            ViewGroup.LayoutParams layoutParams = ccVar2.f2758a.getLayoutParams();
            if (this.e.getColumnWidth() > 0) {
                layoutParams.height = this.e.getColumnWidth();
                layoutParams.width = this.e.getColumnWidth();
            } else {
                layoutParams.height = this.d;
                layoutParams.width = this.d;
            }
            ccVar2.f2758a.setLayoutParams(layoutParams);
            ccVar2.b.setLayoutParams(layoutParams);
            ccVar2.c = new al();
            view.setTag(ccVar2);
            ccVar = ccVar2;
        } else {
            ccVar = (cc) view.getTag();
        }
        ccVar.c.setImageId(this.e.e[i]);
        ccVar.b.setTag(Integer.valueOf(i));
        j.a(ccVar.c, ccVar.f2758a, (ViewGroup) null, 15);
        return view;
    }
}
