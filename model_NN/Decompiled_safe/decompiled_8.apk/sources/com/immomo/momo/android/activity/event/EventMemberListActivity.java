package com.immomo.momo.android.activity.event;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.a.b;
import android.view.View;
import android.widget.ListAdapter;
import com.immomo.momo.R;
import com.immomo.momo.android.a.ch;
import com.immomo.momo.android.activity.ah;
import com.immomo.momo.android.broadcast.s;
import com.immomo.momo.android.c.d;
import com.immomo.momo.android.view.HeaderLayout;
import com.immomo.momo.android.view.LoadingButton;
import com.immomo.momo.android.view.MomoRefreshListView;
import com.immomo.momo.android.view.bl;
import com.immomo.momo.android.view.bu;
import com.immomo.momo.android.view.cv;
import com.immomo.momo.g;
import com.immomo.momo.service.r;
import com.immomo.momo.util.k;
import com.immomo.momo.util.u;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventMemberListActivity extends ah implements b, View.OnClickListener, bl, bu, cv {
    private HeaderLayout h;
    /* access modifiers changed from: private */
    public MomoRefreshListView i;
    /* access modifiers changed from: private */
    public List j = null;
    /* access modifiers changed from: private */
    public r k;
    /* access modifiers changed from: private */
    public ch l;
    private s m;
    /* access modifiers changed from: private */
    public String n;
    /* access modifiers changed from: private */
    public d o;
    /* access modifiers changed from: private */
    public d p;
    /* access modifiers changed from: private */
    public LoadingButton q = null;
    /* access modifiers changed from: private */
    public Map r = null;
    /* access modifiers changed from: private */
    public boolean s;
    private com.immomo.momo.android.broadcast.d t;

    public EventMemberListActivity() {
        new Handler();
        this.s = false;
        this.t = new ae();
    }

    static /* synthetic */ void j(EventMemberListActivity eventMemberListActivity) {
        if (!eventMemberListActivity.s) {
            eventMemberListActivity.q.setVisibility(8);
        } else {
            eventMemberListActivity.q.setVisibility(0);
        }
    }

    /* access modifiers changed from: private */
    public void w() {
        this.l = new ch(this, this.j, this.i);
        this.i.setAdapter((ListAdapter) this.l);
    }

    /* access modifiers changed from: private */
    public void x() {
        this.q.e();
        this.i.n();
    }

    /* access modifiers changed from: protected */
    public final void a(Bundle bundle) {
        super.a(bundle);
        setContentView((int) R.layout.activity_eventmemberlist);
        this.k = new r();
        this.m = new s(this);
        this.m.a(this.t);
        this.i = (MomoRefreshListView) findViewById(R.id.listview);
        this.i.setEnableLoadMoreFoolter(true);
        this.q = this.i.getFooterViewButton();
        this.q.setVisibility(8);
        this.h = (HeaderLayout) findViewById(R.id.layout_header);
        this.h.setTitleText(String.format(g.a((int) R.string.groupmember_list_header_title), Integer.valueOf(getIntent().getIntExtra("count", 0))));
        if (bundle == null || !bundle.getBoolean("from_saveinstance", false)) {
            this.n = getIntent().getStringExtra("eventid");
        } else {
            this.n = (String) bundle.get("eventid");
        }
        this.r = new HashMap();
        r rVar = this.k;
        String str = this.n;
        this.j = u.c(new StringBuilder(String.valueOf(str)).append("member").toString()) ? (List) u.b(String.valueOf(str) + "member") : new ArrayList();
        w();
        this.i.l();
        this.i.setOnPullToRefreshListener$42b903f6(this);
        this.q.setOnProcessListener(this);
        this.i.setOnItemClickListener(new af(this));
    }

    public final void b_() {
        if (this.o != null && !this.o.isCancelled()) {
            this.o.cancel(true);
        }
        this.o = new ag(this, this);
        this.o.execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public final void d() {
    }

    public void onBackPressed() {
        super.onBackPressed();
    }

    public void onClick(View view) {
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.m != null) {
            unregisterReceiver(this.m);
            this.m = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        new k("PO", "P7111").e();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        new k("PI", "P7111").e();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putBoolean("from_saveinstance", true);
        bundle.putString("eventid", this.n);
        super.onSaveInstanceState(bundle);
    }

    public final void u() {
        this.q.f();
        this.p = new ah(this, this);
        this.p.execute(new Object[0]);
    }

    public final void v() {
        x();
        if (this.o != null && !this.o.isCancelled()) {
            this.o.cancel(true);
        }
    }
}
